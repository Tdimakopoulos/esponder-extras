/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package weatherserver;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MEOC-WS1
 */
public class WeatherServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ServerSocket welcomeSocket = new ServerSocket(7887);

            while (true) {
                Socket connectionSocket = welcomeSocket.accept();
                if (connectionSocket != null) {
                    Server TCPSrv = new Server(connectionSocket);
                    TCPSrv.start();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(WeatherServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

class Server extends Thread {

    private Socket connectionSocket;
    private String clientSentence;
    private String capitalizedSentence;
    private BufferedReader inFromClient;
    private DataOutputStream outToClient;

    public Server(Socket c) throws IOException {
        connectionSocket = c;
    }

    public void run() {
        //  int i=10;
        try {
            while (true) {
                System.out.println("Initialize ");
                inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                outToClient = new DataOutputStream(connectionSocket.getOutputStream());
                System.out.println("Wait Input");

                readValues pp = new readValues();
                pp.read();

                SNMP4JHelper snmp= new SNMP4JHelper();
                snmp.readvalues();
                
                capitalizedSentence = "speed" + pp.getTemp();
                outToClient.writeBytes(capitalizedSentence);
             //   Thread.sleep(500);
                System.out.println("Send : "+capitalizedSentence);

                capitalizedSentence = "dir" + pp.getsDir().toLowerCase();
                outToClient.writeBytes(capitalizedSentence);
             //   Thread.sleep(500);
                System.out.println("Send : "+capitalizedSentence);
                
                capitalizedSentence = "temp" + snmp.getTemp().trim();
                outToClient.writeBytes(capitalizedSentence);
             //   Thread.sleep(500);
System.out.println("Send : "+capitalizedSentence);

                capitalizedSentence = "hum" + snmp.getHum().trim();
                outToClient.writeBytes(capitalizedSentence);
                //Thread.sleep(500);
System.out.println("Send : "+capitalizedSentence);

                Thread.sleep(3500);

            }
        } catch (IOException e) {
            System.out.println("Errore: " + e);
        } catch (InterruptedException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}