package eu.swanicare.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ClipData;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import eu.swanicare.R;
import eu.swanicare.adapters.MedicineExpandableListAdapter;
import eu.swanicare.helperclasses.MedicationEntitySchedule;

public class Medication extends Activity {

	public static final String ActivityTag = "Medication Activity";
	public static final String DragListenerTag = "Medication OnDragListener";

	ImageView deleteButton;
	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;
	List<String> listDataHeader;
	HashMap<String, List<MedicationEntitySchedule>> listDataChild;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.medicinelayout);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		TextView activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Medication");

		deleteButton = (ImageView) findViewById(R.id.deleteMedication);
		deleteButton.setOnDragListener(new DragListener());

		// get the listview
		expListView = (ExpandableListView) findViewById(R.id.expandableMedicineList);

		// preparing list data
		prepareListData();

		listAdapter = new MedicineExpandableListAdapter(this, listDataHeader, listDataChild);

		// setting list adapter
		expListView.setAdapter(listAdapter);
	}

	/*
	 * Preparing the list data
	 */
	private void prepareListData() {
		listDataHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<MedicationEntitySchedule>>();

		// Adding child data
		listDataHeader.add("Depon");
		listDataHeader.add("Paracetamol");
		listDataHeader.add("Fenistil");

		// Adding child data
		List<MedicationEntitySchedule> deponChildList = new ArrayList<MedicationEntitySchedule>();
		MedicationEntitySchedule child1 = new MedicationEntitySchedule("Depon", "All days", "09:00 am", true);
		MedicationEntitySchedule child2 = new MedicationEntitySchedule("Depon", "Mon, Wed, Fri", "21:00 pm", false);
		deponChildList.add(child1);
		deponChildList.add(child2);

		List<MedicationEntitySchedule> paracetamolList = new ArrayList<MedicationEntitySchedule>();
		MedicationEntitySchedule child3 = new MedicationEntitySchedule("Paracetamol", "Tue, Thur, Sat", "11:00 am", false);
		MedicationEntitySchedule child4 = new MedicationEntitySchedule("Paracetamol", "All days", "21:00 pm", true);
		paracetamolList.add(child3);
		paracetamolList.add(child4);

		List<MedicationEntitySchedule> fenistilList = new ArrayList<MedicationEntitySchedule>();
		MedicationEntitySchedule child5 = new MedicationEntitySchedule("Fenistil", "All days", "09:00 am", true);
		MedicationEntitySchedule child6 = new MedicationEntitySchedule("Fenistil", "Mon, Wed, Fri", "21:00 pm", false);
		fenistilList.add(child5);
		fenistilList.add(child6);

		listDataChild.put(listDataHeader.get(0), deponChildList); // Header, Child data
		listDataChild.put(listDataHeader.get(1), paracetamolList);
		listDataChild.put(listDataHeader.get(2), fenistilList);
	}




	//	private void initialiseArrayOfDayPicker() {
	//		
	//		ToggleButton monButton = (ToggleButton) findViewById(R.id.toggleButton1);
	//		ToggleButton tueButton = (ToggleButton) findViewById(R.id.toggleButton2);
	//		ToggleButton wedButton = (ToggleButton) findViewById(R.id.toggleButton3);
	//		ToggleButton thuButton = (ToggleButton) findViewById(R.id.toggleButton4);
	//		ToggleButton friButton = (ToggleButton) findViewById(R.id.toggleButton5);
	//		ToggleButton satButton = (ToggleButton) findViewById(R.id.toggleButton6);
	//		ToggleButton sunButton = (ToggleButton) findViewById(R.id.toggleButton7);
	//		
	//		dayButtons[0] = monButton;
	//		dayButtons[1] = tueButton;
	//		dayButtons[2] = wedButton;
	//		dayButtons[3] = thuButton;
	//		dayButtons[4] = friButton;
	//		dayButtons[5] = satButton;
	//		dayButtons[6] = sunButton;
	//		
	//	}


	//	private int[] getDayPickerValue() {
	//		
	//		int values[] = new int[7];
	//		for(int i=0;i<7;i++) {
	//			if(dayButtons[i].isChecked())
	//				values[i] = 1;
	//			else
	//				values[i] = 0;
	//		}
	//		
	//		return values;
	//		
	//	}


	// If it implemented as  a new interface, then several problems arise
	// The DROP and ENTERED events are not captured at all. Instead, we implement
	// this as an inner class that implements the interface.
	class DragListener implements OnDragListener {
		
		long rowID = -1;

		@Override
		public boolean onDrag(View v, DragEvent event) { 
			switch (event.getAction()) {
			case DragEvent.ACTION_DRAG_STARTED:
				// don't do anything yet
				break;
			case DragEvent.ACTION_DRAG_ENTERED:
				// don't do anything yet
				break;
			case DragEvent.ACTION_DROP:
				Log.e(DragListenerTag, "DRAG DROPPED");
				ClipData data = event.getClipData();
				rowID = data.getItemAt(0).getIntent().getLongExtra(MedicineExpandableListAdapter.clipMedSchedID, -1);
				AlertDialog.Builder builder = new Builder(Medication.this);
				builder.setTitle("Are you sure you want to delete this alarm? "+rowID);
				builder.setPositiveButton("OK", null);
				builder.setNegativeButton("Cancel", null);
				builder.create().show();
				break;
			case DragEvent.ACTION_DRAG_LOCATION:
				// don't do anything yet
				break;
			case DragEvent.ACTION_DRAG_ENDED:
				// don't do anything yet
				break;
			default:
				break;
			}

			return true;
		}

	}


}
