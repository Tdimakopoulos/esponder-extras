package eu.swanicare.activities;

import java.text.SimpleDateFormat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import eu.swanicare.R;
import eu.swanicare.adapters.ScheduledEventsAdapter;
import eu.swanicare.calendar.CalendarActivity;
import eu.swanicare.calendar.CalendarView;
import eu.swanicare.helperclasses.ScheduledEventEntity;

public class ScheduleDetails extends Activity
{

	CalendarView mView = null;
	AlertDialog.Builder setScheduleEvent,alertDialog;
	private TimePickerDialog timePickDialog = null;
	EditText timeSelection,userDetails;
	String timeSelectionInput,userDetailsInput;
	LayoutInflater li;
	View setsSheduleventView;
	SimpleDateFormat dateFormat;
	private ListView eventsListView;  
	ScheduledEventsAdapter eventListAdapter;
	private ScheduledEventEntity[] eventsList;
	Cursor c;
	String date;
	Button backButton;
	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText;


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.scheduledetailslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
		TextView activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Scheduled Events");
		
		if(getIntent().hasExtra("date")) {
			date = getIntent().getStringExtra("date");
			Log.e("CONTENTS IN INTENT", date);
		}
		else
			Log.e("NO CONTENTS IN INTENT", "");

//		initializeHeader();
//
//		mView = (CalendarView)findViewById(R.id.calendar);
//		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		eventsListView = (ListView)findViewById(R.id.EventsListView);

		prepareDateForDay();

		eventListAdapter = new ScheduledEventsAdapter(ScheduleDetails.this, eventsList);
		eventsListView.setAdapter(eventListAdapter);

		TextView dateText = (TextView)findViewById(R.id.DateTitleTextBox);
		dateText.setText (CalendarActivity.date);
		
		backButton = (Button) findViewById(R.id.BackButton);
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ScheduleDetails.this.finish();
			}
		});
		
		
	}



	private void prepareDateForDay() {

		
		if(date!=null) {
			if(date.equalsIgnoreCase("2013-10-27")) {
				eventsList = new ScheduledEventEntity[2];
				eventsList[0] = new ScheduledEventEntity(Long.valueOf(0), "Appointment with doctor.", "2013-10-27 11:00 am");
				eventsList[1] = new ScheduledEventEntity(Long.valueOf(0), "Hospital exam.", "2013-10-27 18:00 pm");
			}
			else if(date.equalsIgnoreCase("2013-10-29")) {
				eventsList = new ScheduledEventEntity[2];
				eventsList[0] = new ScheduledEventEntity(Long.valueOf(0), "Appointment with doctor.", "2013-10-29 11:00 am");
				eventsList[1] = new ScheduledEventEntity(Long.valueOf(0), "Hospital exam.", "2013-10-29 18:00 pm");

			}
			else if(date.equalsIgnoreCase("2013-11-03")) {
				eventsList = new ScheduledEventEntity[2];
				eventsList[0] = new ScheduledEventEntity(Long.valueOf(0), "Appointment with doctor.", "2013-11-3 11:00 am");
				eventsList[1] = new ScheduledEventEntity(Long.valueOf(0), "Hospital exam.", "2013-11-3 18:00 pm");

			}
			else {
				eventsList = new ScheduledEventEntity[1];
				eventsList[0] = new ScheduledEventEntity(Long.valueOf(0), "", "No scheduled events available.");

			}

		}
		else {
			eventsList = new ScheduledEventEntity[1];
			eventsList[0] = new ScheduledEventEntity(Long.valueOf(0), "", "No scheduled events available.");

		}
		
	}



	// Click on the Measurements button of Main
	//	public void clickHandler(View view) {
	//		switch (view.getId()) {
	//		case R.id.NewEventImageButton: // New Event
	//			setsSheduleventView = li.inflate(R.layout.setschedulevent, null);
	//			timeSelection = (EditText) setsSheduleventView.findViewById(R.id.UserTimeInput);
	//			userDetails = (EditText) setsSheduleventView.findViewById(R.id.UserDetailsInput);
	//		  	timeSelection.setOnClickListener(new OnClickListener() {
	////		 		@Override
	//			    public void onClick(View v) {
	//					timePickDialog = new TimePickerDialog(v.getContext(),
	//							new TimePickHandler(), 8, 00, true);
	//		 			  timePickDialog.show();
	//			    }
	//			});
	//			setScheduleEvent = new AlertDialog.Builder(this);
	//			setScheduleEvent.setView(setsSheduleventView);
	//			setScheduleEvent.setPositiveButton("OK",
	//					new DialogInterface.OnClickListener() {
	//						public void onClick(DialogInterface dialog,
	//								int which) {
	//							timeSelectionInput = timeSelection.getText().toString();
	//							userDetailsInput = userDetails.getText().toString();
	//							Log.d(" Test!!! "," timeSelection = "+ timeSelectionInput + " kai userDetails = " + userDetailsInput);
	//							
	//	 						 if(timeSelectionInput.equals("--:--") || userDetailsInput.length() < 1){
	//								 alertDialog.show();
	//							 }else{
	//									Log.d(" Ok!!!!! "," timeSelection = "+ timeSelectionInput + " kai userDetails = " + userDetailsInput);
	//									Date date = new Date();
	//									// Insert Event into the db 
	//									ConnectionService.db.insertScheduledTask(0, dateFormat.format(date), null, 2,
	//											dateFormat.format(date), 2, null, null, 4,
	//											null,  0,
	//											null, 1, 1, CalendarActivity.dateForDB+" "+timeSelectionInput, userDetailsInput);  
	//									finish();
	//									Intent scheduleDetails = new Intent(ScheduleDetails.this, ScheduleDetails.class);
	//									startActivity(scheduleDetails);
	//									overridePendingTransition(0, 0);
	//							 }
	//	 		 			}
	//					});
	//			
	//			setScheduleEvent.setNegativeButton("No",null);
	//			setScheduleEvent.show();		
	//			break;
	//		case R.id.CancelImageButton: // Cancel
	//			finish();
	//			break;
	//		default:
	//			break;
	//		}
	//	}
	//	
	//    private class TimePickHandler implements OnTimeSetListener {
	//    	
	//        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	//        	timeSelection.setText((hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute) );
	//            timePickDialog.hide();
	//            
	//        }
	//    }
	//    
	//    private void initializeHeader() {
	//
	//		receiver = new ConnectivityHeaderReceiver();
	//		filter = new IntentFilter("CONTROLS_CHANGED");
	//		registerReceiver(receiver, filter);
	//
	//		//Set the Wifitext by checking connectivity and broadcast inner intent
	//		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
	//		internetStatus.isOnline(ScheduleDetails.this);
	//
	//		//Edit Bluetooth state
	//		btText = (TextView) findViewById(R.id.bluetoothText);
	//		if(ConnectionService.alive)
	//			btText.setText("On");
	//		else
	//			btText.setText("Off");
	//
	//
	//		// Edit battery Image and set onclickListener
	//		batteryImage = (ImageView) findViewById(R.id.batteryicon);
	//		batteryImage.setOnClickListener( new OnClickListener() {
	//
	//			@Override
	//			public void onClick(View v) {
	//				String batteryText = null;
	//				try {
	//					AlertDialog.Builder builder = new AlertDialog.Builder(ScheduleDetails.this);
	//					builder.setCancelable(true);
	//					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
	//					batteryText = batteryTextView.getText().toString();
	//					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
	//					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
	//						builder.setMessage("Your battery is running low, please charge your device.");
	//					else if (Long.valueOf(batteryText) <= 5)
	//						builder.setMessage("Your battery is depleted, please charge your device.");
	//					else
	//						builder.setMessage("Your battery levels are good.");
	//					builder.setNeutralButton("Close", null);
	//					builder.create().show();
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//
	//			}
	//		});
	//
	//		wifiImage = (ImageView) findViewById(R.id.wifiicon);
	//		wifiImage.setOnClickListener(new OnClickListener() {
	//
	//			@Override
	//			public void onClick(View v) {
	//				WifiManager wifiManager = (WifiManager)ScheduleDetails.this.getSystemService(Context.WIFI_SERVICE);
	//				if(wifiManager.isWifiEnabled() ) {
	//					wifiManager.setWifiEnabled(false);
	//					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
	//				}
	//				else {
	//					wifiManager.setWifiEnabled(true);
	//					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
	//				}
	//			}
	//		});
	//
	//
	//	}


}
