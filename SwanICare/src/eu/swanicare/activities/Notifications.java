package eu.swanicare.activities;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import eu.swanicare.R;
import eu.swanicare.adapters.NotificationsAdapter;
import eu.swanicare.helperclasses.NotificationEntity;

public class Notifications extends Activity {

	Button backButton;
	ListView notListView;
	NotificationEntity[] notifications;
	NotificationsAdapter listAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notificationslayout);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		TextView activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Notifications");
		
		backButton = (Button) findViewById(R.id.BackButton);
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Notifications.this.finish();
			}
		});

		notListView = (ListView) findViewById(R.id.notificationsList);

		// preparing list data
		prepareListData();

		listAdapter = new NotificationsAdapter(this, notifications);

		// setting list adapter
		notListView.setAdapter(listAdapter);

	}



	private void prepareListData() {
	
	notifications = new NotificationEntity[5];
	
	NotificationEntity n1 = new NotificationEntity(Long.valueOf(1), "You are doing well. Keep the good work!", true, "24/10/2013 - 13:04");
	NotificationEntity n2 = new NotificationEntity(Long.valueOf(2), "It�s time to take your medication!", false, "25/10/2013 - 18:26");
	NotificationEntity n3 = new NotificationEntity(Long.valueOf(3), "Please phone Doctor Smith.", false, "26/10/2013 - 10:47");
	NotificationEntity n4 = new NotificationEntity(Long.valueOf(3), "It�s time to take your medication!", true, "26/10/2013 - 10:47");
	NotificationEntity n5 = new NotificationEntity(Long.valueOf(3), "Please phone Doctor Smith.", false, "26/10/2013 - 10:47");
	
	notifications[0] = n1;
	notifications[1] = n2;
	notifications[2] = n3;
	notifications[3] = n4;
	notifications[4] = n5;

	}



}
