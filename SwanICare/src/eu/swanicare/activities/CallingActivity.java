package eu.swanicare.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import eu.swanicare.R;

public class CallingActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.callingactivitylayout);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);


		TextView activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Quick Calls");
		
		Button backButton = (Button) findViewById(R.id.backButton);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				CallingActivity.this.finish();
			}
		});


	}


	public void clickHandler(View view) {

		switch (view.getId()) {
		case R.id.callDoctorButton:
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:123456789"));
			startActivity(callIntent);
			break;
		case R.id.callNurseButton:
			 Intent callIntent2 = new Intent(Intent.ACTION_CALL);
			    callIntent2.setData(Uri.parse("tel:234567891"));
			    startActivity(callIntent2);
			break;
		default:
			break;
		}
	}

}
