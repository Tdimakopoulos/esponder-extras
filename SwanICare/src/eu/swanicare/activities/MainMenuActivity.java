package eu.swanicare.activities;

import java.util.Timer;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import eu.swanicare.R;
import eu.swanicare.application.GlobalVar;
import eu.swanicare.calendar.CalendarActivity;
import eu.swanicare.timertasks.NotificationPresenter;

public class MainMenuActivity extends Activity {
	
	Button backButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
		TextView activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Swan iCare Menu");
		
		backButton = (Button) findViewById(R.id.backButton);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainMenuActivity.this.finish();
			}
		});
		
		if(GlobalVar.notificationGeneratorTimer == null) {
			GlobalVar.notificationGeneratorTimer = new Timer();
			GlobalVar.notificationGeneratorTimer.schedule(new NotificationPresenter(MainMenuActivity.this), 3000, 30000);
		}
		else
			Log.e("MAIN MENU ACTIVITY","TIMER IS NOT NULL");
		
		Log.e("MAIN MENU ACTIVITY","ON CREATE FINISHED");
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}
	
	
	public void clickHandler(View view) {

		switch (view.getId()) {
		case R.id.notificationButton: 
			Intent openNotifications = new Intent(MainMenuActivity.this, Notifications.class);
			startActivity(openNotifications);
			break;
		case R.id.medicationButton:	
			Intent openFunctions = new Intent(MainMenuActivity.this, Medication.class);
			startActivity(openFunctions);
			break;
		case R.id.calendarButton: 
			startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null, CalendarActivity.MIME_TYPE));
			break;
		case R.id.callsButton:
			startActivity(new Intent(MainMenuActivity.this, CallingActivity.class));
			break;
		default:
			break;
		}
	}

	@Override
	protected void onDestroy() {
		if(GlobalVar.notificationGeneratorTimer != null) {
			GlobalVar.notificationGeneratorTimer.cancel();
			GlobalVar.notificationGeneratorTimer.purge();
			GlobalVar.notificationGeneratorTimer = null;
		}
		Log.e("MAIN MENU ACTIVITY","ON DESTROY REACHED");
		super.onDestroy();
	}
	
	
	
	

}
