package eu.swanicare.helperclasses;

public class ScheduledEventEntity {
	
	private Long eventID;
	private String eventDesc;
	private String eventDate;
	
	public ScheduledEventEntity(Long eventID, String eventDesc,
			String eventDate) {
		super();
		this.eventID = eventID;
		this.eventDesc = eventDesc;
		this.eventDate = eventDate;
	}
	
	public Long getEventID() {
		return eventID;
	}
	public void setEventID(Long eventID) {
		this.eventID = eventID;
	}
	public String getEventDesc() {
		return eventDesc;
	}
	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
	
}
