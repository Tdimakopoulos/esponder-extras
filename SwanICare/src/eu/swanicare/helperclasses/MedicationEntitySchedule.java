package eu.swanicare.helperclasses;

public class MedicationEntitySchedule {
	
	private Long medicineScheduleID;
	private String medicineName;
	private String daysOfWeek;
	private String timeofDay;
	private boolean activated;
	
	public MedicationEntitySchedule(String medicineName, String daysOfWeek,
			String timeofDay, boolean activated) {
		super();
		this.medicineName = medicineName;
		this.daysOfWeek = daysOfWeek;
		this.timeofDay = timeofDay;
		this.activated = activated;
	}
	
	
	
	public String getDaysOfWeek() {
		return daysOfWeek;
	}
	
	public void setDaysOfWeek(String daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}
	
	public String getMedicineName() {
		return medicineName;
	}
	
	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}
	
	public String getTimeofDay() {
		return timeofDay;
	}
	
	public void setTimeofDay(String timeofDay) {
		this.timeofDay = timeofDay;
	}
	
	public boolean isActivated() {
		return activated;
	}
	
	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	
	public Long getMedicineScheduleID() {
		return medicineScheduleID;
	}

	public void setMedicineScheduleID(Long medicineScheduleID) {
		this.medicineScheduleID = medicineScheduleID;
	}
	
	
	
}
