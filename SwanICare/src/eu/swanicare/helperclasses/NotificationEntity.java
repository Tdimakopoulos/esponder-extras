package eu.swanicare.helperclasses;

public class NotificationEntity {
	
	private Long notificationID;
	private String notificationDesc;
	private boolean acked;
	private String date;
	
	public NotificationEntity(Long notificationID, String notificationDesc,
			boolean acked, String date) {
		this.notificationID = notificationID;
		this.notificationDesc = notificationDesc;
		this.acked = acked;
		this.date = date;
	}
	
	
	
	public Long getNotificationID() {
		return notificationID;
	}
	public void setNotificationID(Long notificationID) {
		this.notificationID = notificationID;
	}
	public String getNotificationDesc() {
		return notificationDesc;
	}
	public void setNotificationDesc(String notificationDesc) {
		this.notificationDesc = notificationDesc;
	}
	public boolean isAcked() {
		return acked;
	}
	public void setAcked(boolean acked) {
		this.acked = acked;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	
	
	
}
