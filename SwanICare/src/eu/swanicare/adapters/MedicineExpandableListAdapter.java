package eu.swanicare.adapters;

import java.util.HashMap;
import java.util.List;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import eu.swanicare.R;
import eu.swanicare.helperclasses.MedicationEntitySchedule;

public class MedicineExpandableListAdapter extends BaseExpandableListAdapter {

	public static final String clipMedSchedID = "medScheduleID";

	private Context context;
	private List<String> _listDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<MedicationEntitySchedule>> _listDataChild;

	public MedicineExpandableListAdapter(Context context,
			List<String> listDataHeader, HashMap<String, List<MedicationEntitySchedule>> listChildData) {
		this.context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final MedicationEntitySchedule childMedicationDesc = (MedicationEntitySchedule) getChild(groupPosition, childPosition);

		LayoutInflater infalInflater = (LayoutInflater) this.context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View rowView = infalInflater.inflate(R.layout.medicine_list_item, null);
		 

		TextView medicineDays = (TextView) rowView.findViewById(R.id.medicineDays);
		TextView medicineTime = (TextView) rowView.findViewById(R.id.medicineTimes);
		ToggleButton activatedButton = (ToggleButton) rowView.findViewById(R.id.activatedButton);

		medicineDays.setText(childMedicationDesc.getDaysOfWeek());
		medicineTime.setText(childMedicationDesc.getTimeofDay());


		activatedButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(buttonView.isChecked()){
					buttonView.setBackgroundResource(R.drawable.on_button);
				}
				else {
					buttonView.setBackgroundResource(R.drawable.off_button);
				}
			}

		});

		activatedButton.setChecked(childMedicationDesc.isActivated());
		if(activatedButton.isChecked())
			activatedButton.setBackgroundResource(R.drawable.on_button);
		else
			activatedButton.setBackgroundResource(R.drawable.off_button);
		
		

		rowView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				
				Intent intent = new Intent();
				// FIXME Add the row id here for deletion when the user drags
				// and drops the row in the delete icon at the bottom
				intent.putExtra(clipMedSchedID, (long)3);
				ClipData data = ClipData.newIntent("Carrying Intent", intent);
//				return v.startDrag(ClipData.newPlainText("", ""), new View.DragShadowBuilder(v), null, 0);
				return v.startDrag(data, new View.DragShadowBuilder(v), null, 0);
			}
		});
		
		return rowView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.medicinelistheaderlayout, null);
		}

		TextView lblListHeader = (TextView) convertView.findViewById(R.id.medicineListHeader);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(headerTitle);

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	

}
