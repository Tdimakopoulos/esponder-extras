package eu.swanicare.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import eu.swanicare.R;
import eu.swanicare.helperclasses.NotificationEntity;

public class NotificationsAdapter extends BaseAdapter {

	private Context context;
	private NotificationEntity[] values;

	public NotificationsAdapter(Context context, NotificationEntity[] notifications) {
		setContext(context);
		setValues(notifications);
	}

	@Override
	public int getCount() {
		return getValues().length;
	}

	@Override
	public Object getItem(int position) {
		return getValues()[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.notificationsrowlayout, parent, false);
		}

		// Set the title
		final NotificationEntity notification = (NotificationEntity) getItem(position);

		TextView notificationDateTextView = (TextView) convertView.findViewById(R.id.notificationdate);
		notificationDateTextView.setText(notification.getDate());

		//Alarm Indicator
		TextView notificationDesc = (TextView) convertView.findViewById(R.id.notificationdesc);
		notificationDesc.setText(notification.getNotificationDesc());

		ImageView notificationAckedImage = (ImageView) convertView.findViewById(R.id.notificationsrowimage);
		if(notification.isAcked())
			notificationAckedImage.setBackgroundResource(R.drawable.select);
		else
			notificationAckedImage.setBackgroundResource(R.drawable.delete);

		return convertView;
	}

	// Getters and setters for private fields of the Adapter
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public NotificationEntity[] getValues() {
		return values;
	}

	public void setValues(NotificationEntity[] values) {
		this.values = values;
	}



}
