package eu.swanicare.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import eu.swanicare.R;
import eu.swanicare.helperclasses.ScheduledEventEntity;

public class ScheduledEventsAdapter extends BaseAdapter {

	private Context context;
	private ScheduledEventEntity[] values;

	public ScheduledEventsAdapter(Context context, ScheduledEventEntity[] events) {
		setContext(context);
		setValues(events);
	}

	@Override
	public int getCount() {
		return getValues().length;
	}

	@Override
	public Object getItem(int position) {
		return getValues()[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.scheduleeventrowlayout, parent, false);
		}
		
		final ScheduledEventEntity event = (ScheduledEventEntity) getItem(position);

		TextView eventDateTextView = (TextView) convertView.findViewById(R.id.scheduledEventDate);
		eventDateTextView.setText(event.getEventDate());

		TextView eventDesc = (TextView) convertView.findViewById(R.id.scheduledEventDesc);
		eventDesc.setText(event.getEventDesc());

		return convertView;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public ScheduledEventEntity[] getValues() {
		return values;
	}

	public void setValues(ScheduledEventEntity[] values) {
		this.values = values;
	}



}
