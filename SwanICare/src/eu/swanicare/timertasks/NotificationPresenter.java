package eu.swanicare.timertasks;

import java.util.Random;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import eu.swanicare.activities.NotificationsDialog;
import eu.swanicare.application.GlobalVar;

public class NotificationPresenter extends TimerTask {

	private Context context;

	public NotificationPresenter(Context context) {
		this.setContext(context);

	}

	@Override
	public void run() {
		Intent intent = new Intent(context, NotificationsDialog.class);
		intent.setAction(GlobalVar.NOTIFICATION_POPUP);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		Bundle bundle = new Bundle();

		Random r = new Random();
		double i = r.nextInt(1001) / 1000.0;
		
		if(0<=i && i<0.33){
			bundle.putString("date", "");
			bundle.putString("notificationDesc", "You are doing well. Keep the good work!");
		}
		else if(0.33<=i && i<0.66){
			bundle.putString("date", "");
			bundle.putString("notificationDesc", "It�s time to take your medication!");
		}
		else {
			bundle.putString("date", "");
			bundle.putString("notificationDesc", "Please phone Doctor Smith.");
		}
		
		intent.putExtras(bundle);
		context.startActivity(intent);

	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	
	public static double randInt(int min, int max) {

	    // Usually this can be a field rather than a method variable
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    double randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}

}
