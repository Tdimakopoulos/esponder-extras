/*
 * Copyright (C) 2011 Chris Gao <chris@exina.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.swanicare.calendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import eu.swanicare.R;
import eu.swanicare.activities.ScheduleDetails;

public class CalendarActivity extends Activity  implements CalendarView.OnCellTouchListener {
	public static final String MIME_TYPE = "vnd.android.cursor.dir/vnd.mobile.calendar.date";
	public static String date = "", dateForDB="";
	View _view = null;
	CalendarView mView = null;
	TextView mHit;
	Handler mHandler = new Handler();
	Button backButton;
	
	IntentFilter filter;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText;
		
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedulelayoutold);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        
        TextView activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Calendar");
		
		backButton = (Button) findViewById(R.id.BackButton);
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CalendarActivity.this.finish();
			}
		});
		        
        mView = (CalendarView)findViewById(R.id.calendar);
        mView.setOnCellTouchListener(this);
        
        ImageView addNote = (ImageView) findViewById(R.id.addnoteimage);
        addNote.setOnClickListener(new OnClickListener() {
			
			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder= new Builder(CalendarActivity.this);
				LayoutInflater mInflater = (LayoutInflater) CalendarActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View dialogView = mInflater.inflate(R.layout.newnotedialoglayout, null, false); 
				builder.setView(dialogView);
				builder.setTitle("Add new event");
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				});
				
				builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				});
				builder.create().show();
				
				
			}
		});
        
        
        TextView monthText = (TextView)findViewById(R.id.MonthTitleTextBox);
        monthText.setText(DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_LONG) + " "+mView.getYear());
                
        if(getIntent().getAction().equals(Intent.ACTION_PICK))
        	findViewById(R.id.hint).setVisibility(View.INVISIBLE);
    }
    
	public void onTouch(Cell cell) 
	{
		Intent intent = getIntent();
		String action = intent.getAction();
		
		//int day = cell.getDayOfMonth();
		date = cell.getDayOfMonth()+"-"+ DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_SHORT) + "-"+mView.getYear();
		dateForDB = mView.getYear()+"-"+((mView.getMonth()+1) < 10 ? "0" + (mView.getMonth()+1) : (mView.getMonth()+1)) +"-"+(cell.getDayOfMonth() < 10 ? "0" + cell.getDayOfMonth() : cell.getDayOfMonth());
		 
		if(action.equals(Intent.ACTION_PICK) || action.equals(Intent.ACTION_GET_CONTENT)) {
			Intent ret = new Intent();
			ret.putExtra("year", mView.getYear());
			ret.putExtra("month", mView.getMonth());
			ret.putExtra("day", cell.getDayOfMonth());
			this.setResult(RESULT_OK, ret);
			Log.e("STARTING FROM FIRST INTENT","STARTING FIRST");
			finish();
			return;
		}	
		
		if (cell.mPaint.getColor()!= Color.TRANSPARENT)
		{
			Log.e("STARTING FROM SECOND INTENT","STARTING SECOND");
			Intent openScheduleDetails = new Intent(CalendarActivity.this, ScheduleDetails.class);
			Log.e("date",CalendarActivity.date);
			Log.e("datefordb",CalendarActivity.dateForDB);
			openScheduleDetails.putExtra("date", CalendarActivity.dateForDB);
			startActivity(openScheduleDetails);		
		}
		else
			return;
		
	}   
	
	public void onDateButtonTouch (View view)
	{   
		try
		{
			if(view.getId() == R.id.MonthForwardImageButton)
			{
				mView.nextMonth();
				TextView monthText = (TextView)findViewById(R.id.MonthTitleTextBox);
				monthText.setText (DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_LONG) + " "+mView.getYear());
				
			}
			else if(view.getId() == R.id.MonthBackImageButton)
			{
				mView.previousMonth();
				TextView monthText = (TextView)findViewById(R.id.MonthTitleTextBox);
				monthText.setText (DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_LONG) + " "+mView.getYear());
			}
			else
				return;
		}
		catch (Exception ex)
		{
    		
		}	
	}
	
	
//	private void initializeHeader() {
//
//		receiver = new ConnectivityHeaderReceiver();
//		filter = new IntentFilter("CONTROLS_CHANGED");
//		registerReceiver(receiver, filter);
//
//		//Set the Wifitext by checking connectivity and broadcast inner intent
//		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
//		internetStatus.isOnline(CalendarActivity.this);
//
//		//Edit Bluetooth state
//		btText = (TextView) findViewById(R.id.bluetoothText);
//		if(ConnectionService.alive)
//			btText.setText("On");
//		else
//			btText.setText("Off");
//
//
//		// Edit battery Image and set onclickListener
//		batteryImage = (ImageView) findViewById(R.id.batteryicon);
//		batteryImage.setOnClickListener( new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				String batteryText = null;
//				try {
//					AlertDialog.Builder builder = new AlertDialog.Builder(CalendarActivity.this);
//					builder.setCancelable(true);
//					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
//					batteryText = batteryTextView.getText().toString();
//					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
//					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
//						builder.setMessage("Your battery is running low, please charge your device.");
//					else if (Long.valueOf(batteryText) <= 5)
//						builder.setMessage("Your battery is depleted, please charge your device.");
//					else
//						builder.setMessage("Your battery levels are good.");
//					builder.setNeutralButton("Close", null);
//					builder.create().show();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
//			}
//		});
//
//		wifiImage = (ImageView) findViewById(R.id.wifiicon);
//		wifiImage.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				WifiManager wifiManager = (WifiManager)CalendarActivity.this.getSystemService(Context.WIFI_SERVICE);
//				if(wifiManager.isWifiEnabled() ) {
//					wifiManager.setWifiEnabled(false);
//					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
//				}
//				else {
//					wifiManager.setWifiEnabled(true);
//					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
//				}
//			}
//		});
//
//
//	}


	//	code for header ****************
//	private class ConnectivityHeaderReceiver extends BroadcastReceiver {
//
//		Animation batteryAnimation ;
//
//		@Override
//		public void onReceive(Context arg0, Intent intent) {
//			if(intent.hasExtra("wifi_state")) {
//				if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
//					TextView wifiText = (TextView) findViewById(R.id.wifiText);
//					wifiText.setText("Off");
//					wifiText.setTextColor(Color.parseColor("#CD0000"));
//				}
//				else
//					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
//						wifiText = (TextView) findViewById(R.id.wifiText);
//						wifiText.setText("On");
//						wifiText.setTextColor(Color.parseColor("#2D9C3E"));
//					}
//			}
//
//			if(intent.hasExtra("bluetooth_state")) {
//				if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
//					btText = (TextView) findViewById(R.id.bluetoothText);
//					btText.setText("Off");
//				}
//				else
//					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
//						btText = (TextView) findViewById(R.id.bluetoothText);
//						btText.setText("On");
//					}
//			}
//
//
//			if(intent.hasExtra("battery_level")) {
//				batteryAnimation = AnimationUtils.loadAnimation(CalendarActivity.this, R.anim.battery_animation);
//
//				Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
//				batteryText = (TextView) findViewById(R.id.batteryText);
//				batteryImage = (ImageView) findViewById(R.id.batteryicon);
//
//				if(batteryLevel != null && batteryLevel != -1) {
//					batteryText.setText(batteryLevel.toString()+"%");
//					if(batteryLevel>20) {
//						batteryImage.clearAnimation();
//						batteryText.setTextColor(Color.parseColor("#2D9C3E"));
//						if(batteryLevel<=100 && batteryLevel>80)
//							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
//						else
//							if(batteryLevel<=80 && batteryLevel>60)
//								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
//							else
//								if(batteryLevel<=60 && batteryLevel>40)
//									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
//								else
//									if(batteryLevel<=40 && batteryLevel>20)
//										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
//					}
//					else {
//						batteryText.setTextColor(Color.RED);
//						if(batteryLevel<=20 && batteryLevel>10) {
//							batteryImage.clearAnimation();
//							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
//						}
//						else {
//							batteryImage.startAnimation(batteryAnimation);
//							if(batteryLevel<=10 && batteryLevel>5)
//								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
//							else
//								if(batteryLevel<=5 && batteryLevel>=0)
//									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
//						}
//					}
//				}
//
//			}
//
//		}
//
//	}
	
	
}