package eu.esponder.client;



import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;


public class EsponderClient
{
	BufferedReader inFromServer;
	DataOutputStream outToServer;
	BufferedReader inFromUser;

	Socket clientSocket;

	ClientState state;

	public EsponderClient(String host, int port)
	{
		inFromUser = new BufferedReader(new InputStreamReader(System.in));
		state = new UnAuthorised();
		try{
			this.clientSocket = new Socket(host, port);
			inFromServer =new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			outToServer =new DataOutputStream(clientSocket.getOutputStream());
		}catch(Exception e){}
		run();
	}

	public static void main(String[] args)
	{
		
		
			//new EsponderClient("rdocs.exodussa.com", 4444);
		new EsponderClient("rdocs.exodussa.com", 4444);
		
	}

	private void run(){
		if(clientSocket != null){
			while(!clientSocket.isClosed()){//while the client is connected to a server

				try{
				    state.update(); //runs the state's logic
			    }catch(Exception e){}
			}
		}else{
			System.out.println("Could not conncet to server, check host name and port numbr, then try again.");
		}
	}

	//interface for a client state
	private interface ClientState
	{
		public abstract void update();
	}

	//requests the password from the client and waits for an appropriate response
	private class UnAuthorised implements ClientState
	{
		public void update() {
			// read a sentence from the client
		    try {
		    	System.out.print("Enter Password:");
		    	String input = inFromUser.readLine();//reads the password in from the user

	    		outToServer.writeBytes(input+"\n");//sends password to the client
				String fromServer =  inFromServer.readLine();//waits for server response
				if(fromServer.equals("001 PASSWD OK")){
					System.out.print("Password Correct.\n");
					state = new Authorised();//if correct, move to the authorised state
				}else if(fromServer.equals("002 PASSWD WRONG")){
					System.out.print("Password Incorrect.\n");//if wrong, simply move stay in this state and continue asking for password
				}else if(fromServer.equals("003 REFUSED")){
					System.out.print("Conncetion Refused.\n");//if the client is in the block list, disconnect it.
					state = new Disconnect();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	//The client will be in this state is the password has been entered correctly. 
	//all other states return to here once they have finished their update method.
	private class Authorised implements ClientState
	{
		public void update() {

		    try {
		    	//re initalize in and out streams
		    	inFromServer =new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				outToServer =new DataOutputStream(clientSocket.getOutputStream());

				String command = inFromUser.readLine();// read a command from the user
				//if its a client side command,
				if(command.equals("exit")){ //or exit
		    		state = new Disconnect(); //move to the disconnect state
		    		outToServer.writeBytes(command + "\n");//tells the server to disconnect too.
		    	}else{ //other wise it is a server side command and 
					outToServer.writeBytes(command + "\n"); //must be sent to the server
					String response = inFromServer.readLine(); //wait for servers response to command
					System.out.println("response : "+response);
					if(response.startsWith("031 REMOTEFILELOG")){ //if the server wants a file sent
						state = new RemoteLogFile(); //move to this state, (pass in the filename)
					}
		    	}
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
	}

	private class Disconnect implements ClientState
	{
		public void update() {
		    try {
		    	System.out.println("Closing conncetion");
		    	Thread.sleep(1000);//wait 4 seconds
				clientSocket.close(); //close the connection
			} catch (IOException e) {
				System.out.println("IO Error, please check connection to server");
			} catch (InterruptedException e) {
				//do nothing if the pause is interupted
			}

		}

	}

		
	private class RemoteLogFile implements ClientState
	{
		public void update() {
			try {
				String file = inFromServer.readLine();

				while(!file.equals("032 ENDOFFILELOG")){//while the end of the file list from the server has not been reached
					System.out.println(file);//print the file's name
					file = inFromServer.readLine();//get the next one
				}
				System.out.println(); 
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			state = new Authorised();//back to the authorised state for next command
		}

	}

	
}