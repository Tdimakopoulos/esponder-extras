package eu.esponder.ESponderFRU.demo.activities;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.nutiteq.MapView;
import com.nutiteq.components.Components;
import com.nutiteq.components.MapPos;
import com.nutiteq.geometry.Marker;
import com.nutiteq.projections.EPSG3857;
import com.nutiteq.projections.Projection;
import com.nutiteq.rasterlayers.TMSMapLayer;
import com.nutiteq.style.LabelStyle;
import com.nutiteq.style.MarkerStyle;
import com.nutiteq.utils.UnscaledBitmapLoader;
import com.nutiteq.vectorlayers.GeometryLayer;
import com.nutiteq.vectorlayers.MarkerLayer;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderFRU.demo.R;
import eu.esponder.ESponderFRU.demo.adapters.MenuAdapter;
import eu.esponder.ESponderFRU.demo.animations.MeasCriticalAnimation;
import eu.esponder.ESponderFRU.demo.application.GlobalVar;
import eu.esponder.ESponderFRU.demo.data.DataManager;
import eu.esponder.ESponderFRU.demo.data.MyLabel;
import eu.esponder.ESponderFRU.demo.data.Poi;
import eu.esponder.ESponderFRU.demo.data.PoiCategory;
import eu.esponder.ESponderFRU.demo.maphelpers.MapEventListener;
import eu.esponder.ESponderFRU.demo.maphelpers.MyLocationCircle;
import eu.esponder.ESponderFRU.demo.runnables.AlarmAnimStopTask;
import eu.esponder.ESponderFRU.demo.runnables.GetSensorsPCDTimerTask;
import eu.esponder.ESponderFRU.demo.runnables.MapRequestEventSender;
import eu.esponder.ESponderFRU.demo.runnables.SPInformationTimerTask;
import eu.esponder.ESponderFRU.demo.utils.ESponderUtils;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;

public class MainActivity extends SlidingActivity  {

	private MapView mapView;

	ImageView centerMapImage;

	SlidingMenu sm;

	MapEventListener mapListener;

	MainMapReceiver receiver;

	Handler handler;

	Timer animTimer;

	//New layer to hold the current fr marker for the map
	MarkerLayer currentFRLayer;
	Long cuID;

	ArrayList<MarkerLayer> markerLayerList;

	ArrayList<GeometryLayer> geometrylayerList;

	MyLocationCircle locationCircle;

	Boolean cuUpdate =false;


	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main_map);

		// if the user is an FRC we do not need a sensor for the layout orientation
		// as a result it is locked on portrait and does not change.
		if(((GlobalVar)getApplicationContext()).getRole()==0) {
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
			setTitle("ESponder FRC Application");	
		}

		// if the user is an IC we force landscape orientation to host the IC layout
		else if(((GlobalVar)getApplicationContext()).getRole()==2) {
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			setTitle("ESponder IC Application");
		}




		handler = new Handler();

		cuID = getSharedPreferences(GlobalVar.spName, MODE_PRIVATE).getLong(GlobalVar.spESponderUserID, -1);
		Log.e("POIs", "FOUND THE CURRENT USER:"+cuID);

		animTimer = new Timer();

		markerLayerList = new ArrayList<MarkerLayer>();
		geometrylayerList = new ArrayList<GeometryLayer>();

		initializeMenu();

		mapView = (MapView) findViewById(R.id.mapView);

		// Restore map state during devicse rotation,
		// it is saved in onRetainNonConfigurationInstance() below
		Components retainObject = (Components) getLastNonConfigurationInstance();
		if (retainObject != null) {
			// just restore configuration and update listener, skip other initializations
			mapView.setComponents(retainObject);
			mapListener = (MapEventListener) mapView.getOptions().getMapListener();
			mapListener.reset(this, mapView);
			mapView.startMapping();
			return;
		} else {
			// Create and set MapView components 
			mapView.setComponents(new Components());
		}

		DataManager.initInstance(this);

		// Initialize the map
		String MapServerURL = DataManager.getInstance().getMapServerURL();
		// Define base layer. Here we use MapQuest open tiles which are free to use
		// Almost all online maps use EPSG3857 projection.
		TMSMapLayer mapLayer = new TMSMapLayer(new EPSG3857(), 0, 18, 0,
				MapServerURL, "/", ".png");
		mapView.getLayers().setBaseLayer(mapLayer);

		// set initial map view camera - optional. "World view" is default 
		// NB! it must be in base layer projection (EPSG3857), so we convert it from lat and long		
		// zoom - 0 = world, like on most web maps
		//		mapView.setZoom((float)DataManager.getInstance().getInitialMapZoom());
		mapView.setZoom((float)17);
		// tilt means perspective view. Default is 90 degrees for "normal" 2D map view, minimum allowed is 30 degrees.
		mapView.setTilt(90.0f);

		// Activate some mapview options to make it smoother
		mapView.getOptions().setPreloading(true);
		mapView.getOptions().setSeamlessHorizontalPan(true);
		mapView.getOptions().setTileFading(true);
		mapView.getOptions().setKineticPanning(true);
		mapView.getOptions().setDoubleClickZoomIn(true);
		mapView.getOptions().setDualClickZoomOut(true);

		// configure texture caching - optional, suggested 
		mapView.getOptions().setTextureMemoryCacheSize(40 * 1024 * 1024);
		mapView.getOptions().setCompressedMemoryCacheSize(8 * 1024 * 1024);

		// define online map persistent caching - optional, suggested. Default - no caching
		mapView.getOptions().setPersistentCachePath(this.getDatabasePath("mapcache").getPath());
		// set persistent raster cache limit to 100MB
		mapView.getOptions().setPersistentCacheSize(100 * 1024 * 1024);

		currentFRLayer = new MarkerLayer(mapLayer.getProjection());
		// Create and set the layers
		createMapPOILayers(mapLayer);

		// add event listener
		mapListener = new MapEventListener(this, mapView);
		mapView.getOptions().setMapListener(mapListener);

		// start mapping
		mapView.startMapping();

		centerMapFunction();

		// Center around the position of the current ESponder user
		setCenterMapButtonOnClickListener();

		// Timer task to update the map
		TimerTask updatemaptask = new TimerTask() {

			@Override
			public void run() {
				Thread thread = new Thread(new MapRequestEventSender(MainActivity.this, "Main Activity"));
				thread.start();
			}
		};



		//The map will be updated only when the current user of the app is an FRC or IC
		if( ((GlobalVar)getApplicationContext()).getRole() == 0 || ((GlobalVar)getApplicationContext()).getRole() == 2 )  {

			if(GlobalVar.mapUpdateTimer == null) {
				int period = 30000;
				GlobalVar.mapUpdateTimer = new Timer();
				GlobalVar.mapUpdateTimer.scheduleAtFixedRate(updatemaptask, period, period);
			}
		}



		if( ((GlobalVar)getApplicationContext()).getRole() == 0 || ((GlobalVar)getApplicationContext()).getRole() == 1 )  {

			if(GlobalVar.pcdSensorTimer == null) {
				GlobalVar.pcdSensorTimer = new Timer();
				// The sensor measurements will be updated only when the current user of the app is an FRC		
				if( ((GlobalVar)getApplicationContext()).getRole() == 0 )
					GlobalVar.pcdSensorTimer.scheduleAtFixedRate(new GetSensorsPCDTimerTask(MainActivity.this.getApplicationContext()), 2000, 10000);

				// The smartphone statistics will be updated only when the current user of the app is an FRC or FR
				if( ((GlobalVar)getApplicationContext()).getRole() == 0 || ((GlobalVar)getApplicationContext()).getRole() == 1 )
					GlobalVar.pcdSensorTimer.scheduleAtFixedRate(new SPInformationTimerTask(MainActivity.this.getApplicationContext()), 0, 15000);
			}
		}

	}


	private void initializeMenu() {

		ListView menuList = null;
		View menuView = null;
		MenuAdapter menuAdapter = null;

		Resources res = getResources();
		String[] menuItems = null;

		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {

			menuView = LayoutInflater.from(MainActivity.this).inflate(R.layout.menu_frame, null);
			menuList = (ListView) menuView.findViewById(R.id.MenuAsList);
			menuItems = res.getStringArray(R.array.frc_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.FRCMenuClickListener(MainActivity.this));
			menuAdapter = new MenuAdapter(MainActivity.this, R.layout.row_sidemenu, menuItems, true);
		}
		else if( ((GlobalVar)getApplicationContext()).getRole() == 2 ) {
			menuList = (ListView) findViewById(R.id.applicationMenu);
			menuItems = res.getStringArray(R.array.ic_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.ICMenuClickListener(MainActivity.this));
			menuAdapter = new MenuAdapter(MainActivity.this, R.layout.row_sidemenu, menuItems, false);
		}

		menuList.setAdapter(menuAdapter);

		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {
			setBehindContentView(menuView);
			sm = getSlidingMenu();
			sm.setShadowWidthRes(R.dimen.shadow_width);
			sm.setShadowDrawable(R.drawable.shadow);
			sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
			sm.setFadeDegree(0.35f);
			sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
			setSlidingActionBarEnabled(true);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(receiver);
	}


	@Override
	protected void onResume() {
		super.onResume();
		receiver = new MainMapReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_MAP_UPDATE);
		filter.addAction(GlobalVar.INNER_MAP_FR_UPDATE);
		filter.addAction(GlobalVar.INNER_ALARM_RECEIVED);
		registerReceiver(receiver, filter);
	}


	//	private Runnable currentPositionFinder = new Runnable() {
	//		
	//		@Override
	//		public void run() {
	//			
	//			// UNCOMMENT THE NEXT 3 LINES TO SHOW GPS LOCATION
	//			MyLocationCircle locationCircle = new MyLocationCircle();
	//			mapListener.setLocationCircle(locationCircle);
	//			initGps(locationCircle);
	//		}
	//	};


	private void setCenterMapButtonOnClickListener() {
		centerMapImage = (ImageView) findViewById(R.id.centerMapImage);
		centerMapImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				centerMapFunction();
			}
		});
	}

	private void centerMapFunction() {

		class CenterMapRunnable implements Runnable {

			@Override
			public void run() {

				SharedPreferences prefs = MainActivity.this.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
				Long currentUserID = prefs.getLong(GlobalVar.spESponderUserID, -1);

				if(currentUserID != null) {
					if(currentUserID != -1) {
						PointDTO centerPoint = GlobalVar.db.getFirstResponderLocationById(currentUserID);
						if(centerPoint != null) {
							mapView.setFocusPoint(mapView.getLayers().getBaseLayer().getProjection().
									fromWgs84(centerPoint.getLongitude().doubleValue(),centerPoint.getLatitude().doubleValue()));
							mapView.setZoom((float)17);
						}
						else {
							Looper.prepare();
							Toast.makeText(MainActivity.this, "Unable to center map...", Toast.LENGTH_LONG).show();
							Looper.loop();
						}

					}
					else {
						Looper.prepare();
						Toast.makeText(MainActivity.this, "Unable to center map...", Toast.LENGTH_LONG).show();
						Looper.loop();
					}
				}
				else
					mapView.setFocusPoint(mapView.getLayers().getBaseLayer().getProjection().
							fromWgs84(DataManager.getInstance().getInitialCenterLatitude(), DataManager.getInstance().getInitialCenterLongitude()));
			}
		}

		Thread thread = new Thread(new CenterMapRunnable());
		thread.start();
	}

	// For every POI category, one layer will be created. The POIs will then be added to the layer
	// and the layer will be added ArrayList<E>p
	public void createMapPOILayers(TMSMapLayer mapLayer) {

		final TMSMapLayer layer = mapLayer;

		class CreatePOILayersRunnable implements Runnable {

			@Override
			public void run() {

				ArrayList<PoiCategory> categories = DataManager.getInstance().getCategories();

				LabelStyle style = LabelStyle.builder()
						.setTitleAlign(Align.CENTER).
						setTitleFont(Typeface.create("Arial", Typeface.BOLD), 26)
						.setDescriptionFont(Typeface.create("Arial", Typeface.NORMAL), 22)
						.setDescriptionAlign(Align.LEFT)
						.setDescriptionFont(Typeface.create("Arial", Typeface.NORMAL), 26)
						.build();

				for(int i = 0; i < categories.size(); i++) {

					ArrayList<Poi> pois  = new ArrayList<Poi>();

					if(categories.get(i).getName().equalsIgnoreCase("MEOC")) {

						Iterator<OCMeocALTDTO> it = GlobalVar.db.getAllMeocsForMap().iterator();
						while(it.hasNext()) {

							OCMeocALTDTO meoc = it.next();
							Poi poi = new Poi(i, Double.valueOf(((SphereDTO)meoc.getSnapshot().getLocationArea()).getCentre().getLongitude().doubleValue()),
									Double.valueOf(((SphereDTO)meoc.getSnapshot().getLocationArea()).getCentre().getLatitude().doubleValue()),
									"MEOC: "+meoc.getTitle(), "", meoc.getTitle(), meoc.getId());
							pois.add(poi);
						}
					}
					else if(categories.get(i).getName().equalsIgnoreCase("FRC")) {

						Iterator<ActorFRCALTDTO> it = GlobalVar.db.getAllFRChiefsForMap().iterator();
						while(it.hasNext()) {

							ActorFRCALTDTO frc = it.next();

							Poi poi = new Poi(i, Double.valueOf(((SphereDTO)frc.getSnapshot().getLocationArea()).getCentre().getLongitude().doubleValue()),
									Double.valueOf(((SphereDTO)frc.getSnapshot().getLocationArea()).getCentre().getLatitude().doubleValue()),
									"FRC: "+frc.getTitle(), "", frc.getTitle(), frc.getId());
							pois.add(poi);
						}

					}
					else if(categories.get(i).getName().equalsIgnoreCase("FR")) {

						Iterator<ActorFRALTDTO> it = GlobalVar.db.getAllFirstRespondersForMap().iterator();
						while(it.hasNext()) {

							ActorFRALTDTO fr = it.next();
							Poi poi = new Poi(i, Double.valueOf(((SphereDTO)fr.getSnapshot().getLocationArea()).getCentre().getLongitude().doubleValue()),
									Double.valueOf(((SphereDTO)fr.getSnapshot().getLocationArea()).getCentre().getLatitude().doubleValue()),
									"FR: "+fr.getTitle(), "", fr.getTitle(), fr.getId());
							pois.add(poi);
						}
					}

					if(pois.size() > 0) {

						if(i==0)
							Log.e("NUMBER OF MEOCS", String.valueOf(pois.size()));
						else if(i==1)
							Log.e("NUMBER OF FRC", String.valueOf(pois.size()));
						else if(i==2)
							Log.e("NUMBER OF FR", String.valueOf(pois.size()));

						// Create the Layer
						MarkerLayer markerLayer = new MarkerLayer(layer.getProjection());
						// Load POI bitmap
						AssetManager assets = getResources().getAssets();
						InputStream buf;
						Bitmap pointMarker = null;
						try {
							String imagePath = new String(categories.get(i).getMarkerBitmap());
							buf = new BufferedInputStream((assets.open(new String(imagePath))));
							pointMarker = BitmapFactory.decodeStream(buf);
						} catch (IOException e) {
							e.printStackTrace();
						}

						for(int j = 0; j < pois.size(); j++) {
							Poi poi = pois.get(j);

							//POI Bitmap
							MarkerStyle markerStyle;
							// If something went wrong (e.g. bitmap missing) it defaults to the black marker
							if(pointMarker != null){
								markerStyle = MarkerStyle.builder().setBitmap(pointMarker).setSize(0.6f).setColor(Color.WHITE).build();
							}
							else{
								pointMarker = UnscaledBitmapLoader.decodeResource(getResources(), R.drawable.marker_black);
								markerStyle = MarkerStyle.builder().setBitmap(pointMarker).setSize(0.6f).setColor(Color.WHITE).build();
							}

							Boolean isFR;
							if(poi.getPoiCategoryCode()!=0)
								isFR = true;
							else
								isFR = false;

							MyLabel markerLabel = new MyLabel(poi.getLabelTitle(), poi.getLabelText(), style, poi.getObjectID(), isFR);

							// POI Location
							MapPos markerLocation = layer.getProjection().fromWgs84(poi.getLongitude(), poi.getLatitude());

							if(!String.valueOf(poi.getObjectID()).equalsIgnoreCase(String.valueOf(cuID))) {
								// Add it to the marker layer
								markerLayer.add(new Marker(markerLocation, markerLabel, markerStyle, null));
								markerLayerList.add(markerLayer);
								mapView.getLayers().addLayer(markerLayer);
							}
							else {
								currentFRLayer.clear();
								currentFRLayer.add(new Marker(markerLocation, markerLabel, markerStyle, null));
								markerLayerList.add(currentFRLayer);
								mapView.getLayers().addLayer(currentFRLayer);
							}

						}

						markerLayerList.add(markerLayer);
						mapView.getLayers().addLayer(markerLayer);
					}
				}
			}
		}

		Thread runnable = new Thread(new CreatePOILayersRunnable());
		runnable.start();
	}


	protected void initGps(final MyLocationCircle locationCircle) {
		final Projection proj = mapView.getLayers().getBaseLayer().getProjection();

		LocationListener locationListener = new LocationListener() 
		{
			public void onLocationChanged(Location location) {
				if (locationCircle != null) {
					locationCircle.setLocation(proj, location);
					locationCircle.setVisible(true);
				}
			}

			public void onStatusChanged(String provider, int status, Bundle extras) {}

			public void onProviderEnabled(String provider) {}

			public void onProviderDisabled(String provider) {}
		};

		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);	        
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 100, locationListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
	}


	public class MainMapReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context ctx, Intent intent) {

			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_MAP_UPDATE)) {

				for(MarkerLayer m : markerLayerList) {
					mapView.getLayers().removeLayer(m);
				}

				for(GeometryLayer gm : geometrylayerList) {
					mapView.getLayers().removeLayer(gm);
				}

				markerLayerList.clear();
				geometrylayerList.clear();

				createMapPOILayers((TMSMapLayer) mapView.getLayers().getBaseLayer());

				//*****************************************************************************************************************
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_ALARM_RECEIVED)) {
				if(intent.hasExtra("frID")) {
					final long frID = intent.getLongExtra("frID", -1);
					if(frID!=-1) {
						Log.e("anim", "anim received");
						final ImageView alarmIcon = (ImageView)findViewById(R.id.mapAlarmIndicator);
						alarmIcon.setVisibility(View.VISIBLE);
						Animation anim = new MeasCriticalAnimation(0.0f, 1.0f);
						alarmIcon.startAnimation(anim);
						animTimer.schedule(new AlarmAnimStopTask(alarmIcon), 8000);
						alarmIcon.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								Intent alarmIntent= new Intent(v.getContext(), Alarms.class);
								alarmIntent.putExtra("frID", frID);
								v.getContext().startActivity(alarmIntent);
							}
						});
					}
				}
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_MAP_FR_UPDATE)) {
				if(cuID!=null && cuID!=-1) {
					if(!cuUpdate) {
						cuUpdate = true;
						new Thread(new currentUserMapUpdater()).start();
					}
				}
			}
		}
	}


	class currentUserMapUpdater implements Runnable {

		LabelStyle style = LabelStyle.builder()
				.setTitleAlign(Align.CENTER).
				setTitleFont(Typeface.create("Arial", Typeface.BOLD), 26)
				.setDescriptionFont(Typeface.create("Arial", Typeface.NORMAL), 22)
				.setDescriptionAlign(Align.LEFT)
				.setDescriptionFont(Typeface.create("Arial", Typeface.NORMAL), 26)
				.build();

		@Override
		public void run() {

			ActorFRCALTDTO frc = GlobalVar.db.getFRChiefById(cuID);
			if(frc!=null) {
				Poi poi = new Poi(1, Double.valueOf(((SphereDTO)frc.getSnapshot().getLocationArea()).getCentre().getLongitude().doubleValue()),
						Double.valueOf(((SphereDTO)frc.getSnapshot().getLocationArea()).getCentre().getLatitude().doubleValue()),
						"FRC: "+frc.getTitle(), "", frc.getTitle(), frc.getId());

				Bitmap pointMarker = UnscaledBitmapLoader.decodeResource(getResources(), R.drawable.frc_marker );
				MarkerStyle markerStyle = MarkerStyle.builder().setBitmap(pointMarker).setSize(0.6f).setColor(Color.WHITE).build();

				MyLabel markerLabel = new MyLabel(poi.getLabelTitle(), poi.getLabelText(), style, poi.getObjectID(), true);

				// POI Location
				MapPos markerLocation = mapView.getLayers().getBaseLayer().getProjection().fromWgs84(poi.getLongitude(), poi.getLatitude());

				// Add it to the marker layer
				currentFRLayer.clear();
				currentFRLayer.add(new Marker(markerLocation, markerLabel, markerStyle, null));
			}

			cuUpdate = false;

		}
	}





}