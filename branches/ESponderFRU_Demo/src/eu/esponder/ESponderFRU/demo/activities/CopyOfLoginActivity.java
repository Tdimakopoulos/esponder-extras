package eu.esponder.ESponderFRU.demo.activities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import eu.esponder.ESponderFRU.demo.R;
import eu.esponder.ESponderFRU.demo.application.GlobalVar;
import eu.esponder.ESponderFRU.demo.data.FRActionObjectiveEntity;
import eu.esponder.ESponderFRU.demo.handlers.LoginHandler;
import eu.esponder.ESponderFRU.demo.runnables.MapRequestEventSender;
import eu.esponder.ESponderFRU.demo.utils.ESponderUtils;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.login.LoginRequestEvent;

public class CopyOfLoginActivity extends Activity {

	Button entryButton;
	LoginHandler loginHandler;
	TextView beText, sensorText, logintext;
	ProgressDialog pdialog;
	LoginReceiver loginReceiver;
	AlertDialog networkInterfaceDialog;
	IntentFilter filter;
	Handler handler;
	Boolean loggedIn = false;
	final Boolean wifiEnabled = false;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.loginlayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		setTitle("ESponder FRU Application");
		setLoginButtonOnClickListener();

		loginHandler = new LoginHandler(beText, sensorText, logintext);

		final WifiManager wifimanager = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		if(!wifimanager.isWifiEnabled()) {
			AlertDialog.Builder builder = new Builder(CopyOfLoginActivity.this);
			builder.setIcon(R.drawable.warning);
			builder.setTitle("Deactivated WiFi");
			builder.setMessage("Would you like to activate the WiFi of your smartphone?");
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					wifimanager.setWifiEnabled(true);
					loginHandler.postDelayed(loginInitialiser, 5000);
				}
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					CopyOfLoginActivity.this.finish();
				}
			});

			networkInterfaceDialog = builder.create();
			networkInterfaceDialog.show();
		}
		else
			loginHandler.postDelayed(loginInitialiser, 500);

//		createsampleActionParts();
		printObjectives((long) 4);

	}


	private void setLoginButtonOnClickListener(){
		((GlobalVar)CopyOfLoginActivity.this.getApplicationContext()).setLoggedIn(true);
		this.entryButton = (Button) findViewById(R.id.login_button);
		this.entryButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(CopyOfLoginActivity.this, MainActivity.class);
				startActivity(intent);
			}
		});
	}


	private Runnable loginInitialiser = new Runnable() {

		@Override
		public void run() {
			pdialog = ProgressDialog.show(CopyOfLoginActivity.this, "Log in attempt", "Sending login request to ESponder server...", false, true);
			createEventForLogin(CopyOfLoginActivity.this);
		}
	};

	private void createEventForLogin(final Context context) {

		class LoginEventSender implements Runnable {
			@Override
			public void run() {
				Log.d("CREATE EVENT FOR LOGIN THREAD", "RUNNING, SENDING REQUEST");

				ESponderUserDTO user = new ESponderUserDTO();
				user.setId(Long.valueOf(0));

				LoginRequestEvent pEvent = new LoginRequestEvent();
				pEvent.setIMEI(((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
				pEvent.setPassword("Password");
				pEvent.setUsername("FRC");
				pEvent.setPkikey(retrievePKIKeyFromFile());
				pEvent.setiLoginType(1);
				pEvent.setEventAttachment(user);
				pEvent.setJournalMessage("Login request Username : "+pEvent.getUsername()+" PKI : "+pEvent.getPkikey());
				pEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
				pEvent.setEventTimestamp(new Date());
				pEvent.setEventSource(user);
				String myIPAddress = ESponderUtils.getIPAddress(context, true);
				if(myIPAddress!= null && !myIPAddress.equalsIgnoreCase(""))
					pEvent.setMyip(myIPAddress);
				ObjectMapper mapper = new ObjectMapper();
				String jsonObject = null;
				try {
					jsonObject = mapper.writeValueAsString(pEvent);
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(jsonObject != null) {
					Intent intent = new Intent(GlobalVar.LOGIN_REQUEST_INTENT_ACTION);
					intent.addCategory(GlobalVar.INTENT_CATEGORY);
					intent.putExtra("event", jsonObject);
					context.sendBroadcast(intent);
				}
				else
					Log.e("SEND LOGIN EVENT","SERIALIZATION HAS FAILED");

			}
		}

		Thread thread = new Thread(new LoginEventSender());
		thread.start();
	}


	private void createEventForMapInit(final Context context) {

		Thread thread = new Thread(new MapRequestEventSender(CopyOfLoginActivity.this, "LoginActivity"));
		thread.start();
	}

	public class LoginReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_APPROVED)) {
				Log.e("Login receiver", "RUNNING LOGIN RESPONSE EVENT APPROVED");
				loggedIn = true;
				if(pdialog != null)
					if(pdialog.isShowing())
						pdialog.dismiss();

				Runnable requestMapPositions = new Runnable() {

					@Override
					public void run() {
						if(CopyOfLoginActivity.this.pdialog.isShowing())
							CopyOfLoginActivity.this.pdialog.dismiss();

						CopyOfLoginActivity.this.pdialog = ProgressDialog.show(CopyOfLoginActivity.this, 
								"Welcome First Responder", "\n\nRequesting information from the backend...", false, true);
						createEventForMapInit(CopyOfLoginActivity.this);
					}
				};
				loginHandler.post(requestMapPositions);

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_DENIED)) {
				Log.e("Login receiver", "RUNNING LOGIN RESPONSE EVENT DENIED");
				if(pdialog != null)
					if(pdialog.isShowing())
						pdialog.dismiss();

				// Unsuccessful Login
				AlertDialog.Builder builder =new Builder(CopyOfLoginActivity.this);
				builder.setCancelable(false);
				builder.setIcon(R.drawable.erroricon);
				builder.setTitle("Login Failed");
				builder.setMessage("Unauthorized Access Attempt, please retry...");
				builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						CopyOfLoginActivity.this.finish();
					}
				});

				builder.create().show();

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_ERROR)) {
				Log.e("Login receiver", "RUNNING LOGIN RESPONSE EVENT ERROR");
				if(pdialog != null)
					if(pdialog.isShowing())
						pdialog.dismiss();

				AlertDialog.Builder builder =new Builder(CopyOfLoginActivity.this);
				builder.setCancelable(false);
				builder.setIcon(R.drawable.erroricon);
				builder.setTitle("Login Failed");
				builder.setMessage("Scrambled response from server, please contact your network administrator...");
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						CopyOfLoginActivity.this.finish();
					}
				});

				builder.create().show();
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY)) {
				if(loggedIn) {
					Log.e("Login receiver", "RUNNING MAP RESPONSE EVENT APPROVED");
					// Data have been received successfully for the hierarchy
					if(CopyOfLoginActivity.this.pdialog.isShowing())
						CopyOfLoginActivity.this.pdialog.dismiss();

					// Enter the map main screen
					startActivity(new Intent(CopyOfLoginActivity.this, MainActivity.class));
				}
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR)) {
				Log.e("Login receiver", "RUNNING MAP RESPONSE EVENT ERROR");
				// Error came up while retrieving the map hierarchy
				if(CopyOfLoginActivity.this.pdialog.isShowing())
					CopyOfLoginActivity.this.pdialog.dismiss();


				AlertDialog.Builder builder =new Builder(CopyOfLoginActivity.this);
				builder.setCancelable(false);
				builder.setIcon(R.drawable.erroricon);
				builder.setTitle("map Init Error");
				builder.setMessage("Error occured while retrieving the initial data for the map...");
				builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						CopyOfLoginActivity.this.finish();
					}
				});

				builder.create().show();
			}

		}
	}


	@Override
	protected void onDestroy() {
		if(pdialog.isShowing())
			pdialog.dismiss();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		unregisterReceiver(loginReceiver);
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		loginReceiver = new LoginReceiver();
		filter = new IntentFilter(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_APPROVED);
		filter.addAction(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_DENIED);
		filter.addAction(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_ERROR);
		filter.addAction(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY);
		filter.addAction(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR);
		registerReceiver(loginReceiver, filter);
	}


	private String retrievePKIKeyFromFile() {

		String filename = "pkikey.txt";
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,filename);

		//Read text from file
		StringBuilder sbuilder = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				sbuilder.append(line);
			}
			br.close();
		}
		catch (IOException e) {
			Log.d("FILE_HANDLER", "Error occurred while opening file");
			e.printStackTrace();
		}
		String skey = sbuilder.toString();
		Log.e("PKI",skey);

		return skey;
	}


//	public void createsampleActionParts() {
//
//		int counter = GlobalVar.db.getActionPartCount();
//		Log.e("COUNT OF OBJECTIVES", String.valueOf(counter));
//
//		if(counter == 0) {
//
//			ActionDTO action1 = new ActionDTO();
//			action1.setActionOperation(ActionOperationEnumDTO.MOVE);
//			action1.setId(Long.valueOf(1));
//			action1.setTitle("Action1");
//			action1.setActionStage(ActionStageEnumDTO.InProgress);
//			action1.setType("oti nanai");
//			action1.setSeverityLevel(SeverityLevelDTO.SERIOUS);
//
//			ActionObjectiveDTO ao1 = new ActionObjectiveDTO();
//			ao1.setAction(action1);
//			ao1.setActionStage(ActionStageEnumDTO.InProgress);
//			ao1.setId(Long.valueOf(1));
//			SphereDTO sphere1 = new SphereDTO();
//			sphere1.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
//			ao1.setLocationArea(sphere1);
//			ao1.setPeriod(new PeriodDTO(Long.valueOf(0), Long.valueOf(0)));
//			ao1.setTitle("AO_1");
//
//
//			ActionObjectiveDTO ao2 = new ActionObjectiveDTO();
//			ao2.setAction(action1);
//			ao2.setActionStage(ActionStageEnumDTO.InProgress);
//			ao2.setId(Long.valueOf(2));
//			SphereDTO sphere2 = new SphereDTO();
//			sphere2.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
//			ao2.setLocationArea(sphere2);
//			ao2.setPeriod(new PeriodDTO(Long.valueOf(0), Long.valueOf(0)));
//			ao2.setTitle("AO_2");
//
//			action1.setActionObjectives(new HashSet<Long>());
//			action1.getActionObjectives().add(Long.valueOf(1));
//			action1.getActionObjectives().add(Long.valueOf(2));
//
//
//			//**********************************************************************************************
//
//
//			ActionPartDTO ap1 = new ActionPartDTO();
//			ap1.setActionId(Long.valueOf(1));
//			ap1.setActionOperation(ActionOperationEnumDTO.MOVE);
//			ap1.setActionStage(ActionStageEnumDTO.InProgress);
//			ap1.setId(Long.valueOf(1));
//			ap1.setActor(Long.valueOf(4));
//			ap1.setSeverityLevel(SeverityLevelDTO.SERIOUS);
//			ap1.setTitle("ActionPart1");
//
//			ActionPartObjectiveDTO apo1 = new ActionPartObjectiveDTO();
//			apo1.setActionPartID(Long.valueOf(1));
//			apo1.setId(Long.valueOf(1));
//			SphereDTO sphere3 = new SphereDTO();
//			sphere3.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
//			apo1.setLocationArea(sphere3);
//			apo1.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
//			apo1.setTitle("APO1");
//
//			ActionPartObjectiveDTO apo2 = new ActionPartObjectiveDTO();
//			apo2.setActionPartID(Long.valueOf(1));
//			apo2.setId(Long.valueOf(2));
//			SphereDTO sphere4 = new SphereDTO();
//			sphere4.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
//			apo2.setLocationArea(sphere4);
//			apo2.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
//			apo2.setTitle("APO2");
//
//
//
//			//*****************************************************************************
//
//			ActionPartDTO ap2 = new ActionPartDTO();
//			ap2.setActionId(Long.valueOf(1));
//			ap2.setActionOperation(ActionOperationEnumDTO.MOVE);
//			ap2.setActionStage(ActionStageEnumDTO.InProgress);
//			ap2.setId(Long.valueOf(2));
//			ap2.setActor(Long.valueOf(5));
//			ap2.setSeverityLevel(SeverityLevelDTO.SERIOUS);
//			ap2.setTitle("ActionPart2");
//
//			ActionPartObjectiveDTO apo3 = new ActionPartObjectiveDTO();
//			apo3.setActionPartID(Long.valueOf(2));
//			apo3.setId(Long.valueOf(3));
//			SphereDTO sphere5 = new SphereDTO();
//			sphere5.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
//			apo3.setLocationArea(sphere5);
//			apo3.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
//			apo3.setTitle("APO3");
//
//			ActionPartObjectiveDTO apo4 = new ActionPartObjectiveDTO();
//			apo4.setActionPartID(Long.valueOf(2));
//			apo4.setId(Long.valueOf(4));
//			SphereDTO sphere6 = new SphereDTO();
//			sphere6.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
//			apo4.setLocationArea(sphere6);
//			apo4.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
//			apo4.setTitle("APO4");
//
//			//*****************************************************************************
//
//			ActionPartDTO ap3 = new ActionPartDTO();
//			ap3.setActionId(Long.valueOf(1));
//			ap3.setActionOperation(ActionOperationEnumDTO.MOVE);
//			ap3.setActionStage(ActionStageEnumDTO.InProgress);
//			ap3.setId(Long.valueOf(3));
//			ap3.setActor(Long.valueOf(6));
//			ap3.setSeverityLevel(SeverityLevelDTO.SERIOUS);
//			ap3.setTitle("ActionPart3");
//
//			ActionPartObjectiveDTO apo5 = new ActionPartObjectiveDTO();
//			apo5.setActionPartID(Long.valueOf(3));
//			apo5.setId(Long.valueOf(5));
//			SphereDTO sphere7 = new SphereDTO();
//			sphere7.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
//			apo5.setLocationArea(sphere7);
//			apo5.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
//			apo5.setTitle("APO5");
//
//			ActionPartObjectiveDTO apo6 = new ActionPartObjectiveDTO();
//			apo6.setActionPartID(Long.valueOf(3));
//			apo6.setId(Long.valueOf(6));
//			SphereDTO sphere8 = new SphereDTO();
//			sphere8.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
//			apo6.setLocationArea(sphere8);
//			apo6.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
//			apo6.setTitle("APO6");
//
//
//			NewActionEvent event = new NewActionEvent();
//			event.setEventAttachment(action1);
//			event.setEventSeverity(SeverityLevelDTO.SERIOUS);
//			event.setEventTimestamp(new Date());
//			Set<ActionPartDTO> apSet = new HashSet<ActionPartDTO>();
//			apSet.add(ap1);
//			apSet.add(ap2);
//			apSet.add(ap3);
//
//			Set<ActionObjectiveDTO> aoSet = new HashSet<ActionObjectiveDTO>();
//			aoSet.add(ao1);
//			aoSet.add(ao2);
//
//
//			Set<ActionPartObjectiveDTO> apoSet = new HashSet<ActionPartObjectiveDTO>();
//			apoSet.add(apo1);
//			apoSet.add(apo2);
//			apoSet.add(apo3);
//			apoSet.add(apo4);
//			apoSet.add(apo5);
//			apoSet.add(apo6);
//
//			event.setActionObjectives(aoSet);
//			event.setActionPartObjectives(apoSet);
//			event.setActionParts(apSet);
//
//			ObjectMapper mapper = new ObjectMapper();
//			String eventJSON = null;
//			try {
//				eventJSON = mapper.writeValueAsString(event);
//			} catch (JsonGenerationException e) {
//				e.printStackTrace();
//			} catch (JsonMappingException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//
//			if(eventJSON != null) {
//
//				Intent intent = new Intent();
//				intent.addCategory(GlobalVar.INTENT_CATEGORY);
//				intent.setAction(GlobalVar.NEW_ACTION_EVENT);
//				intent.putExtra("event", eventJSON);
//				CopyOfLoginActivity.this.sendBroadcast(intent);
//
//			}
//			else
//				Log.e("ACTIONS CREATION","SOMETHING HAS GONE WRONG, EVENT NOT CREATED");
//
//		}
//
//	}

	public void printObjectives (Long frID) {

		List<FRActionObjectiveEntity> list = GlobalVar.db.getAllObjectivesByFR(frID);
		if(list!= null)
			if(list.size()>0) {
				for(FRActionObjectiveEntity apo : list)
					Log.e("AOP",apo.getTitle());
			}
			else
				Log.e("APO","NO RESULTS FOUND");
	}


}
