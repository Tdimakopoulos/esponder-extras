package eu.esponder.ESponderFRU.demo.activities;

import java.util.Timer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import eu.esponder.ESponderFRU.demo.R;
import eu.esponder.ESponderFRU.demo.application.GlobalVar;
import eu.esponder.ESponderFRU.demo.runnables.GetSensorsPCDTimerTask;
import eu.esponder.ESponderFRU.demo.runnables.SPInformationTimerTask;


public class MainFRActivity extends Activity  {

	MainMapReceiver receiver;

	Handler handler;

	Long cuID;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main_fr);

		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		setTitle("ESponder FRC Application");

		handler = new Handler();

		cuID = getSharedPreferences(GlobalVar.spName, MODE_PRIVATE).getLong(GlobalVar.spESponderUserID, -1);
		Log.e("POIs", "FOUND THE CURRENT USER:"+cuID);
		
		if( ((GlobalVar)getApplicationContext()).getRole() == 0 || ((GlobalVar)getApplicationContext()).getRole() == 1 )  {

			if(GlobalVar.pcdSensorTimer == null) {
				GlobalVar.pcdSensorTimer = new Timer();
				// The sensor measurements will be updated only when the current user of the app is an FRC		
				if( ((GlobalVar)getApplicationContext()).getRole() == 0 )
					GlobalVar.pcdSensorTimer.scheduleAtFixedRate(new GetSensorsPCDTimerTask(MainFRActivity.this.getApplicationContext()), 2000, 10000);
				
				// The smartphone statistics will be updated only when the current user of the app is an FRC or FR
				if( ((GlobalVar)getApplicationContext()).getRole() == 0 || ((GlobalVar)getApplicationContext()).getRole() == 1 )
					GlobalVar.pcdSensorTimer.scheduleAtFixedRate(new SPInformationTimerTask(MainFRActivity.this.getApplicationContext()), 0, 15000);
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(receiver);
	}


	@Override
	protected void onResume() {
		super.onResume();
		receiver = new MainMapReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_ALARM_RECEIVED);
		registerReceiver(receiver, filter);
	}

	public class MainMapReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context ctx, Intent intent) {}
		
	}

}