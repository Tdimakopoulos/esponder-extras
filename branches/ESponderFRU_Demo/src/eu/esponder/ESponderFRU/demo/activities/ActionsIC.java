package eu.esponder.ESponderFRU.demo.activities;

import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import eu.esponder.ESponderFRU.demo.R;
import eu.esponder.ESponderFRU.demo.adapters.ActionPartExpandableAdapter;
import eu.esponder.ESponderFRU.demo.adapters.MenuAdapter;
import eu.esponder.ESponderFRU.demo.application.GlobalVar;
import eu.esponder.ESponderFRU.demo.data.FRActionPartEntity;
import eu.esponder.ESponderFRU.demo.utils.ESponderUtils;

public class ActionsIC extends Activity {

	//	private ImageView menuImage;
	private ExpandableListView actionListView;
	private ActionPartExpandableAdapter apoAdapter;
	private FRActionPartEntity[] apos;
	private Long icID;
	private ActionPartObjectiveReceiver receiver;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.actionslayout);
		setTitle("ESponder - Action Objectives");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		// This function initializes the sliding menus to full functionality
		initializeMenu();

		TextView actionsHeader = (TextView) findViewById(R.id.actionHeader);
		actionsHeader.setText("Action Objectives");



		SharedPreferences prefs = ActionsIC.this.getSharedPreferences(GlobalVar.spName, MODE_PRIVATE);
		icID = prefs.getLong(GlobalVar.spESponderUserID, -1);
		if(icID != -1) {
			findObjectivesForAdapter();
			actionListView = (ExpandableListView) findViewById(R.id.apoList);
			apoAdapter = new ActionPartExpandableAdapter(ActionsIC.this, apos);
			actionListView.setAdapter(apoAdapter);
		}


	}


	//Create sample data for Meocs, FRCs, FRs
	// We need id of member, Title of member, type of member etc
	private void initializeMenu() {

		ListView menuList = null;
		View menuView = null;
		MenuAdapter menuAdapter = null;

		Resources res = getResources();
		String[] menuItems = null;

		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {

			menuView = LayoutInflater.from(ActionsIC.this).inflate(R.layout.menu_frame, null);
			menuList = (ListView) menuView.findViewById(R.id.MenuAsList);
			menuItems = res.getStringArray(R.array.frc_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.FRCMenuClickListener(ActionsIC.this));
			menuAdapter = new MenuAdapter(ActionsIC.this, R.layout.row_sidemenu, menuItems, true);
		}
		else if( ((GlobalVar)getApplicationContext()).getRole() == 2 ) {
			menuList = (ListView) findViewById(R.id.applicationMenu);
			menuItems = res.getStringArray(R.array.ic_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.ICMenuClickListener(ActionsIC.this));
			menuAdapter = new MenuAdapter(ActionsIC.this, R.layout.row_sidemenu, menuItems, false);
		}

		menuList.setAdapter(menuAdapter);
	}

	private void findObjectivesForAdapter() {

		if(icID != -1) {
			List<FRActionPartEntity> apoList = GlobalVar.db.getActionPartsForFR(icID);
			if( apoList.size() > 0 ) {
				Iterator<FRActionPartEntity> it = apoList.iterator();
				apos = new FRActionPartEntity[apoList.size()];
				int counter = 0;
				while(it.hasNext()) {
					apos[counter] = it.next();
					counter++;
				}
			}
			else
				apos = new FRActionPartEntity[1];
		}

	}



	class ActionPartObjectiveReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_APO_STATUS_CHANGED)) {
				findObjectivesForAdapter();
				apoAdapter.setActionParts(apos);
				apoAdapter.notifyDataSetChanged();
			}	
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(receiver!=null)
			unregisterReceiver(receiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(receiver == null)
			receiver = new ActionPartObjectiveReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_APO_STATUS_CHANGED);
		registerReceiver(receiver, filter);
	}

}
