package eu.esponder.ESponderFRU.demo.activities;

import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderFRU.demo.R;
import eu.esponder.ESponderFRU.demo.adapters.ActionPartExpandableAdapter;
import eu.esponder.ESponderFRU.demo.adapters.MenuAdapter;
import eu.esponder.ESponderFRU.demo.application.GlobalVar;
import eu.esponder.ESponderFRU.demo.data.FRActionPartEntity;
import eu.esponder.ESponderFRU.demo.utils.ESponderUtils;

public class Actions extends SlidingActivity {

	//	private ImageView menuImage;
	private SlidingMenu sm;
	private ExpandableListView actionListView;
	private ActionPartExpandableAdapter apoAdapter;
	private FRActionPartEntity[] apos;
	private Long frID;
	private ActionPartObjectiveReceiver receiver;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.actionslayout);
		setTitle("ESponder - Action Objectives");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		// This function initializes the sliding menus to full functionality
		initializeMenu();

		TextView actionsHeader = (TextView) findViewById(R.id.actionHeader);
		actionsHeader.setText("Action Objectives");

		if(getIntent().hasExtra("frID")) {

			frID = getIntent().getLongExtra("frID", -1);
			if(frID != -1) {
				String frTitle = GlobalVar.db.getFirstResponderTitle(frID);
				if(frTitle!=null)
					actionsHeader.setText("Action Objectives for "+frTitle);
				findObjectivesForAdapter();
				actionListView = (ExpandableListView) findViewById(R.id.apoList);
				//				apoAdapter = new ActionObjectivesAdapter(Actions.this, apos, frID);
				apoAdapter = new ActionPartExpandableAdapter(Actions.this, apos);
				actionListView.setAdapter(apoAdapter);
			}
		}

	}


	//Create sample data for Meocs, FRCs, FRs
	// We need id of member, Title of member, type of member etc

	private void initializeMenu() {

		ListView menuList = null;
		View menuView = null;
		MenuAdapter menuAdapter = null;

		Resources res = getResources();
		String[] menuItems = null;

		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {

			menuView = LayoutInflater.from(Actions.this).inflate(R.layout.menu_frame, null);
			menuList = (ListView) menuView.findViewById(R.id.MenuAsList);
			menuItems = res.getStringArray(R.array.frc_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.FRCMenuClickListener(Actions.this));
			menuAdapter = new MenuAdapter(Actions.this, R.layout.row_sidemenu, menuItems, true);
		}
		else if( ((GlobalVar)getApplicationContext()).getRole() == 2 ) {
			menuList = (ListView) findViewById(R.id.applicationMenu);
			menuItems = res.getStringArray(R.array.ic_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.ICMenuClickListener(Actions.this));
			menuAdapter = new MenuAdapter(Actions.this, R.layout.row_sidemenu, menuItems, false);
		}
		
		menuList.setAdapter(menuAdapter);
		
		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {
			setBehindContentView(menuView);
			sm = getSlidingMenu();
			sm.setShadowWidthRes(R.dimen.shadow_width);
			sm.setShadowDrawable(R.drawable.shadow);
			sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
			sm.setFadeDegree(0.35f);
			sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
			setSlidingActionBarEnabled(true);
		}
	}

	private void findObjectivesForAdapter() {

		if(frID != -1) {
			List<FRActionPartEntity> apoList = GlobalVar.db.getActionPartsForFR(frID);
			if( apoList.size() > 0 ) {
				Iterator<FRActionPartEntity> it = apoList.iterator();
				apos = new FRActionPartEntity[apoList.size()];
				int counter = 0;
				while(it.hasNext()) {
					apos[counter] = it.next();
					counter++;
				}
			}
			else
				apos = new FRActionPartEntity[1];
		}

	}



	class ActionPartObjectiveReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_APO_STATUS_CHANGED)) {
				findObjectivesForAdapter();
				apoAdapter.setActionParts(apos);
				apoAdapter.notifyDataSetChanged();
			}	
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(receiver!=null)
			unregisterReceiver(receiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(receiver == null)
			receiver = new ActionPartObjectiveReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_APO_STATUS_CHANGED);
		registerReceiver(receiver, filter);
	}

}
