package eu.esponder.ESponderFRU.demo.gps;

import java.io.IOException;
import java.util.Date;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import eu.esponder.ESponderFRU.demo.application.GlobalVar;
import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.datafusion.query.ESponderDFQueryRequestEvent;

public class ESponderLocationListener implements LocationListener {
	
	private Context context;
	
    public ESponderLocationListener(Context context) {
		super();
		this.setContext(context);
	}

	@Override
    public void onLocationChanged(Location loc) {
        
        //FIXME Karfoti timi, remove
        SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
        Long frID = prefs.getLong(GlobalVar.spESponderUserID, -1);
        
        if(frID != -1) {
        	
        	ESponderUserDTO user = new ESponderUserDTO();
    		user.setId(Long.valueOf(0));
    		
            ESponderDFQueryRequestEvent pEvent = new ESponderDFQueryRequestEvent();
    		pEvent.setIMEI(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
    		pEvent.setQueryID(Long.valueOf(2));	// Setting id = 2 is meant for the transmission of the location
    		pEvent.setRequestID(Long.valueOf(20));
    		pEvent.setJournalMessage("JM");
    		
    		
    		QueryParamsDTO pParams= QueryParamsDTO.with("userid", String.valueOf(frID));
    		pParams.and("alt", String.valueOf(loc.getAltitude()));
    		pParams.and("long", String.valueOf(loc.getLongitude()));
    		pParams.and("lat", String.valueOf(loc.getLatitude()));
    		Long date = new Date().getTime();
    		pParams.and("datefrom", String.valueOf(date));
    		pParams.and("dateto", String.valueOf(date));
    		pEvent.setParams(pParams);
    		pEvent.setEventAttachment(user);
    		pEvent.setJournalMessage("Test Event");
    		pEvent.setEventTimestamp(new Date());
    		pEvent.setEventSeverity(SeverityLevelDTO.UNDEFINED);
    		pEvent.setEventSource(user);
    		
          ObjectMapper mapper = new ObjectMapper();
          String eventJson = null;
          try {
  			eventJson = mapper.writeValueAsString(pEvent);
  		} catch (JsonGenerationException e) {
  			e.printStackTrace();
  		} catch (JsonMappingException e) {
  			e.printStackTrace();
  		} catch (IOException e) {
  			e.printStackTrace();
  		}
          if(eventJson != null) {
          	
          	Intent intent = new Intent(GlobalVar.MAP_REQUEST_HIERARCHY);
  			intent.addCategory(GlobalVar.INTENT_CATEGORY);
  			intent.putExtra("event", eventJson);
  			context.sendBroadcast(intent);
          }
          Log.e("LOCATION CHANGED","NEW LOCATION");	
        }
        
    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
}