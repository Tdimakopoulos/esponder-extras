package eu.esponder.ESponderFRU.demo.adapters;

import java.io.File;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import eu.esponder.ESponderFRU.demo.R;
import eu.esponder.ESponderFRU.demo.data.FRFileEntity;
import eu.esponder.ESponderFRU.demo.exceptions.NotSupportedException;
import eu.esponder.ESponderFRU.demo.service.DownloaderService;
import eu.esponder.ESponderFRU.demo.utils.ESponderUtils;

public class FilesAdapter extends BaseAdapter {

	private Context context;
	private FRFileEntity[] values;

	public FilesAdapter(Context context, FRFileEntity[] alarms) {
		setContext(context);
		setValues(alarms);
	}

	@Override
	public int getCount() {
		return getValues().length;
	}

	@Override
	public Object getItem(int position) {
		return getValues()[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final FRFileEntity file = (FRFileEntity) getItem(position);

		if(file !=null) {

			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.filerowlayout, parent, false);

			TextView fileDescTextView = (TextView) convertView.findViewById(R.id.fileDescription);
			TextView fileStatus = (TextView) convertView.findViewById(R.id.fileStatus);
			ImageView alarmsImage = (ImageView) convertView.findViewById(R.id.filerowimage);

			fileDescTextView.setText(file.getDescription());
			
			int fileTypeID = ESponderUtils.processIconForUriMimeType(file.getRelativePath());
			if(fileTypeID != -1)
				alarmsImage.setImageResource(fileTypeID);
			else
				alarmsImage.setImageResource(R.drawable.unknownfiletype);

			//Alarm Indicator
			if(file.getStatus() == 0) {
				fileStatus.setText("Not downloaded yet, click to retry");
				convertView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						
						Uri data = Uri.parse(file.getURL());
						Intent dfIntent = new Intent(context, DownloaderService.class);
						dfIntent.setData(data);
						dfIntent.putExtra("urlpath", file.getURL());
						dfIntent.putExtra("description", file.getDescription());
						dfIntent.putExtra("fileID", file.getId());
						dfIntent.putExtra("beFileID", file.getSource());
						context.startService(dfIntent);

					}
				});
			}
			else {
				fileStatus.setText("Click to open");
				convertView.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {

						try {
							Intent intent = new Intent(Intent.ACTION_VIEW);
							File fileSD = new File(Environment.getExternalStorageDirectory(), file.getRelativePath());
							intent.setDataAndType(Uri.fromFile(fileSD), ESponderUtils.processUriMimeType(file.getRelativePath()));
							context.startActivity(intent);
						} catch (NotSupportedException e) {
							AlertDialog.Builder builder = new AlertDialog.Builder(context);
							builder.setTitle("Error opening file.");
							builder.setIcon(R.drawable.erroricon);
							builder.setMessage("The file you tried to open is not currently supported.");
							builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
								
								public void onClick(DialogInterface dialog, int which) { }
								
							});
							builder.create().show();
						}						
					}
				});
			}

			// Button indicator text
		}
		else {

			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
				convertView = inflater.inflate(R.layout.noalarmsrowlayout, parent, false);
			}

			TextView alarmDesc = (TextView) convertView.findViewById(R.id.alarmdesc);
			alarmDesc.setText("No files available yet");
		}

		return convertView;
	}

	// Getters and setters for private fields of the Adapter
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public FRFileEntity[] getValues() {
		return values;
	}

	public void setValues(FRFileEntity[] values) {
		this.values = values;
	}


}
