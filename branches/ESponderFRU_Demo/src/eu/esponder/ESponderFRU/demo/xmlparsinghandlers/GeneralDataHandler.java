package eu.esponder.ESponderFRU.demo.xmlparsinghandlers;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import eu.esponder.ESponderFRU.demo.data.DataManager;

public class GeneralDataHandler extends DefaultHandler {
	
	private boolean in_MapServerURL = false;
	private boolean in_InitialMapCenterLongitude = false;
	private boolean in_InitialMapCenterLatitude = false;
	private boolean in_InitialZoom = false;
	
	/** Gets be called on opening tags like: <tag> **/
	@Override
	public void characters(char ch[], int start, int length) {
		String s1 = new String(ch, start, length);

		if (this.in_MapServerURL) {
			DataManager.getInstance().setMapServerURL(s1);
		} else if (this.in_InitialMapCenterLongitude) {
			DataManager.getInstance().setInitialCenterLongitude(Double.valueOf(s1));
		} else if (this.in_InitialMapCenterLatitude) {
			DataManager.getInstance().setInitialCenterLatitude(Double.valueOf(s1));
		}else if (this.in_InitialZoom) {
			DataManager.getInstance().setInitialMapZoom(Float.valueOf(s1));
		}

	}
	
	/** Gets be called on opening tags like: <tag> **/
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if (localName.equals("MapServerURL")) {
			this.in_MapServerURL = true;
		} else if (localName.equals("InitialMapCenterLongitude")) {
			this.in_InitialMapCenterLongitude = true;
		} else if (localName.equals("InitialMapCenterLatitude")) {
			this.in_InitialMapCenterLatitude = true;
		} else if (localName.equals("InitialZoom")) {
			this.in_InitialZoom = true;
		} 		
	}

	/**
	 * Gets be called on closing tags like: </tag>
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		if (localName.equals("MapServerURL")) {
			this.in_MapServerURL = false;
		} else if (localName.equals("InitialMapCenterLongitude")) {
			this.in_InitialMapCenterLongitude = false;
		} else if (localName.equals("InitialMapCenterLatitude")) {
			this.in_InitialMapCenterLatitude = false;
		} else if (localName.equals("InitialZoom")) {
			this.in_InitialZoom = false;
		} 	
	}

	public void startDocument() throws SAXException {

	}

	@Override
	public void endDocument() throws SAXException {
		// Nothing to do
	}

}
