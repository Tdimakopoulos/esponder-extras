package eu.esponder.ESponderFRU.demo.xmlparsinghandlers;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import eu.esponder.ESponderFRU.demo.data.PoiCategory;

public class POICategoriesHandler  extends DefaultHandler {

	private boolean in_Category = false;
	private boolean in_Code = false;
	private boolean in_Name = false;
	private boolean in_MapMarkerBitmap = false;
	
	ArrayList<PoiCategory> categories = new ArrayList<PoiCategory>();
	PoiCategory tempCategory = null;
	
	public ArrayList<PoiCategory> getPoiCategories(){
		return categories;
	}
	
	/** Gets be called on opening tags like: <tag> **/
	@Override
	public void characters(char ch[], int start, int length) {
		String s1 = new String(ch, start, length);

		if (this.in_Category) {
			if (this.in_Code) {
				tempCategory.setCategoryCode(Integer.valueOf(s1));
			} else if (this.in_Name) {
				tempCategory.setName(s1);
			}else if (this.in_MapMarkerBitmap) {
				tempCategory.setMarkerBitmap(s1);
			}
		}

	}
	
	/** Gets be called on opening tags like: <tag> **/
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if (localName.equals("Category")) {
			tempCategory = new PoiCategory();
			this.in_Category = true;
		} else if (localName.equals("Code")) {
			this.in_Code = true;
		} else if (localName.equals("Name")) {
			this.in_Name = true;
		} else if (localName.equals("MapMarkerBitmap")) {
			this.in_MapMarkerBitmap = true;
		} 		
	}

	/**
	 * Gets be called on closing tags like: </tag>
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		if (localName.equals("Category")) {
			categories.add(tempCategory);
			this.in_Category = false;			
		} else if (localName.equals("Code")) {
			this.in_Code = false;
		} else if (localName.equals("Name")) {
			this.in_Name = false;
		} else if (localName.equals("MapMarkerBitmap")) {
			this.in_MapMarkerBitmap = false;
		} 	
	}

	public void startDocument() throws SAXException {

	}

	@Override
	public void endDocument() throws SAXException {
		// Nothing to do
	}
}