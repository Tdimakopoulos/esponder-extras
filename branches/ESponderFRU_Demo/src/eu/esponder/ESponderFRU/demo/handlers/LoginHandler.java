package eu.esponder.ESponderFRU.demo.handlers;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

public class LoginHandler extends Handler {
	
	private int BE_LOGIN_UPDATE = 1;
	private int SENSORS_UPDATE = 2;
	private int LOGIN_UPDATE = 3;
	
	private TextView beText;
	private TextView sensorText;
	private TextView loginText;

	public LoginHandler(TextView beText, TextView sensorText, TextView loginText) {
		super();
		this.setBeText(beText);
		this.setSensorText(sensorText);
		this.setLoginText(loginText);
	}

	@Override
	public void handleMessage(Message msg) {
		Bundle bundle;
		bundle = msg.getData();
		String content;
		if(bundle != null) {
			content = bundle.getString("content");
			if(msg.what == BE_LOGIN_UPDATE) {
				beText.setText(content);
				if(content.equalsIgnoreCase("Disconnected"))
					beText.setTextColor(Color.RED);
					
			}
			else if(msg.what == SENSORS_UPDATE) {
				sensorText.setText(content);
				if(content.equalsIgnoreCase("Disconnected"))
					sensorText.setTextColor(Color.RED);
			}
			else if(msg.what == LOGIN_UPDATE) {
				loginText.setText(content);
				if(content.equalsIgnoreCase("Disconnected"))
					loginText.setTextColor(Color.RED);
			}
		}
	}

	public TextView getSensorText() {
		return sensorText;
	}

	public void setSensorText(TextView sensorText) {
		this.sensorText = sensorText;
	}

	public TextView getBeText() {
		return beText;
	}

	public void setBeText(TextView beText) {
		this.beText = beText;
	}

	public TextView getLoginText() {
		return loginText;
	}

	public void setLoginText(TextView loginText) {
		this.loginText = loginText;
	}


}
