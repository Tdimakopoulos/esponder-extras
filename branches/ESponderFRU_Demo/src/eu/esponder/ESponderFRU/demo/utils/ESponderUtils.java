package eu.esponder.ESponderFRU.demo.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import org.apache.http.conn.util.InetAddressUtils;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import eu.esponder.ESponderFRU.demo.R;
import eu.esponder.ESponderFRU.demo.activities.Actions;
import eu.esponder.ESponderFRU.demo.activities.ActionsIC;
import eu.esponder.ESponderFRU.demo.activities.ActorDetails;
import eu.esponder.ESponderFRU.demo.activities.Actors;
import eu.esponder.ESponderFRU.demo.activities.Alarms;
import eu.esponder.ESponderFRU.demo.activities.Files;
import eu.esponder.ESponderFRU.demo.activities.MainActivity;
import eu.esponder.ESponderFRU.demo.activities.MainICActivity;
import eu.esponder.ESponderFRU.demo.activities.Messages;
import eu.esponder.ESponderFRU.demo.application.GlobalVar;
import eu.esponder.ESponderFRU.demo.exceptions.NotSupportedException;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionStageEnumDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;

public class ESponderUtils {

	//This is the onClick Listener for the application menu of an frc
	public static class FRCMenuClickListener implements OnItemClickListener {

		private long frID;
		private Context context;

		public FRCMenuClickListener(Context context) {
			super();
			this.context = context;
			SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			frID = prefs.getLong(GlobalVar.spESponderUserID, -1);
		}

		public long getFrID() {
			return frID;
		}

		public void setFrID(long frID) {
			this.frID = frID;
		}

		public Context getContext() {
			return context;
		}

		public void setContext(Context context) {
			this.context = context;
		}



		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Intent intent;
			switch(position) {
			case 0:
				intent = new Intent(context, MainActivity.class);
				context.startActivity(intent);
				break;
			case 1:
				intent = new Intent(context, ActorDetails.class);
				intent.putExtra("frID", frID);
				context.startActivity(intent);
				break;
			case 2:
				intent = new Intent(context, Actors.class);
				context.startActivity(intent);
				break;
			case 3:
				intent = new Intent(context, Actions.class);
				intent.putExtra("frID", frID);
				context.startActivity(intent);
				break;
			case 4:
				intent = new Intent(context, Alarms.class);
				intent.putExtra("frID", frID);
				context.startActivity(intent);
				break;
			case 5:
				intent = new Intent(context, Files.class);
				context.startActivity(intent);
				break;
			case 6:
				intent = new Intent(context, Messages.class);
				context.startActivity(intent);
				break;
			case 7:
				intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
				break;
			}
		}

	}


	public static class ICMenuClickListener implements OnItemClickListener {

		private long icID;
		private Context context;

		public ICMenuClickListener(Context context) {
			super();
			this.context = context;
			SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			icID = prefs.getLong(GlobalVar.spESponderUserID, -1);
		}

		public long getICID() {
			return icID;
		}

		public void setICID(long icID) {
			this.icID = icID;
		}

		public Context getContext() {
			return context;
		}

		public void setContext(Context context) {
			this.context = context;
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Intent intent;
			switch(position) {
			case 0:
				intent = new Intent(context, MainICActivity.class);
				context.startActivity(intent);
				break;
			case 1:
				intent = new Intent(context, ActionsIC.class);
				intent.putExtra("frID", icID);
				context.startActivity(intent);
				break;
			case 2:
				intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
				break;
			}
		}

	}

	public static long[] vibrationPattern = {
		0,  // Start immediately
		200, 200, 200, 200, 200,    // s
		500,
		500, 200, 500, 200, 500, // o
		500,
		200, 200, 200, 200, 200,    // s
		1000
	};

	public static String processUriMimeType(String filename) throws NotSupportedException {

		if(filename.endsWith("png"))
			return "image/png";
		else if(filename.endsWith("jpg") || filename.endsWith("jpeg"))
			return "image/jpeg";
		else if(filename.endsWith("pdf"))
			return "application/pdf";
		else if(filename.endsWith("txt"))
			return "text/plain";
		else
			throw new NotSupportedException();
	}

	public static String processUriMimeType(Context context, Uri uri) throws NotSupportedException {

		ContentResolver cR = context.getContentResolver();
		String mimeType = cR.getType(uri);

		if(mimeType != null)
			return mimeType;
		else
			throw new NotSupportedException();
	}




	public static int processIconForUriMimeType(String filename) {

		if(filename != null) {
			if(filename.endsWith("png"))
				return R.drawable.jpgicon;
			else if(filename.endsWith("jpg") || filename.endsWith("jpeg"))
				return R.drawable.jpgicon;
			else if(filename.endsWith("pdf"))
				return R.drawable.pdficon;
			else if(filename.endsWith("txt"))
				return R.drawable.txticon;

			return -1;
		}
		else
			return -1;
	}


	public static long getAvailableRAM(Context context) {

		MemoryInfo mi = new MemoryInfo();
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		activityManager.getMemoryInfo(mi);
		long availableMegs = mi.availMem / 1048576L;
		return availableMegs;

	}


	public static String getIPAddress(Context context, boolean useIPv4) {
		try {
			List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
				for (InetAddress addr : addrs) {
					if (!addr.isLoopbackAddress()) {
						String sAddr = addr.getHostAddress().toUpperCase();
						boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr); 
						if (useIPv4) {
							if (isIPv4) 
								return sAddr;
						} else {
							if (!isIPv4) {
								int delim = sAddr.indexOf('%'); // drop ip6 port suffix
								return delim<0 ? sAddr : sAddr.substring(0, delim);
							}
						}
					}
				}
			}
		} catch (Exception ex) { 
			Log.e("", ex.getStackTrace().toString());
		}
		return "";
	}


	public static String gettextForOperationType(int ordinal) {
		if(String.valueOf(ActionOperationEnumDTO.FIX.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "FIX";
		else if(String.valueOf(ActionOperationEnumDTO.MOVE.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "MOVE";
		else if(String.valueOf(ActionOperationEnumDTO.TRANSPORT.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "TRANSPORT";
		else
			return "";
	}


	public static String getTextForOperationStage(int ordinal) {

		if(String.valueOf(ActionStageEnumDTO.Approved.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "Approved";
		else if(String.valueOf(ActionStageEnumDTO.Done.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "Done";
		else if(String.valueOf(ActionStageEnumDTO.Initial.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "Initial";
		else if(String.valueOf(ActionStageEnumDTO.InProgress.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "In progress";
		else if(String.valueOf(ActionStageEnumDTO.PartiallyDone.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "Cancelled";
		else
			return "";
	}


	public static ActionStageEnumDTO getActionStageEnumForOperationStage(int ordinal) {
		if(String.valueOf(ActionStageEnumDTO.Approved.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return ActionStageEnumDTO.Approved;
		else if(String.valueOf(ActionStageEnumDTO.Done.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return ActionStageEnumDTO.Done;
		else if(String.valueOf(ActionStageEnumDTO.Initial.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return ActionStageEnumDTO.Initial;
		else if(String.valueOf(ActionStageEnumDTO.InProgress.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return ActionStageEnumDTO.InProgress;
		else
			return ActionStageEnumDTO.PartiallyDone;
	}

	public static SeverityLevelDTO getSeverityLevelEnum(int ordinal) {
		if(String.valueOf(SeverityLevelDTO.FATAL.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return SeverityLevelDTO.FATAL;
		else if(String.valueOf(SeverityLevelDTO.MEDIUM.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return SeverityLevelDTO.MEDIUM;
		else if(String.valueOf(SeverityLevelDTO.MINIMAL.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return SeverityLevelDTO.MINIMAL;
		else if(String.valueOf(SeverityLevelDTO.SERIOUS.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return SeverityLevelDTO.SERIOUS;
		else if(String.valueOf(SeverityLevelDTO.SEVERE.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return SeverityLevelDTO.SEVERE;
		else 
			return SeverityLevelDTO.UNDEFINED;
	}

	public static ActionOperationEnumDTO getActionOperationEnum(int ordinal) {
		if(String.valueOf(ActionOperationEnumDTO.FIX.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return ActionOperationEnumDTO.FIX;
		else if(String.valueOf(ActionOperationEnumDTO.MOVE.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return ActionOperationEnumDTO.MOVE;
		else
			return ActionOperationEnumDTO.TRANSPORT;


	}

	public static String getSeverityLevelDescription(int ordinal) {
		if(String.valueOf(SeverityLevelDTO.FATAL.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "FATAL";
		else if(String.valueOf(SeverityLevelDTO.MEDIUM.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "MEDIUM";
		else if(String.valueOf(SeverityLevelDTO.MINIMAL.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "MINIMAL";
		else if(String.valueOf(SeverityLevelDTO.SERIOUS.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "SERIOUS";
		else if(String.valueOf(SeverityLevelDTO.SEVERE.ordinal()).equalsIgnoreCase(String.valueOf(ordinal)))
			return "SEVERE";
		else 
			return "UNDEFINED";
	}

}

