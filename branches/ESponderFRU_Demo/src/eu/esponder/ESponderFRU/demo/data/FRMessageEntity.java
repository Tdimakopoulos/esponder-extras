package eu.esponder.ESponderFRU.demo.data;

public class FRMessageEntity {
	
	private long id;
	private String source;
	private String messageBody;
	private long timestamp;
	int acked;
	
	public FRMessageEntity(long id, String source, String messageBody,
			long timestamp, int acked) {
		this.id = id;
		this.source = source;
		this.messageBody = messageBody;
		this.timestamp = timestamp;
		this.setAcked(acked);
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getMessageBody() {
		return messageBody;
	}
	
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public int  getAcked() {
		return acked;
	}

	public void setAcked(int acked) {
		this.acked = acked;
	}

}
