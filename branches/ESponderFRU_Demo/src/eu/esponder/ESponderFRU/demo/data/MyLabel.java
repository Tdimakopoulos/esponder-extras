package eu.esponder.ESponderFRU.demo.data;

import com.nutiteq.components.TextureInfo;
import com.nutiteq.style.LabelStyle;
import com.nutiteq.ui.DefaultLabel;

public class MyLabel extends DefaultLabel {

	private Long frID;
	private boolean fr;

	public MyLabel(String title, String description, Long frID, Boolean isFR) {
		super(title, description);
		this.setFrID(frID);
		this.setFr(isFR);
	}

	public MyLabel(String title, String description, LabelStyle style, Long frID, Boolean isFR) {
		super(title, description, style);
		this.setFrID(frID);
		this.setFr(isFR);
	}

	@Override
	public TextureInfo drawMarkerLabel() {
		return super.drawMarkerLabel();
	}

	public Long getFrID() {
		return frID;
	}

	public void setFrID(Long frID) {
		this.frID = frID;
	}

	public boolean isFr() {
		return fr;
	}

	public void setFr(boolean fr) {
		this.fr = fr;
	}

}
