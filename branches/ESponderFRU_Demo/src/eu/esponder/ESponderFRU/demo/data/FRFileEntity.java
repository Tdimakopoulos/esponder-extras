package eu.esponder.ESponderFRU.demo.data;

public class FRFileEntity {
	
	private Long id;
	private String description;
	private int status;
	private String relativePath;
	private String URL;
	private String fileID;
	private String source;
	
	public FRFileEntity(Long id, String description, int status,
			String relativePath, String URL, String fileID, String source) {
		super();
		this.id = id;
		this.description = description;
		this.status = status;
		this.relativePath = relativePath;
		this.URL = URL;
		this.fileID = fileID;
		this.source = source;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getRelativePath() {
		return relativePath;
	}
	
	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getFileID() {
		return fileID;
	}

	public void setFileID(String fileID) {
		this.fileID = fileID;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
