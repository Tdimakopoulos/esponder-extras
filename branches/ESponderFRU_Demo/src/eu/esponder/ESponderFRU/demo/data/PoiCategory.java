package eu.esponder.ESponderFRU.demo.data;

public class PoiCategory {
	
	private int CategoryCode;
	private String MarkerBitmap;
	private String Name;
	
	public PoiCategory(){
		CategoryCode = -1;
		Name = new String("NoName");
		MarkerBitmap = new String("Default");
	}
	
	public PoiCategory(int categoryCode, String name, String markerBitmap){
		CategoryCode = categoryCode;
		Name = new String(name);
		MarkerBitmap = new String(markerBitmap);
	}
	
	public int getCategoryCode(){
		return CategoryCode;
	}
	
	public void setCategoryCode(int code){
		CategoryCode = code;
	}
	
	public String getMarkerBitmap(){
		return MarkerBitmap;
	}
	
	public void setMarkerBitmap(String markerBitmap){
		MarkerBitmap = markerBitmap;
	}
	
	public String getName(){
		return Name;
	}
	
	public void setName(String name){
		Name = name;
	}
}
