package eu.esponder.ESponderFRU.demo.enums;

public enum SensorUnitEnum {

	ActivitySensor("bpm"),				/*ActivitySensor*/

	BodyTemperatureSensor("�C"),		/*BodyTemperatureSensor*/

	BreathRateSensor("bpm"),				/*BreathRateSensor*/

	HeartBeatRateSensor("bpm"),			/*HeartBeatRateSensor*/

	EnvTemperatureSensor("�C"),			/*EnvTemperatureSensor*/

	GasSensor("ppm"),					/*GasSensor*/

	LPSSensor("�"),						/*LPSSensor*/

	LocationSensor("�")					/*LocationSensor*/, 
	
	MethaneSensor("ppm"), 
	
	CarbonMonoxideSensor("ppm"), 
	
	CarbonDioxideSensor("ppm"), 
	
	OxygenSensor("ppm"), 
	
	HydrogenSulfideSensor("ppm")
	
	;
	
	
	private final String unit;
	
	SensorUnitEnum(final String text) {
		this.unit = text;
	}

	public String getUnit() {
		return unit;
	}
	
	
}
