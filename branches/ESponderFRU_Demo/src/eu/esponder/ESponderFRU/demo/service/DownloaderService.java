package eu.esponder.ESponderFRU.demo.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import org.apache.http.util.ByteArrayBuffer;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.telephony.TelephonyManager;
import android.util.Log;
import eu.esponder.ESponderFRU.demo.R;
import eu.esponder.ESponderFRU.demo.activities.MainActivity;
import eu.esponder.ESponderFRU.demo.application.GlobalVar;
import eu.esponder.ESponderFRU.demo.exceptions.NotSupportedException;
import eu.esponder.ESponderFRU.demo.utils.ESponderUtils;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.event.mobile.MobileFileResponseEvent;

public class DownloaderService extends IntentService {

	private enum DownloadResultenum {

		DOWNLOAD_FAILED,

		DOWNLOAD_SUCCESS,

		IO_EXCEPTION,

		FILENF_EXCEPTION,

		BADURL_EXCEPTION,

		FILENOTSUPPORTED_EXCEPTION

	}

	String urlPath;
	DownloadResultenum result = DownloadResultenum.DOWNLOAD_FAILED;
	Long fileID = (long) -1;
	String beFileID;

	public DownloaderService() {
		super("DownloadService");
	}

	// Will be called asynchronously be Android
	@Override
	protected void onHandleIntent(Intent intent) {

		Uri data = intent.getData();
		urlPath = intent.getStringExtra("urlpath");
		if(intent.hasExtra("fileID"))
			fileID = intent.getLongExtra("fileID", -1);
		if(intent.hasExtra("beFileID")) {
			beFileID = intent.getStringExtra("beFileID");	
			Log.e("beFileID", beFileID);
		}

		InputStream stream = null;
		FileOutputStream fos = null;
		File output = null;
		String mimeType = null;

		String fileName = data.getLastPathSegment();
		try {
			Log.e("FILENAME",fileName);
			mimeType = ESponderUtils.processUriMimeType(fileName);
			Log.e("MIME TYPE",mimeType);
			output = new File(Environment.getExternalStorageDirectory(), fileName);
			if (output.exists()) {
				output.delete();
			}

			URL url = new URL(urlPath);
			stream = url.openConnection().getInputStream();
			BufferedInputStream bis = new BufferedInputStream(stream);
			ByteArrayBuffer baf = new ByteArrayBuffer(50);
            int current = 0;
            while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
            }
			
			fos = new FileOutputStream(output.getPath());
			fos.write(baf.toByteArray());
            
		} catch (MalformedURLException e) {
			result = DownloadResultenum.BADURL_EXCEPTION;
			e.printStackTrace();
		} catch (NotSupportedException e) {
			result = DownloadResultenum.FILENOTSUPPORTED_EXCEPTION;
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			result = DownloadResultenum.FILENF_EXCEPTION;
			e.printStackTrace();
		} catch (IOException e) {
			result = DownloadResultenum.IO_EXCEPTION;
			e.printStackTrace();
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if(result.compareTo(DownloadResultenum.DOWNLOAD_FAILED) ==0) {
			// Sucessfully finished
			result = DownloadResultenum.DOWNLOAD_SUCCESS;	
		}


		if(result.compareTo(DownloadResultenum.DOWNLOAD_SUCCESS) == 0) {
			//File has downloaded, we will update database status and relative path of the file
			if(fileID != -1) {
			GlobalVar.db.updateFilePath(fileID, fileName);
			GlobalVar.db.updateFileStatus(fileID, 1);				
			}
			
			
			createBackendFileReptionNotificationEvent(true, beFileID, -1);
			
			Intent intent1 = new Intent(Intent.ACTION_VIEW);
			intent1.setDataAndType(Uri.fromFile(output), mimeType);
			Log.e("URI APO SERVICE", Uri.fromFile(output).toString());
			TaskStackBuilder stackBuilder = TaskStackBuilder
					.create(DownloaderService.this);
			stackBuilder.addParentStack(MainActivity.class);
			stackBuilder.addNextIntent(intent1);
			final PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					DownloaderService.this)
			.setContentTitle("File has been downloaded, press here to view file.")
			.setContentText(intent.getStringExtra("description")).setSmallIcon(R.drawable.filedownload)
			.setContentIntent(resultPendingIntent)
			.addAction(R.drawable.ic_launcher, "Call", resultPendingIntent);
			mBuilder.setContentIntent(resultPendingIntent);
			mBuilder.setAutoCancel(true);
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(0, mBuilder.build());
		}
		else if(result.compareTo(DownloadResultenum.BADURL_EXCEPTION) == 0) {
			
			
			createBackendFileReptionNotificationEvent(false, beFileID, 0);

			Intent pastIntent = new Intent(this, MainActivity.class);
			//			pastIntent.setAction(Intent.ACTION_MAIN);
			//			pastIntent.addCategory(Intent.CATEGORY_LAUNCHER);
			pastIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
					pastIntent, 0);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					DownloaderService.this)
			.setContentTitle("New file notification has arrived")
			.setContentText("The provided url was not correct, file download has been aborted").setSmallIcon(R.drawable.warning);
			mBuilder.setAutoCancel(true);
			mBuilder.setContentIntent(pendingIntent);
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(0, mBuilder.build());

		}
		else if(result.compareTo(DownloadResultenum.FILENF_EXCEPTION) == 0) {
			
			createBackendFileReptionNotificationEvent(false, beFileID, 2);

			Intent pastIntent = new Intent(this, MainActivity.class);
			pastIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
					pastIntent, 0);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					DownloaderService.this)
			.setContentTitle("New file notification has arrived")
			.setContentText("A file transmission was picked up but a problem has occured").setSmallIcon(R.drawable.warning);
			mBuilder.setAutoCancel(true);
			mBuilder.setContentIntent(pendingIntent);
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(0, mBuilder.build());
			
		}
		else if(result.compareTo(DownloadResultenum.IO_EXCEPTION) == 0) {
			
			createBackendFileReptionNotificationEvent(false, beFileID, 3);

			Intent pastIntent = new Intent(this, MainActivity.class);
			pastIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
					pastIntent, 0);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					DownloaderService.this)
			.setContentTitle("New file notification has arrived")
			.setContentText("Could not access the file remote location, something has gone wrong").setSmallIcon(R.drawable.warning);
			mBuilder.setContentIntent(pendingIntent);
			mBuilder.setAutoCancel(true);
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(0, mBuilder.build());
		}
		else if(result.compareTo(DownloadResultenum.FILENOTSUPPORTED_EXCEPTION) == 0) {
			
			createBackendFileReptionNotificationEvent(false, beFileID, 1);

			Intent pastIntent = new Intent(this, MainActivity.class);
			pastIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, pastIntent, 0);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(DownloaderService.this)
			.setContentTitle("New file notification has arrived")
			.setContentText("The received file type is not supported").setSmallIcon(R.drawable.warning);
			mBuilder.setContentIntent(pendingIntent);
			mBuilder.setAutoCancel(true);
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(0, mBuilder.build());
		}
		
		
		//Send intent to update the list on the files screen, no matter what happened to the newly received file
		Intent notifyFileDownloaded = new Intent(GlobalVar.FILE_POPUP);
		sendBroadcast(notifyFileDownloaded);
		
	}
	
	
	private void createBackendFileReptionNotificationEvent(Boolean success, String beFIleID, int errorID) {
		
		MobileFileResponseEvent rEvent = new MobileFileResponseEvent();
		rEvent.setEventTimestamp(new Date());
		rEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
		rEvent.setImei(((TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
		rEvent.setFileid(beFIleID);
		rEvent.setReceived(success);
		if(!success)
			rEvent.setIerrortype(errorID);
		
		
		ObjectMapper mapper = new ObjectMapper();
		String eventJSON = null;
		try {
			eventJSON = mapper.writeValueAsString(rEvent);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(eventJSON != null) {
			Intent intent = new Intent(GlobalVar.FILE_BE_ACK);
			intent.addCategory(GlobalVar.INTENT_CATEGORY);
			intent.putExtra("event", eventJSON);
			getApplicationContext().sendBroadcast(intent);
			
			Log.e("FILE ACK","SENT");
		}
	}


}