package eu.esponder.ESponderFRU.demo.runnables;

import java.io.IOException;
import java.util.Date;
import java.util.TimerTask;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import eu.esponder.ESponderFRU.demo.application.GlobalVar;
import eu.esponder.ESponderFRU.demo.utils.ESponderUtils;
import eu.esponder.event.mobile.MobileStatusEvent;

public class SPInformationTimerTask extends TimerTask {

	private static Context context;
	GetRssi psListener;
	TelephonyManager manager;
	int gsmSignalStrength;
	int batteryLevel = -1;

	public SPInformationTimerTask(Context context) {
		this.setContext(context);
		this.psListener = new GetRssi();
		this.manager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
	}

	@Override
	public void run() {
		
		// We register this receiver in order to receive the battery which is a system broadcast intent 
		BatteryReceiver bReceiver = new BatteryReceiver(); 
		context.getApplicationContext().registerReceiver(bReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
//		Log.e("BATTERY",String.valueOf(batteryLevel));
		MobileStatusEvent sEvent = new MobileStatusEvent();
		sEvent.setImei(((TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
		sEvent.setBattery(String.valueOf(batteryLevel));;
		Log.e("AVAILABLE MEMORY",String.valueOf(ESponderUtils.getAvailableRAM(context)));
		//Reception of GSM signal Strength
		// as soon as the listener is registered, it gives back an instant value for the signal
		manager.listen(psListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
		// As soon as we get the first value back, we unregister the listener from the manager 
		manager.listen(psListener, PhoneStateListener.LISTEN_NONE);
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		sEvent.setWifistatus(wifiManager.isWifiEnabled());
		sEvent.setWifirssi(String.valueOf(wifiManager.getConnectionInfo().getRssi()));
//		Log.e("LINK SPEED",String.valueOf(wifiManager.getConnectionInfo().getLinkSpeed())+" Mbps");
//		Log.e("WIFI RSSI",String.valueOf(wifiManager.getConnectionInfo().getRssi()));
		
		context.getApplicationContext().unregisterReceiver(bReceiver);
		sEvent.setEventTimestamp(new Date());
		ObjectMapper mapper = new ObjectMapper();
		String eventJSON = null;
		try {
			eventJSON = mapper.writeValueAsString(sEvent);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(eventJSON!=null) {
			Intent intent = new Intent(GlobalVar.SP_INFO);
			intent.putExtra("event", eventJSON);
			intent.addCategory(GlobalVar.INTENT_CATEGORY);
			context.sendBroadcast(intent);
		}
		
	}
	
	private class GetRssi extends PhoneStateListener {
		
	    @Override
	    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
	        super.onSignalStrengthsChanged(signalStrength);
	        gsmSignalStrength = signalStrength.getGsmSignalStrength();
	    }
	}
	
	private class BatteryReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equalsIgnoreCase(Intent.ACTION_BATTERY_CHANGED)) {
				int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
				int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
				batteryLevel = (level*100)/scale;
			}
		}
	}
	
	// Getters - Setters
	public static Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

}
