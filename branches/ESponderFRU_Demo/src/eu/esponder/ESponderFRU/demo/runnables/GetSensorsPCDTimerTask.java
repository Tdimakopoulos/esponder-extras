package eu.esponder.ESponderFRU.demo.runnables;

import java.util.TimerTask;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import eu.ESponderFRU.demo.soapservices.ESponderRestClient;
import eu.ESponderFRU.demo.soapservices.ESponderRestClient.RequestMethod;
import eu.esponder.ESponderFRU.demo.application.GlobalVar;

public class GetSensorsPCDTimerTask extends TimerTask {
	
	Context context;
	
	public GetSensorsPCDTimerTask(Context context) {
	this.context = context;
	}

	@Override
	public void run() {
		
		Log.e("GetSensors Task","GetSensor Task is starting");
		SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
		String rasIP = prefs.getString(GlobalVar.spESponderUserRasIP, "");
		if(!rasIP.equalsIgnoreCase("")) {
			String url = "http://"+rasIP+"/avgfrteam";
			ESponderRestClient client = new ESponderRestClient(context, url);
			try {
				client.Execute(RequestMethod.GET);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}