package eu.esponder.ESponderFRU.demo.runnables;

import java.util.TimerTask;

import android.view.View;
import android.widget.ImageView;

public class AlarmAnimStopTask extends TimerTask {
	
	private ImageView image;

	public AlarmAnimStopTask(ImageView image) {
		this.setImage(image);
	}

	@Override
	public void run() {
		image.setAnimation(null);
		image.setVisibility(View.INVISIBLE);
	}

	public ImageView getImage() {
		return image;
	}

	public void setImage(ImageView image) {
		this.image = image;
	}

}
