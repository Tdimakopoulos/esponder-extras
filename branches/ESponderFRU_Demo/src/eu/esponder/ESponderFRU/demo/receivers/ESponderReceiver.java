/**
 * 
 */
package eu.esponder.ESponderFRU.demo.receivers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.util.Log;
import eu.ESponderFRU.demo.soapservices.ESponderRestClient;
import eu.ESponderFRU.demo.soapservices.ESponderRestClient.RequestMethod;
import eu.esponder.ESponderFRU.demo.R;
import eu.esponder.ESponderFRU.demo.application.GlobalVar;
import eu.esponder.ESponderFRU.demo.dialogs.NotificationsDialog;
import eu.esponder.ESponderFRU.demo.enums.SensorTypeEnum;
import eu.esponder.ESponderFRU.demo.gps.ESponderLocationListener;
import eu.esponder.ESponderFRU.demo.service.DownloaderService;
import eu.esponder.ESponderFRU.demo.utils.ESponderUtils;
import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionStageEnumDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorICALTDTO;
import eu.esponder.dto.model.crisis.alternative.FRTeamALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCEocALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.sensor.ActivitySensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BreathRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.CarbonDioxideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.CarbonMonoxideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.EnvironmentTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HeartBeatRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HydrogenSulfideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.MethaneSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.OxygenSensorDTO;
import eu.esponder.dto.model.datafusion.DatafusionResultsDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.datafusion.query.ESponderDFQueryRequestEvent;
import eu.esponder.event.datafusion.query.ESponderDFQueryResponseEvent;
import eu.esponder.event.mobile.MobileFileNotifierEvent;
import eu.esponder.event.mobile.MobileMessageEvent;
import eu.esponder.event.model.crisis.action.NewActionEvent;
import eu.esponder.event.model.crisis.action.UpdateActionEvent;
import eu.esponder.event.model.datafusion.DatafusionResultsGeneratedEvent;
import eu.esponder.event.model.login.LoginEsponderUserEvent;

/**
 * @author dkar
 *
 */
public class ESponderReceiver extends BroadcastReceiver {

	protected boolean mapInitialized = false;
	private ObjectMapper mapper;

	/* (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
	public void onReceive(Context context, Intent intent) {

		// All incoming and outgoing intents have the same category attached
		if(intent.hasCategory(GlobalVar.INTENT_CATEGORY)) {
			// case for login response
			if(intent.getAction().equalsIgnoreCase(GlobalVar.LOGIN_RESPONSE_INTENT_ACTION)) {
				if(!((GlobalVar)context.getApplicationContext()).isLoggedIn()) {
					String jsonObj = intent.getStringExtra("event");
					Log.e("LOGIN", jsonObj);
					processLoginResponse(context, jsonObj);
				}
				else
					Log.e("LOG IN", "USER IS ALREADY LOGGED IN");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.MAP_RESPONSE_HIERARCHY)) {
				// Checks if user has already passsed the login process, as well if he is a FRC or IC
				Log.e("RECEIVER", "MOCK FOR MAP ARRIVED");
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn() && 
						(((GlobalVar)context.getApplicationContext()).getRole()==0 || ((GlobalVar)context.getApplicationContext()).getRole()==2)) {
					String jsonObj = intent.getStringExtra("event");
					createThreadForMapUpdate(context, jsonObj);
				}
				else
					Log.e("CANNOT UPDATE MAP", "USER NOT LOGGED IN");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.BIOMED_SENSOR_MEASUREMENT)) {

				//Checks if the user is logged in, as well as if he is a FRC or FR
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn()&& 
						(((GlobalVar)context.getApplicationContext()).getRole()==0 || ((GlobalVar)context.getApplicationContext()).getRole()==1)) {
					Bundle b = intent.getBundleExtra("SensorMeasurementBundle");
					if(b !=null) {
						String sensorType = b.getString("sensorType");
						Long timeStamp = b.getLong("timeStamp");
						//The location sensor intent contains different extras than the intent of the other sensors
						if(sensorType.equalsIgnoreCase(LocationSensorDTO.class.getCanonicalName())) {

							Log.e("LPS", String.valueOf(b.getDouble("latitude"))+" / "+String.valueOf(b.getDouble("longitude"))+" / "+String.valueOf(b.getDouble("altitude")) );
							processLPSMeasurement(context, b.getDouble("latitude"), b.getDouble("longitude"), b.getDouble("altitude"), timeStamp);

						}
						else {
							//The rest of the sensors
							Double sensorValue = b.getDouble("sensorValue");
							processMeasurement(context, sensorType, sensorValue, timeStamp);
						}
					}
				}
				else
					Log.e("Sensor-MEASUREMENT PROCESS", "NOT WORKING, APPEARS AS NOT LOGGED IN");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INCOMING_ALARM)) {
				//Checks if the user is logged in as well as if he is a FRC or FR
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn()&& 
						(((GlobalVar)context.getApplicationContext()).getRole()==0 || ((GlobalVar)context.getApplicationContext()).getRole()==1)) {
					if(intent.hasExtra("event")) {
						processAlarmRelatedEvents(context, intent.getStringExtra("event"));
					}
					else
						Log.e("RECEIVER","NO ATTACHMENT FOR ALARMS");
				}
				else
					Log.e("INCOMING ALARM", "NOT WORKING, APPEARS AS NOT LOGGED IN");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INCOMING_MESSAGE)) {
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn() && ((GlobalVar)context.getApplicationContext()).getRole()==0) {
					if(intent.hasExtra("event")) {
						processIncomingMessageEvents(context, intent.getStringExtra("event"));
					}
					else
						Log.e("RECEIVER","NO ATTACHMENT FOR NOTIFICATION");
				}
				else
					Log.e("INCOMING MESSAGE", "NOT WORKING, APPEARS AS NOT LOGGED IN");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INCOMING_FILE)) {
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn() && ((GlobalVar)context.getApplicationContext()).getRole()==0) {
					if(intent.hasExtra("event")) {
						Log.e("RECEIVER","FILE NOTIFICATION RECEIVED");
						processIncomingFileEvents(context, intent.getStringExtra("event"));
					}
					else
						Log.e("RECEIVER","NO ATTACHMENT FOR INCOMING FILE");
				}
				else
					Log.e("INCOMING FILE", "NOT WORKING, APPEARS AS NOT LOGGED IN");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.NEW_ACTION_EVENT) ) {
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn() && 
						(((GlobalVar)context.getApplicationContext()).getRole() == 0 || ((GlobalVar)context.getApplicationContext()).getRole() == 2)) {
					if(intent.hasExtra("event")) {
						// FIXME Re-enable the check here after the testing has finished
						if(((GlobalVar)context.getApplicationContext()).isLoggedIn() && 
								(((GlobalVar)context.getApplicationContext()).getRole() == 0 || ((GlobalVar)context.getApplicationContext()).getRole() == 2)) {
							Log.e("NEW ACTION EVENT", "RECEIVED");
							processNewIncomingActionEvent(intent.getStringExtra("event"));
						}
					}
				}
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.APO_CANCELLED) ) {
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn() && 
						(((GlobalVar)context.getApplicationContext()).getRole() == 0 || ((GlobalVar)context.getApplicationContext()).getRole() == 2)) {
					if(intent.hasExtra("event"))
						processIncomingCancelActionEvent(context, intent.getStringExtra("event"));
				}
			}
			else 
				Log.e("***** OTHER *****", intent.getAction());
		}
		else {
			if(intent.getAction().equalsIgnoreCase(android.net.ConnectivityManager.CONNECTIVITY_ACTION)) {
				Log.e("UNKNOWN ACTION",intent.getAction());
				NetworkInfo nInfoparcel = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
				if(nInfoparcel == null)
					Log.e("The parcel is ", "NULL");
				else
					Log.e("The parcel is ", "NOT NULL");
				Intent connectionChanged = new Intent(GlobalVar.INNER_WIFI_CONNECTED);
				Bundle b = new Bundle();
				b.putParcelable("nInfo", nInfoparcel);
				connectionChanged.putExtras(b);
				context.sendBroadcast(connectionChanged);
			}	
		}
	}

	//*******************************************************************************************************************************

	private boolean processIncomingLoginEvent(Context context, LoginEsponderUserEvent loginResponseEvent) {
		// Process the incoming event details and store them in memory

		Log.e("RECEIVER FOR LOGIN", "iROLE = " + String.valueOf(loginResponseEvent.getiRole()));

		if(loginResponseEvent.getEventAttachment() != null) {

			SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			SharedPreferences.Editor spEditor = prefs.edit();
			spEditor.putString(GlobalVar.spESponderUserRasIP, loginResponseEvent.getRasberryip());
			spEditor.putLong(GlobalVar.spESponderUserID, loginResponseEvent.getEventAttachment().getId());
			PersonnelDTO personnel = loginResponseEvent.getEventAttachment().getPersonnel();
			if(personnel != null)
				spEditor.putString(GlobalVar.spESponderFullName, personnel.getLastName()+" "+personnel.getFirstName());
			spEditor.commit();
		}

		// We set the iRole of the application here
		((GlobalVar)context.getApplicationContext()).setRole(loginResponseEvent.getiRole());

		Log.e("CURRENT USER IS ", String.valueOf(((GlobalVar)context.getApplicationContext()).getRole()));

		//	Criteria for FRC or FR has changed
		if(loginResponseEvent.getiRole() == 0) {
			//  FRC
			((GlobalVar)context.getApplicationContext()).setChief(true);
			context.sendBroadcast(sendIntentForSIPLogin(loginResponseEvent));
			return true;
		}
		else if(loginResponseEvent.getiRole() == 1) {
			//	FR
			((GlobalVar)context.getApplicationContext()).setChief(false);
			context.sendBroadcast(sendIntentForSIPLogin(loginResponseEvent));
			return true;
		}
		else if(loginResponseEvent.getiRole() == 2) {
			//	Incident Commander, MEOC Driver
			((GlobalVar)context.getApplicationContext()).setChief(false);
			context.sendBroadcast(sendIntentForSIPLogin(loginResponseEvent));
			return true;
		}
		else if(loginResponseEvent.getiRole() == -1 ) {
			//	ERROR
			return false;
		}
		else
			return false;

	}

	private Intent sendIntentForSIPLogin(LoginEsponderUserEvent loginResponseEvent) {

		//Create an intent for the VoIP application login process
		Intent voipIntent = new Intent(GlobalVar.SIP_LOGIN);
		voipIntent.putExtra("role", loginResponseEvent.getiRole());
		voipIntent.putExtra("sipUser", loginResponseEvent.getsIPusername());
		voipIntent.putExtra("sipPassword", loginResponseEvent.getsIPpassword());
		return voipIntent;
	}

	//*******************************************************************************************************************************

	private void processLoginResponse(final Context context, final String loginInfo) {

		final String jsonObj = loginInfo;

		class processIncomingLoginResponse implements Runnable {

			@Override
			public void run() {

				LoginEsponderUserEvent loginResponseEvent = null;
				mapper = new ObjectMapper();
				mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				try {
					loginResponseEvent = mapper.readValue(jsonObj, LoginEsponderUserEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(loginResponseEvent != null) {

					if(loginResponseEvent.getIMEI().equalsIgnoreCase(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId())) {

						Log.e("LoginResponseEvent Received","FAMILIAR IMEI, PROCESSING");

						// Process the event contents
						if(processIncomingLoginEvent(context, loginResponseEvent)) {
							// Successful Login
							((GlobalVar)context.getApplicationContext()).setLoggedIn(true);
							Log.e("LOGIN EVENT DECODED", "SUCCESSFUL LOGIN");

							//FIXME Here we activate the gps mechanism in order to constantly receive location changes
							activateGPSMeasurements(context);

							Intent innerIntent = new Intent(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_APPROVED);
							context.sendBroadcast(innerIntent);
						}
						else {
							// Unsuccessful Login
							Log.e("LOGIN EVENT DECODED", "LOGIN DENIED");
							Intent innerIntent = new Intent(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_DENIED);
							context.sendBroadcast(innerIntent);
						}
					}
					else
						Log.e("LoginResponseEvent Received","UNKNOWN IMEI, SKIPPING");
				}
				else {
					Intent innerIntent = new Intent(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_ERROR);
					context.sendBroadcast(innerIntent);
				}
			}
		}

		Thread thread = new Thread(new processIncomingLoginResponse());
		thread.start();

	}


	//*******************************************************************************************************************************

	private void processMeasurement(final Context context, final String sensorType, final Double sensorValue, final Long timeStamp) {

		class processIncomingMeasurement implements Runnable {

			@Override
			public void run() {

				if(sensorType.equalsIgnoreCase(HeartBeatRateSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.HeartBeatRateSensor.getType(), sensorValue, timeStamp);
					Log.e("Sensor-HBRate", String.valueOf(sensorValue));
				}
				else if(sensorType.equalsIgnoreCase(ActivitySensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.ActivitySensor.getType(), sensorValue, timeStamp);
					Log.e("Sensor-Activity", String.valueOf(sensorValue));
				}
				else if(sensorType.equalsIgnoreCase(BreathRateSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.BreathRateSensor.getType(), sensorValue, timeStamp);
					Log.e("Sensor-BRRate", String.valueOf(sensorValue));
				}
				else if(sensorType.equalsIgnoreCase(BodyTemperatureSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.BodyTemperatureSensor.getType(), sensorValue, timeStamp);
					Log.e("Sensor-BodyTempSensor", String.valueOf(sensorValue));
				}
				else if(sensorType.equalsIgnoreCase(MethaneSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.MethaneSensor.getType(), sensorValue, timeStamp);
					Log.e("Sensor-MethaneSensor ", String.valueOf(sensorValue));
				}
				else if(sensorType.equalsIgnoreCase(CarbonMonoxideSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.CarbonMonoxideSensor.getType(), sensorValue, timeStamp);
					Log.e("Sensor-CarbonMonoxideSensor ", String.valueOf(sensorValue));
				}
				else if(sensorType.equalsIgnoreCase(CarbonDioxideSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.CarbonDioxideSensor.getType(), sensorValue, timeStamp);
					Log.e("Sensor-CarbonDioxideSensor ", String.valueOf(sensorValue));
				}
				else if(sensorType.equalsIgnoreCase(OxygenSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.OxygenSensor.getType(), sensorValue, timeStamp);
					Log.e("Sensor-OxygenSensor ", String.valueOf(sensorValue));
				}
				else if(sensorType.equalsIgnoreCase(HydrogenSulfideSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.HydrogenSulfideSensor.getType(), sensorValue, timeStamp);
					Log.e("Sensor-HydrogenSulfideSensor ", String.valueOf(sensorValue));
				}
				else if(sensorType.equalsIgnoreCase(EnvironmentTemperatureSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.EnvTemperatureSensor.getType(), sensorValue, timeStamp);
					Log.e("Sensor-EnvTempSensor ", String.valueOf(sensorValue));
				}
				else {
					Log.e("UNKNOWN INCOMING SENSOR", sensorType);
				}

			}
		}
		Thread thread = new Thread(new processIncomingMeasurement());
		thread.start();
	}


	//*******************************************************************************************************************************

	private void processLPSMeasurement(final Context context, final Double latitude,
			final Double longitude, final Double altitude, final Long timeStamp) {

		SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
		final Long currentUserID = prefs.getLong(GlobalVar.spESponderUserID, -1);

		class LPSSensorTransmitter implements Runnable {

			@Override
			public void run() {

				if(currentUserID != -1) {

					ESponderUserDTO user = new ESponderUserDTO();
					user.setId(Long.valueOf(0));

					ESponderDFQueryRequestEvent pEvent = new ESponderDFQueryRequestEvent();
					pEvent.setIMEI(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
					pEvent.setQueryID(Long.valueOf(2));	// Setting id = 2 is meant for the transmission of the location
					pEvent.setRequestID(Long.valueOf(20));
					pEvent.setJournalMessage("JM");


					QueryParamsDTO pParams= QueryParamsDTO.with("userid", String.valueOf(currentUserID));
					pParams.and("alt", String.valueOf(altitude));
					pParams.and("long", String.valueOf(longitude));
					pParams.and("lat", String.valueOf(latitude));
					Long date = new Date().getTime();
					pParams.and("datefrom", String.valueOf(timeStamp));
					pParams.and("dateto", String.valueOf(timeStamp));
					pEvent.setParams(pParams);
					pEvent.setEventAttachment(user);
					pEvent.setJournalMessage("Test Event");
					pEvent.setEventTimestamp(new Date());
					pEvent.setEventSeverity(SeverityLevelDTO.UNDEFINED);
					pEvent.setEventSource(user);

					ObjectMapper mapper = new ObjectMapper();
					String eventJson = null;
					try {
						eventJson = mapper.writeValueAsString(pEvent);
					} catch (JsonGenerationException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					if(eventJson != null) {
						Intent intent = new Intent(GlobalVar.MAP_REQUEST_HIERARCHY);
						intent.addCategory(GlobalVar.INTENT_CATEGORY);
						intent.putExtra("event", eventJson);
						context.sendBroadcast(intent);
					}
				}


			}			
		}

		class processIncLPSMeasurement implements Runnable {

			@Override
			public void run() {


				GlobalVar.db.updateFRLocation(currentUserID, timeStamp, timeStamp,
						String.valueOf(latitude), String.valueOf(longitude), String.valueOf(altitude));
				Intent intent = new Intent(GlobalVar.INNER_MAP_FR_UPDATE);
				context.sendBroadcast(intent);
			}
		}

		Thread processor = new Thread(new processIncLPSMeasurement());
		processor.start();

		Thread transmitter = new Thread(new LPSSensorTransmitter());
		transmitter.start();
	}

	//*******************************************************************************************************************************

	private void SensorDataSender(final Context context, int st, Double sv, final Long timestamp) {

		final int sensorType = st;
		final Double sensorValue = sv;

		class SensorSender implements Runnable {

			@Override
			public void run() {
				SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
				Long currentUserID = prefs.getLong(GlobalVar.spESponderUserID, -1);
				String rasIP = prefs.getString(GlobalVar.spESponderUserRasIP, "");
				if(currentUserID != -1) {
					ESponderRestClient client = new ESponderRestClient(context, "http://"+rasIP+"/addfr/"
							+ currentUserID +"/" + String.valueOf(sensorType)+ "/" + String.valueOf(sensorValue)
							+ "/" + String.valueOf(timestamp));
					try {
						client.Execute(RequestMethod.POST);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		}

		Thread sensorSender = new Thread(new SensorSender());
		sensorSender.start();
	}

	//*******************************************************************************************************************************

	private void createThreadForMapUpdate(final Context context, final String jsonObj) {

		class MapUpdateThread implements Runnable {

			@Override
			public void run() {

				ESponderDFQueryResponseEvent dfQueryResponseEvent = null;
				mapper = new ObjectMapper();
				mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				try {
					dfQueryResponseEvent = mapper.readValue(jsonObj, ESponderDFQueryResponseEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(dfQueryResponseEvent != null) {

					if(dfQueryResponseEvent.getIMEI().equalsIgnoreCase(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId())) {

						Log.e("MOCK LOCATION","PASSED IMEI");
						if(processIncomingMapData(context, dfQueryResponseEvent)) {
							((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(false);
							if(mapInitialized) {
								Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_UPDATE);
								context.sendBroadcast(hierarchyIntent);
							}
							else {
								mapInitialized = true;
								Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY);
								context.sendBroadcast(hierarchyIntent);
							}
						}
						else {
							Log.e("MOCK LOCATION","ERROR WHILE PROCESSING DATA");
							((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(false);
							Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR);
							context.sendBroadcast(hierarchyIntent);
						}
					}
					else
						Log.e("MapUpdateEvent Received","UNKNOWN IMEI, SKIPPING");
				}
				else {
					Log.e("MOCK LOCATION","COULD NOT BE REPRODUCED FROM JSON");
					((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(false);
					Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR);
					context.sendBroadcast(hierarchyIntent);
				}
			}
		}

		if(!((GlobalVar)context.getApplicationContext()).getMapDataProcessActive()) {

			((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(true);
			Thread thread = new Thread(new MapUpdateThread());
			thread.start();
		}
	}


	//*******************************************************************************************************************************

	private boolean processIncomingMapData(Context context, ESponderDFQueryResponseEvent dfQueryResponseEvent) {

		if(dfQueryResponseEvent.getEventAttachment() != null) {
			//FIXME Decide on the storage of the CrisisContext here
			CrisisContextDTO cc = dfQueryResponseEvent.getEventAttachment();

			if(cc!=null) {
				String ccLocation = cc.getCrisisLocation().getCentre().getLatitude().toString()+"a"+
						cc.getCrisisLocation().getCentre().getLongitude().toString();


				SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
				SharedPreferences.Editor spEditor = prefs.edit();
				spEditor.putString(GlobalVar.spCrisisContextVar, ccLocation);
				spEditor.commit();

			}

		}

		if(dfQueryResponseEvent.getMapPositions() != null) {
			Log.e("MAP POSITIONS","RETURNED NULL, PROBLEM!!!!!!!");
			OrganizeHierarchy(context, dfQueryResponseEvent.getMapPositions());
			return true;
		}
		else
			return false;
	}

	//*******************************************************************************************************************************

	private void OrganizeHierarchy(Context context, OCEocALTDTO hierarchy) {

		boolean found = false;
		Log.e("MAP UPDATED","TRUE");

		for(OCMeocALTDTO meoc : hierarchy.getSubordinateMEOCs()) {
			GlobalVar.db.insertOrUpdateMeocAndLocation(meoc);
			ActorICALTDTO ic = meoc.getIncidentCommander();
			GlobalVar.db.insertOrUpdateIncidentCommander(ic);
			if(meoc.getIncidentCommander().getFrTeams() != null) {
				if(meoc.getIncidentCommander().getFrTeams().size() > 0) {
					for(FRTeamALTDTO team :meoc.getIncidentCommander().getFrTeams()) {
						ActorFRCALTDTO chief = team.getFrchief();
						if(chief != null) {
							GlobalVar.db.insertOrUpdateFRChiefAndLocation(chief);
							if(chief.getSubordinates() != null) {
								if(chief.getSubordinates().size() > 0 ) {
									for(ActorFRALTDTO fr : chief.getSubordinates()) {
										if(!found)
											GlobalVar.db.insertOrUpdateFirstResponderAndLocation(fr);
									}
								}
							}
						}
					}
				}
			}
		}

	}

	//*******************************************************************************************************************************

	private void processNewIncomingActionEvent(final String eventJSON) {

		class EventProcessor implements Runnable {

			@Override
			public void run() {

				ObjectMapper mapper = new ObjectMapper();
				mapper.enableDefaultTyping();
				NewActionEvent event = null;

				try {
					event = mapper.readValue(eventJSON, NewActionEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {

					List<ActionPartDTO> actionParts;
					List<ActionObjectiveDTO> actionObjectives;
					List<ActionPartObjectiveDTO> actionPartObjectives;


					actionParts = (List<ActionPartDTO>) event.getActionParts().getResultList();
					for(ActionPartDTO ap : actionParts)
						GlobalVar.db.insertActionPart(ap.getId(), ap.getActionId(), 
								ap.getActor(), ap.getSeverityLevel().ordinal(),
								ap.getActionOperation().ordinal(), ap.getTitle(),
								new Date().getTime(), ap.getActionStage().ordinal());

					actionObjectives = (List<ActionObjectiveDTO>) event.getActionObjectives().getResultList();
					for(ActionObjectiveDTO ao : actionObjectives)
						GlobalVar.db.insertActionObjective(ao.getId(), ao.getAction().getId(),
								ao.getTitle(), ao.getPeriod().getDateFrom(), ao.getPeriod().getDateTo(),
								Double.valueOf(((SphereDTO)ao.getLocationArea()).getCentre().getLatitude().toString()),
								Double.valueOf(((SphereDTO)ao.getLocationArea()).getCentre().getLongitude().toString()),
								Double.valueOf(((SphereDTO)ao.getLocationArea()).getCentre().getAltitude().toString()), ao.getActionStage().ordinal());

					actionPartObjectives = (List<ActionPartObjectiveDTO>) event.getActionPartObjectives().getResultList();
					for(ActionPartObjectiveDTO apo :actionPartObjectives)
						GlobalVar.db.insertActionPartObjective(apo.getId(), apo.getActionPartID(),
								apo.getTitle(), apo.getPeriod().getDateFrom(), apo.getPeriod().getDateTo(),
								Double.valueOf(((SphereDTO)apo.getLocationArea()).getCentre().getLatitude().toString()),
								Double.valueOf(((SphereDTO)apo.getLocationArea()).getCentre().getLongitude().toString()),
								Double.valueOf(((SphereDTO)apo.getLocationArea()).getCentre().getAltitude().toString()), 0);



					Log.e("RECEIVER", "ALL APOS STORED");
				}
				else
					Log.e("RECEIVER", "error while reconstructing the action event");


			}

		}

		Thread thread = new Thread(new EventProcessor());
		thread.start();


	}


	//*******************************************************************************************************************************

	private void processAlarmRelatedEvents(final Context context, final String eventJSON) {

		class AlarmProcessor implements Runnable {

			@Override
			public void run() {
				ObjectMapper mapper = new ObjectMapper();
				DatafusionResultsGeneratedEvent event = null;
				try {
					event = mapper.readValue(eventJSON, DatafusionResultsGeneratedEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {

					DatafusionResultsDTO results = event.getEventAttachment();
					GlobalVar.db.insertFRAlarm(results.getId(), Long.valueOf(results.getResultsText2()), event.getEventSeverity().ordinal(), results.getResultsText1(), event.getEventTimestamp().getTime(), 0);

					// Vibrate
					Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
					v.vibrate(ESponderUtils.vibrationPattern, -1);

					new SoundTask(R.raw.alarm, context).execute(new Integer[]{R.raw.alarm});

					Intent intent = new Intent(GlobalVar.INNER_ALARM_RECEIVED);
					intent.putExtra("frID", results.getId());
					context.sendBroadcast(intent);
				}
				else
					Log.e("ALARM RECEIVED","UNABLE TO PROCESS");
			}	
		}

		Thread thread = new Thread(new AlarmProcessor());
		thread.run();
	}


	//*******************************************************************************************************************************

	private void processIncomingMessageEvents(final Context context, final String eventJSON) {

		class MessageProcessor implements Runnable {

			@Override
			public void run() {
				ObjectMapper mapper = new ObjectMapper();
				MobileMessageEvent event = null;
				try {
					event = mapper.readValue(eventJSON, MobileMessageEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {

					if(event.getMobileimei().equalsIgnoreCase(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId())) {
						String origin = null;
						if(event.getSourceEoc()!= null || event.getSourceMEoc() != null) {
							if(event.getSourceEoc()!= null)
								origin = event.getSourceEoc();
							else if(event.getSourceMEoc()!= null)
								origin = event.getSourceMEoc();

							GlobalVar.db.insertFRMessage(origin, event.getSzMessage(), event.getEventTimestamp().getTime(), 0);

							Intent intent = new Intent(context, NotificationsDialog.class);
							intent.setAction(GlobalVar.NOTIFICATION_POPUP);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							intent.putExtra("origin", origin);
							intent.putExtra("message", event.getSzMessage());
							context.startActivity(intent);
							Log.e("NOTIFICATION RECEIVED","INFORMED ACIVITY");

						}
						else
							Log.e("NOTIFICATION RECEIVED","NO ORIGIN PRESENT DISCARDING");
					}
					else
						Log.e("NOTIFICATION RECEIVED","UNKNOWN IMEI, NOT INTENDED FOR CURRENT USER");
				}
				else
					Log.e("NOTIFICATION RECEIVED","EMPTY MESSAGE, UNABLE TO PROCESS");
			}	
		}

		Thread thread = new Thread(new MessageProcessor());
		thread.run();
	}


	//*******************************************************************************************************************************

	private void processIncomingFileEvents(final Context context, final String eventJSON) {

		class FileProcessor implements Runnable {

			@Override
			public void run() {
				ObjectMapper mapper = new ObjectMapper();
				MobileFileNotifierEvent event = null;
				try {
					event = mapper.readValue(eventJSON, MobileFileNotifierEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {
					if(event.getImei().equalsIgnoreCase(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId())) {

						Uri data = Uri.parse(event.getFileURL());
						Intent dfIntent = new Intent(context, DownloaderService.class);
						dfIntent.setData(data);
						dfIntent.putExtra("urlpath", event.getFileURL());
						dfIntent.putExtra("description", event.getDescription());

						Long newFileID;
						if(event.getSourceeoc()!= null) {
							newFileID = GlobalVar.db.insertFile(event.getDescription(), 0, null, event.getFileURL(), event.getSourceeoc(), event.getFileid());
							dfIntent.putExtra("beFileID", event.getSourceeoc());
						}

						else {
							newFileID = GlobalVar.db.insertFile(event.getDescription(), 0, null, event.getFileURL(), event.getSourcemeoc(), event.getFileid());
							dfIntent.putExtra("beFileID", event.getSourcemeoc());
						}

						dfIntent.putExtra("fileID", newFileID);
						context.startService(dfIntent);

					}
					else
						Log.e("NOTIFICATION RECEIVED","UNKNOWN IMEI, NOT INTENDED FOR CURRENT USER");
				}
				else
					Log.e("NOTIFICATION RECEIVED","EMPTY MESSAGE, UNABLE TO PROCESS");
			}	
		}

		Thread thread = new Thread(new FileProcessor());
		thread.run();
	}

	//*******************************************************************************************************************************

	private void processIncomingCancelActionEvent(final Context context, final String eventStr) {

		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				UpdateActionEvent event = null;
				ObjectMapper mapper = new ObjectMapper();

				try {
					event = mapper.readValue(eventStr, UpdateActionEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {

					Long actionID = event.getEventAttachment().getId();

					//	1	find the action parts and return an array of ap ids
					ArrayList<Long> apIds = GlobalVar.db.getActionPartIDsForAction(actionID);

					//	2	find the action part objectives that correspond to the array and delete them
					for(Long actionpartID : apIds) {
						//find the corresponding action part objectives for each action part available in the action
						// and set cancelled status ( ActionStageEnumDTO.PartiallyDone )
						ArrayList<Long> apoIds = GlobalVar.db.getActionPartObjectivesIDsForAP(actionpartID);
						for(Long apoID : apoIds) {
							GlobalVar.db.updateActionPartObjectiveStatus(apoID, ActionStageEnumDTO.PartiallyDone.ordinal());
						}
						// for every action part that it's objectives have been cancelled 
						// update the action part to cancelled state as well
						GlobalVar.db.updateActionPartStatus(actionpartID, ActionStageEnumDTO.PartiallyDone.ordinal());
					}
					//	4	delete the action objectives
					GlobalVar.db.updateActionObjectiveStatus(actionID, ActionStageEnumDTO.PartiallyDone.ordinal());
					//	5	delete the action
					GlobalVar.db.updateActionStatus(actionID, ActionStageEnumDTO.PartiallyDone.ordinal());

					//	Notify the adapter to reload the action related information from the local db
					Intent cancelActionIntent = new Intent(GlobalVar.INNER_APO_STATUS_CHANGED);
					context.sendBroadcast(cancelActionIntent);
				}

			}
		};

		Thread actionCanceller = new Thread(runnable);
		actionCanceller.start();
	}

	//*******************************************************************************************************************************

	private void activateGPSMeasurements(final Context context) {
		Thread gpsTHread = new Thread(new Runnable() {

			@Override
			public void run() {
				GlobalVar.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
				GlobalVar.locationListener = new ESponderLocationListener(context);
				Looper.prepare();
				GlobalVar.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 5, GlobalVar.locationListener);
				Looper.loop();
			}
		});
		gpsTHread.start();
	}


	protected class SoundTask extends AsyncTask<Integer, Context, Integer> {

		MediaPlayer mediaPlayer = new MediaPlayer();
		Context context;

		public SoundTask(Integer soundResource, Context ctx) {
			this.context = ctx;
		}


		@Override
		protected Integer doInBackground(Integer... params)
		{
			AssetFileDescriptor afd = context.getResources().openRawResourceFd(params[0]);

			//final MediaPlayer mediaPlayer = new MediaPlayer();
			try {
				mediaPlayer.reset();
				mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getDeclaredLength());
				mediaPlayer.prepare();
				mediaPlayer.setOnPreparedListener(new OnPreparedListener() {

					@Override
					public void onPrepared(MediaPlayer arg0) {
						mediaPlayer.seekTo(0);
						mediaPlayer.start();
					}});

				afd.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}

}
