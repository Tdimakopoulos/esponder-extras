package eu.esponder.ESponderFRU.demo.db;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import eu.esponder.ESponderFRU.demo.data.FRActionObjectiveEntity;
import eu.esponder.ESponderFRU.demo.data.FRActionPartEntity;
import eu.esponder.ESponderFRU.demo.data.FRAlarmEntity;
import eu.esponder.ESponderFRU.demo.data.FRFileEntity;
import eu.esponder.ESponderFRU.demo.data.FRMessageEntity;
import eu.esponder.ESponderFRU.demo.data.FRSensorEntity;
import eu.esponder.ESponderFRU.demo.enums.FRTypeEnum;
import eu.esponder.ESponderFRU.demo.enums.OCTypeEnum;
import eu.esponder.ESponderFRU.demo.enums.SensorTypeEnum;
import eu.esponder.ESponderFRU.demo.utils.ESponderUtils;
import eu.esponder.dto.model.crisis.action.ActionStageEnumDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorICALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;

public class DBAdapter {

	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;
	private Context context;

	public static final String KEY_ROWID = "_id";
	private static final String TAG = "DBAdapter";
	private static final String DATABASE_NAME = "esponder";
	private static final String OC_DBTABLE = "OPERATIONS_CENTRES";
	private static final String FR_DBTABLE = "FIRST_RESPONDERS";
	private static final String FR_SENSOR_DBTABLE = "FR_SENSOR_MEASUREMENTS";
	private static final String FR_LOCATION_DBTABLE = "FR_LOCATIONS";
	private static final String OC_LOCATION_DBTABLE = "OC_LOCATIONS";
	private static final String FR_ALARMS_DBTABLE = "FR_ALARMS";
	private static final String FR_MESSAGES_DBTABLE = "FR_MESSAGES";
	private static final String FR_ACTIONS_DBTABLE = "FR_ACTIONS";
	private static final String FR_ACTIONPARTS_DBTABLE = "FR_ACTION_PARTS";
	private static final String FR_ACTIONOBJECTIVES_DBTABLE = "FR_ACTION_OBJECTIVES";
	private static final String FR_ACTIONPART_OBJECTIVES_DBTABLE = "FR_ACTIONPART_OBJECTIVES";
	private static final String FR_FILES_DBTABLE = "FR_FILES";


	private static final int DATABASE_VERSION = 1;

	private static final String CREATE_OPERATIONS_CENTRES = "create table if not exists "
			+ OC_DBTABLE
			+ " (_id integer primary key autoincrement, "
			+ "OC_ID integer not null,"
			+ " OC_TYPE integer not null,"
			+ " IC_ID numeric not null,"
			+ "TITLE text not null,"
			+ " SUPERV_EOC numeric,"
			+ " VOIP_PROTOCOL text," + " VOIP_PATH text," + " VOIP_HOST text);";

	private static final String CREATE_FIRST_RESPONDERS = "create table if not exists "
			+ FR_DBTABLE
			+ " (_id integer primary key autoincrement, TITLE text,"
			+ " FR_ID integer,"
			+ " FR_TYPE integer,"
			+ " FRC_ID integer,"
			+ " VOIP_PROTOCOL text,"
			+ " VOIP_PATH text,"
			+ " VOIP_HOST texts,"
			+ " FIRSTNAME text," + " SURNAME text);";

	private static final String CREATE_FR_SENSORS_MEASUREMENTS = "create table if not exists "
			+ FR_SENSOR_DBTABLE
			+ " (_id integer primary key autoincrement, "
			+ "FR_ID numeric not null,"
			+ " SENSOR_TYPE integer, "
			+ "STATISTIC_TYPE integer,"
			+ "STATISTIC_UNIT text,"
			+ "SENSOR_VALUE numeric, SENSOR_MINVALUE numeric, SENSOR_MAXVALUE numeric, " +
			" DATE_FROM numeric, DATE_TO numeric);";

	private static final String CREATE_FR_LOCATIONS = "create table if not exists "
			+ FR_LOCATION_DBTABLE
			+ " (_id integer primary key autoincrement, "
			+ "FR_ID integer,"
			+ "DATE_FROM numeric, "
			+ "DATE_TO numeric, LATITUDE text, LONGITUDE text, ALTITUDE text);";

	private static final String CREATE_OC_LOCATIONS = "create table if not exists "
			+ OC_LOCATION_DBTABLE
			+ " (_id integer primary key autoincrement, "
			+ "OC_ID integer,"
			+ "DATE_FROM numeric, "
			+ "DATE_TO numeric, LATITUDE text, LONGITUDE text, ALTITUDE text);";

	private static final String CREATE_FR_ALARMS = "create table if not exists "
			+ FR_ALARMS_DBTABLE
			+ "(_id integer primary key autoincrement, "
			+ "ALARM_ID numeric not null, "
			+ "FR_ID numeric not null,"
			+ "SEVERITY integer not null,"
			+ "DETAILS text not null,"
			+ "DATE numeric not null, "
			+ "ACKED integer not null);";

	private static final String CREATE_FR_MESSAGES = "create table if not exists "
			+ FR_MESSAGES_DBTABLE
			+ "(_id integer primary key autoincrement, "
			+ "MESSAGE_ORIGIN text not null, "
			+ "MESSAGE_TEXT text not null,"
			+ "DATE numeric not null, "
			+ "ACKED integer not null);";

	private static final String CREATE_FR_ACTIONS = "create table if not exists "
			+ FR_ACTIONS_DBTABLE
			+ "(_id integer primary key autoincrement, "
			+ "ACTION_ID numeric not null,"
			+ "SEVERITY integer not null,"
			+ "OPERATION integer not null,"
			+ "TITLE text not null,"
			+ "TYPE numeric not null, "
			+ "DATE numeric not null, "
			+ "STATUS integer not null);";

	private static final String CREATE_FR_ACTIONPARTS = "create table if not exists "
			+ FR_ACTIONPARTS_DBTABLE
			+ "(_id integer primary key autoincrement, "
			+ "ACTIONPART_ID numeric not null,"
			+ "ACTION_ID numeric not null,"
			+ "FR_ID numeric not null,"
			+ "SEVERITY integer not null,"
			+ "OPERATION integer not null,"
			+ "TITLE text not null,"
			+ "DATE numeric not null, "
			+ "STATUS integer not null);";

	private static final String CREATE_FR_ACTIONOBJECTIVES = "create table if not exists "
			+ FR_ACTIONOBJECTIVES_DBTABLE
			+ "(_id integer primary key autoincrement, "
			+ "ACTIONOBJECTIVE_ID numeric not null,"
			+ "ACTION_ID numeric not null,"
			+ "TITLE text not null,"
			+ "DATE_FROM numeric not null, "
			+ "DATE_TO numeric not null, "
			+ "LATITUDE numeric not null, "
			+ "LONGITUDE numeric not null, "
			+ "ALTITUDE numeric not null, "
			+ "STATUS integer not null);";

	private static final String CREATE_FR_ACTIONPART_OBJECTIVES = "create table if not exists "
			+ FR_ACTIONPART_OBJECTIVES_DBTABLE
			+ "(_id integer primary key autoincrement, "
			+ "ACTIONPARTOBJECTIVE_ID numeric not null,"
			+ "ACTIONPART_ID numeric not null,"
			+ "TITLE text not null,"
			+ "DATE_FROM numeric not null, "
			+ "DATE_TO numeric not null, "
			+ "LATITUDE numeric not null, "
			+ "LONGITUDE numeric not null, "
			+ "ALTITUDE numeric not null, "
			+ "STATUS integer not null);";

	private static final String CREATE_FILES = "create table if not exists "
			+ FR_FILES_DBTABLE
			+ "(_id integer primary key autoincrement, "
			+ "DESC text not null,"
			+ "STATUS integer not null,"
			+ "RPATH text,"
			+ "URL text not null," +
			"SOURCE text not null," +
			"BEFILEID text not null);";

	public DBAdapter(Context ctx) {
		this.context = ctx;
		// this.DBHelper = DatabaseHelper.getInstance(context);
		this.DBHelper = new DatabaseHelper(ctx);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {

		// private static DatabaseHelper mInstance = null;

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_OPERATIONS_CENTRES);
			db.execSQL(CREATE_FIRST_RESPONDERS);
			db.execSQL(CREATE_FR_SENSORS_MEASUREMENTS);
			db.execSQL(CREATE_FR_LOCATIONS);
			db.execSQL(CREATE_OC_LOCATIONS);
			db.execSQL(CREATE_FR_ALARMS);
			db.execSQL(CREATE_FR_MESSAGES);
			db.execSQL(CREATE_FR_ACTIONS);
			db.execSQL(CREATE_FR_ACTIONPARTS);
			db.execSQL(CREATE_FR_ACTIONOBJECTIVES);
			db.execSQL(CREATE_FR_ACTIONPART_OBJECTIVES);
			db.execSQL(CREATE_FILES);
			Log.e("**** DB ****", "DB CREATED");

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			onCreate(db);
		}
	}

	// ---opens the database---
	public DBAdapter open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	// ---checks if database is open---
	public boolean isOpen() {
		return db.isOpen();

	}

	// **** OC
	// ****************************************************************************************************************//

	// insert operations centre
	// ********************************************************************
	public long insertOperationsCentre(Long OC_ID, int OC_TYPE, String TITLE,
			Long SUPERV_EOC, String VOIP_PROTOCOL, String VOIP_PATH,
			String VOIP_HOST, long IC_ID) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("OC_ID", OC_ID);
		initialValues.put("IC_ID", IC_ID);
		initialValues.put("OC_TYPE", OC_TYPE);
		initialValues.put("TITLE", TITLE);
		initialValues.put("SUPERV_EOC", SUPERV_EOC);
		initialValues.put("VOIP_PROTOCOL", VOIP_PROTOCOL);
		initialValues.put("VOIP_PATH", VOIP_PATH);
		initialValues.put("VOIP_HOST", VOIP_HOST);

		return db.insert(OC_DBTABLE, null, initialValues);
	}

	// updates an OC
	// ********************************************************************************
	public boolean updateOperationsCentre(Long OC_ID, int OC_TYPE,
			String TITLE, Long SUPERV_EOC, String VOIP_PROTOCOL,
			String VOIP_PATH, String VOIP_HOST, long IC_ID) {
		ContentValues args = new ContentValues();
		args.put("OC_TYPE", OC_TYPE);
		args.put("IC_ID", IC_ID);
		args.put("TITLE", TITLE);
		args.put("SUPERV_EOC", SUPERV_EOC);
		args.put("VOIP_PROTOCOL", VOIP_PROTOCOL);
		args.put("VOIP_PATH", VOIP_PATH);
		args.put("VOIP_HOST", VOIP_HOST);
		return db.update(OC_DBTABLE, args, "OC_ID=" + OC_ID, null) > 0;
	}

	// insert oc locations
	public long insertOCLocation(Long OC_ID, long DATE_FROM, long DATE_TO,
			String LATITUDE, String LONGITUDE, String ALTITUDE) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("OC_ID", OC_ID);
		initialValues.put("DATE_FROM", DATE_FROM);
		initialValues.put("DATE_TO", DATE_TO);
		initialValues.put("LATITUDE", LATITUDE);
		initialValues.put("LONGITUDE", LONGITUDE);
		initialValues.put("ALTITUDE", ALTITUDE);
		return db.insert(OC_LOCATION_DBTABLE, null, initialValues);
	}

	// update oc locations
	public int updateOCLocation(Long OC_ID, long DATE_FROM, long DATE_TO,
			String LATITUDE, String LONGITUDE, String ALTITUDE) {

		ContentValues args = new ContentValues();
		args.put("OC_ID", OC_ID);
		args.put("DATE_FROM", DATE_FROM);
		args.put("DATE_TO", DATE_TO);
		args.put("LATITUDE", LATITUDE);
		args.put("LONGITUDE", LONGITUDE);
		args.put("ALTITUDE", ALTITUDE);
		return db.update(OC_LOCATION_DBTABLE, args, "OC_ID=" + OC_ID, null);
	}

	public Long getOCLocationById(long oc_id) {

		String selectQuery = "SELECT  * FROM " + OC_LOCATION_DBTABLE
				+ " OC_LOCATION WHERE OC_LOCATION.OC_ID = " + oc_id;
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			Long perId = cursor.getLong(0);
			cursor.close();
			return perId;
		}
		cursor.close();
		return null;

	}

	public Long getOCById(long oc_id) {

		String selectQuery = "SELECT  * FROM " + OC_DBTABLE
				+ " OC WHERE OC.OC_ID = " + oc_id;
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			Long perId = cursor.getLong(0);
			cursor.close();
			return perId;
		}
		cursor.close();
		return null;

	}

	public void insertOrUpdateMeocAndLocation(OCMeocALTDTO meoc) {

		int updatedRows = updateOCLocation(meoc.getId(), meoc.getSnapshot()
				.getPeriod().getDateFrom(), meoc.getSnapshot().getPeriod()
				.getDateTo(),
				((SphereDTO) meoc.getSnapshot().getLocationArea()).getCentre()
				.getLatitude().toString(), ((SphereDTO) meoc
						.getSnapshot().getLocationArea()).getCentre()
						.getLongitude().toString(), String.valueOf(0));

		if (updatedRows > 0) {

			updateOperationsCentre(meoc.getId(), OCTypeEnum.MEOC.getStatType(),
					meoc.getTitle(), meoc.getSupervisingOC(), meoc.getVoIPURL()
					.getProtocol(), meoc.getVoIPURL().getPath(), meoc
					.getVoIPURL().getHost(), meoc.getIncidentCommander().getId());

		} else {
			if (insertOperationsCentre(meoc.getId(),
					OCTypeEnum.MEOC.getStatType(), meoc.getTitle(),
					meoc.getSupervisingOC(), meoc.getVoIPURL().getProtocol(),
					meoc.getVoIPURL().getPath(), meoc.getVoIPURL().getHost(), meoc.getIncidentCommander().getId()) != -1) {

				insertOCLocation(meoc.getId(), meoc.getSnapshot().getPeriod()
						.getDateFrom(), meoc.getSnapshot().getPeriod()
						.getDateTo(), ((SphereDTO) meoc.getSnapshot()
								.getLocationArea()).getCentre().getLatitude()
								.toString(), ((SphereDTO) meoc.getSnapshot()
										.getLocationArea()).getCentre().getLongitude()
										.toString(), Long.valueOf(0).toString());

			}
		}
	}

	public List<OCMeocALTDTO> getAllMeocsForMap() {
		List<OCMeocALTDTO> meocList = new ArrayList<OCMeocALTDTO>();
		// Select All Query
		String selectQuery = "SELECT  OC.OC_ID , OC.TITLE , OC.SUPERV_EOC, LOCATION.LATITUDE, LOCATION.LONGITUDE, OC.IC_ID"
				+ " FROM "
				+ OC_DBTABLE
				+ " OC, "
				+ OC_LOCATION_DBTABLE
				+ " LOCATION WHERE OC.OC_ID = LOCATION.OC_ID AND OC_TYPE="
				+ OCTypeEnum.MEOC.getStatType();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				OCMeocALTDTO meoc = new OCMeocALTDTO();
				meoc.setId(cursor.getLong(0));
				meoc.setTitle(cursor.getString(1));
				meoc.setSupervisingOC(cursor.getLong(2));
				ActorICALTDTO ic = new ActorICALTDTO();
				ic.setId(cursor.getLong(5));
				meoc.setIncidentCommander(ic);
				OperationsCentreSnapshotDTO snapshot = new OperationsCentreSnapshotDTO();
				SphereDTO sphere = new SphereDTO(
						new PointDTO(BigDecimal.valueOf(Double.valueOf(cursor
								.getString(3))), BigDecimal.valueOf(Double
										.valueOf(cursor.getString(4))),
										BigDecimal.valueOf(0)), BigDecimal.valueOf(0),
						"");
				snapshot.setLocationArea(sphere);

				meoc.setSnapshot(snapshot);

				// Adding contact to list
				meocList.add(meoc);
			} while (cursor.moveToNext());
		}

		// return contact list
		return meocList;
	}

	public PointDTO getMeocLocationByIC(long icID) {
		// Select All Query
		String selectQuery = "SELECT LOCATION.LATITUDE, LOCATION.LONGITUDE"
				+ " FROM "
				+ OC_DBTABLE
				+ " OC, "
				+ OC_LOCATION_DBTABLE
				+ " LOCATION WHERE OC.IC_ID ="+icID+" AND OC.OC_ID = LOCATION.OC_ID AND OC_TYPE="
				+ OCTypeEnum.MEOC.getStatType();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToNext()) {
			PointDTO point = new PointDTO();
			point.setLatitude(BigDecimal.valueOf(Double.valueOf(cursor
					.getString(0))));
			point.setLongitude(BigDecimal.valueOf(Double.valueOf(cursor
					.getString(1))));
			cursor.close();
			return point;
		}

		return null;
	}


	// **** FR
	// ***************************************************************************************************************//

	// insert incident commander
	// ********************************************************************
	public long insertIncidentCommander(Long FR_ID, int FR_TYPE, String TITLE) {

		Log.e("*******","INSERTED IC");
		ContentValues initialValues = new ContentValues();
		initialValues.put("FR_ID", FR_ID);
		initialValues.put("FR_TYPE", FR_TYPE);
		initialValues.put("TITLE", TITLE);
		return db.insert(FR_DBTABLE, null, initialValues);

	}

	// updates an Incident Commander
	// ********************************************************************************
	public boolean updateIncidentCommander(Long FR_ID, int FR_TYPE, String TITLE) {

		//		Log.e("*******","UPDATED IC");
		//		ContentValues args = new ContentValues();
		//		args.put("FR_TYPE", FR_TYPE);
		//		args.put("TITLE", TITLE);
		//		return db.update(FR_DBTABLE, args, "FR_ID=" + FR_ID, null) > 0;

		ContentValues args = new ContentValues();
		args.put("FR_TYPE", FR_TYPE);
		args.put("TITLE", TITLE);
		int rows = db.update(FR_DBTABLE, args, "FR_ID=" + FR_ID, null);
		if(rows > 0)
			return true;

		Log.e("*******","TRIED TO UPDATE, GOING TO INSERT IC");
		return false;


	}

	public void insertOrUpdateIncidentCommander(ActorICALTDTO ic) {

		if(!updateIncidentCommander(ic.getId(), FRTypeEnum.IC.getStatType(), ic.getTitle()))
			insertIncidentCommander(ic.getId(), FRTypeEnum.IC.getStatType(), ic.getTitle());

	}

	// **** FR
	// ***************************************************************************************************************//

	// insert first responder
	// ********************************************************************
	public long insertFirstReponder(Long FR_ID, int FR_TYPE, String TITLE,
			long FRC_ID, String VOIP_PROTOCOL, String VOIP_PATH,
			String VOIP_HOST, String FIRSTNAME, String SURNAME) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("FR_ID", FR_ID);
		initialValues.put("FR_TYPE", FR_TYPE);
		initialValues.put("TITLE", TITLE);
		initialValues.put("FRC_ID", FRC_ID);
		initialValues.put("VOIP_PROTOCOL", VOIP_PROTOCOL);
		initialValues.put("VOIP_PATH", VOIP_PATH);
		initialValues.put("VOIP_HOST", VOIP_HOST);
		initialValues.put("FIRSTNAME", FIRSTNAME);
		initialValues.put("SURNAME", SURNAME);
		return db.insert(FR_DBTABLE, null, initialValues);

	}

	// updates a First Responder
	// ********************************************************************************
	public boolean updateFirstResponder(Long FR_ID, int FR_TYPE, String TITLE,
			long FRC_ID, String VOIP_PROTOCOL, String VOIP_PATH,
			String VOIP_HOST, String FIRSTNAME, String SURNAME) {
		ContentValues args = new ContentValues();
		args.put("FR_TYPE", FR_TYPE);
		args.put("FRC_ID", FRC_ID);
		args.put("TITLE", TITLE);
		args.put("VOIP_PROTOCOL", VOIP_PROTOCOL);
		args.put("VOIP_PATH", VOIP_PATH);
		args.put("VOIP_HOST", VOIP_HOST);
		args.put("FIRSTNAME", FIRSTNAME);
		args.put("SURNAME", SURNAME);
		return db.update(FR_DBTABLE, args, "FR_ID=" + FR_ID, null) > 0;
	}

	// insert fr_locations
	// ********************************************************************************
	public long insertFRLocation(Long fr_id, long DATE_FROM, long DATE_TO,
			String LATITUDE, String LONGITUDE, String ALTITUDE) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("FR_ID", fr_id);
		initialValues.put("DATE_FROM", DATE_FROM);
		initialValues.put("DATE_TO", DATE_TO);
		initialValues.put("LATITUDE", LATITUDE);
		initialValues.put("LONGITUDE", LONGITUDE);
		initialValues.put("ALTITUDE", ALTITUDE);

		return db.insert(FR_LOCATION_DBTABLE, null, initialValues);
	}

	// update fr_locations
	// ********************************************************************************
	public long updateFRLocation(Long fr_id, long DATE_FROM, long DATE_TO,
			String LATITUDE, String LONGITUDE, String ALTITUDE) {

		ContentValues args = new ContentValues();
		args.put("FR_ID", fr_id);
		args.put("DATE_FROM", DATE_FROM);
		args.put("DATE_TO", DATE_TO);
		args.put("LATITUDE", LATITUDE);
		args.put("LONGITUDE", LONGITUDE);
		args.put("ALTITUDE", ALTITUDE);

		return db.update(FR_LOCATION_DBTABLE, args, "FR_ID=" + fr_id, null);
	}

	// Gets all FR chiefs
	// ********************************************************************************
	public List<ActorFRCALTDTO> getAllFRChiefsForMap() {
		List<ActorFRCALTDTO> frcList = new ArrayList<ActorFRCALTDTO>();
		String selectQuery = "SELECT  FRC.FR_ID , FRC.TITLE, LOCATION.LATITUDE, LOCATION.LONGITUDE"
				+ " FROM "
				+ FR_DBTABLE
				+ " FRC, "
				+ FR_LOCATION_DBTABLE
				+ " LOCATION WHERE FRC.FR_ID = LOCATION.FR_ID AND FRC.FR_TYPE="
				+ FRTypeEnum.FRC.getStatType();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				ActorFRCALTDTO frc = new ActorFRCALTDTO();
				frc.setId(cursor.getLong(0));
				frc.setTitle(cursor.getString(1));
				ActorSnapshotDTO snapshot = new ActorSnapshotDTO();
				SphereDTO sphere = new SphereDTO(
						new PointDTO(BigDecimal.valueOf(Double.valueOf(cursor
								.getString(2))), BigDecimal.valueOf(Double
										.valueOf(cursor.getString(3))),
										BigDecimal.valueOf(0)), BigDecimal.valueOf(0),"");
				snapshot.setLocationArea(sphere);
				frc.setSnapshot(snapshot);
				frcList.add(frc);
			} while (cursor.moveToNext());
		}

		return frcList;
	}


	// Gets all FR chiefs
	// ********************************************************************************
	public ActorFRCALTDTO getFRChiefById(Long frID) {
		String selectQuery = "SELECT  FRC.FR_ID , FRC.TITLE, LOCATION.LATITUDE, LOCATION.LONGITUDE"
				+ " FROM "
				+ FR_DBTABLE
				+ " FRC, "
				+ FR_LOCATION_DBTABLE
				+ " LOCATION WHERE FRC.FR_ID = LOCATION.FR_ID AND FRC.FR_ID="+frID;
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {

			ActorFRCALTDTO frc = new ActorFRCALTDTO();
			frc.setId(cursor.getLong(0));
			frc.setTitle(cursor.getString(1));
			ActorSnapshotDTO snapshot = new ActorSnapshotDTO();
			SphereDTO sphere = new SphereDTO(
					new PointDTO(BigDecimal.valueOf(Double.valueOf(cursor
							.getString(2))), BigDecimal.valueOf(Double
									.valueOf(cursor.getString(3))),
									BigDecimal.valueOf(0)), BigDecimal.valueOf(0),"");
			snapshot.setLocationArea(sphere);
			frc.setSnapshot(snapshot);
			return frc;
		}

		return null;
	}



	// Gets all first responders
	// ********************************************************************************
	public List<ActorFRALTDTO> getAllFirstRespondersForMap() {

		List<ActorFRALTDTO> frList = new ArrayList<ActorFRALTDTO>();
		// Select All Query
		String selectQuery = "SELECT  FR.FR_ID , FR.TITLE, LOCATION.LATITUDE, LOCATION.LONGITUDE,  FR.FRC_ID, FR.FIRSTNAME, FR.SURNAME"
				+ " FROM "
				+ FR_DBTABLE
				+ " FR, "
				+ FR_LOCATION_DBTABLE
				+ " LOCATION WHERE FR.FR_ID = LOCATION.FR_ID AND FR.FR_TYPE="
				+ FRTypeEnum.FR.getStatType();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				ActorFRALTDTO fr = new ActorFRALTDTO();
				fr.setId(cursor.getLong(0));
				fr.setTitle(cursor.getString(1));
				fr.setFrchief(cursor.getLong(4));
				ActorSnapshotDTO snapshot = new ActorSnapshotDTO();
				SphereDTO sphere = new SphereDTO(
						new PointDTO(BigDecimal.valueOf(Double.valueOf(cursor
								.getString(2))), BigDecimal.valueOf(Double
										.valueOf(cursor.getString(3))),
										BigDecimal.valueOf(0)), BigDecimal.valueOf(0),"");
				snapshot.setLocationArea(sphere);
				fr.setSnapshot(snapshot);
				frList.add(fr);
			} while (cursor.moveToNext());
		}

		return frList;
	}

	// Gets all the first responders of the team of the specific chief
	// ********************************************************************************
	public List<ActorFRALTDTO> getAllFirstRespondersByChief(Long chiefID) {
		List<ActorFRALTDTO> frList = new ArrayList<ActorFRALTDTO>();
		// Select All Query
		String selectQuery = "SELECT  FR.FR_ID , FR.TITLE, LOCATION.LATITUDE, LOCATION.LONGITUDE,  FR.FRC_ID, FR.FIRSTNAME, FR.SURNAME"
				+ " FROM "
				+ FR_DBTABLE
				+ " FR, "
				+ FR_LOCATION_DBTABLE
				+ " LOCATION WHERE FR.FRC_ID="
				+ chiefID
				+ " AND FR.FR_ID = LOCATION.FR_ID AND FR.FR_TYPE="
				+ FRTypeEnum.FR.getStatType() + " ORDER BY FR.FR_ID ASC";
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				ActorFRALTDTO fr = new ActorFRALTDTO();
				fr.setId(cursor.getLong(0));
				fr.setTitle(cursor.getString(1));
				fr.setFrchief(cursor.getLong(4));
				ActorSnapshotDTO snapshot = new ActorSnapshotDTO();
				SphereDTO sphere = new SphereDTO(
						new PointDTO(BigDecimal.valueOf(Double.valueOf(cursor
								.getString(2))), BigDecimal.valueOf(Double
										.valueOf(cursor.getString(3))),
										BigDecimal.valueOf(0)), BigDecimal.valueOf(0),
						"");
				snapshot.setLocationArea(sphere);
				fr.setSnapshot(snapshot);
				frList.add(fr);
			} while (cursor.moveToNext());
		}

		return frList;
	}

	// Gets the name/surname of an fr
	// ********************************************************************************
	public String getFirstResponderName(Long frID) {
		// Select All Query
		String selectQuery = "SELECT  FR.FIRSTNAME, FR.SURNAME FROM "
				+ FR_DBTABLE + " FR WHERE FR.FR_ID=" + frID;
		Cursor cursor = db.rawQuery(selectQuery, null);
		String frFullName = null;
		if (cursor.moveToFirst()) {
			if (cursor.getString(0) != null && cursor.getString(1) != null)
				frFullName = cursor.getString(0) + " " + cursor.getString(1);
		}

		return frFullName;
	}

	// Gets the title of an fr
	// ********************************************************************************
	public String getFirstResponderTitle(Long frID) {
		// Select All Query
		String selectQuery = "SELECT  FR.TITLE FROM "
				+ FR_DBTABLE + " FR WHERE FR.FR_ID=" + frID;
		Cursor cursor = db.rawQuery(selectQuery, null);
		String frTitle = null;
		if (cursor.moveToFirst()) {
			if (cursor.getString(0) != null)
				frTitle = cursor.getString(0);
		}

		return frTitle;
	}

	public void insertOrUpdateFRChiefAndLocation(ActorFRCALTDTO frc) {

		if (updateFRLocation(frc.getId(), frc.getSnapshot().getPeriod()
				.getDateFrom(), frc.getSnapshot().getPeriod().getDateTo(),
				((SphereDTO) frc.getSnapshot().getLocationArea()).getCentre()
				.getLatitude().toString(), ((SphereDTO) frc
						.getSnapshot().getLocationArea()).getCentre()
						.getLongitude().toString(), String.valueOf(0)) > 0) {

			updateFirstResponder(frc.getId(), FRTypeEnum.FRC.getStatType(),
					frc.getTitle(), -1, frc.getVoIPURL().getProtocol(), frc
					.getVoIPURL().getPath(),
					frc.getVoIPURL().getHost(), "", "");

		} else {
			if (insertFirstReponder(frc.getId(), FRTypeEnum.FRC.getStatType(),
					frc.getTitle(), -1, frc.getVoIPURL().getProtocol(), frc
					.getVoIPURL().getPath(),
					frc.getVoIPURL().getHost(), "", "") != -1) {

				insertFRLocation(frc.getId(), frc.getSnapshot().getPeriod()
						.getDateFrom(), frc.getSnapshot().getPeriod()
						.getDateTo(), ((SphereDTO) frc.getSnapshot()
								.getLocationArea()).getCentre().getLatitude()
								.toString(), ((SphereDTO) frc.getSnapshot()
										.getLocationArea()).getCentre().getLongitude()
										.toString(), String.valueOf(0));

			}
		}
	}

	public void insertOrUpdateFirstResponderAndLocation(ActorFRALTDTO fr) {

		if (updateFRLocation(fr.getId(), fr.getSnapshot().getPeriod()
				.getDateFrom(), fr.getSnapshot().getPeriod().getDateTo(),
				((SphereDTO) fr.getSnapshot().getLocationArea()).getCentre()
				.getLatitude().toString(), ((SphereDTO) fr
						.getSnapshot().getLocationArea()).getCentre()
						.getLongitude().toString(), String.valueOf(0)) > 0) {

			updateFirstResponder(fr.getId(), FRTypeEnum.FR.getStatType(),
					fr.getTitle(), fr.getFrchief(), fr.getVoIPURL()
					.getProtocol(), fr.getVoIPURL().getPath(), fr
					.getVoIPURL().getHost(), "demo fr name",
					"demo fr surname");

		} else {
			if (insertFirstReponder(fr.getId(), FRTypeEnum.FR.getStatType(),
					fr.getTitle(), fr.getFrchief(), fr.getVoIPURL()
					.getProtocol(), fr.getVoIPURL().getPath(), fr
					.getVoIPURL().getHost(), "demo fr name",
					"demo fr surname") != -1) {

				insertFRLocation(fr.getId(), fr.getSnapshot().getPeriod()
						.getDateFrom(), fr.getSnapshot().getPeriod()
						.getDateTo(), ((SphereDTO) fr.getSnapshot()
								.getLocationArea()).getCentre().getLatitude()
								.toString(), ((SphereDTO) fr.getSnapshot()
										.getLocationArea()).getCentre().getLongitude()
										.toString(), String.valueOf(0));
			}
		}
	}

	public PointDTO getFirstResponderLocationById(long fr_id) {

		String selectQuery = "SELECT LATITUDE, LONGITUDE FROM "
				+ FR_LOCATION_DBTABLE
				+ " FR_LOCATION WHERE FR_LOCATION.FR_ID = " + fr_id;
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			PointDTO point = new PointDTO();
			point.setLatitude(BigDecimal.valueOf(Double.valueOf(cursor
					.getString(0))));
			point.setLongitude(BigDecimal.valueOf(Double.valueOf(cursor
					.getString(1))));
			cursor.close();
			return point;
		}
		cursor.close();
		return null;

	}

	// ****************************************************************************************************************************//




	// insert sensor measurement
	// *****************************************************************

	public void insertOrUpdateSensorMeasurement(long FR_ID, int SENSOR_TYPE,
			int STATISTIC_TYPE, String STATISTIC_UNIT, Double SENSOR_VALUE,
			Double SENSOR_MINVALUE, Double SENSOR_MAXVALUE,
			Long DATE_FROM, Long DATE_TO) {

		FRSensorEntity result = getSensorMeasurementByFRAndSensorType(FR_ID,
				SENSOR_TYPE);
		if (result != null) {
			updateSensorMeasurement(FR_ID, SENSOR_TYPE, STATISTIC_TYPE,
					STATISTIC_UNIT, SENSOR_VALUE, SENSOR_MINVALUE, SENSOR_MAXVALUE,
					DATE_FROM, DATE_TO);
			//			Log.e("SensorMeasurement", "UPDATED");
		} else {
			ContentValues initialValues = new ContentValues();
			initialValues.put("FR_ID", FR_ID);
			initialValues.put("SENSOR_TYPE", SENSOR_TYPE);
			initialValues.put("STATISTIC_TYPE", STATISTIC_TYPE);
			initialValues.put("STATISTIC_UNIT", STATISTIC_UNIT);
			initialValues.put("SENSOR_MINVALUE", SENSOR_MINVALUE);
			initialValues.put("SENSOR_MAXVALUE", SENSOR_MAXVALUE);
			initialValues.put("DATE_FROM", DATE_FROM);
			initialValues.put("DATE_TO", DATE_TO);
			db.insert(FR_SENSOR_DBTABLE, null, initialValues);
			//			Log.e("SensorMeasurement", "INSERTED");
		}
	}

	private boolean updateSensorMeasurement(long FR_ID, int SENSOR_TYPE,
			int STATISTIC_TYPE, String STATISTIC_UNIT, Double SENSOR_VALUE,
			Double SENSOR_MINVALUE, Double SENSOR_MAXVALUE,
			Long DATE_FROM, Long DATE_TO) {

		ContentValues args = new ContentValues();
		args.put("FR_ID", FR_ID);
		args.put("SENSOR_TYPE", SENSOR_TYPE);
		args.put("STATISTIC_TYPE", STATISTIC_TYPE);
		args.put("STATISTIC_UNIT", STATISTIC_UNIT);
		args.put("SENSOR_VALUE", SENSOR_VALUE);
		args.put("SENSOR_MINVALUE", SENSOR_MINVALUE);
		args.put("SENSOR_MAXVALUE", SENSOR_MAXVALUE);
		args.put("DATE_FROM", DATE_FROM);
		args.put("DATE_TO", DATE_TO);
		return db.update(FR_SENSOR_DBTABLE, args, "FR_ID=" + FR_ID
				+ " AND SENSOR_TYPE=" + SENSOR_TYPE, null) > 0;
	}

	public FRSensorEntity getSensorMeasurementByFRAndSensorType(long fr_id,
			int sensor_type) {

		String selectQuery = "SELECT SENSORS.SENSOR_TYPE, SENSORS.STATISTIC_UNIT, SENSORS.SENSOR_VALUE, " +
				"SENSORS.SENSOR_MINVALUE, SENSORS.SENSOR_MAXVALUE, SENSORS.DATE_FROM, SENSORS.DATE_TO FROM "
				+ FR_SENSOR_DBTABLE
				+ " SENSORS WHERE SENSORS.FR_ID = "
				+ fr_id
				+ " AND SENSOR_TYPE = " + sensor_type;
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			FRSensorEntity entity = new FRSensorEntity(cursor.getInt(0),
					cursor.getDouble(2), cursor.getDouble(3),cursor.getDouble(4),
					cursor.getLong(6), cursor.getLong(5),
					cursor.getString(1));
			cursor.close();
			return entity;
		}
		cursor.close();
		return null;

	}

	public FRSensorEntity[] getLatestSensorMeasurementsByFR(long fr_id, FRSensorEntity[] measurements) {

		//Clear the old measurementss
		for(FRSensorEntity entity: measurements)
			entity = null;

		//		FRSensorEntity[] sensors = new FRSensorEntity[8];
		for(SensorTypeEnum value: SensorTypeEnum.values()) {
			FRSensorEntity entity = getSensorMeasurementByFRAndSensorType(fr_id, value.getType());
			if(entity != null)
				measurements[value.getType()] = entity;
		}

		return measurements;
	}

	// ****************************************************************************************************************************//
	// Alarms
	// ****************************************************************************************************************************//

	public long insertFRAlarm(Long ALARM_ID, Long FR_ID, int SEVERITY, String DETAILS, Long DATE, int ACKED) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("ALARM_ID", ALARM_ID);
		initialValues.put("FR_ID", FR_ID);
		initialValues.put("SEVERITY", SEVERITY);
		initialValues.put("DETAILS", DETAILS);
		initialValues.put("DATE", DATE);
		initialValues.put("ACKED", ACKED);
		return db.insert(FR_ALARMS_DBTABLE, null, initialValues);

	}


	public List<FRAlarmEntity> getAllAlarmsForFR(Long FR_ID) {

		List<FRAlarmEntity> alarmList = new ArrayList<FRAlarmEntity>();
		// Select All Query
		String selectQuery = "SELECT  ALARMS._id, ALARMS.ALARM_ID, ALARMS.FR_ID, ALARMS.SEVERITY, ALARMS.DETAILS, ALARMS.DATE, ALARMS.ACKED "
				+ " FROM "
				+ FR_ALARMS_DBTABLE
				+ " ALARMS WHERE ALARMS.FR_ID="+FR_ID+" ORDER BY ALARMS.DATE DESC";
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				FRAlarmEntity alarm = new FRAlarmEntity(cursor.getLong(0), cursor.getLong(1),
						cursor.getLong(2), cursor.getInt(3),
						cursor.getString(4), cursor.getLong(5), cursor.getInt(6));
				alarmList.add(alarm);
			} while (cursor.moveToNext());
		}
		return alarmList;
	}


	public boolean acknowledgeAlarm(long _id) {
		ContentValues args = new ContentValues();
		args.put("ACKED", 1);
		return db.update(FR_ALARMS_DBTABLE, args, "_id=" + _id, null) > 0;
	}


	public int getAlarmsCountForFR(Long FR_ID) {

		// Select All Query
		String selectQuery = "SELECT  ALARMS._id FROM "+ FR_ALARMS_DBTABLE
				+ " ALARMS WHERE ALARMS.FR_ID="+FR_ID;
		Cursor cursor = db.rawQuery(selectQuery, null);
		return cursor.getCount();
	}


	public int getNotAckedAlarmsCountForFR(Long FR_ID) {

		// Select All Query
		String selectQuery = "SELECT  ALARMS._id FROM "+ FR_ALARMS_DBTABLE
				+ " ALARMS WHERE ALARMS.FR_ID="+FR_ID+" AND ACKED=0";
		Cursor cursor = db.rawQuery(selectQuery, null);
		return cursor.getCount();
	}

	// ****************************************************************************************************************************//
	// Messages
	// ****************************************************************************************************************************//

	public long insertFRMessage(String MESSAGE_ORIGIN, String MESSAGE_TEXT, Long DATE, int ACKED) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("MESSAGE_ORIGIN", MESSAGE_ORIGIN);
		initialValues.put("MESSAGE_TEXT", MESSAGE_TEXT);
		initialValues.put("DATE", DATE);
		initialValues.put("ACKED", ACKED);
		return db.insert(FR_MESSAGES_DBTABLE, null, initialValues);

	}


	public List<FRMessageEntity> getAllMessages() {

		List<FRMessageEntity> messageList = new ArrayList<FRMessageEntity>();
		// Select All Query
		String selectQuery = "SELECT MESSAGES._id, MESSAGES.MESSAGE_ORIGIN, MESSAGES.MESSAGE_TEXT, MESSAGES.DATE, MESSAGES.ACKED "
				+ " FROM "
				+ FR_MESSAGES_DBTABLE
				+ " MESSAGES ORDER BY MESSAGES.DATE DESC";
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				FRMessageEntity message = new FRMessageEntity(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
						cursor.getLong(3), cursor.getInt(4));
				messageList.add(message);
			} while (cursor.moveToNext());
		}
		return messageList;
	}


	public boolean acknowledgeMessage(long _id) {
		ContentValues args = new ContentValues();
		args.put("ACKED", 1);
		return db.update(FR_MESSAGES_DBTABLE, args, "_id=" + _id, null) > 0;
	}


	public int getMessageCountForFR() {
		// Select All Query
		String selectQuery = "SELECT  MESSAGES._id FROM "+ FR_MESSAGES_DBTABLE
				+ " MESSAGES";
		Cursor cursor = db.rawQuery(selectQuery, null);
		return cursor.getCount();
	}


	public int getNotAckedMessagesCountForFR() {

		// Select All Query
		String selectQuery = "SELECT  MESSAGES._id FROM "+ FR_MESSAGES_DBTABLE
				+ " MESSAGES WHERE ACKED=0";
		Cursor cursor = db.rawQuery(selectQuery, null);
		return cursor.getCount();
	}


	// ****************************************************************************************************************************//
	// Actions
	// ****************************************************************************************************************************//

	public long insertAction(Long ACTION_ID, int SEVERITY, int OPERATION, String TITLE, int TYPE, long DATE, int STATUS) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("ACTION_ID", ACTION_ID);
		initialValues.put("SEVERITY", SEVERITY);
		initialValues.put("OPERATION", OPERATION);
		initialValues.put("TITLE", TITLE);
		initialValues.put("TYPE", TYPE);
		initialValues.put("DATE", DATE);
		initialValues.put("STATUS", STATUS);

		return db.insert(FR_ACTIONS_DBTABLE, null, initialValues);
	}


	// ****************************************************************************************************************************//
	// ActionParts
	// ****************************************************************************************************************************//

	public long insertActionPart(Long ACTIONPART_ID,Long ACTION_ID, Long FR_ID,
			int SEVERITY, int OPERATION, String TITLE, Long DATE, int STATUS) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("ACTIONPART_ID", ACTIONPART_ID);
		initialValues.put("ACTION_ID", ACTION_ID);
		initialValues.put("FR_ID", FR_ID);
		initialValues.put("SEVERITY", SEVERITY);
		initialValues.put("OPERATION", OPERATION);
		initialValues.put("TITLE", TITLE);
		initialValues.put("DATE", DATE);
		initialValues.put("STATUS", STATUS);

		return db.insert(FR_ACTIONPARTS_DBTABLE, null, initialValues);
	}

	public List<FRActionPartEntity> getActionPartsForFR(long frID) {
		List<FRActionPartEntity> apList = new ArrayList<FRActionPartEntity>();
		String selectQuery = "SELECT * FROM "+FR_ACTIONPARTS_DBTABLE+" WHERE FR_ID="+frID;
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				FRActionPartEntity ap = new FRActionPartEntity();
				ap.setId(cursor.getLong(0));
				ap.setActionPartID(cursor.getLong(1));
				ap.setActionId(cursor.getLong(2));
				ap.setActor(cursor.getLong(3));
				ap.setSeverityLevel(ESponderUtils.getSeverityLevelEnum(cursor.getInt(4)));
				ap.setActionOperation(ESponderUtils.getActionOperationEnum(cursor.getInt(5)));
				ap.setTitle(cursor.getString(6));
				ap.setDate(cursor.getLong(7));
				ap.setActionStage(ESponderUtils.getActionStageEnumForOperationStage(cursor.getInt(8)));
				ap.setActionPartObjectives(getActionPartObjectivesForAP(ap.getActionPartID()));
				apList.add(ap);

			} while (cursor.moveToNext());

		}

		return apList;
	}


	public int getActionPartCount() {

		String selectQuery = "SELECT COUNT(*) FROM " + FR_ACTIONPARTS_DBTABLE;
		Cursor cursor = db.rawQuery(selectQuery, null);
		if(cursor.moveToNext())
			return cursor.getInt(0);
		else
			return 0;
	}


	// ****************************************************************************************************************************//
	// ActionObjectives
	// ****************************************************************************************************************************//

	public long insertActionObjective(Long ACTIONOBJECTIVE_ID,Long ACTION_ID,
			String TITLE, Long DATE_FROM, Long DATE_TO,double LATITUDE, double LONGITUDE,double ALTITUDE, int STATUS) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("ACTIONOBJECTIVE_ID", ACTIONOBJECTIVE_ID);
		initialValues.put("ACTION_ID", ACTION_ID);
		initialValues.put("TITLE", TITLE);
		initialValues.put("DATE_FROM", DATE_FROM);
		initialValues.put("DATE_TO", DATE_TO);
		initialValues.put("LATITUDE", LATITUDE);
		initialValues.put("LONGITUDE", LONGITUDE);
		initialValues.put("ALTITUDE", ALTITUDE);
		initialValues.put("STATUS", STATUS);

		return db.insert(FR_ACTIONOBJECTIVES_DBTABLE, null, initialValues);
	}

	// ****************************************************************************************************************************//
	// ActionPartObjectives
	// ****************************************************************************************************************************//

	public long insertActionPartObjective(Long ACTIONPARTOBJECTIVE_ID, Long ACTIONPART_ID, String TITLE,
			Long DATE_FROM,Long DATE_TO,Double LATITUDE, Double LONGITUDE,Double ALTITUDE, int STATUS) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("ACTIONPARTOBJECTIVE_ID", ACTIONPARTOBJECTIVE_ID);
		initialValues.put("ACTIONPART_ID", ACTIONPART_ID);
		initialValues.put("TITLE", TITLE);
		initialValues.put("DATE_FROM", DATE_FROM);
		initialValues.put("DATE_TO", DATE_TO);
		initialValues.put("LATITUDE", LATITUDE);
		initialValues.put("LONGITUDE", LONGITUDE);
		initialValues.put("ALTITUDE", ALTITUDE);
		initialValues.put("STATUS", STATUS);

		return db.insert(FR_ACTIONPART_OBJECTIVES_DBTABLE, null, initialValues);
	}

	public Map<String, Double> getLocationForActionPartObjective(Long apoID) {

		String selectQuery = "SELECT  APO.LATITUDE, APO.LONGITUDE, APO.ALTITUDE FROM "
				+ FR_ACTIONPART_OBJECTIVES_DBTABLE
				+ " APO WHERE APO._id="+apoID;
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToNext()) {
			Map<String, Double> container = new HashMap<String, Double>();
			container.put("LATITUDE", cursor.getDouble(0));
			container.put("LONGITUDE", cursor.getDouble(1));
			container.put("ALTITUDE", cursor.getDouble(2));
			return container;
		}

		return null;
	}


	public List<FRActionObjectiveEntity> getActionPartObjectivesForAP(long apID) {

		List<FRActionObjectiveEntity> apoList = new ArrayList<FRActionObjectiveEntity>();
		String selectQuery = "SELECT * FROM "+FR_ACTIONPART_OBJECTIVES_DBTABLE+" WHERE ACTIONPART_ID=" + apID;
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				FRActionObjectiveEntity apo = new FRActionObjectiveEntity();
				apo.setId(cursor.getLong(0));
				apo.setApoID(cursor.getLong(1));
				apo.setApID(cursor.getLong(2));
				apo.setTitle(cursor.getString(3));
				apo.setDateFrom(cursor.getLong(4));
				apo.setDateTo(cursor.getLong(5));
				apo.setLatitude(cursor.getDouble(6));
				apo.setLongitude(cursor.getDouble(7));
				apo.setAltitude(cursor.getDouble(8));
				apo.setStatus(ESponderUtils.getActionStageEnumForOperationStage(cursor.getInt(9)));
				apoList.add(apo);

			} while (cursor.moveToNext());

		}
		return apoList;
	}

	public ArrayList<Long> getActionPartObjectivesIDsForAP(long apID) {

		ArrayList<Long> apoList = new ArrayList<Long>();
		String selectQuery = "SELECT ACTIONPARTOBJECTIVE_ID FROM "+FR_ACTIONPART_OBJECTIVES_DBTABLE+" WHERE ACTIONPART_ID=" + apID;
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				apoList.add(cursor.getLong(0));
			} while (cursor.moveToNext());

		}
		return apoList;
	}


	public ArrayList<Long> getActionPartIDsForAction(long actionID) {

		ArrayList<Long> apList = new ArrayList<Long>();
		String selectQuery = "SELECT ACTIONPART_ID FROM "+FR_ACTIONPARTS_DBTABLE+" WHERE ACTION_ID=" + actionID;
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				apList.add(cursor.getLong(0));
			} while (cursor.moveToNext());

		}
		return apList;
	}

	public boolean updateActionPartStatus(Long apID, int status) {
		ContentValues args = new ContentValues();
		args.put("STATUS", status);
		return db.update(FR_ACTIONPARTS_DBTABLE, args, "ACTIONPART_ID=" + apID, null) > 0;
	}

	public boolean updateActionPartObjectiveStatus(Long apoID, int status) {
		ContentValues args = new ContentValues();
		args.put("STATUS", status);
		return db.update(FR_ACTIONPART_OBJECTIVES_DBTABLE, args, "ACTIONPARTOBJECTIVE_ID=" + apoID, null) > 0;
	}

	public boolean updateActionStatus(Long actionID, int status) {
		ContentValues args = new ContentValues();
		args.put("STATUS", status);
		return db.update(FR_ACTIONS_DBTABLE, args, "ACTION_ID=" + actionID, null) > 0;
	}

	public boolean updateActionObjectiveStatus(Long actionID, int status) {
		ContentValues args = new ContentValues();
		args.put("STATUS", status);
		return db.update(FR_ACTIONOBJECTIVES_DBTABLE, args, "ACTION_ID=" + actionID, null) > 0;
	}


	public List<FRActionObjectiveEntity> getAllObjectivesByFR(Long frID) {

		List<FRActionObjectiveEntity> aopList = new ArrayList<FRActionObjectiveEntity>();

		String selectQuery = "SELECT AOP.*, AP.OPERATION FROM "+FR_ACTIONPART_OBJECTIVES_DBTABLE+" AOP, "+FR_ACTIONPARTS_DBTABLE+" AP WHERE AOP.ACTIONPART_ID = AP.ACTIONPART_ID AND AOP.ACTIONPART_ID IN " +
				"(SELECT ACTIONPART_ID FROM "+FR_ACTIONPARTS_DBTABLE+ " AP2 WHERE AP2.FR_ID = "+frID+")";

		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				FRActionObjectiveEntity apo = new FRActionObjectiveEntity();
				apo.setId(cursor.getLong(0));
				apo.setApID(cursor.getLong(2));
				apo.setApoID(cursor.getLong(1));

				apo.setLatitude(cursor.getDouble(6));
				apo.setLongitude(cursor.getDouble(7));
				apo.setAltitude(cursor.getDouble(8));

				apo.setDateFrom(cursor.getLong(4));
				apo.setDateTo(cursor.getLong(5));
				apo.setTitle(cursor.getString(3));
				apo.setObjectiveType(cursor.getInt(9));
				aopList.add(apo);
			} while (cursor.moveToNext());
		}
		return aopList;

	}


	//FIXME	Add functionality here
	public boolean cancelActionPart(Long apID, int status) {
		return updateActionPartStatus(apID, ActionStageEnumDTO.PartiallyDone.ordinal());
	}

	public boolean cancelActionPartObjective(Long apoID, int status) {
		return updateActionPartObjectiveStatus(apoID, ActionStageEnumDTO.PartiallyDone.ordinal());
	}

	public boolean cancelActionObjective(Long aoID, int status) {
		return updateActionObjectiveStatus(aoID, ActionStageEnumDTO.PartiallyDone.ordinal());
	}

	public boolean cancelAction(Long aID) {
		return updateActionStatus(aID, ActionStageEnumDTO.PartiallyDone.ordinal());
	}


	// **** FILES
	// ****************************************************************************************************************//

	// insert file
	// ********************************************************************
	public long insertFile(String DESC, int STATUS, String RPATH, String URL, String SOURCE, String BEFILEID) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("DESC", DESC);
		initialValues.put("STATUS", STATUS);
		initialValues.put("RPATH", RPATH);
		initialValues.put("URL", URL);
		initialValues.put("SOURCE", SOURCE);
		initialValues.put("BEFILEID", BEFILEID);

		return db.insert(FR_FILES_DBTABLE, null, initialValues);
	}

	// updates the status of a File
	// ********************************************************************************
	public boolean updateFileStatus(Long id, int STATUS) {
		ContentValues args = new ContentValues();
		args.put("STATUS", STATUS);
		return db.update(FR_FILES_DBTABLE, args, "_id=" + id, null) > 0;
	}

	// updates the path of a File
	// ********************************************************************************
	public boolean updateFilePath(Long id, String RPATH) {
		ContentValues args = new ContentValues();
		args.put("RPATH", RPATH);
		return db.update(FR_FILES_DBTABLE, args, "_id=" + id, null) > 0;
	}

	// Retrieves a File
	// ********************************************************************************
	public FRFileEntity getFRFileById(Long id) {

		String selectQuery = "SELECT  * FROM " + FR_FILES_DBTABLE
				+ " FILES WHERE FILES._id = " + id;
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToNext()) {
			FRFileEntity entity = new FRFileEntity(cursor.getLong(0), cursor.getString(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
			cursor.close();
			return entity;
		}
		cursor.close();
		return null;
	}


	// Retrieves all Files
	// ********************************************************************************
	public List<FRFileEntity> getAllFiles() {

		List<FRFileEntity> fileList = new ArrayList<FRFileEntity>();
		// Select All Query
		String selectQuery = "SELECT * FROM "
				+ FR_FILES_DBTABLE
				+ " FILES";
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				FRFileEntity file = new FRFileEntity(cursor.getLong(0), cursor.getString(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
				fileList.add(file);
			} while (cursor.moveToNext());
		}
		return fileList;
	}







}
