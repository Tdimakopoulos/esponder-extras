package eu.esponder.ESponderFRU.utils;

import android.util.Log;

public class ESponderLogUtils {
	
	static boolean logsEnabled = true;

	public static void error(String context, String content) {
		if(logsEnabled)
			Log.e(context, content);
	}
	
	public static void debug(String context, String content) {
		if(logsEnabled)
			Log.d(context, content);
	}
	
	public static void info(String context, String content) {
		if(logsEnabled)
			Log.i(context, content);
	}
	
	public static void verbose(String context, String content) {
		if(logsEnabled)
			Log.v(context, content);
	}
	
	public static void warn(String context, String content) {
		if(logsEnabled)
			Log.w(context, content);
	}

}
