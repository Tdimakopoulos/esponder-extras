package eu.esponder.ESponderFRU.data;

import java.util.List;

import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionStageEnumDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;

public class FRActionPartEntity {

	private Long id;
	private Long actionPartID;
	private String title;
	private Long actionId;
	private List<FRActionObjectiveEntity> actionPartObjectives;
	private ActionOperationEnumDTO actionOperation;
	private Long actor;
	private SeverityLevelDTO severityLevel;
	private ActionStageEnumDTO actionStage;
	private Long date;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getActionId() {
		return actionId;
	}

	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}

	public List<FRActionObjectiveEntity> getActionPartObjectives() {
		return actionPartObjectives;
	}

	public void setActionPartObjectives(List<FRActionObjectiveEntity> actionPartObjectives) {
		this.actionPartObjectives = actionPartObjectives;
	}

	public ActionOperationEnumDTO getActionOperation() {
		return actionOperation;
	}

	public void setActionOperation(ActionOperationEnumDTO actionOperation) {
		this.actionOperation = actionOperation;
	}

	public Long getActor() {
		return actor;
	}

	public void setActor(Long actor) {
		this.actor = actor;
	}

	public SeverityLevelDTO getSeverityLevel() {
		return severityLevel;
	}

	public void setSeverityLevel(SeverityLevelDTO severityLevel) {
		this.severityLevel = severityLevel;
	}

	public ActionStageEnumDTO getActionStage() {
		return actionStage;
	}

	public void setActionStage(ActionStageEnumDTO actionStage) {
		this.actionStage = actionStage;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActionPartID() {
		return actionPartID;
	}

	public void setActionPartID(Long actionPartID) {
		this.actionPartID = actionPartID;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}



}
