package eu.esponder.ESponderFRU.data;

public class FRSensorEntity {
	
	private int sensorType;
	private double value;
//	private double minvalue;
//	private double maxvalue;
//	private long dateTo;
//	private long dateFrom;
	private String sensorUnit;
	
	public FRSensorEntity(int sensorType, double value, String sensorUnit) {
		super();
		this.sensorType = sensorType;
		this.value = value;
//		this.setMinvalue(minvalue);
//		this.setMaxvalue(maxvalue);
//		this.dateTo = dateTo;
//		this.dateFrom = dateFrom;
		this.sensorUnit = sensorUnit;
	};
	
	
	public int getSensorType() {
		return sensorType;
	}

	public void setSensorType(int sensorType) {
		this.sensorType = sensorType;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

//	public long getDateTo() {
//		return dateTo;
//	}
//
//	public void setDateTo(long dateTo) {
//		this.dateTo = dateTo;
//	}
//
//	public long getDateFrom() {
//		return dateFrom;
//	}
//
//	public void setDateFrom(long dateFrom) {
//		this.dateFrom = dateFrom;
//	}

	public String getSensorUnit() {
		return sensorUnit;
	}

	public void setSensorUnit(String sensorUnit) {
		this.sensorUnit = sensorUnit;
	}


//	public double getMinvalue() {
//		return minvalue;
//	}
//
//
//	public void setMinvalue(double minvalue) {
//		this.minvalue = minvalue;
//	}
//
//
//	public double getMaxvalue() {
//		return maxvalue;
//	}
//
//
//	public void setMaxvalue(double maxvalue) {
//		this.maxvalue = maxvalue;
//	}
	
}
