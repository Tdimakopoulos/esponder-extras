package eu.esponder.ESponderFRU.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.content.Context;
import android.content.res.AssetManager;
import eu.esponder.ESponderFRU.xmlparsinghandlers.GeneralDataHandler;
import eu.esponder.ESponderFRU.xmlparsinghandlers.POICategoriesHandler;
import eu.esponder.ESponderFRU.xmlparsinghandlers.POISHandler;

public class DataManager {

	// Singleton-related static functions/members
	private static DataManager m_Instance = null;

	public static void initInstance(Context context){
		if(m_Instance == null){
			m_Instance = new DataManager(context);
			m_Instance.ParseDataFromXMLFiles();
		}		
	}

	public static DataManager getInstance() {
		return m_Instance;
	}

	// The context used for various Android operations
	private Context ctx;

	// The poi categories
	private ArrayList<PoiCategory> Categories = null;

	// The categorized pois 
	private ArrayList< ArrayList<Poi> > Pois = null;

	// The map server's url, defaulting to MapQuest open tiles
	private String MapServerURL = "www.in.gr";//"http://otile1.mqcdn.com/tiles/1.0.0/osm/";

	// The initial map zoom
	private float InitialMapZoom = 50.0f;

	// The initial mapcenter longitude & latitude, defaulting to Estias 1, Athens
	private double InitialCenterLongitude = 0.0;//37.988661;
	private double InitialCenterLatitude = 0.0;//23.767674;

	private DataManager(Context context) {
		ctx = context;
	}

	// Uses the parsers to parse 3 XML files that contain :
	// 1. General Data such as Server Address, Initial Center, Initial map zoom
	// 2. The Poi Categories
	// 3. The Pois
	
	private void ParseDataFromXMLFiles(){
		// 1. Parse the general data
		startXMLParsing_GeneralData();

		// 2. Parse the Poi Categories
		startXMLParsing_PoiCategories();

		// Create the pois arraylists
		Pois = new ArrayList< ArrayList<Poi>>();
		for(int i = 0; i < Categories.size(); i++){
			Pois.add(new ArrayList<Poi>());
		}
		// 3. Parse the Pois
		startXMLParsing_Pois();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	// XML Parsing functions
	/////////////////////////////////////////////////////////////////////////////////////////////////
	public void startXMLParsing_GeneralData() {

		// Open the XML file
		String xml = getXmlFromAssets("GeneralData.xml");

		// Parse the file
		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();

			final GeneralDataHandler GeneralDataHandler = new GeneralDataHandler();
			xr.setContentHandler(GeneralDataHandler);

			InputSource inStream = new InputSource();
			inStream.setCharacterStream(new StringReader(xml));
			xr.parse(inStream);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void startXMLParsing_PoiCategories() {

		// Open the xml file
		String xml = getXmlFromAssets("POICategories.xml");

		// Parse the file
		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();

			final POICategoriesHandler getPOICategoriesHandler = new POICategoriesHandler();
			xr.setContentHandler(getPOICategoriesHandler);

			InputSource inStream = new InputSource();
			inStream.setCharacterStream(new StringReader(xml));
			xr.parse(inStream);

			this.Categories = getPOICategoriesHandler.getPoiCategories();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void startXMLParsing_Pois() {

		// Open the xml file
		String xml = getXmlFromAssets("POIS.xml");

		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();

			final POISHandler getPOISHandler = new POISHandler();
			xr.setContentHandler(getPOISHandler);

			InputSource inStream = new InputSource();
			inStream.setCharacterStream(new StringReader(xml));
			xr.parse(inStream);

			//		return getOnlyPrepayBalanceHandler.getOnlyPrepayBalance();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////

	private String getXmlFromAssets(String filename){

		String xmlString = null;
		AssetManager am = ctx.getAssets();
		try {
			InputStream is = am.open(filename);
			int length = is.available();
			byte[] data = new byte[length];
			is.read(data);
			xmlString = new String(data);
		} catch (IOException e1) {
			e1 .printStackTrace();
		}

		return xmlString;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// Getters/Setters
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	public double getInitialCenterLongitude(){
		return InitialCenterLongitude;
	}
	
	public void setInitialCenterLongitude(double val){
		InitialCenterLongitude = val;
	}

	public double getInitialCenterLatitude(){
		return InitialCenterLatitude;
	}
	
	public void setInitialCenterLatitude(double val){
		InitialCenterLatitude = val;
	}

	public float getInitialMapZoom(){
		return InitialMapZoom;		
	}
	
	public void setInitialMapZoom(float val){
		InitialMapZoom = val;		
	}

	public String getMapServerURL(){
		return MapServerURL;
	}
	
	public void setMapServerURL(String val){
		MapServerURL = val;
	}

	public void AddPOI(Poi poi){
		// Find the poi category
		int pos = 0;
		for(; pos < Categories.size(); pos++){
			if(Categories.get(pos).getCategoryCode() == poi.getPoiCategoryCode()){
				break;
			}
		}

		if(pos < Categories.size()){
			// Add it to the category
			Pois.get(pos).add(poi);
		}
	}

	public ArrayList<Poi> getPoisForCategory(int categoryCode){
		int pos = 0;
		for(; pos < Categories.size(); pos++){
			if(Categories.get(pos).getCategoryCode() == categoryCode){
				break;
			}
		}
		return Pois.get(pos);
	}

	public ArrayList<PoiCategory> getCategories(){
		return this.Categories;
	}

}
