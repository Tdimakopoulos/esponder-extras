package eu.esponder.ESponderFRU.maphelpers;

import javax.microedition.khronos.opengles.GL10;

import com.nutiteq.MapView;
import com.nutiteq.components.MapPos;
import com.nutiteq.geometry.VectorElement;
import com.nutiteq.ui.MapListener;

import eu.esponder.ESponderFRU.nutiteqhelperclasses.RouteActivity;

public class RoutingMapEventListener extends MapListener {

//	private Activity activity;
	private MapView mapView;
	private MyLocationCircle locationCircle;
	
	//New Addition
	private RouteActivity activity;
    private MapPos startPos;
    private MapPos stopPos;

	public void setLocationCircle(MyLocationCircle locationCircle) {
		this.locationCircle = locationCircle;
	}

	// activity is often useful to handle click events 
	public RoutingMapEventListener(RouteActivity activity, MapView mapView) {
		this.setActivity(activity);
		this.mapView = mapView;
	}

	// Reset activity and map view
	public void reset(RouteActivity activity, MapView mapView) {
		this.setActivity(activity);
		this.mapView = mapView;
	}

	// Map drawing callbacks for OpenGL manipulations
	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {

	}

	@Override
	public void onDrawFrameAfter3D(GL10 gl, float zoomPow2) {

	}

	@Override
	public void onDrawFrameBefore3D(GL10 gl, float zoomPow2) {
		
	}

	// Vector element (touch) handlers
	@Override
	public void onLabelClicked(VectorElement vectorElement, boolean longClick) {
		
	}

	@Override
	public void onVectorElementClicked(VectorElement vectorElement, double x, double y, boolean longClick) {
		
	}

	// Map View manipulation handlers
	@Override
	public void onMapClicked(final double x, final double y, final boolean longClick) {
		// x and y are in base map projection, we convert them to the familiar WGS84 
		//Toast.makeText(activity, "onMapClicked "+(new EPSG3857()).toWgs84(x, y).x+" "+(new EPSG3857()).toWgs84(x, y).y+" longClick: "+longClick, Toast.LENGTH_SHORT).show();
		
		// x and y are in base map projection, we convert them to the familiar
		// WGS84

//		if(startPos == null){
//		// set start, or start again
//		startPos = (new EPSG3857()).toWgs84(x,y);
//		activity.setStartMarker(new MapPos(x,y));
//		}else if(stopPos == null){
//		// set stop and calculate
//		stopPos = (new EPSG3857()).toWgs84(x,y);
//		activity.setStopMarker(new MapPos(x,y));
//		activity.showRoute(startPos.y, startPos.x, stopPos.y, stopPos.x);
//
//		// restart to force new route next time
//		startPos = null;
//		stopPos = null;
//		}


		
	}

	@Override
	public void onMapMoved() {
		// this method is also called from non-UI thread
	}

	public RouteActivity getActivity() {
		return activity;
	}

	public void setActivity(RouteActivity activity) {
		this.activity = activity;
	}

	public MapPos getStartPos() {
		return startPos;
	}

	public void setStartPos(MapPos startPos) {
		this.startPos = startPos;
	}

	public MapPos getStopPos() {
		return stopPos;
	}

	public void setStopPos(MapPos stopPos) {
		this.stopPos = stopPos;
	}
}
