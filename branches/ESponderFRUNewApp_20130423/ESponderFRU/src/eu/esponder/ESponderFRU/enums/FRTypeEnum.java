package eu.esponder.ESponderFRU.enums;

public enum FRTypeEnum {
	
	IC(1),
	
	FRC(2),
	
	FR(3);
	
	private final int statType;

	FRTypeEnum(final int statType) {
		this.statType = statType;
	}

	public int getStatType() {
		return statType;
	}
	

}
