package eu.esponder.ESponderFRU.enums;

public enum SensorStatTypeEnum {

	/** The mean. */
	MEAN(1),
	
	/** The stdev. */
	STDEV(2),

	/** The autocorrelation. */
	AUTOCORRELATION(3),

	/** The maximum. */
	MAXIMUM(4),
	
	/** The minimum. */
	MINIMUM(5),

	/** The first. */
	FIRST(6),
	
	/** The last. */
	LAST(7),
	
	/** The median. */
	MEDIAN(8),
	
	/** The oldest. */
	OLDEST(9),
	
	/** The newest. */
	NEWEST(10),
	
	/** The Rate. */
	RATE(11);
	
	private final int statType;
	
	SensorStatTypeEnum(final int statType) {
		this.statType = statType;
	}

	public int getStatType() {
		return statType;
	}
	
}
