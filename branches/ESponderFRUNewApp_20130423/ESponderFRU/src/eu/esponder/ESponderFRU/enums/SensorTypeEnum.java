package eu.esponder.ESponderFRU.enums;

public enum SensorTypeEnum {
	
	ActivitySensor(0),
	
	BodyTemperatureSensor(1),
	
	BreathRateSensor(2),
	
	HeartBeatRateSensor(3),
	
	EnvTemperatureSensor(4),
	
	MethaneSensor(5),
	
	CarbonMonoxideSensor(6),
	
	CarbonDioxideSensor(7),
	
	OxygenSensor(8),
	
	HydrogenSulfideSensor(9),
	
	LPSSensor(10),
	
	LocationSensor(11);
	
	private final int type;
	
	SensorTypeEnum(final int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}

}