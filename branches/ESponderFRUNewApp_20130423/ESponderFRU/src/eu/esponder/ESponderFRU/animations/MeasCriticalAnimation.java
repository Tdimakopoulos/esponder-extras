package eu.esponder.ESponderFRU.animations;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

public class MeasCriticalAnimation extends AlphaAnimation {

	public MeasCriticalAnimation(float fromAlpha, float toAlpha) {
		super(fromAlpha, toAlpha);
		setDuration(300);
		setStartOffset(20);
		setRepeatMode(Animation.REVERSE);
		setRepeatCount(Animation.INFINITE);
	}

}
