package eu.esponder.ESponderFRU.application;

import java.util.Timer;

import android.app.Application;
import android.location.LocationListener;
import android.location.LocationManager;
import eu.esponder.ESponderFRU.db.DBAdapter;
import eu.esponder.ESponderFRU.runnables.SensorUDPServer;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;
import eu.esponder.event.datafusion.query.ESponderDFQueryRequestEvent;
import eu.esponder.event.datafusion.query.ESponderDFQueryResponseEvent;
import eu.esponder.event.mobile.MobileFileNotifierEvent;
import eu.esponder.event.mobile.MobileFileResponseEvent;
import eu.esponder.event.mobile.MobileMessageEvent;
import eu.esponder.event.mobile.MobileStatusEvent;
import eu.esponder.event.model.crisis.action.ChangeStatusActionObjectiveEvent;
import eu.esponder.event.model.crisis.action.NewActionEvent;
import eu.esponder.event.model.crisis.action.UpdateActionEvent;
import eu.esponder.event.model.crisis.action.UpdateActionPartEvent;
import eu.esponder.event.model.datafusion.DatafusionResultsGeneratedEvent;
import eu.esponder.event.model.login.LoginEsponderUserEvent;
import eu.esponder.event.model.login.LoginRequestEvent;
import eu.esponder.event.model.login.LogoutEsponderUserEvent;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;

public class GlobalVar extends Application {


	/*
	 * Intent actions to be defined here
	 */

	/**************************************************************************************************************/

	public static DBAdapter db;

	public static Timer pcdSensorTimer;

	public static Timer mapUpdateTimer;

	public static LocationManager locationManager;

	public static LocationListener locationListener;

	public static boolean logsEnabled = true;
	
	public static int udpPort = 25000;
	
	public static SensorUDPServer sender = null;

	public static long alarmDuration = 8000;

	public static long soundDuration = 4000;

	/**************************************************************************************************************/

	//	PROSYST - EXUS IPC intents

	public static final String INTENT_CATEGORY = "org.osgi.service.event.EventAdmin";

	public static final String LOGIN_REQUEST_INTENT_ACTION = LoginRequestEvent.class.getCanonicalName();

	public static final String LOGIN_RESPONSE_INTENT_ACTION = LoginEsponderUserEvent.class.getCanonicalName();

	public static final String MAP_REQUEST_HIERARCHY = ESponderDFQueryRequestEvent.class.getCanonicalName();

	public static final String MAP_RESPONSE_HIERARCHY = ESponderDFQueryResponseEvent.class.getCanonicalName();

	public static final String BIOMED_SENSOR_MEASUREMENT = CreateSensorMeasurementEnvelopeEvent.class.getCanonicalName();

	public static final String UPDATE_ACTION_EVENT = UpdateActionEvent.class.getCanonicalName();

	public static final String UPDATE_ACTIONPART_EVENT = UpdateActionPartEvent.class.getCanonicalName();

	public static final String NEW_ACTION_EVENT = NewActionEvent.class.getCanonicalName();

	public static final String INCOMING_ALARM = DatafusionResultsGeneratedEvent.class.getCanonicalName();

	public static final String INCOMING_MESSAGE = MobileMessageEvent.class.getCanonicalName();

	public static final String INCOMING_FILE = MobileFileNotifierEvent.class.getCanonicalName();

	public static final String FILE_BE_ACK = MobileFileResponseEvent.class.getCanonicalName();

	public static final String SP_INFO = MobileStatusEvent.class.getCanonicalName();

	public static final String APO_CHANGE_STATUS = ChangeStatusActionObjectiveEvent.class.getCanonicalName();

	public static final String APO_CANCELLED = UpdateActionEvent.class.getCanonicalName();

	public static final String LOG_OUT = LogoutEsponderUserEvent.class.getCanonicalName();

	public static final String UI = "eu.esponder.sensor.UI";


	/**************************************************************************************************************/

	//	EXUS - ICCS IPC intents

	public static final String SIP_LOGIN  = "eu.esponder.sip.login";

	/**************************************************************************************************************/

	//	EXUS INNER INTENTS	@Dimitris

	public static final String INNER_WIFI_CONNECTED = "eu.esponder.wifi_connected";

	public static final String INNER_LOGIN_RESPONSE_INTENT_APPROVED = "eu.esponder.login.approved";

	public static final String INNER_LOGIN_RESPONSE_INTENT_DENIED = "eu.esponder.login.denied";

	public static final String INNER_LOGIN_RESPONSE_INTENT_ERROR = "eu.esponder.login.error";

	public static final String INNER_MAP_RESPONSE_HIERARCHY = "eu.esponder.map.hierarchy.received";

	public static final String INNER_MAP_RESPONSE_HIERARCHY_ERROR = "eu.esponder.map.hierarchy.error";

	public static final String INNER_MAP_UPDATE = "eu.esponder.map.hierarchy.received";

	public static final String INNER_MAP_FR_UPDATE = "eu.esponder.map.fr.received";

	public static final String INNER_SENSORS_RECEIVED = "eu.esponder.sensors.received";

	public static final String INNER_ALARM_RECEIVED = "eu.esponder.alarms.received";

	public static final String INNER_MESSAGE_RECEIVED = "eu.esponder.message.received";

	public static final String INNER_ROUTE_RECEIVED = "eu.esponder.route.received";

	public static final String INNER_ROUTE_NOTFOUND = "eu.esponder.route.error";

	public static final String NOTIFICATION_POPUP = "eu.esponder.notification.received";

	public static final String FILE_POPUP = "eu.esponder.file.received";

	public static final String INNER_APO_STATUS_CHANGED = "eu.esponder.apo.statuschanged";

	public static final String INNER_ACTION_RECEIVED = "eu.esponder.apo.actionreceived";

	/**************************************************************************************************************/

	// Shared Preferences Named Variables

	public static final String spName = "eu.esponder.ESponder";

	public static final String spCrisisContextVar = "currentCrisisContext";

	public static final String spESponderUserRasIP = "esponderUserRasIP";
	
	public static final String spESponderExtraNodeIP = "esponderExtraNodeIP";
	
	public static final String spESponderSIPServer = "SIPServerAddress";

	public static final String spESponderUserID = "esponderUserID";

	public static final String spESponderFullName = "esponderUserFullName";

	/**************************************************************************************************************/

	// EXUS	-->	Global Variables in app memory
	// were private, turned into protected to test the garbage collection mechanism of android

	protected boolean loggedIn = false;

	protected boolean loginBeingProcessed = false;

	protected boolean chief = true;

	private int role;

	protected Boolean mapDataProcessActive = false;

	/**************************************************************************************************************/

	@Override
	public void onCreate() {
		ESponderLogUtils.error("GLOBALVAR ONCREATE", "CALLED");
		super.onCreate();
		GlobalVar.db = new DBAdapter(getApplicationContext());
		db.open();

//		locationManager = (LocationManager) (GlobalVar.this).getSystemService(Context.LOCATION_SERVICE);
//		locationListener = new ESponderLocationListener(GlobalVar.this);
	}

	/**************************************************************************************************************/

	public boolean isChief() {
		return chief;
	}

	public void setChief(boolean chief) {
		this.chief = chief;
	}

	public Boolean getMapDataProcessActive() {
		return mapDataProcessActive;
	}

	public void setMapDataProcessActive(Boolean mapDataProcessActive) {
		this.mapDataProcessActive = mapDataProcessActive;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		GlobalVar.this.loggedIn = loggedIn;
	}

	public boolean isLoginBeingProcessed() {
		return loginBeingProcessed;
	}

	public void setLoginBeingProcessed(boolean loginBeingProcessed) {
		this.loginBeingProcessed = loginBeingProcessed;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

}