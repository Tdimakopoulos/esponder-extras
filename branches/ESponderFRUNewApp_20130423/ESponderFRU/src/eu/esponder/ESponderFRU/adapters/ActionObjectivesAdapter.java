package eu.esponder.ESponderFRU.adapters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.activities.RoutingMapActivity;
import eu.esponder.ESponderFRU.data.FRActionObjectiveEntity;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;

public class ActionObjectivesAdapter extends BaseAdapter {

	private Context context;
	private FRActionObjectiveEntity[] values;
	private Long frID;
	private SimpleDateFormat dateFormatter;

	public ActionObjectivesAdapter(Context context, FRActionObjectiveEntity[] alarms, Long frID) {
		setContext(context);
		setValues(alarms);
		setDateFormatter(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
		this.frID = frID;
	}

	@Override
	public int getCount() {
		return getValues().length;
	}

	@Override
	public Object getItem(int position) {
		return getValues()[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		final FRActionObjectiveEntity apo = (FRActionObjectiveEntity) getItem(position);
		
		if(apo != null) {
			
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.actionpartobjectiverowlayout, parent, false);
			
			TextView apoTitle = (TextView) convertView.findViewById(R.id.apoTitle);
			TextView apoDate = (TextView) convertView.findViewById(R.id.apodate);
			TextView apoCoordinates = (TextView) convertView.findViewById(R.id.apoCoordinates);
			Button routeButton = (Button) convertView.findViewById(R.id.showRoute);
			
			apoTitle.setText(apo.getTitle());
			
			if(apo.getObjectiveType() == ActionOperationEnumDTO.MOVE.ordinal())
				apoTitle.setText("TYPE: MOVEMENT");
			else if(apo.getObjectiveType() == ActionOperationEnumDTO.FIX.ordinal())
				apoTitle.setText("TYPE: FIX");
			if(apo.getObjectiveType() == ActionOperationEnumDTO.TRANSPORT.ordinal())
				apoTitle.setText("TYPE: TRANSPORT");
			
			apoDate.setText(DateFormat.getInstance().format(new Date(apo.getDateFrom())));
//			apoDate.setText(String.valueOf(apo.getDateFrom()));
			
			apoCoordinates.setText("Lat: "+apo.getLatitude()+" / Long: "+apo.getLongitude());
			
			routeButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(context, RoutingMapActivity.class);
					intent.putExtra("apoID", apo.getId());
					intent.putExtra("frID", frID);
					context.startActivity(intent);
				}
			});
		}
		else {
			
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
				convertView = inflater.inflate(R.layout.noalarmsrowlayout, parent, false);
			}
			
			TextView alarmDesc = (TextView) convertView.findViewById(R.id.alarmdesc);
			alarmDesc.setText("No actions available yet");
		}
		
		

		return convertView;
	}

	// Getters and setters for private fields of the Adapter
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public FRActionObjectiveEntity[] getValues() {
		return values;
	}

	public void setValues(FRActionObjectiveEntity[] values) {
		this.values = values;
	}

	public SimpleDateFormat getDateFormatter() {
		return dateFormatter;
	}

	public void setDateFormatter(SimpleDateFormat dateFormatter) {
		this.dateFormatter = dateFormatter;
	}


}
