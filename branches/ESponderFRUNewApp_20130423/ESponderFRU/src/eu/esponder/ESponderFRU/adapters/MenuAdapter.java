package eu.esponder.ESponderFRU.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import eu.esponder.ESponderFRU.R;

public class MenuAdapter extends ArrayAdapter<String> {

	Context context;
	int resourceID;
	String[] menuItems;
	boolean frc;

	public MenuAdapter(Context context, int textViewResourceId, String[] objects, boolean frc) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.resourceID = textViewResourceId;
		this.menuItems = objects;
		this.frc = frc;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.row_sidemenu, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.menu_row_text);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.menuRowIcon);
		textView.setText(menuItems[position]);

		switch(position) {
		case 0:
			imageView.setImageResource(R.drawable.location_icon);
			break;
		case 1:
			if(frc)
				imageView.setImageResource(R.drawable.my_status_icon);
			else
				imageView.setImageResource(R.drawable.actions);
			break;
		case 2:
			if(frc)
				imageView.setImageResource(R.drawable.team_members);
			else
				imageView.setImageResource(R.drawable.messagesicon);
			break;
		case 3:
			if(frc)
				imageView.setImageResource(R.drawable.actions);
			else
				imageView.setImageResource(R.drawable.exit_icon);
			break;
		case 4:
			imageView.setImageResource(R.drawable.alarms_icon);
			break;
		case 5:
			imageView.setImageResource(R.drawable.fileicon);
			break;
		case 6:
			imageView.setImageResource(R.drawable.messagesicon);
			break;
		case 7:
			imageView.setImageResource(R.drawable.exit_icon);
			break;
		}

		return rowView;
	}


}

