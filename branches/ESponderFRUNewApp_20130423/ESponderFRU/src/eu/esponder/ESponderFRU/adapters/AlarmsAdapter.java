package eu.esponder.ESponderFRU.adapters;

import java.text.DateFormat;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.data.FRAlarmEntity;

public class AlarmsAdapter extends BaseAdapter {

	private Context context;
	private FRAlarmEntity[] values;

	public AlarmsAdapter(Context context, FRAlarmEntity[] alarms) {
		setContext(context);
		setValues(alarms);
	}

	@Override
	public int getCount() {
		return getValues().length;
	}

	@Override
	public Object getItem(int position) {
		return getValues()[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final FRAlarmEntity alarm = (FRAlarmEntity) getItem(position);

		if(alarm !=null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.alarmrowlayout, parent, false);

			TextView alarmDateTextView = (TextView) convertView.findViewById(R.id.alarmdate);
			TextView alarmDesc = (TextView) convertView.findViewById(R.id.alarmdesc);
			LinearLayout alarmRowContainer = (LinearLayout) convertView.findViewById(R.id.alarmrowcontainer);

			if(alarm.getAcked()==0) {
				alarmRowContainer.setBackgroundResource(android.R.color.white);
//				alarmDateTextView.setTextColor(SeverityLevelDTO.getSeverityColor(Byte.valueOf(String.valueOf(alarm.getSeverity()))));
			}
			else
				alarmRowContainer.setBackgroundResource(R.color.esponderGrey);
			


			alarmDateTextView.setText(DateFormat.getInstance().format(new Date(alarm.getDate())));

			alarmDesc.setText(alarm.getDetails());

			//Setting the dialog pop up that asks the user to ack or not the alarm...
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					AlertDialog.Builder builder = new AlertDialog.Builder(context);
					if(alarm.getAcked()== 0) {
						builder.setTitle("Unacknowledged Alarm");
						builder.setMessage(alarm.getDetails());
						builder.setPositiveButton("Acknowledge", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								GlobalVar.db.acknowledgeAlarm(alarm.getId());
								Intent updateIntent = new Intent(GlobalVar.INNER_ALARM_RECEIVED);
								context.sendBroadcast(updateIntent);
							}
						});

						builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
					}
					else {
						builder.setTitle("Acknowledged Alarm");
						builder.setMessage("The specified alarm has already been acknowledged.");
						builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
					}
					builder.create().show();
				}
			});
		}
		else {

			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
				convertView = inflater.inflate(R.layout.noalarmsrowlayout, parent, false);
			}

			TextView alarmDesc = (TextView) convertView.findViewById(R.id.alarmdesc);
			alarmDesc.setText("No alarms available yet");
		}



		return convertView;
	}

	// Getters and setters for private fields of the Adapter
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public FRAlarmEntity[] getValues() {
		return values;
	}

	public void setValues(FRAlarmEntity[] values) {
		this.values = values;
	}


}
