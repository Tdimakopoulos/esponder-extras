package eu.esponder.ESponderFRU.adapters;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;
import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.activities.RoutingMapActivity;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.data.FRActionObjectiveEntity;
import eu.esponder.ESponderFRU.data.FRActionPartEntity;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;
import eu.esponder.ESponderFRU.utils.ESponderUtils;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionStageEnumDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.event.model.crisis.action.ChangeStatusActionPartObjectiveEvent;

public class ActionPartExpandableAdapter extends BaseExpandableListAdapter {

	private LayoutInflater inflater;
	private Context context;
	private FRActionPartEntity[] actionParts;

	public ActionPartExpandableAdapter(Context context, FRActionPartEntity[] actionParts)
	{
		// Create Layout Inflator
		this.setContext(context);
		this.setActionParts(actionParts);
		setInflater(LayoutInflater.from(context));
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return actionParts[groupPosition].getActionPartObjectives().get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		int size = 0;
		if(actionParts.length == 1 && actionParts[groupPosition] == null)
			return size;
		else if(actionParts[groupPosition].getActionPartObjectives() != null)
			size = actionParts[groupPosition].getActionPartObjectives().size();

		return size;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return actionParts[groupPosition];
	}

	@Override
	public int getGroupCount() {
		return actionParts.length;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parentView) {

		if(actionParts[groupPosition]!=null) {

			final FRActionPartEntity actionPart = actionParts[groupPosition];

			//FIXME Provide appropriate views for tablet IC as well
			convertView = inflater.inflate(R.layout.actionpartrowlayout, parentView, false);

			TextView apoTitle = (TextView) convertView.findViewById(R.id.apTitle);

			//FIXME I manually set a line divider here
			apoTitle.setText(actionPart.getTitle().replace("Part", "Part\n"));

			TextView apoOpStage = (TextView) convertView.findViewById(R.id.apOperationStage);
			String operation = ESponderUtils.gettextForOperationType(actionPart.getActionOperation().ordinal());
			String stage = ESponderUtils.getTextForOperationStage(actionPart.getActionStage().ordinal());
			apoOpStage.setText(operation+" ( "+stage+" ) ");

			TextView apObjectivesCount = (TextView) convertView.findViewById(R.id.apObjectivesCount);

			//Update the AP oBjective counter accordingly
			apObjectivesCount.setText(String.valueOf(GlobalVar.db.getActionPartObjectivesCountForAP(actionPart.getActionPartID())));	


		}
		else {

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.noalarmsrowlayout, null, false);
			}

			TextView alarmDesc = (TextView) convertView.findViewById(R.id.alarmdesc);
			alarmDesc.setText("No actions available yet");
		}

		return convertView;
	}

	@Override
	public View getChildView(final int groupPosition, int childPosition, boolean isLastChild,
			View convertView, ViewGroup parentView) {

		if(actionParts[groupPosition] != null) {

			SimpleDateFormat format = new SimpleDateFormat("d MMM yyyy HH:mm:ss");

			final FRActionObjectiveEntity apo = actionParts[groupPosition].getActionPartObjectives().get(childPosition);

			convertView = inflater.inflate(R.layout.actionpartobjectivecontainer, parentView, false);

			TextView apoTitle = (TextView) convertView.findViewById(R.id.apoTitle);
			String newApoTitle = apo.getTitle().substring(0, apo.getTitle().indexOf("Objective")+9);
			apoTitle.setText(newApoTitle);

			TextView apoDateFrom = (TextView) convertView.findViewById(R.id.apoDateFrom);
			apoDateFrom.setText(format.format(new Date(apo.getDateFrom())));

			TextView apoDateTo = (TextView) convertView.findViewById(R.id.apoDateTo);
			apoDateTo.setText(format.format(new Date(apo.getDateTo())));

			TextView apoObjective = (TextView) convertView.findViewById(R.id.apoObjective);
			apoObjective.setText(ESponderUtils.gettextForOperationType(apo.getObjectiveType()));

			TextView apoStatus = (TextView) convertView.findViewById(R.id.apoStatus);
			apoStatus.setText("( "+ESponderUtils.getTextForOperationStage(apo.getStatus().ordinal())+" )");

			Button routeButton = (Button) convertView.findViewById(R.id.routeButton);
			routeButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(context, RoutingMapActivity.class);
					intent.putExtra("apoID", apo.getId());
					intent.putExtra("frID", actionParts[groupPosition].getActor());
					context.startActivity(intent);
				}
			});


			Button cancelAPOButton = (Button) convertView.findViewById(R.id.cancelAPOButton);
			cancelAPOButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {

					if(GlobalVar.db.updateActionPartObjectiveStatus(apo.getApoID(), ActionStageEnumDTO.PartiallyDone.ordinal())) {

						ESponderLogUtils.error("ACTION PART", "Has been cancelled...");

						final ChangeStatusActionPartObjectiveEvent event = new ChangeStatusActionPartObjectiveEvent();

						ActionPartObjectiveDTO apoToCancel = new ActionPartObjectiveDTO();
						apoToCancel.setId(apo.getApoID());
						event.setEventAttachment(apoToCancel);
						event.setNewStatus(ActionStageEnumDTO.PartiallyDone.ordinal());
						event.setEventTimestamp(new Date());
						event.setEventSeverity(SeverityLevelDTO.UNDEFINED);

						Runnable TransmitEventRunnable = new Runnable() {

							@Override
							public void run() {

								ObjectMapper mapper = new ObjectMapper();
								String eventJSON = null;
								try {
									eventJSON = mapper.writeValueAsString(event);
								} catch (JsonGenerationException e) {
									e.printStackTrace();
								} catch (JsonMappingException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}

								if(eventJSON != null) {
									//Intent to send event to Prosyst app

									Intent apoCancelledIntent = new Intent(GlobalVar.APO_CHANGE_STATUS);
									apoCancelledIntent.addCategory(GlobalVar.INTENT_CATEGORY);
									apoCancelledIntent.putExtra("event",eventJSON);
									context.sendBroadcast(apoCancelledIntent);

									//Intent to send to activity in order to refresh the data
									Intent apoConfirmedIntent = new Intent(GlobalVar.INNER_APO_STATUS_CHANGED);
									context.sendBroadcast(apoConfirmedIntent);
								}
							}
						};

						Thread apoCancelThread = new Thread(TransmitEventRunnable);
						apoCancelThread.start();

					}
					else
						ESponderLogUtils.error("APO CANCELLATION", "NOT CANCELLED");
				}
			});



			Button confirmAPOButton = (Button) convertView.findViewById(R.id.confirmAPOButton);
			confirmAPOButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if(GlobalVar.db.updateActionPartObjectiveStatus(apo.getApoID(), ActionStageEnumDTO.Done.ordinal())) {
						final ChangeStatusActionPartObjectiveEvent event = new ChangeStatusActionPartObjectiveEvent();

						// Check if all the apos have been completed, after confirming this one
						List<FRActionObjectiveEntity> results = GlobalVar.db.getActionPartObjectivesForAP(apo.getApID());
						if(results != null && results.size()>0) {
							boolean allCompleted = true;
							for(FRActionObjectiveEntity entity : results){
								if(String.valueOf(entity.getStatus().ordinal()).equalsIgnoreCase(String.valueOf(ActionStageEnumDTO.Done))) {
									allCompleted = true;
									continue;
								}
								else {
									allCompleted = false;
									break;
								}
							}
							if(allCompleted)
								GlobalVar.db.updateActionPartStatus(apo.getApID(), ActionStageEnumDTO.Done.ordinal());
						}

						ActionPartObjectiveDTO apoToConfirm = new ActionPartObjectiveDTO();
						apoToConfirm.setId(apo.getApoID());
						event.setEventAttachment(apoToConfirm);
						event.setNewStatus(ActionStageEnumDTO.Done.ordinal());
						event.setEventTimestamp(new Date());
						event.setEventSeverity(SeverityLevelDTO.UNDEFINED);

						Runnable TransmitEventRunnable = new Runnable() {	
							@Override
							public void run() {
								ObjectMapper mapper = new ObjectMapper();
								String eventJSON = null;
								try {
									eventJSON = mapper.writeValueAsString(event);
								} catch (JsonGenerationException e) {
									e.printStackTrace();
								} catch (JsonMappingException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}

								if(eventJSON != null) {
									//Intent to send event to Prosyst app
									Intent apoConfirmIntent = new Intent(GlobalVar.APO_CHANGE_STATUS);
									apoConfirmIntent.addCategory(GlobalVar.INTENT_CATEGORY);
									apoConfirmIntent.putExtra("event",eventJSON);
									context.sendBroadcast(apoConfirmIntent);

									//Intent to send to activity in order to refresh the data
									Intent apoConfirmedIntent = new Intent(GlobalVar.INNER_APO_STATUS_CHANGED);
									context.sendBroadcast(apoConfirmedIntent);
								}
							}
						};

						Thread apoConfirmThread = new Thread(TransmitEventRunnable);
						apoConfirmThread.start();

					}	
				}
			});

			if(String.valueOf(apo.getStatus().ordinal()).equalsIgnoreCase(String.valueOf(ActionStageEnumDTO.PartiallyDone)) ||
					String.valueOf(apo.getStatus().ordinal()).equalsIgnoreCase(String.valueOf(ActionStageEnumDTO.Done))) {
				cancelAPOButton.setEnabled(false);
				cancelAPOButton.setClickable(false);
				routeButton.setEnabled(false);
				routeButton.setClickable(false);
				confirmAPOButton.setEnabled(false);
				confirmAPOButton.setClickable(false);
				ESponderLogUtils.error("ACTIONPARTEXPANDABLE ADAPTER", "THE BUTTONS HAVE BEEN DISABLED");
			}
			
		}
		else {
			convertView = null;
		}

		return convertView;
	}


	// ******* Getters - Setters ******

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public LayoutInflater getInflater() {
		return inflater;
	}

	public void setInflater(LayoutInflater inflater) {
		this.inflater = inflater;
	}

	public FRActionPartEntity[] getActionParts() {
		return actionParts;
	}

	public void setActionParts(FRActionPartEntity[] actionParts) {
		this.actionParts = actionParts;
	}

}
