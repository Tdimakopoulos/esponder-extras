package eu.esponder.ESponderFRU.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.activities.Actions;
import eu.esponder.ESponderFRU.activities.ActorDetails;
import eu.esponder.ESponderFRU.activities.Alarms;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;

public class CrisisMemberAdapter extends BaseAdapter {

	private Context context;
	private ActorFRALTDTO[] values;

	public CrisisMemberAdapter(Context context, ActorFRALTDTO[] values) {
		setContext(context);
		setValues(values);
	}

	@Override
	public int getCount() {
		if(getValues() != null)
			return getValues().length;
		else return 0;
	}

	@Override
	public Object getItem(int position) {
		return getValues()[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {



		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.firstresponderrowlayoutnormal, parent, false);
		}

		final ActorFRALTDTO fr = (ActorFRALTDTO) getItem(position);

		// Set the title
		TextView frTitleText = (TextView) convertView.findViewById(R.id.frtitle);
		frTitleText.setText(fr.getTitle());

		//Set the full Name of the first responder, if available
		String frName = GlobalVar.db.getFirstResponderName(fr.getId());
		if(frName != null) {
			TextView frNameText = (TextView) convertView.findViewById(R.id.frpersonnelname);
			frNameText.setText(frName);
		}

		//Alarm Indicator

		RelativeLayout rowLayout = (RelativeLayout) convertView.findViewById(R.id.actorSmallDetailsRelativelayout);

		Boolean alarmsPresent = GlobalVar.db.getNotAckedAlarmsCountForFR(fr.getId()) > 0;

		if(alarmsPresent) {
			rowLayout.setBackgroundResource(R.drawable.emergency_actor_row_background);
//			rowLayout.setAnimation(new MeasCriticalAnimation(0.0f, 1.0f));
		}
		else {
			rowLayout.setBackgroundResource(R.drawable.normal_actor_row_background);
			rowLayout.clearAnimation();
		}


		Button showDetails = (Button) convertView.findViewById(R.id.ShowDetails);
		matchButtonBackground(alarmsPresent, showDetails);


		if(showDetails != null)
			showDetails.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, ActorDetails.class); 
					intent.putExtra("frID", fr.getId());
					context.startActivity(intent);
				}
			});

		Button showActions = (Button) convertView.findViewById(R.id.ShowActions);
		matchButtonBackground(alarmsPresent, showActions);
		
		showActions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent goToActions = new Intent(context, Actions.class);
				goToActions.putExtra("frID", fr.getId());
				context.startActivity(goToActions);
			}
		});


		Button showAlarms = (Button) convertView.findViewById(R.id.showAlarms);
		matchButtonBackground(alarmsPresent, showAlarms);
		showAlarms.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent goToAlarms = new Intent(context, Alarms.class);
				goToAlarms.putExtra("frID", fr.getId());
				context.startActivity(goToAlarms);
			}
		});

		return convertView;
	}


	private void matchButtonBackground(Boolean emergency, Button button) {

		if(emergency)
			button.setBackgroundResource(R.drawable.cornered_button_red);
		else
			button.setBackgroundResource(R.drawable.cornered_button_green);

	}

	// Getters and setters for private fields of the Adapter
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public ActorFRALTDTO[] getValues() {
		return values;
	}

	public void setValues(ActorFRALTDTO[] values) {
		this.values = values;
	}


}
