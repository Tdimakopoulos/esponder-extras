package eu.esponder.ESponderFRU.adapters;

import java.math.BigDecimal;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.data.FRSensorEntity;
import eu.esponder.ESponderFRU.enums.SensorUnitEnum;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;
import eu.esponder.dto.model.snapshot.location.PointDTO;

public class MeasurementsAdapter extends ArrayAdapter<FRSensorEntity> {
	private final Context context;
	private final FRSensorEntity[] values;
	private String[] sensorTitles = {"Activity Rate :","Body Temperature :","Breath Rate :",
			"Heart Beat Rate :","Air Temperature :","Methane :", "Carbon Monoxide :", "Carbon Dioxide :",
			"Oxygen :", "Hydrogen Sulfide :",
			"LPS :","Location :"};

	public MeasurementsAdapter(Context context, FRSensorEntity[] values) {
		super(context, R.layout.measurementrowlayout, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.measurementrowlayout, parent, false);
		TextView measurementTitle = (TextView) rowView.findViewById(R.id.measurementsrowtitle);

		measurementTitle.setText(sensorTitles[position]);

		ImageView imageView = (ImageView) rowView.findViewById(R.id.measurementsrowimage);

		switch(position) {
		case 0:
			imageView.setImageResource(R.drawable.activityicon);
			break;
		case 1:
			imageView.setImageResource(R.drawable.bodytempicon);
			break;
		case 2:
			imageView.setImageResource(R.drawable.breathrateicon);
			break;
		case 3:
			imageView.setImageResource(R.drawable.heartbeatsensor);
			break;
		case 4:
			imageView.setImageResource(R.drawable.envtempicon);
			break;
		case 5:
			imageView.setImageResource(R.drawable.methane_icon);	// methane
			break;
		case 6:
			imageView.setImageResource(R.drawable.carbonmonoxideicon);
			break;
		case 7:
			imageView.setImageResource(R.drawable.carbondioxideicon);
			break;
		case 8:
			imageView.setImageResource(R.drawable.oxygenicon);
			break;
		case 9:
			imageView.setImageResource(R.drawable.hydrogensulfideicon);
			break;
		case 10:
			imageView.setImageResource(R.drawable.lps_icon);
			break;
		case 11:
			imageView.setImageResource(R.drawable.location_icon);
			break;
		}

		// Setting the normal average value
		TextView valueText = (TextView)rowView.findViewById(R.id.measurementsrowvalue);

		if( position == 10 || position == 11 ) {
			PointDTO point = GlobalVar.db.getFirstResponderLocationById(
					context.getApplicationContext().getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE).getLong(GlobalVar.spESponderUserID, 4));
			ESponderLogUtils.error(String.valueOf(position), String.valueOf(point.getLatitude())+" / "+String.valueOf(point.getLongitude()));
			valueText.setText(String.valueOf(point.getLatitude())+" / "+String.valueOf(point.getLongitude()));
			valueText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
		}
		else if( position == 0 ) {
			valueText.setText(translateActivity(Double.valueOf(values[position].getValue()).intValue()));
		}
		else if(values[position] != null) {
			valueText.setText(round((values[position]).getValue(), 2));
		}
		else
			valueText.setText("Not available");


		TextView unitText = null;
		if(!valueText.getText().toString().equalsIgnoreCase("Not available")) {

			unitText = (TextView)rowView.findViewById(R.id.measurementsrowunit);

			switch(position) {
			case 1:
				unitText.setText(SensorUnitEnum.BodyTemperatureSensor.getUnit());
				break;
			case 2:
				unitText.setText(SensorUnitEnum.BreathRateSensor.getUnit());
				break;
			case 3:
				unitText.setText(SensorUnitEnum.HeartBeatRateSensor.getUnit());
				break;
			case 4:
				unitText.setText(SensorUnitEnum.EnvTemperatureSensor.getUnit());
				break;
			case 5:
				unitText.setText(SensorUnitEnum.GasSensor.getUnit());
				break;
			case 6:
				unitText.setText(SensorUnitEnum.GasSensor.getUnit());
				break;
			case 7:
				unitText.setText(SensorUnitEnum.GasSensor.getUnit());
				break;
			case 8:
				unitText.setText(SensorUnitEnum.GasSensor.getUnit());
				break;
			case 9:
				unitText.setText(SensorUnitEnum.GasSensor.getUnit());
				break;
			case 10:
				unitText.setText(SensorUnitEnum.LPSSensor.getUnit());
				break;
			case 11:
				unitText.setText(SensorUnitEnum.LocationSensor.getUnit());
				break;
			}

		}

		// Setting the max value
		//		TextView maxvalueText = (TextView)rowView.findViewById(R.id.maxSensorValue);
		//		if(values[position] != null) {
		//			maxvalueText.setText(round((values[position]).getMaxvalue(), 2) + " " + unitText.getText());
		//		}
		//		else
		//			maxvalueText.setText("Max N/A");

		// Setting the min value
		//		TextView minvalueText = (TextView)rowView.findViewById(R.id.minSensorValue);
		//		if(values[position] != null) {
		//			minvalueText.setText(round((values[position]).getMinvalue(), 2) + " " + unitText.getText());
		//		}
		//		else
		//			minvalueText.setText("Min N/A");

		//		if( position == 10 || position == 11 ) {
		//			TextView separator = (TextView) rowView.findViewById(R.id.minmaxseparator);
		//			separator.setVisibility(View.GONE);
		//			maxvalueText.setVisibility(View.GONE);
		//			minvalueText.setVisibility(View.GONE);
		//		}

		return rowView;
	}

	public static String round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return String.valueOf(bd.doubleValue());
	}

	private String translateActivity(int value) {
		
		switch(value) {
		case 0:
			return "OTHER";
		case 1:
			return "LYING";
		case 2:
			return "SITTING_STANDING";
		case 3:
			return "WALKING";
		case 4:
			return "RUNNING";
		default:
			return "";
		}

	}

	
}