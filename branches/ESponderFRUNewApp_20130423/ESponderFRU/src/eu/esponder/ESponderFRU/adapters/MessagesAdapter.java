package eu.esponder.ESponderFRU.adapters;

import java.text.DateFormat;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.data.FRMessageEntity;

public class MessagesAdapter extends BaseAdapter {

	private Context context;
	private FRMessageEntity[] values;

	public MessagesAdapter(Context context, FRMessageEntity[] alarms) {
		setContext(context);
		setValues(alarms);
	}

	@Override
	public int getCount() {
		return getValues().length;
	}

	@Override
	public Object getItem(int position) {
		return getValues()[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		final FRMessageEntity message = (FRMessageEntity) getItem(position);
		
		if(message !=null) {
				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
				convertView = inflater.inflate(R.layout.messagerowlayout, parent, false);

			TextView messageDateTextView = (TextView) convertView.findViewById(R.id.messagedate);
			TextView messageSenderTextView = (TextView) convertView.findViewById(R.id.messagesenderDesc);
			TextView messageDesc = (TextView) convertView.findViewById(R.id.messageDesc);
			LinearLayout messageRowlayout = (LinearLayout) convertView.findViewById(R.id.messagerowcontainer);
			
			
			messageDateTextView.setText("( "+ DateFormat.getInstance().format(new Date(message.getTimestamp()))+" )");
			messageSenderTextView.setText(message.getSource());
			//Alarm Indicator
			
			messageDesc.setText(message.getMessageBody());

			// Setting ACKED image accordingly
			
			if(message.getAcked()==0)
				messageRowlayout.setBackgroundResource(android.R.color.white);
			else
				messageRowlayout.setBackgroundResource(R.color.esponderGrey);
				

			//Setting the dialog pop up that asks the user to ack or not the alarm...
				convertView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						AlertDialog.Builder builder = new AlertDialog.Builder(context);
						if(message.getAcked()== 0) {
							builder.setTitle("Unacknowledged Message");
							builder.setMessage("Message sender: "+message.getSource());
							builder.setPositiveButton("Acknowledge", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									GlobalVar.db.acknowledgeMessage(message.getId());
									Intent updateIntent = new Intent(GlobalVar.INNER_MESSAGE_RECEIVED);
									context.sendBroadcast(updateIntent);
								}
							});

							builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
								}
							});
						}
						else {
							builder.setTitle("Acknowledged Message");
							builder.setMessage("The specified message has already been acknowledged.");
							builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
								}
							});
						}
						builder.create().show();
					}
				});
		}
		else {
			
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
				convertView = inflater.inflate(R.layout.noalarmsrowlayout, parent, false);
			}
			
			TextView alarmDesc = (TextView) convertView.findViewById(R.id.alarmdesc);
			alarmDesc.setText("No messages available yet");
		}
		
		

		return convertView;
	}

	// Getters and setters for private fields of the Adapter
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public FRMessageEntity[] getValues() {
		return values;
	}

	public void setValues(FRMessageEntity[] values) {
		this.values = values;
	}


}





//package eu.esponder.ESponderFRU.adapters;
//
//import java.text.DateFormat;
//import java.util.Date;
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//import eu.esponder.ESponderFRU.R;
//import eu.esponder.ESponderFRU.application.GlobalVar;
//import eu.esponder.ESponderFRU.data.FRMessageEntity;
//
//public class MessagesAdapter extends BaseAdapter {
//
//	private Context context;
//	private FRMessageEntity[] values;
//
//	public MessagesAdapter(Context context, FRMessageEntity[] alarms) {
//		setContext(context);
//		setValues(alarms);
//	}
//
//	@Override
//	public int getCount() {
//		return getValues().length;
//	}
//
//	@Override
//	public Object getItem(int position) {
//		return getValues()[position];
//	}
//
//	@Override
//	public long getItemId(int position) {
//		return position;
//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		
//		final FRMessageEntity message = (FRMessageEntity) getItem(position);
//		
//		if(message !=null) {
//				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//				convertView = inflater.inflate(R.layout.messagerowlayout2, parent, false);
//
//			TextView messageDateTextView = (TextView) convertView.findViewById(R.id.messagedate);
//			TextView messageSenderTextView = (TextView) convertView.findViewById(R.id.messagesenderDesc);
//			TextView messageDesc = (TextView) convertView.findViewById(R.id.messageDesc);
//			ImageView messageImage = (ImageView) convertView.findViewById(R.id.messageIcon);
//			
//			
//			messageDateTextView.setText("( "+ DateFormat.getInstance().format(new Date(message.getTimestamp()))+" )");
//			messageSenderTextView.setText(message.getSource());
//			//Alarm Indicator
//			
//			messageDesc.setText(message.getMessageBody());
//
//			// Setting ACKED image accordingly
//			
//			if(message.getAcked()==0)
//				messageImage.setImageDrawable(convertView.getResources().getDrawable(R.drawable.delete));
//			else
//				messageImage.setImageDrawable(convertView.getResources().getDrawable(R.drawable.select));
//
//			//Setting the dialog pop up that asks the user to ack or not the alarm...
//				convertView.setOnClickListener(new OnClickListener() {
//
//					@Override
//					public void onClick(View arg0) {
//						AlertDialog.Builder builder = new AlertDialog.Builder(context);
//						if(message.getAcked()== 0) {
//							builder.setTitle("Unacknowledged Message");
//							builder.setMessage("Message sender: "+message.getMessageBody());
//							builder.setPositiveButton("Acknowledge", new DialogInterface.OnClickListener() {
//
//								@Override
//								public void onClick(DialogInterface dialog, int which) {
//									GlobalVar.db.acknowledgeMessage(message.getId());
//									Intent updateIntent = new Intent(GlobalVar.INNER_MESSAGE_RECEIVED);
//									context.sendBroadcast(updateIntent);
//								}
//							});
//
//							builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//								@Override
//								public void onClick(DialogInterface dialog, int which) {
//									dialog.dismiss();
//								}
//							});
//						}
//						else {
//							builder.setTitle("Acknowledged Message");
//							builder.setMessage("The specified message has already been acknowledged.");
//							builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
//
//								@Override
//								public void onClick(DialogInterface dialog, int which) {
//									dialog.dismiss();
//								}
//							});
//						}
//						builder.create().show();
//					}
//				});
//		}
//		else {
//			
//			if (convertView == null) {
//				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//				convertView = inflater.inflate(R.layout.noalarmsrowlayout, parent, false);
//			}
//			
//			TextView alarmDesc = (TextView) convertView.findViewById(R.id.alarmdesc);
//			alarmDesc.setText("No messages available yet");
//		}
//		
//		
//
//		return convertView;
//	}
//
//	// Getters and setters for private fields of the Adapter
//	public Context getContext() {
//		return context;
//	}
//
//	public void setContext(Context context) {
//		this.context = context;
//	}
//
//	public FRMessageEntity[] getValues() {
//		return values;
//	}
//
//	public void setValues(FRMessageEntity[] values) {
//		this.values = values;
//	}
//
//
//}
