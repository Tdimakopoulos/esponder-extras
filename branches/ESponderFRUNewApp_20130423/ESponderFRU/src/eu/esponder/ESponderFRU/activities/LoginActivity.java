package eu.esponder.ESponderFRU.activities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.data.FRActionObjectiveEntity;
import eu.esponder.ESponderFRU.handlers.LoginHandler;
import eu.esponder.ESponderFRU.runnables.MapRequestEventSender;
import eu.esponder.ESponderFRU.runnables.UDPServerController;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;
import eu.esponder.ESponderFRU.utils.ESponderUtils;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.login.LoginRequestEvent;
import eu.esponder.event.model.login.LogoutEsponderUserEvent;

public class LoginActivity extends Activity {

	Button entryButton, logOutButton;
	LoginHandler loginHandler;
	TextView beText, sensorText, logintext;
	ProgressDialog pdialog, wifiPDialog;
	LoginReceiver loginReceiver;
	AlertDialog networkInterfaceDialog;
	IntentFilter filter;
	Handler handler;
	Boolean loggedIn = false;
	final Boolean wifiEnabled = false;

	public void onCreate(Bundle savedInstanceState) {

		ESponderLogUtils.error("LOGIN onCreate","RUNNING");

		super.onCreate(savedInstanceState);
		setContentView(R.layout.loginlayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		setTitle("ESponder FRU Application");

		if(((GlobalVar)getApplicationContext()).isLoggedIn())
			ESponderLogUtils.error("USER","**** LOGGED IN ****");
		else
			ESponderLogUtils.error("USER","**** NOT LOGGED IN ****");

		setLoginButtonOnClickListener();
		logOutButton = (Button) findViewById(R.id.logout_button);
		logOutButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				AlertDialog.Builder builder = new Builder(LoginActivity.this);
				builder.setTitle("Warning");
				builder.setIcon(R.drawable.warning);
				builder.setMessage("Are you sure you want to exit the application? Pressing yes will result in you logging off.");
				builder.setNegativeButton("Cancel", null);
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						logOutProcess();
						LoginActivity.this.finish();
					}
				});

				builder.create().show();

			}
		});

		loginHandler = new LoginHandler(beText, sensorText, logintext);

		final WifiManager wifimanager = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		if(!wifimanager.isWifiEnabled()) {
			AlertDialog.Builder builder = new Builder(LoginActivity.this);
			builder.setIcon(R.drawable.warning);
			builder.setTitle("Deactivated WiFi");
			builder.setMessage("Would you like to activate the WiFi of your smartphone?");
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					wifimanager.setWifiEnabled(true);
					wifiPDialog = ProgressDialog.show(LoginActivity.this, "Network Connection",
							"Please wait while a connection is being established", false, true, new OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							LoginActivity.this.finish();
						}
					});
				}
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					LoginActivity.this.finish();
				}
			});

			networkInterfaceDialog = builder.create();
			networkInterfaceDialog.show();
		}
		else
			loginHandler.postDelayed(loginInitialiser, 500);

		//		createsampleActionParts();
		//		printObjectives((long) 4);

	}


	private void setLoginButtonOnClickListener() {
		this.entryButton = (Button) findViewById(R.id.login_button);
		this.entryButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(((GlobalVar)getApplicationContext()).isLoggedIn()) {
					if(((GlobalVar)getApplicationContext()).getRole() == 0) {
						Intent intent = new Intent(LoginActivity.this, MainActivity.class);
						startActivity(intent);	
					}
					else if(((GlobalVar)getApplicationContext()).getRole() == 1) {
						Intent intent = new Intent(LoginActivity.this, MainFRActivity.class);
						startActivity(intent);	
					}
					else if(((GlobalVar)getApplicationContext()).getRole() == 2) {
						Intent intent = new Intent(LoginActivity.this, MainICActivity.class);
						startActivity(intent);	
					}

				}
				else {
					AlertDialog.Builder builder = new Builder(LoginActivity.this);
					builder.setTitle("Warning");
					builder.setMessage("You are currently not logged in. Do you want to?");
					builder.setPositiveButton("Log In", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							logOutProcess();
							loginHandler.postDelayed(loginInitialiser, 200);
						}
					});

					builder.setNegativeButton("Cancel", null);
					builder.create().show();

				}
			}
		});
	}
	

	//FIXME This will be used for the demo operation of the application instead of the loginInitialiser Runnable
	/*private Runnable loginInitialiser2 = new Runnable() {

		@Override
		public void run() {
			pdialog = ProgressDialog.show(LoginActivity.this, "Log in attempt", "Sending login request to ESponder server...", false, true);

			// Here we produce the mock map response from the back end for presentation purposes

			String imei = ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
			String loginResponse = "{\"sIPusername\":null,\"sIPpassword\":null,\"sIPaddressEOC\":null,\"sIPaddresssrv\":null," +
					"\"imei\":\""+imei+"\",\"sIPaddressMEOC\":null,\"sIPaddressFRC\":\"\",\"sIPaddress\":null,\"webcamip\":null," +
					"\"rasberryip\":\"192.168.2.248:3000\",\"iRole\":0,\"actorType\":null,\"eventDest\":null,\"journalMessageInfo\":\"LoginEsponderUserEvent\"," +
					"\"eventAttachment\":{\"@class\":\"eu.esponder.dto.model.crisis.resource.ActorFRCDTO\",\"resourceId\":\"4:null\",\"id\":4}," +
					"\"eventSource\":{\"@class\":\"eu.esponder.dto.model.user.ESponderUserDTO\",\"actorRole\":0,\"resourceId\":\"1:null\",\"id\":1}," +
					"\"eventTimestamp\":1394199295422,\"eventSeverity\":\"UNDEFINED\",\"journalMessage\":\"Login OK \"}";

			Intent mockLoginrequestIntent = new Intent(GlobalVar.LOGIN_RESPONSE_INTENT_ACTION);
			mockLoginrequestIntent.addCategory(GlobalVar.INTENT_CATEGORY);
			mockLoginrequestIntent.putExtra("event",loginResponse);
			sendBroadcast(mockLoginrequestIntent);

		}
	};*/


	private Runnable loginInitialiser = new Runnable() {

		@Override
		public void run() {
			pdialog = ProgressDialog.show(LoginActivity.this, "Log in attempt", "Sending login request to ESponder server...", false, true);
			createEventForLogin(LoginActivity.this);
		}
	};

	private void createEventForLogin(final Context context) {

		class LoginEventSender implements Runnable {
			@Override
			public void run() {
				Log.d("CREATE EVENT FOR LOGIN THREAD", "RUNNING, SENDING REQUEST");

				ESponderUserDTO user = new ESponderUserDTO();
				user.setId(Long.valueOf(0));

				LoginRequestEvent pEvent = new LoginRequestEvent();
				pEvent.setIMEI(((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
				pEvent.setPassword("Password");
				pEvent.setUsername("FRC");
				pEvent.setPkikey(retrievePKIKeyFromFile());
				pEvent.setiLoginType(1);
				pEvent.setEventAttachment(user);
				pEvent.setJournalMessage("Login request Username : "+pEvent.getUsername()+" PKI : "+pEvent.getPkikey());
				pEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
				pEvent.setEventTimestamp(new Date());
				pEvent.setEventSource(user);
				String myIPAddress = ESponderUtils.getIPAddress(context, true);
				if(myIPAddress!= null && !myIPAddress.equalsIgnoreCase(""))
					pEvent.setMyip(myIPAddress);
				ObjectMapper mapper = new ObjectMapper();
				String jsonObject = null;
				try {
					jsonObject = mapper.writeValueAsString(pEvent);
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(jsonObject != null) {
					Intent intent = new Intent(GlobalVar.LOGIN_REQUEST_INTENT_ACTION);
					intent.addCategory(GlobalVar.INTENT_CATEGORY);
					intent.putExtra("event", jsonObject);
					context.sendBroadcast(intent);
				}
				else
					ESponderLogUtils.error("SEND LOGIN EVENT","SERIALIZATION HAS FAILED");

			}
		}

		Thread thread = new Thread(new LoginEventSender());
		thread.start();
		ESponderLogUtils.error("LOGIN", "STARTED LOGIN THREAD");
	}


	private void createEventForMapInit(final Context context) {

		Thread thread = new Thread(new MapRequestEventSender(LoginActivity.this, "LoginActivity"));
		thread.start();
	}

	public class LoginReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_WIFI_CONNECTED)) {
				NetworkInfo nInfo = intent.getExtras().getParcelable("nInfo");
				if(nInfo != null && nInfo.getType() == ConnectivityManager.TYPE_WIFI) {
					if(wifiPDialog!=null)
						if(wifiPDialog.isShowing())
							wifiPDialog.dismiss();
					ESponderLogUtils.error("WIFI", "CONNECTED");
					if(!((GlobalVar)context).isLoggedIn())
						loginHandler.postDelayed(loginInitialiser, 500);
				}
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_APPROVED)) {
				ESponderLogUtils.error("Login receiver", "RUNNING LOGIN RESPONSE EVENT APPROVED");
				loggedIn = true;
				if(pdialog != null)
					if(pdialog.isShowing())
						pdialog.dismiss();

				//FIXME This will be used for the demo operation of the application instead of the requestMapPositions Runnable
				/*Runnable requestMapPositions2 = new Runnable() {

					@Override
					public void run() {
						if(LoginActivity.this.pdialog.isShowing())
							LoginActivity.this.pdialog.dismiss();

						LoginActivity.this.pdialog = ProgressDialog.show(LoginActivity.this, 
								"Welcome First Responder", "Requesting information from the backend...", false, true);


						// Here we produce the mock map response from the back end for presentation purposes
						String imei = ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
						String mapResponse = "{\"imei\":\""+imei+"\",\"requestID\":20,\"queryID\":1,\"mapPositions\":{\"@class\":\"eu.esponder.dto.model.crisis.alternative.OCEocALTDTO\",\"title\":\"EOC Attica\",\"subordinateMEOCs\":[\"java.util.HashSet\",[{\"@class\":\"eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO\",\"title\":\"MEOC Piraeus\",\"supervisingOC\":1,\"incidentCommander\":{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorICALTDTO\",\"title\":\"IC #2\",\"personnel\":1,\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"hostName\":\"192.168.11.1\",\"path\":\"IC#2\",\"host\":\"192.168.11.1\"},\"crisisManager\":1,\"frTeams\":[\"java.util.HashSet\",[]],\"operationsCentre\":3,\"id\":3},\"meocCrisisContext\":1,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO\",\"id\":4,\"status\":\"MOVING\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":11,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":37.986153,\"longitude\":23.775927},\"radius\":0.00,\"title\":\"Sphere 04\"},\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1393928336623,\"dateTo\":1393928366623},\"operationsCentre\":3},\"id\":3,\"users\":[\"java.util.HashSet\",[7]],\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"hostName\":\"192.168.10.3\",\"path\":\"meocPiraeus\",\"host\":\"192.168.10.3\"}},{\"@class\":\"eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO\",\"title\":\"MEOC Athens\",\"supervisingOC\":1,\"incidentCommander\":{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorICALTDTO\",\"title\":\"IC #1\",\"personnel\":1,\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"hostName\":\"192.168.11.1\",\"path\":\"IC#1\",\"host\":\"192.168.11.1\"},\"crisisManager\":1,\"frTeams\":[\"java.util.HashSet\",[{\"@class\":\"eu.esponder.dto.model.crisis.alternative.FRTeamALTDTO\",\"snapshots\":[\"java.util.HashSet\",[]],\"incidentCommander\":2,\"id\":1,\"frchief\":{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO\",\"title\":\"FRC #1\",\"personnel\":4,\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"hostName\":\"192.168.11.1\",\"path\":\"FRC#1\",\"host\":\"192.168.11.1\"},\"subordinates\":[\"java.util.HashSet\",[{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO\",\"title\":\"FR #1.1\",\"personnel\":5,\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"hostName\":\"192.168.11.1\",\"path\":\"FR#1.1\",\"host\":\"192.168.11.1\"},\"equipmentSet\":[\"java.util.HashSet\",[2]],\"frchief\":4,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\",\"id\":2,\"status\":\"ACTIVE\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":13,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":37.988031,\"longitude\":23.776463},\"radius\":0.00,\"title\":\"Sphere 2\"},\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1393928337407,\"dateTo\":1393928367407},\"actor\":5},\"id\":5},{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO\",\"title\":\"FR #1.2\",\"personnel\":6,\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"hostName\":\"192.168.11.1\",\"path\":\"FR#1.2\",\"host\":\"192.168.11.1\"},\"equipmentSet\":[\"java.util.HashSet\",[3]],\"frchief\":4,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\",\"id\":3,\"status\":\"ACTIVE\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":14,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":37.987777,\"longitude\":23.776592},\"radius\":0.00,\"title\":\"Sphere 3\"},\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1393928337407,\"dateTo\":1393928367407},\"actor\":6},\"id\":6}]],\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\",\"id\":4134,\"status\":\"ACTIVE\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":3873,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":37.991623,\"longitude\":23.740690,\"altitude\":18.000000},\"radius\":0.00,\"title\":\"FRLocation-1394113038483\"},\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1394113022575,\"dateTo\":1394113022575},\"actor\":4},\"equipmentSet\":[\"java.util.HashSet\",[1]],\"id\":4}},{\"@class\":\"eu.esponder.dto.model.crisis.alternative.FRTeamALTDTO\",\"snapshots\":[\"java.util.HashSet\",[]],\"incidentCommander\":2,\"id\":2,\"frchief\":{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO\",\"title\":\"FRC #2\",\"personnel\":7,\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"hostName\":\"192.168.11.1\",\"path\":\"FRC#2\",\"host\":\"192.168.11.1\"},\"subordinates\":[\"java.util.HashSet\",[{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO\",\"title\":\"FR #2.2\",\"personnel\":9,\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"hostName\":\"192.168.11.1\",\"path\":\"FR#2.1\",\"host\":\"192.168.11.1\"},\"equipmentSet\":[\"java.util.HashSet\",[6]],\"frchief\":7,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\",\"id\":6,\"status\":\"ACTIVE\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":17,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":37.986644,\"longitude\":23.779382},\"radius\":0.00,\"title\":\"Sphere 6\"},\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1393928337407,\"dateTo\":1393928367407},\"actor\":9},\"id\":9},{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO\",\"title\":\"FR #2.1\",\"personnel\":8,\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"hostName\":\"192.168.11.1\",\"path\":\"FR#2.1\",\"host\":\"192.168.11.1\"},\"equipmentSet\":[\"java.util.HashSet\",[5]],\"frchief\":7,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\",\"id\":5,\"status\":\"ACTIVE\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":16,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":37.987168,\"longitude\":23.779081},\"radius\":0.00,\"title\":\"Sphere 5\"},\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1393928337407,\"dateTo\":1393928367407},\"actor\":8},\"id\":8}]],\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\",\"id\":4,\"status\":\"ACTIVE\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":15,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":37.986898,\"longitude\":23.778459},\"radius\":0.00,\"title\":\"Sphere 4\"},\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1393928337407,\"dateTo\":1393928367407},\"actor\":7},\"equipmentSet\":[\"java.util.HashSet\",[4]],\"id\":7}}]],\"operationsCentre\":2,\"id\":2},\"meocCrisisContext\":1,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO\",\"id\":2,\"status\":\"MOVING\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":9,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":37.985832,\"longitude\":23.779510},\"radius\":0.00,\"title\":\"Sphere 02\"},\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1393928336623,\"dateTo\":1393928366623},\"operationsCentre\":2},\"id\":2,\"users\":[\"java.util.HashSet\",[6]],\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"hostName\":\"192.168.10.2\",\"path\":\"meocAthens\",\"host\":\"192.168.10.2\"}}]],\"eocCrisisContext\":1,\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":7,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":37.988581,\"longitude\":23.738773},\"radius\":0.00,\"title\":\"Eoc LOcation\"},\"id\":1,\"users\":[\"java.util.HashSet\",[5]],\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"hostName\":\"192.168.10.1\",\"path\":\"eocAttica\",\"host\":\"192.168.10.1\"}},\"pResponse\":null,\"eventDest\":null,\"journalMessageInfo\":\"ESponder DF Query Response Event\",\"eventAttachment\":{\"@class\":\"eu.esponder.dto.model.crisis.CrisisContextDTO\",\"id\":1,\"title\":\"Fire Brigade Drill\",\"crisisLocation\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":3,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":3.000000,\"longitude\":3.000000,\"altitude\":3.000000},\"radius\":2.00,\"title\":\"Fire Brigade Drill Location Area\"},\"eocOperationsCentres\":[\"java.util.HashSet\",[1]],\"meocOperationsCentres\":[\"java.util.HashSet\",[2,3]],\"startDate\":1363046400,\"endDate\":1363132800,\"crisisTypes\":[\"java.util.HashSet\",[{\"@class\":\"eu.esponder.dto.model.type.CrisisDisasterTypeDTO\",\"id\":66,\"title\":\"Fire\",\"children\":[\"java.util.HashSet\",[]]},{\"@class\":\"eu.esponder.dto.model.type.CrisisFeatureTypeDTO\",\"id\":73,\"title\":\"Injury\",\"children\":[\"java.util.HashSet\",[]]}]],\"crisisContextParameters\":[\"java.util.HashSet\",[]],\"crisisStatus\":\"INPROGRESS\",\"crisisAdditionalInfo\":\"Additional Info Field\",\"actions\":[\"java.util.HashSet\",[1,2,3,4,5,6,7,8,9,10,11]]},\"eventSource\":{\"@class\":\"eu.esponder.dto.model.user.ESponderUserDTO\",\"actorRole\":0,\"resourceId\":\"0:null\",\"id\":0},\"eventTimestamp\":1394201240768,\"eventSeverity\":\"UNDEFINED\",\"journalMessage\":\"Test Event\"}";

						Intent mockMapUpdateIntent = new Intent(GlobalVar.MAP_RESPONSE_HIERARCHY);
						mockMapUpdateIntent.addCategory(GlobalVar.INTENT_CATEGORY);
						mockMapUpdateIntent.putExtra("event",mapResponse);
						sendBroadcast(mockMapUpdateIntent);

					}
				};*/

				Runnable requestMapPositions = new Runnable() {

					@Override
					public void run() {
						if(LoginActivity.this.pdialog.isShowing())
							LoginActivity.this.pdialog.dismiss();

						String frTitle = "";
						if(((GlobalVar)LoginActivity.this.getApplicationContext()).getRole() == 0 ||
								((GlobalVar)LoginActivity.this.getApplicationContext()).getRole() == 1)
							frTitle = "First Responder";
						else if(((GlobalVar)LoginActivity.this.getApplicationContext()).getRole() == 2)
							frTitle = "MEOC Commander";


						LoginActivity.this.pdialog = ProgressDialog.show(LoginActivity.this, 
								"Welcome "+frTitle, "\nRequesting information from the backend...", false, true);
						createEventForMapInit(LoginActivity.this);
					}
				};


				// If the user of the app is an fr, then we start the fr main activity
				if(((GlobalVar)LoginActivity.this.getApplicationContext()).getRole() == 1) {
					Intent startFRActivity = new Intent(LoginActivity.this, MainFRActivity.class);
					LoginActivity.this.startActivity(startFRActivity);
				}
				// else, if the user is an FRC or IC, then we ask for location updates from the backend
				else if(((GlobalVar)LoginActivity.this.getApplicationContext()).getRole() == 0 ||
						((GlobalVar)LoginActivity.this.getApplicationContext()).getRole() == 2) {
					loginHandler.post(requestMapPositions);	
				}

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_DENIED)) {
				ESponderLogUtils.error("Login receiver", "RUNNING LOGIN RESPONSE EVENT DENIED");
				if(pdialog != null)
					if(pdialog.isShowing())
						pdialog.dismiss();

				// Unsuccessful Login
				AlertDialog.Builder builder =new Builder(LoginActivity.this);
				builder.setCancelable(false);
				builder.setIcon(R.drawable.erroricon);
				builder.setTitle("Login Failed");
				builder.setMessage("Unauthorized Access Attempt, please retry...");
				builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						LoginActivity.this.finish();
					}
				});

				builder.create().show();

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_ERROR)) {
				ESponderLogUtils.error("Login receiver", "RUNNING LOGIN RESPONSE EVENT ERROR");
				if(pdialog != null)
					if(pdialog.isShowing())
						pdialog.dismiss();

				AlertDialog.Builder builder =new Builder(LoginActivity.this);
				builder.setCancelable(false);
				builder.setIcon(R.drawable.erroricon);
				builder.setTitle("Login Failed");
				builder.setMessage("Scrambled response from server, please contact your network administrator...");
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						LoginActivity.this.finish();
					}
				});

				builder.create().show();
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY)) {
				if(loggedIn) {
					ESponderLogUtils.error("Login receiver", "RUNNING MAP RESPONSE EVENT APPROVED");
					// Data have been received successfully for the hierarchy
					if(LoginActivity.this.pdialog.isShowing())
						LoginActivity.this.pdialog.dismiss();

					// Enter the map main screen
					if(((GlobalVar)getApplicationContext()).getRole() == 0)
						startActivity(new Intent(LoginActivity.this, MainActivity.class));
					else if(((GlobalVar)getApplicationContext()).getRole() == 2)
						startActivity(new Intent(LoginActivity.this, MainICActivity.class));
				}
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR)) {
				ESponderLogUtils.error("Login receiver", "RUNNING MAP RESPONSE EVENT ERROR");
				// Error came up while retrieving the map hierarchy
				if(LoginActivity.this.pdialog.isShowing())
					LoginActivity.this.pdialog.dismiss();


				AlertDialog.Builder builder =new Builder(LoginActivity.this);
				builder.setCancelable(false);
				builder.setIcon(R.drawable.erroricon);
				builder.setTitle("map Init Error");
				builder.setMessage("Error occured while retrieving the initial data for the map...");
				builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						LoginActivity.this.finish();
					}
				});

				builder.create().show();
			}

		}
	}


	@Override
	protected void onDestroy() {
		if(pdialog!=null)
			if(pdialog.isShowing())
				pdialog.dismiss();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		unregisterReceiver(loginReceiver);
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		loginReceiver = new LoginReceiver();
		filter = new IntentFilter(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_APPROVED);
		filter.addAction(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_DENIED);
		filter.addAction(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_ERROR);
		filter.addAction(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY);
		filter.addAction(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR);
		filter.addAction(GlobalVar.INNER_WIFI_CONNECTED);
		registerReceiver(loginReceiver, filter);
	}


	private String retrievePKIKeyFromFile() {

		String filename = "pkikey.txt";
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,filename);
		//Read text from file
		StringBuilder sbuilder = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				sbuilder.append(line);
			}
			br.close();
		}
		catch (IOException e) {
			Log.d("FILE_HANDLER", "Error occurred while opening file");
			e.printStackTrace();
		}
		String skey = sbuilder.toString();
		ESponderLogUtils.error("PKI",skey);

		return skey;
	}


	//	public void createsampleActionParts() {
	//
	//		int counter = GlobalVar.db.getActionPartCount();
	//		ESponderLogUtils.error("COUNT OF OBJECTIVES", String.valueOf(counter));
	//
	//		if(counter == 0) {
	//
	//			ActionDTO action1 = new ActionDTO();
	//			action1.setActionOperation(ActionOperationEnumDTO.MOVE);
	//			action1.setId(Long.valueOf(1));
	//			action1.setTitle("Action1");
	//			action1.setActionStage(ActionStageEnumDTO.InProgress);
	//			action1.setType("oti nanai");
	//			action1.setSeverityLevel(SeverityLevelDTO.SERIOUS);
	//
	//			ActionObjectiveDTO ao1 = new ActionObjectiveDTO();
	//			ao1.setAction(action1);
	//			ao1.setActionStage(ActionStageEnumDTO.InProgress);
	//			ao1.setId(Long.valueOf(1));
	//			SphereDTO sphere1 = new SphereDTO();
	//			sphere1.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
	//			ao1.setLocationArea(sphere1);
	//			ao1.setPeriod(new PeriodDTO(Long.valueOf(0), Long.valueOf(0)));
	//			ao1.setTitle("AO_1");
	//
	//
	//			ActionObjectiveDTO ao2 = new ActionObjectiveDTO();
	//			ao2.setAction(action1);
	//			ao2.setActionStage(ActionStageEnumDTO.InProgress);
	//			ao2.setId(Long.valueOf(2));
	//			SphereDTO sphere2 = new SphereDTO();
	//			sphere2.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
	//			ao2.setLocationArea(sphere2);
	//			ao2.setPeriod(new PeriodDTO(Long.valueOf(0), Long.valueOf(0)));
	//			ao2.setTitle("AO_2");
	//
	//			action1.setActionObjectives(new HashSet<Long>());
	//			action1.getActionObjectives().add(Long.valueOf(1));
	//			action1.getActionObjectives().add(Long.valueOf(2));
	//
	//
	//			//**********************************************************************************************
	//
	//
	//			ActionPartDTO ap1 = new ActionPartDTO();
	//			ap1.setActionId(Long.valueOf(1));
	//			ap1.setActionOperation(ActionOperationEnumDTO.MOVE);
	//			ap1.setActionStage(ActionStageEnumDTO.InProgress);
	//			ap1.setId(Long.valueOf(1));
	//			ap1.setActor(Long.valueOf(4));
	//			ap1.setSeverityLevel(SeverityLevelDTO.SERIOUS);
	//			ap1.setTitle("ActionPart1");
	//
	//			ActionPartObjectiveDTO apo1 = new ActionPartObjectiveDTO();
	//			apo1.setActionPartID(Long.valueOf(1));
	//			apo1.setId(Long.valueOf(1));
	//			SphereDTO sphere3 = new SphereDTO();
	//			sphere3.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
	//			apo1.setLocationArea(sphere3);
	//			apo1.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
	//			apo1.setTitle("APO1");
	//
	//			ActionPartObjectiveDTO apo2 = new ActionPartObjectiveDTO();
	//			apo2.setActionPartID(Long.valueOf(1));
	//			apo2.setId(Long.valueOf(2));
	//			SphereDTO sphere4 = new SphereDTO();
	//			sphere4.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
	//			apo2.setLocationArea(sphere4);
	//			apo2.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
	//			apo2.setTitle("APO2");
	//
	//
	//
	//			//*****************************************************************************
	//
	//			ActionPartDTO ap2 = new ActionPartDTO();
	//			ap2.setActionId(Long.valueOf(1));
	//			ap2.setActionOperation(ActionOperationEnumDTO.MOVE);
	//			ap2.setActionStage(ActionStageEnumDTO.InProgress);
	//			ap2.setId(Long.valueOf(2));
	//			ap2.setActor(Long.valueOf(5));
	//			ap2.setSeverityLevel(SeverityLevelDTO.SERIOUS);
	//			ap2.setTitle("ActionPart2");
	//
	//			ActionPartObjectiveDTO apo3 = new ActionPartObjectiveDTO();
	//			apo3.setActionPartID(Long.valueOf(2));
	//			apo3.setId(Long.valueOf(3));
	//			SphereDTO sphere5 = new SphereDTO();
	//			sphere5.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
	//			apo3.setLocationArea(sphere5);
	//			apo3.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
	//			apo3.setTitle("APO3");
	//
	//			ActionPartObjectiveDTO apo4 = new ActionPartObjectiveDTO();
	//			apo4.setActionPartID(Long.valueOf(2));
	//			apo4.setId(Long.valueOf(4));
	//			SphereDTO sphere6 = new SphereDTO();
	//			sphere6.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
	//			apo4.setLocationArea(sphere6);
	//			apo4.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
	//			apo4.setTitle("APO4");
	//
	//			//*****************************************************************************
	//
	//			ActionPartDTO ap3 = new ActionPartDTO();
	//			ap3.setActionId(Long.valueOf(1));
	//			ap3.setActionOperation(ActionOperationEnumDTO.MOVE);
	//			ap3.setActionStage(ActionStageEnumDTO.InProgress);
	//			ap3.setId(Long.valueOf(3));
	//			ap3.setActor(Long.valueOf(6));
	//			ap3.setSeverityLevel(SeverityLevelDTO.SERIOUS);
	//			ap3.setTitle("ActionPart3");
	//
	//			ActionPartObjectiveDTO apo5 = new ActionPartObjectiveDTO();
	//			apo5.setActionPartID(Long.valueOf(3));
	//			apo5.setId(Long.valueOf(5));
	//			SphereDTO sphere7 = new SphereDTO();
	//			sphere7.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
	//			apo5.setLocationArea(sphere7);
	//			apo5.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
	//			apo5.setTitle("APO5");
	//
	//			ActionPartObjectiveDTO apo6 = new ActionPartObjectiveDTO();
	//			apo6.setActionPartID(Long.valueOf(3));
	//			apo6.setId(Long.valueOf(6));
	//			SphereDTO sphere8 = new SphereDTO();
	//			sphere8.setCentre(new PointDTO(new BigDecimal(37.902981), new BigDecimal(23.753225), new BigDecimal(0)));
	//			apo6.setLocationArea(sphere8);
	//			apo6.setPeriod(new PeriodDTO(Long.valueOf(1), Long.valueOf(1)));
	//			apo6.setTitle("APO6");
	//
	//
	//			NewActionEvent event = new NewActionEvent();
	//			event.setEventAttachment(action1);
	//			event.setEventSeverity(SeverityLevelDTO.SERIOUS);
	//			event.setEventTimestamp(new Date());
	//			
	//			List<ActionPartDTO> apSet = new ArrayList<ActionPartDTO>();
	//			apSet.add(ap1);
	//			apSet.add(ap2);
	//			apSet.add(ap3);
	//
	//			List<ActionObjectiveDTO> aoSet = new ArrayList<ActionObjectiveDTO>();
	//			aoSet.add(ao1);
	//			aoSet.add(ao2);
	//
	//
	//			List<ActionPartObjectiveDTO> apoSet = new ArrayList<ActionPartObjectiveDTO>();
	//			apoSet.add(apo1);
	//			apoSet.add(apo2);
	//			apoSet.add(apo3);
	//			apoSet.add(apo4);
	//			apoSet.add(apo5);
	//			apoSet.add(apo6);
	//
	//			event.setActionObjectives(new ResultListDTO(aoSet));
	//			event.setActionPartObjectives(new ResultListDTO(apoSet));
	//			event.setActionParts(new ResultListDTO(apSet));
	//			
	//			ObjectMapper mapper = new ObjectMapper();
	//			String eventJSON = null;
	//			try {
	//				eventJSON = mapper.writeValueAsString(event);
	//			} catch (JsonGenerationException e) {
	//				e.printStackTrace();
	//			} catch (JsonMappingException e) {
	//				e.printStackTrace();
	//			} catch (IOException e) {
	//				e.printStackTrace();
	//			}
	//
	//			if(eventJSON != null) {
	//
	//				Intent intent = new Intent();
	//				intent.addCategory(GlobalVar.INTENT_CATEGORY);
	//				intent.setAction(GlobalVar.NEW_ACTION_EVENT);
	//				intent.putExtra("event", eventJSON);
	//				LoginActivity.this.sendBroadcast(intent);
	//
	//			}
	//			else
	//				ESponderLogUtils.error("ACTIONS CREATION","SOMETHING HAS GONE WRONG, EVENT NOT CREATED");
	//
	//		}
	//
	//	}

	public void printObjectives (Long frID) {

		List<FRActionObjectiveEntity> list = GlobalVar.db.getAllObjectivesByFR(frID);
		if(list!= null)
			if(list.size()>0) {
				for(FRActionObjectiveEntity apo : list)
					ESponderLogUtils.error("AOP",apo.getTitle());
			}
			else
				ESponderLogUtils.error("APO","NO RESULTS FOUND");
	}


	private void logOutProcess() {

		//Perform the log out related operations here...

		// GlobalVar variables
		((GlobalVar)getApplicationContext()).setLoggedIn(false);
		((GlobalVar)getApplicationContext()).setChief(false);

		// Shared preferences should be cleared also...

		SharedPreferences.Editor editor = getSharedPreferences(GlobalVar.spName, MODE_PRIVATE).edit();
		editor.putLong(GlobalVar.spESponderUserID, -1);
		editor.putString(GlobalVar.spESponderUserRasIP, "");
		editor.putString(GlobalVar.spESponderSIPServer, "");
		editor.putString(GlobalVar.spESponderExtraNodeIP, "");
		editor.putString(GlobalVar.spESponderFullName, "");
		editor.commit();

		// Stop periodically running threads, check global Timers etc

		// This the the timer that controls the thread that updates the map
		// and the locations of the FRs
		
		//FIXME MAP UPDATE MAP UPDATE MAP UPDATE
//		if(GlobalVar.mapUpdateTimer != null) {
//			GlobalVar.mapUpdateTimer.cancel();
//			GlobalVar.mapUpdateTimer.purge();
//			GlobalVar.mapUpdateTimer = null;
//		}

		// This the the timer that controls the threads that receives the bulk sensor measurements from the 
		// beagle board and also transmit an event for the phone status...
		if(GlobalVar.pcdSensorTimer != null) {
			GlobalVar.pcdSensorTimer.cancel();
			GlobalVar.pcdSensorTimer.purge();
			GlobalVar.pcdSensorTimer = null;
		}
		
		UDPServerController.terminateServerInstances();
		
		LogoutEsponderUserEvent logOutEvent = new LogoutEsponderUserEvent();
		logOutEvent.setIMEI(((TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
		ObjectMapper mapper = new ObjectMapper();
		String eventAsString = null;

		try {
			eventAsString = mapper.writeValueAsString(logOutEvent);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//Close the database
		if(GlobalVar.db!=null) {
			if(GlobalVar.db.isOpen()) {
				ESponderLogUtils.error("LogOut Process", "DATABASE IS OPEN");
			}
			else
				ESponderLogUtils.error("LogOut Process", "DATABASE WAS ALREADY CLOSED");
		}



		if(eventAsString!=null) {
			Intent logOutIntent = new Intent(GlobalVar.LOG_OUT);
			logOutIntent.addCategory(GlobalVar.INTENT_CATEGORY);
			LoginActivity.this.sendBroadcast(logOutIntent);
		}

		ESponderLogUtils.error("LOG OFF", "USER LOGGED OUT");

	}

	@Override
	public void onBackPressed() {
		logOutProcess();
		super.onBackPressed();
	}


}
