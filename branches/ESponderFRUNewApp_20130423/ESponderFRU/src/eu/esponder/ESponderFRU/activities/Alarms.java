package eu.esponder.ESponderFRU.activities;

import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.adapters.AlarmsAdapter;
import eu.esponder.ESponderFRU.adapters.MenuAdapter;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.data.FRAlarmEntity;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;
import eu.esponder.ESponderFRU.utils.ESponderUtils;

public class Alarms extends SlidingActivity {

	private SlidingMenu sm;
	private ListView alarmListView;
	private AlarmsAdapter alarmsAdapter;
	private FRAlarmEntity[] alarms;
	private Long frID;
	private AlarmsReceiver receiver;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.alarmslayout);
		setTitle("ESponder - Alarms");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		// This function initializes the sliding menus to full functionality
		initializeMenu();
		
		TextView alarmsHeader = (TextView) findViewById(R.id.AlarmsHeader);
		alarmsHeader.setText("Alarms");
		
		if(getIntent().hasExtra("frID")) {
			
			frID = getIntent().getLongExtra("frID", -1);
			if(frID != -1) {
				
				String frTitle = GlobalVar.db.getFirstResponderTitle(frID);
				if(alarmsHeader!=null)
					alarmsHeader.setText("Alarms for "+frTitle);
				
				findFRAlarmsForAdapter();
				alarmListView = (ListView) findViewById(R.id.AlarmsList);
				alarmsAdapter = new AlarmsAdapter(Alarms.this, alarms);
				alarmListView.setAdapter(alarmsAdapter);	
			}
		}
		
	}


	//Create sample data for Meocs, FRCs, FRs
	// We need id of member, Title of member, type of member etc

	private void initializeMenu() {

		ListView menuList = null;
		View menuView = null;
		MenuAdapter menuAdapter = null;

		Resources res = getResources();
		String[] menuItems = null;

		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {

			menuView = LayoutInflater.from(Alarms.this).inflate(R.layout.menu_frame, null);
			menuList = (ListView) menuView.findViewById(R.id.MenuAsList);
			menuItems = res.getStringArray(R.array.frc_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.FRCMenuClickListener(Alarms.this));
			menuAdapter = new MenuAdapter(Alarms.this, R.layout.row_sidemenu, menuItems, true);
		}
		else if( ((GlobalVar)getApplicationContext()).getRole() == 2 ) {
			menuList = (ListView) findViewById(R.id.applicationMenu);
			menuItems = res.getStringArray(R.array.ic_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.ICMenuClickListener(Alarms.this));
			menuAdapter = new MenuAdapter(Alarms.this, R.layout.row_sidemenu, menuItems, false);
		}
		
		menuList.setAdapter(menuAdapter);
		
		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {
			setBehindContentView(menuView);
			sm = getSlidingMenu();
			sm.setShadowWidthRes(R.dimen.shadow_width);
			sm.setShadowDrawable(R.drawable.shadow);
			sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
			sm.setFadeDegree(0.35f);
			sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
			setSlidingActionBarEnabled(true);
		}
	}


	private void findFRAlarmsForAdapter() {
		
		if(frID != null) {
			if(frID != -1) {
				List<FRAlarmEntity> alarmList = GlobalVar.db.getAllAlarmsForFR(frID);
				if( alarmList.size() > 0 ) {
					Iterator<FRAlarmEntity> it = alarmList.iterator();
					alarms = new FRAlarmEntity[alarmList.size()];
					int counter = 0;
					while(it.hasNext()) {
						alarms[counter] = it.next();
						counter++;
					}
				}
				else
					alarms = new FRAlarmEntity[1];
			}
			else
				ESponderLogUtils.error("Populating the alarms list for the chief", "The chief is unknown");
		}
	}
	
	
	
	
	class AlarmsReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_ALARM_RECEIVED)) {
				findFRAlarmsForAdapter();
				alarmsAdapter.setValues(alarms);
				alarmsAdapter.notifyDataSetChanged();	
			}	
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(receiver!=null)
			unregisterReceiver(receiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(receiver == null)
			receiver = new AlarmsReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_ALARM_RECEIVED);
		registerReceiver(receiver, filter);
		findFRAlarmsForAdapter();
		if(alarms != null) {
			alarmsAdapter.setValues(alarms);
			alarmsAdapter.notifyDataSetChanged();
		}

	}

}
