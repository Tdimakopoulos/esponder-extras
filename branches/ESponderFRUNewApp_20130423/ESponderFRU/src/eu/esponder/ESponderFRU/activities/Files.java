package eu.esponder.ESponderFRU.activities;

import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.adapters.FilesAdapter;
import eu.esponder.ESponderFRU.adapters.MenuAdapter;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.data.FRFileEntity;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;
import eu.esponder.ESponderFRU.utils.ESponderUtils;

public class Files extends SlidingActivity {

	//	private ImageView menuImage;
	private SlidingMenu sm;
	private ListView fileListView;
	private FilesAdapter filesAdapter;
	private FRFileEntity[] files;
	private FileReceiver receiver;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.alarmslayout);
		setTitle("ESponder - Files");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		// This function initializes the sliding menus to full functionality
		initializeMenu();

		TextView alarmsHeader = (TextView) findViewById(R.id.AlarmsHeader);
		alarmsHeader.setText("Received Files");

		findFRFilesForAdapter();
		fileListView = (ListView) findViewById(R.id.AlarmsList);
		filesAdapter = new FilesAdapter(Files.this, files);
		fileListView.setAdapter(filesAdapter);

	}


	//Create sample data for Meocs, FRCs, FRs
	// We need id of member, Title of member, type of member etc

	private void initializeMenu() {

		ListView menuList = null;
		View menuView = null;
		MenuAdapter menuAdapter = null;

		Resources res = getResources();
		String[] menuItems = null;

		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {

			menuView = LayoutInflater.from(Files.this).inflate(R.layout.menu_frame, null);
			menuList = (ListView) menuView.findViewById(R.id.MenuAsList);
			menuItems = res.getStringArray(R.array.frc_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.FRCMenuClickListener(Files.this));
			menuAdapter = new MenuAdapter(Files.this, R.layout.row_sidemenu, menuItems, true);
		}
		else if( ((GlobalVar)getApplicationContext()).getRole() == 2 ) {
			menuList = (ListView) findViewById(R.id.applicationMenu);
			menuItems = res.getStringArray(R.array.ic_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.ICMenuClickListener(Files.this));
			menuAdapter = new MenuAdapter(Files.this, R.layout.row_sidemenu, menuItems, false);
		}
		
		menuList.setAdapter(menuAdapter);
		
		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {
			setBehindContentView(menuView);
			sm = getSlidingMenu();
			sm.setShadowWidthRes(R.dimen.shadow_width);
			sm.setShadowDrawable(R.drawable.shadow);
			sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
			sm.setFadeDegree(0.35f);
			sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
			setSlidingActionBarEnabled(true);
		}
	}

	//	private void setMenuButtonOnClickListener() {
	//
	//		menuImage = (ImageView) findViewById(R.id.menuButton);
	//		if(menuImage!=null) {
	//			menuImage.setOnClickListener(new OnClickListener() {
	//
	//				@Override
	//				public void onClick(View v) {
	//					if(sm!= null)
	//						sm.toggle(true);
	//				}
	//			});
	//		}
	//	}



	private void findFRFilesForAdapter() {

				List<FRFileEntity> alarmList = GlobalVar.db.getAllFiles();
				if( alarmList.size() > 0 ) {
					Iterator<FRFileEntity> it = alarmList.iterator();
					files = new FRFileEntity[alarmList.size()];
					int counter = 0;
					while(it.hasNext()) {
						files[counter] = it.next();
						ESponderLogUtils.error("DESC",files[counter].getDescription());
						counter++;
					}
				}
				else
					files = new FRFileEntity[1];

	}




	class FileReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			if(intent.getAction().equalsIgnoreCase(GlobalVar.FILE_POPUP)) {
				findFRFilesForAdapter();
				filesAdapter.setValues(files);
				filesAdapter.notifyDataSetChanged();	
			}	
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(receiver!=null)
			unregisterReceiver(receiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(receiver == null)
			receiver = new FileReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.FILE_POPUP);
		registerReceiver(receiver, filter);
		findFRFilesForAdapter();
		if(files != null) {
			filesAdapter.setValues(files);
			filesAdapter.notifyDataSetChanged();
		}

	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        Intent a = new Intent(this, MainActivity.class);
	        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        startActivity(a);
	        this.finish();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}

}
