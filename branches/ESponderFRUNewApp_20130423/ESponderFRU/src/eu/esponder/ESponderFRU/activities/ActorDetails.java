package eu.esponder.ESponderFRU.activities;

import java.util.Timer;
import java.util.TimerTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.adapters.MeasurementsAdapter;
import eu.esponder.ESponderFRU.adapters.MenuAdapter;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.data.FRSensorEntity;
import eu.esponder.ESponderFRU.utils.ESponderUtils;

public class ActorDetails extends SlidingActivity {

	Long frID;
	SlidingMenu sm;
	LinearLayout actorLayout;
	LinearLayout actorDetailsLayout;
	ActorDetailsReceiver receiver;
	IntentFilter filter;
	Button alarmsButton, actionButton;

	FRSensorEntity[] measurements;
	ListView measurementsList;
	MeasurementsAdapter adapter;
	Timer t1;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.actordetailslayout);
		setTitle("ESponder - FR Details");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		initializeMenu();

		Intent startingIntent = getIntent();
		frID = startingIntent.getLongExtra("frID", -1);

		if(frID != -1) {

			TextView frName = (TextView) findViewById(R.id.nameContents);
			if(frName!= null) {
				String frNameFromDB = GlobalVar.db.getFirstResponderTitle(frID);
				if(frNameFromDB!=null)
					frName.setText(frNameFromDB);
			}


			measurements = new FRSensorEntity[12];
			measurements =  GlobalVar.db.getLatestSensorMeasurementsByFR(frID, measurements);
			measurementsList = (ListView) findViewById(R.id.actordetailsmeasurementsListView);
			adapter = new MeasurementsAdapter(ActorDetails.this, measurements);
			measurementsList.setAdapter(adapter);

			alarmsButton = (Button) findViewById(R.id.alarmsButton);
			alarmsButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent goToAlarms = new Intent(ActorDetails.this, Alarms.class);
					goToAlarms.putExtra("frID", frID);
					startActivity(goToAlarms);
				}
			});

			actionButton = (Button) findViewById(R.id.actionsButton);
			actionButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent goToActions = new Intent(ActorDetails.this, Actions.class);
					goToActions.putExtra("frID", frID);
					startActivity(goToActions);
				}
			});

		}



	}

	private void initializeMenu() {

		ListView menuList = null;
		View menuView = null;
		MenuAdapter menuAdapter = null;

		Resources res = getResources();
		String[] menuItems = null;

		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {

			menuView = LayoutInflater.from(ActorDetails.this).inflate(R.layout.menu_frame, null);
			menuList = (ListView) menuView.findViewById(R.id.MenuAsList);
			menuItems = res.getStringArray(R.array.frc_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.FRCMenuClickListener(ActorDetails.this));
			menuAdapter = new MenuAdapter(ActorDetails.this, R.layout.row_sidemenu, menuItems, true);
		}
		else if( ((GlobalVar)getApplicationContext()).getRole() == 2 ) {
			menuList = (ListView) findViewById(R.id.applicationMenu);
			menuItems = res.getStringArray(R.array.ic_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.ICMenuClickListener(ActorDetails.this));
			menuAdapter = new MenuAdapter(ActorDetails.this, R.layout.row_sidemenu, menuItems, false);
		}

		menuList.setAdapter(menuAdapter);

		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {
			setBehindContentView(menuView);
			sm = getSlidingMenu();
			sm.setShadowWidthRes(R.dimen.shadow_width);
			sm.setShadowDrawable(R.drawable.shadow);
			sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
			sm.setFadeDegree(0.35f);
			sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
			setSlidingActionBarEnabled(true);
		}
	}



	@Override
	protected void onDestroy() {
		super.onDestroy();
	}



	@Override
	protected void onPause() {
		if(receiver!=null)
			unregisterReceiver(receiver);
		if(t1!= null){
			t1.cancel();
			t1.purge();
		}
		super.onPause();
	}



	@Override
	protected void onResume() {
		super.onResume();
		receiver = new ActorDetailsReceiver();
		filter = new IntentFilter(GlobalVar.INNER_SENSORS_RECEIVED);
		registerReceiver(receiver, filter);

		//FIXME Provide  better implementation for the automatic periodic update of the list
		if(t1 == null) {
			t1 = new Timer();
			t1.schedule(new TimerTask() {

				@Override
				public void run() {
					ActorDetails.this.sendBroadcast(new Intent(GlobalVar.INNER_SENSORS_RECEIVED));
				}
			}, 2000, 3000);
		}
	}

	class ActorDetailsReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_SENSORS_RECEIVED)) {
				GlobalVar.db.getLatestSensorMeasurementsByFR(frID, measurements);
				adapter.notifyDataSetChanged();
			}
		}

	}


}
