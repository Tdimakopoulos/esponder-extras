package eu.esponder.ESponderFRU.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.animations.MeasCriticalAnimation;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;


public class MainFRActivity extends Activity  {

	MainMapReceiver receiver;

	Handler handler;

	Long cuID;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main_fr);

		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		setTitle("ESponder FR Application");

		handler = new Handler();

		cuID = getSharedPreferences(GlobalVar.spName, MODE_PRIVATE).getLong(GlobalVar.spESponderUserID, -1);
		ESponderLogUtils.error("POIs", "FOUND THE CURRENT USER:"+cuID);

//		if( ((GlobalVar)getApplicationContext()).getRole() == 0 || ((GlobalVar)getApplicationContext()).getRole() == 1 )  {
//
//			if(GlobalVar.pcdSensorTimer == null) {
//				GlobalVar.pcdSensorTimer = new Timer();
//				// The sensor measurements will be updated only when the current user of the app is an FRC		
//				if( ((GlobalVar)getApplicationContext()).getRole() == 0 )
//					GlobalVar.pcdSensorTimer.scheduleAtFixedRate(new GetSensorsPCDTimerTask(MainFRActivity.this.getApplicationContext()), 2000, GetSensorsPCDTimerTask.GET_SENSOR_MEAS_PERIOD);
//
//				// The smartphone statistics will be updated only when the current user of the app is an FRC or FR
//				if( ((GlobalVar)getApplicationContext()).getRole() == 0 || ((GlobalVar)getApplicationContext()).getRole() == 1 )
//					GlobalVar.pcdSensorTimer.scheduleAtFixedRate(new SPInformationTimerTask(MainFRActivity.this.getApplicationContext()), 0, 15000);
//			}
//		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(receiver);
	}


	@Override
	protected void onResume() {
		super.onResume();
		receiver = new MainMapReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_ALARM_RECEIVED);
		registerReceiver(receiver, filter);
	}

	public class MainMapReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context ctx, Intent intent) {
			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_ALARM_RECEIVED)) {

				ESponderLogUtils.error("anim", "anim received");
				final ImageView alarmIcon = (ImageView)findViewById(R.id.mapAlarmIndicator);
				alarmIcon.setVisibility(View.VISIBLE);
				final Animation anim = new MeasCriticalAnimation(0.0f, 1.0f);
				anim.setAnimationListener(new AnimationListener() {

					int animCounter = 0;

					@Override
					public void onAnimationStart(Animation animation) {}

					@Override
					public void onAnimationRepeat(Animation animation) {
						animCounter++;
						if(String.valueOf(animCounter).equalsIgnoreCase(String.valueOf(20))){
							anim.cancel();
							anim.reset();
							animCounter = 0;

							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									alarmIcon.setAnimation(null);
									alarmIcon.setVisibility(View.INVISIBLE);
								}
							});	
						}
					}

					@Override
					public void onAnimationEnd(Animation animation) {}
					
				});
				alarmIcon.startAnimation(anim);

			}



		}

	}

}