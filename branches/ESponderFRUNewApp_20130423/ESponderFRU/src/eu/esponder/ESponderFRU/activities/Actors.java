package eu.esponder.ESponderFRU.activities;

import java.util.Iterator;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.adapters.CrisisMemberAdapter;
import eu.esponder.ESponderFRU.adapters.MenuAdapter;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;
import eu.esponder.ESponderFRU.utils.ESponderUtils;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;

public class Actors extends SlidingActivity {

	private SlidingMenu sm;
	private ListView frListView;
	private CrisisMemberAdapter cmAdapter;
	private ActorFRALTDTO[] frs;
	private Long cuID;
	private ActorsReceiver receiver;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.actorslayout);
		setTitle("ESponder - FR Team Members");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		// This function initializes the sliding menus to full functionality
		initializeMenu();

		// Populates the list of subordinate first responders for the chief
		findFirstRepondersForAdapter();
		
		//	prepares the area that contains the chief's details summary
		frListView = (ListView) findViewById(R.id.CrisisMemberList);
		cmAdapter = new CrisisMemberAdapter(Actors.this, frs);
		frListView.setAdapter(cmAdapter);
	}




	//Create sample data for Meocs, FRCs, FRs
	// We need id of member, Title of member, type of member etc

	private void initializeMenu() {

		ListView menuList = null;
		View menuView = null;
		MenuAdapter menuAdapter = null;

		Resources res = getResources();
		String[] menuItems = null;

		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {

			menuView = LayoutInflater.from(Actors.this).inflate(R.layout.menu_frame, null);
			menuList = (ListView) menuView.findViewById(R.id.MenuAsList);
			menuItems = res.getStringArray(R.array.frc_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.FRCMenuClickListener(Actors.this));
			menuAdapter = new MenuAdapter(Actors.this, R.layout.row_sidemenu, menuItems, true);
		}
		else if( ((GlobalVar)getApplicationContext()).getRole() == 2 ) {
			menuList = (ListView) findViewById(R.id.applicationMenu);
			menuItems = res.getStringArray(R.array.ic_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.ICMenuClickListener(Actors.this));
			menuAdapter = new MenuAdapter(Actors.this, R.layout.row_sidemenu, menuItems, false);
		}
		
		menuList.setAdapter(menuAdapter);
		
		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {
			setBehindContentView(menuView);
			sm = getSlidingMenu();
			sm.setShadowWidthRes(R.dimen.shadow_width);
			sm.setShadowDrawable(R.drawable.shadow);
			sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
			sm.setFadeDegree(0.35f);
			sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
			setSlidingActionBarEnabled(true);
		}
	}


	private void findFirstRepondersForAdapter() {
		
		SharedPreferences prefs = getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
		cuID = prefs.getLong(GlobalVar.spESponderUserID, -1);
		
		if(cuID != -1) {
			List<ActorFRALTDTO> firstResponders = GlobalVar.db.getAllFirstRespondersByChief(cuID);

			if( firstResponders.size() > 0 ) {
				Iterator<ActorFRALTDTO> it = firstResponders.iterator();
				frs = new ActorFRALTDTO[firstResponders.size()];
				int counter = 0;
				while(it.hasNext()) {
					frs[counter] = it.next();
					counter++;
				}
			}
		}
		else
			ESponderLogUtils.error("Populating the subordinates list for the chief", "The chief is unknown");
	}
	
	
	
	
	class ActorsReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_ALARM_RECEIVED)) {
				findFirstRepondersForAdapter();
				cmAdapter.setValues(frs);
				cmAdapter.notifyDataSetChanged();	
			}	
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(receiver!=null)
			unregisterReceiver(receiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(receiver == null)
			receiver = new ActorsReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_ALARM_RECEIVED);
		registerReceiver(receiver, filter);
		cmAdapter.notifyDataSetChanged();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        Intent a = new Intent(this, MainActivity.class);
	        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        startActivity(a);
	        this.finish();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}

}
