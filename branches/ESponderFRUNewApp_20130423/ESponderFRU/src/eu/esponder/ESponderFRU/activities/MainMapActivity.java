package eu.esponder.ESponderFRU.activities;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.nutiteq.MapView;
import com.nutiteq.components.Color;
import com.nutiteq.components.Components;
import com.nutiteq.components.MapPos;
import com.nutiteq.geometry.Marker;
import com.nutiteq.projections.EPSG3857;
import com.nutiteq.projections.Projection;
import com.nutiteq.rasterlayers.TMSMapLayer;
import com.nutiteq.style.MarkerStyle;
import com.nutiteq.ui.DefaultLabel;
import com.nutiteq.ui.Label;
import com.nutiteq.utils.UnscaledBitmapLoader;
import com.nutiteq.vectorlayers.MarkerLayer;

import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.data.DataManager;
import eu.esponder.ESponderFRU.data.Poi;
import eu.esponder.ESponderFRU.data.PoiCategory;
import eu.esponder.ESponderFRU.maphelpers.MapEventListener;
import eu.esponder.ESponderFRU.maphelpers.MyLocationCircle;

public class MainMapActivity extends Activity {

	    private MapView mapView;

	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_main_map);
	        
	        mapView = (MapView) findViewById(R.id.mapView);
	        
	        // Restore map state during device rotation,
	        // it is saved in onRetainNonConfigurationInstance() below
	        Components retainObject = (Components) getLastNonConfigurationInstance();
	        if (retainObject != null) {
	            // just restore configuration and update listener, skip other initializations
	          	mapView.setComponents(retainObject);
	          	MapEventListener mapListener = (MapEventListener) mapView.getOptions().getMapListener();
	          	mapListener.reset(this, mapView);
	            mapView.startMapping();
	            return;
	        } else {
	            // Create and set MapView components 
	            mapView.setComponents(new Components());
	        }

	        DataManager.initInstance(this);
	        
	        // Initialize the map
	        String MapServerURL = DataManager.getInstance().getMapServerURL();
	        // Define base layer. Here we use MapQuest open tiles which are free to use
	        // Almost all online maps use EPSG3857 projection.
	        TMSMapLayer mapLayer = new TMSMapLayer(new EPSG3857(), 0, 18, 0,
	        		MapServerURL, "/", ".png");
	        mapView.getLayers().setBaseLayer(mapLayer);
	   
	        // set initial map view camera - optional. "World view" is default 
	        // NB! it must be in base layer projection (EPSG3857), so we convert it from lat and long
	        mapView.setFocusPoint(mapView.getLayers().getBaseLayer().getProjection().fromWgs84(DataManager.getInstance().getInitialCenterLatitude(), DataManager.getInstance().getInitialCenterLongitude()));
	        // zoom - 0 = world, like on most web maps
	        mapView.setZoom((float)DataManager.getInstance().getInitialMapZoom());
	        // tilt means perspective view. Default is 90 degrees for "normal" 2D map view, minimum allowed is 30 degrees.
	        mapView.setTilt(90.0f);
	        
	        // Activate some mapview options to make it smoother 
	        mapView.getOptions().setPreloading(true);
	        mapView.getOptions().setSeamlessHorizontalPan(true);
	        mapView.getOptions().setTileFading(true);
	        mapView.getOptions().setKineticPanning(true);
	        mapView.getOptions().setDoubleClickZoomIn(true);
	        mapView.getOptions().setDualClickZoomOut(true);
	        
	        // configure texture caching - optional, suggested 
	        mapView.getOptions().setTextureMemoryCacheSize(40 * 1024 * 1024);
	        mapView.getOptions().setCompressedMemoryCacheSize(8 * 1024 * 1024);
	        
	        // define online map persistent caching - optional, suggested. Default - no caching
	        mapView.getOptions().setPersistentCachePath(this.getDatabasePath("mapcache").getPath());
	        // set persistent raster cache limit to 100MB
	        mapView.getOptions().setPersistentCacheSize(100 * 1024 * 1024);

	        // Create and set the layers
	        createMapPOILayers(mapLayer);
	        
            // add event listener
            MapEventListener mapListener = new MapEventListener(this, mapView);
            mapView.getOptions().setMapListener(mapListener);
       
            // **************************************
            //TODO UNCOMMENT THE NEXT 3 LINES TO SHOW GPS LOCATION             
//            MyLocationCircle locationCircle = new MyLocationCircle();
//            mapListener.setLocationCircle(locationCircle);
//            initGps(locationCircle);
            
	        // start mapping
	        mapView.startMapping();       
	        
	    }

	    
	    // For every POI category, one layer will be created. The POIs will then be added to the layer 
	    // and the layer will be added ArrayList<E>p
	    public void createMapPOILayers(TMSMapLayer mapLayer){
        	ArrayList<PoiCategory> categories = DataManager.getInstance().getCategories();
        	
        	for(int i = 0; i < categories.size(); i++){
        		ArrayList<Poi> pois = DataManager.getInstance().getPoisForCategory(categories.get(i).getCategoryCode());
        		
        		if(pois.size() > 0){
        			// Create the Layer
        			 MarkerLayer markerLayer = new MarkerLayer(mapLayer.getProjection());
        			 // Load POI bitmap
        			 AssetManager assets = getResources().getAssets();
        			 InputStream buf;
        			 Bitmap pointMarker = null;
					try {
						String imagePath = new String(categories.get(i).getMarkerBitmap());
						buf = new BufferedInputStream((assets.open(new String(imagePath))));
						pointMarker = BitmapFactory.decodeStream(buf);
					} catch (IOException e) {
						e.printStackTrace();
					}
        			 
        			 for(int j = 0; j < pois.size(); j++){
        				 Poi poi = pois.get(j);
        				 
        				 //POI Bitmap
        				 MarkerStyle markerStyle;
        				 // If something went wrong (e.g. bitmap missing) it defaults to the black marker
        				 if(pointMarker != null){
        					 markerStyle = MarkerStyle.builder().setBitmap(pointMarker).setSize(0.5f).setColor(Color.WHITE).build();
        				 }
        				 else{
        					 pointMarker = UnscaledBitmapLoader.decodeResource(getResources(), R.drawable.marker_black);
        				     markerStyle = MarkerStyle.builder().setBitmap(pointMarker).setSize(0.5f).setColor(Color.WHITE).build();
        				 }
        				 
        		         // POI Text
        		         Label markerLabel = new DefaultLabel(poi.getLabelTitle(), poi.getLabelText());
        		         
        		         // POI Location
        		         MapPos markerLocation = mapLayer.getProjection().fromWgs84(poi.getLongitude(), poi.getLatitude());
        		         
        		         // Add it to the marker layer
        		         markerLayer.add(new Marker(markerLocation, markerLabel, markerStyle, null));
        			 }        			 
        			 mapView.getLayers().addLayer(markerLayer);
        		}
        			
        	}
        }
        
	    protected void initGps(final MyLocationCircle locationCircle) {
	        final Projection proj = mapView.getLayers().getBaseLayer().getProjection();
	        
	        LocationListener locationListener = new LocationListener() 
	        {
	            public void onLocationChanged(Location location) {
	                 if (locationCircle != null) {
	                     locationCircle.setLocation(proj, location);
	                     locationCircle.setVisible(true);
	                 }
	            }

	            public void onStatusChanged(String provider, int status, Bundle extras) {}

	            public void onProviderEnabled(String provider) {}

	            public void onProviderDisabled(String provider) {}
	        };
	        
	        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);	        
	        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 100, locationListener);
	        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
	    }
		
		
}
