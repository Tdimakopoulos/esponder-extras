package eu.esponder.ESponderFRU.activities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;

public class IntentCheater extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);







	}


	/*
	 * REMINDER:
	 * bbIp -->  the beagle board of the frc with with ports 3000 and 1337 for the different services
	 * 
	 * sipServerIP -->  the voip server address
	 * 
	 * extranodeIP --> the extra node IP
	 * 
	 * sipMEOCIP -- > supposedly the MEOC voip server IP?
	 * 
	 * 
	 * For the case of the chief the extra node and the beagle board are the same device
	 * 
	 */


	private void IntentCheater(Context context) {

		// COMMENT THE NEXT LINE 
		String key = retrievePKIKeyFromFile();

		//FRC case
		if(key.equalsIgnoreCase("ihzo)j)3:*%>*!*#4d4uj{e|*c*g&p$*ag!|&a<^oz8aqpg0*%(4lh&*|" +
				"{954&tif70<v$3()17l(6c&z#@oloy2)gv*sxs}1}z4")) {


			Intent sipIntent = new Intent(GlobalVar.SIP_LOGIN);
			Bundle sipBundle = new Bundle();
			sipBundle.putString("bbIP", "");
			sipBundle.putString("sipServerIP", "");
			sipBundle.putString("extranodeIP", "");
			sipBundle.putString("username", "");
			sipBundle.putString("password", "");
			sipBundle.putString("sipMEOCIP", "");

			sipIntent.putExtra("sipBundle", sipBundle);
			context.sendBroadcast(sipIntent);

		}
		//FR1 case
		else if(key.equalsIgnoreCase("bb<o&(r^b?24pj}&1s(mo)p*^*ehrkxrn%dz:h!p:df&1}v&r!w" +
				":qav:#bw&t@&<>w3<6abp7&6@(g^7e{&s^yj@xk(*{5@>jz@@")) {

			Intent sipIntent = new Intent(GlobalVar.SIP_LOGIN);
			Bundle sipBundle = new Bundle();
			sipBundle.putString("bbIP", "");
			sipBundle.putString("sipServerIP", "");
			sipBundle.putString("extranodeIP", "");
			sipBundle.putString("username", "");
			sipBundle.putString("password", "");
			sipBundle.putString("sipMEOCIP", "");

			sipIntent.putExtra("sipBundle", sipBundle);
			context.sendBroadcast(sipIntent);

		}
		//FR2 case
		else if(key.equalsIgnoreCase("|3*r89^uojo<r%q4|&d3#%@(3>z^rpg(|3(jkv|>" +
				"rm0l<>*&p80b1@>e!t}&omdlgk*s4>?&^oz>&e0>f1g|c(r{i&<&afki(*!k")) {

			Intent sipIntent = new Intent(GlobalVar.SIP_LOGIN);
			Bundle sipBundle = new Bundle();
			sipBundle.putString("bbIP", "");
			sipBundle.putString("sipServerIP", "");
			sipBundle.putString("extranodeIP", "");
			sipBundle.putString("username", "");
			sipBundle.putString("password", "");
			sipBundle.putString("sipMEOCIP", "");

			sipIntent.putExtra("sipBundle", sipBundle);
			context.sendBroadcast(sipIntent);

		}
		//FR3 case
		else if(key.equalsIgnoreCase("8{u9e<&puo1g&{>^>rn>e8k^3c}3wq&h2r>)2:*" +
				"ikmv}3##o:o<|x@9@i*xa7bxs$5bs7z4?*@8m9cq8(7<om9&?dp:&fe@1p7{l")) {

			Intent sipIntent = new Intent(GlobalVar.SIP_LOGIN);
			Bundle sipBundle = new Bundle();
			sipBundle.putString("bbIP", "");
			sipBundle.putString("sipServerIP", "");
			sipBundle.putString("extranodeIP", "");
			sipBundle.putString("username", "");
			sipBundle.putString("password", "");
			sipBundle.putString("sipMEOCIP", "");

			sipIntent.putExtra("sipBundle", sipBundle);
			context.sendBroadcast(sipIntent);

		}
		//FR4 case
		else if(key.equalsIgnoreCase("(u(l&?6$ji6je@xv^f(!@p&%d(y|0|7r&l:>8l>*%}" +
				"8ysxp&olm1t&(k3ux%y}&%mmyfk%1mop*9x}<mm^p{{717>6p63c(7")) {

			Intent sipIntent = new Intent(GlobalVar.SIP_LOGIN);
			Bundle sipBundle = new Bundle();
			sipBundle.putString("bbIP", "");
			sipBundle.putString("sipServerIP", "");
			sipBundle.putString("extranodeIP", "");
			sipBundle.putString("username", "");
			sipBundle.putString("password", "");
			sipBundle.putString("sipMEOCIP", "");

			sipIntent.putExtra("sipBundle", sipBundle);
			context.sendBroadcast(sipIntent);

		}
	}






	private String retrievePKIKeyFromFile() {

		String filename = "pkikey.txt";
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,filename);
		//Read text from file
		StringBuilder sbuilder = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				sbuilder.append(line);
			}
			br.close();
		}
		catch (IOException e) {
			Log.d("FILE_HANDLER", "Error occurred while opening file");
			e.printStackTrace();
		}
		String skey = sbuilder.toString();
		ESponderLogUtils.error("PKI",skey);

		return skey;
	}



}
