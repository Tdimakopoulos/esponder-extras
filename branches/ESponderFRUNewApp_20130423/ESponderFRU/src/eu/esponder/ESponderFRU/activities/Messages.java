package eu.esponder.ESponderFRU.activities;

import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.adapters.MenuAdapter;
import eu.esponder.ESponderFRU.adapters.MessagesAdapter;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.data.FRMessageEntity;
import eu.esponder.ESponderFRU.utils.ESponderUtils;

public class Messages extends SlidingActivity {

	SlidingMenu sm;
	ListView messageListView;
	MessagesAdapter messageAdapter;
	FRMessageEntity[] messages;
	MessageReceiver receiver;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.messageslayout);
		setTitle("ESponder - Messages");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		// This function initializes the sliding menus to full functionality
		initializeMenu();

		//		TextView alarmsHeader = (TextView) findViewById(R.id.AlarmsHeader);
		//		alarmsHeader.setText("Alarms");

		findFRMessagesForAdapter();
		messageListView = (ListView) findViewById(R.id.messageList);
		messageAdapter = new MessagesAdapter(Messages.this, messages);
		messageListView.setAdapter(messageAdapter);

	}

	private void initializeMenu() {

		ListView menuList = null;
		View menuView = null;
		MenuAdapter menuAdapter = null;

		Resources res = getResources();
		String[] menuItems = null;

		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {

			menuView = LayoutInflater.from(Messages.this).inflate(R.layout.menu_frame, null);
			menuList = (ListView) menuView.findViewById(R.id.MenuAsList);
			menuItems = res.getStringArray(R.array.frc_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.FRCMenuClickListener(Messages.this));
			menuAdapter = new MenuAdapter(Messages.this, R.layout.row_sidemenu, menuItems, true);
		}
		else if( ((GlobalVar)getApplicationContext()).getRole() == 2 ) {
			menuList = (ListView) findViewById(R.id.applicationMenu);
			menuItems = res.getStringArray(R.array.ic_menu_items);
			menuList.setOnItemClickListener(new ESponderUtils.ICMenuClickListener(Messages.this));
			menuAdapter = new MenuAdapter(Messages.this, R.layout.row_sidemenu, menuItems, false);
		}
		
		menuList.setAdapter(menuAdapter);
		
		if( ((GlobalVar)getApplicationContext()).getRole() == 0 ) {
			setBehindContentView(menuView);
			sm = getSlidingMenu();
			sm.setShadowWidthRes(R.dimen.shadow_width);
			sm.setShadowDrawable(R.drawable.shadow);
			sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
			sm.setFadeDegree(0.35f);
			sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
			setSlidingActionBarEnabled(true);
		}
	}


	private void findFRMessagesForAdapter() {

		List<FRMessageEntity> messageList = GlobalVar.db.getAllMessages();
		if( messageList.size() > 0 ) {
			Iterator<FRMessageEntity> it = messageList.iterator();
			messages = new FRMessageEntity[messageList.size()];
			int counter = 0;
			while(it.hasNext()) {
				messages[counter] = it.next();
				counter++;
			}
		}
		else
			messages = new FRMessageEntity[1];
	}


	class MessageReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_MESSAGE_RECEIVED)) {
				findFRMessagesForAdapter();
				messageAdapter.setValues(messages);
				messageAdapter.notifyDataSetChanged();
			}	
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(receiver!=null)
			unregisterReceiver(receiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(receiver == null)
			receiver = new MessageReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_MESSAGE_RECEIVED);
		registerReceiver(receiver, filter);
		findFRMessagesForAdapter();
		if(messages != null) {
			messageAdapter.setValues(messages);
			messageAdapter.notifyDataSetChanged();
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        Intent a = new Intent(this, MainActivity.class);
	        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        startActivity(a);
	        this.finish();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	


}