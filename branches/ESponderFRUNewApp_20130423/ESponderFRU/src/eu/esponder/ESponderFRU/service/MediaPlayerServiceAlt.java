package eu.esponder.ESponderFRU.service;


import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.IBinder;
import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;

public class MediaPlayerServiceAlt extends Service implements OnCompletionListener {
	MediaPlayer mediaPlayer;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		mediaPlayer = new MediaPlayer();
		mediaPlayer.setVolume(1f, 1f);
		mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
		
		try {
			getResources().openRawResource(R.raw.alarm);
			Uri uri = Uri.parse("android.resource://"+getPackageName()+"/" + R.raw.alarm);
			ESponderLogUtils.error("MediaPlayerService Uri", uri.toString());
			mediaPlayer.setDataSource(getApplicationContext(),uri);

			mediaPlayer.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp.start();
				}
			});
			mediaPlayer.setOnCompletionListener(this);

			mediaPlayer.prepareAsync();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return START_STICKY;
	}

	public void onDestroy() {
		if (mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
		}
		mediaPlayer.release();
	}

	public void onCompletion(MediaPlayer _mediaPlayer) {
		stopSelf();
	}

}