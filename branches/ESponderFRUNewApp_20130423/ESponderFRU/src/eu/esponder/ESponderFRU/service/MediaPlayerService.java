package eu.esponder.ESponderFRU.service;


import java.io.IOException;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.IBinder;
import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;

public class MediaPlayerService extends Service implements OnCompletionListener, AudioManager.OnAudioFocusChangeListener {

	MediaPlayer mediaPlayer;
	AudioManager am;
	
	static int mediaType = AudioManager.STREAM_NOTIFICATION;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {

		mediaPlayer = new MediaPlayer();
		mediaPlayer.setAudioStreamType(mediaType);
		am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		ESponderLogUtils.error("STREAM MAX VOLUME", String.valueOf(am.getStreamMaxVolume(mediaType)));
		
		try {
			Uri uri = Uri.parse("android.resource://"+getPackageName()+"/" + R.raw.alarm);
			mediaPlayer.setDataSource(getApplicationContext(),uri);
			mediaPlayer.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					ESponderLogUtils.error("MEDIAPLAYERSERVICE", "GOING FOR PLAYER START");
					mediaPlayer.start();		
				}
			});
			
			mediaPlayer.setOnErrorListener(new OnErrorListener() {
				
				@Override
				public boolean onError(MediaPlayer mp, int what, int extra) {
					ESponderLogUtils.error("MEDIA ERROR OCCURED", "ERROR");
					return false;
				}
			});

			mediaPlayer.setOnCompletionListener(MediaPlayerService.this);

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		
		int focusChange = am.requestAudioFocus(this, mediaType, AudioManager.AUDIOFOCUS_GAIN);
		if(focusChange == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
			ESponderLogUtils.error("AUDIOFOCUS", "AUDIOFOCUS_REQUEST_GRANTED, going for play");
			
			am.setStreamSolo(mediaType, false);
			am.setStreamVolume(mediaType, am.getStreamMaxVolume(mediaType), AudioManager.FLAG_PLAY_SOUND);
			mediaPlayer.prepareAsync();
		}

		return Service.START_STICKY;
	}

	public void onDestroy() {
		if (mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
		}
		mediaPlayer.release();
		ESponderLogUtils.error("MEDIAPLAYERSERVICE", "ONDESTROY");
	}

	public void onCompletion(MediaPlayer _mediaPlayer) {
		am.abandonAudioFocus(this);
		stopSelf();
	}

	@Override
	public void onAudioFocusChange(int focusChange) {
		switch(focusChange) {

		case AudioManager.AUDIOFOCUS_GAIN:
			ESponderLogUtils.error("AUDIOFOCUS_LISTENER", "AUDIOFOCUS_GAIN");
			break;

		case AudioManager.AUDIOFOCUS_REQUEST_FAILED:
			ESponderLogUtils.error("AUDIOFOCUS_LISTENER", "AUDIOFOCUS_REQUEST_FAILED");
			break;

		case AudioManager.AUDIOFOCUS_LOSS:
			ESponderLogUtils.error("AUDIOFOCUS_LISTENER", "AUDIOFOCUS_LOSS");
			break;

		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
			ESponderLogUtils.error("AUDIOFOCUS_LISTENER", "AUDIOFOCUS_LOSS_TRANSIENT");
			break;

		case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK:
			ESponderLogUtils.error("AUDIOFOCUS_LISTENER", "AUDIOFOCUS_LOSS_TRANSIENT_MAY_DUCK");
			break;

		}


	}



}