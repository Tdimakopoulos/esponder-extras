package eu.esponder.ESponderFRU.runnables;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.enums.SensorStatTypeEnum;
import eu.esponder.ESponderFRU.enums.SensorTypeEnum;
import eu.esponder.ESponderFRU.enums.SensorUnitEnum;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;

public class SensorUDPServer extends Thread {

	private static String TAG = "Sensor UDP receiver";
	private static int timeOut = 5000;
	private final int MAX_UDP_DATAGRAM_LEN = 2000;
	private int udpPort;
	private boolean enabled = true;
	private String lastMessage = "";
	private DatagramSocket socket;
	Context context;

	public SensorUDPServer(Context context, int udpPort) {
		this.context = context;
		this.udpPort = udpPort;
	}

	class MeasurementProcessor implements Runnable {

		@Override
		public void run() {
			processLatestMessage(lastMessage);		
		}
	}

	@Override
	public void run() {
		String message;
		byte[] lmessage = new byte[MAX_UDP_DATAGRAM_LEN];
		ESponderLogUtils.error(TAG, udpPort+": Running server thread");
		DatagramPacket packet = new DatagramPacket(lmessage, lmessage.length);
		try {
			socket = new DatagramSocket(udpPort);
			socket.setSoTimeout(timeOut);
			while(enabled) {
				// This inner try/catch block is used because the receive method of the socket
				// is blocking until it receives a packet, unless a timeout is set on the socket.
				// Therefore, this block is used to catch the interrupted exception of the timeout
				//and protect the udp socket thread from dying prematurely.
				try {
					ESponderLogUtils.info(TAG, udpPort+": Waiting to receive");
					socket.receive(packet);	// It blocks until the timeout ends
					message = new String(lmessage, 0, packet.getLength());
					lastMessage = message;
					//				ESponderLogUtils.error(TAG, lastMessage);
					new Thread(new MeasurementProcessor()).start();	
				} catch (Throwable e) {
					
				}
			}
		} catch (Throwable e) {
			ESponderLogUtils.error(TAG, udpPort+": exception has occured in the UDP thread");
		}

		if (socket != null)
			if(!socket.isClosed()) {
				socket.close();
				ESponderLogUtils.error(TAG, udpPort+": was found open and got closed");
			}
			else
				ESponderLogUtils.error(TAG, udpPort+": was found closed. Attempt aborted");
		else
			ESponderLogUtils.error(TAG, udpPort+": was found to be null");
	}


	private void processLatestMessage(String msg) {
		if(msg!=null && !msg.equalsIgnoreCase(""))
			try {
				if(msg.contains("a")) {
					msg = msg.replace("a", "");
					List<String> sRecord = Arrays.asList(msg.split(","));
					ESponderLogUtils.error(TAG, udpPort+": "+sRecord.get(0)+"/"+sRecord.get(1)+"/"+sRecord.get(2));
					GlobalVar.db.insertOrUpdateSensorMeasurement(Integer.valueOf(sRecord.get(0)), Integer.valueOf(sRecord.get(1)), SensorStatTypeEnum.MEAN.getStatType(),
							getSensorUnit(sRecord.get(1)), Double.valueOf(sRecord.get(2)));
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
	}

	public void kill() { 
		enabled = false;
		return;
	}

	public String getLastMessage() {
		return lastMessage;
	}


	private String getSensorUnit(String incUnit) {

		if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.ActivitySensor.getType())))
			return SensorUnitEnum.ActivitySensor.getUnit();

		else if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.BodyTemperatureSensor.getType())))
			return SensorUnitEnum.BodyTemperatureSensor.getUnit(); 

		else if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.BreathRateSensor.getType())))
			return SensorUnitEnum.BreathRateSensor.getUnit(); 

		else if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.EnvTemperatureSensor.getType())))
			return SensorUnitEnum.EnvTemperatureSensor.getUnit();

		else if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.MethaneSensor.getType())))
			return SensorUnitEnum.MethaneSensor.getUnit();

		else if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.CarbonMonoxideSensor.getType())))
			return SensorUnitEnum.CarbonMonoxideSensor.getUnit();

		else if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.CarbonDioxideSensor.getType())))
			SensorUnitEnum.CarbonDioxideSensor.getUnit();

		else if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.OxygenSensor.getType())))
			return SensorUnitEnum.OxygenSensor.getUnit(); 

		else if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.HydrogenSulfideSensor.getType())))
			return SensorUnitEnum.HydrogenSulfideSensor.getUnit();

		else if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.HeartBeatRateSensor.getType())))
			return SensorUnitEnum.HeartBeatRateSensor.getUnit();

		else if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.LocationSensor.getType())))
			return SensorUnitEnum.LocationSensor.getUnit();

		else if(incUnit.equalsIgnoreCase(String.valueOf(SensorTypeEnum.LPSSensor.getType())))
			return SensorUnitEnum.LPSSensor.getUnit();

		return "";
	}

}