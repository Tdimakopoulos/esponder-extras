package eu.esponder.ESponderFRU.runnables;

import android.media.MediaPlayer;
import android.media.Ringtone;
import eu.esponder.ESponderFRU.application.GlobalVar;

public class StopSoundReproduction implements Runnable {
	
	Ringtone ringtone;
	boolean alarm;
	MediaPlayer player;
	
	public StopSoundReproduction(MediaPlayer player,boolean alarm) {
		this.ringtone = ringtone;
		this.player = player;
		this.alarm = alarm;
	}

	@Override
	public void run() {
		try {
			if(alarm)
				Thread.sleep(GlobalVar.alarmDuration);
			else
				Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		player.stop();
		player.release();
		
	}
	
	

}
