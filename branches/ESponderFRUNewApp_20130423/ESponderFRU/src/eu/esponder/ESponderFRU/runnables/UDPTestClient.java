package eu.esponder.ESponderFRU.runnables;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UDPTestClient extends Thread {
	
	@Override
	public void run() {
		super.run();
		runUdpClient();
	}



	private void runUdpClient()  {

		DatagramSocket ds = null;
		try {
			ds = new DatagramSocket();
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		if(ds != null) {
			try {
				int counter = 15;
				
				
				while(counter>0) {
					InetAddress serverAddr = InetAddress.getByName("127.0.0.1");

					DatagramPacket dp;
					
					String udpMsg = "6,1,"+rand(10, 40)+"a";

					dp = new DatagramPacket(udpMsg.getBytes(), udpMsg.length(), serverAddr, 25001);

					ds.send(dp);
					
					counter--;
					
					Thread.sleep(3000);
				}

			} catch (UnknownHostException e) {

				e.printStackTrace();

			} catch (IOException e) {

				e.printStackTrace();

			} catch (Exception e) {

				e.printStackTrace();

			} finally {

				if (ds != null) {

					ds.close();

				}

			}


		}


	}
	
	
	private int rand(int min, int max) {
		return min + (int)(Math.random() * ((max - min) + 1));
	}
	
	
	
	
}

