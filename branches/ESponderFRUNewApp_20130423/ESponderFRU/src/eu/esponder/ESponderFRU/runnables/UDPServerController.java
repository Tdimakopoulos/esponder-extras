package eu.esponder.ESponderFRU.runnables;

import android.content.Context;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;

public class UDPServerController extends Thread {
	
	
	static int[] srvPorts = {25001, 25002, 25003, 25004, 25005, 25006};
	
	static SensorUDPServer[] servers = new SensorUDPServer[6];
	
	private Context context;

	public UDPServerController(Context context) {
		this.setContext(context);		
	}
	
	public void run() {
		for(int i=0; i<servers.length;i++) {
			// Server instances are created
			servers[i] = new SensorUDPServer(context, srvPorts[i]);
			// Server instances are being started
			servers[i].start();
		}
	}
	
	static public void terminateServerInstances(){
		ESponderLogUtils.error("", "Going for termination of the UDP threads");
		for(SensorUDPServer server : servers)
			if(server!=null)
				server.kill();
			else
				ESponderLogUtils.error("", "Server thread was found to be null");
	}
	
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

}
