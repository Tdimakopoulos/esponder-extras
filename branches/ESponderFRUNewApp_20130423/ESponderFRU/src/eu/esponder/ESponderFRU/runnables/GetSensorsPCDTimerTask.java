package eu.esponder.ESponderFRU.runnables;

import java.util.TimerTask;

import android.content.Context;
import android.content.SharedPreferences;
import eu.ESponderFRU.soapservices.ESponderRestClient;
import eu.ESponderFRU.soapservices.ESponderRestClient.RequestMethod;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;

public class GetSensorsPCDTimerTask extends TimerTask {
	
	Context context;
	
	public static long GET_SENSOR_MEAS_PERIOD = 10000;
	
	public GetSensorsPCDTimerTask(Context context) {
	this.context = context;
	}

	@Override
	public void run() {
		
		ESponderLogUtils.error("GetSensors Task","GetSensor Task is starting");
		SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
		String rasIP = prefs.getString(GlobalVar.spESponderUserRasIP, "");
		if(!rasIP.equalsIgnoreCase("")) {
			String url = "http://"+rasIP+"/avgfrteam";
			ESponderRestClient client = new ESponderRestClient(context, url);
			try {
				client.Execute(RequestMethod.GET);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}