package eu.esponder.ESponderFRU.dialogs;


import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnShowListener;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Window;
import eu.esponder.ESponderFRU.R;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.utils.ESponderUtils;

public class NotificationsDialog extends Activity {
	AlertDialog alert;
	AlertDialog.Builder alertd;
	String notificationOrigin, notificationMessage;
	Long notificationID;
	Timer t;
	MediaPlayer player;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		alertd = new AlertDialog.Builder(NotificationsDialog.this);
		alertd.setIcon(R.drawable.warning);

		t = new Timer();
		
		player = MediaPlayer.create(NotificationsDialog.this, R.raw.notification);

		String incomingAction = getIntent().getAction();
		if(incomingAction.equalsIgnoreCase(GlobalVar.NOTIFICATION_POPUP)) {

			Bundle extras = getIntent().getExtras();
			notificationOrigin = extras.getString("origin");
			notificationMessage = extras.getString("message");
			notificationID = extras.getLong("messageID");

			alertd.setTitle("Notification from "+notificationOrigin);
			alertd.setMessage(notificationMessage);

			alertd.setPositiveButton("Accept", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					GlobalVar.db.acknowledgeMessage(notificationID);
					t.cancel();
					NotificationsDialog.this.finish();
				}
			});

			alertd.setNegativeButton("Cancel", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					t.cancel();
					NotificationsDialog.this.finish();
				}
			});

			alert = alertd.create();

			alert.setOnShowListener(new OnShowListener() {

				@Override
				public void onShow(DialogInterface dialog) {
					ESponderUtils.vibrateToPattern(NotificationsDialog.this);
					player.start();
					t.schedule(new TimerTask() {
						public void run() {
							alert.dismiss();
							overridePendingTransition(0, 0);
							t.cancel();
							NotificationsDialog.this.finish();
						}
					}, 5000);
				}
			});

			alert.show();
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		t.cancel();
		if(alert !=null) {
			if(alert.isShowing())
				alert.dismiss();
		}
		NotificationsDialog.this.finish();
	}

	@Override
	public void onDestroy() {
		player.stop();
		player.release();
		super.onDestroy();
	}

}