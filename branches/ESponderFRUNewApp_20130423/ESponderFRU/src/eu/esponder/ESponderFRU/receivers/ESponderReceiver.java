/**
 * 
 */
package eu.esponder.ESponderFRU.receivers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.telephony.TelephonyManager;
import eu.ESponderFRU.soapservices.ESponderRestClient;
import eu.ESponderFRU.soapservices.ESponderRestClient.RequestMethod;
import eu.esponder.ESponderFRU.application.GlobalVar;
import eu.esponder.ESponderFRU.dialogs.NotificationsDialog;
import eu.esponder.ESponderFRU.gps.ESponderLocationListener;
import eu.esponder.ESponderFRU.service.DownloaderService;
import eu.esponder.ESponderFRU.service.MediaPlayerService;
import eu.esponder.ESponderFRU.utils.ESponderLogUtils;
import eu.esponder.ESponderFRU.utils.ESponderUtils;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionStageEnumDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorICALTDTO;
import eu.esponder.dto.model.crisis.alternative.FRTeamALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCEocALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.datafusion.DatafusionResultsDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.event.datafusion.query.ESponderDFQueryResponseEvent;
import eu.esponder.event.mobile.MobileFileNotifierEvent;
import eu.esponder.event.mobile.MobileMessageEvent;
import eu.esponder.event.model.crisis.action.NewActionEvent;
import eu.esponder.event.model.crisis.action.UpdateActionEvent;
import eu.esponder.event.model.datafusion.DatafusionResultsGeneratedEvent;
import eu.esponder.event.model.login.LoginEsponderUserEvent;

/**
 * @author dkar
 *
 */
public class ESponderReceiver extends BroadcastReceiver {

	protected boolean mapInitialized = false;
	private ObjectMapper mapper;

	/* (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		// All incoming and outgoing intents have the same category attached
		if(intent.hasCategory(GlobalVar.INTENT_CATEGORY)) {

			// Login response intent
			if(intent.getAction().equalsIgnoreCase(GlobalVar.LOGIN_RESPONSE_INTENT_ACTION)) {
				if(!((GlobalVar)context.getApplicationContext()).isLoggedIn()) {
					String jsonObj = intent.getStringExtra("event");
					ESponderLogUtils.error("LOGIN", jsonObj);
					processLoginResponse(context, jsonObj);
				}
				else
					ESponderLogUtils.error("LOG IN", "USER IS ALREADY LOGGED IN");

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.MAP_RESPONSE_HIERARCHY)) {

				// Checks if user has already passsed the login process, as well if he is a FRC or IC
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn() && 
						(((GlobalVar)context.getApplicationContext()).getRole()==0 || ((GlobalVar)context.getApplicationContext()).getRole()==2)) {
					String jsonObj = intent.getStringExtra("event");
					createThreadForMapUpdate(context, jsonObj);
				}
				else
					ESponderLogUtils.error("CANNOT UPDATE MAP", "USER NOT LOGGED IN OR IS FR");

			}

			//FIXME The sensor reception has been removed from here because everything will be processed on the beagle board

			//			else if(intent.getAction().equalsIgnoreCase(GlobalVar.BIOMED_SENSOR_MEASUREMENT)) {
			//
			//				//Checks if the user is logged in, as well as if he is a FRC or FR
			//				if(((GlobalVar)context.getApplicationContext()).isLoggedIn()&& 
			//						(((GlobalVar)context.getApplicationContext()).getRole()==0 || ((GlobalVar)context.getApplicationContext()).getRole()==1)) {
			//					Bundle b = intent.getBundleExtra("SensorMeasurementBundle");
			//					if(b !=null) {
			//						String sensorType = b.getString("sensorType");
			//						Long timeStamp = b.getLong("timeStamp");
			//						//The location sensor intent contains different extras than the intent of the other sensors
			//						if(sensorType.equalsIgnoreCase(LocationSensorDTO.class.getCanonicalName())) {
			//
			//							ESponderLogUtils.error("LPS", String.valueOf(b.getDouble("latitude"))+" / "+String.valueOf(b.getDouble("longitude"))+" / "+String.valueOf(b.getDouble("altitude")) );
			//							processLPSMeasurement(context, b.getDouble("latitude"), b.getDouble("longitude"), b.getDouble("altitude"), timeStamp);
			//
			//						}
			//						else {
			//							//The rest of the sensors
			//							Double sensorValue = b.getDouble("sensorValue");
			//							processMeasurement(context, sensorType, sensorValue, timeStamp);
			//						}
			//					}
			//				}
			//				else
			//					ESponderLogUtils.error("Sensor-MEASUREMENT PROCESS", "NOT WORKING, APPEARS AS NOT LOGGED IN");
			//			}
			
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INCOMING_ALARM)) {
				//Checks if the user is logged in as well as if he is a FRC or FR
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn()&& 
						(((GlobalVar)context.getApplicationContext()).getRole()==0 || ((GlobalVar)context.getApplicationContext()).getRole()==1)) {
					if(intent.hasExtra("event")) {
						processAlarmRelatedEvents(context, intent.getStringExtra("event"));
					}
					else
						ESponderLogUtils.error("RECEIVER","NO ATTACHMENT FOR ALARMS");
				}
				else
					ESponderLogUtils.error("INCOMING ALARM", "NOT WORKING, APPEARS AS NOT LOGGED IN");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INCOMING_MESSAGE)) {
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn() && ((GlobalVar)context.getApplicationContext()).getRole()==0) {
					if(intent.hasExtra("event")) {
						processIncomingMessageEvents(context, intent.getStringExtra("event"));
					}
					else
						ESponderLogUtils.error("RECEIVER","NO ATTACHMENT FOR NOTIFICATION");
				}
				else
					ESponderLogUtils.error("INCOMING MESSAGE", "NOT WORKING, APPEARS AS NOT LOGGED IN");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INCOMING_FILE)) {
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn() && ((GlobalVar)context.getApplicationContext()).getRole()==0) {
					if(intent.hasExtra("event")) {
						ESponderLogUtils.error("RECEIVER","FILE NOTIFICATION RECEIVED");
						processIncomingFileEvents(context, intent.getStringExtra("event"));
					}
					else
						ESponderLogUtils.error("RECEIVER","NO ATTACHMENT FOR INCOMING FILE");
				}
				else
					ESponderLogUtils.error("INCOMING FILE", "NOT WORKING, APPEARS AS NOT LOGGED IN");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.NEW_ACTION_EVENT) ) {
				//FIXME
				//				if(((GlobalVar)context.getApplicationContext()).isLoggedIn() && 
				//						(((GlobalVar)context.getApplicationContext()).getRole() == 0 ||
				//						((GlobalVar)context.getApplicationContext()).getRole() == 2)) {
				ESponderLogUtils.error("NEW ACTION EVENT BEFORE CHECK", "RECEIVED");
				if(intent.hasExtra("event")) {
					ESponderLogUtils.error("NEW ACTION EVENT", "RECEIVED");
					processNewIncomingActionEvent(context, intent.getStringExtra("event"));
				}
				//				}
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.APO_CANCELLED) ) {
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn() && 
						(((GlobalVar)context.getApplicationContext()).getRole() == 0 || ((GlobalVar)context.getApplicationContext()).getRole() == 2)) {
					if(intent.hasExtra("event"))
						processIncomingCancelActionEvent(context, intent.getStringExtra("event"));
				}
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.UI)) {

				//FIXME Test alarms with Kostas
				/*int mode = intent.getIntExtra("mode", -1);
				if( !String.valueOf(mode).equalsIgnoreCase(String.valueOf(-1)))
					MobileUIAlarmEventSender(1, context, mode);		//1 is for UI related events, 0 is for be related alarm events
				 */
				String[] data = intent.getStringArrayExtra("data");
				if(data!=null) {

					if(data[0].equalsIgnoreCase("BUTTON")) {
						ESponderLogUtils.error("BUTTON PRESSED", data[1]);
						if(String.valueOf(data[1]).equalsIgnoreCase(String.valueOf(8))) {
							//TODO Provide a mean to upload an event to the back end
							// We'll see what it will do
						}
						MobileUIButtonPressEventSender(context, data[1]);
					}
				}

			}
			else 
				ESponderLogUtils.error("***** OTHER *****", intent.getAction());
		}
		
	}

	//*******************************************************************************************************************************

	private boolean processIncomingLoginEvent(Context context, LoginEsponderUserEvent loginResponseEvent) {
		// Process the incoming event details and store them in memory

		ESponderLogUtils.error("RECEIVER FOR LOGIN", "iROLE = " + String.valueOf(loginResponseEvent.getiRole()));

		if(loginResponseEvent.getEventAttachment() != null) {

			SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			SharedPreferences.Editor spEditor = prefs.edit();
			spEditor.putString(GlobalVar.spESponderExtraNodeIP, loginResponseEvent.getsIPaddresssrv());
			spEditor.putString(GlobalVar.spESponderUserRasIP, loginResponseEvent.getRasberryip());
			spEditor.putString(GlobalVar.spESponderSIPServer, loginResponseEvent.getsIPaddress());
			spEditor.putLong(GlobalVar.spESponderUserID, loginResponseEvent.getEventAttachment().getId());
			PersonnelDTO personnel = loginResponseEvent.getEventAttachment().getPersonnel();
			if(personnel != null)
				spEditor.putString(GlobalVar.spESponderFullName, personnel.getLastName()+" "+personnel.getFirstName());
			spEditor.commit();
		}

		// We set the iRole of the application here
		((GlobalVar)context.getApplicationContext()).setRole(loginResponseEvent.getiRole());

		ESponderLogUtils.error("CURRENT USER IS ", String.valueOf(((GlobalVar)context.getApplicationContext()).getRole()));

		//	Criteria for FRC or FR has changed
		if(loginResponseEvent.getiRole() == 0) {
			//  FRC
			((GlobalVar)context.getApplicationContext()).setChief(true);
			//			context.sendBroadcast(sendIntentForSIPLogin(loginResponseEvent));
			LoginDetailsSender(context, loginResponseEvent);
			return true;
		}
		else if(loginResponseEvent.getiRole() == 1) {
			//	FR
			((GlobalVar)context.getApplicationContext()).setChief(false);
			//			context.sendBroadcast(sendIntentForSIPLogin(loginResponseEvent));...
			LoginDetailsSender(context, loginResponseEvent);
			return true;
		}
		else if(loginResponseEvent.getiRole() == 2) {
			//	Incident Commander, MEOC Driver
			((GlobalVar)context.getApplicationContext()).setChief(false);
			return true;
		}
		else if(loginResponseEvent.getiRole() == -1 ) {
			//	ERROR
			return false;
		}
		else
			return false;

	}

	//*******************************************************************************************************************************

	private void processLoginResponse(final Context context, final String loginInfo) {

		final String jsonObj = loginInfo;

		class processIncomingLoginResponse implements Runnable {

			@Override
			public void run() {

				LoginEsponderUserEvent loginResponseEvent = null;
				mapper = new ObjectMapper();
				mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				try {
					loginResponseEvent = mapper.readValue(jsonObj, LoginEsponderUserEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(loginResponseEvent != null) {

					if(loginResponseEvent.getIMEI().equalsIgnoreCase(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId())) {

						ESponderLogUtils.error("LoginResponseEvent Received","FAMILIAR IMEI, PROCESSING");

						// Process the event contents
						if(processIncomingLoginEvent(context, loginResponseEvent)) {
							// Successful Login
							((GlobalVar)context.getApplicationContext()).setLoggedIn(true);
							ESponderLogUtils.error("LOGIN EVENT DECODED", "SUCCESSFUL LOGIN");

							//FIXME Here we activate the gps mechanism in order to constantly receive location changes
							//							activateGPSMeasurements(context);

							Intent innerIntent = new Intent(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_APPROVED);
							context.sendBroadcast(innerIntent);
						}
						else {
							// Unsuccessful Login
							ESponderLogUtils.error("LOGIN EVENT DECODED", "LOGIN DENIED");
							Intent innerIntent = new Intent(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_DENIED);
							context.sendBroadcast(innerIntent);
						}
					}
					else
						ESponderLogUtils.error("LoginResponseEvent Received","UNKNOWN IMEI, SKIPPING");
				}
				else {
					Intent innerIntent = new Intent(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_ERROR);
					context.sendBroadcast(innerIntent);
				}
			}
		}

		Thread thread = new Thread(new processIncomingLoginResponse());
		thread.start();

	}


	//*******************************************************************************************************************************

//	private void processMeasurement(final Context context, final String sensorType, final Double sensorValue, final Long timeStamp) {
//
//		class processIncomingMeasurement implements Runnable {
//
//			@Override
//			public void run() {
//
//				if(sensorType.equalsIgnoreCase(HeartBeatRateSensorDTO.class.getCanonicalName())) {
//					SensorDataSender(context, SensorTypeEnum.HeartBeatRateSensor.getType(), sensorValue, timeStamp);
//					ESponderLogUtils.error("Sensor-HBRate", String.valueOf(sensorValue));
//					//					Toast.makeText(context, "Sensor-HBRate "+String.valueOf(sensorValue), Toast.LENGTH_SHORT).show();
//				}
//				else if(sensorType.equalsIgnoreCase(ActivitySensorDTO.class.getCanonicalName())) {
//					SensorDataSender(context, SensorTypeEnum.ActivitySensor.getType(), sensorValue, timeStamp);
//					ESponderLogUtils.error("Sensor-Activity", String.valueOf(sensorValue));
//					//					Toast.makeText(context, "Sensor-Activity "+String.valueOf(sensorValue), Toast.LENGTH_SHORT).show();
//				}
//				else if(sensorType.equalsIgnoreCase(BreathRateSensorDTO.class.getCanonicalName())) {
//					SensorDataSender(context, SensorTypeEnum.BreathRateSensor.getType(), sensorValue, timeStamp);
//					ESponderLogUtils.error("Sensor-BRRate", String.valueOf(sensorValue));
//					//					Toast.makeText(context, "Sensor-BRRate "+String.valueOf(sensorValue), Toast.LENGTH_SHORT).show();
//				}
//				else if(sensorType.equalsIgnoreCase(BodyTemperatureSensorDTO.class.getCanonicalName())) {
//					SensorDataSender(context, SensorTypeEnum.BodyTemperatureSensor.getType(), sensorValue, timeStamp);
//					ESponderLogUtils.error("Sensor-BodyTempSensor", String.valueOf(sensorValue));
//					//					Toast.makeText(context, "Sensor-BodyTempSensor "+String.valueOf(sensorValue), Toast.LENGTH_SHORT).show();
//				}
//				else if(sensorType.equalsIgnoreCase(MethaneSensorDTO.class.getCanonicalName())) {
//					SensorDataSender(context, SensorTypeEnum.MethaneSensor.getType(), sensorValue, timeStamp);
//					ESponderLogUtils.error("Sensor-MethaneSensor ", String.valueOf(sensorValue));
//					//					Toast.makeText(context, "Sensor-MethaneSensor "+String.valueOf(sensorValue), Toast.LENGTH_SHORT).show();
//				}
//				else if(sensorType.equalsIgnoreCase(CarbonMonoxideSensorDTO.class.getCanonicalName())) {
//					SensorDataSender(context, SensorTypeEnum.CarbonMonoxideSensor.getType(), sensorValue, timeStamp);
//					ESponderLogUtils.error("Sensor-CarbonMonoxideSensor ", String.valueOf(sensorValue));
//					//					Toast.makeText(context, "Sensor-CarbonMonoxideSensor "+String.valueOf(sensorValue), Toast.LENGTH_SHORT).show();
//				}
//				else if(sensorType.equalsIgnoreCase(CarbonDioxideSensorDTO.class.getCanonicalName())) {
//					SensorDataSender(context, SensorTypeEnum.CarbonDioxideSensor.getType(), sensorValue, timeStamp);
//					ESponderLogUtils.error("Sensor-CarbonDioxideSensor ", String.valueOf(sensorValue));
//					//					Toast.makeText(context, "Sensor-CarbonDioxideSensor "+String.valueOf(sensorValue), Toast.LENGTH_SHORT).show();
//				}
//				else if(sensorType.equalsIgnoreCase(OxygenSensorDTO.class.getCanonicalName())) {
//					SensorDataSender(context, SensorTypeEnum.OxygenSensor.getType(), sensorValue, timeStamp);
//					ESponderLogUtils.error("Sensor-OxygenSensor ", String.valueOf(sensorValue));
//					//					Toast.makeText(context, "Sensor-OxygenSensor "+String.valueOf(sensorValue), Toast.LENGTH_SHORT).show();
//				}
//				else if(sensorType.equalsIgnoreCase(HydrogenSulfideSensorDTO.class.getCanonicalName())) {
//					SensorDataSender(context, SensorTypeEnum.HydrogenSulfideSensor.getType(), sensorValue, timeStamp);
//					ESponderLogUtils.error("Sensor-HydrogenSulfideSensor ", String.valueOf(sensorValue));
//					//					Toast.makeText(context, "Sensor-HydrogenSulfideSensor "+String.valueOf(sensorValue), Toast.LENGTH_SHORT).show();
//				}
//				else if(sensorType.equalsIgnoreCase(EnvironmentTemperatureSensorDTO.class.getCanonicalName())) {
//					SensorDataSender(context, SensorTypeEnum.EnvTemperatureSensor.getType(), sensorValue, timeStamp);
//					ESponderLogUtils.error("Sensor-EnvTempSensor ", String.valueOf(sensorValue));
//					//					Toast.makeText(context, "Sensor-EnvTempSensor "+String.valueOf(sensorValue), Toast.LENGTH_SHORT).show();
//				}
//				else {
//					ESponderLogUtils.error("UNKNOWN INCOMING SENSOR", sensorType);
//				}
//			}
//		}
//		Thread thread = new Thread(new processIncomingMeasurement());
//		thread.start();
//	}


	//*******************************************************************************************************************************

//	private void processLPSMeasurement(final Context context, final Double latitude,
//			final Double longitude, final Double altitude, final Long timeStamp) {
//
//		SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
//		final Long currentUserID = prefs.getLong(GlobalVar.spESponderUserID, -1);
//
//		class LPSSensorTransmitter implements Runnable {
//
//			@Override
//			public void run() {
//
//				if(currentUserID != -1) {
//
//					ESponderUserDTO user = new ESponderUserDTO();
//					user.setId(Long.valueOf(0));
//
//					ESponderDFQueryRequestEvent pEvent = new ESponderDFQueryRequestEvent();
//					pEvent.setIMEI(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
//					pEvent.setQueryID(Long.valueOf(2));	// Setting id = 2 is meant for the transmission of the location
//					pEvent.setRequestID(Long.valueOf(20));
//					pEvent.setJournalMessage("JM");
//
//
//					QueryParamsDTO pParams= QueryParamsDTO.with("userid", String.valueOf(currentUserID));
//					pParams.and("alt", String.valueOf(altitude));
//					pParams.and("long", String.valueOf(longitude));
//					pParams.and("lat", String.valueOf(latitude));
//					Long date = new Date().getTime();
//					pParams.and("datefrom", String.valueOf(timeStamp));
//					pParams.and("dateto", String.valueOf(timeStamp));
//					pEvent.setParams(pParams);
//					pEvent.setEventAttachment(user);
//					pEvent.setJournalMessage("Test Event");
//					pEvent.setEventTimestamp(new Date());
//					pEvent.setEventSeverity(SeverityLevelDTO.UNDEFINED);
//					pEvent.setEventSource(user);
//
//					ObjectMapper mapper = new ObjectMapper();
//					String eventJson = null;
//					try {
//						eventJson = mapper.writeValueAsString(pEvent);
//					} catch (JsonGenerationException e) {
//						e.printStackTrace();
//					} catch (JsonMappingException e) {
//						e.printStackTrace();
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//					if(eventJson != null) {
//						Intent intent = new Intent(GlobalVar.MAP_REQUEST_HIERARCHY);
//						intent.addCategory(GlobalVar.INTENT_CATEGORY);
//						intent.putExtra("event", eventJson);
//						context.sendBroadcast(intent);
//					}
//				}
//
//
//			}			
//		}
//
//		class processIncLPSMeasurement implements Runnable {
//
//			@Override
//			public void run() {
//
//				GlobalVar.db.updateFRLocation(currentUserID, timeStamp, timeStamp,
//						String.valueOf(latitude), String.valueOf(longitude), String.valueOf(altitude));
//				Intent intent = new Intent(GlobalVar.INNER_MAP_FR_UPDATE);
//				context.sendBroadcast(intent);
//			}
//		}
//
//		Thread processor = new Thread(new processIncLPSMeasurement());
//		processor.start();
//
//		Thread transmitter = new Thread(new LPSSensorTransmitter());
//		transmitter.start();
//	}

	//*******************************************************************************************************************************

//	private void SensorDataSender(final Context context, int st, Double sv, final Long timestamp) {
//
//		final int sensorType = st;
//		final Double sensorValue = sv;
//
//		class SensorSender implements Runnable {
//
//			@Override
//			public void run() {
//				SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
//				Long currentUserID = prefs.getLong(GlobalVar.spESponderUserID, -1);
//				String rasIP = prefs.getString(GlobalVar.spESponderUserRasIP, "");
//				if(currentUserID != -1) {
//					ESponderRestClient client = new ESponderRestClient(context, "http://"+rasIP+"/addfr/"
//							+ currentUserID +"/" + String.valueOf(sensorType)+ "/" + String.valueOf(sensorValue)
//							+ "/" + String.valueOf(timestamp));
//					try {
//						client.Execute(RequestMethod.POST);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//
//			}
//		}
//
//		Thread sensorSender = new Thread(new SensorSender());
//		sensorSender.start();
//	}


	//*******************************************************************************************************************************

	private void MobileUIAlarmEventSender(int alarmType, final Context context, int mode) {

		final int modeIntern = mode;

		class AlarmSender implements Runnable {

			@Override
			public void run() {

				ESponderLogUtils.error("MOBILE UI ALARM EVENT SENDER", "ALARM HAS BEEN RECEIVED FROM THE UI");

				//FIXME Check that the url of the web service call ends with the "alarm" segment
				String bbSupIP;
				String ssid = ESponderUtils.checkWifiSSID(context);
				//				if(ssid!=null && ssid.equalsIgnoreCase("MEOC-WIFI")) {
				SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
				String extraNodeIP = prefs.getString(GlobalVar.spESponderExtraNodeIP, "");
				bbSupIP = "http://"+extraNodeIP+"/alarm/";
				//				}
				//				else
				//					bbSupIP = "http://192.168.42.1/alarm/";

				ESponderLogUtils.error("Alarm Address to check", bbSupIP + modeIntern+"/"+String.valueOf(new Date().getTime()));
				ESponderRestClient client = new ESponderRestClient(context, bbSupIP + modeIntern+"/"+String.valueOf(new Date().getTime()));
				try {
					client.Execute(RequestMethod.POST);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		Thread sensorSender = new Thread(new AlarmSender());
		sensorSender.start();
	}

	//*******************************************************************************************************************************

	private void MobileUIButtonPressEventSender(final Context context, final String buttonID) {

		class ButtonPressSender implements Runnable {

			@Override
			public void run() {
				String bbSupIP;
				String ssid = ESponderUtils.checkWifiSSID(context);
				//					if(ssid!=null && ssid.equalsIgnoreCase("MEOC-WIFI")) {
				SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
				String extraNodeIP = prefs.getString(GlobalVar.spESponderExtraNodeIP, "");
				bbSupIP = "http://"+extraNodeIP+"/button/";
				//					}
				//					else
				//						bbSupIP = "http://192.168.42.1/button/";

				ESponderLogUtils.error("Address to check",bbSupIP + buttonID+"/"+String.valueOf(new Date().getTime()));
				ESponderRestClient client = new ESponderRestClient(context, bbSupIP + buttonID+"/"+String.valueOf(new Date().getTime()));
				try {
					client.Execute(RequestMethod.POST);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		Thread sensorSender = new Thread(new ButtonPressSender());
		sensorSender.start();
	}


	//*******************************************************************************************************************************

	private void LoginDetailsSender(final Context context,final LoginEsponderUserEvent event ) {


		class LoginSender implements Runnable {

			@Override
			public void run() {

				String bbSupIP;
				String ssid = ESponderUtils.checkWifiSSID(context);
				//				if(ssid!=null && ssid.equalsIgnoreCase("MEOC-WIFI"))
				bbSupIP = "http://"+event.getsIPaddresssrv()+"/login/";
				//				else
				//					bbSupIP = "http://192.168.42.1/login/";


				//				String bbSupIP = "http://"+event.getsIPaddresssrv()+"/login/";

				Intent sipIntent = new Intent(GlobalVar.SIP_LOGIN);
				Bundle sipBundle = new Bundle();
				sipBundle.putString("bbIP", event.getRasberryip());
				sipBundle.putString("sipServerIP", event.getsIPaddress());
				sipBundle.putString("extranodeIP", event.getsIPaddresssrv());
				sipBundle.putString("username", event.getsIPusername());
				sipBundle.putString("password", event.getsIPpassword());
				sipBundle.putString("sipMEOCIP", event.getsIPaddressMEOC());
				sipIntent.putExtra("sipBundle", sipBundle);
				context.sendBroadcast(sipIntent);

				ESponderLogUtils.error("Address to check",bbSupIP+event.getsIPaddress()+"/"+event.getsIPusername()+"/"+event.getsIPpassword()
						+"/"+String.valueOf(new Date().getTime()));


				ESponderRestClient client = new ESponderRestClient(context, bbSupIP+event.getsIPaddress()+"/"+event.getsIPusername()
						+"/"+event.getsIPpassword()+"/"+String.valueOf(new Date().getTime()));
				try {
					client.Execute(RequestMethod.POST);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		Thread sensorSender = new Thread(new LoginSender());
		sensorSender.start();
	}

	//*******************************************************************************************************************************

	private void createThreadForMapUpdate(final Context context, final String jsonObj) {

		class MapUpdateThread implements Runnable {

			@Override
			public void run() {

				ESponderDFQueryResponseEvent dfQueryResponseEvent = null;
				mapper = new ObjectMapper();
				mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				try {
					dfQueryResponseEvent = mapper.readValue(jsonObj, ESponderDFQueryResponseEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(dfQueryResponseEvent != null) {

					//FIXME MAP UPDATE MAP UPDATE MAP UPDATE
					//					if(dfQueryResponseEvent.getIMEI().equalsIgnoreCase(((TelephonyManager)context.getApplicationContext().
					//							getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId())) {
					ESponderLogUtils.error("UPDATE MAP", "BEGINNING UPDATE");
					if(processIncomingMapData(context, dfQueryResponseEvent)) {
						((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(false);
						if(mapInitialized) {
							Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_UPDATE);
							context.sendBroadcast(hierarchyIntent);
						}
						else {
							mapInitialized = true;
							Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY);
							context.sendBroadcast(hierarchyIntent);
						}
					}
					else {
						((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(false);
						Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR);
						context.sendBroadcast(hierarchyIntent);
					}
					//					}
					//					else
					//						ESponderLogUtils.error("MapUpdateEvent Received","UNKNOWN IMEI, SKIPPING");
				}
				else {
					((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(false);
					Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR);
					context.sendBroadcast(hierarchyIntent);
				}
			}
		}

		if(!((GlobalVar)context.getApplicationContext()).getMapDataProcessActive()) {

			((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(true);
			Thread thread = new Thread(new MapUpdateThread());
			thread.start();
		}
	}


	//*******************************************************************************************************************************

	private boolean processIncomingMapData(Context context, ESponderDFQueryResponseEvent dfQueryResponseEvent) {

		if(dfQueryResponseEvent.getEventAttachment() != null) {
			//FIXME Decide on the storage of the CrisisContext here
			CrisisContextDTO cc = dfQueryResponseEvent.getEventAttachment();

			if(cc!=null) {
				String ccLocation = cc.getCrisisLocation().getCentre().getLatitude().toString()+"a"+
						cc.getCrisisLocation().getCentre().getLongitude().toString();

				SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
				SharedPreferences.Editor spEditor = prefs.edit();
				spEditor.putString(GlobalVar.spCrisisContextVar, ccLocation);
				spEditor.commit();
			}

		}
		else
			ESponderLogUtils.error("CrisisCOntext persistence", "NULL Crisis COntext Object");

		if(dfQueryResponseEvent.getMapPositions() != null) {
			OrganizeHierarchy(context, dfQueryResponseEvent.getMapPositions());
			return true;
		}
		else
			return false;
	}

	//*******************************************************************************************************************************

	private void OrganizeHierarchy(Context context, OCEocALTDTO hierarchy) {

		boolean found = false;
		ESponderLogUtils.error("MAP UPDATED","TRUE");

		for(OCMeocALTDTO meoc : hierarchy.getSubordinateMEOCs()) {
			GlobalVar.db.insertOrUpdateMeocAndLocation(meoc);
			ActorICALTDTO ic = meoc.getIncidentCommander();
			GlobalVar.db.insertOrUpdateIncidentCommander(ic);
			if(meoc.getIncidentCommander().getFrTeams() != null) {
				if(meoc.getIncidentCommander().getFrTeams().size() > 0) {
					for(FRTeamALTDTO team :meoc.getIncidentCommander().getFrTeams()) {
						ActorFRCALTDTO chief = team.getFrchief();
						if(chief != null) {
							GlobalVar.db.insertOrUpdateFRChiefAndLocation(chief);
							if(chief.getSubordinates() != null) {
								if(chief.getSubordinates().size() > 0 ) {
									for(ActorFRALTDTO fr : chief.getSubordinates()) {
										if(!found)
											GlobalVar.db.insertOrUpdateFirstResponderAndLocation(fr);
									}
								}
							}
						}
					}
				}
			}
		}

	}

	//*******************************************************************************************************************************

	private void processNewIncomingActionEvent(final Context context, final String eventJSON) {

		class EventProcessor implements Runnable {

			@Override
			public void run() {

				mapper = new ObjectMapper();
				mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				NewActionEvent event = null;

				//FIXME Provide a means to check for self actions and not actions that belong to another team...
				//Supposedly the IMEI of the chief will be used for matching
				try {
					event = mapper.readValue(eventJSON, NewActionEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {

					List<ActionPartDTO> actionParts;
					List<ActionObjectiveDTO> actionObjectives;
					List<ActionPartObjectiveDTO> actionPartObjectives;


					actionParts = (List<ActionPartDTO>) event.getActionParts().getResultList();
					for(ActionPartDTO ap : actionParts)
						GlobalVar.db.insertActionPart(ap.getId(), ap.getActionId(), 
								ap.getActor(), ap.getSeverityLevel().ordinal(),
								ap.getActionOperation().ordinal(), ap.getTitle(),
								new Date().getTime(), ap.getActionStage().ordinal());

					actionObjectives = (List<ActionObjectiveDTO>) event.getActionObjectives().getResultList();
					for(ActionObjectiveDTO ao : actionObjectives)
						GlobalVar.db.insertActionObjective(ao.getId(), ao.getAction().getId(),
								ao.getTitle(), ao.getPeriod().getDateFrom(), ao.getPeriod().getDateTo(),
								Double.valueOf(((SphereDTO)ao.getLocationArea()).getCentre().getLatitude().toString()),
								Double.valueOf(((SphereDTO)ao.getLocationArea()).getCentre().getLongitude().toString()),
								Double.valueOf(((SphereDTO)ao.getLocationArea()).getCentre().getAltitude().toString()), ao.getActionStage().ordinal());

					actionPartObjectives = (List<ActionPartObjectiveDTO>) event.getActionPartObjectives().getResultList();
					for(ActionPartObjectiveDTO apo :actionPartObjectives)
						GlobalVar.db.insertActionPartObjective(apo.getId(), apo.getActionPartID(),
								apo.getTitle(), apo.getPeriod().getDateFrom(), apo.getPeriod().getDateTo(),
								Double.valueOf(((SphereDTO)apo.getLocationArea()).getCentre().getLatitude().toString()),
								Double.valueOf(((SphereDTO)apo.getLocationArea()).getCentre().getLongitude().toString()),
								Double.valueOf(((SphereDTO)apo.getLocationArea()).getCentre().getAltitude().toString()), 0);

					//Play a notification sound to alert the user
					ESponderUtils.playNotificationSound(context, false);

					//Broadcast the appropriate intent to notify th user of an incoming action event
					Intent newActionIntent = new Intent(GlobalVar.INNER_ACTION_RECEIVED);
					context.sendBroadcast(newActionIntent);

				}
				else
					ESponderLogUtils.error("RECEIVER", "error while reconstructing the action event");
			}
		}

		Thread thread = new Thread(new EventProcessor());
		thread.start();

	}


	//*******************************************************************************************************************************

	private void processAlarmRelatedEvents(final Context context, final String eventJSON) {

		class AlarmProcessor implements Runnable {

			@Override
			public void run() {
				ObjectMapper mapper = new ObjectMapper();
				DatafusionResultsGeneratedEvent event = null;
				try {
					event = mapper.readValue(eventJSON, DatafusionResultsGeneratedEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {

					DatafusionResultsDTO results = event.getEventAttachment();

					//FIXME KARAFIXME

					//I need an alarm to sound both on the fr and the frc application when the alarm concerns the fr
					//In this case, I cannot check for an IMEI, so I will check for the frID. I will only store it if it concerns a member in the same team.

					SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
					Long currentID = prefs.getLong(GlobalVar.spESponderUserID, -1);
					Long incomingID = Long.valueOf(results.getResultsText2());

					//play a sound on the UI of the First Responder
					context.sendBroadcast(turnUIAlarmOn(false));

					if(((GlobalVar)context.getApplicationContext()).getRole() == 0) { //FRC case

						if(String.valueOf(currentID).equalsIgnoreCase(String.valueOf(incomingID)) ||
								checkForID(Long.valueOf(incomingID), currentID)) {

							ESponderLogUtils.error("FRC app", "alarm");

							GlobalVar.db.insertFRAlarm(results.getId(), Long.valueOf(results.getResultsText2()), 
									event.getEventSeverity().ordinal(), results.getResultsText1(), event.getEventTimestamp().getTime(), 0);

							// (Possibly) play a sound on the beagle board
							BEAlarmEventSender(context, ((GlobalVar)context.getApplicationContext()).getRole());

							// play a sound on the mobile
							context.startService(new Intent(context.getApplicationContext(), MediaPlayerService.class));

							Intent intent = new Intent(GlobalVar.INNER_ALARM_RECEIVED);
							intent.putExtra("frID", results.getId());
							context.sendBroadcast(intent);	
						}
					}
					else if(((GlobalVar)context.getApplicationContext()).getRole() == 1) { //FR case
						if(String.valueOf(currentID).equalsIgnoreCase(String.valueOf(incomingID))) {

							ESponderLogUtils.error("FR app", "alarm");

							// (Possibly) play a sound on the beagle board
							BEAlarmEventSender(context, ((GlobalVar)context.getApplicationContext()).getRole());

							// play a sound on the mobile
							context.startService(new Intent(context.getApplicationContext(), MediaPlayerService.class));

							ESponderUtils.playNotificationSound(context, true);
							Intent intent = new Intent(GlobalVar.INNER_ALARM_RECEIVED);
							context.sendBroadcast(intent);
						}	
					}
				}
				else
					ESponderLogUtils.error("ALARM RECEIVED","UNABLE TO PROCESS");
			}

			private boolean checkForID(Long idToCheck, Long currentID) {
				Long current;
				ArrayList<Long> teamIDs = GlobalVar.db.getAllFirstResponderIDsByChief(currentID);
				Iterator<Long> it = teamIDs.iterator();
				while(it.hasNext()) {
					current = it.next();
					ESponderLogUtils.error("Alarm Reception", "FR ID: "+current);
					if(String.valueOf(idToCheck).equalsIgnoreCase(String.valueOf(current)))
						return true;
				}
				return false;
			}
		}

		Thread thread = new Thread(new AlarmProcessor());
		thread.run();
	}


	//*******************************************************************************************************************************

	private void processIncomingMessageEvents(final Context context, final String eventJSON) {

		class MessageProcessor implements Runnable {

			@Override
			public void run() {
				ObjectMapper mapper = new ObjectMapper();
				MobileMessageEvent event = null;
				try {
					event = mapper.readValue(eventJSON, MobileMessageEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {

					if(event.getMobileimei().equalsIgnoreCase(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId())) {
						String origin = null;
						if(event.getSourceEoc()!= null || event.getSourceMEoc() != null) {
							if(event.getSourceEoc()!= null)
								origin = event.getSourceEoc();
							else if(event.getSourceMEoc()!= null)
								origin = event.getSourceMEoc();

							long messageID = GlobalVar.db.insertFRMessage(origin, event.getSzMessage(), event.getEventTimestamp().getTime(), 0);

							// Start the dialog Message Activity
							Intent intent = new Intent(context, NotificationsDialog.class);
							intent.setAction(GlobalVar.NOTIFICATION_POPUP);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							intent.putExtra("origin", origin);
							intent.putExtra("message", event.getSzMessage());
							intent.putExtra("messageID", messageID);
							context.startActivity(intent);
						}
					}
					else
						ESponderLogUtils.error("NOTIFICATION RECEIVED","UNKNOWN IMEI, NOT INTENDED FOR CURRENT USER");
				}
				else
					ESponderLogUtils.error("NOTIFICATION RECEIVED","EMPTY MESSAGE, UNABLE TO PROCESS");
			}	
		}

		Thread thread = new Thread(new MessageProcessor());
		thread.run();
	}


	//*******************************************************************************************************************************

	private void processIncomingFileEvents(final Context context, final String eventJSON) {

		class FileProcessor implements Runnable {

			@Override
			public void run() {
				ObjectMapper mapper = new ObjectMapper();
				MobileFileNotifierEvent event = null;
				try {
					event = mapper.readValue(eventJSON, MobileFileNotifierEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {
					if(event.getImei().equalsIgnoreCase(getSmartPhoneIMEI(context))) {

						Uri data = Uri.parse(event.getFileURL());
						Intent dfIntent = new Intent(context, DownloaderService.class);
						dfIntent.setData(data);
						dfIntent.putExtra("urlpath", event.getFileURL());
						dfIntent.putExtra("description", event.getDescription());

						Long newFileID;
						if(event.getSourceeoc()!= null) {
							newFileID = GlobalVar.db.insertFile(event.getDescription(), 0, null, event.getFileURL(), event.getSourceeoc(), event.getFileid());
							dfIntent.putExtra("beFileID", event.getSourceeoc());
						}

						else {
							newFileID = GlobalVar.db.insertFile(event.getDescription(), 0, null, event.getFileURL(), event.getSourcemeoc(), event.getFileid());
							dfIntent.putExtra("beFileID", event.getSourcemeoc());
						}

						dfIntent.putExtra("fileID", newFileID);
						context.startService(dfIntent);

					}
					else
						ESponderLogUtils.error("NOTIFICATION RECEIVED","UNKNOWN IMEI, NOT INTENDED FOR CURRENT USER");
				}
				else
					ESponderLogUtils.error("NOTIFICATION RECEIVED","EMPTY MESSAGE, UNABLE TO PROCESS");
			}	
		}

		Thread thread = new Thread(new FileProcessor());
		thread.run();
	}

	//*******************************************************************************************************************************

	private void processIncomingCancelActionEvent(final Context context, final String eventStr) {

		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				UpdateActionEvent event = null;
				ObjectMapper mapper = new ObjectMapper();

				try {
					event = mapper.readValue(eventStr, UpdateActionEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {

					Long actionID = event.getEventAttachment().getId();

					//	1	find the action parts and return an array of ap ids
					ArrayList<Long> apIds = GlobalVar.db.getActionPartIDsForAction(actionID);

					//	2	find the action part objectives that correspond to the array and delete them
					for(Long actionpartID : apIds) {
						//find the corresponding action part objectives for each action part available in the action
						// and set cancelled status ( ActionStageEnumDTO.PartiallyDone )
						ArrayList<Long> apoIds = GlobalVar.db.getActionPartObjectivesIDsForAP(actionpartID);
						for(Long apoID : apoIds) {
							GlobalVar.db.updateActionPartObjectiveStatus(apoID, ActionStageEnumDTO.PartiallyDone.ordinal());
						}
						// for every action part that it's objectives have been cancelled 
						// update the action part to cancelled state as well
						GlobalVar.db.updateActionPartStatus(actionpartID, ActionStageEnumDTO.PartiallyDone.ordinal());
					}
					//	4	delete the action objectives
					GlobalVar.db.updateActionObjectiveStatus(actionID, ActionStageEnumDTO.PartiallyDone.ordinal());
					//	5	delete the action
					GlobalVar.db.updateActionStatus(actionID, ActionStageEnumDTO.PartiallyDone.ordinal());

					//	Notify the adapter to reload the action related information from the local db
					Intent cancelActionIntent = new Intent(GlobalVar.INNER_APO_STATUS_CHANGED);
					context.sendBroadcast(cancelActionIntent);
				}

			}
		};

		Thread actionCanceller = new Thread(runnable);
		actionCanceller.start();
	}

	//*******************************************************************************************************************************

	private void activateGPSMeasurements(final Context context) {
		Thread gpsTHread = new Thread(new Runnable() {

			@Override
			public void run() {
				GlobalVar.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
				GlobalVar.locationListener = new ESponderLocationListener(context);
				Looper.prepare();
				GlobalVar.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 5, GlobalVar.locationListener);
				Looper.loop();
			}
		});
		gpsTHread.start();
	}

	private String getSmartPhoneIMEI(Context context) {
		return ((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
	}



	//*******************************************************************************************************************************

	private void BEAlarmEventSender(final Context context, final int role) {

		class ButtonPressSender implements Runnable {

			@Override
			public void run() {
				String bbSupIP = "";
				SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);


				// Repeatable for both FR and FRC
				String extraNodeIP = prefs.getString(GlobalVar.spESponderExtraNodeIP, "");
				bbSupIP = "http://"+extraNodeIP+"/bbalarm/";

				ESponderLogUtils.error("Current Holder Address to check",bbSupIP +String.valueOf(new Date().getTime()));
				ESponderRestClient client = new ESponderRestClient(context, bbSupIP+String.valueOf(new Date().getTime()));
				try {
					client.Execute(RequestMethod.POST);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if(String.valueOf(role).equalsIgnoreCase(String.valueOf(1))) {
					extraNodeIP = prefs.getString(GlobalVar.spESponderUserRasIP, "");
					extraNodeIP = extraNodeIP.replace(":3000", ":1337");
					bbSupIP = "http://" + extraNodeIP + "/bbalarm/";
					ESponderLogUtils.error("FRC Address to check",bbSupIP +String.valueOf(new Date().getTime()));
					client = new ESponderRestClient(context, bbSupIP+String.valueOf(new Date().getTime()));
					try {
						client.Execute(RequestMethod.POST);
					} catch (Exception e) {
						e.printStackTrace();
					}	
				}
			}
		}

		Thread sensorSender = new Thread(new ButtonPressSender());
		sensorSender.start();
	}

	private Intent turnUIAlarmOn(boolean alarmOn) {
		Intent intent = new Intent("eu.esponder.sensor.UI");
		intent.addCategory("org.osgi.service.event.EventAdmin");
		intent.putExtra("mode", alarmOn ? 0 : 4);
		ESponderLogUtils.error("Intent for UI alarm on", "SENT");
		return intent;
	}


}
