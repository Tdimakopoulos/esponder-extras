package eu.esponder.ESponderFRU.xmlparsinghandlers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import eu.esponder.ESponderFRU.data.DataManager;
import eu.esponder.ESponderFRU.data.Poi;

public class POISHandler extends DefaultHandler {
	
	private boolean in_POI = false;
	private boolean in_POICategory = false;
	private boolean in_Lat = false;
	private boolean in_Long = false;
	private boolean in_LabelTitle = false;
	private boolean in_LabelText = false;
	
	Poi tempPoi = null;
	
	/** Gets be called on opening tags like: <tag> **/
	@Override
	public void characters(char ch[], int start, int length) {
		String s1 = new String(ch, start, length);

		if (this.in_POI) {
			if (this.in_POICategory) {
				tempPoi.setPoiCategoryCode(Integer.valueOf(s1));				
			} else if (this.in_Lat) {
				tempPoi.setLatitude(Double.valueOf(s1));				
			}else if (this.in_Long) {
				tempPoi.setLongitude(Double.valueOf(s1));				
			}else if (this.in_LabelTitle) {
				tempPoi.setLabelTitle(s1);				
			}else if (this.in_LabelText) {
				tempPoi.setLabelText(s1);				
			}	
		}
	}
	
	/** Gets be called on opening tags like: <tag> **/
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if (localName.equals("POI")) {
			tempPoi = new Poi();
			this.in_POI = true;
		} else if (localName.equals("POICategory")) {
			this.in_POICategory = true;
		} else if (localName.equals("Lat")) {
			this.in_Lat = true;
		} else if (localName.equals("Long")) {
			this.in_Long = true;
		} else if (localName.equals("LabelTitle")) {
			this.in_LabelTitle = true;
		} else if (localName.equals("LabelText")) {
			this.in_LabelText = true;
		} 	
	}

	/**
	 * Gets be called on closing tags like: </tag>
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		if (localName.equals("POI")) {
			DataManager.getInstance().AddPOI(tempPoi);
			this.in_POI = false;
		} else if (localName.equals("POICategory")) {
			this.in_POICategory = false;
		} else if (localName.equals("Lat")) {
			this.in_Lat = false;
		} else if (localName.equals("Long")) {
			this.in_Long = false;
		} else if (localName.equals("LabelTitle")) {
			this.in_LabelTitle = false;
		} else if (localName.equals("LabelText")) {
			this.in_LabelText = false;
		} 	 	
	}

	public void startDocument() throws SAXException {

	}

	@Override
	public void endDocument() throws SAXException {
		// Nothing to do
	}
}
