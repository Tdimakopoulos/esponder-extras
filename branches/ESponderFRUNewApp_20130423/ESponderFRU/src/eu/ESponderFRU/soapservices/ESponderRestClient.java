package eu.ESponderFRU.soapservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;

public class ESponderRestClient {

	private ArrayList <NameValuePair> params;
	private ArrayList <NameValuePair> headers;

	private String url;

	private int responseCode;

	private String message;

	private String response;

	private Context context;

	public enum RequestMethod {
		GET,
		POST
	}

	public String getResponse() {
		return response;
	}

	public String getErrorMessage() {
		return message;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public ESponderRestClient(Context context,String url)
	{
		this.context = context;
		this.url = url;
		params = new ArrayList<NameValuePair>();
		headers = new ArrayList<NameValuePair>();
	}

	public void AddParam(String name, String value)
	{
		params.add(new BasicNameValuePair(name, value));
	}

	public void AddHeader(String name, String value)
	{
		headers.add(new BasicNameValuePair(name, value));
	}

	public void Execute(RequestMethod method) throws Exception
	{
		switch(method) {
		case GET:
		{
			HttpGet request = new HttpGet(url);
			String getResponse = executeRequest(request, url);
//			processGetRequestResponse(getResponse);
			break;
		}
		case POST:
		{
			HttpPost request = new HttpPost(url);
			executeRequest(request, url);
			break;
		}
		}
	}

	private String executeRequest(HttpUriRequest request, String url)
	{
		HttpClient client = new DefaultHttpClient();

		HttpResponse httpResponse;

		try {
			httpResponse = client.execute(request);
			responseCode = httpResponse.getStatusLine().getStatusCode();
			message = httpResponse.getStatusLine().getReasonPhrase();

			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {

				InputStream instream = entity.getContent();
				response = convertStreamToString(instream);

				// Closing the input stream will trigger connection release
				instream.close();
			}

		} catch (ClientProtocolException e)  {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			client.getConnectionManager().shutdown();
		}

		return response;

	}
	
	
//FIXME REMOVE THIS WHEN ALL SENSOR RELATED CHANGES ARE FIXED
//	private void processGetRequestResponse(String response) {
//
//		// First transmit the incoming string to the backend for processing
//		ESponderLogUtils.error("FR MEASUREMENTS RECEIVED","START");
//		Thread sensorThread = new Thread(new SendPCDSensorsToBackend(this.context, response));
//		sensorThread.start();
//		
//		ESponderLogUtils.error("GetSensorPCD FULL RESPONSE", response);
//
//		// Then process locally the measurements for presentation purposes
//		List<String> frSensorRecords = Arrays.asList(response.split("b"));
//
//		for(String frSensorRecord : frSensorRecords) {
//			List<String> frSensorMeasurements = Arrays.asList(frSensorRecord.split("a"));
//			if(frSensorMeasurements.size() > 0) {
//
//				Long frID = Long.valueOf(frSensorMeasurements.get(0));
//				for(String frSensorMeasurement :frSensorMeasurements) {
//					if(frSensorMeasurement.contains(",")) {
//						//this part contains the measurement
//						String[] singleMeasurement = frSensorMeasurement.split(",");
//						
////						ESponderLogUtils.error("GetSensorPCD TASK", frSensorMeasurement);
//
//						if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.ActivitySensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.ActivitySensor.getUnit(), Double.valueOf(singleMeasurement[1]), Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//
//						else if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.BodyTemperatureSensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.BodyTemperatureSensor.getUnit(), Double.valueOf(singleMeasurement[1]), 
//									Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//
//						else if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.BreathRateSensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.BreathRateSensor.getUnit(), Double.valueOf(singleMeasurement[1]),
//									Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//
//						else if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.EnvTemperatureSensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.EnvTemperatureSensor.getUnit(), Double.valueOf(singleMeasurement[1]),
//									Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//
//						else if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.MethaneSensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.MethaneSensor.getUnit(), Double.valueOf(singleMeasurement[1]),
//									Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//						
//						else if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.CarbonMonoxideSensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.CarbonMonoxideSensor.getUnit(), Double.valueOf(singleMeasurement[1]),
//									Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//						
//						else if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.CarbonDioxideSensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.CarbonDioxideSensor.getUnit(), Double.valueOf(singleMeasurement[1]), 
//									Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//						
//						else if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.OxygenSensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.OxygenSensor.getUnit(), Double.valueOf(singleMeasurement[1]),
//									Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//						
//						else if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.HydrogenSulfideSensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.HydrogenSulfideSensor.getUnit(), Double.valueOf(singleMeasurement[1]), 
//									Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//
//						else if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.HeartBeatRateSensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.HeartBeatRateSensor.getUnit(), Double.valueOf(singleMeasurement[1]),
//									Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//
//						else if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.LocationSensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.LocationSensor.getUnit(), Double.valueOf(singleMeasurement[1]),
//									Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//
//						else if(singleMeasurement[0].equalsIgnoreCase(String.valueOf(SensorTypeEnum.LPSSensor.getType())))
//							GlobalVar.db.insertOrUpdateSensorMeasurement(frID, Integer.valueOf(singleMeasurement[0]), SensorStatTypeEnum.MEAN.getStatType(),
//									SensorUnitEnum.LPSSensor.getUnit(), Double.valueOf(singleMeasurement[1]),
//									Double.valueOf(singleMeasurement[2]),Double.valueOf(singleMeasurement[3]),
//									Long.valueOf(singleMeasurement[4]), Long.valueOf(singleMeasurement[5]));
//					}
//				}
//			}
//		}
//
//		Intent intent = new Intent(GlobalVar.INNER_SENSORS_RECEIVED);
//		context.sendBroadcast(intent);
//
//	}

	private static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}