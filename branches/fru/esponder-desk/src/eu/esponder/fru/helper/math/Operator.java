package eu.esponder.fru.helper.math;

import java.math.BigDecimal;


public enum Operator
{
    GREATER_THAN(">") {
        @Override public boolean apply(BigDecimal x1, BigDecimal x2) {
        	int i=x1.compareTo(x2);
           if(i==1){
        	   return true;
           }
           else{
        	   return false;
           }
        	 
        }
    },
    lESS_THAN("<") {
        @Override public boolean apply(BigDecimal x1, BigDecimal x2) {
        	int i=x1.compareTo(x2);
           if(i==-1){
         	   return true;
            }
            else{
         	   return false;
            }
        }
    },
    GREATER_EQUAL_THAN(">=") {
            @Override public boolean apply(BigDecimal x1, BigDecimal x2) {
            	int i=x1.compareTo(x2);
                if(i==1 || i==0){
             	   return true;
                }
                else{
             	   return false;
                }
            }    
    },
    lESS_EQUAL_THAN("<=") {
        @Override public boolean apply(BigDecimal x1, BigDecimal x2) {
        	int i=x1.compareTo(x2);
            if(i==-1 || i==0){
         	   return true;
            }
            else{
         	   return false;
            }
        }    
      },
      EQUAL("=") {
          @Override public boolean apply(BigDecimal x1, BigDecimal x2) {
          	int i=x1.compareTo(x2);
              if(i==0){
           	   return true;
              }
              else{
           	   return false;
              }
          }    
        };
  
    // You'd include other operators too...

    private final String text;

    private Operator(String text) {
        this.text = text;
    }

    // Yes, enums *can* have abstract methods. This code compiles...
    public abstract boolean apply(BigDecimal x1, BigDecimal x2);

    @Override public String toString() {
        return text;
    }
}
