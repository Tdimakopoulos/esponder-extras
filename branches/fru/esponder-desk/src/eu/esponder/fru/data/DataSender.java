package eu.esponder.fru.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;

import eu.esponder.fru.DataFusionController;
import eu.esponder.fru.helper.Utils;
import eu.esponder.jaxb.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.jaxb.model.crisis.resource.sensor.GasSensorDTO;
import eu.esponder.jaxb.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;

public class DataSender extends Thread {
	
	private synchronized void createSensorMeasurementsEnvelope() {
		Random rand = new Random();
		
		SensorDTO bodyTemperatureSensor = new BodyTemperatureSensorDTO();
		SensorDTO gasSensor = new GasSensorDTO();
		
		ArithmeticSensorMeasurementDTO sensorMeasurement1 = new ArithmeticSensorMeasurementDTO();
		ArithmeticSensorMeasurementDTO sensorMeasurement2 = new ArithmeticSensorMeasurementDTO();
		ArithmeticSensorMeasurementDTO sensorMeasurement3 = new ArithmeticSensorMeasurementDTO();
		
		sensorMeasurement1.setSensor(bodyTemperatureSensor.getClass());
		sensorMeasurement2.setSensor(bodyTemperatureSensor.getClass());
		sensorMeasurement3.setSensor(bodyTemperatureSensor.getClass());
		
		ArithmeticSensorMeasurementDTO sensorMeasurement4 = new ArithmeticSensorMeasurementDTO();
		ArithmeticSensorMeasurementDTO sensorMeasurement5 = new ArithmeticSensorMeasurementDTO();
		
		sensorMeasurement4.setSensor(gasSensor.getClass());
		sensorMeasurement5.setSensor(gasSensor.getClass());
		
		sensorMeasurement1.setMeasurement(new BigDecimal(rand.nextInt(80 - 60 + 1) + 60));
		sensorMeasurement1.setTimestamp(new Date());
		sensorMeasurement2.setMeasurement(new BigDecimal(rand.nextInt(80 - 60 + 1) + 60));
		sensorMeasurement2.setTimestamp(new Date());
		sensorMeasurement3.setMeasurement(new BigDecimal(rand.nextInt(80 - 60 + 1) + 60));
		sensorMeasurement3.setTimestamp(new Date());
		
		sensorMeasurement4.setMeasurement(new BigDecimal(rand.nextInt(rand.nextInt(500 - 300 + 1) + 300)));
		sensorMeasurement4.setTimestamp(new Date());
		sensorMeasurement5.setMeasurement(new BigDecimal(rand.nextInt(rand.nextInt(500 - 300 + 1) + 300)));
		sensorMeasurement5.setTimestamp(new Date());
		
		SensorMeasurementEnvelopeDTO sensorMeasurementsEnvelope = new SensorMeasurementEnvelopeDTO();
		
		sensorMeasurementsEnvelope.add(sensorMeasurement1); 
		sensorMeasurementsEnvelope.add(sensorMeasurement2);
		sensorMeasurementsEnvelope.add(sensorMeasurement3);
		sensorMeasurementsEnvelope.add(sensorMeasurement4);
		sensorMeasurementsEnvelope.add(sensorMeasurement5);
		
		DataFusionController.sensorMeasurementsEnvelopeList.add(sensorMeasurementsEnvelope);
		
	}

	public void run() {

		while (true) 
		{
			
			try {
				Thread.sleep(2000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			createSensorMeasurementsEnvelope();
			
			System.out.println("Steilame!! stis = " + Utils.getTimeString());

		}

	}
}
