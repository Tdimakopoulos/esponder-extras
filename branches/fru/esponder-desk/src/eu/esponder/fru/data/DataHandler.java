package eu.esponder.fru.data;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import eu.esponder.fru.DataFusionController;
import eu.esponder.fru.helper.Utils;
import eu.esponder.jaxb.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;

public class DataHandler extends Thread {

	private List<SensorMeasurementEnvelopeDTO> sensorMeasurementsEnvelopeList;

	private Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>> sensorMeasurementsMap;

	public DataHandler(
			List<SensorMeasurementEnvelopeDTO> sensorMeasurementEnvelope,
			Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>> sensorMeasurementsMap) {
		this.sensorMeasurementsEnvelopeList = sensorMeasurementEnvelope;
		this.sensorMeasurementsMap = sensorMeasurementsMap;
	}

	public List<SensorMeasurementEnvelopeDTO> getSensorMeasurementsEnvelope() {
		return sensorMeasurementsEnvelopeList;
	}

	public void setSensorMeasurementsEnvelope(
			List<SensorMeasurementEnvelopeDTO> sensorMeasurementsEnvelopeList) {
		this.sensorMeasurementsEnvelopeList = sensorMeasurementsEnvelopeList;
	}

	public synchronized void run() {
		while (true) {

			try {
				Thread.sleep(3000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			processSensorMeasurementEnvelope();
		}

	}

	private synchronized void processSensorMeasurementEnvelope() {

		if (sensorMeasurementsEnvelopeList.isEmpty())
			return;

		System.out.println("Peirame (DataHandler)!! stis = "
				+ Utils.getTimeString());
		for (SensorMeasurementEnvelopeDTO sensorMeasurementsEnvelope : sensorMeasurementsEnvelopeList) {
			// for (Iterator<SensorMeasurementEnvelopeDTO> it =
			// sensorMeasurementsEnvelopeList.iterator(); it.hasNext();) {
			// SensorMeasurementEnvelopeDTO
			// sensorMeasurementsEnvelope=it.next();
			if (!sensorMeasurementsEnvelope.getMeasurements().isEmpty()) {

				for (SensorMeasurementDTO measurement : sensorMeasurementsEnvelope
						.getMeasurements()) {

					/*
					 * Get Measurement from OSGi Envelope for a specific sensor
					 */

					Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>> sensorMeasurementsStatisticsMap = DataFusionController.sensorMeasurementMaps
							.get(measurement.getSensorType());
					System.out.println("[" + Utils.getTimeString()
							+ "] - Adding Measurement for sensor"
							+ measurement.getSensorType());
					/*
					 * Put raw measurement to ALL Queues (i.e. for all statistic
					 * types)
					 */
					for (MeasurementStatisticTypeEnum statisticType : sensorMeasurementsStatisticsMap
							.keySet()) {
						Queue<SensorMeasurementDTO> sensorMeasurementQueue = sensorMeasurementsStatisticsMap
								.get(statisticType);
						sensorMeasurementQueue.add(measurement);

						System.out
								.println("["
										+ Utils.getTimeString()
										+ "] - Added Measurement for sensor"
										+ measurement.getSensorType()
										+ " for statistic "
										+ statisticType.toString()
										+ ". Value = "
										+ ((ArithmeticSensorMeasurementDTO) measurement)
												.getMeasurement().toString());
					}
				}
				 sensorMeasurementsEnvelope.clear();
			}
		}
	//	sensorMeasurementsEnvelopeList.clear();
	}

}
