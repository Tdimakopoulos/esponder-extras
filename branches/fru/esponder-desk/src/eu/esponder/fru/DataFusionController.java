package eu.esponder.fru;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import eu.esponder.fru.data.DataHandler;
import eu.esponder.fru.thread.DataFusionProcessorThread;
import eu.esponder.fru.thread.EnvelopeSender;
import eu.esponder.jaxb.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;

public class DataFusionController extends Thread {

	public static Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Long>> configMap;

	public static List<SensorMeasurementEnvelopeDTO> sensorMeasurementsEnvelopeList;

	public static Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>> sensorMeasurementMaps;

	public static List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticsList;

	public static List<Thread> processorThreads = new ArrayList<Thread>();

	public void run() {
		loadConfig();
		init();
		Thread dataHandlerThread = new DataHandler(
				sensorMeasurementsEnvelopeList, sensorMeasurementMaps);
		dataHandlerThread.start();
		Thread envelopeSenderThread = new EnvelopeSender(
				sensorMeasurementStatisticsList);
		envelopeSenderThread.start();

	}

	public static void loadConfig() {

		configMap = ConfigReader
				.readMeasurementStatisticsConfig(FruConstants.FRU_CONFIGURATION_FILE);
		
		FruConfigurator fruConfigurator = new FruConfigurator();
		try {
			fruConfigurator.init();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println(FruConfigProperties.FRU_DATAFUSION_MEASUREMENT_ENVELOPE_TX_PERIOD_KEY + " = " + 
//				fruConfigurator.getProperty(FruConfigProperties.FRU_DATAFUSION_MEASUREMENT_ENVELOPE_TX_PERIOD_KEY));

	}

	public static void init() {

		sensorMeasurementMaps = new HashMap<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>>();
		sensorMeasurementsEnvelopeList = new ArrayList<SensorMeasurementEnvelopeDTO>();
		sensorMeasurementStatisticsList = new ArrayList<SensorMeasurementStatisticDTO>();

		for (Class<? extends SensorDTO> sensorClass : configMap.keySet()) {

			Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>> sensorMeasurementsStatisticsMap = new HashMap<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>();
			sensorMeasurementMaps.put(sensorClass,
					sensorMeasurementsStatisticsMap);

			Map<MeasurementStatisticTypeEnum, Long> configProcessingMap = configMap
					.get(sensorClass);
			for (MeasurementStatisticTypeEnum processingType : configProcessingMap
					.keySet()) {
				sensorMeasurementsStatisticsMap.put(processingType,
						new LinkedList<SensorMeasurementDTO>());

				Queue<SensorMeasurementDTO> measurementsQueue = new LinkedList<SensorMeasurementDTO>();
				Thread statisticProcessingThread = new DataFusionProcessorThread(
						sensorClass, processingType,
						((Long) configProcessingMap.get(processingType)),
						measurementsQueue, sensorMeasurementStatisticsList);
				statisticProcessingThread.start();
				processorThreads.add(statisticProcessingThread);
			}
		}

	}

}
