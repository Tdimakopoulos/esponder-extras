package eu.esponder.fru.thread;

import java.util.Date;
import java.util.List;
import java.util.Queue;

import eu.esponder.fru.DataFusionController;
import eu.esponder.fru.RuleEngine;
import eu.esponder.fru.helper.Utils;
import eu.esponder.fru.helper.math.MathHelper;
import eu.esponder.jaxb.model.crisis.resource.sensor.ActivitySensorDTO;
import eu.esponder.jaxb.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.jaxb.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.jaxb.model.snapshot.PeriodDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;

public class DataFusionProcessorThread extends Thread {

	private long samplingPeriod;

	private MeasurementStatisticTypeEnum stastisticType;

	private Class<? extends SensorDTO> sensorClass;

	private Queue<SensorMeasurementDTO> sensorMeasurementQueue;

	private List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticList;
	private RuleEngine rules;

	public DataFusionProcessorThread(Class<? extends SensorDTO> sensorClass,
			MeasurementStatisticTypeEnum stastisticType, Long samplingPeriod,
			Queue<SensorMeasurementDTO> sensorMeasurementQueue,
			List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticList) {
		this.samplingPeriod = samplingPeriod;
		this.stastisticType = stastisticType;
		this.sensorClass = sensorClass;
		this.sensorMeasurementQueue = sensorMeasurementQueue;
		this.sensorMeasurementStatisticList = sensorMeasurementStatisticList;
	}

	public void run() {
		rules = new RuleEngine();
		while (true) {

			try {
				Thread.sleep(this.samplingPeriod*1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			// System.out.println("Processing Data for sensor");
			processData();
			System.out.println("[" + Utils.getTimeString()
					+ "] - Kanoume process for: "+sensorClass+" kai Type "+stastisticType);
			
		}

	}

	private synchronized void processData() {
		SensorMeasurementStatisticDTO sensorMeasurementStatistic = null;
		 
		if (DataFusionController.sensorMeasurementMaps.get(this.sensorClass)
				.get(stastisticType).isEmpty()) {

			// if (this.sensorMeasurementQueue.isEmpty()) {
			// System.out.println("SensorMeasurementQueue for " +
			// this.sensorClass.getCanonicalName() + " is Empty!!!");
			return;
		}

		System.out.println("SensorMeasurementQueue for "
				+ this.sensorClass.getName() + " is NOT Empty!!!");

		if (sensorClass.equals(LocationSensorDTO.class)) {
			/*
			 * Do nothing for the moment
			 */
			;
		} else if (sensorClass.equals(ActivitySensorDTO.class)) {
			/*
			 * Do nothing for the moment
			 */
			;
		} else {
			/*
			 * this is the case for all sensors that produce a single Arithmetic
			 * measurement
			 */
			System.out.println((!this.sensorMeasurementQueue.isEmpty() ? "NOT"
					: "")
					+ "Perform processing for "
					+ this.sensorClass.toString()
					+ " - "
					+ this.stastisticType.toString());

			sensorMeasurementStatistic = processArithmeticSensorMeasurements(
					this.sensorClass,
					this.samplingPeriod,
					this.stastisticType,
					DataFusionController.sensorMeasurementMaps.get(
							this.sensorClass).get(stastisticType));

			DataFusionController.sensorMeasurementMaps.get(this.sensorClass)
					.get(stastisticType).clear();
		}

		/*
		 * Processing has finished. Placing the statistic in the Statistics
		 * Results List
		 */// EDW ELENXOS RULE!!
		System.out.println("EDW EIMASTE: "
		+ sensorMeasurementStatistic.getStatistic().getSensorType().toString());

		System.out.println("[" + Utils.getTimeString()
				+ "] - Result statistic for: "+sensorMeasurementStatistic.getStatistic().getSensorType()+" Type "+sensorMeasurementStatistic.getStatisticType()
				+" Value :"+ ((ArithmeticSensorMeasurementDTO) sensorMeasurementStatistic
						.getStatistic()).getMeasurement().toString());
		if (rules.process(
				((ArithmeticSensorMeasurementDTO) sensorMeasurementStatistic
						.getStatistic()).getMeasurement(),
				sensorMeasurementStatistic.getStatistic().getSensorType()
						.toString(), sensorMeasurementStatistic.getStatisticType().toString())) {
			this.sensorMeasurementStatisticList.add(sensorMeasurementStatistic);

			System.out
					.println("PASSED RULES MAX = "
							+ ((ArithmeticSensorMeasurementDTO) sensorMeasurementStatistic
									.getStatistic()).getMeasurement());

		} else {
			System.out
					.println("NOT PASSED RULES MAX = "
							+ ((ArithmeticSensorMeasurementDTO) sensorMeasurementStatistic
									.getStatistic()).getMeasurement());
		}
	}

	public synchronized SensorMeasurementStatisticDTO processArithmeticSensorMeasurements(
			Class<? extends SensorDTO> sensorClass, long samplingPeriod,
			MeasurementStatisticTypeEnum statisticType,
			Queue<? extends SensorMeasurementDTO> measurements) {

		System.out.println("MPHKE!!!");

		SensorMeasurementStatisticDTO resultStatistic = new SensorMeasurementStatisticDTO();
		resultStatistic.setStatisticType(statisticType);
		resultStatistic.setSamplingPeriod(samplingPeriod);

		ArithmeticSensorMeasurementDTO sensorMeasurement = new ArithmeticSensorMeasurementDTO();
		sensorMeasurement.setTimestamp(new Date());
		resultStatistic.setStatistic(sensorMeasurement);

		PeriodDTO period = new PeriodDTO();

		Date dateFrom = new Date();
		Date dateTo = new Date();

		if (!measurements.isEmpty()) {
			dateTo = measurements.peek().getTimestamp();
			dateFrom = measurements.peek().getTimestamp();
		}
		for (SensorMeasurementDTO measurement : measurements) {
			if (measurement.getTimestamp().before(dateFrom)) {
				dateFrom = measurement.getTimestamp();
			}
			if (measurement.getTimestamp().after(dateTo)) {
				dateTo = measurement.getTimestamp();
			}
			// dateFrom = measurement.getTimestamp().before(dateFrom) ?
			// measurement.getTimestamp() : dateFrom;
			// dateTo = measurement.getTimestamp().after(dateTo) ?
			// measurement.getTimestamp() : dateTo;
		}

		/**
		 * FIXME: set the PeriodDTO at the SensorMeasurementStatisticDTO Need to
		 * find how this should be done with the XMLGregorianCalendar class
		 */
		// period.setDateFrom((new XMLGregorianCalendar()).setMillisecond((int)
		// (dateFrom.getTime()));
		// period.setDateTo(dateTo);

		resultStatistic.setPeriod(period);

		sensorMeasurement.setMeasurement(MathHelper
				.calculateArithmeticStatistic(statisticType, measurements));

		sensorMeasurement.setSensor(sensorClass);
		return resultStatistic;
	}

}
