package eu.esponder.fru.thread;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Iterator;

import eu.esponder.fru.DataFusionController;
import eu.esponder.fru.helper.Utils;
import eu.esponder.jaxb.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.jaxb.model.crisis.resource.sensor.GasSensorDTO;
import eu.esponder.jaxb.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;

public class EnvelopeSender extends Thread {

	private List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticsList;
	private SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticEnvelope;
	List<SensorMeasurementStatisticDTO> sensorMeasurementStatistic_temp;
String testdata="";
	public EnvelopeSender(
			List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticsList) {
		this.sensorMeasurementStatisticsList = sensorMeasurementStatisticsList;
	}

	public List<SensorMeasurementStatisticDTO> getSensorMeasurementStatisticsList() {
		return sensorMeasurementStatisticsList;
	}

	public void sensorMeasurementStatisticsList(
			List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticsList) {
		this.sensorMeasurementStatisticsList = sensorMeasurementStatisticsList;
	}

	public void run() {
		while (true) {

			try {
				Thread.sleep(3000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			sendEnvelope();
		}

	}

	private synchronized void sendEnvelope() {

		if (sensorMeasurementStatisticsList.isEmpty()){
			System.out.println("Steilame TO ENVELOPE  NO DATA stis = " + Utils.getTimeString());
			return;}
		sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
		sensorMeasurementStatistic_temp = new ArrayList<SensorMeasurementStatisticDTO>();
		
		
		for (Iterator<SensorMeasurementStatisticDTO> iter = sensorMeasurementStatisticsList.iterator(); iter.hasNext();) {
			sensorMeasurementStatistic_temp.add(iter.next());
			 iter.remove();
		}
		
		sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic_temp);
 		 
 		//send sensorMeasurementStatisticEnvelope !!!
 		for(int i=0;i<sensorMeasurementStatisticEnvelope.getMeasurementStatistics().size();i++){
 			System.out.println("Steilame TO ENVELOPE stis = "+ Utils.getTimeString() +" me timh "+ ((ArithmeticSensorMeasurementDTO) sensorMeasurementStatisticEnvelope.getMeasurementStatistics().get(i).getStatistic()).getMeasurement().toString());
 		}
 	//	System.out.println("Steilame TO ENVELOPE stis = " + Utils.getTimeString() +" me times "+ testdata);
 		//System.out.println("ENVELOPE size TEMP"+ sensorMeasurementStatistic_temp.size() + " size ENVELOPER "+sensorMeasurementStatisticEnvelope.getMeasurementStatistics().size() );
 		sensorMeasurementStatistic_temp.clear();
 		//testdata="";
		}

	}

 
