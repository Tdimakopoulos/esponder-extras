package eu.esponder.fru;

import java.math.BigDecimal;

import eu.esponder.fru.helper.math.Operator;

public class RuleEngine {

	public static boolean process(BigDecimal value, String Sensor, String type) {

		if (Sensor.equals("class eu.esponder.jaxb.model.crisis.resource.sensor.BodyTemperatureSensorDTO")) {

			if (type.equals("MEAN")) {
				return (Operator.GREATER_THAN.apply(value,
						FruConstants.MAX_AVG_TEMP));
			}
			if (type.equals("MAXIMUM")) {
				return (Operator.GREATER_THAN.apply(value,
						FruConstants.MIN_MAX_TEMP));
			}
			if (type.equals("MINIMUM")) {
				return (Operator.EQUAL.apply(value, FruConstants.MAX_MIN_TEMP));
			}

		}

		return false;

	}
}
