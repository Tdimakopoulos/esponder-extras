package com.dynamicview;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dynamicview.R;
import eu.esponder.fru.dto.model.crisis.action.ActionDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import eu.esponder.fru.dto.model.crisis.resource.ResourceStatusDTO;


public class OperationActionsMenuListAdapter extends BaseAdapter {

	private List<ActionDTO> items = new ArrayList<ActionDTO>();
	private LayoutInflater inflater;
	private Context context;
	private TextView messages; 
	private LinearLayout linearlayout; 

	public OperationActionsMenuListAdapter(Context context1) {
		context = context1;
		inflater = LayoutInflater.from(context1);
	}

	public View getView(final int position, View convertView, ViewGroup parent) {

		View v = inflater.inflate(R.layout.listview, null);
		  

		final ActionDTO action = items.get(position);
		if (action != null) {

			messages = (TextView) v.findViewById(R.id.backtextview);
			messages.setText(action.getTitle()); 
			
			linearlayout = (LinearLayout) v.findViewById(R.id.backlinearlayout);
			
			linearlayout.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
				 
				   
				   	
				      ObjectMapper  mapper = new ObjectMapper();
					    mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				        mapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
				        String json = null ;
					    mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				         try {
				 		    json = mapper.writeValueAsString(action);
				 			Log.d("xaxaxs","Elaaaa!212!  "+json);
				 		} catch (JsonGenerationException e) {
				 			// TODO Auto-generated catch block
				 			e.printStackTrace();
				 		} catch (JsonMappingException e) {
				 			// TODO Auto-generated catch block
				 			e.printStackTrace();
				 		} catch (IOException e) {
				 			// TODO Auto-generated catch block
				 			e.printStackTrace();
				 		}
					
					
					
					
					
		 		 Intent itemintent = new Intent(view.getContext(),OperationsByActions.class);
			     /*  Bundle info = new Bundle();
			        info.putString("actionTitle", action.getTitle()); */
			        itemintent.putExtra("actionInfo", json);
			       	((Activity) context).startActivity(itemintent);
					}
			});

		}

		return v;

	}

	public void onClick(View arg0) {
		messages.setText("My text on click");
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	public ActionDTO getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void addOrder(List<ActionDTO> message) {
		this.items = message;
	}

	public void clear() {
		this.items.clear();
	}

}

