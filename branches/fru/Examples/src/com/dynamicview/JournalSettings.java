package com.dynamicview;

import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

public class JournalSettings extends Activity {

	SharedPreferences jernalSettings;
	SharedPreferences.Editor prefsEditor;
	CheckBox typeNo1, typeNo2, typeNo3, typeNo4;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jernalsettings);

		typeNo1 = (CheckBox) findViewById(R.id.typeNo1);
		typeNo2 = (CheckBox) findViewById(R.id.typeNo2);
		typeNo3 = (CheckBox) findViewById(R.id.typeNo3);
		typeNo4 = (CheckBox) findViewById(R.id.typeNo4);

		jernalSettings = this.getSharedPreferences("jernalSettings",
				MODE_WORLD_READABLE);
		prefsEditor = jernalSettings.edit();

		typeNo1.setChecked(jernalSettings.getBoolean("typeNo1", false));
		typeNo2.setChecked(jernalSettings.getBoolean("typeNo2", false));
		typeNo3.setChecked(jernalSettings.getBoolean("typeNo3", false));
		typeNo4.setChecked(jernalSettings.getBoolean("typeNo4", false));
		
	} 

	// Click on the Measurements button of Main
	public void clickHandler(View view) {

		switch (view.getId()) {
		case R.id.SaveSettingsButton:
			prefsEditor.putBoolean("typeNo1", typeNo1.isChecked());
			prefsEditor.putBoolean("typeNo2", typeNo2.isChecked());
			prefsEditor.putBoolean("typeNo3", typeNo3.isChecked());
			prefsEditor.putBoolean("typeNo4", typeNo4.isChecked());
			prefsEditor.commit();
			Toast.makeText(JournalSettings.this,
				 	   "Your changes have been saved", Toast.LENGTH_LONG).show();
			break;
		case R.id.CancelSettingsButton:
			Log.d("CancelSettingsButton", "CancelSettingsButton");
		    Intent intent = new Intent(JournalSettings.this, Journal.class);
     		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        	finish();
			break;

		default:
			break;
		}

	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	 Intent intent = new Intent(JournalSettings.this, Journal.class);
	     		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            startActivity(intent);
	            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
	        	finish();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}

}
