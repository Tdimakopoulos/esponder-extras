package com.esponder.apps;
 
import java.util.ArrayList;
import java.util.List;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import android.app.Activity; 
import android.widget.BaseAdapter; 
import android.widget.LinearLayout;
import android.content.Context; 
import android.content.Intent; 
import android.os.Bundle; 
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

class OperationMenuListAdapter extends BaseAdapter {

	private List<ActorDTO> items = new ArrayList<ActorDTO>();
	private LayoutInflater inflater;
	private Context context;
	private TextView messages; 
	private LinearLayout linearlayout; 

	public OperationMenuListAdapter(Context context1) {
		context = context1;
		inflater = LayoutInflater.from(context1);
	}

	public View getView(final int position, View convertView, ViewGroup parent) {

		View v = inflater.inflate(R.layout.listview, null);
		  

		final ActorDTO actor = items.get(position);
		if (actor != null) {

			messages = (TextView) v.findViewById(R.id.backtextview);
			messages.setText(actor.getTitle()); 
			
			linearlayout = (LinearLayout) v.findViewById(R.id.backlinearlayout);
			
			linearlayout.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
		 		 Intent itemintent = new Intent(view.getContext(),OperationsByActor.class);
			       Bundle info = new Bundle();
			        info.putLong("actorId",actor.getId());
			        info.putString("actorName", actor.getTitle()); 
			        itemintent.putExtra("actorInfo", info);
			       	((Activity) context).startActivity(itemintent);
			   
					}
			});

		}

		return v;

	}

	public void onClick(View arg0) {
		messages.setText("My text on click");
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	public ActorDTO getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void addOrder(List<ActorDTO> message) {
		this.items = message;
	}

	public void clear() {
		this.items.clear();
	}

}
