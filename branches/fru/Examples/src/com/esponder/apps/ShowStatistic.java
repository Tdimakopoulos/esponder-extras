package com.esponder.apps;
 
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper; 
import eu.esponder.fru.dto.model.ESponderEntityDTO; 
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO; 
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO; 
import eu.esponder.fru.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.jsonrcp.JSONRPCException; 
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

public class ShowStatistic extends Activity { 

	WebView statisticWebView; 
	TextView titleTextView;
	String  url = "file:///android_asset/my_example.html?";
	String jasonStatistic; 
	Runnable Refresh;
	Format formatter;
	String[] temp;
	Statistic statistic;
	AlertDialog.Builder builder;
	AlertDialog info ;
	boolean show = false;
	private List<SensorMeasurementStatisticDTO> measurements;
	private Handler handler;
	int countMeasurements;
	private String filter = CreateSensorMeasurementStatisticEvent.class .getName();
	private IncomingReceiver receiver;
	private Map< String , String > actorUrl;
	private boolean isUrl = true;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.statisticslayout);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		handler = new Handler();
		formatter = new SimpleDateFormat("yyyy/MM/dd%20HH:mm:ss");
	 
		Intent startingIntent = getIntent();
		if (startingIntent != null) {
			jasonStatistic = startingIntent.getStringExtra("statistic");
			if (jasonStatistic != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				JsonFactory jsonFactory = new JsonFactory();
				try {
					JsonParser jp = jsonFactory.createJsonParser(jasonStatistic);
					// get the statistic
					 statistic = mapper
							.readValue(jp,Statistic.class);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} 
		
		
		titleTextView = (TextView) findViewById(R.id.titleTextView);
		titleTextView.setText(statistic.getMeasurementTitle()); 
		statisticWebView = (WebView) findViewById(R.id.statisticWebView);
		statisticWebView.getSettings().setJavaScriptEnabled(true);  
		measurements = new ArrayList<SensorMeasurementStatisticDTO>();
		actorUrl = new HashMap <String,String>();
		IntentFilter intentFilter = new IntentFilter();
		 
		// if chief register to CreateLocationMeasurementStatisticEvent . yours id
		if(Menu.actor.getSubordinates()!=null){  
			intentFilter.addAction(filter +"."+Menu.actor.getId()); 
		}else{ // register to CreateLocationMeasurementStatisticEvent . chief's id
			 intentFilter.addAction(filter +"."+Menu.actor.getSupervisor().getId()); 
		}
		
	  intentFilter.addCategory("org.osgi.service.event.EventAdmin");
	  receiver = new IncomingReceiver();
	  registerReceiver(receiver, intentFilter);
			
		Log.d(" date "," from = " + statistic.getFrom() + " to = "+ statistic.getTo());
		
		/* try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		//handler.postDelayed(mUpdate, 5000);
		//handler.postDelayed(mUpdate2, 7000);
		// handler.post(mUpdate);

		/*
		 * dailyWebView.setOnTouchListener(new View.OnTouchListener() {
		 * 
		 * @Override public boolean onTouch(View arg0, MotionEvent arg1) {
		 * dailyWebView.getParent().requestDisallowInterceptTouchEvent( true);
		 * // scrolview.setEnabled(false); return false; } });
		 */
 
		ESponderEntityDTO[] allStatistics = null;
		try {
			allStatistics = (ESponderEntityDTO[])Menu.client.call("esponderdb/getAllESponderDTOs", 
			          ESponderEntityDTO[].class, SensorMeasurementStatisticDTO.class.getName());
		} catch (JSONRPCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		if(allStatistics != null){
			  for (int i = 0; i < allStatistics.length; i++) {
				  if(((SensorMeasurementStatisticDTO)allStatistics[i]).getStatistic() instanceof ArithmeticSensorMeasurementDTO){
					  measurements.add((SensorMeasurementStatisticDTO) allStatistics[i]);
				  }
			  }
		}
		
	  	if(measurements != null){
	  		countMeasurements = measurements.size();
	  	Collections.sort(measurements,COMPARATOR);
	     for (int i = 0; i < measurements.size(); i++) {
	        	  if( (((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic().getSensor().getClass().isAssignableFrom(statistic.getClassName()
	        			  )) && ((SensorMeasurementStatisticDTO)measurements.get(i)).getStatisticType().equals(statistic.getStatisticType())) {
	        		    System.out.println("To id tou einai = "+ measurements.get(i).getId() +" All SENSORs FROM DB : " + (((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getMeasurement() + " kai sensor = "+(((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getSensor().getId()  + " tou Actor = " + Menu.actorSensorMap.get((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getSensor().getId()) + " Hmerominia = " + formatter.format((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getTimestamp()));       	        
	        		    temp = Menu.actorSensorMap.get((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getSensor().getId()).split("#");
	        		   String tempActorUrl = actorUrl.get(temp[1]);
	        		   if(tempActorUrl != null){
	        			   tempActorUrl = tempActorUrl  + temp[1] + "=" + (((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getMeasurement() + "&" +temp[1] + "=" + formatter.format((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getTimestamp())+"&";
	        			   actorUrl.put(temp[1], tempActorUrl);
	        		   } else{
	        			   tempActorUrl =  temp[1] + "=" + (((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getMeasurement() + "&" +temp[1] + "=" + formatter.format((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getTimestamp())+"&";
                           actorUrl.put(temp[1], tempActorUrl);
	        		   }
	        		    show = true;
	        	    } 
	        	  }
		 
		    // Test Url
	  //   statisticWebView.loadUrl("file:///android_asset/my_example.html?1=7&1=2011/02/07%2010:30:12&1=4&1=2011/02/07%2011:43:11&1=8&1=2011/02/07%2012:54:11&1=3&1=2011/02/07%2013:00:00&2=5&2=2011/02/07%2010:00:00&2=1&2=2011/02/07%2011:00:00&2=3&2=2011/02/07%2012:00:00&2=6&2=2011/02/07%2020:00:00");
	 //    statisticWebView.loadUrl("file:///android_asset/my_example.html?4444=284&4444=2011/02/07%2010:30:12");
	     if(show){
		        // Kanoniko
	    	 Set set = actorUrl.entrySet();  
	    	// Get an iterator 
	    	Iterator i = set.iterator(); 
	    	 String finalUrl = "";
	    	while(i.hasNext()) { 
	    		Map.Entry me = (Map.Entry)i.next(); 
	    		finalUrl = finalUrl + (String) me.getValue(); 
	    		} 

    		Log.d(" urly "," to urly = " + url + finalUrl);
    		statisticWebView.loadUrl(url+finalUrl.substring(0, finalUrl.length() - 1));
			/*    url = url.substring(0, url.length() - 1);
			    Log.d(" urly "," to urly = " + url);
			    statisticWebView.loadUrl(url);*/
		    } else{ 
	        	statisticWebView.setVisibility(View.GONE);
	        	builder = new AlertDialog.Builder(this);
	        	builder
				.setMessage(" No Relevant data ")
				.setPositiveButton("Yes", null);
	        	 info = builder.create();
	        	 info.show();
		    } 
	     } else{
	    	 builder = new AlertDialog.Builder(this);
	        	builder
				.setMessage(" EventAdmin is not Active. ")
				.setPositiveButton("Ok", null);
	        	 info = builder.create();
	        	 info.show();
	     }
	       
	  //	handler.post(mUpdate);
	}
	
	@Override
	protected void onPause(){
	   super.onPause();
	     if(info != null){
	    	   info.dismiss();
	     }
	   }

	
	/*public Runnable mUpdate = new Runnable() {
		public void run() {
			url = "file:///android_asset/my_example.html?";
			ESponderEntityDTO[] allStatistics = null;
			try {
				allStatistics = (ESponderEntityDTO[])Menu.client.call("esponderdb/getAllESponderDTOs", 
				          ESponderEntityDTO[].class, SensorMeasurementStatisticDTO.class.getName());
			} catch (JSONRPCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
			if(allStatistics != null){
				  for (int i = 0; i < allStatistics.length; i++) {
					  if(((SensorMeasurementStatisticDTO)allStatistics[i]).getStatistic() instanceof ArithmeticSensorMeasurementDTO){
						  measurements.add((SensorMeasurementStatisticDTO) allStatistics[i]);
					  }
				  }
			}
			
		  	if(measurements != null){
		  		if(countMeasurements < measurements.size()){ 
		  	Collections.sort(measurements,COMPARATOR);
		     for (int i = 0; i < measurements.size(); i++) {
		        	  if( (((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic().getSensor().getClass().isAssignableFrom(statistic.getClassName()
		        			  )) && ((SensorMeasurementStatisticDTO)measurements.get(i)).getStatisticType().equals(statistic.getStatisticType())) {
		        		    System.out.println("To id tou einai = "+ measurements.get(i).getId() +" All SENSORs FROM DB : " + (((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getMeasurement() + " kai sensor = "+(((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getSensor().getId()  + " tou Actor = " + Menu.actorSensorMap.get((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getSensor().getId()) + " Hmerominia = " + formatter.format((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getTimestamp()));       	        
		        		    temp = Menu.actorSensorMap.get((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getSensor().getId()).split("#");
		        		    url = url + temp[1] + "=" + (((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getMeasurement() + "&" +temp[1] + "=" + formatter.format((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getTimestamp())+"&";
		        		    show = true;
		        	    } 
		        	  }
		       countMeasurements = measurements.size();
		       url = url.substring(0, url.length() - 1);
			   Log.d(" urly "," to urly = " + url);
			   statisticWebView.loadUrl(url);
		  		}
		     
		  	}
		  		 
			handler.postDelayed(this, 1000);
		}
	};*/
	
	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			 Log.d(" we received "," we receivd");
			// Get the event
			  String incomingEvent = intent.getStringExtra("event");
			  ObjectMapper	mapper = new ObjectMapper();
			  mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
			  JsonFactory jsonFactory = new JsonFactory(); 
				  
	  	  CreateSensorMeasurementStatisticEvent event = null;
			  // deserialize incoming Event
			 try {
			      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
			      // get the event
			       event = mapper.readValue(jp, CreateSensorMeasurementStatisticEvent.class);
			       
					   List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticList =  event.getEventAttachment().getMeasurementStatistics();
					   for(int i =0 ; i < sensorMeasurementStatisticList.size() ; i++){
			              temp = Menu.actorSensorMap.get((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)sensorMeasurementStatisticList.get(i)).getStatistic())).getSensor().getId()).split("#");
			              String tempActorUrl = actorUrl.get(temp[1]);
		        		   if(tempActorUrl != null){
		        			   tempActorUrl = tempActorUrl  + temp[1] + "=" + (((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)sensorMeasurementStatisticList.get(i)).getStatistic())).getMeasurement() + "&" +temp[1] + "=" + formatter.format((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)sensorMeasurementStatisticList.get(i)).getStatistic())).getTimestamp())+"&";
		        			   actorUrl.put(temp[1], tempActorUrl);
		        		   } else{
		        			   tempActorUrl =  temp[1] + "=" + (((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getMeasurement() + "&" +temp[1] + "=" + formatter.format((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)measurements.get(i)).getStatistic())).getTimestamp())+"&";
	                           actorUrl.put(temp[1], tempActorUrl); 
		        		   }
		        		 
		        		 
		        		 Set set = actorUrl.entrySet(); 
		       	    	// Get an iterator 
		       	    	Iterator it = set.iterator(); 
		       	    	 String finalUrl = "";
		       	    	while(it.hasNext()) { 
		       	    		Map.Entry me = (Map.Entry)it.next(); 
		       	    		finalUrl = finalUrl + (String) me.getValue(); 
		       	    		} 

		           		Log.d(" To incoimeing url  "," To incoimeing url = " + url + finalUrl);
		           		statisticWebView.loadUrl(url + finalUrl.substring(0, finalUrl.length() - 1));
		           		
		        	/*	  url = url.substring(0, url.length() - 1); 
		        		  Log.d("To incoimeing url "," To incoimeing url "+url);
		    			  statisticWebView.loadUrl(url);*/
					   }
					   
			  } catch (Exception e) {
			      e.printStackTrace();
			    }
  
      	}
	}
	
	
/*	
	public Runnable mUpdate = new Runnable() {
		public void run() {
			url="file:///android_asset/my_example.html?1=7&1=2011/02/07%2010:00:00&1=4&1=2011/02/07%2011:00:00&1=8&1=2011/02/07%2012:00:00&1=3&1=2011/02/07%2013:00:00&2=5&2=2011/02/07%2010:00:00&2=1&2=2011/02/07%2011:00:00&2=3&2=2011/02/07%2012:00:00&2=6&2=2011/02/07%2013:00:00&2=9&2=2011/02/07%2014:00:00";
	 			dailyWebView.loadUrl(url);
			//handler.postDelayed(this, 1000);
		}
	};
	
	public Runnable mUpdate2 = new Runnable() {
		public void run() {
			url="file:///android_asset/my_example.html?1=7&1=2011/02/07%2010:00:00&1=4&1=2011/02/07%2011:00:00&1=8&1=2011/02/07%2012:00:00&1=3&1=2011/02/07%2013:00:00&2=5&2=2011/02/07%2010:00:00&2=1&2=2011/02/07%2011:00:00&2=3&2=2011/02/07%2012:00:00&2=6&2=2011/02/07%2013:00:00&2=9&2=2011/02/07%2014:00:00&2=3&2=2011/02/07%2015:00:00";
				dailyWebView.loadUrl(url);
		//handler.postDelayed(this, 1000);
		}
	};*/
	
	// Sort SensorMeasurementStatisticDTO by Sensor id
		 private static Comparator<SensorMeasurementStatisticDTO> COMPARATOR = new Comparator<SensorMeasurementStatisticDTO>()
				    {
				     
						@Override
						public int compare(SensorMeasurementStatisticDTO o1, SensorMeasurementStatisticDTO o2) {
							 return (int) ((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)o1).getStatistic())).getSensor().getId()  - (((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)o2).getStatistic())).getSensor().getId() );
						}
				    };
				    
 public void onDestroy() {
	 super.onDestroy();
	  unregisterReceiver(receiver); 
	 }			    
}