package com.esponder.apps;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List; 
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point; 
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class Routing extends MapActivity {

	LinearLayout linearLayout;
	MapView mapView;
	private Road mRoad;
	double fromLat , fromLon , toLat , toLon ;
    Bitmap  from,to;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		Intent routingIntent = getIntent();
 
		if (routingIntent != null) {
			Bundle b = routingIntent.getBundleExtra("routing");
			if (b != null) {
				fromLat = b.getInt("FromLat")/ 1E6;
				fromLon = b.getInt("FromLong")/ 1E6;
				toLat = b.getInt("ToLat")/ 1E6;
				toLon = b.getInt("ToLong")/ 1E6;
				Log.d("Einai :",fromLat+":"+fromLon+":"+toLat+":"+toLon);
 			}
		}

		new Thread() {
			@Override
			public void run() { // 37.990007,23.768663
				//double fromLat = 37.988756, fromLon = 23.767623, toLat = 37.990007, toLon = 23.768663;
				String url = RoadProvider
						.getUrl(fromLat, fromLon, toLat, toLon);
				InputStream is = getConnection(url);
				mRoad = RoadProvider.getRoute(is);
				mHandler.sendEmptyMessage(0);
			}
		}.start();
	}

	Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			TextView textView = (TextView) findViewById(R.id.description);
			textView.setText(mRoad.mName + " " + mRoad.mDescription);
			MapOverlay mapOverlay = new MapOverlay(Routing.this,mRoad, mapView);
			List<Overlay> listOfOverlays = mapView.getOverlays();
			listOfOverlays.clear();
			listOfOverlays.add(mapOverlay);
			mapView.invalidate();
		};
	};

	private InputStream getConnection(String url) {
		InputStream is = null;
		try {
			URLConnection conn = new URL(url).openConnection();
			is = conn.getInputStream();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return is;
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
}

class MapOverlay extends com.google.android.maps.Overlay {
	Road mRoad;
	ArrayList<GeoPoint> mPoints;
	Context context;
	Bitmap from,to;
	public MapOverlay(Context context,Road road, MapView mv) {
		this.context = context;
		mRoad = road;
		if (road.mRoute.length > 0) {
			mPoints = new ArrayList<GeoPoint>();
			for (int i = 0; i < road.mRoute.length; i++) {
				mPoints.add(new GeoPoint((int) (road.mRoute[i][1] * 1000000),
						(int) (road.mRoute[i][0] * 1000000)));
			}
			int moveToLat = (mPoints.get(0).getLatitudeE6() + (mPoints.get(
					mPoints.size() - 1).getLatitudeE6() - mPoints.get(0)
					.getLatitudeE6()) / 2);
			int moveToLong = (mPoints.get(0).getLongitudeE6() + (mPoints.get(
					mPoints.size() - 1).getLongitudeE6() - mPoints.get(0)
					.getLongitudeE6()) / 2);
			GeoPoint moveTo = new GeoPoint(moveToLat, moveToLong);

			MapController mapController = mv.getController();
			mapController.animateTo(moveTo);
			mapController.setZoom(16);

		}
	}

	@Override
	public boolean draw(Canvas canvas, MapView mv, boolean shadow, long when) {
		super.draw(canvas, mv, shadow); 
		drawPath(mv, canvas);
		return true;
	}

	public void drawPath(MapView mv, Canvas canvas) {
		int x1 = -1, y1 = -1, x2 = -1, y2 = -1,x0 = 0,y0 = 0;
		Paint paint = new Paint();
		paint.setColor(Color.GREEN);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(3);
		for (int i = 0; i < mPoints.size(); i++) {
			Point point = new Point();
			mv.getProjection().toPixels(mPoints.get(i), point);
			x2 = point.x;
			y2 = point.y;
			if (i > 0) {
				canvas.drawLine(x1, y1, x2, y2, paint);
			}else{
				x0 =  point.x;
				y0 = point.y;
			}
			x1 = x2;
			y1 = y2;
		} 
		 
		from = BitmapFactory.decodeResource(context.getResources(), R.drawable.marker2);    
		to = BitmapFactory.decodeResource(context.getResources(), android.R.drawable.ic_menu_myplaces);    
	 
		canvas.drawBitmap(from,x0, y0, null);  
		canvas.drawBitmap(to, x1, y1, null);  
		Log.d("OOOOXXQQQQ","SFFFWE "+x1+" kai "+y1);
		
	/*	Paint paint2 = new Paint();
		paint2.setColor(Color.RED);
		paint2.setStyle(Paint.Style.STROKE);
		paint2.setStrokeWidth(3);
		canvas.draw .drawPoint(x1, y1, paint2) ; */
	}
	
	
}