package com.esponder.apps;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import eu.esponder.fru.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.EnvironmentTemperatureSensorDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.GasSensorDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.HeartBeatRateSensorDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TimePicker;

public class MeasurementSelection extends Activity {

	private Spinner measurement,timeFrom,timeTo; 
	private Button btnSubmit;
	
	private int mHourFrom;
	private int mMinuteFrom;

	private int mHourTo;
	private int mMinuteTo;

	static final int TIME_DIALOG_FROM = 0;
	static final int TIME_DIALOG_TO = 1;
    int last,last2;
	boolean flag = true;
	List<String> list2,list3;
	ArrayAdapter<String> dataAdapter2,dataAdapter3;
	SimpleDateFormat format;
	Date date30 ;
	Date date60 ;
	Date date90 ;
	Date date120 ;
	Statistic statistic;
	 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.measurementselection);
		list2 = new ArrayList<String>();
		format = new SimpleDateFormat("HH:mm");

			// get the current time
			final Calendar c = Calendar.getInstance();
			mHourFrom = c.get(Calendar.HOUR_OF_DAY);
			mMinuteFrom = c.get(Calendar.MINUTE);
			date30 = new Date(System.currentTimeMillis() - (30 * 60 * 1000));
			date60 = new Date(System.currentTimeMillis() - ( 60 * 60 * 1000));
			date90 = new Date(System.currentTimeMillis() - (90 * 60 * 1000));
			date120 = new Date(System.currentTimeMillis() - (120 * 60 * 1000));
			 
	 
			mHourTo = c.get(Calendar.HOUR_OF_DAY);
			mMinuteTo = c.get(Calendar.MINUTE);
		    btnSubmit = (Button) findViewById(R.id.btnSubmit);

	     	btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				 try {
					 Date  datefrom = format.parse(mHourFrom + ":" + mMinuteFrom);
					 Date dateto ;
					 if(timeTo.getSelectedItem().toString().equals("Now")){
 						    dateto = format.parse(format.format(new Date(System.currentTimeMillis())));
 							Log.d("Pwwpwpwpw","To!! pwpwpwp"+format.format(dateto));
					 }
					 else{
          		    	  dateto = format.parse(mHourTo + ":" + mMinuteTo);
					 }
				    
				    int count = datefrom.compareTo(dateto);
				    if(count==-1){
					 		Intent openSodiumMeasurements = new Intent(MeasurementSelection.this,
					     	ShowStatistic.class);
					 		statistic = getClassTypeDTO((int) measurement.getSelectedItemId());
					 		statistic.setMeasurementTitle(measurement.getSelectedItem().toString());
					 		statistic.setFrom(format.format(datefrom));
					 		statistic.setTo(format.format(dateto));
			            	
					 		 ObjectMapper  mapper = new ObjectMapper();
					 	     mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
					         mapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
					         String jsonStatistic = null ;
					 	     mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					          try {
					        	  jsonStatistic = mapper.writeValueAsString(statistic); 
					  		} catch (JsonGenerationException e) {
					  			// TODO Auto-generated catch block
					  			e.printStackTrace();
					  		} catch (JsonMappingException e) {
					  			// TODO Auto-generated catch block
					  			e.printStackTrace();
					  		} catch (IOException e) {
					  			// TODO Auto-generated catch block
					  			e.printStackTrace();
					  		}
				            openSodiumMeasurements.putExtra("statistic", jsonStatistic);
				            startActivity(openSodiumMeasurements);
 
				    }
				    else if(count==1 || count==0){
				    	AlertDialog alertDialog = new AlertDialog.Builder(MeasurementSelection.this).create();
				    	alertDialog.setTitle("Tile");
				    	alertDialog.setMessage("Please Check Time Again");
				    	alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				    	      public void onClick(DialogInterface dialog, int which) {			    	 
				    	       //here you can add functions			    	 
				    	    } });
 				    	alertDialog.show();
				    }
				    Log.d("Pwwpwpwpw",format.format(datefrom));
					 Log.d("Pwwpwpwpw",format.format(dateto));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
 
				  
			}

		});

	 
		addItemsOnSpinner();
		 
	}

	// updates the time we display in the TextView
	private void updateDisplay(int id) {
		switch (id) {
		case 0:
			// timeFrom.setText("From :"
			// + new StringBuilder().append(pad(mHourFrom)).append(":")
			// .append(pad(mMinuteFrom)));
			break;
		case 1:
		//	timeTo.setText("To :"
		//			+ new StringBuilder().append(pad(mHourTo)).append(":")
		//					.append(pad(mMinuteTo)));
			break;
		}
	}

	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	// the callback received when the user "sets" the time in the dialog
	private TimePickerDialog.OnTimeSetListener mTimeSetListenerFrom = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		
			mHourFrom = hourOfDay;
			mMinuteFrom = minute;
	 		list2.clear();
			list2.add(format.format(date120));
			list2.add(format.format(date90));
			list2.add(format.format(date60));
			list2.add(format.format(date30));
			list2.add("Select");
			list2.add(mHourFrom + ":" + mMinuteFrom);
			dataAdapter2.notifyDataSetChanged();
			timeFrom.setSelection(5);
		 

		}

	};

	// the callback received when the user "sets" the time in the dialog
	private TimePickerDialog.OnTimeSetListener mTimeSetListenerTo = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			mHourTo = hourOfDay;
			mMinuteTo = minute;
	 		list3.clear();
	 		list3.add(format.format(date90));
			list3.add(format.format(date60));
			list3.add(format.format(date30));
			list3.add("Now");
			list3.add("Select");
			list3.add(mHourTo + ":" + mMinuteTo);
			dataAdapter3.notifyDataSetChanged();
			timeTo.setSelection(5);
		}
	};

	// add items into spinner dynamically
	public void addItemsOnSpinner() {

		measurement = (Spinner) findViewById(R.id.measurement);
		List<String> list1 = new ArrayList<String>(); 
		list1.add("Body Temp AVG");
		list1.add("Body Temp MIN");
		list1.add("Body Temp MAX"); 
		list1.add("Environment Temp AVG");
		list1.add("Environment Temp MIN");
		list1.add("Environment Temp MAX"); 
		list1.add("GAS AVG");
		list1.add("GAS MIN");
		list1.add("GAS MAX"); 
		list1.add("Heart Beat Rate AVG");
		list1.add("Heart Beat Rate MIN");
		list1.add("Heart Beat Rate MAX");
		ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list1);
		dataAdapter1
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		measurement.setAdapter(dataAdapter1);

		timeFrom = (Spinner) findViewById(R.id.timeFrom);
		list2 = new ArrayList<String>();

		list2.add(format.format(date120));
		list2.add(format.format(date90));
		list2.add(format.format(date60));
		list2.add(format.format(date30));
		list2.add("Select");
		dataAdapter2 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list2);
		dataAdapter2
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		timeFrom.setAdapter(dataAdapter2);
		timeFrom.setSelection(list2.size()-2);
		last=list2.size()-2;

		timeFrom
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> adapterView,
							View view, int i, long l) {
						String Text = timeFrom.getSelectedItem().toString();
						dataAdapter2.notifyDataSetChanged();
						if(!Text.equals("Select")){
							last=i;
							String[] value = timeFrom.getSelectedItem().toString().split(":");
							mHourFrom = Integer.parseInt(value[0]);
							mMinuteFrom =  Integer.parseInt(value[1]);
	 						 
						}
						else {
 							showDialog(TIME_DIALOG_FROM);
							Log.d("TimeTimeTime", mHourFrom + ":" + mMinuteFrom);
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub
					}
				});
		
		
	 
				timeTo= (Spinner) findViewById(R.id.timeTo);
				list3 = new ArrayList<String>();
				list3.add(format.format(date90));
				list3.add(format.format(date60));
				list3.add(format.format(date30));
				list3.add("Now");
				list3.add("Select");
				dataAdapter3 = new ArrayAdapter<String>(this,
						android.R.layout.simple_spinner_item, list3);
				dataAdapter3
						.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				timeTo.setAdapter(dataAdapter3);
				timeTo.setSelection(list3.size()-2);
				last2=list3.size()-2;

				timeTo
						.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
							public void onItemSelected(AdapterView<?> adapterView,
									View view, int i, long l) {
								String Text = timeTo.getSelectedItem().toString();
								dataAdapter3.notifyDataSetChanged();
								if(!Text.equals("Select")){
									last2=i;
									if(!Text.equals("Now")){
									String[] value = timeTo.getSelectedItem().toString().split(":");
									mHourTo = Integer.parseInt(value[0]);
									mMinuteTo =  Integer.parseInt(value[1]);
									}
 							}
								else {
		 							showDialog(TIME_DIALOG_TO);
								}
							}

							@Override
							public void onNothingSelected(AdapterView<?> arg0) {
								// TODO Auto-generated method stub
							}
						});

	}
	
 
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case TIME_DIALOG_FROM:
			TimePickerDialog a = new TimePickerDialog(this,
					mTimeSetListenerFrom, mHourFrom, mMinuteFrom, true);
		 	a.setButton2("Cancel", new DialogInterface.OnClickListener() {
			      public void onClick(DialogInterface dialog, int which) {
			    	  timeFrom.setSelection(last);
			    	 
			       } });

 
			return a;

		case TIME_DIALOG_TO:
			TimePickerDialog b = new TimePickerDialog(this,
					mTimeSetListenerTo, mHourTo, mMinuteTo, true);
		 	b.setButton2("Cancel", new DialogInterface.OnClickListener() {
			      public void onClick(DialogInterface dialog, int which) {
			    	  timeTo.setSelection(last2);
			    	 
			       } });
			
			return b;
		}
		return null;
	}
	
	public Statistic getClassTypeDTO(int value){
		Statistic statistic = new Statistic();;
		
		switch(value){
		
		case 0: 
			statistic.setClassName(BodyTemperatureSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MEAN); 
			break;
		case 1: 
			statistic.setClassName(BodyTemperatureSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MINIMUM);  
			break;
		case 2: 
			statistic.setClassName(BodyTemperatureSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MAXIMUM);  
			break;
		case 3: 
			statistic.setClassName(EnvironmentTemperatureSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MEAN);  
			break;
		case 4: 
			statistic.setClassName(EnvironmentTemperatureSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MINIMUM);  
			break;
		case 5: 
			statistic.setClassName(EnvironmentTemperatureSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MAXIMUM);  
			break;
		case 6: 
			statistic.setClassName(GasSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MEAN);  
			break;
		case 7: 
			statistic.setClassName(GasSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MINIMUM);  
			break;
		case 8: 
			statistic.setClassName(GasSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MAXIMUM);  
			break;
		case 9: 
			statistic.setClassName(HeartBeatRateSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MEAN);  
			break;
		case 10: 
			statistic.setClassName(HeartBeatRateSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MINIMUM);  
			break;
		case 11: 
			statistic.setClassName(HeartBeatRateSensorDTO.class);
			statistic.setStatisticType(MeasurementStatisticTypeEnum.MAXIMUM);  
			break;
 		}
		return statistic;
		
	}

}
