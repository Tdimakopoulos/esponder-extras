/*
 * 
 */
package eu.esponder.df.ruleengine.utilities.events;

import java.io.Serializable;
import java.util.Date;

import javax.naming.NamingException;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.df.ruleengine.utilities.ruleresults.RuleResults;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.CreateSensorSnapshotEvent;
import eu.esponder.event.model.snapshot.resource.UpdateActorSnapshotEvent;
import eu.esponder.eventadmin.ESponderEventPublisher;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.test.ResourceLocator;
import eu.esponder.util.ejb.ServiceLocator;



// TODO: Auto-generated Javadoc
/**
 * The Class RuleEvents.
 */
public class RuleEvents implements Serializable {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5651145416673918122L;

	/**
	 * Gets the sensor service.
	 *
	 * @return the sensor service
	 */
	protected SensorRemoteService getSensorService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}
	
	
	// Add rule events functions into the RuleEvents Class
	/**
	 * Publish event.
	 *
	 * @param event the event
	 */
	public void PublishEvent(ESponderEvent<ESponderEntityDTO> event)
	{
		

	}


	/**
	 * Publish update sensor snapshot event.
	 *
	 * @param sensorSnapshot the sensor snapshot
	 * @param szmessage the szmessage
	 */
	public void publishUpdateSensorSnapshotEvent(SensorSnapshotDTO sensorSnapshot,String szmessage) {
		CreateSensorSnapshotEvent event = new CreateSensorSnapshotEvent();
		event.setEventAttachment(sensorSnapshot);
		event.setEventSeverity(SeverityLevelDTO.MEDIUM);
		SensorDTO sensor = this.getSensorService().findSensorByIdRemote(sensorSnapshot.getSensor());
		event.setEventSource(sensor);
		event.setEventTimestamp(new Date());
		event.setJournalMessage(szmessage);
		System.out.println("DF : Publish Datafusion Event (SensorSnapshotUpdate)");
		RuleResults pResults = new RuleResults();
		pResults.RuleResult("Sensor Snapshot Rule Set", szmessage, sensorSnapshot.getValue(), sensor.getTitle());
		ESponderEventPublisher<CreateSensorSnapshotEvent> publisher = new ESponderEventPublisher<CreateSensorSnapshotEvent>(CreateSensorSnapshotEvent.class);
		try {
			publisher.publishEvent(event);
		} catch (EventListenerException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			System.out.println("Error on EventAdmin on Publish SensorSnapshot Event - Event Type : UPDATE. Reason : "+e.getMessage());
		}
		publisher.CloseConnection();

	}

	/**
	 * Publish update actor snasphot event.
	 *
	 * @param actorSnapshot the actor snapshot
	 * @param szmessage the szmessage
	 */
	
	public void publishUpdateActorSnasphotEvent(ActorSnapshotDTO actorSnapshot,String szmessage) {
		
		GenericRemoteService genericService = ResourceLocator.lookup("esponder/ActorBean/remote");
		
		FirstResponderActorDTO actorP = null;
		try {
			actorP = (FirstResponderActorDTO) genericService.getEntityDTO(FirstResponderActorDTO.class, actorSnapshot.getActor());
		} catch (ClassNotFoundException e1) {
			new EsponderCheckedException(this.getClass(),
					"Datafusion Exception : " + e1.getMessage());
		}
		
		if(actorP != null) {
			
			UpdateActorSnapshotEvent event = new UpdateActorSnapshotEvent();
			event.setEventAttachment(actorSnapshot);
			event.setEventSeverity(SeverityLevelDTO.MEDIUM);
			event.setEventSource(actorP);
			event.setEventTimestamp(new Date());
			event.setJournalMessage(szmessage);
			System.out.println("DF : Publish Datafusion Event (ActorSnapshotUpdate)");
			RuleResults pResults = new RuleResults();
			
			pResults.RuleResult("Actor Snapshot Rule Set", szmessage, actorP.getTitle(), actorSnapshot.getLocationArea().toString());
			ESponderEventPublisher<UpdateActorSnapshotEvent> publisher = new ESponderEventPublisher<UpdateActorSnapshotEvent>(UpdateActorSnapshotEvent.class);
			try {
				publisher.publishEvent(event);
			} catch (EventListenerException e) {
				System.out.println("Error on EventAdmin on Publish ActorSnapshot Event - Event Type : UPDATE. Reason : "+e.getMessage());
			}
			publisher.CloseConnection();
			
		}	
		
	}
}
