/*
 * 
 */
package eu.esponder.df.ruleengine.utilities.actorlookup;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorXMLFileEntry.
 */
@Root
public class ActorXMLFileEntry {

	// operation
	/** The sz string1. */
	@Element
	private String szString1;

	// action
	/** The sz string2. */
	@Element
	private String szString2;

	// actor
	/** The sz string3. */
	@Element
	private String szString3;

	// place name
	/** The sz string4. */
	@Element
	private String szString4;

	// date and time
	/** The sz string5. */
	@Element
	private String szString5;

	/** The sz string6. */
	@Element
	private String szString6;

	/** The i int1. */
	@Element
	private int iInt1;
	
	/** The i int2. */
	@Element
	private int iInt2;
	
	/** The i int3. */
	@Element
	private int iInt3;
	
	/** The i int4. */
	@Element
	private int iInt4;
	
	/** The i int5. */
	@Element
	private int iInt5;
	
	/** The i int6. */
	@Element
	private int iInt6;

	// lat
	/** The d double1. */
	@Element
	private double dDouble1;

	// lot
	/** The d double2. */
	@Element
	private double dDouble2;

	// alt
	/** The d double3. */
	@Element
	private double dDouble3;

	// radius
	/** The d double4. */
	@Element
	private double dDouble4;
	
	/** The d double5. */
	@Element
	private double dDouble5;
	
	/** The d double6. */
	@Element
	private double dDouble6;

	/**
	 * Gets the sz string3.
	 *
	 * @return the sz string3
	 */
	public String getSzString3() {
		return szString3;
	}

	/**
	 * Sets the sz string3.
	 *
	 * @param szString3 the new sz string3
	 */
	public void setSzString3(String szString3) {
		this.szString3 = szString3;
	}

	/**
	 * Gets the sz string4.
	 *
	 * @return the sz string4
	 */
	public String getSzString4() {
		return szString4;
	}

	/**
	 * Sets the sz string4.
	 *
	 * @param szString4 the new sz string4
	 */
	public void setSzString4(String szString4) {
		this.szString4 = szString4;
	}

	/**
	 * Gets the sz string5.
	 *
	 * @return the sz string5
	 */
	public String getSzString5() {
		return szString5;
	}

	/**
	 * Sets the sz string5.
	 *
	 * @param szString5 the new sz string5
	 */
	public void setSzString5(String szString5) {
		this.szString5 = szString5;
	}

	/**
	 * Gets the sz string6.
	 *
	 * @return the sz string6
	 */
	public String getSzString6() {
		return szString6;
	}

	/**
	 * Sets the sz string6.
	 *
	 * @param szString6 the new sz string6
	 */
	public void setSzString6(String szString6) {
		this.szString6 = szString6;
	}

	/**
	 * Gets the i int3.
	 *
	 * @return the i int3
	 */
	public int getiInt3() {
		return iInt3;
	}

	/**
	 * Sets the i int3.
	 *
	 * @param iInt3 the new i int3
	 */
	public void setiInt3(int iInt3) {
		this.iInt3 = iInt3;
	}

	/**
	 * Gets the i int4.
	 *
	 * @return the i int4
	 */
	public int getiInt4() {
		return iInt4;
	}

	/**
	 * Sets the i int4.
	 *
	 * @param iInt4 the new i int4
	 */
	public void setiInt4(int iInt4) {
		this.iInt4 = iInt4;
	}

	/**
	 * Gets the i int5.
	 *
	 * @return the i int5
	 */
	public int getiInt5() {
		return iInt5;
	}

	/**
	 * Sets the i int5.
	 *
	 * @param iInt5 the new i int5
	 */
	public void setiInt5(int iInt5) {
		this.iInt5 = iInt5;
	}

	/**
	 * Gets the i int6.
	 *
	 * @return the i int6
	 */
	public int getiInt6() {
		return iInt6;
	}

	/**
	 * Sets the i int6.
	 *
	 * @param iInt6 the new i int6
	 */
	public void setiInt6(int iInt6) {
		this.iInt6 = iInt6;
	}

	/**
	 * Gets the d double1.
	 *
	 * @return the d double1
	 */
	public double getdDouble1() {
		return dDouble1;
	}

	/**
	 * Sets the d double1.
	 *
	 * @param dDouble1 the new d double1
	 */
	public void setdDouble1(double dDouble1) {
		this.dDouble1 = dDouble1;
	}

	/**
	 * Gets the d double2.
	 *
	 * @return the d double2
	 */
	public double getdDouble2() {
		return dDouble2;
	}

	/**
	 * Sets the d double2.
	 *
	 * @param dDouble2 the new d double2
	 */
	public void setdDouble2(double dDouble2) {
		this.dDouble2 = dDouble2;
	}

	/**
	 * Gets the d double3.
	 *
	 * @return the d double3
	 */
	public double getdDouble3() {
		return dDouble3;
	}

	/**
	 * Sets the d double3.
	 *
	 * @param dDouble3 the new d double3
	 */
	public void setdDouble3(double dDouble3) {
		this.dDouble3 = dDouble3;
	}

	/**
	 * Gets the d double4.
	 *
	 * @return the d double4
	 */
	public double getdDouble4() {
		return dDouble4;
	}

	/**
	 * Sets the d double4.
	 *
	 * @param dDouble4 the new d double4
	 */
	public void setdDouble4(double dDouble4) {
		this.dDouble4 = dDouble4;
	}

	/**
	 * Gets the d double5.
	 *
	 * @return the d double5
	 */
	public double getdDouble5() {
		return dDouble5;
	}

	/**
	 * Sets the d double5.
	 *
	 * @param dDouble5 the new d double5
	 */
	public void setdDouble5(double dDouble5) {
		this.dDouble5 = dDouble5;
	}

	/**
	 * Gets the d double6.
	 *
	 * @return the d double6
	 */
	public double getdDouble6() {
		return dDouble6;
	}

	/**
	 * Sets the d double6.
	 *
	 * @param dDouble6 the new d double6
	 */
	public void setdDouble6(double dDouble6) {
		this.dDouble6 = dDouble6;
	}

	/**
	 * Gets the sz string1.
	 *
	 * @return the sz string1
	 */
	public String getSzString1() {
		return szString1;
	}

	/**
	 * Sets the sz string1.
	 *
	 * @param szString1 the new sz string1
	 */
	public void setSzString1(String szString1) {
		this.szString1 = szString1;
	}

	/**
	 * Gets the sz string2.
	 *
	 * @return the sz string2
	 */
	public String getSzString2() {
		return szString2;
	}

	/**
	 * Sets the sz string2.
	 *
	 * @param szString2 the new sz string2
	 */
	public void setSzString2(String szString2) {
		this.szString2 = szString2;
	}

	/**
	 * Gets the i int1.
	 *
	 * @return the i int1
	 */
	public int getiInt1() {
		return iInt1;
	}

	/**
	 * Sets the i int1.
	 *
	 * @param iInt1 the new i int1
	 */
	public void setiInt1(int iInt1) {
		this.iInt1 = iInt1;
	}

	/**
	 * Gets the i int2.
	 *
	 * @return the i int2
	 */
	public int getiInt2() {
		return iInt2;
	}

	/**
	 * Sets the i int2.
	 *
	 * @param iInt2 the new i int2
	 */
	public void setiInt2(int iInt2) {
		this.iInt2 = iInt2;
	}

	/**
	 * Gets the operation.
	 *
	 * @return the operation
	 */
	public String getOperation() {
		return this.getSzString1();
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public String getAction() {
		return this.getSzString2();
	}

	/**
	 * Gets the actor.
	 *
	 * @return the actor
	 */
	public String getActor() {
		return this.getSzString3();
	}

	/**
	 * Gets the place name.
	 *
	 * @return the place name
	 */
	public String getPlaceName() {
		return this.getSzString4();
	}

	/**
	 * Gets the time stamp.
	 *
	 * @return the time stamp
	 */
	public String getTimeStamp() {
		return this.getSzString5();
	}

	/**
	 * Gets the lat.
	 *
	 * @return the lat
	 */
	public double getLat() {
		return this.getdDouble1();
	}

	/**
	 * Gets the lot.
	 *
	 * @return the lot
	 */
	public double getLot() {
		return this.getdDouble2();
	}

	/**
	 * Gets the alt.
	 *
	 * @return the alt
	 */
	public double getAlt() {
		return this.getdDouble3();
	}

	/**
	 * Gets the radius.
	 *
	 * @return the radius
	 */
	public double getRadius() {
		return this.getdDouble4();
	}
}
