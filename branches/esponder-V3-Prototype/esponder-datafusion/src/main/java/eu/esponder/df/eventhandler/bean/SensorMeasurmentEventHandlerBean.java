/*
 * 
 */
package eu.esponder.df.eventhandler.bean;

import javax.ejb.Stateless;

import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.df.eventhandler.SensorMeasurmentEventHandlerRemoteService;
import eu.esponder.df.eventhandler.SensorMeasurmentEventHandlerService;
import eu.esponder.df.ruleengine.utilities.ActorMovementValues;
import eu.esponder.df.ruleengine.utilities.RuleUtilities;
import eu.esponder.df.ruleengine.utilities.SensorTypeHelper;
import eu.esponder.df.rules.profile.ProfileManager;
import eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActorSnapshotStatusDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.test.ResourceLocator;


// TODO: Auto-generated Javadoc
/**
 * The Class SensorMeasurmentEventHandlerBean.
 */
@Stateless
public class SensorMeasurmentEventHandlerBean extends dfEventHandlerBean
		implements SensorMeasurmentEventHandlerService,
		SensorMeasurmentEventHandlerRemoteService {
      
	/* (non-Javadoc)
	 * @see eu.esponder.df.eventhandler.SensorMeasurmentEventHandlerService#ProcessEvent(eu.esponder.event.model.ESponderEvent)
	 */
	public void ProcessEvent(ESponderEvent<?> pEvent)
	{
		System.out.println("******* Sensor Snapshot Event Handler ***********");
		ProfileManager pProfileManager=new ProfileManager();
		
		CreateSensorSnapshot(pEvent);
		
		//Arithmetic sensors snapshots
		for (int i=0;i<createdSensorSnapshots.size();i++)
		{
			//Set rule profile for sensor measurement
			SetRuleEngineType(pProfileManager.GetProfileNameForSensor().getSzProfileType(),pProfileManager.GetProfileNameForSensor().getSzProfileName());
			SetDecisionTable(pProfileManager.GetProfileNameForSensor().getSzDecisionTable(), pProfileManager.GetProfileNameForSensor().isHasDecisionTable());
			//Load Rules
			LoadKnowledge();
			//Get current snapshot
			SensorSnapshotDTO createdSensorSnapshot = new SensorSnapshotDTO();
			createdSensorSnapshot.setId(createdSensorSnapshots.get(i).getId());
			createdSensorSnapshot.setPeriod(createdSensorSnapshots.get(i).getPeriod());
			createdSensorSnapshot.setSensor(createdSensorSnapshots.get(i).getSensor());
			createdSensorSnapshot.setStatisticType(createdSensorSnapshots.get(i).getStatisticType());
			createdSensorSnapshot.setStatus(createdSensorSnapshots.get(i).getStatus());
			createdSensorSnapshot.setValue(createdSensorSnapshots.get(i).getValue());
			RuleUtilities pUtils= new RuleUtilities();
			SensorTypeHelper pSensorType = new SensorTypeHelper();
			pSensorType.SetType(pUtils.CheckSensorType(createdSensorSnapshot.getSensor()));
			if(createdSensorSnapshot.getStatus()==SensorSnapshotStatusDTO.WORKING)
				pSensorType.SetWorking(1);
			else
				pSensorType.SetWorking(0);
			pSensorType.SetSensorID(createdSensorSnapshot.getSensor());
			
			//Add current snapshot
			AddDTOObjects(pSensorType);
			AddDTOObjects(createdSensorSnapshot);
			//Run Rules, this function automatically load the utility classes
			ProcessRules();
			//Empty the session
			ReinitializeEngine();
		}
		
		//Location Sensors snapshots
		for (int i=0;i<createdActorSnapshots.size();i++)
		{
			//Set rule profile for sensor measurement
			SetRuleEngineType(pProfileManager.GetProfileNameForAction().getSzProfileType(),pProfileManager.GetProfileNameForAction().getSzProfileName());
			SetDecisionTable(pProfileManager.GetProfileNameForAction().getSzDecisionTable(), pProfileManager.GetProfileNameForAction().isHasDecisionTable());
			//Load Rules
			LoadKnowledge();
			//Get current snapshot
			ActorSnapshotDTO createdActorSnapshot = new ActorSnapshotDTO();
			createdActorSnapshot.setId(createdActorSnapshots.get(i).getId());
			createdActorSnapshot.setActor(createdActorSnapshots.get(i).getActor());
			createdActorSnapshot.setLocationArea(createdActorSnapshots.get(i).getLocationArea());
			createdActorSnapshot.setPeriod(createdActorSnapshots.get(i).getPeriod());
			createdActorSnapshot.setStatus(createdActorSnapshots.get(i).getStatus());
			//Add current snapshot
			AddDTOObjects(createdActorSnapshot);
			GenericRemoteService genericService = ResourceLocator.lookup("esponder/ActorBean/remote");
			
			FirstResponderActorDTO actorP = null;
			try {
				actorP = (FirstResponderActorDTO) genericService.getEntityDTO(FirstResponderActorDTO.class, createdActorSnapshot.getActor());
			} catch (ClassNotFoundException e1) {
				new EsponderCheckedException(this.getClass(),
						"Datafusion Exception : " + e1.getMessage());
			}
			
			if(actorP != null) {
			String ActorName=actorP.getTitle();
			SphereDTO pSphere=(SphereDTO)createdActorSnapshot.getLocationArea();
			Double plog=pSphere.getCentre().getLongitude().doubleValue();
			Double plat=pSphere.getCentre().getLatitude().doubleValue();
			Double pAlt=pSphere.getCentre().getAltitude().doubleValue();
			Double pradius=pSphere.getRadius().doubleValue();
			ActorMovementValues pActorValues=new ActorMovementValues();
			pActorValues.setpActorname(ActorName);
			pActorValues.setPalt(pAlt);
			pActorValues.setPlat(plat);
			pActorValues.setPlog(plog);
			pActorValues.setPradius(pradius);
			AddDTOObjects(pActorValues);
			
			SensorTypeHelper pSensorType = new SensorTypeHelper();
			pSensorType.SetType(7);
			if(createdActorSnapshot.getStatus()==ActorSnapshotStatusDTO.ACTIVE)
				pSensorType.SetWorking(1);
			else
				pSensorType.SetWorking(0);
			
			//Add current snapshot
			AddDTOObjects(pSensorType);
			//Run Rules, this function automatically load the utility classes
			ProcessRules();
			//Empty the session
			ReinitializeEngine();
			}
		}
	}
	
	
	
}
