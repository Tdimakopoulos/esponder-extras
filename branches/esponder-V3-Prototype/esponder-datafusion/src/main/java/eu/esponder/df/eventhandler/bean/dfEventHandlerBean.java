/*
 * 
 */
package eu.esponder.df.eventhandler.bean;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.ejb.Stateless;

import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.df.actionmanager.location.ActionLocationManager;
import eu.esponder.df.eventhandler.dfEventHandlerRemoteService;
import eu.esponder.df.eventhandler.dfEventHandlerService;
import eu.esponder.df.ruleengine.core.DomainSpecificKnowledgeParser;
import eu.esponder.df.ruleengine.core.RuleEngineGuvnorAssets;
import eu.esponder.df.ruleengine.utilities.RuleLookup;
import eu.esponder.df.ruleengine.utilities.RuleStatistics;
import eu.esponder.df.ruleengine.utilities.RuleUtilities;
import eu.esponder.df.ruleengine.utilities.events.RuleEvents;
import eu.esponder.df.ruleengine.utilities.ruleresults.RuleResults;
import eu.esponder.df.statistic.Correlation;
import eu.esponder.df.statistic.Ratio;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.dto.model.snapshot.status.ActorSnapshotStatusDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.test.ResourceLocator;


// TODO: Auto-generated Javadoc
/**
 * The Class dfEventHandlerBean.
 */
@Stateless
public class dfEventHandlerBean implements dfEventHandlerRemoteService,
		dfEventHandlerService {

	/** The sz flow name. */
	private String szFlowName = null;

	/** The sz rule package name. */
	private String szRulePackageName = null;

	/** The sz dsl name. */
	private String szDSLName = null;

	/** The sz dslr name. */
	@SuppressWarnings("unused")
	private String szDSLRName = null;

	/** The d inference type. */
	private RuleEngineType dInferenceType = RuleEngineType.NONE;

	/** The created sensor snapshots. */
	List<SensorSnapshotDTO> createdSensorSnapshots = new ArrayList<SensorSnapshotDTO>();

	/** The created actor snapshots. */
	List<ActorSnapshotDTO> createdActorSnapshots = new ArrayList<ActorSnapshotDTO>();

	/** The actor service. */
	ActorRemoteService actorService = ResourceLocator
			.lookup("esponder/ActorBean/remote");

	/** The equipment service. */
	EquipmentRemoteService equipmentService = ResourceLocator
			.lookup("esponder/EquipmentBean/remote");

	/** The sensor service. */
	SensorRemoteService sensorService = ResourceLocator
			.lookup("esponder/SensorBean/remote");

	/** The generic service. */
	GenericRemoteService genericService = ResourceLocator
			.lookup("esponder/GenericBean/remote");

	/** The g decision table name. */
	String gDecisionTableName;

	/** The g has decision table. */
	Boolean gHasDecisionTable;

	/**
	 * The Enum RuleEngineType.
	 */
	public enum RuleEngineType {

		/** The none. */
		NONE,
		/** The drl rules. */
		DRL_RULES,
		/** The dsl rules. */
		DSL_RULES,
		/** The flow rules. */
		FLOW_RULES,
		/** The all package. */
		ALL_PACKAGE,
		/** The mix. */
		MIX
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Rule Engine Calls and Variables
	/** The d engine. */
	private RuleEngineGuvnorAssets dEngine = new RuleEngineGuvnorAssets();

	/** The ddsl. */
	private DomainSpecificKnowledgeParser ddsl = new DomainSpecificKnowledgeParser();

	/**
	 * Reinitialize engine.
	 */
	public void ReinitializeEngine() {
		dEngine = new RuleEngineGuvnorAssets();
		ddsl = new DomainSpecificKnowledgeParser();
	}

	/**
	 * Adds the services in drools.
	 */
	private void AddServicesInDrools() {
		AddObjects(sensorService);
		AddObjects(actorService);
		AddObjects(equipmentService);
		AddObjects(genericService);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.df.eventhandler.dfEventHandlerRemoteService#AddDTOObjects
	 * (java.lang.Object)
	 */
	public void AddDTOObjects(Object fact) {
		// System.out.println("Adding DTO Objetc");
		dEngine.InferenceEngineAddDTOObject(fact);
		ddsl.AddDTOObject(fact);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.df.eventhandler.dfEventHandlerRemoteService#AddObjects(java
	 * .lang.Object)
	 */
	public void AddObjects(Object fact) {
		dEngine.InferenceEngineAddObject(fact);
		ddsl.AddObject(fact);
	}

	/**
	 * Adds the knowledge by category.
	 * 
	 * @param szCategory
	 *            the sz category
	 */
	private void AddKnowledgeByCategory(String szCategory) {
		try {
			dEngine.InferenceEngineAddKnowledgeForCategory(szCategory);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Category");
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			
		}
		ddsl.AddKnowledgeForCategory(szCategory);
	}

	/**
	 * Adds the decision table.
	 * 
	 * @param szDecisionTable
	 *            the sz decision table
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	private void AddDecisionTable(String szDecisionTable)
			throws FileNotFoundException {
		dEngine.AddDecisionTable(szDecisionTable);
		ddsl.AddDecisionTable(szDecisionTable);
	}

	/**
	 * Adds the knowledge by package.
	 * 
	 * @param szPackage
	 *            the sz package
	 */
	private void AddKnowledgeByPackage(String szPackage) {
		try {
			dEngine.InferenceEngineAddKnowledgeForPackage(szPackage);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Package");
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			
		}
		ddsl.AddKnowledgeForPackage(szPackage);
	}

	/**
	 * Execute rules.
	 */
	private void ExecuteRules() {
		// run simple drl rules
		dEngine.InferenceEngineRunAssets();

	}

	/**
	 * Execute flow.
	 */
	private void ExecuteFlow() {
		// run all processes
		ddsl.ExecuteFlow();

		// run dslr rules based on dsl and
		ddsl.RunRules();

		// close session only if all processes has finished
		ddsl.Dispose();
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.df.eventhandler.dfEventHandlerRemoteService#LoadKnowledge()
	 */
	public void LoadKnowledge() {
		if (gHasDecisionTable) {
			try {
				AddDecisionTable(gDecisionTableName);
			} catch (FileNotFoundException e) {
				System.out.println("Error on Adding Decision Table");
				new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
				
			}
		}
		// System.out.println("Load Knowledge Start -- Type :"+RuleEngineType.ALL_PACKAGE+" Name : "+szRulePackageName);
		if (this.dInferenceType == RuleEngineType.ALL_PACKAGE) {
			// System.out.println("Load Package : "+szRulePackageName);
			AddKnowledgeByPackage(szRulePackageName);
		}

		if (this.dInferenceType == RuleEngineType.DRL_RULES) {
			// System.out.println("Load Category : "+szRulePackageName);
			AddKnowledgeByCategory(szRulePackageName);
		}

		if (this.dInferenceType == RuleEngineType.FLOW_RULES) {
			AddKnowledgeByCategory(szFlowName);
		}

		if (this.dInferenceType == RuleEngineType.DSL_RULES) {
			AddKnowledgeByCategory(szDSLName);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.df.eventhandler.dfEventHandlerRemoteService#ProcessRules()
	 */
	public void ProcessRules() {
		// //////////////////////////////////////////////////////////////////////
		// Load Default Objects into drools
		// RuleUtils
		// MathUtils
		// EventUtils
		// RuleResultsUtils
		RuleStatistics pStatistics = new RuleStatistics();
		RuleUtilities pUtils = new RuleUtilities();
		RuleResults pResults = new RuleResults();
		RuleEvents pEvents = new RuleEvents();
		RuleLookup pLookup = new RuleLookup();
		Correlation pCorrelation = new Correlation();
		Ratio pRate = new Ratio();
		ActionLocationManager pActionManager = new ActionLocationManager();

		AddObjects(pCorrelation);
		AddObjects(pRate);
		AddObjects(pActionManager);
		AddObjects(pStatistics);
		AddObjects(pUtils);
		AddObjects(pResults);
		AddObjects(pEvents);
		AddObjects(pLookup);

		// load services into working memory
		AddServicesInDrools();
		// //////////////////////////////////////////////////////////////////////

		// Execute Rules using package, DRL,DSL,DSLR,Flow
		if (this.dInferenceType == RuleEngineType.ALL_PACKAGE) {
			ExecuteFlow();
			ExecuteRules();
		}

		// Execute Rules using Mix, DRL,DSL,DSLR,Flow
		if (this.dInferenceType == RuleEngineType.MIX) {
			ExecuteFlow();
			ExecuteRules();
		}

		// Execute Rules using DRL
		if (this.dInferenceType == RuleEngineType.DRL_RULES) {
			ExecuteRules();
		}

		// Execute Rules using DSL
		if (this.dInferenceType == RuleEngineType.DSL_RULES) {
			ExecuteFlow();
		}

		// Execute Rules using Flow or/and DRSR
		if (this.dInferenceType == RuleEngineType.FLOW_RULES) {
			ExecuteFlow();
		}
	}

	
	/**
	 * Creates the snapshots from envelope.
	 * 
	 * @param envelope
	 *            the envelope
	 * @param userID
	 *            the user id
	 * @return the list
	 */
	private List<Object> createSnapshotsFromEnvelope(
			SensorMeasurementStatisticEnvelopeDTO envelope, Long userID) {
		createdSensorSnapshots.clear();
		createdActorSnapshots.clear();
		if (sensorService != null) {
			List<Object> createdSnapshots = new ArrayList<Object>();
			for (SensorMeasurementStatisticDTO statistic : envelope
					.getMeasurementStatistics()) {
				if (statistic.getStatistic() instanceof ArithmeticSensorMeasurementDTO) {
					SensorSnapshotDTO snapshotDTO = transformArithmeticMeasurementToSnapshot(statistic);
					if (snapshotDTO != null) {
						snapshotDTO = sensorService.createSensorSnapshotRemote(
								snapshotDTO, userID);
						if (snapshotDTO != null) {
							createdSnapshots.add(snapshotDTO);
							createdSensorSnapshots.add(snapshotDTO);

						}
					} else
						System.out
								.println("ArithmeticSensorSnapshot has not been persisted, empty snapshot found...");
				} else if (statistic.getStatistic() instanceof LocationSensorMeasurementDTO) {
					ActorSnapshotDTO actorSnapshotDTO = null;
					try {
						actorSnapshotDTO = transformLocationMeasurementToSnapshot(statistic);
					} catch (ClassNotFoundException e) {
						System.err
								.println("DF: Error - Create Snapshot For Actor");
						new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
						
					}
					if (actorSnapshotDTO != null) {
						actorSnapshotDTO = actorService
								.createActorSnapshotRemote(actorSnapshotDTO,
										userID);
						if (actorSnapshotDTO != null) {
							createdSnapshots.add(actorSnapshotDTO);
							createdActorSnapshots.add(actorSnapshotDTO);

						}
					} else
						System.out
								.println("SensorSnapshot has not been persisted, empty snapshot found...");
				}
			}
			// AddDTOObjects(createdSensorSnapshots);
			// AddDTOObjects(createdActorSnapshots);
			return createdSnapshots;
		} else {
			System.out.println("Cannot access SensorRemoteService...");
			return null;
		}

	}

	/**
	 * Transform arithmetic measurement to snapshot.
	 * 
	 * @param statistic
	 *            the statistic
	 * @return the sensor snapshot dto
	 */
	private SensorSnapshotDTO transformArithmeticMeasurementToSnapshot(
			SensorMeasurementStatisticDTO statistic) {
		ArithmeticSensorMeasurementDTO arithmeticMeasurementDTO = (ArithmeticSensorMeasurementDTO) statistic
				.getStatistic();
		SensorSnapshotDTO snapshot = new SensorSnapshotDTO();
		snapshot.setPeriod(statistic.getPeriod());
		snapshot.setSensor(statistic.getStatistic().getSensor().getId());
		snapshot.setStatisticType(statistic.getStatisticType());
		
		
		snapshot.setStatus(SensorSnapshotStatusDTO.WORKING);
		snapshot.setValue(arithmeticMeasurementDTO.getMeasurement().toString());
		return snapshot;
	}

	/**
	 * Transform location measurement to snapshot.
	 * 
	 * @param statistic
	 *            the statistic
	 * @return the actor snapshot dto
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 */
	private ActorSnapshotDTO transformLocationMeasurementToSnapshot(
			SensorMeasurementStatisticDTO statistic)
			throws ClassNotFoundException {
		LocationSensorMeasurementDTO locationMeasurementDTO = (LocationSensorMeasurementDTO) statistic
				.getStatistic();
		ActorSnapshotDTO snapshot = new ActorSnapshotDTO();
		snapshot.setPeriod(statistic.getPeriod());

		PointDTO point1 = locationMeasurementDTO.getPoint();

		SphereDTO sphere1 = new SphereDTO();
		sphere1.setCentre(point1);
		sphere1.setRadius(new BigDecimal(0));

		sphere1.setTitle(new Random().toString());
		SphereDTO sphere1p = (SphereDTO) genericService.createEntityRemote(
				sphere1, new Long(1));

		snapshot.setLocationArea(sphere1p);

		Long actorID = equipmentService.findByIdRemote(
				statistic.getStatistic().getSensor().getEquipmentId())
				.getActor();
		@SuppressWarnings("unused")
		ActorDTO actor = (ActorDTO) genericService.getEntityDTO(ActorDTO.class, actorID);

		snapshot.setStatus(ActorSnapshotStatusDTO.ACTIVE);
		return snapshot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.df.eventhandler.dfEventHandlerRemoteService#CreateSensorSnapshot
	 * (eu.esponder.event.model.ESponderEvent)
	 */
	public List<Object> CreateSensorSnapshot(ESponderEvent<?> pEvent) {

		SensorMeasurementStatisticEnvelopeDTO pobject = (SensorMeasurementStatisticEnvelopeDTO) pEvent
				.getEventAttachment();
		List<Object> pPersistedSnapshots = createSnapshotsFromEnvelope(pobject,
				new Long(1));
		return pPersistedSnapshots;
	}

	/**
	 * Sets the decision table.
	 * 
	 * @param DecisionTableName
	 *            the decision table name
	 * @param HasDecisionTable
	 *            the has decision table
	 */
	public void SetDecisionTable(String DecisionTableName,
			Boolean HasDecisionTable) {
		gDecisionTableName = DecisionTableName;
		gHasDecisionTable = HasDecisionTable;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.df.eventhandler.dfEventHandlerRemoteService#SetRuleEngineType
	 * (eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType,
	 * java.lang.String)
	 */
	public void SetRuleEngineType(RuleEngineType dType,
			String szPackageNameOrFlowName) {
		SetRuleType(dType, szPackageNameOrFlowName, null, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.df.eventhandler.dfEventHandlerRemoteService#SetRuleEngineType
	 * (eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType,
	 * java.lang.String, java.lang.String)
	 */
	public void SetRuleEngineType(RuleEngineType dType, String szDSLName,
			String szDSLRName) {
		SetRuleType(dType, null, szDSLName, szDSLRName);
	}

	/**
	 * Sets the rule type.
	 * 
	 * @param dType
	 *            the d type
	 * @param szPackageNameOrFlowName
	 *            the sz package name or flow name
	 * @param szDSLNamep
	 *            the sz dsl namep
	 * @param szDSLRNamep
	 *            the sz dslr namep
	 */
	private void SetRuleType(RuleEngineType dType,
			String szPackageNameOrFlowName, String szDSLNamep,
			String szDSLRNamep) {
		if (dType == RuleEngineType.DRL_RULES) {
			szRulePackageName = szPackageNameOrFlowName;
			SetInferenceType(dType);
		}
		if (dType == RuleEngineType.ALL_PACKAGE) {
			szRulePackageName = szPackageNameOrFlowName;
			dInferenceType = RuleEngineType.ALL_PACKAGE;
		}
		if (dType == RuleEngineType.FLOW_RULES) {
			szFlowName = szPackageNameOrFlowName;
			SetInferenceType(dType);
		}
		if (dType == RuleEngineType.DSL_RULES) {
			szDSLName = szDSLNamep;
			szDSLRName = szDSLRNamep;
			SetInferenceType(dType);
		}
	}

	/**
	 * Sets the inference type.
	 * 
	 * @param dType
	 *            the d type
	 */
	private void SetInferenceType(RuleEngineType dType) {
		if (dInferenceType == RuleEngineType.ALL_PACKAGE) {
		} else {
			if (dInferenceType == RuleEngineType.NONE)
				dInferenceType = dType;
			else
				dInferenceType = RuleEngineType.MIX;
		}
	}
}
