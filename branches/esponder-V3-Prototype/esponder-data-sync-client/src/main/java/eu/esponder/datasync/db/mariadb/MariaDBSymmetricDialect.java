package eu.esponder.datasync.db.mariadb;

import org.jumpmind.db.platform.IDatabasePlatform;
import org.jumpmind.symmetric.service.IParameterService;

import eu.esponder.datasync.db.mysql.MySqlSymmetricDialect;

public class MariaDBSymmetricDialect extends MySqlSymmetricDialect {

	public MariaDBSymmetricDialect(IParameterService parameterService,
			IDatabasePlatform platform) {
		super(parameterService, platform);
	}

}
