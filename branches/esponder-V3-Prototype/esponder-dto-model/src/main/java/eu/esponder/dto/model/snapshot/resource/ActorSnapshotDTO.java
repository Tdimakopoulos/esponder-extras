/*
 * 
 */
package eu.esponder.dto.model.snapshot.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActorSnapshotStatusDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorSnapshotDTO.
 */
@JsonPropertyOrder({"id", "status", "locationArea", "period", "actor"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActorSnapshotDTO extends SpatialSnapshotDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4264419745021840922L;

	/**
	 * Instantiates a new actor snapshot dto.
	 */
	public ActorSnapshotDTO() { }
	
	/** The status. */
	private ActorSnapshotStatusDTO status;
	
	/** The actor. */
	private Long actor;

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ActorSnapshotStatusDTO getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ActorSnapshotStatusDTO status) {
		this.status = status;
	}

	/**
	 * Gets the actor.
	 *
	 * @return the actor
	 */
	public Long getActor() {
		return actor;
	}

	/**
	 * Sets the actor.
	 *
	 * @param actor the new actor
	 */
	public void setActor(Long actor) {
		this.actor = actor;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.snapshot.SpatialSnapshotDTO#toString()
	 */
	@Override
	public String toString() {
		return "ActorSnapshotDTO [status=" + status + ", locationArea=" + locationArea + ", id=" + id  + "]";
	}
	
}
