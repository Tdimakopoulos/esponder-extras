/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;



// TODO: Auto-generated Javadoc
/**
 * The Class FRTeamDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"title", "frcheif", "meoc","snapshots","incidentCommander"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class FRTeamDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6418102891138209607L;

	/** The title. */
	protected String title;

	/** The frchief. */
	private Long frchief;
	
	
	
	/** The snapshots. */
	private Set<FRTeamSnapshotDTO> snapshots;
	
	/** The incident commander. */
	private Long incidentCommander;
	
	/**
	 * Gets the incident commander.
	 *
	 * @return the incident commander
	 */
	public Long getIncidentCommander() {
		return incidentCommander;
	}

	/**
	 * Sets the incident commander.
	 *
	 * @param incidentCommander the new incident commander
	 */
	public void setIncidentCommander(Long incidentCommander) {
		this.incidentCommander = incidentCommander;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the frchief.
	 *
	 * @return the frchief
	 */
	public Long getFrchief() {
		return frchief;
	}


	/**
	 * Sets the frchief.
	 *
	 * @param frchief the new frchief
	 */
	public void setFrchief(Long frchief) {
		this.frchief = frchief;
	}


	

	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<FRTeamSnapshotDTO> getSnapshots() {
		return snapshots;
	}


	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<FRTeamSnapshotDTO> snapshots) {
		this.snapshots = snapshots;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FRTeamDTO [id=" + id
				+ ", title=" + title + "]";
	}

}
