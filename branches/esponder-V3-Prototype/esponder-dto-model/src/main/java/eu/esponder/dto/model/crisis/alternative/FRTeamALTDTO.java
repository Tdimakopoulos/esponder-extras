/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;

import java.io.Serializable;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class FRTeamDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "title", "frcheif", "meoc", "snapshots",
		"incidentCommander" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class FRTeamALTDTO implements Serializable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -884582583196380264L;

	/** The id. */
	Long id;
	
	/** The title. */
	protected String title;

	/** The frchief. */
	private ActorFRCALTDTO frchief;

	/** The snapshots. */
	private Set<Long> snapshots;

	/** The incident commander. */
//	private ActorICALTDTO incidentCommander;
	private Long incidentCommander;
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the frchief.
	 *
	 * @return the frchief
	 */
	public ActorFRCALTDTO getFrchief() {
		return frchief;
	}

	/**
	 * Sets the frchief.
	 *
	 * @param frchief the new frchief
	 */
	public void setFrchief(ActorFRCALTDTO frchief) {
		this.frchief = frchief;
	}

	/**
	 * Gets the incident commander.
	 *
	 * @return the incident commander
	 */
	public Long getIncidentCommander() {
		return incidentCommander;
	}

	/**
	 * Sets the incident commander.
	 *
	 * @param incidentCommander the new incident commander
	 */
	public void setIncidentCommander(Long incidentCommander) {
		this.incidentCommander = incidentCommander;
	}

	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<Long> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<Long> snapshots) {
		this.snapshots = snapshots;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getName()+" [id=" + id + ", title=" + title + "]";
	}

}
