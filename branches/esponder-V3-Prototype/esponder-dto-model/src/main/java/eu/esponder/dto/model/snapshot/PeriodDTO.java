/*
 * 
 */
package eu.esponder.dto.model.snapshot;

import java.io.Serializable;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class PeriodDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"dateFrom", "dateTo"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PeriodDTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2870132067764797692L;
	

	/** The date from. */
private Long dateFrom;


	/** The date to. */
private Long dateTo;
	
	/**
	 * Instantiates a new period dto.
	 */
	public PeriodDTO() { }
	
	/**
	 * Instantiates a new period dto.
	 *
	 * @param dateFrom the date from
	 * @param dateTo the date to
	 */
	public PeriodDTO(Long dateFrom, Long dateTo) {
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}
	


	/**
	 * Gets the date from.
	 *
	 * @return the date from
	 */
	public Long getDateFrom() {
		return dateFrom;
	}

	/**
	 * Sets the date from.
	 *
	 * @param dateFrom the new date from
	 */
	public void setDateFrom(Long dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * Gets the date to.
	 *
	 * @return the date to
	 */
	public Long getDateTo() {
		return dateTo;
	}

	/**
	 * Sets the date to.
	 *
	 * @param dateTo the new date to
	 */
	public void setDateTo(Long dateTo) {
		this.dateTo = dateTo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Period [dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}

}
