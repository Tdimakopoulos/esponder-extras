/*
 * 
 */
package eu.esponder.util.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderPropertyLoader.
 */
public class ESponderPropertyLoader {

	/**
	 * Gets the properties.
	 *
	 * @param propsFileName the props file name
	 * @return the properties
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static Properties getProperties(String propsFileName) throws FileNotFoundException, IOException {
		Properties props = new Properties();
		if (props.isEmpty()) {
			File propsFile = new File(propsFileName);
			FileInputStream fis = new FileInputStream(propsFile);
			System.out.println("Loading properties from file:" + propsFileName);
			props.load(fis);
			System.out.println("Properties Loaded:");
			for (Object key : props.keySet()) {
				System.out.println(key.toString() + " = " + props.get(key));
			}
		}
		return props;
	}
	
	
}
