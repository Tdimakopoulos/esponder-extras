/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.category;

import org.codehaus.jackson.annotate.JsonTypeInfo;



// TODO: Auto-generated Javadoc
/**
 * The Class LogisticsCategoryDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class LogisticsCategoryDTO extends PlannableResourceCategoryDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1077502768919499655L;


}