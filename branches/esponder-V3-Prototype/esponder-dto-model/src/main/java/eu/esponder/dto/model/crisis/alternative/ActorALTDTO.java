/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;



import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.view.VoIPURLDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ActorDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "resourceId", "title", "personnel", "voIPURL" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class ActorALTDTO implements Serializable{

	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4317826790326091547L;

	/**
	 * Instantiates a new actor dto.
	 */
	public ActorALTDTO() {
	}

	/** The id. */
	Long id;

	/** The voip url. */
	private VoIPURLDTO voIPURL;

	/** The personnel. */
	private Long personnel;

	/** The resourceid. */
	Long resourceid;

	/** The title. */
	String title;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the resourceid.
	 *
	 * @return the resourceid
	 */
	public Long getResourceid() {
		return resourceid;
	}

	/**
	 * Sets the resourceid.
	 *
	 * @param resourceid the new resourceid
	 */
	public void setResourceid(Long resourceid) {
		this.resourceid = resourceid;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getName()+" [id=" + id + ", title=" + title + "]";
	}

	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}

	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}

	/**
	 * Gets the personnel.
	 *
	 * @return the personnel
	 */
	public Long getPersonnel() {
		return personnel;
	}

	/**
	 * Sets the personnel.
	 *
	 * @param personnel the new personnel
	 */
	public void setPersonnel(Long personnel) {
		this.personnel = personnel;
	}

}
