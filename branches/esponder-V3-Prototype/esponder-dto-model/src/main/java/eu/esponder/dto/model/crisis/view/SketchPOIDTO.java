/*
 * 
 */
package eu.esponder.dto.model.crisis.view;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class SketchPOIDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title","description","sketchType","httpURL", "points"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SketchPOIDTO extends ResourcePOIDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8869263968721446925L;

	/**
	 * Instantiates a new sketch poidto.
	 */
	public SketchPOIDTO() { }
	
	/** The description. */
	private String description;
	
	/** The sketch type. */
	private String sketchType;
	
	/** The http url. */
	private HttpURLDTO httpURL;
	
	/** The points. */
	private Set<MapPointDTO> points;

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the sketch type.
	 *
	 * @return the sketch type
	 */
	public String getSketchType() {
		return sketchType;
	}

	/**
	 * Sets the sketch type.
	 *
	 * @param sketchType the new sketch type
	 */
	public void setSketchType(String sketchType) {
		this.sketchType = sketchType;
	}

	/**
	 * Gets the http url.
	 *
	 * @return the http url
	 */
	public HttpURLDTO getHttpURL() {
		return httpURL;
	}

	/**
	 * Sets the http url.
	 *
	 * @param httpURL the new http url
	 */
	public void setHttpURL(HttpURLDTO httpURL) {
		this.httpURL = httpURL;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the points.
	 *
	 * @return the points
	 */
	public Set<MapPointDTO> getPoints() {
		return points;
	}

	/**
	 * Sets the points.
	 *
	 * @param points the new points
	 */
	public void setPoints(Set<MapPointDTO> points) {
		this.points = points;
	}	
	
}
