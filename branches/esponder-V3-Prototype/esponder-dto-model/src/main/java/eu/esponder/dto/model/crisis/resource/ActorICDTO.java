/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorICDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "personnel", "voIPURL",
	"crisisManager", "frTeams","operationsCentre"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActorICDTO extends ActorDTO{


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2273945814165596740L;

	/**
	 * Instantiates a new actor icdto.
	 */
	public ActorICDTO() {}

	/** The crisis manager. */
	private Long crisisManager;

	/** The fr teams. */
	private Set<FRTeamDTO> frTeams;
	
	/** The operations centre. */
	private Long operationsCentre;
	
	/**
	 * Gets the crisis manager.
	 *
	 * @return the crisis manager
	 */
	public Long getCrisisManager() {
		return crisisManager;
	}

	/**
	 * Sets the crisis manager.
	 *
	 * @param crisisManager the new crisis manager
	 */
	public void setCrisisManager(Long crisisManager) {
		this.crisisManager = crisisManager;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public Long getOperationsCentre() {
		return operationsCentre;
	}


	/**
	 * Gets the fr teams.
	 *
	 * @return the fr teams
	 */
	public Set<FRTeamDTO> getFrTeams() {
		return frTeams;
	}

	/**
	 * Sets the fr teams.
	 *
	 * @param frTeams the new fr teams
	 */
	public void setFrTeams(Set<FRTeamDTO> frTeams) {
		this.frTeams = frTeams;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(Long operationsCentre) {
		this.operationsCentre = operationsCentre;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.ActorDTO#toString()
	 */
	@Override
	public String toString() {
		return "ActorICDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}
	
}
