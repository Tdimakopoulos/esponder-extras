/*
 * 
 */
package eu.esponder.event.model;

import java.util.Date;

import eu.esponder.dto.model.user.ESponderUserDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ESponderOperationEvent.
 */

public abstract class ESponderExceptionEvent extends ESponderEvent<ESponderUserDTO> {
	//implements Serializable

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2251911408804997594L;

	/**
	 * The Enum ESponderExceptionType.
	 */
	public enum ESponderExceptionType {

		/** The Checked exception. */
		CheckedException,
		/** The Un checked exception. */
		UnCheckedException,
		/** The Exception. */
		Exception
	}

	/** The journal message info. */
	String JournalMessageInfo;

	/** The event timestamp. */
	Date eventTimestamp;

	/** The journal message. */
	String journalMessage;

	/** The Exception text. */
	String ExceptionText;

	/** The Exception message. */
	String ExceptionMessage;

	/** The Exception type. */
	ESponderExceptionType ExceptionType;

	/**
	 * Gets the journal message info.
	 * 
	 * @return the journal message info
	 */
	public String getJournalMessageInfo() {
		return JournalMessageInfo;
	}

	/**
	 * Sets the journal message info.
	 * 
	 * @param journalMessageInfo
	 *            the new journal message info
	 */
	public void setJournalMessageInfo(String journalMessageInfo) {
		JournalMessageInfo = journalMessageInfo;
	}

	/**
	 * Gets the event timestamp.
	 * 
	 * @return the event timestamp
	 */
	public Date getEventTimestamp() {
		return eventTimestamp;
	}

	/**
	 * Sets the event timestamp.
	 * 
	 * @param eventTimestamp
	 *            the new event timestamp
	 */
	public void setEventTimestamp(Date eventTimestamp) {
		this.eventTimestamp = eventTimestamp;
	}

	/**
	 * Gets the journal message.
	 * 
	 * @return the journal message
	 */
	public String getJournalMessage() {
		return journalMessage;
	}

	/**
	 * Sets the journal message.
	 * 
	 * @param journalMessage
	 *            the new journal message
	 */
	public void setJournalMessage(String journalMessage) {
		this.journalMessage = journalMessage;
	}

	/**
	 * Gets the exception text.
	 * 
	 * @return the exception text
	 */
	public String getExceptionText() {
		return ExceptionText;
	}

	/**
	 * Sets the exception text.
	 * 
	 * @param exceptionText
	 *            the new exception text
	 */
	public void setExceptionText(String exceptionText) {
		ExceptionText = exceptionText;
	}

	/**
	 * Gets the exception message.
	 * 
	 * @return the exception message
	 */
	public String getExceptionMessage() {
		return ExceptionMessage;
	}

	/**
	 * Sets the exception message.
	 * 
	 * @param exceptionMessage
	 *            the new exception message
	 */
	public void setExceptionMessage(String exceptionMessage) {
		ExceptionMessage = exceptionMessage;
	}

	/**
	 * Gets the exception type.
	 * 
	 * @return the exception type
	 */
	public ESponderExceptionType getExceptionType() {
		return ExceptionType;
	}

	/**
	 * Sets the exception type.
	 * 
	 * @param exceptionType
	 *            the new exception type
	 */
	public void setExceptionType(ESponderExceptionType exceptionType) {
		ExceptionType = exceptionType;
	}

}
