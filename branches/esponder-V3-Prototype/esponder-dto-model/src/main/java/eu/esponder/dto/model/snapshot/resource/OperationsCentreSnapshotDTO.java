/*
 * 
 */
package eu.esponder.dto.model.snapshot.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.OperationsCentreSnapshotStatusDTO;



// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreSnapshotDTO.
 */
@JsonPropertyOrder({"id", "status", "locationArea", "period"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OperationsCentreSnapshotDTO extends SpatialSnapshotDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6058516655453683876L;
	
	/** The previous. */
	private OperationsCentreSnapshotDTO previous;
	
	/** The next. */
	private OperationsCentreSnapshotDTO next;
	
	/** The operations centre. */
	private OCMeocDTO operationsCentre;

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public OperationsCentreSnapshotDTO getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(OperationsCentreSnapshotDTO next) {
		this.next = next;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OCMeocDTO getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OCMeocDTO operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	/**
	 * Instantiates a new operations centre snapshot dto.
	 */
	public OperationsCentreSnapshotDTO() { }
	
	/** The status. */
	private OperationsCentreSnapshotStatusDTO status;

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public OperationsCentreSnapshotStatusDTO getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(OperationsCentreSnapshotStatusDTO status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.snapshot.SpatialSnapshotDTO#toString()
	 */
	@Override
	public String toString() {
		return "OperationsCentreSnapshotDTO [status=" + status + ", locationArea=" + locationArea + ", id=" + id + "]";
	}

	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public OperationsCentreSnapshotDTO getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previous the new previous
	 */
	public void setPrevious(OperationsCentreSnapshotDTO previous) {
		this.previous = previous;
	}
	
}
