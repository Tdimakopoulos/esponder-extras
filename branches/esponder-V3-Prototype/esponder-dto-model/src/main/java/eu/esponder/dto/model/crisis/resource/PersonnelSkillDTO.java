/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.type.PersonnelSkillTypeDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelSkillDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "type", "shortTitle", "personnelCategory", "personnelSkillType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PersonnelSkillDTO extends PersonnelCompetenceDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4287360040986460222L;
	
	/** The personnel skill type. */
	private PersonnelSkillTypeDTO personnelSkillType;

	/**
	 * Gets the personnel skill type.
	 *
	 * @return the personnel skill type
	 */
	public PersonnelSkillTypeDTO getPersonnelSkillType() {
		return personnelSkillType;
	}

	/**
	 * Sets the personnel skill type.
	 *
	 * @param personnelSkillType the new personnel skill type
	 */
	public void setPersonnelSkillType(PersonnelSkillTypeDTO personnelSkillType) {
		this.personnelSkillType = personnelSkillType;
	}
	
}