/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class OCMeocDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "supervisingOC", 
	"incidentCommander","subordinateTeams", "meocCrisisContext", "snapshots"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OCMeocDTO extends OperationsCentreDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2589221069018022969L;

	/** The supervising oc. */
	private Long supervisingOC;

	/** The incident commander. */
	private Long incidentCommander;

	
	
	/** The meoc crisis context. */
	private Long meocCrisisContext;
	
	/** The snapshots. */
	private Set<Long> snapshots;
	
	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<Long> getSnapshots() {
		return snapshots;
	}


	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<Long> snapshots) {
		this.snapshots = snapshots;
	}
	
	
	/**
	 * Gets the supervising oc.
	 *
	 * @return the supervising oc
	 */
	public Long getSupervisingOC() {
		return supervisingOC;
	}

	/**
	 * Sets the supervising oc.
	 *
	 * @param supervisingOC the new supervising oc
	 */
	public void setSupervisingOC(Long supervisingOC) {
		this.supervisingOC = supervisingOC;
	}

	/**
	 * Gets the incident commander.
	 *
	 * @return the incident commander
	 */
	public Long getIncidentCommander() {
		return incidentCommander;
	}

	/**
	 * Sets the incident commander.
	 *
	 * @param incidentCommander the new incident commander
	 */
	public void setIncidentCommander(Long incidentCommander) {
		this.incidentCommander = incidentCommander;
	}

	

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.OperationsCentreDTO#toString()
	 */
	@Override
	public String toString() {
		return "OCMeocDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}

	/**
	 * Gets the meoc crisis context.
	 *
	 * @return the meoc crisis context
	 */
	public Long getMeocCrisisContext() {
		return meocCrisisContext;
	}

	/**
	 * Sets the meoc crisis context.
	 *
	 * @param meocCrisisContext the new meoc crisis context
	 */
	public void setMeocCrisisContext(Long meocCrisisContext) {
		this.meocCrisisContext = meocCrisisContext;
	}
}
