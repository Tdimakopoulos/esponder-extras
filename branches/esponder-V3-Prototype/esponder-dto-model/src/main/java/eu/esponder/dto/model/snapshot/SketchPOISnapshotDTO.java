/*
 * 
 */
package eu.esponder.dto.model.snapshot;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.HttpURLDTO;
import eu.esponder.dto.model.crisis.view.MapPointDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class SketchPOISnapshotDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "points", "httpURL","operationsCentre","sketchType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SketchPOISnapshotDTO extends SpatialSnapshotDTO {
	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1734142629191263542L;

	
	/** The id. */
	protected Long id;
	
	
	/** The points. */
	private Set<MapPointDTO> points;
	
	
	/** The http url. */
	private HttpURLDTO httpURL;
	
	
	/** The sketch type. */
	private String sketchType;

	
	/** The title. */
	protected String title;
	
	/** The sketch poi. */
	private SketchPOIDTO sketchPOI;
	
	/**
	 * Gets the sketch poi.
	 *
	 * @return the sketch poi
	 */
	public SketchPOIDTO getSketchPOI() {
		return sketchPOI;
	}

	/**
	 * Sets the sketch poi.
	 *
	 * @param sketchPOI the new sketch poi
	 */
	public void setSketchPOI(SketchPOIDTO sketchPOI) {
		this.sketchPOI = sketchPOI;
	}

	/** The operations centre. */
	protected OperationsCentreDTO operationsCentre;
	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#setId(java.lang.Long)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the points.
	 *
	 * @return the points
	 */
	public Set<MapPointDTO> getPoints() {
		return points;
	}

	/**
	 * Sets the points.
	 *
	 * @param points the new points
	 */
	public void setPoints(Set<MapPointDTO> points) {
		this.points = points;
	}

	/**
	 * Gets the http url.
	 *
	 * @return the http url
	 */
	public HttpURLDTO getHttpURL() {
		return httpURL;
	}

	/**
	 * Sets the http url.
	 *
	 * @param httpURL the new http url
	 */
	public void setHttpURL(HttpURLDTO httpURL) {
		this.httpURL = httpURL;
	}

	/**
	 * Gets the sketch type.
	 *
	 * @return the sketch type
	 */
	public String getSketchType() {
		return sketchType;
	}

	/**
	 * Sets the sketch type.
	 *
	 * @param sketchType the new sketch type
	 */
	public void setSketchType(String sketchType) {
		this.sketchType = sketchType;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OperationsCentreDTO getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OperationsCentreDTO operationsCentre) {
		this.operationsCentre = operationsCentre;
	}
	
}
