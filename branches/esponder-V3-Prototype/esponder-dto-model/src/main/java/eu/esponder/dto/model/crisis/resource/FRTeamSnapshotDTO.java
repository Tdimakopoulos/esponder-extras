/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.math.BigDecimal;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;




// TODO: Auto-generated Javadoc
/**
 * The Class FRTeamSnapshotDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"frteam", "previous", "next","statisticType","value"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class FRTeamSnapshotDTO extends SnapshotDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6418108656199909607L;

	/** The id. */
	protected Long id;
	
	/** The FRTEAM ID . Not Null*/
	private FRTeamDTO frteam;
	
	/** The previous. */
	private FRTeamSnapshotDTO previous;

	/** The next. */

	private FRTeamSnapshotDTO next;

	/** The statistic type. */

	private MeasurementStatisticTypeEnumDTO statisticType;

	/** The value. */

	private BigDecimal value;


	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#setId(java.lang.Long)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the frteam.
	 *
	 * @return the frteam
	 */
	public FRTeamDTO getFrteam() {
		return frteam;
	}

	/**
	 * Sets the frteam.
	 *
	 * @param frteam the new frteam
	 */
	public void setFrteam(FRTeamDTO frteam) {
		this.frteam = frteam;
	}

	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public FRTeamSnapshotDTO getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previous the new previous
	 */
	public void setPrevious(FRTeamSnapshotDTO previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public FRTeamSnapshotDTO getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(FRTeamSnapshotDTO next) {
		this.next = next;
	}

	/**
	 * Gets the statistic type.
	 *
	 * @return the statistic type
	 */
	public MeasurementStatisticTypeEnumDTO getStatisticType() {
		return statisticType;
	}

	/**
	 * Sets the statistic type.
	 *
	 * @param statisticType the new statistic type
	 */
	public void setStatisticType(MeasurementStatisticTypeEnumDTO statisticType) {
		this.statisticType = statisticType;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}
}
