/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;

import java.io.Serializable;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.view.VoIPURLDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "resourceId", "title", "users", "voIPURL" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class OperationsCentreALTDTO implements Serializable {

	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7588884942228071735L;
	
	/** The resourceid. */
	Long resourceid;
	
	/** The title. */
	String title;
	
	/** The users. */
	Set<Long> users;
	
	/** The vo ipurl. */
	VoIPURLDTO voIPURL;
	
	/** The id. */
	Long id;
	
	/** The snapshots. */
//	private Set<Long> snapshots;
	
	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
//	public Set<Long> getSnapshots() {
//		return snapshots;
//	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
//	public void setSnapshots(Set<Long> snapshots) {
//		this.snapshots = snapshots;
//	}

	/**
	 * Instantiates a new operations centre dto.
	 */
	public OperationsCentreALTDTO() {
	}

	/**
	 * Gets the resourceid.
	 *
	 * @return the resourceid
	 */
	public Long getResourceid() {
		return resourceid;
	}

	/**
	 * Sets the resourceid.
	 *
	 * @param resourceid the new resourceid
	 */
	public void setResourceid(Long resourceid) {
		this.resourceid = resourceid;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public Set<Long> getUsers() {
		return users;
	}

	/**
	 * Sets the users.
	 *
	 * @param users the new users
	 */
	public void setUsers(Set<Long> users) {
		this.users = users;
	}

	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}

	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.esponder.dto.model.crisis.resource.ResourceDTO#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getName()+" [id=" + id + ", title=" + title + "]";
	}

}
