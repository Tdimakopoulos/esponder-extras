/*
 * 
 */
package eu.esponder.dto.model.criteria;

import java.util.Collection;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class EsponderUnionCriteriaCollectionDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"restrictions"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EsponderUnionCriteriaCollectionDTO extends EsponderCriteriaCollectionDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4963444702454465879L;

	/**
	 * Instantiates a new esponder union criteria collection dto.
	 */
	public EsponderUnionCriteriaCollectionDTO() { }
	
	/**
	 * Instantiates a new esponder union criteria collection dto.
	 *
	 * @param restrictions the restrictions
	 */
	public EsponderUnionCriteriaCollectionDTO(
			Collection<EsponderQueryRestrictionDTO> restrictions) {
		super(restrictions);
	}

}
