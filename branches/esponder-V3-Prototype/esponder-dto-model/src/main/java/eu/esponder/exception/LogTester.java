/*
 * 
 */
package eu.esponder.exception;



// TODO: Auto-generated Javadoc
/**
 * The Class LogTester.
 */
public class LogTester {
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		EsponderExceptionLogger.LogFatal(new LogTester().getClass(),"***** *** ****** ***** ***** ***** ***** ***** **** **** *****");
		
		//just use the logger
		EsponderExceptionLogger.LogFatal(new LogTester().getClass(),"Fatal Logger Test");
		EsponderExceptionLogger.LogError(new LogTester().getClass(),"Error Logger Test");
		EsponderExceptionLogger.LogInfo(new LogTester().getClass(),"Info Logger Test");
		EsponderExceptionLogger.LogWarn(new LogTester().getClass(),"Warn Logger Test");
		
		//catch checked exception
		try{
			int x=0/0;
			System.out.print(x);
		}catch(Exception ex)
		{
				new EsponderCheckedException(new LogTester().getClass(),"Checked Exception msg:"+ex.getMessage());
		}
		
		//catch checked exception
		try{
			int y=0/0;
			System.out.print(y);
		}catch(Exception ex)
		{
				new EsponderUncheckedException(new LogTester().getClass(),"UnChecked Exception msg:"+ex.getMessage());
		}
		
		
	}

}
