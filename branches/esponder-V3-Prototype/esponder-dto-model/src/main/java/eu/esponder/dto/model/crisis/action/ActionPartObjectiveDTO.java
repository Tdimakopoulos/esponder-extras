/*
 * 
 */
package eu.esponder.dto.model.crisis.action;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;



// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartObjectiveDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "actionPartID", "period", "locationArea", "consumableResources","reusableResources"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionPartObjectiveDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5435315087528431329L;

	/** Short title of the ActionObjective. */
	private String title;
	
	/**
	 * Period during which this ActionPart should be "active". If the current time is after the "dateTo" then we may infer that the Action is delayed etc.  
	 */
	private PeriodDTO period;
	
	/** Target location; used when the "ActionOperationEnumDTO value is MOVE". */
	private LocationAreaDTO locationArea;
	
	/** ConsumableResources used in the ActionPart. */
	private Set<Long> consumableResources;
	
	/** ReusableResources used in the ActionPart. */
	private Set<Long> reusableResources;
	
	
	/**
	 * Gets the reusable resources.
	 *
	 * @return the reusable resources
	 */
	public Set<Long> getReusableResources() {
		return reusableResources;
	}

	// 
	/** Referenced ActionPart. */
	private Long actionPartID;

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#setId(java.lang.Long)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the period.
	 *
	 * @return the period
	 */
	public PeriodDTO getPeriod() {
		return period;
	}

	/**
	 * Sets the period.
	 *
	 * @param period the new period
	 */
	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

	/**
	 * Gets the location area.
	 *
	 * @return the location area
	 */
	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	/**
	 * Sets the location area.
	 *
	 * @param locationArea the new location area
	 */
	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

	/**
	 * Gets the consumable resources.
	 *
	 * @return the consumable resources
	 */
	public Set<Long> getConsumableResources() {
		return consumableResources;
	}

	/**
	 * Sets the consumable resources.
	 *
	 * @param consumableResources the new consumable resources
	 */
	public void setConsumableResources(Set<Long> consumableResources) {
		this.consumableResources = consumableResources;
	}

	/**
	 * Gets the reusuable resources.
	 *
	 * @return the reusuable resources
	 */
	public Set<Long> getReusuableResources() {
		return reusableResources;
	}

	/**
	 * Sets the reusuable resources.
	 *
	 * @param reusableResources the new reusable resources
	 */
	public void setReusableResources(Set<Long> reusableResources) {
		this.reusableResources = reusableResources;
	}

	/**
	 * Gets the action part.
	 *
	 * @return the action part
	 */
	public Long getActionPartID() {
		return actionPartID;
	}

	/**
	 * Sets the action part.
	 *
	 * @param actionPart the new action part
	 */
	public void setActionPartID(Long actionPart) {
		this.actionPartID = actionPart;
	}
	
}
