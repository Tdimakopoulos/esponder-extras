/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorFRCDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "personnel", "voIPURL", "incidentCommander", "team", "subordinates"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActorFRCDTO extends FirstResponderActorDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4062599699350482582L;

	

	/** The team. */
	private Long team;

	/** The subordinates. */
	private Set<Long> subordinates;
	
	
	/**
	 * Gets the team.
	 *
	 * @return the team
	 */
	public Long getTeam() {
		return team;
	}

	/**
	 * Sets the team.
	 *
	 * @param team the new team
	 */
	public void setTeam(Long team) {
		this.team = team;
	}

	/**
	 * Gets the subordinates.
	 *
	 * @return the subordinates
	 */
	public Set<Long> getSubordinates() {
		return subordinates;
	}

	/**
	 * Sets the subordinates.
	 *
	 * @param subordinates the new subordinates
	 */
	public void setSubordinates(Set<Long> subordinates) {
		this.subordinates = subordinates;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO#toString()
	 */
	@Override
	public String toString() {
		return "ActorFRCDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}
}
