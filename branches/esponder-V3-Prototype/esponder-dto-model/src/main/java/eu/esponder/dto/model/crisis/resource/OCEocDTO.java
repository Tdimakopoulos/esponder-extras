/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.CrisisContextDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class OCEocDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "leadingActor", "subordinates"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OCEocDTO extends OperationsCentreDTO{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5218588692128620330L;


	/** The crisis manager. */
	private Long crisisManager;
	
	/** The subordinate meo cs. */
	private Set<Long> subordinateMEOCs;
	
	/** The eoc crisis context. */
	private CrisisContextDTO eocCrisisContext;

	/**
	 * Gets the crisis manager.
	 *
	 * @return the crisis manager
	 */
	public Long getCrisisManager() {
		return crisisManager;
	}


	/**
	 * Sets the crisis manager.
	 *
	 * @param crisisManager the new crisis manager
	 */
	public void setCrisisManager(Long crisisManager) {
		this.crisisManager = crisisManager;
	}


	/**
	 * Gets the subordinate meo cs.
	 *
	 * @return the subordinate meo cs
	 */
	public Set<Long> getSubordinateMEOCs() {
		return subordinateMEOCs;
	}


	/**
	 * Sets the subordinate meo cs.
	 *
	 * @param subordinateMEOCs the new subordinate meo cs
	 */
	public void setSubordinateMEOCs(Set<Long> subordinateMEOCs) {
		this.subordinateMEOCs = subordinateMEOCs;
	}


	/**
	 * Gets the eoc crisis context.
	 *
	 * @return the eoc crisis context
	 */
	public CrisisContextDTO getEocCrisisContext() {
		return eocCrisisContext;
	}


	/**
	 * Sets the eoc crisis context.
	 *
	 * @param eocCrisisContext the new eoc crisis context
	 */
	public void setEocCrisisContext(CrisisContextDTO eocCrisisContext) {
		this.eocCrisisContext = eocCrisisContext;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.OperationsCentreDTO#toString()
	 */
	@Override
	public String toString() {
		return "OCEocDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}

	
}
