/*
 * 
 */
package eu.esponder.dto.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonTypeInfo;



// TODO: Auto-generated Javadoc
/**
 * The Class ESponderEntityDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ESponderEntityDTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1652821671452383819L;

	/** The id. */
	protected Long id;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

}