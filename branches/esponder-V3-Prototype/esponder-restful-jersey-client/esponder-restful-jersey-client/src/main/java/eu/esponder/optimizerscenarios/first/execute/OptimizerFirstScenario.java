package eu.esponder.optimizerscenarios.first.execute;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.optimizerscenarios.first.logisticmanager.LogisticsManager;
import eu.esponder.optimizerscenarios.first.objects.Logistic;
import eu.esponder.optimizerscenarios.first.objects.OperationCenter;
import eu.esponder.optimizerscenarios.first.objects.person;
import eu.esponder.optimizerscenarios.first.operationcentersmanager.OperationCentersManager;
import eu.esponder.optimizerscenarios.first.personnelmanager.PersonnelManager;
import eu.esponder.optimizerscenarios.first.resourceplan.ResourcePlanInfoStatic;
import eu.esponder.ws.client.logic.CrisisActionAndActionPartManager;
import eu.esponder.ws.client.logic.CrisisResourceManager;
import eu.esponder.ws.client.query.QueryManager;

public class OptimizerFirstScenario {

	/**
	 * We assume that we will need 1 fire fighter and 1 police officer, you can
	 * modify this number in ResourcePlanInfoStatic
	 * 
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	public static void main(String[] args) throws JsonGenerationException,
			JsonMappingException, IOException {

		//set to false to show updates on screen, set to true to update the database
		Boolean bUpdateDB=false;
		
		// load all personnel from database
		PersonnelManager ppman = new PersonnelManager();

		// Variables needed for the Association
		CrisisResourceManager pCCManager = new CrisisResourceManager();
		CrisisActionAndActionPartManager pAAManager = new CrisisActionAndActionPartManager();
		QueryManager pQuery = new QueryManager();
		ActionPartDTO pActionPart = null;
		ActorDTO pActoradded = null;
		ActionDTO pAction = null;
		
		// First we need to know how many fire fighters and how many police
		// officers we will need
		// We store all informations associated with the crisis into the
		// ResourcePlanInfoStatic
		ResourcePlanInfoStatic pRPlan = new ResourcePlanInfoStatic();
		int iFireF = pRPlan.GetFire();
		int iPoliceO = pRPlan.GetPolice();
		int iEOC= pRPlan.GetEOC();
		int ilog=pRPlan.GetMaxResources();
				
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" OPTIMIZER FIRST SCENARIO STARTED ");
		
		System.out.println(" ");
		System.out.println(" ");
		
		System.out.println("Available Personnel");
		System.out.println(" ");
		System.out.println(" ");
		
		// print personnel
		ppman.PrintAllPersonnel();

		// Start the association
		// we get the list of personnel from the already loaded personnel array
		// list
		List<person> pList = ppman.GetAllPersonnel();

		System.out.println(" ");
		System.out.println(" ");
		
		System.out.println("Associate Personnel with Crisis - Process Start");

		System.out.println(" ");
		System.out.println(" ");
		
		// we loop for all personnel to get the needed fire fighter and police
		// officers
		// this is a very stupid method, the first we find the first we add.
		for (int i = 0; i < pList.size(); i++) {
			if (iFireF > 0) {
				if (pList.get(i).getPosition()
						.equalsIgnoreCase(pRPlan.GetNameOFFirefighter())) {
					System.out.println(" ");
					System.out.println(" ");
					System.out.println("Fire Fighter added");
					System.out.println(" ");
					System.out.println(" ");
					if(bUpdateDB)
					{
					pActoradded = pCCManager.AssociateActor(
							String.valueOf(pList.get(i).getNumber()), "1");
					
					pAction = pAAManager.CreateAction(pQuery
							.getCrisisContextID("1", pRPlan.GetCrisisID()
									.toString()));
					
					pActionPart = pAAManager.CreateActionPartPersonnel(pAction,
							pQuery.getCrisisContextID("1", pRPlan.GetCrisisID()
									.toString()), pActoradded.getId()
									.toString());
					}
					iFireF = iFireF - 1;
				}
			}

			if (iPoliceO > 0) {
				if (pList.get(i).getPosition()
						.equalsIgnoreCase(pRPlan.GetNameOfPolice())) {
					System.out.println(" ");
					System.out.println(" ");
					System.out.println("Police Officer added");
					System.out.println(" ");
					System.out.println(" ");
					if(bUpdateDB)
					{
					pActoradded = pCCManager.AssociateActor(
							String.valueOf(pList.get(i).getNumber()), "1");
					
					pAction = pAAManager.CreateAction(pQuery
							.getCrisisContextID("1", pRPlan.GetCrisisID()
									.toString()));
					
					pActionPart = pAAManager.CreateActionPartPersonnel(pAction,
							pQuery.getCrisisContextID("1", pRPlan.GetCrisisID()
									.toString()), pActoradded.getId()
									.toString());
					}
					iPoliceO = iPoliceO - 1;
				}
			}
		}
		
		
		//now we will look for Operation Centers and do the association
		OperationCentersManager pOCM= new OperationCentersManager();
		
		System.out.println(" ");
		System.out.println(" ");
		
		System.out.println("Available OC");
		
		System.out.println(" ");
		System.out.println(" ");
		
		pOCM.PrintOC();
		
		System.out.println(" ");
		System.out.println(" ");
		
		System.out.println("Associate OC with Crisis - Process Start");

		System.out.println(" ");
		System.out.println(" ");
		
		// now lets get the list to look for oc to associate with the crisis.
		List<OperationCenter> plist=pOCM.GetOCList();
		for (int i = 0; i < plist.size(); i++) {
			if (iEOC > 0) {
				if(plist.get(i).getCategory().equalsIgnoreCase(pRPlan.GetEOCName()))
				{
					System.out.println(" ");
					System.out.println(" ");
					System.out.println("EOC added");
					System.out.println(" ");
					System.out.println(" ");
					if(bUpdateDB)
					{
						pCCManager.AssociateOC(pRPlan.GetCrisisID()
								.toString(), String.valueOf(plist.get(i).getNumber()), "1");
					}
					iEOC=iEOC-1;
				}
			}
		}
		
		
		System.out.println(" ");
		System.out.println(" ");
		
		System.out.println("Available Logistics");
		
		System.out.println(" ");
		System.out.println(" ");
		
		LogisticsManager plM=new LogisticsManager();
		plM.PrintAllLogistics();
		System.out.println(" ");
		System.out.println(" ");
		
		System.out.println("Associate Logistics with Crisis - Process Start");

		System.out.println(" ");
		System.out.println(" ");
		// now lets get the list to look for logistics to associate with the crisis.
		List<Logistic> plistl=plM.GetLogistics();
		for (int i = 0; i < plistl.size(); i++) {
			if(ilog>0)
			{
				if(plistl.get(i).getCategory().equalsIgnoreCase(pRPlan.Getmm1()))
				{
					System.out.println(" ");
					System.out.println(" ");
					System.out.println("Logistic "+plistl.get(i).getType()+" added");
					System.out.println(" ");
					System.out.println(" ");
					if(bUpdateDB)
					{
						//we need to check if the logistic is reusable or consumable for the association
						if (plistl.get(i).getType().equalsIgnoreCase("reu")) {
							try {
								ReusableResourceDTO preturn = pCCManager
										.AssociateReusable(pRPlan.GetCrisisID().toString(), 
												String.valueOf(plistl.get(i).getNumber()), 
												"1");

							} catch (JsonGenerationException ex) {

							} catch (JsonMappingException ex) {

							} catch (IOException ex) {

							}
						} else {
							try {
								ConsumableResourceDTO preturn1 = pCCManager
										.AssociateConsumable(pRPlan.GetCrisisID().toString(),
												String.valueOf(plistl.get(i).getNumber()), 
												"1");

							} catch (JsonGenerationException ex) {

							} catch (JsonMappingException ex) {

							} catch (IOException ex) {

							}
						}
					}
					ilog=ilog-1;
				}
				if(plistl.get(i).getCategory().equalsIgnoreCase(pRPlan.Getmm2()))
				{
					System.out.println(" ");
					System.out.println(" ");
					System.out.println("Logistic "+plistl.get(i).getType()+" added");
					System.out.println(" ");
					System.out.println(" ");
					if(bUpdateDB)
					{
						//we need to check if the logistic is reusable or consumable for the association
						if (plistl.get(i).getType().equalsIgnoreCase("reu")) {
							try {
								ReusableResourceDTO preturn = pCCManager
										.AssociateReusable(pRPlan.GetCrisisID().toString(), 
												String.valueOf(plistl.get(i).getNumber()), 
												"1");

							} catch (JsonGenerationException ex) {

							} catch (JsonMappingException ex) {

							} catch (IOException ex) {

							}
						} else {
							try {
								ConsumableResourceDTO preturn1 = pCCManager
										.AssociateConsumable(pRPlan.GetCrisisID().toString(),
												String.valueOf(plistl.get(i).getNumber()), 
												"1");

							} catch (JsonGenerationException ex) {

							} catch (JsonMappingException ex) {

							} catch (IOException ex) {

							}
						}
					}
					ilog=ilog-1;
				}
			}
		}
	}
}
