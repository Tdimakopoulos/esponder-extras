package eu.esponder.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.resource.OrganisationDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.type.RankTypeDTO;
import eu.esponder.test.dbtest.ControllerServiceTest;
import eu.esponder.util.logger.ESponderLogger;

public class PersonnelServiceTest extends ControllerServiceTest {

	@SuppressWarnings("unused")
	@Test(groups="createResources")
	public void createPersonnel() throws ClassNotFoundException {
		
		PersonnelCategoryDTO personnelCategory1 = (PersonnelCategoryDTO) genericService.getEntityDTO(PersonnelCategoryDTO.class, new Long(13));
		PersonnelCategoryDTO personnelCategory2 = (PersonnelCategoryDTO) genericService.getEntityDTO(PersonnelCategoryDTO.class, new Long(14));
		
		PersonnelDTO personnelDTOCM = createPersonnelDTO(true, "RankType1", "Organisation1", "Evangelos", "Managerinis", "Manager No1", ResourceStatusDTO.AVAILABLE, personnelCategory1);
		
		PersonnelDTO personnelDTOIC = createPersonnelDTO(true, "RankType1", "Organisation1", "Lampros", "Incidentis", "Incident Commander No1", ResourceStatusDTO.AVAILABLE, personnelCategory1);
		
		PersonnelDTO personnelDTOIC2 = createPersonnelDTO(true, "RankType1", "Organisation1", "Thrasivoulos", "Incidentisdos", "Incident Commander No2", ResourceStatusDTO.AVAILABLE, personnelCategory1);
		
		PersonnelDTO personnelDTO1 = createPersonnelDTO(true, "RankType1", "Organisation1", "Giannis", "Papagiannis", "Employee No1", ResourceStatusDTO.AVAILABLE, personnelCategory1);

		PersonnelDTO personnelDTO2 = createPersonnelDTO(true, "RankType1", "Organisation1", "Giorgos", "Papageorgiou", "Employee No2", ResourceStatusDTO.AVAILABLE, personnelCategory2);

		PersonnelDTO personnelDTO3 = createPersonnelDTO(true, "RankType2", "Organisation1", "Mixalis", "Avramidis", "Employee No3", ResourceStatusDTO.AVAILABLE, personnelCategory1);

		PersonnelDTO personnelDTO4 = createPersonnelDTO(true, "RankType2", "Organisation1", "Nikos", "Oikonomou", "Employee No4", ResourceStatusDTO.AVAILABLE, personnelCategory2);

		PersonnelDTO personnelDTO5 = createPersonnelDTO(true, "RankType3", "Organisation1", "Dimitris", "Kallioras", "Employee No5", ResourceStatusDTO.AVAILABLE, personnelCategory1);
		
		PersonnelDTO personnelDTO6 = createPersonnelDTO(true, "RankType3", "Organisation1", "Panos", "Fourlas", "Employee No6", ResourceStatusDTO.AVAILABLE, personnelCategory2);
	}

	
	private PersonnelDTO createPersonnelDTO(Boolean availability, String rankType,String organisationTitle, 
											String firstName, String lastName, String personnelTitle, ResourceStatusDTO status, PersonnelCategoryDTO personnelCategory)
											throws ClassNotFoundException {

		RankTypeDTO rankTypeDTO = (RankTypeDTO) typeService.findDTOByTitle(rankType);
		OrganisationDTO organisationDTO = organisationService.findDTOByTitle(organisationTitle);
		if(rankTypeDTO != null && organisationDTO != null) {
			PersonnelDTO personnelDTO = new PersonnelDTO();
			personnelDTO.setAvailability(availability);
			personnelDTO.setFirstName(firstName);
			personnelDTO.setLastName(lastName);
			personnelDTO.setTitle(personnelTitle);
			personnelDTO.setOrganisation(organisationDTO);
			personnelDTO.setRank(rankTypeDTO);
			personnelDTO.setStatus(status);
			personnelDTO.setPersonnelCategory(personnelCategory);
			return personnelService.createPersonnelRemote(personnelDTO, this.userID);
		}
		else {
			ESponderLogger.debug(this.getClass(), "Unknown rank type or organisation passed for personnel creation");
			return null;
		}

	}

}

