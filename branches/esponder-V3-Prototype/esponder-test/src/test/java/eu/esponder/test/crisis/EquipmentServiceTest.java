package eu.esponder.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.category.EquipmentCategoryDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.EquipmentSnapshotStatusDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class EquipmentServiceTest extends ControllerServiceTest {
	
	private static int SECONDS = 20;
	
	@Test(groups="createResources")
	public void testCreateEquipmentDTO() throws ClassNotFoundException {
		
		EquipmentTypeDTO wimax = (EquipmentTypeDTO) typeService.findDTOByTitle("FRU WiMAX");
		EquipmentTypeDTO wifi = (EquipmentTypeDTO) typeService.findDTOByTitle("FRU WiFi");
		
		ActorFRCDTO firstFRC = actorService.findFRCByTitleRemote("FRC #1");
		ActorFRDTO firstFR = actorService.findFRByTitleRemote("FR #1.1");
		ActorFRDTO secondFR = actorService.findFRByTitleRemote("FR #1.2");
		ActorFRCDTO secondFRC = actorService.findFRCByTitleRemote("FRC #2");
		ActorFRDTO thirdFR= actorService.findFRByTitleRemote("FR #2.1");
		ActorFRDTO fourthFR = actorService.findFRByTitleRemote("FR #2.2");
		
		createEquipmentDTO(wimax, "FRU #1", firstFRC);
		createEquipmentDTO(wifi, "FRU #1.1", firstFR);
		createEquipmentDTO(wifi, "FRU #1.2", secondFR);
		createEquipmentDTO(wimax, "FRU #2", secondFRC);
		createEquipmentDTO(wifi, "FRU #2.1", thirdFR);
		createEquipmentDTO(wifi, "FRU #2.2", fourthFR);
	}
	
	@Test(groups="createSnapshots")
	public void testCreateEquipmentSnapshots() throws ClassNotFoundException {
		
		EquipmentDTO firstFRCEquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #1");
		EquipmentDTO firstFREquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #1.1");
		EquipmentDTO secondFREquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #1.2");
		EquipmentDTO secondFRCEquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #2");
		EquipmentDTO thirdFREquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #2.1");
		EquipmentDTO fourthFREquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #2.2");

		PeriodDTO period = this.createPeriodDTO(SECONDS);
		createEquipmentSnapshotDTO(firstFRCEquipment, period);
		createEquipmentSnapshotDTO(firstFREquipment, period);
		createEquipmentSnapshotDTO(secondFREquipment, period);
		createEquipmentSnapshotDTO(secondFRCEquipment, period);
		createEquipmentSnapshotDTO(thirdFREquipment, period);
		createEquipmentSnapshotDTO(fourthFREquipment, period);
	}
	
	private EquipmentDTO createEquipmentDTO(EquipmentTypeDTO type, String title, FirstResponderActorDTO actor) {
		
		EquipmentDTO equipment = new EquipmentDTO();		
		EquipmentCategoryDTO equipmentCategory = resourceCategoryService.findEquipmentCategoryDTOByType(type);
		equipment.setEquipmentCategoryId(equipmentCategory.getId());
		equipment.setTitle(title);
		equipment.setStatus(ResourceStatusDTO.AVAILABLE);
		equipment.setActor(actor.getId());
		return equipmentService.createEquipmentRemote(equipment, this.userID);
	}
	
	private EquipmentSnapshotDTO createEquipmentSnapshotDTO(EquipmentDTO equipment, PeriodDTO period) {
		
		EquipmentSnapshotDTO snapshot = new EquipmentSnapshotDTO();
		snapshot.setEquipment(equipment);
		snapshot.setPeriod(period);
		snapshot.setStatus(EquipmentSnapshotStatusDTO.WORKING);
		return equipmentService.createEquipmentSnapshotRemote(snapshot, this.userID);
	}
	
}