package eu.esponder.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class UserServiceTest extends ControllerServiceTest {

	@Test(groups = "createBasics")
	public void testCreateUsers() {
		 ESponderUserDTO pki = new ESponderUserDTO();
		 //pki.setUserName("pki");
		 pki.setTitle("pki");
		 pki.setStatus(ResourceStatusDTO.AVAILABLE);
		 userService.createUserRemote(pki, null);
		
		 ESponderUserDTO ctri = new ESponderUserDTO();
		 ctri.setTitle("ctri");
		 ctri.setStatus(ResourceStatusDTO.AVAILABLE);
		 userService.createUserRemote(ctri, null);
	
		 ESponderUserDTO gleo = new ESponderUserDTO();
		 gleo.setTitle("gleo");
		 gleo.setStatus(ResourceStatusDTO.AVAILABLE);
		 userService.createUserRemote(gleo, null);
		//
		 ESponderUserDTO kotso = new ESponderUserDTO();
		 kotso.setTitle("kotso");
		 kotso.setStatus(ResourceStatusDTO.AVAILABLE);
		 userService.createUserRemote(kotso, null);

		 ESponderUserDTO dimitris = new ESponderUserDTO();
		 dimitris.setTitle("Dimitris");
		 dimitris.setStatus(ResourceStatusDTO.AVAILABLE);
		 userService.createUserRemote(dimitris, null);
		//
		 ESponderUserDTO thomas = new ESponderUserDTO();
		 thomas.setTitle("Thomas");
		 thomas.setStatus(ResourceStatusDTO.AVAILABLE);
		 userService.createUserRemote(thomas, null);
		//
		 ESponderUserDTO Jesus = new ESponderUserDTO();
		 Jesus.setTitle("Jesus");
		 Jesus.setStatus(ResourceStatusDTO.AVAILABLE);
		 userService.createUserRemote(Jesus, null);
		//
		 ESponderUserDTO Fabian = new ESponderUserDTO();
		 Fabian.setTitle("Fabian");
		 Fabian.setStatus(ResourceStatusDTO.AVAILABLE);
		 userService.createUserRemote(Fabian, null);

	}

}
