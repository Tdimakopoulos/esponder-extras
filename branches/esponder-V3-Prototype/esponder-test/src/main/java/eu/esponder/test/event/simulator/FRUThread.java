package eu.esponder.test.event.simulator;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.naming.NamingException;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.controller.crisis.resource.SensorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.ArithmeticMeasurementSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationMeasurementSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.dto.osgi.service.event.ESponderEventPublisher;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.util.ejb.ServiceLocator;

public class FRUThread extends Thread {

	private Long fruId;

	private ActorDTO eventSource;

	private long threadPeriodFRU;

	private List<Thread> threadsList;

	private List<SensorDTO> sensors;

	private ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher;

	public List<SensorMeasurementStatisticDTO> statisticsList;

	private int rangeTemp;

	private int minTemp;

	private int minStep;

	private int maxStep;

	private PointDTO startPoint;

	private PointDTO destPoint;

	public FRUThread(ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher,
			ActorDTO eventSource, long threadPeriodFRU, int range,
			int min, PointDTO startingPoint, PointDTO destinationPoint, int minStep, int maxStep) throws ClassNotFoundException {
		this.publisher = publisher;
		this.eventSource = eventSource;
		this.sensors = createListOfSensorsFromActor(this.eventSource);
		this.threadPeriodFRU = threadPeriodFRU;
		statisticsList = new ArrayList<SensorMeasurementStatisticDTO>();
		this.rangeTemp = range;
		this.minTemp = min;
		this.setMinStep(minStep);
		this.setMaxStep(maxStep);
		this.setStartPoint(startingPoint);
		this.setDestPoint(destinationPoint);
	}

	@Override
	public void run() {

		int counter = 15;

		this.setThreadsList(CreateSensorThreads());

		for(Thread thread : threadsList) {
			thread.start();
		}

		while(counter > 0) {
			try {
				// 1. Get measurements from SensorThreads
				if(getStatisticsList().size() != 0)
					// 2. Put them into a SensorMeasurementStatisticEnvelopeDTO
					accessStatistics(this.getPublisher());
				else
					System.out.println("\nEnvelope is empty \n");
				counter--;
				Thread.sleep(threadPeriodFRU);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		for(Thread thread : threadsList)
			thread.interrupt();

		for(Thread thread : threadsList) {
			try {
				thread.join(500);
			} catch (InterruptedException e) {
				System.out.println("Interrupted while waiting for Thread "+thread.getName()+"/"+thread.getId()+" to die...");
			}
		}

		printThreadsState();

		if(getStatisticsList().size() != 0) {
			accessStatistics(publisher);
		}
	}


	private List<Thread> CreateSensorThreads() {

		List<Thread> tempThreadsList = new ArrayList<Thread>();

		System.out.println("Thread executing : " + Thread.currentThread().getName() + " with id : "+Thread.currentThread().getId());
		for(SensorDTO sensor : this.sensors) {
			for(StatisticsConfigDTO config : sensor.getConfiguration()) {
				SensorThread sensorThread = null;
				if (sensor instanceof ArithmeticMeasurementSensorDTO) { 
					sensorThread = new ArithmeticSensorThread(new Long(config.getSamplingPeriodMilliseconds()), sensor, config.getMeasurementStatisticType(), minTemp, rangeTemp, statisticsList);
				} else if (sensor instanceof LocationMeasurementSensorDTO) {
					sensorThread = new LocationSensorThread(new Long(config.getSamplingPeriodMilliseconds()), sensor, config.getMeasurementStatisticType(), this.getStartPoint(),
							this.getDestPoint(), statisticsList, this.getMinStep(), this.getMaxStep());
				}
				tempThreadsList.add(sensorThread);
			}
		}
		return tempThreadsList;
	}

	private void accessStatistics(ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher) {

		SensorMeasurementStatisticEnvelopeDTO envelope = new SensorMeasurementStatisticEnvelopeDTO();
		envelope.setMeasurementStatistics(new ArrayList<SensorMeasurementStatisticDTO>());
		synchronized (this.statisticsList) {

			for( Iterator< SensorMeasurementStatisticDTO > it = statisticsList.iterator(); it.hasNext();)
			{
				SensorMeasurementStatisticDTO statisticMeasurement = it.next();
				envelope.getMeasurementStatistics().add(statisticMeasurement);
				it.remove();
			}

//			System.out.println(Thread.currentThread().getClass() + " / " + Thread.currentThread().getName() + "with id : "
//					+Thread.currentThread().getId()+"\nEnvelope Contents size is : " + envelope.getMeasurementStatistics().size());

			//	3. Publish a CreateSensorMeasurementStatisticEvent
			CreateSensorMeasurementStatisticEvent event = createSensorMeasurementStatisticEvent(envelope);
			try {
				publisher.publishEvent(event);
			} catch (EventListenerException e) {
				e.printStackTrace();
			}
		}

	}

	private CreateSensorMeasurementStatisticEvent createSensorMeasurementStatisticEvent(SensorMeasurementStatisticEnvelopeDTO envelope) {
		CreateSensorMeasurementStatisticEvent event = new CreateSensorMeasurementStatisticEvent();
		event.setEventAttachment(envelope);
		event.setEventSeverity(SeverityLevelDTO.UNDEFINED);
		event.setEventSource(this.eventSource);
		event.setJournalMessage("Test Journal Message");
		event.setEventTimestamp(new Date());
		return event;
	}

	private List<SensorDTO> createListOfSensorsFromActor(ActorDTO actor) throws ClassNotFoundException {
		
		List<SensorDTO> fruSensors = new ArrayList<SensorDTO>();
		for(EquipmentDTO actorEquipment : ((FirstResponderActorDTO)actor).getEquipmentSet()) {
			for(Long sensorID: actorEquipment.getSensors()) {
				Sensor sensor = getSensorService().findSensorById(sensorID);
				SensorDTO sensorDTO = (SensorDTO) getMappingService().mapESponderEntity(sensor, 
						getMappingService().getDTOEntityClass(sensor.getClass())); 
				if(sensorDTO.getType().contentEquals("TEMP") || sensorDTO.getType().contentEquals("LOCATION"))
					fruSensors.add(sensorDTO);
			}
		}
		System.out.println("Sensor Created : "+fruSensors.size());
		return fruSensors;
	}

	private void printThreadsState() {

		System.out.println("\n\n********************	Threads State	********************");
		System.out.println(Thread.currentThread().getName()+"/"+Thread.currentThread().getId());
		for(Thread thread : this.threadsList) {
			System.out.println(thread.getName()+":"+thread.getId()+":"+ thread.getState()+":"+ thread.isAlive());
		}
	}
	
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected SensorService getSensorService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}


	public synchronized List<SensorMeasurementStatisticDTO> getStatisticsList() {
		return statisticsList;
	}

	public synchronized void setStatisticsList(List<SensorMeasurementStatisticDTO> statisticsList) {
		this.statisticsList = statisticsList;
	}

	public Long getFruId() {
		return fruId;
	}

	public void setFruId(Long fruId) {
		this.fruId = fruId;
	}

	public long getThreadPeriodFRU() {
		return threadPeriodFRU;
	}

	public void setThreadPeriodFRU(long threadPeriod) {
		this.threadPeriodFRU = threadPeriod;
	}

	public List<SensorDTO> getSensors() {
		return sensors;
	}

	public void setSensors(List<SensorDTO> sensors) {
		this.sensors = sensors;
	}

	public List<Thread> getThreadsList() {
		return threadsList;
	}

	public void setThreadsList(List<Thread> threadsList) {
		this.threadsList = threadsList;
	}

	public ActorDTO getEventSource() {
		return eventSource;
	}

	public void setEventSource(ActorDTO eventSource) {
		this.eventSource = eventSource;
	}

	public PointDTO getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(PointDTO startPoint) {
		this.startPoint = startPoint;
	}

	public PointDTO getDestPoint() {
		return destPoint;
	}

	public void setDestPoint(PointDTO destPoint) {
		this.destPoint = destPoint;
	}

	public ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> getPublisher() {
		return publisher;
	}

	public void setPublisher(
			ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher) {
		this.publisher = publisher;
	}

	public int getMinStep() {
		return minStep;
	}

	public void setMinStep(int minStep) {
		this.minStep = minStep;
	}

	public int getMaxStep() {
		return maxStep;
	}

	public void setMaxStep(int maxStep) {
		this.maxStep = maxStep;
	}

}
