/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.CrisisService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
// 
/**
 * The Class CrisisContextIdConverter.
 */
public class CrisisContextIdConverter implements CustomConverter {
	
	/**
	 * Gets the crisis service.
	 *
	 * @return the crisis service
	 */
	protected CrisisService getCrisisService() {
		try {
			return ServiceLocator.getResource("esponder/CrisisBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		
		if (sourceClass == CrisisContext.class && source != null) {	
			CrisisContext sourceCrisisContext = (CrisisContext) source;
			Long crisisContextID = sourceCrisisContext.getId();
			destination = crisisContextID;
		}
		else if(sourceClass == Long.class && source!=null) {
			Long crisisContextID = (Long) source;
			CrisisContext destCrisisContext = (CrisisContext) this.getCrisisService().findCrisisContextById(crisisContextID);
			destination = destCrisisContext;
		}
		else {
			new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
