/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.resource.FRTeam;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class SetOfSubordinateTeamsInMEOCs implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected ActorService getActorService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			
			if(it.hasNext()){
				Object next = it.next();
				if(next.getClass() == FRTeam.class) {

					Set<FRTeam> sourceFRTeamSet = (Set<FRTeam>) source;
					Set<Long> destFRTeamDTOSet = new HashSet<Long>();
					for(FRTeam team : sourceFRTeamSet) {
						destFRTeamDTOSet.add(team.getId());
					}
					destination = destFRTeamDTOSet;
				}
				else
					if(next.getClass() == Long.class) {

						Set<Long> sourceFRTeamDTOSet = (Set<Long>) source;
						Set<FRTeam> destFRTeamSet = new HashSet<FRTeam>();
						for(Long teamID : sourceFRTeamDTOSet) {
							FRTeam destFRTeam = (FRTeam) this.getActorService().findFRTeamById(teamID);
							destFRTeamSet.add(destFRTeam);
						}
						destination = destFRTeamSet;
					}
					else
						destination = null;
			}
		}
		else {
			new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
