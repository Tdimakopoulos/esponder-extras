/*
 * 
 */
package eu.esponder.controller.crisis.user;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.user.ESponderUserDTO;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface UserRemoteService.
 */
@Remote
public interface UserRemoteService {
	
	/**
	 * Find user by id remote.
	 *
	 * @param userID the user id
	 * @return the e sponder user dto
	 */
	public ESponderUserDTO findUserByIdRemote(Long userID);

	/**
	 * Find user by name remote.
	 *
	 * @param userName the user name
	 * @return the e sponder user dto
	 */
	public ESponderUserDTO findUserByNameRemote(String userName);

	/**
	 * Creates the user remote.
	 *
	 * @param user the user
	 * @param userID the user id
	 * @return the e sponder user dto
	 */
	public ESponderUserDTO createUserRemote(ESponderUserDTO user, Long userID);

	/**
	 * Update user remote.
	 *
	 * @param user the user
	 * @param userID the user id
	 * @return the e sponder user dto
	 */
	public ESponderUserDTO updateUserRemote(ESponderUserDTO user, Long userID);

	/**
	 * Find all users remote.
	 *
	 * @return the list
	 */
	public List<ESponderUserDTO> findAllUsersRemote();

	/**
	 * Delete user remote.
	 *
	 * @param deletedUserId the deleted user id
	 * @param userID the user id
	 * @return the long
	 */
	public Long deleteUserRemote(Long deletedUserId, Long userID);

}
