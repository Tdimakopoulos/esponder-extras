/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.util.ejb.ServiceLocator;


// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartActorConverter.
 */
public class ActionPartActorConverter implements CustomConverter {
	
	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the actor service.
	 *
	 * @return the actor service
	 */
	protected ActorService getActorService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {
		
		if (source instanceof Actor && source != null) {
			Object destActorDTO;
			try {
				destActorDTO = destinationClass.newInstance();
				Actor sourceActor = (Actor) source;
				((Actor)destActorDTO).setId(sourceActor.getId());
				((Actor)destActorDTO).setTitle(sourceActor.getTitle());
				
				destination = destActorDTO;
			} catch (InstantiationException e) {
				new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
				return null;
			} catch (IllegalAccessException e) {
				new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
				return null;
			}
			
		}
		else if(source instanceof ActorDTO && source != null) {
			ActorDTO sourceActorDTO = (ActorDTO) source;
			Actor destActor = this.getActorService().findById(sourceActorDTO.getId());
			destination = destActor;
		}
		else {
			new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
