/*
 * 
 */
package eu.esponder.controller.crisis.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.CrisisService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.CrisisContextParameterDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.crisis.resource.plan.PlannableResourcePowerDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.CrisisContextParameter;
import eu.esponder.model.crisis.resource.plan.CrisisResourcePlan;
import eu.esponder.model.crisis.resource.plan.PlannableResourcePower;
import eu.esponder.model.snapshot.CrisisContextSnapshot;

// TODO: Auto-generated Javadoc
// 
// 
/**
 * The Class CrisisBean.
 */
@Stateless
public class CrisisBean implements CrisisService, CrisisRemoteService{

	/** The crisis crud service. */
	@EJB
	private CrudService<CrisisContext> crisisCrudService;

	/** The crisis parameters crud service. */
	@EJB
	private CrudService<CrisisContextParameter> crisisParametersCrudService;

	/** The power crud service. */
	@EJB
	CrudService<PlannableResourcePower> powerCrudService;

	/** The crisis resource plan crud service. */
	@EJB
	private CrudService<CrisisResourcePlan> crisisResourcePlanCrudService;

	/** The crisis snapshot crud service. */
	@EJB
	private CrudService<CrisisContextSnapshot> crisisSnapshotCrudService;

	/** The mapping service. */
	@EJB
	ESponderMappingService mappingService;


	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findCrisisContextDTOById(java.lang.Long)
	 */
	@Override
	public CrisisContextDTO findCrisisContextDTOById(Long crisisContextID) {
		CrisisContext crisisContext = findCrisisContextById(crisisContextID);
		return (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
	}	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findCrisisContextById(java.lang.Long)
	 */
	@Override
	public CrisisContext findCrisisContextById(Long crisisContextID) {
		return (CrisisContext) crisisCrudService.find(CrisisContext.class, crisisContextID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findCrisisContextDTOById(java.lang.Long)
	 */
	@Override
	public CrisisContextParameterDTO findCrisisContextParameterDTOById(Long crisisParamID) {
		CrisisContextParameter crisisParam = findCrisisContextParameterById(crisisParamID);
		return (CrisisContextParameterDTO) mappingService.mapESponderEntity(crisisParam, CrisisContextParameterDTO.class);
	}	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findCrisisContextById(java.lang.Long)
	 */
	@Override
	public CrisisContextParameter findCrisisContextParameterById(Long crisisParamID) {
		return (CrisisContextParameter) crisisParametersCrudService.find(CrisisContextParameter.class, crisisParamID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findPlannableResourcePowerByIdRemote(java.lang.Long)
	 */
	@Override
	public PlannableResourcePowerDTO findPlannableResourcePowerByIdRemote(Long powerID) {
		return (PlannableResourcePowerDTO) mappingService.mapESponderEntity(findPlannableResourcePowerById(powerID), PlannableResourcePowerDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findPlannableResourcePowerById(java.lang.Long)
	 */
	@Override
	public PlannableResourcePower findPlannableResourcePowerById(Long powerID) {
		return (PlannableResourcePower) powerCrudService.find(PlannableResourcePower.class, powerID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findCrisisResourcePlanByIdRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public CrisisResourcePlanDTO findCrisisResourcePlanByIdRemote(Long crisisResourcePlanId, Long userId) {
		CrisisResourcePlan crisisResourcePlan = findCrisisResourcePlanById(crisisResourcePlanId, userId);
		return (CrisisResourcePlanDTO) mappingService.mapESponderEntity(crisisResourcePlan, CrisisResourcePlanDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findCrisisResourcePlanById(java.lang.Long, java.lang.Long)
	 */
	@Override
	public CrisisResourcePlan findCrisisResourcePlanById(Long crisisResourcePlanId, Long userId) {
		return crisisResourcePlanCrudService.find(CrisisResourcePlan.class, crisisResourcePlanId);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findCrisisContextSnapshotDTOById(java.lang.Long)
	 */
	@Override
	public CrisisContextSnapshotDTO findCrisisContextSnapshotDTOById(Long crisisContextSnapshotID) {
		CrisisContextSnapshot crisisContextSnapshot = findCrisisContextSnapshotById(crisisContextSnapshotID);
		return (CrisisContextSnapshotDTO) mappingService.mapESponderEntity(crisisContextSnapshot, CrisisContextSnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findCrisisContextSnapshotById(java.lang.Long)
	 */
	@Override
	public CrisisContextSnapshot findCrisisContextSnapshotById(Long crisisContextSnapshotID) {
		return (CrisisContextSnapshot) crisisSnapshotCrudService.find(CrisisContextSnapshot.class, crisisContextSnapshotID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findCrisisResourcePlanByTitleRemote(java.lang.String, java.lang.Long)
	 */
	@Override
	public CrisisResourcePlanDTO findCrisisResourcePlanByTitleRemote(String crisisResourcePlanTitle, Long userId) {
		CrisisResourcePlan crisisResourcePlan = findCrisisResourcePlanByTitle(crisisResourcePlanTitle, userId);
		return (CrisisResourcePlanDTO) mappingService.mapESponderEntity(crisisResourcePlan, CrisisResourcePlanDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findCrisisResourcePlanByTitle(java.lang.String, java.lang.Long)
	 */
	@Override
	public CrisisResourcePlan findCrisisResourcePlanByTitle(String crisisResourcePlanTitle, Long userId) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("title", crisisResourcePlanTitle);
		return (CrisisResourcePlan) crisisResourcePlanCrudService.findSingleWithNamedQuery("CrisisResourcePlan.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findCrisisResourcePlanByTitleRemote(java.lang.String, java.lang.Long)
	 */
	@Override
	public CrisisContextParameterDTO findCrisisParameterByNameRemote(String crisisParamName, Long userId) {
		CrisisContextParameter crisisParam = findCrisisParameterByName(crisisParamName, userId);
		return (CrisisContextParameterDTO) mappingService.mapESponderEntity(crisisParam, CrisisContextParameterDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findCrisisResourcePlanByTitle(java.lang.String, java.lang.Long)
	 */
	@Override
	public CrisisContextParameter findCrisisParameterByName(String crisisParamName, Long userId) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("paramName", crisisParamName);
		return (CrisisContextParameter) crisisParametersCrudService.findSingleWithNamedQuery("CrisisContextParameter.findByName", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findAllCrisisContextsRemote()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CrisisContextDTO> findAllCrisisContextsRemote() {
		List<CrisisContext> crisisContexts = findAllCrisisContexts();
		return (List<CrisisContextDTO>) mappingService.mapESponderEntity(crisisContexts, CrisisContextDTO.class);
	}	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findAllCrisisContexts()
	 */
	@Override
	public List<CrisisContext> findAllCrisisContexts() {
		return (List<CrisisContext>) crisisCrudService.findWithNamedQuery("CrisisContext.findAll");
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findAllCrisisResourcePlansRemote()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CrisisResourcePlanDTO> findAllCrisisResourcePlansRemote() {
		List<CrisisResourcePlan> crisisResourcePlans = findAllCrisisResourcePlans();
		return (List<CrisisResourcePlanDTO>) mappingService.mapESponderEntity(crisisResourcePlans, CrisisResourcePlanDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findAllCrisisResourcePlans()
	 */
	@Override
	public List<CrisisResourcePlan> findAllCrisisResourcePlans() {
		return (List<CrisisResourcePlan>) crisisResourcePlanCrudService.findWithNamedQuery("CrisisResourcePlan.findAll");
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findCrisisContextDTOByTitle(java.lang.String)
	 */
	@Override
	public CrisisContextDTO findCrisisContextDTOByTitle(String title) {
		CrisisContext crisisContext = findCrisisContextByTitle(title);
		return (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
	}	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findCrisisContextByTitle(java.lang.String)
	 */
	@Override
	public CrisisContext findCrisisContextByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (CrisisContext) crisisCrudService.findSingleWithNamedQuery("CrisisContext.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#createCrisisContextRemote(eu.esponder.dto.model.crisis.CrisisContextDTO, java.lang.Long)
	 */
	@Override
	public CrisisContextDTO createCrisisContextRemote(CrisisContextDTO crisisContextDTO, Long userID) {
		CrisisContext crisisContext = (CrisisContext) mappingService.mapESponderEntityDTO(crisisContextDTO, CrisisContext.class);
		crisisContext = createCrisisContext(crisisContext, userID);
		CrisisContextDTO crisisContextDTOPersisted = (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);


		return crisisContextDTOPersisted;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#createCrisisContext(eu.esponder.model.crisis.CrisisContext, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public CrisisContext createCrisisContext(CrisisContext crisisContext, Long userID) {
		return (CrisisContext) crisisCrudService.create(crisisContext);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#createCrisisParameterRemote(java.lang.Long, java.lang.String, java.lang.String, java.lang.Long)
	 */
	@Override
	public CrisisContextParameterDTO createCrisisParameterRemote(Long crisisContextID, String paramName, String paramValue, Long userID) {
		CrisisContextParameter crisisParamPersisted = createCrisisParameter(crisisContextID, paramName, paramValue, userID);
		CrisisContextParameterDTO crisisParamDTOPersisted = (CrisisContextParameterDTO) mappingService.mapESponderEntity(crisisParamPersisted, CrisisContextParameterDTO.class);
		return crisisParamDTOPersisted;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#createCrisisParameter(java.lang.Long, java.lang.String, java.lang.String, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public CrisisContextParameter createCrisisParameter(Long crisisContextID, String paramName, String paramValue, Long userID) {
		CrisisContext crisisContext = findCrisisContextById(crisisContextID);
		CrisisContextParameter crisisParam = new CrisisContextParameter(crisisContext, paramName, paramValue);
		return (CrisisContextParameter) crisisParametersCrudService.create(crisisParam);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#createCrisisContextSnapshotRemote(eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO, java.lang.Long)
	 */
	@Override
	public CrisisContextSnapshotDTO createCrisisContextSnapshotRemote(CrisisContextSnapshotDTO crisisContextSnapshotDTO, Long userID) {
		CrisisContextSnapshot crisisContextSnapshot = (CrisisContextSnapshot) mappingService.mapESponderEntityDTO(crisisContextSnapshotDTO, CrisisContextSnapshot.class);
		crisisContextSnapshot = createCrisisContextSnapshot(crisisContextSnapshot, userID);
		CrisisContextSnapshotDTO crisisContextSnapshotDTOPersisted = (CrisisContextSnapshotDTO) mappingService.mapESponderEntity(crisisContextSnapshot, CrisisContextSnapshotDTO.class);
		return crisisContextSnapshotDTOPersisted;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#createCrisisContextSnapshot(eu.esponder.model.snapshot.CrisisContextSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public CrisisContextSnapshot createCrisisContextSnapshot(CrisisContextSnapshot crisisContextSnapshot, Long userID) {
		return (CrisisContextSnapshot) crisisSnapshotCrudService.create(crisisContextSnapshot);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#createPlannableResourcePowerRemote(eu.esponder.dto.model.crisis.resource.plan.PlannableResourcePowerDTO, java.lang.Long)
	 */
	@Override
	public PlannableResourcePowerDTO createPlannableResourcePowerRemote(PlannableResourcePowerDTO plannableResourcePowerDTO, Long userID) {
		PlannableResourcePower plannableResourcePower = (PlannableResourcePower) mappingService.mapESponderEntityDTO(plannableResourcePowerDTO, PlannableResourcePower.class);
		return (PlannableResourcePowerDTO) mappingService.mapESponderEntity(createPlannableResourcePower(plannableResourcePower, userID), PlannableResourcePowerDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#createPlannableResourcePower(eu.esponder.model.crisis.resource.plan.PlannableResourcePower, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public PlannableResourcePower createPlannableResourcePower(PlannableResourcePower plannableResourcePower, Long userID) {
		return powerCrudService.create(plannableResourcePower);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#createCrisisResourcePlanRemote(eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO, java.lang.Long)
	 */
	@Override
	public CrisisResourcePlanDTO createCrisisResourcePlanRemote(CrisisResourcePlanDTO crisisResourcePlanDTO, Long userID) {
		CrisisResourcePlan crisisResourcePlan = (CrisisResourcePlan) mappingService.mapESponderEntityDTO(crisisResourcePlanDTO, CrisisResourcePlan.class);
		crisisResourcePlan = createCrisisResourcePlan(crisisResourcePlan, userID);
		CrisisResourcePlanDTO crisisResourcePlanDTOPersisted = (CrisisResourcePlanDTO) mappingService.mapESponderEntity(crisisResourcePlan, CrisisResourcePlanDTO.class);
		return crisisResourcePlanDTOPersisted;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#createCrisisResourcePlan(eu.esponder.model.crisis.resource.plan.CrisisResourcePlan, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public CrisisResourcePlan createCrisisResourcePlan(CrisisResourcePlan crisisResourcePlan, Long userID) {
		return (CrisisResourcePlan) crisisResourcePlanCrudService.create(crisisResourcePlan);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#deletePlannableResourcePowerRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deletePlannableResourcePowerRemote(Long powerID, Long userID) {
		this.deletePlannableResourcePower(powerID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#deletePlannableResourcePower(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deletePlannableResourcePower(Long powerID, Long userID) {
		powerCrudService.delete(PlannableResourcePower.class, powerID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#deleteCrisisContextRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteCrisisContextRemote(Long crisisContextId, Long userID) {
		deleteCrisisContext(crisisContextId, userID);	
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#deleteCrisisContext(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteCrisisContext(Long crisisContextId, Long userID) {
		crisisCrudService.delete(CrisisContext.class, crisisContextId);
	}

	// -------------------------------------------------------------------------

	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#deleteCrisisContextParameterRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public Long deleteCrisisContextParameterRemote(Long crisisContextParameterId, Long userID) {
		return deleteCrisisContextParameter(crisisContextParameterId, userID);	
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#deleteCrisisContextParameter(java.lang.Long, java.lang.Long)
	 */
	@Override
	public Long deleteCrisisContextParameter(Long crisisContextParameterId, Long userID) {
		crisisParametersCrudService.delete(CrisisContextParameter.class, crisisContextParameterId);
		return crisisContextParameterId;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#deleteCrisisContextSnapshotRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteCrisisContextSnapshotRemote(Long crisisContextSnapshotId, Long userID) {
		deleteCrisisContextSnapshot(crisisContextSnapshotId, userID);	
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#deleteCrisisContextSnapshot(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteCrisisContextSnapshot(Long crisisContextSnapshotId, Long userID) {
		crisisSnapshotCrudService.delete(CrisisContextSnapshot.class, crisisContextSnapshotId);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#deleteCrisisResourcePlanRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteCrisisResourcePlanRemote(Long crisisResourcePlanId, Long userID) {
		deleteCrisisResourcePlan(crisisResourcePlanId, userID);	
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#deleteCrisisResourcePlan(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteCrisisResourcePlan(Long crisisResourcePlanId, Long userID) {
		crisisResourcePlanCrudService.delete(CrisisResourcePlan.class, crisisResourcePlanId);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#updateCrisisContextRemote(eu.esponder.dto.model.crisis.CrisisContextDTO, java.lang.Long)
	 */
	@Override
	public CrisisContextDTO updateCrisisContextRemote( CrisisContextDTO crisisContextDTO, Long userID) {
		CrisisContext crisisContext = (CrisisContext) mappingService.mapESponderEntityDTO(crisisContextDTO, CrisisContext.class);
		crisisContext = updateCrisisContext(crisisContext, userID);
		crisisContextDTO = (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
		return crisisContextDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#updateCrisisContext(eu.esponder.model.crisis.CrisisContext, java.lang.Long)
	 */
	@Override
	public CrisisContext updateCrisisContext(CrisisContext crisisContext, Long userID) {
		CrisisContext crisisContextPersisted = findCrisisContextById(crisisContext.getId());
		mappingService.mapEntityToEntity(crisisContext, crisisContextPersisted);
		crisisContextPersisted = crisisCrudService.update(crisisContextPersisted);
		return crisisContextPersisted;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#updateCrisisContextParametersRemote(java.lang.Long, java.lang.String, java.lang.Long)
	 */
	@Override
	public CrisisContextParameterDTO updateCrisisContextParametersRemote( Long paramID, String paramValue, Long userID) {
		CrisisContextParameter crisisParameterUpdated = updateCrisisContextparameter(paramID, paramValue, userID);
		CrisisContextParameterDTO crisisContextParametersDTO = (CrisisContextParameterDTO) mappingService.mapESponderEntity(crisisParameterUpdated, CrisisContextParameterDTO.class);
		return crisisContextParametersDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#updateCrisisContextparameter(java.lang.Long, java.lang.String, java.lang.Long)
	 */
	@Override
	public CrisisContextParameter updateCrisisContextparameter(Long paramID, String paramValue, Long userID) {
		CrisisContextParameter paramPersisted = this.findCrisisContextParameterById(paramID);
		CrisisContextParameter paramToUpdate = paramPersisted;
		paramToUpdate.setParamValue(paramValue);
		mappingService.mapEntityToEntity(paramToUpdate, paramPersisted);
		paramPersisted = crisisParametersCrudService.update(paramPersisted);
		return paramPersisted;
	}


	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#updateCrisisContextSnapshotRemote(eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO, java.lang.Long)
	 */
	@Override
	public CrisisContextSnapshotDTO updateCrisisContextSnapshotRemote( CrisisContextSnapshotDTO crisisContextSnapshotDTO, Long userID) {
		CrisisContextSnapshot crisisContextSnapshot = (CrisisContextSnapshot) mappingService.mapESponderEntityDTO(crisisContextSnapshotDTO, CrisisContextSnapshot.class);
		crisisContextSnapshot = updateCrisisContextSnapshot(crisisContextSnapshot, userID);
		crisisContextSnapshotDTO = (CrisisContextSnapshotDTO) mappingService.mapESponderEntity(crisisContextSnapshot, CrisisContextSnapshotDTO.class);
		return crisisContextSnapshotDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#updateCrisisContextSnapshot(eu.esponder.model.snapshot.CrisisContextSnapshot, java.lang.Long)
	 */
	@Override
	public CrisisContextSnapshot updateCrisisContextSnapshot(CrisisContextSnapshot crisisContextSnapshot, Long userID) {
		CrisisContextSnapshot snapshotPersisted = findCrisisContextSnapshotById(crisisContextSnapshot.getId());
		mappingService.mapEntityToEntity(crisisContextSnapshot, snapshotPersisted);
		snapshotPersisted = crisisSnapshotCrudService.update(snapshotPersisted);
		return snapshotPersisted;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#updatePlannableResourcePowerRemote(eu.esponder.dto.model.crisis.resource.plan.PlannableResourcePowerDTO, java.lang.Long)
	 */
	@Override
	public PlannableResourcePowerDTO updatePlannableResourcePowerRemote(PlannableResourcePowerDTO plannableResourcePowerDTO, Long userID) {
		PlannableResourcePower plannableResourcePower = (PlannableResourcePower) mappingService.mapESponderEntityDTO(plannableResourcePowerDTO, PlannableResourcePower.class);
		return (PlannableResourcePowerDTO) mappingService.mapESponderEntity(updatePlannableResourcePower(plannableResourcePower, userID), PlannableResourcePowerDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#updatePlannableResourcePower(eu.esponder.model.crisis.resource.plan.PlannableResourcePower, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public PlannableResourcePower updatePlannableResourcePower(PlannableResourcePower plannableResourcePower, Long userID) {
		PlannableResourcePower powerPersisted = findPlannableResourcePowerById(plannableResourcePower.getId());
		mappingService.mapEntityToEntity(plannableResourcePower, powerPersisted);
		return (PlannableResourcePower) powerCrudService.update(powerPersisted);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#updateCrisisResourcePlanRemote(eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO, java.lang.Long)
	 */
	@Override
	public CrisisResourcePlanDTO updateCrisisResourcePlanRemote( CrisisResourcePlanDTO crisisResourcePlanDTO, Long userID) {
		CrisisResourcePlan crisisResourcePlan = (CrisisResourcePlan) mappingService.mapESponderEntityDTO(crisisResourcePlanDTO, CrisisResourcePlan.class);
		crisisResourcePlan = updateCrisisResourcePlan(crisisResourcePlan, userID);
		crisisResourcePlanDTO = (CrisisResourcePlanDTO) mappingService.mapESponderEntity(crisisResourcePlan, CrisisResourcePlanDTO.class);
		return crisisResourcePlanDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#updateCrisisResourcePlan(eu.esponder.model.crisis.resource.plan.CrisisResourcePlan, java.lang.Long)
	 */
	@Override
	public CrisisResourcePlan updateCrisisResourcePlan(CrisisResourcePlan crisisResourcePlan, Long userID) {
		CrisisResourcePlan planPersisted = findCrisisResourcePlanById(crisisResourcePlan.getId(), userID);
		mappingService.mapEntityToEntity(crisisResourcePlan, planPersisted);
		planPersisted = crisisResourcePlanCrudService.update(planPersisted);
		return planPersisted;
	}





}
