/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.Date;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface EquipmentRemoteService.
 */
@Remote
public interface EquipmentRemoteService {

	/**
	 * Find by id remote.
	 *
	 * @param equipmentID the equipment id
	 * @return the equipment dto
	 */
	public EquipmentDTO findByIdRemote(Long equipmentID);

	/**
	 * Find by title remote.
	 *
	 * @param title the title
	 * @return the equipment dto
	 */
	public EquipmentDTO findByTitleRemote(String title);

	/**
	 * Creates the equipment remote.
	 *
	 * @param equipmentDTO the equipment dto
	 * @param userID the user id
	 * @return the equipment dto
	 */
	public EquipmentDTO createEquipmentRemote(EquipmentDTO equipmentDTO, Long userID);
	
	/**
	 * Update equipment remote.
	 *
	 * @param equipmentDTO the equipment dto
	 * @param userID the user id
	 * @return the equipment dto
	 */
	public EquipmentDTO updateEquipmentRemote(EquipmentDTO equipmentDTO, Long userID);
	
	/**
	 * Delete equipment remote.
	 *
	 * @param equipmentId the equipment id
	 * @param userID the user id
	 */
	public void deleteEquipmentRemote(Long equipmentId, Long userID);

	/**
	 * Find equipment snapshot by date remote.
	 *
	 * @param equipmentID the equipment id
	 * @param maxDate the max date
	 * @return the equipment snapshot dto
	 */
	public EquipmentSnapshotDTO findEquipmentSnapshotByDateRemote(Long equipmentID, Date maxDate);
	
	/**
	 * Creates the equipment snapshot remote.
	 *
	 * @param snapshotDTO the snapshot dto
	 * @param userID the user id
	 * @return the equipment snapshot dto
	 */
	public EquipmentSnapshotDTO createEquipmentSnapshotRemote(EquipmentSnapshotDTO snapshotDTO, Long userID);

	/**
	 * Update equipment snapshot remote.
	 *
	 * @param snapshotDTO the snapshot dto
	 * @param userID the user id
	 * @return the equipment snapshot dto
	 */
	public EquipmentSnapshotDTO updateEquipmentSnapshotRemote(EquipmentSnapshotDTO snapshotDTO, Long userID);

	/**
	 * Delete equipment snapshot remote.
	 *
	 * @param equipmentSnapshotId the equipment snapshot id
	 * @param userID the user id
	 */
	public void deleteEquipmentSnapshotRemote(Long equipmentSnapshotId, Long userID);
	
}
