/*
 * 
 */
package eu.esponder.controller.mapping;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface ESponderMappingService.
 */
@Local
public interface ESponderMappingService extends ESponderRemoteMappingService {

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#mapESponderEntity(java.util.List, java.lang.Class)
	 */
	public List<? extends ESponderEntityDTO> mapESponderEntity(List<? extends ESponderEntity<Long>> resultsList, Class<? extends ESponderEntityDTO> targetClass);

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#mapESponderEntity(eu.esponder.model.ESponderEntity, java.lang.Class)
	 */
	public ESponderEntityDTO mapESponderEntity(ESponderEntity<Long> entity, Class<? extends ESponderEntityDTO> targetClass);

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#mapESponderEntityDTO(java.util.List, java.lang.Class)
	 */
	public List<? extends ESponderEntity<?>> mapESponderEntityDTO(List<? extends ESponderEntityDTO> resultsList, Class<? extends ESponderEntity<?>> targetClass);

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#mapESponderEntityDTO(eu.esponder.dto.model.ESponderEntityDTO, java.lang.Class)
	 */
	public ESponderEntity<?> mapESponderEntityDTO(ESponderEntityDTO entity, Class<? extends ESponderEntity<?>> targetClass);

	//***************************************************************************************************************	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#mapESponderEnumDTO(java.lang.Object, java.lang.Class)
	 */
	public Object mapESponderEnumDTO(Object entity, Class<?> targetClass);

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#getManagedEntityClass(java.lang.Class)
	 */
	public Class<? extends ESponderEntity<Long>> getManagedEntityClass(Class<? extends ESponderEntityDTO> clz) throws ClassNotFoundException;

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#getDTOEntityClass(java.lang.Class)
	 */
	public Class<? extends ESponderEntityDTO> getDTOEntityClass(Class<? extends ESponderEntity<Long>> clz) throws ClassNotFoundException;

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#getEntityClass(java.lang.String)
	 */
	public Class<?> getEntityClass(String className) throws ClassNotFoundException;

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#getManagedEntityName(java.lang.String)
	 */
	public String getManagedEntityName(String queriedEntityDTO);

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#getDTOEntityName(java.lang.String)
	 */
	public String getDTOEntityName(String entityName);

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#mapEntityToEntity(eu.esponder.model.ESponderEntity, eu.esponder.model.ESponderEntity)
	 */
	public void mapEntityToEntity(ESponderEntity<Long> src, ESponderEntity<Long> dest);

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderRemoteMappingService#mapObject(java.lang.Object, java.lang.Class)
	 */
	public Object mapObject(Object obj, Class<?> targetClass);


}
