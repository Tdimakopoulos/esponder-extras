/*
 * 
 */
package eu.esponder.controller.persistence;

import javax.ejb.Local;

/**
 * The Interface CrudService.
 *
 * @param <T> the generic type
 */
@Local
public interface CrudService<T> extends CrudRemoteService<T> {
    
}
