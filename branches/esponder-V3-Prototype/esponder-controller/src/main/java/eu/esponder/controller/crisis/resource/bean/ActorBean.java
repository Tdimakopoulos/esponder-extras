/*
 * 
 */
package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.crisis.resource.OperationsCentreService;
import eu.esponder.controller.generic.GenericService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.controller.persistence.criteria.EsponderCriterion;
import eu.esponder.controller.persistence.criteria.EsponderCriterionExpressionEnum;
import eu.esponder.dto.model.crisis.resource.ActorCMDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.ActorCM;
import eu.esponder.model.crisis.resource.ActorFR;
import eu.esponder.model.crisis.resource.ActorFRC;
import eu.esponder.model.crisis.resource.ActorIC;
import eu.esponder.model.crisis.resource.FRTeam;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.snapshot.resource.ActorSnapshot;
import eu.esponder.model.user.ESponderUser;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActorBean.
 */
@Stateless
public class ActorBean implements ActorService, ActorRemoteService {

	/** The actor crud service. */
	@EJB
	private CrudService<Actor> actorCrudService;

	/** The fr team crud service. */
	@EJB
	private CrudService<FRTeam> frTeamCrudService;

	/** The actor snapshot crud service. */
	@EJB
	private CrudService<ActorSnapshot> actorSnapshotCrudService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;

	/** The operations centre service. */
	@EJB
	private OperationsCentreService operationsCentreService;

	/** The type service. */
	@EJB
	private TypeService typeService;

	/** The generic service. */
	@EJB
	private GenericService genericService;

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRTeamByIdRemote(java.lang.Long)
	 */
	@Override
	public FRTeamDTO findFRTeamByIdRemote(Long frTeamID) {
		return (FRTeamDTO) mappingService.mapESponderEntity(findFRTeamById(frTeamID), FRTeamDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRTeamById(java.lang.Long)
	 */
	@Override
	public FRTeam findFRTeamById(Long frTeamID) {
		return (FRTeam) frTeamCrudService.find(FRTeam.class, frTeamID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRTeamChiefRemote(java.lang.Long)
	 */
	@Override
	public ActorFRCDTO findFRTeamChiefRemote(Long frTeamID) {
		return (ActorFRCDTO) mappingService.mapESponderEntity(findFRTeamChiefById(frTeamID), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRTeamChiefById(java.lang.Long)
	 */
	@Override
	public ActorFRC findFRTeamChiefById(Long frTeamID) {
		return frTeamCrudService.find(FRTeam.class, frTeamID).getFrchief();
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRTeamsForMeocRemote(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FRTeamDTO> findFRTeamsForMeocRemote(Long meocID) {
		return (List<FRTeamDTO>) mappingService.mapESponderEntity(findFRTeamsForMeoc(meocID), FRTeamDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRTeamsForMeoc(java.lang.Long)
	 */
	@Override
	public List<FRTeam> findFRTeamsForMeoc(Long meocID) {
		OperationsCentre meoc = operationsCentreService.findOperationCentreById(meocID);
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("meoc", meoc);
		return frTeamCrudService.findWithNamedQuery("FRTeam.findTeamsByMeoc", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRTeamForChiefRemote(java.lang.Long)
	 */
	@Override
	public FRTeamDTO findFRTeamForChiefRemote(Long frchiefID) {
		return (FRTeamDTO) mappingService.mapESponderEntity(findFRTeamsForMeoc(frchiefID), FRTeamDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRTeamForChief(java.lang.Long)
	 */
	@Override
	public FRTeam findFRTeamForChief(Long frchiefID) {
		Actor chief = findFRChiefById(frchiefID);
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("frchief", chief);
		return (FRTeam) frTeamCrudService.findSingleWithNamedQuery("FRTeam.findTeamsByChief", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findByIdRemote(java.lang.Long)
	 */

	//FIXME DIMITRIS REMOVE AND IMPLEMENT ACCORDINGLY
	@Override
	public ActorDTO findByIdRemote(Long actorID) {
		return (ActorDTO) mappingService.mapESponderEntity(findById(actorID), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findById(java.lang.Long)
	 */
	@Override
	public Actor findById(Long actorID) {
		return (Actor) actorCrudService.find(Actor.class, actorID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findByIdRemote(java.lang.Long)
	 */

	@Override
	public String findActorClassByIdRemote(Long actorID) {

		Object actor = findActorAsObjectById(actorID);

		if(actor != null ) {
			if(actor.getClass() == ESponderUser.class)
				return ESponderUserDTO.class.getSimpleName();
			else if(actor.getClass() == ActorCM.class)
				return ActorCMDTO.class.getSimpleName();
			else if(actor.getClass() == ActorIC.class)
				return ActorICDTO.class.getSimpleName();
			else if(actor.getClass() == ActorCM.class)
				return ActorCMDTO.class.getSimpleName();
			else if(actor.getClass() == ActorFRC.class)
				return ActorFRCDTO.class.getSimpleName();
			else if(actor.getClass() == ActorFR.class)
				return ActorFRDTO.class.getSimpleName();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findById(java.lang.Long)
	 */
	@Override
	public Object findActorAsObjectById(Long actorID) {
		return (Object) actorCrudService.find(Actor.class, actorID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findESponderUserByIdRemote(java.lang.Long)
	 */
	@Override
	public ESponderUserDTO findESponderUserByIdRemote(Long actorID) {
		return (ESponderUserDTO) mappingService.mapESponderEntity(findESponderUserById(actorID), ESponderUserDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findESponderUserById(java.lang.Long)
	 */
	@Override
	public ESponderUser findESponderUserById(Long actorID) {
		return (ESponderUser) actorCrudService.find(Actor.class, actorID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findCrisisManagerByIdRemote(java.lang.Long)
	 */
	@Override
	public ActorCMDTO findCrisisManagerByIdRemote(Long actorID) {
		return (ActorCMDTO) mappingService.mapESponderEntity(findCrisisManagerById(actorID), ActorCMDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findCrisisManagerById(java.lang.Long)
	 */
	@Override
	public ActorCM findCrisisManagerById(Long actorID) {
		return (ActorCM) actorCrudService.find(Actor.class, actorID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findIncidentCommanderByIdRemote(java.lang.Long)
	 */
	@Override
	public ActorICDTO findIncidentCommanderByIdRemote(Long actorID) {
		return (ActorICDTO) mappingService.mapESponderEntity(findIncidentCommanderById(actorID), ActorICDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findIncidentCommanderById(java.lang.Long)
	 */
	@Override
	public ActorIC findIncidentCommanderById(Long actorID) {
		return (ActorIC) actorCrudService.find(Actor.class, actorID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRByIdRemote(java.lang.Long)
	 */
	@Override
	public ActorFRDTO findFRByIdRemote(Long frID) {
		return (ActorFRDTO) mappingService.mapESponderEntity(findFRById(frID), ActorFRDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRById(java.lang.Long)
	 */
	@Override
	public ActorFR findFRById(Long frID) {
		return (ActorFR) actorCrudService.find(Actor.class, frID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRChiefByIdRemote(java.lang.Long)
	 */
	@Override
	public ActorFRCDTO findFRChiefByIdRemote(Long chiefID) {
		return (ActorFRCDTO) mappingService.mapESponderEntity(findFRChiefById(chiefID), ActorFRCDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRChiefById(java.lang.Long)
	 */
	@Override
	public ActorFRC findFRChiefById(Long chiefID) {
		return (ActorFRC) actorCrudService.find(Actor.class, chiefID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findAllActorsRemote()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ActorDTO> findAllActorsRemote() {
		return (List<ActorDTO>) mappingService.mapESponderEntity(findAllActors(), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findAllActors()
	 */
	@Override
	public List<Actor> findAllActors() {
		return (List<Actor>) actorCrudService.findWithNamedQuery("Actor.findAll");
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findByTitleRemote(java.lang.String)
	 */
	@Override
	public Object findActorByTitleRemote(String title) throws InstantiationException, IllegalAccessException {
		Object actor = findActorByTitle(title);
		if(actor!=null)
			return (ActorDTO) mappingService.mapObject(findActorByTitle(title), actor.getClass());
		else 
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findByTitle(java.lang.String)
	 */
	@Override
	public Object findActorByTitle(String title) throws InstantiationException, IllegalAccessException {
		//			Map<String, Object> params = new HashMap<String, Object>();
		//			params.put("title", title);
		//			return (Actor) actorCrudService.findSingleWithNamedQuery("Actor.findByTitle", params);

		EsponderCriterion criterion = new EsponderCriterion();
		criterion.setExpression(EsponderCriterionExpressionEnum.EQUAL);
		criterion.setField("title");
		criterion.setValue(title);
		List<? extends ESponderEntity<Long>> results = genericService.getEntities(Actor.class, criterion, 10, 1);
		if(!results.isEmpty()) {
			if(results.size()>1)
				return null;
			else {
				Object actor = results.get(0);
				return actor;
			}
		}
		else
			return null;


	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRCByTitleRemote(java.lang.String)
	 */
	@Override
	public ActorFRCDTO findFRCByTitleRemote(String title) {
		return (ActorFRCDTO) mappingService.mapESponderEntity(findFRCByTitle(title), ActorFRCDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRCByTitle(java.lang.String)
	 */
	@Override
	public ActorFRC findFRCByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ActorFRC) actorCrudService.findSingleWithNamedQuery("ActorFRC.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRCByTitleRemote(java.lang.String)
	 */
	@Override
	public ActorFRDTO findFRByTitleRemote(String title) {
		return (ActorFRDTO) mappingService.mapESponderEntity(findFRByTitle(title), ActorFRDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRCByTitle(java.lang.String)
	 */
	@Override
	public ActorFR findFRByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ActorFR) actorCrudService.findSingleWithNamedQuery("ActorFR.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findESponderUserByTitleRemote(java.lang.String)
	 */
	@Override
	public ESponderUserDTO findESponderUserByTitleRemote(String title) {
		return (ESponderUserDTO) mappingService.mapESponderEntity(findESponderUserByTitle(title), ESponderUserDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findESponderUserByTitle(java.lang.String)
	 */
	@Override
	public ESponderUser findESponderUserByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ESponderUser) actorCrudService.findSingleWithNamedQuery("ESponderUser.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRCByTitleRemote(java.lang.String)
	 */
	@Override
	public ActorCMDTO findCMByTitleRemote(String title) {
		return (ActorCMDTO) mappingService.mapESponderEntity(findCMByTitle(title), ActorCMDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRCByTitle(java.lang.String)
	 */
	@Override
	public ActorCM findCMByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ActorCM) actorCrudService.findSingleWithNamedQuery("ActorCM.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRCByTitleRemote(java.lang.String)
	 */
	@Override
	public ActorICDTO findICByTitleRemote(String title) {
		return (ActorICDTO) mappingService.mapESponderEntity(findICByTitle(title), ActorICDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRCByTitle(java.lang.String)
	 */
	@Override
	public ActorIC findICByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ActorIC) actorCrudService.findSingleWithNamedQuery("ActorIC.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findSupervisorForActorRemote(java.lang.Long)
	 */

	@Override
	public ActorDTO findSupervisorForActorRemote(Long actorID) {
		return (ActorDTO) mappingService.mapESponderEntity(findSupervisorForActor(actorID), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findSupervisorForActor(java.lang.Long)
	 */
	@Override
	public Actor findSupervisorForActor(Long actorID) {

		Actor actor = findById(actorID);
		if(actor instanceof ActorFRC)
			return ((ActorFRC)actor).getTeam().getIncidentCommander();
		else if(actor instanceof ActorFR)
			return ((ActorFR)actor).getFRChief();
		else if(actor instanceof ActorIC)
			return ((ActorIC)actor).getSupervisor();
		else if(actor instanceof Actor)
			return ((ActorIC)actor).getSupervisor();
		return null;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findSubordinatesByIdRemote(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ActorDTO> findSubordinatesByIdRemote(Long actorID) {
		return (List<ActorDTO>) mappingService.mapESponderEntity(findSubordinatesById(actorID), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findSubordinatesById(java.lang.Long)
	 */
	@Override
	public List<Actor> findSubordinatesById(Long actorID) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("actorID", actorID);
		return  actorCrudService.findWithNamedQuery("Actor.findSubordinatesForActor", parameters);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findActorSnapshotByDateRemote(java.lang.Long, java.util.Date)
	 */
	@Override
	public ActorSnapshotDTO findActorSnapshotByDateRemote(Long actorID, Date maxDate) {
		return (ActorSnapshotDTO) mappingService.mapESponderEntity(findActorSnapshotByDate(actorID, maxDate), ActorSnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findActorSnapshotByDate(java.lang.Long, java.util.Date)
	 */
	@Override
	public ActorSnapshot findActorSnapshotByDate(Long actorID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("actorID", actorID);
		params.put("maxDate", maxDate.getTime());
		ActorSnapshot snapshot =
				(ActorSnapshot) actorSnapshotCrudService.findSingleWithNamedQuery("ActorSnapshot.findByActorAndDate", params);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#createIncidentCommanderRemote(eu.esponder.dto.model.crisis.resource.ActorICDTO, long)
	 */
	@Override
	public ActorICDTO createIncidentCommanderRemote(ActorICDTO incidentCommanderDTO, long userID) {
		ActorIC actor = (ActorIC) mappingService.mapESponderEntityDTO(incidentCommanderDTO, ActorIC.class);
		actor = (ActorIC) createIncidentCommander(actor, userID);
		return (ActorICDTO) mappingService.mapESponderEntity(actor, ActorICDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#createIncidentCommander(eu.esponder.model.crisis.resource.ActorIC, long)
	 */
	@Override
	public ActorIC createIncidentCommander(ActorIC incidentCommander, long userID) {

		return (ActorIC) actorCrudService.create(incidentCommander);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#createCrisisManagerRemote(eu.esponder.dto.model.crisis.resource.ActorCMDTO, long)
	 */
	@Override
	public ActorCMDTO createCrisisManagerRemote(ActorCMDTO crisisManagerDTO, long userID) {

		ActorCM actor = (ActorCM) mappingService.mapESponderEntityDTO(crisisManagerDTO, ActorCM.class);
		actor = (ActorCM) createCrisisManager(actor, userID);
		return (ActorCMDTO) mappingService.mapESponderEntity(actor, ActorCMDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#createCrisisManager(eu.esponder.model.crisis.resource.ActorCM, long)
	 */
	@Override
	public ActorCM createCrisisManager(ActorCM crisisManager, long userID) {
		return (ActorCM) actorCrudService.create(crisisManager);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#createFRChiefRemote(eu.esponder.dto.model.crisis.resource.ActorFRCDTO, long)
	 */
	@Override
	public ActorFRCDTO createFRChiefRemote(ActorFRCDTO frChiefDTO, long userID) {

		ActorFRC chief = (ActorFRC) mappingService.mapESponderEntityDTO(frChiefDTO, ActorFRC.class);
		chief = (ActorFRC) createFRChief(chief, userID);
		return (ActorFRCDTO) mappingService.mapESponderEntity(chief, ActorFRCDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#createFRChief(eu.esponder.model.crisis.resource.ActorFRC, long)
	 */
	@Override
	public ActorFRC createFRChief(ActorFRC frChief, long userID) {
		return (ActorFRC) actorCrudService.create(frChief);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#createFRRemote(eu.esponder.dto.model.crisis.resource.ActorFRDTO, long)
	 */
	@Override
	public ActorFRDTO createFRRemote(ActorFRDTO frDTO, long userID) {

		ActorFR fr = (ActorFR) mappingService.mapESponderEntityDTO(frDTO, ActorFR.class);
		fr = (ActorFR) createFR(fr, userID);
		return (ActorFRDTO) mappingService.mapESponderEntity(fr, ActorFRDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#createFR(eu.esponder.model.crisis.resource.ActorFR, long)
	 */
	@Override
	public ActorFR createFR(ActorFR fr, long userID) {
		return (ActorFR) actorCrudService.create(fr);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#createESponderUseremote(eu.esponder.dto.model.user.ESponderUserDTO, long)
	 */
	@Override
	public ESponderUserDTO createESponderUseremote(ESponderUserDTO userDTO, long userID) {

		ESponderUser user = (ESponderUser) mappingService.mapESponderEntityDTO(userDTO, ESponderUser.class);
		user = (ESponderUser) createESponderUser(user, userID);
		return (ESponderUserDTO) mappingService.mapESponderEntity(user, ESponderUserDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#createESponderUser(eu.esponder.model.user.ESponderUser, long)
	 */
	@Override
	public ESponderUser createESponderUser(ESponderUser user, long userID) {
		return (ESponderUser) actorCrudService.create(user);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#deleteActorRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActorRemote(Long actorID, Long userID) {
		this.deleteActor(actorID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#deleteActor(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActor(Long actorID, Long userID) {
		Actor actor = actorCrudService.find(Actor.class, actorID);
		actorCrudService.delete(actor);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#updateActorRemote(eu.esponder.dto.model.crisis.resource.ActorDTO, java.lang.Long)
	 */
	@Override
	public ActorICDTO updateIncidentCommanderRemote(ActorICDTO actorICDTO, Long userID) {
		ActorIC actorIC = (ActorIC) mappingService.mapESponderEntityDTO(actorICDTO, ActorIC.class);
		return (ActorICDTO) mappingService.mapESponderEntity(updateIncidentCommander(actorIC, userID), ActorICDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#updateActor(eu.esponder.model.crisis.resource.Actor, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorIC updateIncidentCommander(ActorIC actorIC, Long userID) {
		ActorIC pactorIC = findIncidentCommanderById(actorIC.getId());
		mappingService.mapEntityToEntity(actorIC, pactorIC);
		return (ActorIC) actorCrudService.update(pactorIC);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#updateActorRemote(eu.esponder.dto.model.crisis.resource.ActorDTO, java.lang.Long)
	 */
	@Override
	public ESponderUserDTO updateEsponderUserRemote(ESponderUserDTO esponderUserDTO, Long userID) {
		ESponderUser esponderUser = (ESponderUser) mappingService.mapESponderEntityDTO(esponderUserDTO, ESponderUser.class);
		return (ESponderUserDTO) mappingService.mapESponderEntity(updateEsponderUser(esponderUser, userID), ESponderUserDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#updateActor(eu.esponder.model.crisis.resource.Actor, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderUser updateEsponderUser(ESponderUser esponderUser, Long userID) {
		ESponderUser pesponderUser = findESponderUserById(esponderUser.getId());
		mappingService.mapEntityToEntity(esponderUser, pesponderUser);
		return (ESponderUser) actorCrudService.update(pesponderUser);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#updateCrisisManagerRemote(eu.esponder.dto.model.crisis.resource.ActorCMDTO, java.lang.Long)
	 */
	@Override
	public ActorCMDTO updateCrisisManagerRemote(ActorCMDTO actorCMDTO, Long userID) {
		ActorCM actorCM = (ActorCM) mappingService.mapESponderEntityDTO(actorCMDTO, ActorCM.class);
		return (ActorCMDTO) mappingService.mapESponderEntity(updateCrisisManager(actorCM, userID), ActorCMDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#updateActor(eu.esponder.model.crisis.resource.Actor, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorCM updateCrisisManager(ActorCM actorCM, Long userID) {
		ActorCM pactorCM = findCrisisManagerById(actorCM.getId());
		mappingService.mapEntityToEntity(actorCM, pactorCM);
		return (ActorCM) actorCrudService.update(pactorCM);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#updateFRCRemote(eu.esponder.dto.model.crisis.resource.ActorFRCDTO, java.lang.Long)
	 */
	@Override
	public ActorFRCDTO updateFRCRemote(ActorFRCDTO frcDTO, Long userID) {
		ActorFRC actorFRC = (ActorFRC) mappingService.mapESponderEntityDTO(frcDTO, ActorFRC.class);
		return (ActorFRCDTO) mappingService.mapESponderEntity(updateFRC(actorFRC, userID), ActorFRDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#updateFRC(eu.esponder.model.crisis.resource.ActorFRC, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorFRC updateFRC(ActorFRC actorFRC, Long userID) {

		ActorFRC pactorFRC = findFRChiefById(actorFRC.getId());
		mappingService.mapEntityToEntity(actorFRC, pactorFRC);
		return (ActorFRC) actorCrudService.update(pactorFRC);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#updateFRRemote(eu.esponder.dto.model.crisis.resource.ActorFRDTO, java.lang.Long)
	 */
	@Override
	public ActorFRDTO updateFRRemote(ActorFRDTO frDTO, Long userID) {
		ActorFR actorFR = (ActorFR) mappingService.mapESponderEntityDTO(frDTO, ActorFR.class);
		return (ActorFRDTO) mappingService.mapESponderEntity(updateFR(actorFR, userID), ActorFRDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#updateFR(eu.esponder.model.crisis.resource.ActorFR, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorFR updateFR(ActorFR actorFR, Long userID) {

		ActorFR pactorFR = findFRById(actorFR.getId());
		mappingService.mapEntityToEntity(actorFR, pactorFR);
		return (ActorFR) actorCrudService.update(pactorFR);
	}

	//-------------------------------------------------------------------------		

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#createActorSnapshotRemote(eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO, java.lang.Long)
	 */
	@Override
	public ActorSnapshotDTO createActorSnapshotRemote(ActorSnapshotDTO snapshotDTO, Long userID) {
		ActorSnapshot actorSnapshot = (ActorSnapshot) mappingService.mapESponderEntityDTO(snapshotDTO, ActorSnapshot.class);
		return (ActorSnapshotDTO) mappingService.mapESponderEntity(createActorSnapshot(actorSnapshot, userID), ActorSnapshotDTO.class);
	}		

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#createActorSnapshot(eu.esponder.model.snapshot.resource.ActorSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorSnapshot createActorSnapshot(ActorSnapshot snapshot, Long userID) {
		snapshot = actorSnapshotCrudService.create(snapshot);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findActorSnapshotByIdRemote(java.lang.Long)
	 */
	@Override
	public ActorSnapshotDTO findActorSnapshotByIdRemote(Long actorID) {
		ActorSnapshot actorSnapshot = findActorSnapshotById(actorID);
		ActorSnapshotDTO actorSnapshotDTO = (ActorSnapshotDTO) mappingService.mapESponderEntity(actorSnapshot, ActorSnapshotDTO.class);
		return actorSnapshotDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findActorSnapshotById(java.lang.Long)
	 */
	@Override
	public ActorSnapshot findActorSnapshotById(Long actorID) {
		ActorSnapshot actorSnapshot = (ActorSnapshot) actorSnapshotCrudService.find(ActorSnapshot.class, actorID);
		return actorSnapshot;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#updateActorSnapshotRemote(eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO, java.lang.Long)
	 */
	@Override
	public ActorSnapshotDTO updateActorSnapshotRemote(ActorSnapshotDTO actorSnapshotDTO, Long userID) {
		ActorSnapshot actorSnapshot = (ActorSnapshot) mappingService.mapESponderEntityDTO(actorSnapshotDTO, ActorSnapshot.class);
		return (ActorSnapshotDTO) mappingService.mapESponderEntity(updateActorSnapshot(actorSnapshot, userID), ActorSnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#updateActorSnapshot(eu.esponder.model.snapshot.resource.ActorSnapshot, java.lang.Long)
	 */
	@Override
	public ActorSnapshot updateActorSnapshot(ActorSnapshot actorSnapshot, Long userID) {
		return (ActorSnapshot) actorSnapshotCrudService.update(actorSnapshot);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#deleteActorSnapshotRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActorSnapshotRemote(Long actorSnapshotDTOID, Long userID) {
		deleteActorSnapshot(actorSnapshotDTOID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#deleteActorSnapshot(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActorSnapshot(Long actorSnapshotID, Long userID) {
		actorSnapshotCrudService.delete(ActorSnapshot.class, userID);

	}

}
