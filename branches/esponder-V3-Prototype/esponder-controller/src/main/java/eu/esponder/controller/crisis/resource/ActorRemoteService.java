/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.ActorCMDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.model.crisis.resource.Actor;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface ActorRemoteService.
 */
@Remote
public interface ActorRemoteService {

//	/**
//	 * Find by title remote.
//	 *
//	 * @param title the title
//	 * @return the actor dto
//	 */
//	public ActorDTO findByTitleRemote(String title);
	
	/**
	 * Find subordinates by id remote.
	 *
	 * @param actorID the actor id
	 * @return the list
	 */
	public List<ActorDTO> findSubordinatesByIdRemote(Long actorID);
	
	/**
	 * Delete actor remote.
	 *
	 * @param actorID the actor id
	 * @param userID the user id
	 */
	public void deleteActorRemote(Long actorID, Long userID);
	
	/**
	 * Find actor snapshot by date remote.
	 *
	 * @param actorID the actor id
	 * @param maxDate the max date
	 * @return the actor snapshot dto
	 */
	public ActorSnapshotDTO findActorSnapshotByDateRemote(Long actorID, Date maxDate);
	
	/**
	 * Find actor snapshot by id remote.
	 *
	 * @param actorID the actor id
	 * @return the actor snapshot dto
	 */
	public ActorSnapshotDTO findActorSnapshotByIdRemote(Long actorID);

	/**
	 * Creates the actor snapshot remote.
	 *
	 * @param snapshotDTO the snapshot dto
	 * @param userID the user id
	 * @return the actor snapshot dto
	 */
	public ActorSnapshotDTO createActorSnapshotRemote(ActorSnapshotDTO snapshotDTO, Long userID);
	
	/**
	 * Update actor snapshot remote.
	 *
	 * @param actorSnapshotDTO the actor snapshot dto
	 * @param userID the user id
	 * @return the actor snapshot dto
	 */
	public ActorSnapshotDTO updateActorSnapshotRemote(ActorSnapshotDTO actorSnapshotDTO, Long userID);
	
	/**
	 * Delete actor snapshot remote.
	 *
	 * @param actorSnapshotDTOID the actor snapshot dtoid
	 * @param userID the user id
	 */
	public void deleteActorSnapshotRemote(Long actorSnapshotDTOID, Long userID);

	/**
	 * Find all actors remote.
	 *
	 * @return the list
	 */
	public List<ActorDTO> findAllActorsRemote();

	/**
	 * Find supervisor for actor remote.
	 *
	 * @param actorID the actor id
	 * @return the actor dto
	 */
	public ActorDTO findSupervisorForActorRemote(Long actorID);

	/**
	 * Find fr team by id remote.
	 *
	 * @param frTeamID the fr team id
	 * @return the fR team dto
	 */
	public FRTeamDTO findFRTeamByIdRemote(Long frTeamID);

	/**
	 * Find fr team chief remote.
	 *
	 * @param frTeamID the fr team id
	 * @return the actor dto
	 */
	public ActorDTO findFRTeamChiefRemote(Long frTeamID);

	/**
	 * Find fr teams for meoc remote.
	 *
	 * @param meocID the meoc id
	 * @return the list
	 */
	public List<FRTeamDTO> findFRTeamsForMeocRemote(Long meocID);

	/**
	 * Find fr team for chief remote.
	 *
	 * @param frchiefID the frchief id
	 * @return the fR team dto
	 */
	public FRTeamDTO findFRTeamForChiefRemote(Long frchiefID);

	/**
	 * Find frc by title remote.
	 *
	 * @param title the title
	 * @return the actor frcdto
	 */
	public ActorFRCDTO findFRCByTitleRemote(String title);

	/**
	 * Find fr by title remote.
	 *
	 * @param title the title
	 * @return the actor frdto
	 */
	public ActorFRDTO findFRByTitleRemote(String title);

	/**
	 * Find cm by title remote.
	 *
	 * @param title the title
	 * @return the actor cmdto
	 */
	public ActorCMDTO findCMByTitleRemote(String title);

	/**
	 * Find ic by title remote.
	 *
	 * @param title the title
	 * @return the actor icdto
	 */
	public ActorICDTO findICByTitleRemote(String title);

	/**
	 * Find actor by title remote.
	 *
	 * @param title the title
	 * @return the object
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	public Object findActorByTitleRemote(String title) throws InstantiationException, IllegalAccessException;

	/**
	 * Creates the incident commander remote.
	 *
	 * @param incidentCommanderDTO the incident commander dto
	 * @param userID the user id
	 * @return the actor icdto
	 */
	public ActorICDTO createIncidentCommanderRemote(ActorICDTO incidentCommanderDTO,
			long userID);

	/**
	 * Creates the crisis manager remote.
	 *
	 * @param crisisManagerDTO the crisis manager dto
	 * @param userID the user id
	 * @return the actor cmdto
	 */
	public ActorCMDTO createCrisisManagerRemote(ActorCMDTO crisisManagerDTO,
			long userID);

	/**
	 * Creates the fr chief remote.
	 *
	 * @param frChiefDTO the fr chief dto
	 * @param userID the user id
	 * @return the actor frcdto
	 */
	public ActorFRCDTO createFRChiefRemote(ActorFRCDTO frChiefDTO, long userID);

	/**
	 * Creates the fr remote.
	 *
	 * @param frDTO the fr dto
	 * @param userID the user id
	 * @return the actor frdto
	 */
	public ActorFRDTO createFRRemote(ActorFRDTO frDTO, long userID);

	/**
	 * Creates the e sponder useremote.
	 *
	 * @param userDTO the user dto
	 * @param userID the user id
	 * @return the e sponder user dto
	 */
	public ESponderUserDTO createESponderUseremote(ESponderUserDTO userDTO, long userID);

	/**
	 * Update incident commander remote.
	 *
	 * @param actorICDTO the actor icdto
	 * @param userID the user id
	 * @return the actor icdto
	 */
	public ActorICDTO updateIncidentCommanderRemote(ActorICDTO actorICDTO, Long userID);

	/**
	 * Update esponder user remote.
	 *
	 * @param esponderUserDTO the esponder user dto
	 * @param userID the user id
	 * @return the e sponder user dto
	 */
	public ESponderUserDTO updateEsponderUserRemote(ESponderUserDTO esponderUserDTO,
			Long userID);

	/**
	 * Update crisis manager remote.
	 *
	 * @param actorCMDTO the actor cmdto
	 * @param userID the user id
	 * @return the actor cmdto
	 */
	public ActorCMDTO updateCrisisManagerRemote(ActorCMDTO actorCMDTO, Long userID);

	/**
	 * Update frc remote.
	 *
	 * @param frcDTO the frc dto
	 * @param userID the user id
	 * @return the actor frcdto
	 */
	public ActorFRCDTO updateFRCRemote(ActorFRCDTO frcDTO, Long userID);

	/**
	 * Update fr remote.
	 *
	 * @param frDTO the fr dto
	 * @param userID the user id
	 * @return the actor frdto
	 */
	public ActorFRDTO updateFRRemote(ActorFRDTO frDTO, Long userID);

	/**
	 * Find fr chief by id remote.
	 *
	 * @param chiefID the chief id
	 * @return the actor frcdto
	 */
	public ActorFRCDTO findFRChiefByIdRemote(Long chiefID);

	/**
	 * Find fr by id remote.
	 *
	 * @param frID the fr id
	 * @return the actor frdto
	 */
	public ActorFRDTO findFRByIdRemote(Long frID);

	/**
	 * Find incident commander by id remote.
	 *
	 * @param actorID the actor id
	 * @return the actor icdto
	 */
	public ActorICDTO findIncidentCommanderByIdRemote(Long actorID);

	/**
	 * Find crisis manager by id remote.
	 *
	 * @param actorID the actor id
	 * @return the actor cmdto
	 */
	public ActorCMDTO findCrisisManagerByIdRemote(Long actorID);

	/**
	 * Find e sponder user by id remote.
	 *
	 * @param actorID the actor id
	 * @return the e sponder user dto
	 */
	public ESponderUserDTO findESponderUserByIdRemote(Long actorID);

	/**
	 * Find by id remote.
	 *
	 * @param actorID the actor id
	 * @return the actor dto
	 */
	public ActorDTO findByIdRemote(Long actorID);

	/**
	 * Find e sponder user by title remote.
	 *
	 * @param title the title
	 * @return the e sponder user dto
	 */
	public ESponderUserDTO findESponderUserByTitleRemote(String title);

	/**
	 * Find actor class by id remote.
	 *
	 * @param actorID the actor id
	 * @return the string
	 */
	public String findActorClassByIdRemote(Long actorID);
	
}
