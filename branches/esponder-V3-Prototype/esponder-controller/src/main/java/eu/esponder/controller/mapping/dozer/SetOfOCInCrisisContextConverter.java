/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.OperationsCentreService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.resource.OCEoc;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class SetOfOCInCrisisContextConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected OperationsCentreService getOCService() {
		try {
			return ServiceLocator.getResource("esponder/OperationsCentreBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				Object next = it.next();
				if(next.getClass() == OCEoc.class) {

					Set<OCEoc> sourceOCEocSet = (Set<OCEoc>) source;
					Set<Long> destOCEocDTOSet = new HashSet<Long>();
					for(OCEoc eoc : sourceOCEocSet) {
						destOCEocDTOSet.add(eoc.getId());
					}
					destination = destOCEocDTOSet;
				}
				else
					if(next.getClass() == Long.class) {

						Set<Long> sourceOCEocDTOSet = (Set<Long>) source;
						Set<OCEoc> destOCEocSet = new HashSet<OCEoc>();
						for(Long ocID : sourceOCEocDTOSet) {
							OCEoc destOC = (OCEoc) this.getOCService().findOperationCentreById(ocID);
							destOCEocSet.add(destOC);
						}
						destination = destOCEocSet;
					}
					else
						destination = null;
			}
		}
		else {
			new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
