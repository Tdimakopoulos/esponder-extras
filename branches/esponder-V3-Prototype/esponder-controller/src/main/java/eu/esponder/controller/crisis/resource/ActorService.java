/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.ActorCM;
import eu.esponder.model.crisis.resource.ActorFR;
import eu.esponder.model.crisis.resource.ActorFRC;
import eu.esponder.model.crisis.resource.ActorIC;
import eu.esponder.model.crisis.resource.FRTeam;
import eu.esponder.model.snapshot.resource.ActorSnapshot;
import eu.esponder.model.user.ESponderUser;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface ActorService.
 */
@Local
public interface ActorService extends ActorRemoteService {
	
	
//	/**
//	 * Find by title.
//	 *
//	 * @param title the title
//	 * @return the actor
//	 */
//	public Actor findByTitle(String title);
	
	/**
	 * Find subordinates by id.
	 *
	 * @param actorID the actor id
	 * @return the list
	 */
	public List<Actor> findSubordinatesById(Long actorID);
	
	/**
	 * Delete actor.
	 *
	 * @param actorID the actor id
	 * @param userID the user id
	 */
	public void deleteActor(Long actorID, Long userID);
	
	/**
	 * Find actor snapshot by date.
	 *
	 * @param actorID the actor id
	 * @param dateTo the date to
	 * @return the actor snapshot
	 */
	public ActorSnapshot findActorSnapshotByDate(Long actorID, Date dateTo);
	
	/**
	 * Find actor snapshot by id.
	 *
	 * @param actorID the actor id
	 * @return the actor snapshot
	 */
	public ActorSnapshot findActorSnapshotById(Long actorID);
	
	/**
	 * Creates the actor snapshot.
	 *
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the actor snapshot
	 */
	public ActorSnapshot createActorSnapshot(ActorSnapshot snapshot, Long userID);
	
	/**
	 * Update actor snapshot.
	 *
	 * @param actorSnapshot the actor snapshot
	 * @param userID the user id
	 * @return the actor snapshot
	 */
	public ActorSnapshot updateActorSnapshot(ActorSnapshot actorSnapshot, Long userID);
	
	/**
	 * Delete actor snapshot.
	 *
	 * @param actorSnapshotID the actor snapshot id
	 * @param userID the user id
	 */
	public void deleteActorSnapshot(Long actorSnapshotID, Long userID);

	/**
	 * Find all actors.
	 *
	 * @return the list
	 */
	public List<Actor> findAllActors();

	/**
	 * Find supervisor for actor.
	 *
	 * @param actorID the actor id
	 * @return the actor
	 */
	public Actor findSupervisorForActor(Long actorID);

	/**
	 * Find fr team by id.
	 *
	 * @param frTeamID the fr team id
	 * @return the fR team
	 */
	public FRTeam findFRTeamById(Long frTeamID);

	/**
	 * Find fr team chief by id.
	 *
	 * @param frTeamID the fr team id
	 * @return the actor
	 */
	public Actor findFRTeamChiefById(Long frTeamID);

	/**
	 * Find fr teams for meoc.
	 *
	 * @param meocID the meoc id
	 * @return the list
	 */
	public List<FRTeam> findFRTeamsForMeoc(Long meocID);

	/**
	 * Find fr team for chief.
	 *
	 * @param frchiefID the frchief id
	 * @return the fR team
	 */
	public FRTeam findFRTeamForChief(Long frchiefID);

	/**
	 * Find frc by title.
	 *
	 * @param title the title
	 * @return the actor frc
	 */
	public ActorFRC findFRCByTitle(String title);

	/**
	 * Find fr by title.
	 *
	 * @param title the title
	 * @return the actor fr
	 */
	public ActorFR findFRByTitle(String title);

	/**
	 * Find cm by title.
	 *
	 * @param title the title
	 * @return the actor cm
	 */
	public ActorCM findCMByTitle(String title);

	/**
	 * Find ic by title.
	 *
	 * @param title the title
	 * @return the actor ic
	 */
	public ActorIC findICByTitle(String title);

	/**
	 * Find actor by title.
	 *
	 * @param title the title
	 * @return the object
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	public Object findActorByTitle(String title) throws InstantiationException, IllegalAccessException;

	/**
	 * Creates the incident commander.
	 *
	 * @param incidentCommander the incident commander
	 * @param userID the user id
	 * @return the actor ic
	 */
	public ActorIC createIncidentCommander(ActorIC incidentCommander, long userID);

	/**
	 * Creates the crisis manager.
	 *
	 * @param crisisManager the crisis manager
	 * @param userID the user id
	 * @return the actor cm
	 */
	public ActorCM createCrisisManager(ActorCM crisisManager, long userID);

	/**
	 * Creates the fr chief.
	 *
	 * @param frChief the fr chief
	 * @param userID the user id
	 * @return the actor frc
	 */
	public ActorFRC createFRChief(ActorFRC frChief, long userID);

	/**
	 * Creates the fr.
	 *
	 * @param fr the fr
	 * @param userID the user id
	 * @return the actor fr
	 */
	public ActorFR createFR(ActorFR fr, long userID);

	/**
	 * Creates the e sponder user.
	 *
	 * @param user the user
	 * @param userID the user id
	 * @return the e sponder user
	 */
	public ESponderUser createESponderUser(ESponderUser user, long userID);

	/**
	 * Update incident commander.
	 *
	 * @param actorIC the actor ic
	 * @param userID the user id
	 * @return the actor ic
	 */
	public ActorIC updateIncidentCommander(ActorIC actorIC, Long userID);

	/**
	 * Update esponder user.
	 *
	 * @param esponderUser the esponder user
	 * @param userID the user id
	 * @return the e sponder user
	 */
	public ESponderUser updateEsponderUser(ESponderUser esponderUser, Long userID);

	/**
	 * Update crisis manager.
	 *
	 * @param actorCM the actor cm
	 * @param userID the user id
	 * @return the actor cm
	 */
	public ActorCM updateCrisisManager(ActorCM actorCM, Long userID);

	/**
	 * Update frc.
	 *
	 * @param actorFRC the actor frc
	 * @param userID the user id
	 * @return the actor frc
	 */
	public ActorFRC updateFRC(ActorFRC actorFRC, Long userID);

	/**
	 * Update fr.
	 *
	 * @param actorFR the actor fr
	 * @param userID the user id
	 * @return the actor fr
	 */
	public ActorFR updateFR(ActorFR actorFR, Long userID);

	/**
	 * Find fr chief by id.
	 *
	 * @param chiefID the chief id
	 * @return the actor frc
	 */
	public ActorFRC findFRChiefById(Long chiefID);

	/**
	 * Find fr by id.
	 *
	 * @param frID the fr id
	 * @return the actor fr
	 */
	public ActorFR findFRById(Long frID);

	/**
	 * Find incident commander by id.
	 *
	 * @param actorID the actor id
	 * @return the actor ic
	 */
	public ActorIC findIncidentCommanderById(Long actorID);

	/**
	 * Find crisis manager by id.
	 *
	 * @param actorID the actor id
	 * @return the actor cm
	 */
	public ActorCM findCrisisManagerById(Long actorID);

	/**
	 * Find e sponder user by id.
	 *
	 * @param actorID the actor id
	 * @return the e sponder user
	 */
	public ESponderUser findESponderUserById(Long actorID);

	/**
	 * Find by id.
	 *
	 * @param actorID the actor id
	 * @return the actor
	 */
	public Actor findById(Long actorID);

	/**
	 * Find actor as object by id.
	 *
	 * @param actorID the actor id
	 * @return the object
	 */
	public Object findActorAsObjectById(Long actorID);

	/**
	 * Find e sponder user by title.
	 *
	 * @param title the title
	 * @return the e sponder user
	 */
	public ESponderUser findESponderUserByTitle(String title);
	
}
