/*
 * 
 */
package eu.esponder.controller.configuration;

import javax.ejb.Local;

import eu.esponder.model.config.ESponderConfigParameter;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface ESponderConfigurationService.
 */
@Local
public interface ESponderConfigurationService extends ESponderConfigurationRemoteService {
	
	/**
	 * Find e sponder config by name.
	 *
	 * @param configName the config name
	 * @return the e sponder config parameter
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderConfigParameter findESponderConfigByName(String configName) throws ClassNotFoundException;

	/**
	 * Find e sponder config by id.
	 *
	 * @param configId the config id
	 * @return the e sponder config parameter
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderConfigParameter findESponderConfigById(Long configId) throws ClassNotFoundException;

	/**
	 * Creates the e sponder config.
	 *
	 * @param config the config
	 * @param userID the user id
	 * @return the e sponder config parameter
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderConfigParameter createESponderConfig(ESponderConfigParameter config, Long userID) throws ClassNotFoundException;
	
	/**
	 * Update e sponder config.
	 *
	 * @param config the config
	 * @param userID the user id
	 * @return the e sponder config parameter
	 */
	public ESponderConfigParameter updateESponderConfig(ESponderConfigParameter config, Long userID);
	
	/**
	 * Delete e sponder config.
	 *
	 * @param ESponderConfigID the e sponder config id
	 * @param userID the user id
	 */
	public void deleteESponderConfig(Long ESponderConfigID, Long userID);

}
