/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.TypeService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.type.ActionType;
import eu.esponder.util.ejb.ServiceLocator;


// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionTypeFieldConverter.
 */
public class ActionTypeFieldConverter implements CustomConverter {

	/**
	 * Gets the type service.
	 *
	 * @return the type service
	 */
	protected TypeService getTypeService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert(Object destination, 
			Object source, 
			Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == String.class && source != null) {
			String type = (String) source;
			ActionType actionType = (ActionType) getTypeService().findByTitle(type);
			destination = actionType;
		}
		else if(ActionType.class.isAssignableFrom(sourceClass)) {
			ActionType ActionType = (ActionType) source;
			destination = (String) ActionType.getTitle();
		}
		else {
			new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}