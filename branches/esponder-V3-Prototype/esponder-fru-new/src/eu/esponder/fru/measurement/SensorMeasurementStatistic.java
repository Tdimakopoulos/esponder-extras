package eu.esponder.fru.measurement;

public class SensorMeasurementStatistic {
	
	private SensorMeasurement measurement;
	
	private MeasurementProcessingType measurementType;

	public SensorMeasurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(SensorMeasurement measurement) {
		this.measurement = measurement;
	}

	public MeasurementProcessingType getMeasurementType() {
		return measurementType;
	}

	public void setMeasurementType(MeasurementProcessingType measurementType) {
		this.measurementType = measurementType;
	}
	
}
