/*
 * 
 */
package eu.esponder.model.crisis.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;


// TODO: Auto-generated Javadoc
/**
 * The Class URL.
 */
@MappedSuperclass
public abstract class URL implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5214691610570185146L;
	
	/** The Constant separator. */
	private static final String separator = "/";
	
	/** The Constant protocolSeparator. */
	private static final String protocolSeparator = "://";
	
	/** The protocol. */
	@Column(name="PROTOCOL")
	private String protocol;
	
	/** The host name. */
	@Column(name="HOST")
	private String hostName;
	
	/** The path. */
	@Column(name="PATH")
	private String path;
	
	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getURL() {
		return protocol + protocolSeparator + hostName + separator + path; 
	}
	
	/**
	 * Gets the protocol.
	 *
	 * @return the protocol
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * Sets the protocol.
	 *
	 * @param protocol the new protocol
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * Gets the host.
	 *
	 * @return the host
	 */
	public String getHost() {
		return hostName;
	}

	/**
	 * Sets the host.
	 *
	 * @param host the new host
	 */
	public void setHost(String host) {
		this.hostName = host;
	}

	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Sets the path.
	 *
	 * @param path the new path
	 */
	public void setPath(String path) {
		this.path = path;
	}

}
