/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class RankType.
 */
@Entity
@DiscriminatorValue("RANK")
public class RankType extends ESponderType {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7247305696249606800L;
	
}
