/*
 * 
 */
package eu.esponder.model.snapshot.location;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;


// TODO: Auto-generated Javadoc
/**
 * The Class LocationArea.
 */
@Entity
@Table(name="location_area")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="DISCRIMINATOR")
public class LocationArea extends ESponderEntity<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5734852483011769458L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="LOCATION_AREA_ID")
	protected Long id;
	
	/** The title. Not Null,Unique*/
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
}
