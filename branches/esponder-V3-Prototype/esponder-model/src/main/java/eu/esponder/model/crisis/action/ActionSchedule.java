/*
 * 
 */
package eu.esponder.model.crisis.action;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;


// TODO: Auto-generated Javadoc
/**
 * The Class ActionSchedule.
 * Entity class that manage the schedule of action
 */
@Entity
@Table(name="action_schedule")
public class ActionSchedule extends ESponderEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6440269024338454840L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_SCHEDULE_ID")
	private Long id;
	
	/** The title. Not Null,Unique*/
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	/** The action. Not Null*/
	@ManyToOne
	@JoinColumn(name="ACTION_ID", nullable=false)
	private Action action;
	
	/** The prerequisite action. Not Null*/
	@ManyToOne
	@JoinColumn(name="PREREQUISITE_ACTION_ID", nullable=false)
	private Action prerequisiteAction;
	
	/** The criteria. The criteria*/
	@Embedded
	private ActionScheduleCriteria criteria;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public Action getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(Action action) {
		this.action = action;
	}

	/**
	 * Gets the prerequisite action.
	 *
	 * @return the prerequisite action
	 */
	public Action getPrerequisiteAction() {
		return prerequisiteAction;
	}

	/**
	 * Sets the prerequisite action.
	 *
	 * @param prerequisiteAction the new prerequisite action
	 */
	public void setPrerequisiteAction(Action prerequisiteAction) {
		this.prerequisiteAction = prerequisiteAction;
	}

	/**
	 * Gets the criteria.
	 *
	 * @return the criteria
	 */
	public ActionScheduleCriteria getCriteria() {
		return criteria;
	}

	/**
	 * Sets the criteria.
	 *
	 * @param criteria the new criteria
	 */
	public void setCriteria(ActionScheduleCriteria criteria) {
		this.criteria = criteria;
	}
	
}


