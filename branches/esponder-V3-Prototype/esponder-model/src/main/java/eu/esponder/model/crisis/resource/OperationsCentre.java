/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.plan.PlannableResource;
import eu.esponder.model.crisis.view.VoIPURL;
import eu.esponder.model.user.ESponderUser;


// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentre.
 * Entity class that manage the Operations Centers
 */
@Entity
@Table(name="operations_centre")
@NamedQueries({
	@NamedQuery(name="OperationsCentre.findByTitle", query="select c from OperationsCentre c where c.title=:title"),
	@NamedQuery(name="OperationsCentre.findAll", query="select c from OperationsCentre c")
	
})
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "operationsCentreType")
public class OperationsCentre extends PlannableResource {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1911200656062784549L;

	/** The actors. Actors can be platform users and operationals actors */
	@OneToMany(mappedBy="operationsCentre")
	private Set<ESponderUser> users;


	/** The voipurl. */
	@Embedded
	private VoIPURL voIPURL;

	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURL getVoIPURL() {
		return voIPURL;
	}

	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURL voIPURL) {
		this.voIPURL = voIPURL;
	}
	
	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public Set<ESponderUser> getUsers() {
		return users;
	}

	/**
	 * Sets the users.
	 *
	 * @param users the new users
	 */
	public void setUsers(Set<ESponderUser> users) {
		this.users = users;
	}

}
