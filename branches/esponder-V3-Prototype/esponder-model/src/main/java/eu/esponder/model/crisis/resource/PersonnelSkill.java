/*
 * 
 */
package eu.esponder.model.crisis.resource;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import eu.esponder.model.type.PersonnelSkillType;


// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelSkill.
 * Entity class that manage the skills for each personnel
 */
@Entity
@DiscriminatorValue("SKILL")
public class PersonnelSkill extends PersonnelCompetence {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3556997131154345038L;
	
	/** The personnel skill type. */
	@OneToOne
	@JoinColumn(name="SKILL_TYPE")
	private PersonnelSkillType personnelSkillType;

	/**
	 * Gets the personnel skill type.
	 *
	 * @return the personnel skill type
	 */
	public PersonnelSkillType getPersonnelSkillType() {
		return personnelSkillType;
	}

	/**
	 * Sets the personnel skill type.
	 *
	 * @param personnelSkillType the new personnel skill type
	 */
	public void setPersonnelSkillType(PersonnelSkillType personnelSkillType) {
		this.personnelSkillType = personnelSkillType;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.model.crisis.resource.PersonnelCompetence#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    final PersonnelSkill other = (PersonnelSkill) obj;
	    if (this.getId()!= other.getId()) {
	        return false;
	    }
//	    if (!this.getPesonnelCategory().equals(other.getPesonnelCategory())) {
//	    	return false;
//	    }
	    if (!this.getShortTitle().equals(other.getShortTitle())) {
	    	return false;
	    }
	    if (!this.getDescription().equals(other.getDescription())) {
	    	return false;
	    }
	    if (!this.getRecordStatus().equals(other.getRecordStatus())) {
	    	return false;
	    }
	    if (!this.getPersonnelSkillType().equals(other.getPersonnelSkillType())) {
	    	return false;
	    }
	    return true;
	}

}
