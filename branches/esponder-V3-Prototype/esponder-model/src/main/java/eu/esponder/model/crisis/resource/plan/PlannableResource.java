/*
 * 
 */
package eu.esponder.model.crisis.resource.plan;

import javax.persistence.MappedSuperclass;

import eu.esponder.model.crisis.resource.Resource;


// TODO: Auto-generated Javadoc
/**
 * The Class PlannableResource.
 */

@MappedSuperclass
public abstract class PlannableResource extends Resource {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6210270086779113742L;

}
