/*
 * 
 */
package eu.esponder.model.crisis.resource;


import javax.persistence.DiscriminatorColumn;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.view.VoIPURL;



// TODO: Auto-generated Javadoc
/**
 * The Class Actor.
 * This is a entity class which manage information about a actor, a platform user is also a actor
 */
@Entity
@Table(name="actor")
@NamedQueries({
	@NamedQuery(name="Actor.findByTitle", query="select a from Actor a where a.title=:title"),
	@NamedQuery(name="Actor.findAll", query="select a from Actor a")
})
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "actorType")
public class Actor extends Resource {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2224027092652397963L;

	/** The personnel. Used to map the personnel to actor*/
	@OneToOne
	@JoinColumn(name="PERSON_ID")
	private Personnel personnel;
	
	/** The voip url. */
	@Embedded
	private VoIPURL voIPURL;
	

	

	/* (non-Javadoc)
	 * @see eu.esponder.model.crisis.resource.Resource#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.crisis.resource.Resource#setId(java.lang.Long)
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * Sets the equipment set.
	 *
	 * @return the personnel
	 */
	
	/**
	 * Gets the personnel.
	 *
	 * @return the personnel
	 */
	public Personnel getPersonnel() {
		return personnel;
	}

	/**
	 * Sets the personnel.
	 *
	 * @param personnel the new personnel
	 */
	public void setPersonnel(Personnel personnel) {
		this.personnel = personnel;
	}


	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURL getVoIPURL() {
		return voIPURL;
	}

	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURL voIPURL) {
		this.voIPURL = voIPURL;
	}

}
