/*
 * 
 */
package eu.esponder.model.snapshot.resource;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.FRTeam;
import eu.esponder.model.snapshot.Snapshot;
import eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class FRTeamSnapshot. This is the entity class that define the snapshots
 * for Team.
 */
@Entity
@Table(name = "frteamsnapshot")
public class FRTeamSnapshot extends Snapshot<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1911859556062995500L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name = "FRTEAM_SNAPSHOT_ID")
	protected Long id;

	/** The FRTEAM ID . Not Null */
	@ManyToOne
	@JoinColumn(name = "FRTEAM_ID", nullable = false)
	private FRTeam frteam;

	/** The previous. */
	@OneToOne
	@JoinColumn(name = "PREVIOUS_SNAPSHOT_ID")
	private FRTeamSnapshot previous;

	/** The next. */
	@OneToOne(mappedBy = "previous")
	private FRTeamSnapshot next;

	/** The statistic type. */
	@Column(name = "STATISTIC_TYPE")
	private MeasurementStatisticTypeEnum statisticType;

	/** The value. */
	@Column(name = "VALUE")
	private BigDecimal value;

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	@Override
	public Long getId() {

		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	@Override
	public void setId(Long id) {
		this.id = id;

	}

	/**
	 * Gets the frteam.
	 * 
	 * @return the frteam
	 */
	public FRTeam getFrteam() {
		return frteam;
	}

	/**
	 * Sets the frteam.
	 * 
	 * @param frteam
	 *            the new frteam
	 */
	public void setFrteam(FRTeam frteam) {
		this.frteam = frteam;
	}

	/**
	 * Gets the previous.
	 * 
	 * @return the previous
	 */
	public FRTeamSnapshot getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 * 
	 * @param previous
	 *            the new previous
	 */
	public void setPrevious(FRTeamSnapshot previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 * 
	 * @return the next
	 */
	public FRTeamSnapshot getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 * 
	 * @param next
	 *            the new next
	 */
	public void setNext(FRTeamSnapshot next) {
		this.next = next;
	}

	/**
	 * Gets the statistic type.
	 * 
	 * @return the statistic type
	 */
	public MeasurementStatisticTypeEnum getStatisticType() {
		return statisticType;
	}

	/**
	 * Sets the statistic type.
	 * 
	 * @param statisticType
	 *            the new statistic type
	 */
	public void setStatisticType(MeasurementStatisticTypeEnum statisticType) {
		this.statisticType = statisticType;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
