/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;


// TODO: Auto-generated Javadoc
/**
 * The Class OCMeoc.
 * Entity class that manage the Operations Centers MEOC
 */
@Entity
@DiscriminatorValue("MEOC")
@NamedQueries({
		@NamedQuery(name = "OCMeoc.findByTitle", query = "select c from OCMeoc c where c.title=:title"),
		@NamedQuery(name = "OCMeoc.findAll", query = "select c from OCMeoc c") })
public class OCMeoc extends OperationsCentre {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1867112082251273511L;

	/** The supervising oc. */
	@ManyToOne
	@JoinColumn(name = "EOC")
	private OCEoc supervisingOC;

	/** The incident commander. */
	@OneToOne
	@JoinColumn(name = "INC_COMMANDER")
	private ActorIC incidentCommander;

	/** The meoc crisis context. */
	@ManyToOne
	@JoinColumn(name = "CRISIS_CONTEXT_FOR_MEOC")
	private CrisisContext meocCrisisContext;

	/** The snapshots. */
	@OneToMany(mappedBy = "operationsCentre")
	private Set<OperationsCentreSnapshot> snapshots;

	/**
	 * Gets the meoc crisis context.
	 *
	 * @return the meoc crisis context
	 */
	public CrisisContext getMeocCrisisContext() {
		return meocCrisisContext;
	}

	/**
	 * Sets the meoc crisis context.
	 *
	 * @param meocCrisisContext the new meoc crisis context
	 */
	public void setMeocCrisisContext(CrisisContext meocCrisisContext) {
		this.meocCrisisContext = meocCrisisContext;
	}

	/**
	 * Gets the snapshots.
	 * 
	 * @return the snapshots
	 */
	public Set<OperationsCentreSnapshot> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 * 
	 * @param snapshots
	 *            the new snapshots
	 */
	public void setSnapshots(Set<OperationsCentreSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	/**
	 * Gets the supervising oc.
	 *
	 * @return the supervising oc
	 */
	public OCEoc getSupervisingOC() {
		return supervisingOC;
	}

	/**
	 * Sets the supervising oc.
	 *
	 * @param supervisingOC the new supervising oc
	 */
	public void setSupervisingOC(OCEoc supervisingOC) {
		this.supervisingOC = supervisingOC;
	}

	/**
	 * Gets the incident commander.
	 *
	 * @return the incident commander
	 */
	public ActorIC getIncidentCommander() {
		return incidentCommander;
	}

	/**
	 * Sets the incident commander.
	 *
	 * @param incidentCommander the new incident commander
	 */
	public void setIncidentCommander(ActorIC incidentCommander) {
		this.incidentCommander = incidentCommander;
	}

}
