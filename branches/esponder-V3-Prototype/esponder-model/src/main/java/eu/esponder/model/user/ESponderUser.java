/*
 * 
 */
package eu.esponder.model.user;



import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.OperationsCentre;


// TODO: Auto-generated Javadoc
/**
 * The Class ESponderUser.
 * Entity that manage the esponderuser, esponder user is system users, for portal, optimizer etc.
 */
@Entity
@NamedQueries({
	@NamedQuery(name="ESponderUser.findAll", query="select u from ESponderUser u"),
	@NamedQuery(name="ESponderUser.findByTitle", query="select u from ESponderUser u where u.title=:title")
})
@DiscriminatorValue("EUSER")
public class ESponderUser extends Actor {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2512583407793541959L;

	/** The operations centre. */
	@ManyToOne
	@JoinColumn(name="OC_ID")
	private OperationsCentre operationsCentre;
	
	/** The actor that represent the user. */
	private Long actorID;
	
	/** The user in event admin that represent the user. */
	private Long eventadminID;
	
	/** The user in event admin that represent the user. */
	private Long pkiID;
	
	/**
	 * Gets the actor id.
	 *
	 * @return the actor id
	 */
	public Long getActorID() {
		return actorID;
	}

	/**
	 * Sets the actor id.
	 *
	 * @param actorID the new actor id
	 */
	public void setActorID(Long actorID) {
		this.actorID = actorID;
	}

	/**
	 * Gets the eventadmin id.
	 *
	 * @return the eventadmin id
	 */
	public Long getEventadminID() {
		return eventadminID;
	}

	/**
	 * Sets the eventadmin id.
	 *
	 * @param eventadminID the new eventadmin id
	 */
	public void setEventadminID(Long eventadminID) {
		this.eventadminID = eventadminID;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
//		return "ESponderUser [id=" + id + ", userName=" + actorID + "]";
		return id.toString();
	}

	
	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OperationsCentre getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OperationsCentre operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	/**
	 * Gets the pki id.
	 *
	 * @return the pki id
	 */
	public Long getPkiID() {
		return pkiID;
	}

	/**
	 * Sets the pki id.
	 *
	 * @param pkiID the new pki id
	 */
	public void setPkiID(Long pkiID) {
		this.pkiID = pkiID;
	}

}
