/*
 * 
 */
package eu.esponder.model.snapshot.action;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.snapshot.SpatialSnapshot;
import eu.esponder.model.snapshot.status.ActionSnapshotStatus;


// TODO: Auto-generated Javadoc
/**
 * The Class ActionSnapshot.
 */
@Entity
@Table(name="action_snapshot")
@NamedQueries({
	@NamedQuery(
		name="ActionSnapshot.findByActionAndDate",
		query="SELECT s FROM ActionSnapshot s WHERE s.action.id = :actionID AND s.period.dateTo <= :maxDate AND s.period.dateTo = " +
				"(SELECT max(s.period.dateTo) FROM ActionSnapshot s WHERE s.action.id = :actionID)"),
	@NamedQuery(
		name="ActionSnapshot.findPreviousSnapshot",
		query="Select s from ActionSnapshot s where s.id=(select max(snapshot.id) from ActionSnapshot snapshot where snapshot.action.id = :sensorID)")
})
public class ActionSnapshot extends SpatialSnapshot<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3858107002910361432L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_SNAPSHOT_ID")
	private Long id;
	
	/** The action. Not Null*/
	@ManyToOne
	@JoinColumn(name="ACTION_ID", nullable=false)
	private Action action;
	
	/** The status. Not Null*/
	@Enumerated(EnumType.STRING)
	@Column(name="ACTION_SNAPSHOT_STATUS", nullable=false)
	private ActionSnapshotStatus status;
	
	/** The previous. */
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private ActionSnapshot previous;
	
	/** The next. */
	@OneToOne(mappedBy="previous")
	private ActionSnapshot next;
	
	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public Action getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(Action action) {
		this.action = action;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ActionSnapshotStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ActionSnapshotStatus status) {
		this.status = status;
	}

	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public ActionSnapshot getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previous the new previous
	 */
	public void setPrevious(ActionSnapshot previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public ActionSnapshot getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(ActionSnapshot next) {
		this.next = next;
	}
	
}
