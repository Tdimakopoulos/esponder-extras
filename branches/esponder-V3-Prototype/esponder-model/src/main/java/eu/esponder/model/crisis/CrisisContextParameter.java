/*
 * 
 */
package eu.esponder.model.crisis;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class CrisisContextParameter.
 */
@Entity
@Table(name="crisis_context_params")
@NamedQueries({
	@NamedQuery(name="CrisisContextParameters.findByName", query="select ccp from CrisisContextParameter ccp where ccp.paramName=:paramName")
})
public class CrisisContextParameter extends ESponderEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8076445828145240467L;
	
	/**
	 * Instantiates a new crisis context parameter.
	 */
	public CrisisContextParameter() {
		super();
	}
	
	/**
	 * Instantiates a new crisis context parameter.
	 *
	 * @param crisisContext the crisis context
	 * @param paramName the param name
	 * @param paramValue the param value
	 */
	public CrisisContextParameter(CrisisContext crisisContext, String paramName, String paramValue) {
		super();
		this.paramName = paramName;
		this.paramValue = paramValue;
		this.crisisContext = crisisContext;
	}

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PARAM_ID")
	private Long id;
	
	/** The param name. */
	@Column(name="paramname", nullable=false)
	private String paramName;
	
	/** The param value. */
	@Column(name="paramvalue", nullable=false)
	private String paramValue;
	
	/** The crisis context. */
	@ManyToOne
	@JoinColumn(name="CrisisContext")
	private CrisisContext crisisContext;
	
	/**
	 * Gets the crisis context.
	 *
	 * @return the crisis context
	 */
	public CrisisContext getCrisisContext() {
		return crisisContext;
	}

	/**
	 * Sets the crisis context.
	 *
	 * @param crisisContext the new crisis context
	 */
	public void setCrisisContext(CrisisContext crisisContext) {
		this.crisisContext = crisisContext;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	

	/**
	 * Gets the param name.
	 *
	 * @return the param name
	 */
	public String getParamName() {
		return paramName;
	}

	/**
	 * Sets the param name.
	 *
	 * @param paramName the new param name
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	/**
	 * Gets the param value.
	 *
	 * @return the param value
	 */
	public String getParamValue() {
		return paramValue;
	}

	/**
	 * Sets the param value.
	 *
	 * @param paramValue the new param value
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	
	

}
