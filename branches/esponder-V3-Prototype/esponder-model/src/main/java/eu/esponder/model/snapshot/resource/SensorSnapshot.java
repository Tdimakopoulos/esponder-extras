/*
 * 
 */
package eu.esponder.model.snapshot.resource;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.model.snapshot.Snapshot;
import eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum;
import eu.esponder.model.snapshot.status.SensorSnapshotStatus;

// TODO: Auto-generated Javadoc
/**
 * The Class SensorSnapshot.
 */
@Entity
@Table(name = "sensor_snapshot")
@NamedQueries({
		@NamedQuery(name = "SensorSnapshot.findBySensorAndDate", query = "SELECT s FROM SensorSnapshot s WHERE s.sensor.id = :sensorID AND s.period.dateTo <= :maxDate AND s.period.dateTo = "
				+ "(SELECT max(s.period.dateTo) FROM SensorSnapshot s WHERE s.sensor.id = :sensorID)"),
		@NamedQuery(name = "SensorSnapshot.findBySensorAndPeriod", query = "SELECT s FROM SensorSnapshot s WHERE s.sensor.id = :sensorID AND s.period.dateTo <= :maxDate AND s.period.dateFrom >= "
				+ ":minDate"),
		@NamedQuery(name = "SensorSnapshot.findBySensorAndPeriodAndType", query = "SELECT s FROM SensorSnapshot s WHERE s.sensor.id = :sensorID AND s.period.dateTo <= :maxDate AND s.period.dateFrom >= "
				+ ":minDate" + " AND s.statisticType = :statisticTYPE"),
		@NamedQuery(name = "SensorSnapshot.findPreviousSnapshot", query = "Select s from SensorSnapshot s where s.id=(select max(snapshot.id) from SensorSnapshot snapshot where snapshot.sensor.id = :sensorID)"),
		@NamedQuery(name = "SensorSnapshot.findAll", query = "Select s from SensorSnapshot s"),
		@NamedQuery(name = "SensorSnapshot.findAllBySensor", query = "Select s from SensorSnapshot s where s.sensor.id = :sensorID"),
		@NamedQuery(name = "SensorSnapshot.findAllBySensorAndType", query = "Select s from SensorSnapshot s where s.sensor = :sensor"
				+ " AND s.statisticType = :statisticTYPE") })
public class SensorSnapshot extends Snapshot<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7546751894098413109L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SENSOR_SNAPSHOT_ID")
	private Long id;

	/** The status. Not Null */
	@Enumerated(EnumType.STRING)
	@Column(name = "SENSOR_SNAPSHOT_STATUS", nullable = false)
	private SensorSnapshotStatus status;

	/** The sensor. Not Null */
	@ManyToOne
	@JoinColumn(name = "SENSOR_ID", nullable = false)
	private Sensor sensor;

	/** The previous. */
	@OneToOne
	@JoinColumn(name = "PREVIOUS_SNAPSHOT_ID")
	private SensorSnapshot previous;

	/** The next. */
	@OneToOne(mappedBy = "previous")
	private SensorSnapshot next;

	/** The statistic type. */
	@Column(name = "STATISTIC_TYPE")
	private MeasurementStatisticTypeEnum statisticType;

	/** The value. */
	@Column(name = "VALUE")
	private BigDecimal value;

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public SensorSnapshotStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(SensorSnapshotStatus status) {
		this.status = status;
	}

	/**
	 * Gets the sensor.
	 * 
	 * @return the sensor
	 */
	public Sensor getSensor() {
		return sensor;
	}

	/**
	 * Sets the sensor.
	 * 
	 * @param sensor
	 *            the new sensor
	 */
	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

	/**
	 * Gets the previous.
	 * 
	 * @return the previous
	 */
	public SensorSnapshot getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 * 
	 * @param previous
	 *            the new previous
	 */
	public void setPrevious(SensorSnapshot previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 * 
	 * @return the next
	 */
	public SensorSnapshot getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 * 
	 * @param next
	 *            the new next
	 */
	public void setNext(SensorSnapshot next) {
		this.next = next;
	}

	/**
	 * Gets the statistic type.
	 * 
	 * @return the statistic type
	 */
	public MeasurementStatisticTypeEnum getStatisticType() {
		return statisticType;
	}

	/**
	 * Sets the statistic type.
	 * 
	 * @param statisticType
	 *            the new statistic type
	 */
	public void setStatisticType(MeasurementStatisticTypeEnum statisticType) {
		this.statisticType = statisticType;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
