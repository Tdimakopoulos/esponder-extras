/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.snapshot.resource.FRTeamSnapshot;


// TODO: Auto-generated Javadoc
/**
 * The Class FRTeam.
 * This is the entity class that define the Team. 
 */
@Entity
@Table(name="frteam")
@NamedQueries({
	@NamedQuery(name="FRTeam.findTeamsByChief", query="select t from FRTeam t where t.frchief= :frchief")
})
public class FRTeam extends ESponderEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1911200656062999563L;
	
	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="FRTEAM_ID")
	protected Long id;
	
	/** The title. Not Null, Unique, this store the name of the team*/
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	protected String title;

	/** The FR Chief. */
	@OneToOne
	@JoinColumn(name="FRCHIEF_ID")
	private ActorFRC frchief;
	
	/** The incident commander. */
	@ManyToOne
	@JoinColumn(name="incidentCommander")
	private ActorIC incidentCommander;
	

	/**
	 * Gets the incident commander.
	 *
	 * @return the incident commander
	 */
	public ActorIC getIncidentCommander() {
		return incidentCommander;
	}

	/**
	 * Sets the incident commander.
	 *
	 * @param incidentCommander the new incident commander
	 */
	public void setIncidentCommander(ActorIC incidentCommander) {
		this.incidentCommander = incidentCommander;
	}
	
	/** The snapshots. */
	@OneToMany(mappedBy="frteam")
	private Set<FRTeamSnapshot> snapshots;
	
	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<FRTeamSnapshot> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<FRTeamSnapshot> snapshots) {
		this.snapshots = snapshots;
	}


	
	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	@Override
	public Long getId() {

		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	@Override
	public void setId(Long id) {

		this.id=id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the frchief.
	 *
	 * @return the frchief
	 */
	public ActorFRC getFrchief() {
		return frchief;
	}

	/**
	 * Sets the frchief.
	 *
	 * @param frchief the new frchief
	 */
	public void setFrchief(ActorFRC frchief) {
		this.frchief = frchief;
	}

	
}
