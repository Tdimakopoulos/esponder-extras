package eu.esponder.ws.client.logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.ws.client.logic.objects.SensorSnapshotDetailsObject;
import eu.esponder.ws.client.query.QueryManager;

public class SensorSnapshotFullDetails {

	List<SensorSnapshotDetailsObject> pSensorsSnapshots = new ArrayList<SensorSnapshotDetailsObject>();

	public List<SensorSnapshotDetailsObject> getpSensorsSnapshots() {
		return pSensorsSnapshots;
	}

	public void setpSensorsSnapshots(
			List<SensorSnapshotDetailsObject> pSensorsSnapshots) {
		this.pSensorsSnapshots = pSensorsSnapshots;
	}

	public void LoadSensorsSnapshots(String userID) throws JsonParseException,
			JsonMappingException, IOException {
		QueryManager pMan = new QueryManager();
		ResultListDTO pSensorsList = pMan.getAllSensorSnapshots(userID);
		for (int i = 0; i < pSensorsList.getResultList().size(); i++) {
			SensorSnapshotDetailsObject pItem = new SensorSnapshotDetailsObject();
			SensorSnapshotDTO pSensorSnapshot = new SensorSnapshotDTO();
			pSensorSnapshot = (SensorSnapshotDTO) pSensorsList.getResultList()
					.get(i);
			pItem.setpSensorSnapshot(pSensorSnapshot);
			//pSensorSnapshot.getSensor()

			EquipmentDTO pEquipment = pMan.getEquipment(userID, pSensorSnapshot
					.getSensor().getEquipmentId());
			pItem.setpEquipment(pEquipment);
			//pEquipment.getActor()
			ActorDTO pActor = pMan.getActorID(userID, pEquipment.getActor()
					.getId().toString());
			pItem.setpActor(pActor);
			pSensorsSnapshots.add(pItem);
		}
	}

}
