package eu.esponder.fru.measurement;

public class TemperatureMeasurement extends SensorMeasurement {

	private double value;

	public TemperatureMeasurement(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	public void setTimestamp(double value) {
		this.value = value;
	}

}
