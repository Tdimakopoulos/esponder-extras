package eu.esponder;
import java.math.BigDecimal;


public class Controls {
	public static BigDecimal MAX_AVG_TEMP = new BigDecimal("70.00");
	public static BigDecimal MIN_AVG_TEMP = new BigDecimal("65.00");
	
	public static BigDecimal MAX_MAX_TEMP = new BigDecimal("85.00");
	public static BigDecimal MIN_MAX_TEMP = new BigDecimal("75.00");
	
	public static BigDecimal MAX_MIN_TEMP = new BigDecimal("55.00");
	public static BigDecimal MIN_MIN_TEMP = new BigDecimal("50.00");
}
