/*
 * 
 */
package eu.esponder.event.model.datafusion;

import eu.esponder.dto.model.datafusion.DatafusionResultsDTO;
import eu.esponder.event.model.CreateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class DatafusionResultsGeneratedEvent.
 */
public class DatafusionResultsGeneratedEvent extends DataFusionEvent<DatafusionResultsDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6318868022859370822L;
	
	/**
	 * Instantiates a new datafusion results generated event.
	 */
	public DatafusionResultsGeneratedEvent(){
		setJournalMessageInfo("DatafusionResultsGeneratedEvent");
	}
}
