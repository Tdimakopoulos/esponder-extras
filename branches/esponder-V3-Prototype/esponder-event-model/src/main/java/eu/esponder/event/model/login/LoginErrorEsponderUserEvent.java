/*
 * 
 */
package eu.esponder.event.model.login;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.crisis.resource.ActorEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginErrorEsponderUserEvent.
 */
public class LoginErrorEsponderUserEvent extends ActorEvent<ESponderUserDTO> implements LoginError {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5408128404989456038L;
	
	/**
	 * Instantiates a new login error esponder user event.
	 */
	public LoginErrorEsponderUserEvent(){
		setJournalMessageInfo("LoginErrorEsponderUserEvent");
	}

}
