/*
 * 
 */
package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.event.model.CreateEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateSensorSnapshotEvent.
 */
public class CreateSensorSnapshotEvent extends SnapshotEvent<SensorSnapshotDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4737435535926991762L;

	/**
	 * Instantiates a new creates the sensor snapshot event.
	 */
	public CreateSensorSnapshotEvent(){
		setJournalMessageInfo("CreateSensorSnapshotEvent");
	}
}
