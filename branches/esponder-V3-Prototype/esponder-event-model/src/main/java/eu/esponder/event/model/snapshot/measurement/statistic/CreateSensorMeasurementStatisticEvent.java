/*
 * 
 */
package eu.esponder.event.model.snapshot.measurement.statistic;

import java.util.List;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.CreateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class CreateSensorMeasurementStatisticEvent.
 */
public class CreateSensorMeasurementStatisticEvent extends SensorMeasurementStatisticEvent<SensorMeasurementStatisticEnvelopeDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8358352435469079669L;

	/**
	 * Instantiates a new creates the sensor measurement statistic event.
	 */
	public CreateSensorMeasurementStatisticEvent(){
		setJournalMessageInfo("Received Sensor Measurement Statistics Envelope");
	}
	
	
	/**
	 * Gets the journal message info details.
	 *
	 * @return the journal message info details
	 */
	public String getJournalMessageInfoDetails() {
		List<SensorMeasurementStatisticDTO> arithmeticMeasurements = this.getEventAttachment().getMeasurementStatistics();
		
		String journalMessageInfo = "Received Sensor Measurement Statistics Envelope including: \n";
		for (SensorMeasurementStatisticDTO sensorMeasurement : arithmeticMeasurements) {
			ArithmeticSensorMeasurementDTO arithmeticMeasurement =  (ArithmeticSensorMeasurementDTO) sensorMeasurement.getStatistic();
			journalMessageInfo += arithmeticMeasurement.getSensor().getName() + " : " + arithmeticMeasurement.toString() + " " + arithmeticMeasurement.getSensor().getMeasurementUnit().toString()+"\n" ;
		}
		return journalMessageInfo;
	}
}
