/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.event.model.UpdateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateOperationsCentreEvent.
 */
public class UpdateOperationsCentreEvent extends OperationsCentreEvent<OperationsCentreDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9085599295942847029L;
	
	/**
	 * Instantiates a new update operations centre event.
	 */
	public UpdateOperationsCentreEvent(){
		setJournalMessageInfo("UpdateOperationsCentreEvent");
	}
	

}
