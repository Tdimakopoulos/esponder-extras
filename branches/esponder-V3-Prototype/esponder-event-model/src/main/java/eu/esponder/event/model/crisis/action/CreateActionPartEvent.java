/*
 * 
 */
package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.event.model.CreateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionPartEvent.
 */
public class CreateActionPartEvent extends ActionPartEvent<ActionPartDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3298404952899961147L;
	
	/**
	 * Instantiates a new creates the action part event.
	 */
	public CreateActionPartEvent(){
		setJournalMessageInfo("CreateActionPartEvent");
	}
}
