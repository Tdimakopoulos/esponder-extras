/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.event.model.ESponderEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class ResourceEvent.
 *
 * @param <T> the generic type
 */
public class ResourceEvent<T extends ResourceDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 790086496951447162L;

	/**
	 * Instantiates a new resource event.
	 */
	public ResourceEvent(){
		setJournalMessageInfo("ResourceEvent");
	}
}
