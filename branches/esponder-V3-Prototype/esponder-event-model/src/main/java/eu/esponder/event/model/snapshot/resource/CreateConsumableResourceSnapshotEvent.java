/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.event.model.CreateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateConsumableResourceSnapshotEvent.
 */
public class CreateConsumableResourceSnapshotEvent extends ConsumableResourceSnapshotEvent<SnapshotDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4505854454250082213L;

	/**
	 * Instantiates a new creates the consumable resource snapshot event.
	 */
	public CreateConsumableResourceSnapshotEvent(){
		setJournalMessageInfo("CreateConsumableResourceSnapshotEvent");
	}
}
