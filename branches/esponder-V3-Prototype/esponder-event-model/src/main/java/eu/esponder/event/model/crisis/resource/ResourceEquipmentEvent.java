/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.event.model.ESponderEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class ResourceEquipmentEvent.
 *
 * @param <T> the generic type
 */
public abstract class ResourceEquipmentEvent<T extends EquipmentDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1970943470682183275L;

}
