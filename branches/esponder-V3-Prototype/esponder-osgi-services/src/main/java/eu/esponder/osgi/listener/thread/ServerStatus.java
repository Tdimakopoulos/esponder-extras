/*
 * 
 */
package eu.esponder.osgi.listener.thread;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

import eu.esponder.exception.EsponderCheckedException;


// TODO: Auto-generated Javadoc
/**
 * The Class ServerStatus.
 */
public class ServerStatus {

	/**
	 * Removes the filename.
	 *
	 * @param szInput the sz input
	 * @return the string
	 */
	public String RemoveFilename(String szInput) {
		File f = new File(szInput);
		String path = f.getParent();
		return path;
	}

	/**
	 * Appand file on path.
	 *
	 * @param szInput the sz input
	 * @return the string
	 */
	public String AppandFileOnPath(String szInput) {
		String szFullPath = null;
		szFullPath = szInput + File.separator + "server.status";
		return szFullPath;
	}

	/**
	 * Appand server file on path.
	 *
	 * @param szInput the sz input
	 * @return the string
	 */
	public String AppandServerFileOnPath(String szInput) {
		String szFullPath = null;
		szFullPath = szInput + File.separator + "serverinfo.status";
		return szFullPath;
	}

	/**
	 * Read file.
	 *
	 * @param path the path
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String readFile(String path) throws IOException {
		CreateFileIfNotExists(path);
		FileInputStream stream = new FileInputStream(new File(path));
		try {
			FileChannel fc = stream.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0,
					fc.size());
			/* Instead of using default, pass in a decoder. */
			return Charset.defaultCharset().decode(bb).toString();
		} finally {
			stream.close();
		}
	}

	/**
	 * Creates the file if not exists.
	 *
	 * @param filename the filename
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void CreateFileIfNotExists(String filename) throws IOException {
		File f;
		f = new File(filename);
		if (!f.exists()) {
			f.createNewFile();
			WriteOnFile(filename, "1");
		}
	}

	/**
	 * Write on file.
	 *
	 * @param filename the filename
	 * @param content the content
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void WriteOnFile(String filename, String content) throws IOException {

		String text = content;
		BufferedWriter out = new BufferedWriter(new FileWriter(filename));
		out.write(text);
		out.close();

	}

	/**
	 * Update server running.
	 *
	 * @param szFilename the sz filename
	 * @param info the info
	 */
	public void UpdateServerRunning(String szFilename, String info) {
		String NewFile = AppandServerFileOnPath(RemoveFilename(szFilename));
		try {
			WriteOnFile(NewFile, info);
		} catch (IOException e) {
			System.out.println("Write server status info error : "
					+ e.getMessage());
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
		}
	}
}
