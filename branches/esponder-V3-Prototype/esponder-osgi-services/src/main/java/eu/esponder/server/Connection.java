/*
 * 
 */
package eu.esponder.server;


import java.io.BufferedReader;
//import java.io.DataInputStream;
import java.io.DataOutputStream;
//import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Vector;

import eu.esponder.exception.EsponderCheckedException;


// TODO: Auto-generated Javadoc
/**
 * The Class Connection.
 */
public class Connection implements Runnable {
	
	/** The client socket. */
	Socket clientSocket;

	/** The Constant THE_PASSWORD. */
	static final String THE_PASSWORD = "esponder";// the password
	
	/** The authorised. */
	boolean authorised = false;

	/** The in from client. */
	BufferedReader inFromClient;// buffered input from client
	
	/** The out to client. */
	DataOutputStream outToClient;// data output to client

	/** The state. */
	ServerState state;// the state that the server is in.

	/**
	 * Instantiates a new connection.
	 *
	 * @param clientSocket the client socket
	 */
	public Connection(Socket clientSocket) {
		this.clientSocket = clientSocket;// client socket that the client has
											// connceted through
		state = new UnAuthorised();
		try {
			inFromClient = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			outToClient = new DataOutputStream(clientSocket.getOutputStream());
		} catch (Exception e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		System.out.println("Client connected @ "
				+ clientSocket.getInetAddress());
		while (!clientSocket.isClosed()) {

			try {

				state.update();
			} catch (Exception e) {
				if (!clientSocket.isClosed()) {
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {

					}// wait 4 seconds
					try {
						clientSocket.close();
					} catch (IOException e1) {
						
					} // close the connection
				}
			}
		}
		System.out.println("Client disconnected");

	}

	// interface defines a server state
	/**
	 * The Interface ServerState.
	 */
	private interface ServerState {
		
		/**
		 * Update.
		 */
		public abstract void update();
	}

	// when the client has yet ot enter a valid password
	/**
	 * The Class UnAuthorised.
	 */
	private class UnAuthorised implements ServerState {
		
		/* (non-Javadoc)
		 * @see eu.esponder.server.Connection.ServerState#update()
		 */
		public void update() {
			// read a sentence from the client
			try {

				String clientIp = clientSocket.getInetAddress()
						.getHostAddress();
				if (!inBlockList(clientIp)) {// if the client's ip is not
												// blocked (in forbidden.txt)
					outToClient.flush();
					String password = inFromClient.readLine();// wait for the
																// password from
																// the client
					if (password.equals(THE_PASSWORD)) {// if password is
														// correct
						outToClient.writeBytes("001 PASSWD OK\n");
						state = new Authorised();// move to the authorised state
					} else {
						outToClient.writeBytes("002 PASSWD WRONG\n");// otherwise
																		// tell
																		// the
																		// client
																		// and
																		// stay
																		// in
																		// this
																		// state
					}
				} else {
					outToClient.writeBytes("003 REFUSED\n");// if blocked, tell
															// the client
					state = new Disconnect();// disconnect the client
				}
			} catch (IOException e) {
				if (!clientSocket.isClosed()) {
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {

					}// wait 4 seconds
					try {
						clientSocket.close();
					} catch (IOException e1) {
						
					} // close the connection
				}
			}

		}

		/**
		 * Gets the block list.
		 *
		 * @return the block list
		 */
		private ArrayList<String> getBlockList() {
			FileInputStream fStream;
			ArrayList<String> blocklist = new ArrayList<String>();
			try {
				fStream = new FileInputStream("forbidden.txt");
				BufferedReader in = new BufferedReader(new InputStreamReader(
						fStream));
				while (in.ready()) {
					blocklist.add(in.readLine());// adds each line in the
													// forbidden file to the
													// blocked array list.
				}
				in.close();// close the forbidden.txt's reader
			} catch (FileNotFoundException e) {
				return new ArrayList<String>(); // if no block list exists,
												// return a blank list
			} catch (IOException e) {
				if (!clientSocket.isClosed()) {
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {

					}// wait 4 seconds
					try {
						clientSocket.close();
					} catch (IOException e1) {

					} // close the connection
				}
			}
			return blocklist;// return the blocklist
		}

		/**
		 * In block list.
		 *
		 * @param clientIp the client ip
		 * @return true, if successful
		 */
		private boolean inBlockList(String clientIp) {
			ArrayList<String> blocklist = getBlockList();
			String clientDomain = clientSocket.getInetAddress()
					.getCanonicalHostName();// attempts to resolve the hostname
											// of the ip address
			if (blocklist.contains(clientIp)
					|| blocklist.contains(clientDomain)) {
				return true; // if the ip/cleint domain is in the list, return
								// true
			} else {
				return false;// otherwise, return false
			}
		}

	}

	/**
	 * The Class Authorised.
	 */
	private class Authorised implements ServerState {
		
		/* (non-Javadoc)
		 * @see eu.esponder.server.Connection.ServerState#update()
		 */
		public void update() {

			try {
				String command = inFromClient.readLine();// read a command from
															// the client
				// if it is a valid command word
				if (command.equals("exit")) {
					state = new Disconnect(); // change state to disconnect
				} else if (command.equals("log")) {
					state = new TailLogFile(); // move to lit remote files state
				} else if (command.equals("")) {
				}
			} catch (IOException e) {
				if (!clientSocket.isClosed()) {
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {

					}// wait 4 seconds
					try {
						clientSocket.close();
					} catch (IOException e1) {

					} // close the connection
				}
			}
		}
	}

	/**
	 * The Class Disconnect.
	 */
	private class Disconnect implements ServerState {
		
		/* (non-Javadoc)
		 * @see eu.esponder.server.Connection.ServerState#update()
		 */
		public void update() {
			try {
				Thread.sleep(4000);// wait 4 seconds
				clientSocket.close(); // close the connection
			} catch (IOException e) {
				if (!clientSocket.isClosed()) {
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {

					}// wait 4 seconds
					try {
						clientSocket.close();
					} catch (IOException e1) {

					} // close the connection
				}
			} catch (InterruptedException e) {
				if (!clientSocket.isClosed()) {
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {

					}// wait 4 seconds
					try {
						clientSocket.close();
					} catch (IOException e1) {

					} // close the connection
				}
			}

		}

	}

	/**
	 * The Class TailLogFile.
	 */
	private class TailLogFile implements ServerState {
		
		/* (non-Javadoc)
		 * @see eu.esponder.server.Connection.ServerState#update()
		 */
		@SuppressWarnings("unchecked")
		public void update() {

			String fileName = "/home/exodus/ESPONDER/jboss-6.1.0/server/default/log/server.log";
			int linesFromEnd = 4000;
			@SuppressWarnings("rawtypes")
			Vector atTheEnd = new Vector();
			RandomAccessFile raf;
			try {
				raf = new RandomAccessFile(fileName, "r");
				// Line size to 100 bytes, shold be a better way to make a
				// guess.
				int lineSize = 100;
				long iseek = raf.length() - linesFromEnd * lineSize;

				if (iseek <= 0)
					iseek = raf.length() - 3000 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 2500 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 2000 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 1800 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 1600 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 1500 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 1300 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 1100 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 1000 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 500 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 400 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 300 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 100 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 50 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 10 * lineSize;
				if (iseek <= 0)
					iseek = raf.length() - 1 * lineSize;
				// set the file pointer to "linesFromEnd" before end.
				raf.seek(iseek);
				// read from that place to end of file

				String line = "";
				while ((line = raf.readLine()) != null) {
					atTheEnd.add(line);
				}
				// release resources
				raf.close();
				System.out.println("Size -- " + atTheEnd.size());
			} catch (FileNotFoundException e1) {

				e1.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}

			try {
				outToClient.writeBytes("031 REMOTEFILELOG\n");// tells the
																// client that
																// it will
																// receive a
																// remote file
																// list

				for (int i = 0; i < atTheEnd.size(); i++) {
					outToClient.writeBytes(atTheEnd.elementAt(i).toString()
							+ "\n");

				}
				outToClient.writeBytes("032 ENDOFFILELOG\n");// end of remote
																// fiel list.
			} catch (IOException e) {
				if (!clientSocket.isClosed()) {
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {

					}// wait 4 seconds
					try {
						clientSocket.close();
					} catch (IOException e1) {

					} // close the connection
				}
			}
			state = new Authorised();

		}

	}
}