package eu.esponder.jaxb.model;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonIgnore;

import eu.esponder.jaxb.model.crisis.CrisisContextDTO;
import eu.esponder.jaxb.model.crisis.action.ActionDTO;
import eu.esponder.jaxb.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.jaxb.model.crisis.action.ActionPartDTO;
import eu.esponder.jaxb.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.jaxb.model.crisis.action.ActionScheduleCriteriaDTO;
import eu.esponder.jaxb.model.crisis.action.ActionScheduleDTO;
import eu.esponder.jaxb.model.crisis.resource.ResourceDTO;
import eu.esponder.jaxb.model.crisis.view.MapPointDTO;
import eu.esponder.jaxb.model.crisis.view.ResourcePOIDTO;
import eu.esponder.jaxb.model.snapshot.SnapshotDTO;
import eu.esponder.jaxb.model.snapshot.location.LocationAreaDTO;
import eu.esponder.jaxb.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.jaxb.model.user.ESponderUserDTO;


@XmlTransient
@XmlSeeAlso({
	ActionDTO.class,
	ActionObjectiveDTO.class,
	ActionPartDTO.class,
	ActionPartObjectiveDTO.class,
	ActionScheduleCriteriaDTO.class,
	ActionScheduleDTO.class,
	CrisisContextDTO.class,
	ESponderUserDTO.class,
	LocationAreaDTO.class,
	MapPointDTO.class,
	ResourceDTO.class,
	ResourcePOIDTO.class,
	SnapshotDTO.class,
	StatisticsConfigDTO.class
})
public abstract class ESponderEntityDTO {
	
	protected Long id;

	@JsonIgnore
//	@XmlTransient
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}

