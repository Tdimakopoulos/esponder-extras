package eu.esponder.jaxb.model.snapshot.status;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="CrisisContextSnapshotStatus")
public enum CrisisContextSnapshotStatusDTO {
	STARTED,
	RESOLVED,
	UNRESOLVED
}
