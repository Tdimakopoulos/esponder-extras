/*
 * 
 */
package eu.esponder.rest.PointsAndCircles;

import org.codehaus.enunciate.XmlTransient;

// TODO: Auto-generated Javadoc
/**
 * The Class Utils.
 */
@XmlTransient
public class Utils {

	//return the distance between two points
	/**
	 * Find distace.
	 *
	 * @param lat_a the lat_a
	 * @param lng_a the lng_a
	 * @param lat_b the lat_b
	 * @param lng_b the lng_b
	 * @return the double
	 */
	@SuppressWarnings("unused")
	private double FindDistace(float lat_a, float lng_a, float lat_b, float lng_b) {
	    float pk = (float) (180/3.14169);

	    float a1 = lat_a / pk;
	    float a2 = lng_a / pk;
	    float b1 = lat_b / pk;
	    float b2 = lng_b / pk;

	    double t1 = Math.cos(a1)*Math.cos(a2)*Math.cos(b1)*Math.cos(b2);
	    double t2 = Math.cos(a1)*Math.sin(a2)*Math.cos(b1)*Math.sin(b2);
	    double t3 = Math.sin(a1)*Math.sin(b1);
	    double tt = Math.acos(t1 + t2 + t3);

	    return 6366000*tt;
	}
	
	//return the center between two points
	/**
	 * Find center.
	 *
	 * @param lat1 the lat1
	 * @param lon1 the lon1
	 * @param lat2 the lat2
	 * @param lon2 the lon2
	 * @return the double
	 */
	@SuppressWarnings("unused")
	private double FindCenter(double lat1, double lon1, double lat2, double lon2) {
		
		double dLat = DegreeToRad(lat2-lat1);
		double dLon = DegreeToRad(lon2-lon1);
		double Bx = Math.cos(lat2) * Math.cos(dLon);
		double By = Math.cos(lat2) * Math.sin(dLon);
		double lat3 = Math.atan2(Math.sin(lat1)+Math.sin(lat2),
	                      Math.sqrt( (Math.cos(lat1)+Bx)*(Math.cos(lat1)+Bx) + By*By) ); 
		double lon3 = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);
		return lon3;
	}

   // convert degrees to rad
   /**
    * Degree to rad.
    *
    * @param degree the degree
    * @return the double
    */
   private double DegreeToRad(double degree)
   {
	   //Get radians value for degree  
	   return Math.toRadians(degree);
   }
}
