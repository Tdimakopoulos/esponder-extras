/*
 * 
 */
package eu.esponder.rest.portal;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.EquipmentCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OperationsCentreCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PlannableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.dto.model.type.RankTypeDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderCategories.
 */
@Path("/portal/categories")
public class ESponderCategories extends ESponderResource {

	

	/**
	 * Find plannable resource category by id.
	 *
	 * @param categoryID the category id
	 * @param pkiKey the pki key
	 * @return the plannable resource category dto
	 */
	
	@GET
	@Path("/ResourceCategories/findById")
	@Produces({MediaType.APPLICATION_JSON})
	public PlannableResourceCategoryDTO findPlannableResourceCategoryById(
			@QueryParam("categoryID") @NotNull(message = "categoryID may not be null") Long categoryID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);

		PlannableResourceCategoryDTO categoryDTO = (PlannableResourceCategoryDTO) this.getResourceCategoryRemoteService().
				findByIdRemote(PlannableResourceCategoryDTO.class, categoryID);
		return categoryDTO;
	}
	
	
	/**
	 * Find organisation category by type.
	 *
	 * @param disciplineTypeID the discipline type id
	 * @param organisationTypeID the organisation type id
	 * @param pkiKey the pki key
	 * @return the organisation category dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@GET
	@Path("/ResourceCategories/findOrganisationCategoryByType")
	@Produces({MediaType.APPLICATION_JSON})
	public OrganisationCategoryDTO findOrganisationCategoryByType(
			@QueryParam("disciplineTypeID") @NotNull(message = "Discipline Type ID may not be null") Long disciplineTypeID,
			@QueryParam("organisationTypeID") @NotNull(message = "Organisation Type ID may not be null") Long organisationTypeID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws ClassNotFoundException{

		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);
		DisciplineTypeDTO disciplineType = (DisciplineTypeDTO) this.getTypeRemoteService().findDTOById(disciplineTypeID);
		OrganisationTypeDTO organisationType = (OrganisationTypeDTO) this.getTypeRemoteService().findDTOById(organisationTypeID);
		if(disciplineType!= null && organisationType != null)
			return this.getResourceCategoryRemoteService().findOrganisationCategoryDTOByType(disciplineType, organisationType);
		else
			return null;
	}
	
	/**
	 * Find equipment category by type.
	 *
	 * @param equipmentTypeID the equipment type id
	 * @param pkiKey the pki key
	 * @return the equipment category dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@GET
	@Path("/ResourceCategories/findEquipmentCategoryByType")
	@Produces({MediaType.APPLICATION_JSON})
	public EquipmentCategoryDTO findEquipmentCategoryByType(
			@QueryParam("equipmentTypeID") @NotNull(message = "Equipment Type ID may not be null") Long equipmentTypeID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws ClassNotFoundException {

		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);
		EquipmentTypeDTO equipmentType = (EquipmentTypeDTO) this.getTypeRemoteService().findDTOById(equipmentTypeID);
		if(equipmentType!= null)
			return this.getResourceCategoryRemoteService().findEquipmentCategoryDTOByType(equipmentType);
		else
			return null;
	}
	
	/**
	 * Find consumable category by type.
	 *
	 * @param consumableTypeID the consumable type id
	 * @param pkiKey the pki key
	 * @return the consumable resource category dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@GET
	@Path("/ResourceCategories/findConsumableCategoryByType")
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceCategoryDTO findConsumableCategoryByType(
			@QueryParam("consumableTypeID") @NotNull(message = "Consumable Type ID may not be null") Long consumableTypeID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws ClassNotFoundException {

		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);
		ConsumableResourceTypeDTO consumablesType = (ConsumableResourceTypeDTO) this.getTypeRemoteService().findDTOById(consumableTypeID);
		if(consumablesType!= null)
			return this.getResourceCategoryRemoteService().findConsumableCategoryDTOByType(consumablesType);
		else
			return null;
	}
	
	/**
	 * Find reusable category by type.
	 *
	 * @param reusableTypeID the reusable type id
	 * @param pkiKey the pki key
	 * @return the reusable resource category dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@GET
	@Path("/ResourceCategories/findReusableCategoryByType")
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceCategoryDTO findReusableCategoryByType(
			@QueryParam("reusableTypeID") @NotNull(message = "Reusable Type ID may not be null") Long reusableTypeID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws ClassNotFoundException {

		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);
		ReusableResourceTypeDTO reusableType = (ReusableResourceTypeDTO) this.getTypeRemoteService().findDTOById(reusableTypeID);
		if(reusableType!= null)
			return this.getResourceCategoryRemoteService().findReusableCategoryDTOByType(reusableType);
		else
			return null;
	}
	
	

	
	
	/**
 * Creates the resource category.
 *
 * @param category the category
 * @param pkiKey the pki key
 * @return the resource category dto
 * @throws ClassNotFoundException the class not found exception
 */
@POST
	@Path("/ResourceCategories/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResourceCategoryDTO createResourceCategory(
			@NotNull(message="category may not be null") ResourceCategoryDTO category,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey
			) throws ClassNotFoundException {
		
	if(!SecurityCheck(pkiKey,1))
	{
		//throw security exception and stop
		return null;
	}
	Long userID=SecurityGetUserID(pkiKey);
		
		return this.getResourceCategoryRemoteService().create(category, userID);
	}
	
	
	/**
	 * Update organisation category.
	 *
	 * @param organisationCategoryID the organisation category id
	 * @param disciplineTypeID the discipline type id
	 * @param organisationTypeId the organisation type id
	 * @param organisationID the organisation id
	 * @param pkiKey the pki key
	 * @return the organisation category dto
	 */
	@PUT
	@Path("/ResourceCategories/updateOrganisationCategory")
	@Produces({MediaType.APPLICATION_JSON})
	public OrganisationCategoryDTO updateOrganisationCategory(
			@QueryParam("organisationCategoryID") @NotNull(message="Organisation category may not be null") Long organisationCategoryID,
			@QueryParam("disciplineTypeID") @NotNull(message="Discipline Type ID may not be null") Long disciplineTypeID,
			@QueryParam("organisationTypeId") @NotNull(message="Organisation Type ID may not be null") Long organisationTypeId,
			@QueryParam("organisationID") @NotNull(message="Organisation ID may not be null") Long organisationID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey
			) {
		
		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);
		
		return this.getResourceCategoryRemoteService().updateOrganizationCategory(organisationCategoryID, disciplineTypeID, organisationTypeId, organisationID, userID);
	}
	
	/**
	 * Update equipment category.
	 *
	 * @param equipmentCategoryID the equipment category id
	 * @param equipmentTypeID the equipment type id
	 * @param pkiKey the pki key
	 * @return the equipment category dto
	 */
	@PUT
	@Path("/ResourceCategories/updateEquipmentCategory")
	@Produces({MediaType.APPLICATION_JSON})
	public EquipmentCategoryDTO updateEquipmentCategory(
			@QueryParam("equipmentCategoryID") @NotNull(message="Equipemnt category ID may not be null") Long equipmentCategoryID,
			@QueryParam("equipmentTypeID") @NotNull(message="Equipment Type ID may not be null") Long equipmentTypeID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey
			) {
		
		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);
		
		return this.getResourceCategoryRemoteService().updateEquipmentCategory(equipmentCategoryID, equipmentTypeID, userID);
	}
	
	/**
	 * Update reusable resource category.
	 *
	 * @param reusableCategoryID the reusable category id
	 * @param reusableTypeID the reusable type id
	 * @param pkiKey the pki key
	 * @return the reusable resource category dto
	 */
	@PUT
	@Path("/ResourceCategories/updateReusableResourceCategory")
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceCategoryDTO updateReusableResourceCategory(
			@QueryParam("reusableCategoryID") @NotNull(message="Reusable Resource category ID may not be null") Long reusableCategoryID,
			@QueryParam("reusableTypeID") @NotNull(message="Reusable Type ID may not be null") Long reusableTypeID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey
			) {
		
		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);
		
		return this.getResourceCategoryRemoteService().updateReusableResourceCategory(reusableCategoryID, reusableTypeID, userID);
	}
	
	
	/**
	 * Update consumable resource category.
	 *
	 * @param consumableCategoryID the consumable category id
	 * @param consumableTypeID the consumable type id
	 * @param pkiKey the pki key
	 * @return the consumable resource category dto
	 */
	@PUT
	@Path("/ResourceCategories/updateConsumableResourceCategory")
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceCategoryDTO updateConsumableResourceCategory(
			@QueryParam("consumableCategoryID") @NotNull(message="Consumable Resource category ID may not be null") Long consumableCategoryID,
			@QueryParam("reusableTypeID") @NotNull(message="Reusable Type ID may not be null") Long consumableTypeID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey
			){
		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);
		
		return this.getResourceCategoryRemoteService().updateConsumableResourceCategory(consumableCategoryID, consumableTypeID, userID);
	}
	
	/**
	 * Update personnel category.
	 *
	 * @param personnelCategoryID the personnel category id
	 * @param personnelCompetenceIDs the personnel competence i ds
	 * @param rankID the rank id
	 * @param organisationCategoryID the organisation category id
	 * @param pkiKey the pki key
	 * @return the personnel category dto
	 * @throws ClassNotFoundException the class not found exception
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	@PUT
	@Path("/ResourceCategories/updatePersonnelCategory")
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelCategoryDTO updatePersonnelCategory(
			@QueryParam("personnelCategoryID") @NotNull(message="Personnel category ID may not be null") Long personnelCategoryID,
			@QueryParam("competenceIDs") @NotNull(message="Personnel Competence IDs may not be null") Set<Long> personnelCompetenceIDs,
			@QueryParam("rankID") @NotNull(message="Rank ID may not be null") Long rankID,
			@QueryParam("organisationCategoryID") @NotNull(message="Organisation category ID may not be null") Long organisationCategoryID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey
			) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);
		return this.getResourceCategoryRemoteService().updatePersonnelCategory(personnelCategoryID, personnelCompetenceIDs, rankID, organisationCategoryID, userID);
	}
	
	/**
	 * Update operations centre category.
	 *
	 * @param OperationsCentreCategoryID the operations centre category id
	 * @param operationsCentreTypeID the operations centre type id
	 * @param pkiKey the pki key
	 * @return the operations centre category dto
	 */
	@PUT
	@Path("/ResourceCategories/updateOperationsCentreCategory")
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreCategoryDTO updateOperationsCentreCategory(
			@QueryParam("OperationsCentreCategoryID") @NotNull(message="Operations Centre category ID may not be null") Long OperationsCentreCategoryID,
			@QueryParam("OperationsCentreTypeID") @NotNull(message="OperationsCentre Type ID may not be null") Long operationsCentreTypeID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey
			) {
		
		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);
		
		return this.getResourceCategoryRemoteService().updateOperationsCentreCategory(OperationsCentreCategoryID, operationsCentreTypeID, userID);
	}
	
	
	/**
	 * Delete resource category.
	 *
	 * @param resourceCategoryID the resource category id
	 * @param pkiKey the pki key
	 * @return the long
	 */
	@DELETE
	@Path("/ResourceCategories/deleteResourceCategory")
	public Long deleteResourceCategory(
			@QueryParam("resourceCategoryID") @NotNull(message="Operations Centre category ID may not be null") Long resourceCategoryID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey
			) {
		
		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);
		return this.getResourceCategoryRemoteService().deleteResourceCategoryRemote(resourceCategoryID);
	}
	
	
	

}
