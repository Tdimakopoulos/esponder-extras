/*
 * 
 */
package eu.esponder.rest.portal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.naming.NamingException;

import org.codehaus.enunciate.XmlTransient;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.controller.crisis.resource.SensorService;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.ArithmeticMeasurementSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationMeasurementSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.dto.osgi.service.event.ESponderEventPublisher;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class FRUThread.
 */
@XmlTransient
public class FRUThread extends Thread {

	/** The fru id. */
	private Long fruId;

	/** The event source. */
	private FirstResponderActorDTO eventSource;

	/** The thread period fru. */
	private long threadPeriodFRU;

	/** The threads list. */
	private List<Thread> threadsList;

	/** The sensors. */
	private List<SensorDTO> sensors;

	/** The publisher. */
	private ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher;

	/** The statistics list. */
	public List<SensorMeasurementStatisticDTO> statisticsList;

	/** The range temp. */
	private int rangeTemp;

	/** The min temp. */
	private int minTemp;

	/** The min step. */
	private int minStep;

	/** The max step. */
	private int maxStep;

	/** The start point. */
	private PointDTO startPoint;

	/** The dest point. */
	private PointDTO destPoint;

	/**
	 * Gets the sensor service.
	 *
	 * @return the sensor service
	 */
	protected SensorService getSensorService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Instantiates a new fRU thread.
	 *
	 * @param publisher the publisher
	 * @param eventSource the event source
	 * @param threadPeriodFRU the thread period fru
	 * @param range the range
	 * @param min the min
	 * @param startingPoint the starting point
	 * @param destinationPoint the destination point
	 * @param minStep the min step
	 * @param maxStep the max step
	 */
	public FRUThread(ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher,
			FirstResponderActorDTO eventSource, long threadPeriodFRU, int range,
			int min, PointDTO startingPoint, PointDTO destinationPoint, int minStep, int maxStep) {
		this.publisher = publisher;
		this.eventSource = eventSource;
		this.sensors = createListOfSensorsFromActor(this.eventSource);
		this.threadPeriodFRU = threadPeriodFRU;
		statisticsList = new ArrayList<SensorMeasurementStatisticDTO>();
		this.rangeTemp = range;
		this.minTemp = min;
		this.setMinStep(minStep);
		this.setMaxStep(maxStep);
		this.setStartPoint(startingPoint);
		this.setDestPoint(destinationPoint);
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {

		int counter = 15;

		this.setThreadsList(CreateSensorThreads());

		for(Thread thread : threadsList) {
			thread.start();
		}

		while(counter > 0) {
			try {
				// 1. Get measurements from SensorThreads
				if(getStatisticsList().size() != 0)
					// 2. Put them into a SensorMeasurementStatisticEnvelopeDTO
					accessStatistics(this.getPublisher());
				else
					System.out.println("\nEnvelope is empty \n");
				counter--;
				Thread.sleep(threadPeriodFRU);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		for(Thread thread : threadsList)
			thread.interrupt();

		for(Thread thread : threadsList) {
			try {
				thread.join(500);
			} catch (InterruptedException e) {
				System.out.println("Interrupted while waiting for Thread "+thread.getName()+"/"+thread.getId()+" to die...");
			}
		}

		printThreadsState();

		if(getStatisticsList().size() != 0) {
			accessStatistics(publisher);
		}
	}


	/**
	 * Creates the sensor threads.
	 *
	 * @return the list
	 */
	private List<Thread> CreateSensorThreads() {

		List<Thread> tempThreadsList = new ArrayList<Thread>();

		System.out.println("Thread executing : " + Thread.currentThread().getName() + " with id : "+Thread.currentThread().getId());
		for(SensorDTO sensor : this.sensors) {
			for(StatisticsConfigDTO config : sensor.getConfiguration()) {
				SensorThread sensorThread = null;
				if (sensor instanceof ArithmeticMeasurementSensorDTO) { 
					sensorThread = new ArithmeticSensorThread(new Long(config.getSamplingPeriodMilliseconds()), sensor, config.getMeasurementStatisticType(), minTemp, rangeTemp, statisticsList);
				} else if (sensor instanceof LocationMeasurementSensorDTO) {
					sensorThread = new LocationSensorThread(new Long(config.getSamplingPeriodMilliseconds()), sensor, config.getMeasurementStatisticType(), this.getStartPoint(),
							this.getDestPoint(), statisticsList, this.getMinStep(), this.getMaxStep());
				}
				tempThreadsList.add(sensorThread);
			}
		}
		return tempThreadsList;
	}

	/**
	 * Access statistics.
	 *
	 * @param publisher the publisher
	 */
	private void accessStatistics(ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher) {

		SensorMeasurementStatisticEnvelopeDTO envelope = new SensorMeasurementStatisticEnvelopeDTO();
		envelope.setMeasurementStatistics(new ArrayList<SensorMeasurementStatisticDTO>());
		synchronized (this.statisticsList) {

			for( Iterator< SensorMeasurementStatisticDTO > it = statisticsList.iterator(); it.hasNext();)
			{
				SensorMeasurementStatisticDTO statisticMeasurement = it.next();
				envelope.getMeasurementStatistics().add(statisticMeasurement);
				it.remove();
			}

			//			System.out.println(Thread.currentThread().getClass() + " / " + Thread.currentThread().getName() + "with id : "
			//					+Thread.currentThread().getId()+"\nEnvelope Contents size is : " + envelope.getMeasurementStatistics().size());

			//	3. Publish a CreateSensorMeasurementStatisticEvent
			CreateSensorMeasurementStatisticEvent event = createSensorMeasurementStatisticEvent(envelope);
			try {
				publisher.publishEvent(event);
			} catch (EventListenerException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * Creates the sensor measurement statistic event.
	 *
	 * @param envelope the envelope
	 * @return the creates the sensor measurement statistic event
	 */
	private CreateSensorMeasurementStatisticEvent createSensorMeasurementStatisticEvent(SensorMeasurementStatisticEnvelopeDTO envelope) {
		CreateSensorMeasurementStatisticEvent event = new CreateSensorMeasurementStatisticEvent();
		event.setEventAttachment(envelope);
		event.setEventSeverity(SeverityLevelDTO.UNDEFINED);
		event.setEventSource(this.eventSource);
		event.setJournalMessage("Test Journal Message");
		event.setEventTimestamp(new Date());
		return event;
	}

	/**
	 * Creates the list of sensors from actor.
	 *
	 * @param actor the actor
	 * @return the list
	 */
	private List<SensorDTO> createListOfSensorsFromActor(FirstResponderActorDTO actor) {
		List<SensorDTO> fruSensors = new ArrayList<SensorDTO>();
		for(EquipmentDTO actorEquipment : actor.getEquipmentSet()) {
			for(Long sensorID : actorEquipment.getSensors()) {
				SensorDTO sensor = getSensorService().findSensorByIdRemote(sensorID);
				if(sensor!=null){
					if(sensor.getType().contentEquals("TEMP") || sensor.getType().contentEquals("LOCATION"))
						fruSensors.add(sensor);	
				}

			}
		}
		System.out.println("Sensor Created : "+fruSensors.size());
		return fruSensors;
	}

	/**
	 * Prints the threads state.
	 */
	private void printThreadsState() {

		System.out.println("\n\n********************	Threads State	********************");
		System.out.println(Thread.currentThread().getName()+"/"+Thread.currentThread().getId());
		for(Thread thread : this.threadsList) {
			System.out.println(thread.getName()+":"+thread.getId()+":"+ thread.getState()+":"+ thread.isAlive());
		}
	}


	/**
	 * Gets the statistics list.
	 *
	 * @return the statistics list
	 */
	public synchronized List<SensorMeasurementStatisticDTO> getStatisticsList() {
		return statisticsList;
	}

	/**
	 * Sets the statistics list.
	 *
	 * @param statisticsList the new statistics list
	 */
	public synchronized void setStatisticsList(List<SensorMeasurementStatisticDTO> statisticsList) {
		this.statisticsList = statisticsList;
	}

	/**
	 * Gets the fru id.
	 *
	 * @return the fru id
	 */
	public Long getFruId() {
		return fruId;
	}

	/**
	 * Sets the fru id.
	 *
	 * @param fruId the new fru id
	 */
	public void setFruId(Long fruId) {
		this.fruId = fruId;
	}

	/**
	 * Gets the thread period fru.
	 *
	 * @return the thread period fru
	 */
	public long getThreadPeriodFRU() {
		return threadPeriodFRU;
	}

	/**
	 * Sets the thread period fru.
	 *
	 * @param threadPeriod the new thread period fru
	 */
	public void setThreadPeriodFRU(long threadPeriod) {
		this.threadPeriodFRU = threadPeriod;
	}

	/**
	 * Gets the sensors.
	 *
	 * @return the sensors
	 */
	public List<SensorDTO> getSensors() {
		return sensors;
	}

	/**
	 * Sets the sensors.
	 *
	 * @param sensors the new sensors
	 */
	public void setSensors(List<SensorDTO> sensors) {
		this.sensors = sensors;
	}

	/**
	 * Gets the threads list.
	 *
	 * @return the threads list
	 */
	public List<Thread> getThreadsList() {
		return threadsList;
	}

	/**
	 * Sets the threads list.
	 *
	 * @param threadsList the new threads list
	 */
	public void setThreadsList(List<Thread> threadsList) {
		this.threadsList = threadsList;
	}

	/**
	 * Gets the event source.
	 *
	 * @return the event source
	 */
	public FirstResponderActorDTO getEventSource() {
		return eventSource;
	}

	/**
	 * Sets the event source.
	 *
	 * @param eventSource the new event source
	 */
	public void setEventSource(FirstResponderActorDTO eventSource) {
		this.eventSource = eventSource;
	}

	/**
	 * Gets the start point.
	 *
	 * @return the start point
	 */
	public PointDTO getStartPoint() {
		return startPoint;
	}

	/**
	 * Sets the start point.
	 *
	 * @param startPoint the new start point
	 */
	public void setStartPoint(PointDTO startPoint) {
		this.startPoint = startPoint;
	}

	/**
	 * Gets the dest point.
	 *
	 * @return the dest point
	 */
	public PointDTO getDestPoint() {
		return destPoint;
	}

	/**
	 * Sets the dest point.
	 *
	 * @param destPoint the new dest point
	 */
	public void setDestPoint(PointDTO destPoint) {
		this.destPoint = destPoint;
	}

	/**
	 * Gets the publisher.
	 *
	 * @return the publisher
	 */
	public ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> getPublisher() {
		return publisher;
	}

	/**
	 * Sets the publisher.
	 *
	 * @param publisher the new publisher
	 */
	public void setPublisher(
			ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher) {
		this.publisher = publisher;
	}

	/**
	 * Gets the min step.
	 *
	 * @return the min step
	 */
	public int getMinStep() {
		return minStep;
	}

	/**
	 * Sets the min step.
	 *
	 * @param minStep the new min step
	 */
	public void setMinStep(int minStep) {
		this.minStep = minStep;
	}

	/**
	 * Gets the max step.
	 *
	 * @return the max step
	 */
	public int getMaxStep() {
		return maxStep;
	}

	/**
	 * Sets the max step.
	 *
	 * @param maxStep the new max step
	 */
	public void setMaxStep(int maxStep) {
		this.maxStep = maxStep;
	}

}
