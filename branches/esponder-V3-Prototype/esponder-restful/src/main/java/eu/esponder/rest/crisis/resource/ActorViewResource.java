/*
 * 
 */
package eu.esponder.rest.crisis.resource;

import java.io.IOException;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorCMDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.security.KeyStorageDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class ActorViewResource.
 */
@Path("/crisis/resource")
public class ActorViewResource extends ESponderResource {

	
	/**
	 * Gets the actor.
	 *
	 * @param actorID the actor id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor class
	 */
	@GET
	@Path("/actor/findActorClass")
	@Produces({MediaType.APPLICATION_JSON})
	public String getActorClass(
			@QueryParam("actorID") @NotNull(message="actorID may not be null" ) Long actorID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().findActorClassByIdRemote(actorID);
	}
	
	/**
	 * Gets the actor.
	 *
	 * @param actorID the actor id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor
	 */
	@GET
	@Path("/actor/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO getActor( @QueryParam("actorID") @NotNull(message="actorID may not be null" ) Long actorID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().findByIdRemote(actorID);
	}
	
	/**
	 * Gets the actor.
	 *
	 * @param actorCMID the actor cmid
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor
	 */
	@GET
	@Path("/actor/findCrisisManagerByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorCMDTO findActorCMByID( @QueryParam("actorCMID") @NotNull(message="actorCMID may not be null" ) Long actorCMID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().findCrisisManagerByIdRemote(actorCMID);
	}
	
	/**
	 * Gets the actor.
	 *
	 * @param actorICID the actor icid
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor
	 */
	@GET
	@Path("/actor/findIncidentCommanderByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorICDTO findActorICByID( @QueryParam("actorICID") @NotNull(message="actorICID may not be null" ) Long actorICID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().findIncidentCommanderByIdRemote(actorICID);
	}
	
	/**
	 * Gets the actor.
	 *
	 * @param esponderUserID the esponder user id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor
	 */
	@GET
	@Path("/actor/findESponderUserByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderUserDTO findESponderUserDTOByID( @QueryParam("esponderUserID") @NotNull(message="esponderUserID may not be null" ) Long esponderUserID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().findESponderUserByIdRemote(esponderUserID);
	}
	
	/**
	 * Gets the actor.
	 *
	 * @param actorFRCID the actor frcid
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor
	 */
	@GET
	@Path("/actor/findFRCByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRCDTO findActorFRCByID( @QueryParam("actorFRCID") @NotNull(message="actorFRCID may not be null" ) Long actorFRCID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().findFRChiefByIdRemote(actorFRCID);
	}
	
	/**
	 * Gets the actor.
	 *
	 * @param actorFRID the actor frid
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor
	 */
	@GET
	@Path("/actor/findFRByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRDTO findActorFRByID( @QueryParam("actorFRID") @NotNull(message="actorFRID may not be null" ) Long actorFRID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().findFRByIdRemote(actorFRID);
	}
	
	
	
	/**
	 * Gets the actor.
	 *
	 * @param actorTitle the actor title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor
	 */
	@GET
	@Path("/actor/getFRCByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRCDTO getFRCByTitle( @QueryParam("actorTitle") @NotNull(message="actorTitle may not be null" ) String actorTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().findFRCByTitleRemote(actorTitle);
	}
	
	/**
	 * Gets the actor.
	 *
	 * @param actorTitle the actor title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor
	 */
	@GET
	@Path("/actor/getFRByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRDTO getFRByTitle( @QueryParam("actorTitle") @NotNull(message="actorTitle may not be null" ) String actorTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().findFRByTitleRemote(actorTitle);

	}
	
	/**
	 * Gets the actor.
	 *
	 * @param actorTitle the actor title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor
	 */
	@GET
	@Path("/actor/getCMByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorCMDTO getCMByTitle( @QueryParam("actorTitle") @NotNull(message="actorTitle may not be null" ) String actorTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().findCMByTitleRemote(actorTitle);

	}
	
	/**
	 * Gets the actor.
	 *
	 * @param actorTitle the actor title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor
	 */
	@GET
	@Path("/actor/getICByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorICDTO getICByTitle( @QueryParam("actorTitle") @NotNull(message="actorTitle may not be null" ) String actorTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().findICByTitleRemote(actorTitle);

	}

	/**
	 * Gets the all actors.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the all actors
	 */
	@GET
	@Path("/actor/getAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getAllActors(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return new ResultListDTO(this.getActorRemoteService().findAllActorsRemote());
	}

	// Find Subordinates By ActorID
	/**
	 * Gets the actor subordinates.
	 *
	 * @param actorID the actor id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor subordinates
	 */
	@GET
	@Path("/actor/getSubordinates")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getActorSubordinates(
			@QueryParam("actorID") @NotNull(message="actorID may not be null" ) Long actorID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ResultListDTO resultList = new ResultListDTO(this.getActorRemoteService().findSubordinatesByIdRemote(actorID));
		return resultList;
	}

	/**
	 * Creates the FR.
	 *
	 * @param actorDTO the actor FR dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor FR dto
	 */
	@POST
	@Path("/actor/createFR")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRDTO createActorFR(@NotNull(message="actorFRDTO may not be null" ) ActorFRDTO actorDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().createFRRemote(actorDTO, userID);
	}
	
	/**
	 * Creates the actor FRC.
	 *
	 * @param actorDTO the actor FRC dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor FRC dto
	 */
	@POST
	@Path("/actor/createFRC")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRCDTO createActorFRC(@NotNull(message="actorFRCDTO may not be null" ) ActorFRCDTO actorDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().createFRChiefRemote(actorDTO, userID);
	}
	
	/**
	 * Creates the actor.
	 *
	 * @param esponderUserDTO the esponder user dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the esponder User DTO
	 */
	@POST
	@Path("/actor/createESponderUser")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderUserDTO createESponderUser(@NotNull(message="ESponderUserDTO may not be null" ) ESponderUserDTO esponderUserDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().createESponderUseremote(esponderUserDTO, userID);
	}
	
	/**
	 * Creates the crisis Manager.
	 *
	 * @param actorCMDTO the crisis manager dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis manager dto
	 */
	@POST
	@Path("/actor/createCM")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorCMDTO createActorCM(@NotNull(message="actorCMDTO may not be null" ) ActorCMDTO actorCMDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getActorRemoteService().createCrisisManagerRemote(actorCMDTO, userID);
	}
	
	/**
	 * Creates the incident commander.
	 *
	 * @param actorICDTO the actor IC dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor IC dto
	 */
	@POST
	@Path("/actor/createIC")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorICDTO createActorIC(@NotNull(message="actorDTO may not be null" ) ActorICDTO actorICDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getActorRemoteService().createIncidentCommanderRemote(actorICDTO, userID);
	}

	/**
	 * Update actor.
	 *
	 * @param actorCMDTO the actor cmdto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@PUT
	@Path("/actor/updateCM")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorCMDTO updateActorCM(@NotNull(message="actorCMDTO may not be null" ) ActorCMDTO actorCMDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().updateCrisisManagerRemote(actorCMDTO, userID);
	}
	
	/**
	 * Update actor.
	 *
	 * @param actorICDTO the actor icdto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor IC dto
	 */
	@PUT
	@Path("/actor/updateIC")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorICDTO updateActorIC(@NotNull(message="actorICDTO may not be null" ) ActorICDTO actorICDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().updateIncidentCommanderRemote(actorICDTO, userID);
	}
	
	/**
	 * Update actor.
	 *
	 * @param esponderUserDTO the esponder user dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@PUT
	@Path("/actor/updateEsponderUser")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderUserDTO updateESponderUser(@NotNull(message="ESponderUserDTO may not be null" ) ESponderUserDTO esponderUserDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().updateEsponderUserRemote(esponderUserDTO, userID);
	}
	
	/**
	 * Update actor.
	 *
	 * @param actorFRCDTO the actor frcdto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@PUT
	@Path("/actor/updateActorFRC")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRCDTO updateActorFRC(@NotNull(message="actorFRCDTO may not be null" ) ActorFRCDTO actorFRCDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getActorRemoteService().updateFRCRemote(actorFRCDTO, userID);
	}
	
	/**
	 * Update actor.
	 *
	 * @param actorFRDTO the actor frdto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@PUT
	@Path("/actor/updateActorFR")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRDTO updateActorFR(@NotNull(message="actorFRDTO may not be null" ) ActorFRDTO actorFRDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getActorRemoteService().updateFRRemote(actorFRDTO, userID);
	}

	// Delete Actor
	/**
	 * Delete actor.
	 *
	 * @param actorID the actor id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/actor/delete")
	public Long deleteActor(@QueryParam("actorID") @NotNull(message="actorID may not be null" ) Long actorID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getActorRemoteService().deleteActorRemote(actorID, userID);
		return actorID;
	}

}
