/*
 * 
 */
package eu.esponder.rest.crisis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.crisis.resource.OCEocDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.security.KeyStorageDTO;
import eu.esponder.rest.ESponderResource;
// TODO: Auto-generated Javadoc
/*
FRTeamResource.
 */

/**
 * The Class FRTeamResource.
 */
@Path("/crisis/context")
public class FRTeamResource extends ESponderResource {

	

	/**
	 * Gets the cheif for team.
	 *
	 * @param frTeamID the fr team id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@GET
	@Path("/Teams/GetChiefForTeam")
	@Produces({ MediaType.APPLICATION_JSON })
	public ActorFRCDTO GetChiefForTeam(
			@QueryParam("frTeamID") @NotNull(message="frTeamID may not be null") Long frTeamID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return (ActorFRCDTO) this.getActorRemoteService().findFRTeamChiefRemote(frTeamID);
	}

	/**
	 * Gets the teams for crisis.
	 *
	 * @param crisisContextID the crisis context id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the list
	 */
	@GET
	@Path("/Teams/GetTeamsForCrisis")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO GetTeamsForCrisis(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextID may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		List<OCMeocDTO> ocList = this.getOperationsCentreRemoteService().findMeocsByCrisisRemote(crisisContextID);
		List<FRTeamDTO> teamsList = new ArrayList<FRTeamDTO>();
		for(OCMeocDTO meoc : ocList) {
			teamsList.addAll(this.getActorRemoteService().findFRTeamsForMeocRemote(meoc.getId()));
		}
		return new ResultListDTO(teamsList);
	}

	/**
	 * Gets the teams for eoc.
	 *
	 * @param eocID the eoc id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the list
	 */
	@GET
	@Path("/Teams/GetTeamsForEoc")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO GetTeamsForEoc(
			@QueryParam("eocID") @NotNull(message="eocID may not be null") Long eocID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		OCEocDTO eoc = (OCEocDTO) this.getOperationsCentreRemoteService().findOperationCentreByIdRemote(eocID);
		return GetTeamsForCrisis(eoc.getEocCrisisContext().getId(), userID, sessionID);
	}

	/**
	 * Gets the team for chief.
	 *
	 * @param frchiefID the frchief id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the fR team dto
	 */
	@GET
	@Path("/Teams/GetTeamForChief")
	@Produces({ MediaType.APPLICATION_JSON })
	public FRTeamDTO GetTeamForChief(
			@QueryParam("frchiefID") @NotNull(message="frchiefID may not be null") Long frchiefID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getActorRemoteService().findFRTeamForChiefRemote(frchiefID);
	}

	/**
	 * Gets the fr us for chief.
	 *
	 * @param frchiefID the frchief id
	 * @param pkiKey the pki key
	 * @return the list
	 */
	
//	FIXME Decide if we keep this cause there is a getActorSubordinates method
//	 inside the ActorViewResource
//	@GET
//	@Path("/Teams/GetFRsForChief")
//	@Consumes({ MediaType.APPLICATION_JSON })
//	@Produces({ MediaType.APPLICATION_JSON })
//	public List<ActorFRDTO> GetFRUsForChief(
//			@QueryParam("frchiefID") @NotNull(message="frchiefID may not be null") Long frchiefID,
//			@QueryParam("sessionDetails") @NotNull(message="SessionDetails may not be null") String sessionDetails) {
//
//	ObjectMapper mapper = new ObjectMapper();
//	KeyStorageDTO sessionInfo = null;
//	try {
//		sessionInfo = mapper.readValue(sessionDetails, KeyStorageDTO.class);
//	} catch (JsonParseException e) {
//		e.printStackTrace();
//	} catch (JsonMappingException e) {
//		e.printStackTrace();
//	} catch (IOException e) {
//		e.printStackTrace();
//	}
//	
//	Long userID = sessionInfo.getUserID();
//		return this.getActorRemoteService().findSubordinatesByIdRemote(frchiefID);
//	}

}
