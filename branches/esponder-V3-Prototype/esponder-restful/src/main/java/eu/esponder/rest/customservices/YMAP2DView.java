/*
 * 
 */
package eu.esponder.rest.customservices;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.crisis.alternative.ActorCMALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorICALTDTO;
import eu.esponder.dto.model.crisis.alternative.FRTeamALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCEocALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO;
import eu.esponder.dto.model.crisis.resource.ActorCMDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamSnapshotDTO;
import eu.esponder.dto.model.crisis.resource.OCEocDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class YMAP2DView.
 */
@Path("/crisis/view")
public class YMAP2DView extends ESponderResource{



	/**
	 * Find2 d details for oc.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the oC eoc altdto
	 */
	@GET
	@Path("/2d/oc")
	@Produces({MediaType.APPLICATION_JSON})
	public OCEocALTDTO find2DDetailsForOC(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		OCEocALTDTO altEoc = new OCEocALTDTO();
		OCEocDTO eoc = new OCEocDTO();

		ESponderUserDTO esponderUser = (ESponderUserDTO) this.getActorRemoteService().findESponderUserByIdRemote(userID);

		Long ocID = esponderUser.getOperationsCentre().getId();
		String ocClass = this.getOperationsCentreRemoteService().findOperationsCentreClassByIdRemote(ocID);
		
		if(ocClass != null || !ocClass.isEmpty()) {

			if(ocClass.equalsIgnoreCase("OCEocDTO")) {
				System.out.println("YMAP2DView.class, It's an EOC");
				try {
					eoc = (OCEocDTO) getOperationsCentreRemoteService().findOCByIdRemote(ocID);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					return null;
				}
			}
			else if(ocClass.equalsIgnoreCase("OCMeocDTO")) {
				System.out.println("YMAP2DView.class, It's a MEOC");
				Long eocID = this.getOperationsCentreRemoteService().findEocIDBYMeocIDRemote(ocID);
				if(eocID != null)
					try {
						eoc = (OCEocDTO) getOperationsCentreRemoteService().findOCByIdRemote(eocID);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
						return null;
					}
			}
			else
				System.out.println("YMAP2DView.class, It's unknown");

			if(eoc != null) {
				System.out.println("YMAP2DView.class, EOC has been FOUND");

				altEoc = copyEocToEocAlt(eoc);
				return altEoc;
			}

		}
		else
			return null;

		return null;
	}



	//----------------------------------------------------------------------------------------
	//Method to initially copy OCEocDTO to OCEocALTDTO object
	/**
	 * Copy eoc to eoc alt.
	 *
	 * @param eoc the eoc
	 * @return the oC eoc altdto
	 */
	private OCEocALTDTO copyEocToEocAlt(OCEocDTO eoc) {
		OCEocALTDTO altEoc = new OCEocALTDTO();
		altEoc.setEocCrisisContext(eoc.getEocCrisisContext().getId());
		ActorCMDTO crisisManager = this.getActorRemoteService().findCrisisManagerByIdRemote(eoc.getCrisisManager());
		if(crisisManager != null)
			altEoc.setCrisisManager(convertCMToCMALT(crisisManager));
		altEoc.setId(eoc.getId());
		altEoc.setSubordinateMEOCs(copyMeocSetToMeocAltSet(eoc.getSubordinateMEOCs()));
		altEoc.setTitle(eoc.getTitle());
		altEoc.setUsers(eoc.getUsers());
		altEoc.setVoIPURL(eoc.getVoIPURL());
		return altEoc;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy meoc set to meoc alt set.
	 *
	 * @param meocSet the meoc set
	 * @return the sets the
	 */
	private Set<OCMeocALTDTO> copyMeocSetToMeocAltSet(Set<Long> meocSet) {

		Set<OCMeocALTDTO> meocAltSet = new HashSet<OCMeocALTDTO>();
		for(Long meocID : meocSet) {
			OCMeocDTO meocDTO = this.getOperationsCentreRemoteService().findMeocByIdRemote(meocID);
			if(meocDTO != null)
				meocAltSet.add(copyMeocToMeocAlt(meocDTO));
		}
		return meocAltSet;

	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy meoc to meoc alt.
	 *
	 * @param meoc the meoc
	 * @return the oC meoc altdto
	 */
	private OCMeocALTDTO copyMeocToMeocAlt(OCMeocDTO meoc) {

		OCMeocALTDTO altMeoc = new OCMeocALTDTO();
		altMeoc.setId(meoc.getId());
		ActorICDTO incidentCommander = this.getActorRemoteService().findIncidentCommanderByIdRemote(meoc.getIncidentCommander());
		if(incidentCommander != null)
			altMeoc.setIncidentCommander(copyICToICAlt(incidentCommander));
		altMeoc.setMeocCrisisContext(meoc.getMeocCrisisContext());
		if(meoc.getSnapshots()!= null ) {
			if(!meoc.getSnapshots().isEmpty() || meoc.getSnapshots().size() == 0) {
				Set<OperationsCentreSnapshotDTO> snapshots = new HashSet<OperationsCentreSnapshotDTO>();
				for(Long snaphotID : meoc.getSnapshots()) {
					snapshots.add(this.getOperationsCentreRemoteService().findOperationsCentreSnapshotByIdRemote(snaphotID));
				}
				altMeoc.setSnapshots(snapshots);
			}
		}
		altMeoc.setSupervisingOC(meoc.getSupervisingOC());
		altMeoc.setTitle(meoc.getTitle());
		altMeoc.setUsers(meoc.getUsers());
		altMeoc.setVoIPURL(meoc.getVoIPURL());
		return altMeoc;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Convert cm to cmalt.
	 *
	 * @param actor the actor
	 * @return the actor cmaltdto
	 */
	private ActorCMALTDTO convertCMToCMALT(ActorCMDTO actor) {
		ActorCMALTDTO altCM = new ActorCMALTDTO();
		altCM.setId(actor.getId());
		altCM.setOperationsCentre(actor.getOperationsCentre());
		altCM.setPersonnel(actor.getPersonnel().getId());
		altCM.setSubordinates(copyICSetToICAltSet(actor.getSubordinates()));
		altCM.setTitle(actor.getTitle());
		altCM.setVoIPURL(actor.getVoIPURL());
		return null;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy ic set to ic alt set.
	 *
	 * @param icSet the ic set
	 * @return the sets the
	 */
	private Set<ActorICALTDTO> copyICSetToICAltSet(Set<Long> icSet) {

		Set<ActorICALTDTO> altICSet = new HashSet<ActorICALTDTO>();
		for(Long icID : icSet) {
			ActorICDTO actorICDTO = this.getActorRemoteService().findIncidentCommanderByIdRemote(icID);
			if(actorICDTO != null)
				altICSet.add(copyICToICAlt(actorICDTO));
		}
		return altICSet;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy ic to ic alt.
	 *
	 * @param ic the ic
	 * @return the actor icaltdto
	 */
	private ActorICALTDTO copyICToICAlt(ActorICDTO ic) {

		ActorICALTDTO altIC = new ActorICALTDTO();
		altIC.setId(ic.getId());
		altIC.setCrisisManager(ic.getCrisisManager());
		altIC.setOperationsCentre(ic.getOperationsCentre());
		altIC.setFrTeams(copyFRTeamSetToFRTeamAltSet(ic.getFrTeams()));
		altIC.setPersonnel(ic.getCrisisManager());
		altIC.setTitle(ic.getTitle());
		altIC.setVoIPURL(ic.getVoIPURL());
		return altIC;
	}


	//----------------------------------------------------------------------------------------

	/**
	 * Copy fr team set to fr team alt set.
	 *
	 * @param teamSet the team set
	 * @return the sets the
	 */
	private Set<FRTeamALTDTO> copyFRTeamSetToFRTeamAltSet(Set<FRTeamDTO> teamSet) {

		Set<FRTeamALTDTO> altTeamSet = new HashSet<FRTeamALTDTO>();
		for(FRTeamDTO team : teamSet) {
			altTeamSet.add(copyFRTeamToFRTeamAlt(team));
		}
		return altTeamSet;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy fr team to fr team alt.
	 *
	 * @param team the team
	 * @return the fR team altdto
	 */
	private FRTeamALTDTO copyFRTeamToFRTeamAlt(FRTeamDTO team) {

		FRTeamALTDTO altTeam = new FRTeamALTDTO();

		altTeam.setId(team.getId());
		altTeam.setIncidentCommander(team.getIncidentCommander());
		altTeam.setSnapshots(convertSetOfTeamSnapshotsToSetOfLongs(team.getSnapshots()));
		ActorFRCDTO chief = this.getActorRemoteService().findFRChiefByIdRemote(team.getFrchief());
		if(chief != null)
			altTeam.setFrchief(copyFRCToFRCAlt(chief));
		return altTeam ;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Convert set of team snapshots to set of longs.
	 *
	 * @param snapshots the snapshots
	 * @return the sets the
	 */
	private Set<Long> convertSetOfTeamSnapshotsToSetOfLongs(Set<FRTeamSnapshotDTO> snapshots) {

		Set<Long> snapshotIDs = new HashSet<Long>();
		for(FRTeamSnapshotDTO snapshot : snapshots)
			snapshotIDs.add(snapshot.getId());
		return snapshotIDs;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy frc to frc alt.
	 *
	 * @param chief the chief
	 * @return the actor frcaltdto
	 */
	private ActorFRCALTDTO copyFRCToFRCAlt(ActorFRCDTO chief) {

		ActorFRCALTDTO chiefAlt = new ActorFRCALTDTO();
		chiefAlt.setId(chief.getId());
		chiefAlt.setEquipmentSet(convertEquipmentSetToLong(chief.getEquipmentSet()));
		chiefAlt.setPersonnel(chief.getPersonnel().getId());
		chiefAlt.setSnapshots(convertFRCSnapshotSetToLongSet(chief.getSnapshots()));
		chiefAlt.setSubordinates(convertSetOfLongToSetOfFRs(chief.getSubordinates()));
		chiefAlt.setTitle(chief.getTitle());
		chiefAlt.setVoIPURL(chief.getVoIPURL());
		return chiefAlt;
	}

	/**
	 * Convert equipment set to long.
	 *
	 * @param equipmentSet the equipment set
	 * @return the sets the
	 */
	private Set<Long> convertEquipmentSetToLong(Set<EquipmentDTO> equipmentSet) {

		Set<Long> longEquipment = new HashSet<Long>();
		for(EquipmentDTO equipment : equipmentSet)
			longEquipment.add(equipment.getId());

		return longEquipment;
	}

	/**
	 * Convert frc snapshot set to long set.
	 *
	 * @param snapshotSet the snapshot set
	 * @return the sets the
	 */
	private Set<Long> convertFRCSnapshotSetToLongSet(Set<ActorSnapshotDTO> snapshotSet) {

		Set<Long> longSnapshots = new HashSet<Long>();
		for(ActorSnapshotDTO snapshot : snapshotSet)
			longSnapshots.add(snapshot.getId());

		return longSnapshots;
	}

	/**
	 * Convert set of long to set of f rs.
	 *
	 * @param frs the frs
	 * @return the sets the
	 */
	private Set<ActorFRALTDTO> convertSetOfLongToSetOfFRs(Set<Long> frs) {
		if(frs != null) {
			ActorFRDTO actor = null;
			Set<ActorFRALTDTO> altSet = new HashSet<ActorFRALTDTO>();
			for(Long frID : frs) {
				actor = this.getActorRemoteService().findFRByIdRemote(frID);
				if(actor != null)
					altSet.add(copyFRToFRAlt(actor));
			}

			return altSet;
		}
		else
			return null;

	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy fr to fr alt.
	 *
	 * @param actorFR the actor fr
	 * @return the actor fraltdto
	 */
	private ActorFRALTDTO copyFRToFRAlt(ActorFRDTO actorFR) {

		ActorFRALTDTO frAlt = new ActorFRALTDTO();
		frAlt.setEquipmentSet(convertEquipmentSetToLong(actorFR.getEquipmentSet()));
		frAlt.setFrchief(actorFR.getFRChief());
		frAlt.setId(actorFR.getId());
		frAlt.setPersonnel(actorFR.getPersonnel().getId());
		frAlt.setSnapshots(convertFRCSnapshotSetToLongSet(actorFR.getSnapshots()));
		frAlt.setTitle(actorFR.getTitle());
		frAlt.setVoIPURL(actorFR.getVoIPURL());
		return frAlt ;
	}

	//----------------------------------------------------------------------------------------


}
