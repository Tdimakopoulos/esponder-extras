/*
 * 
 */
package eu.esponder.rest.datafusion;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlTransient;

import eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService;
import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;
import eu.esponder.rest.ESponderResource;
import eu.esponder.test.ResourceLocator;



// TODO: Auto-generated Javadoc
/**
 * The Class DatafusionRepositoryManager.
 */
@Path("/datafusion/repository")
@XmlTransient
public class DatafusionRepositoryManager extends ESponderResource {

	
	
	/**
	 * Remotetolocal.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the string
	 */
	@GET
	@Path("/remotetolocal")
	public String remotetolocal (
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.ReinitializeRepository();
		

		return szReturnStatus;
	}

	/**
	 * Ruleresults.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the rule results
	 * @throws Exception the exception
	 */
	@GET
	@Path("/ruleresults")
	@Produces({ MediaType.APPLICATION_JSON })
	public RuleResults ruleresults (@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws Exception {
		
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		
		List<RuleResultsXML> pResults=pservice.GetRuleResults();
		return new RuleResults(pResults);
	}
	
	/**
	 * Deleterepo.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the string
	 */
	@GET
	@Path("/deleterepo")
	public String deleterepo (@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.DeleteRepository();
		

		return szReturnStatus;
	}

	/**
	 * Switchtolocal.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the string
	 */
	@GET
	@Path("/switchtolocal")
	public String switchtolocal (@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.SetToLocal();
		

		return szReturnStatus;
	}

	/**
	 * Switchtoguvnor.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the string
	 */
	@GET
	@Path("/switchtoguvnor")
	public String switchtoguvnor (@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.SetToLive();
		

		return szReturnStatus;
	}
}
