/*
 * 
 */
package eu.esponder.rest.events;

import java.io.IOException;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.events.entity.OsgiEventsEntityDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class OsgiEventsView.
 */
@Path("/events/view")
public class OsgiEventsView extends ESponderResource {


	
	/**
	 * Osgi server stop.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the string
	 */
	@POST
	@Path("/server/stop")
	public String OsgiServerStop(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		OsgiSettings pSettings = new OsgiSettings();
		pSettings.LoadSettings();
		String szFilename = pSettings.getSzPropertiesFileName();
		ServerStatus pStatus = new ServerStatus();
		try {
			pStatus.WriteOnFile(pStatus.AppandFileOnPath(pStatus
					.RemoveFilename(szFilename)), "0");
		} catch (IOException e) {
			return "Server cannot be stopped : Error on accessing Server Lock File";
		}
		return "Server will be stopped in the next 5 minutes";
	}

	/**
	 * Osgi server start.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the string
	 */
	@POST
	@Path("/server/start")
	public String OsgiServerStart(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		OsgiSettings pSettings = new OsgiSettings();
		pSettings.LoadSettings();
		String szFilename = pSettings.getSzPropertiesFileName();
		ServerStatus pStatus = new ServerStatus();
		try {
			pStatus.WriteOnFile(pStatus.AppandFileOnPath(pStatus
					.RemoveFilename(szFilename)), "1");
		} catch (IOException e) {
			return "Server cannot be started : Error on accessing Server Lock File";
		}
		return "Server will be started in the next 5 minutes";
	}
	
	//FindByID
	/**
	 * Gets the osgi events entity by id.
	 *
	 * @param osgiEventsEntityDTOID the osgi events entity dtoid
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the osgi events entity by id
	 */
	@GET
	@Path("/osgievents/getByID")
	@Produces({MediaType.APPLICATION_JSON})
	public OsgiEventsEntityDTO getOsgiEventsEntityById(
			@QueryParam("osgiEventsEntityDTOID") @NotNull(message="osgiEventsEntityDTOID may not be null" ) Long osgiEventsEntityDTOID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOsgiEventsEntityRemoteService().findOsgiEventsEntityByIdRemote(osgiEventsEntityDTOID);
	}

	//FindBySeverity
	/**
	 * Gets the osgi events entity by severity.
	 *
	 * @param osgiEventsEntitySeverity the osgi events entity severity
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the osgi events entity by severity
	 */
	@GET
	@Path("/osgievents/getBySeverity")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getOsgiEventsEntityBySeverity(
			@QueryParam("osgiEventsEntitySeverity") @NotNull(message="osgiEventsEntitySeverity may not be null" ) String osgiEventsEntitySeverity,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		List<OsgiEventsEntityDTO> pResults=this.getOsgiEventsEntityRemoteService().findOsgiEventsEntitiesBySeverityRemote(osgiEventsEntitySeverity);
		ResultListDTO pReturn= new ResultListDTO();
		pReturn.setResultList(pResults);
		return pReturn;
	}

	//FindBySourceId
	/**
	 * Gets the osgi events entity by sourceid.
	 *
	 * @param osgiEventsEntitySourceid the osgi events entity sourceid
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the osgi events entity by sourceid
	 */
	@GET
	@Path("/osgievents/getBySourceId")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getOsgiEventsEntityBySourceid(
			@QueryParam("osgiEventsEntitySourceid") @NotNull(message="osgiEventsEntitySourceid may not be null" ) Long osgiEventsEntitySourceid,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return new ResultListDTO(this.getOsgiEventsEntityRemoteService().findOsgiEventsEntitiesBySourceIdRemote(osgiEventsEntitySourceid));
	}

	// create OsgiEventsEntity
	/**
	 * Creates the osgi events entity.
	 *
	 * @param osgiEventsEntityDTO the osgi events entity dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the osgi events entity dto
	 */
	@POST
	@Path("/osgievents/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public OsgiEventsEntityDTO createOsgiEventsEntity(@NotNull(message="osgiEventsEntityDTO may not be null" ) OsgiEventsEntityDTO osgiEventsEntityDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOsgiEventsEntityRemoteService().createOsgiEventsEntityRemote(osgiEventsEntityDTO, userID);
	}


	// Update OsgiEventsEntity
	/**
	 * Update osgi events entity.
	 *
	 * @param osgiEventsEntityDTO the osgi events entity dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the osgi events entity dto
	 */
	@PUT
	@Path("/osgievents/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public OsgiEventsEntityDTO updateOsgiEventsEntity(@NotNull(message="osgiEventsEntityDTO may not be null" ) OsgiEventsEntityDTO osgiEventsEntityDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOsgiEventsEntityRemoteService().updateOsgiEventsEntityRemote(osgiEventsEntityDTO, userID);
	}

	// Delete OsgiEventsEntity
	/**
	 * Delete osgi events entity dto.
	 *
	 * @param osgiEventsEntityDTOID the osgi events entity dtoid
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/osgievents/delete")
	public Long deleteOsgiEventsEntityDTO(
			@QueryParam("osgiEventsEntityDTOID") @NotNull(message="osgiEventsEntityDTOID may not be null" ) Long osgiEventsEntityDTOID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getOsgiEventsEntityRemoteService().deleteOsgiEventsEntityRemote(osgiEventsEntityDTOID, userID);
		return osgiEventsEntityDTOID;
	}

}
