package eu.esponder.EventAdminTest;

import java.math.BigDecimal;
import java.util.Date;

import org.testng.annotations.Test;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.dto.osgi.service.event.ESponderEventPublisher;
import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.test.ResourceLocator;

public class CreateCrisisContextEventToEventAdminTest {
	

	CrisisRemoteService crisisService = ResourceLocator.lookup("esponder/CrisisBean/remote");
	GenericRemoteService genericService = ResourceLocator.lookup("esponder/GenericBean/remote");
	UserRemoteService userService = ResourceLocator.lookup("esponder/UserBean/remote");
	
	Long userID = null;
	
	@Test
	public void CreateCrisisContext() throws ClassNotFoundException, EventListenerException {
		
		ESponderUserDTO userDTO = userService.findUserByNameRemote("ctri");
		if (null != userDTO) {
			this.userID = userDTO.getId();
			
			ActorDTO subactorDTO = new ActorDTO();
		    subactorDTO.setId(new Long(2));
		    subactorDTO.setType("FRC 2222");
		    subactorDTO.setTitle("FRC #2222");
		    subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		    
			PointDTO pointDTO = new PointDTO(new BigDecimal(38.025334), new BigDecimal(23.802717), new BigDecimal(1.000000));
			SphereDTO crisisLocation = new SphereDTO(pointDTO, new BigDecimal(2), "CrisisLocation for EventAdmin Test");
			crisisLocation = (SphereDTO) this.genericService.createEntityRemote(crisisLocation, this.userID);
			
			CrisisContextDTO crisisContextDTO = new CrisisContextDTO();
			crisisContextDTO.setTitle("CrisisContext Title for EventAdmin Test");
			crisisContextDTO.setCrisisLocation(crisisLocation);
			CrisisContextDTO crisisContextPersisted = crisisService.createCrisisContextRemote(crisisContextDTO, this.userID);
			
			CreateCrisisContextEvent crisisContextEvent = new CreateCrisisContextEvent();
			crisisContextEvent.setEventAttachment(crisisContextPersisted);
			crisisContextEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
			crisisContextEvent.setEventTimestamp(new Date());
			crisisContextEvent.setJournalMessage("New Crisis Context has been created");
			crisisContextEvent.setEventSource(subactorDTO);
			
			ESponderEventPublisher<CreateCrisisContextEvent> publisher = new ESponderEventPublisher<CreateCrisisContextEvent>(CreateCrisisContextEvent.class);
			publisher.publishEvent(crisisContextEvent);
			
		}
		else
			System.out.println("Selected user has not been found, cannot proceed...");
	}
	
	
//	protected ActionRemoteService getActionService() {
//		try {
//			return ServiceLocator.getResource("esponder/ActionBean/remote");
//		} catch (NamingException e) {
//			throw new RuntimeException(e);
//		}
//	}
//	
//	protected GenericRemoteService getGenericService() {
//		try {
//			return ServiceLocator.getResource("esponder/GenericBean/remote");
//		} catch (NamingException e) {
//			throw new RuntimeException(e);
//		}
//	}
//	
//	protected UserRemoteService getUserService() {
//		try {
//			return ServiceLocator.getResource("esponder/UserBean/remote");
//		} catch (NamingException e) {
//			throw new RuntimeException(e);
//		}
//	}

}
