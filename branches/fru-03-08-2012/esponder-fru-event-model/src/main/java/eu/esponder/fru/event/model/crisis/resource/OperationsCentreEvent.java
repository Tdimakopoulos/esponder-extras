package eu.esponder.fru.event.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.OperationsCentreDTO;


public abstract class OperationsCentreEvent<T extends OperationsCentreDTO> extends ResourceEvent<T> {

	private static final long serialVersionUID = 4415380713125894915L;

}
