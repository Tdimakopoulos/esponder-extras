package eu.esponder.fru.event.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.fru.event.model.UpdateEvent;


public class UpdateConsumableResourceEvent extends ConsumableResourceEvent<ConsumableResourceDTO> implements UpdateEvent {

	private static final long serialVersionUID = -7576724843419044712L;

}
