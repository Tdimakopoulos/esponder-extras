package eu.esponder.fru.event.model;

import eu.esponder.fru.dto.model.ESponderEntityDTO;


public abstract class ESponderTypeEvent<T extends ESponderEntityDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = -9036535870972195826L;
	
}

