package eu.esponder.fru.event.model.snapshot.action;

import eu.esponder.fru.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.fru.event.model.snapshot.SnapshotEvent;


public abstract class ActionSnapshotEvent<T extends ActionSnapshotDTO> extends SnapshotEvent<T> {

	private static final long serialVersionUID = -3799871246977498985L;

}
