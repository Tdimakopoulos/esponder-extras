package eu.esponder.fru.event.model.snapshot.measurement;

import eu.esponder.fru.dto.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.fru.event.model.CreateEvent;

public class CreateSensorMeasurementEnvelopeEvent extends SensorMeasurementEnvelopeEvent<SensorMeasurementEnvelopeDTO> implements CreateEvent {

	private static final long serialVersionUID = -722332256961688586L;

	@Override
	public String getJournalMessageInfo() {
		String journalMessageInfo = "Received Sensor Measurement Envelope including: \n";
		return journalMessageInfo;
	}
}
