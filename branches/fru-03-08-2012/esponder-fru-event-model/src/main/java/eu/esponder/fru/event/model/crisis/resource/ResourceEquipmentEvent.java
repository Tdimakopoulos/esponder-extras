package eu.esponder.fru.event.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.fru.event.model.ESponderEvent;

/**
 * TODO: check the generic type of the attachment for this type of events 
 *
 */
public abstract class ResourceEquipmentEvent<T extends EquipmentDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = 1970943470682183275L;

}
