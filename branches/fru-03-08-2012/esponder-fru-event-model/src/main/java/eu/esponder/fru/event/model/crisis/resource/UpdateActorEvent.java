package eu.esponder.fru.event.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import eu.esponder.fru.event.model.UpdateEvent;


public class UpdateActorEvent extends ActorEvent<ActorDTO> implements UpdateEvent {

	private static final long serialVersionUID = -9062099773119274785L;
	
}
