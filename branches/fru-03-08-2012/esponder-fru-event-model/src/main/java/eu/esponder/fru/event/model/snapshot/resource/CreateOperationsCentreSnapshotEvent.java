package eu.esponder.fru.event.model.snapshot.resource;

import eu.esponder.fru.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.fru.event.model.CreateEvent;


public class CreateOperationsCentreSnapshotEvent extends OperationsCentreSnapshotEvent<OperationsCentreSnapshotDTO> implements CreateEvent {

	private static final long serialVersionUID = 1150492225030805266L;

}
