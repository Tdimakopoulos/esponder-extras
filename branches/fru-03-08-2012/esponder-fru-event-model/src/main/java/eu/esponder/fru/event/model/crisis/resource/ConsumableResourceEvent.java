package eu.esponder.fru.event.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.ConsumableResourceDTO;


public abstract class ConsumableResourceEvent<T extends ConsumableResourceDTO> extends ResourceEvent<T> {

	private static final long serialVersionUID = -4847055252359575844L;

}
