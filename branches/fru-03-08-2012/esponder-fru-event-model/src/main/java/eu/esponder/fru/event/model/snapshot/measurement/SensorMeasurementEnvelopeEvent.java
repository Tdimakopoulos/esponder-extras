package eu.esponder.fru.event.model.snapshot.measurement;

import eu.esponder.fru.dto.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.fru.event.model.ESponderEvent;

public abstract class SensorMeasurementEnvelopeEvent<T extends SensorMeasurementEnvelopeDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = -8470374826798188512L;

}
