package eu.esponder.fru.event.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.fru.event.model.DeleteEvent;


public class DeleteConsumableResourceEvent extends ConsumableResourceEvent<ConsumableResourceDTO> implements DeleteEvent {

	private static final long serialVersionUID = 1886755992124247660L;

}
