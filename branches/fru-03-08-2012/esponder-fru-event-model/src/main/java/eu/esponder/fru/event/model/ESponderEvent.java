package eu.esponder.fru.event.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.codehaus.jackson.annotate.JsonIgnore;
import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.fru.dto.model.crisis.resource.ResourceDTO;

//@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class ESponderEvent<T extends ESponderEntityDTO> implements Serializable {

	private static final long serialVersionUID = 3165213086735540677L;
	
	//private static SimpleDateFormat formatter = new SimpleDateFormat ("yyyy.MM.dd ww 'at' hh:mm:ss a zzz");
	private static SimpleDateFormat formatter = new SimpleDateFormat ("yyyy.MM.dd kk:mm:ss");
	
	private ResourceDTO eventSource;
	
	private T eventAttachment;
	
	private Date eventTimestamp;
	
	private SeverityLevelDTO eventSeverity;
	
	private String journalMessage;
	
//	public ESponderEvent() {
//		this.journalMessage = 
//				"TYPE: " + this.getClass().getSimpleName() + "\n" +
//				"TIME: " + getEventTimestampStr() + "\n" + 
//				"SOURCE: " + getEventSourceStr() + "\n" +
//				"SEVERITY: " + getEventSeverity().toString() + "\n" + 
//				"INFO: " + getJournalMessageInfo();
//	}
	
	@JsonIgnore
	public String getEventSourceStr() {
		return this.getEventSource().getResourceId();
	}
	@JsonIgnore
	public abstract String getJournalMessageInfo();
	
	private String getEventTimestampStr() {
		return formatter.format(eventTimestamp);
	}
	
	public T getEventAttachment() {
		return eventAttachment;
	}

	public ResourceDTO getEventSource() {
		return eventSource;
	}

	public void setEventSource(ResourceDTO eventSource) {
		this.eventSource = eventSource;
	}

	public void setEventAttachment(T eventAttachment) {
		this.eventAttachment = eventAttachment;
	}

	public Date getEventTimestamp() {
		return eventTimestamp;
	}

	public void setEventTimestamp(Date eventTimestamp) {
		this.eventTimestamp = eventTimestamp;
	}

	public SeverityLevelDTO getEventSeverity() {
		return eventSeverity;
	}

	public void setEventSeverity(SeverityLevelDTO eventSeverity) {
		this.eventSeverity = eventSeverity;
	}

	public String getJournalMessage() {
		this.journalMessage = 
				//"TYPE: " + this.getClass().getSimpleName() + "\n" +
				"TIME: " + getEventTimestampStr() + "\n" + 
				"SOURCE: " + getEventSourceStr() + "\n" +
				"SEVERITY: " + getEventSeverity().toString(); /*+ "\n" + 
				"INFO: " + this.getJournalMessageInfo();*/
		return journalMessage;
	}

	public void setJournalMessage(String journalMessage) {
		this.journalMessage = journalMessage;
	}
	
}

