package eu.esponder.fru.event.model.snapshot.resource;

import eu.esponder.fru.dto.model.snapshot.SnapshotDTO;


public abstract class ConsumableResourceSnapshotEvent<T extends SnapshotDTO> extends ResourceSnapshotEvent<T> {

	private static final long serialVersionUID = 2544414366303024642L;

}
