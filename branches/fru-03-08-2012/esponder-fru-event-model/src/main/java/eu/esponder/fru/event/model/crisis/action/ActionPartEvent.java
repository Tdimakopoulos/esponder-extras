package eu.esponder.fru.event.model.crisis.action;

import eu.esponder.fru.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.fru.event.model.ESponderEvent;


public abstract class ActionPartEvent<T extends ActionPartDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = -9001909010882055239L;
	
}
