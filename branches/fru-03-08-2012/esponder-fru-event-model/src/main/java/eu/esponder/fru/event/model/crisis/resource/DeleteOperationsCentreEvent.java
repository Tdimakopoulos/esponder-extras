package eu.esponder.fru.event.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.fru.event.model.DeleteEvent;


public class DeleteOperationsCentreEvent extends OperationsCentreEvent<OperationsCentreDTO> implements DeleteEvent {

	private static final long serialVersionUID = 2788080828499796379L;

}
