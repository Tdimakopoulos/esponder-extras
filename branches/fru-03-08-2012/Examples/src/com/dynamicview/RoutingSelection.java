package com.dynamicview;

import java.io.IOException;
import java.util.List;

import com.dynamicview.SelectStreet.MyArrayAdapter;
import com.google.android.maps.GeoPoint;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class RoutingSelection extends Activity {

	static GeoPoint GeoPointFrom, GeoPointTo,GeoPointCurrent;
	final static int MAX_RESULT = 10;
	final static String DEFAULT_SEARCH = "";
	ListView listviewResult,listviewResultTo;
	Button searchbutton,searchbuttonTo,goRouting; 
	EditText addressText,addressTextTo;
	Geocoder myGeocoder;
	static String streetName;
	List<Address> addresses = null;
	List<Address> result;
	MyArrayAdapter adapterFrom,adapterTo;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 Intent pointIntent = getIntent();
	          if (pointIntent != null)
	        {
	        	Bundle b = pointIntent.getBundleExtra("currentpos");
	        	if (b != null){
	        		GeoPointCurrent = new GeoPoint(b.getInt("Latitude"),  b.getInt("Longitude"));
	         }}
	          
		loadMenu();
		myGeocoder = new Geocoder(this);
	}

	private void showDialogFrom() {
		final CharSequence[] options = { "Current", "Map", "Text" };

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Address From");

		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int pos) {
				if (pos == 0) {// Option 1
					addressText.setText(posToaddress(GeoPointCurrent));
					GeoPointFrom=GeoPointCurrent;
				} else if (pos == 1) {// Option 2
					Intent intent = new Intent(RoutingSelection.this,
							SelectOnMap.class);
					Bundle bundle = new Bundle();
					bundle.putInt("Mode", 0);
					bundle.putInt("Latitude", GeoPointCurrent.getLatitudeE6());
					bundle.putInt("Longitude", GeoPointCurrent.getLongitudeE6());
					intent.putExtras(bundle);
					startActivityForResult(intent, 0);
				} else if (pos == 2) { // Option 2
					String searchString = addressText.getText().toString();
					searchFromLocationName(searchString,listviewResult,adapterFrom);
				}

			}
		});

		AlertDialog dlg = builder.create();
		dlg.show();
	}

	private void showDialogTo() {
		final CharSequence[] options = { "Map", "Text" };

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		if(GeoPointFrom!=null){
		builder.setTitle("Address To");
    	builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int pos) {
			   if (pos == 0) {// Option 2
					Intent intent = new Intent(RoutingSelection.this,
							SelectOnMap.class);
					Bundle bundle = new Bundle();
					bundle.putInt("Mode", 1);
					bundle.putInt("Latitude", GeoPointFrom.getLatitudeE6());
					bundle.putInt("Longitude", GeoPointFrom.getLongitudeE6());
					intent.putExtras(bundle);
					startActivityForResult(intent, 0);
				} else if (pos == 1) { // Option 2
					String searchString = addressTextTo.getText().toString();
					searchFromLocationName(searchString,listviewResultTo,adapterTo);
				}

			}
		});
		}else{
			builder.setMessage("Please Select Firstly the From Destination");
			builder.setPositiveButton("Ok", null);
		}
		AlertDialog dlg = builder.create();
		dlg.show();
	}
	
	private void loadMenu() {
		setContentView(R.layout.main3);
	    searchbutton = (Button) findViewById(R.id.searchbutton);
		addressText = (EditText) findViewById(R.id.addresstext);
		listviewResult = (ListView) findViewById(R.id.listviewresult);
		
		 searchbuttonTo = (Button) findViewById(R.id.searchbuttonTo);
		 addressTextTo = (EditText) findViewById(R.id.addresstextTo);
		 listviewResultTo = (ListView) findViewById(R.id.listviewresultTo);
		 goRouting = (Button) findViewById(R.id.goRouting);
		 

		searchbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				showDialogFrom();
			}

		});
		 
		listviewResult.setOnItemClickListener(new OnItemClickListener() {
	
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub

				double lat = ((Address) parent.getItemAtPosition(position))
						.getLatitude();
				double lon = ((Address) parent.getItemAtPosition(position))
						.getLongitude();
				String loc = "lat: " + lat + "\n" + "lon: " + lon;
				GeoPointFrom = new GeoPoint((int) (lat*1E6), (int) (lon*1E6));
			//	Toast.makeText(RoutingSelection.this, loc,Toast.LENGTH_LONG).show();
				Address ad=(Address) parent.getItemAtPosition(position);
				addressText.setText("");
				addressText.setText( ad.getAddressLine(0)+" "+ad.getAddressLine(1)+" "+ad.getAddressLine(2));
				result.clear();
				((ArrayAdapter<Address>) listviewResult.getAdapter()).notifyDataSetChanged();
			}

		});	
		
		searchbuttonTo.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				showDialogTo();
			}

		});
		
		
		listviewResultTo.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub

				double lat = ((Address) parent.getItemAtPosition(position))
						.getLatitude();
				double lon = ((Address) parent.getItemAtPosition(position))
						.getLongitude();
				String loc = "lat: " + lat + "\n" + "lon: " + lon;
				GeoPointTo = new GeoPoint((int) (lat*1E6), (int) (lon*1E6));
			//	Toast.makeText(RoutingSelection.this, loc,Toast.LENGTH_LONG).show();
				Address ad=(Address) parent.getItemAtPosition(position);
				addressTextTo.setText("");
				addressTextTo.setText( ad.getAddressLine(0)+" "+ad.getAddressLine(1)+" "+ad.getAddressLine(2));
				result.clear();
				((ArrayAdapter<Address>) listviewResultTo.getAdapter()).notifyDataSetChanged();
			}

		});	
		
		goRouting.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				if(GeoPointFrom==null || GeoPointTo==null){
					AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
					builder.setMessage("Your Destinations are not Valid.Please try again");
					builder.setPositiveButton("Ok", null);
					AlertDialog dlg = builder.create();
					dlg.show();
				}
				else{
				Intent routing = new Intent(RoutingSelection.this, Routing.class);
				Bundle bundle = new Bundle(); 
				bundle.putInt("FromLat", GeoPointFrom.getLatitudeE6());    
				bundle.putInt("FromLong", GeoPointFrom.getLongitudeE6());
				bundle.putInt("ToLat", GeoPointTo.getLatitudeE6());
				bundle.putInt("ToLong", GeoPointTo.getLongitudeE6());
				routing.putExtra("routing", bundle);
				startActivity(routing);
			}}

		});
	}

	private String posToaddress(GeoPoint Point) {
		String address="";
		try {
			if(Point!=null){
			addresses = myGeocoder.getFromLocation(
					(double) Point.getLatitudeE6() / 1E6,
					(double) Point.getLongitudeE6() / 1E6, 1);
		  	address = addresses.get(0).getAddressLine(0) + " "
					+ addresses.get(0).getAddressLine(1) + " "
					+ addresses.get(0).getAddressLine(2);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 

		return address;

	}

 	private void searchFromLocationName(String name,ListView listResult,MyArrayAdapter adapter) {
		try {
			  result = myGeocoder.getFromLocationName(name,
					MAX_RESULT);

			if ((result == null) || (result.isEmpty())) {
				Toast.makeText(
						RoutingSelection.this,
						"No matches were found or there is no backend service!",
						Toast.LENGTH_LONG).show();
			} else {

				  adapter = new MyArrayAdapter(this,
						android.R.layout.simple_list_item_1, result);
				listResult.setAdapter(adapter);

				Toast.makeText(RoutingSelection.this, "Finished Search",
						Toast.LENGTH_LONG).show();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(
					RoutingSelection.this,
					"The network is unavailable or any other I/O problem occurs!",
					Toast.LENGTH_LONG).show();
		}
	}

	public class MyArrayAdapter extends ArrayAdapter<Address> {
		Context mycontext;

		public MyArrayAdapter(Context context, int textViewResourceId,
				List<Address> objects) {
			super(context, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
			mycontext = context;
		}

	@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			int maxAddressLineIndex = getItem(position)
					.getMaxAddressLineIndex();
			String addressLine = "";

			for (int j = 0; j <= maxAddressLineIndex; j++) {
				addressLine += getItem(position).getAddressLine(j) + ",";
			}

			TextView rowAddress = new TextView(mycontext);
			rowAddress.setText(addressLine);

			return rowAddress;

		}

	}

	@Override
	public void onRestart() {
		super.onRestart();
		addressText.setText(posToaddress(GeoPointFrom));  
		addressTextTo.setText(posToaddress(GeoPointTo));
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	    	GeoPointFrom=null;
	    	finish();
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
}


/*
private void OpenIntentAndroidMapView(GeoPoint startLocation) {
	Intent intent = new Intent();
	intent.setClass(RoutingSelection.this, Map.class);

	Bundle bundle = new Bundle();

	if (startLocation == null) {
		bundle.putInt("Mode", 0);
	} else {
		bundle.putInt("Mode", 1);
		bundle.putInt("Longitude", startLocation.getLongitudeE6());
		bundle.putInt("Latitude", startLocation.getLatitudeE6());
	}

	intent.putExtras(bundle);
	startActivityForResult(intent, 0);
}

private void OpenMissingOptionDialog() {
	new AlertDialog.Builder(this).setTitle("missing selection")
			.setMessage("Please select one of the option")
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialoginterface, int i) {
				}
			}).show();
}*/