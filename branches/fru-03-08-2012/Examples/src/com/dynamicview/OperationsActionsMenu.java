package com.dynamicview;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import eu.esponder.fru.dto.model.crisis.action.ActionDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import eu.esponder.fru.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.fru.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.fru.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.fru.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.fru.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;

public class OperationsActionsMenu extends Activity {

 
	private ListView actorListView;
	private List<ActionDTO> actionsList;
	private OperationActionsMenuListAdapter adapterList;
	private Iterator iteratorSubordinates;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operationsmenu);
		actorListView = (ListView) findViewById(R.id.MessageListView);
		actionsList = new ArrayList<ActionDTO>();
		adapterList = new OperationActionsMenuListAdapter(this); 
		actorListView.setAdapter(adapterList);
		


// <----------------- Start Action ------------------> 
 	  	
	   	ActorDTO subactorDTO = new ActorDTO();
	   	subactorDTO.setId(new Long(4));
	   	subactorDTO.setType("FRC 4444");
	   	subactorDTO.setTitle("FRC #4444");
	   	subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
	   	
		ActorDTO subactorDTO2= new ActorDTO();
		subactorDTO2.setId(new Long(2));
		subactorDTO2.setType("FRC 2222");
		subactorDTO2.setTitle("FRC #2222");
		subactorDTO2.setStatus(ResourceStatusDTO.AVAILABLE);
		
		  ActionDTO action = new ActionDTO();  
	      action.setTitle("General No1");
	      action.setId(new Long(4));
	       
	      
	 	  ReusableResourceDTO reusableResource1 = new ReusableResourceDTO();
	 	  reusableResource1.setType("truck");
	 	  HashSet  reusableResourceSet = new HashSet();
	 	  reusableResourceSet.add(reusableResource1);
	 	  
	 	  ReusableResourceDTO reusableResource2 = new ReusableResourceDTO();
	 	  reusableResource2.setType("red tools");
	 	  HashSet  reusableResourceSet2 = new HashSet();
	 	  reusableResourceSet2.add(reusableResource2);
	 	  
	 	  ConsumableResourceDTO  ConsumableResource1 = new ConsumableResourceDTO();
	 	  ConsumableResource1.setTitle("food packages");
	 	  ConsumableResource1.setQuantity(new BigDecimal(10));
	      HashSet consumableResourceSet = new HashSet(); 
	      consumableResourceSet.add(ConsumableResource1);
	      
	      ConsumableResourceDTO  ConsumableResource2 = new ConsumableResourceDTO();
	      ConsumableResource2.setTitle("telecom lines"); 
	      HashSet consumableResourceSet2 = new HashSet(); 
	      consumableResourceSet2.add(ConsumableResource2);

	 
	      ActionPartObjectiveDTO actionPartObjective1 = new ActionPartObjectiveDTO();
	 	  actionPartObjective1.setConsumableResources(consumableResourceSet);
	 	  HashSet actionPartObjectiveSet = new HashSet(); 
	 	  actionPartObjectiveSet.add(actionPartObjective1);
	 	  
	      ActionPartObjectiveDTO actionPartObjective2 = new ActionPartObjectiveDTO();
	 	  actionPartObjective2.setConsumableResources(consumableResourceSet2);
	 	  HashSet actionPartObjectiveSet2 = new HashSet(); 
	 	  actionPartObjectiveSet2.add(actionPartObjective2);
	 	  	
	      // Action No1
	 	  ActionPartDTO actoinPart1= new ActionPartDTO();
	 	  actoinPart1.setUsedReusuableResources(reusableResourceSet);
	  	  actoinPart1.setUsedConsumableResources(consumableResourceSet);
	  	  actoinPart1.setActionPartObjectives(actionPartObjectiveSet);
	  	  actoinPart1.setActionOperation(ActionOperationEnumDTO.MOVE);
	      ActionPartSnapshotDTO actionsnap1 = new ActionPartSnapshotDTO();
	      actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	      actoinPart1.setSnapshot(actionsnap1);
	      actoinPart1.setId(new Long(1));
	 //     actoinPart1.setAction(action);  
	      actoinPart1.setActor(subactorDTO);
	      
	      // Action No2
	 		  ActionPartDTO actoinPart2= new ActionPartDTO();
	 		  actoinPart2.setId(new Long(2));
	 		  actoinPart2.setUsedReusuableResources(reusableResourceSet2);
	 	  	  actoinPart2.setUsedConsumableResources(consumableResourceSet2);
	 	  	  actoinPart2.setActionPartObjectives(actionPartObjectiveSet2);
	 	  	  actoinPart2.setActionOperation(ActionOperationEnumDTO.FIX);
	 	      ActionPartSnapshotDTO actionsnap2 = new ActionPartSnapshotDTO();
	 	      actionsnap2.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	 	      actoinPart2.setSnapshot(actionsnap1); 
	      
	   // Action No3
	 	  ActionPartDTO actoinPart3= new ActionPartDTO();
	 	 actoinPart3.setUsedReusuableResources(reusableResourceSet);
	 	actoinPart3.setUsedConsumableResources(consumableResourceSet);
	 	actoinPart3.setActionPartObjectives(actionPartObjectiveSet);
	 	actoinPart3.setActionOperation(ActionOperationEnumDTO.MOVE);
	        actionsnap1 = new ActionPartSnapshotDTO();
	      actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	      actoinPart3.setSnapshot(actionsnap1);
	      actoinPart3.setId(new Long(3));
	   //   actoinPart3.setAction(action);
	      actoinPart3.setActor(subactorDTO);
	      
	   // Action No4
	 	  ActionPartDTO actoinPart4= new ActionPartDTO();
	 	 actoinPart4.setUsedReusuableResources(reusableResourceSet);
	 	actoinPart4.setUsedConsumableResources(consumableResourceSet);
	 	actoinPart4.setActionPartObjectives(actionPartObjectiveSet);
	 	actoinPart4.setActionOperation(ActionOperationEnumDTO.MOVE);
	       actionsnap1 = new ActionPartSnapshotDTO();
	       actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	      actoinPart4.setSnapshot(actionsnap1);
	      actoinPart4.setId(new Long(4)); 
	       
	   // Action No4
	 	  ActionPartDTO actoinPart5= new ActionPartDTO();
	 	 actoinPart5.setUsedReusuableResources(reusableResourceSet);
	 	actoinPart5.setUsedConsumableResources(consumableResourceSet);
	 	actoinPart5.setActionPartObjectives(actionPartObjectiveSet);
	 	actoinPart5.setActionOperation(ActionOperationEnumDTO.MOVE);
	       actionsnap1 = new ActionPartSnapshotDTO();
	       actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	       actoinPart5.setSnapshot(actionsnap1);
	       actoinPart5.setId(new Long(5));
	   //    actoinPart5.setAction(action);
	       actoinPart5.setActor(subactorDTO2);
	       
		      // Action No3
		 	  ActionPartDTO actoinPart6= new ActionPartDTO();
		 	 actoinPart6.setUsedReusuableResources(reusableResourceSet);
		 	actoinPart6.setUsedConsumableResources(consumableResourceSet);
		 	actoinPart6.setActionPartObjectives(actionPartObjectiveSet);
		 	actoinPart6.setActionOperation(ActionOperationEnumDTO.MOVE);
		        actionsnap1 = new ActionPartSnapshotDTO();
		      actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
		      actoinPart6.setSnapshot(actionsnap1);
		      actoinPart6.setId(new Long(6)); 
	  	  

	      HashSet<ActionPartDTO> actionParts = new HashSet<ActionPartDTO>();
	      actionParts.add(actoinPart1);
	       actionParts.add(actoinPart3);
         actionParts.add(actoinPart5);
       action.setActionParts(actionParts); 
	 	  	
	 	 
	     	// <----------------- End Action ------------------> 

	    	  actionsList.add(action);
	       
		  
		  ShowOrder((List<ActionDTO>) actionsList);
	}

	public void ShowOrder(final List<ActionDTO> actor) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.addOrder(actor);
				adapterList.notifyDataSetChanged();
			}
		});
	}

	public void ClearOrder() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.clear();
				adapterList.notifyDataSetChanged();
			}
		});
	}
	
}