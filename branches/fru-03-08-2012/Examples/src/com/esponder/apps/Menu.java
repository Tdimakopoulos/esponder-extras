package com.esponder.apps;
 

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import eu.esponder.fru.JSONParser;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import eu.esponder.fru.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.jsonrcp.JSONRPCClient;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Menu extends Activity {

	public static JSONRPCClient  client;
	JSONParser jpa;
	static ActorDTO actor,actorTemp;
	static Map< Long , String > actorSensorMap;
	Iterator iteratorEquipment,iteratorSensors,iteratorSubordinates;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
		client = JSONRPCClient.create("http://localhost:8080/remote/json-rpc");
		 jpa = new JSONParser();
		 actor = jpa.getActor();
		 
		 actorSensorMap = new HashMap <Long,String>();
		 iteratorEquipment = actor.getEquipmentSet().iterator();  
		 iteratorSensors = ((EquipmentDTO)iteratorEquipment.next()).getSensors().iterator();
	 	 while(iteratorSensors.hasNext()){
	 		actorSensorMap.put(((SensorDTO)iteratorSensors.next()).getId(), actor.getTitle());
		 }
	 	 
	 	if(actor.getSubordinates()!=null){
	 		iteratorSubordinates =  actor.getSubordinates().iterator(); 
	 		  while(iteratorSubordinates.hasNext()){ 
	 			 actorTemp =(ActorDTO)iteratorSubordinates.next();
	  			  iteratorEquipment = actorTemp.getEquipmentSet().iterator();
	  			  iteratorSensors = ((EquipmentDTO)iteratorEquipment.next()).getSensors().iterator();
	  			 while(iteratorSensors.hasNext()){
	  		 		actorSensorMap.put(((SensorDTO)iteratorSensors.next()).getId(), actorTemp.getTitle());
	  			 }
	 		  }
	 	}
	 	 
	 	Set set = actorSensorMap.entrySet(); 
	 // Get an iterator 
	 Iterator i = set.iterator(); 
	 // Display elements 
	 while(i.hasNext()) { 
	 Map.Entry me = (Map.Entry)i.next(); 
	 System.out.print(me.getKey() + ": "); 
	 System.out.println(me.getValue()); 
	 } 
	 	
	}

	// Click on the Measurements button of Main
	public void clickHandler(View view) {
		switch (view.getId()) {
		case R.id.Measurements: // Measurements
			Intent openMeasurements= new Intent(Menu.this, MeasurementsMenu.class);
			startActivity(openMeasurements);
			break;
		case R.id.Map: // Maps
			Intent openMap = new Intent(Menu.this, Maps.class);
			startActivity(openMap);
			break;
		case R.id.Journal: // Journal
			Intent openJournal = new Intent(Menu.this, Journal.class);
			startActivity(openJournal);
			break;
		case R.id.Operations: // Operations
			if(actor.getSubordinates()!=null){
				Intent openOperationsActorMenu = new Intent(Menu.this, OperationsMenu.class);
				startActivity(openOperationsActorMenu);
			}else{
			    Bundle info = new Bundle();
		        info.putLong("actorId",actor.getId());
		        info.putString("actorName", actor.getTitle());  
				Intent openOperations = new Intent(Menu.this, OperationsByActor.class);
				openOperations.putExtra("actorInfo", info);
				startActivity(openOperations);
			} 
			break;
		case R.id.Receiver: // Receiving Data
			Intent openReceivingMenu = new Intent(Menu.this, ReceivingMenu.class);
			startActivity(openReceivingMenu);
		    break;
		default:
			break;
		}
	}

}