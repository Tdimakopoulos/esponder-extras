package com.esponder.apps;

import java.io.IOException;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SelectStreet extends Activity {

	EditText searchText;
	Button searchButton;
	ListView listviewResult;

	Geocoder myGeocoder;
	final static int MAX_RESULT = 10;
	final static String DEFAULT_SEARCH = "ermou";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selectstreet);
		searchText = (EditText) findViewById(R.id.searchtext);
		searchText.setText(DEFAULT_SEARCH);
		searchButton = (Button) findViewById(R.id.searchbutton);
		listviewResult = (ListView) findViewById(R.id.listviewresult);

		searchButton.setOnClickListener(searchButtonOnClickListener);

		myGeocoder = new Geocoder(this);

		// for API Level 9 or higher
	/*	if (!Geocoder.isPresent()) {
			Toast.makeText(SelectStreet.this,
					"Sorry! Geocoder service not Present.", Toast.LENGTH_LONG)
					.show();
		}
*/
		listviewResult
				.setOnItemClickListener(listviewResultOnItemClickListener);
	}

	OnItemClickListener listviewResultOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub

			double lat = ((Address) parent.getItemAtPosition(position))
					.getLatitude();
			double lon = ((Address) parent.getItemAtPosition(position))
					.getLongitude();
			String loc = "lat: " + lat + "\n" + "lon: " + lon;
			Toast.makeText(SelectStreet.this, loc,
					Toast.LENGTH_LONG).show();
			List<Address> addresses = null;
			try {
				addresses = myGeocoder.getFromLocation(lat, lon, 1);
			//	myGeocoder.get
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.d("!!GIA DES!!",lat+":"+lon+"   Edwpa :"+ addresses.get(0));
			Log.d("!!GIA DES!!","Edwpa1 :"+ addresses.get(0).getAddressLine(0));
			Log.d("!!GIA DES!!","Edwpa1 :"+ addresses.get(0).getAddressLine(1));
			Log.d("!!GIA DES!!","Edwpa1 :"+ addresses.get(0).getAddressLine(2)); 
		}

	};

	Button.OnClickListener searchButtonOnClickListener = new Button.OnClickListener() {

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			String searchString = searchText.getText().toString();
			searchFromLocationName(searchString);
		}
	};

	private void searchFromLocationName(String name) {
		try {
			List<Address> result = myGeocoder.getFromLocationName(name,
					MAX_RESULT);

			if ((result == null) || (result.isEmpty())) {
				Toast.makeText(
						SelectStreet.this,
						"No matches were found or there is no backend service!",
						Toast.LENGTH_LONG).show();
			} else {

				MyArrayAdapter adapter = new MyArrayAdapter(this,
						android.R.layout.simple_list_item_1, result);
				listviewResult.setAdapter(adapter);

				Toast.makeText(SelectStreet.this,
						"Finished!", Toast.LENGTH_LONG).show();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(
					SelectStreet.this,
					"The network is unavailable or any other I/O problem occurs!",
					Toast.LENGTH_LONG).show();
		}
	}

	public class MyArrayAdapter extends ArrayAdapter<Address> {
		Context mycontext;

		public MyArrayAdapter(Context context, int textViewResourceId,
				List<Address> objects) {
			super(context, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
			mycontext = context;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			int maxAddressLineIndex = getItem(position)
					.getMaxAddressLineIndex();
			String addressLine = "";

			for (int j = 0; j <= maxAddressLineIndex; j++) {
				addressLine += getItem(position).getAddressLine(j) + ",";
			}

			TextView rowAddress = new TextView(mycontext);
			rowAddress.setText(addressLine);

			return rowAddress;

		}

	}

}
