package eu.esponder.fru.dto.model.snapshot.location;

import java.math.BigDecimal;

public class CubeVolumeDTO extends LocationAreaDTO {

	private PointDTO point;

	private BigDecimal acme;

	public PointDTO getPoint() {
		return point;
	}

	public void setPoint(PointDTO point) {
		this.point = point;
	}

	public BigDecimal getAcme() {
		return acme;
	}

	public void setAcme(BigDecimal acme) {
		this.acme = acme;
	}

}
