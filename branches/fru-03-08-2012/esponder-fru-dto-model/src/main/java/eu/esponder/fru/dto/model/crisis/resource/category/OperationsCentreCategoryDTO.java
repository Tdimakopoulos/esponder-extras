package eu.esponder.fru.dto.model.crisis.resource.category;

import java.util.Set;

import eu.esponder.fru.dto.model.crisis.resource.OperationsCentreDTO;

public class OperationsCentreCategoryDTO extends PlannableResourceCategoryDTO {

	private static final long serialVersionUID = 2966454872810486900L;

	public OperationsCentreCategoryDTO() { }
	
	private OperationsCentreTypeDTO operationsCentreType;
	
	private Set<OperationsCentreDTO> operationsCentres;
	
	public OperationsCentreTypeDTO getOperationsCentreType() {
		return operationsCentreType;
	}

	public void setOperationsCentreType(OperationsCentreTypeDTO operationsCentreType) {
		this.operationsCentreType = operationsCentreType;
	}

	public Set<OperationsCentreDTO> getOperationsCentres() {
		return operationsCentres;
	}

	public void setOperationsCentres(Set<OperationsCentreDTO> operationsCentres) {
		this.operationsCentres = operationsCentres;
	}
}
