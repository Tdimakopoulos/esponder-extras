package eu.esponder.fru.dto.model.snapshot.location;
 

import eu.esponder.fru.dto.model.ESponderEntityDTO;


 
public abstract class LocationAreaDTO extends ESponderEntityDTO {

public LocationAreaDTO() {}
	
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
