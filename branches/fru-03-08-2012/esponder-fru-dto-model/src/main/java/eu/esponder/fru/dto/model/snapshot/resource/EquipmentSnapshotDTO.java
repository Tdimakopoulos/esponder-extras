package eu.esponder.fru.dto.model.snapshot.resource;

import eu.esponder.fru.dto.model.snapshot.SnapshotDTO;
import eu.esponder.fru.dto.model.snapshot.status.EquipmentSnapshotStatusDTO;
import eu.esponder.fru.dto.model.crisis.resource.EquipmentDTO;

public class EquipmentSnapshotDTO extends SnapshotDTO {
	
	public EquipmentSnapshotDTO() { }
	
	private EquipmentSnapshotStatusDTO status;

	private EquipmentDTO equipment;
	
	public EquipmentSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(EquipmentSnapshotStatusDTO status) {
		this.status = status;
	}

	public EquipmentDTO getEquipment() {
		return equipment;
	}

	public void setEquipment(EquipmentDTO equipment) {
		this.equipment = equipment;
	}
	
	@Override
	public String toString() {
		return "EquipmentSnapshotDTO [status=" + status + ", period=" + period + ", id=" + id + "]";
	}
	
}
