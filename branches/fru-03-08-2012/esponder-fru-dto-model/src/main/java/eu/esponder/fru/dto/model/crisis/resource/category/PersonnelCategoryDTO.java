package eu.esponder.fru.dto.model.crisis.resource.category;

import java.util.Set;

import eu.esponder.fru.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.fru.dto.model.crisis.resource.PersonnelDTO;

public class PersonnelCategoryDTO extends PlannableResourceCategoryDTO {

	/**
	 * Models the discipline-specific (RankType) rank, i.e. General, Colonel etc. 
	 */
	private RankTypeDTO rank;
	
	private PersonnelDTO personnel;
	
	private Set<PersonnelCompetenceDTO> personnelCompetences;
	
	private OrganisationCategoryDTO organisationCategory;

	public RankTypeDTO getRank() {
		return rank;
	}

	public void setRank(RankTypeDTO rank) {
		this.rank = rank;
	}

	public PersonnelDTO getPersonnel() {
		return personnel;
	}

	public void setPersonnel(PersonnelDTO personnel) {
		this.personnel = personnel;
	}

	public Set<PersonnelCompetenceDTO> getPersonnelCompetences() {
		return personnelCompetences;
	}

	public void setPersonnelCompetences(
			Set<PersonnelCompetenceDTO> personnelCompetences) {
		this.personnelCompetences = personnelCompetences;
	}

	public OrganisationCategoryDTO getOrganisationCategory() {
		return organisationCategory;
	}

	public void setOrganisationCategory(OrganisationCategoryDTO organisationCategory) {
		this.organisationCategory = organisationCategory;
	}
	
}
