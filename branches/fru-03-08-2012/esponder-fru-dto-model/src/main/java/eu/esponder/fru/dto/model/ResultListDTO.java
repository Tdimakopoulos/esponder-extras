package eu.esponder.fru.dto.model;

import java.util.List;

public class ResultListDTO {

	private List<? extends ESponderEntityDTO> resultList;

	public ResultListDTO() {
	}

	public ResultListDTO(List<? extends ESponderEntityDTO> resultsList) {
		this.resultList = resultsList;
	}

	public List<? extends ESponderEntityDTO> getResultList() {
		return resultList;
	}

	public void setResultList(List<? extends ESponderEntityDTO> resultList) {
		this.resultList = resultList;
	}

	public String toString() {
		String result = "[ResultsList:";
		for (ESponderEntityDTO entity : this.getResultList()) {
			result += entity.toString();
		}
		result += "]";
		return result;
	}

}
