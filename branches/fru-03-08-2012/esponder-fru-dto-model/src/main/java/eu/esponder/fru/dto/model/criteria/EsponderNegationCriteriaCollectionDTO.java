package eu.esponder.fru.dto.model.criteria;

import java.util.Collection;

public class EsponderNegationCriteriaCollectionDTO extends EsponderCriteriaCollectionDTO {

	private static final long serialVersionUID = -786849464712579302L;

	public EsponderNegationCriteriaCollectionDTO(
			Collection<EsponderQueryRestrictionDTO> restrictions) {
		super(restrictions);
	}
	
	public EsponderNegationCriteriaCollectionDTO() { }

}
