package eu.esponder.fru.dto.model.crisis.resource;

import java.util.Set;

import eu.esponder.fru.dto.model.crisis.resource.plan.PlannableResourceDTO;
import eu.esponder.fru.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.fru.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;

public class OperationsCentreDTO extends PlannableResourceDTO {
	
	public OperationsCentreDTO() { }
	
	private Set<ActorDTO> actors;
	
	private OperationsCentreDTO supervisor;
	
	private Set<OperationsCentreDTO> subordinates;
	
	private OperationsCentreSnapshotDTO snapshot;
	
	private Long operationsCentreCategoryId;
	
	private VoIPURLDTO voIPURL;

	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}

	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}
	
	public Set<ActorDTO> getActors() {
		return actors;
	}
	
	public void setActors(Set<ActorDTO> actors) {
		this.actors = actors;
	}
	
	public Set<OperationsCentreDTO> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(Set<OperationsCentreDTO> subordinates) {
		this.subordinates = subordinates;
	}

	public OperationsCentreSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(OperationsCentreSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	public Long getOperationsCentreCategoryId() {
		return operationsCentreCategoryId;
	}

	public void setOperationsCentreCategoryId(Long operationsCentreCategoryId) {
		this.operationsCentreCategoryId = operationsCentreCategoryId;
	}

	public OperationsCentreDTO getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(OperationsCentreDTO supervisor) {
		this.supervisor = supervisor;
	}
	
	@Override
	public String toString() {
		return "OperationsCentreDTO [type=" + type + ", status=" + status
				+ ", id=" + id + ", title=" + title + "]";
	}
	
}
