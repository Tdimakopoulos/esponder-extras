package eu.esponder.fru.dto.model.crisis.action;

public enum ActionOperationEnumDTO {

	MOVE,
	TRANSPORT,
	FIX
}
