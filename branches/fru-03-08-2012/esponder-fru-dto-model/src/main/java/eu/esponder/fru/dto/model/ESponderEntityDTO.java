package eu.esponder.fru.dto.model;

public abstract class ESponderEntityDTO {
	
	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}

