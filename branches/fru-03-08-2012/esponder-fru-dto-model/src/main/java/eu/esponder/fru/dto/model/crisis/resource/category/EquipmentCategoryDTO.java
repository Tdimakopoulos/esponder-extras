package eu.esponder.fru.dto.model.crisis.resource.category;

import java.util.Set;

import eu.esponder.fru.dto.model.crisis.resource.EquipmentDTO;

public class EquipmentCategoryDTO extends PlannableResourceCategoryDTO {

	private EquipmentTypeDTO equipmentType;
	
	private Set<EquipmentDTO> equipment;

	public EquipmentTypeDTO getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(EquipmentTypeDTO equipmentType) {
		this.equipmentType = equipmentType;
	}

	public Set<EquipmentDTO> getEquipment() {
		return equipment;
	}

	public void setEquipment(Set<EquipmentDTO> equipment) {
		this.equipment = equipment;
	}
	
}
