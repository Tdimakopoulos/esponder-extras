package eu.esponder.fru.dto.model.snapshot.location;

import java.io.Serializable;




public class AddressDTO  {
	
	

	public AddressDTO() {}
	
	private String number;
	
	private String street;
	
	private String zipCode;
	
	private String prefecture;
	
	private String country;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPrefecture() {
		return prefecture;
	}

	public void setPrefecture(String prefecture) {
		this.prefecture = prefecture;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
