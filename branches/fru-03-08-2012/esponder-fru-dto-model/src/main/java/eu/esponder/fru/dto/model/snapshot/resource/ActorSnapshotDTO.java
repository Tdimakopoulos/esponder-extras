package eu.esponder.fru.dto.model.snapshot.resource;

import eu.esponder.fru.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.fru.dto.model.snapshot.status.ActorSnapshotStatusDTO;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
public class ActorSnapshotDTO extends SpatialSnapshotDTO {
	
	public ActorSnapshotDTO() { }
	
	private ActorSnapshotStatusDTO status;

	private ActorDTO actor;
	
	public ActorSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActorSnapshotStatusDTO status) {
		this.status = status;
	}

	public ActorDTO getActor() {
		return actor;
	}

	public void setActor(ActorDTO actor) {
		this.actor = actor;
	}
	@Override
	public String toString() {
		return "ActorSnapshotDTO [status=" + status + ", locationArea=" + locationArea + ", id=" + id  + "]";
	}
	
}
