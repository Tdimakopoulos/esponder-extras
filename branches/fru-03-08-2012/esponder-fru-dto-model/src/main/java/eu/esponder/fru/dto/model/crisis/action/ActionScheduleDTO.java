package eu.esponder.fru.dto.model.crisis.action;

import eu.esponder.fru.dto.model.ESponderEntityDTO;

public class ActionScheduleDTO extends ESponderEntityDTO {

	private String title;
	
	private ActionDTO action;
	
	private ActionDTO prerequisiteAction;
	
	private ActionScheduleCriteriaDTO criteria;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ActionDTO getAction() {
		return action;
	}

	public void setAction(ActionDTO action) {
		this.action = action;
	}

	public ActionDTO getPrerequisiteAction() {
		return prerequisiteAction;
	}

	public void setPrerequisiteAction(ActionDTO prerequisiteAction) {
		this.prerequisiteAction = prerequisiteAction;
	}

	public ActionScheduleCriteriaDTO getCriteria() {
		return criteria;
	}

	public void setCriteria(ActionScheduleCriteriaDTO criteria) {
		this.criteria = criteria;
	}

}


