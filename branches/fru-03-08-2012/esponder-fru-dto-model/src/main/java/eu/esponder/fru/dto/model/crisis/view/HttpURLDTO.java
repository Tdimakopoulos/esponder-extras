package eu.esponder.fru.dto.model.crisis.view;

public class HttpURLDTO extends URLDTO {

	private static final long serialVersionUID = 7908917079873002927L;

	public HttpURLDTO() {
		this.setProtocol("http");
	}
}
