package eu.esponder.fru.dto.model.crisis.resource.category;

import java.util.Set;

import eu.esponder.fru.dto.model.crisis.resource.ReusableResourceDTO;

public class ReusableResourceCategoryDTO extends LogisticsCategoryDTO {

	private ReusableResourceTypeDTO reusableResourceType;

	private Set<ReusableResourceDTO> reusableResources;

	public ReusableResourceTypeDTO getReusableResourceType() {
		return reusableResourceType;
	}

	public void setReusableResourceType(ReusableResourceTypeDTO reusableResourceType) {
		this.reusableResourceType = reusableResourceType;
	}

	public Set<ReusableResourceDTO> getReusableResources() {
		return reusableResources;
	}

	public void setReusableResources(Set<ReusableResourceDTO> reusableResources) {
		this.reusableResources = reusableResources;
	}

}
