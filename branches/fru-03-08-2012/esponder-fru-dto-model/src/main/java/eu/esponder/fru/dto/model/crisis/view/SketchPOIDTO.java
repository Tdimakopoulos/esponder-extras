package eu.esponder.fru.dto.model.crisis.view;

import java.util.Set;

public class SketchPOIDTO extends ResourcePOIDTO {	

	public SketchPOIDTO() { }
	
	private String description;
	
	private String sketchType;
	
	private HttpURLDTO httpURL;
	
	private Set<MapPointDTO> points;

	public String getDescription() {
		return description;
	}

	public String getSketchType() {
		return sketchType;
	}

	public void setSketchType(String sketchType) {
		this.sketchType = sketchType;
	}

	public HttpURLDTO getHttpURL() {
		return httpURL;
	}

	public void setHttpURL(HttpURLDTO httpURL) {
		this.httpURL = httpURL;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<MapPointDTO> getPoints() {
		return points;
	}

	public void setPoints(Set<MapPointDTO> points) {
		this.points = points;
	}	
	
}
