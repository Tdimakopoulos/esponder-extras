package eu.esponder.fru.dto.model.crisis.resource.category;

import java.util.Set;

import eu.esponder.fru.dto.model.crisis.resource.ConsumableResourceDTO;

public class ConsumableResourceCategoryDTO extends LogisticsCategoryDTO {

	private ConsumableResourceTypeDTO consumableResourceType;

	private Set<ConsumableResourceDTO> consumableResources;

	public ConsumableResourceTypeDTO getConsumableResourceType() {
		return consumableResourceType;
	}

	public void setConsumableResourceType(
			ConsumableResourceTypeDTO consumableResourceType) {
		this.consumableResourceType = consumableResourceType;
	}

	public Set<ConsumableResourceDTO> getConsumableResources() {
		return consumableResources;
	}

	public void setConsumableResources(
			Set<ConsumableResourceDTO> consumableResources) {
		this.consumableResources = consumableResources;
	}

}
