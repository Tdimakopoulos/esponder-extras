package eu.esponder.fru.dto.model.snapshot.sensor.config;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;

public class StatisticsConfigDTO extends ESponderEntityDTO {
	
	private MeasurementStatisticTypeEnumDTO measurementStatisticType;
	
	private long samplingPeriodMilliseconds;
	
	private Long sensorId;
	
	public StatisticsConfigDTO() { }
	
	public MeasurementStatisticTypeEnumDTO getMeasurementStatisticType() {
		return measurementStatisticType;
	}

	public void setMeasurementStatisticType(
			MeasurementStatisticTypeEnumDTO measurementStatisticType) {
		this.measurementStatisticType = measurementStatisticType;
	}

	public long getSamplingPeriodMilliseconds() {
		return samplingPeriodMilliseconds;
	}

	public void setSamplingPeriodMilliseconds(long samplingPeriodMilliseconds) {
		this.samplingPeriodMilliseconds = samplingPeriodMilliseconds;
	}
	
	public String toString() {
		return "[Type : " + measurementStatisticType + ", sampling Period (ms): " + samplingPeriodMilliseconds + " ]";
	}

	public Long getSensorId() {
		return sensorId;
	}

	public void setSensorId(Long sensorId) {
		this.sensorId = sensorId;
	}
}
