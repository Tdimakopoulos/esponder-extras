package eu.esponder.fru.dto.model.crisis.action;

import javax.xml.datatype.XMLGregorianCalendar;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.snapshot.status.ActionSnapshotStatusDTO;


public class ActionScheduleCriteriaDTO extends ESponderEntityDTO {

	private ActionSnapshotStatusDTO status;

	private XMLGregorianCalendar dateAfter;

	public ActionSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActionSnapshotStatusDTO status) {
		this.status = status;
	}

	public XMLGregorianCalendar getDateAfter() {
		return dateAfter;
	}

	public void setDateAfter(XMLGregorianCalendar dateAfter) {
		this.dateAfter = dateAfter;
	}

}
