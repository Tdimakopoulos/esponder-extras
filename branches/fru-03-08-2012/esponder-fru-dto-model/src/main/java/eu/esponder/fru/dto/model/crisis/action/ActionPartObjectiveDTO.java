package eu.esponder.fru.dto.model.crisis.action;

import java.util.Set;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.fru.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.fru.dto.model.snapshot.PeriodDTO;
import eu.esponder.fru.dto.model.snapshot.location.LocationAreaDTO;


public class ActionPartObjectiveDTO extends ESponderEntityDTO {

	private String title;
	
	private PeriodDTO period;
	
	private LocationAreaDTO locationArea;
	
	/*
	 * These resources are part of the action part's target. For example the "food packages" 
	 * in the following sentence: Move these food-packages using a truck
	 */
	private Set<ConsumableResourceDTO> consumableResources;
	
	/*
	 * These resources are part of the action part's target. For example the "telecom equipment" 
	 * in the following sentence: Move this telecom equipment using a truck
	 */
	private Set<ReusableResourceDTO> reusuableResources;
	
	private ActionPartDTO actionPart;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

	public Set<ConsumableResourceDTO> getConsumableResources() {
		return consumableResources;
	}

	public void setConsumableResources(Set<ConsumableResourceDTO> consumableResources) {
		this.consumableResources = consumableResources;
	}

	public Set<ReusableResourceDTO> getReusuableResources() {
		return reusuableResources;
	}

	public void setReusuableResources(Set<ReusableResourceDTO> reusuableResources) {
		this.reusuableResources = reusuableResources;
	}

	public ActionPartDTO getActionPart() {
		return actionPart;
	}

	public void setActionPart(ActionPartDTO actionPart) {
		this.actionPart = actionPart;
	}
	
}
