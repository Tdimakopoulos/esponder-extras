package eu.esponder.fru.dto.model.crisis.resource;

import java.math.BigDecimal;

import eu.esponder.fru.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.fru.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;


public class ConsumableResourceDTO extends LogisticsResourceDTO {
	
	public ConsumableResourceDTO() { }
	
	private BigDecimal quantity;
	
	private ConsumableResourceCategoryDTO consumableResourceCategory;
	
	private ActionPartDTO actionPart;
	
	private ReusableResourceDTO container;

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public ConsumableResourceCategoryDTO getConsumableResourceCategory() {
		return consumableResourceCategory;
	}

	public void setConsumableResourceCategory(ConsumableResourceCategoryDTO consumableResourceCategory) {
		this.consumableResourceCategory = consumableResourceCategory;
	}

	public ActionPartDTO getActionPart() {
		return actionPart;
	}

	public void setActionPart(ActionPartDTO actionPart) {
		this.actionPart = actionPart;
	}

	public ReusableResourceDTO getContainer() {
		return container;
	}

	public void setContainer(ReusableResourceDTO container) {
		this.container = container;
	}

}
