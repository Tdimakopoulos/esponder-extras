package eu.esponder.fru.dto.model.crisis.action;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.snapshot.PeriodDTO;
import eu.esponder.fru.dto.model.snapshot.location.LocationAreaDTO;


public class ActionObjectiveDTO extends ESponderEntityDTO {

	private String title;
	
	private PeriodDTO period;
	
	private LocationAreaDTO locationArea;

	private ActionDTO action;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

	public ActionDTO getAction() {
		return action;
	}

	public void setAction(ActionDTO action) {
		this.action = action;
	}
	
}
