package eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.snapshot.PeriodDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;

public class SensorMeasurementStatisticDTO extends ESponderEntityDTO {
	
	private MeasurementStatisticTypeEnumDTO statisticType;
	
	/**
	 * Sampling period in msec
	 */
	private long samplingPeriod;
	
	private SensorMeasurementDTO statistic;
	
	private PeriodDTO period;
	
	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

	public long getSamplingPeriod() {
		return samplingPeriod;
	}

	public void setSamplingPeriod(long samplingPeriod) {
		this.samplingPeriod = samplingPeriod;
	}

	public SensorMeasurementDTO getStatistic() {
		return statistic;
	}

	public void setStatistic(SensorMeasurementDTO statistic) {
		this.statistic = statistic;
	}

	public MeasurementStatisticTypeEnumDTO getStatisticType() {
		return statisticType;
	}

	public void setStatisticType(MeasurementStatisticTypeEnumDTO statisticType) {
		this.statisticType = statisticType;
	}
	
}
