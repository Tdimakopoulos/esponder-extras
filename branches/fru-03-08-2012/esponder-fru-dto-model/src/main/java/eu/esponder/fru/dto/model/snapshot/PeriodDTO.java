package eu.esponder.fru.dto.model.snapshot;

import javax.xml.datatype.XMLGregorianCalendar;

public class PeriodDTO {
	
	private XMLGregorianCalendar dateFrom;
	
	private XMLGregorianCalendar dateTo;

	public XMLGregorianCalendar getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(XMLGregorianCalendar dateFrom) {
		this.dateFrom = dateFrom;
	}

	public XMLGregorianCalendar getDateTo() {
		return dateTo;
	}

	public void setDateTo(XMLGregorianCalendar dateTo) {
		this.dateTo = dateTo;
	}

	@Override
	public String toString() {
		return "Period [dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}

}
