package eu.esponder.fru.dto.model.snapshot.status;


public enum ActorSnapshotStatusDTO {
	READY,
	ACTIVE,
	INACTIVE
}
