package eu.esponder.fru.dto.model.crisis.view;


public class ReferencePOIDTO extends ResourcePOIDTO {
	
	private String referenceFile;
	
	private Float averageSize;

	public String getReferenceFile() {
		return referenceFile;
	}

	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

	public Float getAverageSize() {
		return averageSize;
	}

	public void setAverageSize(Float averageSize) {
		this.averageSize = averageSize;
	}

}
