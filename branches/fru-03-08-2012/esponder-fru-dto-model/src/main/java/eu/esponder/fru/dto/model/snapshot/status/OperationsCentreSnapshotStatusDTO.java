package eu.esponder.fru.dto.model.snapshot.status;


public enum OperationsCentreSnapshotStatusDTO {
	STATIONARY,
	MOVING
}
