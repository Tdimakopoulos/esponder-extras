package eu.esponder.fru.dto.model.crisis.resource.sensor;

import java.util.Set;

import eu.esponder.fru.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;

public abstract class SensorDTO extends ResourceDTO {
	
	public SensorDTO() { }
	
	private String name;
	
	private MeasurementUnitEnumDTO measurementUnit;
	
	private Set<StatisticsConfigDTO> statisticsConfig;
	
	private String label;
	
	private Long equipmentId;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MeasurementUnitEnumDTO getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(MeasurementUnitEnumDTO measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	public Set<StatisticsConfigDTO> getStatisticsConfig() {
		return statisticsConfig;
	}

	public void setStatisticsConfig(Set<StatisticsConfigDTO> statisticsConfig) {
		this.statisticsConfig = statisticsConfig;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(null == obj || obj instanceof SensorDTO)) return false;
		
		SensorDTO sensor = (SensorDTO) obj;
		if (!(sensor.getClass() == SensorDTO.class)) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "SensorDTO [name=" + name + ", type=" + type + ", measurementUnit="
				+ measurementUnit + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}
	
	public Long getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(Long equipmentId) {
		this.equipmentId = equipmentId;
	}
	
}
