package eu.esponder.fru.dto.model.snapshot.action;

import eu.esponder.fru.dto.model.crisis.action.ActionDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.fru.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.fru.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;


public class ActionPartSnapshotDTO extends SpatialSnapshotDTO {
	
	private ActionPartSnapshotStatusDTO status;
	
	private ActionPartDTO actionPart;

	public ActionPartSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActionPartSnapshotStatusDTO status) {
		this.status = status;
	}
	
	public void setActionPart(ActionPartDTO actionPart) {
		this.actionPart = actionPart;
	}
	
	public ActionPartDTO getActionPart() {
		return actionPart;
	}
	
		
}
