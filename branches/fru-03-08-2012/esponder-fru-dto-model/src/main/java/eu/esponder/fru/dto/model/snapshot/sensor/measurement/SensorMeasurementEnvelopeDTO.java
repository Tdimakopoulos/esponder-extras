package eu.esponder.fru.dto.model.snapshot.sensor.measurement;

import java.util.ArrayList;
import java.util.List;

import eu.esponder.fru.dto.model.ESponderEntityDTO;

public class SensorMeasurementEnvelopeDTO extends ESponderEntityDTO {

	private List<SensorMeasurementDTO> measurements;

	public List<SensorMeasurementDTO> getMeasurements() {
		return measurements;
	}

	public void setMeasurements(List<SensorMeasurementDTO> measurements) {
		this.measurements = measurements;
	}
	
	public void add(SensorMeasurementDTO measurement) {
		if (null == this.measurements) {
			this.measurements = new ArrayList<SensorMeasurementDTO>();
		}
		this.measurements.add(measurement);
	}
	
	public void clear() {
		this.measurements.clear();
	}
	
	
}
