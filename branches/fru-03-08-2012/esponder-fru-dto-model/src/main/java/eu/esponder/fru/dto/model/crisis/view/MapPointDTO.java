package eu.esponder.fru.dto.model.crisis.view;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.snapshot.location.PointDTO;

public class MapPointDTO extends ESponderEntityDTO {
	
	private String title;

	private PointDTO point;
	
	private Integer icon;
	
	public String getTitle() {
		return title;
	}

	public Integer getIcon() {
		return icon;
	}

	public void setIcon(Integer icon) {
		this.icon = icon;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public PointDTO getPoint() {
		return point;
	}

	public void setPoint(PointDTO point) {
		this.point = point;
	}

}
