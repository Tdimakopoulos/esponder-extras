package eu.esponder.fru.dto.model.snapshot.status;


public enum SensorSnapshotStatusDTO {
	WORKING,
	MALFUNCTIONING,
	DAMAGED
}
