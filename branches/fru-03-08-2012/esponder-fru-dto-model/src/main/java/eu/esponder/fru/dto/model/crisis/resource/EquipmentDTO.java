package eu.esponder.fru.dto.model.crisis.resource;

import java.util.Set;

import eu.esponder.fru.dto.model.crisis.resource.plan.PlannableResourceDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.fru.dto.model.snapshot.resource.EquipmentSnapshotDTO;

public class EquipmentDTO extends PlannableResourceDTO {
	
	public EquipmentDTO() { }
	
	private Set<SensorDTO> sensors;
	
	private  EquipmentSnapshotDTO snapshot;
	
	private Long equipmentCategoryId;
	
	private ActorDTO actor;

	public ActorDTO getActor() {
		return actor;
	}

	public void setActor(ActorDTO actor) {
		this.actor = actor;
	}

	public Set<SensorDTO> getSensors() {
		return sensors;
	}

	public void setSensors(Set<SensorDTO> sensors) {
		this.sensors = sensors;
	}

	public EquipmentSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(EquipmentSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	@Override
	public String toString() {
		return "EquipmentDTO [type=" + type + ", status=" + status + ", id="
				+ id + ", title=" + title + "]";
	}

	public Long getEquipmentCategoryId() {
		return equipmentCategoryId;
	}

	public void setEquipmentCategoryId(Long equipmentCategoryId) {
		this.equipmentCategoryId = equipmentCategoryId;
	}
	
}
