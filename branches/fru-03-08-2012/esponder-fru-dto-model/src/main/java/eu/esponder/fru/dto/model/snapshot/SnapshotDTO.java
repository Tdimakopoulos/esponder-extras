package eu.esponder.fru.dto.model.snapshot;

import eu.esponder.fru.dto.model.ESponderEntityDTO;

public abstract class SnapshotDTO extends ESponderEntityDTO {

	protected PeriodDTO period;

	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

}
