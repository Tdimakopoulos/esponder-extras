package eu.esponder.fru.dto.model.crisis.resource.plan;

import java.util.Set;

import eu.esponder.fru.dto.model.ESponderEntityDTO;

public class CrisisResourcePlanDTO extends ESponderEntityDTO {

	private Set<PlannableResourcePowerDTO> plannableResources;
	
	/**
	 * This field indicates either the CrisisDisasterType or the CrisisFeatureType for the plan.
	 * It is for further study whether the combination of them (i.e DisasterType and FeatureType) 
	 * should mandate that an additive exclusive result of the plannableResourcePower should be applied.
	 * 
	 */
	private CrisisTypeDTO crisisType;

	public Set<PlannableResourcePowerDTO> getPlannableResources() {
		return plannableResources;
	}

	public void setPlannableResources(
			Set<PlannableResourcePowerDTO> plannableResources) {
		this.plannableResources = plannableResources;
	}

	public CrisisTypeDTO getCrisisType() {
		return crisisType;
	}

	public void setCrisisType(CrisisTypeDTO crisisType) {
		this.crisisType = crisisType;
	}
	
}
