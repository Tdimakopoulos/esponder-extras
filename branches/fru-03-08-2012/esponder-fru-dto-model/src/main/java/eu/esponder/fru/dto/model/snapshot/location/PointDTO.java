package eu.esponder.fru.dto.model.snapshot.location;

import java.math.BigDecimal;

public class PointDTO {

	public PointDTO() { }
	
	
	private BigDecimal latitude;

	private BigDecimal longitude;

	private BigDecimal altitude;

	public PointDTO(BigDecimal latitude, BigDecimal longitude, BigDecimal altitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}
	
	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getAltitude() {
		return altitude;
	}

	public void setAltitude(BigDecimal altitude) {
		this.altitude = altitude;
	}

	@Override
	public String toString() {
		return "Point [latitude=" + latitude + ", longitude=" + longitude
				+ ", altitude=" + altitude + "]";
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (null == this.getAltitude() ? 0 : this.getAltitude().hashCode());
		hash = 31 * hash + (null == this.getLongitude() ? 0 : this.getLongitude().hashCode());
		hash = 31 * hash + (null == this.getLatitude() ? 0 : this.getLatitude().hashCode());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if ((obj == null) || (obj.getClass() != this.getClass())) {
			return false;
		}
		PointDTO pointDTO = (PointDTO) obj;
		if (compareCoordinates(this.getAltitude(), pointDTO.getAltitude())
				&& compareCoordinates(this.getLatitude(),
						pointDTO.getLatitude())
				&& compareCoordinates(this.getLongitude(),
						pointDTO.getLongitude())) {
			return true;
		}
		return false;
	}

	private boolean compareCoordinates(BigDecimal c1, BigDecimal c2) {
		if (c1 == null && c2 == null) {
			return true;
		} else if ((c1 != null && c2 == null) || (c1 == null && c2 != null)) {
			return false;
		} else {
			return c1.compareTo(c2) == 0 ? true : false;
		}

	}

}
