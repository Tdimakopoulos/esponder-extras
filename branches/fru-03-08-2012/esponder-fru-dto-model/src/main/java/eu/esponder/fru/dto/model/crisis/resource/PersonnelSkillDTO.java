package eu.esponder.fru.dto.model.crisis.resource;


public class PersonnelSkillDTO extends PersonnelCompetenceDTO {

	private static final long serialVersionUID = -4287360040986460222L;
	
	private PersonnelSkillTypeDTO personnelSkillType;

	public PersonnelSkillTypeDTO getPersonnelSkillType() {
		return personnelSkillType;
	}

	public void setPersonnelSkillType(PersonnelSkillTypeDTO personnelSkillType) {
		this.personnelSkillType = personnelSkillType;
	}
	
}