package eu.esponder.fru.dto.model.crisis.view;

import java.io.Serializable;

public abstract class URLDTO implements Serializable {

	private static final long serialVersionUID = 5414252531599578444L;

	private static final String separator = "/";
	
	private static final String protocolSeparator = "://";
	
	private String protocol;
	
	private String hostName;
	
	private String path;
	
	public String getURL() {
		return protocol + protocolSeparator + hostName + separator + path; 
	}
	
	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
		return hostName;
	}

	public void setHost(String host) {
		this.hostName = host;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
