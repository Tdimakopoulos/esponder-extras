package eu.esponder.fru.dto.model.snapshot.action;

import eu.esponder.fru.dto.model.crisis.action.ActionDTO;
import eu.esponder.fru.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.fru.dto.model.snapshot.status.ActionSnapshotStatusDTO;


public class ActionSnapshotDTO extends SpatialSnapshotDTO {
	
	private ActionSnapshotStatusDTO status;
	
	private ActionDTO action;
	
	public ActionSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActionSnapshotStatusDTO status) {
		this.status = status;
	}
	
	public void setAction(ActionDTO action) {
		this.action = action;
	}
	
	public ActionDTO getAction() {
		return action;
	}

}
