package eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic;

public enum MeasurementStatisticTypeEnumDTO {

	MEAN,
	STDEV,

	AUTOCORRELATION,

	MAXIMUM,
	MINIMUM,

	FIRST,
	LAST,
	MEDIAN,
	
	OLDEST,
	NEWEST
	
}
