package eu.esponder.fru.dto.model.criteria;

import java.util.Collection;

public class EsponderUnionCriteriaCollectionDTO extends EsponderCriteriaCollectionDTO {

	private static final long serialVersionUID = -4963444702454465879L;

	public EsponderUnionCriteriaCollectionDTO() { }
	
	public EsponderUnionCriteriaCollectionDTO(
			Collection<EsponderQueryRestrictionDTO> restrictions) {
		super(restrictions);
	}

}
