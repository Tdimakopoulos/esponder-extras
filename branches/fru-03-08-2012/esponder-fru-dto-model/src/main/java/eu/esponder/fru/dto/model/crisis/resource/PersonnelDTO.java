package eu.esponder.fru.dto.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.fru.dto.model.crisis.resource.plan.PlannableResourceDTO;

public class PersonnelDTO extends PlannableResourceDTO {
	
	private static final long serialVersionUID = 5687655161402727780L;

	public PersonnelDTO() { }
	
	private Long id;
	
	private String firstName;
	
	private String lastName;
	
	private RankTypeDTO rank;
	
	private boolean availability;
	
	private OrganisationDTO organisation;
	
	private PersonnelCategoryDTO personnelCategory;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public RankTypeDTO getRank() {
		return rank;
	}

	public void setRank(RankTypeDTO rank) {
		this.rank = rank;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public boolean isAvailability() {
		return availability;
	}

	public void setAvailability(boolean availability) {
		this.availability = availability;
	}

	public OrganisationDTO getOrganisation() {
		return organisation;
	}

	public void setOrganisation(OrganisationDTO organisation) {
		this.organisation = organisation;
	}

	public PersonnelCategoryDTO getPersonnelCategory() {
		return personnelCategory;
	}

	public void setPersonnelCategory(PersonnelCategoryDTO personnelCategory) {
		this.personnelCategory = personnelCategory;
	}

}
