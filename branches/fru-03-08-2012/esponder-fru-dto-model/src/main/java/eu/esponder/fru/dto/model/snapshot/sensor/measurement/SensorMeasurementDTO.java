package eu.esponder.fru.dto.model.snapshot.sensor.measurement;

import java.util.Date;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.SensorDTO;

public abstract class SensorMeasurementDTO extends ESponderEntityDTO {
	
	private SensorDTO sensor;
	
	private Date timestamp;

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public SensorDTO getSensor() {
		return sensor;
	}

	public void setSensor(SensorDTO sensor) {
		this.sensor = sensor;
	}


}
