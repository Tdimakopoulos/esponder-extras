package eu.esponder.fru.dto.model.crisis.action;

import java.util.Set;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.CrisisContextDTO;
import eu.esponder.fru.dto.model.snapshot.action.ActionSnapshotDTO;

public class ActionDTO extends ESponderEntityDTO {
	
	private String title;

	private String type;
	
	private Set<ActionPartDTO> actionParts;
	
	private CrisisContextDTO crisisContext;
	
	private ActionSnapshotDTO snapshot;
	
	private Set<ActionDTO> children;
	
	private Set<ActionObjectiveDTO> actionObjectives;
	
	private SeverityLevelDTO severityLevel;
	
	private ActionOperationEnumDTO actionOperation;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<ActionPartDTO> getActionParts() {
		return actionParts;
	}

	public void setActionParts(Set<ActionPartDTO> actionParts) {
		this.actionParts = actionParts;
	}

	public ActionSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(ActionSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	public Set<ActionDTO> getChildren() {
		return children;
	}

	public void setChildren(Set<ActionDTO> children) {
		this.children = children;
	}

	public Set<ActionObjectiveDTO> getActionObjectives() {
		return actionObjectives;
	}

	public void setActionObjectives(Set<ActionObjectiveDTO> actionObjectives) {
		this.actionObjectives = actionObjectives;
	}

	public SeverityLevelDTO getSeverityLevel() {
		return severityLevel;
	}

	public void setSeverityLevel(SeverityLevelDTO severityLevel) {
		this.severityLevel = severityLevel;
	}

	public ActionOperationEnumDTO getActionOperation() {
		return actionOperation;
	}

	public void setActionOperation(ActionOperationEnumDTO actionOperation) {
		this.actionOperation = actionOperation;
	}

	public CrisisContextDTO getCrisisContext() {
		return crisisContext;
	}

	public void setCrisisContext(CrisisContextDTO crisisContext) {
		this.crisisContext = crisisContext;
	}
	
}
