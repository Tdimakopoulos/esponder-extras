package eu.esponder.fru.dto.model.snapshot.sensor.measurement;

import eu.esponder.fru.dto.model.snapshot.location.PointDTO;

public class LocationSensorMeasurementDTO extends SensorMeasurementDTO {

	private PointDTO point;

	public PointDTO getPoint() {
		return point;
	}

	public void setPoint(PointDTO point) {
		this.point = point;
	}

	@Override
	public String toString() {
		return "LocationSensorMeasurement: " + point.toString();
	}
}
