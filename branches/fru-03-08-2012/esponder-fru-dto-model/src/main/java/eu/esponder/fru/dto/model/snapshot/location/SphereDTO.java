package eu.esponder.fru.dto.model.snapshot.location;

import java.math.BigDecimal;


public class SphereDTO extends LocationAreaDTO {

public SphereDTO() { }
	
	public SphereDTO(PointDTO centre, BigDecimal radius, String title) {
		super();
		this.centre = centre;
		this.radius = radius;
		this.setTitle(title);
	}
	
	private PointDTO centre;
	
	private BigDecimal radius;

	public PointDTO getCentre() {
		return centre;
	}

	public void setCentre(PointDTO centre) {
		this.centre = centre;
	}

	public BigDecimal getRadius() {
		return radius;
	}

	public void setRadius(BigDecimal radius) {
		this.radius = radius;
	}

	@Override
	public String toString() {
		return "SphereDTO [centre=" + centre + ", radius=" + radius + "]";
	}
	
}
