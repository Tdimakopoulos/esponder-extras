package eu.esponder.fru.dto.model.snapshot;

import eu.esponder.fru.dto.model.snapshot.location.LocationAreaDTO;
 
public abstract class SpatialSnapshotDTO extends SnapshotDTO {
	
	protected LocationAreaDTO locationArea;

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

	@Override
	public String toString() {
		return "SpatialSnapshotDTO [locationArea=" + locationArea + ", id=" + id + "]";
	}

}
