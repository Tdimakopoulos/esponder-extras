package eu.esponder.fru.dto.model.crisis.resource;

import java.util.Set;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.fru.dto.model.snapshot.location.AddressDTO;
import eu.esponder.fru.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.fru.dto.model.snapshot.location.PointDTO;

public class OrganisationDTO extends ESponderEntityDTO {

	public OrganisationDTO() { }
	
	private String title;
	
	private Set<OrganisationCategoryDTO> organisationCategories;
	
	private OrganisationDTO parent;
	
	private Set<OrganisationDTO> children;
	
	private AddressDTO address;
	
	private PointDTO location;
	
	private LocationAreaDTO responsibilityArea;
	
	public OrganisationDTO getParent() {
		return parent;
	}

	public void setParent(OrganisationDTO parent) {
		this.parent = parent;
	}

	public Set<OrganisationDTO> getChildren() {
		return children;
	}

	public void setChildren(Set<OrganisationDTO> children) {
		this.children = children;
	}
	
	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public PointDTO getLocation() {
		return location;
	}

	public void setLocation(PointDTO location) {
		this.location = location;
	}

	public LocationAreaDTO getResponsibilityArea() {
		return responsibilityArea;
	}

	public void setResponsibilityArea(LocationAreaDTO responsibilityArea) {
		this.responsibilityArea = responsibilityArea;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<OrganisationCategoryDTO> getOrganisationCategories() {
		return organisationCategories;
	}

	public void setOrganisationCategories(Set<OrganisationCategoryDTO> organisationCategories) {
		this.organisationCategories = organisationCategories;
	}
	
	

}
