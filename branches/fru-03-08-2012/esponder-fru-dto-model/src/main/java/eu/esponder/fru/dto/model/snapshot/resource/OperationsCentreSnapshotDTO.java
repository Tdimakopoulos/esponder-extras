package eu.esponder.fru.dto.model.snapshot.resource;

import eu.esponder.fru.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.fru.dto.model.snapshot.status.OperationsCentreSnapshotStatusDTO;

public class OperationsCentreSnapshotDTO extends SpatialSnapshotDTO {

	private OperationsCentreSnapshotStatusDTO status;

	public OperationsCentreSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(OperationsCentreSnapshotStatusDTO status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "OperationsCentreSnapshotDTO [status=" + status + ", locationArea=" + locationArea + ", id=" + id + "]";
	}
	
}
