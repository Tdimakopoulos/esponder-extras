package eu.esponder.fru.dto.model.crisis.resource;


public enum ResourceStatusDTO {
	AVAILABLE,
	UNAVAILABLE,
	RESERVED,
	ALLOCATED
}
