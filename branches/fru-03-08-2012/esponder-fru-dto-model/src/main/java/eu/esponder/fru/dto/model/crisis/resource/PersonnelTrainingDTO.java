package eu.esponder.fru.dto.model.crisis.resource;

public class PersonnelTrainingDTO extends PersonnelCompetenceDTO {

	private static final long serialVersionUID = 1572773899518146208L;
	
	private PersonnelTrainingTypeDTO personnelTrainingType;

	public PersonnelTrainingTypeDTO getPersonnelTrainingType() {
		return personnelTrainingType;
	}

	public void setPersonnelTrainingType(
			PersonnelTrainingTypeDTO personnelTrainingType) {
		this.personnelTrainingType = personnelTrainingType;
	}

}
