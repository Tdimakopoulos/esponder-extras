package eu.esponder.fru.dto.model.crisis.resource;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.resource.category.PersonnelCategoryDTO;

public abstract class PersonnelCompetenceDTO extends ESponderEntityDTO {

	private String shortTitle;
	
	private PersonnelCategoryDTO personnelCategory;

	public String getShortTitle() {
		return shortTitle;
	}

	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}

	public PersonnelCategoryDTO getPersonnelCategory() {
		return personnelCategory;
	}

	public void setPersonnelCategory(PersonnelCategoryDTO personnelCategory) {
		this.personnelCategory = personnelCategory;
	}
	
}
