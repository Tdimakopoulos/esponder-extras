package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.event.model.DeleteEvent;


public class DeleteActionEvent extends ActionEvent<ActionDTO> implements DeleteEvent {

	private static final long serialVersionUID = -5675665888890540687L;
	
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
