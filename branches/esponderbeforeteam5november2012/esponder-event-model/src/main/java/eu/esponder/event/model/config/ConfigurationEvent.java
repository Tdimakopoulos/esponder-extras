package eu.esponder.event.model.config;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.event.model.ESponderEvent;

public abstract class ConfigurationEvent<T extends ResourceDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = -3867440182566407274L;

}
