package eu.esponder.event.model.snapshot.measurement.statistic;

import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.ESponderEvent;

public abstract class SensorMeasurementStatisticEvent<T extends SensorMeasurementStatisticEnvelopeDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = 1827678852021067614L;

}
