package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateConsumableResourceSnapshotEvent extends ConsumableResourceSnapshotEvent<SnapshotDTO> implements UpdateEvent {

	private static final long serialVersionUID = -3471927216194630496L;
	
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
