package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.event.model.snapshot.SnapshotEvent;


public abstract class ResourceSnapshotEvent<T extends SnapshotDTO> extends SnapshotEvent<T> {

	private static final long serialVersionUID = -6302091892935250120L;
	
}
