package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateOperationsCentreSnapshotEvent extends OperationsCentreSnapshotEvent<OperationsCentreSnapshotDTO> implements UpdateEvent {

	private static final long serialVersionUID = 819365363519771841L;

}
