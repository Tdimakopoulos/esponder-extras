package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.event.model.CreateEvent;


public class CreateActionPartEvent extends ActionPartEvent<ActionPartDTO> implements CreateEvent {

	private static final long serialVersionUID = -3298404952899961147L;
	
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
