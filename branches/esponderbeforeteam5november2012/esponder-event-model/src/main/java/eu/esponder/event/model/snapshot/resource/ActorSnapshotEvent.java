package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;


public abstract class ActorSnapshotEvent<T extends ActorSnapshotDTO> extends ResourceSnapshotEvent<T> {

	private static final long serialVersionUID = -1475358159029683283L;
	
}
