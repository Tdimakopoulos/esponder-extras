package eu.esponder.event.model.snapshot.measurement;

import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;

public abstract class ArithmeticSensorMeasurementEvent<T extends ArithmeticSensorMeasurementDTO> extends SensorMeasurementEvent<T> {

	private static final long serialVersionUID = -3365711964069162650L;

}
