package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateCrisisContextSnapshotEvent extends CrisisContextSnapshotEvent<CrisisContextSnapshotDTO> implements UpdateEvent {

	private static final long serialVersionUID = -49454585587817763L;
	
}
