package eu.esponder.rest.events;

import java.io.IOException;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.events.entity.OsgiEventsEntityDTO;
import eu.esponder.rest.ESponderResource;

@Path("/events/view")
public class OsgiEventsView extends ESponderResource {

	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}
	
	@POST
	@Path("/server/stop")
	public String OsgiServerStop(
			@QueryParam("userID") @NotNull(message = "userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		OsgiSettings pSettings = new OsgiSettings();
		pSettings.LoadSettings();
		String szFilename = pSettings.getSzPropertiesFileName();
		ServerStatus pStatus = new ServerStatus();
		try {
			pStatus.WriteOnFile(pStatus.AppandFileOnPath(pStatus
					.RemoveFilename(szFilename)), "0");
		} catch (IOException e) {
			return "Server cannot be stopped : Error on accessing Server Lock File";
		}
		return "Server will be stopped in the next 5 minutes";
	}

	@POST
	@Path("/server/start")
	public String OsgiServerStart(
			@QueryParam("userID") @NotNull(message = "userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		OsgiSettings pSettings = new OsgiSettings();
		pSettings.LoadSettings();
		String szFilename = pSettings.getSzPropertiesFileName();
		ServerStatus pStatus = new ServerStatus();
		try {
			pStatus.WriteOnFile(pStatus.AppandFileOnPath(pStatus
					.RemoveFilename(szFilename)), "1");
		} catch (IOException e) {
			return "Server cannot be started : Error on accessing Server Lock File";
		}
		return "Server will be started in the next 5 minutes";
	}
	
	//FindByID
	@GET
	@Path("/osgievents/getByID")
	@Produces({MediaType.APPLICATION_JSON})
	public OsgiEventsEntityDTO getOsgiEventsEntityById(
			@QueryParam("osgiEventsEntityDTOID") @NotNull(message="osgiEventsEntityDTOID may not be null" ) Long osgiEventsEntityDTOID,
			@QueryParam("userID") @NotNull(message="userID may not be null" ) String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		return this.getOsgiEventsEntityRemoteService().findOsgiEventsEntityByIdRemote(osgiEventsEntityDTOID);
	}

	//FindBySeverity
	@GET
	@Path("/osgievents/getBySeverity")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getOsgiEventsEntityBySeverity(
			@QueryParam("osgiEventsEntitySeverity") @NotNull(message="osgiEventsEntitySeverity may not be null" ) String osgiEventsEntitySeverity,
			@QueryParam("userID") @NotNull(message="userID may not be null" ) String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		List<OsgiEventsEntityDTO> pResults=this.getOsgiEventsEntityRemoteService().findOsgiEventsEntitiesBySeverityRemote(osgiEventsEntitySeverity);
		ResultListDTO pReturn= new ResultListDTO();
		pReturn.setResultList(pResults);
		return pReturn;
	}

	//FindBySourceId
	@GET
	@Path("/osgievents/getBySourceId")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getOsgiEventsEntityBySourceid(
			@QueryParam("osgiEventsEntitySourceid") @NotNull(message="osgiEventsEntitySourceid may not be null" ) Long osgiEventsEntitySourceid,
			@QueryParam("userID") @NotNull(message="userID may not be null" ) String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		return new ResultListDTO(this.getOsgiEventsEntityRemoteService().findOsgiEventsEntitiesBySourceIdRemote(osgiEventsEntitySourceid));
	}

	// create OsgiEventsEntity
	@POST
	@Path("/osgievents/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public OsgiEventsEntityDTO createOsgiEventsEntity(@NotNull(message="osgiEventsEntityDTO may not be null" ) OsgiEventsEntityDTO osgiEventsEntityDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null" ) String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		return this.getOsgiEventsEntityRemoteService().createOsgiEventsEntityRemote(osgiEventsEntityDTO, userID);
	}


	// Update OsgiEventsEntity
	@PUT
	@Path("/osgievents/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public OsgiEventsEntityDTO updateOsgiEventsEntity(@NotNull(message="osgiEventsEntityDTO may not be null" ) OsgiEventsEntityDTO osgiEventsEntityDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null" ) String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		return this.getOsgiEventsEntityRemoteService().updateOsgiEventsEntityRemote(osgiEventsEntityDTO, userID);
	}

	// Delete OsgiEventsEntity
	@DELETE
	@Path("/osgievents/delete")
	public Long deleteOsgiEventsEntityDTO(
			@QueryParam("osgiEventsEntityDTOID") @NotNull(message="osgiEventsEntityDTOID may not be null" ) Long osgiEventsEntityDTOID,
			@QueryParam("userID") @NotNull(message="userID may not be null" ) String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		this.getOsgiEventsEntityRemoteService().deleteOsgiEventsEntityRemote(osgiEventsEntityDTOID, userID);
		return osgiEventsEntityDTOID;
	}

}
