package eu.esponder.rest.geolocation.calculations;

import eu.esponder.rest.geolocation.lib.Ellipsoid;
import eu.esponder.rest.geolocation.lib.GeodeticCalculator;
import eu.esponder.rest.geolocation.lib.GlobalCoordinates;

public class DirectLineCalculations2D {
	
	public GlobalCoordinates TwoDimensionalDirectCalculation(double lat1,double lon1,double lat2,double lon2, double step)
	   {
	     // instantiate the calculator
	     GeodeticCalculator geoCalc = new GeodeticCalculator();

	     // select a reference elllipsoid
	     Ellipsoid reference = Ellipsoid.WGS84;

	     // set Lincoln Memorial coordinates
	     GlobalCoordinates startingPoint;
	     startingPoint = new GlobalCoordinates( lat1, lon1 );

	     // set the direction and distance
	     BearingCalculation pbearing = new BearingCalculation();
	     double startBearing = pbearing.bearingDeg(lat1,lon1,lat2,lon2);
	     double distance = step;

	     // find the destination
	     double[] endBearing = new double[1];
	     GlobalCoordinates dest = geoCalc.calculateEndingGlobalCoordinates(reference, startingPoint, startBearing, distance, endBearing);
	     return dest;
	   }

	public double distFrom(float lat1, float lng1, float lat2, float lng2) {
	    double earthRadius = 3958.75;
	    double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(lng2-lng1);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	               Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
	               Math.sin(dLng/2) * Math.sin(dLng/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;

	    int meterConversion = 1609;

	    return dist * meterConversion;
	    }
}
