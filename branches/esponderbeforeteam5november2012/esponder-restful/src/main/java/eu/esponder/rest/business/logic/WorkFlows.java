package eu.esponder.rest.business.logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.time.DateUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.SensorResultListDTO;
import eu.esponder.dto.model.SensorSnapshotDetailsList;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionExpressionEnumDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.CrisisContextSnapshotStatusDTO;
import eu.esponder.dto.model.type.ActorTypeDTO;
import eu.esponder.event.model.snapshot.CreateCrisisContextSnapshotEvent;
import eu.esponder.model.snapshot.Period;
import eu.esponder.osgi.service.event.ESponderEventPublisher;
import eu.esponder.rest.ESponderResource;

@Path("/business/logic")
public class WorkFlows extends ESponderResource{

	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}
	
	@GET
	@Path("/crisis/associateActor")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO associateActor(
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		Long userID=SecurityCheck(szuserID);

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.RESERVED);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorDTO actor = new ActorDTO();
		actor.setPersonnel(personnelUpdated);
		actor.setStatus(ResourceStatusDTO.AVAILABLE);
		ActorTypeDTO actorType;
		try {
			actorType = (ActorTypeDTO) this.getTypeRemoteService().findDTOByTitle("INIT_FR");
			actor.setType(actorType.getTitle());
		} catch (ClassNotFoundException e) {
			System.out.println("Error getting Initial Actor Type");
		}

		actor.setTitle(personnel.getTitle());
		actor = this.getActorRemoteService().createActorRemote(actor, userID);

		return actor;
	}


	@GET
	@Path("/crisis/deassociateActor")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO deassociateActor(
			@QueryParam("actorID") @NotNull(message="actorID may not be null") Long actorID,
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		Long userID=SecurityCheck(szuserID);

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.AVAILABLE);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorDTO actor = this.getActorRemoteService().findByIdRemote(actorID);
		actor.setPersonnel(personnelUpdated);
		actor.setStatus(ResourceStatusDTO.UNAVAILABLE);
		actor = this.getActorRemoteService().updateActorRemote(actor, userID);
		return actor;
	}


	@GET
	@Path("/crisis/associateReusable")
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceDTO associateReusable(
			@QueryParam("crisisContextID") @NotNull(message="Crisis Context ID may not be null") Long crisisContextID,
			@QueryParam("regReusableID") @NotNull(message="Registered Reusable Resource ID may not be null") Long regreusableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		RegisteredReusableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredReusableResourceByIdRemote(regreusableID);

		ReusableResourceDTO reusableResource = new ReusableResourceDTO();
		reusableResource.setCrisisContextId(crisisContextID);
		reusableResource.setQuantity(regResourceDTO.getQuantity());
		reusableResource.setTitle(regResourceDTO.getTitle());
		reusableResource.setStatus(ResourceStatusDTO.AVAILABLE);
		ReusableResourceCategoryDTO reusableCategoryDTO = (ReusableResourceCategoryDTO) this.getResourceCategoryRemoteService().
				findByIdRemote(ReusableResourceCategoryDTO.class, regResourceDTO.getReusableResourceCategoryId()); 
		reusableResource.setReusableResourceCategory(reusableCategoryDTO);
		ReusableResourceDTO reusableResourcep = this.getLogisticsRemoteService().createReusableResourceRemote(reusableResource, userID);


		regResourceDTO.setStatus(ResourceStatusDTO.RESERVED);
		this.getLogisticsRemoteService().updateRegisteredReusableResourceRemote(regResourceDTO, userID);

		return reusableResourcep;
	}

	@GET
	@Path("/crisis/deassociateReusable")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredReusableResourceDTO deassociateReusable(
			@QueryParam("reusableID") @NotNull(message="Reusable Resource ID may not be null") Long reusableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		ReusableResourceDTO reusableResource = this.getLogisticsRemoteService().findReusableResourceByIdRemote(reusableID);
		RegisteredReusableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredReusableResourceByTitleRemote(reusableResource.getTitle());

		regResourceDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		this.getLogisticsRemoteService().updateRegisteredReusableResourceRemote(regResourceDTO, userID);
		this.getLogisticsRemoteService().deleteReusableResourceRemote(reusableID, userID);
		return regResourceDTO;
	}

	@GET
	@Path("/crisis/associateConsumable")
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceDTO associateConsumable(
			@QueryParam("crisisContextID") @NotNull(message="Crisis Context ID may not be null") Long crisisContextID,
			@QueryParam("regConsumableID") @NotNull(message="Registered Consumable Resource ID may not be null") Long regconsumableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		RegisteredConsumableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredConsumableResourceByIdRemote(regconsumableID);

		ConsumableResourceDTO consumableResource = new ConsumableResourceDTO();
		consumableResource.setCrisisContextId(crisisContextID);
		consumableResource.setQuantity(regResourceDTO.getQuantity());
		consumableResource.setTitle(regResourceDTO.getTitle());
		consumableResource.setStatus(ResourceStatusDTO.AVAILABLE);
		ConsumableResourceCategoryDTO consumableCategoryDTO = (ConsumableResourceCategoryDTO) this.getResourceCategoryRemoteService().
				findByIdRemote(ConsumableResourceCategoryDTO.class, regResourceDTO.getConsumableResourceCategoryId()); 
		consumableResource.setConsumableResourceCategory(consumableCategoryDTO);
		ConsumableResourceDTO consumableResourcep=this.getLogisticsRemoteService().createConsumableResourceRemote(consumableResource, userID);


		regResourceDTO.setStatus(ResourceStatusDTO.RESERVED);
		this.getLogisticsRemoteService().updateRegisteredConsumableResourceRemote(regResourceDTO, userID);

		return consumableResourcep;
	}

	@GET
	@Path("/crisis/deassociateConsumable")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredConsumableResourceDTO deassociateConsumable(
			@QueryParam("consumableID") @NotNull(message="Consumable Resource ID may not be null") Long consumableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		ConsumableResourceDTO consumableResource = this.getLogisticsRemoteService().findConsumableResourceByIdRemote(consumableID);
		RegisteredConsumableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredConsumableResourceByTitleRemote(consumableResource.getTitle());

		regResourceDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		this.getLogisticsRemoteService().updateRegisteredConsumableResourceRemote(regResourceDTO, userID);
		this.getLogisticsRemoteService().deleteConsumableResourceRemote(consumableID, userID);
		return regResourceDTO;
	}

	@GET
	@Path("/crisis/associateoc")
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreDTO associateOC(
			@QueryParam("ccID") @NotNull(message="Crisis Context Id may not be null") Long ccID,
			@QueryParam("regocID") @NotNull(message="Registerd Operations Centre ID may not be null") Long regocID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		RegisteredOperationsCentreDTO regOC = this.getOperationsCentreRemoteService().findRegisteredOperationCentreByIdRemote(regocID);

		OperationsCentreDTO oc = new OperationsCentreDTO();
		oc.setCrisisContextId(ccID);
		oc.setTitle(regOC.getTitle());
		oc.setType(regOC.getType());
		oc.setOperationsCentreCategoryId(regOC.getOperationsCentreCategoryId());
		oc.setVoIPURL(regOC.getVoIPURL());
		oc.setStatus(ResourceStatusDTO.AVAILABLE);

		OperationsCentreDTO ocp = this.getOperationsCentreRemoteService().createOperationsCentreRemote(oc, userID);

		regOC.setStatus(ResourceStatusDTO.RESERVED);
		this.getOperationsCentreRemoteService().updateRegisteredOperationsCentreRemote(regOC, userID);

		return ocp;
	}

	@GET
	@Path("/crisis/deassociateoc")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredOperationsCentreDTO deassociateOC(
			@QueryParam("ocID") @NotNull(message="Operations Centre ID may not be null") Long ocID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		OperationsCentreDTO oc = this.getOperationsCentreRemoteService().findOperationCentreByIdRemote(ocID);
		RegisteredOperationsCentreDTO regOC = this.getOperationsCentreRemoteService().findRegisteredOperationsCentreByTitleRemote(oc.getTitle());
		regOC.setStatus(ResourceStatusDTO.AVAILABLE);
		regOC = this.getOperationsCentreRemoteService().updateRegisteredOperationsCentreRemote(regOC, userID);
		this.getOperationsCentreRemoteService().deleteOperationsCentreRemote(ocID, userID);
		return regOC;
	}



	@POST
	@Path("/crisis/createcrisissnapshot")
	@Produces({ MediaType.APPLICATION_JSON })
	public CrisisContextSnapshotDTO CreateCrisisContextSnapshots(

			@QueryParam("ccID") @NotNull(message = "Crisis Context Id may not be null") Long ccID,
			@QueryParam("LocationTitle") @NotNull(message = "Location area Title may not be null") String szLocationAreaTitle,
			@QueryParam("userID") @NotNull(message = "userID may not be null") String szuserID) {


		Long userID=SecurityCheck(szuserID);
		
		CrisisContextSnapshotDTO crisisContextSnapshotPersisted = this.getCrisisRemoteService().createCrisisContextSnapshotRemote(
				CreateContextSnap(ccID, szLocationAreaTitle, szuserID), userID);

		CreateCrisisContextSnapshotEvent crisisContextSnapshotEvent = new CreateCrisisContextSnapshotEvent();
		crisisContextSnapshotEvent.setEventAttachment(crisisContextSnapshotPersisted);
		crisisContextSnapshotEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
		crisisContextSnapshotEvent.setEventTimestamp(new Date());
		crisisContextSnapshotEvent.setJournalMessage(crisisContextSnapshotEvent.getJournalMessageInfo());
		ActorDTO subActorDTO = this.getActorRemoteService().findByIdRemote(userID);
		crisisContextSnapshotEvent.setEventSource(subActorDTO);

		ESponderEventPublisher<CreateCrisisContextSnapshotEvent> publisher = new ESponderEventPublisher<CreateCrisisContextSnapshotEvent>(CreateCrisisContextSnapshotEvent.class);
		try {
			publisher.publishEvent(crisisContextSnapshotEvent);
		} catch (EventListenerException e) {
			e.printStackTrace();
		}
		publisher.CloseConnection();

		return crisisContextSnapshotPersisted;

	}

	@GET
	@Path("/sensorsnapshots/getAllSnapshotsByAllActors")
	@Produces({ MediaType.APPLICATION_JSON })
	public SensorResultListDTO LoadSensorsSnapshots(
			@QueryParam("userID") @NotNull(message = "userID may not be null") String szuserID)
					throws JsonParseException, JsonMappingException, IOException, ClassNotFoundException {

		Long userID=SecurityCheck(szuserID);
		// intialize return variable
		List<SensorSnapshotDetailsList> pSensorsSnapshots = new ArrayList<SensorSnapshotDetailsList>();

		// Get all actors
		List<ActorDTO> actorsList = this.getActorRemoteService().findAllActorsRemote();

		for(ActorDTO actor : actorsList) {
			for(EquipmentDTO equipment : actor.getEquipmentSet()) {
				for(SensorDTO sensorDTO : equipment.getSensors()) {

					List<SensorSnapshotDTO> sensorSnapshots = (List<SensorSnapshotDTO>) this.getSensorRemoteService().findAllSensorSnapshotsBySensorRemote(sensorDTO, 10);
					for(SensorSnapshotDTO sensorSnapshotDTO : sensorSnapshots) {
						SensorSnapshotDetailsList pItem = new SensorSnapshotDetailsList();
						pItem.setpActor(actor);
						pItem.setpEquipment(equipment);
						pItem.setpSensorSnapshot(sensorSnapshotDTO);
						pSensorsSnapshots.add(pItem);
					}
				}
			}
		}
		return new SensorResultListDTO(pSensorsSnapshots);
	}

	@GET
	@Path("/sensorsnapshots/getAllSnapshotsByActor")
	@Produces({ MediaType.APPLICATION_JSON })
	public SensorResultListDTO LoadSensorsSnapshots(
			@QueryParam("actorID") @NotNull(message = "actorID may not be null") Long actorID,
			@QueryParam("userID") @NotNull(message = "userID may not be null") String szuserID)
					throws JsonParseException, JsonMappingException, IOException, ClassNotFoundException {

		Long userID=SecurityCheck(szuserID);
		// intialize return variable
		List<SensorSnapshotDetailsList> pSensorsSnapshots = new ArrayList<SensorSnapshotDetailsList>();

		// Get all actors
		List<ActorDTO> actorsList = this.getActorRemoteService().findAllActorsRemote();
		ActorDTO actorDTO = this.getActorRemoteService().findByIdRemote(actorID);

		for(EquipmentDTO equipment : actorDTO.getEquipmentSet()) {
			for(SensorDTO sensorDTO : equipment.getSensors()) {

				List<SensorSnapshotDTO> sensorSnapshots = (List<SensorSnapshotDTO>) this.getSensorRemoteService().findAllSensorSnapshotsBySensorRemote(sensorDTO, 10);
				for(SensorSnapshotDTO sensorSnapshotDTO : sensorSnapshots) {
					SensorSnapshotDetailsList pItem = new SensorSnapshotDetailsList();
					pItem.setpActor(actorDTO);
					pItem.setpEquipment(equipment);
					pItem.setpSensorSnapshot(sensorSnapshotDTO);
					pSensorsSnapshots.add(pItem);
				}
			}
		}
		return new SensorResultListDTO(pSensorsSnapshots);
	}
	
	@GET
	@Path("/sensorsnapshots/getAllActorsByCrisisContext")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO LoadActorsByCrisisContext(
			@QueryParam("crisisContextID") @NotNull(message = "crisisContextID may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message = "userID may not be null") String szuserID)
					throws JsonParseException, JsonMappingException, IOException, ClassNotFoundException {

		Long userID=SecurityCheck(szuserID);
		CrisisContextDTO crisisContextDTO = this.getCrisisRemoteService().findCrisisContextDTOById(crisisContextID);
		List<ActorDTO> actorsList = new ArrayList<ActorDTO>();
		for(OperationsCentreDTO oc : crisisContextDTO.getOperationsCentres()) {
			actorsList.addAll(oc.getActors());
		}
		
		return new ResultListDTO(actorsList);
	}


	private CrisisContextSnapshotDTO CreateContextSnap(Long ccID,
			String szLocationAreaTitle, String szuserID) {
		CrisisContextSnapshotDTO crisisContextSnapshotDTO = new CrisisContextSnapshotDTO();
		CrisisContextSnapshotStatusDTO SnapshotStatus = CrisisContextSnapshotStatusDTO.STARTED;
		PeriodDTO period = createPeriodDTO(10);

		Long userID=SecurityCheck(szuserID);
		crisisContextSnapshotDTO.setCrisisContext(this.getCrisisRemoteService()
				.findCrisisContextDTOById(ccID));// .crisisService.findCrisisContextDTOByTitle("Fire Brigade Drill"));
		// crisisContextSnapshotDTO.setLocationArea(this.createSphereDTO(38.025334,
		// 23.802717, null, RADIUS, "SnapshotLoc3"));

		LocationAreaDTO plocation = GetLocation(szLocationAreaTitle);
		crisisContextSnapshotDTO.setLocationArea(plocation);
		crisisContextSnapshotDTO.setPeriod(period);
		crisisContextSnapshotDTO.setStatus(SnapshotStatus);
		return crisisContextSnapshotDTO;

	}

	@SuppressWarnings("unchecked")
	private LocationAreaDTO GetLocation(String szLocationAreaTitle) {
		LocationAreaDTO areaDTO1 = null;

		List<LocationAreaDTO> results = null;
		try {
			results = (List<LocationAreaDTO>) this.getGenericRemoteService().getDTOEntities(LocationAreaDTO.class.getName(),new EsponderCriterionDTO("title",	EsponderCriterionExpressionEnumDTO.EQUAL,szLocationAreaTitle), 10, 0);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!results.isEmpty() && results.size() < 2) {
			areaDTO1 = results.get(0);
		}
		return areaDTO1;
	}

	protected PeriodDTO createPeriodDTO(int seconds) {
		java.util.Date now = new java.util.Date();
		Period period = new Period(now.getTime(), now.getTime()+new Long(seconds));
		PeriodDTO periodDTO = (PeriodDTO) this.getMappingRemoteService().mapObject(period, PeriodDTO.class);
		return periodDTO;
	}
	
}