package eu.esponder.rest.crisis;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.dto.model.snapshot.ReferencePOISnapshotDTO;
import eu.esponder.dto.model.snapshot.SketchPOISnapshotDTO;
import eu.esponder.rest.ESponderResource;

@Path("/crisis/view")
public class CrisisViewResource extends ESponderResource {

	//TODO Create OperationsCentres Web Services
	
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}
	
	@GET
	@Path("/sketch/read")
	@Produces({MediaType.APPLICATION_JSON})
	public List<SketchPOIDTO> readSketches(
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID, 
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findSketchPOIRemote(operationsCentreID);
	}
	
	@GET
	@Path("/sketch/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO readSketchById(
			@QueryParam("sketchPOIId") @NotNull(message="sketchPOIId may not be null") Long sketchPOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		
		SketchPOIDTO sketchPOIDTO = this.getOperationsCentreRemoteService().findSketchPOIByIdRemote(sketchPOIId);
		return sketchPOIDTO;
	}
	
	@GET
	@Path("/sketch/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO readSketchByTitle(
			@QueryParam("sketchPOITitle") @NotNull(message="sketchPOITitle may not be null") String sketchPOITitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		SketchPOIDTO sketchPOIDTO = this.getOperationsCentreRemoteService().findSketchPOIByTitleRemote(sketchPOITitle);
		return sketchPOIDTO;
	}
	
	@POST
	@Path("/sketch/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO createSketchReturning(
			@NotNull(message="sketch object may not be null") SketchPOIDTO sketchDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().createSketchPOIRemote(operationsCentreDTO, sketchDTO, userID);
		}
		return null;
	}
	
	
	@PUT
	@Path("/sketch/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO updateSketchPOI(
			@NotNull(message="sketch object may not be null") SketchPOIDTO sketchDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().updateSketchPOIRemote(operationsCentreDTO, sketchDTO, userID);
		}
		return null;
	}
	
	
	@DELETE
	@Path("/sketch/delete")
	public Long deleteSketchPOI(
			@QueryParam("sketchPOIId") @NotNull(message="SketchPOIId may not be null") Long sketchPOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		this.getOperationsCentreRemoteService().deleteSketchPOIRemote(sketchPOIId, userID);
		return sketchPOIId;
	}
	
	
	@GET
	@Path("/ref/read")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ReferencePOIDTO> readReferencePOIs(
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID, 
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findReferencePOIsRemote(operationsCentreID);
	}
	
	
	@GET
	@Path("/ref/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO readReferenceById(
			@QueryParam("referencePOIId") @NotNull(message="referencePOIId may not be null") Long referencePOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findReferencePOIByIdRemote(referencePOIId);
	}
	
	@GET
	@Path("/ref/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO readReferenceByTitle(
			@QueryParam("referencePOITitle") @NotNull(message="referencePOITitle may not be null") String referencePOITitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findReferencePOIByTitleRemote(referencePOITitle);
	}
	
	@POST
	@Path("/ref/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO createReferencePOIReturning(
			@NotNull(message="referencePOI object may not be null") ReferencePOIDTO referencePOIDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().createReferencePOIRemote(operationsCentreDTO, referencePOIDTO, userID);
		}
		return null;
	}
	
	
	@PUT
	@Path("/ref/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO updateReferencePOI(
			@NotNull(message="sketch object may not be null") ReferencePOIDTO referencePOIDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().updateReferencePOIRemote(operationsCentreDTO, referencePOIDTO, userID);
		}
		return null;
	}
	
	
	@DELETE
	@Path("/ref/delete")
	public Long deletereferencePOI(
			@QueryParam("referencePOIId") @NotNull(message="referencePOIId may not be null") Long referencePOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		this.getOperationsCentreRemoteService().deleteReferencePOIRemote(referencePOIId, userID);
		return referencePOIId;
	}
	
//	//Sketch Snapshots
	@POST
	@Path("/sketch/snapshot/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOISnapshotDTO createSketchSnapshotPOI(
			@NotNull(message="sketchPOISnapshot object may not be null") SketchPOISnapshotDTO snapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		SketchPOISnapshotDTO sketchPOISnapshotDTO = this.getOperationsCentreRemoteService().createSketchPOISnapshotRemote(snapshotDTO, userID);
		return sketchPOISnapshotDTO;
	}
	
	@GET
	@Path("/sketch/snapshot/findbyid")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOISnapshotDTO readSketchSnapshotPOIbyid(
			@QueryParam("snapshotsketchID") @NotNull(message="snapshotsketchID may not be null") Long snapshotsketchID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findSketchPOISnapshotByIdRemote(snapshotsketchID);
	}
	
	@GET
	@Path("/sketch/snapshot/findall")
	@Produces({MediaType.APPLICATION_JSON})
	public List<SketchPOISnapshotDTO> readSketchSnapshotPOIall(
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findAllSketchPOISnapshotsRemote();
	}
	
	//causing problems in deployment
	@GET
	@Path("/sketch/snapshot/findbysketchpoi")
	@Produces({MediaType.APPLICATION_JSON})
	public List<SketchPOISnapshotDTO> readSketchSnapshotPOIbyPOI(
			@QueryParam("sketchPOI") @NotNull(message="sketchPOI may not be null") Long sensorDTO,
			@QueryParam("resultLimit") @NotNull(message="resultLimit may not be null") int resultLimit,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findAllSketchPOISnapshotsByPoiRemote(sensorDTO, resultLimit);
	}
	
	@GET
	@Path("/sketch/snapshot/findbydate")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOISnapshotDTO readSketchSnapshotPOIbydate(
			@QueryParam("sketchID") @NotNull(message="sketchPOI may not be null") Long sketchID,
			@QueryParam("dateTo") @NotNull(message="resultLimit may not be null") Long dDate,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		Date xDate=new Date();
		xDate.setTime(dDate);
		return this.getOperationsCentreRemoteService().findSketchPOISnapshotByDateRemote(sketchID, xDate);
	}
	
	@GET
	@Path("/sketch/snapshot/findprevious")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOISnapshotDTO readSketchSnapshotPOIbydate(
			@QueryParam("sketchID") @NotNull(message="sketchPOI may not be null") Long sketchID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);

		return this.getOperationsCentreRemoteService().findPreviousSketchPOISnapshotRemote(sketchID);
	}


	@PUT
	@Path("/sketch/snapshot/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOISnapshotDTO updateSketchSnapshotPOI(
			@NotNull(message="sketch object may not be null") SketchPOISnapshotDTO sensorSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		
			return this.getOperationsCentreRemoteService().updateSketchPOISnapshotRemote(sensorSnapshotDTO, userID);
		
	}

	@DELETE
	@Path("/sketch/snapshot/delete")
	public void deletesketchsnapshotPOI(
			@QueryParam("sketchSnapshotId") @NotNull(message="sketchSnapshotId may not be null") Long sketchSnapshotId,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		this.getOperationsCentreRemoteService().deleteSketchPOISnapshotRemote(sketchSnapshotId, userID);
		
	}
	
//	
//	//Ref Snapshots
	@POST
	@Path("/ref/snapshot/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOISnapshotDTO createReferencePOISnapshot(
			@NotNull(message="snapshotDTO object may not be null") ReferencePOISnapshotDTO snapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		ReferencePOISnapshotDTO sketchPOISnapshotDTO = this.getOperationsCentreRemoteService().createReferencePOISnapshotRemote(snapshotDTO, userID);
		return sketchPOISnapshotDTO;
	}
	
	@GET
	@Path("/ref/snapshot/findbyid")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOISnapshotDTO readReferencePOISnapshotbyid(
			@QueryParam("snapshotRefID") @NotNull(message="snapshotsketchID may not be null") Long snapshotrefID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findReferencePOISnapshotByIdRemote(snapshotrefID);
	}
	
	@GET
	@Path("/ref/snapshot/findall")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ReferencePOISnapshotDTO> readReferencePOISnapshotPOIall(
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findAllReferencePOISnapshotsRemote();
	}
	
	@GET
	@Path("/ref/snapshot/findbypoi")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ReferencePOISnapshotDTO> readReferencePOISnapshotbyPOI(
			@QueryParam("referencePOIid") @NotNull(message="sketchPOI may not be null") Long sensorDTO,
			@QueryParam("resultLimit") @NotNull(message="resultLimit may not be null") int resultLimit,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) throws ClassNotFoundException {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findAllReferencePOISnapshotsByReferencePOIRemote(sensorDTO, resultLimit);
	}
	
	@GET
	@Path("/ref/snapshot/findbydate")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOISnapshotDTO readReferencePOISnapshotbydate(
			@QueryParam("referencePOIID") @NotNull(message="sketchPOI may not be null") Long sketchID,
			@QueryParam("dateTo") @NotNull(message="resultLimit may not be null") Long dDate,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		Date xDate=new Date();
		xDate.setTime(dDate);
		return this.getOperationsCentreRemoteService().findReferencePOISnapshotByDateRemote(sketchID, xDate);
	}
	
	@GET
	@Path("/ref/snapshot/findprevious")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOISnapshotDTO readReferencePOISnapshotbydate(
			@QueryParam("referencePOIID") @NotNull(message="sketchPOI may not be null") Long sketchID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);

		return this.getOperationsCentreRemoteService().findPreviousReferencePOISnapshotRemote(sketchID);
	}


	@PUT
	@Path("/ref/snapshot/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOISnapshotDTO updateReferencePOISnapshot(
			@NotNull(message="referencePOI object may not be null") ReferencePOISnapshotDTO sensorSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		
			return this.getOperationsCentreRemoteService().updateReferencePOISnapshotRemote(sensorSnapshotDTO, userID);
		
	}

	@DELETE
	@Path("/ref/snapshot/delete")
	public void deleteReferencePOISnapshot(
			@QueryParam("referenceSnapshotId") @NotNull(message="sketchSnapshotId may not be null") Long sketchSnapshotId,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		this.getOperationsCentreRemoteService().deleteReferencePOISnapshotRemote(sketchSnapshotId, userID);
		
	}

}
