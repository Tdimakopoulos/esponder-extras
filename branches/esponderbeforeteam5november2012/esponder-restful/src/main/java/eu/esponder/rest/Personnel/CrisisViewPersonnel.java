package eu.esponder.rest.Personnel;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.rest.ESponderResource;

@Path("/crisis/view")
public class CrisisViewPersonnel extends ESponderResource {

	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}
	
	@GET
	@Path("/personnel/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelDTO readPersonnelById(
			@QueryParam("personnelID") @NotNull(message="personnelId may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		PersonnelDTO personnelDTO = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		return personnelDTO;
	}
	
	@GET
	@Path("/personnel/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllPersonnel(
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		return new ResultListDTO(this.getPersonnelRemoteService().findAllPersonnelRemote());
		
	}

	@GET
	@Path("/personnelCompetence/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelCompetenceDTO readPersonnelCompetenceById(
			@QueryParam("personnelCompetenceID") @NotNull(message="personnelCompetenceId may not be null") Long personnelCompetenceID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		PersonnelCompetenceDTO personnelCompetenceDTO = this.getPersonnelRemoteService().findPersonnelCompetenceByIdRemote(personnelCompetenceID);
		return personnelCompetenceDTO;
	}

	@GET
	@Path("/personnel/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelDTO readPersonnelByTitle(
			@QueryParam("personnelTitle") @NotNull(message="personnelTitle may not be null") String personnelTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		PersonnelDTO personnelDTO = this.getPersonnelRemoteService().findPersonnelByTitleRemote(personnelTitle);
		return personnelDTO;
	}

	@GET
	@Path("/personnelCompetence/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelCompetenceDTO readPersonnelCompetenceByTitle(
			@QueryParam("personnelCompetenceTitle") @NotNull(message="personnelCompetenceTitle may not be null") String personnelCompetenceTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		PersonnelCompetenceDTO personnelCompetenceDTO = this.getPersonnelRemoteService().findPersonnelCompetenceByTitleRemote(personnelCompetenceTitle);
		return personnelCompetenceDTO;
	}

	@POST
	@Path("/personnel/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelDTO createPersonnel(
			@NotNull(message="Personnel object may not be null") PersonnelDTO personnelDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		PersonnelDTO PersonnelDTOPersisted = this.getPersonnelRemoteService().createPersonnelRemote(personnelDTO, userID);
		return PersonnelDTOPersisted;
	}

	@POST
	@Path("/personnelCompetence/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelCompetenceDTO createPersonnelCompetence(
			@NotNull(message="PersonnelCompetence object may not be null") PersonnelCompetenceDTO personnelCompetenceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) throws ClassNotFoundException {

		Long userID=SecurityCheck(szuserID);
		
		PersonnelCompetenceDTO PersonnelCompetenceDTOPersisted = this.getPersonnelRemoteService().createPersonnelCompetenceRemote(personnelCompetenceDTO, userID);
		return PersonnelCompetenceDTOPersisted;
	}

	@PUT
	@Path("/personnel/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelDTO updatePersonnel(
			@NotNull(message="Personnel object may not be null") PersonnelDTO personnelDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		PersonnelDTO PersonnelDTOPersisted = this.getPersonnelRemoteService().updatePersonnelRemote(personnelDTO, userID);
		return PersonnelDTOPersisted;
	}

	@PUT
	@Path("/personnelCompetence/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelCompetenceDTO updatePersonnelCompetence(
			@NotNull(message="PersonnelCompetence object may not be null") PersonnelCompetenceDTO personnelCompetenceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) throws ClassNotFoundException {

		Long userID=SecurityCheck(szuserID);
		
		PersonnelCompetenceDTO PersonnelCompetenceDTOPersisted = this.getPersonnelRemoteService().updatePersonnelCompetenceRemote(personnelCompetenceDTO, userID);
		return PersonnelCompetenceDTOPersisted;
	}
	
	@DELETE
	@Path("/personnel/delete")
	@Produces({MediaType.APPLICATION_JSON})
	public Long deletePersonnelById(
			@QueryParam("personnelID") @NotNull(message="personnelId may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		this.getPersonnelRemoteService().deletePersonnelByIdRemote(personnelID);
		return personnelID;
	}

	@DELETE
	@Path("/personnelCompetence/delete")
	@Produces({MediaType.APPLICATION_JSON})
	public Long deletePersonnelCompetenceById(
			@QueryParam("personnelCompetenceID") @NotNull(message="personnelCompetenceId may not be null") Long personnelCompetenceID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {

		Long userID=SecurityCheck(szuserID);
		
		this.getPersonnelRemoteService().deletePersonnelCompetenceByIdRemote(personnelCompetenceID);
		return personnelCompetenceID;
	}
	
}
