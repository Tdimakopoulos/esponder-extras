package eu.esponder.rest;

import javax.naming.NamingException;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.LogisticsRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.events.EventsEntityRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.util.ejb.ServiceLocator;

public abstract class ESponderResource {

	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	//*******************************************************************************

	protected SensorRemoteService getSensorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected ActorRemoteService getActorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected EquipmentRemoteService getEquipmentRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EquipmentBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected ESponderRemoteMappingService getMappingRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected OperationsCentreRemoteService getOperationsCentreRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/OperationsCentreBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	protected ActionRemoteService getActionRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	protected GenericRemoteService getGenericRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected ESponderConfigurationRemoteService getConfigurationRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderConfigurationBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected UserRemoteService getUserRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/UserBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected PersonnelRemoteService getPersonnelRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/PersonnelBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected LogisticsRemoteService getLogisticsRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/LogisticsBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected EventsEntityRemoteService getOsgiEventsEntityRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EventsEntityBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected ResourceCategoryRemoteService getResourceCategoryRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ResourceCategoryBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected TypeRemoteService getTypeRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected CrisisRemoteService getCrisisRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/CrisisBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

}
