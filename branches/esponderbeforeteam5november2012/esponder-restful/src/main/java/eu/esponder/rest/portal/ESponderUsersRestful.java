package eu.esponder.rest.portal;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.rest.ESponderResource;

@Path("/portal/users")
public class ESponderUsersRestful extends ESponderResource {

	@GET
	@Path("/findById")
	@Produces({ MediaType.APPLICATION_JSON })
	public ESponderUserDTO readAUserById(
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID) throws ClassNotFoundException {

		return this.getUserRemoteService().findUserByIdRemote(userID);
	}
	
	@GET
	@Path("/findByTitle")
	@Produces({ MediaType.APPLICATION_JSON })
	public ESponderUserDTO readAUserByuserName(
			@QueryParam("userName") @NotNull(message = "userID may not be null") String userName,
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID) throws ClassNotFoundException {

		return this.getUserRemoteService().findUserByNameRemote(userName);
	}
	
	@GET
	@Path("/findAll")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO readAllUsers(
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID) throws ClassNotFoundException {

		List<ESponderUserDTO> pResults = this.getUserRemoteService().findAllUsersRemote();
		return new ResultListDTO(pResults);

	}
	
	@POST
	@Path("/createUser")
	@Produces({ MediaType.APPLICATION_JSON })
	public ESponderUserDTO createUser(
			@NotNull(message = "ESponder User may not be null") ESponderUserDTO esponderUSer,
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID) throws ClassNotFoundException {

		return this.getUserRemoteService().createUserRemote(esponderUSer, userID);
	}
	
	@PUT
	@Path("/updateUser")
	@Produces({ MediaType.APPLICATION_JSON })
	public ESponderUserDTO updateUser(
			@NotNull(message = "ESponder User may not be null") ESponderUserDTO esponderUSer,
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID) throws ClassNotFoundException {

		return this.getUserRemoteService().updateUserRemote(esponderUSer, userID);
	}
	
	@DELETE
	@Path("/deleteUser")
	@Produces({ MediaType.APPLICATION_JSON })
	public Long deleteUser(
			@QueryParam("deletedUserID") @NotNull(message = "ESponder User id to be deleted may not be null") Long deletedUserID,
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID) throws ClassNotFoundException {

		this.getUserRemoteService().deleteUserRemote(deletedUserID, userID);
		return deletedUserID; 
	}
	
}