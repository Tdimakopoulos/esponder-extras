package eu.esponder.rest.generic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionExpressionEnumDTO;
import eu.esponder.dto.model.criteria.EsponderQueryRestrictionDTO;
import eu.esponder.dto.model.criteria.EsponderUnionCriteriaCollectionDTO;
import eu.esponder.rest.ESponderResource;

@Path("/crisis/generic")
public class CrisisEntityResource extends ESponderResource {

	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}
	
	@GET
	@Path("/get")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getEntity (
			@QueryParam("idList") @NotNull(message="criteriaDTO may not be null") String idListStr,
			@QueryParam("queriedEntityDTO") @NotNull(message="queriedEntity may not be null") String queriedEntityDTO,
			@QueryParam("pageNumber") @NotNull(message="Page Number may not be null") int pageNumber,
			@QueryParam("pageSize") @NotNull(message="Page Size may not be null") int pageSize,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) throws Exception {

		Long userID=SecurityCheck(szuserID);
		
		ArrayList<Long> idList = StringToIDList(idListStr);	
		EsponderQueryRestrictionDTO restrictions = generateRestrictionsFromIdList(idList);
		if(restrictions!=null)
			return postEntity(restrictions, queriedEntityDTO, pageSize, pageNumber, szuserID);
		else
			return null;
	}
	
	@GET
	@Path("/getByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getEntityByTitle (
			@QueryParam("titleList") @NotNull(message="criteriaDTO may not be null") String titleList,
			@QueryParam("queriedEntityDTO") @NotNull(message="queriedEntity may not be null") String queriedEntityDTO,
			@QueryParam("pageNumber") @NotNull(message="Page Number may not be null") int pageNumber,
			@QueryParam("pageSize") @NotNull(message="Page Size may not be null") int pageSize,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) throws Exception {

		Long userID=SecurityCheck(szuserID);
		
		ArrayList<String> idList = StringToStringList(titleList);	
		EsponderQueryRestrictionDTO restrictions = generateRestrictionsFromTitleList(idList);
		if(restrictions!=null)
			return postEntity(restrictions, queriedEntityDTO, pageSize, pageNumber, szuserID);
		else
			return null;
	}

	
	@POST
	@Path("/post")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO postEntity(
			EsponderQueryRestrictionDTO criteriaDTO,
			@QueryParam("queriedEntityDTO") @NotNull(message="queriedEntity may not be null") String queriedEntityDTO,  
			@QueryParam("pageNumber") @NotNull(message="Page Number may not be null") int pageNumber,
			@QueryParam("pageSize") @NotNull(message="Page Size may not be null") int pageSize,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) throws Exception {

		Long userID=SecurityCheck(szuserID);
		
		ResultListDTO resultsList = new ResultListDTO(this.getGenericRemoteService().getDTOEntities(queriedEntityDTO, criteriaDTO, pageSize, pageNumber));
		return resultsList;
	}
	
	
	@POST
	@Path("/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderEntityDTO createEntity(
			@NotNull(message="entity to be created may not be null") ESponderEntityDTO entityDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) throws Exception {

		Long userID=SecurityCheck(szuserID);
		
		ESponderEntityDTO createdEntity = this.getGenericRemoteService().createEntityRemote(entityDTO, userID);
		return createdEntity;
	}
	
	
	@PUT
	@Path("/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderEntityDTO updateEntity( 
			@NotNull(message="entity to be updated may not be null") ESponderEntityDTO entityDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) throws Exception {

		Long userID=SecurityCheck(szuserID);
		
		ESponderEntityDTO updatedEntity = this.getGenericRemoteService().updateEntityRemote(entityDTO, userID);
		return updatedEntity;
	}
	

	@SuppressWarnings("unchecked")
	@DELETE
	@Path("/delete")
	public Long deleteEntity(
			@QueryParam("entityClass") @NotNull(message="entityClass may not be null") String entityClass,
			@QueryParam("entityID") @NotNull(message="entityID may not be null") Long entityID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) throws Exception {

		Long userID=SecurityCheck(szuserID);
		
		Class<? extends ESponderEntityDTO> targetDTOClass = (Class<? extends ESponderEntityDTO>) this.getMappingService().getEntityClass(entityClass);
		if(targetDTOClass != null) {
			this.getGenericRemoteService().deleteEntityRemote(targetDTOClass, entityID);
			return entityID;
		}
		else {
			//FIXME Integrate custom ESponderException when implemented
			
		}
		return null;
	}
	

	private EsponderQueryRestrictionDTO generateRestrictionsFromIdList (List<Long> idList) {

		
		
		EsponderUnionCriteriaCollectionDTO criteriaDTO = new EsponderUnionCriteriaCollectionDTO(new HashSet<EsponderQueryRestrictionDTO>());
		if(!idList.isEmpty()) {
			for(Long id : idList) {
				criteriaDTO.add( generateCriterion( id ) );
			}
			return criteriaDTO;
		}
		else {
			/*
			 * TODO: integrate the User Exceptions Hierarchy that will be defined later
			 * i.e., list of ids is empty
			 */
			System.out.println("idList is Null, cannot continue...");
		}
		return null;
	}
	
	private EsponderQueryRestrictionDTO generateRestrictionsFromTitleList (List<String> titleList) {

		EsponderUnionCriteriaCollectionDTO criteriaDTO = new EsponderUnionCriteriaCollectionDTO(new HashSet<EsponderQueryRestrictionDTO>());
		if(!titleList.isEmpty()) {
			for(String title : titleList) {
				criteriaDTO.add( generateCriterionFromTitle(title) );
			}
			return criteriaDTO;
		}
		else {
			/*
			 * TODO: integrate the User Exceptions Hierarchy that will be defined later
			 * i.e., list of ids is empty
			 */
			System.out.println("idList is Null, cannot continue...");
		}
		return null;
	}


	private EsponderCriterionDTO generateCriterion (Long id) {
		EsponderCriterionDTO criterion = new EsponderCriterionDTO();
		criterion.setField("id");
		criterion.setExpression(EsponderCriterionExpressionEnumDTO.EQUAL);
		criterion.setValue(id.toString());
		return criterion;
	}
	
	private EsponderCriterionDTO generateCriterionFromTitle (String title) {
		EsponderCriterionDTO criterion = new EsponderCriterionDTO();
		criterion.setField("title");
		criterion.setExpression(EsponderCriterionExpressionEnumDTO.EQUAL);
		criterion.setValue(title);
		return criterion;
	}

	private ArrayList<Long> StringToIDList(String idListStr) {
		idListStr = idListStr.replace("[", "").replace("]", "");
		String delims = ", ";
		String[] tokens = idListStr.split(delims);
		ArrayList<Long> idList = new ArrayList<Long>();
		for(String i : tokens) {
			idList.add(new Long(i));
		}
		return idList;
	}
	
	private ArrayList<String> StringToStringList(String titleListStr) {
		titleListStr = titleListStr.replace("[", "").replace("]", "");
		String delims = ", ";
		String[] tokens = titleListStr.split(delims);
		ArrayList<String> titleList = new ArrayList<String>();
		for(String i : tokens) {
			titleList.add(i);
		}
		return titleList;
	}

}