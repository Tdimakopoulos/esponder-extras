package eu.esponder.rest.portal;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;
import eu.esponder.util.ejb.ServiceLocator;

public class MeasurementSimulator {

	protected SensorRemoteService getSensorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected ActorRemoteService getActorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected CrisisRemoteService getCrisisRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/CrisisBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected ActionRemoteService getActionRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected GenericRemoteService getGenericRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	
//	CrisisRemoteService crisisService = ServiceLocator.getResource("esponder/CrisisBean/remote");
//	ActionRemoteService actionService = ServiceLocator.getResource("esponder/ActionBean/remote");
//	GenericRemoteService genericService = ServiceLocator.getResource("esponder/GenericBean/remote");
 
	Long threadPeriodFRU = (long) 15000;

	List<Thread> FRUThreads = new ArrayList<Thread>();
	
	//	Temperature Configuration
	int minTemp = 35;		//	minimum produced temperature
	int rangeTemp = 3;		//	temperature deviation

	//	Location Configuration
	int minStep = 60;		//	minimum distance travelled
	int maxStep = 70;		//	maximum distance travelled

	// FRU Routes, Coordinates for source and destination points  
	// Syggrou-Fix Station -----> Faliriko Delta, along Syggrou Avenue
	PointDTO startingPointFRU1 = new PointDTO(new BigDecimal(37.959655), new BigDecimal(23.720305), null);
	PointDTO destinationPointFRU1 = new PointDTO(new BigDecimal(37.940703), new BigDecimal(23.696158), null);

	// Nea Smirni Square -----> Axilleos St, along Eleftheriou Venizelou Avenue
	PointDTO startingPointFRU2 = new PointDTO(new BigDecimal(37.946997), new BigDecimal(23.715169), null);
	PointDTO destinationPointFRU2 = new PointDTO(new BigDecimal(37.929668), new BigDecimal(23.709848), null);

	List<String> actorsTitles = new ArrayList<String>(Arrays.asList("FRC #1", "FR #1.1", "FR #1.2","FRC #2",  "FR #2.1", "FR #2.2"));

	List<ESponderEventPublisher<CreateSensorMeasurementStatisticEvent>> publishersList = new ArrayList<ESponderEventPublisher<CreateSensorMeasurementStatisticEvent>>();


	public void runSimulation() throws InterruptedException {

		int counter = 0;
		
//		try {
//			sendCreateActionEvent();
//		} catch (ClassNotFoundException e1) {
//			e1.printStackTrace();
//		}
		
		for(String actorTitle : actorsTitles) {
			
			ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher = null;
			try {
				publisher = new ESponderEventPublisher<CreateSensorMeasurementStatisticEvent>(CreateSensorMeasurementStatisticEvent.class);
			} catch (Exception e) {
				System.out.println("\n\nUnable to get a connection to EventAdmin, simulation will be terminated\n\n");
				continue;
			}
			publishersList.add(publisher);
			ActorDTO actor = this.getActorRemoteService().findByTitleRemote(actorTitle);
			FRUThread fruThread;
			if(actor != null) {
				if(counter<=2)
					fruThread = new FRUThread(publisher, actor, threadPeriodFRU, rangeTemp, minTemp, startingPointFRU1, destinationPointFRU1, minStep, maxStep );
				else
					fruThread = new FRUThread(publisher, actor, threadPeriodFRU, rangeTemp, minTemp, startingPointFRU2, destinationPointFRU2, minStep, maxStep );

				FRUThreads.add(fruThread);
			}
			else {
				publisher.CloseConnection();
				System.out.println("\n\nUnable to find selected actor with title "+actorTitle+", actor will not be added to simulation\n\n");
			}
			counter++;
		}

		// Start Threads
		for(Thread thread : FRUThreads)
			thread.start();

		boolean threadStatus = true;

		while (threadStatus == true) {
			for(Thread thread : FRUThreads)
				if(!thread.isAlive()) 
					threadStatus = false;
				else {
					threadStatus =true;
					break;
				}
		}

		System.out.println("\n\nSimulation has ended successfully");

		for(ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher: publishersList)
			publisher.CloseConnection();

	}
	
	private void sendCreateActionEvent() throws ClassNotFoundException {
		
		String actorTitle = "FRC #1";
		
		ActorDTO actor = this.getActorRemoteService().findByTitleRemote(actorTitle);
		if(actor != null) {
			
			CrisisContextDTO crisisContext = this.getCrisisRemoteService().findCrisisContextDTOByTitle("Fire Brigade Drill");
			Long userID = new Long(1);
			
			ESponderEventPublisher<CreateActionEvent> publisher = new ESponderEventPublisher<CreateActionEvent>(CreateActionEvent.class);
			
			String date = (new Date()).toString();
			ActionDTO action = new ActionDTO();
			action.setActionOperation(ActionOperationEnumDTO.MOVE);
			action.setCrisisContext(crisisContext);
			action.setSeverityLevel(SeverityLevelDTO.MEDIUM);
			action.setTitle("Initial DF action"+date);
			action.setType("OpActionType");
			ActionDTO paction = this.getActionRemoteService().createActionRemote(action, userID);
			
			
			
			ActionPartDTO actionPart = new ActionPartDTO();
			actionPart.setActionOperation(ActionOperationEnumDTO.MOVE);
			actionPart.setActor(actor);
			actionPart.setSeverityLevel(SeverityLevelDTO.MEDIUM);
			actionPart.setTitle("Initial DF Action Part"+(new Date()).toString());
			actionPart.setActionId(paction.getId());
			ActionPartDTO pactionPart = this.getActionRemoteService().createActionPartRemote(actionPart, userID);
			
			SphereDTO location = new SphereDTO(new PointDTO(new BigDecimal(37.940703), new BigDecimal(23.696158), null), new BigDecimal(2),"Initial DF location Area"+date );
			SphereDTO plocation = (SphereDTO) this.getGenericRemoteService().createEntityRemote(location, userID);
			
			ActionPartObjectiveDTO objective = new ActionPartObjectiveDTO();
			objective.setActionPart(pactionPart);
			objective.setLocationArea(plocation);
			objective.setTitle("Initial DF Action Part Objective"+(new Date()).toString());
			objective.setPeriod(new PeriodDTO(new Date().getTime(), new Date().getTime()));
			this.getActionRemoteService().createActionPartObjectiveRemote(objective, userID);
			
			
			ActionDTO actionForEvent = this.getActionRemoteService().findActionDTOByTitle("Initial DF action"+date);
			
			CreateActionEvent event = new CreateActionEvent();
			event.setEventAttachment(actionForEvent);
			event.setEventSeverity(SeverityLevelDTO.MEDIUM);
			event.setEventSource(actor);
			event.setEventTimestamp(new Date());
			event.setJournalMessage("Initial DF CreateActionEvent");
			
			try {
				publisher.publishEvent(event);
			} catch (EventListenerException e) {
				System.out.println("Unable to send event through publisher");
			}
			
			System.out.println("Event has been sent...");
			publisher.CloseConnection();
			
		}
		else
			System.out.println("\nActor not found, event will not be published...\n");
		
	}

}
