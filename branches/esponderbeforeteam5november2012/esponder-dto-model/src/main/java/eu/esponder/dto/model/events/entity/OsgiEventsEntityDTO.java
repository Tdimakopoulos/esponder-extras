package eu.esponder.dto.model.events.entity;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;


@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "journalMsg", "journalMsgInfo", "timestamp", "severity", "attachment", "source", "sourceid"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OsgiEventsEntityDTO extends ESponderEntityDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6940684176472034542L;

	private Long id;

	private String journalMsg;

	private String journalMsgInfo;

	private Long timeStamp;

	private String severity;

	private String attachment;

	private String source;

	private Long sourceid;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJournalMsg() {
		return journalMsg;
	}

	public void setJournalMsg(String journalMsg) {
		this.journalMsg = journalMsg;
	}

	public String getJournalMsgInfo() {
		return journalMsgInfo;
	}

	public void setJournalMsgInfo(String journalMsgInfo) {
		this.journalMsgInfo = journalMsgInfo;
	}

	public Long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Long getSourceid() {
		return sourceid;
	}

	public void setSourceid(Long sourceid) {
		this.sourceid = sourceid;
	}

}
