package eu.esponder.dto.model.snapshot;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;


@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "referenceFile", "averageSize","operationsCentre","referencePOI"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ReferencePOISnapshotDTO extends SpatialSnapshotDTO {
	
	private static final long serialVersionUID = -1729481522641331340L;

	protected Long id;
	protected String referenceFile;
	protected Float averageSize;
	protected String title;
	protected OperationsCentreDTO operationsCentre;
	protected ReferencePOIDTO referencePOI;
	private ReferencePOISnapshotDTO previous;
	private ReferencePOISnapshotDTO next;
	
	public String getReferenceFile() {
		return referenceFile;
	}

	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

	public Float getAverageSize() {
		return averageSize;
	}

	public void setAverageSize(Float averageSize) {
		this.averageSize = averageSize;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public OperationsCentreDTO getOperationsCentre() {
		return operationsCentre;
	}

	public void setOperationsCentre(OperationsCentreDTO operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	public ReferencePOIDTO getReferencePOI() {
		return referencePOI;
	}

	public void setReferencePOI(ReferencePOIDTO referencePOI) {
		this.referencePOI = referencePOI;
	}

	public ReferencePOISnapshotDTO getPrevious() {
		return previous;
	}

	public void setPrevious(ReferencePOISnapshotDTO previous) {
		this.previous = previous;
	}

	public ReferencePOISnapshotDTO getNext() {
		return next;
	}

	public void setNext(ReferencePOISnapshotDTO next) {
		this.next = next;
	}
}



