package eu.esponder.dto.model.crisis.action;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;


@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "actionpart", "prerequisiteActionpart", "criteria"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionPartScheduleDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = -703599359640439512L;

	private String title;
	
	private ActionPartDTO actionpart;
	
	private ActionPartDTO prerequisiteActionpart;
	
	private ActionPartScheduleCriteriaDTO criteria;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ActionPartDTO getActionpart() {
		return actionpart;
	}

	public void setActionpart(ActionPartDTO actionpart) {
		this.actionpart = actionpart;
	}

	public ActionPartDTO getPrerequisiteActionpart() {
		return prerequisiteActionpart;
	}

	public void setPrerequisiteActionpart(ActionPartDTO prerequisiteActionpart) {
		this.prerequisiteActionpart = prerequisiteActionpart;
	}

	public ActionPartScheduleCriteriaDTO getCriteria() {
		return criteria;
	}

	public void setCriteria(ActionPartScheduleCriteriaDTO criteria) {
		this.criteria = criteria;
	}

	
}


