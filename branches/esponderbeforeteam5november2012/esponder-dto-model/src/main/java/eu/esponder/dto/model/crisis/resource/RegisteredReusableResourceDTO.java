package eu.esponder.dto.model.crisis.resource;

import java.math.BigDecimal;
import java.util.Set;



public class RegisteredReusableResourceDTO extends ResourceDTO {

	private static final long serialVersionUID = 1249171957154114291L;


	private BigDecimal quantity;
	

	private Set<RegisteredConsumableResourceDTO> registeredConsumables;
	
	private RegisteredReusableResourceDTO parent;
	

	private Set<RegisteredReusableResourceDTO> children;
	
	/**
	 * TODO: Fix this to point to OperationsCentreCategory. For now we just keep the correct OperationsCentreCategoryId
	 */
//	@ManyToOne
//	@JoinColumn(name="CATEGORY_ID")
//	private ReusableResourceCategory reusableResourceCategory;
	private Long reusableResourceCategoryId;

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Set<RegisteredConsumableResourceDTO> getRegisteredConsumables() {
		return registeredConsumables;
	}

	public void setRegisteredConsumables(
			Set<RegisteredConsumableResourceDTO> registeredConsumables) {
		this.registeredConsumables = registeredConsumables;
	}

	public RegisteredReusableResourceDTO getParent() {
		return parent;
	}

	public void setParent(RegisteredReusableResourceDTO parent) {
		this.parent = parent;
	}

	public Set<RegisteredReusableResourceDTO> getChildren() {
		return children;
	}

	public void setChildren(Set<RegisteredReusableResourceDTO> children) {
		this.children = children;
	}

	public Long getReusableResourceCategoryId() {
		return reusableResourceCategoryId;
	}

	public void setReusableResourceCategoryId(Long reusableResourceCategoryId) {
		this.reusableResourceCategoryId = reusableResourceCategoryId;
	}

	
}
