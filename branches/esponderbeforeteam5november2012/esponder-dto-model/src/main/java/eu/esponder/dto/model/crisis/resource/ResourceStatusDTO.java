package eu.esponder.dto.model.crisis.resource;



public enum ResourceStatusDTO {
	AVAILABLE,
	UNAVAILABLE,
	RESERVED,
	ALLOCATED
}
