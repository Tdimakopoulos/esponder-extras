package eu.esponder.dto.model;
import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({ "pSensorSnapshot,pActor,pEquipment" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class SensorSnapshotDetailsList implements Serializable {
	
	private static final long serialVersionUID = 9013625599098496480L;
	
	SensorSnapshotDTO pSensorSnapshot= new SensorSnapshotDTO();
	ActorDTO pActor = new ActorDTO();
	EquipmentDTO pEquipment = new EquipmentDTO();
    
	public SensorSnapshotDetailsList()
	{
		
	}
	public SensorSnapshotDetailsList(SensorSnapshotDTO pSensorSnapshots,ActorDTO pActors,EquipmentDTO pEquipments)
	{
		pSensorSnapshot=pSensorSnapshots;
		pActor=pActors;
		pEquipment=pEquipments;
	}
	
	public SensorSnapshotDTO getpSensorSnapshot() {
		return pSensorSnapshot;
	}
	public void setpSensorSnapshot(SensorSnapshotDTO pSensorSnapshot) {
		this.pSensorSnapshot = pSensorSnapshot;
	}
	public ActorDTO getpActor() {
		return pActor;
	}
	public void setpActor(ActorDTO pActor) {
		this.pActor = pActor;
	}
	public EquipmentDTO getpEquipment() {
		return pEquipment;
	}
	public void setpEquipment(EquipmentDTO pEquipment) {
		this.pEquipment = pEquipment;
	}
}
