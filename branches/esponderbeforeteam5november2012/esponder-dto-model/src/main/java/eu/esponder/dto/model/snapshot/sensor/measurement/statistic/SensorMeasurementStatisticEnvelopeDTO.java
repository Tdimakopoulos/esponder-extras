package eu.esponder.dto.model.snapshot.sensor.measurement.statistic;

import java.util.List;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "measurementStatistics", "fRId"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SensorMeasurementStatisticEnvelopeDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = -4109736575559482473L;

	private List<SensorMeasurementStatisticDTO> measurementStatistics;
	
	private String fRId;

	public String getfRId() {
		return fRId;
	}

	public void setfRId(String fRId) {
		this.fRId = fRId;
	}

	public List<SensorMeasurementStatisticDTO> getMeasurementStatistics() {
		return measurementStatistics;
	}

	public void setMeasurementStatistics(
			List<SensorMeasurementStatisticDTO> measurementStatistics) {
		this.measurementStatistics = measurementStatistics;
	}
	
}
