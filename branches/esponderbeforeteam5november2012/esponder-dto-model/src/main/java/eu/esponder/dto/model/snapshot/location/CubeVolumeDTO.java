package eu.esponder.dto.model.snapshot.location;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonPropertyOrder({"id", "point", "acme"})
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CubeVolumeDTO extends LocationAreaDTO {

	private static final long serialVersionUID = -1716194673074175161L;

	public CubeVolumeDTO() { }
	
	private PointDTO point;

	private BigDecimal acme;

	public PointDTO getPoint() {
		return point;
	}

	public void setPoint(PointDTO point) {
		this.point = point;
	}

	public BigDecimal getAcme() {
		return acme;
	}

	public void setAcme(BigDecimal acme) {
		this.acme = acme;
	}

}
