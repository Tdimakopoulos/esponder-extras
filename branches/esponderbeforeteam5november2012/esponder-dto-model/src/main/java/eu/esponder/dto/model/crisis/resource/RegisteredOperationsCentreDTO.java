package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.view.VoIPURLDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "operationsCentreCategoryId", "title", "status", "voIPURL"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class RegisteredOperationsCentreDTO extends ResourceDTO {

	private static final long serialVersionUID = 443517368669208349L;
	
	public RegisteredOperationsCentreDTO() { }

	private Long operationsCentreCategoryId;

	private VoIPURLDTO voIPURL;

	public Long getOperationsCentreCategoryId() {
		return operationsCentreCategoryId;
	}

	public void setOperationsCentreCategoryId(Long operationsCentreCategoryId) {
		this.operationsCentreCategoryId = operationsCentreCategoryId;
	}

	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}

	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}

	
}
