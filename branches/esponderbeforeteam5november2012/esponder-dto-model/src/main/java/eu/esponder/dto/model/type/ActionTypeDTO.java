package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ActionTypeDTO extends ESponderTypeDTO {

	private static final long serialVersionUID = -4116663244782312272L;
	
}
