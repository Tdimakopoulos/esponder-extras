package eu.esponder.dto.model.criteria;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class EsponderQueryRestrictionDTO implements Serializable {

	private static final long serialVersionUID = -6325570244012530074L;

}