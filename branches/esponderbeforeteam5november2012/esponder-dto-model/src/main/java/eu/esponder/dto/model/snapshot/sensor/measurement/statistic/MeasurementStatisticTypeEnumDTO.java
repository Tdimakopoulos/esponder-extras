package eu.esponder.dto.model.snapshot.sensor.measurement.statistic;


public enum MeasurementStatisticTypeEnumDTO {

	MEAN,
	STDEV,

	AUTOCORRELATION,

	MAXIMUM,
	MINIMUM,

	FIRST,
	LAST,
	MEDIAN,
	
	OLDEST,
	NEWEST,
	RATE
	
}
