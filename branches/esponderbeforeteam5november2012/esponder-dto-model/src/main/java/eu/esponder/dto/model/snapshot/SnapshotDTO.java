package eu.esponder.dto.model.snapshot;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class SnapshotDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = 3358588722380975179L;
	
	protected PeriodDTO period;

	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

}
