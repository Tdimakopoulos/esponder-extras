package eu.esponder.dto.model.snapshot;

import java.io.Serializable;
import java.sql.Timestamp;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"dateFrom", "dateTo"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PeriodDTO implements Serializable {
	
	private static final long serialVersionUID = -2870132067764797692L;
	
//	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Long dateFrom;

//	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Long dateTo;
	
	public PeriodDTO() { }
	
	public PeriodDTO(Long dateFrom, Long dateTo) {
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}
	


	public Long getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Long dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Long getDateTo() {
		return dateTo;
	}

	public void setDateTo(Long dateTo) {
		this.dateTo = dateTo;
	}

	@Override
	public String toString() {
		return "Period [dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}

}
