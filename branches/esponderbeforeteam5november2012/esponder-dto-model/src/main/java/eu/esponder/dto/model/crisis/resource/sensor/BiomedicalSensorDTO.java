package eu.esponder.dto.model.crisis.resource.sensor;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class BiomedicalSensorDTO extends SensorDTO {

	private static final long serialVersionUID = -4394240267122716480L;

}
