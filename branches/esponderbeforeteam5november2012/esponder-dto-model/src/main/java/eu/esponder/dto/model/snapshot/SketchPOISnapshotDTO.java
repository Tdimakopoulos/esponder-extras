package eu.esponder.dto.model.snapshot;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.HttpURLDTO;
import eu.esponder.dto.model.crisis.view.MapPointDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "points", "httpURL","operationsCentre","sketchType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SketchPOISnapshotDTO extends SpatialSnapshotDTO {
	

	private static final long serialVersionUID = -1734142629191263542L;

	
	protected Long id;
	
	
	private Set<MapPointDTO> points;
	
	
	private HttpURLDTO httpURL;
	
	
	private String sketchType;

	
	protected String title;
	
	private SketchPOIDTO sketchPOI;
	
	public SketchPOIDTO getSketchPOI() {
		return sketchPOI;
	}

	public void setSketchPOI(SketchPOIDTO sketchPOI) {
		this.sketchPOI = sketchPOI;
	}

	protected OperationsCentreDTO operationsCentre;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<MapPointDTO> getPoints() {
		return points;
	}

	public void setPoints(Set<MapPointDTO> points) {
		this.points = points;
	}

	public HttpURLDTO getHttpURL() {
		return httpURL;
	}

	public void setHttpURL(HttpURLDTO httpURL) {
		this.httpURL = httpURL;
	}

	public String getSketchType() {
		return sketchType;
	}

	public void setSketchType(String sketchType) {
		this.sketchType = sketchType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public OperationsCentreDTO getOperationsCentre() {
		return operationsCentre;
	}

	public void setOperationsCentre(OperationsCentreDTO operationsCentre) {
		this.operationsCentre = operationsCentre;
	}
	
}
