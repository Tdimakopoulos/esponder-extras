package eu.esponder.dto.model.user;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"userName", "operationsCentres", "role"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ESponderUserDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = 6015093808353080103L;

	private String userName;
	
	private String role;
	
	private Set<OperationsCentreDTO> operationsCentres;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Set<OperationsCentreDTO> getOperationsCentres() {
		return operationsCentres;
	}

	public void setOperationsCentres(Set<OperationsCentreDTO> operationsCentres) {
		this.operationsCentres = operationsCentres;
	}

	@Override
	public String toString() {
		return "ESponderUserDTO [userName=" + userName + ", id=" + id + "]";
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
