package eu.esponder.dto.model.crisis.resource;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;


@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "quantity", "actionPart","configuration", "consumableResourceCategoryId"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ConsumableResourceDTO extends LogisticsResourceDTO {
	
	private static final long serialVersionUID = -3549004897135122493L;

	public ConsumableResourceDTO() { }
	
	private BigDecimal quantity;
	
	private ConsumableResourceCategoryDTO consumableResourceCategory;
	
	private ActionPartDTO actionPart;
	
	private ReusableResourceDTO container;

	private Long crisisContextId;
	
	public Long getCrisisContextId() {
		return crisisContextId;
	}

	public void setCrisisContextId(Long crisisContextId) {
		this.crisisContextId = crisisContextId;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public ReusableResourceDTO getContainer() {
		return container;
	}

	public void setContainer(ReusableResourceDTO container) {
		this.container = container;
	}

	public ActionPartDTO getActionPart() {
		return actionPart;
	}

	public void setActionPart(ActionPartDTO actionPart) {
		this.actionPart = actionPart;
	}

	public ConsumableResourceCategoryDTO getConsumableResourceCategory() {
		return consumableResourceCategory;
	}

	public void setConsumableResourceCategory(ConsumableResourceCategoryDTO consumableResourceCategory) {
		this.consumableResourceCategory = consumableResourceCategory;
	}

}
