package eu.esponder.test.sensor;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.test.ResourceLocator;

public class ActorSerializeTest {
	
	protected ActorRemoteService actorService = ResourceLocator.lookup("esponder/ActorBean/remote");
	
	@Test
	public void testActorSerialize() throws JsonGenerationException, JsonMappingException, IOException {
		ActorDTO firstFR = (ActorDTO) actorService.findByTitleRemote("FRC #1");
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
		mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		String resultJSON = mapper.writeValueAsString(firstFR);
		System.out.println(resultJSON);
	}
	
	@Test
	public void testJSONForFabian() throws JsonParseException, JsonMappingException, IOException {
		
		String Json = "{\"@class\":\"eu.esponder.dto.model.crisis.view.SketchPOIDTO\",\"title\":\"new Test SketchPOI2\",\"points\":[{\"@class\":\"eu.esponder.dto.model.crisis.view.MapPointDTO\",\"title\":\"new map point test2\",\"point\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":10,\"longitude\":10}}]}";
		ObjectMapper mapper = new ObjectMapper();
		SketchPOIDTO sketchPOI = mapper.readValue(Json, SketchPOIDTO.class);
		System.out.println(sketchPOI.getTitle());
	}

}