package eu.esponder.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.resource.PersonnelSkillDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelTrainingDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.EquipmentCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OperationsCentreCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.dto.model.type.OperationsCentreTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.dto.model.type.PersonnelSkillTypeDTO;
import eu.esponder.dto.model.type.PersonnelTrainingTypeDTO;
import eu.esponder.dto.model.type.RankTypeDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class CategoryServiceTest extends ControllerServiceTest {
	
	@Test(groups="createBasics")
	public void createPersonnelCompetencesDTO() throws ClassNotFoundException {
		
		// Personnel Skills
		createPersonnelSkillDTO("Fire Truck Driving", "Driving");
		createPersonnelSkillDTO("Crane Operation Handling", "Tool Handling");
		createPersonnelSkillDTO("Firewall Planning", "Administration");
		
		// Personnel Training
		createPersonnelTrainingDTO("Driving", "Driving");
		createPersonnelTrainingDTO("Tool Handling", "Tools");
		createPersonnelTrainingDTO("Administration", "Administration");
		
	}
	
	
	@SuppressWarnings("unused")
	@Test(groups="createBasics")
	public void createCategoriesDTO() throws ClassNotFoundException {
		
		//	OC Categories
		OperationsCentreCategoryDTO operationsCentreCategoryDTO = createOperationsCentreCategoryDTO("EOC");
		OperationsCentreCategoryDTO operationsCentreCategoryDTO2 = createOperationsCentreCategoryDTO("MEOC");
		
		//	Equipment Categories
		EquipmentCategoryDTO equipmentCategoryDTO = createEquipmentCategoryDTO("FRU WiMAX"); 
		EquipmentCategoryDTO equipmentCategoryDTO2 = createEquipmentCategoryDTO("FRU WiFi");
	
		//	Consumable Categories
		ConsumableResourceCategoryDTO consumableResourceCategoryDTO = createConsumableResourceCategoryDTO("Drink"); 
		ConsumableResourceCategoryDTO consumableResourceCategoryDTO2 = createConsumableResourceCategoryDTO("Food");
		ConsumableResourceCategoryDTO consumableResourceCategoryDTO3 = createConsumableResourceCategoryDTO("Medical Resource");
		
		//	Reusable Categories
		ReusableResourceCategoryDTO reusableResourceCategoryDTO = createReusableResourceCategoryDTO("Water Container");
		ReusableResourceCategoryDTO reusableResourceCategoryDTO2 = createReusableResourceCategoryDTO("Food Package");
		ReusableResourceCategoryDTO reusableResourceCategoryDTO3 = createReusableResourceCategoryDTO("Medical Kit");
		ReusableResourceCategoryDTO reusableResourceCategoryDTO4 = createReusableResourceCategoryDTO("Tools");
		
		//	Organisation Categories
		OrganisationCategoryDTO organisationCategory1 = createOrganisationCategoryDTO("Headquarters", "Police Force");
		OrganisationCategoryDTO organisationCategory2 = createOrganisationCategoryDTO("Local Station", "Police Force");
		
		OrganisationCategoryDTO organisationCategory3 = createOrganisationCategoryDTO("Headquarters", "Fire Brigade");
		OrganisationCategoryDTO organisationCategory4 = createOrganisationCategoryDTO("Local Station", "Fire Brigade");
		
		OrganisationCategoryDTO organisationCategory5 = createOrganisationCategoryDTO("Headquarters", "Coast Guard");
		OrganisationCategoryDTO organisationCategory6 = createOrganisationCategoryDTO("Local Station", "Coast Guard");
		
		OrganisationCategoryDTO organisationCategory7 = createOrganisationCategoryDTO("Headquarters", "Army");
		OrganisationCategoryDTO organisationCategory8 = createOrganisationCategoryDTO("Local Station", "Army");
		
	}
	
	@Test(groups="createBasics")
	public void createPersonnelCategories() throws ClassNotFoundException {
		
		
		//	FIXME 
		
		// Personnel Categories
		
		createPersonnelCategoryDTO("RankType1", "Fire Brigade", "Headquarters");
		createPersonnelCategoryDTO("RankType2", "Fire Brigade", "Local Station");
		createPersonnelCategoryDTO("RankType3", "Police Force", "Local Station");
		
//		RankTypeDTO rankTypeDTO1 = (RankTypeDTO) typeService.findDTOByTitle("RankType1");
//		RankTypeDTO rankTypeDTO2 = (RankTypeDTO) typeService.findDTOByTitle("RankType2");
//		RankTypeDTO rankTypeDTO3 = (RankTypeDTO) typeService.findDTOByTitle("RankType3");

//		PersonnelCategoryDTO personnelCategoryDTO1 = new PersonnelCategoryDTO();
//		personnelCategoryDTO1.setRank(rankTypeDTO1);
//		personnelCategoryDTO1.setOrganisationCategory(organisationCategory6);
//
//		
//		PersonnelCategoryDTO personnelCategoryDTO2 = new PersonnelCategoryDTO();
//		personnelCategoryDTO2.setRank(rankTypeDTO2);
//		personnelCategoryDTO1.setOrganisationCategory(organisationCategory7);
//		
//		
//		PersonnelCategoryDTO personnelCategoryDTO3 = new PersonnelCategoryDTO();
//		personnelCategoryDTO3.setRank(rankTypeDTO3);
//		personnelCategoryDTO1.setOrganisationCategory(organisationCategory8);
		
	}
	
	
	private PersonnelCategoryDTO createPersonnelCategoryDTO(String rankTypeTitle, String disciplineTypeTitle, String organisationTypeTitle) throws ClassNotFoundException {
		
		RankTypeDTO rankTypeDTO = (RankTypeDTO) typeService.findDTOByTitle(rankTypeTitle);
		
		DisciplineTypeDTO disciplineTypeDTO = (DisciplineTypeDTO) typeService.findDTOByTitle(disciplineTypeTitle);
		OrganisationTypeDTO organisationTypeDTO = (OrganisationTypeDTO) typeService.findDTOByTitle(organisationTypeTitle);
		OrganisationCategoryDTO organisationCategoryDTO = resourceCategoryService.findDTOByType(disciplineTypeDTO, organisationTypeDTO);
		
		if(organisationCategoryDTO != null && disciplineTypeDTO != null) {
			PersonnelCategoryDTO personnelCategoryDTO = new PersonnelCategoryDTO();
			personnelCategoryDTO.setRank(rankTypeDTO);
			personnelCategoryDTO.setOrganisationCategory(organisationCategoryDTO);
			
			personnelCategoryDTO = (PersonnelCategoryDTO) resourceCategoryService.create(personnelCategoryDTO, userID);
			
			return personnelCategoryDTO;
			
		}
		else
			return null;
		
		
	}
	
	
	private PersonnelSkillDTO createPersonnelSkillDTO(String personnelSkillTypeTitle, String personnelSkillTitle) throws ClassNotFoundException {
		PersonnelSkillDTO personnelSkillDTO = new PersonnelSkillDTO();
		PersonnelSkillTypeDTO personnelSkillType = (PersonnelSkillTypeDTO) typeService.findDTOByTitle(personnelSkillTypeTitle);
		personnelSkillDTO.setPersonnelSkillType(personnelSkillType);
		personnelSkillDTO.setShortTitle(personnelSkillTitle);
		personnelSkillDTO = (PersonnelSkillDTO) personnelService.createPersonnelCompetenceRemote(personnelSkillDTO, userID);
		return personnelSkillDTO;
	}
	
	private PersonnelTrainingDTO createPersonnelTrainingDTO(String personnelTrainingTypeTitle, String personnelTrainingTitle) throws ClassNotFoundException {
		PersonnelTrainingDTO personnelTrainingDTO = new PersonnelTrainingDTO();
		PersonnelTrainingTypeDTO personnelTrainingType = (PersonnelTrainingTypeDTO) typeService.findDTOByTitle(personnelTrainingTypeTitle);
		personnelTrainingDTO.setPersonnelTrainingType(personnelTrainingType);
		personnelTrainingDTO.setShortTitle(personnelTrainingTitle);
		personnelTrainingDTO = (PersonnelTrainingDTO) personnelService.createPersonnelCompetenceRemote(personnelTrainingDTO, userID);
		return personnelTrainingDTO;
	}
	
	private OperationsCentreCategoryDTO createOperationsCentreCategoryDTO(String operationsCentreCategoryTitle) throws ClassNotFoundException {
		OperationsCentreCategoryDTO operationsCentreCategoryDTO = new OperationsCentreCategoryDTO();
		OperationsCentreTypeDTO operationsCentreTypeDTO = (OperationsCentreTypeDTO) typeService.findDTOByTitle(operationsCentreCategoryTitle);
		operationsCentreCategoryDTO.setOperationsCentreType(operationsCentreTypeDTO);
		operationsCentreCategoryDTO = (OperationsCentreCategoryDTO) resourceCategoryService.create(operationsCentreCategoryDTO, this.userID);
		return operationsCentreCategoryDTO;
	}
	
	private EquipmentCategoryDTO createEquipmentCategoryDTO(String equipmentCategoryTitle) throws ClassNotFoundException {
		EquipmentCategoryDTO equipmentCategoryDTO = new EquipmentCategoryDTO();
		EquipmentTypeDTO equipmentTypeDTO = (EquipmentTypeDTO) typeService.findDTOByTitle(equipmentCategoryTitle);
		equipmentCategoryDTO.setEquipmentType(equipmentTypeDTO);
		equipmentCategoryDTO = (EquipmentCategoryDTO) resourceCategoryService.create(equipmentCategoryDTO, this.userID);
		return equipmentCategoryDTO;
	}
	
	private ConsumableResourceCategoryDTO createConsumableResourceCategoryDTO(String consumableResourceTypeTitle) throws ClassNotFoundException {
		ConsumableResourceCategoryDTO consumableCategoryDTO = new ConsumableResourceCategoryDTO();
		ConsumableResourceTypeDTO consumableResourceTypeDTO = (ConsumableResourceTypeDTO) typeService.findDTOByTitle(consumableResourceTypeTitle);
		consumableCategoryDTO.setConsumableResourceType(consumableResourceTypeDTO);
		consumableCategoryDTO = (ConsumableResourceCategoryDTO) resourceCategoryService.create(consumableCategoryDTO, this.userID);
		return consumableCategoryDTO;
	}
	
	private ReusableResourceCategoryDTO createReusableResourceCategoryDTO(String reusableResourceTypeTitle) throws ClassNotFoundException {
		ReusableResourceCategoryDTO reusableCategoryDTO = new ReusableResourceCategoryDTO();
		ReusableResourceTypeDTO reusableResourceTypeDTO = (ReusableResourceTypeDTO) typeService.findDTOByTitle(reusableResourceTypeTitle);
		reusableCategoryDTO.setReusableResourceType(reusableResourceTypeDTO);
		reusableCategoryDTO = (ReusableResourceCategoryDTO) resourceCategoryService.create(reusableCategoryDTO, this.userID);
		return reusableCategoryDTO;
	}
	
	private OrganisationCategoryDTO createOrganisationCategoryDTO(String organisationTypeTitle, String disciplineTypeTitle) throws ClassNotFoundException {
		OrganisationCategoryDTO organisationCategoryDTO = new OrganisationCategoryDTO();
		OrganisationTypeDTO organisationTypeDTO = (OrganisationTypeDTO) typeService.findDTOByTitle(organisationTypeTitle);
		DisciplineTypeDTO disciplineTypeDTO = (DisciplineTypeDTO) typeService.findDTOByTitle(disciplineTypeTitle);
		organisationCategoryDTO.setOrganisationType(organisationTypeDTO);
		organisationCategoryDTO.setDisciplineType(disciplineTypeDTO);
		organisationCategoryDTO = (OrganisationCategoryDTO) resourceCategoryService.create(organisationCategoryDTO, this.userID);
		return organisationCategoryDTO;
	}

}
