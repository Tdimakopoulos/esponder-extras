package eu.esponder.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class UserServiceTest extends ControllerServiceTest {
	
	@Test(groups="createBasics")
	public void testCreateUsers() {
		ESponderUserDTO pki = new ESponderUserDTO();
		pki.setUserName("pki");
		userService.createUserRemote(pki, null);
		
		ESponderUserDTO ctri = new ESponderUserDTO();
		ctri.setUserName("ctri");
		userService.createUserRemote(ctri, null);
		
		ESponderUserDTO gleo = new ESponderUserDTO();
		gleo.setUserName("gleo");
		userService.createUserRemote(gleo, null);
		
		ESponderUserDTO kotso = new ESponderUserDTO();
		kotso.setUserName("kotso");
		userService.createUserRemote(kotso, null);
		
//		ESponderUserDTO dimitris = new ESponderUserDTO();
//		ctri.setUserName("Dimitris");
//		userService.createUserRemote(dimitris, null);
//		
//		ESponderUserDTO thomas = new ESponderUserDTO();
//		ctri.setUserName("Thomas");
//		userService.createUserRemote(thomas, null);
//		
//		ESponderUserDTO Jesus = new ESponderUserDTO();
//		ctri.setUserName("Jesus");
//		userService.createUserRemote(Jesus, null);
//		
//		ESponderUserDTO Fabian = new ESponderUserDTO();
//		ctri.setUserName("Fabian");
//		userService.createUserRemote(Fabian, null);
		
	}
	
}
