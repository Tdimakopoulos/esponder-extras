package eu.esponder.test.event.simulator;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.geolocation.calculations.DirectLineCalculations2D;
import eu.esponder.geolocation.lib.GlobalCoordinates;

public class LocationSensorThread extends SensorThread {

	private double startLatitude;

	private double startLongitude;

	private double destLatitude;

	private double destLongitude;
	
	private int minStep;
	
	private int maxStep;

	public LocationSensorThread(Long period, SensorDTO sensor,
			MeasurementStatisticTypeEnumDTO statisticType, PointDTO startPoint, PointDTO destPoint, List<SensorMeasurementStatisticDTO> statisticsEnvelope, int minStep, int maxStep) {
		super(period, sensor, statisticType, statisticsEnvelope);	// statisticsEnvelope --> List of statistics, not actual envelope

		this.setStartLatitude(startPoint.getLatitude().doubleValue());
		this.setStartLongitude(startPoint.getLongitude().doubleValue());
		this.setDestLatitude(destPoint.getLatitude().doubleValue());
		this.setDestLongitude(destPoint.getLongitude().doubleValue());
		this.setMinStep(minStep);
		this.setMaxStep(maxStep);
	}

	@Override
	public void run() {

		Long meas_from = new Date().getTime();
		try {
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		while(!this.isInterrupted()) {
			
			try {
				super.run();
				this.setStatisticMeasurement(getMeasurement(meas_from, this.startLatitude, this.startLongitude, this.destLatitude, this.destLongitude, this.minStep, this.maxStep));

//				System.out.println("Measurement for Location Sensor"+Thread.currentThread().getName() + " with id : " + Thread.currentThread().getId()+" is : "
//						+ ((LocationSensorMeasurementDTO)this.getStatisticMeasurement().getStatistic()).getPoint()
//						+" with period : "+(this.getStatisticMeasurement().getPeriod().getDateFrom().toString()+" : "
//						+(this.getStatisticMeasurement().getPeriod().getDateTo())));

				synchronized (this.getStatisticsList()) {
					this.getStatisticsList().add(this.getStatisticMeasurement());
				}

				// Current starting Point is the point returned as measurement
				this.setStartLatitude(((LocationSensorMeasurementDTO)this.getStatisticMeasurement().getStatistic()).getPoint().getLatitude().doubleValue());
				this.setStartLongitude(((LocationSensorMeasurementDTO)this.getStatisticMeasurement().getStatistic()).getPoint().getLongitude().doubleValue());

				meas_from = new Date().getTime();
				Thread.sleep(this.getPeriod());
				
			} catch (InterruptedException e) {
				System.out.println("\n\n"+Thread.currentThread().getName()+"/"+Thread.currentThread().getId()+" : "+"Caught me during Sleep\n\n");
				break;
			}
		}

	}

	public SensorMeasurementStatisticDTO getMeasurement(Long meas_from, double startLat, double startLong, double destLat, double destLon, int minLocStep, int maxLocStep) {

		double step = (double)(minLocStep + (int)(Math.random() * ((maxLocStep - minLocStep) + 1)));
		LocationSensorMeasurementDTO measurement = new LocationSensorMeasurementDTO();
		measurement.setPoint(getLocationMeasurementValue(startLat, startLong, destLat, destLon, step));
		measurement.setSensor(getSensor());
		measurement.setTimestamp(new Date().getTime());
		this.getStatisticMeasurement().setStatistic(measurement);
		Long meas_to = new Date().getTime();
		PeriodDTO measurementPeriod = new PeriodDTO();
		measurementPeriod.setDateFrom(meas_from);
		measurementPeriod.setDateTo( meas_to);
		this.getStatisticMeasurement().setPeriod(measurementPeriod);
		this.getStatisticMeasurement().setSamplingPeriod(this.getPeriod());
		return getStatisticMeasurement();
	}


	private PointDTO getLocationMeasurementValue(double lat1, double lon1, double lat2, double lon2, double step) {
		/*
		 * use range (choose proper type) to determine randomness of generated values
		 */

		DirectLineCalculations2D calc = new DirectLineCalculations2D();
		GlobalCoordinates coordinates = calc.TwoDimensionalDirectCalculation(lat1, lon1, lat2, lon2, step);
		PointDTO measuredPoint = new PointDTO(new BigDecimal(coordinates.getLatitude()), new BigDecimal(coordinates.getLongitude()), null);
		return measuredPoint;
	}

	public double getStartLatitude() {
		return startLatitude;
	}

	public void setStartLatitude(double startLatitude) {
		this.startLatitude = startLatitude;
	}

	public double getStartLongitude() {
		return startLongitude;
	}

	public void setStartLongitude(double startLongitude) {
		this.startLongitude = startLongitude;
	}

	public double getDestLatitude() {
		return destLatitude;
	}

	public void setDestLatitude(double destLatitude) {
		this.destLatitude = destLatitude;
	}

	public double getDestLongitude() {
		return destLongitude;
	}

	public void setDestLongitude(double destLongitude) {
		this.destLongitude = destLongitude;
	}

	public int getMinStep() {
		return minStep;
	}

	public void setMinStep(int minStep) {
		this.minStep = minStep;
	}

	public int getMaxStep() {
		return maxStep;
	}

	public void setMaxStep(int maxStep) {
		this.maxStep = maxStep;
	}

}
