package eu.esponder.df.statistic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Correlation extends SensorStatistics {

	public List<Double> CalculateAutoCorrelation(Long SensorID,Date dFrom,Date dTo)
	{
		List<Double> pList=LoadSensorSnapshots(SensorID,dFrom,dTo);
		
		return CalculateAutoCorrelation(pList);	
	}
	
	public List<Double> CalculateAutoCorrelationLatest(Long SensorID,Date dFrom)
	{
		
		List<Double> pList=LoadSensorSnapshots(SensorID,dFrom);
		
		return CalculateAutoCorrelation(pList);
	}
	
	public List<Double> CalculateAutoCorrelationUsingMeasurements(Long SensorID,int iNumber)
	{
		List<Double> pList=LoadSensorSnapshots(SensorID,iNumber);
		
		return CalculateAutoCorrelation(pList);
	}
	
	private List<Double> CalculateAutoCorrelation(List<Double> pList)
	{
		List<Double> pReturn = new ArrayList<Double>();
		Double sum= new Double(0);
		
		for (int i=0;i<pList.size();i++)
		{
			sum=new Double(0);
			for(int j=0;j<pList.size()-1;j++)
			{
				sum=sum+(pList.get(j)*pList.get(j+1));
			}
			pReturn.add(sum);
		}
		return pReturn;
	}
}
