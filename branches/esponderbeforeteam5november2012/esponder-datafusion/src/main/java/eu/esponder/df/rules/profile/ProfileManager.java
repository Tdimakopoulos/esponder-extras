package eu.esponder.df.rules.profile;

import eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType;

public class ProfileManager {

	public ProfileData GetProfileNameForSensor() {
		ProfileData pReturn = new ProfileData();

		pReturn.setSzProfileName("Esponder.Sensors");
		pReturn.setSzProfileType(RuleEngineType.DRL_RULES);
		pReturn.setSzDecisionTable("NA");
		pReturn.setHasDecisionTable(false);
		
		return pReturn;
	}
	
	public ProfileData GetProfileNameForAction() {
		ProfileData pReturn = new ProfileData();

		pReturn.setSzProfileName("Esponder.Action");
		pReturn.setSzProfileType(RuleEngineType.DRL_RULES);
		pReturn.setSzDecisionTable("NA");
		pReturn.setHasDecisionTable(false);
		
		return pReturn;
	}
	
	public ProfileData GetProfileNameForEsponder() {
		ProfileData pReturn = new ProfileData();

		pReturn.setSzProfileName("esponder");
		pReturn.setSzProfileType(RuleEngineType.ALL_PACKAGE);
		pReturn.setSzDecisionTable("NA");
		pReturn.setHasDecisionTable(false);
		
		return pReturn;
	}
	
	
}
