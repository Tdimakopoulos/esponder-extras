package eu.esponder.df.ruleengine.utilities;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import eu.esponder.df.ruleengine.settings.DroolSettings;
import eu.esponder.df.ruleengine.utilities.actorlookup.ActorConfigurationXML;
import eu.esponder.df.ruleengine.utilities.actorlookup.ActorXMLFileEntry;
import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;
import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXMLConfiguration;

public class RuleLookup {

	DroolSettings pSettings = new DroolSettings(false);

	String szActorsFile = "Actions-Lookup.xml";
	
	String szRuleResultsFile = "RuleResults-Lookup.xml";

	public List<ActorXMLFileEntry> pEntries = new LinkedList<ActorXMLFileEntry>();
	
	public List<RuleResultsXML> pEntries2 = new LinkedList<RuleResultsXML>();



	public enum RuleLookupType {
		DLU_ACTORS, DLU_RULERESULTS
	}

	public RuleLookup()
	{
		File theDir = new File(pSettings.getSzTmpRepositoryDirectoryPath());

		  
		  if (!theDir.exists())
		  {
		    System.out.println("Warning Local Repository Directory is not Exist : Create.");
		    theDir.mkdir();
		  }
		  
		  theDir = new File(pSettings.getSzTmpRepositoryDirectoryPath()+"lookup"+File.separatorChar);

		  
		  if (!theDir.exists())
		  {
		    System.out.println("Warning Local Lookup Directory is not Exist : Create.");
		    theDir.mkdir();
		  }
	}
	
	private String GetPathForLookupDir()
	{
		return pSettings.getSzTmpRepositoryDirectoryPath()+"lookup"+File.separatorChar;
	}
	
	public List<RuleResultsXML> getEntries() {
		return pEntries2;
	}

	public void setEntries(List<RuleResultsXML> entries) {
		this.pEntries2 = entries;
	}

	public void Initialize() {
		pEntries = new ArrayList<ActorXMLFileEntry>();
	}
	
	public void Initialize2() {
		pEntries2 = new ArrayList<RuleResultsXML>();
	}

	public void AddXMLEntry(ActorXMLFileEntry pEntry) {

		pEntries.add(pEntry);
	}

	public void AddRuleResults(RuleResultsXML pNewResults)
	{
		pEntries2.add(pNewResults);
	}
	
	public String ActorLocationCheck(String szActor, double lat,
			double lon, double alt, double radius)
	{
		return NewActorLocationMeasurment("Operation",
				"Move", szActor, "Esponder Location",  lat,
				 lon,  alt,  radius) ;
	}
	
	public String NewActorLocationMeasurment(String szOperation,
			String szAction, String szActor, String szPlaceName, double lat,
			double lon, double alt, double radius) {
		/*
		 * Check to see if the actor is inside the xml on Operation - Move
		 * section - Actor is not on the section - Do nothing. - Actor is on the
		 * section - Call Process Actor Movements
		 */
		String results = "";
		boolean bFound = false;
		try {
			ReadActorXMLFile(szActorsFile);
		} catch (Exception e) {
			return "0 - Error on Loading the XML LookupList";
		}

		for (int i = 0; i < pEntries.size(); i++) {

			if (szActor.equalsIgnoreCase(pEntries.get(i).getActor()))
				if (pEntries.get(i).getAction().equalsIgnoreCase("Move")) {
					results = ProcessActorMovements(szOperation, szAction,
							szActor, szPlaceName, lat, lon, alt, radius,
							pEntries.get(i).getLat(), pEntries.get(i).getLot(),
							pEntries.get(i).getAlt(), pEntries.get(i)
									.getRadius());
					if (results
							.equalsIgnoreCase("3 - Actor is Reached Final Destination")) {
						// update location from Move to Moved
						ActorXMLFileEntry pFileEntry = new ActorXMLFileEntry();
						pFileEntry = pEntries.get(i);
						pFileEntry.setSzString2("Moved");
						pEntries.set(i, pFileEntry);
					}
					SaveActorOnFile(szOperation, szAction, szActor,
							szPlaceName, lat, lon, alt, radius);
					bFound = true;
				}
		}
		if (!bFound)
			results = "1 - No Actor Found";

		if (bFound)
			try {
				WriteXMLFile(RuleLookupType.DLU_ACTORS);
			} catch (Exception e) {
				results = "0 - Error on Updating the XML LookupList";
			}

		return results;
	}

	private String ProcessActorMovements(String szOperation, String szAction,
			String szActor, String szPlaceName, double lat, double lon,
			double alt, double radius, double flat, double flon, double falt,
			double fradius) {
		String szReturn = "2 - Actor is Moving Toward Goal";

		/*
		 * Aditional Checks - Nice to have ..... Check to see if actor left the
		 * current location and moving toward the final destination - If no
		 * raise event No Start Moving
		 * 
		 * Check to see if actor still on the same small circle for a long time
		 * - If Yes then raise a error event Not MOVING
		 * 
		 * Check to see if actor moving on the direction toward the final point
		 * - Get currrent position and find bearing then compare with location -
		 * if no raise Error Moving
		 */

		double distance = distFrom(lat, lon, flat, flon);

		if (distance <= radius)
			szReturn = "3 - Actor is Reached Final Destination";

		return szReturn;
	}

	private double distFrom(double lat1, double lng1, double lat2, double lng2) {
		double earthRadius = 3958.75;
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
				* Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = earthRadius * c;

		int meterConversion = 1609;

		return dist * meterConversion;
	}

	public void SaveActionOnFile(String szOperation, String szAction,
			String szActor, String szPlaceName, double lat, double lon,
			double alt, double radius) {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		ActorXMLFileEntry pEntry2 = new ActorXMLFileEntry();
		pEntry2.setdDouble1(lat);
		pEntry2.setdDouble2(lon);
		pEntry2.setdDouble3(alt);
		pEntry2.setdDouble4(radius);
		pEntry2.setSzString1(szOperation);
		pEntry2.setSzString2(szAction);
		pEntry2.setSzString3(szActor);
		pEntry2.setSzString4(szPlaceName);
		pEntry2.setSzString5(dateFormat.format(date));
		pEntry2.setSzString6("N/A");
		pEntries.add(pEntry2);
	}

	public void SaveActorOnFile(String szOperation, String szAction,
			String szActor, String szPlaceName, double lat, double lon,
			double alt, double radius) {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		ActorXMLFileEntry pEntry2 = new ActorXMLFileEntry();
		pEntry2.setdDouble1(lat);
		pEntry2.setdDouble2(lon);
		pEntry2.setdDouble3(alt);
		pEntry2.setdDouble4(radius);
		pEntry2.setSzString1(szOperation);
		pEntry2.setSzString2(szAction);
		pEntry2.setSzString3(szActor);
		pEntry2.setSzString4(szPlaceName);
		pEntry2.setSzString5(dateFormat.format(date));
		pEntry2.setSzString6("N/A");
		pEntries.add(pEntry2);
	}

	public void ReadXMLFile(RuleLookupType LookupType) throws Exception {
		if (LookupType == RuleLookupType.DLU_ACTORS) {
			ReadActorXMLFile(szActorsFile);
		} else if (LookupType == RuleLookupType.DLU_RULERESULTS) {
			ReadRuleResultsXMLFile(szRuleResultsFile);
		}
	}
	
	public  List<RuleResultsXML> ReadXMLFileWithReturn() throws Exception {
		
			return ReadRuleResultsXMLFileWithReturn(szRuleResultsFile);
		
	}

	
	
	public void WriteXMLFile(RuleLookupType LookupType) throws Exception {
		if (LookupType == RuleLookupType.DLU_ACTORS) {
			WriteActorXMLFile(szActorsFile);
		} else if (LookupType == RuleLookupType.DLU_RULERESULTS) {
			WriteRuleResultsXMLFile(szRuleResultsFile);
		}
	}

	private void ReadActorXMLFile(String fileLocation) throws Exception {
		Serializer serializer = new Persister();
		File source = new File(GetPathForLookupDir()
				+ fileLocation);
		// ActorConfigurationXML pSaveData= new ActorConfigurationXML();
		ActorConfigurationXML configurator = serializer.read(
				ActorConfigurationXML.class, source);
		pEntries = configurator.getEntries();
	}

	private void WriteActorXMLFile(String fileLocation) throws Exception {
		Serializer serializer = new Persister();

		File result = new File(GetPathForLookupDir()
				+ fileLocation);
		ActorConfigurationXML pSaveData = new ActorConfigurationXML();
		pSaveData.setEntries(pEntries);
		serializer.write(pSaveData, result);
	}
	
	private void ReadRuleResultsXMLFile(String fileLocation) throws Exception {
		Serializer serializer = new Persister();
		File source = new File(GetPathForLookupDir()
				+ fileLocation);
		// ActorConfigurationXML pSaveData= new ActorConfigurationXML();
		RuleResultsXMLConfiguration configurator = serializer.read(
				RuleResultsXMLConfiguration.class, source);
		pEntries2 = configurator.getEntries();
		
	}

	public List<RuleResultsXML> ReadRuleResultsXMLFileWithReturn(String fileLocation) throws Exception {
		Serializer serializer = new Persister();
		File source = new File(GetPathForLookupDir()
				+ fileLocation);
		// ActorConfigurationXML pSaveData= new ActorConfigurationXML();
		RuleResultsXMLConfiguration configurator = serializer.read(
				RuleResultsXMLConfiguration.class, source);
		pEntries2 = configurator.getEntries();
		return pEntries2;
	}
	
	private void WriteRuleResultsXMLFile(String fileLocation) throws Exception {
		Serializer serializer = new Persister();

		File result = new File(GetPathForLookupDir()
				+ fileLocation);
		RuleResultsXMLConfiguration pSaveData = new RuleResultsXMLConfiguration();
		pSaveData.setEntries(pEntries2);
		serializer.write(pSaveData, result);
	}
}
