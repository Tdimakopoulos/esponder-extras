package eu.esponder.df.eventhandler.bean;


import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.ejb.Stateless;

import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.df.actionmanager.location.ActionLocationManager;
import eu.esponder.df.eventhandler.dfEventHandlerRemoteService;
import eu.esponder.df.eventhandler.dfEventHandlerService;
import eu.esponder.df.ruleengine.core.DomainSpecificKnowledgeParser;
import eu.esponder.df.ruleengine.core.RuleEngineGuvnorAssets;
import eu.esponder.df.ruleengine.utilities.RuleLookup;
import eu.esponder.df.ruleengine.utilities.RuleStatistics;
import eu.esponder.df.ruleengine.utilities.RuleUtilities;
import eu.esponder.df.ruleengine.utilities.events.RuleEvents;
import eu.esponder.df.ruleengine.utilities.ruleresults.RuleResults;
import eu.esponder.df.statistic.Correlation;
import eu.esponder.df.statistic.Ratio;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.dto.model.snapshot.status.ActorSnapshotStatusDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.test.ResourceLocator;


@Stateless
public class dfEventHandlerBean implements dfEventHandlerRemoteService, dfEventHandlerService {

	private String szFlowName=null;
	private String szRulePackageName=null;
	private String szDSLName=null;
	@SuppressWarnings("unused")
	private String szDSLRName=null;
	private RuleEngineType dInferenceType=RuleEngineType.NONE;
	List<SensorSnapshotDTO> createdSensorSnapshots = new ArrayList<SensorSnapshotDTO>();
	List<ActorSnapshotDTO> createdActorSnapshots = new ArrayList<ActorSnapshotDTO>();
	
	ActorRemoteService actorService = ResourceLocator.lookup("esponder/ActorBean/remote");
	EquipmentRemoteService equipmentService = ResourceLocator.lookup("esponder/EquipmentBean/remote");
	SensorRemoteService sensorService = ResourceLocator.lookup("esponder/SensorBean/remote");
	GenericRemoteService genericService=ResourceLocator.lookup("esponder/GenericBean/remote");
	String gDecisionTableName;
	Boolean gHasDecisionTable;
	
	
	public enum RuleEngineType {
		NONE,DRL_RULES, DSL_RULES, FLOW_RULES, ALL_PACKAGE, MIX
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Rule Engine Calls and Variables
	private RuleEngineGuvnorAssets dEngine = new RuleEngineGuvnorAssets();
	private DomainSpecificKnowledgeParser ddsl = new DomainSpecificKnowledgeParser();

	public void ReinitializeEngine()
	{
		dEngine = new RuleEngineGuvnorAssets();
		ddsl = new DomainSpecificKnowledgeParser();
	}
	
	private void AddServicesInDrools()
	{
		AddObjects(sensorService);
		AddObjects(actorService);
		AddObjects(equipmentService);
		AddObjects(genericService);
	}
	
	public void AddDTOObjects(Object fact) {
		//System.out.println("Adding DTO Objetc");
		dEngine.InferenceEngineAddDTOObject(fact);
		ddsl.AddDTOObject(fact);
	}

	public void AddObjects(Object fact) {
		dEngine.InferenceEngineAddObject(fact);
		ddsl.AddObject(fact);
	}

	private void AddKnowledgeByCategory(String szCategory) {
		try {
			dEngine.InferenceEngineAddKnowledgeForCategory(szCategory);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Category");
			e.printStackTrace();
		}
		ddsl.AddKnowledgeForCategory(szCategory);
	}

	private void AddDecisionTable(String szDecisionTable) throws FileNotFoundException
	{
		dEngine.AddDecisionTable(szDecisionTable);
		ddsl.AddDecisionTable(szDecisionTable);
	}
	
	private void AddKnowledgeByPackage(String szPackage) {
		try {
			dEngine.InferenceEngineAddKnowledgeForPackage(szPackage);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Package");
			e.printStackTrace();
		}
		ddsl.AddKnowledgeForPackage(szPackage);
	}

	private void ExecuteRules() {
		// run simple drl rules
		dEngine.InferenceEngineRunAssets();

	}

	private void ExecuteFlow() {
		// run all processes
		ddsl.ExecuteFlow();

		// run dslr rules based on dsl and
		ddsl.RunRules();

		// close session only if all processes has finished
		ddsl.Dispose();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void LoadKnowledge()
	{
		if (gHasDecisionTable)
		{
			try {
				AddDecisionTable(gDecisionTableName);
			} catch (FileNotFoundException e) {
			System.out.println("Error on Adding Decision Table");
			}
		}
		//System.out.println("Load Knowledge Start -- Type :"+RuleEngineType.ALL_PACKAGE+" Name : "+szRulePackageName);
		if(this.dInferenceType==RuleEngineType.ALL_PACKAGE)
		{
			//System.out.println("Load Package : "+szRulePackageName);
			AddKnowledgeByPackage(szRulePackageName);
		}

		if(this.dInferenceType==RuleEngineType.DRL_RULES)
		{
			//System.out.println("Load Category : "+szRulePackageName);
			AddKnowledgeByCategory(szRulePackageName);
		}

		if(this.dInferenceType==RuleEngineType.FLOW_RULES)
		{
			AddKnowledgeByCategory(szFlowName);
		}

		if(this.dInferenceType==RuleEngineType.DSL_RULES)
		{
			AddKnowledgeByCategory(szDSLName);
		}


	}

	public void ProcessRules()
	{
		////////////////////////////////////////////////////////////////////////
		//Load Default Objects into drools
		// RuleUtils
		// MathUtils
		// EventUtils
		// RuleResultsUtils
		RuleStatistics pStatistics = new RuleStatistics();
		RuleUtilities pUtils = new RuleUtilities();
		RuleResults pResults = new RuleResults();
		RuleEvents pEvents = new RuleEvents();
		RuleLookup pLookup = new RuleLookup();
		Correlation pCorrelation = new Correlation();
		Ratio pRate = new Ratio();
		ActionLocationManager pActionManager = new ActionLocationManager();
		
		AddObjects(pCorrelation);
		AddObjects(pRate);
		AddObjects(pActionManager);
		AddObjects(pStatistics);
		AddObjects(pUtils);
		AddObjects(pResults);
		AddObjects(pEvents);
		AddObjects(pLookup);
		
		//load services into working memory
		AddServicesInDrools();
		////////////////////////////////////////////////////////////////////////

		//Execute Rules using package, DRL,DSL,DSLR,Flow
		if (this.dInferenceType==RuleEngineType.ALL_PACKAGE)
		{
			ExecuteFlow();
			ExecuteRules();
		}

		//Execute Rules using Mix, DRL,DSL,DSLR,Flow
		if (this.dInferenceType==RuleEngineType.MIX)
		{
			ExecuteFlow();
			ExecuteRules();
		}

		//Execute Rules using  DRL
		if (this.dInferenceType==RuleEngineType.DRL_RULES)
		{
			ExecuteRules();
		}

		//Execute Rules using  DSL
		if (this.dInferenceType==RuleEngineType.DSL_RULES)
		{
			ExecuteFlow();
		}

		//Execute Rules using  Flow or/and DRSR
		if (this.dInferenceType==RuleEngineType.FLOW_RULES)
		{
			ExecuteFlow();
		}
	}

	

	//TODO Check Snapshot Transformation Status (working or not)
	private List<Object> createSnapshotsFromEnvelope(SensorMeasurementStatisticEnvelopeDTO envelope, Long userID) {
		createdSensorSnapshots.clear();
		createdActorSnapshots.clear();
		if(sensorService != null) {
			List<Object> createdSnapshots = new ArrayList<Object>();
			for(SensorMeasurementStatisticDTO statistic : envelope.getMeasurementStatistics()) {
				if(statistic.getStatistic() instanceof ArithmeticSensorMeasurementDTO) {
					SensorSnapshotDTO snapshotDTO = transformArithmeticMeasurementToSnapshot(statistic);
					if(snapshotDTO != null){
						snapshotDTO = sensorService.createSensorSnapshotRemote(snapshotDTO, userID);
						if(snapshotDTO !=null)
						{
							createdSnapshots.add(snapshotDTO);
							createdSensorSnapshots.add(snapshotDTO);
							
						}
					}
					else
						System.out.println("ArithmeticSensorSnapshot has not been persisted, empty snapshot found...");
				}
				else if (statistic.getStatistic() instanceof LocationSensorMeasurementDTO) {
					ActorSnapshotDTO actorSnapshotDTO = null;
					try {
						actorSnapshotDTO = transformLocationMeasurementToSnapshot(statistic);
					} catch (ClassNotFoundException e) {
						System.err.println("DF: Error - Create Snapshot For Actor");
					}
					if(actorSnapshotDTO != null) {
						actorSnapshotDTO = actorService.createActorSnapshotRemote(actorSnapshotDTO, userID);
						if(actorSnapshotDTO !=null){
							createdSnapshots.add(actorSnapshotDTO);
							createdActorSnapshots.add(actorSnapshotDTO);
							
						}
					}
					else
						System.out.println("SensorSnapshot has not been persisted, empty snapshot found...");
				}
			}
			//AddDTOObjects(createdSensorSnapshots);
			//AddDTOObjects(createdActorSnapshots);
			return createdSnapshots;
		}
		else {
			System.out.println("Cannot access SensorRemoteService...");
			return null;
		}

	}

	private SensorSnapshotDTO transformArithmeticMeasurementToSnapshot(SensorMeasurementStatisticDTO statistic) {
		ArithmeticSensorMeasurementDTO arithmeticMeasurementDTO = (ArithmeticSensorMeasurementDTO) statistic.getStatistic();
		SensorSnapshotDTO snapshot = new SensorSnapshotDTO();
		snapshot.setPeriod(statistic.getPeriod());
		snapshot.setSensor(statistic.getStatistic().getSensor());
		snapshot.setStatisticType(statistic.getStatisticType());
		//TODO 	Decide whether each working snapshot automatically takes a "WORKING" status or
		//		it is derived from the (missing yet) sensor status
		snapshot.setStatus(SensorSnapshotStatusDTO.WORKING);
		snapshot.setValue(arithmeticMeasurementDTO.getMeasurement().toString());
		return snapshot;
	}

	private ActorSnapshotDTO transformLocationMeasurementToSnapshot(SensorMeasurementStatisticDTO statistic) throws ClassNotFoundException {
		LocationSensorMeasurementDTO locationMeasurementDTO = (LocationSensorMeasurementDTO) statistic.getStatistic();
		ActorSnapshotDTO snapshot = new ActorSnapshotDTO();
		snapshot.setPeriod(statistic.getPeriod());
		
		PointDTO point1 = locationMeasurementDTO.getPoint();
		

		SphereDTO sphere1 = new SphereDTO();
		sphere1.setCentre(point1);
		sphere1.setRadius(new BigDecimal(0));

		sphere1.setTitle(new Random().toString());
		SphereDTO sphere1p=(SphereDTO) genericService.createEntityRemote(sphere1, new Long(1));
		
		snapshot.setLocationArea(sphere1p);
		//TODO 	Decide whether each working snapshot automatically takes a "WORKING" status or
		//		it is derived from the (missing yet) sensor status
		ActorDTO actor = equipmentService.findByIdRemote(statistic.getStatistic().getSensor().getEquipmentId()).getActor();
		snapshot.setActor(actor);
		snapshot.setStatus(ActorSnapshotStatusDTO.ACTIVE);
		return snapshot;
	}

	public List<Object> CreateSensorSnapshot(ESponderEvent<?> pEvent)
	{
		
		SensorMeasurementStatisticEnvelopeDTO pobject= (SensorMeasurementStatisticEnvelopeDTO)pEvent.getEventAttachment();
		List<Object> pPersistedSnapshots=createSnapshotsFromEnvelope(pobject,new Long(1));
		return pPersistedSnapshots;
	}


	
	public void SetDecisionTable(String DecisionTableName,Boolean HasDecisionTable)
	{
		gDecisionTableName=DecisionTableName;
		gHasDecisionTable=HasDecisionTable;
	}

	public void SetRuleEngineType(RuleEngineType dType,String szPackageNameOrFlowName)
	{
		SetRuleType(dType,szPackageNameOrFlowName,null,null);
	}

	public void SetRuleEngineType(RuleEngineType dType,String szDSLName,String szDSLRName)
	{
		SetRuleType(dType,null,szDSLName,szDSLRName);
	}

	private void SetRuleType(RuleEngineType dType,String szPackageNameOrFlowName,String szDSLNamep,String szDSLRNamep)
	{
		if (dType==RuleEngineType.DRL_RULES)
		{
			szRulePackageName=szPackageNameOrFlowName;
			SetInferenceType(dType);
		}
		if (dType==RuleEngineType.ALL_PACKAGE)
		{
			szRulePackageName=szPackageNameOrFlowName;
			dInferenceType=RuleEngineType.ALL_PACKAGE;
		}
		if (dType==RuleEngineType.FLOW_RULES)
		{
			szFlowName=szPackageNameOrFlowName;
			SetInferenceType(dType);
		}
		if(dType==RuleEngineType.DSL_RULES)
		{
			szDSLName=szDSLNamep;
			szDSLRName=szDSLRNamep;
			SetInferenceType(dType);
		}
	}

	private void SetInferenceType(RuleEngineType dType)
	{
		if (dInferenceType==RuleEngineType.ALL_PACKAGE)
		{}else{
			if (dInferenceType==RuleEngineType.NONE)
				dInferenceType=dType;
			else
				dInferenceType=RuleEngineType.MIX;
		}
	}
}

