package eu.esponder.df.ruleengine.utilities;

public class SensorTypeHelper {

	int itype;
	int iworking;
	Long iSensorID;
	
	public void SetSensorID(Long sensorid)
	{
		iSensorID=sensorid;
	}
	
	public Long GetSensorID()
	{
		return iSensorID;
	}
	public void SetWorking ( int piworking)
	{
		iworking=piworking;
	}
	
	public int GetSensorStatus()
	{
		return iworking;
	}
	public void SetType(int pitype)
	{
		itype=pitype;
	}
	public int GetSensorType()
	{
		return itype;
	}
}
