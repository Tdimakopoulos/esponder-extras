package eu.esponder.df.statistic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;

import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.util.ejb.ServiceLocator;

public class SensorStatistics {

	List<Double> pReturn = new ArrayList<Double>();
	
	
	
	public void SetList(List<Double> pList)
	{
		pReturn=pList;
	}
	
	public List<Double> LoadSensorSnapshots(Long SensorID,Date From,Date To)
	{
		List<SensorSnapshotDTO> pSnapshots=getSensorRemoteService().findAllSensorSnapshotsFromToRemote(SensorID, From.getTime(), To.getTime());
		
		for (int i=0;i<pSnapshots.size();i++)
		{
			pReturn.add(pSnapshots.get(i).GetValueAsDouble());
		}
		return pReturn;
	}
	
	public List<Double> LoadSensorSnapshots(Long SensorID,Date From)
	{
	
		List<SensorSnapshotDTO> pSnapshots=getSensorRemoteService().findAllSensorSnapshotsFromToRemote(SensorID, From.getTime(), (new Date()).getTime());
		
		for (int i=0;i<pSnapshots.size();i++)
		{
			pReturn.add(pSnapshots.get(i).GetValueAsDouble());
		}
		return pReturn;
	}
	
	public List<Double> LoadSensorSnapshots(Long SensorID,int isize)
	{
		SensorDTO pSensor = getSensorRemoteService().findSensorByIdRemote(SensorID);
		
		try {
			List<SensorSnapshotDTO> pSnapshots=getSensorRemoteService().findAllSensorSnapshotsBySensorRemote(pSensor, isize);
			for (int i=0;i<pSnapshots.size();i++)
			{
				pReturn.add(pSnapshots.get(i).GetValueAsDouble());
			}
		} catch (ClassNotFoundException e) {
			System.out.println("Error on Loading Sensor Snapshots");
		}
		return pReturn;
	}
	
	private SensorRemoteService getSensorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
}
