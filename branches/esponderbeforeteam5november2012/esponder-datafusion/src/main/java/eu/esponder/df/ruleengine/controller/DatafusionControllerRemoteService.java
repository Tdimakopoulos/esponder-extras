/*
 * 
 */
package eu.esponder.df.ruleengine.controller;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;
import eu.esponder.event.model.ESponderEvent;



// TODO: Auto-generated Javadoc
/**
 * The Interface DatafusionControllerRemoteService.
 */
@Remote
public interface DatafusionControllerRemoteService extends DatafusionControllerService {

	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerService#EsponderEventReceivedHandler(eu.esponder.event.model.ESponderEvent)
	 */
	public void EsponderEventReceivedHandler(ESponderEvent<?> pEvent);
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerService#ReinitializeRepository()
	 */
	public void ReinitializeRepository();
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerService#DeleteRepository()
	 */
	public void DeleteRepository();
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerService#SetToLive()
	 */
	public void SetToLive();
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerService#SetToLocal()
	 */
	public void SetToLocal();
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerService#GetRuleResults()
	 */
	public List<RuleResultsXML> GetRuleResults() throws Exception;
}

