package eu.esponder.df.ruleengine.utilities.ruleresults.object;

import org.simpleframework.xml.Element;

public class RuleResultsXML {

	@Element
	String szRuleName;
	@Element
	String szResultsText1;
	@Element
	String szResultsText2;
	@Element
	String szResultsText3;
	@Element
	private
	String szResultsText4;
	public String getSzRuleName() {
		return szRuleName;
	}
	public void setSzRuleName(String szRuleName) {
		this.szRuleName = szRuleName;
	}
	public String getSzResultsText1() {
		return szResultsText1;
	}
	public void setSzResultsText1(String szResultsText1) {
		this.szResultsText1 = szResultsText1;
	}
	public String getSzResultsText2() {
		return szResultsText2;
	}
	public void setSzResultsText2(String szResultsText2) {
		this.szResultsText2 = szResultsText2;
	}
	public String getSzResultsText3() {
		return szResultsText3;
	}
	public void setSzResultsText3(String szResultsText3) {
		this.szResultsText3 = szResultsText3;
	}
	public String getSzResultsText4() {
		return szResultsText4;
	}
	public void setSzResultsText4(String szResultsText4) {
		this.szResultsText4 = szResultsText4;
	}
}
