package eu.esponder.df.ruleengine.utilities;

public class ActorMovementValues {

	String pActorname;
	Double plog;
	Double plat;
	Double palt;
	Double pradius;
	public String getpActorname() {
		return pActorname;
	}
	public void setpActorname(String pActorname) {
		this.pActorname = pActorname;
	}
	public Double getPlog() {
		return plog;
	}
	public void setPlog(Double plog) {
		this.plog = plog;
	}
	public Double getPlat() {
		return plat;
	}
	public void setPlat(Double plat) {
		this.plat = plat;
	}
	public Double getPalt() {
		return palt;
	}
	public void setPalt(Double palt) {
		this.palt = palt;
	}
	public Double getPradius() {
		return pradius;
	}
	public void setPradius(Double pradius) {
		this.pradius = pradius;
	}
	
}
