/**
 * EngineKnowledgeLocalRepository
 * 
 * This Java class manage the storage of the rules from the guvnor web repository to the local repository
 * no web service calls to the guvnor take part in here. 
 *
 * @Project   esponder
 * @package   Datafusion
*/

package eu.esponder.df.ruleengine.core;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author tdim
 *
 */
public class EngineKnowledgeLocalRepository {

	RuleEngineRulesAssetsHelper dRules= new RuleEngineRulesAssetsHelper();
	RuleEngineRulesAssetsHelper dDSL= new RuleEngineRulesAssetsHelper();
	RuleEngineRulesAssetsHelper drf= new RuleEngineRulesAssetsHelper();
	
	/**
	 * Constructor
	 */
	public EngineKnowledgeLocalRepository()
	{
		dRules.Initialize();
		dDSL.Initialize();
	}
	
	/**
	 * @param szRuleSourceURL - rule source url from guvnor web service
	 * @param szFilePath - file path to local rule repository
	 * @param szRuleContents - rule contents ( this is the actual rule)
	 */
	public void AddNewRuleOnLocalRepository(String szRuleSourceURL,String szFilePath,String szRuleContents)
	{
		SaveRuleToFile(szFilePath+GetRuleNameFromSourceURL(szRuleSourceURL)+".drl",szRuleContents);
		dRules.AddSourcePoint(szFilePath+GetRuleNameFromSourceURL(szRuleSourceURL)+".drl");
	}
	
	/**
	 * @param szRuleSourceURL - rule source url from guvnor web service
	 * @param szFilePath - file path to local rule repository
	 * @param szRuleContents - rule contents ( this is the actual rule)
	 */
	public void AddNewRuleDSLOnLocalRepository(String szRuleSourceURL,String szFilePath,String szRuleContents,String szType)
	{
		SaveRuleToFile(szFilePath+GetRuleNameFromSourceURL(szRuleSourceURL)+"."+szType,szRuleContents);
		if(szType=="dslr")
		{
			dRules.AddSourcePoint(szFilePath+GetRuleNameFromSourceURL(szRuleSourceURL)+"."+szType);
			
		}
		if(szType=="dsl")
		{
			dDSL.AddSourcePoint(szFilePath+GetRuleNameFromSourceURL(szRuleSourceURL)+"."+szType);
			
		}
		if(szType=="rf")
		{
			drf.AddSourcePoint(szFilePath+GetRuleNameFromSourceURL(szRuleSourceURL)+"."+szType);
			
		}
	}
	
	/**
	 * @return string array of all rules
	 */
	public String[] ReturnRF()
	{
		String[] szReturn=new String[drf.GetRulesNumber()];
		for (int i=0;i<drf.GetRulesNumber();i++)
			szReturn[i]=drf.GetRuleSourcePoint(i);
		return szReturn;
	}
	
	/**
	 * @return string array of all rules
	 */
	public String[] ReturnRules()
	{
		String[] szReturn=new String[dRules.GetRulesNumber()];
		for (int i=0;i<dRules.GetRulesNumber();i++)
			szReturn[i]=dRules.GetRuleSourcePoint(i);
		return szReturn;
	}
	
	/**
	 * @return string array of all rules
	 */
	public String[] ReturnDSL()
	{
		String[] szReturn=new String[dDSL.GetRulesNumber()];
		for (int i=0;i<dDSL.GetRulesNumber();i++)
			szReturn[i]=dDSL.GetRuleSourcePoint(i);
		return szReturn;
	}
	
	/**
	 * @param szURL this is the source URL to access the rule from the guvnor rep
	 * @return the rule name which is the last part from the url before /source
	 */
	public String GetRuleNameFromSourceURL(String szURL) {
		int ifindlast;
		int ifindbeforelast;

		ifindlast = szURL.lastIndexOf("/");
		ifindbeforelast = szURL.lastIndexOf("/", ifindlast - 1);

		String szRuleName = "";
		szRuleName = szURL.substring(ifindbeforelast + 1, ifindlast);

		return szRuleName;
	}
	
	/**
	 * @param szFilename the filename 
	 * @param szContents the rule contents
	 */
	private void SaveRuleToFile(String szFilename,String szContents)
	{
		try {
		    BufferedWriter out = new BufferedWriter(new FileWriter(szFilename));
		    out.write(szContents);
		    out.close();
		} catch (IOException e) {
		}
	}	
}
