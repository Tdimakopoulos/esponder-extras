package eu.esponder.df.rules.profile;

import eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType;

public class ProfileData {

	private String szProfileName;
	private RuleEngineType szProfileType;
	String szDecisionTable;
	boolean hasDecisionTable;
	
	public String getSzDecisionTable() {
		return szDecisionTable;
	}
	public void setSzDecisionTable(String szDecisionTable) {
		this.szDecisionTable = szDecisionTable;
	}
	public boolean isHasDecisionTable() {
		return hasDecisionTable;
	}
	public void setHasDecisionTable(boolean hasDecisionTable) {
		this.hasDecisionTable = hasDecisionTable;
	}
	public String getSzProfileName() {
		return szProfileName;
	}
	public void setSzProfileName(String szProfileName) {
		this.szProfileName = szProfileName;
	}
	public RuleEngineType getSzProfileType() {
		return szProfileType;
	}
	public void setSzProfileType(RuleEngineType szProfileType) {
		this.szProfileType = szProfileType;
	}
	
	
}
