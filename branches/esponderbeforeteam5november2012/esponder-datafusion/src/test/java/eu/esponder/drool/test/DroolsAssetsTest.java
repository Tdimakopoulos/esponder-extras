package eu.esponder.drool.test;

import org.testng.annotations.Test;

import eu.esponder.df.ruleengine.core.RuleEngineGuvnorAssets;

/**
 * @author tdim
 *
 */
public class DroolsAssetsTest {

	/**
	 * This test gets all rules for category from the remote repository and store them into the 
	 * local repository (make sure the settings are correct)
	 */
	@Test
	public void GetAssetsByCategory()
	{
		System.out.println("==============================================");
		System.out.println("                Category Test                 ");
		RuleEngineGuvnorAssets dAssets= new RuleEngineGuvnorAssets();
		String[] rules=dAssets.PopulateLocalRepositoryForCategory("Sensors");
		for (int i=0;i<rules.length;i++)
			System.out.println(rules[i]);
		System.out.println("==============================================");
	}
	
	/**
	 * This test gets all rules for package from the remote repository and store them into the 
	 * local repository (make sure the settings are correct)
	 */
	@Test
	public void GetAssetsByPackage()
	{
		System.out.println("==============================================");
		System.out.println("                Package Test                  ");
		RuleEngineGuvnorAssets dAssets= new RuleEngineGuvnorAssets();
		String[] rules=dAssets.PopulateLocalRepositoryForPackage("esponder");
		for (int i=0;i<rules.length;i++)
			System.out.println(rules[i]);
		System.out.println("==============================================");
	}
}
