package eu.esponder.test.rest.crisis.actions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.status.CrisisContextSnapshotStatusDTO;
import eu.esponder.rest.client.ResteasyClient;

public class ActionsTest {

	ObjectMapper mapper = new ObjectMapper();


	@Test
	public void testCreateCrisisContext() throws RuntimeException, Exception {

		String SPHERE_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/generic/create";
		String SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/view";

		PointDTO pointDTO = new PointDTO(new BigDecimal(3), new BigDecimal(3), new BigDecimal(3)); 
		SphereDTO sphereDTO = new SphereDTO(pointDTO, new BigDecimal(2), "Fire Brigade Drill Location Area 1");

		String sphereJSON = mapper.writeValueAsString(sphereDTO);
		printJSON(sphereJSON);
		ResteasyClient sphereClient =new ResteasyClient(SPHERE_SERVICE_URI, "application/json");
		Map<String, String> Params = new HashMap<String, String>();
		Params.put("userID", "1");
		String sphereResult = sphereClient.post(Params, sphereJSON);
		SphereDTO spherePersisted = mapper.readValue(sphereResult, SphereDTO.class);


		CrisisContextDTO crisisContextDTO = new CrisisContextDTO();
		crisisContextDTO.setCrisisLocation(spherePersisted);
		crisisContextDTO.setTitle("Fire Brigade Drill test");

		String crisisContextJSON = mapper.writeValueAsString(crisisContextDTO);
		printJSON(crisisContextJSON);

		ResteasyClient crisisClient =new ResteasyClient(SERVICE_URI+"/crisisContext/create", "application/json");
		String crisisResult = crisisClient.post(Params, crisisContextJSON);
		//CrisisContextDTO crisisContextPersisted = mapper.readValue(crisisResult, CrisisContextDTO.class);
	}


	@Test
	public void testCreateCrisisContextSnapshot() throws RuntimeException, Exception {

		String CRISISSNAPSHOT_CREATE_SERVICE = "http://localhost:8080/esponder-restful/crisis/view/crisisContextSnapshot/create";

		Map<String, String> ccParams = new HashMap<String, String>();
		ccParams.put("userID", "1");
		ccParams.put("crisisContextTitle","Fire Brigade Drill test");
		ResteasyClient ccClient =new ResteasyClient("http://localhost:8080/esponder-restful/crisis/view/crisisContext/findByTitle", "application/json");
		String ccJSON = ccClient.get(ccParams);

		if(ccJSON != null) {
			printJSON(ccJSON);
			CrisisContextDTO ccRetrieved = mapper.readValue(ccJSON, CrisisContextDTO.class);

			CrisisContextSnapshotDTO ccSnapshot = new CrisisContextSnapshotDTO();
			ccSnapshot.setCrisisContext(ccRetrieved);
			ccSnapshot.setPeriod(new PeriodDTO(new Date().getTime(), new Date().getTime()));
			ccSnapshot.setStatus(CrisisContextSnapshotStatusDTO.STARTED);

			String ccSnapshotJSON = mapper.writeValueAsString(ccSnapshot);
			System.out.println("\nCRISIS_CONTEXT_SNAPSHOT");
			printJSON(ccSnapshotJSON);

			Map<String, String> ccSnapshotParams = new HashMap<String, String>();
			ccSnapshotParams.put("userID", "1");
			ResteasyClient ccSnapshotClient =new ResteasyClient(CRISISSNAPSHOT_CREATE_SERVICE, "application/json");
			String ccSnapshotJSONRetrieved = ccSnapshotClient.post(ccParams, ccSnapshotJSON);

			printJSON(ccSnapshotJSONRetrieved);

		}
		else
			System.out.println("No Crisis Context Object found...");
	}


	@Test
	public void testCreateAction() throws RuntimeException, Exception {




	}



	private void printJSON(String jsonStr) {
		System.out.println("\n\n******* JSON * START *******");
		System.out.println(jsonStr);
		System.out.println("******** JSON * END ********\n\n");
	}

}
