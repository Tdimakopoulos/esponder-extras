/*
 * 
 */
package eu.esponder.model.crisis.action;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.snapshot.action.ActionSnapshot;
import eu.esponder.model.type.ActionType;

// TODO: Auto-generated Javadoc
/**
 * The Class Action.
 */
@Entity
@Table(name="action")
@NamedQueries({
	@NamedQuery(name="Action.findByTitle", query="select a from Action a where a.title=:title")
})
public class Action extends ESponderEntity<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7256777022590609183L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_ID")
	private Long id;
	
	/** The title. */
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	/** The action type. */
	@ManyToOne
	@JoinColumn(name="ACTION_TYPE_ID", nullable=false)
	private ActionType actionType;
	
	/** The action parts. */
	@OneToMany(mappedBy="action")
	private Set<ActionPart> actionParts;
	
	/** The snapshots. */
	@OneToMany(mappedBy="action")
	private Set<ActionSnapshot> snapshots;
	
	/** The crisis context. */
	@ManyToOne
	@JoinColumn(name="CRISIS_CONTEXT_ID", nullable=false)
	private CrisisContext crisisContext;
	
	/** The parent. */
	@ManyToOne
	@JoinColumn(name="PARENT_ID")
	private Action parent;
	
	/** The children. */
	@OneToMany(mappedBy="parent")
	private Set<Action> children;
	
	/** The action objectives. */
	@OneToMany(mappedBy="action")
	private Set<ActionObjective> actionObjectives;
	
	/** The severity level. */
	@Enumerated
	@Column(name="SEVERITY_LEVEL")
	private SeverityLevel severityLevel;
	
	/** The action operation. */
	@Enumerated
	@Column(name="ACTION_OPERATION")
	private ActionOperationEnum actionOperation;
	
	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the action type.
	 *
	 * @return the action type
	 */
	public ActionType getActionType() {
		return actionType;
	}

	/**
	 * Sets the action type.
	 *
	 * @param actionType the new action type
	 */
	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	/**
	 * Gets the action parts.
	 *
	 * @return the action parts
	 */
	public Set<ActionPart> getActionParts() {
		return actionParts;
	}

	/**
	 * Sets the action parts.
	 *
	 * @param actionParts the new action parts
	 */
	public void setActionParts(Set<ActionPart> actionParts) {
		this.actionParts = actionParts;
	}

	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<ActionSnapshot> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<ActionSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	/**
	 * Gets the crisis context.
	 *
	 * @return the crisis context
	 */
	public CrisisContext getCrisisContext() {
		return crisisContext;
	}

	/**
	 * Sets the crisis context.
	 *
	 * @param crisisContext the new crisis context
	 */
	public void setCrisisContext(CrisisContext crisisContext) {
		this.crisisContext = crisisContext;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public Action getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(Action parent) {
		this.parent = parent;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public Set<Action> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the new children
	 */
	public void setChildren(Set<Action> children) {
		this.children = children;
	}

	/**
	 * Gets the action objectives.
	 *
	 * @return the action objectives
	 */
	public Set<ActionObjective> getActionObjectives() {
		return actionObjectives;
	}

	/**
	 * Sets the action objectives.
	 *
	 * @param actionObjectives the new action objectives
	 */
	public void setActionObjectives(Set<ActionObjective> actionObjectives) {
		this.actionObjectives = actionObjectives;
	}

	/**
	 * Gets the severity level.
	 *
	 * @return the severity level
	 */
	public SeverityLevel getSeverityLevel() {
		return severityLevel;
	}

	/**
	 * Sets the severity level.
	 *
	 * @param severityLevel the new severity level
	 */
	public void setSeverityLevel(SeverityLevel severityLevel) {
		this.severityLevel = severityLevel;
	}

	/**
	 * Gets the action operation.
	 *
	 * @return the action operation
	 */
	public ActionOperationEnum getActionOperation() {
		return actionOperation;
	}

	/**
	 * Sets the action operation.
	 *
	 * @param actionOperation the new action operation
	 */
	public void setActionOperation(ActionOperationEnum actionOperation) {
		this.actionOperation = actionOperation;
	}
	
}
