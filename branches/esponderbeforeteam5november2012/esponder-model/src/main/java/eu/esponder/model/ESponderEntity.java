/*
 * 
 */
package eu.esponder.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import eu.esponder.model.user.ESponderUser;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderEntity.
 *
 * @param <T> the generic type
 */
@MappedSuperclass
public abstract class ESponderEntity<T> implements Identifiable<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4578786002919885934L;

	/** The description. */
	@Column(name="DESCR", length=1023)
	protected String description;

	/** The record status. */
	@Column(name="RECORD_STATUS")
	private Integer recordStatus;

	/** The create audit. */
	@Embedded
	@AssociationOverride(name="who", joinColumns=@JoinColumn(name="CREATED_BY"))
	@AttributeOverride(name="when", column=@Column(name="CREATION_DATE", nullable=false)) 
	private ActionAudit createAudit; 

	/** The update audit. */
	@Embedded
	@AssociationOverride(name="who", joinColumns=@JoinColumn(name="UPDATED_BY"))
	@AttributeOverride(name="when", column=@Column(name="UPDATE_DATE"))
	private ActionAudit updateAudit;

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the record status.
	 *
	 * @return the record status
	 */
	public Integer getRecordStatus() {
		return recordStatus;
	}

	/**
	 * Sets the record status.
	 *
	 * @param recordStatus the new record status
	 */
	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}

	/**
	 * Pre persist.
	 */
	@PrePersist
	public void prePersist() {
		if (null == this.createAudit) {
			this.createAudit = new ActionAudit();
			ESponderUser user = (ESponderUser) ActionAudit.getWho(Thread.currentThread());
			if (null != user) {
				this.createAudit.setWho(user);
			}
		}
	}

	/**
	 * Pre update.
	 */
	@PreUpdate
	public void preUpdate() {
		this.updateAudit = new ActionAudit();
		ESponderUser user = (ESponderUser) ActionAudit.getWho(Thread.currentThread());
		if (null != user) {
			this.updateAudit.setWho(user);
		}
	}

}

