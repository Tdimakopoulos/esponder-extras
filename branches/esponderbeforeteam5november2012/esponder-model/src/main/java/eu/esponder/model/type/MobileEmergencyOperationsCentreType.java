/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class MobileEmergencyOperationsCentreType.
 */
@Entity
@DiscriminatorValue("MEOC")
public final class MobileEmergencyOperationsCentreType extends OperationsCentreType {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9129311973994216522L;
	
}
