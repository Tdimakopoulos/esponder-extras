/*
 * 
 */
package eu.esponder.model.events.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
/**
 * The Class OsgiEventsEntity.
 */
@Entity
@Table(name="osgievents")
@NamedQueries({
	@NamedQuery(name="OsgiEventsEntity.findByID", query="select t from OsgiEventsEntity t where t.id=:id"),
	@NamedQuery(name="OsgiEventsEntity.findBySeverity", query="select t from OsgiEventsEntity t where t.severity=:severity"),
	@NamedQuery(name="OsgiEventsEntity.findBySourceid", query="select t from OsgiEventsEntity t where t.sourceid=:sourceid"),
	@NamedQuery(name="OsgiEventsEntity.findAll", query="select t from OsgiEventsEntity t")
})
public class OsgiEventsEntity extends ESponderEntity<Long>{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4128510854440416914L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="OSGIEVENTS_ID")
	private Long id;
	
	/** The journal msg. */
	@Column(name="OSGIEVENTS_JOURNALMSG", nullable=true,  length=50000)
	private String journalMsg;
	
	/** The journal msg info. */
	@Column(name="OSGIEVENTS_JOURNALMSGINFO", nullable=true,  length=50000)
	private String journalMsgInfo;
	
	/** The time stamp. */
	@Column(name="OSGIEVENTS_TIMESTAMP", nullable=true,  length=50000)
	private Long timeStamp;
	
	/** The severity. */
	@Column(name="OSGIEVENTS_SEVERITY", nullable=true,  length=50000)
	private String severity;
	
	
	
	/** The attachment. */
	@Column(name="OSGIEVENTS_ATTACHMENT", nullable=true,  length=50000)
	private String attachment;
	
	
	
	/** The source. */
	@Column(name="OSGIEVENTS_SOURCETYPE", nullable=true,  length=50000)
	private String source;
	
	/** The sourceid. */
	@Column(name="OSGIEVENTS_SOURCEID", nullable=true)
	private Long sourceid;

	/**
	 * Gets the sourceid.
	 *
	 * @return the sourceid
	 */
	public Long getSourceid() {
		return sourceid;
	}

	/**
	 * Sets the sourceid.
	 *
	 * @param sourceid the new sourceid
	 */
	public void setSourceid(Long sourceid) {
		this.sourceid = sourceid;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the journal msg.
	 *
	 * @return the journal msg
	 */
	public String getJournalMsg() {
		return journalMsg;
	}

	/**
	 * Sets the journal msg.
	 *
	 * @param journalMsg the new journal msg
	 */
	public void setJournalMsg(String journalMsg) {
		this.journalMsg = journalMsg;
	}

	/**
	 * Gets the journal msg info.
	 *
	 * @return the journal msg info
	 */
	public String getJournalMsgInfo() {
		return journalMsgInfo;
	}

	/**
	 * Sets the journal msg info.
	 *
	 * @param journalMsgInfo the new journal msg info
	 */
	public void setJournalMsgInfo(String journalMsgInfo) {
		this.journalMsgInfo = journalMsgInfo;
	}

	/**
	 * Gets the time stamp.
	 *
	 * @return the time stamp
	 */
	public Long getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Sets the time stamp.
	 *
	 * @param timeStamp the new time stamp
	 */
	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * Gets the severity.
	 *
	 * @return the severity
	 */
	public String getSeverity() {
		return severity;
	}

	/**
	 * Sets the severity.
	 *
	 * @param severity the new severity
	 */
	public void setSeverity(String severity) {
		this.severity = severity;
	}

	/**
	 * Gets the attachment.
	 *
	 * @return the attachment
	 */
	public String getAttachment() {
		return attachment;
	}

	/**
	 * Sets the attachment.
	 *
	 * @param attachment the new attachment
	 */
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	/**
	 * Gets the source.
	 *
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Sets the source.
	 *
	 * @param source the new source
	 */
	public void setSource(String source) {
		this.source = source;
	}

}
