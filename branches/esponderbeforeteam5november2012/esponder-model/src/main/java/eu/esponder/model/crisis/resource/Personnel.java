/*
 * 
 */
package eu.esponder.model.crisis.resource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.category.PersonnelCategory;
import eu.esponder.model.crisis.resource.plan.PlannableResource;

// TODO: Auto-generated Javadoc
/**
 * The Class Personnel.
 */
@Entity
@Table(name="personnel")
@NamedQueries({
	@NamedQuery(name="Personnel.findByTitle", query="select p from Personnel p where p.title=:title"),
	@NamedQuery(name="Personnel.findAll", query="select p from Personnel p")
})
public class Personnel extends PlannableResource {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3981713854457369736L;

	/** The first name. */
	@Column(name="FIRST_NAME")
	private String firstName;
	
	/** The last name. */
	@Column(name="LAST_NAME")
	private String lastName;
	
	/** The organisation. */
	@ManyToOne
	@JoinColumn(name="ORGANISATION_ID")
	private Organisation organisation;
	
	/**
	 * Gets the organisation.
	 *
	 * @return the organisation
	 */
	public Organisation getOrganisation() {
		return organisation;
	}

	/**
	 * Sets the organisation.
	 *
	 * @param organisation the new organisation
	 */
	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}

	/**
	 * Indicates whether this person is available to be assigned to a crisis or not.
	 * A particular service needs to be developed to keep this indicator up to date
	 */
	@Column(name="AVAILABILITY")
	private boolean availability;
	
	/** The personnel category. */
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private PersonnelCategory personnelCategory;
	
	/**
	 * Gets the personnel category.
	 *
	 * @return the personnel category
	 */
	public PersonnelCategory getPersonnelCategory() {
		return personnelCategory;
	}

	/**
	 * Sets the personnel category.
	 *
	 * @param personnelCategory the new personnel category
	 */
	public void setPersonnelCategory(PersonnelCategory personnelCategory) {
		this.personnelCategory = personnelCategory;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Checks if is availability.
	 *
	 * @return true, if is availability
	 */
	public boolean isAvailability() {
		return availability;
	}

	/**
	 * Sets the availability.
	 *
	 * @param availability the new availability
	 */
	public void setAvailability(boolean availability) {
		this.availability = availability;
	}

}
