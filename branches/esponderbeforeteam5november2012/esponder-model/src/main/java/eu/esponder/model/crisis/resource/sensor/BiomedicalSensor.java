/*
 * 
 */
package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class BiomedicalSensor.
 */
@Entity
@DiscriminatorValue("BIOMED")
public abstract class BiomedicalSensor extends Sensor {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6993139817263562576L;

}
