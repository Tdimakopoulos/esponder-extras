/*
 * 
 */
package eu.esponder.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.WeakHashMap;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eu.esponder.model.user.ESponderUser;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionAudit.
 */
@Embeddable
public class ActionAudit implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7234944727522572621L;
	
	/** The t locals. */
	private static Map<Thread, ThreadLocal<ESponderUser>> tLocals = new WeakHashMap<Thread, ThreadLocal<ESponderUser>>();
	
	/**
	 * Store who.
	 *
	 * @param thread the thread
	 * @param who the who
	 */
	public static void storeWho(Thread thread, ESponderUser who) {
		ThreadLocal<ESponderUser> tLocal = tLocals.get(thread);
		if (null == tLocal) {
			tLocal = new ThreadLocal<ESponderUser>();
		}
		tLocal.set(who);
		tLocals.put(thread, tLocal);
	}

	/**
	 * Gets the who.
	 *
	 * @param thread the thread
	 * @return the who
	 */
	public static  ESponderUser getWho(Thread thread) {
		ThreadLocal<ESponderUser> threadLocal = tLocals.get(thread);
		return null != threadLocal ? threadLocal.get() : null;
	}
	
	/** The who. */
	@ManyToOne
	private ESponderUser who;
	
	/** The when. */
	@Temporal(TemporalType.TIMESTAMP)
	private Date when;

	/**
	 * Instantiates a new action audit.
	 */
	public ActionAudit() {
		super();
		this.when = new Date();
	}

	/**
	 * Gets the who.
	 *
	 * @return the who
	 */
	public ESponderUser getWho() {
		return who;
	}

	/**
	 * Sets the who.
	 *
	 * @param who the new who
	 */
	public void setWho(ESponderUser who) {
		this.who = who;
	}

	/**
	 * Gets the when.
	 *
	 * @return the when
	 */
	public Date getWhen() {
		return when;
	}

	/**
	 * Sets the when.
	 *
	 * @param when the new when
	 */
	public void setWhen(Date when) {
		this.when = when;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ActionAudit [who=" + who + ", when=" + when + "]";
	}

}
