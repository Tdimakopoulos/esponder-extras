/*
 * 
 */
package eu.esponder.model.crisis.action;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.ReusableResource;
import eu.esponder.model.snapshot.action.ActionPartSnapshot;


// TODO: Auto-generated Javadoc
/**
 * The Class ActionPart.
 */
@Entity
@Table(name="action_part")
@NamedQueries({
	@NamedQuery(name="ActionPart.findByTitle", query="select ap from ActionPart ap where ap.title=:title")
})
public class ActionPart extends ESponderEntity<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -441973801978686457L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_PART_ID")
	private Long id;
	
	/** The title. */
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	/** The actor. */
	@ManyToOne
	@JoinColumn(name="ACTOR_ID", nullable=false)
	private Actor actor;
	
	/** The action. */
	@ManyToOne
	@JoinColumn(name="ACTION_ID", nullable=false)
	private Action action;
	
	/** The snapshots. */
	@OneToMany(mappedBy="actionPart")
	private Set<ActionPartSnapshot> snapshots;
	
	/*
	 * These resources are *used by* the actor. For example the "truck" in the following sentence: 
	 * Move this telecom equipment kits using a truck
	 */
	/** The used reusable resources. */
	@OneToMany(mappedBy="actionPart")
	private Set<ReusableResource> usedReusableResources;
	
	/*
	 *  These resources are *used by* the actor. For example the "truck" in the following sentence: 
	 *  Move this telecom equipment kits using a truck
	 */
	/** The used consumable resources. */
	@OneToMany(mappedBy="actionPart")
	private Set<ConsumableResource> usedConsumableResources;
	
	/** The severity level. */
	@Enumerated
	@Column(name="SEVERITY_LEVEL")
	private SeverityLevel severityLevel;
	
	/** The action operation. */
	@Enumerated
	@Column(name="ACTION_OPERATION")
	private ActionOperationEnum actionOperation;
	
	/** The action part objectives. */
	@OneToMany(mappedBy="actionPart")
	private Set<ActionPartObjective> actionPartObjectives;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the actor.
	 *
	 * @return the actor
	 */
	public Actor getActor() {
		return actor;
	}

	/**
	 * Sets the actor.
	 *
	 * @param actor the new actor
	 */
	public void setActor(Actor actor) {
		this.actor = actor;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public Action getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(Action action) {
		this.action = action;
	}

	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<ActionPartSnapshot> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<ActionPartSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	/**
	 * Gets the used reusable resources.
	 *
	 * @return the used reusable resources
	 */
	public Set<ReusableResource> getUsedReusableResources() {
		return usedReusableResources;
	}

	/**
	 * Sets the used reusable resources.
	 *
	 * @param usedReusableResources the new used reusable resources
	 */
	public void setUsedReusableResources(Set<ReusableResource> usedReusableResources) {
		this.usedReusableResources = usedReusableResources;
	}

	/**
	 * Gets the used consumable resources.
	 *
	 * @return the used consumable resources
	 */
	public Set<ConsumableResource> getUsedConsumableResources() {
		return usedConsumableResources;
	}

	/**
	 * Sets the used consumable resources.
	 *
	 * @param usedConsumableResources the new used consumable resources
	 */
	public void setUsedConsumableResources(
			Set<ConsumableResource> usedConsumableResources) {
		this.usedConsumableResources = usedConsumableResources;
	}

	/**
	 * Gets the severity level.
	 *
	 * @return the severity level
	 */
	public SeverityLevel getSeverityLevel() {
		return severityLevel;
	}

	/**
	 * Sets the severity level.
	 *
	 * @param severityLevel the new severity level
	 */
	public void setSeverityLevel(SeverityLevel severityLevel) {
		this.severityLevel = severityLevel;
	}

	/**
	 * Gets the action operation.
	 *
	 * @return the action operation
	 */
	public ActionOperationEnum getActionOperation() {
		return actionOperation;
	}

	/**
	 * Sets the action operation.
	 *
	 * @param actionOperation the new action operation
	 */
	public void setActionOperation(ActionOperationEnum actionOperation) {
		this.actionOperation = actionOperation;
	}

	/**
	 * Gets the action part objectives.
	 *
	 * @return the action part objectives
	 */
	public Set<ActionPartObjective> getActionPartObjectives() {
		return actionPartObjectives;
	}

	/**
	 * Sets the action part objectives.
	 *
	 * @param actionPartObjectives the new action part objectives
	 */
	public void setActionPartObjectives(
			Set<ActionPartObjective> actionPartObjectives) {
		this.actionPartObjectives = actionPartObjectives;
	}

}
