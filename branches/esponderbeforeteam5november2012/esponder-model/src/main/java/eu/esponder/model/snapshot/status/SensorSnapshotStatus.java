/*
 * 
 */
package eu.esponder.model.snapshot.status;

// TODO: Auto-generated Javadoc
/**
 * The Enum SensorSnapshotStatus.
 */
public enum SensorSnapshotStatus {
	
	/** The working. */
	WORKING,
	
	/** The malfunctioning. */
	MALFUNCTIONING,
	
	/** The damaged. */
	DAMAGED
}
