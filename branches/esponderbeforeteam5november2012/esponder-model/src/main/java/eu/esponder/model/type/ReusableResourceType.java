/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class ReusableResourceType.
 */
@Entity
@DiscriminatorValue("REUSABLE")
public final class ReusableResourceType extends LogisticsResourceType {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3416073165515169747L;
	
	
}
