/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import eu.esponder.model.snapshot.status.MeasurementUnitEnum;


// TODO: Auto-generated Javadoc
/**
 * The Class SensorType.
 */
@Entity
@DiscriminatorValue("SENSOR")
public class SensorType extends ESponderType {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2832452320920328627L;
	
	/** The measurement unit. */
	@Column(name="MEASUREMENT_UNIT")
	private MeasurementUnitEnum measurementUnit;
	
	/**
	 * Gets the measurement unit.
	 *
	 * @return the measurement unit
	 */
	public MeasurementUnitEnum getMeasurementUnit() {
		return measurementUnit;
	}

	/**
	 * Sets the measurement unit.
	 *
	 * @param measurementUnit the new measurement unit
	 */
	public void setMeasurementUnit(MeasurementUnitEnum measurementUnit) {
		this.measurementUnit = measurementUnit;
	}
	
}
