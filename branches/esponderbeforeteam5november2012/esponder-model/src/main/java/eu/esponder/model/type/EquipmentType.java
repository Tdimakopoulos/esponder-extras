/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class EquipmentType.
 */
@Entity
@DiscriminatorValue("EQUIPMENT")
public final class EquipmentType extends ESponderType {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7218166530641914993L;
	
}
