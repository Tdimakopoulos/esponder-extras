/*
 * 
 */
package eu.esponder.model.crisis.action;

// TODO: Auto-generated Javadoc
/**
 * The Enum ActionOperationEnum.
 */
public enum ActionOperationEnum {

	/** The move. */
	MOVE,
	
	/** The transport. */
	TRANSPORT,
	
	/** The fix. */
	FIX
}
