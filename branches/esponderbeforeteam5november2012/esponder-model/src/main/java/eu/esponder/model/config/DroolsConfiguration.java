/*
 * 
 */
package eu.esponder.model.config;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

// TODO: Auto-generated Javadoc
/**
 * The Class DroolsConfiguration.
 */
@Entity
@DiscriminatorValue("DROOLS")
public class DroolsConfiguration extends ESponderConfigParameter {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1472802157259896819L;
	
}
