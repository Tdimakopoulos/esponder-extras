/*
 * 
 */
package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class GasSensor.
 */
@Entity
@DiscriminatorValue("ENV_GAS")
public class GasSensor extends EnvironmentalSensor implements ArithmeticMeasurementSensor {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5760021964807956319L;

}
