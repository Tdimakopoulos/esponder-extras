/*
 * 
 */
package eu.esponder.model.crisis.view;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

// TODO: Auto-generated Javadoc
/**
 * The Class ReferencePOI.
 */
@Entity
@DiscriminatorValue("REF")
@NamedQueries({
	@NamedQuery(name="ReferencePOI.findByOperationsCentre", query="select r from ReferencePOI r where r.operationsCentre.id=:operationsCentreID"),
	@NamedQuery(name="ReferencePOI.findByReferencePOIId", query="select s from ReferencePOI s where s.id=:referencePOIId"),
	@NamedQuery(name="ReferencePOI.findByTitle", query="select s from ReferencePOI s where s.title=:referencePOITitle")
})
public class ReferencePOI extends ResourcePOI {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7856655848723729892L;
	
	/** The reference file. */
	@Column(name="REF_FILE")
	private String referenceFile;
	
	/** The average size. */
	@Column(name="AVERAGE_SIZE")
	private Float averageSize;

	/**
	 * Gets the reference file.
	 *
	 * @return the reference file
	 */
	public String getReferenceFile() {
		return referenceFile;
	}

	/**
	 * Sets the reference file.
	 *
	 * @param referenceFile the new reference file
	 */
	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

	/**
	 * Gets the average size.
	 *
	 * @return the average size
	 */
	public Float getAverageSize() {
		return averageSize;
	}

	/**
	 * Sets the average size.
	 *
	 * @param averageSize the new average size
	 */
	public void setAverageSize(Float averageSize) {
		this.averageSize = averageSize;
	}

	
}
