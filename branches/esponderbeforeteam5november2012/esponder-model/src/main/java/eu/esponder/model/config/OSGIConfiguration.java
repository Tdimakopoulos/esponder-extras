/*
 * 
 */
package eu.esponder.model.config;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

// TODO: Auto-generated Javadoc
/**
 * The Class OSGIConfiguration.
 */
@Entity
@DiscriminatorValue("OSGI")
public class OSGIConfiguration extends ESponderConfigParameter {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1472802157259993847L;
	
}
