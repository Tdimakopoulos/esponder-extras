/*
 * 
 */
package eu.esponder.model.crisis.resource.category;

import javax.persistence.MappedSuperclass;

// TODO: Auto-generated Javadoc
/**
 * The Class LogisticsCategory.
 */
@MappedSuperclass
public abstract class LogisticsCategory extends PlannableResourceCategory {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4667107943033401089L;

}