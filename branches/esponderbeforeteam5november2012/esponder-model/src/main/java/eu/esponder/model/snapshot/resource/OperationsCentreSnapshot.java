/*
 * 
 */
package eu.esponder.model.snapshot.resource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.snapshot.SpatialSnapshot;
import eu.esponder.model.snapshot.status.OperationsCentreSnapshotStatus;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreSnapshot.
 */
@Entity
@Table(name="operations_centre_snapshot")
@NamedQueries({
	@NamedQuery(
		name="OperationsCentreSnapshot.findByOperationsCentreAndDate",
		query="SELECT s FROM OperationsCentreSnapshot s WHERE s.operationsCentre.id = :operationsCentreID AND s.period.dateTo <= :maxDate AND s.period.dateTo = " +
				"(SELECT max(s.period.dateTo) FROM OperationsCentreSnapshot s WHERE s.operationsCentre.id = :operationsCentreID)")
})
public class OperationsCentreSnapshot extends SpatialSnapshot<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9169930917504748793L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="OPERATIONS_CENTRE_SNAPSHOT_ID")
	private Long id;
	
	/** The status. */
	@Enumerated(EnumType.STRING)
	@Column(name="OPERATIONS_CENTRE_SNAPSHOT_STATUS", nullable=false)
	private OperationsCentreSnapshotStatus status;
	
	/** The operations centre. */
	@ManyToOne
	@JoinColumn(name="OPERATIONS_CENTRE_ID", nullable=false)
	private OperationsCentre operationsCentre;
	
	/** The previous. */
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private OperationsCentreSnapshot previous;
	
	/** The next. */
	@OneToOne(mappedBy="previous")
	private OperationsCentreSnapshot next;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public OperationsCentreSnapshotStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(OperationsCentreSnapshotStatus status) {
		this.status = status;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OperationsCentre getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OperationsCentre operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public OperationsCentreSnapshot getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previous the new previous
	 */
	public void setPrevious(OperationsCentreSnapshot previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public OperationsCentreSnapshot getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(OperationsCentreSnapshot next) {
		this.next = next;
	}

}
