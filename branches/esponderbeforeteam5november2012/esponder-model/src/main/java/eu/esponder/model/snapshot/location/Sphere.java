/*
 * 
 */
package eu.esponder.model.snapshot.location;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class Sphere.
 */
@Entity
@DiscriminatorValue("SPHERE")
public class Sphere extends LocationArea {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8670528613518375245L;

	/**
	 * Instantiates a new sphere.
	 */
	public Sphere() {
		super();
	}

	/**
	 * Instantiates a new sphere.
	 *
	 * @param centre the centre
	 * @param radius the radius
	 */
	public Sphere(Point centre, BigDecimal radius) {
		super();
		this.centre = centre;
		this.radius = radius;
	}

	/** The centre. */
	@Embedded
	private Point centre;
	
	/** The radius. */
	@Column(name="RADIUS")
	private BigDecimal radius;

	/**
	 * Gets the centre.
	 *
	 * @return the centre
	 */
	public Point getCentre() {
		return centre;
	}

	/**
	 * Sets the centre.
	 *
	 * @param centre the new centre
	 */
	public void setCentre(Point centre) {
		this.centre = centre;
	}

	/**
	 * Gets the radius.
	 *
	 * @return the radius
	 */
	public BigDecimal getRadius() {
		return radius;
	}

	/**
	 * Sets the radius.
	 *
	 * @param radius the new radius
	 */
	public void setRadius(BigDecimal radius) {
		this.radius = radius;
	}
	
}
