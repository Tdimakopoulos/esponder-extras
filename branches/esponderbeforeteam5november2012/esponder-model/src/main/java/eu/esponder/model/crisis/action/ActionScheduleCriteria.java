/*
 * 
 */
package eu.esponder.model.crisis.action;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eu.esponder.model.snapshot.status.ActionSnapshotStatus;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionScheduleCriteria.
 */
@Embeddable
public class ActionScheduleCriteria implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6947038776364763386L;
	
	/** The status. */
	@Enumerated(EnumType.STRING)
	@Column(name="PREREQUISITE_STATUS")
	private ActionSnapshotStatus status;

	
	/** The date after. */
	@Column(name="DATE_AFTER")
	private Long dateAfter;

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ActionSnapshotStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ActionSnapshotStatus status) {
		this.status = status;
	}

	/**
	 * Gets the date after.
	 *
	 * @return the date after
	 */
	public Long getDateAfter() {
		return dateAfter;
	}

	/**
	 * Sets the date after.
	 *
	 * @param dateAfter the new date after
	 */
	public void setDateAfter(Long dateAfter) {
		this.dateAfter = dateAfter;
	}

}
