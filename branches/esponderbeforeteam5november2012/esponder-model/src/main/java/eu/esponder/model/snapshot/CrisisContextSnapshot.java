/*
 * 
 */
package eu.esponder.model.snapshot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.snapshot.status.CrisisContextSnapshotStatus;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisContextSnapshot.
 */
@Entity
@Table(name="crisis_context_snapshot")
public class CrisisContextSnapshot extends SpatialSnapshot<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3318916280076239374L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CRISIS_CONTEXT_SNAPSHOT_ID")
	private Long id;
	
	/** The crisis context. */
	@ManyToOne
	@JoinColumn(name="CRISIS_CONTEXT_ID", nullable=false)
	private CrisisContext crisisContext;
	
	/** The status. */
	@Enumerated(EnumType.STRING)
	@Column(name="CRISIS_CONTEXT_SNAPSHOT_STATUS", nullable=false)
	private CrisisContextSnapshotStatus status;
	
	/** The previous. */
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private CrisisContextSnapshot previous;
	
	/** The next. */
	@OneToOne(mappedBy="previous")
	private CrisisContextSnapshot next;
	
	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the crisis context.
	 *
	 * @return the crisis context
	 */
	public CrisisContext getCrisisContext() {
		return crisisContext;
	}

	/**
	 * Sets the crisis context.
	 *
	 * @param crisisContext the new crisis context
	 */
	public void setCrisisContext(CrisisContext crisisContext) {
		this.crisisContext = crisisContext;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public CrisisContextSnapshotStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(CrisisContextSnapshotStatus status) {
		this.status = status;
	}

	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public CrisisContextSnapshot getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previous the new previous
	 */
	public void setPrevious(CrisisContextSnapshot previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public CrisisContextSnapshot getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(CrisisContextSnapshot next) {
		this.next = next;
	}
		
}
