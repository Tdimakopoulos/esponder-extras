/*
 * 
 */
package eu.esponder.model.crisis.resource.category;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.type.OperationsCentreType;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreCategory.
 */
@Entity
@Table(name="operations_centre_category")
@NamedQueries({
	@NamedQuery(name="OperationsCentreCategory.findByType", query="select c from OperationsCentreCategory c where c.operationsCentreType=:operationsCentreType")
})
public class OperationsCentreCategory extends PlannableResourceCategory {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5912869430610187184L;

	/** The operations centre type. */
	@OneToOne
	@JoinColumn(name="OPERATIONS_CENTRE_TYPE_ID", nullable=false)
	private OperationsCentreType operationsCentreType;
	
	/**
	 * TODO: Refactor This to point to RegisteredOperationsCentre.
	 *
	 * @return the operations centre type
	 */
//	@OneToMany(mappedBy="operationsCentreCategory")
//	private Set<OperationsCentre> operationsCentres;

	public OperationsCentreType getOperationsCentreType() {
		return operationsCentreType;
	}

	/**
	 * Sets the operations centre type.
	 *
	 * @param operationsCentreType the new operations centre type
	 */
	public void setOperationsCentreType(OperationsCentreType operationsCentreType) {
		this.operationsCentreType = operationsCentreType;
	}

//	public Set<OperationsCentre> getOperationsCentres() {
//		return operationsCentres;
//	}
//
//	public void setOperationsCentres(Set<OperationsCentre> operationsCentres) {
//		this.operationsCentres = operationsCentres;
//	}


}
