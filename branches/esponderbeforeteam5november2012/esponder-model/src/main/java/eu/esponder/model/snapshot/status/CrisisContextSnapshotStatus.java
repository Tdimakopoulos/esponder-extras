/*
 * 
 */
package eu.esponder.model.snapshot.status;

// TODO: Auto-generated Javadoc
/**
 * The Enum CrisisContextSnapshotStatus.
 */
public enum CrisisContextSnapshotStatus {
	
	/** The started. */
	STARTED,
	
	/** The resolved. */
	RESOLVED,
	
	/** The unresolved. */
	UNRESOLVED
}
