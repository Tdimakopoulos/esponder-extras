/*
 * 
 */
package eu.esponder.model.crisis.resource;

import javax.persistence.MappedSuperclass;

import eu.esponder.model.crisis.resource.plan.PlannableResource;

// TODO: Auto-generated Javadoc
/**
 * The Class LogisticsResource.
 */
@MappedSuperclass
public abstract class LogisticsResource extends PlannableResource {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4407982840446904207L;

}
