/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class StrategicActorType.
 */
@Entity
@DiscriminatorValue("STR_ACTOR")
public final class StrategicActorType extends ActorType {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1734043841975502816L;

}
