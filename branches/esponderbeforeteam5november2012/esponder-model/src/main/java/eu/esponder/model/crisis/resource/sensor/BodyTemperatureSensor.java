/*
 * 
 */
package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class BodyTemperatureSensor.
 */
@Entity
@DiscriminatorValue("BIOMED_BODY_TEMP")
public class BodyTemperatureSensor extends BiomedicalSensor implements ArithmeticMeasurementSensor {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2017421161095334173L;

}
