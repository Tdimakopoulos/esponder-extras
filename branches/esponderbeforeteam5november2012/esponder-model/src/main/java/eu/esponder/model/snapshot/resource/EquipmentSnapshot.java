/*
 * 
 */
package eu.esponder.model.snapshot.resource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.snapshot.Snapshot;
import eu.esponder.model.snapshot.status.EquipmentSnapshotStatus;

// TODO: Auto-generated Javadoc
/**
 * The Class EquipmentSnapshot.
 */
@Entity
@Table(name="equipment_snapshot")
@NamedQueries({
	@NamedQuery(
		name="EquipmentSnapshot.findByEquipmentAndDate",
		query="SELECT s FROM EquipmentSnapshot s WHERE s.equipment.id = :equipmentID AND s.period.dateTo <= :maxDate AND s.period.dateTo = " +
				"(SELECT max(s.period.dateTo) FROM EquipmentSnapshot s WHERE s.equipment.id = :equipmentID)")
})
public class EquipmentSnapshot extends Snapshot<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7464811282991787536L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="EQUIPMENT_SNAPSHOT_ID")
	private Long id;
	
	/** The status. */
	@Enumerated(EnumType.STRING)
	@Column(name="EQUIPMENT_SNAPSHOT_STATUS", nullable=false)
	private EquipmentSnapshotStatus status;
	
	/** The equipment. */
	@ManyToOne
	@JoinColumn(name="EQUIPMENT_ID", nullable=false)
	private Equipment equipment;
	
	/** The previous. */
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private EquipmentSnapshot previous;
	
	/** The next. */
	@OneToOne(mappedBy="previous")
	private EquipmentSnapshot next;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public EquipmentSnapshotStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(EquipmentSnapshotStatus status) {
		this.status = status;
	}

	/**
	 * Gets the equipment.
	 *
	 * @return the equipment
	 */
	public Equipment getEquipment() {
		return equipment;
	}

	/**
	 * Sets the equipment.
	 *
	 * @param equipment the new equipment
	 */
	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public EquipmentSnapshot getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previous the new previous
	 */
	public void setPrevious(EquipmentSnapshot previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public EquipmentSnapshot getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(EquipmentSnapshot next) {
		this.next = next;
	}

}
