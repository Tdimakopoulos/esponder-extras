/*
 * 
 */
package eu.esponder.model.crisis.view;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.snapshot.SpatialSnapshot;

// TODO: Auto-generated Javadoc
/**
 * The Class SketchPOISnapshot.
 */
@Entity
@Table(name="sketch_poi_snapshot")
@NamedQueries({
	@NamedQuery(name="SketchPOISnapshot.findByOperationsCentre", query="select s from SketchPOISnapshot s where s.operationsCentre.id=:operationsCentreID"),
	@NamedQuery(name="SketchPOISnapshot.findBySketchPOIId", query="select s from SketchPOISnapshot s where s.id=:sketchsnapshotPOIId"),
	@NamedQuery(name="SketchPOISnapshot.findByTitle", query="select s from SketchPOISnapshot s where s.title=:sketchsnapshotPOITitle"),
	@NamedQuery(name = "SketchPOISnapshot.findBySketchPOIAndDate", query = "SELECT s FROM SketchPOISnapshot s WHERE s.sketchPOI.id = :sketchID AND s.period.dateTo <= :maxDate AND s.period.dateTo = "
			+ "(SELECT max(s.period.dateTo) FROM SketchPOISnapshot s WHERE s.sketchPOI.id = :sketchID)"),
	@NamedQuery(name = "SketchPOISnapshot.findPreviousSnapshot", query = "Select s from SketchPOISnapshot s where s.id=(select max(snapshot.id) from SketchPOISnapshot snapshot where snapshot.sketchPOI.id = :sketchID)"),
	@NamedQuery(name = "SketchPOISnapshot.findAll", query = "Select s from SketchPOISnapshot s")
})
public class SketchPOISnapshot extends SpatialSnapshot<Long> {
	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1734142629191263542L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SKETCH_POI_ID_SNAPSHOT")
	protected Long id;
	
	/** The points. */
	@ManyToMany(cascade={CascadeType.PERSIST,CascadeType.MERGE, CascadeType.REMOVE})
	@JoinTable(name="sketch_points_snapshots",
			joinColumns=@JoinColumn(name="SKETCH_POI_ID_SNAPSHOT"),
			inverseJoinColumns=@JoinColumn(name="POINT_ID"))
	private Set<MapPoint> points;
	
	/** The http url. */
	@Embedded
	private HttpURL httpURL;
	
	/** The sketch type. */
	@Column(name="sketchType_snapshot")
	private String sketchType;
	
	/** The sketch poi. */
	@Column(name="sketchpoi_snapshot")
	private SketchPOI sketchPOI;

	/** The title. */
	@Column(name="TITLE_SNAPSHOT", nullable=false, unique=true, length=255)
	protected String title;
	
	/** The operations centre. */
	@ManyToOne
	@JoinColumn(name="OPERATIONS_CENTRE_ID_SNAPSHOT")
	protected OperationsCentre operationsCentre;
	
	/**
	 * Gets the sketch poi.
	 *
	 * @return the sketch poi
	 */
	public SketchPOI getSketchPOI() {
		return sketchPOI;
	}

	/**
	 * Sets the sketch poi.
	 *
	 * @param sketchPOI the new sketch poi
	 */
	public void setSketchPOI(SketchPOI sketchPOI) {
		this.sketchPOI = sketchPOI;
	}


	
	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the points.
	 *
	 * @return the points
	 */
	public Set<MapPoint> getPoints() {
		return points;
	}

	/**
	 * Sets the points.
	 *
	 * @param points the new points
	 */
	public void setPoints(Set<MapPoint> points) {
		this.points = points;
	}

	/**
	 * Gets the http url.
	 *
	 * @return the http url
	 */
	public HttpURL getHttpURL() {
		return httpURL;
	}

	/**
	 * Sets the http url.
	 *
	 * @param httpURL the new http url
	 */
	public void setHttpURL(HttpURL httpURL) {
		this.httpURL = httpURL;
	}

	/**
	 * Gets the sketch type.
	 *
	 * @return the sketch type
	 */
	public String getSketchType() {
		return sketchType;
	}

	/**
	 * Sets the sketch type.
	 *
	 * @param sketchType the new sketch type
	 */
	public void setSketchType(String sketchType) {
		this.sketchType = sketchType;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OperationsCentre getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OperationsCentre operationsCentre) {
		this.operationsCentre = operationsCentre;
	}
	
}
