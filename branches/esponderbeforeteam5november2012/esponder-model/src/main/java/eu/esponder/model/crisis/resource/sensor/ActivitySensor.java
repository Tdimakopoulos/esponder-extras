/*
 * 
 */
package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

// TODO: Auto-generated Javadoc
/**
 * The Class ActivitySensor.
 */
@Entity
@DiscriminatorValue("ACTIVITY")
public class ActivitySensor extends BiomedicalSensor implements EnumeratedMeasurementSensor {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7899651164427099687L;

}
