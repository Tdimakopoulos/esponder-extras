/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.collections.CollectionUtils;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.resource.category.OperationsCentreCategory;
import eu.esponder.model.crisis.resource.plan.PlannableResource;
import eu.esponder.model.crisis.view.VoIPURL;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;
import eu.esponder.model.user.ESponderUser;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentre.
 */
@Entity
@Table(name="operations_centre")
@NamedQueries({
	@NamedQuery(name="OperationsCentre.findByTitle", query="select c from OperationsCentre c where c.title=:title"),
	@NamedQuery(name="OperationsCentre.findAll", query="select c from OperationsCentre c"),
	@NamedQuery(name="OperationsCentre.findByIdAndUser", query="select c from OperationsCentre c, ESponderUser u where c.id=:operationsCentreID and u.id=:userID and u member of c.users"),
	@NamedQuery(name="OperationsCentre.findByCrisisContext", query="select c from OperationsCentre c where c.crisisContext = :crisisContext")
})
public class OperationsCentre extends PlannableResource {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1911200656062784549L;

	/** The supervisor. */
	@ManyToOne
	@JoinColumn(name="SUPERVISOR_ID")
	private OperationsCentre supervisor;
	
	/** The subordinates. */
	@OneToMany(mappedBy="supervisor")
	private Set<OperationsCentre> subordinates;
	
	/** The actors. */
	@OneToMany(mappedBy="operationsCentre")
	private Set<Actor> actors;
	
	/** The snapshots. */
	@OneToMany(mappedBy="operationsCentre")
	private Set<OperationsCentreSnapshot> snapshots;
	
	/** The users. */
	@ManyToMany(mappedBy="operationsCentres")
	private Set<ESponderUser> users;
	
	/** The operations centre category. */
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private OperationsCentreCategory operationsCentreCategory;

	/** The vo ipurl. */
	@Embedded
	private VoIPURL voIPURL;
	
	/** The crisis context. */
	@ManyToOne
	@JoinColumn(name="CRISIS_CONTEXT_ID")
	private CrisisContext crisisContext;
	
	/**
	 * Gets the crisis context.
	 *
	 * @return the crisis context
	 */
	public CrisisContext getCrisisContext() {
		return crisisContext;
	}

	/**
	 * Sets the crisis context.
	 *
	 * @param crisisContext the new crisis context
	 */
	public void setCrisisContext(CrisisContext crisisContext) {
		this.crisisContext = crisisContext;
	}

	/**
	 * Gets the operations centre category.
	 *
	 * @return the operations centre category
	 */
	public OperationsCentreCategory getOperationsCentreCategory() {
		return operationsCentreCategory;
	}

	/**
	 * Sets the operations centre category.
	 *
	 * @param operationsCentreCategory the new operations centre category
	 */
	public void setOperationsCentreCategory(
			OperationsCentreCategory operationsCentreCategory) {
		this.operationsCentreCategory = operationsCentreCategory;
	}

	/**
	 * Gets the supervisor.
	 *
	 * @return the supervisor
	 */
	public OperationsCentre getSupervisor() {
		return supervisor;
	}

	/**
	 * Sets the supervisor.
	 *
	 * @param supervisor the new supervisor
	 */
	public void setSupervisor(OperationsCentre supervisor) {
		this.supervisor = supervisor;
	}

	/**
	 * Gets the subordinates.
	 *
	 * @return the subordinates
	 */
	public Set<OperationsCentre> getSubordinates() {
		return subordinates;
	}

	/**
	 * Sets the subordinates.
	 *
	 * @param subordinates the new subordinates
	 */
	public void setSubordinates(Set<OperationsCentre> subordinates) {
		this.subordinates = subordinates;
	}

	/**
	 * Gets the actors.
	 *
	 * @return the actors
	 */
	public Set<Actor> getActors() {
		return actors;
	}

	/**
	 * Sets the actors.
	 *
	 * @param actors the new actors
	 */
	public void setActors(Set<Actor> actors) {
		this.actors = actors;
	}

	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<OperationsCentreSnapshot> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<OperationsCentreSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public Set<ESponderUser> getUsers() {
		return users;
	}

	/**
	 * Sets the users.
	 *
	 * @param users the new users
	 */
	public void setUsers(Set<ESponderUser> users) {
		this.users = users;
	}

	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURL getVoIPURL() {
		return voIPURL;
	}

	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURL voIPURL) {
		this.voIPURL = voIPURL;
	}

	/**
	 * Traverse subordinates.
	 */
	public void traverseSubordinates() {
		if (CollectionUtils.isNotEmpty(this.subordinates)) {
			for (OperationsCentre operationsCentre : this.subordinates) {
				operationsCentre.traverseSubordinates();
			}
		}
	}

}
