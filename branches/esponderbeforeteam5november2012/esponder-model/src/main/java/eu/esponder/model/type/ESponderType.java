/*
 * 
 */
package eu.esponder.model.type;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderType.
 */
@Entity
@Table(name="entity_type")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="DISCRIMINATOR")
@NamedQueries({
	@NamedQuery(name="ESponderType.findByTitle", query="select t from ESponderType t where t.title=:title"),
	@NamedQuery(name="ESponderType.findAll", query="select t from ESponderType t")
})
public abstract class ESponderType extends ESponderEntity<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5092746393389044558L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ENTITY_TYPE_ID")
	protected Long id;
	
	/** TODO: Rename the "title" field as "description". */
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	protected String title;
	
	/** The parent. */
	@ManyToOne
	@JoinColumn(name="PARENT_ID")
	protected ESponderType parent;
	
	/** The children. */
	@OneToMany(mappedBy="parent")
	protected Set<ESponderType> children;
	
	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public ESponderType getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(ESponderType parent) {
		this.parent = parent;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public Set<ESponderType> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the new children
	 */
	public void setChildren(Set<ESponderType> children) {
		this.children = children;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
	    final int PRIME = 31;
	    int result = 1;
	    result = PRIME * result + id.intValue();
	    return result;
	}
	 
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    final ESponderType other = (ESponderType) obj;
	    if (id != other.id) {
	        return false;
	    }
	    if (!this.getTitle().equals(other.getTitle())) {
	    	return false;
	    } if (!this.getParent().equals(other.getParent())) {
	    	return false;
	    }
	    return true;
	}
	
}

