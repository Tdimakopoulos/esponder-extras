/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class StrategicActionType.
 */
@Entity
@DiscriminatorValue("STR_ACTION")
public final class StrategicActionType extends ActionType {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -87605023946998985L;
	
	
}
