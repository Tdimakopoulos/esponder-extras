/*
 * 
 */
package eu.esponder.model.snapshot.location;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;

// TODO: Auto-generated Javadoc
/**
 * The Class CubeVolume.
 */
@Entity
@DiscriminatorValue("CUBE")
public class CubeVolume extends LocationArea {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3142156536477281310L;

	/**
	 * Instantiates a new cube volume.
	 */
	public CubeVolume() { }
	
	/**
	 * Instantiates a new cube volume.
	 *
	 * @param point the point
	 * @param acme the acme
	 */
	public CubeVolume(Point point, BigDecimal acme) {
		this.acme = acme;
		this.point = point;
	}
	
	/** The point. */
	@Embedded
	private Point point;
	
	/** The acme. */
	@Column(name="ACME")
	private BigDecimal acme;
	
	/**
	 * Gets the point.
	 *
	 * @return the point
	 */
	public Point getPoint() {
		return point;
	}

	/**
	 * Sets the point.
	 *
	 * @param point the new point
	 */
	public void setPoint(Point point) {
		this.point = point;
	}

	/**
	 * Gets the acme.
	 *
	 * @return the acme
	 */
	public BigDecimal getAcme() {
		return acme;
	}

	/**
	 * Sets the acme.
	 *
	 * @param acme the new acme
	 */
	public void setAcme(BigDecimal acme) {
		this.acme = acme;
	}
	

}
