/*
 * 
 */
package eu.esponder.model.crisis.view;

// TODO: Auto-generated Javadoc
/**
 * The Class VoIPURL.
 */
public class VoIPURL extends URL {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3193388189632590549L;

	/**
	 * Instantiates a new vo ipurl.
	 */
	public VoIPURL() {
		this.setProtocol("sip");
	}
}
