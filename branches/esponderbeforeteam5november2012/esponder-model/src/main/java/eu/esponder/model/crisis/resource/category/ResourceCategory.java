/*
 * 
 */
package eu.esponder.model.crisis.resource.category;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceCategory.
 */
@MappedSuperclass
public abstract class ResourceCategory extends ESponderEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2401622867316452380L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="CATEGORY_ID")
	protected Long id;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

}
