/*
 * 
 */
package eu.esponder.model.crisis.action;

// TODO: Auto-generated Javadoc
/**
 * The Enum SeverityLevel.
 */
public enum SeverityLevel {
	
	/** The minimal. */
	MINIMAL,
	
	/** The medium. */
	MEDIUM,
	
	/** The serious. */
	SERIOUS
}
