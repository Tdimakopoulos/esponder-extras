/*
 * 
 */
package eu.esponder.model.crisis.view;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.snapshot.location.Point;

// TODO: Auto-generated Javadoc
/**
 * The Class MapPoint.
 */
@Entity
@Table(name="map_point")
@NamedQueries({
	@NamedQuery(name="MapPoint.findByTitle", query="select mp from MapPoint mp where mp.title=:title")
})
public class MapPoint extends ESponderEntity<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2699139388286227165L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="POINT_ID")
	private Long id;
	
	/** The title. */
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	/** The icon. */
	@Column(name="ICON")
	private Integer icon;

	/** The point. */
	@Embedded
	private Point point;
	
	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the point.
	 *
	 * @return the point
	 */
	public Point getPoint() {
		return point;
	}

	/**
	 * Sets the point.
	 *
	 * @param point the new point
	 */
	public void setPoint(Point point) {
		this.point = point;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public Integer getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(Integer icon) {
		this.icon = icon;
	}

}
