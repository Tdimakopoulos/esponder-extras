/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class TacticalActionType.
 */
@Entity
@DiscriminatorValue("TCL_ACTION")
public final class TacticalActionType extends ActionType {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2113957125209924699L;

	
}
