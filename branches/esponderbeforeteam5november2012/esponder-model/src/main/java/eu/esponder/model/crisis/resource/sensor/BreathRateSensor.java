/*
 * 
 */
package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class BreathRateSensor.
 */
@Entity
@DiscriminatorValue("BIOMED_BREATH_RATE")
public class BreathRateSensor extends BiomedicalSensor implements ArithmeticMeasurementSensor {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5211712220701237629L;

}
