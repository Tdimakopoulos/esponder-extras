/*
 * 
 */
package eu.esponder.model.crisis.resource;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.category.PersonnelCategory;

// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelCompetence.
 */
@Entity
@Table(name="competence")
@DiscriminatorColumn(name="DISCRIMINATOR")
@NamedQueries({
	@NamedQuery(name="PersonnelCompetence.findByTitle", query="select p from PersonnelCompetence p where p.shortTitle=:shortTitle")
})
public abstract class PersonnelCompetence extends ESponderEntity<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7628260391266784842L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COMPETENCE_ID")
	private Long id;

	/** The short title. */
	@Column(name="SHORT_DESCR")
	private String shortTitle;
	
	/** The personnel category. */
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private PersonnelCategory personnelCategory;
	
	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the short title.
	 *
	 * @return the short title
	 */
	public String getShortTitle() {
		return shortTitle;
	}

	/**
	 * Sets the short title.
	 *
	 * @param shortTitle the new short title
	 */
	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}

	/**
	 * Gets the pesonnel category.
	 *
	 * @return the pesonnel category
	 */
	public PersonnelCategory getPesonnelCategory() {
		return personnelCategory;
	}

	/**
	 * Sets the pesonnel category.
	 *
	 * @param pesonnelCategory the new pesonnel category
	 */
	public void setPesonnelCategory(PersonnelCategory pesonnelCategory) {
		this.personnelCategory = pesonnelCategory;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    final PersonnelCompetence other = (PersonnelCompetence) obj;
	    if (id != other.id) {
	        return false;
	    }
	    if (!this.getPesonnelCategory().equals(other.getPesonnelCategory())) {
	    	return false;
	    }
	    if (!this.getShortTitle().equals(other.getShortTitle())) {
	    	return false;
	    }
	    if (!this.getDescription().equals(other.getDescription())) {
	    	return false;
	    }
	    if (!this.getRecordStatus().equals(other.getRecordStatus())) {
	    	return false;
	    }
	    return true;
	}

}
