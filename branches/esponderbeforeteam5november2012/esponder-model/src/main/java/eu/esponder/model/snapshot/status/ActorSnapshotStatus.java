/*
 * 
 */
package eu.esponder.model.snapshot.status;

// TODO: Auto-generated Javadoc
/**
 * The Enum ActorSnapshotStatus.
 */
public enum ActorSnapshotStatus {
	
	/** The ready. */
	READY,
	
	/** The active. */
	ACTIVE,
	
	/** The inactive. */
	INACTIVE
}
