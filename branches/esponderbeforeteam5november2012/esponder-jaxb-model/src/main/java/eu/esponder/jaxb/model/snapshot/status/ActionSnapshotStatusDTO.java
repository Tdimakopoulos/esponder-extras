package eu.esponder.jaxb.model.snapshot.status;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="ActionSnapshotStatus")
public enum ActionSnapshotStatusDTO {
	STARTED,
	PAUSED,
	COMPLETED
}
