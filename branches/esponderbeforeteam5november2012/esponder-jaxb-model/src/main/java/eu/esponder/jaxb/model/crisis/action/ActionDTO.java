package eu.esponder.jaxb.model.crisis.action;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.snapshot.action.ActionSnapshotDTO;

@XmlRootElement(name="action")
@XmlType(name="Action")
@JsonSerialize(include=Inclusion.NON_NULL)
public class ActionDTO extends ESponderEntityDTO {
	
	private String title;

	private String type;
	
	private Set<ActionPartDTO> actionParts;
	
	private ActionSnapshotDTO snapshot;
	
	private Set<ActionDTO> children;
	
	private Set<ActionObjectiveDTO> actionObjectives;
	
	private SeverityLevelDTO severityLevel;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<ActionPartDTO> getActionParts() {
		return actionParts;
	}

	public void setActionParts(Set<ActionPartDTO> actionParts) {
		this.actionParts = actionParts;
	}

	public ActionSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(ActionSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	public Set<ActionDTO> getChildren() {
		return children;
	}

	public void setChildren(Set<ActionDTO> children) {
		this.children = children;
	}

	public Set<ActionObjectiveDTO> getActionObjectives() {
		return actionObjectives;
	}

	public void setActionObjectives(Set<ActionObjectiveDTO> actionObjectives) {
		this.actionObjectives = actionObjectives;
	}

	public SeverityLevelDTO getSeverityLevel() {
		return severityLevel;
	}

	public void setSeverityLevel(SeverityLevelDTO severityLevel) {
		this.severityLevel = severityLevel;
	}
	
}
