package eu.esponder.jaxb.model.crisis.resource;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.snapshot.resource.OperationsCentreSnapshotDTO;

@XmlRootElement(name="operationsCentre")
@XmlType(name="OperationsCentre")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "actors", "subordinates", "snapshot"})
public class OperationsCentreDTO extends ResourceDTO {
	
	private Set<ActorDTO> actors;
	
	private Set<OperationsCentreDTO> subordinates;
	
	private OperationsCentreSnapshotDTO snapshot;
	
	public Set<ActorDTO> getActors() {
		return actors;
	}
	
	public void setActors(Set<ActorDTO> actors) {
		this.actors = actors;
	}
	
	public Set<OperationsCentreDTO> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(Set<OperationsCentreDTO> subordinates) {
		this.subordinates = subordinates;
	}

	public OperationsCentreSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(OperationsCentreSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	@Override
	public String toString() {
		return "OperationsCentreDTO [type=" + type + ", status=" + status
				+ ", id=" + id + ", title=" + title + "]";
	}
	
}
