package eu.esponder.controller.mapping.dozer;

import org.dozer.CustomConverter;
import org.hibernate.collection.PersistentSet;

import eu.esponder.model.crisis.resource.sensor.ActivitySensor;
import eu.esponder.model.crisis.resource.sensor.BodyTemperatureSensor;
import eu.esponder.model.crisis.resource.sensor.BreathRateSensor;
import eu.esponder.model.crisis.resource.sensor.EnvironmentTemperatureSensor;
import eu.esponder.model.crisis.resource.sensor.GasSensor;
import eu.esponder.model.crisis.resource.sensor.HeartBeatRateSensor;
import eu.esponder.model.crisis.resource.sensor.LocationSensor;


public class EquipmentSensorsFieldConverter implements CustomConverter {
	
	@Override
	public Object convert(Object destination,
			Object source, Class<?> destinationClass,
			Class<?> sourceClass) {


		if(sourceClass == PersistentSet.class) {
			
			
			for(Object i : ((PersistentSet)source) ) {
				
				
			}
			
		}
		
		if (sourceClass == ActivitySensor.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == BodyTemperatureSensor.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == BreathRateSensor.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == HeartBeatRateSensor.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == EnvironmentTemperatureSensor.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == GasSensor.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == LocationSensor.class) {
			destination = source;
			return destination;
		}
		
		return null;
	}

}
