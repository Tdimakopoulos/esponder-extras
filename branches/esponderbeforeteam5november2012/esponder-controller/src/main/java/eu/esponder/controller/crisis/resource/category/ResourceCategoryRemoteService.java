package eu.esponder.controller.crisis.resource.category;

import java.util.Set;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.EquipmentCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OperationsCentreCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.dto.model.type.RankTypeDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;

@Remote
public interface ResourceCategoryRemoteService {

	public ResourceCategoryDTO create(ResourceCategoryDTO resourceCategoryDTO, Long UserID) throws ClassNotFoundException;
	
	public OrganisationCategoryDTO findDTOByType(DisciplineTypeDTO disciplineType, OrganisationTypeDTO organizationType);
	
	public EquipmentCategoryDTO findDTOByType(EquipmentTypeDTO equipmentTypeDTO);
	
	public ConsumableResourceCategoryDTO findDTOByType(ConsumableResourceTypeDTO consumablesType);
	
	public ReusableResourceCategoryDTO findDTOByType(ReusableResourceTypeDTO reusablesType);
	
	public OperationsCentreCategoryDTO findDTOByType(OperationsCentreCategoryDTO operationsCentreCategoryDTO);
	
	public PersonnelCategoryDTO findDTOByType(RankTypeDTO rankType, Set<PersonnelCompetenceDTO> competence, OrganisationCategoryDTO organizationCategory);
	
	public OrganisationCategoryDTO updateOrganizationCategory(Long organizationCategory, Long disciplineTypeId, Long organizationTypeId,  Long organisationID, Long userId);
	
	public EquipmentCategoryDTO updateEquipmentCategory(Long equipmentCategoryId, Long equipmentTypeId, Long userId);
	
	public ConsumableResourceCategoryDTO updateConsumableResourceCategory(Long consumableCategoryId, Long consumableTypeId, Long userId);
	
	public ReusableResourceCategoryDTO updateReusableResourceCategory(Long reusableCategoryId, Long reusableTypeId,	Long userId);
	
	public PersonnelCategoryDTO updatePersonnelCategory(Long personnelCategoryId, Set<Long> competenceId, Long rankId, Long organizationCategoryId, Long userId) throws InstantiationException, IllegalAccessException;

	public ResourceCategoryDTO findByIdRemote(Class<? extends ResourceCategoryDTO> clz, Long resourceCategoryID);
	
}
