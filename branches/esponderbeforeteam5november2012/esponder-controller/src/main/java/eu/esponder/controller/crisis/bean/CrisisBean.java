package eu.esponder.controller.crisis.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.CrisisService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.crisis.resource.plan.PlannableResourcePowerDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.resource.plan.CrisisResourcePlan;
import eu.esponder.model.crisis.resource.plan.PlannableResourcePower;
import eu.esponder.model.snapshot.CrisisContextSnapshot;

@Stateless
public class CrisisBean implements CrisisService, CrisisRemoteService{

	@EJB
	private CrudService<CrisisContext> crisisCrudService;

	@EJB
	CrudService<PlannableResourcePower> powerCrudService;

	@EJB
	private CrudService<CrisisResourcePlan> crisisResourcePlanCrudService;

	@EJB
	private CrudService<CrisisContextSnapshot> crisisSnapshotCrudService;

	@EJB
	ESponderMappingService mappingService;


	// -------------------------------------------------------------------------

	@Override
	public CrisisContextDTO findCrisisContextDTOById(Long crisisContextID) {
		CrisisContext crisisContext = findCrisisContextById(crisisContextID);
		return (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
	}	

	@Override
	public CrisisContext findCrisisContextById(Long crisisContextID) {
		return (CrisisContext) crisisCrudService.find(CrisisContext.class, crisisContextID);
	}
	//-------------------------------------------------------------------------

	@Override
	public PlannableResourcePowerDTO findPlannableResourcePowerByIdRemote(Long powerID) {
		return (PlannableResourcePowerDTO) mappingService.mapESponderEntity(findPlannableResourcePowerById(powerID), PlannableResourcePowerDTO.class);
	}

	@Override
	public PlannableResourcePower findPlannableResourcePowerById(Long powerID) {
		return (PlannableResourcePower) powerCrudService.find(PlannableResourcePower.class, powerID);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisResourcePlanDTO findCrisisResourcePlanByIdRemote(Long crisisResourcePlanId, Long userId) {
		CrisisResourcePlan crisisResourcePlan = findCrisisResourcePlanById(crisisResourcePlanId, userId);
		return (CrisisResourcePlanDTO) mappingService.mapESponderEntity(crisisResourcePlan, CrisisResourcePlanDTO.class);
	}

	@Override
	public CrisisResourcePlan findCrisisResourcePlanById(Long crisisResourcePlanId, Long userId) {
		return crisisResourcePlanCrudService.find(CrisisResourcePlan.class, crisisResourcePlanId);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextSnapshotDTO findCrisisContextSnapshotDTOById(Long crisisContextSnapshotID) {
		CrisisContextSnapshot crisisContextSnapshot = findCrisisContextSnapshotById(crisisContextSnapshotID);
		return (CrisisContextSnapshotDTO) mappingService.mapESponderEntity(crisisContextSnapshot, CrisisContextSnapshotDTO.class);
	}

	@Override
	public CrisisContextSnapshot findCrisisContextSnapshotById(Long crisisContextSnapshotID) {
		return (CrisisContextSnapshot) crisisSnapshotCrudService.find(CrisisContextSnapshot.class, crisisContextSnapshotID);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisResourcePlanDTO findCrisisResourcePlanByTitleRemote(String crisisResourcePlanTitle, Long userId) {
		CrisisResourcePlan crisisResourcePlan = findCrisisResourcePlanByTitle(crisisResourcePlanTitle, userId);
		return (CrisisResourcePlanDTO) mappingService.mapESponderEntity(crisisResourcePlan, CrisisResourcePlanDTO.class);
	}

	@Override
	public CrisisResourcePlan findCrisisResourcePlanByTitle(String crisisResourcePlanTitle, Long userId) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("title", crisisResourcePlanTitle);
		return (CrisisResourcePlan) crisisResourcePlanCrudService.findSingleWithNamedQuery("CrisisResourcePlan.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	@Override
	public List<CrisisContextDTO> findAllCrisisContextsRemote() {
		List<CrisisContext> crisisContexts = findAllCrisisContexts();
		return (List<CrisisContextDTO>) mappingService.mapESponderEntity(crisisContexts, CrisisContextDTO.class);
	}	

	@Override
	public List<CrisisContext> findAllCrisisContexts() {
		return (List<CrisisContext>) crisisCrudService.findWithNamedQuery("CrisisContext.findAll");
	}

	// -------------------------------------------------------------------------

	@Override
	public List<CrisisResourcePlanDTO> findAllCrisisResourcePlansRemote() {
		List<CrisisResourcePlan> crisisResourcePlans = findAllCrisisResourcePlans();
		return (List<CrisisResourcePlanDTO>) mappingService.mapESponderEntity(crisisResourcePlans, CrisisResourcePlanDTO.class);
	}

	@Override
	public List<CrisisResourcePlan> findAllCrisisResourcePlans() {
		return (List<CrisisResourcePlan>) crisisResourcePlanCrudService.findWithNamedQuery("CrisisResourcePlan.findAll");
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextDTO findCrisisContextDTOByTitle(String title) {
		CrisisContext crisisContext = findCrisisContextByTitle(title);
		return (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
	}	

	@Override
	public CrisisContext findCrisisContextByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (CrisisContext) crisisCrudService.findSingleWithNamedQuery("CrisisContext.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextDTO createCrisisContextRemote(CrisisContextDTO crisisContextDTO, Long userID) {
		CrisisContext crisisContext = (CrisisContext) mappingService.mapESponderEntityDTO(crisisContextDTO, CrisisContext.class);
		crisisContext = createCrisisContext(crisisContext, userID);
		CrisisContextDTO crisisContextDTOPersisted = (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);


		return crisisContextDTOPersisted;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public CrisisContext createCrisisContext(CrisisContext crisisContext, Long userID) {
		return (CrisisContext) crisisCrudService.create(crisisContext);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextSnapshotDTO createCrisisContextSnapshotRemote(CrisisContextSnapshotDTO crisisContextSnapshotDTO, Long userID) {
		CrisisContextSnapshot crisisContextSnapshot = (CrisisContextSnapshot) mappingService.mapESponderEntityDTO(crisisContextSnapshotDTO, CrisisContextSnapshot.class);
		crisisContextSnapshot = createCrisisContextSnapshot(crisisContextSnapshot, userID);
		CrisisContextSnapshotDTO crisisContextSnapshotDTOPersisted = (CrisisContextSnapshotDTO) mappingService.mapESponderEntity(crisisContextSnapshot, CrisisContextSnapshotDTO.class);
		return crisisContextSnapshotDTOPersisted;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public CrisisContextSnapshot createCrisisContextSnapshot(CrisisContextSnapshot crisisContextSnapshot, Long userID) {
		return (CrisisContextSnapshot) crisisSnapshotCrudService.create(crisisContextSnapshot);
	}

	//-------------------------------------------------------------------------

	@Override
	public PlannableResourcePowerDTO createPlannableResourcePowerRemote(PlannableResourcePowerDTO plannableResourcePowerDTO, Long userID) {
		PlannableResourcePower plannableResourcePower = (PlannableResourcePower) mappingService.mapESponderEntityDTO(plannableResourcePowerDTO, PlannableResourcePower.class);
		return (PlannableResourcePowerDTO) mappingService.mapESponderEntity(createPlannableResourcePower(plannableResourcePower, userID), PlannableResourcePowerDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public PlannableResourcePower createPlannableResourcePower(PlannableResourcePower plannableResourcePower, Long userID) {
		return powerCrudService.create(plannableResourcePower);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisResourcePlanDTO createCrisisResourcePlanRemote(CrisisResourcePlanDTO crisisResourcePlanDTO, Long userID) {
		CrisisResourcePlan crisisResourcePlan = (CrisisResourcePlan) mappingService.mapESponderEntityDTO(crisisResourcePlanDTO, CrisisResourcePlan.class);
		crisisResourcePlan = createCrisisResourcePlan(crisisResourcePlan, userID);
		CrisisResourcePlanDTO crisisResourcePlanDTOPersisted = (CrisisResourcePlanDTO) mappingService.mapESponderEntity(crisisResourcePlan, CrisisResourcePlanDTO.class);
		return crisisResourcePlanDTOPersisted;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public CrisisResourcePlan createCrisisResourcePlan(CrisisResourcePlan crisisResourcePlan, Long userID) {
		return (CrisisResourcePlan) crisisResourcePlanCrudService.create(crisisResourcePlan);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deletePlannableResourcePowerRemote(Long powerID, Long userID) {
		this.deletePlannableResourcePower(powerID, userID);
	}

	@Override
	public void deletePlannableResourcePower(Long powerID, Long userID) {
		powerCrudService.delete(PlannableResourcePower.class, powerID);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteCrisisContextRemote(Long crisisContextId, Long userID) {
		deleteCrisisContext(crisisContextId, userID);	
	}

	@Override
	public void deleteCrisisContext(Long crisisContextId, Long userID) {
		crisisCrudService.delete(CrisisContext.class, crisisContextId);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteCrisisContextSnapshotRemote(Long crisisContextSnapshotId, Long userID) {
		deleteCrisisContextSnapshot(crisisContextSnapshotId, userID);	
	}

	@Override
	public void deleteCrisisContextSnapshot(Long crisisContextSnapshotId, Long userID) {
		crisisSnapshotCrudService.delete(CrisisContextSnapshot.class, crisisContextSnapshotId);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteCrisisResourcePlanRemote(Long crisisResourcePlanId, Long userID) {
		deleteCrisisResourcePlan(crisisResourcePlanId, userID);	
	}

	@Override
	public void deleteCrisisResourcePlan(Long crisisResourcePlanId, Long userID) {
		crisisResourcePlanCrudService.delete(CrisisResourcePlan.class, crisisResourcePlanId);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextDTO updateCrisisContextRemote( CrisisContextDTO crisisContextDTO, Long userID) {
		CrisisContext crisisContext = (CrisisContext) mappingService.mapESponderEntityDTO(crisisContextDTO, CrisisContext.class);
		crisisContext = updateCrisisContext(crisisContext, userID);
		crisisContextDTO = (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
		return crisisContextDTO;
	}

	@Override
	public CrisisContext updateCrisisContext(CrisisContext crisisContext, Long userID) {
		crisisContext = crisisCrudService.update(crisisContext);
		return crisisContext;
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextSnapshotDTO updateCrisisContextSnapshotRemote( CrisisContextSnapshotDTO crisisContextSnapshotDTO, Long userID) {
		CrisisContextSnapshot crisisContextSnapshot = (CrisisContextSnapshot) mappingService.mapESponderEntityDTO(crisisContextSnapshotDTO, CrisisContextSnapshot.class);
		crisisContextSnapshot = updateCrisisContextSnapshot(crisisContextSnapshot, userID);
		crisisContextSnapshotDTO = (CrisisContextSnapshotDTO) mappingService.mapESponderEntity(crisisContextSnapshot, CrisisContextSnapshotDTO.class);
		return crisisContextSnapshotDTO;
	}

	@Override
	public CrisisContextSnapshot updateCrisisContextSnapshot(CrisisContextSnapshot crisisContextSnapshot, Long userID) {
		crisisContextSnapshot = crisisSnapshotCrudService.update(crisisContextSnapshot);
		return crisisContextSnapshot;
	}

	//-------------------------------------------------------------------------

	@Override
	public PlannableResourcePowerDTO updatePlannableResourcePowerRemote(PlannableResourcePowerDTO plannableResourcePowerDTO, Long userID) {
		PlannableResourcePower plannableResourcePower = (PlannableResourcePower) mappingService.mapESponderEntityDTO(plannableResourcePowerDTO, PlannableResourcePower.class);
		return (PlannableResourcePowerDTO) mappingService.mapESponderEntity(updatePlannableResourcePower(plannableResourcePower, userID), PlannableResourcePowerDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public PlannableResourcePower updatePlannableResourcePower(PlannableResourcePower plannableResourcePower, Long userID) {
		PlannableResourcePower powerPersisted = findPlannableResourcePowerById(plannableResourcePower.getId());
		mappingService.mapEntityToEntity(plannableResourcePower, powerPersisted);
		return (PlannableResourcePower) powerCrudService.update(powerPersisted);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisResourcePlanDTO updateCrisisResourcePlanRemote( CrisisResourcePlanDTO crisisResourcePlanDTO, Long userID) {
		CrisisResourcePlan crisisResourcePlan = (CrisisResourcePlan) mappingService.mapESponderEntityDTO(crisisResourcePlanDTO, CrisisResourcePlan.class);
		crisisResourcePlan = updateCrisisResourcePlan(crisisResourcePlan, userID);
		crisisResourcePlanDTO = (CrisisResourcePlanDTO) mappingService.mapESponderEntity(crisisResourcePlan, CrisisResourcePlanDTO.class);
		return crisisResourcePlanDTO;
	}

	@Override
	public CrisisResourcePlan updateCrisisResourcePlan(CrisisResourcePlan crisisResourcePlan, Long userID) {
		crisisResourcePlan = crisisResourcePlanCrudService.update(crisisResourcePlan);
		return crisisResourcePlan;
	}





}
