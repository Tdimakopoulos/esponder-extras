package eu.esponder.controller.crisis.action;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;

@Remote
public interface ActionRemoteService {

	public ActionDTO findActionDTOById(Long actionID);

	public ActionDTO findActionDTOByTitle(String title);

	public ActionDTO createActionRemote(ActionDTO actionDTO, Long userID);

	public ActionDTO updateActionRemote(ActionDTO actionDTO, Long userID);

	public void deleteActionRemote(Long actionDTOID, Long userID);

	public ActionPartDTO findActionPartDTOById(Long actionPartID);

	public ActionPartDTO findActionPartDTOByTitle(String title);

	public ActionPartDTO createActionPartRemote(ActionPartDTO actionPartDTO, Long userID);

	public ActionPartDTO updateActionPartRemote(ActionPartDTO actionPartDTO, Long userID);

	public void deleteActionPartRemote(Long actionPartDTOID, Long userID);

	public ActionObjectiveDTO findActionObjectiveDTOByTitle(String title);

	public ActionObjectiveDTO findActionObjectiveDTOById(Long actionID);

	public ActionObjectiveDTO createActionObjectiveRemote(ActionObjectiveDTO actionObjectiveDTO, Long userID);

	public ActionObjectiveDTO updateActionObjectiveRemote(ActionObjectiveDTO actionObjectiveDTO, Long userID);

	public void deleteActionObjectiveRemote(Long actionObjectiveDTOId, Long userID);

	public ActionPartObjectiveDTO findActionPartObjectiveDTOById(Long actionID);

	public ActionPartObjectiveDTO findActionPartObjectiveDTOByTitle(String title);

	public ActionPartObjectiveDTO createActionPartObjectiveRemote(ActionPartObjectiveDTO actionPartObjectiveDTO, Long userID);

	public ActionPartObjectiveDTO updateActionPartObjectiveRemote(ActionPartObjectiveDTO actionPartObjectiveDTO, Long userID);

	public void deleteActionPartObjectiveRemote(Long actionPartObjectiveDTOId, Long userID);

	public ActionSnapshotDTO findActionSnapshotDTOById(Long actionSnapshotID);

	public ActionPartSnapshotDTO findActionPartSnapshotDTOById(Long actionPartSnapshotID);

	public ActionSnapshotDTO createActionSnapshotRemote(ActionSnapshotDTO actionSnapshotDTO, Long userID);

	public ActionPartSnapshotDTO createActionPartSnapshotRemote(ActionPartSnapshotDTO actionPartSnapshotDTO, Long userID);

	public ActionSnapshotDTO updateActionSnapshotRemote(ActionSnapshotDTO actionSnapshotDTO, Long userID);

	public ActionPartSnapshotDTO updateActionPartSnapshotRemote(ActionPartSnapshotDTO actionPartSnapshotDTO, Long userID);

	public void deleteActionSnapshotRemote(Long actionSnapshotDTOID, Long userID);

	public void deleteActionPartSnapshotRemote(Long actionPartSnapshotDTOID, Long userID);
}
