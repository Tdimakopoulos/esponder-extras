package eu.esponder.controller.crisis.user;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.user.ESponderUserDTO;

@Remote
public interface UserRemoteService {
	
	public ESponderUserDTO findUserByIdRemote(Long userID);

	public ESponderUserDTO findUserByNameRemote(String userName);

	public ESponderUserDTO createUserRemote(ESponderUserDTO user, Long userID);

	public ESponderUserDTO updateUserRemote(ESponderUserDTO user, Long userID);

	public List<ESponderUserDTO> findAllUsersRemote();

	public Long deleteUserRemote(Long deletedUserId, Long userID);

}
