package eu.esponder.controller.configuration;

import javax.ejb.Remote;

import eu.esponder.dto.model.config.ESponderConfigParameterDTO;

@Remote
public interface ESponderConfigurationRemoteService {
	
	public ESponderConfigParameterDTO findESponderConfigByNameRemote(String configDTOTitle) throws ClassNotFoundException;

	public ESponderConfigParameterDTO findESponderConfigByIdRemote(Long configDTOId) throws ClassNotFoundException;

	public ESponderConfigParameterDTO createESponderConfigRemote(ESponderConfigParameterDTO configDTO, Long userID) throws ClassNotFoundException;
	
	public ESponderConfigParameterDTO updateESponderConfigRemote(ESponderConfigParameterDTO configDTO, Long userID);
	
	public void deleteESponderConfigRemote(Long ESponderConfigDTOID, Long userID);

}
