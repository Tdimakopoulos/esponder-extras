package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.snapshot.resource.ActorSnapshot;

@Local
public interface ActorService extends ActorRemoteService {
	
	public Actor findById(Long actorID);
	
	public Actor findByTitle(String title);
	
	public List<Actor> findSubordinatesById(Long actorID);
	
	public Actor createActor(Actor actor, Long userID);
	
	public Actor updateActor(Actor actor, Long userID);
	
	public void deleteActor(Long actorID, Long userID);
	
	public ActorSnapshot findActorSnapshotByDate(Long actorID, Date dateTo);
	
	public ActorSnapshot findActorSnapshotById(Long actorID);
	
	public ActorSnapshot createActorSnapshot(ActorSnapshot snapshot, Long userID);
	
	public ActorSnapshot updateActorSnapshot(ActorSnapshot actorSnapshot, Long userID);
	
	public void deleteActorSnapshot(Long actorSnapshotID, Long userID);

	public List<Actor> findAllActors();
	
}
