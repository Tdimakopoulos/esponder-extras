package eu.esponder.controller.mapping.dozer;

import org.dozer.CustomConverter;

import eu.esponder.model.type.CrisisDisasterType;
import eu.esponder.model.type.CrisisFeatureType;

public class CrisisTypeConverter implements CustomConverter {

	@Override
	public Object convert(Object destination,
			Object source, Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == CrisisDisasterType.class) {
			destination = source;
			return destination;
		}
		if (sourceClass == CrisisFeatureType.class) {
			destination = source;
			return destination;
		}
		
		return null;
	}

}
