package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.CrisisService;
import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.util.ejb.ServiceLocator;

public class CrisisContextIdConverter implements CustomConverter {
	
	protected CrisisService getCrisisService() {
		try {
			return ServiceLocator.getResource("esponder/CrisisBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		
		if (sourceClass == CrisisContext.class && source != null) {	
			CrisisContext sourceCrisisContext = (CrisisContext) source;
			Long crisisContextID = sourceCrisisContext.getId();
			destination = crisisContextID;
		}
		else if(sourceClass == Long.class && source!=null) {
			Long crisisContextID = (Long) source;
			CrisisContext destCrisisContext = (CrisisContext) this.getCrisisService().findCrisisContextById(crisisContextID);
			destination = destCrisisContext;
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
