package eu.esponder.controller.generic;

import java.util.List;
import java.util.Set;

import javax.ejb.Local;

import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.model.ESponderEntity;

@Local
public interface GenericService extends GenericRemoteService {

	public List<? extends ESponderEntity<Long>> getEntities(
			Class<? extends ESponderEntity<Long>> clz, EsponderQueryRestriction ecriterion, int pageSize, int pageNumber) 
			throws InstantiationException, IllegalAccessException;

	public ESponderEntity<Long> createEntity(ESponderEntity<Long> entity, Long userID) throws ClassNotFoundException;
	
	public ESponderEntity<Long> updateEntity(ESponderEntity<Long> entity, Long userID) throws ClassNotFoundException;
	
	public ESponderEntity<Long> getEntity( Class<? extends ESponderEntity<Long>> clz, Long objectID);
	
	public void deleteEntity(Class<? extends ESponderEntity<Long>> targetClass, Long entityID) throws ClassNotFoundException;
	
	public EsponderQueryRestriction createCriteriaforBulkFindById(Set<Long> competenceId);
	
}
