package eu.esponder.controller.mapping.dozer;

import org.dozer.CustomConverter;

import eu.esponder.model.type.InitialActorType;
import eu.esponder.model.type.OperationalActorType;
import eu.esponder.model.type.StrategicActorType;
import eu.esponder.model.type.TacticalActorType;

public class ActorTypeCustomConverter implements CustomConverter {

	@Override
	public Object convert(Object destination,
			Object source, Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == OperationalActorType.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == InitialActorType.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == StrategicActorType.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == TacticalActorType.class) {
			destination = source;
			return destination;
		}
		
		return null;
	}

}
