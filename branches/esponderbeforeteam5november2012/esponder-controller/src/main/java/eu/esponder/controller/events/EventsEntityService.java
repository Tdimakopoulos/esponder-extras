package eu.esponder.controller.events;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.events.entity.OsgiEventsEntity;

@Local
public interface EventsEntityService {

	public OsgiEventsEntity findOsgiEventsEntityById(Long osgiEventsEntityID);

	public List<OsgiEventsEntity> findAllOsgiEventsEntities();

	public List<OsgiEventsEntity> findOsgiEventsEntitiesBySeverity(String severity);

	public List<OsgiEventsEntity> findOsgiEventsEntitiesBySourceId(Long sourceId);

	public OsgiEventsEntity createOsgiEventsEntity(OsgiEventsEntity osgiEventsEntity,
			Long userID);

	public void deleteOsgiEventsEntity(Long osgiEventsEntityID, Long userID);

	public OsgiEventsEntity updateOsgiEventsEntity(OsgiEventsEntity osgiEventsEntity,
			Long userID);

}
