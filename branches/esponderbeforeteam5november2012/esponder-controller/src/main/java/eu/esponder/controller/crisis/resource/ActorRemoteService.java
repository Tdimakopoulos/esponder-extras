package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;

@Remote
public interface ActorRemoteService {

	public ActorDTO findByIdRemote(Long actorID);

	public ActorDTO findByTitleRemote(String title);
	
	public List<ActorDTO> findSubordinatesByIdRemote(Long actorID);

	public ActorDTO createActorRemote(ActorDTO actorDTO, Long userID);

	public ActorDTO updateActorRemote(ActorDTO actorDTO, Long userID);
	
	public void deleteActorRemote(Long actorID, Long userID);
	
	public ActorSnapshotDTO findActorSnapshotByDateRemote(Long actorID, Date maxDate);
	
	public ActorSnapshotDTO findActorSnapshotByIdRemote(Long actorID);

	public ActorSnapshotDTO createActorSnapshotRemote(ActorSnapshotDTO snapshotDTO, Long userID);
	
	public ActorSnapshotDTO updateActorSnapshotRemote(ActorSnapshotDTO actorSnapshotDTO, Long userID);
	
	public void deleteActorSnapshotRemote(Long actorSnapshotDTOID, Long userID);

	public List<ActorDTO> findAllActorsRemote();
	
}
