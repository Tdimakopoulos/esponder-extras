package eu.esponder.osgi.settings;

import java.io.File;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
import eu.esponder.test.ResourceLocator;

public class OsgiSettings {

	// uncommen for unix, comment for windows
	private String szPropertiesFileNameUnix = "//home//exodus//osgi//osgi.config.properties";

	// comment for unix, uncomment for windows
	private String szPropertiesFileNameWindows = "C://Development//osgi.config.properties";

	private String szPropertiesFileName = "";

	private boolean isUnix() {
		if (File.separatorChar == '/')
			return true;
		else
			return false;
	}

	public OsgiSettings() {
		
			GetAllSettings();

		
	}

	public void LoadSettings() {
		
			GetAllSettings();
		
	}

	private void GetAllSettings()   {

		try {
			ESponderConfigurationRemoteService cfgService = ResourceLocator
					.lookup("esponder/ESponderConfigurationBean/remote");

			// Get DroolsTempRep
			ESponderConfigParameterDTO dTempRep = cfgService
					.findESponderConfigByNameRemote("OSGIPropertiesFiles");
			try {
				szPropertiesFileName = dTempRep.getParameterValue();
			} catch (Exception e) {
				if (isUnix()) {
					szPropertiesFileName = szPropertiesFileNameUnix;
				} else {
					szPropertiesFileName = szPropertiesFileNameWindows;
				}
			}
			
			
			//javax.naming.NoInitialContextException
			
		} catch(NoClassDefFoundError e) {
			if (isUnix()) {
				szPropertiesFileName = szPropertiesFileNameUnix;
			} else {
				szPropertiesFileName = szPropertiesFileNameWindows;
			}
		}
		catch(RuntimeException e2) {
			if (isUnix()) {
				szPropertiesFileName = szPropertiesFileNameUnix;
			} else {
				szPropertiesFileName = szPropertiesFileNameWindows;
			}
		}
		catch(ExceptionInInitializerError e3) {
			if (isUnix()) {
				szPropertiesFileName = szPropertiesFileNameUnix;
			} else {
				szPropertiesFileName = szPropertiesFileNameWindows;
			}
		} catch (ClassNotFoundException e1) {
			if (isUnix()) {
				szPropertiesFileName = szPropertiesFileNameUnix;
			} else {
				szPropertiesFileName = szPropertiesFileNameWindows;
			}
		}
		

	}

	public String getSzPropertiesFileName() {
		if (szPropertiesFileName == null || szPropertiesFileName.length() < 2) {
			if (isUnix()) {
				szPropertiesFileName = szPropertiesFileNameUnix;
			} else {
				szPropertiesFileName = szPropertiesFileNameWindows;
			}
		}
		return szPropertiesFileName;
	}

	public void setSzPropertiesFileName(String szPropertiesFileName) {
		this.szPropertiesFileName = szPropertiesFileName;
	}
}
