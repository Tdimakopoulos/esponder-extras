package com.esponder.apps;
 
import java.util.LinkedList;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import android.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateLocationMeasurementStatisticEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.fru.JSONParser;

public class Journal extends Activity {

	private ListView messageListView;
	private Button getMore; 
	private JournalListAdapter adapterList;
	private LinkedList<JournalMessage> messageList,tempList;  
	private String filter1 = CreateSensorMeasurementStatisticEvent.class.getName();
	private String filter2 = CreateLocationMeasurementStatisticEvent.class.getName();
    int count = 5;
    long actorId;
	private IncomingReceiver receiver;
	SharedPreferences jernalSettings;
	private String action;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.journal);
		messageListView = (ListView) findViewById(R.id.MessageListView);
	    getMore = (Button)this.findViewById(R.id.more);
		messageList = new LinkedList<JournalMessage>();
		tempList = new LinkedList<JournalMessage>();
		adapterList = new JournalListAdapter(this);  
		messageListView.setAdapter(adapterList);
		JSONParser jpa = new JSONParser();
		ActorDTO actor = jpa.getActor();
		
		if(actor.getSubordinates()!=null){
			actorId = actor.getId();
		 }else{
		    actorId = actor.getSupervisor().getId();
		 }
			
		jernalSettings = this.getSharedPreferences("jernalSettings",MODE_WORLD_READABLE);
		receiver = new IncomingReceiver(); 
		
		IntentFilter intentFilter = new IntentFilter();
		if(jernalSettings.getBoolean("sensorMeasurementsEvents", false)){
			intentFilter.addAction(filter1 +"." + actorId);
		}
		if(jernalSettings.getBoolean("locationMeasurementsEvents", false)){
			intentFilter.addAction(filter2 +"." + actorId);
		}
		intentFilter.addCategory("org.osgi.service.event.EventAdmin");
		registerReceiver(receiver, intentFilter); 
		
		// Log.d("prwto","einai " + intentFilter.getAction(0));
	    // Log.d("deutrero","einai " + intentFilter.getAction(1));
		
		EventsToMessages eventsToMessages = new EventsToMessages();
		messageList = eventsToMessages.getMessages(this);
		
		if(messageList.size() > 5){
		     for(int i=0;i<count;i++){
			    tempList.add(messageList.get(i));
		       }  
		     ShowOrder((LinkedList<JournalMessage>) tempList);
		  }else{
			 ShowOrder((LinkedList<JournalMessage>) messageList);
			 getMore.setVisibility(View.GONE);
		  }
		
		 
	/*	Thread s = new Sender(this);
		s.start();*/
		
		getMore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            
            	AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
				  dialog.setTitle("Title");
			      dialog.setMessage("No other Events");
			      dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			          public void onClick(DialogInterface dialog, int id) {
			          }
			      });
			    boolean isEnd = false;   
            	count = count+5;
            	ClearOrder();
        		for(int i=0;i<count;i++){
        			if(i<messageList.size()){
        				tempList.add(messageList.get(i));
        			}else{
        				isEnd = true;
        				break;
        			}
        			 
        		}
        		
        		if(isEnd){
        		dialog.show();
        		}
        		else{
        			ShowOrder((LinkedList<JournalMessage>) tempList);
        			messageListView.smoothScrollToPosition(tempList.size()-1);
        		}
            }

        });

	}

	public void ShowOrder(final LinkedList<JournalMessage> journalMessage) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.addOrder(journalMessage);
				adapterList.notifyDataSetChanged();
			}
		});
	}

	public void ClearOrder() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.clear();
				adapterList.notifyDataSetChanged();
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menujernal, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
			Intent jernalSettings = new Intent(Journal.this, JournalSettings.class);
			startActivity(jernalSettings);
			finish();
			break;
		}
		return true;
	}
	
	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) { 
			
			// Get the event
			  String incomingEvent = intent.getStringExtra("event");
			  ObjectMapper	mapper = new ObjectMapper();
			  mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
			  JsonFactory jsonFactory = new JsonFactory(); 
			  JournalMessage message = new JournalMessage();
			//get action
		  	action = intent.getAction();
		  	if (action.contains(CreateSensorMeasurementStatisticEvent.class.getName())) {
		  	  CreateSensorMeasurementStatisticEvent event = null;
			  // deserialize incoming Event
			 try {
			      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
			      // get the event
			       event = mapper.readValue(jp, CreateSensorMeasurementStatisticEvent.class);
			  } catch (Exception e) {
			      e.printStackTrace();
			    }
			 
		    	message = new JournalMessage();
				message.setSeverity(event.getEventSeverity());
				message.setMessage("TYPE: " + ((SensorMeasurementStatisticEnvelopeDTO) event.getEventAttachment()).getMeasurementStatistics().get(0).getStatistic().getSensor().getName()+" \n"+event.getJournalMessage());
				message.setEvent(event);
		  		
		  	} else if(action.contains(CreateLocationMeasurementStatisticEvent.class.getName())){
		  		 CreateLocationMeasurementStatisticEvent event = null;
				  // deserialize incoming Event
				 try {
				      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
				      // get the event
				      event = mapper.readValue(jp, CreateLocationMeasurementStatisticEvent.class);
				  } catch (Exception e) {
				      e.printStackTrace();
				    }
					message = new JournalMessage();
					message.setSeverity(event.getEventSeverity());
					message.setMessage("TYPE: " + ((SensorMeasurementStatisticEnvelopeDTO) event.getEventAttachment()).getMeasurementStatistics().get(0).getStatistic().getSensor().getName()+" \n"+event.getJournalMessage());
					message.setEvent(event);
		  	}
			tempList.addFirst(message);
			adapterList.notifyDataSetChanged();
        /*    tempList.clear();
			messageList.clear();
		 
			
			EventsToMessages eventsToMessages = new EventsToMessages();
			messageList = eventsToMessages.getMessages(context);
			count = 5;
			if(messageList.size() > 5){
			     for(int i=0;i<count;i++){
				    tempList.add(messageList.get(i));
			       }  
			     ShowOrder((LinkedList<JournalMessage>) tempList);
			  }else{
				 ShowOrder((LinkedList<JournalMessage>) messageList);
				 getMore.setVisibility(View.GONE);
			  }*/
		

		}

	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

}