package com.esponder.apps;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.widget.Toast;
import eu.esponder.event.model.config.UpdateConfigurationEvent;
import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.event.model.crisis.UpdateCrisisContextEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionPartSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateLocationMeasurementStatisticEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;

public class ReceivingControl extends Service {
  

	private String filter1 = CreateSensorMeasurementStatisticEvent.class.getName();
	private String filter2 = CreateLocationMeasurementStatisticEvent.class.getName();
	private String filter3 = CreateActionEvent.class.getName();
	private String filter4 = UpdateActionPartSnapshotEvent.class.getName(); 
    private String filter5 = UpdateActionSnapshotEvent.class.getName();
    private String filter6 = UpdateConfigurationEvent.class.getName();
    private String filter7 = CreateCrisisContextEvent.class.getName();
    private String filter8 = UpdateCrisisContextEvent.class.getName();
    private Long myID;
     
	private Receiver receiver; 
 

	@Override
	public IBinder onBind(Intent intent) {
		// Not implemented...this sample is only for starting and stopping
		// services.
		// Service binding will be covered in another tutorial
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	public int onStartCommand(Intent intent, int flags, int startId) {

		// Announcement about starting
		Toast.makeText(this, "Starting the Receiver Service",
				Toast.LENGTH_SHORT).show();

		// Context con = this; 
		myID = Menu.actor.getId();
		
		receiver = new Receiver(myID);
		IntentFilter intentFilter = new IntentFilter();
		
		// if chief register to all events
		if(Menu.actor.getSubordinates() != null){
			intentFilter.addAction(filter1 +"." + myID);
			intentFilter.addAction(filter2 +"." + myID);
			intentFilter.addAction(filter3 +"." + myID);
			intentFilter.addAction(filter4 +"." + myID);
			intentFilter.addAction(filter5 +"." + myID);  
			intentFilter.addAction(filter6 +"." + myID);
			intentFilter.addAction(filter7 +"." + myID); 
			intentFilter.addAction(filter8 +"." + myID); 
		}else{ // if not chief register only to CreateActionEvent,UpdateActionPartSnapshotEvent,UpdateActionSnapshotEvent
			 intentFilter.addAction(filter3 +"." +  Menu.actor.getSupervisor().getId());
			 intentFilter.addAction(filter4 +"." +  Menu.actor.getSupervisor().getId());
			 intentFilter.addAction(filter5 +"." +  Menu.actor.getSupervisor().getId());
		     intentFilter.addAction(filter6 +"." +  Menu.actor.getSupervisor().getId());
			 intentFilter.addAction(filter7 +"." +  Menu.actor.getSupervisor().getId());
			 intentFilter.addAction(filter8 +"." +  Menu.actor.getSupervisor().getId());
		}
		
		intentFilter.addCategory("org.osgi.service.event.EventAdmin");
		registerReceiver(receiver, intentFilter);
		
		// Start our Sender
		Thread s = new Sender(this);
		s.start();
 
		return 0;
	}

	@Override
	public void onDestroy() {
		super.onDestroy(); 
		Toast.makeText(this, "Stopping the Receiver Service",
				Toast.LENGTH_SHORT).show();
		
		unregisterReceiver(receiver); 
	}
}
