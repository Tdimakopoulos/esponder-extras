package com.esponder.apps;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import android.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;
import eu.esponder.dto.model.snapshot.status.ActionSnapshotStatusDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionSnapshotEvent;
import eu.esponder.jsonrcp.JSONRPCException;

public class OperationsByActions extends Activity {

	private ListView messageListView;
	private OperationsListAdapter adapterList;
	private LinkedList<ActionPartDTO> messageList;
	private String filter = CreateActionEvent.class.getName();
    int count = 5;
	private IncomingReceiver receiver;
	private String actionName; 
    private TextView actionTitle;
    Iterator actionPartIterator;
    private Button updateAll;
    private CharSequence[] choices = {"STARTED", "PAUSED", "COMPLETED","CANCELED"};
    private AlertDialog.Builder menu,builder; 
	ActionDTO action;
	ObjectMapper eventMapper;
	String eventToJson;
	Intent intent;
	ESponderEvent<ActionSnapshotDTO>  updateActionSnapshotEvent;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operationsbyaction);
		messageListView = (ListView) findViewById(R.id.MessageListView);
		actionTitle      = (TextView) findViewById(R.id.Title);	 
		messageList = new LinkedList<ActionPartDTO>();
		adapterList = new OperationsListAdapter(this,2);  
		messageListView.setAdapter(adapterList);
		updateAll = (Button) findViewById(R.id.update);	 
		
		Intent incomingIntent = getIntent();
	       if (incomingIntent.getStringExtra("actionInfo") != null)
	        {  
	    	    ObjectMapper mapper = new ObjectMapper();
				mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				JsonFactory jsonFactory = new JsonFactory();
				try { 
					JsonParser jp = jsonFactory.createJsonParser(incomingIntent.getStringExtra("actionInfo"));
					// get the ActionDTO
					 action = mapper.readValue(jp,ActionDTO.class);
					
				} catch (Exception e) {
					e.printStackTrace();
				} 
	        	
				    actionName = action.getTitle() ;   
	        		actionTitle.setText(actionName);
	        		actionTitle.setTextColor(Color.parseColor("#FFFF01"));
	        		actionTitle.setBackgroundColor(Color.parseColor("#580000"));
	        	 
	        }
		
		
		  builder = new AlertDialog.Builder(this);
	      builder.setMessage("Your Request has been send")
		       .setCancelable(false)
		       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		           }
		       }); 
		
	
	     updateAll.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {  
				 menu = new AlertDialog.Builder(view.getContext());
			     menu.setTitle("Select Status");
			     menu.setItems(choices, new DialogInterface.OnClickListener() {
			         public void onClick(DialogInterface dialog, int item) {
			        	 
			        	 // Send Update ActionSnapshot Event
			        	ActionSnapshotDTO actionSnapshot = new ActionSnapshotDTO();
			            actionSnapshot.setStatus(ActionSnapshotStatusDTO.getActionSnapshotStatusEnum(item)); 
			            actionSnapshot.setAction(action);
			        	 
			        	updateActionSnapshotEvent = new UpdateActionSnapshotEvent();
			        	updateActionSnapshotEvent.setEventSeverity(SeverityLevelDTO.MEDIUM);
			        	updateActionSnapshotEvent.setEventTimestamp(new Date());
			        	updateActionSnapshotEvent.setEventAttachment(actionSnapshot);
			            updateActionSnapshotEvent.setEventSource(Menu.actor);
			            
			             eventMapper = new ObjectMapper(); 
	 		        	 eventMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
			        	 eventMapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
			    	 	 eventToJson = null ;
			    	 	 eventMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			    	          try {
			    	        	  eventToJson = eventMapper.writeValueAsString(updateActionSnapshotEvent);
			    	  		} catch (JsonGenerationException e) {
			    	  			// TODO Auto-generated catch block
			    	  			e.printStackTrace();
			    	  		} catch (JsonMappingException e) {
			    	  			// TODO Auto-generated catch block
			    	  			e.printStackTrace();
			    	  		} catch (IOException e) {
			    	  			// TODO Auto-generated catch block
			    	  			e.printStackTrace();
			    	  		}
			    	          
			     	 	  intent = new Intent(UpdateActionSnapshotEvent.class.getName() + "." + Menu.actor.getId());
			    	 	  intent.addCategory("org.osgi.service.event.EventAdmin");
			    	 	  intent.putExtra("event", eventToJson); 
			    	 	  sendBroadcast(intent); 
			    	 	  Log.d("steilameeeeeeee"," steilameeeeeeeee = "+ intent.getAction());
			    	 	  
			    	 	  HashSet<ActionPartDTO>  actionPartsSet =  (HashSet<ActionPartDTO>) action.getActionParts();
			    	      actionPartIterator = actionPartsSet.iterator(); 
			    	      while (actionPartIterator.hasNext()){
			    	    	  ActionPartDTO actionPartTemp =  ((ActionPartDTO) actionPartIterator.next());
			    	    	  actionPartTemp.getSnapshot().setStatus(ActionPartSnapshotStatusDTO.getActionPartSnapshotStatusEnum(item));
			    	      	  } 
			    	      
			    	      try {
				            	Log.d(" before "," before ");
								Menu.client.call("esponderdb/updateESponderDTO", null, action);
								Log.d(" after "," after ");
							} catch (JSONRPCException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			     // TO do with static views
			    
			    	 	  builder.show();
			         }
			     }); 
				
				menu.show();
			}

		});	 
	       	
	       
	     receiver = new IncomingReceiver();
			IntentFilter intentFilter = new IntentFilter(); 
			 
			// if chief register to CreateLocationMeasurementStatisticEvent . yours id
			if(Menu.actor.getSubordinates()!=null){
				intentFilter.addAction(filter +"."+Menu.actor.getId());
			}else{ // register to CreateLocationMeasurementStatisticEvent . chief's id
				 intentFilter.addAction(filter +"."+Menu.actor.getSupervisor().getId());
			}
			
		    intentFilter.addCategory("org.osgi.service.event.EventAdmin");
			registerReceiver(receiver, intentFilter);
		 	  
	      HashSet<ActionPartDTO>  actionPartsSet =  (HashSet<ActionPartDTO>) action.getActionParts();
	      actionPartIterator = actionPartsSet.iterator(); 
	      while (actionPartIterator.hasNext()){
	    	  ActionPartDTO actionPartTemp =  ((ActionPartDTO) actionPartIterator.next());
	    	  messageList.add(actionPartTemp);
	      	  } 
	 	 
	     	// <----------------- End Action No1 ------------------> 
 
	      ShowOrder((LinkedList<ActionPartDTO>) messageList);
		
    }

	public void ShowOrder(final LinkedList<ActionPartDTO> operationMessage) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.addOrder(operationMessage);
				adapterList.notifyDataSetChanged();
			}
		});
	}

	public void ClearOrder() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.clear();
				adapterList.notifyDataSetChanged();
			}
		});
	}
	
/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menujernal, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
			Intent jernalSettings = new Intent(Jernal.this, JernalSettings.class);
			startActivity(jernalSettings);
			finish();
			break;
		}
		return true;
	}*/
	
	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			   AlertDialog.Builder dialog = new AlertDialog.Builder(context);
			    dialog.setTitle(" New Action Arrived ");
		       //dialog.setMessage("No other Events");
		        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int id) {
		            }
		          });
		         dialog.show(); 
		}

	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

}
