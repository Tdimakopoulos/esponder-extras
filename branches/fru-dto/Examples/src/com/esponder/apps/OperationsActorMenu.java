package com.esponder.apps;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import eu.esponder.dto.model.crisis.resource.ActorDTO;

public class OperationsActorMenu extends Activity {

 
	private ListView actorListView;
	private List<ActorDTO> actors;
	private OperationMenuListAdapter adapterList;
	private Iterator iteratorSubordinates;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operationsmenu);
		actorListView = (ListView) findViewById(R.id.MessageListView);
		actors = new ArrayList<ActorDTO>();
		adapterList = new OperationMenuListAdapter(this); 
		actorListView.setAdapter(adapterList);
		
		actors.add(Menu.actor);
	 	iteratorSubordinates =  Menu.actor.getSubordinates().iterator(); 	
        while(iteratorSubordinates.hasNext()){
			  actors.add((ActorDTO)iteratorSubordinates.next());
		  }
		  
		  ShowOrder((List<ActorDTO>) actors);
	}

	public void ShowOrder(final List<ActorDTO> actor) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.addOrder(actor);
				adapterList.notifyDataSetChanged();
			}
		});
	}

	public void ClearOrder() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.clear();
				adapterList.notifyDataSetChanged();
			}
		});
	}
	
}