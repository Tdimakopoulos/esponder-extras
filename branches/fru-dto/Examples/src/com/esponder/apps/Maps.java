package com.esponder.apps;
 
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import android.R;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.Projection;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.event.model.crisis.UpdateCrisisContextEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateLocationMeasurementStatisticEvent;
import eu.esponder.fru.JSONParser;
import eu.esponder.jsonrcp.JSONRPCClient;
import eu.esponder.jsonrcp.JSONRPCException;

public class Maps extends MapActivity {

	private MapView mapView;

	private static int currentLon = 0;
	private static int currentLat = 0;  
	Long myId;

	MapController mapController;
	GeoPoint current;
	List<Overlay> mapOverlays;
	CustomItemizedOverlay myOverlay,othersOverlay;
	Drawable myDrawable,othersDrawable;
	Iterator iteratorSubordinates;
	private String filter = CreateLocationMeasurementStatisticEvent.class .getName();
	private String filter2 = UpdateCrisisContextEvent.class .getName();
	private IncomingReceiver receiver;
	private GeoPoint updatedPoint;
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);

		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);

		mapOverlays = mapView.getOverlays();

		receiver = new IncomingReceiver();

		othersDrawable = this.getResources().getDrawable(R.drawable.marker2);
		myDrawable = this.getResources().getDrawable(R.drawable.marker);

		JSONParser jpa = new JSONParser();
		ActorDTO actor = jpa.getActor();
		myId = actor.getId();
		Log.d("to id mou einai!!","einai = " + myId);

		myOverlay = new CustomItemizedOverlay(myDrawable, this);
		othersOverlay = new CustomItemizedOverlay(othersDrawable, this);

		IntentFilter intentFilter = new IntentFilter();
		 
		// if chief register to CreateLocationMeasurementStatisticEvent . yours id
		if(actor.getSubordinates()!=null){  
			intentFilter.addAction(filter +"."+actor.getId());
		    intentFilter.addAction(filter2 +"."+actor.getId());
		}else{ // register to CreateLocationMeasurementStatisticEvent . chief's id
			 intentFilter.addAction(filter +"."+actor.getSupervisor().getId());
			 intentFilter.addAction(filter2 +"."+actor.getSupervisor().getId());
		}
		
	  intentFilter.addCategory("org.osgi.service.event.EventAdmin");
		registerReceiver(receiver, intentFilter);
		
		/*
		 * GeoPoint current = new GeoPoint(latitudeChief, longitudeChief);
		 * OverlayItem overlayitem2_1 = new OverlayItem(current,
		 * "I am here!","Me"); chiefOverlay.addOverlay(overlayitem2_1);
		 */

      /*  GeoPoint point1 = new GeoPoint(37943578, 23732063); 
        OverlayItem overlayitem = new OverlayItem(point1, "id = "+myId, "Name = FRC #5555");
        myOverlay.addOverlay(overlayitem);*/
        
		/*
		 * GeoPoint point1 = new GeoPoint(latitudeR1, longitudeR1); GeoPoint
		 * point2 = new GeoPoint(latitudeR2, longitudeR2); GeoPoint point3 = new
		 * GeoPoint(latitudeR3, longitudeR3);
		 * 
		 * OverlayItem overlayitem = new OverlayItem(point1, "Fr1", "Fr1");
		 * OverlayItem overlayitem2 = new OverlayItem(point2, "Fr2", "Fr2");
		 * OverlayItem overlayitem3 = new OverlayItem(point3, "Fr3", "Fr3");
		 * 
		 * frsOverlay.addOverlay(overlayitem);
		 * frsOverlay.addOverlay(overlayitem2);
		 * frsOverlay.addOverlay(overlayitem3);
		 */

        if (myOverlay.size()!=0){
			mapOverlays.add(myOverlay);
		}
        
        if (othersOverlay.size()!=0){
			mapOverlays.add(othersOverlay);
		}

	 
		mapController = mapView.getController();

		mapController.setZoom(4);
		// Crisis Context Area
		JSONRPCClient  client = JSONRPCClient.create("http://localhost:8080/remote/json-rpc");
	    try {
		    	ESponderEvent[] crisisContextEvent = (ESponderEvent[]) client.call("esponderdb/getESponderEvents", 
		              ESponderEvent[].class, CreateCrisisContextEvent.class.getName()); 
		    	if( crisisContextEvent.length > 0){
		    		Log.d(" To size  einai  "," To size einai = " + crisisContextEvent.length); 
		    		CrisisContextDTO crisisContext =  (CrisisContextDTO) crisisContextEvent[0].getEventAttachment();
		    		Log.d(" To crisis einai  "," To crisis einai = " + crisisContext.getTitle() + " location "+ crisisContext.getCrisisLocation());

		    	/*    Log.d(" some info "," some info = "+crisisContext.getCrisisLocation().getCentre());
		    		BigDecimal lat = crisisContext.getCrisisLocation().getCentre().getLatitude();
		    		BigDecimal lon = crisisContext.getCrisisLocation().getCentre().getLongitude();
		    		 GeoPoint point1 = new GeoPoint(lat.intValue() , lon.intValue()); 
		    	 	 mapView.getOverlays().add(new CircleOverlay(this, point1.getLatitudeE6(),point1.getLongitudeE6()));*/
		    	} 
     	  } catch (JSONRPCException e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
	   }
		 
  
	 
	
		/*Thread s = new Sender(this);
		s.start();*/

	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.routing:
			Intent routingSelection = new Intent(Maps.this,
					RoutingSelection.class);
			Bundle bundle = new Bundle();
			bundle.putInt("Latitude", currentLon);
			bundle.putInt("Longitude", currentLat);
			routingSelection.putExtra("currentpos", bundle);
			startActivity(routingSelection);
			break;
		case R.id.settings:
			Toast.makeText(this, "You pressed the setting!", Toast.LENGTH_LONG)
					.show();
			break;
		case R.id.home:
			if(updatedPoint!=null){
				mapController.animateTo(updatedPoint);
				mapController.setZoom(15);
			}
			break;
		}
		return true;
	}

	@Override
	public void onRestart() {
		super.onRestart();
		if(current!=null){
		mapController.animateTo(current);
		}
	}

	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			
			//get action
		  String action = intent.getAction();
		  
		    ObjectMapper mapper = new ObjectMapper();
			mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
			JsonFactory jsonFactory = new JsonFactory();
			
		  if (action.contains(CreateLocationMeasurementStatisticEvent.class.getName())) {
				if (intent.hasExtra("event")) {
					String position = intent.getStringExtra("event");
					 
					try {
						Log.d("locationa","locationaaaa");
						JsonParser jp = jsonFactory.createJsonParser(position);
						// get the event
						CreateLocationMeasurementStatisticEvent event = mapper
								.readValue(jp,CreateLocationMeasurementStatisticEvent.class);
						// extract attachment
						SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticListDTO = event
								.getEventAttachment();
  
						LocationSensorMeasurementDTO  sensorMeasurementDTO =   (LocationSensorMeasurementDTO) sensorMeasurementStatisticListDTO.getMeasurementStatistics().get(0).getStatistic();
						
					    updatedPoint = new GeoPoint((int)(sensorMeasurementDTO.getPoint().getLatitude().doubleValue() * 1E6),(int)(sensorMeasurementDTO.getPoint().getLongitude().doubleValue()  * 1E6));
						Log.d("yessssssss", "Gia ton Actor "+ event.getEventSource().getId()+"  Incomeing lat = " + (int)(sensorMeasurementDTO.getPoint().getLatitude().doubleValue() * 1E6) + " lon ="
								+ (int)(sensorMeasurementDTO.getPoint().getLongitude().doubleValue()  * 1E6)+" xaxaxaxa = " + event.getEventSourceStr() + event.getJournalMessageInfo());
					 
						Long tempId =   event.getEventSource().getId();
						OverlayItem updatedItem = new OverlayItem(updatedPoint,
								"id = " + tempId , "Name = " + event.getEventSource().getTitle() );
					    if(myId == tempId){
					    	myOverlay.remove("id = "+Long.toString(tempId));
							myOverlay.addOverlay(updatedItem);
							mapView.getOverlays().add(myOverlay);
						//	mapController.animateTo(updatedPoint);

							currentLat = updatedPoint.getLongitudeE6();
							currentLon = updatedPoint.getLatitudeE6(); 
					    }else{
					    	othersOverlay.remove("id = "+Long.toString(tempId));
					    	othersOverlay.addOverlay(updatedItem);
							mapView.getOverlays().add(othersOverlay);
					    }
					   
						mapView.invalidate();

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
		 } else if(action.contains(UpdateCrisisContextEvent.class.getName())) {
			  String incomingEvent = intent.getStringExtra("event");
			  UpdateCrisisContextEvent event = null;
			  // deserialize incoming Event
			 try {
			      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
			      // get the event
			       event = mapper.readValue(jp, UpdateCrisisContextEvent.class);
			      
			   	BigDecimal lat = event.getEventAttachment().getCrisisLocation().getCentre().getLatitude();
	    		BigDecimal lon = event.getEventAttachment().getCrisisLocation().getCentre().getLongitude();
	    		GeoPoint point1 = new GeoPoint(lat.intValue() , lon.intValue()); 
	    	 	mapView.getOverlays().add(new CircleOverlay(context, point1.getLatitudeE6(),point1.getLongitudeE6()));
	    	 	mapView.invalidate();
	    	 	 
			  } catch (Exception e) {
			      e.printStackTrace();
			    }
		  }
		}

	}
 
	// Radius
	public class CircleOverlay extends Overlay{

	    Context context;
	    double mLat;
	    double mLon;

	     public CircleOverlay(Context _context, double _lat, double _lon ) {
	            context = _context;
	            mLat = _lat;
	            mLon = _lon;
	     }

	     public void draw(Canvas canvas, MapView mapView, boolean shadow) {

	         super.draw(canvas, mapView, shadow); 

	         Projection projection = mapView.getProjection();

	         Point pt = new Point();

	         GeoPoint geo = new GeoPoint((int) (mLat), (int)(mLon));

	         projection.toPixels(geo ,pt);

	         float circleRadius = 15;

	         Paint circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

	            circlePaint.setColor(0x30000000);
	            circlePaint.setStyle(Style.FILL_AND_STROKE);

	         canvas.drawCircle((float)pt.x, (float)pt.y, circleRadius, circlePaint);
             Drawable drawable = context.getResources().getDrawable(R.drawable.marker2);
             Bitmap markerBitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.marker);
             canvas.drawBitmap(markerBitmap,pt.x,pt.y-markerBitmap.getHeight(),null);
	        }
	}
	
	public void onDestroy() {
		super.onDestroy();
	   unregisterReceiver(receiver); 
	}

}
