package com.esponder.apps;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.crisis.UpdateCrisisContextEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateLocationMeasurementStatisticEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;


class Sender extends Thread 
{
    Context context;
    public Sender(Context ctx)
    {
        context = ctx;
    }

    public void run()
    {
    	int count=0;
    	  while(true){
    	try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  
    	
    	
    	//   ----------------------------------------   Start send Arithmetic Statistic  ---------------------------------------- 
    	
    	 BodyTemperatureSensorDTO bodyTemperatureSensor = new BodyTemperatureSensorDTO();
         bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
         bodyTemperatureSensor.setName("Sensor_Temperature");
         bodyTemperatureSensor.setType("Sensor_Temperature");
         bodyTemperatureSensor.setLabel("BdTemp");
         bodyTemperatureSensor.setTitle("Body Temperature Sensor");
         bodyTemperatureSensor.setId(new Long("1337583259923"));
         bodyTemperatureSensor.setStatus(ResourceStatusDTO.AVAILABLE);
		
         Random rand = new Random();
         ArithmeticSensorMeasurementDTO sensorMeasurement1 = new ArithmeticSensorMeasurementDTO();
         sensorMeasurement1.setMeasurement(new BigDecimal(rand.nextInt(80 - 60 + 1) + 60));
         sensorMeasurement1.setTimestamp(new Date());
         sensorMeasurement1.setSensor(bodyTemperatureSensor);
         sensorMeasurement1.setId(System.currentTimeMillis());

         SensorMeasurementStatisticDTO statistic1  = new SensorMeasurementStatisticDTO();
         statistic1.setStatistic(sensorMeasurement1);
         statistic1.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
         statistic1.setId(System.currentTimeMillis());
         statistic1.setSamplingPeriod(new Long("5"));
         statistic1.setPeriod(new PeriodDTO());
	  
         List<SensorMeasurementStatisticDTO> sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
          sensorMeasurementStatistic.add(statistic1); 
          
         SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
         sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
		
        ActorDTO actorDTO = new ActorDTO();
        actorDTO.setId(new Long(7));
        actorDTO.setType("FRC 5555");
        actorDTO.setTitle("FRC #7777");
        actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
         
         CreateSensorMeasurementStatisticEvent eventArethmitic = new CreateSensorMeasurementStatisticEvent();
		    eventArethmitic.setEventSeverity(SeverityLevelDTO.MEDIUM);
		    eventArethmitic.setEventTimestamp(new Date());
	     	eventArethmitic.setEventAttachment(sensorMeasurementStatisticEnvelope);
	     	eventArethmitic.setEventSource(actorDTO);
	     	
	     	
	        ObjectMapper  mapperArethmetic = new ObjectMapper();
	     	mapperArethmetic.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
	     	mapperArethmetic.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);   
	     	mapperArethmetic.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	     	 String jsonArethmitic = null;
	          try {
	        	  jsonArethmitic = mapperArethmetic.writeValueAsString(eventArethmitic);
	  			Log.d(" Sender Send "," Sender Send  "+jsonArethmitic);
	  		} catch (JsonGenerationException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		} catch (JsonMappingException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		} catch (IOException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		}
	          
	          

	  	    Intent intArethmitic = new Intent(CreateSensorMeasurementStatisticEvent.class.getName()+".4"); 
	    	intArethmitic.putExtra("event", jsonArethmitic); 
	 	 	context.sendBroadcast(intArethmitic);
	 	 	
    	
	 	 	
	 	 	
	 	 /*	  bodyTemperatureSensor = new BodyTemperatureSensorDTO();
	         bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnum.DEGREES_CELCIUS);
	         bodyTemperatureSensor.setName("Sensor_Temperature");
	         bodyTemperatureSensor.setType("Sensor_Temperature");
	         bodyTemperatureSensor.setLabel("BdTemp");
	         bodyTemperatureSensor.setTitle("Body Temperature Sensor");
	         bodyTemperatureSensor.setId(new Long("1337583259922"));
	         bodyTemperatureSensor.setStatus(ResourceStatusDTO.AVAILABLE);
			
	           rand = new Random();
	           sensorMeasurement1 = new ArithmeticSensorMeasurementDTO();
	         sensorMeasurement1.setMeasurement(new BigDecimal(rand.nextInt(80 - 60 + 1) + 60));
	         sensorMeasurement1.setTimestamp(new Date());
	         sensorMeasurement1.setSensor(bodyTemperatureSensor);
	         sensorMeasurement1.setId(System.currentTimeMillis());

	           statistic1  = new SensorMeasurementStatisticDTO();
	         statistic1.setStatistic(sensorMeasurement1);
	         statistic1.setStatisticType(MeasurementStatisticTypeEnum.MEAN);
	         statistic1.setId(System.currentTimeMillis());
	         statistic1.setSamplingPeriod(new Long("5"));
	         statistic1.setPeriod(new PeriodDTO());
		  
	          sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
	          sensorMeasurementStatistic.add(statistic1); 
	          
	         sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
	         sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
			
	          actorDTO = new ActorDTO();
	        actorDTO.setId(new Long(7));
	        actorDTO.setType("FRC 4444");
	        actorDTO.setTitle("FRC #4444");
	        actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
	         
	           eventArethmitic = new CreateSensorMeasurementStatisticEvent();
			    eventArethmitic.setEventSeverity(SeverityLevelDTO.MEDIUM);
			    eventArethmitic.setEventTimestamp(new Date());
		     	eventArethmitic.setEventAttachment(sensorMeasurementStatisticEnvelope);
		     	eventArethmitic.setEventSource(actorDTO);
		     	
		     	
		           mapperArethmetic = new ObjectMapper();
		     	mapperArethmetic.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
		     	mapperArethmetic.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);   
		     	mapperArethmetic.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		     	   jsonArethmitic = null;
		          try {
		        	  jsonArethmitic = mapperArethmetic.writeValueAsString(eventArethmitic);
		  			Log.d(" Sender Send "," Sender Send  "+jsonArethmitic);
		  		} catch (JsonGenerationException e) {
		  			// TODO Auto-generated catch block
		  			e.printStackTrace();
		  		} catch (JsonMappingException e) {
		  			// TODO Auto-generated catch block
		  			e.printStackTrace();
		  		} catch (IOException e) {
		  			// TODO Auto-generated catch block
		  			e.printStackTrace();
		  		}
		          
		          

		  	      intArethmitic = new Intent(CreateSensorMeasurementStatisticEvent.class.getName()+".4"); 
		    	intArethmitic.putExtra("event", jsonArethmitic); 
		 	 	context.sendBroadcast(intArethmitic);*/
    	
    	//   ----------------------------------------   End send Arithmetic Statistic  ---------------------------------------- 
    	
	    	
	    	
	 	 	
	 	 	
	 	//   ----------------------------------------   Start send Action  ---------------------------------------- 
	 	 	
	 	 	
	 	 	ActionDTO action = new ActionDTO();
	 	    action.setTitle("General No7");
	 	    action.setId(new Long(7));
	 	    
	 	        ActorDTO actorDTO2 = new ActorDTO();
	 	        actorDTO2.setId(new Long(4));
	 	        actorDTO2.setType("FRC 4444");
	 	        actorDTO2.setTitle("FRC #4444");
	 	        actorDTO2.setStatus(ResourceStatusDTO.AVAILABLE);
	 		   	
	 			ActorDTO actorDTO3= new ActorDTO();
	 			actorDTO3.setId(new Long(7));
	 			actorDTO3.setType("FRC 7777");
	 			actorDTO3.setTitle("FRC #7777");
	 			actorDTO3.setStatus(ResourceStatusDTO.AVAILABLE);
	 	 	
	 	    ReusableResourceDTO reusableResource1 = new ReusableResourceDTO();
	 	    reusableResource1.setTitle("truck");
	 	    HashSet reusableResourceSet = new HashSet();
	 	    reusableResourceSet.add(reusableResource1);
	 	    
	 	    ReusableResourceDTO reusableResource2 = new ReusableResourceDTO();
	 		reusableResource2.setType("red tools");
	 	    HashSet  reusableResourceSet2 = new HashSet();
	 		reusableResourceSet2.add(reusableResource2);

	 	    ConsumableResourceDTO ConsumableResource1 = new ConsumableResourceDTO();
	 	    ConsumableResource1.setTitle("food packages");
	 	    ConsumableResource1.setQuantity(new BigDecimal(10));
	 	    HashSet consumableResourceSet = new HashSet();
	 	    consumableResourceSet.add(ConsumableResource1);

	 	    ConsumableResourceDTO ConsumableResource2 = new ConsumableResourceDTO();
	 	    ConsumableResource2.setTitle("telecom lines");
	 	    HashSet consumableResourceSet2 = new HashSet();
	 	    consumableResourceSet2.add(ConsumableResource2);

	 	    ActionPartObjectiveDTO actionPartObjective1 = new ActionPartObjectiveDTO();
	 	    actionPartObjective1.setConsumableResources(consumableResourceSet);
	 	    HashSet actionPartObjectiveSet = new HashSet();
	 	    actionPartObjectiveSet.add(actionPartObjective1);

	 	    ActionPartObjectiveDTO actionPartObjective2 = new ActionPartObjectiveDTO();
	 	    actionPartObjective2.setConsumableResources(consumableResourceSet2);
	 	    HashSet actionPartObjectiveSet2 = new HashSet();
	 	    actionPartObjectiveSet2.add(actionPartObjective2);

	 	    // Action No1
	 	    ActionPartDTO actoinPart1 = new ActionPartDTO();
	 	    actoinPart1.setUsedReusuableResources(reusableResourceSet);
	 	    actoinPart1.setUsedConsumableResources(consumableResourceSet);
	 	    actoinPart1.setActionPartObjectives(actionPartObjectiveSet);
	 	    actoinPart1.setActionOperation(ActionOperationEnumDTO.MOVE);
	 	    ActionPartSnapshotDTO actionsnap1 = new ActionPartSnapshotDTO();
	 	    actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	 	    actoinPart1.setSnapshot(actionsnap1);
	 	    actoinPart1.setId(new Long(11));
	 	    actoinPart1.setActionID(action.getId());
	 	    actoinPart1.setActor(actorDTO2);
	 	    
	 	    // Action No3
	 	    ActionPartDTO actoinPart3 = new ActionPartDTO();
	 	    actoinPart3.setUsedReusuableResources(reusableResourceSet);
	 	    actoinPart3.setUsedConsumableResources(consumableResourceSet);
	 	    actoinPart3.setActionPartObjectives(actionPartObjectiveSet);
	 	    actoinPart3.setActionOperation(ActionOperationEnumDTO.MOVE);
	 	    actionsnap1 = new ActionPartSnapshotDTO();
	 	    actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	 	    actoinPart3.setSnapshot(actionsnap1);
	 	    actoinPart3.setId(new Long(13));
	 	    actoinPart3.setActionID(action.getId());
	 	    actoinPart3.setActor(actorDTO2);

	 	    // Action No4
	 	    ActionPartDTO actoinPart5 = new ActionPartDTO();
	 	    actoinPart5.setUsedReusuableResources(reusableResourceSet);
	 	    actoinPart5.setUsedConsumableResources(consumableResourceSet);
	 	    actoinPart5.setActionPartObjectives(actionPartObjectiveSet);
	 	    actoinPart5.setActionOperation(ActionOperationEnumDTO.MOVE);
	 	    actionsnap1 = new ActionPartSnapshotDTO();
	 	    actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	 	    actoinPart5.setSnapshot(actionsnap1);
	 	    actoinPart5.setId(new Long(15));
	 	    actoinPart5.setActionID(action.getId());
	 	    actoinPart5.setActor(actorDTO3);

	 	    HashSet<ActionPartDTO> actionParts = new HashSet<ActionPartDTO>();
	 	    actionParts.add(actoinPart1);
	 	    actionParts.add(actoinPart3);
	 	    actionParts.add(actoinPart5);

	 	    action.setActionParts(actionParts);
	 	 	
	 	 	
	 	   CreateActionEvent eventAction = new CreateActionEvent();
	 	     eventAction.setEventSeverity(SeverityLevelDTO.MEDIUM);
	 	     eventAction.setEventTimestamp(new Date());
	 	     eventAction.setEventAttachment(action);
	 	     eventAction.setEventSource(actorDTO);
	     	
	     	
	        ObjectMapper  mapperAction = new ObjectMapper();
	        mapperAction.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
	        mapperAction.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);   
	        mapperAction.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	     	 String jsonAction = null;
	          try {
	        	  jsonAction = mapperArethmetic.writeValueAsString(eventAction);
	  			Log.d(" Sender Send "," Sender Send  "+jsonAction);
	  		} catch (JsonGenerationException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		} catch (JsonMappingException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		} catch (IOException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		}
	          
	          

	  	    Intent intAction = new Intent(CreateActionEvent.class.getName()+".4"); 
	  	    intAction.putExtra("event", jsonAction); 
	     //	context.sendBroadcast(intAction);
	 	 	
	 	 	
	 	 	
	 	//   ----------------------------------------   End send Action  ---------------------------------------- 
	 	 	
	 	 	
	 	 	
	 	//   ----------------------------- Start Send Location  ------------------------------
    	//  Resourse o Sensor
		LocationSensorDTO sensorDTO = new LocationSensorDTO(); 
	    sensorDTO.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES);
	    sensorDTO.setName("LPS");
	    sensorDTO.setType("LPS");
	    sensorDTO.setTitle("Local Positioning Sensor");
	    sensorDTO.setId(System.currentTimeMillis());
	    sensorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
	    
	    //  Resourse o Actor
		ActorDTO subactorDTO= new ActorDTO();
	   	subactorDTO.setId(new Long(7));
	   	subactorDTO.setType("FRC 7777");
	   	subactorDTO.setTitle("FRC #7777");
	   	subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
	    
	    PointDTO pointDTO = new PointDTO();
	    pointDTO.setAltitude(new BigDecimal(220000));
	    pointDTO.setLongitude(new BigDecimal(13727288)); 
	    pointDTO.setLatitude(new BigDecimal(51028791));  // 51.029034,13.728747
	    
	    
	    LocationSensorMeasurementDTO sensorData = new LocationSensorMeasurementDTO();
	    sensorData.setPoint(pointDTO);
	    sensorData.setSensor(sensorDTO);
	    
	    
	    
	    
	    SensorMeasurementStatisticDTO statisticloc  = new SensorMeasurementStatisticDTO();
	    statisticloc.setStatistic(sensorData);
	    statisticloc.setStatisticType(MeasurementStatisticTypeEnumDTO.LAST);
	    statisticloc.setId(System.currentTimeMillis());
	    statisticloc.setSamplingPeriod(new Long("5"));
	    statisticloc.setPeriod(new PeriodDTO());
	  
        List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticloc = new ArrayList<SensorMeasurementStatisticDTO>();
        sensorMeasurementStatisticloc.add(statisticloc); 
         
        SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticEnvelopeloc = new SensorMeasurementStatisticEnvelopeDTO();
        sensorMeasurementStatisticEnvelopeloc.setMeasurementStatistics(sensorMeasurementStatisticloc);
	    
	    
	    CreateLocationMeasurementStatisticEvent event = new CreateLocationMeasurementStatisticEvent();
	    event.setEventSeverity(SeverityLevelDTO.MEDIUM);
	    event.setEventTimestamp(new Date());
	    event.setEventAttachment(sensorMeasurementStatisticEnvelopeloc);
	    event.setEventSource(subactorDTO);
	   
        ObjectMapper  mapper = new ObjectMapper();
	    mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
        mapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
        String json = null ;
	    mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
         try {
 		    json = mapper.writeValueAsString(event);
 			Log.d(" Send Location ", " Send Location  "+json);
 		} catch (JsonGenerationException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		} catch (JsonMappingException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		} catch (IOException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
         
     	 
	    Intent intLoc = new Intent(CreateLocationMeasurementStatisticEvent.class.getName()+".4"); 
	    intLoc.putExtra("event", json); 
	  //  context.sendBroadcast(intLoc);
       // Log.d("o sender","o sender "+CreateLocationSensorMeasurementEvent.class.getName()+".1");
         
         
         
    //   ----------------------------- End Send  Location  ------------------------------
         
	//   ----------------------------- Start Send  CrisisContext  ------------------------------
	    
	    
	    PointDTO a = new PointDTO();
	    a.setLatitude( new BigDecimal("37943578"));
	    a.setLongitude( new BigDecimal("23732063"));
	    a.setAltitude( new BigDecimal("1")); 
	    
	    SphereDTO sphere = new SphereDTO();
	    sphere.setCentre(a);
	    sphere.setRadius(new BigDecimal("10"));
	    sphere.setId(System.currentTimeMillis());
	    
	    CrisisContextDTO crisisContext = new CrisisContextDTO();
	    crisisContext.setTitle("Crisis Context Title");
	    crisisContext.setCrisisLocation(sphere);
	    crisisContext.setId(System.currentTimeMillis());
	    
	    UpdateCrisisContextEvent eventCrisisContext = new UpdateCrisisContextEvent();
	    eventCrisisContext.setEventSeverity(SeverityLevelDTO.MEDIUM);
	    eventCrisisContext.setEventTimestamp(new Date());
	    eventCrisisContext.setEventAttachment(crisisContext);
	    eventCrisisContext.setEventSource(subactorDTO);
	   
         mapper = new ObjectMapper();
	    mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
        mapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
         json = null ;
	    mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
         try {
 		    json = mapper.writeValueAsString(eventCrisisContext);
 			Log.d(" Send Location ", " Send Location  "+json);
 		} catch (JsonGenerationException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		} catch (JsonMappingException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		} catch (IOException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
         
     	 
	    Intent intContext = new Intent(UpdateCrisisContextEvent.class.getName()+".4"); 
	    intContext.putExtra("event", json); 
	      //context.sendBroadcast(intContext);
	    
	    
	    
	    
	//   ----------------------------- End Send  CrisisContext  ------------------------------
         
          
	    	
	    	// ----------------------------- 	Send Envelope With Statistics ------------------------------
	      
		
	    	
	    	ArithmeticSensorMeasurementDTO  arithmeticSensorMeasurement = new ArithmeticSensorMeasurementDTO();
	    	arithmeticSensorMeasurement.setMeasurement(new BigDecimal(810 + count));
	    	arithmeticSensorMeasurement.setTimestamp(new Date());
	    	arithmeticSensorMeasurement.setSensor(bodyTemperatureSensor);

	    	 SensorMeasurementStatisticDTO statistic  = new SensorMeasurementStatisticDTO();
	    	 statistic.setStatistic(arithmeticSensorMeasurement);
	    	 statistic.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
	    	 
	    	 
	    	 	ArithmeticSensorMeasurementDTO  arithmeticSensorMeasurement2 = new ArithmeticSensorMeasurementDTO();
	    	 	arithmeticSensorMeasurement2.setMeasurement(new BigDecimal(900 + count));
	    	 	arithmeticSensorMeasurement2.setTimestamp(new Date());
	    	 	arithmeticSensorMeasurement2.setSensor(bodyTemperatureSensor);

		    	 SensorMeasurementStatisticDTO statistic2  = new SensorMeasurementStatisticDTO();
		    	 statistic2.setStatistic(arithmeticSensorMeasurement2);
		    	 statistic2.setStatisticType(MeasurementStatisticTypeEnumDTO.MAXIMUM);
	
     sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
   sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
     sensorMeasurementStatistic.add(statistic);
     sensorMeasurementStatistic.add(statistic2);
    sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
    
    
    
    
    
    
    pointDTO = new PointDTO();
    pointDTO.setAltitude(new BigDecimal(220000));
    pointDTO.setLongitude(new BigDecimal(23762063+(count*10000))); 
    pointDTO.setLatitude(new BigDecimal(37983578));
    
    
    sensorData = new LocationSensorMeasurementDTO();
    sensorData.setPoint(pointDTO);
    
    
     statistic  = new SensorMeasurementStatisticDTO();
	 statistic.setStatistic(sensorData);
	 statistic.setStatisticType(MeasurementStatisticTypeEnumDTO.LAST);
    
    sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
    sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
    sensorMeasurementStatistic.add(statistic);
    sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
    
        subactorDTO= new ActorDTO();
	   	subactorDTO.setId(new Long(1));
	   	subactorDTO.setType("FRC 1111");
	   	subactorDTO.setTitle("FRC #1111");
	   	subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
    
    
    ESponderEvent<SensorMeasurementStatisticEnvelopeDTO> statisticEvent = new CreateLocationMeasurementStatisticEvent();
    statisticEvent.setEventSeverity(SeverityLevelDTO.MEDIUM);
    statisticEvent.setEventTimestamp(new Date());
    statisticEvent.setEventAttachment(sensorMeasurementStatisticEnvelope);
    statisticEvent.setEventSource(subactorDTO);
  		
     
 	   ObjectMapper  mapperloc = new ObjectMapper();
 	   mapperloc.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
 	   mapperloc.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
 	     String jsonloc = null ;
 	     mapperloc.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          try {
         	 jsonloc = mapperloc.writeValueAsString(statisticEvent);
  		//	Log.d("xaxaxs","Elaaaa!location!  "+jsonloc);
  		} catch (JsonGenerationException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (JsonMappingException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
          
          
      	/*Bundle envelopeloc= new Bundle();
      	envelopeloc.putString("event", jsonloc); */
 	    Intent iloc = new Intent(CreateLocationMeasurementStatisticEvent.class.getName()+".4");
 	    iloc.addCategory("org.osgi.service.event.EventAdmin");
 	    iloc.putExtra("event", jsonloc); 
 	  // 	context.sendBroadcast(iloc);
 	  	
 		
 	    pointDTO = new PointDTO();
 	    pointDTO.setAltitude(new BigDecimal(220000));
 	    pointDTO.setLongitude(new BigDecimal(23115931+(count*10000)));  
 	    pointDTO.setLatitude(new BigDecimal(31928125));
 	    
 	    
 	    sensorData = new LocationSensorMeasurementDTO();
 	    sensorData.setPoint(pointDTO);
 	    
 	    
 	     statistic  = new SensorMeasurementStatisticDTO();
 		 statistic.setStatistic(sensorData);
 		 statistic.setStatisticType(MeasurementStatisticTypeEnumDTO.LAST);
 	    
 	    sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
 	    sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
 	    sensorMeasurementStatistic.add(statistic);
 	    sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
 	    
 	        subactorDTO= new ActorDTO();
 		   	subactorDTO.setId(new Long(4));
 		   	subactorDTO.setType("FRC 4444");
 		   	subactorDTO.setTitle("FRC #4444");
 		   	subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
 	    
 	    
 	    statisticEvent = new CreateLocationMeasurementStatisticEvent();
 	    statisticEvent.setEventSeverity(SeverityLevelDTO.MEDIUM);
 	    statisticEvent.setEventTimestamp(new Date());
 	    statisticEvent.setEventAttachment(sensorMeasurementStatisticEnvelope);
 	    statisticEvent.setEventSource(subactorDTO);
 	  		
 	     
 	 	   mapperloc = new ObjectMapper();
 	 	   mapperloc.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
 	 	   mapperloc.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
 	 	   jsonloc = null ;
 	 	     mapperloc.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
 	          try {
 	         	 jsonloc = mapperloc.writeValueAsString(statisticEvent);
 	  		//	Log.d("xaxaxs","Elaaaa!location!  "+jsonloc);
 	  		} catch (JsonGenerationException e) {
 	  			// TODO Auto-generated catch block
 	  			e.printStackTrace();
 	  		} catch (JsonMappingException e) {
 	  			// TODO Auto-generated catch block
 	  			e.printStackTrace();
 	  		} catch (IOException e) {
 	  			// TODO Auto-generated catch block
 	  			e.printStackTrace();
 	  		}
 	          
 	          
 	      	/*Bundle envelopeloc= new Bundle();
 	      	envelopeloc.putString("event", jsonloc); */
 	 	    iloc = new Intent(CreateLocationMeasurementStatisticEvent.class.getName()+".1");
 	 	    iloc.addCategory("org.osgi.service.event.EventAdmin");
 	 	    iloc.putExtra("event", jsonloc); 
 	 	  	//context.sendBroadcast(iloc);
 	  	 
 	 	  	
    count = count + 5;
      }  // while end  
    }
}