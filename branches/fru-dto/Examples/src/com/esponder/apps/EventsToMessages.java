package com.esponder.apps;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.event.model.crisis.UpdateCrisisContextEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateLocationMeasurementStatisticEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.jsonrcp.JSONRPCException;

public class EventsToMessages {

	private LinkedList<JournalMessage> messageList;
	SharedPreferences jernalSettings;
	boolean show = true;
	AlertDialog.Builder builder;
	AlertDialog info ;
	
	public LinkedList<JournalMessage> getMessages(Context context) {

		messageList = new LinkedList<JournalMessage>();
		
		jernalSettings = context.getSharedPreferences("jernalSettings",context.MODE_WORLD_READABLE);
		
		Log.d("locationMeasurementsEvents", "Einai = "+jernalSettings.getBoolean("locationMeasurementsEvents", false));
		Log.d("sensorMeasurementsEvents", "Einai = "+jernalSettings.getBoolean("sensorMeasurementsEvents", false)); 
	    
	    try {
			Log.d(" before "," before ");
			
			if(jernalSettings.getBoolean("sensorMeasurementsEvents", false)){
			ESponderEvent[] allEvents = (ESponderEvent[])Menu.client.call("esponderdb/getESponderEvents", 
		              ESponderEvent[].class, CreateSensorMeasurementStatisticEvent.class.getName());
			  if(allEvents != null || allEvents.length == 0){
				  show = false;
				 
		          for (int i = 0; i < allEvents.length; i++) {
		            System.out.println("All Events FROM DB : " + allEvents[i] + "extra info = " +  allEvents[i].getJournalMessage());
		           
		            JournalMessage message = new JournalMessage();
			    	message = new JournalMessage();
					message.setSeverity(allEvents[i].getEventSeverity());
					message.setMessage("TYPE: " + ((SensorMeasurementStatisticEnvelopeDTO) allEvents[i].getEventAttachment()).getMeasurementStatistics().get(0).getStatistic().getSensor().getName()+" \n"+allEvents[i].getJournalMessage());
					message.setEvent(allEvents[i]);
					messageList.add(message);
					 
		           }
			   } 
			} 
			if(jernalSettings.getBoolean("locationMeasurementsEvents", false)){
				ESponderEvent[] allEvents = (ESponderEvent[])Menu.client.call("esponderdb/getESponderEvents", 
			              ESponderEvent[].class, CreateLocationMeasurementStatisticEvent.class.getName());
			        if(allEvents != null || allEvents.length == 0){
			        	show = false;
			         
				  for (int i = 0; i < allEvents.length; i++) {
			            System.out.println("All Events FROM DB : " + allEvents[i] + "extra info = " +  allEvents[i].getJournalMessage());
			           
			            JournalMessage message = new JournalMessage();
				    	message = new JournalMessage();
						message.setSeverity(allEvents[i].getEventSeverity());
						message.setMessage("TYPE: " + ((SensorMeasurementStatisticEnvelopeDTO) allEvents[i].getEventAttachment()).getMeasurementStatistics().get(0).getStatistic().getSensor().getName()+" \n"+allEvents[i].getJournalMessage());
						message.setEvent(allEvents[i]);
						messageList.add(message);
						 
			          }
			        }
			}
			if(jernalSettings.getBoolean("crisisContextEvents", false)){
				ESponderEvent[] allEvents = (ESponderEvent[])Menu.client.call("esponderdb/getESponderEvents", 
			              ESponderEvent[].class, CreateCrisisContextEvent.class.getName());
			        if(allEvents != null || allEvents.length == 0){
			        	show = false;
			         
				  for (int i = 0; i < allEvents.length; i++) {
			            System.out.println("All Events FROM DB : " + allEvents[i] + "extra info = " +  allEvents[i].getJournalMessage());
			           
			            JournalMessage message = new JournalMessage();
				    	message = new JournalMessage();
						message.setSeverity(allEvents[i].getEventSeverity());
						message.setMessage("TYPE: Crisis Context " + " \n"+allEvents[i].getJournalMessage());
						message.setEvent(allEvents[i]);
						messageList.add(message);
						 
			          }
			        }
			}
			if(jernalSettings.getBoolean("updateCrisisContextEvents", false)){
				ESponderEvent[] allEvents = (ESponderEvent[])Menu.client.call("esponderdb/getESponderEvents", 
			              ESponderEvent[].class, UpdateCrisisContextEvent.class.getName());
			        if(allEvents != null || allEvents.length == 0){
			        	show = false;
			         
				  for (int i = 0; i < allEvents.length; i++) {
			            System.out.println("All Events FROM DB : " + allEvents[i] + "extra info = " +  allEvents[i].getJournalMessage());
			           
			            JournalMessage message = new JournalMessage();
				    	message = new JournalMessage();
						message.setSeverity(allEvents[i].getEventSeverity());
						message.setMessage("TYPE: Update Crisis Context " + " \n"+allEvents[i].getJournalMessage());
						message.setEvent(allEvents[i]);
						messageList.add(message);
						 
			          }
			        }
			}
			if(jernalSettings.getBoolean("actionEvents", false)){
				ESponderEvent[] allEvents = (ESponderEvent[])Menu.client.call("esponderdb/getESponderEvents", 
			              ESponderEvent[].class, CreateActionEvent.class.getName());
			        if(allEvents != null || allEvents.length == 0){
			        	show = false;
			         
				  for (int i = 0; i < allEvents.length; i++) {
			            System.out.println("All Events FROM DB : " + allEvents[i] + "extra info = " +  allEvents[i].getJournalMessage());
			           
			            JournalMessage message = new JournalMessage();
				    	message = new JournalMessage();
						message.setSeverity(allEvents[i].getEventSeverity());
						message.setMessage("TYPE: Action Event " + " \n"+allEvents[i].getJournalMessage());
						message.setEvent(allEvents[i]);
						messageList.add(message);
						 
			          }
			        }
			}
		          
		          Log.d(" after "," after ");
	    	/* Menu.client.call("esponderdb/addESponderEvent", null, statisticEvent2);
	          
	          CreateSensorMeasurementEnvelopeEvent sensor = (CreateSensorMeasurementEnvelopeEvent)Menu.client.call("esponderdb/getESponderEvent", 
	              CreateSensorMeasurementEnvelopeEvent.class, event.getClass().getName(), event.getEventTimestamp().getTime());
	          System.out.println("Event FROM DB : " + sensor);

	          ESponderEvent[] allSensors = (ESponderEvent[])Menu.client.call("esponderdb/getESponderEvents", 
	                  ESponderEvent[].class, sensor.getClass().getName());
	              for (int i = 0; i < allSensors.length; i++) {
	                System.out.println("All Events FROM DB : " + allSensors[i]);
	              }*/
	         
    	} catch (JSONRPCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	    if(show){
    	    builder = new AlertDialog.Builder(context);
        	builder
			.setMessage(" Not Events. Open EventAdmin if closed. ")
			.setPositiveButton("Ok", null);
        	 info = builder.create();
        	 info.show();
      }
	    
	    Collections.sort(messageList,COMPARATOR);
 	return messageList;
	}
	
	// Sort Events 
			 private static Comparator<JournalMessage> COMPARATOR = new Comparator<JournalMessage>()
					    {
					     
							@Override
							public int compare(JournalMessage o1, JournalMessage o2) {
								 return (int) ( o2.getEvent().getEventTimestamp().compareTo(o1.getEvent().getEventTimestamp()));
							}
					    };
}
