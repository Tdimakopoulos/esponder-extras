package com.esponder.apps;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import android.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.jsonrcp.JSONRPCException;

public class OperationsByActor extends Activity {

	private ListView messageListView;
	private OperationsListAdapter adapterList;
	private LinkedList<ActionPartDTO> actionPartList;
	private String filter = CreateActionEvent.class.getName();
    int count = 5;
	private IncomingReceiver receiver;
	private String actorName;
	private Long actorId;
    private TextView actorTitle;
    Iterator actionPartIterator;
    AlertDialog.Builder builder;
	AlertDialog info ;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operationsbyactor);
		messageListView = (ListView) findViewById(R.id.MessageListView);
		actorTitle      = (TextView) findViewById(R.id.actorTitle);	 
		actionPartList = new LinkedList<ActionPartDTO>();
		adapterList = new OperationsListAdapter(this,1);  
		messageListView.setAdapter(adapterList);
		
		Intent intent = getIntent();
	       if (intent != null)
	        {
	        	Bundle b = intent.getBundleExtra("actorInfo");
	        	if (b != null)
	        	{
	        		actorId = b.getLong("actorId") ; 
	        		actorName = b.getString("actorName") ;   
	        		actorTitle.setText(actorName);
	        	} 
	        	 Log.d(" ActoInfo ","actorId = " + actorId + " actorName = " + actorName);
	        }
		
	    receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(); 
		 
		// if chief register to CreateLocationMeasurementStatisticEvent . yours id
		if(Menu.actor.getSubordinates()!=null){
			intentFilter.addAction(filter +"."+Menu.actor.getId());
		}else{ // register to CreateLocationMeasurementStatisticEvent . chief's id
			 intentFilter.addAction(filter +"."+Menu.actor.getSupervisor().getId());
		}
		
	    intentFilter.addCategory("org.osgi.service.event.EventAdmin");
		registerReceiver(receiver, intentFilter);
		
		// Tests
	/*	   HashSet<ActionPartDTO>  actionPartsSet =  (HashSet<ActionPartDTO>) action.getActionParts();
		      actionPartIterator = actionPartsSet.iterator(); 
		      while (actionPartIterator.hasNext()){
		    	  ActionPartDTO actionPartTemp =  ((ActionPartDTO) actionPartIterator.next());
		    	  actionPartList.add(actionPartTemp);
		      	  } 
		 	   
		     actionPartsSet =  (HashSet<ActionPartDTO>) action2.getActionParts();
		      actionPartIterator = actionPartsSet.iterator(); 
		      while (actionPartIterator.hasNext()){
		    	  ActionPartDTO actionPartTemp =  ((ActionPartDTO) actionPartIterator.next());
		    	  actionPartList.add(actionPartTemp);
		      	  }  */
	      
	      
	    	ESponderEntityDTO[] allActions = null;
	    	try {
	    		allActions = (ESponderEntityDTO[])Menu.client.call("esponderdb/getAllESponderDTOs", 
	    		          ESponderEntityDTO[].class, ActionDTO.class.getName());
	    	} catch (JSONRPCException e) {
	    		// TODO Auto-generated catch block
	    		e.printStackTrace();
	    	} 
	    	   if( allActions != null ){
	    	              Log.d(" Size "," Size " + allActions);
	    	     for(int i=0;i<allActions.length;i++){
	    	    	 ActionDTO actionTemp = (ActionDTO) allActions[i];
	    	    	  HashSet<ActionPartDTO>  actionPartsSet =  (HashSet<ActionPartDTO>) actionTemp.getActionParts();
	    		      actionPartIterator = actionPartsSet.iterator(); 
	    		      while (actionPartIterator.hasNext()){
	    		    	  ActionPartDTO actionPartTemp =  ((ActionPartDTO) actionPartIterator.next());
	    		    	    if(actionPartTemp.getActor().getId() == actorId){
	    		    	     Log.d(" natoooooo "," nato = " + actionPartTemp.getActionID());
	    		    	    	actionPartList.add(actionPartTemp);
	    		    	      }
	    		    	  } 
	                	 
	                 }
	    	   } else{
	    		    builder = new AlertDialog.Builder(this);
		        	builder
					.setMessage(" EventAdmin is not Active. ")
					.setPositiveButton("Ok", null);
		        	 info = builder.create();
		        	 info.show();
	    	   }
	      
	        	Collections.sort(actionPartList, COMPARATOR);
	     
 		
		ShowOrder((LinkedList<ActionPartDTO>) actionPartList);
		
    }

	public void ShowOrder(final LinkedList<ActionPartDTO> operationMessage) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.addOrder(operationMessage);
				adapterList.notifyDataSetChanged();
			}
		});
	}

	public void ClearOrder() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.clear();
				adapterList.notifyDataSetChanged();
			}
		});
	}
	 
	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
             boolean forMe = false;
			 String incomingEvent = intent.getStringExtra("event");
			  ObjectMapper	mapper = new ObjectMapper();
			  mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
			  JsonFactory jsonFactory = new JsonFactory(); 
			  
			  CreateActionEvent event = null;
			  // deserialize incoming Event
			 try {
			      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
			      // get the event
			       event = mapper.readValue(jp, CreateActionEvent.class);
			       
			  } catch (Exception e) {
			      e.printStackTrace();
			    }
			
			 HashSet<ActionPartDTO>  actionPartsSet =  (HashSet<ActionPartDTO>) event.getEventAttachment().getActionParts();
			  Iterator actionPartIterator = actionPartsSet.iterator(); 
		      while (actionPartIterator.hasNext()){
		    	  ActionPartDTO actionPartTemp =  ((ActionPartDTO) actionPartIterator.next());
		    	    if(actionPartTemp.getActor().getId() == actorId){ 
		    	    	forMe = true;
		    	      }
		    	  } 
		      
			if(forMe){
			   AlertDialog.Builder dialog = new AlertDialog.Builder(context);
			    dialog.setTitle(" New Action Arrived ");
		       //dialog.setMessage("No other Events");
		        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int id) {
		            	    Intent itemintent = new Intent(OperationsByActor.this,OperationsByActor.class);
		        	        Bundle info = new Bundle();
				            info.putLong("actorId",actorId);
				            info.putString("actorName", actorName); 
				            itemintent.putExtra("actorInfo", info);
	 			            startActivity(itemintent);
						    overridePendingTransition(0, 0);
						    finish();
		            }
		          });
		         dialog.show(); 
			}
		}

	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

	
	@Override
	public void onResume()
	{  super.onResume();
	   ClearOrder();
	
   	ESponderEntityDTO[] allActions = null;
	try {
		allActions = (ESponderEntityDTO[])Menu.client.call("esponderdb/getAllESponderDTOs", 
		          ESponderEntityDTO[].class, ActionDTO.class.getName());
	} catch (JSONRPCException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	   if( allActions != null ){
	              Log.d(" Size "," Size " + allActions);
	     for(int i=0;i<allActions.length;i++){
	    	 ActionDTO actionTemp = (ActionDTO) allActions[i];
	    	  HashSet<ActionPartDTO>  actionPartsSet =  (HashSet<ActionPartDTO>) actionTemp.getActionParts();
		      actionPartIterator = actionPartsSet.iterator(); 
		      while (actionPartIterator.hasNext()){
		    	  ActionPartDTO actionPartTemp =  ((ActionPartDTO) actionPartIterator.next());
		    	    if(actionPartTemp.getActor().getId() == actorId){
		    	     Log.d(" natoooooo "," nato = " + actionPartTemp.getActionID());
		    	    	actionPartList.add(actionPartTemp);
		    	      }
		    	  } 
            	 
             }
	   } else{
		    builder = new AlertDialog.Builder(this);
        	builder
			.setMessage(" EventAdmin is not Active. ")
			.setPositiveButton("Ok", null);
        	 info = builder.create();
        	 info.show();
	   }
  
    	Collections.sort(actionPartList, COMPARATOR);
 
	
        ShowOrder((LinkedList<ActionPartDTO>) actionPartList);
	}
	
	
	
	// Sort ActionPartDTO by id
	 private static Comparator<ActionPartDTO> COMPARATOR = new Comparator<ActionPartDTO>()
			    {
			        public int compare(ActionPartDTO o1, ActionPartDTO o2)
			        {
			            return (int) (o1.getActionID() - o2.getActionID());
			        }
			    };
}