package com.esponder.apps;

import android.R;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ReceivingMenu extends Activity {


	TextView status;
	Button startB,stopB;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.receivingmenu);
		status = (TextView) findViewById(R.id.ReceivingStatus);
		startB = (Button) findViewById(R.id.Start);
		stopB = (Button) findViewById(R.id.Stop);
		 
		if(isMyServiceRunning()){
			status.setText("Controller Active");
			startB.setEnabled(false);
	 	}else{
	 		stopB.setEnabled(false);
	 	}   
  	}

	public void clickHandler(View view) {
		switch (view.getId()) {
		case R.id.Start: // Start Controller
			
			//Start the Service
			Intent start = new Intent("com.esponder.apps.ReceivingControl");
			ReceivingMenu.this.startService(start);
			status.setText("Receiving Active");
			view.setEnabled(false);
			if(!stopB.isEnabled()){
				stopB.setEnabled(true);
			}
				
			break;
		case R.id.Stop: // Stop Controller
			
			//Stop the Service
			Intent stop = new Intent("com.esponder.apps.ReceivingControl");
			ReceivingMenu.this.stopService(stop);
			status.setText("Receiving Not Active");
			view.setEnabled(false);
			if(!startB.isEnabled()){
				startB.setEnabled(true);
			}
			break;
		default:
			break;
		}
	}
 
	private boolean isMyServiceRunning() {
	    ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if ("com.esponder.apps.ReceivingControl".equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
}
