package com.dynamicview;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import android.app.AlertDialog;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

class JournalListAdapter extends BaseAdapter {

	private List<JournalMessage> items = new ArrayList<JournalMessage>();
	private LayoutInflater inflater;
	private Context context;
	private TextView messages;
	private LinearLayout linearLayout;
	Drawable messageIcon;

	public JournalListAdapter(Context context1) {
		context = context1;
		inflater = LayoutInflater.from(context1);
	}

	public View getView(final int position, View convertView, ViewGroup parent) {

		View v = inflater.inflate(R.layout.journallistview, null);
		  

		final JournalMessage journalMessage = items.get(position);
		if (journalMessage != null) {

			linearLayout = (LinearLayout) v.findViewById(R.id.backlinearlayout);
			messages = (TextView) v.findViewById(R.id.textview);
			messages.setText(journalMessage.getMessage()); 
			messages.setBackgroundColor(SeverityLevelDTO.getSeverityColor(journalMessage.getEvent().getEventSeverity().getValue())); 
					
			if (journalMessage.getType().equals("MINIMAL")) { 
				
				messageIcon = context.getResources().getDrawable(R.drawable.minimal);
				messages.setCompoundDrawablesWithIntrinsicBounds(messageIcon, null, null, null);
				
			} else if (journalMessage.getType().equals("MEDIUM")) {

				messageIcon = context.getResources().getDrawable(R.drawable.medium);
				messages.setCompoundDrawablesWithIntrinsicBounds(messageIcon, null, null, null);

			} else if (journalMessage.getType().equals("SERIOUS")) {

				messageIcon = context.getResources().getDrawable(R.drawable.serious);
				messages.setCompoundDrawablesWithIntrinsicBounds(messageIcon, null, null, null);

			}
			messages.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View view) {
	             	AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
					  dialog.setTitle("Message Info");
				      dialog.setMessage(journalMessage.getMessage()+"\nINFO: "+journalMessage.getEvent().getJournalMessageInfo());
				      dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				          public void onClick(DialogInterface dialog, int id) {
				          }
				      });
				  	dialog.show();
	            }

	        });


		}

		return v;

	}

	public void onClick(View arg0) {
		messages.setText("My text on click");
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	public JournalMessage getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void addOrder(List<JournalMessage> message) {
		this.items = message;
	}

	public void clear() {
		this.items.clear();
	}

}
