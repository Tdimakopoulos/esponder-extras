package com.dynamicview;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;

import android.R;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ScrollView;
import android.widget.TextView;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.jsonrcp.JSONRPCException;

public class ShowStatistic extends Activity {
	// WebView dailySodiumWebView;
	// WebView weeklySodiumWebView;

	WebView dailyWebView;
	WebView weeklyWebView;
	TextView DailyTrendTextView;
	ScrollView scrolview;
	String url = "file:///android_asset/dailygraph.html?",
			url2 = "file:///android_asset/weeklygraph.html?",
			url3 = "file:///android_asset/my_example.html?";
	String measurement,measurementClass,measurementStatisticType;
	int i, count;
	String From,To;
	Handler handler;
	Runnable Refresh;
	Format formatter;
	String[] temp,temp2;
	 
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.statisticslayout);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		handler = new Handler();
		formatter = new SimpleDateFormat("yyyy/MM/dd%HH:mm:ss");
		i = 0;
		count = 0;
		Intent startingIntent = getIntent();
		if (startingIntent != null) {
			Bundle b = startingIntent.getBundleExtra("values");
			if (b != null) {
				temp2 =  (b.getString("measurementClassType")).split("-");
				measurement = b.getString("measurement");
				From = b.getString("From");
				To = b.getString("To"); 
				Log.d("From",From);
				Log.d("To",To);
			}
		} 
		
		scrolview = (ScrollView) findViewById(R.id.scrollview);
		DailyTrendTextView = (TextView) findViewById(R.id.DailyTrendTextView);
		DailyTrendTextView.setText(measurement);
		// Load daily statistics
		dailyWebView = (WebView) findViewById(R.id.DailyWebView);
		dailyWebView.getSettings().setJavaScriptEnabled(true);
		measurementClass = temp2[0];
		measurementStatisticType = temp2[1];
		// Load daily statistics
	//	dailyWebView.loadUrl("file:///android_asset/my_example.html");
		 
		/* try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		//handler.postDelayed(mUpdate, 5000);
		//handler.postDelayed(mUpdate2, 7000);
		// handler.post(mUpdate);

		/*
		 * dailyWebView.setOnTouchListener(new View.OnTouchListener() {
		 * 
		 * @Override public boolean onTouch(View arg0, MotionEvent arg1) {
		 * dailyWebView.getParent().requestDisallowInterceptTouchEvent( true);
		 * // scrolview.setEnabled(false); return false; } });
		 */

	/*	   try {
				Menu.client.call("esponderdb/deleteAllESponderDTOs", 
				         null, SensorMeasurementStatisticDTO.class.getName());
			} catch (JSONRPCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		
		
		ESponderEntityDTO[] allSensors = null;
		try {
			allSensors = (ESponderEntityDTO[])Menu.client.call("esponderdb/getAllESponderDTOs", 
			          ESponderEntityDTO[].class, SensorMeasurementStatisticDTO.class.getName());
		} catch (JSONRPCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	        
		Arrays.sort(allSensors,COMPARATOR);
		
		    for (int i = 0; i < allSensors.length; i++) {
	        	  if( ((SensorMeasurementStatisticDTO)allSensors[i]).getStatistic().getSensor() instanceof BodyTemperatureSensorDTO ) {
	        		    System.out.println("To id tou einai = "+ allSensors[i].getId() +" All SENSORs FROM DB : " + (((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)allSensors[i]).getStatistic())).getMeasurement() + " kai sensor = "+(((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)allSensors[i]).getStatistic())).getSensor().getId()  + " tou Actor = " + Menu.actorSensorMap.get((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)allSensors[i]).getStatistic())).getSensor().getId()) + " Hmerominia = " + formatter.format((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)allSensors[i]).getStatistic())).getTimestamp()));       	        
	        		    temp = Menu.actorSensorMap.get((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)allSensors[i]).getStatistic())).getSensor().getId()).split("#");
	        		    url3 = url3 + temp[1] + "=" + (((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)allSensors[i]).getStatistic())).getMeasurement() + "&" +temp[1] + "=" + formatter.format((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)allSensors[i]).getStatistic())).getTimestamp())+"&";
	        	  
	        	  }  else if(  ((SensorMeasurementStatisticDTO)allSensors[i]).getStatistic() instanceof LocationSensorMeasurementDTO ){
	        	
	        		  PointDTO point =   ((LocationSensorMeasurementDTO)((SensorMeasurementStatisticDTO)allSensors[i]).getStatistic()).getPoint();
	        		  
	      		       System.out.println("To id tou einai = "+ allSensors[i].getId() +" All SENSORs FROM DB : lat = " + point.getLatitude() + " lon "+ point.getLongitude());       	        
	       	  }
	           }
		 
		    // Test Url
		  //  dailyWebView.loadUrl("file:///android_asset/my_example.html?1=7&1=2011/02/07%2010:00:00&1=4&1=2011/02/07%2011:00:00&1=8&1=2011/02/07%2012:00:00&1=3&1=2011/02/07%2013:00:00&2=5&2=2011/02/07%2010:00:00&2=1&2=2011/02/07%2011:00:00&2=3&2=2011/02/07%2012:00:00&2=6&2=2011/02/07%2013:00:00");

		    // Kanoniko
		    url3 = url3.substring(0, url3.length() - 1);
		    Log.d(" urly "," to urly = " + url3);
		    dailyWebView.loadUrl(url3);

	}
	/*
	 * public Runnable mUpdate = new Runnable() { public void run() {
	 * url="file:///android_asset/dailygraph.html?"; Cursor c =
	 * Nephron.db.getTodayMeasurementsUntilNow(code); while (c.moveToNext()) {
	 * url = url + "value=" + c.getDouble(0) + "&" + "time=" + c.getString(1) +
	 * "&"; } count = c.getCount(); Log.d("!!POSAAAAAAAAA!!",
	 * Integer.toString(count)); c.close(); url = url.substring(0, url.length()
	 * - 1); url = url.replaceAll("-", "/"); url = url.replaceAll(" ", "%20");
	 * // Log.d("!!PEIMENOUME DATABASE!!", url); if (i < count) {
	 * Log.d("!!yohhhhhhhoooooooooo!!", "!!yohhhhhhhoooooooooo!!");
	 * 
	 * dailyWebView.loadUrl(url);
	 * 
	 * i = count;
	 * 
	 * } Log.d("!!cooooooooooouuuuuuuuuuuuunt!!", url); // Load weekly
	 * statistics
	 * 
	 * handler.postDelayed(this, 1000);
	 * 
	 * } };
	 * 
	 * protected void onDestroy() { super.onDestroy();
	 * 
	 * handler.removeCallbacks(mUpdate);
	 * 
	 * }
	 */

	
	public Runnable mUpdate = new Runnable() {
		public void run() {
			url="file:///android_asset/my_example.html?1=7&1=2011/02/07%2010:00:00&1=4&1=2011/02/07%2011:00:00&1=8&1=2011/02/07%2012:00:00&1=3&1=2011/02/07%2013:00:00&2=5&2=2011/02/07%2010:00:00&2=1&2=2011/02/07%2011:00:00&2=3&2=2011/02/07%2012:00:00&2=6&2=2011/02/07%2013:00:00&2=9&2=2011/02/07%2014:00:00";
		 
			
	 			dailyWebView.loadUrl(url);

			 
		 
			//handler.postDelayed(this, 1000);

		}
	};
	
	
	
	public Runnable mUpdate2 = new Runnable() {
		public void run() {
			url="file:///android_asset/my_example.html?1=7&1=2011/02/07%2010:00:00&1=4&1=2011/02/07%2011:00:00&1=8&1=2011/02/07%2012:00:00&1=3&1=2011/02/07%2013:00:00&2=5&2=2011/02/07%2010:00:00&2=1&2=2011/02/07%2011:00:00&2=3&2=2011/02/07%2012:00:00&2=6&2=2011/02/07%2013:00:00&2=9&2=2011/02/07%2014:00:00&2=3&2=2011/02/07%2015:00:00";
		 
			
	 			dailyWebView.loadUrl(url);

			 
		 
			//handler.postDelayed(this, 1000);

		}
	};
	
	// Sort SensorMeasurementStatisticDTO by Sensor id
		 private static Comparator<ESponderEntityDTO> COMPARATOR = new Comparator<ESponderEntityDTO>()
				    {
				     
						@Override
						public int compare(ESponderEntityDTO o1, ESponderEntityDTO o2) {
							 return (int) ((((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)o1).getStatistic())).getSensor().getId()  - (((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)o2).getStatistic())).getSensor().getId() );
						}
				    };
}