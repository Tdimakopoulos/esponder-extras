package com.dynamicview;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateLocationMeasurementStatisticEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;

public class EventsToMessages {

	private LinkedList<JournalMessage> messageList;
	SharedPreferences jernalSettings;
	public LinkedList<JournalMessage> getMessages(Context context) {

		messageList = new LinkedList<JournalMessage>();
		
		jernalSettings = context.getSharedPreferences("jernalSettings",context.MODE_WORLD_READABLE);
		
		Log.d("typeNo1", "Einai = "+jernalSettings.getBoolean("typeNo1", false));
		Log.d("typeNo2", "Einai = "+jernalSettings.getBoolean("typeNo2", false));
		Log.d("typeNo3", "Einai = "+jernalSettings.getBoolean("typeNo3", false));
		Log.d("typeNo4", "Einai = "+jernalSettings.getBoolean("typeNo4", false));	
		
	 	BodyTemperatureSensorDTO bodyTemperatureSensor = new BodyTemperatureSensorDTO();
			bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
			bodyTemperatureSensor.setName("Body Temperature");
			bodyTemperatureSensor.setType("BodyTemp");
			bodyTemperatureSensor.setLabel("BdTemp");
			bodyTemperatureSensor.setTitle("Body Temperature Sensor");
			bodyTemperatureSensor.setId(System.currentTimeMillis());
			bodyTemperatureSensor.setStatus(ResourceStatusDTO.AVAILABLE);
			bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
	 
			ArithmeticSensorMeasurementDTO sensorMeasurement1 = new ArithmeticSensorMeasurementDTO();
			sensorMeasurement1.setMeasurement(new BigDecimal(200));
			sensorMeasurement1.setId(99L);
			sensorMeasurement1.setTimestamp(new Date());
			sensorMeasurement1.setSensor(bodyTemperatureSensor);
		 
			 SensorMeasurementStatisticDTO statisticArethmetic  = new SensorMeasurementStatisticDTO();
			 statisticArethmetic.setStatistic(sensorMeasurement1);
			 statisticArethmetic.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
			 sensorMeasurement1.setId(89L);
			 
			  
			    List<SensorMeasurementStatisticDTO> sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
			    sensorMeasurementStatistic.add(statisticArethmetic);
			    
			    SensorMeasurementStatisticEnvelopeDTO  sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
			    sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
			    sensorMeasurementStatisticEnvelope.setId(System.currentTimeMillis());
			    
			    ActorDTO  subactorDTO= new ActorDTO();
				   	subactorDTO.setId(new Long(2));
				   	subactorDTO.setType("FRC 2222");
				   	subactorDTO.setTitle("FRC #2222");
				   	subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				   	
				    ESponderEvent<SensorMeasurementStatisticEnvelopeDTO> statisticEvent2 = new CreateSensorMeasurementStatisticEvent();
				    statisticEvent2.setEventSeverity(SeverityLevelDTO.SEVERE);
				    statisticEvent2.setEventTimestamp(new Date());
				    statisticEvent2.setEventAttachment(sensorMeasurementStatisticEnvelope);
				    statisticEvent2.setEventSource(subactorDTO);
				    				    
				    ESponderEvent<SensorMeasurementStatisticEnvelopeDTO> statisticEvent3 = new CreateSensorMeasurementStatisticEvent();
				    statisticEvent3.setEventSeverity(SeverityLevelDTO.FATAL);
				    statisticEvent3.setEventTimestamp(new Date());
				    statisticEvent3.setEventAttachment(sensorMeasurementStatisticEnvelope);
				    statisticEvent3.setEventSource(subactorDTO);
				    
					LocationSensorDTO sensorDTO = new LocationSensorDTO(); 
				    sensorDTO.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES);
				    sensorDTO.setName("LPS");
				    sensorDTO.setType("LPS");
				    sensorDTO.setTitle("Local Positioning Sensor");
				    sensorDTO.setId(System.currentTimeMillis());
				    sensorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
			 
		PointDTO pointDTO = new PointDTO();
	    pointDTO.setAltitude(new BigDecimal(220000));
	    pointDTO.setLongitude(new BigDecimal(23762063)); 
	    pointDTO.setLatitude(new BigDecimal(37983578));
	    
	    
	    LocationSensorMeasurementDTO sensorData = new LocationSensorMeasurementDTO();
	    sensorData.setPoint(pointDTO);
	    sensorData.setSensor(sensorDTO); 
	    
	    
	    SensorMeasurementStatisticDTO statistic  = new SensorMeasurementStatisticDTO();
		 statistic.setStatistic(sensorData);
		 statistic.setStatisticType(MeasurementStatisticTypeEnumDTO.LAST);
	    
		   sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
	     sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
	    sensorMeasurementStatistic.add(statistic);
	    sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
	    
	       subactorDTO= new ActorDTO();
		   	subactorDTO.setId(new Long(1));
		   	subactorDTO.setType("FRC 1111");
		   	subactorDTO.setTitle("FRC #1111");
		   	subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
	    
	    
	    ESponderEvent<SensorMeasurementStatisticEnvelopeDTO> statisticEvent = new CreateLocationMeasurementStatisticEvent();
	    statisticEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
	    statisticEvent.setEventTimestamp(new Date());
	    statisticEvent.setEventAttachment(sensorMeasurementStatisticEnvelope);
	    statisticEvent.setEventSource(subactorDTO);

	    
	    
	/*    // ESponder events
        
         subactorDTO = new ActorDTO();
        subactorDTO.setId(new Long(1));
        subactorDTO.setType("FRC 9999");
        subactorDTO.setTitle("FRC #9999");
        subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);

        ArithmeticSensorMeasurementDTO measurmentDTO = new ArithmeticSensorMeasurementDTO();
        measurmentDTO.setId(99L);
        measurmentDTO.setMeasurement(new BigDecimal(1111));
        measurmentDTO.setTimestamp(new Date());
        
        List<SensorMeasurementDTO> measurmentsList = new ArrayList<SensorMeasurementDTO>();
        measurmentsList.add(measurmentDTO);

        SensorMeasurementEnvelopeDTO envelopeDTO = new SensorMeasurementEnvelopeDTO();
        envelopeDTO.setMeasurements(measurmentsList);
        envelopeDTO.setId(System.currentTimeMillis());

        CreateSensorMeasurementEnvelopeEvent event = new CreateSensorMeasurementEnvelopeEvent();
        event.setEventSeverity(SeverityLevelDTO.MEDIUM);
        event.setEventAttachment(envelopeDTO);
        event.setEventSource(subactorDTO);
        event.setEventTimestamp(new Date());*/
	    
	    
/*	    try {
	    	Log.d(" before "," before ");
			Menu.client.call("esponderdb/addESponderEvent", null, statisticEvent);
		    Menu.client.call("esponderdb/addESponderEvent", null, statisticEvent2);
			Menu.client.call("esponderdb/addESponderEvent", null, statisticEvent3);
		   	Log.d(" after "," after ");
			
			
			 ESponderEvent[] allEvents = (ESponderEvent[])Menu.client.call("esponderdb/getESponderEvents", 
		              ESponderEvent[].class, statisticEvent.getClass().getName());
		          for (int i = 0; i < allEvents.length; i++) {
		            System.out.println("All Events FROM DB : " + allEvents[i]);
		          }
			
	    	 Menu.client.call("esponderdb/addESponderEvent", null, statisticEvent2);
	          
	          CreateSensorMeasurementEnvelopeEvent sensor = (CreateSensorMeasurementEnvelopeEvent)Menu.client.call("esponderdb/getESponderEvent", 
	              CreateSensorMeasurementEnvelopeEvent.class, event.getClass().getName(), event.getEventTimestamp().getTime());
	          System.out.println("Event FROM DB : " + sensor);

	          ESponderEvent[] allSensors = (ESponderEvent[])Menu.client.call("esponderdb/getESponderEvents", 
	                  ESponderEvent[].class, sensor.getClass().getName());
	              for (int i = 0; i < allSensors.length; i++) {
	                System.out.println("All Events FROM DB : " + allSensors[i]);
	              }
	          
			
		} catch (JSONRPCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  */
	    
	    
	    
		     	
		    /* 	JournalMessage message = new JournalMessage();
		    	message = new JournalMessage();
				message.setType("SERIOUS");
				message.setMessage("TYPE: "+statisticEvent.getEventAttachment().getMeasurementStatistics().get(0).getStatistic().getSensor().getName()+" \n"+statisticEvent.getJournalMessage());
				message.setEvent(statisticEvent);
				messageList.add(message);
				
				message = new JournalMessage();
				message.setType("MINIMAL");
				message.setMessage("TYPE: " + statisticEvent2.getEventAttachment().getMeasurementStatistics().get(0).getStatistic().getSensor().getName() + " \n" + statisticEvent2.getJournalMessage());
				message.setEvent(statisticEvent2);
				messageList.add(message);

				message = new JournalMessage();
				message.setType("MEDIUM");
				message.setMessage("TYPE: " + statisticEvent3.getEventAttachment().getMeasurementStatistics().get(0).getStatistic().getSensor().getName() + " \n" + statisticEvent3.getJournalMessage());
				message.setEvent(statisticEvent3);
				messageList.add(message);*/
				
				 
		/*JournalMessage message = new JournalMessage();
		message.setMessage("no1 Measurement");
		message.setType("measurement");
		message.setEvent(eventArethmitic);
		if(jernalSettings.getBoolean("typeNo1", false) == true){
		messageList.add(message);
		}
		message = new JournalMessage();
		message.setMessage("no2 alarm");
		message.setType("alarm");
		if(jernalSettings.getBoolean("typeNo2", false) == true){
			messageList.add(message);
			}

		message = new JournalMessage();
		message.setMessage("no3 measurement");
		message.setType("measurement");
		if(jernalSettings.getBoolean("typeNo1", false) == true){
			messageList.add(message);
			}
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("4");
		messageList.add(message);
		
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("5");
		messageList.add(message);
		
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("6");
		messageList.add(message);
		
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("7");
		messageList.add(message);
		
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("8");
		messageList.add(message);
		
		
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("9");
		messageList.add(message);
		
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("10");
		messageList.add(message);
		
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("11");
		messageList.add(message);
		
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("12");
		messageList.add(message);
		
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("13");
		messageList.add(message);
		
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("14");
		messageList.add(message);
		
		message = new JournalMessage();
		message.setType("measurement");
		message.setMessage("15");
		messageList.add(message);*/
		

		return messageList;
	}
}
