package com.dynamicview;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;
import eu.esponder.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;
import eu.esponder.event.model.snapshot.measurement.CreateArithmeticSensorMeasurementEvent;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class OperationsByActor extends Activity {

	private ListView messageListView;
	private OperationsListAdapter adapterList;
	private LinkedList<ActionPartDTO> messageList;
	private String filter = "com.dynamicview.Jernal";
    int count = 5;
	private IncomingReceiver receiver;
	private String actorName;
	private Long actorId;
    private TextView actorTitle;
    Iterator actionPartIterator;
    
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operationsbyactor);
		messageListView = (ListView) findViewById(R.id.MessageListView);
		actorTitle      = (TextView) findViewById(R.id.actorTitle);	 
		messageList = new LinkedList<ActionPartDTO>();
		adapterList = new OperationsListAdapter(this,1);  
		messageListView.setAdapter(adapterList);
		
		Intent intent = getIntent();
	       if (intent != null)
	        {
	        	Bundle b = intent.getBundleExtra("actorInfo");
	        	if (b != null)
	        	{
	        		actorId = b.getLong("actorId") ; 
	        		actorName = b.getString("actorName") ;   
	        		actorTitle.setText(actorName);
	        	} 
	        	 Log.d(" ActoInfo ","actorId = " + actorId + " actorName = " + actorName);
	        }
	       
	     
		
	    receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(filter);
		registerReceiver(receiver, intentFilter);
		
		// <----------------- Start Action No1 ------------------> 
 	  	
		  ActionDTO action = new ActionDTO();  
	      action.setTitle("General No1");
	      action.setId(new Long(4));
	      
	      ActionDTO action2 = new ActionDTO();  
	      action2.setTitle("General No2");
	      action2.setId(new Long(3));
	      
	 	  ReusableResourceDTO reusableResource1 = new ReusableResourceDTO();
	 	  reusableResource1.setType("truck");
	 	  HashSet  reusableResourceSet = new HashSet();
	 	  reusableResourceSet.add(reusableResource1);
	 	  
	 	  ReusableResourceDTO reusableResource2 = new ReusableResourceDTO();
	 	  reusableResource2.setType("red tools");
	 	  HashSet  reusableResourceSet2 = new HashSet();
	 	  reusableResourceSet2.add(reusableResource2);
	 	  
	 	  ConsumableResourceDTO  ConsumableResource1 = new ConsumableResourceDTO();
	 	  ConsumableResource1.setTitle("food packages");
	 	  ConsumableResource1.setQuantity(new BigDecimal(10));
	      HashSet consumableResourceSet = new HashSet(); 
	      consumableResourceSet.add(ConsumableResource1);
	      
	      ConsumableResourceDTO  ConsumableResource2 = new ConsumableResourceDTO();
	      ConsumableResource2.setTitle("telecom lines"); 
	      HashSet consumableResourceSet2 = new HashSet(); 
	      consumableResourceSet2.add(ConsumableResource2);

	 
	      ActionPartObjectiveDTO actionPartObjective1 = new ActionPartObjectiveDTO();
	 	  actionPartObjective1.setConsumableResources(consumableResourceSet);
	 	  HashSet actionPartObjectiveSet = new HashSet(); 
	 	  actionPartObjectiveSet.add(actionPartObjective1);
	 	  
	      ActionPartObjectiveDTO actionPartObjective2 = new ActionPartObjectiveDTO();
	 	  actionPartObjective2.setConsumableResources(consumableResourceSet2);
	 	  HashSet actionPartObjectiveSet2 = new HashSet(); 
	 	  actionPartObjectiveSet2.add(actionPartObjective2);
	 	  	
	      // Action No1
	 	  ActionPartDTO actoinPart1= new ActionPartDTO();
	 	  actoinPart1.setUsedReusuableResources(reusableResourceSet);
	  	  actoinPart1.setUsedConsumableResources(consumableResourceSet);
	  	  actoinPart1.setActionPartObjectives(actionPartObjectiveSet);
	  	  actoinPart1.setActionOperation(ActionOperationEnumDTO.MOVE);
	      ActionPartSnapshotDTO actionsnap1 = new ActionPartSnapshotDTO();
	      actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	      actoinPart1.setSnapshot(actionsnap1);
	      actoinPart1.setId(new Long(1));
	      actoinPart1.setAction(action);
	      
	      // Action No2
	 		  ActionPartDTO actoinPart2= new ActionPartDTO();
	 		  actoinPart2.setId(new Long(2));
	 		  actoinPart2.setUsedReusuableResources(reusableResourceSet2);
	 	  	  actoinPart2.setUsedConsumableResources(consumableResourceSet2);
	 	  	  actoinPart2.setActionPartObjectives(actionPartObjectiveSet2);
	 	  	  actoinPart2.setActionOperation(ActionOperationEnumDTO.FIX);
	 	      ActionPartSnapshotDTO actionsnap2 = new ActionPartSnapshotDTO();
	 	      actionsnap2.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	 	      actoinPart2.setSnapshot(actionsnap1);
	 	     actoinPart2.setAction(action2);
	      
	   // Action No3
	 	  ActionPartDTO actoinPart3= new ActionPartDTO();
	 	 actoinPart3.setUsedReusuableResources(reusableResourceSet);
	 	actoinPart3.setUsedConsumableResources(consumableResourceSet);
	 	actoinPart3.setActionPartObjectives(actionPartObjectiveSet);
	 	actoinPart3.setActionOperation(ActionOperationEnumDTO.MOVE);
	        actionsnap1 = new ActionPartSnapshotDTO();
	      actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	      actoinPart3.setSnapshot(actionsnap1);
	      actoinPart3.setId(new Long(3));
	      actoinPart3.setAction(action);
	      
	   // Action No4
	 	  ActionPartDTO actoinPart4= new ActionPartDTO();
	 	 actoinPart4.setUsedReusuableResources(reusableResourceSet);
	 	actoinPart4.setUsedConsumableResources(consumableResourceSet);
	 	actoinPart4.setActionPartObjectives(actionPartObjectiveSet);
	 	actoinPart4.setActionOperation(ActionOperationEnumDTO.MOVE);
	       actionsnap1 = new ActionPartSnapshotDTO();
	       actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	      actoinPart4.setSnapshot(actionsnap1);
	      actoinPart4.setId(new Long(4));
	      actoinPart4.setAction(action2);
	       
	   // Action No4
	 	  ActionPartDTO actoinPart5= new ActionPartDTO();
	 	 actoinPart5.setUsedReusuableResources(reusableResourceSet);
	 	actoinPart5.setUsedConsumableResources(consumableResourceSet);
	 	actoinPart5.setActionPartObjectives(actionPartObjectiveSet);
	 	actoinPart5.setActionOperation(ActionOperationEnumDTO.MOVE);
	       actionsnap1 = new ActionPartSnapshotDTO();
	       actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
	       actoinPart5.setSnapshot(actionsnap1);
	       actoinPart5.setId(new Long(5));
	       actoinPart5.setAction(action);
	       
		      // Action No3
		 	  ActionPartDTO actoinPart6= new ActionPartDTO();
		 	 actoinPart6.setUsedReusuableResources(reusableResourceSet);
		 	actoinPart6.setUsedConsumableResources(consumableResourceSet);
		 	actoinPart6.setActionPartObjectives(actionPartObjectiveSet);
		 	actoinPart6.setActionOperation(ActionOperationEnumDTO.MOVE);
		        actionsnap1 = new ActionPartSnapshotDTO();
		      actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
		      actoinPart6.setSnapshot(actionsnap1);
		      actoinPart6.setId(new Long(6));
		      actoinPart6.setAction(action2);
	  	  
	  
	 	  	

	      HashSet<ActionPartDTO> actionParts = new HashSet<ActionPartDTO>();
	      actionParts.add(actoinPart1);
	      actionParts.add(actoinPart3);
        actionParts.add(actoinPart5);


	      action.setActionParts(actionParts); 
	      
	      HashSet<ActionPartDTO> actionParts2 = new HashSet<ActionPartDTO>();
	      actionParts2.add(actoinPart2);
	      actionParts2.add(actoinPart4);
        actionParts2.add(actoinPart6);


	      action2.setActionParts(actionParts2); 
	 	  
	      HashSet<ActionPartDTO>  actionPartsSet =  (HashSet<ActionPartDTO>) action.getActionParts();
	      actionPartIterator = actionPartsSet.iterator(); 
	      while (actionPartIterator.hasNext()){
	    	  ActionPartDTO actionPartTemp =  ((ActionPartDTO) actionPartIterator.next());
	    	  messageList.add(actionPartTemp);
	      	  } 
	 	 
	     	// <----------------- End Action No1 ------------------> 
		
		
	     actionPartsSet =  (HashSet<ActionPartDTO>) action2.getActionParts();
	      actionPartIterator = actionPartsSet.iterator(); 
	      while (actionPartIterator.hasNext()){
	    	  ActionPartDTO actionPartTemp =  ((ActionPartDTO) actionPartIterator.next());
	    	  messageList.add(actionPartTemp);
	      	  }     
	        	Collections.sort(messageList, COMPARATOR);
	     
 		
		ShowOrder((LinkedList<ActionPartDTO>) messageList);
		
    }

	public void ShowOrder(final LinkedList<ActionPartDTO> operationMessage) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.addOrder(operationMessage);
				adapterList.notifyDataSetChanged();
			}
		});
	}

	public void ClearOrder() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.clear();
				adapterList.notifyDataSetChanged();
			}
		});
	}
	
/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menujernal, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
			Intent jernalSettings = new Intent(Jernal.this, JernalSettings.class);
			startActivity(jernalSettings);
			finish();
			break;
		}
		return true;
	}*/
	
	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			
/*
		 	BodyTemperatureSensorDTO bodyTemperatureSensor = new BodyTemperatureSensorDTO();
				bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnum.DEGREES_CELCIUS);
				bodyTemperatureSensor.setName("BodyTemp");
				bodyTemperatureSensor.setType("BodyTemp");
				bodyTemperatureSensor.setLabel("BdTemp");
				bodyTemperatureSensor.setTitle("Body Temperature Sensor");
				bodyTemperatureSensor.setId(System.currentTimeMillis());
				bodyTemperatureSensor.setStatus(ResourceStatusDTO.AVAILABLE);
				
				ArithmeticSensorMeasurementDTO sensorMeasurement1 = new ArithmeticSensorMeasurementDTO();
				sensorMeasurement1.setMeasurement(new BigDecimal(200));
				sensorMeasurement1.setTimestamp(new Date());
			 
				
				    CreateArithmeticSensorMeasurementEvent eventArethmitic = new CreateArithmeticSensorMeasurementEvent();
				    eventArethmitic.setEventSeverity(SeverityLevelDTO.MEDIUM);
				    eventArethmitic.setEventTimestamp(new Date());
			     	eventArethmitic.setEventAttachment(sensorMeasurement1);
			     	eventArethmitic.setEventSource(bodyTemperatureSensor);
			     	
			    	JernalMessage message = new JernalMessage();
					message.setMessage("Measurement apo Listener");
					message.setType("measurement");
					message.setEvent(eventArethmitic);
			
			
					tempList.addFirst(message);
			adapterList.notifyDataSetChanged();
			*/
		

		}

	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

	// Sort ActionPartDTO by id
	 private static Comparator<ActionPartDTO> COMPARATOR = new Comparator<ActionPartDTO>()
			    {
			        public int compare(ActionPartDTO o1, ActionPartDTO o2)
			        {
			            return (int) (o1.getAction().getId() - o2.getAction().getId());
			        }
			    };
}