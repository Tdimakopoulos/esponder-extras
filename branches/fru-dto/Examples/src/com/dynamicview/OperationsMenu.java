package com.dynamicview;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost; 

public class OperationsMenu extends TabActivity  {

	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.opmenu);

        TabHost tabHost = this.getTabHost();  
	    TabHost.TabSpec tab;  
	    Intent intent; 

	    intent = new Intent().setClass(this, OperationsActorMenu.class);
	    tab = tabHost.newTabSpec("Actors").setIndicator("Actors", null)
	                  .setContent(intent);
	    tabHost.addTab(tab);
	    
	    intent = new Intent().setClass(this, OperationsActionsMenu.class);
	    tab = tabHost.newTabSpec("Actions").setIndicator("Actions",null)
	                  .setContent(intent);
	    tabHost.addTab(tab);

	}

}