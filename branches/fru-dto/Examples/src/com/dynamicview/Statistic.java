package com.dynamicview;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;

public class Statistic {

	String from, to, measurementTitle;
 	Class<? extends SensorDTO> classObject;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getMeasurementTitle() {
		return measurementTitle;
	}

	public void setMeasurementTitle(String measurementTitle) {
		this.measurementTitle = measurementTitle;
	}
	
	public Class getClassName() {
		return classObject;
	}

	public void setClassName(Class classObject) {
		this.classObject = classObject;
	}
	
}
