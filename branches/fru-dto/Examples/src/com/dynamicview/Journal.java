package com.dynamicview;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;
import eu.esponder.event.model.snapshot.measurement.CreateArithmeticSensorMeasurementEvent;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class Journal extends Activity {

	private ListView messageListView;
	private Button getMore; 
	private JournalListAdapter adapterList;
	private LinkedList<JournalMessage> messageList,tempList;
	private String filter = "com.dynamicview.Jernal";
    int count = 5;
	private IncomingReceiver receiver;
	//SharedPreferences jernalSettings;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.journal);
		messageListView = (ListView) findViewById(R.id.MessageListView);
	    getMore = (Button)this.findViewById(R.id.more);
		messageList = new LinkedList<JournalMessage>();
		tempList = new LinkedList<JournalMessage>();
		adapterList = new JournalListAdapter(this);  
		messageListView.setAdapter(adapterList);
		 
		//jernalSettings = this.getSharedPreferences("jernalSettings",MODE_WORLD_READABLE);
		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(filter);
		registerReceiver(receiver, intentFilter);
		
		EventsToMessages eventsToMessages = new EventsToMessages();
		messageList = eventsToMessages.getMessages(this);
		
		if(messageList.size()>5){
		     for(int i=0;i<count;i++){
			    tempList.add(messageList.get(i));
		       }  
		     ShowOrder((LinkedList<JournalMessage>) tempList);
		  }else{
			 ShowOrder((LinkedList<JournalMessage>) messageList);
			 getMore.setVisibility(View.GONE);
		  }
		
		 
	/*	Thread s = new Sender(this);
		s.start();*/
		
		getMore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            
            	AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
				  dialog.setTitle("Title");
			      dialog.setMessage("No other Events");
			      dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			          public void onClick(DialogInterface dialog, int id) {
			          }
			      });
			    boolean isEnd = false;   
            	count = count+5;
            	ClearOrder();
        		for(int i=0;i<count;i++){
        			if(i<messageList.size()){
        				tempList.add(messageList.get(i));
        			}else{
        				isEnd = true;
        				break;
        			}
        			 
        		}
        		
        		if(isEnd){
        		dialog.show();
        		}
        		else{
        			ShowOrder((LinkedList<JournalMessage>) tempList);
        			messageListView.smoothScrollToPosition(tempList.size()-1);
        		}
            }

        });

	}

	public void ShowOrder(final LinkedList<JournalMessage> journalMessage) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.addOrder(journalMessage);
				adapterList.notifyDataSetChanged();
			}
		});
	}

	public void ClearOrder() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.clear();
				adapterList.notifyDataSetChanged();
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menujernal, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
			Intent jernalSettings = new Intent(Journal.this, JournalSettings.class);
			startActivity(jernalSettings);
			finish();
			break;
		}
		return true;
	}
	
	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			

		 	BodyTemperatureSensorDTO bodyTemperatureSensor = new BodyTemperatureSensorDTO();
				bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
				bodyTemperatureSensor.setName("BodyTemp");
				bodyTemperatureSensor.setType("BodyTemp");
				bodyTemperatureSensor.setLabel("BdTemp");
				bodyTemperatureSensor.setTitle("Body Temperature Sensor");
				bodyTemperatureSensor.setId(System.currentTimeMillis());
				bodyTemperatureSensor.setStatus(ResourceStatusDTO.AVAILABLE);
				
				ArithmeticSensorMeasurementDTO sensorMeasurement1 = new ArithmeticSensorMeasurementDTO();
				sensorMeasurement1.setMeasurement(new BigDecimal(200));
				sensorMeasurement1.setTimestamp(new Date());
			 
				
				    CreateArithmeticSensorMeasurementEvent eventArethmitic = new CreateArithmeticSensorMeasurementEvent();
				    eventArethmitic.setEventSeverity(SeverityLevelDTO.MEDIUM);
				    eventArethmitic.setEventTimestamp(new Date());
			     	eventArethmitic.setEventAttachment(sensorMeasurement1);
			     	eventArethmitic.setEventSource(bodyTemperatureSensor);
			     	
			    	JournalMessage message = new JournalMessage();
					message.setMessage("Measurement apo Listener");
					message.setType("measurement");
					message.setEvent(eventArethmitic);
			
			
					tempList.addFirst(message);
			adapterList.notifyDataSetChanged();
			
		

		}

	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

}