package eu.esponder.ESponderDriver.application;

import java.math.BigDecimal;
import java.util.Timer;
import java.util.concurrent.CopyOnWriteArrayList;

import android.app.Application;
import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import eu.esponder.ESponderDriver.db.DBAdapter;
import eu.esponder.ESponderDriver.gps.ESponderLocationListener;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.event.datafusion.query.ESponderDFQueryRequestEvent;
import eu.esponder.event.datafusion.query.ESponderDFQueryResponseEvent;
import eu.esponder.event.mobile.MobileFileNotifierEvent;
import eu.esponder.event.mobile.MobileMessageEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.crisis.action.CreateActionPartEvent;
import eu.esponder.event.model.crisis.action.UpdateActionEvent;
import eu.esponder.event.model.crisis.action.UpdateActionPartEvent;
import eu.esponder.event.model.datafusion.DatafusionResultsGeneratedEvent;
import eu.esponder.event.model.login.LoginEsponderUserEvent;
import eu.esponder.event.model.login.LoginRequestEvent;
import eu.esponder.event.model.snapshot.action.CreateActionPartSnapshotEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionPartSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;

public class GlobalVar extends Application {


	/*
	 * Intent actions to be defined here
	 */

	/**************************************************************************************************************/

	public static DBAdapter db;
	
	public static Timer pcdSensorTimer;
	
	public static Timer mapUpdateTimer;
	
	public static LocationManager locationManager;
	
	public static LocationListener locationListener;
	
	/**************************************************************************************************************/
	
	//	PROSYST - EXUS IPC intents

	public static final String INTENT_CATEGORY = "org.osgi.service.event.EventAdmin";

	public static final String LOGIN_REQUEST_INTENT_ACTION = LoginRequestEvent.class.getCanonicalName();

	public static final String LOGIN_RESPONSE_INTENT_ACTION = LoginEsponderUserEvent.class.getCanonicalName();

	public static final String MAP_REQUEST_HIERARCHY = ESponderDFQueryRequestEvent.class.getCanonicalName();

	public static final String MAP_RESPONSE_HIERARCHY = ESponderDFQueryResponseEvent.class.getCanonicalName();

	public static final String BIOMED_SENSOR_MEASUREMENT = CreateSensorMeasurementEnvelopeEvent.class.getCanonicalName();
	
	public static final String CREATE_ACTION_EVENT = CreateActionEvent.class.getCanonicalName();
	
	public static final String CREATE_ACTIONPART_EVENT = CreateActionPartEvent.class.getCanonicalName();
	
	public static final String CREATE_ACTION_SNAPSHOT_EVENT = CreateActionSnapshotEvent.class.getCanonicalName();
	
	public static final String CREATE_ACTIONPART_SNAPSHOT_EVENT = CreateActionPartSnapshotEvent.class.getCanonicalName();
	
	public static final String UPDATE_ACTION_EVENT = UpdateActionEvent.class.getCanonicalName();
	
	public static final String UPDATE_ACTIONPART_EVENT = UpdateActionPartEvent.class.getCanonicalName();
	
	public static final String UPDATE_ACTION_SNAPSHOT_EVENT = UpdateActionSnapshotEvent.class.getCanonicalName();
	
	public static final String UPDATE_ACTIONPART_SNAPSHOT_EVENT = UpdateActionPartSnapshotEvent.class.getCanonicalName();
	
	public static final String INCOMING_ALARM = DatafusionResultsGeneratedEvent.class.getCanonicalName();
	
	public static final String INCOMING_MESSAGE = MobileMessageEvent.class.getCanonicalName();
	
	public static final String INCOMING_FILE = MobileFileNotifierEvent.class.getCanonicalName();

	/**************************************************************************************************************/

	//	EXUS INNER INTENTS	@Dimitris

	public static final String INNER_LOGIN_RESPONSE_INTENT_APPROVED = "eu.esponder.login.approved";

	public static final String INNER_LOGIN_RESPONSE_INTENT_DENIED = "eu.esponder.login.denied";

	public static final String INNER_LOGIN_RESPONSE_INTENT_ERROR = "eu.esponder.login.error";

	public static final String INNER_MAP_RESPONSE_HIERARCHY = "eu.esponder.map.hierarchy.received";

	public static final String INNER_MAP_RESPONSE_HIERARCHY_ERROR = "eu.esponder.map.hierarchy.error";
	
	public static final String INNER_MAP_UPDATE = "eu.esponder.map.hierarchy.received";
	
	public static final String INNER_SENSORS_RECEIVED = "eu.esponder.sensors.received";
	
	public static final String INNER_ALARM_RECEIVED = "eu.esponder.alarms.received";
	
	public static final String INNER_ROUTE_RECEIVED = "eu.esponder.route.received";
	
	public static final String NOTIFICATION_POPUP = "eu.esponder.notification.received";
	
	public static final String FILE_POPUP = "eu.esponder.file.received";

	/**************************************************************************************************************/
	
	// Shared Preferences Named Variables
	
	public static final String spName = "eu.esponder.ESponder";
	
	public static final String spCrisisContextVar = "currentCrisisContext";
	
	public static final String spESponderUser = "esponderUser";
	
	public static final String spESponderUserRasIP = "esponderUserRasIP";
	
	public static final String spESponderUserID = "esponderUserID";
	
	public static final String spESponderFullName = "esponderUserFullName";
	
	/**************************************************************************************************************/

	// EXUS	-->	Global Variables in app memory
	// were private, turned into protected to test the garbage collection mechanism of android
	
	protected boolean loggedIn = false;
	
	protected boolean loginBeingProcessed = false;

	protected boolean chief = true;
	
	protected Boolean mapDataProcessActive = false;

	protected CopyOnWriteArrayList<BigDecimal> temperatureMeasurements = new CopyOnWriteArrayList<BigDecimal>();

	protected CopyOnWriteArrayList<BigDecimal> activityMeasurements = new CopyOnWriteArrayList<BigDecimal>();

	protected CopyOnWriteArrayList<BigDecimal> breathrateMeasurements = new CopyOnWriteArrayList<BigDecimal>();

	protected CopyOnWriteArrayList<BigDecimal> heartbeatMeasurements = new CopyOnWriteArrayList<BigDecimal>();

	protected CopyOnWriteArrayList<BigDecimal> gasMeasurements = new CopyOnWriteArrayList<BigDecimal>();

	protected CopyOnWriteArrayList<BigDecimal> envTempMeasurements = new CopyOnWriteArrayList<BigDecimal>();

	protected CopyOnWriteArrayList<PointDTO> locationMeasurements = new CopyOnWriteArrayList<PointDTO>();

	protected CopyOnWriteArrayList<PointDTO> lpsMeasurements = new CopyOnWriteArrayList<PointDTO>();

	/**************************************************************************************************************/

	@Override
	public void onCreate() {
		super.onCreate();
		GlobalVar.db = new DBAdapter(getApplicationContext());
		db.open();
		
		locationManager = (LocationManager) (GlobalVar.this).getSystemService(Context.LOCATION_SERVICE);
		locationListener = new ESponderLocationListener(GlobalVar.this);
	}

	/**************************************************************************************************************/

	public boolean isChief() {
		return chief;
	}

	public void setChief(boolean chief) {
		this.chief = chief;
	}

	public CopyOnWriteArrayList<BigDecimal> getTemperatureMeasurements() {
		return temperatureMeasurements;
	}

	public void setTemperatureMeasurements(CopyOnWriteArrayList<BigDecimal> temperatureMeasurements) {
		this.temperatureMeasurements = temperatureMeasurements;
	}

	public CopyOnWriteArrayList<BigDecimal> getActivityMeasurements() {
		return activityMeasurements;
	}

	public void setActivityMeasurements(CopyOnWriteArrayList<BigDecimal> activityMeasurements) {
		this.activityMeasurements = activityMeasurements;
	}

	public CopyOnWriteArrayList<BigDecimal> getBreathrateMeasurements() {
		return breathrateMeasurements;
	}

	public void setBreathrateMeasurements(CopyOnWriteArrayList<BigDecimal> breathrateMeasurements) {
		this.breathrateMeasurements = breathrateMeasurements;
	}

	public CopyOnWriteArrayList<BigDecimal> getHeartbeatMeasurements() {
		return heartbeatMeasurements;
	}

	public void setHeartbeatMeasurements(CopyOnWriteArrayList<BigDecimal> heartbeatMeasurements) {
		this.heartbeatMeasurements = heartbeatMeasurements;
	}

	public CopyOnWriteArrayList<BigDecimal> getEnvTempMeasurements() {
		return envTempMeasurements;
	}

	public void setEnvTempMeasurements(
			CopyOnWriteArrayList<BigDecimal> envTempMeasurements) {
		this.envTempMeasurements = envTempMeasurements;
	}

	public CopyOnWriteArrayList<BigDecimal> getGasMeasurements() {
		return gasMeasurements;
	}

	public void setGasMeasurements(CopyOnWriteArrayList<BigDecimal> gasMeasurements) {
		this.gasMeasurements = gasMeasurements;
	}

	public CopyOnWriteArrayList<PointDTO> getLocationMeasurements() {
		return locationMeasurements;
	}

	public void setLocationMeasurements(
			CopyOnWriteArrayList<PointDTO> locationMeasurements) {
		this.locationMeasurements = locationMeasurements;
	}

	public CopyOnWriteArrayList<PointDTO> getLpsMeasurements() {
		return lpsMeasurements;
	}

	public void setLpsMeasurements(CopyOnWriteArrayList<PointDTO> lpsMeasurements) {
		this.lpsMeasurements = lpsMeasurements;
	}
	
	public Boolean getMapDataProcessActive() {
		return mapDataProcessActive;
	}

	public void setMapDataProcessActive(Boolean mapDataProcessActive) {
		this.mapDataProcessActive = mapDataProcessActive;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	public boolean isLoginBeingProcessed() {
		return loginBeingProcessed;
	}

	public void setLoginBeingProcessed(boolean loginBeingProcessed) {
		this.loginBeingProcessed = loginBeingProcessed;
	}

}