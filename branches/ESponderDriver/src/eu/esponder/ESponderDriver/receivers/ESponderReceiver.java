/**
 * 
 */
package eu.esponder.ESponderDriver.receivers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.util.Log;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.ESponderDriver.dialogs.NotificationsDialog;
import eu.esponder.ESponderDriver.enums.SensorTypeEnum;
import eu.esponder.ESponderDriver.service.DownloaderService;
import eu.esponder.ESponderDriver.soapservices.ESponderRestClient;
import eu.esponder.ESponderDriver.soapservices.ESponderRestClient.RequestMethod;
import eu.esponder.ESponderDriver.utils.ESponderUtils;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO;
import eu.esponder.dto.model.crisis.alternative.FRTeamALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCEocALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.sensor.ActivitySensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BreathRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.EnvironmentTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.GasSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HeartBeatRateSensorDTO;
import eu.esponder.dto.model.datafusion.DatafusionResultsDTO;
import eu.esponder.event.datafusion.query.ESponderDFQueryResponseEvent;
import eu.esponder.event.mobile.MobileFileNotifierEvent;
import eu.esponder.event.mobile.MobileMessageEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.crisis.action.CreateActionPartEvent;
import eu.esponder.event.model.crisis.action.UpdateActionEvent;
import eu.esponder.event.model.crisis.action.UpdateActionPartEvent;
import eu.esponder.event.model.datafusion.DatafusionResultsGeneratedEvent;
import eu.esponder.event.model.login.LoginEsponderUserEvent;
import eu.esponder.event.model.snapshot.action.CreateActionPartSnapshotEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionPartSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionSnapshotEvent;

/**
 * @author dkar
 *
 */
public class ESponderReceiver extends BroadcastReceiver {

	protected boolean mapInitialized = false;
	private ObjectMapper mapper;

	/* (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
	public void onReceive(Context context, Intent intent) {

		// All incoming and outgoing intents have the same category attached
		if(intent.hasCategory(GlobalVar.INTENT_CATEGORY)) {
			// case for login response
			if(intent.getAction().equalsIgnoreCase(GlobalVar.LOGIN_RESPONSE_INTENT_ACTION)) {
				if(!((GlobalVar)context.getApplicationContext()).isLoginBeingProcessed()) {
					((GlobalVar)context.getApplicationContext()).setLoginBeingProcessed(true);
					String jsonObj = intent.getStringExtra("event");
					Log.e("", jsonObj);
					processLoginResponse(context, jsonObj);
				}

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.MAP_RESPONSE_HIERARCHY)) {
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn()) {
					String jsonObj = intent.getStringExtra("event");

					createThreadForMapUpdate(context, jsonObj);
				}
				else
					Log.e("CANNOT UPDATE MAP", "USER NOT LOGGED IN");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.BIOMED_SENSOR_MEASUREMENT)) {
				if(((GlobalVar)context.getApplicationContext()).isLoggedIn()) {
					Bundle b = intent.getBundleExtra("SensorMeasurementBundle");
					if(b !=null) {
						String sensorType = b.getString("sensorType");
						Double sensorValue = b.getDouble("sensorValue");
						Long timeStamp = b.getLong("timeStamp");
						processMeasurementAlt(context, sensorType, sensorValue, timeStamp);	
					}
				}
				else
					Log.e("MEASUREMENT PROCESS", "NOT WORKING, APPEARS AS NOT LOGGED IN");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INCOMING_ALARM)) {
				if(intent.hasExtra("event")) {
					processAlarmRelatedEvents(context, intent.getStringExtra("event"));
				}
				else
					Log.e("RECEIVER","NO ATTACHMENT FOR ALARMS");

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INCOMING_MESSAGE)) {
				if(intent.hasExtra("event")) {
					processIncomingMessageEvents(context, intent.getStringExtra("event"));
				}
				else
					Log.e("RECEIVER","NO ATTACHMENT FOR NOTIFICATION");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INCOMING_FILE)) {
				if(intent.hasExtra("event")) {
					Log.e("RECEIVER","FILE NOTIFICATION RECEIVED");
					processIncomingFileEvents(context, intent.getStringExtra("event"));
				}
				else
					Log.e("RECEIVER","NO ATTACHMENT FOR INCOMING FILE");
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.CREATE_ACTION_EVENT)) {
				Log.e("RECEIVER","CREATE_ACTION_EVENT RECEIVED");
				processActionRelatedEvents(intent.getStringExtra("event"), CreateActionEvent.class);

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.CREATE_ACTIONPART_EVENT)) {
				Log.e("RECEIVER","CREATE_ACTIONPART_EVENT RECEIVED");
				processActionRelatedEvents(intent.getStringExtra("event"), CreateActionPartEvent.class);

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.CREATE_ACTION_SNAPSHOT_EVENT)) {
				Log.e("RECEIVER","CREATE_ACTION_SNAPSHOT_EVENT RECEIVED");
				processActionRelatedEvents(intent.getStringExtra("event"), CreateActionSnapshotEvent.class);

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.CREATE_ACTIONPART_SNAPSHOT_EVENT)) {
				Log.e("RECEIVER","CREATE_ACTIONPART_SNAPSHOT_EVENT RECEIVED");
				processActionRelatedEvents(intent.getStringExtra("event"), CreateActionPartSnapshotEvent.class);

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.UPDATE_ACTION_EVENT)) {
				Log.e("RECEIVER","UPDATE_ACTION_EVENT RECEIVED");
				processActionRelatedEvents(intent.getStringExtra("event"), UpdateActionEvent.class);

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.UPDATE_ACTIONPART_EVENT)) {
				Log.e("RECEIVER","UPDATE_ACTIONPART_EVENT RECEIVED");
				processActionRelatedEvents(intent.getStringExtra("event"), UpdateActionPartEvent.class);

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.UPDATE_ACTION_SNAPSHOT_EVENT)) {
				Log.e("RECEIVER","UPDATE_ACTION_SNAPSHOT_EVENT RECEIVED");
				processActionRelatedEvents(intent.getStringExtra("event"), UpdateActionSnapshotEvent.class);

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.UPDATE_ACTIONPART_SNAPSHOT_EVENT)) {
				Log.e("RECEIVER","UPDATE_ACTIONPART_SNAPSHOT_EVENT RECEIVED");
				processActionRelatedEvents(intent.getStringExtra("event"), UpdateActionPartSnapshotEvent.class);
			}

		}

	}

	//*******************************************************************************************************************************

	private boolean processIncomingLoginEvent(Context context, LoginEsponderUserEvent loginResponseEvent) {
		// Process the incoming event details and store them in memory

		Log.e("RECEIVER FOR LOGIN", "iROLE = " + String.valueOf(loginResponseEvent.getiRole()));

		if(loginResponseEvent.getEventAttachment() != null) {

			SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			SharedPreferences.Editor spEditor = prefs.edit();
			//TODO Remove the user if not needed to be stored completely
			spEditor.putString(GlobalVar.spESponderUserRasIP, loginResponseEvent.getRasberryip());
			spEditor.putLong(GlobalVar.spESponderUserID, loginResponseEvent.getEventAttachment().getId());
			PersonnelDTO personnel = loginResponseEvent.getEventAttachment().getPersonnel();
			if(personnel != null)
				spEditor.putString(GlobalVar.spESponderFullName, personnel.getLastName()+" "+personnel.getFirstName());
			spEditor.commit();
		}

		//	Criteria for FRC or FR has changed
		if(loginResponseEvent.getiRole() == 0) {
			//  FRC
			((GlobalVar)context.getApplicationContext()).setChief(true);
			return true;
		}
		else if(loginResponseEvent.getiRole() == 1) {
			//	FR
			((GlobalVar)context.getApplicationContext()).setChief(false);
			return true;
		}
		else if(loginResponseEvent.getiRole() == -1 ) {
			//	ERROR
			return false;
		}
		else
			return false;

	}

	//*******************************************************************************************************************************

	// FIXME remove this when communications are working
	private String retrieveFileContents(String filename) {

		if(filename != null) {
			File sdcard = Environment.getExternalStorageDirectory();
			//Get the text file
			File file = new File(sdcard,filename);

			//Read text from file
			StringBuilder sbuilder = new StringBuilder();

			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;

				while ((line = br.readLine()) != null) {
					sbuilder.append(line);
				}
				br.close();
			}
			catch (IOException e) {
				Log.d("FILE_HANDLER", "Error occurred while opening file");
				e.printStackTrace();
			}
			String skey = sbuilder.toString();

			return skey;

		}
		else
			return null;
	}


	//*******************************************************************************************************************************

	private void processLoginResponse(final Context context, final String loginInfo) {

		final String jsonObj = loginInfo;

		class processIncomingLoginResponse implements Runnable {

			@Override
			public void run() {

				LoginEsponderUserEvent loginResponseEvent = null;
				mapper = new ObjectMapper();
				mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				try {
					loginResponseEvent = mapper.readValue(jsonObj, LoginEsponderUserEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(loginResponseEvent != null) {

					if(loginResponseEvent.getIMEI().equalsIgnoreCase(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId())) {

						Log.e("LoginResponseEvent Received","FAMILIAR IMEI, PROCESSING");

						// Process the event contents
						if(processIncomingLoginEvent(context, loginResponseEvent)) {
							// Successful Login
							((GlobalVar)context.getApplicationContext()).setLoggedIn(true);
							Log.e("LOGIN EVENT DECODED", "SUCCESSFUL LOGIN");


							//FIXME Here we activate the gps mechanism in order to constantly receive location changes
							activateGPSMeasurements(context);

							Intent innerIntent = new Intent(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_APPROVED);
							context.sendBroadcast(innerIntent);
						}
						else {
							// Unsuccessful Login
							Log.e("LOGIN EVENT DECODED", "LOGIN DENIED");
							Intent innerIntent = new Intent(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_DENIED);
							context.sendBroadcast(innerIntent);
						}
					}
					else
						Log.e("LoginResponseEvent Received","UNKNOWN IMEI, SKIPPING");
				}
				else {
					Intent innerIntent = new Intent(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_ERROR);
					context.sendBroadcast(innerIntent);
				}
			}
		}

		Thread thread = new Thread(new processIncomingLoginResponse());
		thread.start();

	}


	//*******************************************************************************************************************************

	private void processMeasurementAlt(final Context context, final String sensorType, final Double sensorValue, final Long timeStamp) {

		class processIncomingMeasurement implements Runnable {

			@Override
			public void run() {

				if(sensorType.equalsIgnoreCase(HeartBeatRateSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.HeartBeatRateSensor.getType(), sensorValue, timeStamp);
					Log.e("BIOMED SENSOR JSON", "HBRate "+sensorValue);
				}
				else if(sensorType.equalsIgnoreCase(ActivitySensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.ActivitySensor.getType(), sensorValue, timeStamp);
					Log.e("BIOMED SENSOR JSON", "Activity "+sensorValue);
				}
				else if(sensorType.equalsIgnoreCase(BreathRateSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.BreathRateSensor.getType(), sensorValue, timeStamp);
					Log.e("BIOMED SENSOR JSON", "BRRate "+sensorValue);
				}
				else if(sensorType.equalsIgnoreCase(BodyTemperatureSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.BodyTemperatureSensor.getType(), sensorValue, timeStamp);
					Log.e("BIOMED SENSOR JSON", "BodyTempSensor "+sensorValue);
				}
				else if(sensorType.equalsIgnoreCase(GasSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.MethaneSensor.getType(), sensorValue, timeStamp);
					Log.e("BIOMED SENSOR JSON", "MethaneSensor "+sensorValue);
				}
				else if(sensorType.equalsIgnoreCase(GasSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.CarbonMonoxideSensor.getType(), sensorValue, timeStamp);
					Log.e("BIOMED SENSOR JSON", "CarbonMonoxideSensor "+sensorValue);
				}
				else if(sensorType.equalsIgnoreCase(GasSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.CarbonDioxideSensor.getType(), sensorValue, timeStamp);
					Log.e("BIOMED SENSOR JSON", "CarbonDioxideSensor "+sensorValue);
				}
				else if(sensorType.equalsIgnoreCase(GasSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.OxygenSensor.getType(), sensorValue, timeStamp);
					Log.e("BIOMED SENSOR JSON", "OxygenSensor "+sensorValue);
				}
				else if(sensorType.equalsIgnoreCase(GasSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.HydrogenSulfideSensor.getType(), sensorValue, timeStamp);
					Log.e("BIOMED SENSOR JSON", "HydrogenSulfideSensor "+sensorValue);
				}
				else if(sensorType.equalsIgnoreCase(EnvironmentTemperatureSensorDTO.class.getCanonicalName())) {
					SensorDataSender(context, SensorTypeEnum.EnvTemperatureSensor.getType(), sensorValue, timeStamp);
					Log.e("BIOMED SENSOR JSON", "EnvTempSensor "+sensorValue);
				}
				else
					Log.e("INCOMING SENSOR", "UNKNOWN SENSOR READING, SKIPPING");

			}
		}
		Thread thread = new Thread(new processIncomingMeasurement());
		thread.start();
	}

	//*******************************************************************************************************************************

	private void SensorDataSender(final Context context, int st, Double sv, final Long timestamp) {

		final int sensorType = st;
		final Double sensorValue = sv;

		class SensorSender implements Runnable {

			@Override
			public void run() {
				SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
				Long currentUserID = prefs.getLong(GlobalVar.spESponderUserID, -1);
				String rasIP = prefs.getString(GlobalVar.spESponderUserRasIP, "");
				if(currentUserID != -1) {

					ESponderRestClient client = new ESponderRestClient(context, "http://"+rasIP+"/addfr/"
							+ currentUserID +"/" + String.valueOf(sensorType)+ "/" + String.valueOf(sensorValue)
							+ "/" + String.valueOf(timestamp));
					
					
					Log.e("Command To Beagle", "http://"+rasIP+"/addfr/"
							+ currentUserID +"/" + String.valueOf(sensorType)+ "/" + String.valueOf(sensorValue)
							+ "/" + String.valueOf(timestamp));
					try {
						client.Execute(RequestMethod.POST);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		}

		Thread sensorSender = new Thread(new SensorSender());
		sensorSender.start();
	}

	//*******************************************************************************************************************************

	private void createThreadForMapUpdate(final Context context, final String jsonObj) {

		class MapUpdateThread implements Runnable {

			@Override
			public void run() {

				ESponderDFQueryResponseEvent dfQueryResponseEvent = null;
				mapper = new ObjectMapper();
				mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
				mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				try {
					dfQueryResponseEvent = mapper.readValue(jsonObj, ESponderDFQueryResponseEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(dfQueryResponseEvent != null) {

					if(dfQueryResponseEvent.getIMEI().equalsIgnoreCase(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId())) {

						if(processIncomingMapData(context, dfQueryResponseEvent)) {
							((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(false);
							if(mapInitialized) {
								Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_UPDATE);
								context.sendBroadcast(hierarchyIntent);
							}
							else {
								mapInitialized = true;
								Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY);
								context.sendBroadcast(hierarchyIntent);
							}
						}
						else {
							((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(false);
							Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR);
							context.sendBroadcast(hierarchyIntent);
						}
					}
					else
						Log.e("MapUpdateEvent Received","UNKNOWN IMEI, SKIPPING");
				}
				else {
					((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(false);
					Intent hierarchyIntent = new Intent(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR);
					context.sendBroadcast(hierarchyIntent);
				}
			}
		}

		if(!((GlobalVar)context.getApplicationContext()).getMapDataProcessActive()) {

			((GlobalVar)context.getApplicationContext()).setMapDataProcessActive(true);
			Thread thread = new Thread(new MapUpdateThread());
			thread.start();
			Log.e("MAP UPDATE PROCESS","STARTING");
		}
	}


	//*******************************************************************************************************************************

	private boolean processIncomingMapData(Context context, ESponderDFQueryResponseEvent dfQueryResponseEvent) {

		if(dfQueryResponseEvent.getEventAttachment() != null) {
			//FIXME Store the CrisisContext here
			CrisisContextDTO cc = dfQueryResponseEvent.getEventAttachment();

			if(cc!=null) {
				//				cc.getCrisisLocation().getCentre().getAltitude()
				String ccLocation = cc.getCrisisLocation().getCentre().getLatitude().toString()+"a"+
						cc.getCrisisLocation().getCentre().getLongitude().toString();


				SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
				SharedPreferences.Editor spEditor = prefs.edit();
				spEditor.putString(GlobalVar.spCrisisContextVar, ccLocation);
				spEditor.commit();

			}

			//			ObjectMapper mapper = new ObjectMapper();
			//			String ccJson = null;
			//			try {
			//				ccJson = mapper.writeValueAsString(cc);
			//			} catch (JsonGenerationException e) {
			//				e.printStackTrace();
			//			} catch (JsonMappingException e) {
			//				e.printStackTrace();
			//			} catch (IOException e) {
			//				e.printStackTrace();
			//			}
			//			if(ccJson != null) {
			//				SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			//				SharedPreferences.Editor spEditor = prefs.edit();
			//				spEditor.putString(GlobalVar.spCrisisContextVar, ccJson);
			//				spEditor.commit();
			//			}

		}

		if(dfQueryResponseEvent.getMapPositions() != null) {
			OrganizeHierarchy(context, dfQueryResponseEvent.getMapPositions());
			return true;
		}
		else
			return false;
	}

	//*******************************************************************************************************************************

	private void OrganizeHierarchy(Context context, OCEocALTDTO hierarchy) {

		boolean found = false;

		for(OCMeocALTDTO meoc : hierarchy.getSubordinateMEOCs()) {
			GlobalVar.db.insertOrUpdateMeocAndLocation(meoc);
			if(meoc.getIncidentCommander().getFrTeams() != null) {
				if(meoc.getIncidentCommander().getFrTeams().size() > 0) {
					for(FRTeamALTDTO team :meoc.getIncidentCommander().getFrTeams()) {
						ActorFRCALTDTO chief = team.getFrchief();
						if(chief != null) {
							GlobalVar.db.insertOrUpdateFRChiefAndLocation(chief);
							if(chief.getSubordinates() != null) {
								if(chief.getSubordinates().size() > 0 ) {
									for(ActorFRALTDTO fr : chief.getSubordinates()) {
										if(!found)
											GlobalVar.db.insertOrUpdateFirstResponderAndLocation(fr);									}
								}
							}
						}
					}
				}
			}
		}

	}

	//*******************************************************************************************************************************

	private void processActionRelatedEvents(String event, Class classType) {





	}


	//*******************************************************************************************************************************

	private void processAlarmRelatedEvents(final Context context, final String eventJSON) {

		class AlarmProcessor implements Runnable {

			@Override
			public void run() {
				ObjectMapper mapper = new ObjectMapper();
				DatafusionResultsGeneratedEvent event = null;
				try {
					event = mapper.readValue(eventJSON, DatafusionResultsGeneratedEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {

					DatafusionResultsDTO results = event.getEventAttachment();
					GlobalVar.db.insertFRAlarm(results.getId(), Long.valueOf(results.getResultsText2()), event.getEventSeverity().ordinal(), results.getResultsText1(), event.getEventTimestamp().getTime(), 0);

					// Vibrate
					Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
					v.vibrate(ESponderUtils.vibrationPattern, -1);

					//new SoundTask(eu.esponder.ESponderFRU.R.raw.alarm, context).execute(new Integer[]{eu.esponder.ESponderFRU.R.raw.alarm});

					Intent intent = new Intent(GlobalVar.INNER_ALARM_RECEIVED);
					intent.putExtra("frID", results.getId());
					context.sendBroadcast(intent);
				}
				else
					Log.e("ALARM RECEIVED","UNABLE TO PROCESS");
			}	
		}

		Thread thread = new Thread(new AlarmProcessor());
		thread.run();
	}


	//*******************************************************************************************************************************

	private void processIncomingMessageEvents(final Context context, final String eventJSON) {

		class MessageProcessor implements Runnable {

			@Override
			public void run() {
				ObjectMapper mapper = new ObjectMapper();
				MobileMessageEvent event = null;
				try {
					event = mapper.readValue(eventJSON, MobileMessageEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {

					if(event.getMobileimei().equalsIgnoreCase(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId())) {
						String origin = null;
						if(event.getSourceEoc()!= null || event.getSourceMEoc() != null) {
							if(event.getSourceEoc()!= null)
								origin = event.getSourceEoc();
							else if(event.getSourceMEoc()!= null)
								origin = event.getSourceMEoc();

							Intent intent = new Intent(context, NotificationsDialog.class);
							intent.setAction(GlobalVar.NOTIFICATION_POPUP);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							intent.putExtra("origin", origin);
							intent.putExtra("message", event.getSzMessage());
							context.startActivity(intent);
							Log.e("NOTIFICATION RECEIVED","INFORMED ACIVITY");

						}
						else
							Log.e("NOTIFICATION RECEIVED","NO ORIGIN PRESENT DISCARDING");
					}
					else
						Log.e("NOTIFICATION RECEIVED","UNKNOWN IMEI, NOT INTENDED FOR CURRENT USER");
				}
				else
					Log.e("NOTIFICATION RECEIVED","EMPTY MESSAGE, UNABLE TO PROCESS");
			}	
		}

		Thread thread = new Thread(new MessageProcessor());
		thread.run();
	}


	//*******************************************************************************************************************************

	private void processIncomingFileEvents(final Context context, final String eventJSON) {

		class FileProcessor implements Runnable {

			@Override
			public void run() {
				ObjectMapper mapper = new ObjectMapper();
				MobileFileNotifierEvent event = null;
				try {
					event = mapper.readValue(eventJSON, MobileFileNotifierEvent.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(event != null) {
					if(event.getImei().equalsIgnoreCase(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId())) {
						
						Log.e("Received file",event.getFileURL());
						
						Long newFileID = GlobalVar.db.insertFile(event.getDescription(), 0, null, event.getFileURL());
						Uri data = Uri.parse(event.getFileURL());
						Intent dfIntent = new Intent(context, DownloaderService.class);
						dfIntent.setData(data);
						dfIntent.putExtra("urlpath", event.getFileURL());
						dfIntent.putExtra("description", event.getDescription());
						dfIntent.putExtra("fileID", newFileID);
						context.startService(dfIntent);
						
					}
					else
						Log.e("NOTIFICATION RECEIVED","UNKNOWN IMEI, NOT INTENDED FOR CURRENT USER");
				}
				else
					Log.e("NOTIFICATION RECEIVED","EMPTY MESSAGE, UNABLE TO PROCESS");
			}	
		}

		Thread thread = new Thread(new FileProcessor());
		thread.run();
	}

	//*******************************************************************************************************************************

	private void activateGPSMeasurements(final Context context) {
		Thread gpsTHread = new Thread(new Runnable() {

			@Override
			public void run() {
				//	GlobalVarlocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
				//	LocationListener locationListener = new ESponderLocationListener(context);
				Looper.prepare();
				GlobalVar.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 5, GlobalVar.locationListener);
				Looper.loop();
			}
		});
		gpsTHread.start();
	}





	protected class SoundTask extends AsyncTask<Integer, Context, Integer> {

		MediaPlayer mediaPlayer = new MediaPlayer();
		Context context;

		public SoundTask(Integer soundResource, Context ctx) {
			this.context = ctx;
		}


		@Override
		protected Integer doInBackground(Integer... params) 
		{
			AssetFileDescriptor afd = context.getResources().openRawResourceFd(params[0]);


			//final MediaPlayer mediaPlayer = new MediaPlayer();
			try {
				mediaPlayer.reset();
				mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getDeclaredLength());
				mediaPlayer.prepare();
				mediaPlayer.setOnPreparedListener(new OnPreparedListener() {

					@Override
					public void onPrepared(MediaPlayer arg0) {
						mediaPlayer.seekTo(0);
						mediaPlayer.start();
					}});

				afd.close();
			} catch (Exception e) {
				e.printStackTrace();
			}



			return null;
		}
	}



}
