package eu.esponder.ESponderDriver.runnables;

import java.io.IOException;
import java.util.Date;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.datafusion.query.ESponderDFQueryRequestEvent;

public class MapRequestEventSender implements Runnable {
	
	Context ctx;
	String source;

	public MapRequestEventSender(Context context, String source) {
		this.ctx = context;
		this.source = source;
	}

	@Override
	public void run() {

		ESponderUserDTO user = new ESponderUserDTO();
		user.setId(Long.valueOf(0));

		ESponderDFQueryRequestEvent pEvent = new ESponderDFQueryRequestEvent();
		pEvent.setIMEI(((TelephonyManager)ctx.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
		pEvent.setQueryID(Long.valueOf(1));
		pEvent.setRequestID(Long.valueOf(20));
		pEvent.setJournalMessage("JM");
		QueryParamsDTO pParams= QueryParamsDTO.with("USERID", "5");
		pParams.and("IMEI", "test");
		pEvent.setParams(pParams);
		pEvent.setEventAttachment(user);
		pEvent.setJournalMessage("Test Event");
		pEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
		pEvent.setEventTimestamp(new Date());
		pEvent.setEventSeverity(SeverityLevelDTO.UNDEFINED);
		pEvent.setEventSource(user);

		ObjectMapper mapper = new ObjectMapper();
		mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
		mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		String jsonObject = null;
		try {
			jsonObject = mapper.writeValueAsString(pEvent);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if(jsonObject != null) {

			//Log.e("Map Request JSON", jsonObject);
			Intent intent = new Intent(GlobalVar.MAP_REQUEST_HIERARCHY);
			intent.addCategory(GlobalVar.INTENT_CATEGORY);
			intent.putExtra("event", jsonObject);
			ctx.sendBroadcast(intent);

			
			Log.e(MapRequestEventSender.class.getName().toString(), source + " / map request details event has been broadcast...");

		}
		else
			Log.e("SEND MAP REQUEST EVENT","SERIALIZATION HAS FAILED");

	}
	
	
}
