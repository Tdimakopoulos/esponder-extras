package eu.esponder.ESponderDriver.runnables;

import java.util.TimerTask;

import android.content.Context;
import android.content.SharedPreferences;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.ESponderDriver.soapservices.ESponderRestClient;
import eu.esponder.ESponderDriver.soapservices.ESponderRestClient.RequestMethod;

public class GetSensorsPCDTimerTask extends TimerTask {
	
	Context context;
	
	public GetSensorsPCDTimerTask(Context context) {
	this.context = context;
	}

	@Override
	public void run() {
		
		SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
		String rasIP = prefs.getString(GlobalVar.spESponderUserRasIP, "");
		if(!rasIP.equalsIgnoreCase("")) {
			String url = "http://"+rasIP+"/avgfrteam";
			ESponderRestClient client = new ESponderRestClient(context, url);
			try {
				client.Execute(RequestMethod.GET);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}