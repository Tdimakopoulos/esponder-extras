package eu.esponder.ESponderDriver.runnables;

import java.io.IOException;
import java.util.Date;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.util.Log;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.datafusion.query.ESponderDFQueryRequestEvent;

public class SendPCDSensorsToBackend implements Runnable {
	
	private Context context;
	private String sensorMeasurements;
	
	public SendPCDSensorsToBackend(Context ctx, String sm) {
		this.context = ctx;
		this.sensorMeasurements = sm;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		SharedPreferences prefs = context.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
        Long frID = prefs.getLong(GlobalVar.spESponderUserID, -1);
        
        if(frID != -1) {
        	
        	ESponderUserDTO user = new ESponderUserDTO();
    		user.setId(Long.valueOf(0));
    		
            ESponderDFQueryRequestEvent pEvent = new ESponderDFQueryRequestEvent();
    		pEvent.setIMEI(((TelephonyManager)context.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
    		pEvent.setQueryID(Long.valueOf(3));
    		pEvent.setRequestID(Long.valueOf(20));
    		pEvent.setJournalMessage("JM");
    		
    		
    		QueryParamsDTO pParams= QueryParamsDTO.with("userid", String.valueOf(frID));
    		pParams.and("sensorMeasurements", sensorMeasurements);
    		pEvent.setParams(pParams);
    		pEvent.setEventAttachment(user);
    		pEvent.setJournalMessage("Test Event");
    		pEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
    		pEvent.setEventTimestamp(new Date());
    		pEvent.setEventSeverity(SeverityLevelDTO.UNDEFINED);
    		pEvent.setEventSource(user);
    		
          ObjectMapper mapper = new ObjectMapper();
          String eventJson = null;
          try {
  			eventJson = mapper.writeValueAsString(pEvent);
  		} catch (JsonGenerationException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (JsonMappingException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
          if(eventJson != null) {
          	
          	Intent intent = new Intent(GlobalVar.MAP_REQUEST_HIERARCHY);
  			intent.addCategory(GlobalVar.INTENT_CATEGORY);
  			intent.putExtra("event", eventJson);
  			context.sendBroadcast(intent);
          }
          Log.e("PCDUINO","MEAS SENT TO BACKEND");
        	
        }
		
	}

}
