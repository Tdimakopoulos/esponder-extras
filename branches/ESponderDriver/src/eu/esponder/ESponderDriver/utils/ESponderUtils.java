package eu.esponder.ESponderDriver.utils;

import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.exceptions.NotSupportedException;

public class ESponderUtils {

	public static long[] vibrationPattern = {
		0,  // Start immediately
		200, 200, 200, 200, 200,    // s
		500,
		500, 200, 500, 200, 500, // o
		500,
		200, 200, 200, 200, 200,    // s
		1000
	};
	
	
	public static String processUriMimeType(String filename) throws NotSupportedException {

		if(filename.endsWith("png"))
			return "image/png";
		else if(filename.endsWith("jpg") || filename.endsWith("jpeg"))
			return "image/jpeg";
		else if(filename.endsWith("pdf"))
			return "application/pdf";
		else if(filename.endsWith("txt"))
			return "text/plain";
		else
			throw new NotSupportedException();
	}
	
	public static int processIconForUriMimeType(String filename) {

		if(filename != null) {
			if(filename.endsWith("png"))
				return R.drawable.jpgicon;
			else if(filename.endsWith("jpg") || filename.endsWith("jpeg"))
				return R.drawable.jpgicon;
			else if(filename.endsWith("pdf"))
				return R.drawable.pdficon;
			else if(filename.endsWith("txt"))
				return R.drawable.txticon;
			
			return -1;
		}
		else
			return -1;	
		
		
		
	}

}
