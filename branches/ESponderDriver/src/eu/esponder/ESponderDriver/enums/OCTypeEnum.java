package eu.esponder.ESponderDriver.enums;

public enum OCTypeEnum {

	EOC(1),

	MEOC(2);

	private final int statType;

	OCTypeEnum(final int statType) {
		this.statType = statType;
	}

	public int getStatType() {
		return statType;
	}


}
