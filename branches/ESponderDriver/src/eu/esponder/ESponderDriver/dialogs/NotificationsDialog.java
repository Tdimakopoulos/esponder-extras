package eu.esponder.ESponderDriver.dialogs;


import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.application.GlobalVar;

public class NotificationsDialog extends Activity {
	AlertDialog alert;
	AlertDialog.Builder alertd;
	String notificationOrigin, notificationMessage;
	Timer t;
	Context context;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		context = getApplicationContext();

		alertd = new AlertDialog.Builder(NotificationsDialog.this);
		alertd.setIcon(R.drawable.warning);

		t = new Timer();

		String incomingAction = getIntent().getAction();
		if(incomingAction.equalsIgnoreCase(GlobalVar.NOTIFICATION_POPUP)) {

			Bundle extras = getIntent().getExtras();
			notificationOrigin = extras.getString("origin");
			notificationMessage = extras.getString("message");

			alertd.setTitle("Notification from "+notificationOrigin);
			alertd.setMessage(notificationMessage);

			alertd.setPositiveButton("Accept", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					t.cancel();
					NotificationsDialog.this.finish();
				}
			});

			alertd.setNegativeButton("Cancel", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					t.cancel();
					NotificationsDialog.this.finish();
				}
			});

			alert = alertd.create();
			alert.show();
		}
	}



	@Override
	public void onBackPressed() {
		super.onBackPressed();
		t.cancel();
		if(alert !=null) {
			if(alert.isShowing())
				alert.dismiss();
		}
		NotificationsDialog.this.finish();
	}


	@Override
	public void onStart() {
		super.onStart();

		if(alert!=null) {

			alert.show();
			t.schedule(new TimerTask() {
				public void run() {
					alert.dismiss();
					overridePendingTransition(0, 0);
					t.cancel();
					NotificationsDialog.this.finish();
				}
			}, 5000);
		}
		else
			Log.e("Inside HANDLER", "*****************ALERT IS NULL FOR SOME REASON*****************");
	}

	public void onDestroy() {
		super.onDestroy();
	}

}