package eu.esponder.ESponderDriver.data;

public class FRAlarmEntity {
	
	private long id;
	private long frID;
	private long alarmID;
	private int severity;
	private String details;
	private long date;
	private int acked;
	
	public FRAlarmEntity(long id, long alarmID,long frID, int severity, String details, long date, int ack) {
		this.id = id;
		this.alarmID = alarmID;
		this.frID = frID;
		this.severity = severity;
		this.details = details;
		this.date = date;
		this.setAcked(ack);
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getFrID() {
		return frID;
	}
	
	public void setFrID(long frID) {
		this.frID = frID;
	}
	
	public int getSeverity() {
		return severity;
	}
	
	public void setSeverity(int severity) {
		this.severity = severity;
	}
	
	public String getDetails() {
		return details;
	}
	
	public void setDetails(String details) {
		this.details = details;
	}
	
	public long getDate() {
		return date;
	}
	
	public void setDate(long date) {
		this.date = date;
	}

	public int getAcked() {
		return acked;
	}

	public void setAcked(int acked) {
		this.acked = acked;
	}

	public long getAlarmID() {
		return alarmID;
	}

	public void setAlarmID(long alarmID) {
		this.alarmID = alarmID;
	}

}