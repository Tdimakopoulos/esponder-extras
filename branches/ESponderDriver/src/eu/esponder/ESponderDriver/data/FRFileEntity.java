package eu.esponder.ESponderDriver.data;

public class FRFileEntity {
	
	private Long id;
	private String description;
	private int status;
	private String relativePath;
	private String URL;
	
	public FRFileEntity(Long id, String description, int status,
			String relativePath, String URL) {
		super();
		this.id = id;
		this.description = description;
		this.status = status;
		this.relativePath = relativePath;
		this.URL = URL;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getRelativePath() {
		return relativePath;
	}
	
	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

}
