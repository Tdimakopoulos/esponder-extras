package eu.esponder.ESponderDriver.data;

public class Poi {
	
	private int PoiCategoryCode;
	private double Longitude;
	private double Latitude;
	private String LabelTitle;
	private String LabelText;
	private String objectTitle;
	private long objectID;
	
	public Poi(){
		PoiCategoryCode = 0; // Default Poi Category
		Longitude = 37.988661;
		Latitude = 23.767674;	
		LabelTitle = new String("Default Poi Title");
		LabelText = new String("Default Poi Text");
		objectTitle = new String("Default Poi objectTitle");
	}
	
	public Poi(int poiCategoryCode, double longitude, double latitude, String labelTitle, String labelText, String objectTitle, Long id){
		this.PoiCategoryCode = poiCategoryCode;
		this.Longitude = longitude;
		this.Latitude = latitude;
		this.LabelTitle = labelTitle;
		this.LabelText = labelText;
		this.objectTitle = objectTitle;
		this.objectID = id;
	}
	
	public int getPoiCategoryCode(){
		return PoiCategoryCode;
	}
	
	public void setPoiCategoryCode(int code){
		PoiCategoryCode = code;
	}
	
	public double getLongitude(){
		return Longitude;
	}
	
	public void setLongitude(double longitude){
		Longitude = longitude;
	}
	
	public double getLatitude(){
		return Latitude;
	}
	
	public void setLatitude(double latitude){
		Latitude = latitude;
	}
	public String getLabelTitle(){
		return LabelTitle;
	}
	
	public void setLabelTitle(String labelTitle){
		LabelTitle = labelTitle;
	}
	
	public String getLabelText(){
		return LabelText;
	}
	
	public void setLabelText(String labelText){
		LabelText = labelText;
	}

	public String getObjectTitle() {
		return objectTitle;
	}

	public void setObjectTitle(String objectTitle) {
		this.objectTitle = objectTitle;
	}

	public long getObjectID() {
		return objectID;
	}

	public void setObjectID(long objectID) {
		this.objectID = objectID;
	}

}
