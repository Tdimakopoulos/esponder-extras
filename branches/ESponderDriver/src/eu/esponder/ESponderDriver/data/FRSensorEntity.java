package eu.esponder.ESponderDriver.data;

public class FRSensorEntity {
	
	private int sensorType;
	private double value;
	private long dateTo;
	private long dateFrom;
	private String sensorUnit;
	
	public FRSensorEntity(int sensorType, double value, long dateTo,
			long dateFrom, String sensorUnit) {
		super();
		this.sensorType = sensorType;
		this.value = value;
		this.dateTo = dateTo;
		this.dateFrom = dateFrom;
		this.sensorUnit = sensorUnit;
	};
	
	
	public int getSensorType() {
		return sensorType;
	}

	public void setSensorType(int sensorType) {
		this.sensorType = sensorType;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public long getDateTo() {
		return dateTo;
	}

	public void setDateTo(long dateTo) {
		this.dateTo = dateTo;
	}

	public long getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(long dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getSensorUnit() {
		return sensorUnit;
	}

	public void setSensorUnit(String sensorUnit) {
		this.sensorUnit = sensorUnit;
	}
	
}
