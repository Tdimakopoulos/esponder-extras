package eu.esponder.ESponderDriver.data;

public class FRActionObjectiveEntity {
	
	private long id;
	private long apoID;
	private long apID;
	private String title;
	private long dateFrom;
	private long dateTo;
	private double latitude, longitude,altitude;
	private int objectiveType;
	
	public FRActionObjectiveEntity(long id, long apoID, long apID,
			String title, long dateFrom, long dateTo, double latitude,
			double longitude, double altitude, int type) {
		this.setId(id);
		this.setApoID(apoID);
		this.setApID(apID);
		this.setTitle(title);
		this.setDateFrom(dateFrom);
		this.setDateTo(dateTo);
		this.setLatitude(latitude);
		this.setLongitude(longitude);
		this.setAltitude(altitude);
		this.setObjectiveType(type);
	}
	
	public FRActionObjectiveEntity() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getApoID() {
		return apoID;
	}

	public void setApoID(long apoID) {
		this.apoID = apoID;
	}

	public long getApID() {
		return apID;
	}

	public void setApID(long apID) {
		this.apID = apID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getDateTo() {
		return dateTo;
	}

	public void setDateTo(long dateTo) {
		this.dateTo = dateTo;
	}

	public long getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(long dateFrom) {
		this.dateFrom = dateFrom;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public int getObjectiveType() {
		return objectiveType;
	}

	public void setObjectiveType(int objectiveType) {
		this.objectiveType = objectiveType;
	}
	
	
	

}
