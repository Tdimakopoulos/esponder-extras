package eu.esponder.ESponderDriver.adapters;

import java.math.BigDecimal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.animations.MeasCriticalAnimation;
import eu.esponder.ESponderDriver.data.FRSensorEntity;
import eu.esponder.ESponderDriver.enums.SensorUnitEnum;

public class MeasurementsAdapter extends ArrayAdapter<FRSensorEntity> {
	private final Context context;
	private final FRSensorEntity[] values;
	private String[] sensorTitles = {"Activity Rate :","Body Temperature :","Breath Rate :",
			"Heart Beat Rate :","Environment Temperature :","Methane Reading :", "Carbon Monoxide Reading :", "Carbon Dioxide Reading :",
			"Oxygen Reading :", "Hydrogen Sulfide Reading :",
			"LPS Reading :","Location Reading :"};

	public MeasurementsAdapter(Context context, FRSensorEntity[] values) {
		super(context, R.layout.measurementrowlayout, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Animation anim = new MeasCriticalAnimation(0.0f, 1.0f);

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.measurementrowlayout, parent, false);
		TextView measurementTitle = (TextView) rowView.findViewById(R.id.measurementsrowtitle);

		measurementTitle.setText(sensorTitles[position]);

		ImageView imageView = (ImageView) rowView.findViewById(R.id.measurementsrowimage);

		switch(position) {
		case 0:
			imageView.setImageResource(R.drawable.activitysensor);
			break;
		case 1:
			imageView.setImageResource(R.drawable.temperaturesensor);
			break;
		case 2:
			imageView.setImageResource(R.drawable.breathratesensor);
			break;
		case 3:
			imageView.setImageResource(R.drawable.heartbeatsensor);
			break;
		case 4:
			imageView.setImageResource(R.drawable.temperaturesensor);
			break;
		case 5:
			imageView.setImageResource(R.drawable.gassensor);
			break;
		case 6:
			imageView.setImageResource(R.drawable.gassensor);
			break;
		case 7:
			imageView.setImageResource(R.drawable.gassensor);
			break;
		case 8:
			imageView.setImageResource(R.drawable.gassensor);
			break;
		case 9:
			imageView.setImageResource(R.drawable.gassensor);
			break;
		case 10:
			imageView.setImageResource(R.drawable.locationsensor);
			break;
		case 11:
			imageView.setImageResource(R.drawable.locationsensor);
			break;
		}

		TextView valueText = (TextView)rowView.findViewById(R.id.measurementsrowvalue);

		if(values[position] != null) {
			valueText.setText(round((values[position]).getValue(), 2));
		}
		else
			valueText.setText("Not available");


		if(!valueText.getText().toString().equalsIgnoreCase("Not available")) {

			TextView unitText = (TextView)rowView.findViewById(R.id.measurementsrowunit);

			switch(position) {
			case 0:
				unitText.setText(SensorUnitEnum.ActivitySensor.getUnit());
				break;
			case 1:
				unitText.setText(SensorUnitEnum.BodyTemperatureSensor.getUnit());
				break;
			case 2:
				unitText.setText(SensorUnitEnum.BreathRateSensor.getUnit());
				break;
			case 3:
				unitText.setText(SensorUnitEnum.HeartBeatRateSensor.getUnit());
				break;
			case 4:
				unitText.setText(SensorUnitEnum.EnvTemperatureSensor.getUnit());
				break;
			case 5:
				unitText.setText(SensorUnitEnum.GasSensor.getUnit());
				break;
			case 6:
				unitText.setText(SensorUnitEnum.GasSensor.getUnit());
				break;
			case 7:
				unitText.setText(SensorUnitEnum.GasSensor.getUnit());
				break;
			case 8:
				unitText.setText(SensorUnitEnum.GasSensor.getUnit());
				break;
			case 9:
				unitText.setText(SensorUnitEnum.GasSensor.getUnit());
				break;
			case 10:
				unitText.setText(SensorUnitEnum.LPSSensor.getUnit());
				break;
			case 11:
				unitText.setText(SensorUnitEnum.LocationSensor.getUnit());
				break;
			}

		}

		return rowView;
	}
	
	public static String round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
	    return String.valueOf(bd.doubleValue());
	}


} 