package eu.esponder.ESponderDriver.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.activities.Actions;
import eu.esponder.ESponderDriver.activities.ActorDetails;
import eu.esponder.ESponderDriver.activities.Alarms;
import eu.esponder.ESponderDriver.animations.MeasCriticalAnimation;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;

public class CrisisMemberAdapter extends BaseAdapter {
	
	private Context context;
	private ActorFRALTDTO[] values;

	public CrisisMemberAdapter(Context context, ActorFRALTDTO[] values) {
		setContext(context);
		setValues(values);
	}
	
	@Override
	public int getCount() {
		return getValues().length;
	}

	@Override
	public Object getItem(int position) {
		return getValues()[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		
		
		if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.firstresponderrowlayoutnormal, parent, false);
        }
		
		final ActorFRALTDTO fr = (ActorFRALTDTO) getItem(position);
		
		// Set the title
		TextView frTitleText = (TextView) convertView.findViewById(R.id.frtitle);
		frTitleText.setText(fr.getTitle());
		
		//Set the full Name of the first responder, if available
		String frName = GlobalVar.db.getFirstResponderName(fr.getId());
		if(frName != null) {
			TextView frNameText = (TextView) convertView.findViewById(R.id.frpersonnelname);
			frNameText.setText(frName);
		}
		
		//Alarm Indicator
		ImageView alarmIndicator = (ImageView) convertView.findViewById(R.id.alarmIndicator);
		if(alarmIndicator!=null) {
			if(GlobalVar.db.getNotAckedAlarmsCountForFR(fr.getId()) > 0) {
				alarmIndicator.setVisibility(View.VISIBLE);
				alarmIndicator.setAnimation(new MeasCriticalAnimation(0.0f, 1.0f));
			}
			else
				alarmIndicator.setVisibility(View.INVISIBLE);
			
		}
		else
			Log.e("NOT FOUND","NOT FOUND");
			
		
		Button showDetails = (Button) convertView.findViewById(R.id.ShowDetails);
		
		
		if(showDetails != null)
			showDetails.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, ActorDetails.class); 
					intent.putExtra("frID", fr.getId());
					context.startActivity(intent);
				}
			});
		
		Button showActions = (Button) convertView.findViewById(R.id.ShowActions);
		showActions.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent goToActions = new Intent(context, Actions.class);
				goToActions.putExtra("frID", fr.getId());
				context.startActivity(goToActions);
			}
		});
		
		
		Button showAlarms = (Button) convertView.findViewById(R.id.showAlarms);
		showAlarms.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent goToAlarms = new Intent(context, Alarms.class);
				goToAlarms.putExtra("frID", fr.getId());
				context.startActivity(goToAlarms);
			}
		});
		
		return convertView;
	}

	

	
	// Getters and setters for private fields of the Adapter
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public ActorFRALTDTO[] getValues() {
		return values;
	}

	public void setValues(ActorFRALTDTO[] values) {
		this.values = values;
	}
	

}
