package eu.esponder.ESponderDriver.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import eu.esponder.ESponderDriver.R;

public class MenuAdapter extends ArrayAdapter<String> {

	Context context;
	int resourceID;
	String[] menuItems;

	public MenuAdapter(Context context, int textViewResourceId, String[] objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.resourceID = textViewResourceId;
		this.menuItems = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.row_sidemenu, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.menu_row_text);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.menuRowIcon);
		textView.setText(menuItems[position]);

		switch(position) {
		case 0:
			imageView.setImageResource(R.drawable.ic_launcher);
			break;
		case 1:
			imageView.setImageResource(R.drawable.measurements);
			break;
		case 2:
			imageView.setImageResource(R.drawable.fireman);
			break;
		case 3:
			imageView.setImageResource(R.drawable.actionslist);
			break;
		case 4:
			imageView.setImageResource(R.drawable.alarmmenuicon);
			break;
//		case 5:
//			imageView.setImageResource(R.drawable.settingsicon);
//			break;
		case 5:
			imageView.setImageResource(R.drawable.exiticon);
			break;
		}

		return rowView;
	}


}

