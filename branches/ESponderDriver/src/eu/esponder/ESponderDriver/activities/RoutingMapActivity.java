package eu.esponder.ESponderDriver.activities;

import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.nutiteq.MapView;
import com.nutiteq.components.Components;
import com.nutiteq.components.MapPos;
import com.nutiteq.projections.EPSG3857;
import com.nutiteq.projections.Projection;
import com.nutiteq.rasterlayers.TMSMapLayer;
import com.nutiteq.style.LineStyle;
import com.nutiteq.style.StyleSet;
import com.nutiteq.vectorlayers.GeometryLayer;
import com.nutiteq.vectorlayers.MarkerLayer;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.adapters.MenuAdapter;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.ESponderDriver.data.DataManager;
import eu.esponder.ESponderDriver.maphelpers.MyLocationCircle;
import eu.esponder.ESponderDriver.maphelpers.RoutingMapEventListener;
import eu.esponder.ESponderDriver.nutiteqhelperclasses.MapQuestDirections;
import eu.esponder.ESponderDriver.nutiteqhelperclasses.Route;
import eu.esponder.ESponderDriver.nutiteqhelperclasses.RouteActivity;
import eu.esponder.dto.model.snapshot.location.PointDTO;

public class RoutingMapActivity extends SlidingActivity implements RouteActivity {

	private MapView mapView;
	ImageView menuImage, centerMapImage;
	SlidingMenu sm;
	RoutingMapEventListener mapListener;
	Handler handler;
	private static final float MARKER_SIZE = 0.4f;
	private static final String MAPQUEST_KEY = "Fmjtd%7Cluub2qu82q%2C70%3Do5-961w1w";
	private GeometryLayer routeLayer;
	private MapPos startPos, stopPos;
	private MarkerLayer markerLayer;
	private MapQuestDirections directionsService;
	private Long frID = (long) -1;
	RoutingMapReceiver receiver;
	ProgressDialog pDialog;



	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.routingmaplayout);

		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		setTitle("ESponder - Routing");

		handler = new Handler();

		initializeMenu();

		// Enable Logging
		//		Log.enableAll();
		//		Log.setTag("OSMDemoApp");

		mapView = (MapView) findViewById(R.id.mapView);

		// Restore map state during devicse rotation,
		// it is saved in onRetainNonConfigurationInstance() below
		Components retainObject = (Components) getLastNonConfigurationInstance();
		if (retainObject != null) {
			// just restore configuration and update listener, skip other initializations
			mapView.setComponents(retainObject);
			mapListener = (RoutingMapEventListener) mapView.getOptions().getMapListener();
			mapListener.reset(this, mapView);
			mapView.startMapping();
			return;
		} else {
			// Create and set MapView components 
			mapView.setComponents(new Components());
		}

		DataManager.initInstance(this);

		// Initialize the map
		String MapServerURL = DataManager.getInstance().getMapServerURL();
		// Define base layer. Here we use MapQuest open tiles which are free to use
		// Almost all online maps use EPSG3857 projection.
		TMSMapLayer mapLayer = new TMSMapLayer(new EPSG3857(), 0, 18, 0,
				MapServerURL, "/", ".png");
		mapView.getLayers().setBaseLayer(mapLayer);

		// set initial map view camera - optional. "World view" is default 
		// NB! it must be in base layer projection (EPSG3857), so we convert it from lat and long		
		// zoom - 0 = world, like on most web maps
		//		mapView.setZoom((float)DataManager.getInstance().getInitialMapZoom());
		mapView.setZoom((float)17);
		// tilt means perspective view. Default is 90 degrees for "normal" 2D map view, minimum allowed is 30 degrees.
		mapView.setTilt(180.0f);

		// Activate some mapview options to make it smoother
		mapView.getOptions().setPreloading(true);
		mapView.getOptions().setSeamlessHorizontalPan(true);
		mapView.getOptions().setTileFading(true);
		mapView.getOptions().setKineticPanning(true);
		mapView.getOptions().setDoubleClickZoomIn(true);
		mapView.getOptions().setDualClickZoomOut(true);

		// configure texture caching - optional, suggested 
		mapView.getOptions().setTextureMemoryCacheSize(40 * 1024 * 1024);
		mapView.getOptions().setCompressedMemoryCacheSize(8 * 1024 * 1024);

		// define online map persistent caching - optional, suggested. Default - no caching
		mapView.getOptions().setPersistentCachePath(this.getDatabasePath("mapcache").getPath());
		// set persistent raster cache limit to 100MB
		mapView.getOptions().setPersistentCacheSize(100 * 1024 * 1024);

		// add event listener
		mapListener = new RoutingMapEventListener(RoutingMapActivity.this, mapView);
		mapView.getOptions().setMapListener(mapListener);

		//New Addition
		routeLayer = new GeometryLayer(new EPSG3857());
		mapView.getLayers().addLayer(routeLayer);

		markerLayer = new MarkerLayer(new EPSG3857());
		mapView.getLayers().addLayer(markerLayer);

		//				Bitmap olMarker = UnscaledBitmapLoader.decodeResource(getResources(),
		//						R.drawable.fireman);
		//				StyleSet<MarkerStyle> startMarkerStyleSet = new StyleSet<MarkerStyle>(
		//						MarkerStyle.builder().setBitmap(olMarker).setColor(Color.GREEN)
		//						.setSize(MARKER_SIZE).build());



		//		StyleSet<MarkerStyle> stopMarkerStyleSet = new StyleSet<MarkerStyle>(
		//				MarkerStyle.builder().setBitmap(olMarker).setColor(Color.RED)
		//				.setSize(MARKER_SIZE).build());
		//		stopPos = new MapPos(37.953422,23.770273);




		//		 make markers invisible until we need them
		//		 startMarker.setVisible(false);
		//		 stopMarker.setVisible(false);
		//		
		//						markerLayer.add(startMarker);
		//						markerLayer.add(stopMarker);


		//				this.setStartMarker(new MapPos(37.875066,23.756819));
		//				this.setStopMarker(new MapPos(37.953422,23.770273));


		if(getIntent().hasExtra("apoID")) {
			Long apoID = getIntent().getLongExtra("apoID", -1);
			if(apoID != -1) {
				Map<String, Double> coords = GlobalVar.db.getLocationForActionPartObjective(apoID);
				if(coords != null)
					stopPos = new MapPos(coords.get("LATITUDE"),coords.get("LONGITUDE"));
			}
		}
		if(getIntent().hasExtra("frID")) {
			frID = getIntent().getLongExtra("frID", -1);
			if(frID != -1) {
				PointDTO point = GlobalVar.db.getFirstResponderLocationById(frID);
				if(point != null)
					startPos = new MapPos(point.getLatitude().doubleValue(),point.getLongitude().doubleValue());
			}

		}

		Runnable showPDialog = new Runnable() {

			@Override
			public void run() {
				pDialog = ProgressDialog.show(RoutingMapActivity.this, "", "Waiting for routing information...", false, true );
			}
		};

		if(startPos!= null && stopPos !=null) {

			//Center around the current position of the user
			SharedPreferences prefs = RoutingMapActivity.this.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			frID = prefs.getLong(GlobalVar.spESponderUserID, -1);
			if(frID != -1) {
				PointDTO point = GlobalVar.db.getFirstResponderLocationById(frID);
				if(point != null) {
					mapView.setFocusPoint(mapView.getLayers().getBaseLayer().getProjection().fromWgs84(Double.
							valueOf(point.getLongitude().toString()), Double.valueOf(point.getLatitude().toString()))); //	long, lat --> opposite than usual use
					mapView.setZoom((float)14);
				}
			}

			Handler handler = new Handler();
			handler.post(showPDialog);
			
			mapView.startMapping();
			if(startPos!= null && stopPos !=null) {
				showRoute(startPos.x, startPos.y, stopPos.x, stopPos.y);
			}

		}

	}


	private void initializeMenu() {

		//		setMenuButtonOnClickListener();

		View menuView = LayoutInflater.from(RoutingMapActivity.this).inflate(R.layout.menu_frame, null);

		ListView menuList = (ListView) menuView.findViewById(R.id.MenuAsList);

		Resources res = getResources();
		String[] menuItems = res.getStringArray(R.array.frc_menu_items);

		MenuAdapter menuAdapter = new MenuAdapter(RoutingMapActivity.this, R.layout.row_sidemenu, menuItems);

		menuList.setAdapter(menuAdapter);

		menuList.setOnItemClickListener(new OnItemClickListener() {

			SharedPreferences prefs = RoutingMapActivity.this.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			Long frID = prefs.getLong(GlobalVar.spESponderUserID, -1);

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent;
				switch(position) {
				case 0:
					intent = new Intent(RoutingMapActivity.this, MainActivity.class);
					startActivity(intent);
					break;
				case 1:
					intent = new Intent(RoutingMapActivity.this, ActorDetails.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 2:
					intent = new Intent(RoutingMapActivity.this, Actors.class);
					startActivity(intent);
					break;
				case 3:
					intent = new Intent(RoutingMapActivity.this, Actions.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 4:
					intent = new Intent(RoutingMapActivity.this, Alarms.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
					/*case 5:
					intent = new Intent(RoutingMapActivity.this, Settings.class);
					startActivity(intent);
					break;*/
				case 5:
					intent = new Intent(Intent.ACTION_MAIN);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					break;
				}

			}
		});

		setBehindContentView(menuView);

		sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);

		setSlidingActionBarEnabled(true);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_map, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(receiver);
	}


	@Override
	protected void onResume() {
		// start mapping
		super.onResume();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_ROUTE_RECEIVED);
		receiver = new RoutingMapReceiver();
		registerReceiver(receiver, filter);
	}


	@Override
	public void onBackPressed() {
		Toast.makeText(RoutingMapActivity.this, "Please use the menu if you want to navigate or exit the application...", Toast.LENGTH_SHORT).show();
	}


	protected void initGps(final MyLocationCircle locationCircle) {
		final Projection proj = mapView.getLayers().getBaseLayer().getProjection();

		LocationListener locationListener = new LocationListener() 
		{
			public void onLocationChanged(Location location) {
				if (locationCircle != null) {
					locationCircle.setLocation(proj, location);
					locationCircle.setVisible(true);
				}
			}

			public void onStatusChanged(String provider, int status, Bundle extras) {}

			public void onProviderEnabled(String provider) {}

			public void onProviderDisabled(String provider) {}
		};

		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);	        
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 100, locationListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
	}


	//	New Addition
	@Override
	public void showRoute(double fromLat, double fromLon, double toLat,
			double toLon) {
		// TODO Auto-generated method stub
		Projection proj = mapView.getLayers().getBaseLayer().getProjection();

		StyleSet<LineStyle> routeLineStyle = new StyleSet<LineStyle>(LineStyle.builder().setWidth(0.1f).setColor(0xff0000CD).build());
		Map<String, String> routeOptions = new HashMap<String,String>();
		routeOptions.put("unit", "K"); // K - km, M - miles
		routeOptions.put("routeType", "fastest");
		// Add other route options here, see http://open.mapquestapi.com/directions/

		directionsService = new MapQuestDirections(this, new MapPos(fromLon, fromLat), new MapPos(toLon, toLat), routeOptions, MAPQUEST_KEY, proj, routeLineStyle);
		directionsService.route();

		//		MapPos origmark = mapView.getLayers().getBaseLayer().getProjection().fromWgs84(37.987283, 23.774918);
		//		GeoLocation factory = GeoLocation.fromRadians(origmark.x, origmark.y);
		//		GeoLocation factory = GeoLocation.fromRadians(37.987283, 23.774918);
		//		GeoLocation[] circle = factory.boundingCoordinates(2, 6371.01); 
		//		List<MapPos> list = new ArrayList<MapPos>();
		//		for(int i=0; i<circle.length; i++)
		//			list.add(i, new MapPos(circle[i].getLongitudeInRadians(), circle[i].getLatitudeInRadians()));
		//		list.add(circle.length, list.get(0));
		//		Polygon poly = new Polygon(list, new DefaultLabel("Test text"), PolygonStyle.builder().setLineStyle(LineStyle.builder().setColor(Color.CYAN).setWidth(0.4f).build()).setColor(Color.DKGRAY).build(), null);
		//		GeometryLayer geomlayer = new GeometryLayer(mapView.getLayers().getBaseLayer().getProjection());
		//		mapView.getLayers().addLayer(geomlayer);
		//		geomlayer.add(poly);
		//		mapView.startMapping();
	}


	@Override
	public void setStartMarker(MapPos startPos) {
		//				routeLayer.clear();
		//				markerLayer.clear();
		//				this.startPos = startPos;
		//		markerLayer.add(startMarker);
		//		startMarker.setMapPos(startPos);
		//		startMarker.setVisible(true);
	}


	@Override
	public void setStopMarker(MapPos mapPos) {
		//		markerLayer.add(stopMarker);
		//		stopMarker.setMapPos(mapPos);
		//		stopMarker.setVisible(true);
	}


	@Override
	public void routeResult(Route route) {
		// TODO Auto-generated method stub
		if(route.getRouteResult() != Route.ROUTE_RESULT_OK){
			Toast.makeText(this, "Route error", Toast.LENGTH_LONG).show();
			return;
		}

		markerLayer.clear();
		routeLayer.clear();

		routeLayer.add(route.getRouteLine());
		//		Log.debug("route line points: "+route.getRouteLine().getVertexList().size());
		// Log.debug("route line: "+route.getRouteLine().toString());
		mapView.setFocusPoint(mapView.getLayers().getBaseLayer().getProjection().fromWgs84(startPos.y, startPos.x)); //	long, lat --> opposite than usual use
		mapView.setZoom((float)15);
		mapView.requestRender();
		Toast.makeText(this, "Route "+route.getRouteSummary(), Toast.LENGTH_LONG).show();
		directionsService.startRoutePointMarkerLoading(markerLayer, MARKER_SIZE);
		Intent routeReceivedIntent = new Intent(GlobalVar.INNER_ROUTE_RECEIVED);
		RoutingMapActivity.this.sendBroadcast(routeReceivedIntent);
	}




	public class RoutingMapReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			Log.e("ROUTING RECEIVER","received");
			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_ROUTE_RECEIVED)) {
				if(pDialog!=null)
					if(pDialog.isShowing())
						pDialog.dismiss();
			}
		}
	}


}