package eu.esponder.ESponderDriver.activities;

import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.adapters.FilesAdapter;
import eu.esponder.ESponderDriver.adapters.MenuAdapter;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.ESponderDriver.data.FRFileEntity;

public class Files extends SlidingActivity {

	//	private ImageView menuImage;
	private SlidingMenu sm;
	private ListView fileListView;
	private FilesAdapter filesAdapter;
	private FRFileEntity[] files;
	private FileReceiver receiver;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.alarmslayout);
		setTitle("ESponder - Files");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		// This function initializes the sliding menus to full functionality
		initializeMenu();

		TextView alarmsHeader = (TextView) findViewById(R.id.AlarmsHeader);
		alarmsHeader.setText("Received Files");

		findFRFilesForAdapter();
		fileListView = (ListView) findViewById(R.id.AlarmsList);
		filesAdapter = new FilesAdapter(Files.this, files);
		fileListView.setAdapter(filesAdapter);

	}


	//Create sample data for Meocs, FRCs, FRs
	// We need id of member, Title of member, type of member etc

	@Override
	public void onBackPressed() {
		Toast.makeText(Files.this, "Please use the menu if you want to navigate or exit the application...", Toast.LENGTH_SHORT).show();
	}

	private void initializeMenu() {

		//		setMenuButtonOnClickListener();

		View menuView = LayoutInflater.from(Files.this).inflate(R.layout.menu_frame, null);

		ListView menuList = (ListView) menuView.findViewById(R.id.MenuAsList);

		Resources res = getResources();
		String[] menuItems = res.getStringArray(R.array.frc_menu_items);

		MenuAdapter menuAdapter = new MenuAdapter(Files.this, R.layout.row_sidemenu, menuItems);

		menuList.setAdapter(menuAdapter);

		menuList.setOnItemClickListener(new OnItemClickListener() {

			SharedPreferences prefs = Files.this.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			Long frID = prefs.getLong(GlobalVar.spESponderUserID, -1);

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent;
				switch(position) {
				case 0:
					intent = new Intent(Files.this, MainActivity.class);
					startActivity(intent);
					break;
				case 1:
					intent = new Intent(Files.this, ActorDetails.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 2:
					intent = new Intent(Files.this, Actors.class);
					startActivity(intent);
					break;
				case 3:
					intent = new Intent(Files.this, Actions.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 4:
					intent = new Intent(Files.this, Files.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
					/*case 5:
					intent = new Intent(MainActivity.this, Settings.class);
					startActivity(intent);
					break;*/
				case 5:
					intent = new Intent(Intent.ACTION_MAIN);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					break;
				}

			}
		});

		setBehindContentView(menuView);


		sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);

		setSlidingActionBarEnabled(true);
	}

	//	private void setMenuButtonOnClickListener() {
	//
	//		menuImage = (ImageView) findViewById(R.id.menuButton);
	//		if(menuImage!=null) {
	//			menuImage.setOnClickListener(new OnClickListener() {
	//
	//				@Override
	//				public void onClick(View v) {
	//					if(sm!= null)
	//						sm.toggle(true);
	//				}
	//			});
	//		}
	//	}



	private void findFRFilesForAdapter() {

				List<FRFileEntity> alarmList = GlobalVar.db.getAllFiles();
				if( alarmList.size() > 0 ) {
					Iterator<FRFileEntity> it = alarmList.iterator();
					files = new FRFileEntity[alarmList.size()];
					int counter = 0;
					while(it.hasNext()) {
						files[counter] = it.next();
						counter++;
					}
				}
				else
					files = new FRFileEntity[1];

	}




	class FileReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			if(intent.getAction().equalsIgnoreCase(GlobalVar.FILE_POPUP)) {
				findFRFilesForAdapter();
				filesAdapter.setValues(files);
				filesAdapter.notifyDataSetChanged();	
			}	
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(receiver!=null)
			unregisterReceiver(receiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(receiver == null)
			receiver = new FileReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.FILE_POPUP);
		registerReceiver(receiver, filter);
		findFRFilesForAdapter();
		if(files != null) {
			filesAdapter.setValues(files);
			filesAdapter.notifyDataSetChanged();
		}

	}

}
