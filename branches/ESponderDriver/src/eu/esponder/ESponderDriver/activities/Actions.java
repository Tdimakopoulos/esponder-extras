package eu.esponder.ESponderDriver.activities;

import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.adapters.ActionObjectivesAdapter;
import eu.esponder.ESponderDriver.adapters.MenuAdapter;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.ESponderDriver.data.FRActionObjectiveEntity;

public class Actions extends SlidingActivity {

//	private ImageView menuImage;
	private SlidingMenu sm;
	private ListView actionListView;
	private ActionObjectivesAdapter apoAdapter;
	private FRActionObjectiveEntity[] apos;
	private Long frID;
//	private AlarmsReceiver receiver;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.actionslayout);
		setTitle("ESponder - Action Objectives");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		// This function initializes the sliding menus to full functionality
		initializeMenu();
		
		TextView actionsHeader = (TextView) findViewById(R.id.actionHeader);
		actionsHeader.setText("Action Objectives");
		
		if(getIntent().hasExtra("frID")) {
			
			frID = getIntent().getLongExtra("frID", -1);
			if(frID != -1) {
				String frTitle = GlobalVar.db.getFirstResponderTitle(frID);
				if(frTitle!=null)
					actionsHeader.setText("Action Objectives for "+frTitle);
				findObjectivesForAdapter();
				actionListView = (ListView) findViewById(R.id.apoList);
				apoAdapter = new ActionObjectivesAdapter(Actions.this, apos, frID);
				actionListView.setAdapter(apoAdapter);
			}
		}
		
	}


	//Create sample data for Meocs, FRCs, FRs
	// We need id of member, Title of member, type of member etc

	@Override
	public void onBackPressed() {
		Toast.makeText(Actions.this, "Please use the menu if you want to navigate or exit the application...", Toast.LENGTH_SHORT).show();
	}

	private void initializeMenu() {

//		setMenuButtonOnClickListener();

		View menuView = LayoutInflater.from(Actions.this).inflate(R.layout.menu_frame, null);

		ListView menuList = (ListView) menuView.findViewById(R.id.MenuAsList);

		Resources res = getResources();
		String[] menuItems = res.getStringArray(R.array.frc_menu_items);

		MenuAdapter menuAdapter = new MenuAdapter(Actions.this, R.layout.row_sidemenu, menuItems);

		menuList.setAdapter(menuAdapter);

		menuList.setOnItemClickListener(new OnItemClickListener() {

			SharedPreferences prefs = Actions.this.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			Long frID = prefs.getLong(GlobalVar.spESponderUserID, -1);

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent;
				switch(position) {
				case 0:
					intent = new Intent(Actions.this, MainActivity.class);
					startActivity(intent);
					break;
				case 1:
					intent = new Intent(Actions.this, ActorDetails.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 2:
					intent = new Intent(Actions.this, Actors.class);
					startActivity(intent);
					break;
				case 3:
					intent = new Intent(Actions.this, Actions.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 4:
					intent = new Intent(Actions.this, Alarms.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				/*case 5:
					intent = new Intent(MainActivity.this, Settings.class);
					startActivity(intent);
					break;*/
				case 5:
					intent = new Intent(Intent.ACTION_MAIN);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					break;
				}

			}
		});

		setBehindContentView(menuView);


		sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);

		setSlidingActionBarEnabled(true);
	}

//	private void setMenuButtonOnClickListener() {
//
//		menuImage = (ImageView) findViewById(R.id.menuButton);
//		if(menuImage!=null) {
//			menuImage.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					if(sm!= null)
//						sm.toggle(true);
//				}
//			});
//		}
//	}



	private void findObjectivesForAdapter() {
		
		if(frID != -1) {
			List<FRActionObjectiveEntity> apoList = GlobalVar.db.getAllObjectivesByFR(frID);
			if( apoList.size() > 0 ) {
				Iterator<FRActionObjectiveEntity> it = apoList.iterator();
				apos = new FRActionObjectiveEntity[apoList.size()];
				int counter = 0;
				while(it.hasNext()) {
					apos[counter] = it.next();
					counter++;
				}
			}
			else
				apos = new FRActionObjectiveEntity[1];
		}
		
	}
	
	
	
	
//	class AlarmsReceiver extends BroadcastReceiver {
//
//		@Override
//		public void onReceive(Context context, Intent intent) {
//			
//			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_ALARM_RECEIVED)) {
//				findFRAlarmsForAdapter();
//				alarmsAdapter.setValues(alarms);
//				alarmsAdapter.notifyDataSetChanged();	
//			}	
//		}
//	}


	@Override
	protected void onPause() {
		super.onPause();
//		if(receiver!=null)
//			unregisterReceiver(receiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
//		if(receiver == null)
//			receiver = new AlarmsReceiver();
//		IntentFilter filter = new IntentFilter(GlobalVar.INNER_ALARM_RECEIVED);
//		registerReceiver(receiver, filter);
	}

}
