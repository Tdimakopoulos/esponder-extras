package eu.esponder.ESponderDriver.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.adapters.MenuAdapter;
import eu.esponder.ESponderDriver.application.GlobalVar;

public class Settings extends SlidingActivity {
	
ImageView menuImage;

SlidingMenu sm;

	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settingslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
		setTitle("ESponder - Settings");
		
		initializeMenu();
		
	}
	
	@Override
	public void onBackPressed() {
		Toast.makeText(Settings.this, "Please use the menu if you want to navigate or exit the application...", Toast.LENGTH_SHORT).show();
	}
	
	
//	private void setMenuButtonOnClickListener() {
//
//		menuImage = (ImageView) findViewById(R.id.menuButton);
//		if(menuImage!=null) {
//			menuImage.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					if(sm!= null)
//						sm.toggle(true);
//				}
//			});
//		}
//	}
	
	
	private void initializeMenu() {

//		setMenuButtonOnClickListener();

		View menuView = LayoutInflater.from(Settings.this).inflate(R.layout.menu_frame, null);

		ListView menuList = (ListView) menuView.findViewById(R.id.MenuAsList);

		Resources res = getResources();
		String[] menuItems = res.getStringArray(R.array.frc_menu_items);

		MenuAdapter menuAdapter = new MenuAdapter(Settings.this, R.layout.row_sidemenu, menuItems);

		menuList.setAdapter(menuAdapter);

		menuList.setOnItemClickListener(new OnItemClickListener() {

			SharedPreferences prefs = Settings.this.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			Long frID = prefs.getLong(GlobalVar.spESponderUserID, -1);

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent;
				switch(position) {
				case 0:
					intent = new Intent(Settings.this, MainActivity.class);
					startActivity(intent);
					break;
				case 1:
					intent = new Intent(Settings.this, ActorDetails.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 2:
					intent = new Intent(Settings.this, Actors.class);
					startActivity(intent);
					break;
				case 3:
					intent = new Intent(Settings.this, Actions.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 4:
					intent = new Intent(Settings.this, Alarms.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 5:
					intent = new Intent(Settings.this, Settings.class);
					startActivity(intent);
					break;
				case 6:
					intent = new Intent(Intent.ACTION_MAIN);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					break;
				}

			}
		});

		setBehindContentView(menuView);


		sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);

		setSlidingActionBarEnabled(true);
	}
	

}
