package eu.esponder.ESponderDriver.activities;

import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.adapters.AlarmsAdapter;
import eu.esponder.ESponderDriver.adapters.MenuAdapter;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.ESponderDriver.data.FRAlarmEntity;

public class Alarms extends SlidingActivity {

//	private ImageView menuImage;
	private SlidingMenu sm;
	private ListView alarmListView;
	private AlarmsAdapter alarmsAdapter;
	private FRAlarmEntity[] alarms;
	private Long frID;
	private AlarmsReceiver receiver;

	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.alarmslayout);
		setTitle("ESponder - Alarms");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		// This function initializes the sliding menus to full functionality
		initializeMenu();
		
		TextView alarmsHeader = (TextView) findViewById(R.id.AlarmsHeader);
		alarmsHeader.setText("Alarms");
		
		if(getIntent().hasExtra("frID")) {
			
			frID = getIntent().getLongExtra("frID", -1);
			if(frID != -1) {
				
				String frTitle = GlobalVar.db.getFirstResponderTitle(frID);
				if(alarmsHeader!=null)
					alarmsHeader.setText("Alarms for "+frTitle);
				
				findFRAlarmsForAdapter();
				alarmListView = (ListView) findViewById(R.id.AlarmsList);
				alarmsAdapter = new AlarmsAdapter(Alarms.this, alarms);
				alarmListView.setAdapter(alarmsAdapter);	
			}
		}
		
	}


	//Create sample data for Meocs, FRCs, FRs
	// We need id of member, Title of member, type of member etc

	@Override
	public void onBackPressed() {
		Toast.makeText(Alarms.this, "Please use the menu if you want to navigate or exit the application...", Toast.LENGTH_SHORT).show();
	}

	private void initializeMenu() {

//		setMenuButtonOnClickListener();

		View menuView = LayoutInflater.from(Alarms.this).inflate(R.layout.menu_frame, null);

		ListView menuList = (ListView) menuView.findViewById(R.id.MenuAsList);

		Resources res = getResources();
		String[] menuItems = res.getStringArray(R.array.frc_menu_items);

		MenuAdapter menuAdapter = new MenuAdapter(Alarms.this, R.layout.row_sidemenu, menuItems);

		menuList.setAdapter(menuAdapter);

		menuList.setOnItemClickListener(new OnItemClickListener() {

			SharedPreferences prefs = Alarms.this.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			Long frID = prefs.getLong(GlobalVar.spESponderUserID, -1);

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent;
				switch(position) {
				case 0:
					intent = new Intent(Alarms.this, MainActivity.class);
					startActivity(intent);
					break;
				case 1:
					intent = new Intent(Alarms.this, ActorDetails.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 2:
					intent = new Intent(Alarms.this, Actors.class);
					startActivity(intent);
					break;
				case 3:
					intent = new Intent(Alarms.this, Actions.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 4:
					intent = new Intent(Alarms.this, Alarms.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				/*case 5:
					intent = new Intent(MainActivity.this, Settings.class);
					startActivity(intent);
					break;*/
				case 5:
					intent = new Intent(Intent.ACTION_MAIN);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					break;
				}

			}
		});

		setBehindContentView(menuView);


		sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(eu.esponder.ESponderDriver.R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);

		setSlidingActionBarEnabled(true);
	}

//	private void setMenuButtonOnClickListener() {
//
//		menuImage = (ImageView) findViewById(R.id.menuButton);
//		if(menuImage!=null) {
//			menuImage.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					if(sm!= null)
//						sm.toggle(true);
//				}
//			});
//		}
//	}



	private void findFRAlarmsForAdapter() {
		
		if(frID != null) {
			if(frID != -1) {
				List<FRAlarmEntity> alarmList = GlobalVar.db.getAllAlarmsForFR(frID);
				if( alarmList.size() > 0 ) {
					Iterator<FRAlarmEntity> it = alarmList.iterator();
					alarms = new FRAlarmEntity[alarmList.size()];
					int counter = 0;
					while(it.hasNext()) {
						alarms[counter] = it.next();
						counter++;
					}
				}
				else
					alarms = new FRAlarmEntity[1];
			}
			else
				Log.e("Populating the subordinates list for the chief","The chief is unknown");
		}
	}
	
	
	
	
	class AlarmsReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_ALARM_RECEIVED)) {
				findFRAlarmsForAdapter();
				alarmsAdapter.setValues(alarms);
				alarmsAdapter.notifyDataSetChanged();	
			}	
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(receiver!=null)
			unregisterReceiver(receiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(receiver == null)
			receiver = new AlarmsReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_ALARM_RECEIVED);
		registerReceiver(receiver, filter);
		findFRAlarmsForAdapter();
		if(alarms != null) {
			alarmsAdapter.setValues(alarms);
			alarmsAdapter.notifyDataSetChanged();
		}

	}

}
