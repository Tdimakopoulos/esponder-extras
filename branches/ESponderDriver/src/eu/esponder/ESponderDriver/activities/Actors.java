package eu.esponder.ESponderDriver.activities;

import java.util.Iterator;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.adapters.CrisisMemberAdapter;
import eu.esponder.ESponderDriver.adapters.MenuAdapter;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;

public class Actors extends SlidingActivity {

	private ImageView menuImage;
	private SlidingMenu sm;
	private ListView frListView;
	private CrisisMemberAdapter cmAdapter;
	private ActorFRALTDTO[] frs;
	private Long cuID;
	private ActorsReceiver receiver;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.actorslayout);
		setTitle("ESponder - FR Team Members");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		// This function initializes the sliding menus to full functionality
		initializeMenu();

		// Populates the list of subordinate first responders for the chief
		findFirstRepondersForAdapter();
		
		
		//	prepares the area that contains the chief's details summary
//		RelativeLayout chiefContainer = (RelativeLayout) findViewById(R.id.actorSmallDetailsRelativelayout);
//		chiefContainer.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				Toast.makeText(Actors.this, "clicked..", Toast.LENGTH_SHORT).show();
//			}
//		});
		
		
		//	prepares the area that contains the chief's details summary
		frListView = (ListView) findViewById(R.id.CrisisMemberList);
		cmAdapter = new CrisisMemberAdapter(Actors.this, frs);
		frListView.setAdapter(cmAdapter);
	}




	//Create sample data for Meocs, FRCs, FRs
	// We need id of member, Title of member, type of member etc

	@Override
	public void onBackPressed() {
		Toast.makeText(Actors.this, "Please use the menu if you want to navigate or exit the application...", Toast.LENGTH_SHORT).show();
	}

	private void initializeMenu() {

//		setMenuButtonOnClickListener();

		View menuView = LayoutInflater.from(Actors.this).inflate(R.layout.menu_frame, null);

		ListView menuList = (ListView) menuView.findViewById(R.id.MenuAsList);

		Resources res = getResources();
		String[] menuItems = res.getStringArray(R.array.frc_menu_items);

		MenuAdapter menuAdapter = new MenuAdapter(Actors.this, R.layout.row_sidemenu, menuItems);

		menuList.setAdapter(menuAdapter);

		menuList.setOnItemClickListener(new OnItemClickListener() {

			SharedPreferences prefs = Actors.this.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			Long frID = prefs.getLong(GlobalVar.spESponderUserID, -1);

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent;
				switch(position) {
				case 0:
					intent = new Intent(Actors.this, MainActivity.class);
					startActivity(intent);
					break;
				case 1:
					intent = new Intent(Actors.this, ActorDetails.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 2:
					intent = new Intent(Actors.this, Actors.class);
					startActivity(intent);
					break;
				case 3:
					intent = new Intent(Actors.this, Actions.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 4:
					intent = new Intent(Actors.this, Alarms.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				/*case 5:
					intent = new Intent(MainActivity.this, Settings.class);
					startActivity(intent);
					break;*/
				case 5:
					intent = new Intent(Intent.ACTION_MAIN);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					break;
				}

			}
		});

		setBehindContentView(menuView);


		sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);

		setSlidingActionBarEnabled(true);
	}

//	private void setMenuButtonOnClickListener() {
//
//		menuImage = (ImageView) findViewById(R.id.menuButton);
//		if(menuImage!=null) {
//			menuImage.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					if(sm!= null)
//						sm.toggle(true);
//				}
//			});
//		}
//	}



	private void findFirstRepondersForAdapter() {
		
		SharedPreferences prefs = getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
		cuID = prefs.getLong(GlobalVar.spESponderUserID, -1);
		
		if(cuID != -1) {
			List<ActorFRALTDTO> firstResponders = GlobalVar.db.getAllFirstRespondersByChief(cuID);

			if( firstResponders.size() > 0 ) {
				Iterator<ActorFRALTDTO> it = firstResponders.iterator();
				frs = new ActorFRALTDTO[firstResponders.size()];
				int counter = 0;
				while(it.hasNext()) {
					frs[counter] = it.next();
					counter++;
				}
			}
		}
		else
			Log.e("Populating the subordinates list for the chief","The chief is unknown");
		


	}
	
	
	
	
	class ActorsReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			
			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_ALARM_RECEIVED)) {
				findFirstRepondersForAdapter();
				cmAdapter.setValues(frs);
				cmAdapter.notifyDataSetChanged();	
			}	
		}
	}


	@Override
	protected void onPause() {
		super.onPause();
		if(receiver!=null)
			unregisterReceiver(receiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(receiver == null)
			receiver = new ActorsReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_ALARM_RECEIVED);
		registerReceiver(receiver, filter);
	}

}
