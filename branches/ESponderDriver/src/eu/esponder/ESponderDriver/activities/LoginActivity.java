package eu.esponder.ESponderDriver.activities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.ESponderDriver.data.FRActionObjectiveEntity;
import eu.esponder.ESponderDriver.handlers.LoginHandler;
import eu.esponder.ESponderDriver.runnables.MapRequestEventSender;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.login.LoginRequestEvent;

public class LoginActivity extends Activity {

	Button entryButton;
	LoginHandler loginHandler;
	TextView beText, sensorText, logintext;
	ProgressDialog pdialog;
	LoginReceiver loginReceiver;
	IntentFilter filter;
	Handler handler;
	Boolean loggedIn = false;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.loginlayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		setTitle("ESponder FRU Application");
		setLoginButtonOnClickListener();

		loginHandler = new LoginHandler(beText, sensorText, logintext);
		loginHandler.postDelayed(loginInitialiser, 500);


		createsampleActionParts();
		printObjectives((long) 4);

	}


	private void setLoginButtonOnClickListener(){
		((GlobalVar)LoginActivity.this.getApplicationContext()).setLoggedIn(true);
		this.entryButton = (Button) findViewById(R.id.login_button);
		this.entryButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this, MainActivityTablet.class);
				startActivity(intent);
			}
		});
	}


	private Runnable loginInitialiser = new Runnable() {

		@Override
		public void run() {
			pdialog = ProgressDialog.show(LoginActivity.this, "Log in attempt", "Sending login request to ESponder server...", false, true);
			createEventForLogin(LoginActivity.this);
		}
	};

	private void createEventForLogin(final Context context) {

		class LoginEventSender implements Runnable {
			@Override
			public void run() {
				Log.d("CREATE EVENT FOR LOGIN THREAD", "RUNNING, SENDING REQUEST");

				ESponderUserDTO user = new ESponderUserDTO();
				user.setId(Long.valueOf(0));

				LoginRequestEvent pEvent = new LoginRequestEvent();
				pEvent.setIMEI(((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
				pEvent.setPassword("Password");
				pEvent.setUsername("FRC");
				pEvent.setPkikey(retrievePKIKeyFromFile());
				pEvent.setiLoginType(1);
				pEvent.setEventAttachment(user);
				pEvent.setJournalMessage("Login request Username : "+pEvent.getUsername()+" PKI : "+pEvent.getPkikey());
				pEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
				pEvent.setEventTimestamp(new Date());
				pEvent.setEventSource(user);

				ObjectMapper mapper = new ObjectMapper();
				String jsonObject = null;
				try {
					jsonObject = mapper.writeValueAsString(pEvent);
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if(jsonObject != null) {
					Intent intent = new Intent(GlobalVar.LOGIN_REQUEST_INTENT_ACTION);
					intent.addCategory(GlobalVar.INTENT_CATEGORY);
					intent.putExtra("event", jsonObject);
					context.sendBroadcast(intent);
				}
				else
					Log.e("SEND LOGIN EVENT","SERIALIZATION HAS FAILED");

			}
		}

		Thread thread = new Thread(new LoginEventSender());
		thread.start();
	}


	private void createEventForMapInit(final Context context) {

		Thread thread = new Thread(new MapRequestEventSender(LoginActivity.this, "LoginActivity"));
		thread.start();
	}



	public class LoginReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_APPROVED)) {
				Log.e("Login receiver", "RUNNING LOGIN RESPONSE EVENT APPROVED");
				loggedIn = true;
				if(pdialog != null)
					if(pdialog.isShowing())
						pdialog.dismiss();

				Runnable requestMapPositions = new Runnable() {

					@Override
					public void run() {
						if(LoginActivity.this.pdialog.isShowing())
							LoginActivity.this.pdialog.dismiss();

						LoginActivity.this.pdialog = ProgressDialog.show(LoginActivity.this, 
								"Welcome First Responder", "\n\nRequesting information from the backend...", false, true);
						createEventForMapInit(LoginActivity.this);
					}
				};
				loginHandler.post(requestMapPositions);

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_DENIED)) {
				Log.e("Login receiver", "RUNNING LOGIN RESPONSE EVENT DENIED");
				if(pdialog != null)
					if(pdialog.isShowing())
						pdialog.dismiss();

				// Unsuccessful Login
				AlertDialog.Builder builder =new Builder(LoginActivity.this);
				builder.setCancelable(false);
				builder.setIcon(R.drawable.erroricon);
				builder.setTitle("Login Failed");
				builder.setMessage("Unauthorized Access Attempt, please retry...");
				builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						LoginActivity.this.finish();
					}
				});

				builder.create().show();

			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_ERROR)) {
				Log.e("Login receiver", "RUNNING LOGIN RESPONSE EVENT ERROR");
				if(pdialog != null)
					if(pdialog.isShowing())
						pdialog.dismiss();

				AlertDialog.Builder builder =new Builder(LoginActivity.this);
				builder.setCancelable(false);
				builder.setIcon(R.drawable.erroricon);
				builder.setTitle("Login Failed");
				builder.setMessage("Scrambled response from server, please contact your network administrator...");
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						LoginActivity.this.finish();
					}
				});

				builder.create().show();
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY)) {
				if(loggedIn) {
					Log.e("Login receiver", "RUNNING MAP RESPONSE EVENT APPROVED");
					// Data have been received successfully for the hierarchy
					if(LoginActivity.this.pdialog.isShowing())
						LoginActivity.this.pdialog.dismiss();

					// Enter the map main screen
					startActivity(new Intent(LoginActivity.this, MainActivityTablet.class));
				}
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR)) {
				Log.e("Login receiver", "RUNNING MAP RESPONSE EVENT ERROR");
				// Error came up while retrieving the map hierarchy
				if(LoginActivity.this.pdialog.isShowing())
					LoginActivity.this.pdialog.dismiss();


				AlertDialog.Builder builder =new Builder(LoginActivity.this);
				builder.setCancelable(false);
				builder.setIcon(R.drawable.erroricon);
				builder.setTitle("map Init Error");
				builder.setMessage("Error occured while retrieving the initial data for the map...");
				builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						LoginActivity.this.finish();
					}
				});

				builder.create().show();
			}

		}
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
	}


	@Override
	protected void onPause() {
		unregisterReceiver(loginReceiver);
		super.onPause();
	}


	@Override
	protected void onResume() {
		super.onResume();
		loginReceiver = new LoginReceiver();
		filter = new IntentFilter(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_APPROVED);
		filter.addAction(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_DENIED);
		filter.addAction(GlobalVar.INNER_LOGIN_RESPONSE_INTENT_ERROR);
		filter.addAction(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY);
		filter.addAction(GlobalVar.INNER_MAP_RESPONSE_HIERARCHY_ERROR);
		registerReceiver(loginReceiver, filter);
	}


	private String retrievePKIKeyFromFile() {

		String filename = "pkikey.txt";
		if(filename != null) {
			File sdcard = Environment.getExternalStorageDirectory();
			//Get the text file
			File file = new File(sdcard,filename);

			//Read text from file
			StringBuilder sbuilder = new StringBuilder();

			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;

				while ((line = br.readLine()) != null) {
					sbuilder.append(line);
				}
				br.close();
			}
			catch (IOException e) {
				Log.d("FILE_HANDLER", "Error occurred while opening file");
				e.printStackTrace();
			}
			String skey = sbuilder.toString();
			Log.e("PKI",skey);

			return skey;

		}
		else
			return null;
	}




	public void createsampleActionParts() {
		int counter = GlobalVar.db.getActionPartCount();
		Log.e("COUNT OF OBJECTIVES", String.valueOf(counter));
		if(counter == 0) {

			GlobalVar.db.insertActionPart(Long.valueOf(1), Long.valueOf(2), Long.valueOf(4), SeverityLevelDTO.SERIOUS.ordinal(), ActionOperationEnumDTO.MOVE.ordinal(), "FR-4-MOVE", new Date().getTime(), 0);

			GlobalVar.db.insertActionPart(Long.valueOf(2), Long.valueOf(2), Long.valueOf(5), SeverityLevelDTO.SERIOUS.ordinal(), ActionOperationEnumDTO.MOVE.ordinal(), "FR-5-MOVE", new Date().getTime(), 0);

			GlobalVar.db.insertActionPart(Long.valueOf(3), Long.valueOf(2), Long.valueOf(6), SeverityLevelDTO.SERIOUS.ordinal(), ActionOperationEnumDTO.MOVE.ordinal(), "FR-6-MOVE", new Date().getTime(), 0);

			GlobalVar.db.insertActionPartObjective(Long.valueOf(1), Long.valueOf(1), "FR-4-MOVE-LOCATION", new Date().getTime(), new Date().getTime(), 37.953557, 23.770702, (double) 0, 0);

			GlobalVar.db.insertActionPartObjective(Long.valueOf(1), Long.valueOf(2), "FR-5-MOVE-LOCATION", new Date().getTime(), new Date().getTime(), 37.953557, 23.770702, (double) 0, 0);

			GlobalVar.db.insertActionPartObjective(Long.valueOf(1), Long.valueOf(3), "FR-6-MOVE-LOCATION", new Date().getTime(), new Date().getTime(), 37.953557, 23.770702, (double) 0, 0);

			GlobalVar.db.insertActionPartObjective(Long.valueOf(1), Long.valueOf(1), "FR-4-MOVE-LOCATION-2", new Date().getTime(), new Date().getTime(), 37.986069,23.759544, (double) 0, 0);

			GlobalVar.db.insertActionPartObjective(Long.valueOf(1), Long.valueOf(2), "FR-5-MOVE-LOCATION-2", new Date().getTime(), new Date().getTime(), 37.986069,23.759544, (double) 0, 0);

			GlobalVar.db.insertActionPartObjective(Long.valueOf(1), Long.valueOf(3), "FR-6-MOVE-LOCATION-2", new Date().getTime(), new Date().getTime(), 37.986069,23.759544, (double) 0, 0);	
		}
		else
			Log.e("COUNT OF OBJECTIVES", "ACTIONS HAVE BEEN FOUND, NOT CREATING");


	}

	public void printObjectives (Long frID) {

		List<FRActionObjectiveEntity> list = GlobalVar.db.getAllObjectivesByFR(frID);
		if(list!= null)
			if(list.size()>0) {
				for(FRActionObjectiveEntity apo : list)
					Log.e("AOP",apo.getTitle());
			}
			else
				Log.e("APO","NO RESULTS FOUND");



	}


}
