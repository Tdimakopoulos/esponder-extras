package eu.esponder.ESponderDriver.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;

import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.adapters.MeasurementsAdapter;
import eu.esponder.ESponderDriver.adapters.MenuAdapter;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.ESponderDriver.data.FRSensorEntity;

public class ActorDetails extends SlidingActivity {

	ImageView menuImage;
	//	Boolean initParams = false;
	//	int category =  -1;
	//	String cmTitle = "Default";
	Long frID;
	SlidingMenu sm;
	LinearLayout actorLayout;
	LinearLayout actorDetailsLayout;
	ActorDetailsReceiver receiver;
	IntentFilter filter;
	Button alarmsButton, actionButton;

	FRSensorEntity[] measurements;
	ListView measurementsList;
	MeasurementsAdapter adapter;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.actordetailslayout);
		setTitle("ESponder - FR Details");
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		initializeMenu();

		Intent startingIntent = getIntent();
		frID = startingIntent.getLongExtra("frID", -1);

		if(frID != -1) {
			
			TextView frName = (TextView) findViewById(R.id.nameContents);
			if(frName!= null) {
				String frNameFromDB = GlobalVar.db.getFirstResponderTitle(frID);
				if(frNameFromDB!=null)
					frName.setText(frNameFromDB);
			}
			

			measurements = new FRSensorEntity[12];
			measurements =  GlobalVar.db.getLatestSensorMeasurementsByFR(frID, measurements);
			measurementsList = (ListView) findViewById(R.id.actordetailsmeasurementsListView);
			adapter = new MeasurementsAdapter(ActorDetails.this, measurements);
			measurementsList.setAdapter(adapter);
			
			alarmsButton = (Button) findViewById(R.id.alarmsButton);
			alarmsButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent goToAlarms = new Intent(ActorDetails.this, Alarms.class);
					goToAlarms.putExtra("frID", frID);
					startActivity(goToAlarms);
				}
			});
			
			actionButton = (Button) findViewById(R.id.actionsButton);
			actionButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Intent goToActions = new Intent(ActorDetails.this, Actions.class);
					goToActions.putExtra("frID", frID);
					startActivity(goToActions);
				}
			});
			
		}



	}

	private void initializeMenu() {

		View menuView = LayoutInflater.from(ActorDetails.this).inflate(R.layout.menu_frame, null);

		ListView menuList = (ListView) menuView.findViewById(R.id.MenuAsList);

		Resources res = getResources();
		String[] menuItems = res.getStringArray(R.array.frc_menu_items);

		MenuAdapter menuAdapter = new MenuAdapter(ActorDetails.this, R.layout.row_sidemenu, menuItems);

		menuList.setAdapter(menuAdapter);

		menuList.setOnItemClickListener(new OnItemClickListener() {

			SharedPreferences prefs = ActorDetails.this.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			Long frID = prefs.getLong(GlobalVar.spESponderUserID, -1);

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent;
				switch(position) {
				case 0:
					intent = new Intent(ActorDetails.this, MainActivity.class);
					startActivity(intent);
					break;
				case 1:
					intent = new Intent(ActorDetails.this, ActorDetails.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 2:
					intent = new Intent(ActorDetails.this, Actors.class);
					startActivity(intent);
					break;
				case 3:
					intent = new Intent(ActorDetails.this, Actions.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 4:
					intent = new Intent(ActorDetails.this, Alarms.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				/*case 5:
					intent = new Intent(MainActivity.this, Settings.class);
					startActivity(intent);
					break;*/
				case 5:
					intent = new Intent(Intent.ACTION_MAIN);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					break;
				}

			}
		});

		setBehindContentView(menuView);


		sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);

		setSlidingActionBarEnabled(true);
	}



	@Override
	protected void onDestroy() {
		super.onDestroy();
	}



	@Override
	protected void onPause() {
		if(receiver!=null)
			unregisterReceiver(receiver);
		super.onPause();
	}



	@Override
	protected void onResume() {
		super.onResume();
		receiver = new ActorDetailsReceiver();
		filter = new IntentFilter(GlobalVar.INNER_SENSORS_RECEIVED);
		registerReceiver(receiver, filter);
	}

	class ActorDetailsReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_SENSORS_RECEIVED)) {
				GlobalVar.db.getLatestSensorMeasurementsByFR(frID, measurements);
				adapter.notifyDataSetChanged();
			}
		}

	}


}
