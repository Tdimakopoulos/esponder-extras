package eu.esponder.ESponderDriver.activities;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.nutiteq.MapView;
import com.nutiteq.components.Components;
import com.nutiteq.components.MapPos;
import com.nutiteq.geometry.Marker;
import com.nutiteq.projections.EPSG3857;
import com.nutiteq.projections.Projection;
import com.nutiteq.rasterlayers.TMSMapLayer;
import com.nutiteq.style.LabelStyle;
import com.nutiteq.style.MarkerStyle;
import com.nutiteq.utils.UnscaledBitmapLoader;
import com.nutiteq.vectorlayers.GeometryLayer;
import com.nutiteq.vectorlayers.MarkerLayer;
import com.slidingmenu.lib.SlidingMenu;

import eu.esponder.ESponderDriver.R;
import eu.esponder.ESponderDriver.adapters.MenuAdapter;
import eu.esponder.ESponderDriver.animations.MeasCriticalAnimation;
import eu.esponder.ESponderDriver.application.GlobalVar;
import eu.esponder.ESponderDriver.data.DataManager;
import eu.esponder.ESponderDriver.data.MyLabel;
import eu.esponder.ESponderDriver.data.Poi;
import eu.esponder.ESponderDriver.data.PoiCategory;
import eu.esponder.ESponderDriver.maphelpers.MapEventListener;
import eu.esponder.ESponderDriver.maphelpers.MyLocationCircle;
import eu.esponder.ESponderDriver.runnables.AlarmAnimStopTask;
import eu.esponder.ESponderDriver.runnables.MapRequestEventSender;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;

public class MainActivityTablet extends Activity  {

	private MapView mapView;

	ImageView menuImage, centerMapImage;

	SlidingMenu sm;

	MapEventListener mapListener;

	MainMapReceiver receiver;

	Handler handler;

	Timer animTimer;

	ArrayList<MarkerLayer> markerLayerList;

	ArrayList<GeometryLayer> geometrylayerList;
	
	MyLocationCircle locationCircle;


	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.maintabletlayout);

		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		setTitle("ESponder FRC Application");

		handler = new Handler();

		animTimer = new Timer();

		markerLayerList = new ArrayList<MarkerLayer>();
		geometrylayerList = new ArrayList<GeometryLayer>();

		initializeMenu();

		// Enable Logging
		//		Log.enableAll();
		//		Log.setTag("OSMDemoApp");

		mapView = (MapView) findViewById(R.id.mapView);

		// Restore map state during devicse rotation,
		// it is saved in onRetainNonConfigurationInstance() below
		Components retainObject = (Components) getLastNonConfigurationInstance();
		if (retainObject != null) {
			// just restore configuration and update listener, skip other initializations
			mapView.setComponents(retainObject);
			mapListener = (MapEventListener) mapView.getOptions().getMapListener();
			mapListener.reset(this, mapView);
			mapView.startMapping();
			return;
		} else {
			// Create and set MapView components 
			mapView.setComponents(new Components());
		}

		DataManager.initInstance(this);

		// Initialize the map
		String MapServerURL = DataManager.getInstance().getMapServerURL();
		// Define base layer. Here we use MapQuest open tiles which are free to use
		// Almost all online maps use EPSG3857 projection.
		TMSMapLayer mapLayer = new TMSMapLayer(new EPSG3857(), 0, 18, 0,
				MapServerURL, "/", ".png");
		mapView.getLayers().setBaseLayer(mapLayer);

		// set initial map view camera - optional. "World view" is default 
		// NB! it must be in base layer projection (EPSG3857), so we convert it from lat and long		
		// zoom - 0 = world, like on most web maps
		//		mapView.setZoom((float)DataManager.getInstance().getInitialMapZoom());
		mapView.setZoom((float)17);
		// tilt means perspective view. Default is 90 degrees for "normal" 2D map view, minimum allowed is 30 degrees.
		mapView.setTilt(90.0f);

		// Activate some mapview options to make it smoother
		mapView.getOptions().setPreloading(true);
		mapView.getOptions().setSeamlessHorizontalPan(true);
		mapView.getOptions().setTileFading(true);
		mapView.getOptions().setKineticPanning(true);
		mapView.getOptions().setDoubleClickZoomIn(true);
		mapView.getOptions().setDualClickZoomOut(true);

		// configure texture caching - optional, suggested 
		mapView.getOptions().setTextureMemoryCacheSize(40 * 1024 * 1024);
		mapView.getOptions().setCompressedMemoryCacheSize(8 * 1024 * 1024);

		// define online map persistent caching - optional, suggested. Default - no caching
		mapView.getOptions().setPersistentCachePath(this.getDatabasePath("mapcache").getPath());
		// set persistent raster cache limit to 100MB
		mapView.getOptions().setPersistentCacheSize(100 * 1024 * 1024);

		// Create and set the layers
		createMapPOILayers(mapLayer);

		// add event listener
		mapListener = new MapEventListener(this, mapView);
		mapView.getOptions().setMapListener(mapListener);


		// start mapping
		mapView.startMapping();

		centerMapFunction();

		// Center around the position of the current ESponder user
		setCenterMapButtonOnClickListener();

		// Timer task to update the map
		TimerTask updatemaptask = new TimerTask() {

			@Override
			public void run() {
				Thread thread = new Thread(new MapRequestEventSender(MainActivityTablet.this, "Main Activity"));
				thread.start();
			}
		};

		//FIXME transform period to a user selectable value in the app settings screen
		if(GlobalVar.mapUpdateTimer == null) {
			int period = 30000;
			GlobalVar.mapUpdateTimer = new Timer();
			GlobalVar.mapUpdateTimer.scheduleAtFixedRate(updatemaptask, period, period);
		}

//		if(GlobalVar.pcdSensorTimer == null) {
//			GlobalVar.pcdSensorTimer = new Timer();
//			GlobalVar.pcdSensorTimer.scheduleAtFixedRate(new GetSensorsPCDTimerTask(MainActivityTablet.this.getApplicationContext()), 2000, 10000);
//		}
		
//		Long id = GlobalVar.db.insertFile("Passenger list for aircrash", 0, null, "http://62.169.248.67/mob/1.pdf");
//		Long id = GlobalVar.db.insertFile("Passenger list for aircrash", 0, null, "http://railsboxtech.com/singing/song/ishq_wala_instrument.mp3");
//		
//		if(id!=null)
//			Log.e("ID",id.toString());
//		else
//			Log.e("ID","NO ID GIVEN");
		
//		Uri data = Uri.parse("http://192.168.2.249/mob/1.pdf");
//		Intent dfIntent = new Intent(MainActivity.this, DownloaderService.class);
//		dfIntent.setData(data);
//		dfIntent.putExtra("urlpath", "http://192.168.2.249/mob/1.pdf");
//		dfIntent.putExtra("description", "Aircraft site");
//		startService(dfIntent);
		
		
	}


	private void initializeMenu() {

//		setMenuButtonOnClickListener();

//		View menuView = LayoutInflater.from(MainActivityTablet.this).inflate(R.layout.menu_frame, null);

		ListView menuList = (ListView) findViewById(R.id.applicationMenu);

		Resources res = getResources();
		String[] menuItems = res.getStringArray(R.array.frc_menu_items);

		MenuAdapter menuAdapter = new MenuAdapter(MainActivityTablet.this, R.layout.row_sidemenu, menuItems);

		menuList.setAdapter(menuAdapter);

		menuList.setOnItemClickListener(new OnItemClickListener() {
			
			SharedPreferences prefs = MainActivityTablet.this.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
			Long frID = prefs.getLong(GlobalVar.spESponderUserID, -1);

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent;
				switch(position) {
				case 0:
					intent = new Intent(MainActivityTablet.this, MainActivity.class);
					startActivity(intent);
					break;
				case 1:
					intent = new Intent(MainActivityTablet.this, ActorDetails.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 2:
					intent = new Intent(MainActivityTablet.this, Actors.class);
					startActivity(intent);
					break;
				case 3:
					intent = new Intent(MainActivityTablet.this, Actions.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				case 4:
					intent = new Intent(MainActivityTablet.this, Alarms.class);
					intent.putExtra("frID", frID);
					startActivity(intent);
					break;
				/*case 5:
					intent = new Intent(MainActivity.this, Settings.class);
					startActivity(intent);
					break;*/
				case 5:
					intent = new Intent(MainActivityTablet.this, Files.class);
					startActivity(intent);
					break;
				case 6:
					intent = new Intent(Intent.ACTION_MAIN);
					intent.addCategory(Intent.CATEGORY_HOME);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					break;
				}

			}
		});
		
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		switch (item.getItemId())
		{
		case R.id.action_settings:
			Toast.makeText(MainActivityTablet.this, "Settings is Selected", Toast.LENGTH_SHORT).show();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(receiver);
	}


	@Override
	protected void onResume() {
		super.onResume();
		receiver = new MainMapReceiver();
		IntentFilter filter = new IntentFilter(GlobalVar.INNER_MAP_UPDATE);
		filter.addAction(GlobalVar.INNER_ALARM_RECEIVED);
		registerReceiver(receiver, filter);
	}


	@Override
	public void onBackPressed() {
		Toast.makeText(MainActivityTablet.this, "Please use the menu if you want to navigate or exit the application...", Toast.LENGTH_SHORT).show();
	}


	//	private Runnable currentPositionFinder = new Runnable() {
	//		
	//		@Override
	//		public void run() {
	//			
	//			// UNCOMMENT THE NEXT 3 LINES TO SHOW GPS LOCATION
	//			MyLocationCircle locationCircle = new MyLocationCircle();
	//			mapListener.setLocationCircle(locationCircle);
	//			initGps(locationCircle);
	//		}
	//	};


	private void setCenterMapButtonOnClickListener() {
		centerMapImage = (ImageView) findViewById(R.id.centerMapImage);
		centerMapImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				centerMapFunction();
			}
		});
	}

	private void centerMapFunction() {

		class CenterMapRunnable implements Runnable {

			@Override
			public void run() {

				SharedPreferences prefs = MainActivityTablet.this.getSharedPreferences(GlobalVar.spName, Context.MODE_PRIVATE);
				Long currentUserID = prefs.getLong(GlobalVar.spESponderUserID, -1);

				if(currentUserID != null) {
					if(currentUserID != -1) {
						PointDTO centerPoint = GlobalVar.db.getFirstResponderLocationById(currentUserID);
						if(centerPoint != null) {
							mapView.setFocusPoint(mapView.getLayers().getBaseLayer().getProjection().
									fromWgs84(centerPoint.getLongitude().doubleValue(),centerPoint.getLatitude().doubleValue()));
							mapView.setZoom((float)17);
						}
						else {
							Looper.prepare();
							Toast.makeText(MainActivityTablet.this, "Unable to center map...", Toast.LENGTH_LONG).show();
							Looper.loop();
						}

					}
					else {
						Looper.prepare();
						Toast.makeText(MainActivityTablet.this, "Unable to center map...", Toast.LENGTH_LONG).show();
						Looper.loop();
					}
				}
				else
					mapView.setFocusPoint(mapView.getLayers().getBaseLayer().getProjection().
							fromWgs84(DataManager.getInstance().getInitialCenterLatitude(), DataManager.getInstance().getInitialCenterLongitude()));
			}
		}

		Thread thread = new Thread(new CenterMapRunnable());
		thread.start();
	}

	// For every POI category, one layer will be created. The POIs will then be added to the layer
	// and the layer will be added ArrayList<E>p
	public void createMapPOILayers(TMSMapLayer mapLayer) {

		final TMSMapLayer layer = mapLayer;

		class CreatePOILayersRunnable implements Runnable {

			@Override
			public void run() {

				//*****************************************************************************************************************

				//	FIXME	Add a circle for the position of the crisis
				
//				SharedPreferences prefs = MainActivity.this.getSharedPreferences(GlobalVar.spName, MODE_PRIVATE);
//				String ccLocation = prefs.getString(GlobalVar.spCrisisContextVar, "");
//				
//				if(!ccLocation.equalsIgnoreCase("")) {
//					
//					Log.e("",ccLocation);
//					if(locationCircle == null)
//						locationCircle = new MyLocationCircle();
//					
//					String[] location = ccLocation.split("a");
//					
//					locationCircle.setLocation(mapView.getLayers().getBaseLayer().getProjection(), new BigDecimal(location[0]).doubleValue(), new BigDecimal(location[1]).doubleValue());
//					locationCircle.setVisible(true);
//					mapListener.setLocationCircle(locationCircle);	
//					
//					
//				}
				
				

//				GeometryLayer gLayer = new GeometryLayer(layer.getProjection());
//				List<MapPos> mapPositionList = new ArrayList<MapPos>();
//				mapPositionList.add(new MapPos(gLayer.getProjection().fromWgs84((float)23.774918,(float)37.987283)));
//				mapPositionList.add(new MapPos(gLayer.getProjection().fromWgs84((float)23.777064,(float)37.985322)));
//				mapPositionList.add(new MapPos(gLayer.getProjection().fromWgs84((float)23.780347,(float)37.986353)));
//				mapPositionList.add(new MapPos(gLayer.getProjection().fromWgs84((float)23.777987,(float)37.988924)));
//				mapPositionList.add(new MapPos(gLayer.getProjection().fromWgs84((float)23.774918,(float)37.987283)));
//				PolygonStyle polygonStyle = PolygonStyle.builder().setColor(Color.argb(150, 255, 0, 47)).build();
//				StyleSet<PolygonStyle> polygonStyleSet = new StyleSet<PolygonStyle>(null);
//				polygonStyleSet.setZoomStyle(15, polygonStyle);
//				Polygon polygon = new Polygon(mapPositionList, new MyLabel("Malakia", "Malakia", (long) -1, false), polygonStyleSet, null);
//				gLayer.add(polygon);
//				geometrylayerList.add(gLayer);
//				mapView.getLayers().addLayer(gLayer);

				//*****************************************************************************************************************


				ArrayList<PoiCategory> categories = DataManager.getInstance().getCategories();

				LabelStyle style = LabelStyle.builder()
						.setTitleAlign(Align.CENTER).
						setTitleFont(Typeface.create("Arial", Typeface.BOLD), 26)
						.setDescriptionFont(Typeface.create("Arial", Typeface.NORMAL), 22)
						.setDescriptionAlign(Align.LEFT)
						.setDescriptionFont(Typeface.create("Arial", Typeface.NORMAL), 26)
						.build();

				for(int i = 0; i < categories.size(); i++) {

					ArrayList<Poi> pois  = new ArrayList<Poi>();

					if(categories.get(i).getName().equalsIgnoreCase("MEOC")) {

						Iterator<OCMeocALTDTO> it = GlobalVar.db.getAllMeocsForMap().iterator();
						while(it.hasNext()) {

							OCMeocALTDTO meoc = it.next();
							Poi poi = new Poi(i, Double.valueOf(((SphereDTO)meoc.getSnapshot().getLocationArea()).getCentre().getLongitude().doubleValue()),
									Double.valueOf(((SphereDTO)meoc.getSnapshot().getLocationArea()).getCentre().getLatitude().doubleValue()),
									"MEOC: "+meoc.getTitle(), "", meoc.getTitle(), meoc.getId());
							pois.add(poi);
						}
					}
					else if(categories.get(i).getName().equalsIgnoreCase("FRC")) {

						Iterator<ActorFRCALTDTO> it = GlobalVar.db.getAllFRChiefsForMap().iterator();
						while(it.hasNext()) {

							ActorFRCALTDTO frc = it.next();
							Poi poi = new Poi(i, Double.valueOf(((SphereDTO)frc.getSnapshot().getLocationArea()).getCentre().getLongitude().doubleValue()),
									Double.valueOf(((SphereDTO)frc.getSnapshot().getLocationArea()).getCentre().getLatitude().doubleValue()),
									"FRC: "+frc.getTitle(), "", frc.getTitle(), frc.getId());
							pois.add(poi);
						}

					}
					else if(categories.get(i).getName().equalsIgnoreCase("FR")) {

						Iterator<ActorFRALTDTO> it = GlobalVar.db.getAllFirstRespondersForMap().iterator();
						while(it.hasNext()) {

							ActorFRALTDTO fr = it.next();
							Poi poi = new Poi(i, Double.valueOf(((SphereDTO)fr.getSnapshot().getLocationArea()).getCentre().getLongitude().doubleValue()),
									Double.valueOf(((SphereDTO)fr.getSnapshot().getLocationArea()).getCentre().getLatitude().doubleValue()),
									"FR: "+fr.getTitle(), "", fr.getTitle(), fr.getId());
							pois.add(poi);
						}
					}

					if(pois.size() > 0) {

						if(i==0)
							Log.e("NUMBER OF MEOCS", String.valueOf(pois.size()));
						else if(i==1)
							Log.e("NUMBER OF FRC", String.valueOf(pois.size()));
						else if(i==2)
							Log.e("NUMBER OF FR", String.valueOf(pois.size()));

						// Create the Layer
						MarkerLayer markerLayer = new MarkerLayer(layer.getProjection());
						// Load POI bitmap
						AssetManager assets = getResources().getAssets();
						InputStream buf;
						Bitmap pointMarker = null;
						try {
							String imagePath = new String(categories.get(i).getMarkerBitmap());
							buf = new BufferedInputStream((assets.open(new String(imagePath))));
							pointMarker = BitmapFactory.decodeStream(buf);
						} catch (IOException e) {
							e.printStackTrace();
						}

						for(int j = 0; j < pois.size(); j++) {
							Poi poi = pois.get(j);

							//POI Bitmap
							MarkerStyle markerStyle;
							// If something went wrong (e.g. bitmap missing) it defaults to the black marker
							if(pointMarker != null){
								markerStyle = MarkerStyle.builder().setBitmap(pointMarker).setSize(0.6f).setColor(Color.WHITE).build();
							}
							else{
								pointMarker = UnscaledBitmapLoader.decodeResource(getResources(), R.drawable.marker_black);
								markerStyle = MarkerStyle.builder().setBitmap(pointMarker).setSize(0.6f).setColor(Color.WHITE).build();
							}

							// POI Text
							//							MyLabel markerLabel = new MyLabel(poi.getLabelTitle(), poi.getLabelText(), poi.getPoiCategoryCode(), poi.getObjectTitle());

							Boolean isFR;
							if(poi.getPoiCategoryCode()!=0)
								isFR = true;
							else
								isFR = false;

							MyLabel markerLabel = new MyLabel(poi.getLabelTitle(), poi.getLabelText(), style, poi.getObjectID(), isFR);

							// POI Location
							MapPos markerLocation = layer.getProjection().fromWgs84(poi.getLongitude(), poi.getLatitude());

							// Add it to the marker layer
							markerLayer.add(new Marker(markerLocation, markerLabel, markerStyle, null));
						}
						markerLayerList.add(markerLayer);
						mapView.getLayers().addLayer(markerLayer);
					}
				}


			}
		}

		Thread runnable = new Thread(new CreatePOILayersRunnable());
		runnable.start();
	}


	protected void initGps(final MyLocationCircle locationCircle) {
		final Projection proj = mapView.getLayers().getBaseLayer().getProjection();

		LocationListener locationListener = new LocationListener() 
		{
			public void onLocationChanged(Location location) {
				if (locationCircle != null) {
					locationCircle.setLocation(proj, location);
					locationCircle.setVisible(true);
				}
			}

			public void onStatusChanged(String provider, int status, Bundle extras) {}

			public void onProviderEnabled(String provider) {}

			public void onProviderDisabled(String provider) {}
		};

		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);	        
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 100, locationListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
	}


	public class MainMapReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context ctx, Intent intent) {

			if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_MAP_UPDATE)) {

				for(MarkerLayer m : markerLayerList) {
					mapView.getLayers().removeLayer(m);
				}

				for(GeometryLayer gm : geometrylayerList) {
					mapView.getLayers().removeLayer(gm);
				}

				markerLayerList.clear();
				geometrylayerList.clear();

				createMapPOILayers((TMSMapLayer) mapView.getLayers().getBaseLayer());

				//*****************************************************************************************************************
			}
			else if(intent.getAction().equalsIgnoreCase(GlobalVar.INNER_ALARM_RECEIVED)) {
				if(intent.hasExtra("frID")) {
					final long frID = intent.getLongExtra("frID", -1);
					if(frID!=-1) {
						Log.e("anim", "anim received");
						final ImageView alarmIcon = (ImageView)findViewById(R.id.mapAlarmIndicator);
						alarmIcon.setVisibility(View.VISIBLE);
						Animation anim = new MeasCriticalAnimation(0.0f, 1.0f);
						alarmIcon.startAnimation(anim);
						animTimer.schedule(new AlarmAnimStopTask(alarmIcon), 8000);
						alarmIcon.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								Log.e("ALARM","IMAGE PRESSED");
								Intent alarmIntent= new Intent(v.getContext(), Alarms.class);
								alarmIntent.putExtra("frID", frID);
								v.getContext().startActivity(alarmIntent);
							}
						});
					}
				}
			}

		}

	}


}