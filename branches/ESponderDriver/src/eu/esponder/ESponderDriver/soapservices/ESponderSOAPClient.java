package eu.esponder.ESponderDriver.soapservices;

import java.io.IOException;
import java.util.Date;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

public class ESponderSOAPClient {

	private Context context;

	String serverAddress = "";

	public ESponderSOAPClient(Context context, String address) {
		
		this.context = context;
		if(address == null || address == "")
			serverAddress = "178.63.69.149:80";
		else
			this.serverAddress = address;
	}

	public String executeWeightRequest(String weightData) {

		String nameSpace = "http://weight.nephron.eu/";
		String URL = "http://" + serverAddress + "/SmartPhoneCommunicationManager/WeightManager";
		String soapAction = "http://weight.nephron.eu/WeightManager/WeightManagerNewValueReceived";
		String methodName = "WeightManagerNewValueReceived";

		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();

		SoapObject request = new SoapObject(nameSpace, methodName);

		request.addProperty("imei", deviceID);
		request.addProperty("date", String.valueOf(new Date().getTime()));
		request.addProperty("reply", weightData);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				if(response.toString().equalsIgnoreCase("OK"))
					return response.toString();
				else
					return null;
			}

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			Log.d("SOAP XML PARSER EXCEPTION", "SOAP XML PARSER EXCEPTION");
			e.printStackTrace();
			return null;
		}
		return null;
	}


	public String executeUploadFileRequest(byte[] fileContents, Long length, Long date) {

		String nameSpace = "http://ws.file.nephron.eu/";
		String URL = "http://" + serverAddress + "/NephronFileAttachments/NephronECGManagerWS";
		String soapAction = "http://ws.file.nephron.eu/NephronECGManagerWS/ECGTransfer";
		String methodName = "ECGTransfer";

		SoapObject request = new SoapObject(nameSpace, methodName);
		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();

		request.addProperty("ECG", fileContents);

		request.addProperty("IMEI", deviceID);
		request.addProperty("Date", date);
		request.addProperty("Length", length);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		new MarshalBase64().register(envelope);   //serialization
		envelope.encodingStyle = SoapEnvelope.ENC;

		envelope.bodyOut = request;
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				return response.toString();
			}

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}	

}