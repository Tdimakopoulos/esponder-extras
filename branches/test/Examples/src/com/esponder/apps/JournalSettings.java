package com.esponder.apps;
 
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox; 
import android.widget.Toast;

public class JournalSettings extends Activity {

	SharedPreferences jernalSettings;
	SharedPreferences.Editor prefsEditor;
	CheckBox  locationMeasurementsEvents, sensorMeasurementsEvents, crisisContextEvents, updateCrisisContextEvents, actionEvents;
	 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jernalsettings);

		locationMeasurementsEvents = (CheckBox) findViewById(R.id.locationMeasurementsEvents);
		sensorMeasurementsEvents = (CheckBox) findViewById(R.id.sensorMeasurementsEvents); 
		crisisContextEvents = (CheckBox) findViewById(R.id.crisisContextEvents); 
		updateCrisisContextEvents = (CheckBox) findViewById(R.id.updateCrisisContextEvents); 
		actionEvents = (CheckBox) findViewById(R.id.actionEvents); 

		jernalSettings = this.getSharedPreferences("jernalSettings",
				MODE_WORLD_READABLE);
		prefsEditor = jernalSettings.edit();

		locationMeasurementsEvents.setChecked(jernalSettings.getBoolean("locationMeasurementsEvents", false));
		sensorMeasurementsEvents.setChecked(jernalSettings.getBoolean("sensorMeasurementsEvents", false));
		crisisContextEvents.setChecked(jernalSettings.getBoolean("crisisContextEvents", false));
		updateCrisisContextEvents.setChecked(jernalSettings.getBoolean("updateCrisisContextEvents", false));
		actionEvents.setChecked(jernalSettings.getBoolean("actionEvents", false));
		
	} 

	// Click on the Measurements button of Main
	public void clickHandler(View view) {

		switch (view.getId()) {
		case R.id.SaveSettingsButton:
			prefsEditor.putBoolean("locationMeasurementsEvents", locationMeasurementsEvents.isChecked());
			prefsEditor.putBoolean("sensorMeasurementsEvents", sensorMeasurementsEvents.isChecked()); 
			prefsEditor.putBoolean("crisisContextEvents", crisisContextEvents.isChecked()); 
			prefsEditor.putBoolean("updateCrisisContextEvents", updateCrisisContextEvents.isChecked()); 
			prefsEditor.putBoolean("actionEvents", actionEvents.isChecked()); 
			prefsEditor.commit();
			Toast.makeText(JournalSettings.this,
				 	   "Your changes have been saved", Toast.LENGTH_LONG).show();
			break;
		case R.id.CancelSettingsButton:
			Log.d("CancelSettingsButton", "CancelSettingsButton");
		    Intent intent = new Intent(JournalSettings.this, Journal.class);
     		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        	finish();
			break;

		default:
			break;
		}

	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	    	 Intent intent = new Intent(JournalSettings.this, Journal.class);
	     		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            startActivity(intent);
	            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
	        	finish();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}

}
