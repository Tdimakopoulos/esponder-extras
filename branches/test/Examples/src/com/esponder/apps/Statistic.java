package com.esponder.apps;

import eu.esponder.fru.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;

public class Statistic {

	private String from, to, measurementTitle;
 	private Class<? extends SensorDTO> classObject;
 	private MeasurementStatisticTypeEnum statisticType;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getMeasurementTitle() {
		return measurementTitle;
	}

	public void setMeasurementTitle(String measurementTitle) {
		this.measurementTitle = measurementTitle;
	}
	
	public Class<? extends SensorDTO> getClassName() {
		return classObject;
	}

	public void setClassName(Class<? extends SensorDTO> classObject) {
		this.classObject = classObject;
	}
	
	public MeasurementStatisticTypeEnum getStatisticType() {
		return statisticType;
	}

	public void setStatisticType(MeasurementStatisticTypeEnum statisticType) {
		this.statisticType = statisticType;
	}
	
}
