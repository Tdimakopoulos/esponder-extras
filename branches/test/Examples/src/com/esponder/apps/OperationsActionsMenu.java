package com.esponder.apps;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.widget.ListView;
import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionDTO;
import eu.esponder.jsonrcp.JSONRPCException;

public class OperationsActionsMenu extends Activity {

 
	private ListView actorListView;
	private List<ActionDTO> actionsList;
	private OperationActionsMenuListAdapter adapterList;
	private Iterator iteratorSubordinates;
	AlertDialog.Builder builder;
	AlertDialog info ;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operationsmenu);
		actorListView = (ListView) findViewById(R.id.MessageListView);
		actionsList = new ArrayList<ActionDTO>();
		adapterList = new OperationActionsMenuListAdapter(this); 
		actorListView.setAdapter(adapterList);
		
       
   	ESponderEntityDTO[] allActions = null;
	try {
		allActions = (ESponderEntityDTO[])Menu.client.call("esponderdb/getAllESponderDTOs", 
		          ESponderEntityDTO[].class, ActionDTO.class.getName());
	} catch (JSONRPCException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
            
	     if(allActions != null){
	       for(int i=0;i<allActions.length;i++){
            	 actionsList.add((ActionDTO) allActions[i]);
             }
	     } else{
	    		builder = new AlertDialog.Builder(this);
	        	builder
				.setMessage(" EventAdmin is not Active. ")
				.setPositiveButton("Ok", null);
	        	 info = builder.create();
	        	 info.show();
	     }
	    	   
	       
		  
		  ShowOrder((List<ActionDTO>) actionsList);
	}

	public void ShowOrder(final List<ActionDTO> actor) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.addOrder(actor);
				adapterList.notifyDataSetChanged();
			}
		});
	}

	public void ClearOrder() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapterList.clear();
				adapterList.notifyDataSetChanged();
			}
		});
	}
	
	@Override
	public void onResume()
	{  super.onResume();
	   ClearOrder();
	
		ESponderEntityDTO[] allActions = null;
		try {
			allActions = (ESponderEntityDTO[])Menu.client.call("esponderdb/getAllESponderDTOs", 
			          ESponderEntityDTO[].class, ActionDTO.class.getName());
		} catch (JSONRPCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	            
		     if(allActions != null){
		       for(int i=0;i<allActions.length;i++){
	            	 actionsList.add((ActionDTO) allActions[i]);
	             }
		     } else{
		    		builder = new AlertDialog.Builder(this);
		        	builder
					.setMessage(" EventAdmin is not Active. ")
					.setPositiveButton("Ok", null);
		        	 info = builder.create();
		        	 info.show();
		     }
		 ShowOrder((List<ActionDTO>) actionsList);   
	}
	
}