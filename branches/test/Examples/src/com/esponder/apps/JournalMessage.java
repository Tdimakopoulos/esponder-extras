package com.esponder.apps;

import eu.esponder.fru.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.fru.event.model.ESponderEvent;

public class JournalMessage {

	private String message;
	private ESponderEvent event;
	private SeverityLevelDTO severity;

	public String getMessage() {
		return message;
	}

	public SeverityLevelDTO getSeverity() {
		return severity;
	}

	public ESponderEvent getEvent() {
		return event;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setSeverity(SeverityLevelDTO type) {
		this.severity = type;
	}

	public void setEvent(ESponderEvent event) {
		this.event = event;
	}
}
