package com.esponder.apps;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import eu.esponder.fru.JSONParser;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import eu.esponder.fru.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.fru.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TableMeasurements extends Activity {

	boolean flag = true;
	public static Map<String, String> frsmeauserements1;
	public static Map<String, String> frsmeauserements2;
	public static Map<String, String> frsmeauserements3;

	public static Map<String, Map<String, String>> typemeauserements;
	public static Map<String, Map<String, Map<String, String>>> meauserementsMap;
	Iterator iteratorEquipment,iteratorSensors,iteratorStatisticType,iteratorSubordinates,iteratorSubordinatesSensors;
	List<String> statisticType,actroID;
	
	private String filter = CreateSensorMeasurementStatisticEvent.class
			.getName();
	private IncomingReceiver receiver;
	IntentFilter intentFilter;
	LinearLayout sensorslayout,typeslayout,headerslayout,measurementslayout,linearlayout,dynamic;
	TextView senTextView,typosHead,head;
	HorizontalScrollView horizontal_scroll_view;
	int j=0;
	AlertDialog.Builder builder;
	AlertDialog info ;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.graphs);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		dynamic = (LinearLayout) findViewById(R.id.dynamic);
   	    sensorslayout = (LinearLayout) findViewById(R.id.Sensors);
	    typeslayout = (LinearLayout) findViewById(R.id.Types);
	 	headerslayout = (LinearLayout) findViewById(R.id.header);
	 	measurementslayout = (LinearLayout) findViewById(R.id.Meauserements);
	    horizontal_scroll_view = (HorizontalScrollView) findViewById(R.id.horizontal_scroll_view);

		JSONParser jpa = new JSONParser();
		ActorDTO actor = jpa.getActor();
		// String actor = jpa.get("",this);
		Log.d("Title", "title = " + actor.getEquipmentSet());
		Log.d("Title", "actor = " + actor);
		
		receiver = new IncomingReceiver();
	    intentFilter = new IntentFilter();
		
		 meauserementsMap = new HashMap<String, Map<String, Map<String, String>>>();
		 actroID = new ArrayList<String>(); 
		 
		iteratorEquipment = actor.getEquipmentSet().iterator();  
		iteratorSensors = ((EquipmentDTO)iteratorEquipment.next()).getSensors().iterator();
	    actroID.add(Long.toString(actor.getId()));
	    updateMap(iteratorSensors,actor);
	    
		if(actor.getSubordinates()!=null){
			intentFilter.addAction(filter +"."+actor.getId());
			iteratorSubordinates =  actor.getSubordinates().iterator(); 	
			  while(iteratorSubordinates.hasNext()){
	  	 		  actor = (ActorDTO)iteratorSubordinates.next();
	  			  iteratorEquipment = actor.getEquipmentSet().iterator();
	  			  iteratorSensors = ((EquipmentDTO)iteratorEquipment.next()).getSensors().iterator();
	  			  actroID.add(Long.toString(actor.getId()));
	  			  updateMap(iteratorSensors,actor);
	  	 	  } 
		}else{
			 intentFilter.addAction(filter +"."+actor.getSupervisor().getId());
		}
     Log.d("To diko mas einai"," action = " + intentFilter.getAction(0) );
		// Get Map in Set interface to get key and value
		Set s = meauserementsMap.entrySet();

		// Move next key and value of Map by iterator
		Iterator it = s.iterator();
		render(it);

		intentFilter.addCategory("org.osgi.service.event.EventAdmin");
		registerReceiver(receiver, intentFilter);

		/*Thread sender = new Sender(this);
		sender.start();*/
		
		
		//  Get  SensorMeasurementStatisticDTO from DB
/*		SensorMeasurementStatisticDTO statisticMeasurement = new SensorMeasurementStatisticDTO();
		System.out.println("yoyoyoyoy = " + statisticMeasurement.getClass().getName());*/
		
		
		/*ESponderEntityDTO[] allStatistics = null;
		try {
			allStatistics = (ESponderEntityDTO[])Menu.client.call("esponderdb/getAllESponderDTOs", 
			          ESponderEntityDTO[].class, SensorMeasurementStatisticDTO.class.getName());
		} catch (JSONRPCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 if(allStatistics != null){
	          for (int i = 0; i < allStatistics.length; i++) {
	        	  if( ((SensorMeasurementStatisticDTO)allStatistics[i]).getStatistic() instanceof ArithmeticSensorMeasurementDTO ) {
	        		    System.out.println("To id tou einai = "+ allStatistics[i].getId() +" All SENSORs FROM DB : " + (  ((ArithmeticSensorMeasurementDTO)((SensorMeasurementStatisticDTO)allStatistics[i]).getStatistic())).getMeasurement()  );       	        
	        	  }  else if(  ((SensorMeasurementStatisticDTO)allStatistics[i]).getStatistic() instanceof LocationSensorMeasurementDTO ){
	        	
	        		  PointDTO point =   ((LocationSensorMeasurementDTO)((SensorMeasurementStatisticDTO)allStatistics[i]).getStatistic()).getPoint();
	        		  
	      		       System.out.println("To id tou einai = "+ allStatistics[i].getId() +" All SENSORs FROM DB : lat = " + point.getLatitude() + " lon "+ point.getLongitude());       	        
	       	  }
	           }
		 }else {
				builder = new AlertDialog.Builder(this);
	        	builder
				.setMessage(" EventAdmin is not Active. ")
				.setPositiveButton("Ok", null);
	        	 info = builder.create();
	        	 info.show();
		 }*/
		
	 /*     //    Log.d("sizezzzz"," to size tou ="+allSensors.length);
	          try {
				Menu.client.call("esponderdb/deleteAllESponderDTOs", 
				         null, statisticMeasurement.getClass().getName());
			} catch (JSONRPCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
	           
		
	 	
	}

	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
				if (intent.hasExtra("event")) {
					
					String measurementEncelope = intent.getStringExtra("event");
					ObjectMapper mapper = new ObjectMapper();
					mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
					JsonFactory jsonFactory = new JsonFactory();
					try {
						JsonParser jp = jsonFactory.createJsonParser(measurementEncelope);
						// get the event
						CreateSensorMeasurementStatisticEvent event = mapper
								.readValue(jp,CreateSensorMeasurementStatisticEvent.class);
						
						// extract attachment
						SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticEnvelope = event
								.getEventAttachment();
	
					          
						for (SensorMeasurementStatisticDTO statisticMeauserement : sensorMeasurementStatisticEnvelope.getMeasurementStatistics()) {
						 	SensorDTO sensor = statisticMeauserement.getStatistic().getSensor();
                 		    BigDecimal value = ((ArithmeticSensorMeasurementDTO)statisticMeauserement.getStatistic()).getMeasurement();
                 		    DecimalFormat myFormatter = new DecimalFormat("##.##");
                 		    String output = myFormatter.format(value);
                 		    String statisticType =  statisticMeauserement.getStatisticType().toString();
                 		    Long id =  event.getEventSource().getId();
                 			Log.d(" plirofories "," sensoras = " + sensor.getLabel() + " value = "+value+  " showValue = " + myFormatter.format(value) + " Statistic Type = "+statisticType);
     				     	
                 			if(meauserementsMap.containsKey(sensor.getLabel())){
                 				Log.d("bike 1 1 1  "," bike 1 1 1 ");
                 				if(meauserementsMap.get(sensor.getLabel()).containsKey(statisticType)){
                 					Log.d("bike 2 2 2  "," bike 2 2 2 2");
                 					 meauserementsMap.get(sensor.getLabel()).get(statisticType).put(Long.toString(id), myFormatter.format(value)); 
                 				}
    						}
						}
						
				/*	//	SensorDTO sensoras = sensorMeasurementDTO.getSensor();

					//	BigDecimal lat = sensorMeasurementDTO.getMeasurement();
						if(meauserementsMap.containsKey(key)){
							
						}
					    meauserementsMap.get("BdTemp").get("MEANeee").put("1", "5"); 
*/

						measurementslayout.removeAllViews();
						rerender(meauserementsMap.entrySet().iterator()); 
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
		}

	}
	
	public void render(Iterator it){
		  for(int i=0;i<actroID.size();i++){
		    	head = new TextView(this);
				head.setText(actroID.get(i));
				head.setTextSize(22);
				head.setWidth(60);
				head.setTextColor(Color.GREEN);
				headerslayout.addView(head);
		    }
	while (it.hasNext()) {
		 
		// key=value separator this by Map.Entry to get key and value
		Map.Entry m = (Map.Entry) it.next();
		// getKey is used to get key of Map
		String key = (String) m.getKey();

		Map map2 = (Map) m.getValue();
		Set s2 = map2.entrySet();
		Iterator it2 = s2.iterator();
		while (it2.hasNext()) {
			Map.Entry m2 = (Map.Entry) it2.next();
			// getKey is used to get key of Map
			String keys = (String) m2.getKey();

			typosHead = new TextView(this);
			typosHead.setText(keys);
			typosHead.setTextSize(22);
			typeslayout.addView(typosHead);

			senTextView = new TextView(this);
			senTextView.setText(key);
			senTextView.setTextSize(22);
			sensorslayout.addView(senTextView);

			Map map3 = (Map) m2.getValue();
			Set s3 = map3.entrySet();
			Iterator it3 = s3.iterator();
			Iterator it4 = s3.iterator();
		    linearlayout = new LinearLayout(this);
 	    
 		    String[] a = new String[actroID.size()];
		    for(int i=0;i<a.length;i++){
		    	 a[i]="";
		    	  }
		    
			while (it4.hasNext()) {
				Map.Entry m3 = (Map.Entry) it4.next();
				String keye = (String) m3.getKey();
				for(int i=0;i<actroID.size();i++){
					if(keye.equals(actroID.get(i))){
						a[i]=(String) m3.getValue();
					}
				}}
			
				for(int i=0;i<a.length;i++){
				//String frs = (String) m3.getValue();
				TextView measuerements = new TextView(this);
				measuerements.setText(a[i]);
				measuerements.setTextSize(22);
				measuerements.setWidth(60);
				
				linearlayout.addView(measuerements);
			}
			//measurementslayout.removeView(linearlayout);
			
			measurementslayout.addView(linearlayout);
		//	linearlayout.removeAllViews();
		}
	}
	 
	}

	
	public void rerender(Iterator it){
		while (it.hasNext()) {
			 
			// key=value separator this by Map.Entry to get key and value
			Map.Entry m = (Map.Entry) it.next();
			// getKey is used to get key of Map
			String key = (String) m.getKey();

			Map map2 = (Map) m.getValue();
			Set s2 = map2.entrySet();
			Iterator it2 = s2.iterator();
			while (it2.hasNext()) {
				Map.Entry m2 = (Map.Entry) it2.next();
				// getKey is used to get key of Map
				String keys = (String) m2.getKey();
    
				Map map3 = (Map) m2.getValue();
				Set s3 = map3.entrySet();
				Iterator it3 = s3.iterator();
				Iterator it4 = s3.iterator();
			    linearlayout = new LinearLayout(this);
				 
			   
			    String[] a = new String[actroID.size()];
			    for(int i=0;i<a.length;i++){
			    	 a[i]="";
			    	  }
			    
				while (it4.hasNext()) {
					Map.Entry m3 = (Map.Entry) it4.next();
					String keye = (String) m3.getKey();
					for(int i=0;i<actroID.size();i++){
						if(keye.equals(actroID.get(i))){
							a[i]=(String) m3.getValue();
						}
					}}
				
					for(int i=0;i<a.length;i++){
					//String frs = (String) m3.getValue();
					TextView measuerements = new TextView(this);
					measuerements.setText(a[i]);
					measuerements.setTextSize(22);
					measuerements.setWidth(60);
					
					linearlayout.addView(measuerements);
				} 
				
				measurementslayout.addView(linearlayout);
		 
			}
		}
		
	}
	
	public void updateMap(Iterator it,ActorDTO actor){
		  while (it.hasNext()){ 

    		  SensorDTO sensor = (SensorDTO)it.next();
     	      statisticType = new ArrayList<String>(); 
		      iteratorStatisticType = sensor.getStatisticsConfig().iterator();
		       
		      while (iteratorStatisticType.hasNext()){ 
	 	    	  statisticType.add(((StatisticsConfigDTO)iteratorStatisticType.next()).getMeasurementStatisticType().toString());
		      }
		      for(int k=0;k<statisticType.size();k++){
		    	  Log.d("statustoctype","statustoctype  "+ statisticType.get(k)+"einai k= "+k); 		    	 
		      }
			  
			  if(!meauserementsMap.containsKey(sensor.getLabel())){
				  typemeauserements = new HashMap<String, Map<String, String>>();
				  for(int k=0;k<statisticType.size();k++){
					  frsmeauserements1 = new HashMap<String, String>();
					  frsmeauserements1.put(Long.toString(actor.getId()), "-");
					  typemeauserements.put(statisticType.get(k), frsmeauserements1); 	    	 
			      } 
				  meauserementsMap.put(sensor.getLabel(), typemeauserements);
			  }
			  else{
				  for(int k=0;k<statisticType.size();k++){
				  if(!meauserementsMap.get(sensor.getLabel()).containsKey(statisticType.get(k))){
					 frsmeauserements1 = new HashMap<String, String>();
					 frsmeauserements1.put(Long.toString(actor.getId()), "-");
					 typemeauserements = meauserementsMap.get(sensor.getLabel());
					 typemeauserements.put(statisticType.get(k), frsmeauserements1);
	 				 }
				 else{
					 frsmeauserements1 = meauserementsMap.get(sensor.getLabel()).get(statisticType.get(k));
					 frsmeauserements1.put(Long.toString(actor.getId()), "-");
				 }}				  
				  
			  }
		  }  
	}
	
	public void onDestroy() {
		super.onDestroy();
		if (5 > 1) {
			unregisterReceiver(receiver);
		}
	}

}

 