package com.esponder.apps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap; 
import java.util.Iterator;
import java.util.List; 
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize; 
import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.fru.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.fru.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.fru.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.fru.event.model.ESponderEvent;
import eu.esponder.fru.event.model.snapshot.action.UpdateActionPartSnapshotEvent; 
import eu.esponder.fru.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;
import eu.esponder.jsonrcp.JSONRPCException;
import android.app.AlertDialog; 
import android.widget.BaseAdapter; 
import android.widget.Button;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


class OperationsListAdapter extends BaseAdapter {

	private List<ActionPartDTO> items = new ArrayList<ActionPartDTO>(); 
	private List<TextView> TextViews = new ArrayList<TextView>(); 
	private LayoutInflater inflater;
	private Context context;
	private Button button;
	private TextView actionName,messages,status;
	private CharSequence[] choices = {"STARTED", "PAUSED", "COMPLETED","CANCELED"};
	private AlertDialog.Builder menu,builder;
	Iterator actionPartIterator,actionPartObjectivesIterator, usedReusuableResourcesIterator; 
	private Long actionId;
	HashMap<Long,Integer> mp = new HashMap<Long,Integer>(); 
	int id;
	ESponderEvent<ActionPartSnapshotDTO>  updateActionPartSnapshotEvent;
	ObjectMapper eventMapper;
	String eventToJson;
	Intent intent;
	ActionDTO action = null;
	
	 
	public OperationsListAdapter(Context context,int id) {
		this.context =  context;
		inflater = LayoutInflater.from(context);
		this.id = id;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		
		View v = inflater.inflate(R.layout.operationlistview, null);
		  
		final ActionPartDTO actionPart = items.get(position);
		if (actionPart != null) { 
			 
			try {
				action = (ActionDTO) Menu.client.call("esponderdb/getESponderDTO",
				         ESponderEntityDTO.class, actionPart.getActionID(), ActionDTO.class.getName());
			} catch (JSONRPCException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		 
			 String actionToString ="";
				if(id == 1){
					  actionId = actionPart.getActionID(); 
			    	  if(!mp.containsKey(actionId)){
			    		 mp.put(actionId, position);
			    	  }
			    	  actionName = (TextView) v.findViewById(R.id.action);
			    	  if(mp.get(actionId) == position){
			    		  actionName.setVisibility(View.VISIBLE);
			    		  actionName.setText(action.getTitle()); 
			    	  }
			  		}
				else if(id == 2){
					actionToString = "Who : " +  actionPart.getActor().getTitle() +"\n";
				}
	    	  actionToString = actionToString+"Operation : " + actionPart.getActionOperation().toString() + "\nWhat : ";
	    	  actionPartObjectivesIterator   = actionPart.getActionPartObjectives().iterator(); 
	    	  usedReusuableResourcesIterator = actionPart.getUsedReusuableResources().iterator();
	    	  
	    	 
	    	   while (actionPartObjectivesIterator.hasNext()){
	    		 actionToString = actionToString +" " + ((ActionPartObjectiveDTO) actionPartObjectivesIterator.next()).getConsumableResources().iterator().next().getTitle() + "\n";
	     	  }
	    	  actionToString = actionToString +"How : ";
	    	  while (usedReusuableResourcesIterator.hasNext()){
	    		  actionToString = actionToString +" " +  ((ReusableResourceDTO) usedReusuableResourcesIterator.next()).getTitle();
	    	  }
	    	 
	    	 
	     
		 messages = (TextView) v.findViewById(R.id.message);
	     messages.setText(actionToString +"  "+ actionPart.getId());
         status = (TextView) v.findViewById(R.id.status); 
         status.setText("Status : " + actionPart.getSnapshot().getStatus().toString());
	     button = (Button) v.findViewById(R.id.update);
	     TextViews.add(status);
	     
 
	     button.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) { 
					Log.d(" button "," button "+position);
					Log.d(" sizesize "," sizesize "+TextViews.size());
					
					 menu = new AlertDialog.Builder(view.getContext());
				     menu.setTitle("Select Status");
				     menu.setItems(choices, new DialogInterface.OnClickListener() {
				         public void onClick(DialogInterface dialog, int item) {
				        	 Log.d("Pthsame "," Pathsame = " +  choices[item]);
			             	//  status.setText("Status: "+choices[item]);
			             	  Log.d(" possssitionnnnnn "," positionsnnn "+position);
			             	 // Log.d(" possssitionnnnnn "," id tou actionPart "+actionPart.getId());
			             	  Log.d(" ston pinaka "," ston pinaka  "+TextViews.get(position));
			             	 ((TextView)TextViews.get(position)).setText("Status : "+choices[item]);
			             	 
			             	 
			             	 // Send Update ActionPartSnapshot Event
			             	ActionPartSnapshotDTO actionPartSnapshot = new ActionPartSnapshotDTO();
			             	actionPartSnapshot.setStatus(ActionPartSnapshotStatusDTO.getActionPartSnapshotStatusEnum(item)); 
			             	actionPartSnapshot.setActionPart(actionPart);
				        	 
				            updateActionPartSnapshotEvent = new UpdateActionPartSnapshotEvent();
				            updateActionPartSnapshotEvent.setEventSeverity(SeverityLevelDTO.MEDIUM);
				            updateActionPartSnapshotEvent.setEventTimestamp(new Date());
				            updateActionPartSnapshotEvent.setEventAttachment(actionPartSnapshot);
				            updateActionPartSnapshotEvent.setEventSource(Menu.actor);
				            
				             eventMapper = new ObjectMapper(); 
		 		        	 eventMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				        	 eventMapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
				    	 	 eventToJson = null ;
				    	 	 eventMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				    	          try {
				    	        	  eventToJson = eventMapper.writeValueAsString(updateActionPartSnapshotEvent);
				    	  		} catch (JsonGenerationException e) {
				    	  			// TODO Auto-generated catch block
				    	  			e.printStackTrace();
				    	  		} catch (JsonMappingException e) {
				    	  			// TODO Auto-generated catch block
				    	  			e.printStackTrace();
				    	  		} catch (IOException e) {
				    	  			// TODO Auto-generated catch block
				    	  			e.printStackTrace();
				    	  		}
				    	          
				     	 	  intent = new Intent(UpdateActionPartSnapshotEvent.class.getName() + "." + Menu.actor.getId());
				    	 	  intent.addCategory("org.osgi.service.event.EventAdmin");
				    	 	  intent.putExtra("event", eventToJson); 
				    	 	  context.sendBroadcast(intent); 
				    	 	  Log.d("steilameeeeeeee"," steilameeeeeeeee = "+ intent.getAction() + " gia action = "+updateActionPartSnapshotEvent.getEventAttachment().getActionPart().getActionID());
			             	 
				    	 		try {
				    				action = (ActionDTO) Menu.client.call("esponderdb/getESponderDTO",
				    				         ESponderEntityDTO.class, actionPart.getActionID(), ActionDTO.class.getName());
				    			} catch (JSONRPCException e1) {
				    				// TODO Auto-generated catch block
				    				e1.printStackTrace();
				    			}
				    	 	 actionPartIterator = action.getActionParts().iterator();
				    	 	 while (actionPartIterator.hasNext()){
				    	 		ActionPartDTO actionPartTemp = (ActionPartDTO) actionPartIterator.next();
				    	 		Log.d(" ta idia einai "," is = "+actionPartTemp.getId()+" kai "+actionPart.getId());
				    	 		if(actionPartTemp.getId() == actionPart.getId() ){
				    	 			actionPartTemp.getSnapshot().setStatus(ActionPartSnapshotStatusDTO.getActionPartSnapshotStatusEnum(item));
				    	 		}
				    	 	 }
				    	 	   
				              try {
				            	Log.d(" before "," before ");
								Menu.client.call("esponderdb/updateESponderDTO", null, action);
								Log.d(" after "," after ");
							} catch (JSONRPCException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				              
				              builder = new AlertDialog.Builder(context);
				    	      builder.setMessage("Your Request has been send")
				    		       .setCancelable(false)
				    		       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				    		           public void onClick(DialogInterface dialog, int id) {
				    		           }
				    		       }).show(); 
				              
		 		         }
				     }); 
					
					menu.show();
				}

			});
	     
		}
	 

		return v;

	}

	public void onClick(View arg0) {
		messages.setText("My text on click");  
	    }
	
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	public ActionPartDTO getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void addOrder(List<ActionPartDTO> message) {
		this.items = message;
	}

	public void clear() {
		this.items.clear();
	}

}
