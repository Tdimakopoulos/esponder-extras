package com.dynamicview;

import eu.esponder.fru.event.model.ESponderEvent;

public class JournalMessage {

	String message, type;
	ESponderEvent event;

	public String getMessage() {
		return message;
	}

	public String getType() {
		return type;
	}

	public ESponderEvent getEvent() {
		return event;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setEvent(ESponderEvent event) {
		this.event = event;
	}
}
