package com.dynamicview;
 

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
 
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;

public class CustomItemizedOverlay extends ItemizedOverlay<OverlayItem> {
   
   private ArrayList<OverlayItem> mapOverlays = new ArrayList<OverlayItem>();
   
   private Context context;
   
   public CustomItemizedOverlay(Drawable defaultMarker) {
        super(boundCenterBottom(defaultMarker));
   }
   
   public CustomItemizedOverlay(Drawable defaultMarker, Context context) {
        this(defaultMarker);
        this.context = context;
   }

   @Override
   protected OverlayItem createItem(int i) {
      return mapOverlays.get(i);
   }

   @Override
   public int size() {
      return mapOverlays.size();
   }
   
   @Override
   protected boolean onTap(int index) {
      OverlayItem item = mapOverlays.get(index);
      AlertDialog.Builder dialog = new AlertDialog.Builder(context);
      dialog.setTitle(item.getTitle());
      dialog.setMessage(item.getSnippet());
      dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
       	    
          }
      });
      dialog.show();
      return true;
   }
   
   protected boolean remove(String title){
	   for(int i=0;i<mapOverlays.size();i++){
		   if( mapOverlays.get(i).getTitle().equals(title)){
			   mapOverlays.remove(i);
		   }
	   }
	  
	   return true;
   }
   /*
   protected boolean update(int index,int lon,int lat) {
      OverlayItem item = mapOverlays.get(index);
      GeoPoint point1 = new GeoPoint(lat, lon);
      OverlayItem overlayitem = new OverlayItem(point1, "Frnew", "Frnew");
      mapOverlays.add(index, overlayitem);
      return true;
   }*/
   
   public void addOverlay(OverlayItem overlay) {
      mapOverlays.add(overlay);
       this.populate();
   }

}
