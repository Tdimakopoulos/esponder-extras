package com.dynamicview;

import java.util.Iterator;
import java.util.List;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import eu.esponder.fru.JSONParser;
import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO; 
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.fru.event.model.snapshot.measurement.CreateLocationSensorMeasurementEvent;
import eu.esponder.fru.event.model.snapshot.measurement.statistic.CreateLocationMeasurementStatisticEvent;
import eu.esponder.jsonrcp.JSONRPCClient;
import eu.esponder.jsonrcp.JSONRPCException;


public class Map extends MapActivity {

	private MapView mapView;

	private static final int latitudeR1 = 37988578;
	private static final int longitudeR1 = 23768063;

	private static final int latitudeR2 = 37989432;
	private static final int longitudeR2 = 23776217;

	private static final int latitudeR3 = 37989026;
	private static final int longitudeR3 = 23764586;

	private static final int latitudeChief = 37988756;
	private static final int longitudeChief = 23767623;
	Long myId;

	MapController mapController;
	GeoPoint current;
	List<Overlay> mapOverlays;
	CustomItemizedOverlay myOverlay,othersOverlay;
	Drawable myDrawable,othersDrawable;
	Iterator iteratorSubordinates;
	private String filter = CreateLocationMeasurementStatisticEvent.class
			.getName();
	private IncomingReceiver receiver;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);

		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);

		mapOverlays = mapView.getOverlays();

		receiver = new IncomingReceiver();

		othersDrawable = this.getResources().getDrawable(R.drawable.marker2);
		myDrawable = this.getResources().getDrawable(R.drawable.marker);

		JSONParser jpa = new JSONParser();
		ActorDTO actor = jpa.getActor();
		myId = actor.getId();
		Log.d("to id mou einai!!","einai = " + myId);

		myOverlay = new CustomItemizedOverlay(myDrawable, this);
		othersOverlay = new CustomItemizedOverlay(othersDrawable, this);

		IntentFilter intentFilter = new IntentFilter();
		 
		
		if(actor.getSubordinates()!=null){
			intentFilter.addAction(filter +"."+actor.getId());
	/*	  iteratorSubordinates =  actor.getSubordinates().iterator(); 	
			  while(iteratorSubordinates.hasNext()){
	  	 		  actor = (ActorDTO)iteratorSubordinates.next();
	  	 	      intentFilter.addAction(filter  +"."+ actor.getId());
	  	 	  }  */
		}else{
			 intentFilter.addAction(filter +"."+actor.getSupervisor().getId());
		/*	 iteratorSubordinates= actor.getSupervisor().getSubordinates().iterator();
			  while(iteratorSubordinates.hasNext()){
	  	 		  actor = (ActorDTO)iteratorSubordinates.next();
	  	 	      intentFilter.addAction(filter  +"."+ actor.getId());
	  	 	  }  */
		}
		
	   // intentFilter.addAction("eu.esponder.fru.event.model.snapshot.measurement.statistic.CreateLocationMeasurementStatisticEvent.4"); 
		intentFilter.addCategory("org.osgi.service.event.EventAdmin");
		registerReceiver(receiver, intentFilter);
		// Log.d("prwto","einai " + intentFilter.getAction(0));
      /*  Log.d("einai ta actions","einai "+intentFilter.countActions());
        Log.d("prwto","einai " + intentFilter.getAction(0));
        Log.d("deutrero","einai " + intentFilter.getAction(1));
        Log.d("trito","einai " + intentFilter.getAction(2));*/
       // Log.d("tetarto","einai " + intentFilter.getAction(3));
		
		
		/*
		 * GeoPoint current = new GeoPoint(latitudeChief, longitudeChief);
		 * OverlayItem overlayitem2_1 = new OverlayItem(current,
		 * "I am here!","Me"); chiefOverlay.addOverlay(overlayitem2_1);
		 */

      /*  GeoPoint point1 = new GeoPoint(37943578, 23732063); 
        OverlayItem overlayitem = new OverlayItem(point1, "id = "+myId, "Name = FRC #5555");
        myOverlay.addOverlay(overlayitem);*/
        
		/*
		 * GeoPoint point1 = new GeoPoint(latitudeR1, longitudeR1); GeoPoint
		 * point2 = new GeoPoint(latitudeR2, longitudeR2); GeoPoint point3 = new
		 * GeoPoint(latitudeR3, longitudeR3);
		 * 
		 * OverlayItem overlayitem = new OverlayItem(point1, "Fr1", "Fr1");
		 * OverlayItem overlayitem2 = new OverlayItem(point2, "Fr2", "Fr2");
		 * OverlayItem overlayitem3 = new OverlayItem(point3, "Fr3", "Fr3");
		 * 
		 * frsOverlay.addOverlay(overlayitem);
		 * frsOverlay.addOverlay(overlayitem2);
		 * frsOverlay.addOverlay(overlayitem3);
		 */

        if (myOverlay.size()!=0){
			mapOverlays.add(myOverlay);
		}
        
        if (othersOverlay.size()!=0){
			mapOverlays.add(othersOverlay);
		}

	 
		mapController = mapView.getController();

	//	mapController.animateTo(current);
		mapController.setZoom(15);

		Thread s = new Sender(this);
		s.start();

	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.routing:
			Intent routingSelection = new Intent(Map.this,
					RoutingSelection.class);
			Bundle bundle = new Bundle();
			bundle.putInt("Latitude", latitudeChief);
			bundle.putInt("Longitude", longitudeChief);
			routingSelection.putExtra("currentpos", bundle);
			startActivity(routingSelection);
			break;
		case R.id.settings:
			Toast.makeText(this, "You pressed the setting!", Toast.LENGTH_LONG)
					.show();
			break;
		}
		return true;
	}

	@Override
	public void onRestart() {
		super.onRestart();
		mapController.animateTo(current);
	}

	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
				if (intent.hasExtra("event")) {
					String position = intent.getStringExtra("event");
					ObjectMapper mapper = new ObjectMapper();
					mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
					JsonFactory jsonFactory = new JsonFactory();
					try {
						Log.d("locationa","locationaaaa");
						JsonParser jp = jsonFactory.createJsonParser(position);
						// get the event
						CreateLocationMeasurementStatisticEvent event = mapper
								.readValue(jp,CreateLocationMeasurementStatisticEvent.class);
						// extract attachment
						SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticListDTO = event
								.getEventAttachment();
  
						LocationSensorMeasurementDTO  sensorMeasurementDTO =   (LocationSensorMeasurementDTO) sensorMeasurementStatisticListDTO.getMeasurementStatistics().get(0).getStatistic();
						
						/*int lat = sensorMeasurementDTO.getPoint().getLatitude()
								.intValue();
						int lon = sensorMeasurementDTO.getPoint()
								.getLongitude().intValue();

						Log.d("yessssssss", "Incomeing lat = " + lat + " lon ="
								+ lon);*/

						/*GeoPoint updatedPoint = new GeoPoint(lat, lon);*/
						GeoPoint updatedPoint = new GeoPoint((int)(sensorMeasurementDTO.getPoint().getLongitude().doubleValue()  * 1E6),(int)(sensorMeasurementDTO.getPoint().getLatitude().doubleValue() * 1E6));
						Log.d("yessssssss", "Gia ton Actor "+ event.getEventSource().getId()+"  Incomeing lat = " + (int)(sensorMeasurementDTO.getPoint().getLatitude().doubleValue() * 1E6) + " lon ="
								+ (int)(sensorMeasurementDTO.getPoint().getLongitude().doubleValue()  * 1E6)+" xaxaxaxa = " + event.getEventSourceStr() + event.getJournalMessageInfo());
					 
						Long tempId =   event.getEventSource().getId();
						OverlayItem updatedItem = new OverlayItem(updatedPoint,
								"id = " + tempId , "Name = " + event.getEventSource().getTitle() );
					    if(myId == tempId){
					    	myOverlay.remove("id = "+Long.toString(tempId));
							myOverlay.addOverlay(updatedItem);
							mapView.getOverlays().add(myOverlay);
							mapController.animateTo(updatedPoint);
					    }else{
					    	othersOverlay.remove("id = "+Long.toString(tempId));
					    	othersOverlay.addOverlay(updatedItem);
							mapView.getOverlays().add(othersOverlay);
					    }
						 
					//	mapView.getOverlays().remove(frsOverlay);
						mapView.invalidate();

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
		}

	}
	
	public void onDestroy() {
		super.onDestroy();
	   unregisterReceiver(receiver); 
	}

}
