package com.dynamicview;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import eu.esponder.fru.dto.model.crisis.action.ActionDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.fru.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.fru.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import eu.esponder.fru.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.fru.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.fru.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.fru.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.fru.dto.model.snapshot.location.PointDTO;
import eu.esponder.fru.dto.model.snapshot.location.SphereDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnum;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.fru.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;
import eu.esponder.fru.event.model.ESponderEvent;
import eu.esponder.fru.event.model.snapshot.measurement.CreateArithmeticSensorMeasurementEvent;
import eu.esponder.fru.event.model.snapshot.measurement.CreateLocationSensorMeasurementEvent;
import eu.esponder.fru.event.model.snapshot.measurement.statistic.CreateLocationMeasurementStatisticEvent;
import eu.esponder.fru.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.fru.event.model.snapshot.resource.SensorSnapshotEvent;


class Sender extends Thread 
{
    Context context;
    public Sender(Context ctx)
    {
        context = ctx;
    }

    public void run()
    {
    	int count=0;
    	while(true){
    	try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  
    	
    	//   ----------------------------- Start Send Location  ------------------------------
    	//  Resourse o Sensor
		LocationSensorDTO sensorDTO = new LocationSensorDTO(); 
	    sensorDTO.setMeasurementUnit(MeasurementUnitEnum.DEGREES);
	    sensorDTO.setName("LPS");
	    sensorDTO.setType("LPS");
	    sensorDTO.setTitle("Local Positioning Sensor");
	    sensorDTO.setId(System.currentTimeMillis());
	    sensorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
	    
	    //  Resourse o Actor
		ActorDTO subactorDTO= new ActorDTO();
	   	subactorDTO.setId(new Long(4));
	   	subactorDTO.setType("FRC 5555");
	   	subactorDTO.setTitle("FRC #5555");
	   	subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
	    
	    PointDTO pointDTO = new PointDTO();
	    pointDTO.setAltitude(new BigDecimal(220000));
	    pointDTO.setLongitude(new BigDecimal(23762063));
	    pointDTO.setLatitude(new BigDecimal(37983578));
	    
	    
	    LocationSensorMeasurementDTO sensorData = new LocationSensorMeasurementDTO();
	    sensorData.setPoint(pointDTO);
	    
	    
	    CreateLocationSensorMeasurementEvent event = new CreateLocationSensorMeasurementEvent();
	    event.setEventSeverity(SeverityLevelDTO.MEDIUM);
	    event.setEventTimestamp(new Date());
	    event.setEventAttachment(sensorData);
	    event.setEventSource(subactorDTO);
	   
        ObjectMapper  mapper = new ObjectMapper();
	    mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
        mapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
        String json = null ;
	    mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
         try {
 		    json = mapper.writeValueAsString(event);
 			Log.d("xaxaxs","Elaaaa!212!  "+json);
 		} catch (JsonGenerationException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		} catch (JsonMappingException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		} catch (IOException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
         Log.d("xaxaxs","Elaaaa!1111!  "+json);
         
     	Bundle locaBa = new Bundle();
     	locaBa.putString("event", json);  
	    Intent intLoc = new Intent(CreateLocationSensorMeasurementEvent.class.getName()+".4"); 
	    intLoc.putExtra("event", json); 
	  //  context.sendBroadcast(intLoc);
       // Log.d("o sender","o sender "+CreateLocationSensorMeasurementEvent.class.getName()+".1");
         

         // second
         //  Resourse o Actor
 	    subactorDTO= new ActorDTO();
 	   	subactorDTO.setId(new Long(1));
 	   	subactorDTO.setType("FRC 2222");
 	   	subactorDTO.setTitle("FRC #2222");
 	   	subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
 	    
 	    pointDTO = new PointDTO();
 	    pointDTO.setAltitude(new BigDecimal(220000));
 	    pointDTO.setLongitude(new BigDecimal(23764586));
 	    pointDTO.setLatitude(new BigDecimal(37989026));
 	    
 	    
 	    sensorData = new LocationSensorMeasurementDTO();
 	    sensorData.setPoint(pointDTO);
 	    
 	    
 	    event = new CreateLocationSensorMeasurementEvent();
 	    event.setEventSeverity(SeverityLevelDTO.MEDIUM);
 	    event.setEventTimestamp(new Date());
 	    event.setEventAttachment(sensorData);
 	    event.setEventSource(subactorDTO);
 	   
           mapper = new ObjectMapper();
 	    mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
         mapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
          json = null ;
 	    mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          try {
  		    json = mapper.writeValueAsString(event);
  			Log.d("xaxaxs","Elaaaa!212!  "+json);
  		} catch (JsonGenerationException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (JsonMappingException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
          Log.d("xaxaxs","Elaaaa!1111!  "+json);
          
      	 locaBa = new Bundle();
      	locaBa.putString("event", json);  
 	     intLoc = new Intent(CreateLocationSensorMeasurementEvent.class.getName()+".4"); 
 	    intLoc.putExtra("event", locaBa); 
 //	 	context.sendBroadcast(intLoc);
          Log.d("o sender","o sender "+CreateLocationSensorMeasurementEvent.class.getName()+".4");
         
         
    //   ----------------------------- End Send  Location  ------------------------------
         
         
         
	 	BodyTemperatureSensorDTO bodyTemperatureSensor = new BodyTemperatureSensorDTO();
		bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnum.DEGREES_CELCIUS);
		bodyTemperatureSensor.setName("BodyTemp");
		bodyTemperatureSensor.setType("BodyTemp");
		bodyTemperatureSensor.setLabel("BdTemp");
		bodyTemperatureSensor.setTitle("Body Temperature Sensor");
		bodyTemperatureSensor.setId(System.currentTimeMillis());
		bodyTemperatureSensor.setStatus(ResourceStatusDTO.AVAILABLE);
		
		ArithmeticSensorMeasurementDTO sensorMeasurement1 = new ArithmeticSensorMeasurementDTO();
		sensorMeasurement1.setMeasurement(new BigDecimal(200));
		sensorMeasurement1.setTimestamp(new Date());
	 
		
		    CreateArithmeticSensorMeasurementEvent eventArethmitic = new CreateArithmeticSensorMeasurementEvent();
		    eventArethmitic.setEventSeverity(SeverityLevelDTO.MEDIUM);
		    eventArethmitic.setEventTimestamp(new Date());
	     	eventArethmitic.setEventAttachment(sensorMeasurement1);
	     	eventArethmitic.setEventSource(bodyTemperatureSensor);
	     	
	     	
	        ObjectMapper  mapperArethmetic = new ObjectMapper();
	     	mapperArethmetic.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
	     	mapperArethmetic.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
	       
	 	    mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	          try {
	  			  json = mapper.writeValueAsString(eventArethmitic);
	  			Log.d("xaxaxs","Elaaaa!212!  "+json);
	  		} catch (JsonGenerationException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		} catch (JsonMappingException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		} catch (IOException e) {
	  			// TODO Auto-generated catch block
	  			e.printStackTrace();
	  		}
	          
	          
	      	Bundle arethmiticBa= new Bundle();
	      	arethmiticBa.putString("event", json); 
	  	    Intent intArethmitic = new Intent(CreateArithmeticSensorMeasurementEvent.class.getName()); 
	    	intArethmitic.putExtra("event", arethmiticBa); 
	 	// 	context.sendBroadcast(intArethmitic);
	 	 	
	 	 	
	 		 arethmiticBa= new Bundle();
	      	arethmiticBa.putString("event", json); 
	  	     intArethmitic = new Intent("com.dynamicview.Jernal"); 
	    	intArethmitic.putExtra("event", arethmiticBa); 
	 	// 	context.sendBroadcast(intArethmitic);
	    	
	    	// ----------------------------- 	Send Envelope With Statistics ------------------------------
	      
		
	    	
	    	ArithmeticSensorMeasurementDTO  arithmeticSensorMeasurement = new ArithmeticSensorMeasurementDTO();
	    	arithmeticSensorMeasurement.setMeasurement(new BigDecimal(810 + count));
	    	arithmeticSensorMeasurement.setTimestamp(new Date());
	    	arithmeticSensorMeasurement.setSensor(bodyTemperatureSensor);

	    	 SensorMeasurementStatisticDTO statistic  = new SensorMeasurementStatisticDTO();
	    	 statistic.setStatistic(arithmeticSensorMeasurement);
	    	 statistic.setStatisticType(MeasurementStatisticTypeEnum.MEAN);
	    	 
	    	 
	    	 	ArithmeticSensorMeasurementDTO  arithmeticSensorMeasurement2 = new ArithmeticSensorMeasurementDTO();
	    	 	arithmeticSensorMeasurement2.setMeasurement(new BigDecimal(900 + count));
	    	 	arithmeticSensorMeasurement2.setTimestamp(new Date());
	    	 	arithmeticSensorMeasurement2.setSensor(bodyTemperatureSensor);

		    	 SensorMeasurementStatisticDTO statistic2  = new SensorMeasurementStatisticDTO();
		    	 statistic2.setStatistic(arithmeticSensorMeasurement2);
		    	 statistic2.setStatisticType(MeasurementStatisticTypeEnum.MAXIMUM);
	
    SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
    List<SensorMeasurementStatisticDTO> sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
     sensorMeasurementStatistic.add(statistic);
     sensorMeasurementStatistic.add(statistic2);
    sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
    
    
    
    
    
    
    pointDTO = new PointDTO();
    pointDTO.setAltitude(new BigDecimal(220000));
    pointDTO.setLongitude(new BigDecimal(23762063+(count*10000))); 
    pointDTO.setLatitude(new BigDecimal(37983578));
    
    
    sensorData = new LocationSensorMeasurementDTO();
    sensorData.setPoint(pointDTO);
    
    
     statistic  = new SensorMeasurementStatisticDTO();
	 statistic.setStatistic(sensorData);
	 statistic.setStatisticType(MeasurementStatisticTypeEnum.LAST);
    
    sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
    sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
    sensorMeasurementStatistic.add(statistic);
    sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
    
        subactorDTO= new ActorDTO();
	   	subactorDTO.setId(new Long(1));
	   	subactorDTO.setType("FRC 1111");
	   	subactorDTO.setTitle("FRC #1111");
	   	subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
    
    
    ESponderEvent<SensorMeasurementStatisticEnvelopeDTO> statisticEvent = new CreateLocationMeasurementStatisticEvent();
    statisticEvent.setEventSeverity(SeverityLevelDTO.MEDIUM);
    statisticEvent.setEventTimestamp(new Date());
    statisticEvent.setEventAttachment(sensorMeasurementStatisticEnvelope);
    statisticEvent.setEventSource(subactorDTO);
  		
     
 	   ObjectMapper  mapperloc = new ObjectMapper();
 	   mapperloc.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
 	   mapperloc.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
 	     String jsonloc = null ;
 	     mapperloc.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          try {
         	 jsonloc = mapperloc.writeValueAsString(statisticEvent);
  		//	Log.d("xaxaxs","Elaaaa!location!  "+jsonloc);
  		} catch (JsonGenerationException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (JsonMappingException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
          
          
      	/*Bundle envelopeloc= new Bundle();
      	envelopeloc.putString("event", jsonloc); */
 	    Intent iloc = new Intent(CreateLocationMeasurementStatisticEvent.class.getName()+".4");
 	    iloc.addCategory("org.osgi.service.event.EventAdmin");
 	    iloc.putExtra("event", jsonloc); 
 	   	context.sendBroadcast(iloc);
 	  	
 		
 	    pointDTO = new PointDTO();
 	    pointDTO.setAltitude(new BigDecimal(220000));
 	    pointDTO.setLongitude(new BigDecimal(23115931+(count*10000)));  
 	    pointDTO.setLatitude(new BigDecimal(31928125));
 	    
 	    
 	    sensorData = new LocationSensorMeasurementDTO();
 	    sensorData.setPoint(pointDTO);
 	    
 	    
 	     statistic  = new SensorMeasurementStatisticDTO();
 		 statistic.setStatistic(sensorData);
 		 statistic.setStatisticType(MeasurementStatisticTypeEnum.LAST);
 	    
 	    sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
 	    sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
 	    sensorMeasurementStatistic.add(statistic);
 	    sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
 	    
 	        subactorDTO= new ActorDTO();
 		   	subactorDTO.setId(new Long(4));
 		   	subactorDTO.setType("FRC 4444");
 		   	subactorDTO.setTitle("FRC #4444");
 		   	subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
 	    
 	    
 	    statisticEvent = new CreateLocationMeasurementStatisticEvent();
 	    statisticEvent.setEventSeverity(SeverityLevelDTO.MEDIUM);
 	    statisticEvent.setEventTimestamp(new Date());
 	    statisticEvent.setEventAttachment(sensorMeasurementStatisticEnvelope);
 	    statisticEvent.setEventSource(subactorDTO);
 	  		
 	     
 	 	   mapperloc = new ObjectMapper();
 	 	   mapperloc.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
 	 	   mapperloc.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
 	 	   jsonloc = null ;
 	 	     mapperloc.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
 	          try {
 	         	 jsonloc = mapperloc.writeValueAsString(statisticEvent);
 	  		//	Log.d("xaxaxs","Elaaaa!location!  "+jsonloc);
 	  		} catch (JsonGenerationException e) {
 	  			// TODO Auto-generated catch block
 	  			e.printStackTrace();
 	  		} catch (JsonMappingException e) {
 	  			// TODO Auto-generated catch block
 	  			e.printStackTrace();
 	  		} catch (IOException e) {
 	  			// TODO Auto-generated catch block
 	  			e.printStackTrace();
 	  		}
 	          
 	          
 	      	/*Bundle envelopeloc= new Bundle();
 	      	envelopeloc.putString("event", jsonloc); */
 	 	    iloc = new Intent(CreateLocationMeasurementStatisticEvent.class.getName()+".1");
 	 	    iloc.addCategory("org.osgi.service.event.EventAdmin");
 	 	    iloc.putExtra("event", jsonloc); 
 	 	  	//context.sendBroadcast(iloc);
 	  	
   	
 	 	  	
 	 	  	// <----------------- Start Send Action ------------------> 
 	 	  	
 	 	  ReusableResourceDTO reusableResource1 = new ReusableResourceDTO();
 	 	  reusableResource1.setTitle("truck");
 	 	  HashSet  reusableResourceSet = new HashSet();
 	 	  reusableResourceSet.add(reusableResource1);
 	 	  
 	 	  ReusableResourceDTO reusableResource2 = new ReusableResourceDTO();
 	 	  reusableResource2.setTitle("red tools");
 	 	  HashSet  reusableResourceSet2 = new HashSet();
 	 	  reusableResourceSet2.add(reusableResource2);
 	 	  
 	 	  ConsumableResourceDTO  ConsumableResource1 = new ConsumableResourceDTO();
 	 	  ConsumableResource1.setTitle("food packages");
 	 	  ConsumableResource1.setQuantity(new BigDecimal(10));
 	      HashSet consumableResourceSet = new HashSet(); 
	      consumableResourceSet.add(ConsumableResource1);
	      
	      ConsumableResourceDTO  ConsumableResource2 = new ConsumableResourceDTO();
	      ConsumableResource2.setTitle("telecom lines"); 
 	      HashSet consumableResourceSet2 = new HashSet(); 
 	      consumableResourceSet2.add(ConsumableResource2);

 	 
	      ActionPartObjectiveDTO actionPartObjective1 = new ActionPartObjectiveDTO();
 	 	  actionPartObjective1.setConsumableResources(consumableResourceSet);
 	 	  HashSet actionPartObjectiveSet = new HashSet(); 
 	 	  actionPartObjectiveSet.add(actionPartObjective1);
 	 	  
 	      ActionPartObjectiveDTO actionPartObjective2 = new ActionPartObjectiveDTO();
 	 	  actionPartObjective2.setConsumableResources(consumableResourceSet);
 	 	  HashSet actionPartObjectiveSet2 = new HashSet(); 
 	 	  actionPartObjectiveSet2.add(actionPartObjective2);
 	 	  	
 	      // Action No1
 	 	  ActionPartDTO actoinPart1= new ActionPartDTO();
 	  	  actoinPart1.setUsedConsumableResources(consumableResourceSet);
 	  	  actoinPart1.setActionPartObjectives(actionPartObjectiveSet);
 	  	  actoinPart1.setActionOperation(ActionOperationEnumDTO.MOVE);
 	      ActionPartSnapshotDTO actionsnap1 = new ActionPartSnapshotDTO();
 	      actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
 	      actoinPart1.setSnapshot(actionsnap1);
 	  	  
 	      // Action No2
 		  ActionPartDTO actoinPart2= new ActionPartDTO();
 	  	  actoinPart2.setUsedConsumableResources(consumableResourceSet2);
 	  	  actoinPart2.setActionPartObjectives(actionPartObjectiveSet2);
 	  	  actoinPart2.setActionOperation(ActionOperationEnumDTO.FIX);
 	  	  ActionPartSnapshotDTO actionsnap2 = new ActionPartSnapshotDTO();
 	      actionsnap2.setStatus(ActionPartSnapshotStatusDTO.STARTED);
 	      actoinPart2.setSnapshot(actionsnap1);
 	 	  	

 	      HashSet actionParts = new HashSet();
 	      actionParts.add(actoinPart1);
 	      
 	 	  ActionDTO action = new ActionDTO(); 
 	      action.setActionParts(actionParts);
 	 	  
 	 	  	
 	 	 
 	     	// <----------------- End Send Action ------------------> 
 	 	  	
    count = count + 5;
    }
    }
}