package eu.esponder.event.model.crisis.resource;

import eu.esponder.event.model.DeleteEvent;


public class DeleteResourceEquipmentEvent extends ResourceEquipmentEvent implements DeleteEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8961408917425131483L;
	
}
