package eu.esponder.event.model.crisis.resource.type;

import eu.esponder.event.model.UpdateEvent;


public class UpdateConsumableResourceTypeEvent extends ConsumableResourceTypeEvent implements UpdateEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5442212899744539366L;
	
}
