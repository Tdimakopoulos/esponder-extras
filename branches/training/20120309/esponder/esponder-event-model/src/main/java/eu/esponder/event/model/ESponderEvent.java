package eu.esponder.event.model;

import java.io.Serializable;

import eu.esponder.jaxb.model.ESponderEntityDTO;


public abstract class ESponderEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3165213086735540677L;
	
	private ESponderEntityDTO eventAttachment;

	public ESponderEntityDTO getEventAttachment() {
		return eventAttachment;
	}

	public void setEventAttachment(ESponderEntityDTO eventAttachment) {
		this.eventAttachment = eventAttachment;
	}
	
	
}

