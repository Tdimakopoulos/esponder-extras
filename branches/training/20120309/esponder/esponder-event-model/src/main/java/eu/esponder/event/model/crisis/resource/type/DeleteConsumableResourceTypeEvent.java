package eu.esponder.event.model.crisis.resource.type;

import eu.esponder.event.model.DeleteEvent;


public class DeleteConsumableResourceTypeEvent extends ConsumableResourceTypeEvent implements DeleteEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1792364444509033309L;
	
}
