package eu.esponder.event.model.snapshot.resource;

import eu.esponder.event.model.CreateEvent;


public class CreateEquipmentSnapshotEvent extends EquipmentSnapshotEvent implements CreateEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6363754485823085786L;
	
}
