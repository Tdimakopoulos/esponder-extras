package eu.esponder.jaxb.model.snapshot.resource;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import eu.esponder.jaxb.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.status.OperationsCentreSnapshotStatusDTO;

@XmlRootElement(name="operationsCentreSnapshot")
@XmlType(name="OperationsCentreSnapshot")
@JsonPropertyOrder({"status", "locationArea", "period"})
public class OperationsCentreSnapshotDTO extends SpatialSnapshotDTO {

	private OperationsCentreSnapshotStatusDTO status;

	public OperationsCentreSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(OperationsCentreSnapshotStatusDTO status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "OperationsCentreSnapshotDTO [status=" + status + ", locationArea=" + locationArea + ", id=" + id + "]";
	}
	
}
