package eu.esponder.jaxb.model.snapshot.sensor.measurement;

import eu.esponder.jaxb.model.snapshot.location.LocationAreaDTO;

public class LocationSensorMeasurementDTO extends SensorMeasurementDTO {

	private LocationAreaDTO locationArea;

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}
	
}
