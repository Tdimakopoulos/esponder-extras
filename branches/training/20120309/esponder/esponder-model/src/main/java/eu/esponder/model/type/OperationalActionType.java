package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@DiscriminatorValue("OP_ACTION")
public final class OperationalActionType extends ActionType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3958343462000323443L;
	
	@ManyToOne
	@JoinColumn(name="PARENT_ID")
	private TacticalActionType parent;

	public TacticalActionType getParent() {
		return parent;
	}

	public void setParent(TacticalActionType parent) {
		this.parent = parent;
	}
	
}
