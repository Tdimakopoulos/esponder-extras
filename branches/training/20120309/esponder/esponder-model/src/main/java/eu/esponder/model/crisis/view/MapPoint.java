package eu.esponder.model.crisis.view;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.snapshot.location.Point;

@Entity
@Table(name="map_point")
@NamedQueries({
	@NamedQuery(name="MapPoint.findByTitle", query="select mp from MapPoint mp where mp.title=:title")
})
public class MapPoint extends ESponderEntity<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2699139388286227165L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="POINT_ID")
	private Long id;
	
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;

	@Embedded
	private Point point;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

}
