package eu.esponder.util.jaxb;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class Parser {

    private Unmarshaller um;

	public Parser(Class<?>[] classes) throws JAXBException {
//		new Class[] {PointDTO.class}
		JAXBContext ctx = JAXBContext.newInstance(classes);
        um = ctx.createUnmarshaller();
        
	}
	
	public Object unmarshal(String xmlFile) throws JAXBException {
		InputStream is = new ByteArrayInputStream(xmlFile.getBytes());
		return this.um.unmarshal(is);
	}

}
