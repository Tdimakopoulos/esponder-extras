package eu.esponder.controller.test.crisis;

import java.math.BigDecimal;


import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.crisis.resource.ResourceStatus;
import eu.esponder.model.crisis.resource.Sensor;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.snapshot.status.SensorSnapshotStatus;
import eu.esponder.model.type.SensorType;

public class PrevSensorTest extends ControllerServiceTest {
	
	private static int SECONDS = 10;
	
	@Test(groups="createSnapshots")
	public void testCreateSensorSnapshots() {		
		
		Sensor firstFRCTemperature = sensorService.findByTitle("FRU #1 TEMP.");
		/*Sensor firstFRCHeartbeat = sensorService.findByTitle("FRU #1 HB.");
		Sensor firstFRTemperature = sensorService.findByTitle("FRU #1.1 TEMP.");
		Sensor firstFRHeartbeat = sensorService.findByTitle("FRU #1.1 HB.");
		Sensor secondFRTemperature = sensorService.findByTitle("FRU #1.2 TEMP.");
		Sensor secondFRHeartbeat = sensorService.findByTitle("FRU #1.2 HB.");
		Sensor secondFRCTemperature = sensorService.findByTitle("FRU #2 TEMP.");
		Sensor secondFRCHeartbeat = sensorService.findByTitle("FRU #2 HB.");
		Sensor thirdFRTemperature = sensorService.findByTitle("FRU #2.1 TEMP.");
		Sensor thirdFRHeartbeat = sensorService.findByTitle("FRU #2.1 HB.");
		Sensor fourthFRTemperature = sensorService.findByTitle("FRU #2.2 TEMP.");
		Sensor fourthFRHeartbeat = sensorService.findByTitle("FRU #2.2 HB.");*/
		
		Period period = this.createPeriod(SECONDS);
		createSensorSnapshot(firstFRCTemperature, new BigDecimal("30"), period);
		/*createSensorSnapshot(firstFRCHeartbeat, new BigDecimal("80"), period);
		createSensorSnapshot(firstFRTemperature, new BigDecimal("31"), period);
		createSensorSnapshot(firstFRHeartbeat, new BigDecimal("70"), period);
		createSensorSnapshot(secondFRTemperature, new BigDecimal("31"), period);
		createSensorSnapshot(secondFRHeartbeat, new BigDecimal("85"), period);
		createSensorSnapshot(secondFRCTemperature, new BigDecimal("35"), period);
		createSensorSnapshot(secondFRCHeartbeat, new BigDecimal("79"), period);
		createSensorSnapshot(thirdFRTemperature, new BigDecimal("36"), period);
		createSensorSnapshot(thirdFRHeartbeat, new BigDecimal("74"), period);
		createSensorSnapshot(fourthFRTemperature, new BigDecimal("34"), period);
		createSensorSnapshot(fourthFRHeartbeat, new BigDecimal("84"), period);*/
		 
		
		
	}
	
	private Sensor createSensor(SensorType type, String title, Equipment equipment) {
		Sensor sensor = new Sensor();
		sensor.setSensorType(type);
		sensor.setTitle(title);
		sensor.setStatus(ResourceStatus.ALLOCATED);
		sensor.setEquipment(equipment);
		return sensorService.create(sensor, this.userID);
	}
	
	private SensorSnapshot createSensorSnapshot(Sensor sensor, BigDecimal meanValue, Period period) {
		SensorSnapshot snapshot = new SensorSnapshot();
		snapshot.setSensor(sensor);
		snapshot.setMeanValue(meanValue);
		snapshot.setPeriod(period);
		snapshot.setStatus(SensorSnapshotStatus.WORKING);
		SensorSnapshot prev_snapshot = sensorService.findPreviousSnapshotBySensor(sensor.getId());
		System.out.println("\nThe previous snapshot for that sensor is :  "+prev_snapshot.getId() + "\n");
		snapshot.setPrevious(prev_snapshot);
		return sensorService.createSnapshot(snapshot, this.userID);
	}
	
}
