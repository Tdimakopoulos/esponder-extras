package eu.esponder.controller.crisis.resource;

import java.util.Date;

import javax.ejb.Remote;

import eu.esponder.model.crisis.resource.Sensor;
import eu.esponder.model.snapshot.resource.SensorSnapshot;

@Remote
public interface SensorRemoteService {
	
	public Sensor findById(Long sensorID);
	
	public Sensor findByTitle(String title);
	
	public SensorSnapshot findSnapshotByDate(Long sensorID, Date dateTo);
	
	public Sensor create(Sensor sensor, Long userID);
	
	public SensorSnapshot createSnapshot(SensorSnapshot snapshot, Long userID);

	// Dkar
	public SensorSnapshot findPreviousSnapshotBySensor(Long sensorID);
	
}
