package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.EquipmentService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.resource.SensorService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.model.crisis.resource.Sensor;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.type.SensorType;

@Stateless
@SuppressWarnings("unchecked")
public class SensorBean implements SensorService, SensorRemoteService {
	
	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	@EJB
	private TypeService typeService;
	
	@EJB
	private EquipmentService equipmentService;
	
	//-------------------------------------------------------------------------
	
	@Override
	public Sensor findById(Long sensorID) {
		return (Sensor) crudService.find(Sensor.class, sensorID);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public Sensor findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Sensor) crudService.findSingleWithNamedQuery("Sensor.findByTitle", params);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public SensorSnapshot findSnapshotByDate(Long sensorID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sensorID", sensorID);
		params.put("maxDate", maxDate);
		SensorSnapshot snapshot =
				(SensorSnapshot) crudService.findSingleWithNamedQuery("SensorSnapshot.findBySensorAndDate", params);
		return snapshot;
	}

	//-------------------------------------------------------------------------
	
	
	// Dkar
	@Override
	public SensorSnapshot findPreviousSnapshotBySensor(Long sensorID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sensorID", sensorID);
		SensorSnapshot snapshot =
				(SensorSnapshot) crudService.findSingleWithNamedQuery("SensorSnapshot.findPreviousSnapshotBySensor", params);
		return snapshot;
	}

	//-------------------------------------------------------------------------
	
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Sensor create(Sensor sensor, Long userID) {
		sensor.setSensorType((SensorType)typeService.findById(sensor.getSensorType().getId()));
		sensor.setEquipment(equipmentService.findById(sensor.getEquipment().getId()));
		crudService.create(sensor);
		return sensor;
	}

	//-------------------------------------------------------------------------
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SensorSnapshot createSnapshot(SensorSnapshot snapshot, Long userID) {
		snapshot.setSensor((Sensor)this.findById(snapshot.getSensor().getId()));
		crudService.create(snapshot);
		return snapshot;
	}
	
}
