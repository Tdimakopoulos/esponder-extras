package eu.esponder.controller.crisis.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.model.type.ESponderType;

@Stateless
@SuppressWarnings("unchecked")
public class TypeBean implements TypeService, TypeRemoteService {
	
	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	//-------------------------------------------------------------------------
	
	@Override
	public ESponderType findById(Long typeId) {
		return (ESponderType) crudService.find(ESponderType.class, typeId);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public ESponderType findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ESponderType) crudService.findSingleWithNamedQuery("ESponderType.findByTitle", params);
	}

	//-------------------------------------------------------------------------
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderType create(ESponderType type, Long userID) {
		crudService.create(type);
		return type;
	}
	
}
