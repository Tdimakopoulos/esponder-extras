package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.crisis.view.SketchPOI;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;

@Remote
public interface OperationsCentreRemoteService {
	
	public OperationsCentre findById(Long operationsCentreID);
	
	public OperationsCentre findByTitle(String title);
	
	public OperationsCentre findByUser(Long operationsCentreID, Long userID);
	
	public OperationsCentreSnapshot findSnapshotByDate(Long operationsCentreID, Date maxDate);
	
	public OperationsCentre create(OperationsCentre operationsCentre, Long userID);
	
	public OperationsCentreSnapshot createSnapshot(OperationsCentre operationsCentre, OperationsCentreSnapshot snapshot, Long userID);
	
	public List<SketchPOI> findSketchPOIs(Long operationsCentreID);
	
	public SketchPOI createSketch(OperationsCentre operationsCentre, SketchPOI sketch, Long userID);
	
	public List<ReferencePOI> findReferencePOIs(Long operationsCentreID);
	
	public ReferencePOI createReferencePOI(OperationsCentre operationsCentre, ReferencePOI referencePOI, Long userID);
	
}
