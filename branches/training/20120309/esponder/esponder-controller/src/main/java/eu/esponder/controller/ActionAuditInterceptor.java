package eu.esponder.controller;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import eu.esponder.controller.crisis.user.UserService;
import eu.esponder.model.ActionAudit;
import eu.esponder.model.user.ESponderUser;
import eu.esponder.util.ejb.ServiceLocator;

public class ActionAuditInterceptor {

	@AroundInvoke
	public Object intercept(InvocationContext ctx) throws Exception {
		Object[] params = ctx.getParameters();
		Long userID = (Long) params[params.length-1];
		if (null != userID) {
			UserService userService = ServiceLocator.getResource("esponder/UserBean/local");
			ESponderUser user = userService.findById(userID);
			if (null == user) {
				throw new RuntimeException("User with ID: " + userID + " not found.");
			}
			ActionAudit.storeWho(Thread.currentThread(), user);
		}
		return ctx.proceed();
	}
	
}
