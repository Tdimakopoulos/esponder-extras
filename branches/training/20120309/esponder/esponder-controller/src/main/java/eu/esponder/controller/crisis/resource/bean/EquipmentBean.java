package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.snapshot.resource.EquipmentSnapshot;
import eu.esponder.model.type.EquipmentType;

@Stateless
@SuppressWarnings("unchecked")
public class EquipmentBean implements EquipmentService, EquipmentRemoteService {
	
	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	@EJB
	private TypeService typeService;
	
	@EJB
	private ActorService actorService;
	
	//-------------------------------------------------------------------------
	
	@Override
	public Equipment findById(Long equipmentID) {
		return (Equipment) crudService.find(Equipment.class, equipmentID);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public Equipment findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Equipment) crudService.findSingleWithNamedQuery("Equipment.findByTitle", params);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public EquipmentSnapshot findSnapshotByDate(Long equipmentID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("equipmentID", equipmentID);
		params.put("maxDate", maxDate);
		EquipmentSnapshot snapshot =
				(EquipmentSnapshot) crudService.findSingleWithNamedQuery("EquipmentSnapshot.findByEquipmentAndDate", params);
		return snapshot;
	}

	//-------------------------------------------------------------------------
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Equipment create(Equipment equipment, Long userID) {
		equipment.setEquipmentType((EquipmentType)typeService.findById(equipment.getEquipmentType().getId()));
		if (null != equipment.getActor()) {
			equipment.setActor(actorService.findById(equipment.getActor().getId()));
		}
		crudService.create(equipment);
		return equipment;
	}
	
	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public EquipmentSnapshot createSnapshot(EquipmentSnapshot snapshot, Long userID) {
		snapshot.setEquipment((Equipment)this.findById(snapshot.getEquipment().getId()));
		crudService.create(snapshot);
		return snapshot;
	}
	
}
