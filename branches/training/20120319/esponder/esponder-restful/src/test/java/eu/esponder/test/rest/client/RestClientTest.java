package eu.esponder.test.rest.client;
import java.math.BigDecimal;

import javax.xml.bind.JAXBException;

import org.testng.annotations.Test;

import eu.esponder.jaxb.model.snapshot.location.PointDTO;
import eu.esponder.rest.client.RestGetClient;
import eu.esponder.rest.client.RestPostClient;
import eu.esponder.util.jaxb.Parser;

@Deprecated
public class RestClientTest {
	
	@Test
	public void testRestGetClient() throws JAXBException {
		String resourceURI = "http://localhost:8080/esponder-restful/crisis/contextTest/test?userID=1";
		RestGetClient restGetClient = new RestGetClient(resourceURI);
		
		String xmlResponse = restGetClient.getXML();
		
        
        Parser parser = new Parser(new Class[] {PointDTO.class});
        PointDTO pointDTO = (PointDTO) parser.unmarshal(xmlResponse);
       
        PointDTO pointDTOCheck = new PointDTO();
        pointDTOCheck.setAltitude(new BigDecimal(1));
        pointDTOCheck.setLongitude(new BigDecimal(2));
        pointDTOCheck.setLatitude(new BigDecimal(3));
        
        System.out.println(pointDTO.toString());
        
        assert pointDTOCheck.equals(pointDTO);
		
		return;
	}
	
	@Test
	public void testRestPostClient() throws JAXBException {
		String resourceURI = "http://localhost:8080/esponder-restful/crisis/contextTest/testPost?userID=1";
		RestPostClient restPostClient = new RestPostClient(resourceURI);
		
		String xmlResponse = restPostClient.postXML(null);
		
        Parser parser = new Parser(new Class[] {PointDTO.class});
        PointDTO pointDTO = (PointDTO) parser.unmarshal(xmlResponse);
       
        PointDTO pointDTOCheck = new PointDTO();
        pointDTOCheck.setAltitude(new BigDecimal(55L));
        pointDTOCheck.setLongitude(new BigDecimal(2));
        pointDTOCheck.setLatitude(new BigDecimal(3));
        
        System.out.println(pointDTO.toString());
        
        assert pointDTOCheck.equals(pointDTO);
		
		return;
	}

}
