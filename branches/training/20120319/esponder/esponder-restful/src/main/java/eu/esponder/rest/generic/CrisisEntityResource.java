package eu.esponder.rest.generic;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.hibernate.validator.constraints.NotEmpty;

import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.controller.persistence.criteria.EsponderIntersectionCriteriaCollection;
import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.criteria.EsponderCriteriaCollectionDTO;
import eu.esponder.jaxb.model.criteria.EsponderIntersectionCriteriaCollectionDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.rest.ESponderResource;
import eu.esponder.util.ejb.ServiceLocator;

@Path("/crisis/entity")
public class CrisisEntityResource extends ESponderResource {

	@GET
	@Path("/generic")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public List<ESponderEntityDTO> getEntity(
			@QueryParam("queriedEntity") @NotNull(message="queriedEntity may not be null") String queriedEntity,  
			@QueryParam("criteria") @NotNull(message="criteria may not be null") @NotEmpty(message="criteria may not be empty") EsponderCriteriaCollectionDTO criteriaDTO,
			@QueryParam("maxresults") @NotNull(message="maxresults may not be null") Integer resultLimit,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {

		
		ESponderRemoteMappingService testMappingService;
		GenericRemoteService genericService;
		
		@SuppressWarnings("unchecked")
		Class<? extends ESponderEntity<Long>> queriedClass = (Class<? extends ESponderEntity<Long>>) Class.forName(queriedEntity);
		
		testMappingService = ServiceLocator.getResource("esponder/ESponderMappingBean/remote");
		genericService = ServiceLocator.getResource("esponder/GenericBean/remote");

		/*EsponderIntersectionCriteriaCollectionDTO testCollectionDTO = new EsponderIntersectionCriteriaCollectionDTO(new HashSet<EsponderQueryRestrictionDTO>());
		EsponderCriterionDTO ecriterion = new EsponderCriterionDTO();
		EsponderCriterionDTO ecriterion2 = new EsponderCriterionDTO();

		ecriterion.setFieldName("id");
		ecriterion.setFieldValue(new Long(2));
		ecriterion.setExpression(EsponderCriterionExpressionEnumDTO.GREATER_THAN);
		testCollectionDTO.add(ecriterion);

		ecriterion2.setFieldName("id");
		ecriterion2.setFieldValue(new Long(6));
		ecriterion2.setExpression(EsponderCriterionExpressionEnumDTO.LESS_THAN_OR_EQUAL);
		testCollectionDTO.add(ecriterion2);*/

		if(criteriaDTO instanceof EsponderIntersectionCriteriaCollectionDTO)
		{
			System.out.println("Instance of EsponderIntersectionCriteriaCollection");		
			EsponderQueryRestriction testCollection = testMappingService.mapCriteriaCollection(criteriaDTO);
			System.out.println("transformation successful");
			final List<?> resultsList = (List<?>) genericService.getEntities( (Class<? extends ESponderEntity>) queriedClass, (EsponderIntersectionCriteriaCollection) testCollection, resultLimit);
			if(resultsList.isEmpty()){
				System.out.println("\n\nEmpty result List, something went wrong or the query returned no results...\n\n");
				return null;
			}
			else {
				System.out.println("\n Results returned size : "+ resultsList.size());
				return null;
			}
		}
		else System.out.println("Mapping was not executed, unknown DTO class type...");
		return null;

		/*List<ESponderEntityDTO> resultsList = testMappingService.
		return resultsList;*/
		
	}
}