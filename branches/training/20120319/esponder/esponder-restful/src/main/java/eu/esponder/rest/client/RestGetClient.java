package eu.esponder.rest.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

@Deprecated
public class RestGetClient {

	private String XML_REQUEST_TYPE_HEADER = "application/xml";
	private String JSON_REQUEST_TYPE_HEADER = "application/json";
	private String resourceURI = "";

	public RestGetClient(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	public String getXML() {
		return get(XML_REQUEST_TYPE_HEADER);
	}

	public String getJSON() {
		return get(JSON_REQUEST_TYPE_HEADER);
	}

	public String get(String serviceType) {
		String output = "";
		String outputResponse = "";
		DefaultHttpClient httpClient = null;
		try {

			httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(resourceURI);
			getRequest.addHeader("accept", serviceType);

			HttpResponse response = httpClient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));

			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				outputResponse += output; 
			}
			System.out.println("Output from Server FINISHED");


		} catch (ClientProtocolException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			if (httpClient != null) {
				httpClient.getConnectionManager().shutdown();
			}
		}
		return outputResponse;
	}

}
