package eu.esponder.util.mapping;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;


/**
 * This implementation of <code>DozerTransformationService</code> uses dozer for
 * the object-to-object transformation. You can use multiple mapping files, but you
 * have to configure them in a dozer configuration file.
 * 
 */
public class MappingBean implements MappingService {

	private static final String MAPPING_FILES = "dozer.mapping.files";
	private static final Logger LOG = Logger.getLogger(MappingBean.class);
	
	private DozerBeanMapper mapper;
	private String configFile;
	
	protected MappingBean(String configFile) {
		this.configFile = configFile;
		try {
			initializeDozer();
		} catch (IOException e) {
			LOG.error(e);
		}
	}
	
	private void initializeDozer() throws IOException {
		this.mapper = new DozerBeanMapper();
		this.mapper.setMappingFiles(this.getMappingFiles());
	}
	
	private List<String> getMappingFiles() throws IOException {
		
		URL configFileUrl = 
				Thread.currentThread().getContextClassLoader().getResource(configFile);
 		
		ArrayList<String> mappingFiles = new ArrayList<String>();
		if (null != configFileUrl) {
			Properties properties = new Properties();
			try {
				properties.load(configFileUrl.openStream());
			} catch (IOException e) {
				LOG.error(e);
			}

			for (String mappingFile: properties.getProperty(MAPPING_FILES).split(",")) {
				mappingFiles.add(mappingFile.trim());
			}
		}
 		return mappingFiles;
	}
	
	public <T> T transform(Object src, Class<T> targetClass) {
		return (T) this.mapper.map(src, targetClass);
	}
	
	public <T extends Object, U> List<U> transform(Collection<T> src, Class<U> targetClass) {
		List<U> list = new ArrayList<U>();
		for (T item : src) {
			list.add((U)this.mapper.map(item, targetClass));
		}
		return list;
	} 
	
}
