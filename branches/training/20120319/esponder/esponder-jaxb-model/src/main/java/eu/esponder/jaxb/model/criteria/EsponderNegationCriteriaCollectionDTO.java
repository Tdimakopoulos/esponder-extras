package eu.esponder.jaxb.model.criteria;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class EsponderNegationCriteriaCollectionDTO extends EsponderCriteriaCollectionDTO {

	private static final long serialVersionUID = -786849464712579302L;

	public Set<EsponderQueryRestrictionDTO> restrictions = new HashSet<EsponderQueryRestrictionDTO>();


	public Set<EsponderQueryRestrictionDTO> getRestrictions() {
		return (Set<EsponderQueryRestrictionDTO>) restrictions;
	}

	public void setRestrictions(Set<EsponderQueryRestrictionDTO> restrictions) {
		this.restrictions = restrictions;
	}

	public EsponderNegationCriteriaCollectionDTO(
			Collection<EsponderQueryRestrictionDTO> restrictions) {
		super(restrictions);
	}

}
