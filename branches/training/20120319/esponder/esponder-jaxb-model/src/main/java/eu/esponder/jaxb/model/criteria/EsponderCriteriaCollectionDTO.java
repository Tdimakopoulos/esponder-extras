package eu.esponder.jaxb.model.criteria;

import java.util.Collection;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
@XmlSeeAlso({
	EsponderIntersectionCriteriaCollectionDTO.class,
	EsponderUnionCriteriaCollectionDTO.class,
	EsponderNegationCriteriaCollectionDTO.class
})
public abstract class EsponderCriteriaCollectionDTO extends EsponderQueryRestrictionDTO {
	
	private static final long serialVersionUID = -7905934732438391487L;
	
	private Collection<EsponderQueryRestrictionDTO> restrictions;
	
	public EsponderCriteriaCollectionDTO () { }
	
	public EsponderCriteriaCollectionDTO(Collection<EsponderQueryRestrictionDTO> restrictions) {
		this.restrictions = restrictions;
	}

	public void setRestrictions(Collection<EsponderQueryRestrictionDTO> restrictions) {
		this.restrictions = restrictions;
	}
	
	
	public Collection<EsponderQueryRestrictionDTO> getRestrictions() {
		return restrictions;
	}

	public void add(EsponderQueryRestrictionDTO restriction) {
		this.getRestrictions().add(restriction);
	}
	
	public void remove(EsponderQueryRestrictionDTO restriction) {
		this.getRestrictions().remove(restriction);
	}

}
