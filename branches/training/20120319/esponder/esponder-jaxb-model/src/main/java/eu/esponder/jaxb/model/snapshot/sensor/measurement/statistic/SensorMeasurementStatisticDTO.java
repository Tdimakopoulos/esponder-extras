package eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic;

import eu.esponder.jaxb.model.snapshot.PeriodDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementDTO;

public class SensorMeasurementStatisticDTO {
	
	private MeasurementStatisticTypeEnum statisticType;
	
	/**
	 * Sampling period in msec
	 */
	private long samplingPeriod;
	
	private SensorMeasurementDTO statistic;
	
	private PeriodDTO period;
	
	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

	public long getSamplingPeriod() {
		return samplingPeriod;
	}

	public void setSamplingPeriod(long samplingPeriod) {
		this.samplingPeriod = samplingPeriod;
	}

	public SensorMeasurementDTO getStatistic() {
		return statistic;
	}

	public void setStatistic(SensorMeasurementDTO statistic) {
		this.statistic = statistic;
	}

	public MeasurementStatisticTypeEnum getStatisticType() {
		return statisticType;
	}

	public void setStatisticType(MeasurementStatisticTypeEnum statisticType) {
		this.statisticType = statisticType;
	}
	
}
