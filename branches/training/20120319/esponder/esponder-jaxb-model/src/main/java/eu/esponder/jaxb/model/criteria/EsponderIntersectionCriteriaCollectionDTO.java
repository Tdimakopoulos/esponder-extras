package eu.esponder.jaxb.model.criteria;

import java.util.Collection;


public class EsponderIntersectionCriteriaCollectionDTO extends EsponderCriteriaCollectionDTO {

	private static final long serialVersionUID = -4395158116445919854L;
		
	public EsponderIntersectionCriteriaCollectionDTO (Collection<EsponderQueryRestrictionDTO> restrictions) {		
		super(restrictions);
	}

}
