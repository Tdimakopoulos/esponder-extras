package eu.esponder.jaxb.model.snapshot.sensor.measurement;

import java.util.Date;

import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.crisis.resource.sensor.SensorDTO;

public abstract class SensorMeasurementDTO extends ESponderEntityDTO {
	
	private Class<? extends SensorDTO> sensorType;
	
	private Date timestamp;

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Class<? extends SensorDTO> getSensorType() {
		return sensorType;
	}

	public void setSensor(Class <? extends SensorDTO> sensorType) {
		this.sensorType = sensorType;
	}
	
}
