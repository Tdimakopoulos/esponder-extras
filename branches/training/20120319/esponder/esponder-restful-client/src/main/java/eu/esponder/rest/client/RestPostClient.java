package eu.esponder.rest.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import eu.esponder.jaxb.model.ESponderEntityDTO;

public class RestPostClient {

	private String XML_REQUEST_TYPE_HEADER = "application/xml";
	private String JSON_REQUEST_TYPE_HEADER = "application/json";
	private String resourceURI = "";

	public RestPostClient(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	public String postXML(ESponderEntityDTO entity) {
		return post(XML_REQUEST_TYPE_HEADER, entity);
	}

	public String postJSON(ESponderEntityDTO entity) {
		return post(JSON_REQUEST_TYPE_HEADER, entity);
	}

	private String post(String serviceType, ESponderEntityDTO entity) {
		String output = "";
		String outputResponse = "";
		DefaultHttpClient httpClient = null;
		try {
			
//			HttpPost httpget = new HttpPost(resourceURI);
//
//            System.out.println("executing request " + httpget.getURI());
//
//            // Create a response handler
//            ResponseHandler<String> responseHandler = new BasicResponseHandler();
//            String responseBody = httpClient.execute(httpget, responseHandler);
//            System.out.println("----------------------------------------");
//            System.out.println(responseBody);
//            System.out.println("----------------------------------------");
//			
//			
			

			httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(resourceURI);
//			postRequest.addHeader("accept", serviceType);
			

			String POSTText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><point><altitude>1</altitude><latitude>3</latitude><longitude>2</longitude></point>";
			StringEntity entityStr = new StringEntity(POSTText, "UTF-8");
//			BasicHeader basicHeader = new BasicHeader(HTTP.CONTENT_TYPE, javax.ws.rs.core.MediaType.APPLICATION_XML);
//			BasicHeader basicHeader = new BasicHeader("Accept", javax.ws.rs.core.MediaType.APPLICATION_XML);
			postRequest.getParams().setBooleanParameter("http.protocol.expect-continue", false);
			postRequest.setHeader("Content-Type", XML_REQUEST_TYPE_HEADER);
			postRequest.setHeader("Accept", XML_REQUEST_TYPE_HEADER);
//			entityStr.setContentType(basicHeader);
			postRequest.setEntity(entityStr);
			
			HttpResponse response = httpClient.execute(postRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));

			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				outputResponse += output; 
			}
			System.out.println("Output from Server FINISHED");


		} catch (ClientProtocolException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			if (httpClient != null) {
				httpClient.getConnectionManager().shutdown();
			}
		}
		return outputResponse;
	}

}
