package eu.esponder.datafusion.dataprocessing.helper;

import java.math.BigDecimal;
import java.util.LinkedList;

public class MeasurementWindowConverter {


	public static double[] getMeasurementsWindowArray(LinkedList<BigDecimal> measurementsWindow) {
		double[] measurementsArray = new double[measurementsWindow.size()];
		for (int i = 0; i <= measurementsWindow.size(); i++) {
			measurementsArray[i] = measurementsWindow.get(i).doubleValue();
		}
		return measurementsArray;
	}
	
	public static LinkedList<BigDecimal> getMeasurementsList(double[] measurementsWindowArray) {
		LinkedList<BigDecimal> measurementsWindow = new LinkedList<BigDecimal>();
		for (double measurement : measurementsWindowArray) {
			measurementsWindow.add(new BigDecimal(measurement));
		}
		
		return measurementsWindow;
	}
}
