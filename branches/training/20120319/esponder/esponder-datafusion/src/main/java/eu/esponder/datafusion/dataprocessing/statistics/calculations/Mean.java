package eu.esponder.datafusion.dataprocessing.statistics.calculations;

import eu.esponder.datafusion.dataprocessing.MeasurementWindow;
import eu.esponder.datafusion.dataprocessing.statistics.StatisticResult;

public class Mean extends Calculator {

	@Override
	public StatisticResult calculate(MeasurementWindow measurementsWindow) {
		StatisticResult resultMean = new StatisticResult();
		Sum sum = new Sum();
		resultMean.setStatisticalResult(sum.calculate(measurementsWindow).getStatisticalResult() / (new Double(measurementsWindow.getWindowSize())));
		return resultMean;
	}

}
