package eu.esponder.datafusion.dataprocessing;

import java.util.LinkedList;
import java.util.List;

public class MeasurementWindow {
	
	private LinkedList<Double> measurementsWindow = null;
	private String measurementWindowType = null;
	private int windowSize = 0;
	
	public MeasurementWindow(int windowSize, String sensorType) {
		this.windowSize = windowSize;
		this.setMeasurementWindowType(sensorType);
	}
	
	public int getWindowSize() {
		return windowSize;
	}

	public void setWindowSize(int windowSize) {
		this.windowSize = windowSize;
	}

	public List<Double> getMeasurements() {
		return measurementsWindow;
	}

	public void setMeasurements(LinkedList<Double> measurements) {
		this.measurementsWindow = measurements;
	}
	
	public void addMeasurement(Double measurement) {
		measurementsWindow.add(measurement);
		if (measurementsWindow.size() >= windowSize) {
			measurementsWindow.remove();
		}
	}
	
	public void changeWindowSize(int newWindowSize) {
		if (this.windowSize > newWindowSize) {
			for (int i = 0; i < this.windowSize - newWindowSize; i++) {
				measurementsWindow.remove();
			}
		}
		this.windowSize = newWindowSize;
	}

	public String getMeasurementWindowType() {
		return measurementWindowType;
	}

	public void setMeasurementWindowType(String measurementWindowType) {
		this.measurementWindowType = measurementWindowType;
	}
	
}
