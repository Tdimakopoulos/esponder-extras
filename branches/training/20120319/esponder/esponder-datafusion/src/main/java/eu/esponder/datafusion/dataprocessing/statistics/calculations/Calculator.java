package eu.esponder.datafusion.dataprocessing.statistics.calculations;

import eu.esponder.datafusion.dataprocessing.MeasurementWindow;
import eu.esponder.datafusion.dataprocessing.statistics.StatisticResult;

public abstract class Calculator {
	
	public abstract StatisticResult calculate(MeasurementWindow measurementsWindow);

}
