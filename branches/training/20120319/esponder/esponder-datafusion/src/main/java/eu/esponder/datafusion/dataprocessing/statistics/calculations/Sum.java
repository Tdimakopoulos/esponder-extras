package eu.esponder.datafusion.dataprocessing.statistics.calculations;

import eu.esponder.datafusion.dataprocessing.MeasurementWindow;
import eu.esponder.datafusion.dataprocessing.statistics.StatisticResult;

public class Sum extends Calculator {

	@Override
	public StatisticResult calculate(MeasurementWindow measurementsWindow) {
		StatisticResult resultSum = new StatisticResult();
		for (Double measurement : measurementsWindow.getMeasurements()) {
			resultSum.setStatisticalResult(resultSum.getStatisticalResult() + measurement);
		}
		
		return resultSum;
	}

}
