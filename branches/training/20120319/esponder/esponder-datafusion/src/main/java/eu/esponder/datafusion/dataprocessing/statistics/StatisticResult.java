package eu.esponder.datafusion.dataprocessing.statistics;


public class StatisticResult {
	
	private Double statisticalResult = null;
	
	public StatisticResult() {
		this.statisticalResult = new Double(0);
	}

	public Double getStatisticalResult() {
		return statisticalResult;
	}

	public void setStatisticalResult(Double statisticalResult) {
		this.statisticalResult = statisticalResult;
	}
	

}
