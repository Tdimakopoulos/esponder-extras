package eu.esponder.datafusion.test;

import org.testng.annotations.Test;

import eu.esponder.datafusion.dataprocessing.MeasurementWindow;
import eu.esponder.model.type.SensorType;

public class DatafusionTest {
	
	@Test(groups="processSensorMeasurements")
	public void testProcessFRUSensorMeasurments() {
		System.out.println("Starting Test...");

		// Collect Measurements (e.g. input from file)
		// TODO: Skipping this for now. 
		
		// Create Measurement Window List
		MeasurementWindow measurementWindow = getMeasurementsWindow();

		
		// Process Measurements for Window and Produce Statistics
		
		
		// Create Final Envelope of Sensor Measurements 
		
		
		System.out.println("Test Finished. Printing Results...");
		return;
		
	}
	
	private MeasurementWindow getMeasurementsWindow() {
		String sensorType = "SensorType";
		MeasurementWindow measurements = new MeasurementWindow(10, sensorType);
		
		
		
		return measurements;
	}

}
