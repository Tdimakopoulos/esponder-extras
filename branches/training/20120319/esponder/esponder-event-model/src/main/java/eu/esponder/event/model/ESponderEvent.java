package eu.esponder.event.model;

import java.io.Serializable;
import java.util.Date;

import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.crisis.action.SeverityLevelDTO;
import eu.esponder.jaxb.model.crisis.resource.ResourceDTO;


public abstract class ESponderEvent implements Serializable {

	private static final long serialVersionUID = 3165213086735540677L;
	
	private ResourceDTO eventSource;
	
	private ESponderEntityDTO eventAttachment;
	
	private Date eventTimestamp;
	
	private SeverityLevelDTO eventSeverity;

	public ESponderEntityDTO getEventAttachment() {
		return eventAttachment;
	}

	public ResourceDTO getEventSource() {
		return eventSource;
	}

	public void setEventSource(ResourceDTO eventSource) {
		this.eventSource = eventSource;
	}

	public void setEventAttachment(ESponderEntityDTO eventAttachment) {
		this.eventAttachment = eventAttachment;
	}

	public Date getEventTimestamp() {
		return eventTimestamp;
	}

	public void setEventTimestamp(Date eventTimestamp) {
		this.eventTimestamp = eventTimestamp;
	}

	public SeverityLevelDTO getEventSeverity() {
		return eventSeverity;
	}

	public void setEventSeverity(SeverityLevelDTO eventSeverity) {
		this.eventSeverity = eventSeverity;
	}
	
}

