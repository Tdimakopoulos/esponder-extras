package eu.esponder.model.snapshot.status;

public enum ActionPartSnapshotStatus {
	STARTED,
	PAUSED,
	COMPLETED
}
