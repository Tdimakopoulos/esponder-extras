package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.type.EquipmentType;


@Entity
@Table(name="equipment")
@NamedQueries({
	@NamedQuery(name="Equipment.findByTitle", query="select e from Equipment e where e.title=:title")
})
public class Equipment extends Resource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4705569532455885376L;

	@ManyToOne
	@JoinColumn(name="EQUIPMENT_TYPE_ID", nullable=false)
	private EquipmentType equipmentType;
	
	@ManyToOne
	@JoinColumn(name="ACTOR_ID")
	private Actor actor;
	
	@ManyToOne
	@JoinColumn(name="CONTAINER_ID")
	private Equipment container;
	
	@OneToMany(mappedBy="container")
	private Set<Equipment> parts;
	
	@OneToMany(mappedBy="equipment")
	private Set<Sensor> sensors;

	public EquipmentType getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(EquipmentType equipmentType) {
		this.equipmentType = equipmentType;
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Equipment getContainer() {
		return container;
	}

	public void setContainer(Equipment container) {
		this.container = container;
	}

	public Set<Equipment> getParts() {
		return parts;
	}

	public void setParts(Set<Equipment> parts) {
		this.parts = parts;
	}

	public Set<Sensor> getSensors() {
		return sensors;
	}

	public void setSensors(Set<Sensor> sensors) {
		this.sensors = sensors;
	}
	
}
