package eu.esponder.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.WeakHashMap;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eu.esponder.model.user.ESponderUser;

@Embeddable
public class ActionAudit implements Serializable {

	private static final long serialVersionUID = -7234944727522572621L;
	
	private static Map<Thread, ThreadLocal<ESponderUser>> tLocals = new WeakHashMap<Thread, ThreadLocal<ESponderUser>>();
	
	public static void storeWho(Thread thread, ESponderUser who) {
		ThreadLocal<ESponderUser> tLocal = tLocals.get(thread);
		if (null == tLocal) {
			tLocal = new ThreadLocal<ESponderUser>();
		}
		tLocal.set(who);
		tLocals.put(thread, tLocal);
	}

	public static  ESponderUser getWho(Thread thread) {
		ThreadLocal<ESponderUser> threadLocal = tLocals.get(thread);
		return null != threadLocal ? threadLocal.get() : null;
	}
	
	@ManyToOne
	private ESponderUser who;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date when;

	public ActionAudit() {
		super();
		this.when = new Date();
	}

	public ESponderUser getWho() {
		return who;
	}

	public void setWho(ESponderUser who) {
		this.who = who;
	}

	public Date getWhen() {
		return when;
	}

	public void setWhen(Date when) {
		this.when = when;
	}

	@Override
	public String toString() {
		return "ActionAudit [who=" + who + ", when=" + when + "]";
	}

}
