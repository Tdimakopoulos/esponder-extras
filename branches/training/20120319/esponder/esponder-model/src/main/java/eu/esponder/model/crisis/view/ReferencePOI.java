package eu.esponder.model.crisis.view;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@DiscriminatorValue("REF")
@NamedQueries({
	@NamedQuery(name="ReferencePOI.findByOperationsCentre", query="select r from ReferencePOI r where r.operationsCentre.id=:operationsCentreID")
})
public class ReferencePOI extends ResourcePOI {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7856655848723729892L;
	
	@Column(name="REF_FILE")
	private String referenceFile;

	public String getReferenceFile() {
		return referenceFile;
	}

	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

	
}
