package eu.esponder.controller.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.crisis.resource.ResourceStatus;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.resource.EquipmentSnapshot;
import eu.esponder.model.snapshot.status.EquipmentSnapshotStatus;
import eu.esponder.model.type.EquipmentType;

public class EquipmentServiceTest extends ControllerServiceTest {
	
	private static int SECONDS = 20;
	
	@Test(groups="createResources")
	public void testCreateEquipment() {
		
		EquipmentType wimax = (EquipmentType) typeService.findByTitle("FRU WiMAX");
		EquipmentType wifi = (EquipmentType) typeService.findByTitle("FRU WiFi");
		
		Actor firstFRC = actorService.findByTitle("FRC #1");
		Actor firstFR = actorService.findByTitle("FR #1.1");
		Actor secondFR = actorService.findByTitle("FR #1.2");
		Actor secondFRC = actorService.findByTitle("FRC #2");
		Actor thirdFR= actorService.findByTitle("FR #2.1");
		Actor fourthFR = actorService.findByTitle("FR #2.2");
		
		createEquipment(wimax, "FRU #1", firstFRC);
		createEquipment(wifi, "FRU #1.1", firstFR);
		createEquipment(wifi, "FRU #1.2", secondFR);
		createEquipment(wimax, "FRU #2", secondFRC);
		createEquipment(wifi, "FRU #2.1", thirdFR);
		createEquipment(wifi, "FRU #2.2", fourthFR);
	}
	
	@Test(groups="createSnapshots")
	public void testCreateEquipmentSnapshots() {		
		
		Equipment firstFRCEquipment = (Equipment) equipmentService.findByTitle("FRU #1");
		Equipment firstFREquipment = (Equipment) equipmentService.findByTitle("FRU #1.1");
		Equipment secondFREquipment = (Equipment) equipmentService.findByTitle("FRU #1.2");
		Equipment secondFRCEquipment = (Equipment) equipmentService.findByTitle("FRU #2");
		Equipment thirdFREquipment = (Equipment) equipmentService.findByTitle("FRU #2.1");
		Equipment fourthFREquipment = (Equipment) equipmentService.findByTitle("FRU #2.2");

		Period period = this.createPeriod(SECONDS);
		createEquipmentSnapshot(firstFRCEquipment, period);
		createEquipmentSnapshot(firstFREquipment, period);
		createEquipmentSnapshot(secondFREquipment, period);
		createEquipmentSnapshot(secondFRCEquipment, period);
		createEquipmentSnapshot(thirdFREquipment, period);
		createEquipmentSnapshot(fourthFREquipment, period);
	}
	
	private Equipment createEquipment(EquipmentType type, String title, Actor actor) {
		
		Equipment equipment = new Equipment();
		equipment.setEquipmentType(type);
		equipment.setTitle(title);
		equipment.setStatus(ResourceStatus.AVAILABLE);
		equipment.setActor(actor);
		return equipmentService.create(equipment, this.userID);
	}
	
	private EquipmentSnapshot createEquipmentSnapshot(Equipment equipment, Period period) {
		
		EquipmentSnapshot snapshot = new EquipmentSnapshot();
		snapshot.setEquipment(equipment);
		snapshot.setPeriod(period);
		snapshot.setStatus(EquipmentSnapshotStatus.WORKING);
		return equipmentService.createSnapshot(snapshot, this.userID);
	}
	
}
