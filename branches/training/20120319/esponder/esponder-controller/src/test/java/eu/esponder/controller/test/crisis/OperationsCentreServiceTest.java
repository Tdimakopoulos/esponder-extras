package eu.esponder.controller.test.crisis;

import java.math.BigDecimal;
import java.util.HashSet;

import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.resource.ResourceStatus;
import eu.esponder.model.crisis.view.MapPoint;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.crisis.view.SketchPOI;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.location.LocationArea;
import eu.esponder.model.snapshot.location.Point;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;
import eu.esponder.model.snapshot.status.OperationsCentreSnapshotStatus;
import eu.esponder.model.type.EmergencyOperationsCentre;
import eu.esponder.model.type.MobileEmergencyOperationsCentre;
import eu.esponder.model.type.OperationsCentreType;
import eu.esponder.model.user.ESponderUser;

public class OperationsCentreServiceTest extends ControllerServiceTest {
	
	private static int SECONDS = 30;
	private static double RADIUS = 1;
	
	@Test(groups="createResources")
	public void testCreateOperationsCentres() {
		
		EmergencyOperationsCentre eoc = (EmergencyOperationsCentre) typeService.findByTitle("EOC");
		MobileEmergencyOperationsCentre meoc = (MobileEmergencyOperationsCentre) typeService.findByTitle("MEOC");
		
		OperationsCentre eocAttica = createOperationsCentre(eoc, "EOC Attica", null);
		OperationsCentre meocAthens = createOperationsCentre(meoc, "MEOC Athens", eocAttica);
		OperationsCentre meocPiraeus = createOperationsCentre(meoc, "MEOC Piraeus", eocAttica);
		
		ESponderUser ctri = userService.findByUserName("ctri");
		ESponderUser gleo = userService.findByUserName("gleo");
		ESponderUser kotso = userService.findByUserName("kotso");
		
		HashSet<OperationsCentre> ctriChannels = new HashSet<OperationsCentre>();
		ctriChannels.add(meocAthens);
		ctri.setOperationsCentres(ctriChannels);
		userService.update(ctri, this.userID);
		
		HashSet<OperationsCentre> gleoChannels = new HashSet<OperationsCentre>();
		gleoChannels.add(meocPiraeus);
		gleo.setOperationsCentres(gleoChannels);
		userService.update(gleo, this.userID);
		
		HashSet<OperationsCentre> kotsoChannels = new HashSet<OperationsCentre>();
		kotsoChannels.add(eocAttica);
		kotso.setOperationsCentres(kotsoChannels);
		userService.update(kotso, this.userID);
	}
	
	@Test(groups="createSnapshots")
	public void testCreateOperationsCentreSnapshots() {		
		
		OperationsCentre eocAttica = (OperationsCentre) operationsCentreService.findByTitle("EOC Attica");
		OperationsCentre meocAthens = (OperationsCentre) operationsCentreService.findByTitle("MEOC Athens");
		OperationsCentre meocPiraeus = (OperationsCentre) operationsCentreService.findByTitle("MEOC Piraeus");

		Period period = this.createPeriod(SECONDS);
		
		createOperationsCentreSnapshot(eocAttica, period, this.createSphere(37.939577, 23.829346, null, RADIUS));
		createOperationsCentreSnapshot(meocAthens, period, this.createSphere(38.023069, 23.78643, null, RADIUS));
		createOperationsCentreSnapshot(meocPiraeus, period, this.createSphere(37.950623, 23.642406, null, RADIUS));
	}
	
	@Test(groups="createPOIs")
	public void testCreateSketches() {
		
		OperationsCentre meocAthens = (OperationsCentre) operationsCentreService.findByTitle("MEOC Athens");
		OperationsCentre meocPiraeus = (OperationsCentre) operationsCentreService.findByTitle("MEOC Piraeus");
		
		SketchPOI sketch1 = new SketchPOI();
		sketch1.setTitle("Sketch 1");
		sketch1.setPoints(new HashSet<MapPoint>());
		sketch1.getPoints().add(createMapPoint("point11"));
		sketch1.getPoints().add(createMapPoint("point12"));
		sketch1.getPoints().add(createMapPoint("point13"));
	
		SketchPOI sketch2 = new SketchPOI();
		sketch2.setTitle("Sketch 2");
		sketch2.setPoints(new HashSet<MapPoint>());
		sketch2.getPoints().add(createMapPoint("point21"));
		sketch2.getPoints().add(createMapPoint("point22"));		
		
		operationsCentreService.createSketch(meocAthens, sketch1, this.userID);
		operationsCentreService.createSketch(meocAthens, sketch2, this.userID);
		
		SketchPOI sketch3 = new SketchPOI();
		sketch3.setTitle("Sketch 3");
		sketch3.setPoints(new HashSet<MapPoint>());
		sketch3.getPoints().add(createMapPoint("point31"));
		sketch3.getPoints().add(createMapPoint("point32"));
		sketch3.getPoints().add(createMapPoint("point33"));
		sketch3.getPoints().add(createMapPoint("point34"));
		
		operationsCentreService.createSketch(meocPiraeus, sketch3, this.userID);
	}
	
	@Test(groups="createPOIs")
	public void testCreateReferencePOIs() {
		
		OperationsCentre meocAthens = (OperationsCentre) operationsCentreService.findByTitle("MEOC Athens");
		OperationsCentre meocPiraeus = (OperationsCentre) operationsCentreService.findByTitle("MEOC Piraeus");
		
		ReferencePOI ref1 = createReferencePOI("file 1", "first file location");
		operationsCentreService.createReferencePOI(meocAthens, ref1, this.userID);
		
		ReferencePOI ref2 = createReferencePOI("file 2", "second file location");
		operationsCentreService.createReferencePOI(meocPiraeus, ref2, this.userID);
	}

	private OperationsCentre createOperationsCentre(
			OperationsCentreType type, String title, OperationsCentre supervisor) {
		
		OperationsCentre operationsCentre = new OperationsCentre();
		operationsCentre.setOperationsCentreType(type);
		operationsCentre.setTitle(title);
		operationsCentre.setStatus(ResourceStatus.AVAILABLE);
		operationsCentre.setSupervisor(supervisor);
		return operationsCentreService.create(operationsCentre, this.userID);
	}
	
	private OperationsCentreSnapshot createOperationsCentreSnapshot(
			OperationsCentre operationsCentre, Period period, LocationArea locationArea) {
		
		OperationsCentreSnapshot snapshot = new OperationsCentreSnapshot();
		snapshot.setLocationArea(locationArea);
		snapshot.setPeriod(period);
		snapshot.setStatus(OperationsCentreSnapshotStatus.MOVING);
		return operationsCentreService.createSnapshot(operationsCentre, snapshot, this.userID);
	}
	
	private MapPoint createMapPoint(String title) {
		MapPoint mapPoint = new MapPoint();
		mapPoint.setTitle(title);
		Point point = new Point();
		point.setLongitude(BigDecimal.ONE);
		point.setLatitude(BigDecimal.ONE);
		mapPoint.setPoint(point);
		return mapPoint;
	}
	
	private ReferencePOI createReferencePOI(String referenceFile, String title) {
		ReferencePOI ref = new ReferencePOI();
		ref.setReferenceFile(referenceFile);
		ref.setTitle(title);
		return ref;
	}
	
}