package eu.esponder.controller.test.crisis;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.Test;

import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.controller.persistence.criteria.EsponderCriterion;
import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.jaxb.model.criteria.EsponderCriterionDTO;
import eu.esponder.jaxb.model.criteria.EsponderCriterionExpressionEnumDTO;
import eu.esponder.jaxb.model.criteria.EsponderIntersectionCriteriaCollectionDTO;
import eu.esponder.jaxb.model.criteria.EsponderQueryRestrictionDTO;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.test.ResourceLocator;



public class CriteriaMappingTest {

	protected static final String USER_NAME = "ctri";
	protected Long userID;
	protected UserRemoteService userService;
	protected ESponderRemoteMappingService testMappingService;
	protected GenericRemoteService genericService;

	@Test
	public void testCriteriaMapping () throws Exception {

		testMappingService = ResourceLocator.lookup("esponder/ESponderMappingBean/remote");
		genericService = ResourceLocator.lookup("esponder/GenericBean/remote");

		EsponderIntersectionCriteriaCollectionDTO testCollectionDTO = new EsponderIntersectionCriteriaCollectionDTO(new HashSet<EsponderQueryRestrictionDTO>());
		EsponderCriterionDTO ecriterion = new EsponderCriterionDTO();
		EsponderCriterionDTO ecriterion2 = new EsponderCriterionDTO();

		ecriterion.setFieldName("id");
		ecriterion.setFieldValue(new Long(15));
		ecriterion.setExpression(EsponderCriterionExpressionEnumDTO.GREATER_THAN);
		testCollectionDTO.add(ecriterion);

		ecriterion2.setFieldName("id");
		ecriterion2.setFieldValue(new Long(50));
		ecriterion2.setExpression(EsponderCriterionExpressionEnumDTO.LESS_THAN_OR_EQUAL);
		testCollectionDTO.add(ecriterion2);


		EsponderQueryRestriction testCollection = testMappingService.mapCriteriaCollection((EsponderQueryRestrictionDTO)testCollectionDTO);


		/*******************************************************/

		

		
		EsponderQueryRestriction restriction = testMappingService.mapSimpleCriterion(ecriterion);
		String targetFieldName = ((EsponderCriterion) restriction).getFieldName();

		Field testField = SensorSnapshot.class.getDeclaredField(targetFieldName);
		Class<?> testClass = testField.getType();
		System.out.println("Inspected Class is "+testField.getType());

		if(testClass.getClass().getName()==Long.class.getName())
			System.out.println("It's a Long Class object");
		else
			System.out.println("Sth went wrong here");

		/*******************************************************/

		List<?> resultsList = (List<?>) genericService.getEntities(SensorSnapshot.class, (EsponderQueryRestriction) testCollection, new Integer(5));

		if(resultsList.isEmpty()){
			System.out.println("\n\nEmpty result List, something went wrong or the query returned no results...\n\n");
		}
		else {
			System.out.println("\n Results returned size : "+ resultsList.size());
			System.out.println("Sensor Title \t\t Mean Value");
			Iterator<?> i = resultsList.iterator();
			while(i.hasNext())
			{
				SensorSnapshot element = (SensorSnapshot) i.next();
				System.out.println(element.getSensor().getTitle()+" \t\t "+element.getMeanValue());
			}
		}

	}

}
