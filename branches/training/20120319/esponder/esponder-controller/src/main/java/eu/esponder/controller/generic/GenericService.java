package eu.esponder.controller.generic;

import javax.ejb.Local;

@Local
public interface GenericService extends GenericRemoteService {

}
