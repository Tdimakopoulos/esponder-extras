package eu.esponder.controller.persistence.criteria;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class EsponderUnionCriteriaCollection extends EsponderCriteriaCollection {

	private static final long serialVersionUID = 7938678292927863036L;

	public Set<EsponderQueryRestriction> restrictions = new HashSet<EsponderQueryRestriction>();
	
	
	public Set<EsponderQueryRestriction> getRestrictions() {
		return (Set<EsponderQueryRestriction>) restrictions;
	}
	
	public void setRestrictions(Set<EsponderQueryRestriction> restrictions) {
		this.restrictions = restrictions;
	}
	
	public EsponderUnionCriteriaCollection(
			Collection<EsponderQueryRestriction> restrictions) {
		super(restrictions);
	}

}
