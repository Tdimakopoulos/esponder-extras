package eu.esponder.controller.persistence.bean;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import eu.esponder.controller.persistence.CrudRemoteService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.controller.persistence.criteria.EsponderCriteriaCollection;
import eu.esponder.controller.persistence.criteria.EsponderCriterion;
import eu.esponder.controller.persistence.criteria.EsponderIntersectionCriteriaCollection;
import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.controller.persistence.criteria.EsponderUnionCriteriaCollection;
import eu.esponder.model.ESponderEntity;

@Stateless
public class CrudBean<T> implements CrudService<T>, CrudRemoteService<T> {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Flushes the persistence context.
	 */
	public void flush() {
		em.flush();
	}

	/**
	 * Perform an initial save of a previously unsaved T entity. All subsequent
	 * persist actions of this entity should use the #update() method. This
	 * operation must be performed within the a database transaction context for
	 * the entity's data to be permanently saved to the persistence store, i.e.,
	 * database. This method uses the
	 * {@link javax.persistence.EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 * 
	 * @param t
	 *            T entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public T create(T t) {
		this.em.persist(t);
		this.em.flush();
		this.em.refresh(t);
		return t;

	}

	/**
	 * Find a T entity based on its id.
	 * 
	 * @param id
	 *            The T id.
	 * @return A T entity.
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public T find(Class<T> type, Object id) {
		return this.em.find(type, id);
	}

	public void delete(Class<T> type, Object id) {
		Object ref = this.em.getReference(type, id);
		this.em.remove(ref);
	}

	/**
	 * Delete a persistent T entity. This operation must be
	 * performed within the a database transaction context for the entity's data
	 * to be permanently deleted from the persistence store, i.e., database.
	 * This method uses the
	 * {@link javax.persistence.EntityManager#remove(Object) operation.
	 * 
	 * @param entity
	 *            entity to delete
	 */
	public void delete(T entity) {
		em.remove(entity);
	}

	/**
	 * Persist a previously saved T entity and return it or a copy of it to the
	 * sender. A copy of the T entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 * 
	 * @param t
	 *            T entity to update
	 * @return T the persisted T entity instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public T update(T t) {
		return this.em.merge(t);
	}

	/**
	 * @param t
	 *            T entity to refresh
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public void refresh(T t) {
		this.em.refresh(t);
	}

	@SuppressWarnings("unchecked")
	public List<T> findWithNamedQuery(String namedQueryName) {
		return this.em.createNamedQuery(namedQueryName).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<T> findWithNamedQuery(String queryName, int resultLimit) {
		return this.em.createNamedQuery(queryName).setMaxResults(resultLimit)
				.getResultList();
	}

	public List<T> findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters) {
		return this.findWithNamedQuery(namedQueryName, parameters, 0);

	}

	@SuppressWarnings("unchecked")
	public List<T> findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters, int resultLimit) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createNamedQuery(namedQueryName);
		if (resultLimit > 0)
			query.setMaxResults(resultLimit);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();

	}

	public Object findSingleWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters) {
		Object result = null;

		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createNamedQuery(namedQueryName);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		try {
			result = query.getSingleResult();
		} catch (NoResultException nre) {
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public List<T> findWithQuery(String query) {
		return this.em.createQuery(query).getResultList();

	}

	@SuppressWarnings("unchecked")
	public List<T> findWithQuery(String query, int resultLimit) {
		return this.em.createQuery(query).setMaxResults(resultLimit)
				.getResultList();
	}

	public List<T> findWithQuery(String query, Map<String, Object> parameters) {
		return this.findWithQuery(query, parameters, 0);

	}

	@SuppressWarnings("unchecked")
	public List<T> findWithQuery(String queryStr,
			Map<String, Object> parameters, int resultLimit) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createQuery(queryStr);
		if (resultLimit > 0)
			query.setMaxResults(resultLimit);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();

	}

	@SuppressWarnings("rawtypes")
	public List<T> findWithCriteriaQuery(Class<? extends ESponderEntity> queriedClass,
			EsponderQueryRestriction criteria, int resultLimit) {

		org.hibernate.Session session = (Session) em.getDelegate();

		Criteria crit = session.createCriteria(queriedClass);
		Criterion criterion= null;

		if (criteria instanceof EsponderCriterion) {
			EsponderCriterion esponderCriterion = (EsponderCriterion) criteria;
			if(complementaryMapping(queriedClass, esponderCriterion)) {
				criterion = (Criterion) createSimpleExpression(esponderCriterion);
			}			
		} else {
			EsponderCriteriaCollection esponderCriteriaCollection = (EsponderCriteriaCollection) criteria;
			Junction junction = getJunction(esponderCriteriaCollection);
			for (EsponderQueryRestriction restriction : esponderCriteriaCollection.getRestrictions()) {
				handleEsponderRestrictionCollection(queriedClass, junction, restriction);
			}
			criterion = junction;
		}

		crit.add(criterion);
		if(resultLimit!=0)
		{
			crit.setMaxResults(resultLimit);
		}
		System.out.println("query was built successfully, criteria added...");

		@SuppressWarnings("unchecked")
		List<T> results = crit.list();

		session.close();

		return results;
	}

	private Junction getJunction(EsponderCriteriaCollection collection) {
		if (collection.getClass().equals(EsponderUnionCriteriaCollection.class)) {
			return Restrictions.disjunction();
		} else if (collection.getClass().equals(
				EsponderIntersectionCriteriaCollection.class)) {
			return Restrictions.conjunction();
		} else {
			return null;
		}
	}

	private void handleEsponderRestrictionCollection(Class<? extends ESponderEntity> queriedClass, Junction junction, EsponderQueryRestriction restriction) {

		if (restriction instanceof EsponderCriterion) {
			EsponderCriterion esponderCriterion = (EsponderCriterion) restriction;
			System.out.println("Selected Class is : "+queriedClass);
			if(complementaryMapping(queriedClass, restriction)) {
				addCriterion(junction, esponderCriterion);
			}
		} else {
			EsponderCriteriaCollection collection = (EsponderCriteriaCollection) restriction;
			for (EsponderQueryRestriction innerRestriction : collection.getRestrictions()) {
				// junction.add(handleEsponderRestrictionCollection(junction, innerRestriction));
				handleEsponderRestrictionCollection(queriedClass, junction, innerRestriction);
			}
		}
	}


	private void addCriterion(Junction junction,
			EsponderCriterion esponderCriterion) {
		junction.add(createSimpleExpression(esponderCriterion));

	}

	private SimpleExpression createSimpleExpression(EsponderCriterion esponderCriterion) {

		String fieldStr = esponderCriterion.getFieldName();
		Object valueObj = esponderCriterion.getFieldValue();
		SimpleExpression simpleExpression = null;
		switch (esponderCriterion.getExpression()) {
		case EQUAL:
			simpleExpression = getRestrictionEquals(fieldStr, valueObj);
			break;
		case GREATER_THAN:
			simpleExpression = getRestrictionGT(fieldStr, valueObj);
			break;
		case GREATER_THAN_OR_EQUAL:
			simpleExpression = getRestrictionGTE(fieldStr, valueObj);
			break;
		case LESS_THAN:
			simpleExpression = getRestrictionLT(fieldStr, valueObj);
			break;
		case LESS_THAN_OR_EQUAL:
			simpleExpression = getRestrictionLTE(fieldStr, valueObj);
			break;
		default:
			System.out.println("BUG in simpleExpression creation");
		}
		return simpleExpression;
	}


	private Boolean complementaryMapping(Class<? extends ESponderEntity> queriedClass, EsponderQueryRestriction restriction) throws SecurityException {
		String targetFieldName = ((EsponderCriterion) restriction).getFieldName();
		Field testField=null;

		Field[] arrayDeclaredField =  queriedClass.getDeclaredFields();
		for(Field i:arrayDeclaredField) {
			//System.out.println("DeclaredFields : "+i.getName());
			if( targetFieldName.equals(i.getName()) ) {
				testField = i;
				break;
			}
		}

		if(testField == null) {
			Field[] arrayGetField =  queriedClass.getFields();
			for(Field i:arrayGetField) {
				//System.out.println("GetFields : "+i.getName());
				if( targetFieldName.equals(i.getName()) ) {
					testField = i;
					break;
				}
			}
		}

		if(testField == null) {
			List<Field> fieldsList = getAllFields(queriedClass);
			if (fieldsList != null) {
				for(Field i : fieldsList)
				{
					if( targetFieldName.equals(i.getName()) ) {
						testField = i;
						//System.out.println("Field Found");
						break;
					}
				}
			}
		}


		if(testField!=null) {
			Class<?> testClass = testField.getType();
			Constructor<?> constructor = null;
			try {
				constructor = (testClass).getConstructor(String.class);
			} catch (NoSuchMethodException e) {
				System.out.println("No such method exists for selected class");
				e.printStackTrace();
			}
			Object testObject = null;
			try {
				testObject = constructor.newInstance(((EsponderCriterion) restriction).getFieldValue().toString());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				return false;
			} catch (InstantiationException e) {
				e.printStackTrace();
				return false;
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				return false;
			} catch (InvocationTargetException e) {
				e.printStackTrace();
				return false;
			}
			((EsponderCriterion) restriction).setFieldValue(testObject);
			return true;
		}			
		else {
			//System.out.println("Field not found");
			return false;
		}

	}


	public static List<Field> getAllFields(Class<?> clz) {
		List<Field> fields = new ArrayList<Field>();
		Class<?> superclass = clz.getSuperclass();
		while (superclass !=null) {
			System.out.println("The class is : "+superclass.getName());
			for (Field field : superclass.getFields()) {
				fields.add(field);
			}
			for (Field field : superclass.getDeclaredFields()) {
				fields.add(field);
			}
			/*if((superclass.getName().equals("eu.esponder.model.ESponderEntity")))
				break;*/
			superclass = superclass.getSuperclass();
		}
		return fields;
	}


	private SimpleExpression getRestrictionEquals(String field, Object value) {
		return Restrictions.eq(field, value);
	}

	private SimpleExpression getRestrictionGT(String field, Object value) {
		return Restrictions.gt(field, value);
	}

	private SimpleExpression getRestrictionGTE(String field, Object value) {
		return Restrictions.ge(field, value);
	}

	private SimpleExpression getRestrictionLT(String field, Object value) {
		return Restrictions.lt(field, value);
	}

	private SimpleExpression getRestrictionLTE(String field, Object value) {
		return Restrictions.le(field, value);
	}

	public T getReference(Class<T> type, Object id) {
		return this.em.getReference(type, id);
	}

	public EntityManager getEntityManager() {
		return em;
	}

}
