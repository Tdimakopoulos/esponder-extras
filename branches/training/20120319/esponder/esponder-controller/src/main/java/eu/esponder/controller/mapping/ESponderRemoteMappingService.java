package eu.esponder.controller.mapping;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.esponder.controller.persistence.criteria.EsponderCriterion;
import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.jaxb.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.jaxb.model.crisis.view.ReferencePOIDTO;
import eu.esponder.jaxb.model.crisis.view.SketchPOIDTO;
import eu.esponder.jaxb.model.criteria.EsponderCriterionDTO;
import eu.esponder.jaxb.model.criteria.EsponderQueryRestrictionDTO;
import eu.esponder.jaxb.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.crisis.view.SketchPOI;

@Remote
public interface ESponderRemoteMappingService {

	public List<OperationsCentreDTO> mapUserOperationsCentres(Long userID);
	
	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID);
	
	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID, Long userID);
	
	public List<SketchPOIDTO> mapOperationsCentreSketches(Long operationsCentreID, Long userID);
	
	public List<ReferencePOIDTO> mapOperationsCentreReferencePOIs(Long operationsCentreID, Long userID);
	
	public OperationsCentreSnapshotDTO mapOperationsCentreSnapshot(Long operationsCentreID, Date maxDate);
	
	public ActorSnapshotDTO mapActorSnapshot(Long actorID, Date maxDate);
	
	public EquipmentSnapshotDTO mapEquipmentSnapshot(Long equipmentID, Date maxDate);
	
	public SensorSnapshotDTO mapSensorSnapshot(Long sensorID, Date maxDate);
	
	public SketchPOI mapSketch(SketchPOIDTO sketchDTO);
	
	public ReferencePOI mapReferencePOI(ReferencePOIDTO referencePOIDTO);
	
	public EsponderQueryRestriction mapCriteriaCollection(EsponderQueryRestrictionDTO criteriaDTO);
	
	public EsponderCriterion mapSimpleCriterion(EsponderCriterionDTO criteria);
	
}
