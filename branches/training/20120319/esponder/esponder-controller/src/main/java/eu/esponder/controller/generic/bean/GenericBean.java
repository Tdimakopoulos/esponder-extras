package eu.esponder.controller.generic.bean;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.generic.GenericService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.model.ESponderEntity;

@Stateless
public class GenericBean implements GenericRemoteService, GenericService {
	
	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<? extends ESponderEntity> getEntities(
			Class<? extends ESponderEntity> clz,
			EsponderQueryRestriction criteria, int resultLimit) {
		return (List<ESponderEntity>) crudService.findWithCriteriaQuery(clz, criteria, resultLimit);
	}

}
