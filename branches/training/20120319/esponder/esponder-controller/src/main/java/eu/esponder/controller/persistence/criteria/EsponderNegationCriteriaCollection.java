package eu.esponder.controller.persistence.criteria;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class EsponderNegationCriteriaCollection extends EsponderCriteriaCollection {
	
	private static final long serialVersionUID = 1754280753704823556L;

	public Set<EsponderQueryRestriction> restrictions = new HashSet<EsponderQueryRestriction>();
	
	
	public Set<EsponderQueryRestriction> getRestrictions() {
		return (Set<EsponderQueryRestriction>) restrictions;
	}
	
	public void setRestrictions(Set<EsponderQueryRestriction> restrictions) {
		this.restrictions = restrictions;
	}
	
	
	public EsponderNegationCriteriaCollection(
			Collection<EsponderQueryRestriction> restrictions) {
		super(restrictions);
	}

}
