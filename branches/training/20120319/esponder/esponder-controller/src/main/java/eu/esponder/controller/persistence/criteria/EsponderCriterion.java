package eu.esponder.controller.persistence.criteria;

public class EsponderCriterion extends EsponderQueryRestriction  {

	private static final long serialVersionUID = -6667733307605361832L;

	private String field;
	
	private EsponderCriterionExpressionEnum expression;
	
	private Object fieldValue;

	public String getFieldName() {
		return field;
	}

	public void setFieldName(String field) {
		this.field = field;
	}

	public EsponderCriterionExpressionEnum getExpression() {
		return expression;
	}

	public void setExpression(EsponderCriterionExpressionEnum expression) {
		this.expression = expression;
	}

	public Object getFieldValue() {
		return fieldValue;
	}

	/*public void setFieldValue(Serializable value) {
		this.fieldValue = value;
	}*/
	
	public void setFieldValue(Object value) {
		this.fieldValue = value;
	}
	
}
