package eu.esponder.controller.generic;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.model.ESponderEntity;

@Remote
public interface GenericRemoteService {

	@SuppressWarnings("rawtypes")
	public List<? extends ESponderEntity> getEntities(Class<? extends ESponderEntity> clz, 
			EsponderQueryRestriction ecriterion, int resultLimit);

}
