package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.crisis.resource.OperationsCentreService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.snapshot.resource.ActorSnapshot;
import eu.esponder.model.type.ActorType;

@Stateless
@SuppressWarnings("unchecked")
public class ActorBean implements ActorService, ActorRemoteService {
	
	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	@EJB
	private TypeService typeService;
	
	@EJB
	private OperationsCentreService operationsCentreService;
	
	//-------------------------------------------------------------------------
	
	@Override
	public Actor findById(Long actorID) {
		return (Actor) crudService.find(Actor.class, actorID);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public Actor findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Actor) crudService.findSingleWithNamedQuery("Actor.findByTitle", params);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public ActorSnapshot findSnapshotByDate(Long actorID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("actorID", actorID);
		params.put("maxDate", maxDate);
		ActorSnapshot snapshot =
				(ActorSnapshot) crudService.findSingleWithNamedQuery("ActorSnapshot.findByActorAndDate", params);
		return snapshot;
	}
	
	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Actor create(Actor actor, Long userID) {
		actor.setActorType((ActorType)typeService.findById(actor.getActorType().getId()));
		if (null != actor.getSupervisor()) {
			actor.setSupervisor(this.findById(actor.getSupervisor().getId()));
		}
		if (null != actor.getOperationsCentre()) {
			actor.setOperationsCentre(operationsCentreService.findById(actor.getOperationsCentre().getId()));
		}
		crudService.create(actor);
		return actor;
	}

	//-------------------------------------------------------------------------
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorSnapshot createSnapshot(ActorSnapshot snapshot, Long userID) {
		snapshot.setActor((Actor)this.findById(snapshot.getActor().getId()));
		crudService.create(snapshot);
		return snapshot;
	}
	
}
