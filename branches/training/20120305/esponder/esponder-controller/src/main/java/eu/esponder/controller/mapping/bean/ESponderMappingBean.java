package eu.esponder.controller.mapping.bean;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.crisis.resource.EquipmentService;
import eu.esponder.controller.crisis.resource.OperationsCentreService;
import eu.esponder.controller.crisis.resource.SensorService;
import eu.esponder.controller.crisis.user.UserService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.jaxb.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.jaxb.model.crisis.view.ReferencePOIDTO;
import eu.esponder.jaxb.model.crisis.view.SketchPOIDTO;
import eu.esponder.jaxb.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.crisis.view.SketchPOI;
import eu.esponder.model.snapshot.resource.ActorSnapshot;
import eu.esponder.model.snapshot.resource.EquipmentSnapshot;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.user.ESponderUser;
import eu.esponder.util.mapping.MappingService;
import eu.esponder.util.mapping.MappingServiceFactory;
import eu.esponder.util.mapping.MappingServiceFactory.MappingServiceType;

@Stateless
public class ESponderMappingBean implements ESponderMappingService {
	
	private final static MappingService MAPPER = 
			MappingServiceFactory.getMappingService(MappingServiceType.CONFIG);
	
	@EJB
	private UserService userService;
	
	@EJB
	private OperationsCentreService operationsCentreService;
	
	@EJB
	private ActorService actorService;
	
	@EJB
	private EquipmentService equipmentService;
	
	@EJB
	private SensorService sensorService;
	
	@Override
	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID) {
		OperationsCentre operationsCentre = operationsCentreService.findById(operationsCentreID);
		return null != operationsCentre ? MAPPER.transform(operationsCentre, OperationsCentreDTO.class) : null;
	}
	
	@Override
	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID, Long userID) {
		
		OperationsCentre operationsCentre = operationsCentreService.findByUser(operationsCentreID, userID);
		return null != operationsCentre ? MAPPER.transform(operationsCentre, OperationsCentreDTO.class) : null;
	}
	
	@Override
	public List<OperationsCentreDTO> mapUserOperationsCentres(Long userID) {
		
		ESponderUser user = userService.findById(userID);
		return null != user ? MAPPER.transform(user.getOperationsCentres(), OperationsCentreDTO.class) : null;
	}
	
	@Override
	public List<SketchPOIDTO> mapOperationsCentreSketches(Long operationsCentreID, Long userID) {

		return MAPPER.transform(operationsCentreService.findSketchPOIs(operationsCentreID), SketchPOIDTO.class);
	}
	
	@Override
	public List<ReferencePOIDTO> mapOperationsCentreReferencePOIs(Long operationsCentreID, Long userID) {
		
		return MAPPER.transform(operationsCentreService.findReferencePOIs(operationsCentreID), ReferencePOIDTO.class);
	}
	
	@Override
	public OperationsCentreSnapshotDTO mapOperationsCentreSnapshot(Long operationsCentreID, Date maxDate) {
		
		OperationsCentreSnapshot snapshot = operationsCentreService.findSnapshotByDate(operationsCentreID, maxDate);
		return null != snapshot ? MAPPER.transform(snapshot, OperationsCentreSnapshotDTO.class) : null;
	}
	
	@Override
	public ActorSnapshotDTO mapActorSnapshot(Long actorID, Date maxDate) {
		
		ActorSnapshot snapshot = actorService.findSnapshotByDate(actorID, maxDate);
		return null != snapshot ? MAPPER.transform(snapshot, ActorSnapshotDTO.class) : null;
	}
	
	@Override
	public EquipmentSnapshotDTO mapEquipmentSnapshot(Long equipmentID, Date maxDate) {
		
		EquipmentSnapshot snapshot = equipmentService.findSnapshotByDate(equipmentID, maxDate);
		return null != snapshot ? MAPPER.transform(snapshot, EquipmentSnapshotDTO.class) : null;
	}
	
	@Override
	public SensorSnapshotDTO mapSensorSnapshot(Long sensorID, Date maxDate) {
		
		SensorSnapshot snapshot = sensorService.findSnapshotByDate(sensorID, maxDate);
		return null != snapshot ? MAPPER.transform(snapshot, SensorSnapshotDTO.class) : null;
	}
	
	@Override
	public SketchPOI mapSketch(SketchPOIDTO sketchDTO) {
		return null != sketchDTO ? MAPPER.transform(sketchDTO, SketchPOI.class) : null;
	}
	
	@Override
	public ReferencePOI mapReferencePOI(ReferencePOIDTO referencePOIDTO) {
		return null != referencePOIDTO ? MAPPER.transform(referencePOIDTO, ReferencePOI.class) : null;
	}

}
