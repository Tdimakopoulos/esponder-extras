package eu.esponder.controller.crisis;

import javax.ejb.Remote;

import eu.esponder.model.type.ESponderType;

@Remote
public interface TypeRemoteService {
	
	public ESponderType findById(Long typeId);
	
	public ESponderType findByTitle(String title);
	
	public ESponderType create(ESponderType type, Long userID);

}
