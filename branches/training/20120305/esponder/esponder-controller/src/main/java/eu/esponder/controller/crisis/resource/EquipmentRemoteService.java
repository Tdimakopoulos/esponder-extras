package eu.esponder.controller.crisis.resource;

import java.util.Date;

import javax.ejb.Remote;

import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.snapshot.resource.EquipmentSnapshot;

@Remote
public interface EquipmentRemoteService {
	
	public Equipment findById(Long equipmentID);
	
	public Equipment findByTitle(String title);
	
	public EquipmentSnapshot findSnapshotByDate(Long equipmentID, Date dateTo);
	
	public Equipment create(Equipment equipment, Long userID);
	
	public EquipmentSnapshot createSnapshot(EquipmentSnapshot snapshot, Long userID);
	
}
