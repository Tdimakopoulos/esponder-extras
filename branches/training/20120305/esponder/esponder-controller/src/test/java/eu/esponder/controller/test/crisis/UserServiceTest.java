package eu.esponder.controller.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.model.user.ESponderUser;

public class UserServiceTest extends ControllerServiceTest {
	
	@Test(groups="createBasics")
	public void testCreateUsers() {
		ESponderUser ctri = new ESponderUser();
		ctri.setUserName("ctri");
		userService.create(ctri, null);
		
		ESponderUser gleo = new ESponderUser();
		gleo.setUserName("gleo");
		userService.create(gleo, null);
		
		ESponderUser kotso = new ESponderUser();
		kotso.setUserName("kotso");
		userService.create(kotso, null);
	}
	
}
