package eu.esponder.controller.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.resource.ResourceStatus;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.location.LocationArea;
import eu.esponder.model.snapshot.resource.ActorSnapshot;
import eu.esponder.model.snapshot.status.ActorSnapshotStatus;
import eu.esponder.model.type.ActorType;
import eu.esponder.model.type.OperationalActorType;

public class ActorServiceTest extends ControllerServiceTest {
	
	private static int SECONDS = 30;
	private static double RADIUS = 1;

	@Test(groups="createResources")
	public void testCreateActors() {
		
		OperationalActorType frc = (OperationalActorType) typeService.findByTitle("FRC");
		OperationalActorType fr = (OperationalActorType) typeService.findByTitle("FR");
		
		OperationsCentre meocAthens = (OperationsCentre) operationsCentreService.findByTitle("MEOC Athens");
		OperationsCentre meocPiraeus = (OperationsCentre) operationsCentreService.findByTitle("MEOC Piraeus");

		Actor firstChief = createActor(frc, "FRC #1", null, meocAthens);
		createActor(fr, "FR #1.1", firstChief, null);
		createActor(fr, "FR #1.2", firstChief, null);
		Actor secondChief = createActor(frc, "FRC #2", null, meocPiraeus);
		createActor(fr, "FR #2.1", secondChief, null);
		createActor(fr, "FR #2.2", secondChief, null);
	}
	
	@Test(groups="createSnapshots")
	public void testCreateActorSnapshots() {		
		
		Actor firstFRC = (Actor) actorService.findByTitle("FRC #1");
		Actor firstFR = (Actor) actorService.findByTitle("FR #1.1");
		Actor secondFR = (Actor) actorService.findByTitle("FR #1.2");
		Actor secondFRC = (Actor) actorService.findByTitle("FRC #2");
		Actor thirdFR = (Actor) actorService.findByTitle("FR #2.1");
		Actor fourthFR = (Actor) actorService.findByTitle("FR #2.2");

		Period period = this.createPeriod(SECONDS);
		createActorSnapshot(firstFRC,period, this.createSphere(38.025334, 23.802717, null, RADIUS));
		createActorSnapshot(firstFR, period, this.createSphere(38.02732, 23.805871, null, RADIUS));
		createActorSnapshot(secondFR, period, this.createSphere(38.024615, 23.799348, null, RADIUS));
		createActorSnapshot(secondFRC, period, this.createSphere(37.950118, 23.650045, null, RADIUS));
		createActorSnapshot(thirdFR, period, this.createSphere(37.948493, 23.648157, null, RADIUS));
		createActorSnapshot(fourthFR, period, this.createSphere(37.951065, 23.639746, null, RADIUS));
	}
	
	private Actor createActor(
			ActorType type, String title, Actor supervisor, OperationsCentre operationsCentre) {
		
		Actor actor = new Actor();
		actor.setActorType(type);
		actor.setTitle(title);
		actor.setStatus(ResourceStatus.AVAILABLE);
		actor.setSupervisor(supervisor);
		actor.setOperationsCentre(operationsCentre);
		return actorService.create(actor, this.userID);
	}
	
	private ActorSnapshot createActorSnapshot(Actor actor,Period period, LocationArea locationArea) {
		
		ActorSnapshot snapshot = new ActorSnapshot();
		snapshot.setActor(actor);
		snapshot.setLocationArea(locationArea);
		snapshot.setPeriod(period);
		snapshot.setStatus(ActorSnapshotStatus.ACTIVE);
		return actorService.createSnapshot(snapshot, this.userID);
	}

}
