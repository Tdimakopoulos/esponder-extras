package eu.esponder.util.mapping;

import java.util.WeakHashMap;

/**
 * Factory to acquire an instance of <code>DozerTransformationService</code>.
 *
 */
public class MappingServiceFactory {
	
	private static WeakHashMap<String, MappingBean> map = new WeakHashMap<String, MappingBean>();
	
	public static synchronized MappingService getMappingService(MappingServiceType type) {
		
		MappingBean transformationService = null; 
		
		if (!map.containsKey(type.getConfigFileName())) {
			transformationService = new MappingBean(type.getConfigFileName());
			map.put(type.getConfigFileName(), transformationService);
		} else {			
			transformationService = map.get(type.getConfigFileName());
		}
		return transformationService;
	}
	
	public static enum MappingServiceType {
		CONFIG("dozer.properties");
		
		private String configFileName;
		
		private MappingServiceType(String configFileName) {
			this.configFileName = configFileName;
		}
		
		public String getConfigFileName() {
			return this.configFileName;
		}
	}
	
}
