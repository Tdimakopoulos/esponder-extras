package eu.esponder.util.mapping;

import java.util.Collection;
import java.util.List;

/**
 * Simple interface to provide object-to-object transformation services.
 * 
 */
public interface MappingService {

	public abstract <T> T transform(Object src, Class<T> targetClass);
	
	public abstract <T extends Object, U> List<U> transform(Collection<T> src, Class<U> targetClass);
}
