package eu.esponder.model.crisis.resource;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.model.type.ReusableResourceType;


@Entity
@Table(name="reusable_resource")
@NamedQueries({
	@NamedQuery(name="ReusableResource.findByTitle", query="select rr from ReusableResource rr where rr.title=:title")
})
public class ReusableResource extends LogisticsResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8161494840654388942L;

	@ManyToOne
	@JoinColumn(name="REUSABLE_RESOURCE_TYPE_ID", nullable=false)
	private ReusableResourceType reusableResourceType;
	
	@Column(name="QUANTITY")
	private BigDecimal quantity;
	
	@OneToMany(mappedBy="container")
	private Set<ConsumableResource> consumables;
	
	@ManyToOne
	@JoinColumn(name="PARENT_ID")
	private ReusableResource parent;
	
	@OneToMany(mappedBy="parent")
	private Set<ReusableResource> children;
	
	@ManyToOne
	@JoinColumn(name="ACTION_PART_ID")
	private ActionPart actionPart;

	public ReusableResourceType getReusableResourceType() {
		return reusableResourceType;
	}

	public void setReusableResourceType(ReusableResourceType reusableResourceType) {
		this.reusableResourceType = reusableResourceType;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Set<ConsumableResource> getConsumables() {
		return consumables;
	}

	public void setConsumables(Set<ConsumableResource> consumables) {
		this.consumables = consumables;
	}

	public ReusableResource getParent() {
		return parent;
	}

	public void setParent(ReusableResource parent) {
		this.parent = parent;
	}

	public Set<ReusableResource> getChildren() {
		return children;
	}

	public void setChildren(Set<ReusableResource> children) {
		this.children = children;
	}

	public ActionPart getActionPart() {
		return actionPart;
	}

	public void setActionPart(ActionPart actionPart) {
		this.actionPart = actionPart;
	}

}
