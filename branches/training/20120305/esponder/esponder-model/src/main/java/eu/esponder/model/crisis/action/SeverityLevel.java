package eu.esponder.model.crisis.action;

public enum SeverityLevel {
	MINIMAL,
	MEDIUM,
	SERIOUS
}
