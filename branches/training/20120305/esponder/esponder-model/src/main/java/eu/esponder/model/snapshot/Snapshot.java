package eu.esponder.model.snapshot;

import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;

import eu.esponder.model.ESponderEntity;

@MappedSuperclass
public abstract class Snapshot<T> extends ESponderEntity<T> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6928365958293895105L;
	
	@Embedded
	protected Period period;

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

}
