package eu.esponder.model.snapshot.resource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.snapshot.Snapshot;
import eu.esponder.model.snapshot.status.EquipmentSnapshotStatus;

@Entity
@Table(name="equipment_snapshot")
@NamedQueries({
	@NamedQuery(
		name="EquipmentSnapshot.findByEquipmentAndDate",
		query="SELECT s FROM EquipmentSnapshot s WHERE s.equipment.id = :equipmentID AND s.period.dateTo <= :maxDate AND s.period.dateTo = " +
				"(SELECT max(s.period.dateTo) FROM EquipmentSnapshot s WHERE s.equipment.id = :equipmentID)")
})
public class EquipmentSnapshot extends Snapshot<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7464811282991787536L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="EQUIPMENT_SNAPSHOT_ID")
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="EQUIPMENT_SNAPSHOT_STATUS", nullable=false)
	private EquipmentSnapshotStatus status;
	
	@ManyToOne
	@JoinColumn(name="EQUIPMENT_ID", nullable=false)
	private Equipment equipment;
	
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private EquipmentSnapshot previous;
	
	@OneToOne(mappedBy="previous")
	private EquipmentSnapshot next;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipmentSnapshotStatus getStatus() {
		return status;
	}

	public void setStatus(EquipmentSnapshotStatus status) {
		this.status = status;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public EquipmentSnapshot getPrevious() {
		return previous;
	}

	public void setPrevious(EquipmentSnapshot previous) {
		this.previous = previous;
	}

	public EquipmentSnapshot getNext() {
		return next;
	}

	public void setNext(EquipmentSnapshot next) {
		this.next = next;
	}

}
