package eu.esponder.model.crisis.action;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.snapshot.action.ActionSnapshot;
import eu.esponder.model.type.ActionType;

@Entity
@Table(name="action")
@NamedQueries({
	@NamedQuery(name="Action.findByTitle", query="select a from Action a where a.title=:title")
})
public class Action extends ESponderEntity<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7256777022590609183L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_ID")
	private Long id;
	
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	@ManyToOne
	@JoinColumn(name="ACTION_TYPE_ID", nullable=false)
	private ActionType actionType;
	
	@OneToMany(mappedBy="action")
	private Set<ActionPart> actionParts;
	
	@OneToMany(mappedBy="action")
	private Set<ActionSnapshot> snapshots;
	
	@ManyToOne
	@JoinColumn(name="CRISIS_CONTEXT_ID")
	private CrisisContext crisisContext;
	
	@ManyToOne
	@JoinColumn(name="PARENT_ID")
	private Action parent;
	
	@OneToMany(mappedBy="parent")
	private Set<Action> children;
	
	@OneToMany(mappedBy="action")
	private Set<ActionObjective> actionObjectives;
	
	@Enumerated
	@Column(name="SEVERITY_LEVEL")
	private SeverityLevel severityLevel;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ActionType getActionType() {
		return actionType;
	}

	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	public Set<ActionPart> getActionParts() {
		return actionParts;
	}

	public void setActionParts(Set<ActionPart> actionParts) {
		this.actionParts = actionParts;
	}

	public Set<ActionSnapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<ActionSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	public CrisisContext getCrisisContext() {
		return crisisContext;
	}

	public void setCrisisContext(CrisisContext crisisContext) {
		this.crisisContext = crisisContext;
	}

	public Action getParent() {
		return parent;
	}

	public void setParent(Action parent) {
		this.parent = parent;
	}

	public Set<Action> getChildren() {
		return children;
	}

	public void setChildren(Set<Action> children) {
		this.children = children;
	}

	public Set<ActionObjective> getActionObjectives() {
		return actionObjectives;
	}

	public void setActionObjectives(Set<ActionObjective> actionObjectives) {
		this.actionObjectives = actionObjectives;
	}

	public SeverityLevel getSeverityLevel() {
		return severityLevel;
	}

	public void setSeverityLevel(SeverityLevel severityLevel) {
		this.severityLevel = severityLevel;
	}
	
	
	
}
