package eu.esponder.jaxb.model.crisis.resource;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.jaxb.model.snapshot.resource.EquipmentSnapshotDTO;

@XmlRootElement(name="equipment")
@XmlType(name="Equipment")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "sensors", "snapshot"})
public class EquipmentDTO extends ResourceDTO {
	
	private Set<SensorDTO> sensors;
	
	private  EquipmentSnapshotDTO snapshot;

	public Set<SensorDTO> getSensors() {
		return sensors;
	}

	public void setSensors(Set<SensorDTO> sensors) {
		this.sensors = sensors;
	}

	public EquipmentSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(EquipmentSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	@Override
	public String toString() {
		return "EquipmentDTO [type=" + type + ", status=" + status + ", id="
				+ id + ", title=" + title + "]";
	}
	
}
