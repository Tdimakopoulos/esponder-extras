package eu.esponder.jaxb.model.snapshot.resource;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import eu.esponder.jaxb.model.snapshot.SnapshotDTO;
import eu.esponder.jaxb.model.snapshot.status.SensorSnapshotStatusDTO;

@XmlRootElement(name="sensorSnapshot")
@XmlType(name="SensorSnapshot")
@JsonPropertyOrder({"status", "meanValue", "standardDeviation", "period"})
public class SensorSnapshotDTO extends SnapshotDTO {

	private SensorSnapshotStatusDTO status;
	
	private String meanValue;
	
	private String standardDeviation;

	public SensorSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(SensorSnapshotStatusDTO status) {
		this.status = status;
	}

	public String getMeanValue() {
		return meanValue;
	}

	public void setMeanValue(String meanValue) {
		this.meanValue = meanValue;
	}

	public String getStandardDeviation() {
		return standardDeviation;
	}

	public void setStandardDeviation(String standardDeviation) {
		this.standardDeviation = standardDeviation;
	}

	@Override
	public String toString() {
		return "SensorSnapshotDTO [status=" + status + ", meanValue="
				+ meanValue + ", standardDeviation=" + standardDeviation
				+ ", period=" + period + ", id=" + id + "]";
	}
	
}
