package eu.esponder.jaxb.model.snapshot.action;

import eu.esponder.jaxb.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.status.ActionSnapshotStatusDTO;


public class ActionSnapshotDTO extends SpatialSnapshotDTO {
	
	private ActionSnapshotStatusDTO status;
	
	public ActionSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActionSnapshotStatusDTO status) {
		this.status = status;
	}

}
