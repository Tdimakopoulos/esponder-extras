package eu.esponder.event.model.crisis.resource.type;

import eu.esponder.event.model.CreateEvent;


public class CreateConsumableResourceTypeEvent extends ConsumableResourceTypeEvent implements CreateEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5720793245670829465L;
	
}
