package eu.esponder.rest.crisis.test;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.jaxb.model.snapshot.location.PointDTO;
import eu.esponder.rest.ESponderResource;


@Path("/crisis/contextTest")
public class TestResource extends ESponderResource {
	
	@GET
	@Path("/test")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public PointDTO getRestTest(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		PointDTO point = new PointDTO();
		
		point.setAltitude(new BigDecimal(1));
		point.setLongitude(new BigDecimal(2));
		point.setLatitude(new BigDecimal(3));
		
		return point; 
	}
	
}
