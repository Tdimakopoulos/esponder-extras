package eu.esponder.rest.crisis;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.jaxb.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.rest.ESponderResource;


@Path("/crisis/context")
public class CrisisContextResource extends ESponderResource {
	
	@GET
	@Path("/oc")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public List<OperationsCentreDTO> getUserOperationsCentres(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		return this.getMappingService().mapUserOperationsCentres(userID); 
	}
	
	
}
