package eu.esponder.test.rest.client;
import java.math.BigDecimal;

import javax.xml.bind.JAXBException;

import org.testng.annotations.Test;

import eu.esponder.jaxb.model.snapshot.location.PointDTO;
import eu.esponder.rest.client.RestClient;
import eu.esponder.util.jaxb.Parser;


public class RestClientTest {
	
	@Test
	public void testRestClient() throws JAXBException {
		String resourceURI = "http://localhost:8080/esponder-restful/crisis/contextTest/test?userID=1";
		RestClient restClient = new RestClient(resourceURI);
		
		String xmlResponse = restClient.getXML();
		
        
        Parser parser = new Parser(new Class[] {PointDTO.class});
        PointDTO pointDTO = (PointDTO) parser.unmarshal(xmlResponse);
       
        PointDTO pointDTOCheck = new PointDTO();
        pointDTOCheck.setAltitude(new BigDecimal(1));
        pointDTOCheck.setLongitude(new BigDecimal(2));
        pointDTOCheck.setLatitude(new BigDecimal(3));
        
        System.out.println(pointDTO.toString());
        
        assert pointDTOCheck.equals(pointDTO);
		
		return;
	}

}
