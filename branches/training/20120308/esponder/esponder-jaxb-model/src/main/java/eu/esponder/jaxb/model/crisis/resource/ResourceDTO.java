package eu.esponder.jaxb.model.crisis.resource;

import javax.xml.bind.annotation.XmlSeeAlso;

import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.crisis.resource.sensor.SensorDTO;

@XmlSeeAlso({
	ActorDTO.class,
	EquipmentDTO.class,
	LogisticsResourceDTO.class,
	OperationsCentreDTO.class,
	SensorDTO.class
})
public abstract class ResourceDTO extends ESponderEntityDTO {

	protected String type;
	
	protected String title;
	
	protected ResourceStatusDTO status;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ResourceStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ResourceStatusDTO status) {
		this.status = status;
	}

	public String getResourceId() {
		return id+":"+this.type;
	}

	@Override
	public String toString() {
		return "ResourceDTO [status=" + status + ", id=" + id + ", title="
				+ title + "]";
	}

}
