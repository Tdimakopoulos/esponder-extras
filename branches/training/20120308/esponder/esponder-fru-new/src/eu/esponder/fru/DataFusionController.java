package eu.esponder.fru;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import eu.esponder.fru.data.DataHandler;
import eu.esponder.fru.data.DataSender;
import eu.esponder.fru.thread.DataFusionProcessorThread;
import eu.esponder.jaxb.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;

public class DataFusionController {

	public static Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Long>> configMap;

	public static List<SensorMeasurementEnvelopeDTO> sensorMeasurementsEnvelopeList;

	public static Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>> sensorMeasurementMaps;

	public static List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticsList;

	public static List<Thread> processorThreads = new ArrayList<Thread>();

	public DataFusionController() {
		loadConfig();
	}
	
	public static void loadConfig() {

		configMap = ConfigReader
				.readMeasurementStatisticsConfig(FruConstants.FRU_CONFIGURATION_FILE);

	}

	public static void init() {

		sensorMeasurementMaps = new HashMap<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>>();
		sensorMeasurementsEnvelopeList = new ArrayList<SensorMeasurementEnvelopeDTO>();
		sensorMeasurementStatisticsList = new ArrayList<SensorMeasurementStatisticDTO>();

		for (Class<? extends SensorDTO> sensorClass : configMap.keySet()) {

			Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>> sensorMeasurementsStatisticsMap = 
					new HashMap<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>();
			sensorMeasurementMaps.put(sensorClass,
					sensorMeasurementsStatisticsMap);

			Map<MeasurementStatisticTypeEnum, Long> configProcessingMap = configMap.get(sensorClass);
			for (MeasurementStatisticTypeEnum processingType : configProcessingMap.keySet()) {
				sensorMeasurementsStatisticsMap.put(processingType,
						new LinkedList<SensorMeasurementDTO>());

				Queue<SensorMeasurementDTO> measurementsQueue = new LinkedList<SensorMeasurementDTO>();
				Thread statisticProcessingThread = new DataFusionProcessorThread(
						sensorClass, processingType,
						((Long)configProcessingMap.get(processingType)),
						measurementsQueue, sensorMeasurementStatisticsList);
				statisticProcessingThread.start();
				processorThreads.add(statisticProcessingThread);
			}
		}
		
		Thread dataSenderThread = new DataSender();
		dataSenderThread.start();

		/**
		 * FIXME: What's the purpose of sleeping for 1ms??? Is it necessary?
		 * 
		 */
		// try {
		// Thread.sleep(1);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		Thread dataHandlerThread = new DataHandler(sensorMeasurementsEnvelopeList, sensorMeasurementMaps);
		dataHandlerThread.start();
		

		/**
		 * FIXME: What's the purpose of sleeping for 2sec??? Is it necessary?
		 * 
		 */
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
