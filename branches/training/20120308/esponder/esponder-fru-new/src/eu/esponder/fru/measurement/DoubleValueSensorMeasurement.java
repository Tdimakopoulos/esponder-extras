package eu.esponder.fru.measurement;

import java.math.BigDecimal;

public class DoubleValueSensorMeasurement extends SensorMeasurement {

	private BigDecimal value;
	
	public DoubleValueSensorMeasurement() {}
	
	public DoubleValueSensorMeasurement(int value) {
		this.value = new BigDecimal(value);
	}
	
	public DoubleValueSensorMeasurement(BigDecimal value) {
		this.value = value;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
	
}
