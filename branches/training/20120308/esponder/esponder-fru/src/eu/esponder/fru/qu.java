package eu.esponder.fru;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class qu {
	 public static void main(String args[]) {
	 Queue<Double> qe=new LinkedList<Double>();

     qe.add(1.1); 
     qe.add(3.5); 
     qe.add(7.8); 
     qe.add(4.4);  
     qe.add(0.1); 
     qe.add(4.8);  
     qe.clear();
     Iterator it=qe.iterator();
    

      System.out.println("Initial Size of Queue :"+qe.size());

     while(it.hasNext())
     {
         double iteratorValue=(Double)it.next();
         System.out.println("Queue Next Value :"+iteratorValue);
     }

     System.out.println("AVG :"+getMinValue(qe));
     
     // get value and does not remove element from queue
  //   System.out.println("Queue peek :"+qe.peek());

     // get first value and remove that object from queue
   //  System.out.println("Queue poll :"+qe.poll());

   //  System.out.println("Final Size of Queue :"+qe.size());
     
     
 }
		public static double average(Queue seq){
			// calculate sum
			double sum = 0;
		    Iterator it=seq.iterator();
	     while(it.hasNext())
		     {
		    	 sum = sum + (Double)it.next();
		     }
		    // calculate average
		    double average = (sum *10)/ seq.size();
		    return average/10;
		    
			}
		
		public static double getMaxValue(Queue seq){  
			  double maxValue = (Double) seq.peek();  
			  double current;
			   Iterator it=seq.iterator();
			   
			   while(it.hasNext())
			     {current= (Double)it.next();
			     if( current > maxValue){  
				      maxValue = current;  
				    }  
			     
			     }
			   
	 		  return maxValue;  
			} 
		
		
		public static double getMinValue(Queue seq){  
			  double minValue =  (Double) seq.peek();  
			  double current;
			  Iterator it=seq.iterator();
			   
			  while(it.hasNext())
			     {current= (Double)it.next();
			     if( current < minValue){  
			    	 minValue = current;  
				    }  
			     
			     }
			  
			  return minValue;  
			}  
		
		

}
