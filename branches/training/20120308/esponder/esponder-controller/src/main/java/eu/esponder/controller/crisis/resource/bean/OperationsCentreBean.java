package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.crisis.view.SketchPOI;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;

@Stateless
@SuppressWarnings("unchecked")
public class OperationsCentreBean implements OperationsCentreService, OperationsCentreRemoteService {
	
	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	//-------------------------------------------------------------------------
	
	@Override
	public OperationsCentre findById(Long operationsCentreID) {
		return (OperationsCentre) crudService.find(OperationsCentre.class, operationsCentreID);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public OperationsCentre findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (OperationsCentre) crudService.findSingleWithNamedQuery("OperationsCentre.findByTitle", params);
	}
	
	//-------------------------------------------------------------------------
	
	public OperationsCentre findByUser(Long operationsCentreID, Long userID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		params.put("userID", userID);
		return (OperationsCentre) crudService.findSingleWithNamedQuery("OperationsCentre.findByIdAndUser", params);
	}

	//-------------------------------------------------------------------------
	
	@Override
	public OperationsCentreSnapshot findSnapshotByDate(Long operationsCentreID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		params.put("maxDate", maxDate);
		OperationsCentreSnapshot snapshot =
				(OperationsCentreSnapshot) crudService.findSingleWithNamedQuery("OperationsCentreSnapshot.findByOperationsCentreAndDate", params);
		return snapshot;
	}
	
	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OperationsCentre create(OperationsCentre operationsCentre, Long userID) {
		crudService.create(operationsCentre);
		return operationsCentre;
	}
	
	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OperationsCentreSnapshot createSnapshot(OperationsCentre operationsCentre, OperationsCentreSnapshot snapshot, Long userID) {
		snapshot.setOperationsCentre(operationsCentre);
		crudService.create(snapshot);
		return snapshot;
	}

	//-------------------------------------------------------------------------
	
	@Override
	public List<SketchPOI> findSketchPOIs(Long operationsCentreID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		return crudService.findWithNamedQuery("SketchPOI.findByOperationsCentre", params);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public SketchPOI createSketch(OperationsCentre operationsCentre, SketchPOI sketch, Long userID) {
		sketch.setOperationsCentre(operationsCentre);
		crudService.create(sketch);
		return sketch;
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public List<ReferencePOI> findReferencePOIs(Long operationsCentreID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		return crudService.findWithNamedQuery("ReferencePOI.findByOperationsCentre", params);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public ReferencePOI createReferencePOI(OperationsCentre operationsCentre, ReferencePOI referencePOI, Long userID) {
		referencePOI.setOperationsCentre(operationsCentre);
		crudService.create(referencePOI);
		return referencePOI;
	}
	
}
