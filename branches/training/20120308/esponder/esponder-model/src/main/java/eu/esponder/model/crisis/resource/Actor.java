package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.collections.CollectionUtils;

import eu.esponder.model.snapshot.resource.ActorSnapshot;
import eu.esponder.model.type.ActorType;

@Entity
@Table(name="actor")
@NamedQueries({
	@NamedQuery(name="Actor.findByTitle", query="select a from Actor a where a.title=:title"),
	@NamedQuery(
		name="Actor.findActorsWithSubordinates",
		query="SELECT s FROM Actor s WHERE s.operationsCentre.id = :operationsCentreID AND " +
				"(s.subordinates IS NOT NULL OR (s.subordinates IS NULL and s.supervisor IS NULL))")
})
public class Actor extends Resource {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2224027092652397963L;

	@ManyToOne
	@JoinColumn(name="ACTOR_TYPE_ID", nullable=false)
	private ActorType actorType;
	
	@ManyToOne
	@JoinColumn(name="SUPERVISOR_ID")
	private Actor supervisor;
	
	@OneToMany(mappedBy="supervisor")
	private Set<Actor> subordinates;
	
	@ManyToOne
	@JoinColumn(name="OPERATIONS_CENTRE_ID")
	private OperationsCentre operationsCentre;
	
	@OneToMany(mappedBy="actor")
	private Set<Equipment> equipmentSet;
	
	@OneToMany(mappedBy="actor")
	private Set<ActorSnapshot> snapshots;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ActorType getActorType() {
		return actorType;
	}

	public void setActorType(ActorType actorType) {
		this.actorType = actorType;
	}

	public Actor getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Actor supervisor) {
		this.supervisor = supervisor;
	}

	public Set<Actor> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(Set<Actor> subordinates) {
		this.subordinates = subordinates;
	}

	public OperationsCentre getOperationsCentre() {
		return operationsCentre;
	}

	public void setOperationsCentre(OperationsCentre operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	public Set<Equipment> getEquipmentSet() {
		return equipmentSet;
	}

	public void setEquipmentSet(Set<Equipment> equipmentSet) {
		this.equipmentSet = equipmentSet;
	}

	public Set<ActorSnapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<ActorSnapshot> snapshots) {
		this.snapshots = snapshots;
	}
	
	public void traverseSubordinates() {
		if (CollectionUtils.isNotEmpty(this.subordinates)) {
			for (Actor actor : this.subordinates) {
				actor.traverseSubordinates();
			}
		}
	}

}
