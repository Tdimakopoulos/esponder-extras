package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("CONSUMABLE")
public final class ConsumableResourceType extends LogisticsResourceType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -684790725225587637L;
	
	
}
