package eu.esponder.model.snapshot.resource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.snapshot.SpatialSnapshot;
import eu.esponder.model.snapshot.status.OperationsCentreSnapshotStatus;

@Entity
@Table(name="operations_centre_snapshot")
@NamedQueries({
	@NamedQuery(
		name="OperationsCentreSnapshot.findByOperationsCentreAndDate",
		query="SELECT s FROM OperationsCentreSnapshot s WHERE s.operationsCentre.id = :operationsCentreID AND s.period.dateTo <= :maxDate AND s.period.dateTo = " +
				"(SELECT max(s.period.dateTo) FROM OperationsCentreSnapshot s WHERE s.operationsCentre.id = :operationsCentreID)")
})
public class OperationsCentreSnapshot extends SpatialSnapshot<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9169930917504748793L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="OPERATIONS_CENTRE_SNAPSHOT_ID")
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="OPERATIONS_CENTRE_SNAPSHOT_STATUS", nullable=false)
	private OperationsCentreSnapshotStatus status;
	
	@ManyToOne
	@JoinColumn(name="OPERATIONS_CENTRE_ID", nullable=false)
	private OperationsCentre operationsCentre;
	
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private OperationsCentreSnapshot previous;
	
	@OneToOne(mappedBy="previous")
	private OperationsCentreSnapshot next;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OperationsCentreSnapshotStatus getStatus() {
		return status;
	}

	public void setStatus(OperationsCentreSnapshotStatus status) {
		this.status = status;
	}

	public OperationsCentre getOperationsCentre() {
		return operationsCentre;
	}

	public void setOperationsCentre(OperationsCentre operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	public OperationsCentreSnapshot getPrevious() {
		return previous;
	}

	public void setPrevious(OperationsCentreSnapshot previous) {
		this.previous = previous;
	}

	public OperationsCentreSnapshot getNext() {
		return next;
	}

	public void setNext(OperationsCentreSnapshot next) {
		this.next = next;
	}

}
