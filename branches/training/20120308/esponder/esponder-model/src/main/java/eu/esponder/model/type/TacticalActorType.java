package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("TCL_ACTOR")
public final class TacticalActorType extends ActorType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5463150247445092220L;
	
	
}
