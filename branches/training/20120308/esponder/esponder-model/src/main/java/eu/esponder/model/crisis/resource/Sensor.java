package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.type.SensorType;

@Entity
@Table(name="sensor")
@NamedQueries({
	@NamedQuery(name="Sensor.findByTitle", query="select s from Sensor s where s.title=:title")
})
public class Sensor extends Resource {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5390561449137637770L;

	@ManyToOne
	@JoinColumn(name="SENSOR_TYPE_ID", nullable=false)
	private SensorType sensorType;
	
	@ManyToOne
	@JoinColumn(name="EQUIPMENT_ID")
	private Equipment equipment;
	
	@Column(name="MEASUREMENT_UNIT")
	private String measurementUnit;
	
	@OneToMany(mappedBy="sensor")
	private Set<SensorSnapshot> snapshots;

	public SensorType getSensorType() {
		return sensorType;
	}

	public void setSensorType(SensorType sensorType) {
		this.sensorType = sensorType;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public String getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	public Set<SensorSnapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<SensorSnapshot> snapshots) {
		this.snapshots = snapshots;
	}
	
}
