package eu.esponder.model.snapshot.status;

public enum OperationsCentreSnapshotStatus {
	STATIONARY,
	MOVING
}
