package org.esponder.mprm.snippets;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;

public class TestEventData0 {

  public static ESponderEvent getTestData() {
    ActorDTO subactorDTO = new ActorDTO();
    subactorDTO.setId(new Long(1));
    subactorDTO.setType("FRC 9999");
    subactorDTO.setTitle("FRC #9999");
    subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);

    ArithmeticSensorMeasurementDTO measurmentDTO = new ArithmeticSensorMeasurementDTO();
    measurmentDTO.setId(99L);
    measurmentDTO.setMeasurement(new BigDecimal(1111));
    measurmentDTO.setTimestamp(new Date());
    
    List<SensorMeasurementDTO> measurmentsList = new ArrayList<SensorMeasurementDTO>();
    measurmentsList.add(measurmentDTO);

    SensorMeasurementEnvelopeDTO envelopeDTO = new SensorMeasurementEnvelopeDTO();
    envelopeDTO.setMeasurements(measurmentsList);
    envelopeDTO.setId(System.currentTimeMillis());

    CreateSensorMeasurementEnvelopeEvent event = new CreateSensorMeasurementEnvelopeEvent();
    event.setEventSeverity(SeverityLevelDTO.MEDIUM);
    event.setEventAttachment(envelopeDTO);
    event.setEventSource(subactorDTO);
    event.setEventTimestamp(new Date());

    return event;
  }

}
