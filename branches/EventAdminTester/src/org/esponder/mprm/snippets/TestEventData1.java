package org.esponder.mprm.snippets;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;

public class TestEventData1 {

  public static ESponderEvent getTestData() {
    BodyTemperatureSensorDTO bodyTemperatureSensor = new BodyTemperatureSensorDTO();
    bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
    bodyTemperatureSensor.setName("Body Temperature");
    bodyTemperatureSensor.setType("BodyTemp");
    bodyTemperatureSensor.setLabel("BdTemp");
    bodyTemperatureSensor.setTitle("Body Temperature Sensor");
    bodyTemperatureSensor.setId(System.currentTimeMillis());
    bodyTemperatureSensor.setStatus(ResourceStatusDTO.AVAILABLE);
    bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);

    ArithmeticSensorMeasurementDTO sensorMeasurement1 = new ArithmeticSensorMeasurementDTO();
    sensorMeasurement1.setMeasurement(new BigDecimal(200));
    sensorMeasurement1.setId(99L);
    sensorMeasurement1.setTimestamp(new Date());
    sensorMeasurement1.setSensor(bodyTemperatureSensor);

    SensorMeasurementStatisticDTO statisticArethmetic = new SensorMeasurementStatisticDTO();
    statisticArethmetic.setStatistic(sensorMeasurement1);
    statisticArethmetic.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
    sensorMeasurement1.setId(89L);

    List<SensorMeasurementStatisticDTO> sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
    sensorMeasurementStatistic.add(statisticArethmetic);

    SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
    sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
    sensorMeasurementStatisticEnvelope.setId(System.currentTimeMillis());

    ActorDTO subactorDTO = new ActorDTO();
    subactorDTO.setId(new Long(2));
    subactorDTO.setType("FRC 2222");
    subactorDTO.setTitle("FRC #2222");
    subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);

    ESponderEvent<SensorMeasurementStatisticEnvelopeDTO> statisticEvent2 = new CreateSensorMeasurementStatisticEvent();
    statisticEvent2.setEventSeverity(SeverityLevelDTO.SERIOUS);
    statisticEvent2.setEventTimestamp(new Date());
    statisticEvent2.setEventAttachment(sensorMeasurementStatisticEnvelope);
    statisticEvent2.setEventSource(subactorDTO);
    
    return statisticEvent2;
  }

}
