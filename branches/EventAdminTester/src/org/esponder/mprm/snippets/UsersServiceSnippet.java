package org.esponder.mprm.snippets;

import java.util.Dictionary;
import java.util.Hashtable;

import com.prosyst.mprm.admin.useradmin.AGroup;
import com.prosyst.mprm.admin.useradmin.ARole;
import com.prosyst.mprm.admin.useradmin.AUser;
import com.prosyst.mprm.admin.useradmin.AUserAdmin;

import com.prosyst.mprm.common.ManagementException;
import com.prosyst.mprm.rac.RemoteAccessClient;

/**
 * Snippet showing user management service usage for authentication and authorization.
 *
 * @author Kaloyan Radev, e-mail: k.radev@prosyst.bg
 * @version 1.0
 */
public class UsersServiceSnippet {

  private final AUserAdmin userAdmin;

  public UsersServiceSnippet(RemoteAccessClient rac) throws ManagementException {
    // get User Service, it will be used for some AAA play
    this.userAdmin = (AUserAdmin)rac.getService(AUserAdmin.class.getName());
    System.out.println("User service: " + userAdmin);
  }

  public void showAARolesUsage() {
    // create test user
    String testUserName = "Test-User-FRU" + System.currentTimeMillis();
    String testPassword = "test";
    AUser user = (AUser)userAdmin.createRole(testUserName, ARole.USER);

    // setup the user credentials
    try {
      Dictionary<String, String> credentials = user.getCredentials();
      if (credentials == null) {
        credentials = new Hashtable<String, String>();
      }
      credentials.put(AUserAdmin.PASSWORD, testPassword);
      userAdmin.setCredentials(user.getName(), credentials);
    } catch (Exception e) {
      e.printStackTrace();
    }

    // try to login with wrong pass
    makeLoginAttempt(testUserName, "123");

    // try to login with correct pass
    makeLoginAttempt(testUserName, testPassword);

    // create test permissions group
    String testGroupName = "Test-Perm-Group-MEOC" + System.currentTimeMillis();
    AGroup group = (AGroup)userAdmin.createRole(testGroupName, ARole.GROUP);

  /** Even logged the user will actually not be allowed almost nothing in the system!
      The role will need to be applied a set of permissions to be able to do this. */

    // check if the user has administrative permissions, result is no
    makePermissionCheck(user.getName(), AUserAdmin.ADMINISTRATION);

    // if user has test group permissions, result is no
    makePermissionCheck(user.getName(), group.getName());

    // now add the user to the group
    addUserToGroup(user.getName(), group.getName());

    // and check again if user has test group permissions, result is now yes
    makePermissionCheck(user.getName(), group.getName());

    // clean up before we go
    userAdmin.removeRole(user.getName());
    userAdmin.removeRole(group.getName());
  }

  /**
   * This method will try to connect to the system with the supplied credentials.
   *
   * @param userName
   * @param password
   */
  private void makeLoginAttempt(String userName, String password) {
    // prepare connection credentials
    Hashtable credentials = new Hashtable();
    credentials.put(RemoteAccessClient.USER_PASSWORD, password);

    try {
      // try to connect to the E-Sponder mPRM
      System.out.println("Login with user/pass: " + userName + "/" + password);
      RemoteAccessClient.connect(ConnectionEstablishment.URI, userName, credentials, null);
      System.out.println("Login successful!");
    } catch (Exception e) {
      System.out.println("Login failed : " + e.getMessage());
    }
  }

  /**
   * This method perform some authorisation check on the supplied roles.
   *
   * @param user
   * @param permissionGroupName
   */
  private void makePermissionCheck(String userName, String groupName) {
    // check permissions for the administratiion group group
    System.out.println("Check authorization of user " + userName + " for " + groupName);

    // simple service check usage example
    boolean hasPermissions = userAdmin.hasRole(userName, groupName);
    System.out.println("Check authorization: " + (hasPermissions ? "Allow!" : "Denied"));
  }

  /**
   * Helper membership control method.
   */
  private void addUserToGroup(String userName, String groupName) {
    try {
      userAdmin.setBasicMembers(groupName, new String[] {userName});
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
