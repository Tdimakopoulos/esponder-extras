package org.esponder.mprm.snippets;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import com.prosyst.mprm.backend.event.Event;
import com.prosyst.mprm.backend.event.EventListener;
import com.prosyst.mprm.backend.event.EventListenerException;
import com.prosyst.mprm.data.DictionaryInfo;

import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;

/**
 * Simple event receiving class.
 *
 * @author Kaloyan Radev, e-mail: k.radev@prosyst.bg
 * @version 1.0
 */
public class EventServiceSubscription implements EventListener {

	static final String TOPIC = "eu/esponder/event/model/meoc/test/CreateSensorMeasurementStatisticEvent";
	  
  static final String PROPERTY_EVENT = "event";

  private final ObjectMapper mapper;

  public EventServiceSubscription(ObjectMapper mapper) {
    this.mapper = mapper;
  }

  // define Event Listener implementation
  public void event(Event event) throws EventListenerException {
    try {
      System.out.println("Event: " + event.getEventData());

      // get the event data
      DictionaryInfo data = (DictionaryInfo)event.getEventData();
      System.out.println("Data # " + data);

      String esponderEventData = (String)data.get(PROPERTY_EVENT);
      System.out.println("Esponder Data # " + data);

      // parse the event data
      JsonFactory jsonFactory = new JsonFactory();
      JsonParser jp = jsonFactory.createJsonParser(esponderEventData);
      ESponderEvent esponderEvent = mapper.readValue(jp, CreateSensorMeasurementStatisticEvent.class);

      // and just print it
      System.out.println("###########################################");
      System.out.println("Event # " + esponderEvent);
      System.out.println("Event attachment # " + esponderEvent.getEventAttachment());
      System.out.println("Event severity # " + esponderEvent.getEventSeverity());
      System.out.println("Event source # " + esponderEvent.getEventSource());
      System.out.println("Event timestamp# " + esponderEvent.getEventTimestamp());

    } catch (Throwable e) {
      e.printStackTrace();
    }
  }


}


