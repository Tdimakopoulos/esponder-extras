package org.esponder.mprm.snippets;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

import com.prosyst.mprm.backend.event.EventService;
import com.prosyst.mprm.common.ManagementException;
import com.prosyst.mprm.data.DictionaryInfo;
import com.prosyst.mprm.rac.RemoteAccessClient;

import eu.esponder.event.model.ESponderEvent;

/**
 * Snippet showing distributed event service data sending and receiving.
 *
 * @author Kaloyan Radev, e-mail: k.radev@prosyst.bg
 * @version 1.0
 */
public class EventServiceSnippet {

  private final RemoteAccessClient rac;

  private final EventService eventService;

  private EventServiceSubscription subscriber;

  private ObjectMapper mapper;

  public EventServiceSnippet(RemoteAccessClient rac) throws ManagementException {
    this.rac = rac;

    // get Event Service, it can be used only to publish new events
    this.eventService = (EventService)rac.getService(EventService.class.getName());
    System.out.println("Event service: " + eventService);
    
    // create jackson deserialization mapper
    mapper = new ObjectMapper();
    mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
    mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  public void subscribe() {
    try {
      // subscribe for events using the newly created subscriber
      subscriber = new EventServiceSubscription(mapper);
      rac.addEventListener(EventServiceSubscription.TOPIC, subscriber);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void sendSampleEvent() {
    try {
      // create sample event, it will be received from other connected clients
      DictionaryInfo data = new DictionaryInfo();
      ESponderEvent event = TestEventData1.getTestData();
      data.put(EventServiceSubscription.PROPERTY_EVENT, mapper.writeValueAsString(event));
      
      // send the event
      eventService.event(EventServiceSubscription.TOPIC, data);
      System.out.println("Event Sent: " + data);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void dispose() {
    try {
      // unsubscribe for events
      if (rac != null && subscriber != null) {
        rac.removeEventListener(EventServiceSubscription.TOPIC, subscriber);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}


