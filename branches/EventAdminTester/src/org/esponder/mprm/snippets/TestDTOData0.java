package org.esponder.mprm.snippets;

import java.math.BigDecimal;
import java.util.Date;

import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;

public class TestDTOData0 {

  public static SensorMeasurementStatisticDTO getData() {
    LocationSensorDTO sensorDTO = new LocationSensorDTO();
    sensorDTO.setName("LPS");
    sensorDTO.setType("LPS");
    sensorDTO.setTitle("Local Positioning Sensor");
    sensorDTO.setId(System.currentTimeMillis());
    sensorDTO.setStatus(ResourceStatusDTO.AVAILABLE);

    ArithmeticSensorMeasurementDTO  sensorMeasurement = new ArithmeticSensorMeasurementDTO();
    sensorMeasurement.setMeasurement(new BigDecimal(810));
    sensorMeasurement.setTimestamp(new Date());

    SensorMeasurementStatisticDTO statistic  = new SensorMeasurementStatisticDTO();
    statistic.setStatistic(sensorMeasurement);
    statistic.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);

    // from Panos

    BodyTemperatureSensorDTO bodyTemperatureSensor = new BodyTemperatureSensorDTO();
    bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
    bodyTemperatureSensor.setName("Sensor_Temperature");
    bodyTemperatureSensor.setType("Sensor_Temperature");
    bodyTemperatureSensor.setTitle("Body Temperature Sensor");
    bodyTemperatureSensor.setId(System.currentTimeMillis());
    bodyTemperatureSensor.setStatus(ResourceStatusDTO.AVAILABLE);


    ArithmeticSensorMeasurementDTO sensorMeasurement1 = new ArithmeticSensorMeasurementDTO();
    sensorMeasurement1.setMeasurement(new BigDecimal(810));
    sensorMeasurement1.setTimestamp(new Date());
    sensorMeasurement1.setSensor(bodyTemperatureSensor);
    sensorMeasurement1.setId(System.currentTimeMillis());

    SensorMeasurementStatisticDTO statistic1  = new SensorMeasurementStatisticDTO();
    statistic1.setStatistic(sensorMeasurement1);
    statistic1.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
    statistic1.setId(System.currentTimeMillis());
    statistic1.setSamplingPeriod(new Long("5"));
    statistic1.setPeriod(new PeriodDTO());

    return statistic1;
  }
}
