package org.esponder.mprm.snippets;

import java.util.Hashtable;

import org.codehaus.jackson.map.ObjectMapper;

import com.prosyst.mprm.common.ManagementException;
import com.prosyst.mprm.rac.RemoteAccessClient;

import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationSensorDTO;

/**
 * Simple snippet showing connection establishment and administration.
 *
 * @author Kaloyan Radev, e-mail: k.radev@prosyst.bg
 * @version 1.0
 */
public class ConnectionEstablishment {

  // Sofia mPRM external access location
  public static String URI = "socket://212.95.166.50:11449";

  // Sofia mPRM internal access location
  //public static String URI = "socket://172.22.150.201:11449";

  private static RemoteAccessClient rac;
  private static Hashtable credentials;

  private static String userName = "system";
  private static String password = "system";

  private static UsersServiceSnippet usersServiceSnippet;
  private static EventServiceSnippet eventServiceSnippet;

  public static void main(String[] args) {

  	// prepare connection credentials

    credentials = new Hashtable();
    credentials.put(RemoteAccessClient.USER_PASSWORD, password);

    try {

    	// connect to the E-Sponder mPRM
      rac = RemoteAccessClient.connect(URI, userName, credentials, null);

      // make simple user management show case
      usersServiceSnippet = new UsersServiceSnippet(rac);
      usersServiceSnippet.showAARolesUsage();

      // play around the event service
      eventServiceSnippet = new EventServiceSnippet(rac);
      eventServiceSnippet.sendSampleEvent();
      eventServiceSnippet.subscribe();

      try 
      {
        LocationSensorDTO sensorDTO = new LocationSensorDTO();
        sensorDTO.setName("LPS");
        sensorDTO.setType("LPS");
        sensorDTO.setTitle("Local Positioning Sensor");
        sensorDTO.setId(System.currentTimeMillis());
        sensorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
        sensorDTO.setId((long)sensorDTO.getName().hashCode());

        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
        try {
          String json = mapper.writeValueAsString(sensorDTO);
          System.out.println(json);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
      
      // block execution to allow events receiving
      while (true) {
        try {
          Thread.sleep(10000000);
        } catch (InterruptedException e) {
        }
      }

    } catch(ManagementException me) {
      me.printStackTrace();

    } finally {
    	if (rac != null) {
        // closing the session after all the work is done
        rac.close();
    	}
    }
  }
}
