package org.esponder.mprm.snippets;

import java.math.BigDecimal;
import java.util.HashSet;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;

public class TestDTOData1 {

  public static ActionDTO getData() {
    ActionDTO action = new ActionDTO();
    action.setTitle("General No1");
    action.setId(new Long(4));

    ReusableResourceDTO reusableResource1 = new ReusableResourceDTO();
    reusableResource1.setType("truck");
    HashSet reusableResourceSet = new HashSet();
    reusableResourceSet.add(reusableResource1);

    ConsumableResourceDTO ConsumableResource1 = new ConsumableResourceDTO();
    ConsumableResource1.setTitle("food packages");
    ConsumableResource1.setQuantity(new BigDecimal(10));
    HashSet consumableResourceSet = new HashSet();
    consumableResourceSet.add(ConsumableResource1);

    ConsumableResourceDTO ConsumableResource2 = new ConsumableResourceDTO();
    ConsumableResource2.setTitle("telecom lines");
    HashSet consumableResourceSet2 = new HashSet();
    consumableResourceSet2.add(ConsumableResource2);

    ActionPartObjectiveDTO actionPartObjective1 = new ActionPartObjectiveDTO();
    actionPartObjective1.setConsumableResources(consumableResourceSet);
    HashSet actionPartObjectiveSet = new HashSet();
    actionPartObjectiveSet.add(actionPartObjective1);

    ActionPartObjectiveDTO actionPartObjective2 = new ActionPartObjectiveDTO();
    actionPartObjective2.setConsumableResources(consumableResourceSet2);
    HashSet actionPartObjectiveSet2 = new HashSet();
    actionPartObjectiveSet2.add(actionPartObjective2);

    // Action No1
    ActionPartDTO actoinPart1 = new ActionPartDTO();
    actoinPart1.setUsedReusuableResources(reusableResourceSet);
    actoinPart1.setUsedConsumableResources(consumableResourceSet);
    actoinPart1.setActionPartObjectives(actionPartObjectiveSet);
    actoinPart1.setActionOperation(ActionOperationEnumDTO.MOVE);
    ActionPartSnapshotDTO actionsnap1 = new ActionPartSnapshotDTO();
    actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
    actoinPart1.setSnapshot(actionsnap1);
    actoinPart1.setId(new Long(1));
    actoinPart1.setActionId(action.getId());

    // Action No3
    ActionPartDTO actoinPart3 = new ActionPartDTO();
    actoinPart3.setUsedReusuableResources(reusableResourceSet);
    actoinPart3.setUsedConsumableResources(consumableResourceSet);
    actoinPart3.setActionPartObjectives(actionPartObjectiveSet);
    actoinPart3.setActionOperation(ActionOperationEnumDTO.MOVE);
    actionsnap1 = new ActionPartSnapshotDTO();
    actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
    actoinPart3.setSnapshot(actionsnap1);
    actoinPart3.setId(new Long(3));
    actoinPart3.setActionId(action.getId());

    // Action No4
    ActionPartDTO actoinPart5 = new ActionPartDTO();
    actoinPart5.setUsedReusuableResources(reusableResourceSet);
    actoinPart5.setUsedConsumableResources(consumableResourceSet);
    actoinPart5.setActionPartObjectives(actionPartObjectiveSet);
    actoinPart5.setActionOperation(ActionOperationEnumDTO.MOVE);
    actionsnap1 = new ActionPartSnapshotDTO();
    actionsnap1.setStatus(ActionPartSnapshotStatusDTO.STARTED);
    actoinPart5.setSnapshot(actionsnap1);
    actoinPart5.setId(new Long(5));
    actoinPart5.setActionId(action.getId());

    HashSet<ActionPartDTO> actionParts = new HashSet<ActionPartDTO>();
    actionParts.add(actoinPart1);
    actionParts.add(actoinPart3);
    actionParts.add(actoinPart5);
    action.setActionParts(actionParts);

    return action;
  }
}
