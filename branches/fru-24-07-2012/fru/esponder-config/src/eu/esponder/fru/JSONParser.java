package eu.esponder.fru;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import android.util.Log;

import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;

public class JSONParser {

	public ActorDTO getActor() {
		String actor = "";
 		ActorDTO actorDTO = null;
	
		try {
			File myFile = new File("/sdcard/esponder/config.json");
			FileInputStream fIn = new FileInputStream(myFile);
			BufferedReader myReader = new BufferedReader(new InputStreamReader(
					fIn));
			String dataRow = "";
			while ((dataRow = myReader.readLine()) != null) {
				actor += dataRow + "\n";
			}
			Log.d("peirame", "actor = " + actor);
			myReader.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	  
		ObjectMapper mapperarrive = new ObjectMapper();
		mapperarrive
				.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
		JsonFactory jsonFactory = new JsonFactory();

		try {
			JsonParser jp = jsonFactory.createJsonParser(actor);
			// get the event

			actorDTO = mapperarrive.readValue(jp, ActorDTO.class);

		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return actorDTO;
	}
}
