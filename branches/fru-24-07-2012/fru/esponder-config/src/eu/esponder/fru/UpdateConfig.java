package eu.esponder.fru;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import eu.esponder.fru.event.model.config.UpdateConfigurationEvent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

public class UpdateConfig extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		String jsonActor = null;

		Bundle b = intent.getBundleExtra("event");
		String event = b.getString("event");
		Log.d("Peirame", "UpdateEvent Dto arrived = " + event);
		ObjectMapper mapperActor = new ObjectMapper();
		mapperActor
				.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
		JsonFactory jsonFactory = new JsonFactory();
		try {
			JsonParser jp = jsonFactory.createJsonParser(event);
			// get the event
			UpdateConfigurationEvent eventConfig = mapperActor.readValue(jp,
					UpdateConfigurationEvent.class);
			ActorDTO actorDTO = (ActorDTO) eventConfig.getEventAttachment();
			Log.d("Peirame!!!!", "Title = " + actorDTO.getTitle());

			mapperActor = new ObjectMapper();
			mapperActor.getSerializationConfig().setSerializationInclusion(
					JsonSerialize.Inclusion.NON_NULL);
			mapperActor.configure(
					DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,
					false);
			jsonActor = mapperActor.writeValueAsString(actorDTO);

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Write To SD Card
		File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File(sdCard.getAbsolutePath() + "/esponder");
		dir.mkdirs();
		File file = new File(dir, "config.json");
		try {
			FileOutputStream f = new FileOutputStream(file);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(f);
			myOutWriter.write(jsonActor);

			myOutWriter.close();
			f.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
