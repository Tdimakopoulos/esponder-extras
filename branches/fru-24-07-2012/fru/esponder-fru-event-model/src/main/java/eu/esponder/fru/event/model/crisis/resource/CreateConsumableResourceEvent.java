package eu.esponder.fru.event.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.fru.event.model.CreateEvent;


public class CreateConsumableResourceEvent extends ConsumableResourceEvent<ConsumableResourceDTO> implements CreateEvent {

	private static final long serialVersionUID = -4348849414526185747L;

}
