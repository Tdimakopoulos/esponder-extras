package eu.esponder.fru.event.model.config;

import eu.esponder.fru.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.fru.event.model.ESponderEvent;

public abstract class ConfigurationEvent<T extends ResourceDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = -3867440182566407274L;

}
