package eu.esponder.fru.event.model.snapshot;

import eu.esponder.fru.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.fru.event.model.UpdateEvent;


public class UpdateCrisisContextSnapshotEvent extends CrisisContextSnapshotEvent<CrisisContextSnapshotDTO> implements UpdateEvent {

	private static final long serialVersionUID = -49454585587817763L;
	
}
