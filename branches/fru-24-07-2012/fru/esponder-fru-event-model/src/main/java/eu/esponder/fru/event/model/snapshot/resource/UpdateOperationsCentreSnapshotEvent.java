package eu.esponder.fru.event.model.snapshot.resource;

import eu.esponder.fru.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.fru.event.model.UpdateEvent;


public class UpdateOperationsCentreSnapshotEvent extends OperationsCentreSnapshotEvent<OperationsCentreSnapshotDTO> implements UpdateEvent {

	private static final long serialVersionUID = 819365363519771841L;

}
