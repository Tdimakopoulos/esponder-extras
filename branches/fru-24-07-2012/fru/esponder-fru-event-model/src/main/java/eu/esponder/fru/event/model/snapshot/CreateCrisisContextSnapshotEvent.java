package eu.esponder.fru.event.model.snapshot;

import eu.esponder.fru.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.fru.event.model.CreateEvent;


public class CreateCrisisContextSnapshotEvent extends CrisisContextSnapshotEvent<CrisisContextSnapshotDTO> implements CreateEvent {

	private static final long serialVersionUID = 4658214780264993947L;
	
}
