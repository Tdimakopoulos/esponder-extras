package eu.esponder.fru.event.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.fru.event.model.CreateEvent;


public class CreateOperationsCentreEvent extends OperationsCentreEvent<OperationsCentreDTO> implements CreateEvent {

	private static final long serialVersionUID = -4230148122726013587L;

}
