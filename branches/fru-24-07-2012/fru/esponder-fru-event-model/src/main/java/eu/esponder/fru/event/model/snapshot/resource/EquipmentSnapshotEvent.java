package eu.esponder.fru.event.model.snapshot.resource;

import eu.esponder.fru.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.fru.event.model.snapshot.SnapshotEvent;


public abstract class EquipmentSnapshotEvent<T extends EquipmentSnapshotDTO> extends SnapshotEvent<T> {

	private static final long serialVersionUID = 4318763503799378366L;
	
}
