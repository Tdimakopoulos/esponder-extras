package eu.esponder.fru.event.model.snapshot.measurement.statistic;

import java.util.List;

import eu.esponder.fru.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.fru.event.model.CreateEvent;

public class CreateLocationMeasurementStatisticEvent extends SensorMeasurementStatisticEvent<SensorMeasurementStatisticEnvelopeDTO> implements CreateEvent { 
	
	private static final long serialVersionUID = -2994208980374328457L;

	@Override
	public String getJournalMessageInfo() {
		List<SensorMeasurementStatisticDTO> locationMeasurements = this.getEventAttachment().getMeasurementStatistics();
		
		String journalMessageInfo = "Received Location Measurement Statistics Envelope including: \n";
		for (SensorMeasurementStatisticDTO sensorMeasurement : locationMeasurements) {
			LocationSensorMeasurementDTO locMeasurement = (LocationSensorMeasurementDTO) sensorMeasurement.getStatistic();
			journalMessageInfo += locMeasurement.toString();
		}
		return journalMessageInfo;
	}
}
