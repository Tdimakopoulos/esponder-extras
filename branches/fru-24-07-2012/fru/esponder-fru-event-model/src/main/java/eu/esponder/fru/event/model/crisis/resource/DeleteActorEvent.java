package eu.esponder.fru.event.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import eu.esponder.fru.event.model.DeleteEvent;


public class DeleteActorEvent extends ActorEvent<ActorDTO> implements DeleteEvent {

	private static final long serialVersionUID = 451689214673500716L;
	
}
