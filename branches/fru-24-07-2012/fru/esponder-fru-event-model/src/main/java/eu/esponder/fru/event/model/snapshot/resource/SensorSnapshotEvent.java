package eu.esponder.fru.event.model.snapshot.resource;

import eu.esponder.fru.dto.model.snapshot.resource.EquipmentSnapshotDTO;

public abstract class SensorSnapshotEvent<T extends EquipmentSnapshotDTO> extends EquipmentSnapshotEvent<T> {

	private static final long serialVersionUID = -1369626844210367218L;

}
