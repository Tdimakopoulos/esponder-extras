package eu.esponder.fru.event.model.crisis.resource;

import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;


public abstract class ActorEvent<T extends ActorDTO> extends ResourceEvent<T> {

	private static final long serialVersionUID = 8357301175649628740L;

}
