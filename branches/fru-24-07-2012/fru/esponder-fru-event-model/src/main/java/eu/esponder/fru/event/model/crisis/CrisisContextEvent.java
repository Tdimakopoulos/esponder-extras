package eu.esponder.fru.event.model.crisis;

import eu.esponder.fru.dto.model.crisis.CrisisContextDTO;
import eu.esponder.fru.event.model.ESponderEvent;


public abstract class CrisisContextEvent<T extends CrisisContextDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = 2806433512434184568L;

}
