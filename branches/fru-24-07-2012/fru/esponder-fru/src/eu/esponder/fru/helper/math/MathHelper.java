package eu.esponder.fru.helper.math;

import java.math.BigDecimal;
import java.util.Queue;

import org.apache.commons.math.stat.descriptive.SummaryStatistics;
import org.apache.commons.math.stat.descriptive.SynchronizedSummaryStatistics;

import eu.esponder.fru.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;



public class MathHelper {


	private static double[] getDataArray(Queue<? extends SensorMeasurementDTO> measurements) {
		double result[] = new double[measurements.size()];
		
		int i = 0;
		
		for (SensorMeasurementDTO measurement : measurements) {
			if (measurement instanceof ArithmeticSensorMeasurementDTO) {
				ArithmeticSensorMeasurementDTO arithmeticMeasurement = (ArithmeticSensorMeasurementDTO) measurement;
				result[i] = arithmeticMeasurement.getMeasurement().doubleValue();
				i++;
			}
			
		}
		return result;
	}
	
	private static SummaryStatistics getSummaryStatistics(Queue<? extends SensorMeasurementDTO> measurements) {
		double array[] = getDataArray(measurements);
		SummaryStatistics stats = new SynchronizedSummaryStatistics();
		for (double stat : array) {
			stats.addValue(stat);
		}
		return stats;
	}
	
	public static BigDecimal calculateArithmeticStatistic(MeasurementStatisticTypeEnum statisticType, 
			Queue<? extends SensorMeasurementDTO> measurements) {
		
		SummaryStatistics stats = getSummaryStatistics(measurements);
		
		switch (statisticType) {
			case MEAN:
				return (new BigDecimal(stats.getMean()));
			case MAXIMUM:
				return (new BigDecimal(stats.getMax()));
			case MINIMUM:
				return (new BigDecimal(stats.getMin()));
			}
		
		return null;
	}

}
