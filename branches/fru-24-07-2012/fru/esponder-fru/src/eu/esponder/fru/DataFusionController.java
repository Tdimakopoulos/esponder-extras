package eu.esponder.fru;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;


import android.content.Context;
import eu.esponder.fru.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.fru.thread.DataFusionProcessorThread;
import eu.esponder.fru.thread.EnvelopeSender;
import eu.esponder.jsonrcp.JSONRPCClient;

public class DataFusionController  {

	public static Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Long>> configMap;

	public static List<SensorMeasurementEnvelopeDTO> sensorMeasurementsEnvelopeList;

	public static Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>> sensorMeasurementMaps;

	public static List<SensorMeasurementStatisticDTO> sensorMeasurementArethmeticStatisticsList,sensorMeasurementLocationStatisticsList;

	public static List<Thread> processorThreads = new ArrayList<Thread>();
	
	public static JSONRPCClient  client;
	
   
	Thread envelopeSenderMeasurementsArethmitic,envelopeSenderMeasurementsLocation;

	static Context con;	 
	
	public DataFusionController(Context con){
		this.con=con; 
	}
	
	public static void loadConfig(Context con) {

		configMap = ConfigReader
				.readMeasurementStatisticsConfig(FruConstants.FRU_CONFIGURATION_FILE,con);
		 
		/*FruConfigurator fruConfigurator = new FruConfigurator();
		try {
			fruConfigurator.init();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
//		System.out.println(FruConfigProperties.FRU_DATAFUSION_MEASUREMENT_ENVELOPE_TX_PERIOD_KEY + " = " + 
//				fruConfigurator.getProperty(FruConfigProperties.FRU_DATAFUSION_MEASUREMENT_ENVELOPE_TX_PERIOD_KEY));

	}

	public void init() {
     	
		loadConfig(con);
		client = JSONRPCClient.create("http://localhost:8080/remote/json-rpc");
	 	sensorMeasurementMaps = new HashMap<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>>();
		sensorMeasurementsEnvelopeList = new ArrayList<SensorMeasurementEnvelopeDTO>();
		sensorMeasurementArethmeticStatisticsList = new ArrayList<SensorMeasurementStatisticDTO>();
		sensorMeasurementLocationStatisticsList = new ArrayList<SensorMeasurementStatisticDTO>();

		for (Class<? extends SensorDTO> sensorClass : configMap.keySet()) {

			Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>> sensorMeasurementsStatisticsMap = new HashMap<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>();
			sensorMeasurementMaps.put(sensorClass,
					sensorMeasurementsStatisticsMap);
			 
			Map<MeasurementStatisticTypeEnum, Long> configProcessingMap = configMap
					.get(sensorClass);
			for (MeasurementStatisticTypeEnum processingType : configProcessingMap
					.keySet()) {
				sensorMeasurementsStatisticsMap.put(processingType,
						new LinkedList<SensorMeasurementDTO>());

				Queue<SensorMeasurementDTO> measurementsQueue = new LinkedList<SensorMeasurementDTO>();
				Thread statisticProcessingThread ;
				 
			if(sensorClass.getSimpleName().equals("LocationSensorDTO")){
				      statisticProcessingThread= new DataFusionProcessorThread(
						sensorClass, processingType,
						((Long) configProcessingMap.get(processingType)),
						measurementsQueue, sensorMeasurementLocationStatisticsList);
			}
				else {
					    statisticProcessingThread= new DataFusionProcessorThread(
						sensorClass, processingType,
						((Long) configProcessingMap.get(processingType)),
						measurementsQueue, sensorMeasurementArethmeticStatisticsList);
	 	     	}
			    statisticProcessingThread.start();
				processorThreads.add(statisticProcessingThread);
			}
		}
		
	//	Thread dataHandlerThread = new DataHandler(
	//			sensorMeasurementsEnvelopeList, sensorMeasurementMaps);
	//	 dataHandlerThread.start();
	 
		//envelope senders
		        envelopeSenderMeasurementsArethmitic = new EnvelopeSender(
	 	 		sensorMeasurementArethmeticStatisticsList,2,con);
	 	        envelopeSenderMeasurementsArethmitic.start();
		
		        envelopeSenderMeasurementsLocation = new EnvelopeSender(
		 		sensorMeasurementLocationStatisticsList,3,con);
		        envelopeSenderMeasurementsLocation.start();
		        
		        processorThreads.add(envelopeSenderMeasurementsArethmitic);
		        processorThreads.add(envelopeSenderMeasurementsLocation);     
 
	}

}
