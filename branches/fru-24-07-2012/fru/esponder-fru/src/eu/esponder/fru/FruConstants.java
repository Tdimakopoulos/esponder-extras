package eu.esponder.fru;

import java.math.BigDecimal;

public class FruConstants {
	
	public static final String FRU_CONFIGURATION_FILE = "config/config.txt";
	
	public static BigDecimal MAX_AVG_TEMP = new BigDecimal("20.00");
	public static BigDecimal MIN_AVG_TEMP = new BigDecimal("20.00");
	
	public static BigDecimal MAX_MAX_TEMP = new BigDecimal("20.00");
	public static BigDecimal MIN_MAX_TEMP = new BigDecimal("20.00");
	
	public static BigDecimal MAX_MIN_TEMP = new BigDecimal("55.00");
	public static BigDecimal MIN_MIN_TEMP = new BigDecimal("50.00");

}


