package eu.esponder.fru;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.content.Context;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import eu.esponder.fru.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;

public class ConfigReader {
   
	public static Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Long>> readMeasurementStatisticsConfig(
			String fileName,Context con) {
		
		Iterator iteratorEquipment,iteratorSensors,iteratorStatisticType;
		Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Long>> configMap = new HashMap<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Long>>();


		Class<? extends SensorDTO> sensorClass;

		Map<MeasurementStatisticTypeEnum, Long> tempStatisticConfigMap = null;

		try {
			
			JSONParser jpa = new JSONParser();
			ActorDTO actor = jpa.getActor();
		    
			iteratorEquipment = actor.getEquipmentSet().iterator();  
			iteratorSensors = ((EquipmentDTO)iteratorEquipment.next()).getSensors().iterator();
	 		
			while(iteratorSensors.hasNext()){
				 SensorDTO sensor = (SensorDTO)iteratorSensors.next();
				 sensorClass = sensor.getClass();
				 
				 iteratorStatisticType = sensor.getStatisticsConfig().iterator();
				  
				 while (iteratorStatisticType.hasNext()){ 
					 StatisticsConfigDTO statistic = (StatisticsConfigDTO) iteratorStatisticType.next();
					 
						if (configMap.containsKey(sensorClass)) {
							tempStatisticConfigMap = configMap.get(sensorClass);
							tempStatisticConfigMap.put(statistic.getMeasurementStatisticType(), statistic.getSamplingPeriodMilliseconds());
						} else {
							tempStatisticConfigMap = new HashMap<MeasurementStatisticTypeEnum, Long>();
							configMap.put(sensorClass, tempStatisticConfigMap);
							tempStatisticConfigMap.put(statistic.getMeasurementStatisticType(), statistic.getSamplingPeriodMilliseconds());
						}
					 
					 
				 }
				 
				 
			}
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

		return configMap;
	}
}
