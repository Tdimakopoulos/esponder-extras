package eu.esponder.fru.activities;

import java.math.BigDecimal;
import java.util.Date;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import eu.esponder.fru.DataFusionController;
import eu.esponder.fru.JSONParser;
import eu.esponder.fru.R;
import eu.esponder.fru.UpdateConfig;
import eu.esponder.fru.data.DataHandler;
import eu.esponder.fru.data.DataSender;
import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import eu.esponder.fru.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.fru.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnum;
import eu.esponder.fru.event.model.config.UpdateConfigurationEvent;
import eu.esponder.fru.event.model.snapshot.measurement.CreateArithmeticSensorMeasurementEvent;
import eu.esponder.fru.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;
import eu.esponder.jsonrcp.JSONRPCClient;
import eu.esponder.jsonrcp.JSONRPCException;

public class DFController extends Activity {

	private String filter = CreateSensorMeasurementEnvelopeEvent.class
			.getName();
	private DataHandler receiver;

	private String filterConfing = UpdateConfigurationEvent.class.getName();
	private UpdateConfig receiverConfig;
	
	TextView status;
	Button startB,stopB;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.controller);
		status = (TextView) findViewById(R.id.ControllerStatus);
		startB = (Button) findViewById(R.id.Start);
		stopB = (Button) findViewById(R.id.Stop);
		 
		if(isMyServiceRunning()){
			status.setText("Controller Active");
			startB.setEnabled(false);
	 	}else{
	 		stopB.setEnabled(false);
	 	}   
	        
  	}

	// Click on the Measurements button of Main
	public void clickHandler(View view) {
		switch (view.getId()) {
		case R.id.Start: // Start Controller
			//  Log.d("!!START!!", "!!START!!");
			//  DataHandler.flag = true;
			//Start the Service
			Intent start = new Intent("eu.esponder.fru.activities.Controller");
			DFController.this.startService(start);
			status.setText("Controller Active");
			view.setEnabled(false);
			if(!stopB.isEnabled()){
				stopB.setEnabled(true);
			}
			
		/*	Thread s = new DataSender(this);
			s.start();*/
				
			break;
		case R.id.Stop: // Stop Controller
			//  Log.d("!!STOP!!", "!!STOP!!");
			//  DataHandler.flag = false;
			
			//Stop the Service
			Intent stop = new Intent("eu.esponder.fru.activities.Controller");
			DFController.this.stopService(stop);
			status.setText("Controller Not Active");
			view.setEnabled(false);
			if(!startB.isEnabled()){
				startB.setEnabled(true);
			}
			break;
		default:
			break;
		}
	}
 
	private boolean isMyServiceRunning() {
	    ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if ("eu.esponder.fru.activities.Controller".equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
}