package eu.esponder.fru.dto.model.crisis.view;

import java.util.Set;

public class SketchPOIDTO extends ResourcePOIDTO {	
	
	private Set<MapPointDTO> points;

	public Set<MapPointDTO> getPoints() {
		return points;
	}

	public void setPoints(Set<MapPointDTO> points) {
		this.points = points;
	}	
	
}
