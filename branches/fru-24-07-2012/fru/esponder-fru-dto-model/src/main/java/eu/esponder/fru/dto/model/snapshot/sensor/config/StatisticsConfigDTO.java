package eu.esponder.fru.dto.model.snapshot.sensor.config;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;

public class StatisticsConfigDTO extends ESponderEntityDTO {
	
	private MeasurementStatisticTypeEnum measurementStatisticType;
	
	private long samplingPeriodMilliseconds;
	
	public MeasurementStatisticTypeEnum getMeasurementStatisticType() {
		return measurementStatisticType;
	}

	public void setMeasurementStatisticType(
			MeasurementStatisticTypeEnum measurementStatisticType) {
		this.measurementStatisticType = measurementStatisticType;
	}

	public long getSamplingPeriodMilliseconds() {
		return samplingPeriodMilliseconds;
	}

	public void setSamplingPeriodMilliseconds(long samplingPeriodMilliseconds) {
		this.samplingPeriodMilliseconds = samplingPeriodMilliseconds;
	}
	
}
