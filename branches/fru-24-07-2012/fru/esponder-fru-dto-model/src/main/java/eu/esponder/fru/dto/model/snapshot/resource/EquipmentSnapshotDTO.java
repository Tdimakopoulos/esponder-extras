package eu.esponder.fru.dto.model.snapshot.resource;

import eu.esponder.fru.dto.model.snapshot.SnapshotDTO;
import eu.esponder.fru.dto.model.snapshot.status.EquipmentSnapshotStatusDTO;

public class EquipmentSnapshotDTO extends SnapshotDTO {
	
	private EquipmentSnapshotStatusDTO status;

	public EquipmentSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(EquipmentSnapshotStatusDTO status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "EquipmentSnapshotDTO [status=" + status + ", period=" + period + ", id=" + id + "]";
	}
	
}
