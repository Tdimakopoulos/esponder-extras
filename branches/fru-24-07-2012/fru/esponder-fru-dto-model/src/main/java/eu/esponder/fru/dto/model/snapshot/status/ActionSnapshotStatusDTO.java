package eu.esponder.fru.dto.model.snapshot.status;

public enum ActionSnapshotStatusDTO {
	STARTED,
	PAUSED,
	COMPLETED,
	CANCELED;

	public static ActionSnapshotStatusDTO getActionSnapshotStatusEnum(int value) {
		switch (value) {
		case 0:
			return STARTED;
		case 1:
			return PAUSED;
		case 2:
			return COMPLETED;
		case 3:
			return CANCELED;
		default:
			return null;
		}
	}
}
