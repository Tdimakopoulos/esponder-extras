package eu.esponder.fru.dto.model.crisis;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.snapshot.location.SphereDTO;

public class CrisisContextDTO extends ESponderEntityDTO {
	
	private String title;
	
	private SphereDTO crisisLocation;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public SphereDTO getCrisisLocation() {
		return crisisLocation;
	}

	public void setCrisisLocation(SphereDTO crisisLocation) {
		this.crisisLocation = crisisLocation;
	}

	public String toString() {
		return "CrisisContextDTO [id=" + id + ", title=" + title + "]";
	}
		
}
