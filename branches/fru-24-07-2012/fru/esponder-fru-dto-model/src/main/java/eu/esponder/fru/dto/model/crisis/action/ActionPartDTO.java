package eu.esponder.fru.dto.model.crisis.action;

import java.util.Set;
import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.resource.ActorDTO;
import eu.esponder.fru.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.fru.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.fru.dto.model.snapshot.action.ActionPartSnapshotDTO;


public class ActionPartDTO extends ESponderEntityDTO {
	
	private String title;
	
	private ActionPartSnapshotDTO snapshot;
	
	private Set<ActionPartObjectiveDTO> actionPartObjectives;
	
	/*
	 * These resources are *used by* the actor. For example the "truck" in the following sentence: 
	 * Move this telecom equipment kits using a truck
	 */
	private Set<ConsumableResourceDTO> usedConsumableResources;
	
	/*
	 * These resources are *used by* the actor. For example the "truck" in the following sentence: 
	 * Move this telecom equipment kits using a truck
	 */
	private Set<ReusableResourceDTO> usedReusuableResources;
	
	private ActionOperationEnumDTO actionOperation;
	
	private ActorDTO actor;
	
	private Long actionID;

	public ActorDTO getActor() {
		return actor;
	}

	public void setActionID(Long actionID) {
		this.actionID = actionID;
	}

	public Long getActionID() {
		return actionID;
	}

	public void setActor(ActorDTO actor) {
		this.actor = actor;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ActionPartSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(ActionPartSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	public Set<ActionPartObjectiveDTO> getActionPartObjectives() {
		return actionPartObjectives;
	}

	public void setActionPartObjectives(Set<ActionPartObjectiveDTO> actionPartObjectives) {
		this.actionPartObjectives = actionPartObjectives;
	}

	public Set<ConsumableResourceDTO> getUsedConsumableResources() {
		return usedConsumableResources;
	}

	public void setUsedConsumableResources(Set<ConsumableResourceDTO> usedConsumableResources) {
		this.usedConsumableResources = usedConsumableResources;
	}

	public Set<ReusableResourceDTO> getUsedReusuableResources() {
		return usedReusuableResources;
	}

	public void setUsedReusuableResources(Set<ReusableResourceDTO> usedReusuableResources) {
		this.usedReusuableResources = usedReusuableResources;
	}

	public ActionOperationEnumDTO getActionOperation() {
		return actionOperation;
	}

	public void setActionOperation(ActionOperationEnumDTO actionOperation) {
		this.actionOperation = actionOperation;
	}

}
