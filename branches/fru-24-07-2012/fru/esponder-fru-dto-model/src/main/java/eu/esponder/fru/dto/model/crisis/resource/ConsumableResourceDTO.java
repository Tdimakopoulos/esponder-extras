package eu.esponder.fru.dto.model.crisis.resource;

import java.math.BigDecimal;


public class ConsumableResourceDTO extends LogisticsResourceDTO {

	private String type;
	
	private BigDecimal quantity;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

}
