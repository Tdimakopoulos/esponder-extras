package eu.esponder.fru.dto.model.snapshot;


import eu.esponder.fru.dto.model.crisis.CrisisContextDTO;
import eu.esponder.fru.dto.model.snapshot.status.CrisisContextSnapshotStatusDTO;

public class CrisisContextSnapshotDTO extends SpatialSnapshotDTO {
	
	private CrisisContextDTO crisisContext;
	
	private CrisisContextSnapshotStatusDTO status;
	
	private CrisisContextSnapshotDTO previous;
	
	private CrisisContextSnapshotDTO next;

	public CrisisContextDTO getCrisisContext() {
		return crisisContext;
	}

	public void setCrisisContext(CrisisContextDTO crisisContext) {
		this.crisisContext = crisisContext;
	}

	public CrisisContextSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(CrisisContextSnapshotStatusDTO status) {
		this.status = status;
	}

	public CrisisContextSnapshotDTO getPrevious() {
		return previous;
	}

	public void setPrevious(CrisisContextSnapshotDTO previous) {
		this.previous = previous;
	}

	public CrisisContextSnapshotDTO getNext() {
		return next;
	}

	public void setNext(CrisisContextSnapshotDTO next) {
		this.next = next;
	}

}
