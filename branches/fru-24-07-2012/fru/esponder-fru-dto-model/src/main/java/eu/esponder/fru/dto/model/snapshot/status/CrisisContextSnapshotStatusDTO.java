package eu.esponder.fru.dto.model.snapshot.status;


public enum CrisisContextSnapshotStatusDTO {
	STARTED,
	RESOLVED,
	UNRESOLVED
}
