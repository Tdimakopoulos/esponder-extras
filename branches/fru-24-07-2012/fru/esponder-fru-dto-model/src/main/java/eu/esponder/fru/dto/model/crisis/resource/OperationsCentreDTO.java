package eu.esponder.fru.dto.model.crisis.resource;

import java.util.Set;

import eu.esponder.fru.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;

public class OperationsCentreDTO extends ResourceDTO {
	
	private Set<ActorDTO> actors;
	
	private Set<OperationsCentreDTO> subordinates;
	
	private OperationsCentreSnapshotDTO snapshot;
	
	public Set<ActorDTO> getActors() {
		return actors;
	}
	
	public void setActors(Set<ActorDTO> actors) {
		this.actors = actors;
	}
	
	public Set<OperationsCentreDTO> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(Set<OperationsCentreDTO> subordinates) {
		this.subordinates = subordinates;
	}

	public OperationsCentreSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(OperationsCentreSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	@Override
	public String toString() {
		return "OperationsCentreDTO [type=" + type + ", status=" + status
				+ ", id=" + id + ", title=" + title + "]";
	}
	
}
