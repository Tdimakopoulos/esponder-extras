package eu.esponder.fru.dto.model.snapshot.resource;

import eu.esponder.fru.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.fru.dto.model.snapshot.SnapshotDTO;
import eu.esponder.fru.dto.model.snapshot.status.SensorSnapshotStatusDTO;

public class SensorSnapshotDTO extends SnapshotDTO {

	private SensorSnapshotStatusDTO status;
	
	private String meanValue;
	
	private String standardDeviation;
	
	private SensorDTO sensor;

	public SensorSnapshotStatusDTO getStatus() {
		return status;
	}

	public SensorDTO getSensor() {
		return sensor;
	}

	public void setSensor(SensorDTO sensor) {
		this.sensor = sensor;
	}

	public void setStatus(SensorSnapshotStatusDTO status) {
		this.status = status;
	}

	public String getMeanValue() {
		return meanValue;
	}

	public void setMeanValue(String meanValue) {
		this.meanValue = meanValue;
	}

	public String getStandardDeviation() {
		return standardDeviation;
	}

	public void setStandardDeviation(String standardDeviation) {
		this.standardDeviation = standardDeviation;
	}

	@Override
	public String toString() {
		return "SensorSnapshotDTO [status=" + status + ", meanValue="
				+ meanValue + ", standardDeviation=" + standardDeviation
				+ ", period=" + period + ", id=" + id + "]";
	}
	
}
