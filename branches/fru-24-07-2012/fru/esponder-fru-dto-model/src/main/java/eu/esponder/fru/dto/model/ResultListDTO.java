package eu.esponder.fru.dto.model;

import java.util.List;

public class ResultListDTO {
	
	private List<? extends ESponderEntityDTO> resultList;

	public List<? extends ESponderEntityDTO> getResultList() {
		return resultList;
	}

	public void setResultList(List<? extends ESponderEntityDTO> resultList) {
		this.resultList = resultList;
	}

	
}
