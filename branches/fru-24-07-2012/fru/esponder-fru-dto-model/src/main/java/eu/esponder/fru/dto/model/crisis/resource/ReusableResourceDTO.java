package eu.esponder.fru.dto.model.crisis.resource;

import java.math.BigDecimal;
import java.util.Set;


public class ReusableResourceDTO extends LogisticsResourceDTO {

	private String type;
	
	private BigDecimal quantity;
	
	private Set<ConsumableResourceDTO> consumables;
	
	private Set<ReusableResourceDTO> children;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Set<ConsumableResourceDTO> getConsumables() {
		return consumables;
	}

	public void setConsumables(Set<ConsumableResourceDTO> consumables) {
		this.consumables = consumables;
	}

	public Set<ReusableResourceDTO> getChildren() {
		return children;
	}

	public void setChildren(Set<ReusableResourceDTO> children) {
		this.children = children;
	}
	
}
