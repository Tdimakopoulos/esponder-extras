package eu.esponder.fru.dto.model.crisis.resource.sensor;

import java.util.Set;

import eu.esponder.fru.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnum;

public abstract class SensorDTO extends ResourceDTO {
	
	private String name;
	
	private MeasurementUnitEnum measurementUnit;
	
	private Set<StatisticsConfigDTO> statisticsConfig;
	
	private String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MeasurementUnitEnum getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(MeasurementUnitEnum measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	public Set<StatisticsConfigDTO> getStatisticsConfig() {
		return statisticsConfig;
	}

	public void setStatisticsConfig(Set<StatisticsConfigDTO> statisticsConfig) {
		this.statisticsConfig = statisticsConfig;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(null == obj || obj instanceof SensorDTO)) return false;
		
		SensorDTO sensor = (SensorDTO) obj;
		if (!(sensor.getClass() == SensorDTO.class)) return false;
		return true;
	}
	@Override
	public String toString() {
		return "SensorDTO [name=" + name + ", type=" + type + ", measurementUnit="
				+ measurementUnit + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}
	
}
