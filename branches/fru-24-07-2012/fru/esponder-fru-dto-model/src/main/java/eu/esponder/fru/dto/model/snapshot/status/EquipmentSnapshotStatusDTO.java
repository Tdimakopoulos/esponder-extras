package eu.esponder.fru.dto.model.snapshot.status;


public enum EquipmentSnapshotStatusDTO {
	WORKING,
	MALFUNCTIONING,
	DAMAGED
}
