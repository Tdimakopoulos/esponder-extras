package eu.esponder.fru.dto.model.crisis.view;

 

import eu.esponder.fru.dto.model.ESponderEntityDTO;


 
public abstract class ResourcePOIDTO extends ESponderEntityDTO {
	
	protected String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
