package eu.esponder.fru.dto.model.crisis.view;


public class ReferencePOIDTO extends ResourcePOIDTO {
	
	private String referenceFile;

	public String getReferenceFile() {
		return referenceFile;
	}

	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

}
