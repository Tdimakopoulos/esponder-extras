package eu.esponder.fru.dto.model.crisis.view;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.snapshot.location.PointDTO;

public class MapPointDTO extends ESponderEntityDTO {
	
	private String title;

	private PointDTO point;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public PointDTO getPoint() {
		return point;
	}

	public void setPoint(PointDTO point) {
		this.point = point;
	}

}
