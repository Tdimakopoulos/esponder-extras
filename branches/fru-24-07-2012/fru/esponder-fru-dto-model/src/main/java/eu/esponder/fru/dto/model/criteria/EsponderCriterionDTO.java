package eu.esponder.fru.dto.model.criteria;

import java.io.Serializable;

public class EsponderCriterionDTO extends EsponderQueryRestrictionDTO {

	private static final long serialVersionUID = -4927714081578656589L;

	private String field;
	
	private EsponderCriterionExpressionEnumDTO expression;
	
	private Serializable value;

	public String getFieldName() {
		return field;
	}

	public void setFieldName(String fieldName) {
		this.field = fieldName;
	}

	public EsponderCriterionExpressionEnumDTO getExpression() {
		return expression;
	}

	public void setExpression(EsponderCriterionExpressionEnumDTO expression) {
		this.expression = expression;
	}

	public Serializable getFieldValue() {
		return value;
	}

	public void setFieldValue(Serializable fieldValue) {
		this.value = fieldValue;
	}

}
