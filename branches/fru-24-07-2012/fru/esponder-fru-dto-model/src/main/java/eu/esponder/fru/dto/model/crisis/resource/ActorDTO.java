package eu.esponder.fru.dto.model.crisis.resource;

import java.util.Set;

import eu.esponder.fru.dto.model.snapshot.resource.ActorSnapshotDTO;

public class ActorDTO extends ResourceDTO {

	private Set<EquipmentDTO> equipmentSet;

	private Set<ActorDTO> subordinates;

	private ActorSnapshotDTO snapshot;

	private ActorDTO supervisor;

	public Set<EquipmentDTO> getEquipmentSet() {
		return equipmentSet;
	}

	public void setEquipmentSet(Set<EquipmentDTO> equipmentSet) {
		this.equipmentSet = equipmentSet;
	}

	public Set<ActorDTO> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(Set<ActorDTO> subordinates) {
		this.subordinates = subordinates;
	}

	public ActorSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(ActorSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	public ActorDTO getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(ActorDTO supervisor) {
		this.supervisor = supervisor;
	}

	@Override
	public String toString() {
		return "ActorDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}

}
