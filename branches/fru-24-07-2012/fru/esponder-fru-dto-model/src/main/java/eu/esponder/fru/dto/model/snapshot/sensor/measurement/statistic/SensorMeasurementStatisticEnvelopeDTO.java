package eu.esponder.fru.dto.model.snapshot.sensor.measurement.statistic;

import java.util.List;

import eu.esponder.fru.dto.model.ESponderEntityDTO;

public class SensorMeasurementStatisticEnvelopeDTO extends ESponderEntityDTO {

	private List<SensorMeasurementStatisticDTO> measurementStatistics;
	
	private String fRId;

	public String getfRId() {
		return fRId;
	}

	public void setfRId(String fRId) {
		this.fRId = fRId;
	}

	public List<SensorMeasurementStatisticDTO> getMeasurementStatistics() {
		return measurementStatistics;
	}

	public void setMeasurementStatistics(
			List<SensorMeasurementStatisticDTO> measurementStatistics) {
		this.measurementStatistics = measurementStatistics;
	}
	
}
