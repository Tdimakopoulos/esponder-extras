package eu.esponder.fru.dto.model.snapshot.resource;

import eu.esponder.fru.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.fru.dto.model.snapshot.status.ActorSnapshotStatusDTO;

public class ActorSnapshotDTO extends SpatialSnapshotDTO {
	
	private ActorSnapshotStatusDTO status;

	public ActorSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActorSnapshotStatusDTO status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ActorSnapshotDTO [status=" + status + ", locationArea=" + locationArea + ", id=" + id  + "]";
	}
	
}
