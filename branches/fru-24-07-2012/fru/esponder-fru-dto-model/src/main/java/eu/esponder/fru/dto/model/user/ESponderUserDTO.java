package eu.esponder.fru.dto.model.user;

import java.util.Set;

import eu.esponder.fru.dto.model.ESponderEntityDTO;
import eu.esponder.fru.dto.model.crisis.resource.OperationsCentreDTO;

public class ESponderUserDTO extends ESponderEntityDTO {
	
	private String userName;
	
	private Set<OperationsCentreDTO> operationsCentres;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Set<OperationsCentreDTO> getOperationsCentres() {
		return operationsCentres;
	}

	public void setOperationsCentres(Set<OperationsCentreDTO> operationsCentres) {
		this.operationsCentres = operationsCentres;
	}

	@Override
	public String toString() {
		return "ESponderUserDTO [userName=" + userName + ", id=" + id + "]";
	}

}
