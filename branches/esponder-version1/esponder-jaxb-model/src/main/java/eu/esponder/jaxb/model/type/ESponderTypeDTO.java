package eu.esponder.jaxb.model.type;

import java.util.Set;

import eu.esponder.jaxb.model.ESponderEntityDTO;

public class ESponderTypeDTO extends ESponderEntityDTO {
	
	public ESponderTypeDTO() {}
	
	protected Long id;
	
	protected String title;
	
	protected ESponderTypeDTO parent;
	
	protected Set<ESponderTypeDTO> children;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ESponderTypeDTO getParent() {
		return parent;
	}

	public void setParent(ESponderTypeDTO parent) {
		this.parent = parent;
	}

	public Set<ESponderTypeDTO> getChildren() {
		return children;
	}

	public void setChildren(Set<ESponderTypeDTO> children) {
		this.children = children;
	}
	
}
