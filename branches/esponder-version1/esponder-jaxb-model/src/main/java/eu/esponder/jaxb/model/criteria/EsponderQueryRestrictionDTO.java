package eu.esponder.jaxb.model.criteria;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
@XmlSeeAlso({
	EsponderIntersectionCriteriaCollectionDTO.class,
	EsponderUnionCriteriaCollectionDTO.class,
	EsponderNegationCriteriaCollectionDTO.class,
//	EsponderCriteriaCollectionDTO.class,
	EsponderCriterionDTO.class
})
public abstract class EsponderQueryRestrictionDTO implements Serializable {

	private static final long serialVersionUID = -6325570244012530074L;

}
