package eu.esponder.jaxb.model.snapshot;

import javax.xml.bind.annotation.XmlSeeAlso;
import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.resource.SensorSnapshotDTO;

@XmlSeeAlso({
	EquipmentSnapshotDTO.class,
	SensorSnapshotDTO.class,
	SpatialSnapshotDTO.class
})
public abstract class SnapshotDTO extends ESponderEntityDTO {
	
	protected PeriodDTO period;

	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

}
