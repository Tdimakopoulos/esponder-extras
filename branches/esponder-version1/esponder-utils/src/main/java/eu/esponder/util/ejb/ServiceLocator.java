package eu.esponder.util.ejb;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ServiceLocator/*<T>*/ {
	
	private static Context ctx;

	static {
		try {
			ctx = new InitialContext();
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getResource(Class<T> type, String resourceName) throws NamingException {
		return (T) ctx.lookup(resourceName);
	}
	
	/**
	 * Implements the actual JNDI lookup and cache management.
	 * 
	 * @param resourceName the JNDI resource identifier to lookup
	 * @return the retrieved object
	 * @throws NamingException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getResource(String resourceName) throws NamingException {
		return (T) ctx.lookup(resourceName);
	}
	
}
