package eu.esponder.dto.model.type;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@XmlSeeAlso ({
	OperationalActorTypeDTO.class,
	StrategicActorTypeDTO.class,
	TacticalActorTypeDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActorTypeDTO extends ESponderTypeDTO {

	private static final long serialVersionUID = -4092646127121534860L;

}
