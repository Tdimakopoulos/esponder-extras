package eu.esponder.dto.model.crisis.view;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@XmlRootElement(name="referencePOI")
@XmlType(name="ReferencePOI")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "referenceFile", "averageSize"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ReferencePOIDTO extends ResourcePOIDTO {
	
	private static final long serialVersionUID = 698766046297897727L;

	private String referenceFile;
	
	private Float averageSize;

	public String getReferenceFile() {
		return referenceFile;
	}

	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

	public Float getAverageSize() {
		return averageSize;
	}

	public void setAverageSize(Float averageSize) {
		this.averageSize = averageSize;
	}

}
