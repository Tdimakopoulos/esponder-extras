package eu.esponder.dto.model.crisis.action;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="SeverityLevel")
public enum SeverityLevelDTO {
	MINIMAL,
	MEDIUM,
	SERIOUS,
	SEVERE,
	FATAL,
	UNDEFINED
}
