package eu.esponder.dto.model.config;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@XmlRootElement(name="droolsConfiguration")
@XmlType(name="DroolsConfiguration")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "parameterName", "parameterValue", "description"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class DroolsConfigurationDTO extends ESponderConfigParameterDTO {

	private static final long serialVersionUID = -3533725912580406116L;
	
	public DroolsConfigurationDTO() {};

}
