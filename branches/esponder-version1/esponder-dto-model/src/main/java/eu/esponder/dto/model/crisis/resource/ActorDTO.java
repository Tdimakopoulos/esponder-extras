package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;

@XmlRootElement(name="actor")
@XmlType(name="Actor")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status","operationsCentre",  "equipmentSet", "subordinates", "snapshot", "supervisor", "voIPURL"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActorDTO extends ResourceDTO {
	
	private static final long serialVersionUID = 660813303586822307L;

	public ActorDTO() { }

	private Set<EquipmentDTO> equipmentSet;

	private Set<ActorDTO> subordinates;

	private ActorSnapshotDTO snapshot;

	private ActorDTO supervisor;
	
	private VoIPURLDTO voIPURL;
	
//	private OperationsCentreDTO operationsCentre;
	
	private Long operationsCentreId;
	
	private PersonnelDTO personnel;

	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}

	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}

	public Set<EquipmentDTO> getEquipmentSet() {
		return equipmentSet;
	}

	public void setEquipmentSet(Set<EquipmentDTO> equipmentSet) {
		this.equipmentSet = equipmentSet;
	}

	public Set<ActorDTO> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(Set<ActorDTO> subordinates) {
		this.subordinates = subordinates;
	}

	public ActorDTO getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(ActorDTO supervisor) {
		this.supervisor = supervisor;
	}

	@Override
	public String toString() {
		return "ActorDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}

	public ActorSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(ActorSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

//	public OperationsCentreDTO getOperationsCentre() {
//		return operationsCentre;
//	}
//
//	public void setOperationsCentre(OperationsCentreDTO operationsCentre) {
//		this.operationsCentre = operationsCentre;
//	}

	public PersonnelDTO getPersonnel() {
		return personnel;
	}

	public void setPersonnel(PersonnelDTO personnel) {
		this.personnel = personnel;
	}

	public Long getOperationsCentreId() {
		return operationsCentreId;
	}

	public void setOperationsCentreId(Long operationsCentreId) {
		this.operationsCentreId = operationsCentreId;
	}

}
