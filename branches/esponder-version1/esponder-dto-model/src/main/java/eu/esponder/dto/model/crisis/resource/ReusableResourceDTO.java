package eu.esponder.dto.model.crisis.resource;

import java.math.BigDecimal;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;

@XmlRootElement(name="reusableResource")
@XmlType(name="ReusableResource")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "quantity", "consumables", "children", "configuration"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ReusableResourceDTO extends LogisticsResourceDTO {

	private static final long serialVersionUID = -1012441430710219912L;

	public ReusableResourceDTO() { }

	private String type;
	
	private BigDecimal quantity;
	
	private Set<ConsumableResourceDTO> consumables;
	
	private Set<ReusableResourceDTO> children;
	
	private ActionPartDTO actionPart;
	
	private ReusableResourceCategoryDTO reusableResourceCategory;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Set<ConsumableResourceDTO> getConsumables() {
		return consumables;
	}

	public void setConsumables(Set<ConsumableResourceDTO> consumables) {
		this.consumables = consumables;
	}

	public Set<ReusableResourceDTO> getChildren() {
		return children;
	}

	public void setChildren(Set<ReusableResourceDTO> children) {
		this.children = children;
	}

	public ActionPartDTO getActionPart() {
		return actionPart;
	}

	public void setActionPart(ActionPartDTO actionPart) {
		this.actionPart = actionPart;
	}

	public ReusableResourceCategoryDTO getReusableResourceCategory() {
		return reusableResourceCategory;
	}

	public void setReusableResourceCategory(ReusableResourceCategoryDTO reusableResourceCategory) {
		this.reusableResourceCategory = reusableResourceCategory;
	}
	
}
