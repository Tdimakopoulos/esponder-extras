package eu.esponder.dto.model.crisis.resource.category;

import java.util.Set;

import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.type.RankTypeDTO;

public class PersonnelCategoryDTO extends PlannableResourceCategoryDTO {

	private static final long serialVersionUID = 9187806862545991852L;

	/**
	 * Models the discipline-specific (RankType) rank, i.e. General, Colonel etc. 
	 */
	private RankTypeDTO rank;
	
	private PersonnelDTO personnel;
	
	private Set<PersonnelCompetenceDTO> personnelCompetences;
	
	private OrganisationCategoryDTO organisationCategory;

	public RankTypeDTO getRank() {
		return rank;
	}

	public void setRank(RankTypeDTO rank) {
		this.rank = rank;
	}

	public PersonnelDTO getPersonnel() {
		return personnel;
	}

	public void setPersonnel(PersonnelDTO personnel) {
		this.personnel = personnel;
	}

	public Set<PersonnelCompetenceDTO> getPersonnelCompetences() {
		return personnelCompetences;
	}

	public void setPersonnelCompetences(
			Set<PersonnelCompetenceDTO> personnelCompetences) {
		this.personnelCompetences = personnelCompetences;
	}

	public OrganisationCategoryDTO getOrganisationCategory() {
		return organisationCategory;
	}

	public void setOrganisationCategory(OrganisationCategoryDTO organisationCategory) {
		this.organisationCategory = organisationCategory;
	}
	
}
