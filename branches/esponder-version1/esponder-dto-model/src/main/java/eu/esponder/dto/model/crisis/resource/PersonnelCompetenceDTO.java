package eu.esponder.dto.model.crisis.resource;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;

@XmlSeeAlso({
	PersonnelSkillDTO.class,
	PersonnelTrainingDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class PersonnelCompetenceDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = 886664390428578001L;

	private String shortTitle;
	
	private PersonnelCategoryDTO personnelCategory;

	public String getShortTitle() {
		return shortTitle;
	}

	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}

	public PersonnelCategoryDTO getPersonnelCategory() {
		return personnelCategory;
	}

	public void setPersonnelCategory(PersonnelCategoryDTO personnelCategory) {
		this.personnelCategory = personnelCategory;
	}
	
}
