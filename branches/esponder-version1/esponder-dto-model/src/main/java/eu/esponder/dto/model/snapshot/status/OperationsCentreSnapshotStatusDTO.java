package eu.esponder.dto.model.snapshot.status;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="OperationsCentreSnapshotStatus")
public enum OperationsCentreSnapshotStatusDTO {
	STATIONARY,
	MOVING
}
