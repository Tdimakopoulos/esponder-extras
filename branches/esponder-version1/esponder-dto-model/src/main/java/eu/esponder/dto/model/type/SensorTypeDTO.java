package eu.esponder.dto.model.type;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;

@XmlRootElement(name="sensorType")
@XmlType(name="SensorType")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "measurementUnit", "parent", "children"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SensorTypeDTO extends ESponderTypeDTO {
	
	private static final long serialVersionUID = -1374104680044702555L;

	public SensorTypeDTO() { }
	
	private MeasurementUnitEnumDTO measurementUnit;
	
	public MeasurementUnitEnumDTO getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(MeasurementUnitEnumDTO measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

}
