package eu.esponder.dto.model.criteria;

import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@XmlRootElement(name="EsponderNegationCriteriaCollectionDTO")
@XmlType(name="EsponderNegationCriteriaCollectionDTO")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"restrictions"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EsponderNegationCriteriaCollectionDTO extends EsponderCriteriaCollectionDTO {

	private static final long serialVersionUID = -786849464712579302L;

	public EsponderNegationCriteriaCollectionDTO(
			Collection<EsponderQueryRestrictionDTO> restrictions) {
		super(restrictions);
	}
	
	public EsponderNegationCriteriaCollectionDTO() { }

}
