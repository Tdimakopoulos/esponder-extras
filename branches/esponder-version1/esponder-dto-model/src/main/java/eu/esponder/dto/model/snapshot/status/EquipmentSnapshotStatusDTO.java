package eu.esponder.dto.model.snapshot.status;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="EquipmentSnapshotStatus")
public enum EquipmentSnapshotStatusDTO {
	WORKING,
	MALFUNCTIONING,
	DAMAGED
}
