package eu.esponder.dto.model.snapshot.status;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="SensorSnapshotStatus")
public enum SensorSnapshotStatusDTO {
	WORKING,
	MALFUNCTIONING,
	DAMAGED
}
