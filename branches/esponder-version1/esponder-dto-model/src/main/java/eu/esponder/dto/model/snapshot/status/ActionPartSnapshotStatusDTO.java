package eu.esponder.dto.model.snapshot.status;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="ActionPartSnapshotStatus")
public enum ActionPartSnapshotStatusDTO {
	STARTED,
	PAUSED,
	COMPLETED,
	CANCELED
}
