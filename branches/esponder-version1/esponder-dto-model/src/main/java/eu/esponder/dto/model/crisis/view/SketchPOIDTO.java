package eu.esponder.dto.model.crisis.view;

import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@XmlRootElement(name="sketchPOI")
@XmlType(name="SketchPOI")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title","description","sketchType","httpURL", "points"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SketchPOIDTO extends ResourcePOIDTO {	
	
	private static final long serialVersionUID = -8869263968721446925L;

	public SketchPOIDTO() { }
	
	private String description;
	
	private String sketchType;
	
	private HttpURLDTO httpURL;
	
	@XmlElementWrapper(name="points")
	@XmlElement(name="points")
	private Set<MapPointDTO> points;

	public String getDescription() {
		return description;
	}

	public String getSketchType() {
		return sketchType;
	}

	public void setSketchType(String sketchType) {
		this.sketchType = sketchType;
	}

	public HttpURLDTO getHttpURL() {
		return httpURL;
	}

	public void setHttpURL(HttpURLDTO httpURL) {
		this.httpURL = httpURL;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<MapPointDTO> getPoints() {
		return points;
	}

	public void setPoints(Set<MapPointDTO> points) {
		this.points = points;
	}	
	
}
