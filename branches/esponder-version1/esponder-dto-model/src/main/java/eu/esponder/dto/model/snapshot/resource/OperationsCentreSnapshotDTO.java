package eu.esponder.dto.model.snapshot.resource;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.OperationsCentreSnapshotStatusDTO;

@XmlRootElement(name="operationsCentreSnapshot")
@XmlType(name="OperationsCentreSnapshot")
@JsonPropertyOrder({"id", "status", "locationArea", "period"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OperationsCentreSnapshotDTO extends SpatialSnapshotDTO {

	private static final long serialVersionUID = -6058516655453683876L;

	public OperationsCentreSnapshotDTO() { }
	
	private OperationsCentreSnapshotStatusDTO status;

	public OperationsCentreSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(OperationsCentreSnapshotStatusDTO status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "OperationsCentreSnapshotDTO [status=" + status + ", locationArea=" + locationArea + ", id=" + id + "]";
	}
	
}
