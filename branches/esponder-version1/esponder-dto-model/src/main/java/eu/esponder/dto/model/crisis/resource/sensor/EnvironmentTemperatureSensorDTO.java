package eu.esponder.dto.model.crisis.resource.sensor;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@XmlRootElement(name="environmentTemperatureSensor")
@XmlType(name="EnvironmentTemperatureSensor")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "measurementUnit", "configuration"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EnvironmentTemperatureSensorDTO extends EnvironmentalSensorDTO {

	private static final long serialVersionUID = 3270938273131158445L;

}
