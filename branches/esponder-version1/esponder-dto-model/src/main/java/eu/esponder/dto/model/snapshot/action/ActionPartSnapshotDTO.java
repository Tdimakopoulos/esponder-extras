package eu.esponder.dto.model.snapshot.action;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;

@XmlRootElement(name="actionPartSnapshot")
@XmlType(name="ActionPartSnapshot")
@JsonPropertyOrder({"id", "status","locationArea", "period", "actionPartId"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionPartSnapshotDTO extends SpatialSnapshotDTO {
	
	private static final long serialVersionUID = 6629496009765832542L;

	public ActionPartSnapshotDTO() { }
	
	private Long actionPartId;
	
	private ActionPartSnapshotStatusDTO status;

	public Long getActionPartId() {
		return actionPartId;
	}

	public void setActionPartId(Long actionPartId) {
		this.actionPartId = actionPartId;
	}

	public ActionPartSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActionPartSnapshotStatusDTO status) {
		this.status = status;
	}
	
		
}
