package eu.esponder.dto.model.snapshot.status;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="ActorSnapshotStatus")
public enum ActorSnapshotStatusDTO {
	READY,
	ACTIVE,
	INACTIVE
}
