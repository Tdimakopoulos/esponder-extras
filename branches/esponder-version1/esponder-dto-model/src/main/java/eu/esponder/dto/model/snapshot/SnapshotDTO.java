package eu.esponder.dto.model.snapshot;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;

@XmlSeeAlso({
	EquipmentSnapshotDTO.class,
	SensorSnapshotDTO.class,
	SpatialSnapshotDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class SnapshotDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = 3358588722380975179L;
	
	protected PeriodDTO period;

	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

}
