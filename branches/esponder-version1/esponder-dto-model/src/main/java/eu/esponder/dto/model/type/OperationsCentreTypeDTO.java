package eu.esponder.dto.model.type;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@XmlSeeAlso ({
	EmergencyOperationsCentreTypeDTO.class,
	MobileEmergencyOperationsCentreTypeDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class OperationsCentreTypeDTO extends ESponderTypeDTO {

	private static final long serialVersionUID = -4180901117994144848L;

}
