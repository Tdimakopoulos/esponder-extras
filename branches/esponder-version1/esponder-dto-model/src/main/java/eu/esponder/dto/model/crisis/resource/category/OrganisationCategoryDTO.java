package eu.esponder.dto.model.crisis.resource.category;

import java.util.Set;

import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;


public class OrganisationCategoryDTO extends ResourceCategoryDTO {

	private static final long serialVersionUID = 8982078577957843565L;

	private DisciplineTypeDTO disciplineType;
	
	/**
	 * Models the hierarchy of internal organisation elements of the particular organisation,
	 * e.g. for FireFighters: Headquarters, Fire Stations, Garages etc., 
	 * thus advanced searching is enabled.
	 */
	private OrganisationTypeDTO organisationType;
	
//	private OrganisationDTO organisation;
	
	private Long organisationId;
	
	private Set<PersonnelCategoryDTO> personnelCategories;

	public DisciplineTypeDTO getDisciplineType() {
		return disciplineType;
	}

	public void setDisciplineType(DisciplineTypeDTO disciplineType) {
		this.disciplineType = disciplineType;
	}

	public OrganisationTypeDTO getOrganisationType() {
		return organisationType;
	}

	public void setOrganisationType(OrganisationTypeDTO organisationType) {
		this.organisationType = organisationType;
	}

//	public OrganisationDTO getOrganisation() {
//		return organisation;
//	}
//
//	public void setOrganisation(OrganisationDTO organisation) {
//		this.organisation = organisation;
//	}

	public Set<PersonnelCategoryDTO> getPersonnelCategories() {
		return personnelCategories;
	}

	public void setPersonnelCategories(Set<PersonnelCategoryDTO> personnelCategories) {
		this.personnelCategories = personnelCategories;
	}

	public Long getOrganisationId() {
		return organisationId;
	}

	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

	

}
