package eu.esponder.dto.model.crisis.view;

public class VoIPURLDTO extends URLDTO {

	private static final long serialVersionUID = -3608367404473258930L;

	public VoIPURLDTO() {
		this.setProtocol("sip");
	}
}
