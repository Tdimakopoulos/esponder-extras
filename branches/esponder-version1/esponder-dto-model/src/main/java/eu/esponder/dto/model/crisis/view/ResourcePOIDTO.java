package eu.esponder.dto.model.crisis.view;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;

@XmlSeeAlso({
	ReferencePOIDTO.class,
	SketchPOIDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ResourcePOIDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = 8785404155107069797L;
	
	protected String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
