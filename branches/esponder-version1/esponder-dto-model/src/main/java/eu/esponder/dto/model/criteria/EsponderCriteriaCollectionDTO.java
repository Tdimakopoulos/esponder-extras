package eu.esponder.dto.model.criteria;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({EsponderIntersectionCriteriaCollectionDTO.class, EsponderUnionCriteriaCollectionDTO.class, EsponderNegationCriteriaCollectionDTO.class})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class EsponderCriteriaCollectionDTO extends
		EsponderQueryRestrictionDTO {

	private static final long serialVersionUID = -7905934732438391487L;

	@XmlElement(name = "EsponderQueryRestrictionDTO")
	private Collection<EsponderQueryRestrictionDTO> restrictions;

	public EsponderCriteriaCollectionDTO() {
	}

	public EsponderCriteriaCollectionDTO(
			Collection<EsponderQueryRestrictionDTO> restrictions) {
		this.restrictions = restrictions;
	}

	public void setRestrictions(
			Collection<EsponderQueryRestrictionDTO> restrictions) {
		this.restrictions = restrictions;
	}

	public Collection<EsponderQueryRestrictionDTO> getRestrictions() {
		return restrictions;
	}

	public void add(EsponderQueryRestrictionDTO restriction) {
		this.getRestrictions().add(restriction);
	}

	public void remove(EsponderQueryRestrictionDTO restriction) {
		this.getRestrictions().remove(restriction);
	}

}
