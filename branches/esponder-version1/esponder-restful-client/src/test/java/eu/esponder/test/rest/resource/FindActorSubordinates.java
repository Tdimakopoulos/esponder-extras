package eu.esponder.test.rest.resource;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.rest.client.ResteasyClient;
import eu.esponder.util.jaxb.Parser;

public class FindActorSubordinates {
	private String ACTOR_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/resource/actor";

	@Test
	public void FindSubordinatesWithXml() throws ClassNotFoundException, Exception {
	
		Parser parser = new Parser(new Class[] {ResultListDTO.class});
		String serviceName = ACTOR_SERVICE_URI + "/getSubordinates";
		ResteasyClient postClient = new ResteasyClient(serviceName, "application/xml");
		Map<String, String> serviceParams =  getActorSubordinatesParameters();
		
		String resultXML = postClient.get(serviceParams);
		ResultListDTO results = (ResultListDTO) parser.unmarshal(resultXML);

		System.out.println("\n*********FULL_XML*******************************\n");
		System.out.println(resultXML);
		System.out.println("*********FULL_XML_END***************************\n");
		
		System.out.println("\n\nresultListDTO after unmarshall" + results.toString());
	}
	

	@Test
	public void FindSubordinatesWithJson() throws ClassNotFoundException, Exception {
	
		String serviceName = ACTOR_SERVICE_URI + "/getSubordinates";
		ResteasyClient postClient = new ResteasyClient(serviceName, "application/json");
		Map<String, String> serviceParams =  getActorSubordinatesParameters();
		
		String resultJson = postClient.get(serviceParams);

		System.out.println("\n*********FULL_JSON*******************************\n");
		System.out.println(resultJson);
		System.out.println("*********FULL_JSON_END***************************\n");
		
		ObjectMapper mapper = new ObjectMapper();
		ResultListDTO results = mapper.readValue(resultJson, ResultListDTO.class);
		
		System.out.println("\n\nresultListDTO after unmarshall" + results.toString());
	}

	private Map<String, String> getActorSubordinatesParameters() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("actorID", "19");
		params.put("userID", "1");
		return params;
	}

}
