package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.event.model.CreateEvent;


public class CreateOperationsCentreSnapshotEvent extends OperationsCentreSnapshotEvent<OperationsCentreSnapshotDTO> implements CreateEvent {

	private static final long serialVersionUID = 1150492225030805266L;

}
