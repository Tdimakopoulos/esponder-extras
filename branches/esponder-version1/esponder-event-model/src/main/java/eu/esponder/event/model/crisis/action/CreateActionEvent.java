package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionDTO;


public class CreateActionEvent extends ActionEvent<ActionDTO> /*implements CreateEvent*/ {

	private static final long serialVersionUID = 2006035559724305579L;
	
	public CreateActionEvent() { }
	
}
