package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.crisis.CrisisContextDTO;


public class CrisisContextSnapshotEvent extends SnapshotEvent<CrisisContextDTO> {

	private static final long serialVersionUID = 5971268269850771662L;

}
