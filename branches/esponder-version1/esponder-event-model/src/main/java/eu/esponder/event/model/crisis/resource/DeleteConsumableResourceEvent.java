package eu.esponder.event.model.crisis.resource;

import eu.esponder.event.model.DeleteEvent;


public class DeleteConsumableResourceEvent extends ConsumableResourceEvent implements DeleteEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1886755992124247660L;

}
