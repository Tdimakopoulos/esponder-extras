package eu.esponder.fru.helper.math;

import java.util.Iterator;
import java.util.List;
import java.util.Queue;

public class MathHelper {

	public double average(List<Integer> seq) {
		// calculate sum
		int sum = 0;
		for (int i = 0; i < seq.size(); i++) {
			sum = sum + seq.get(i);
		}
		// calculate average
		return sum / seq.size();

	}

	public static int getMaxValue(List<Integer> seq) {
		int maxValue = seq.get(0);
		for (int i = 1; i < seq.size(); i++) {
			if (seq.get(i) > maxValue) {
				maxValue = seq.get(i);
			}
		}
		return maxValue;
	}

	public static int getMinValue(List<Integer> seq) {
		int minValue = seq.get(0);
		for (int i = 1; i < seq.size(); i++) {
			if (seq.get(i) < minValue) {
				minValue = seq.get(i);
			}
		}
		return minValue;
	}

	public static double average(Queue<Double> seq) {
		// calculate sum
		double sum = 0;
		Iterator<Double> it = seq.iterator();
		while (it.hasNext()) {
			sum = sum + (Double) it.next();
		}
		// calculate average
		double average = (sum * 10) / seq.size();
		return average / 10;

	}

	public static double getMaxValue(Queue<Double> seq) {
		double maxValue = (Double) seq.peek();
		double current;
		Iterator<Double> it = seq.iterator();

		while (it.hasNext()) {
			current = (Double) it.next();
			if (current > maxValue) {
				maxValue = current;
			}

		}

		return maxValue;
	}

	public static double getMinValue(Queue<Double> seq) {
		double minValue = (Double) seq.peek();
		double current;
		Iterator<Double> it = seq.iterator();

		while (it.hasNext()) {
			current = (Double) it.next();
			if (current < minValue) {
				minValue = current;
			}

		}

		return minValue;
	}

}
