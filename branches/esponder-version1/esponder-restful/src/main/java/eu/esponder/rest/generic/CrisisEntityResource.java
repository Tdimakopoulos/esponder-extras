package eu.esponder.rest.generic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.ClientResponseType;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionExpressionEnumDTO;
import eu.esponder.dto.model.criteria.EsponderQueryRestrictionDTO;
import eu.esponder.dto.model.criteria.EsponderUnionCriteriaCollectionDTO;
import eu.esponder.rest.ESponderResource;

@Path("/crisis/generic")
public class CrisisEntityResource extends ESponderResource {

	@POST
	@Path("/create")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public ESponderEntityDTO createEntity(
			@NotNull(message="entity to be created may not be null") ESponderEntityDTO entityDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {

		ESponderEntityDTO createdEntity = getMappingService().mapGenericCreateEntity(entityDTO, userID);
		return createdEntity;
	}

	@DELETE
	@Path("/delete")
	public void deleteEntity(
			@QueryParam("entityClass") @NotNull(message="entityClass may not be null") String entityClass,
			@QueryParam("entityID") @NotNull(message="entityID may not be null") Long entityID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {

		getMappingService().mapGenericDeleteEntity(entityClass, entityID);
	}

	@PUT
	@Path("/update")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public ESponderEntityDTO updateEntity( 
			@NotNull(message="entity to be updated may not be null") ESponderEntityDTO entityDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {

		ESponderEntityDTO updatedEntity = getMappingService().mapGenericUpdateEntity(entityDTO, userID);
		return updatedEntity;
	}


	@GET
	@Path("/get")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public ResultListDTO getEntity (
			@QueryParam("idList") @NotNull(message="criteriaDTO may not be null") String idListStr,
			@QueryParam("queriedEntityDTO") @NotNull(message="queriedEntity may not be null") String queriedEntityDTO,
			@QueryParam("pageNumber") @NotNull(message="Page Number may not be null") int pageNumber,
			@QueryParam("pageSize") @NotNull(message="Page Size may not be null") int pageSize,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {

		ArrayList<Long> idList = StringToIDList(idListStr);	
		EsponderQueryRestrictionDTO restrictions = generateRestrictionsFromIdList(idList);
		if(restrictions!=null)
			return postEntity(restrictions, queriedEntityDTO, pageSize, pageNumber, userID);
		else
			return null;
	}

	@POST
	@Path("/post")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public ResultListDTO postEntity(
			EsponderQueryRestrictionDTO criteriaDTO,
			@QueryParam("queriedEntityDTO") @NotNull(message="queriedEntity may not be null") String queriedEntityDTO,  
			@QueryParam("pageNumber") @NotNull(message="Page Number may not be null") int pageNumber,
			@QueryParam("pageSize") @NotNull(message="Page Size may not be null") int pageSize,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {

		ResultListDTO resultsList = new ResultListDTO(this.getGenericService().getDTOEntities(
				queriedEntityDTO, criteriaDTO, pageSize, pageNumber));
		return resultsList;
	}

	private EsponderQueryRestrictionDTO generateRestrictionsFromIdList (List<Long> idList) {

		EsponderUnionCriteriaCollectionDTO criteriaDTO = new EsponderUnionCriteriaCollectionDTO(new HashSet<EsponderQueryRestrictionDTO>());
		if(!idList.isEmpty()) {
			for(Long id : idList) {
				criteriaDTO.add( generateCriterion( id ) );
			}
			return criteriaDTO;
		}
		else {
			/*
			 * TODO: integrate the User Exceptions Hierarchy that will be defined later
			 * i.e., list of ids is empty
			 */
			System.out.println("idList is Null, cannot continue...");
		}
		return null;
	}


	private EsponderCriterionDTO generateCriterion (Long id) {
		EsponderCriterionDTO criterion = new EsponderCriterionDTO();
		criterion.setField("id");
		criterion.setExpression(EsponderCriterionExpressionEnumDTO.EQUAL);
		criterion.setValue(id.toString());
		return criterion;
	}

	private ArrayList<Long> StringToIDList(String idListStr) {
		idListStr = idListStr.replace("[", "").replace("]", "");
		String delims = ", ";
		String[] tokens = idListStr.split(delims);
		ArrayList<Long> idList = new ArrayList<Long>();
		for(String i : tokens) {
			idList.add(new Long(i));
		}
		return idList;
	}

	/*
	 * TODO: Remove this. This is for testing the Resteasy Client with Proxy 
	 */
	@GET
	@Path("/leoleis")
	@ClientResponseType(entityType = ResultListDTO.class)
//	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML/*, MediaType.APPLICATION_JSON*/})
	public ResultListDTO testRestEasyProxy(@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {
		System.out.println("Received Invocation with UserID: " + userID.toString());
		String idList = "[1, 3]";
		ResultListDTO resultList = getEntity(idList, "eu.esponder.dto.model.crisis.resource.ActorDTO", 0, 10, new Long(1));
		return resultList;
	}


}