package eu.esponder.rest.crisis.test;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.rest.ESponderResource;


@Path("/crisis/contextTest")
public class TestResource extends ESponderResource {
	
	@GET
	@Path("/test")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public PointDTO getRestTest(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		PointDTO point = new PointDTO();
		
		point.setAltitude(new BigDecimal(1));
		point.setLongitude(new BigDecimal(2));
		point.setLatitude(new BigDecimal(3));
		
		return point; 
	}
	
	
	@POST
	@Path("/testPost")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public PointDTO postRestTest(
			@NotNull(message="point may not be null") PointDTO point, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
//		PointDTO anotherPoint = new PointDTO();
		System.out.println("Server: POST operation received");
		System.out.println(point.toString());
		point.setAltitude(new BigDecimal(55L));
		System.out.println("Server: POST operation returning");
		return point;
	}
	
}
