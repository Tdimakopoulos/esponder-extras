package eu.esponder.model.snapshot.location;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("CUBE")
public class CubeVolume extends LocationArea {
	
	private static final long serialVersionUID = -3142156536477281310L;

	public CubeVolume() { }
	
	public CubeVolume(Point point, BigDecimal acme) {
		this.acme = acme;
		this.point = point;
	}
	
	@Embedded
	private Point point;
	
	@Column(name="ACME")
	private BigDecimal acme;
	
	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public BigDecimal getAcme() {
		return acme;
	}

	public void setAcme(BigDecimal acme) {
		this.acme = acme;
	}
	

}
