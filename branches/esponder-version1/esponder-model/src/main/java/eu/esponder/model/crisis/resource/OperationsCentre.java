package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.collections.CollectionUtils;

import eu.esponder.model.crisis.resource.category.OperationsCentreCategory;
import eu.esponder.model.crisis.resource.plan.PlannableResource;
import eu.esponder.model.crisis.view.VoIPURL;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;
import eu.esponder.model.user.ESponderUser;

@Entity
@Table(name="operations_centre")
@NamedQueries({
	@NamedQuery(name="OperationsCentre.findByTitle", query="select c from OperationsCentre c where c.title=:title"),
	@NamedQuery(name="OperationsCentre.findAll", query="select c from OperationsCentre c"),
	@NamedQuery(name="OperationsCentre.findByIdAndUser", query="select c from OperationsCentre c, ESponderUser u where c.id=:operationsCentreID and u.id=:userID and u member of c.users")
})
public class OperationsCentre extends PlannableResource {
	
	private static final long serialVersionUID = 1911200656062784549L;

	@ManyToOne
	@JoinColumn(name="SUPERVISOR_ID")
	private OperationsCentre supervisor;
	
	@OneToMany(mappedBy="supervisor")
	private Set<OperationsCentre> subordinates;
	
	@OneToMany(mappedBy="operationsCentre")
	private Set<Actor> actors;
	
	@OneToMany(mappedBy="operationsCentre")
	private Set<OperationsCentreSnapshot> snapshots;
	
	@ManyToMany(mappedBy="operationsCentres")
	private Set<ESponderUser> users;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private OperationsCentreCategory operationsCentreCategory;

	@Embedded
	private VoIPURL voIPURL;
	
	public OperationsCentreCategory getOperationsCentreCategory() {
		return operationsCentreCategory;
	}

	public void setOperationsCentreCategory(
			OperationsCentreCategory operationsCentreCategory) {
		this.operationsCentreCategory = operationsCentreCategory;
	}

	public OperationsCentre getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(OperationsCentre supervisor) {
		this.supervisor = supervisor;
	}

	public Set<OperationsCentre> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(Set<OperationsCentre> subordinates) {
		this.subordinates = subordinates;
	}

	public Set<Actor> getActors() {
		return actors;
	}

	public void setActors(Set<Actor> actors) {
		this.actors = actors;
	}

	public Set<OperationsCentreSnapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<OperationsCentreSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	public Set<ESponderUser> getUsers() {
		return users;
	}

	public void setUsers(Set<ESponderUser> users) {
		this.users = users;
	}

	public VoIPURL getVoIPURL() {
		return voIPURL;
	}

	public void setVoIPURL(VoIPURL voIPURL) {
		this.voIPURL = voIPURL;
	}

	public void traverseSubordinates() {
		if (CollectionUtils.isNotEmpty(this.subordinates)) {
			for (OperationsCentre operationsCentre : this.subordinates) {
				operationsCentre.traverseSubordinates();
			}
		}
	}

}
