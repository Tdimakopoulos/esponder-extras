package eu.esponder.model.crisis.view;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@DiscriminatorValue("SKETCH")
@NamedQueries({
	@NamedQuery(name="SketchPOI.findByOperationsCentre", query="select s from SketchPOI s where s.operationsCentre.id=:operationsCentreID"),
	@NamedQuery(name="SketchPOI.findBySketchPOIId", query="select s from SketchPOI s where s.id=:sketchPOIId"),
	@NamedQuery(name="SketchPOI.findByTitle", query="select s from SketchPOI s where s.title=:sketchPOITitle")
})
public class SketchPOI extends ResourcePOI {
	
	private static final long serialVersionUID = 3958925970048521465L;

	
	@ManyToMany(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinTable(name="sketch_points",
			joinColumns=@JoinColumn(name="SKETCH_ID"),
			inverseJoinColumns=@JoinColumn(name="POINT_ID"))
	private Set<MapPoint> points;
	
	@Embedded
	private HttpURL httpURL;
	
	@Column(name="sketchType")
	private String sketchType;

	public Set<MapPoint> getPoints() {
		return points;
	}

	public void setPoints(Set<MapPoint> points) {
		this.points = points;
	}

	public HttpURL getHttpURL() {
		return httpURL;
	}

	public void setHttpURL(HttpURL httpURL) {
		this.httpURL = httpURL;
	}

	public String getSketchType() {
		return sketchType;
	}

	public void setSketchType(String sketchType) {
		this.sketchType = sketchType;
	}
	
}
