package eu.esponder.model.crisis.resource.sensor;

import java.util.Set;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.crisis.resource.Resource;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.snapshot.sensor.config.StatisticsConfig;
import eu.esponder.model.type.SensorType;

@Entity
@Table(name="sensor")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="DISCRIMINATOR")
@NamedQueries({
	@NamedQuery(name="Sensor.findByTitle", query="select s from Sensor s where s.title=:title")
})
public abstract class Sensor extends Resource {

	private static final long serialVersionUID = -5390561449137637770L;

	@ManyToOne
	@JoinColumn(name="SENSOR_TYPE_ID", nullable=false)
	private SensorType sensorType;
	
	@ManyToOne
	@JoinColumn(name="EQUIPMENT_ID")
	private Equipment equipment;
	
	@OneToMany(mappedBy="sensor")
	private Set<SensorSnapshot> snapshots;
	
	@OneToOne(mappedBy="sensor")
	private StatisticsConfig configuration;

	public SensorType getSensorType() {
		return sensorType;
	}

	public void setSensorType(SensorType sensorType) {
		this.sensorType = sensorType;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public Set<SensorSnapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<SensorSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	public StatisticsConfig getConfiguration() {
		return configuration;
	}

	public void setConfiguration(StatisticsConfig configuration) {
		this.configuration = configuration;
	}
	
	
	
}
