package eu.esponder.model.crisis.resource;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import eu.esponder.model.type.PersonnelTrainingType;


@Entity
@DiscriminatorValue("TRAINING")
public class PersonnelTraining extends PersonnelCompetence {

	private static final long serialVersionUID = -2454388646575931422L;
	
	@OneToOne
	@JoinColumn(name="TRAIN_TYPE")
	private PersonnelTrainingType personnelTrainingType;

	public PersonnelTrainingType getPersonnelTrainingType() {
		return personnelTrainingType;
	}

	public void setPersonnelTrainingType(PersonnelTrainingType personnelTrainingType) {
		this.personnelTrainingType = personnelTrainingType;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    final PersonnelTraining other = (PersonnelTraining) obj;
	    if (this.getId()!= other.getId()) {
	        return false;
	    }
	    if (!this.getPesonnelCategory().equals(other.getPesonnelCategory())) {
	    	return false;
	    }
	    if (!this.getShortTitle().equals(other.getShortTitle())) {
	    	return false;
	    }
	    if (!this.getDescription().equals(other.getDescription())) {
	    	return false;
	    }
	    if (!this.getRecordStatus().equals(other.getRecordStatus())) {
	    	return false;
	    }
	    if (!this.getPersonnelTrainingType().equals(other.getPersonnelTrainingType())) {
	    	return false;
	    }
	    return true;
	}

}
