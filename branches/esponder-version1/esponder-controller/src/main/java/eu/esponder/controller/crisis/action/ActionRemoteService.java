package eu.esponder.controller.crisis.action;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;

@Remote
public interface ActionRemoteService {

	public ActionDTO findActionDTOById(Long actionID);

	public ActionDTO findActionDTOByTitle(String title);

	public ActionDTO createActionRemote(ActionDTO actionDTO, Long userID);

	public ActionDTO updateActionRemote(ActionDTO actionDTO, Long userID);

	public void deleteActionRemote(Long actionDTOID, Long userID);

	public ActionPartDTO findActionPartDTOById(Long actionPartID);

	public ActionPartDTO findActionPartDTOByTitle(String title);

	public ActionPartDTO createActionPartRemote(ActionPartDTO actionPartDTO, Long userID);

	public ActionPartDTO updateActionPartRemote(ActionPartDTO actionPartDTO, Long userID);

	public void deleteActionPartRemote(Long actionPartDTOID, Long userID);

	public CrisisContextDTO findCrisisContextDTOById(Long actionID);

	public CrisisContextDTO findCrisisContextDTOByTitle(String title);

	public CrisisContextDTO createCrisisContextRemote(CrisisContextDTO crisisContextDTO, Long userID);

	public CrisisContextDTO updateCrisisContextRemote(CrisisContextDTO crisisContextDTO, Long userID);

	public void deleteCrisisContextDTO(Long crisisContextId, Long userID);

	public ActionObjectiveDTO findActionObjectiveDTOByTitle(String title);

	public ActionObjectiveDTO findActionObjectiveDTOById(Long actionID);

	public ActionObjectiveDTO createActionObjectiveDTO(ActionObjectiveDTO actionObjectiveDTO, Long userID);

	public ActionObjectiveDTO updateActionObjectiveDTO(ActionObjectiveDTO actionObjectiveDTO, Long userID);

	public void deleteActionObjectiveDTO(Long actionObjectiveDTOId, Long userID);

	public ActionPartObjectiveDTO findActionPartObjectiveDTOById(Long actionID);

	public ActionPartObjectiveDTO findActionPartObjectiveDTOByTitle(String title);

	public ActionPartObjectiveDTO createActionPartObjectiveDTO(ActionPartObjectiveDTO actionPartObjectiveDTO, Long userID);

	public ActionPartObjectiveDTO updateActionPartObjectiveDTO(ActionPartObjectiveDTO actionPartObjectiveDTO, Long userID);

	public void deleteActionPartObjectiveDTO(Long actionPartObjectiveDTOId, Long userID);

}
