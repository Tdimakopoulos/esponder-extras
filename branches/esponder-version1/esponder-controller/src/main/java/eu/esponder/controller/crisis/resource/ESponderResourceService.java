package eu.esponder.controller.crisis.resource;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Resource;

@Local
public interface ESponderResourceService extends ESponderResourceRemoteService {
	
	public Resource findByID(Class<? extends Resource> clz, Long id);

	public Resource findByTitle(Class<? extends Resource> clz, String title);

}
