package eu.esponder.controller.mapping;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.model.ESponderEntity;

@Local
public interface ESponderMappingService extends ESponderRemoteMappingService {

//	public List<OperationsCentreDTO> mapUserOperationsCentres(Long userID);
//
//	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID);
//
//	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID, Long userID);
//
//	public List<SketchPOIDTO> mapOperationsCentreSketches(Long operationsCentreID, Long userID);
//
//	public List<ReferencePOIDTO> mapOperationsCentreReferencePOIs(Long operationsCentreID, Long userID);
//
//	public OperationsCentreSnapshotDTO mapOperationsCentreSnapshot(Long operationsCentreID, Date maxDate);
//
//	public ActorSnapshotDTO mapActorSnapshot(Long actorID, Date maxDate);
//
//	public EquipmentSnapshotDTO mapEquipmentSnapshot(Long equipmentID, Date maxDate);
//
//	public SensorSnapshotDTO mapSensorSnapshot(Long sensorID, Date maxDate);
//
//	public SketchPOI mapSketch(SketchPOIDTO sketchDTO);
//
//	public ReferencePOI mapReferencePOI(ReferencePOIDTO referencePOIDTO);
//
//	public EsponderQueryRestriction mapCriteriaCollection(EsponderQueryRestrictionDTO criteria);
//
//	public EsponderCriterion mapSimpleCriterion(EsponderCriterionDTO criteria);
//
	public List<? extends ESponderEntityDTO> mapESponderEntity(List<? extends ESponderEntity<Long>> resultsList, Class<? extends ESponderEntityDTO> targetClass);

	public List<? extends ESponderEntity<?>> mapESponderEntityDTO(List<? extends ESponderEntityDTO> resultsList, Class<? extends ESponderEntity<?>> targetClass);

	public ESponderEntityDTO mapESponderEntity(ESponderEntity<Long> entity, Class<? extends ESponderEntityDTO> targetClass);
	
	public ESponderEntity<?> mapESponderEntityDTO(ESponderEntityDTO entity, Class<? extends ESponderEntity<?>> targetClass);
	
	public Object mapESponderEnumDTO(Object entity, Class<?> targetClass);
	
	public Class<? extends ESponderEntity<Long>> getManagedEntityClass(Class<? extends ESponderEntityDTO> clz) throws ClassNotFoundException;
	
	public Class<? extends ESponderEntityDTO> getDTOEntityClass(Class<? extends ESponderEntity<Long>> clz) throws ClassNotFoundException;

	public Class<?> getEntityClass(String className) throws ClassNotFoundException;

	public String getManagedEntityName(String queriedEntityDTO);
	
	public String getDTOEntityName(String entityName);

	public void mapEntityToEntity(ESponderEntity<Long> src, ESponderEntity<Long> dest);
	

}
