package eu.esponder.controller.crisis.resource;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.PersonnelDTO;


@Remote
public interface PersonnelRemoteService {

	public PersonnelDTO findDTOById(Long personnelID);

	public PersonnelDTO findDTOByTitle(String personnelTitle);

	public PersonnelDTO createPersonnelDTO(PersonnelDTO personnelDTO);

	public PersonnelDTO updatePersonnelDTO(PersonnelDTO personnelDTO);

	public void deletePersonnelDTO(PersonnelDTO personnelDTO);

	public void deletePersonnelDTOById(Long personnelDTOId);

}
