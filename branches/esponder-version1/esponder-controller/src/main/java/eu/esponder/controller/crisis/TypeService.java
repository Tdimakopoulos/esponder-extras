package eu.esponder.controller.crisis;

import javax.ejb.Local;

import eu.esponder.model.type.ESponderType;

@Local
public interface TypeService extends TypeRemoteService {
	
	public ESponderType findById(Long typeId);
	
	public ESponderType findByTitle(String title);
	
	public ESponderType createType(ESponderType type, Long userID);
	
	public ESponderType updateType(ESponderType type, Long userID);
	
	public void deleteType(Long ESponderTypeID, Long userID);
	
}
