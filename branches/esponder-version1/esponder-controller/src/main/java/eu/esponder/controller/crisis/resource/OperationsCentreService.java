package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.crisis.view.SketchPOI;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;

@Local
public interface OperationsCentreService extends OperationsCentreRemoteService {

	public OperationsCentre findOperationCentreById(Long operationsCentreID);

	public OperationsCentre findOperationsCentreByTitle(String title);

	public OperationsCentre findOperationsCentreByUser(Long operationsCentreID, Long userID);

	public OperationsCentre setSupervisor(OperationsCentre operationsCentre, OperationsCentre supervisingOperationsCentre, Long userID);

	public OperationsCentre createOperationsCentre(OperationsCentre operationsCentre, Long userID);

	public OperationsCentre updateOperationsCentre(OperationsCentre operationsCentre, Long userID);

	public void deleteOperationsCentre(Long operationsCentreId, Long userID);

	public OperationsCentreSnapshot findOperationsCentreSnapshotByDate(Long operationsCentreID, Date maxDate);

	public OperationsCentreSnapshot createOperationsCentreSnapshot(OperationsCentre operationsCentre, OperationsCentreSnapshot snapshot, Long userID);
	
	public OperationsCentreSnapshot updateOperationsCentreSnapshot(OperationsCentre operationsCentre, OperationsCentreSnapshot snapshot, Long userID);
	
	public void deleteOperationsCentreSnapshot(Long operationsCentreSnapshotID);
	
	public SketchPOI findSketchPOIById(Long sketchPOIId);

	public SketchPOI findSketchPOIByTitle(String title);
	
	public List<SketchPOI> findSketchPOI(Long operationsCentreID);

	public SketchPOI createSketchPOI(OperationsCentre operationsCentre, SketchPOI sketch, Long userID);
	
	public SketchPOI updateSketchPOI(OperationsCentre operationsCentre, SketchPOI sketch, Long userID);
	
	public void deleteSketchPOI(Long sketchPOIId, Long userID);

	public ReferencePOI findReferencePOIByTitle(String title);

	public ReferencePOI findReferencePOIById(Long referencePOIId);
	
	public List<ReferencePOI> findReferencePOIs(Long operationsCentreID);

	public ReferencePOI createReferencePOI(OperationsCentre operationsCentre, ReferencePOI referencePOI, Long userID);

	public ReferencePOI updateReferencePOI(OperationsCentre operationsCentre, ReferencePOI referencePOI, Long userID);
	
	public void deleteReferencePOI(Long referencePOIId, Long userID);

}
