package eu.esponder.controller.crisis.resource;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;

@Remote
public interface LogisticsRemoteService {
	
	public ReusableResourceDTO findReusableResourceByIdRemote(Long reusableID);
	
	public ReusableResourceDTO findReusableResourceByTitleRemote(String title);
	
	public ReusableResourceDTO createReusableResourceRemote(ReusableResourceDTO reusableResourceDTO, Long userID);
	
	public ReusableResourceDTO updateReusableResourceDTO(ReusableResourceDTO reusableResourceDTO, Long userID);
	
	public void deleteReusableResourceRemote(Long reusableResourceDTOID, Long userID);
	
	public ConsumableResourceDTO findConsumableResourceByIdRemote(Long consumableID);
	
	public ConsumableResourceDTO findConsumableResourceByTitleRemote(String title);
	
	public ConsumableResourceDTO createConsumableResourceRemote(ConsumableResourceDTO consumableResourceDTO, Long userID);
	
	public ConsumableResourceDTO updateConsumableResourceRemote(ConsumableResourceDTO consumableResourceDTO, Long userID);
	
	public void deleteConsumableResourceRemote(Long consumableResourceDTOID, Long userID);

}
