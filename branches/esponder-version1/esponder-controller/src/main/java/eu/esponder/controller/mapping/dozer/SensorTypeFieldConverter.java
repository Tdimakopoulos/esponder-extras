package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.TypeService;
import eu.esponder.model.type.SensorType;
import eu.esponder.util.ejb.ServiceLocator;
import eu.esponder.util.logger.ESponderLogger;


public class SensorTypeFieldConverter implements CustomConverter {

	protected TypeService getTypeService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Object convert(Object destination, 
			Object source, 
			Class<?> destinationClass,
			Class<?> sourceClass) {

		ESponderLogger.warn(this.getClass(), "SensorTypeFieldConverter reached");
		if (sourceClass == String.class && source != null) {
			String type = (String) source;
			SensorType sensorType = (SensorType) getTypeService().findByTitle(type);
			destination = sensorType;
		}
		else if(SensorType.class.isAssignableFrom(sourceClass)) {
			SensorType sensorType = (SensorType) source;
			destination = (String) sensorType.getTitle();
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}