package eu.esponder.controller.crisis.action.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.action.ActionService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.crisis.action.ActionObjective;
import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.model.crisis.action.ActionPartObjective;
import eu.esponder.model.type.ActionType;

@Stateless
public class ActionBean<T> implements ActionService, ActionRemoteService {

	@EJB
	private CrudService<CrisisContext> crisisCrudService;

	@EJB
	private CrudService<Action> actionCrudService;

	@EJB
	private CrudService<ActionPart> actionPartCrudService;

	@EJB
	private CrudService<ActionObjective> actionObjectiveCrudService;

	@EJB
	private CrudService<ActionPartObjective> actionPartObjectiveCrudService;

	@EJB
	private TypeService typeService;

	@EJB
	private ESponderMappingService mappingService;


	// -------------------------------------------------------------------------

	@Override
	public ActionDTO findActionDTOById(Long actionID) {
		Action action = findActionById(actionID);
		return (ActionDTO) mappingService.mapESponderEntity(action, ActionDTO.class);	
	}

	@Override
	public Action findActionById(Long actionID) {
		return (Action) actionCrudService.find(Action.class, actionID);
	}

	// -------------------------------------------------------------------------

	@Override
	public ActionDTO findActionDTOByTitle(String title) {
		Action action = findActionByTitle(title);
		return (ActionDTO) mappingService.mapESponderEntity(action, ActionDTO.class);
	}


	@Override
	public Action findActionByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Action) actionCrudService.findSingleWithNamedQuery(
				"Action.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	@Override
	public ActionPartDTO findActionPartDTOById(Long actionPartID) {
		ActionPart actionPart = findActionPartById(actionPartID);
		return (ActionPartDTO) mappingService.mapESponderEntity(actionPart, ActionPartDTO.class);
	}

	@Override
	public ActionPart findActionPartById(Long actionPartID) {
		return (ActionPart) actionPartCrudService.find(ActionPart.class, actionPartID);
	}

	// -------------------------------------------------------------------------

	@Override
	public ActionPartDTO findActionPartDTOByTitle(String title) {
		ActionPart actionPart = findActionPartByTitle(title);
		return (ActionPartDTO) mappingService.mapESponderEntity(actionPart, ActionPartDTO.class);
	}

	@Override
	public ActionPart findActionPartByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ActionPart) actionPartCrudService.findSingleWithNamedQuery(
				"ActionPart.findByTitle", params);
	}


	// -------------------------------------------------------------------------

	@Override
	public ActionObjectiveDTO findActionObjectiveDTOById(Long actionID) {
		ActionObjective actionObjective = findActionObjectiveById(actionID);
		return (ActionObjectiveDTO) mappingService.mapESponderEntity(actionObjective, ActionObjectiveDTO.class);	
	}

	@Override
	public ActionObjective findActionObjectiveById(Long actionID) {
		return (ActionObjective) actionObjectiveCrudService.find(ActionObjective.class, actionID);
	}

	// -------------------------------------------------------------------------

	@Override
	public ActionObjectiveDTO findActionObjectiveDTOByTitle(String title) {
		ActionObjective actionObjective = findActionObjectiveByTitle(title);
		return (ActionObjectiveDTO) mappingService.mapESponderEntity(actionObjective, ActionObjectiveDTO.class);
	}

	@Override
	public ActionObjective findActionObjectiveByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ActionObjective) actionPartObjectiveCrudService.findSingleWithNamedQuery(
				"ActionPartObjective.findByTitle", params);
	}


	// -------------------------------------------------------------------------

	@Override
	public ActionPartObjectiveDTO findActionPartObjectiveDTOById(Long actionID) {
		ActionPartObjective actionPartObjective = findActionPartObjectiveById(actionID);
		return (ActionPartObjectiveDTO) mappingService.mapESponderEntity(actionPartObjective, ActionPartObjectiveDTO.class);	
	}

	@Override
	public ActionPartObjective findActionPartObjectiveById(Long actionID) {
		return (ActionPartObjective) actionPartObjectiveCrudService.find(ActionPartObjective.class, actionID);
	}

	// -------------------------------------------------------------------------

	@Override
	public ActionPartObjectiveDTO findActionPartObjectiveDTOByTitle(String title) {
		ActionPartObjective actionPartObjective = findActionPartObjectiveByTitle(title);
		return (ActionPartObjectiveDTO) mappingService.mapESponderEntity(actionPartObjective, ActionPartObjectiveDTO.class);
	}

	@Override
	public ActionPartObjective findActionPartObjectiveByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ActionPartObjective) actionPartObjectiveCrudService.findSingleWithNamedQuery(
				"ActionPartObjective.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextDTO findCrisisContextDTOById(Long actionID) {
		CrisisContext crisisContext = findCrisisContextById(actionID);
		return (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
	}	

	@Override
	public CrisisContext findCrisisContextById(Long actionID) {
		return (CrisisContext) crisisCrudService.find(CrisisContext.class, actionID);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextDTO findCrisisContextDTOByTitle(String title) {
		CrisisContext crisisContext = findCrisisContextByTitle(title);
		return (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
	}	

	@Override
	public CrisisContext findCrisisContextByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (CrisisContext) crisisCrudService.findSingleWithNamedQuery("CrisisContext.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionDTO createActionRemote(ActionDTO actionDTO, Long userID) {
		Action action = (Action) mappingService.mapESponderEntityDTO(actionDTO, Action.class);
		action = createAction(action, userID);
		return (ActionDTO) mappingService.mapESponderEntity(action, ActionDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Action createAction(Action action, Long userID) {
		ActionType actionType = (ActionType) typeService.findById(action.getActionType().getId());
		action.setActionType((actionType));
		actionCrudService.create(action);
		return action;
	}


	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionDTO updateActionRemote(ActionDTO actionDTO, Long userID) {
		Action action = (Action) mappingService.mapESponderEntityDTO(actionDTO, Action.class);
		action = updateAction(action, userID);
		return (ActionDTO) mappingService.mapESponderEntity(action, ActionDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Action updateAction(Action action, Long userID) {
		ActionType actionType = (ActionType) typeService.findById(action.getActionType().getId());
		action.setActionType((actionType));
		actionCrudService.update(action);
		return action;
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionPartDTO createActionPartRemote(ActionPartDTO actionPartDTO, Long userID) {
		ActionPart actionPart = (ActionPart) mappingService.mapESponderEntityDTO(actionPartDTO, ActionPart.class);
		actionPart = createActionPart(actionPart, userID);
		return (ActionPartDTO) mappingService.mapESponderEntity(actionPart, ActionPartDTO.class); 
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionPart createActionPart(ActionPart actionPart, Long userID) {
		ActionPart actionPartPersisted = actionPartCrudService.create(actionPart);
		return actionPartPersisted;
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionPartDTO updateActionPartRemote(ActionPartDTO actionPartDTO, Long userID) {
		ActionPart actionPart = (ActionPart) mappingService.mapESponderEntityDTO(actionPartDTO, ActionPart.class);
		actionPart = updateActionPart(actionPart, userID);
		return (ActionPartDTO) mappingService.mapESponderEntity(actionPart, ActionPartDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionPart updateActionPart(ActionPart actionPart, Long userID) {
		Action action = actionCrudService.find(Action.class, actionPart.getAction().getId());
		if( action !=  null) {
			actionPart.setAction(action);
		}
		actionPart = actionPartCrudService.update(actionPart);
		return actionPart;
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextDTO createCrisisContextRemote(CrisisContextDTO crisisContextDTO, Long userID) {
		CrisisContext crisisContext = (CrisisContext) mappingService.mapESponderEntityDTO(crisisContextDTO, CrisisContext.class);
		crisisContext = createCrisisContext(crisisContext, userID);
		return (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public CrisisContext createCrisisContext(CrisisContext crisisContext, Long userID) {
		return (CrisisContext) crisisCrudService.create(crisisContext);
	}
	
	// -------------------------------------------------------------------------

	@Override
	public CrisisContextDTO updateCrisisContextRemote( CrisisContextDTO crisisContextDTO, Long userID) {
		CrisisContext crisisContext = (CrisisContext) mappingService.mapESponderEntityDTO(crisisContextDTO, CrisisContext.class);
		crisisContext = updateCrisisContext(crisisContext, userID);
		crisisContextDTO = (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
		return crisisContextDTO;
	}

	@Override
	public CrisisContext updateCrisisContext(CrisisContext crisisContext, Long userID) {
		crisisContext = crisisCrudService.update(crisisContext);
		return crisisContext;
	}
	
	// -------------------------------------------------------------------------

	@Override
	public void deleteCrisisContextDTO(Long crisisContextId, Long userID) {
		deleteCrisisContext(crisisContextId, userID);	
	}

	@Override
	public void deleteCrisisContext(Long crisisContextId, Long userID) {
		crisisCrudService.delete(CrisisContext.class, crisisContextId);
	}
	
	// -------------------------------------------------------------------------

	@Override
	public void deleteActionRemote(Long actionDTOID, Long userID) {
		deleteAction(actionDTOID, userID);
	}

	@Override
	public void deleteAction(Long actionID, Long userID) {
		actionCrudService.delete(Action.class, actionID);
		
	}

	// -------------------------------------------------------------------------
	
	@Override
	public void deleteActionPartRemote(Long actionPartDTOID, Long userID) {
		deleteActionPart(actionPartDTOID, userID);
	}

	@Override
	public void deleteActionPart(Long actionPartID, Long userID) {
		actionPartCrudService.delete(ActionPart.class, actionPartID);
	}
	
	// -------------------------------------------------------------------------

	@Override
	public ActionObjectiveDTO createActionObjectiveDTO(ActionObjectiveDTO actionObjectiveDTO, Long userID) {
		ActionObjective actionObjective = (ActionObjective) mappingService.mapESponderEntityDTO(actionObjectiveDTO, ActionObjective.class);
		actionObjective = createActionObjective(actionObjective, userID);
		actionObjectiveDTO = (ActionObjectiveDTO) mappingService.mapESponderEntity(actionObjective, ActionObjectiveDTO.class);
		return actionObjectiveDTO;
	}
	
	@Override
	public ActionObjective createActionObjective(ActionObjective actionObjective, Long userID) {
		Action action = actionCrudService.find(Action.class, actionObjective.getAction().getId());
		if(action != null){
			actionObjective.setAction(action);
			actionObjective = actionObjectiveCrudService.create(actionObjective);
			return actionObjective;
		}
		return null;
	}
	
	// -------------------------------------------------------------------------

	@Override
	public ActionObjectiveDTO updateActionObjectiveDTO(ActionObjectiveDTO actionObjectiveDTO, Long userID) {
		ActionObjective actionObjective = (ActionObjective) mappingService.mapESponderEntityDTO(actionObjectiveDTO, ActionObjective.class);
		actionObjective = updateActionObjective(actionObjective, userID);
		actionObjectiveDTO = (ActionObjectiveDTO) mappingService.mapESponderEntity(actionObjective, ActionObjectiveDTO.class);
		return null;
	}
	
	@Override
	public ActionObjective updateActionObjective( ActionObjective actionObjective, Long userID) {
		Action action = actionCrudService.find(Action.class, actionObjective.getAction().getId());
		if(action != null){
			actionObjective.setAction(action);
			actionObjective = actionObjectiveCrudService.update(actionObjective);
			return actionObjective;
		}
		return null;
	}

	// -------------------------------------------------------------------------
	
	@Override
	public void deleteActionObjectiveDTO(Long actionObjectiveDTOId, Long userID) {
		deleteActionObjective(actionObjectiveDTOId, userID);
	}	

	@Override
	public void deleteActionObjective(Long actionObjectiveId, Long userID) {
		actionObjectiveCrudService.delete(ActionObjective.class, actionObjectiveId);
	}
	
	// -------------------------------------------------------------------------

	@Override
	public ActionPartObjectiveDTO createActionPartObjectiveDTO( ActionPartObjectiveDTO actionPartObjectiveDTO, Long userID) {
		ActionPartObjective actionPartObjective = (ActionPartObjective) mappingService.mapESponderEntityDTO(actionPartObjectiveDTO, ActionPartObjective.class);
		actionPartObjective = createActionPartObjective(actionPartObjective, userID);
		actionPartObjectiveDTO = (ActionPartObjectiveDTO) mappingService.mapESponderEntity(actionPartObjective, ActionPartObjectiveDTO.class);
		return actionPartObjectiveDTO;
	}
	
	@Override
	public ActionPartObjective createActionPartObjective( ActionPartObjective actionPartObjective, Long userID) {
		ActionPart actionPart = actionPartCrudService.find(ActionPart.class, actionPartObjective.getActionPart().getId());
		if(actionPart != null){
			actionPartObjective.setActionPart(actionPart);
			actionPartObjective = actionPartObjectiveCrudService.create(actionPartObjective);
			return actionPartObjective;
		}
		return null;
	}
	
	// -------------------------------------------------------------------------

	@Override
	public ActionPartObjectiveDTO updateActionPartObjectiveDTO( ActionPartObjectiveDTO actionPartObjectiveDTO, Long userID) {
		ActionPartObjective actionPartObjective = (ActionPartObjective) mappingService.mapESponderEntityDTO(actionPartObjectiveDTO, ActionPartObjective.class);
		actionPartObjective = updateActionPartObjective(actionPartObjective, userID);
		actionPartObjectiveDTO = (ActionPartObjectiveDTO) mappingService.mapESponderEntity(actionPartObjective, ActionPartObjectiveDTO.class);
		return actionPartObjectiveDTO;
	}
	
	@Override
	public ActionPartObjective updateActionPartObjective(ActionPartObjective actionPartObjective, Long userID) {
		ActionPart actionPart = actionPartCrudService.find(ActionPart.class, actionPartObjective.getActionPart().getId());
		if(actionPart != null){
			actionPartObjective.setActionPart(actionPart);
			actionPartObjective = actionPartObjectiveCrudService.update(actionPartObjective);
			return actionPartObjective;
		}
		return null;
	}
	
	// -------------------------------------------------------------------------

	@Override
	public void deleteActionPartObjectiveDTO(Long actionPartObjectiveDTOId, Long userID) {
		deleteActionPartObjective(actionPartObjectiveDTOId, userID);
	}	

	@Override
	public void deleteActionPartObjective(Long actionPartObjectiveId, Long userID) {
		actionPartObjectiveCrudService.delete(ActionPartObjective.class, actionPartObjectiveId);
	}

}

