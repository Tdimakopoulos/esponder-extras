package eu.esponder.controller.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.dto.model.user.ESponderUserDTO;

public class UserServiceTest extends ControllerServiceTest {
	
	@Test(groups="createBasics")
	public void testCreateUsers() {
		ESponderUserDTO ctri = new ESponderUserDTO();
		ctri.setUserName("ctri");
		userService.createUserRemote(ctri, null);
		
		ESponderUserDTO gleo = new ESponderUserDTO();
		gleo.setUserName("gleo");
		userService.createUserRemote(gleo, null);
		
		ESponderUserDTO kotso = new ESponderUserDTO();
		kotso.setUserName("kotso");
		userService.createUserRemote(kotso, null);
	}
	
}
