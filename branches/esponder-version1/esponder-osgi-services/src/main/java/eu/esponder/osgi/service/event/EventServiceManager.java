package eu.esponder.osgi.service.event;

import javax.ejb.Local;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;

@Local
public interface EventServiceManager {

	public ESponderEventListener<? extends ESponderEvent<? extends ESponderEntityDTO>> getListener(Class<? extends ESponderEvent<? extends ESponderEntityDTO>> eventClass);

	public ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>> getPublisher(Class<? extends ESponderEvent<? extends ESponderEntityDTO>> eventClass);
	
}
