package eu.esponder.osgi.service.event;

import java.io.IOException;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.prosyst.mprm.backend.event.Event;
import com.prosyst.mprm.backend.event.EventListener;
import com.prosyst.mprm.backend.event.EventListenerException;
import com.prosyst.mprm.common.ManagementException;
import com.prosyst.mprm.data.DictionaryInfo;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.osgi.service.connection.ConnectionHandler;

public class ESponderEventListener<T extends ESponderEvent<? extends ESponderEntityDTO>> implements EventListener {

	private static ConnectionHandler connectionHandler = new ConnectionHandler();
	
	private static ObjectMapper mapper = initialiseMapper();
	
	private String eventTopic;
	
	private Class<T> eventTopicClass;
	
	public ESponderEventListener(Class<T> eventClass) {
		
		this.eventTopicClass = eventClass;
		this.setEventTopic(eventClass.getCanonicalName());
		
		try {
			if (connectionHandler.getRac() == null) {
				connectionHandler.connect();
			}
		}
		catch (ManagementException me) {
			me.printStackTrace();
		}
		this.subscribe();
	}
	
	public void subscribe() {
		try {
			/*
			 * Subsribe for events using the newly created subscriber
			 */
			connectionHandler.getRac().addEventListener(this.getEventTopic(), this);
		} catch (ManagementException me) {
			me.printStackTrace();
		}
	}

	@Override
	public void event(Event event) throws EventListenerException {
		
		System.out.println("Event: " + event.getEventData());

		/*
		 * Get the event data from the EventAdmin
		 */
		DictionaryInfo data = (DictionaryInfo) event.getEventData();
		System.out.println("Data # " + data.toString());

		String esponderEventData = (String) data.get(ConnectionHandler.EVENT_PROPERTY_NAME);
		System.out.println("Esponder Data # " + data.toString());

		parseEventData(esponderEventData);

		/*
		 * TODO: Invoke appropriate service of the EventHandler
		 */
		

	}
	
	private ESponderEvent<? extends ESponderEntityDTO> parseEventData(String eventData) {
		ESponderEvent<? extends ESponderEntityDTO> cssEvent = null;
		
		try {
			/*
			 * Parse the event data
			 */
			JsonFactory jsonFactory = new JsonFactory();
			JsonParser jp;
			
			jp = jsonFactory.createJsonParser(eventData);
			Class<? extends ESponderEvent<? extends ESponderEntityDTO>> clz = 
					(Class<? extends ESponderEvent<? extends ESponderEntityDTO>>) CreateActionSnapshotEvent.class;
			cssEvent = mapper.readValue(jp, clz);
			
			/*
			 * and just print them
			 */
			printEvent(cssEvent);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return cssEvent;
	}
	
	private void printEvent(ESponderEvent<? extends ESponderEntityDTO> event) {
		System.out.println("###########################################");
		System.out.println("CSSE # " + event.toString());
		System.out.println("CSSE attachment # " + event.getEventAttachment().toString());
		System.out.println("CSSE severity # " + event.getEventSeverity());
		System.out.println("CSSE source # " + event.getEventSource());
		System.out.println("CSSE timestamp# " + event.getEventTimestamp());

	}

	public String getEventTopic() {
		return eventTopic;
	}

	public void setEventTopic(String eventTopic) {
		this.eventTopic = eventTopic;
	}

	
	public static ObjectMapper initialiseMapper() {
		/*
		 * Create Jackson deserialization mapper
		 */
		ObjectMapper objMapper = new ObjectMapper();
		objMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
		objMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return objMapper;
	}

	public Class<T> getEventTopicClass() {
		return eventTopicClass;
	}

	public void setEventTopicClass(Class<T> eventTopicClass) {
		this.eventTopicClass = eventTopicClass;
	}

}
