package eu.esponder.df.rules.service;

import eu.esponder.model.snapshot.Snapshot;


public interface CreateSnapshotService<T extends Snapshot<Long>> extends CreateSnapshotRemoteService<T> {

}
