/**
 * DroolSettings
 * 
 * This Java class store all settings need by the project to access the drool repository
 * and identify the temporary local repository using a file path 
 *
 * @Project   esponder
 * @package   Datafusion
*/
package eu.esponder.df.ruleengine.settings;

/**
 * @author tdim
 *
 */
public class DroolSettings {

	// Variable to store the local repository (file path)
	String szTmpRepositoryDirectoryPath="c:\\tmprepo\\";
	// Drool server URL
	//String szDroolApplicationServerURL="http://localhost:8080/";
	String szDroolApplicationServerURL="http://localhost:8181/";
	//Drool deployment name
	//String szDroolWarDeploymentPath="guvnor-5.4.0.Final-tomcat-6.0";
	String szDroolWarDeploymentPath="guvnor";
	//Service entry point
	String szDroolServiceEntryPoint="rest";
	//Drool username
	String szDroolUsername="guest";
	//Drool password
	String szDroolPassword="";
	
	/**
	 * @return drool username (user account manage by drool web interface)
	 */
	public String getSzDroolUsername() {
		return szDroolUsername;
	}

	
	/**
	 * @param szDroolUsername
	 */
	public void setSzDroolUsername(String szDroolUsername) {
		this.szDroolUsername = szDroolUsername;
	}

	
	/**
	 * @return drool password
	 */
	public String getSzDroolPassword() {
		return szDroolPassword;
	}

	/**
	 * @param szDroolPassword
	 */
	public void setSzDroolPassword(String szDroolPassword) {
		this.szDroolPassword = szDroolPassword;
	}
	
	/**
	 * @return temporary local repository file path
	 */
	public String getSzTmpRepositoryDirectoryPath() {
		return szTmpRepositoryDirectoryPath;
	}
	
	/**
	 * @param szTmpRepositoryDirectoryPath
	 */
	public void setSzTmpRepositoryDirectoryPath(String szTmpRepositoryDirectoryPath) {
		this.szTmpRepositoryDirectoryPath = szTmpRepositoryDirectoryPath;
	}
	
	/**
	 * @return the url where drool and all sub components are install
	 */
	public String getSzDroolApplicationServerURL() {
		return szDroolApplicationServerURL;
	}
	
	/**
	 * @param szDroolApplicationServerURL
	 */
	public void setSzDroolApplicationServerURL(String szDroolApplicationServerURL) {
		this.szDroolApplicationServerURL = szDroolApplicationServerURL;
	}
	
	/**
	 * @return drool guvnor deployment path
	 */
	public String getSzDroolWarDeploymentPath() {
		return szDroolWarDeploymentPath;
	}
	
	/**
	 * @param szDroolWarDeploymentPath
	 */
	public void setSzDroolWarDeploymentPath(String szDroolWarDeploymentPath) {
		this.szDroolWarDeploymentPath = szDroolWarDeploymentPath;
	}
	
	/**
	 * @return service entry point ( default is rest )
	 */
	public String getSzDroolServiceEntryPoint() {
		return szDroolServiceEntryPoint;
	}

	/**
	 * @param szDroolServiceEntryPoint
	 */
	public void setSzDroolServiceEntryPoint(String szDroolServiceEntryPoint) {
		this.szDroolServiceEntryPoint = szDroolServiceEntryPoint;
	}
}
