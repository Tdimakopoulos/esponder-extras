package eu.esponder.df.manager.service;

import javax.ejb.Local;

@Local
public interface SensorSnapshotManagerService extends
		SensorSnapshotManagerRemoteService {

}
