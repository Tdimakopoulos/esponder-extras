/**
 * RuleEngineGuvnorAssets
 * 
 * This Java class implement a rule engine which can get the rules from the drools guvnor web repository.
 * What acctualy does is that get all rules from the repository , create a local repository and then execute 
 * the rules based on facts localy.
 * 
 * 
 * @Project   esponder
 * @package   Datafusion
 */
package eu.esponder.df.ruleengine.core;

import java.io.File;

import org.apache.cxf.jaxrs.client.WebClient;

import eu.esponder.df.ruleengine.settings.DroolSettings;

/**
 * @author tdim
 * 
 */
public class RuleEngineGuvnorAssets extends RuleEngine {

	DroolSettings dSettings = new DroolSettings();
	EngineKnowledgeLocalRepository dLocalRepository = new EngineKnowledgeLocalRepository();
	RuleEngineRulesAssetsHelper dRuleAssetsAccess = new RuleEngineRulesAssetsHelper();
	RuleEngineXMLHelper dXMLHelper = new RuleEngineXMLHelper();
	private boolean bSession = false;

	/**
	 * @param szPackage
	 *            the package name to get all rules
	 * @return the rules for package
	 */
	public String[] PopulateLocalRepositoryForPackage(String szPackage) {

		return ProcessXMLAssets(szPackage, GetAssetsXMLForPackage(szPackage));
	}

	/**
	 * @param szCategory
	 *            the category to get all rules
	 * @return the rules for category
	 */
	public String[] PopulateLocalRepositoryForCategory(String szCategory) {

		return ProcessXMLAssets(szCategory, GetAssetsXMLForCategory(szCategory));
	}

	/**
	 * @param szPackage
	 *            the package name to use the rules
	 * @throws Exception
	 */
	public void InferenceEngineAddKnowledgeForPackage(String szPackage)
			throws Exception {
		AddRules(PopulateLocalRepositoryForPackage(szPackage));
	}

	/**
	 * @param szCategory
	 *            the category name to use the rules
	 * @throws Exception
	 */
	public void InferenceEngineAddKnowledgeForCategory(String szCategory)
			throws Exception {
		AddRules(PopulateLocalRepositoryForCategory(szCategory));
	}

	/**
	 * @param Rules
	 *            the array of rules
	 * @throws Exception
	 */
	public void InferenceEngineAddKnowledge(String[] Rules) throws Exception {
		AddRules(Rules);
	}

	private void InferenceEngineAddObjectInitilizeSessions() {
		if (bSession == false) {
			CreateSession();
			bSession = true;
		}
	}

	public void InferenceEngineAddObject(Object fact) {
		InferenceEngineAddObjectInitilizeSessions();
		AddObject(fact);
	}

	public void InferenceEngineAddDTOObject(Object fact) {
		InferenceEngineAddObjectInitilizeSessions();
		AddObject(fact);
	}

	/**
	 * @param facts
	 *            the facts to be added in rule engine session
	 */
	public void InferenceEngineAddObjects(Object[] facts) {
		CreateSession();
		AddObjects(facts);
	}

	/**
	 * rule all rules
	 */
	public void InferenceEngineRunAssets() {
		try {
			RunRules();
		} catch (Exception e) {
			System.out.println("Problem with rule execution");
			e.printStackTrace();
		}
		CloseSession();
	}

	/**
	 * Empty local repository
	 */
	public void EmptyRepository() {
		DeleteDirectory(dSettings.getSzTmpRepositoryDirectoryPath());
	}

	/**
	 * @param directoryName
	 *            the directory to delete all files inside
	 */
	private void DeleteDirectory(String directoryName) {
		File directory = new File(directoryName);
		// Get all files in directory
		File[] files = directory.listFiles();
		for (File file : files) {
			// Delete each file
			if (!file.delete()) {
				// Failed to delete file
				System.out.println("Failed to delete " + file);
			}
		}
	}

	/**
	 * @param szPackage
	 *            the package name
	 * @param szXMLAssets
	 *            the assets string
	 * @return the rules in file system
	 */
	private String[] ProcessXMLAssets(String szPackage, String szXMLAssets) {

		String szURL = null;
		String szContent = null;

		dRuleAssetsAccess.SetRulesAssets(dXMLHelper.ProcessAssets(szXMLAssets));
		for (int i = 0; i < dRuleAssetsAccess.GetRulesNumber(); i++) {
			szURL = dRuleAssetsAccess.GetRuleSourcePoint(i);
			szContent = GetAssetsTextForURL(szURL);
			dLocalRepository.AddNewRuleOnLocalRepository(szURL,
					dSettings.getSzTmpRepositoryDirectoryPath(), szContent);
		}
		return dLocalRepository.ReturnRules();
	}

	/**
	 * @param szUser
	 *            username
	 * @param szPassword
	 *            password
	 * @return session string
	 */
	private String CreateAuthorizationHeader(String szUser, String szPassword) {
		String szAuthorization = szUser + ":" + szPassword;
		String szAuthorizationHeader = "Basic "
				+ org.apache.cxf.common.util.Base64Utility
						.encode(szAuthorization.getBytes());
		return szAuthorizationHeader;
		//return null;
	}

	/**
	 * @param szPackage
	 *            the package name
	 * @return the xml assets
	 */
	public String GetAssetsXMLForPackage(String szPackage) {
		WebClient client = WebClient.create(dSettings
				.getSzDroolApplicationServerURL());
		client.header(
				"Authorization",
				CreateAuthorizationHeader(dSettings.getSzDroolUsername(),
						dSettings.getSzDroolPassword()));
		String content = client
				.path(dSettings.getSzDroolWarDeploymentPath() + "/"
						+ dSettings.getSzDroolServiceEntryPoint()
						+ "/packages/" + szPackage + "/assets")
				.accept("application/xml").get(String.class);
		return content;
		//return null;
	}

	/**
	 * @param szCategory
	 *            the category name
	 * @return the xml assets
	 */
	public String GetAssetsXMLForCategory(String szCategory) {
		WebClient client = WebClient.create(dSettings
				.getSzDroolApplicationServerURL());
		client.header(
				"Authorization",
				CreateAuthorizationHeader(dSettings.getSzDroolUsername(),
						dSettings.getSzDroolPassword()));
		String content = client
				.path(dSettings.getSzDroolWarDeploymentPath() + "/"
						+ dSettings.getSzDroolServiceEntryPoint() + "/"
						+ "categories/" + szCategory + "/assets")
				.accept("application/xml").get(String.class);
		// System.out.println("------> "+content);
		return content;
//		return null;
	}

	/**
	 * @param szURL
	 *            asset source url
	 * @return the source code of asset
	 */
	public String GetAssetsTextForURL(String szURL) {
		String szn = szURL.substring(dSettings.getSzDroolApplicationServerURL()
				.length(), szURL.length());
		WebClient client = WebClient.create(dSettings
				.getSzDroolApplicationServerURL());
		client.header(
				"Authorization",
				CreateAuthorizationHeader(dSettings.getSzDroolUsername(),
						dSettings.getSzDroolPassword()));
		String content = client.path(szn).accept("text/plain")
				.get(String.class);
		return content;
		//return null;
	}
}
