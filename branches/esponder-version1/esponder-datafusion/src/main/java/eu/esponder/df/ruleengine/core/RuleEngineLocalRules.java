/**
 * RuleEngineLocalRules
 * 
 * This Java class implement a rule engine for local rule repository. This class is implements so the Rule engine 
 * base class will never be called. 
 * 
 * First we add the rule : InferenceEngineAddKnowledge
 * Second we add the facts : InferenceEngineAddObjects
 * Third we execute the rules : InferenceEngineRunAssets 
 * 
 * 
 * @Project   esponder
 * @package   Datafusion
*/
package eu.esponder.df.ruleengine.core;

/**
 * @author tdim
 *
 */
public class RuleEngineLocalRules extends RuleEngine{
	private boolean bSession=false;
	/**
	 * @param Rules the rules in local repository
	 * @throws Exception
	 */
	public void InferenceEngineAddKnowledge(String[] Rules) throws Exception
	{
		AddRules(Rules);
	}
	
	/**
	 * @param facts the facts to used by rules
	 */
	public void InferenceEngineAddObjects(Object[] facts)
	{
		CreateSession();
		AddObjects(facts);
	}
	
	private void InferenceEngineAddObjectInitilizeSessions()
	{
		if (bSession==false)
		{
			CreateSession();
			bSession=true;
		}
	}
	
	public void InferenceEngineAddObject(Object fact)
	{
		InferenceEngineAddObjectInitilizeSessions();
		AddObject(fact);
	}
	/**
	 * run all rules using the facts provided
	 */
	public void InferenceEngineRunAssets()
	{
		try {
			RunRules();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Problem with rule execution");
			e.printStackTrace();
		}
		CloseSession();
	}
}
