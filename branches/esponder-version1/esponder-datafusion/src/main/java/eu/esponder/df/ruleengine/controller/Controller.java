/**
 * Controller
 * 
 * This Java class implement a controller for the rules engine, this is the class that must be called to run 
 * the rule engine, support DRL, DSL and DSLR type of assets. And Functions but need to be inline the rule.
 * 
 * 
 * @Project   esponder
 * @package   Datafusion
 */
package eu.esponder.df.ruleengine.controller;

import java.sql.Date;

import eu.esponder.df.ruleengine.core.DomainSpecificKnowledgeParser;
import eu.esponder.df.ruleengine.core.RuleEngineGuvnorAssets;
import eu.esponder.df.ruleengine.utilities.RuleUtilities;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;

public class Controller {

	private RuleEngineGuvnorAssets dEngine = new RuleEngineGuvnorAssets();
	private DomainSpecificKnowledgeParser ddsl = new DomainSpecificKnowledgeParser();

	// Main Function Block
	// -- New Type of event arrives
	// -- -- Check Event type
	// -- -- -- If Sensor
	// -- -- -- -- Load rules for category sensor
	// -- -- -- -- Add DTO object
	// -- -- -- -- Add Utility object
	// -- -- -- -- Run Rules
	// -- -- -- -- -- Execute Rules function (drl -> dslr -> rf)

	public void AddDTOObjects(Object fact) {
		dEngine.InferenceEngineAddDTOObject(fact);
		ddsl.AddDTOObject(fact);
	}

	public void AddObjects(Object fact) {
		dEngine.InferenceEngineAddObject(fact);
		ddsl.AddObject(fact);
	}

	public void AddKnowledgeByCategory(String szCategory) {
		try {
			dEngine.InferenceEngineAddKnowledgeForCategory(szCategory);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Category");
		}
		ddsl.AddKnowledgeForCategory(szCategory);
	}

	public void AddKnowledgeByPackage(String szPackage) {
		try {
			dEngine.InferenceEngineAddKnowledgeForPackage(szPackage);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Package");
		}
		ddsl.AddKnowledgeForPackage(szPackage);
	}

	public void ExecuteRules() {
		// run simple drl rules
		dEngine.InferenceEngineRunAssets();

		// run all processes
		ddsl.ExecuteFlow();

		// run dslr rules based on dsl and
		ddsl.RunRules();

		// close session only if all processes has finished
		ddsl.Dispose();
	}

	/**
	 * @param args
	 *            Remove only for testing the controller
	 */
	public static void main(String[] args) {

		Date date = new Date(1000);
		RuleUtilities dutils = new RuleUtilities();
		PeriodDTO period = new PeriodDTO(date, date);
		Controller dEngine = new Controller();
		SensorDTO sensor = null;

		sensor = new BodyTemperatureSensorDTO();

		sensor.setType("title");
		sensor.setTitle("title");
		sensor.setStatus(ResourceStatusDTO.ALLOCATED);
		sensor.setEquipmentId((long) 1);

		SensorSnapshotDTO snapshot = new SensorSnapshotDTO();
		snapshot.setSensor(sensor);
		snapshot.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
		snapshot.setValue("35");
		snapshot.setPeriod(period);
		snapshot.setStatus(SensorSnapshotStatusDTO.WORKING);

		dEngine.AddKnowledgeByPackage("esponder");

		dEngine.AddDTOObjects(dutils);
		dEngine.AddDTOObjects(snapshot);

		dEngine.ExecuteRules();

	}

}
