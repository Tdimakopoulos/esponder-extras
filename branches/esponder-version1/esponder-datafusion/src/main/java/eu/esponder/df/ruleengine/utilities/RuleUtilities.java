package eu.esponder.df.ruleengine.utilities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class RuleUtilities {

	public BigDecimal ConvertStringToBD(String szvalue)
	{
		return new BigDecimal(szvalue);		
	}
	
	public int ConvertStringToint(String szvalue)
	{
		return Integer.valueOf(szvalue);		
	}
	
	/**
	  * This String utility or util method can be used to merge 2 arrays of
	  * string values. If the input arrays are like this array1 = {"a", "b" ,
	  * "c"} array2 = {"c", "d", "e"} Then the output array will have {"a", "b" ,
	  * "c", "d", "e"}
	  * 
	  * This takes care of eliminating duplicates and checks null values.
	  * 
	  * @param values
	  * @return
	  */
	 @SuppressWarnings({ "unchecked", "rawtypes" })
	public  String[] mergeStringArrays(String array1[], String array2[]) {
	 
	  if (array1 == null || array1.length == 0)
	   return array2;
	  if (array2 == null || array2.length == 0)
	   return array1;
	  List array1List = Arrays.asList(array1);
	  List array2List = Arrays.asList(array2);
	  List result = new ArrayList(array1List);  
	  List tmp = new ArrayList(array1List);
	  tmp.retainAll(array2List);
	  result.removeAll(tmp);
	  result.addAll(array2List);  
	  return ((String[]) result.toArray(new String[result.size()]));
	 }
	 
	 /**
	  * This String utility or util method can be used to trim all the String
	  * values in list of Strings. For input [" a1 ", "b1 ", " c1"] the output
	  * will be {"a1", "b1", "c1"} Method takes care of null values. This method
	  * is collections equivalent of the trim method for String array.
	  *
	  * @param values
	  * @return
	  */
	 @SuppressWarnings({ "rawtypes", "unchecked" })
	public  List trim(final List values) {

	  List newValues = new ArrayList();
	  for (int i = 0, length = values.size(); i < length; i++) {
	   String v = (String) values.get(i);
	   if (v != null) {
	    v = v.trim();
	   }
	   newValues.add(v);
	  }
	  return newValues;
	 }
	 
	 /**
	  * This String utility or util method can be used to
	  * trim all the String values in the string array.
	  * For input {" a1 ", "b1 ", " c1"}
	  * the output will be {"a1", "b1", "c1"}
	  * Method takes care of null values.
	  * @param values
	  * @return
	  */
	 public  String[] trim(final String[] values) {

	  for (int i = 0, length = values.length; i < length; i++) {
	   if (values[i] != null) {
	    values[i] = values[i].trim();                                
	   }
	  }
	  return values;

	 }
	 
	 
	 /**
	  * This String util method removes single or double quotes
	  * from a string if its quoted.
	  * for input string = "mystr1" output will be = mystr1
	  * for input string = 'mystr2' output will be = mystr2
	  *
	  * @param String value to be unquoted.
	  * @return value unquoted, null if input is null.
	  *
	  */
	 public  String unquote(String s) {

	  if (s != null
	    && ((s.startsWith("\"") && s.endsWith("\""))
	    || (s.startsWith("'") && s.endsWith("'")))) {

	   s = s.substring(1, s.length() - 1);
	  }
	  return s;
	 }
	 /**
	  * Same method as above but using the ?: syntax which is shorter. You can use whichever you prefer.
	  * This String util method removes single or double quotes from a string if
	  * its quoted. for input string = "mystr1" output will be = mystr1 for input
	  *  
	  * string = 'mystr2' output will be = mystr2
	  *
	  * @param String
	  *            value to be unquoted.
	  * @return value unquoted, null if input is null.
	  *
	  */
	 public  String unquote2(String s) {

	  return (s != null && ((s.startsWith("\"") && s.endsWith("\"")) || (s
	    .startsWith("'") && s.endsWith("'")))) ? s = s.substring(1, s
	    .length() - 1) : s;

	 } 

	 /**
	 * This method is used to split the given string into different tokens at
	 * the occurrence of specified delimiter
	 * An example :
	 * <code>"abcdzefghizlmnop"</code> and using a delimiter <code>"z"</code>
	 * would give following output
	 * <code>"abcd" "efghi" "lmnop"</code>
	 *
	 * @param str The string that needs to be broken
	 * @param delimeter The delimiter used to break the string
	 * @return a string array
	 */

	 public  String[] getTokensArray(String str, String delimeter) {

	 String[] data;
	 if(str == null){
	     return null;
	 }

	 if (delimeter == null || "".equals(delimeter)
	 || "".equals(str)) {
	     data = new String[1];
	     data[0] = str;
	     return data;
	 } else {
	     StringTokenizer st = new StringTokenizer(str, delimeter);
	     int tokenCount = st.countTokens();
	     data = new String[tokenCount];
	     for (int i = 0; st.hasMoreTokens(); i++) {
	     data[i] = st.nextToken();
	 }
	 return data;
	 }
	 }

	 
	 /**
	 * Check a String ends with another string ignoring the case.
	 *
	 * @param str
	 * @param suffix
	 * @return
	 */
	 public  boolean endsWithIgnoreCase(String str, String suffix) {

	     if (str == null || suffix == null) {
	         return false;
	     }
	     if (str.endsWith(suffix)) {
	         return true;
	     }
	     if (str.length() < suffix.length()) {
	         return false;
	     } else {
	         return str.toLowerCase().endsWith(suffix.toLowerCase());
	     }
	 }

	 /**
	 * Check a String starts with another string ignoring the case.
	 *
	 * @param str
	 * @param prefix
	 * @return
	 */

	 public  boolean startsWithIgnoreCase(String str, String prefix) {

	     if (str == null || prefix == null) {
	         return false;
	     }
	     if (str.startsWith(prefix)) {
	         return true;
	     }
	     if (str.length() < prefix.length()) {
	         return false;
	     } else {
	         return str.toLowerCase().startsWith(prefix.toLowerCase());
	     }
	 }
	 
	
	 
	 /**
	 * Return a not null string.
	 *
	 * @param s String
	 * @return empty string if it is null otherwise the string passed in as
	 * parameter.
	 */

	 public  String nonNull(String s) {

	 if (s == null) {
	     return "";
	 }
	 return s;
	 }
	 

}
