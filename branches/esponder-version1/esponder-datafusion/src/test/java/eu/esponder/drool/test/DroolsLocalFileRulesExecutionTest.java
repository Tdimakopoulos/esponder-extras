package eu.esponder.drool.test;

/**
 * @author tdim
 *
 */
import org.testng.annotations.Test;
import eu.esponder.df.ruleengine.core.*;
/*
 * The test rule Sensor-test.drl needs to have the following contents
 * 
 * rule "Rule 01"   
 * when
 *	eval( 1==1 )
 * then
 *	System.out.println( "Rule 01 Works" );
 * end
 * 
 * Make sure the path is the same as define in the DroolSettings.java or in the Database
 */
public class DroolsLocalFileRulesExecutionTest {

	@Test
	public void ExecuteLocalFileTest() throws Exception
	{
		System.out.println("==============================================");
		System.out.println("      Local Rule Execution Test               ");
		
		RuleEngineLocalRules dEngine= new RuleEngineLocalRules();
		
		//full path of rule. 
		dEngine.InferenceEngineAddKnowledge(new String[]{"c:\\tmprepo\\Sensor-test.drl"});
		dEngine.InferenceEngineAddObjects(new Object[0]);
		dEngine.InferenceEngineRunAssets();
		
		System.out.println("==============================================");
	}
}
