package eu.esponder.fru.data;

import java.util.Calendar;
import java.util.Random;

import eu.esponder.fru.Main;
import eu.esponder.fru.measurement.CarbonDioxideMeasurement;
import eu.esponder.fru.measurement.TemperatureMeasurement;

public class DataSender extends Thread {

	public synchronized void run() {

		while (true) {
			try {
				Thread.sleep(2000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Random rand = new Random();
			TemperatureMeasurement temp1 = new TemperatureMeasurement(rand.nextInt(80 - 60 + 1) + 60);
			TemperatureMeasurement temp2 = new TemperatureMeasurement(rand.nextInt(80 - 60 + 1) + 60);
			TemperatureMeasurement temp3 = new TemperatureMeasurement(rand.nextInt(80 - 60 + 1) + 60);

			CarbonDioxideMeasurement ca_dio1 = new CarbonDioxideMeasurement(rand.nextInt(500 - 300 + 1) + 300);
			CarbonDioxideMeasurement ca_dio2 = new CarbonDioxideMeasurement(rand.nextInt(500 - 300 + 1) + 300);
			CarbonDioxideMeasurement ca_dio3 = new CarbonDioxideMeasurement(rand.nextInt(500 - 300 + 1) + 300);

			Main.envelope.add(temp1);
			Main.envelope.add(temp2);
			Main.envelope.add(ca_dio1);
			Main.envelope.add(ca_dio2);
			Main.envelope.add(ca_dio3);
			Main.envelope.add(temp3);
			
			Calendar cal = Calendar.getInstance();

			int minute = cal.get(Calendar.MINUTE);
			int second = cal.get(Calendar.SECOND);

			System.out.println("Steilame!! stis = " + minute + ":" + second);

		}

	}
}
