package eu.esponder.util.logger;

import org.apache.log4j.Logger;

public class ESponderLogger {
	
	public static Logger logger;
	static final String LOG_PROPERTIES_FILE = "src/main/resources/log4J.properties";
	
//	public ESponderLogger(Class<?> loggerClass, Level level) {
//		
//		ESponderLogger.logger = Logger.getLogger(loggerClass);
//		ESponderLogger.logger.setLevel(level);
//		ESponderLogger.logger.info("Logger initialized");
//	}
	
	public static Logger getInstance(Class<?> clz) {
		return Logger.getLogger(clz);
	}
	
	public static void debug(Class<?> clz, String message) {
		Logger thisLogger = Logger.getLogger(clz);
		thisLogger.debug(message);
	}
	
	public static void info(Class<?> clz, String message) {
		Logger thisLogger = Logger.getLogger(clz);
		thisLogger.info(message);
	}
	
	public static void warn(Class<?> clz, String message) {
		Logger thisLogger = Logger.getLogger(clz);
		thisLogger.warn(message);
	}
	
	public static void trace(Class<?> clz, String message) {
		Logger thisLogger = Logger.getLogger(clz);
		thisLogger.trace(message);
	}
	
	public static void error(Class<?> clz, String message) {
		Logger thisLogger = Logger.getLogger(clz);
		thisLogger.error(message);
	}
	
	public static void fatal(Class<?> clz, String message) {
		Logger thisLogger = Logger.getLogger(clz);
		thisLogger.fatal(message);
	}
		
	/*private static void initializeLogger() {
	
	Properties logProperties = new Properties();
    try
    {
      // load our log4j properties / configuration file
      logProperties.load(new FileInputStream(LOG_PROPERTIES_FILE));
      PropertyConfigurator.configure(logProperties);
    }
    catch(IOException e)
    {
      throw new RuntimeException("Unable to load logging property " + LOG_PROPERTIES_FILE);
    }		
	
}*/
	
}