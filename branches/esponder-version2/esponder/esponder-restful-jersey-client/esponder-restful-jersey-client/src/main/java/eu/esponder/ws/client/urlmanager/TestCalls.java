/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.ws.client.urlmanager;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OperationsCentreCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PlannableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.crisis.resource.plan.PlannableResourcePowerDTO;
import eu.esponder.ws.client.logic.CrisisTypeQueryManager;
import eu.esponder.ws.client.query.QueryManager;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Tom
 */
public class TestCalls {

    public static void main(String args[]) throws JsonParseException,
            JsonMappingException, IOException {


        QueryManager pMan = new QueryManager();
//		 System.out.println(pMan.getResourcePlansAll("1").getResultList().size());


        ResultListDTO pre = pMan.getCrisisResourcePlanAll("1");
        ObjectMapper mapper = new ObjectMapper();
        for (int i = 0; i < pre.getResultList().size(); i++) {
            CrisisResourcePlanDTO pItem = (CrisisResourcePlanDTO) pre.getResultList().get(i);
            Set<PlannableResourcePowerDTO> pStorage = pItem.getPlannableResources();
            System.out.println("plan title " + pItem.getTitle());
            //System.out.println(pItem.getCrisisType().getTitle());
            Object[] arrayView = pStorage.toArray();
            for (int d = 0; d < arrayView.length; d++) {
                PlannableResourcePowerDTO pitem = (PlannableResourcePowerDTO) arrayView[d];
                System.out.println("item power " + pitem.getPower());
                System.out.println("item constraint " + pitem.getConstraint());
                //System.out.println(pitem.getPlanableResourceCategoryId());
                PlannableResourceCategoryDTO pCategory = pMan.getresourceCategory("1", pitem.getPlanableResourceCategoryId().toString());
                if (pCategory instanceof PersonnelCategoryDTO) {
                    PersonnelCategoryDTO pperson = (PersonnelCategoryDTO) pCategory;
                    //System.out.println(pperson.getRank().getTitle());
                    //System.out.println(pperson.getOrganisationCategory().getOrganisationType().getTitle());
                    System.out.println("personnel type " + pperson.getOrganisationCategory().getDisciplineType().getTitle());

                }
                if (pCategory instanceof OperationsCentreCategoryDTO) {
                    OperationsCentreCategoryDTO popecenter = (OperationsCentreCategoryDTO) pCategory;
                    System.out.println("opcen type " + popecenter.getOperationsCentreType().getTitle());
                }

                if (pCategory instanceof ConsumableResourceCategoryDTO) {
                    ConsumableResourceCategoryDTO pcons = (ConsumableResourceCategoryDTO) pCategory;
                    System.out.println("cons type " + pcons.getConsumableResourceType().getTitle());
                }

                if (pCategory instanceof ReusableResourceCategoryDTO) {
                    ReusableResourceCategoryDTO pres = (ReusableResourceCategoryDTO) pCategory;
                    System.out.println("res type " + pres.getReusableResourceType().getTitle());
                }
            }

        }
        //CrisisResourcePlanDTO pp= (CrisisResourcePlanDTO) pre.getResultList().get(0);
        //EquipmentDTO pFind=pMan.getEquipment("1",new Long(10));
//		 System.out.println(pFind.getTitle());
        //
        // ActorDTO pafind=pMan.getActorID("1",
        // pFind.getActor().getId().toString());
        // System.out.println(pafind.getTitle());
        // System.out.println(pafind.getType());
        // SensorSnapshotFullDetails pobject = new SensorSnapshotFullDetails();
        // pobject.LoadSensorsSnapshots("1");
        // +System.out.println(pobject.getpSensorsSnapshots().size());
        // ResultListDTO pre=pMan.getAllTypes("1");
        // System.out.println(pre.getResultList().size());
        // for (int i=0;i<pre.getResultList().size();i++)
        // {
        // if (pre.getResultList().get(i) instanceof CrisisDisasterTypeDTO )
        // {
        // System.out.println(pre.getResultList().get(i).getClass());
        // }
        // }
//		 CrisisTypeQueryManager pct = new CrisisTypeQueryManager();
//		 pct.LoadAllCrisisTypes();
//                 for (int i=0;i<pct.getpCrisisDisasterTypes().size();i++)
//		 System.out.println(pct.getpCrisisDisasterTypes().get(i).getTitle());
//                 for (int i=0;i<pct.getpCrisisFeatureTypes().size();i++)
//		 System.out.println(pct.getpCrisisFeatureTypes().get(i).getTitle());
        // System.out.println(pMan.getEventsBySeverity("1",
        // "UNDEFINED").getResultList().size());
        // System.out.println(pMan.getEventsByid("1", "1").getAttachment());
    }
}
