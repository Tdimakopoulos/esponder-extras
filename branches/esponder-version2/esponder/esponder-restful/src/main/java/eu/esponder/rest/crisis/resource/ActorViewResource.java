/*
 * 
 */
package eu.esponder.rest.crisis.resource;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class ActorViewResource.
 */
@Path("/crisis/resource")
public class ActorViewResource extends ESponderResource {

	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}

	//FindByID
	/**
	 * Gets the actor.
	 *
	 * @param actorID the actor id
	 * @param pkiKey the pki key
	 * @return the actor
	 */
	@GET
	@Path("/actor/getID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO getActor( @QueryParam("actorID") @NotNull(message="actorID may not be null" ) Long actorID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return this.getActorRemoteService().findByIdRemote(actorID);
	}

	//FindByTitle
	/**
	 * Gets the actor.
	 *
	 * @param actorTitle the actor title
	 * @param pkiKey the pki key
	 * @return the actor
	 */
	@GET
	@Path("/actor/getTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO getActor( @QueryParam("actorTitle") @NotNull(message="actorTitle may not be null" ) String actorTitle,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return this.getActorRemoteService().findByTitleRemote(actorTitle);

	}

	/**
	 * Gets the all actors.
	 *
	 * @param pkiKey the pki key
	 * @return the all actors
	 */
	@GET
	@Path("/actor/getAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getAllActors(@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return new ResultListDTO(this.getActorRemoteService().findAllActorsRemote());
	}

	// Find Subordinates By ActorID
	/**
	 * Gets the actor subordinates.
	 *
	 * @param actorID the actor id
	 * @param pkiKey the pki key
	 * @return the actor subordinates
	 */
	@GET
	@Path("/actor/getSubordinates")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getActorSubordinates(
			@QueryParam("actorID") @NotNull(message="actorID may not be null" ) Long actorID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		ResultListDTO resultList = new ResultListDTO(this.getActorRemoteService().findSubordinatesByIdRemote(actorID));
		return resultList;
	}

	// create Actor
	/**
	 * Creates the actor.
	 *
	 * @param actorDTO the actor dto
	 * @param pkiKey the pki key
	 * @return the actor dto
	 */
	@POST
	@Path("/actor/post")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO createActor(@NotNull(message="actorDTO may not be null" ) ActorDTO actorDTO,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return this.getActorRemoteService().createActorRemote(actorDTO, userID);
	}


	// Update Actor
	/**
	 * Update actor.
	 *
	 * @param actorDTO the actor dto
	 * @param pkiKey the pki key
	 * @return the actor dto
	 */
	@PUT
	@Path("/actor/put")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO updateActor(@NotNull(message="actorDTO may not be null" ) ActorDTO actorDTO,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return this.getActorRemoteService().updateActorRemote(actorDTO, userID);
	}

	// Delete Actor
	/**
	 * Delete actor.
	 *
	 * @param actorID the actor id
	 * @param pkiKey the pki key
	 * @return the long
	 */
	@DELETE
	@Path("/actor/delete")
	public Long deleteActor(@QueryParam("actorID") @NotNull(message="actorID may not be null" ) Long actorID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		this.getActorRemoteService().deleteActorRemote(actorID, userID);
		return actorID;
	}

}
