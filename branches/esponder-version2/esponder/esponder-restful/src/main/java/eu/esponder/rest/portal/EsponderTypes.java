/*
 * 
 */
package eu.esponder.rest.portal;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class EsponderTypes.
 */
@Path("/portal/types")
public class EsponderTypes extends ESponderResource {

	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}

	/**
	 * Read all personnel.
	 *
	 * @param pkiKey the pki key
	 * @return the result list dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@GET
	@Path("/findAll")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO readAllTypes(
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws ClassNotFoundException {

		Long userID=SecurityCheck(pkiKey);

		List<ESponderTypeDTO> pResults = this.getTypeRemoteService().findDTOAllTypes();
		return new ResultListDTO(pResults);

	}

}