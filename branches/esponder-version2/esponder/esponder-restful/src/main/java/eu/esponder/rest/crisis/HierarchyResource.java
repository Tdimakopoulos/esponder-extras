/*
 * 
 */
package eu.esponder.rest.crisis;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class HierarchyResource.
 */
@Path("/crisis/context")
public class HierarchyResource  extends ESponderResource {

	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}

	/**
	 * Gets the meocs for crisis.
	 *
	 * @param crisisContextID the crisis context id
	 * @param pkiKey the pki key
	 * @return the operations centre dto
	 */
	@GET
	@Path("/Hierarchy/GetMeocsForCrisis")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO GetMeocsForCrisis(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextID may not be null") Long crisisContextID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {
		
		Long userID = SecurityCheck(pkiKey);
		
		ResultListDTO resultList = new ResultListDTO();
		resultList.setResultList(this.getOperationsCentreRemoteService().findMeocsByCrisisRemote(crisisContextID));
		return resultList;
	}

	/**
	 * Gets the eoc for crisis.
	 *
	 * @param crisisContextID the crisis context id
	 * @param pkiKey the pki key
	 * @return the operations centre dto
	 */
	@GET
	@Path("/Hierarchy/GetEocForCrisis")
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreDTO GetEocForCrisis(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextID may not be null") Long crisisContextID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);
		return this.getOperationsCentreRemoteService().findEocByCrisisRemote(crisisContextID);
	}

	/**
	 * Gets the supervisors for meoc.
	 *
	 * @param meocID the meoc id
	 * @param pkiKey the pki key
	 * @return the actor dto
	 */
	@GET
	@Path("/Hierarchy/GetSupervisorForMeoc")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO GetSupervisorsForMeoc(
			@QueryParam("meocID") @NotNull(message="meocID may not be null") Long meocID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {
		
		Long userID=SecurityCheck(pkiKey);
		return this.getOperationsCentreRemoteService().findMeocSupervisorRemote(meocID);
	}

	/**
	 * Gets the supervisors for eoc.
	 *
	 * @param eocID the eoc id
	 * @param pkiKey the pki key
	 * @return the actor dto
	 */
	@GET
	@Path("/Hierarchy/GetSupervisorForEoc")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO GetSupervisorsForEoc(
			@QueryParam("eocID") @NotNull(message="eocID may not be null") Long eocID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);
		return this.getOperationsCentreRemoteService().findEocSupervisorRemote(eocID);
	}

	/**
	 * Gets the supervisors for actor.
	 *
	 * @param actorID the actor id
	 * @param pkiKey the pki key
	 * @return the actor dto
	 */
	@GET
	@Path("/Hierarchy/GetSupervisorForActor")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO GetSupervisorsForActor(
			@QueryParam("actorID") @NotNull(message="actorID may not be null") Long actorID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey)	{

		Long userID=SecurityCheck(pkiKey);
		return this.getActorRemoteService().findSupervisorForActorRemote(actorID);
	}

}