/*
 * 
 */
package eu.esponder.rest.datafusion;


import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;

// TODO: Auto-generated Javadoc
/**
 * The Class RuleResults.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({ "ruleresultList" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class RuleResults implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -363245325332031836L;

	/** The result list. */
	private List<RuleResultsXML> resultList;

	/**
	 * Instantiates a new rule results.
	 */
	public RuleResults() {
	}

	/**
	 * Instantiates a new rule results.
	 *
	 * @param resultsList the results list
	 */
	public RuleResults(List<RuleResultsXML> resultsList) {
		this.resultList = resultsList;
	}

	/**
	 * Gets the result list.
	 *
	 * @return the result list
	 */
	public List<RuleResultsXML> getResultList() {
		return resultList;
	}

	/**
	 * Sets the result list.
	 *
	 * @param resultList the new result list
	 */
	public void setResultList(List<RuleResultsXML> resultList) {
		this.resultList = resultList;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String result = "[ResultsList:";
		for (RuleResultsXML entity : this.getResultList()) {
			result += entity.toString();
		}
		result += "]";
		return result;
	}

}