/*
 * 
 */
package eu.esponder.rest.crisis;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.collections.CollectionUtils;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.rest.ESponderResource;
import eu.esponder.util.rest.StringParamUnmarshaller.DateFormat;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisSnapshotResource.
 */
@Path("/crisis/snapshot")
public class CrisisSnapshotResource extends ESponderResource {

	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}

	/**
	 * Gets the snapshot by date.
	 *
	 * @param operationsCentreID the operations centre id
	 * @param maxDate the max date
	 * @param pkiKey the pki key
	 * @return the snapshot by date
	 */
	@GET()
	@Path("/oc")
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreDTO getSnapshotByDate(
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null")  Long operationsCentreID, 
			@QueryParam("maxDate") @DateFormat("yyyy-MM-dd'T'HH:mm:ss") @NotNull(message="maxDate may not be null") Date maxDate, 
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		//		OperationsCentreDTO operationsCentre = this.getMappingService().mapOperationsCentre(operationsCentreID, userID);

		OperationsCentreDTO operationsCentreDTO =  this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);

		if (null != operationsCentreDTO) {
			setOperationsCentreSnapshot(operationsCentreDTO, maxDate);
			return operationsCentreDTO;
		}
		return null;
	}

	/**
	 * Gets the sensor snapshot by date.
	 *
	 * @param sensorID the sensor id
	 * @param maxDate the max date
	 * @param pkiKey the pki key
	 * @return the sensor snapshot by date
	 */
	@GET()
	@Path("/sensor")
	@Produces({MediaType.APPLICATION_JSON})
	public SensorSnapshotDTO getSensorSnapshotByDate(
			@QueryParam("sensorID") @NotNull(message="operationsCentreID may not be null")  Long sensorID, 
			@QueryParam("maxDate") @DateFormat("yyyy-MM-dd'T'HH:mm:ss") @NotNull(message="maxDate may not be null") Date maxDate, 
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return getSensorSnapshot(sensorID, maxDate);
	}

	/**
	 * Gets the all sensor snapshot.
	 *
	 * @param pkiKey the pki key
	 * @return the all sensor snapshot
	 */
	@GET()
	@Path("/sensorsnapshotfindall")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getAllSensorSnapshot(
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return new ResultListDTO(this.getSensorRemoteService().findAllSensorSnapshotsRemote());
	}


	/**
	 * Sets the operations centre snapshot.
	 *
	 * @param operationsCentre the operations centre
	 * @param maxDate the max date
	 */
	private void setOperationsCentreSnapshot(OperationsCentreDTO operationsCentre, Date maxDate) {

		if (CollectionUtils.isNotEmpty(operationsCentre.getSubordinates())) {
			for (OperationsCentreDTO subordinate : operationsCentre.getSubordinates()) {
				setOperationsCentreSnapshot(subordinate, maxDate);
			}
		}
		OperationsCentreSnapshotDTO snapshot = this.getOperationsCentreRemoteService().findOperationsCentreSnapshotByDateRemote(operationsCentre.getId(), maxDate);
		operationsCentre.setSnapshot(snapshot);
		for (ActorDTO actor : operationsCentre.getActors()) {
			setActorSnapshot(actor, maxDate);
		}
	}

	/**
	 * Sets the actor snapshot.
	 *
	 * @param actor the actor
	 * @param maxDate the max date
	 */
	private void setActorSnapshot(ActorDTO actor, Date maxDate) {

		if (CollectionUtils.isNotEmpty(actor.getSubordinates())) {
			for (ActorDTO subordinate : actor.getSubordinates()) {
				setActorSnapshot(subordinate, maxDate);
			}
		}
		//		ActorSnapshotDTO snapshot = this.getMappingService().mapActorSnapshot(actor.getId(), maxDate);
		ActorSnapshotDTO snapshot = this.getActorRemoteService().findActorSnapshotByDateRemote(actor.getId(), maxDate);
		actor.setSnapshot(snapshot);
		for (EquipmentDTO equipment : actor.getEquipmentSet()) {
			setEquipmentSnapshot(equipment, maxDate);
		}
	}

	/**
	 * Sets the equipment snapshot.
	 *
	 * @param equipment the equipment
	 * @param maxDate the max date
	 */
	private void setEquipmentSnapshot(EquipmentDTO equipment, Date maxDate) {

		//		EquipmentSnapshotDTO snapshot = this.getMappingService().mapEquipmentSnapshot(equipment.getId(), maxDate);
		EquipmentSnapshotDTO snapshot = this.getEquipmentRemoteService().findEquipmentSnapshotByDateRemote(equipment.getId(), maxDate);
		equipment.setSnapshot(snapshot);
		//		for (SensorDTO sensor : equipment.getSensors()) {
		//			setSensorSnapshot(sensor, maxDate);
		//		}
	}

	/**
	 * Gets the sensor snapshot.
	 *
	 * @param sensorID the sensor id
	 * @param maxDate the max date
	 * @return the sensor snapshot
	 */
	private SensorSnapshotDTO getSensorSnapshot(Long sensorID, Date maxDate) {
		//		SensorSnapshotDTO snapshot = this.getMappingService().mapLatestSensorSnapshot(sensorID, maxDate);
		SensorSnapshotDTO snapshot = this.getSensorRemoteService().findSensorSnapshotByDateRemote(sensorID, maxDate);
		return snapshot;
	}

}
