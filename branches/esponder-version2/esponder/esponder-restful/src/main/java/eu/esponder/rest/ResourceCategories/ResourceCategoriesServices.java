/*
 * 
 */
package eu.esponder.rest.ResourceCategories;

import javax.ws.rs.Path;

import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceCategoriesServices.
 */
@Path("/crisis/context")
public class ResourceCategoriesServices extends ESponderResource {
	
	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}

}