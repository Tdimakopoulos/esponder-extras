/*
 * 
 */
package eu.esponder.rest.PointsAndCircles;


// TODO: Auto-generated Javadoc
/**
 * The Class Circle.
 */
public class Circle {
    
    /** The radius. */
    public double radius;
    
    /** The y. */
    public double x,y; 
    
    /**
     * Creates a new instance of Circle.
     */
    public Circle() {
    }
    
    /**
     * Instantiates a new circle.
     *
     * @param x the x
     * @param y the y
     * @param radius the radius
     */
    public Circle(double x, double y, double radius)
    {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }
    
    /**
     * Checks if is in circle.
     *
     * @param p the p
     * @return true, if is in circle
     */
    public boolean isInCircle(MovablePoint p)
    {
        return ( (x - p.getMidX())*(x - p.getMidX()) + (y - p.getMidY())*(y - p.getMidY()) <= radius * radius);
    }
    
}
