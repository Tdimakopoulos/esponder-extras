/*
 * 
 */
package eu.esponder.rest.crisis;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc

/**
 * The Class FRTeamResource.
 */

@Path("/crisis/context")
public class FRTeamResource extends ESponderResource {

	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}

	/**
	 * Gets the cheif for team.
	 *
	 * @param frTeamID the fr team id
	 * @param pkiKey the pki key
	 * @return the actor dto
	 */
	@GET
	@Path("/Teams/GetChiefForTeam")
	@Produces({ MediaType.APPLICATION_JSON })
	public ActorDTO GetChiefForTeam(
			@QueryParam("frTeamID") @NotNull(message="frTeamID may not be null") Long frTeamID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);
		return this.getActorRemoteService().findFRTeamChiefRemote(frTeamID);
	}

	/**
	 * Gets the teams for crisis.
	 *
	 * @param crisisContextID the crisis context id
	 * @param pkiKey the pki key
	 * @return the list
	 */
	@GET
	@Path("/Teams/GetTeamsForCrisis")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO GetTeamsForCrisis(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextID may not be null") Long crisisContextID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);
		List<OperationsCentreDTO> ocList = this.getOperationsCentreRemoteService().findMeocsByCrisisRemote(crisisContextID);
		List<FRTeamDTO> teamsList = new ArrayList<FRTeamDTO>();
		for(OperationsCentreDTO meoc : ocList) {
			teamsList.addAll(this.getActorRemoteService().findFRTeamsForMeocRemote(meoc.getId()));
		}
		return new ResultListDTO(teamsList);
	}

	/**
	 * Gets the teams for eoc.
	 *
	 * @param eocID the eoc id
	 * @param pkiKey the pki key
	 * @return the list
	 */
	@GET
	@Path("/Teams/GetTeamsForEoc")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO GetTeamsForEoc(
			@QueryParam("eocID") @NotNull(message="eocID may not be null") Long eocID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);
		OperationsCentreDTO eoc = this.getOperationsCentreRemoteService().findOperationCentreByIdRemote(eocID);
		return GetTeamsForCrisis(eoc.getCrisisContextId(), pkiKey);
	}

	/**
	 * Gets the team for chief.
	 *
	 * @param frchiefID the frchief id
	 * @param pkiKey the pki key
	 * @return the fR team dto
	 */
	@GET
	@Path("/Teams/GetTeamForChief")
	@Produces({ MediaType.APPLICATION_JSON })
	public FRTeamDTO GetTeamForChief(
			@QueryParam("frchiefID") @NotNull(message="frchiefID may not be null") Long frchiefID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {
		
		Long userID=SecurityCheck(pkiKey);
		return this.getActorRemoteService().findFRTeamForChiefRemote(frchiefID);
	}

	/**
	 * Gets the fr us for chief.
	 *
	 * @param frchiefID the frchief id
	 * @param pkiKey the pki key
	 * @return the list
	 */
	@GET
	@Path("/Teams/GetFRsForChief")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ActorDTO> GetFRUsForChief(
			@QueryParam("frchiefID") @NotNull(message="frchiefID may not be null") Long frchiefID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {
		
		Long userID=SecurityCheck(pkiKey);
		return this.getActorRemoteService().findSubordinatesByIdRemote(frchiefID);
	}

}
