/*
 * 
 */
package eu.esponder.rest.portal;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.rest.geolocation.calculations.DirectLineCalculations2D;
import eu.esponder.rest.geolocation.lib.GlobalCoordinates;

// TODO: Auto-generated Javadoc
/**
 * The Class LocationSensorThread.
 */
public class LocationSensorThread extends SensorThread {

	/** The start latitude. */
	private double startLatitude;

	/** The start longitude. */
	private double startLongitude;

	/** The dest latitude. */
	private double destLatitude;

	/** The dest longitude. */
	private double destLongitude;
	
	/** The min step. */
	private int minStep;
	
	/** The max step. */
	private int maxStep;

	/**
	 * Instantiates a new location sensor thread.
	 *
	 * @param period the period
	 * @param sensor the sensor
	 * @param statisticType the statistic type
	 * @param startPoint the start point
	 * @param destPoint the dest point
	 * @param statisticsEnvelope the statistics envelope
	 * @param minStep the min step
	 * @param maxStep the max step
	 */
	public LocationSensorThread(Long period, SensorDTO sensor,
			MeasurementStatisticTypeEnumDTO statisticType, PointDTO startPoint, PointDTO destPoint, List<SensorMeasurementStatisticDTO> statisticsEnvelope, int minStep, int maxStep) {
		super(period, sensor, statisticType, statisticsEnvelope);	// statisticsEnvelope --> List of statistics, not actual envelope

		this.setStartLatitude(startPoint.getLatitude().doubleValue());
		this.setStartLongitude(startPoint.getLongitude().doubleValue());
		this.setDestLatitude(destPoint.getLatitude().doubleValue());
		this.setDestLongitude(destPoint.getLongitude().doubleValue());
		this.setMinStep(minStep);
		this.setMaxStep(maxStep);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.rest.portal.SensorThread#run()
	 */
	@Override
	public void run() {

		Long meas_from = new Date().getTime();
		try {
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		while(!this.isInterrupted()) {
			
			try {
				super.run();
				this.setStatisticMeasurement(getMeasurement(meas_from, this.startLatitude, this.startLongitude, this.destLatitude, this.destLongitude, this.minStep, this.maxStep));

//				System.out.println("Measurement for Location Sensor"+Thread.currentThread().getName() + " with id : " + Thread.currentThread().getId()+" is : "
//						+ ((LocationSensorMeasurementDTO)this.getStatisticMeasurement().getStatistic()).getPoint()
//						+" with period : "+(this.getStatisticMeasurement().getPeriod().getDateFrom().toString()+" : "
//						+(this.getStatisticMeasurement().getPeriod().getDateTo())));

				synchronized (this.getStatisticsList()) {
					this.getStatisticsList().add(this.getStatisticMeasurement());
				}

				// Current starting Point is the point returned as measurement
				this.setStartLatitude(((LocationSensorMeasurementDTO)this.getStatisticMeasurement().getStatistic()).getPoint().getLatitude().doubleValue());
				this.setStartLongitude(((LocationSensorMeasurementDTO)this.getStatisticMeasurement().getStatistic()).getPoint().getLongitude().doubleValue());

				meas_from = new Date().getTime();
				Thread.sleep(this.getPeriod());
				
			} catch (InterruptedException e) {
				System.out.println("\n\n"+Thread.currentThread().getName()+"/"+Thread.currentThread().getId()+" : "+"Caught me during Sleep\n\n");
				break;
			}
		}

	}

	/**
	 * Gets the measurement.
	 *
	 * @param meas_from the meas_from
	 * @param startLat the start lat
	 * @param startLong the start long
	 * @param destLat the dest lat
	 * @param destLon the dest lon
	 * @param minLocStep the min loc step
	 * @param maxLocStep the max loc step
	 * @return the measurement
	 */
	public SensorMeasurementStatisticDTO getMeasurement(Long meas_from, double startLat, double startLong, double destLat, double destLon, int minLocStep, int maxLocStep) {

		double step = (double)(minLocStep + (int)(Math.random() * ((maxLocStep - minLocStep) + 1)));
		LocationSensorMeasurementDTO measurement = new LocationSensorMeasurementDTO();
		measurement.setPoint(getLocationMeasurementValue(startLat, startLong, destLat, destLon, step));
		measurement.setSensor(getSensor());
		measurement.setTimestamp(new Date().getTime());
		this.getStatisticMeasurement().setStatistic(measurement);
		Long meas_to = new Date().getTime();
		PeriodDTO measurementPeriod = new PeriodDTO();
		measurementPeriod.setDateFrom( meas_from);
		measurementPeriod.setDateTo( meas_to);
		this.getStatisticMeasurement().setPeriod(measurementPeriod);
		this.getStatisticMeasurement().setSamplingPeriod(this.getPeriod());
		return getStatisticMeasurement();
	}


	/**
	 * Gets the location measurement value.
	 *
	 * @param lat1 the lat1
	 * @param lon1 the lon1
	 * @param lat2 the lat2
	 * @param lon2 the lon2
	 * @param step the step
	 * @return the location measurement value
	 */
	private PointDTO getLocationMeasurementValue(double lat1, double lon1, double lat2, double lon2, double step) {
		/*
		 * use range (choose proper type) to determine randomness of generated values
		 */

		DirectLineCalculations2D calc = new DirectLineCalculations2D();
		GlobalCoordinates coordinates = calc.TwoDimensionalDirectCalculation(lat1, lon1, lat2, lon2, step);
		PointDTO measuredPoint = new PointDTO(new BigDecimal(coordinates.getLatitude()), new BigDecimal(coordinates.getLongitude()), null);
		return measuredPoint;
	}

	/**
	 * Gets the start latitude.
	 *
	 * @return the start latitude
	 */
	public double getStartLatitude() {
		return startLatitude;
	}

	/**
	 * Sets the start latitude.
	 *
	 * @param startLatitude the new start latitude
	 */
	public void setStartLatitude(double startLatitude) {
		this.startLatitude = startLatitude;
	}

	/**
	 * Gets the start longitude.
	 *
	 * @return the start longitude
	 */
	public double getStartLongitude() {
		return startLongitude;
	}

	/**
	 * Sets the start longitude.
	 *
	 * @param startLongitude the new start longitude
	 */
	public void setStartLongitude(double startLongitude) {
		this.startLongitude = startLongitude;
	}

	/**
	 * Gets the dest latitude.
	 *
	 * @return the dest latitude
	 */
	public double getDestLatitude() {
		return destLatitude;
	}

	/**
	 * Sets the dest latitude.
	 *
	 * @param destLatitude the new dest latitude
	 */
	public void setDestLatitude(double destLatitude) {
		this.destLatitude = destLatitude;
	}

	/**
	 * Gets the dest longitude.
	 *
	 * @return the dest longitude
	 */
	public double getDestLongitude() {
		return destLongitude;
	}

	/**
	 * Sets the dest longitude.
	 *
	 * @param destLongitude the new dest longitude
	 */
	public void setDestLongitude(double destLongitude) {
		this.destLongitude = destLongitude;
	}

	/**
	 * Gets the min step.
	 *
	 * @return the min step
	 */
	public int getMinStep() {
		return minStep;
	}

	/**
	 * Sets the min step.
	 *
	 * @param minStep the new min step
	 */
	public void setMinStep(int minStep) {
		this.minStep = minStep;
	}

	/**
	 * Gets the max step.
	 *
	 * @return the max step
	 */
	public int getMaxStep() {
		return maxStep;
	}

	/**
	 * Sets the max step.
	 *
	 * @param maxStep the new max step
	 */
	public void setMaxStep(int maxStep) {
		this.maxStep = maxStep;
	}

}
