/*
 * 
 */
package eu.esponder.rest.Personnel;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisViewPersonnel.
 */
@Path("/crisis/view")
public class CrisisViewPersonnel extends ESponderResource {

	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}

	/**
	 * Read personnel by id.
	 *
	 * @param personnelID the personnel id
	 * @param pkiKey the pki key
	 * @return the personnel dto
	 */
	@GET
	@Path("/personnel/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelDTO readPersonnelById(
			@QueryParam("personnelID") @NotNull(message="personnelId may not be null") Long personnelID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		PersonnelDTO personnelDTO = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		return personnelDTO;
	}

	/**
	 * Read all personnel.
	 *
	 * @param pkiKey the pki key
	 * @return the result list dto
	 */
	@GET
	@Path("/personnel/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllPersonnel(
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return new ResultListDTO(this.getPersonnelRemoteService().findAllPersonnelRemote());

	}
	
	/**
	 * Read all personnel Competences.
	 *
	 * @param pkiKey the pki key
	 * @return the result list dto
	 */
	@GET
	@Path("/personnelCompetence/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllPersonnelCompetences(
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return new ResultListDTO(this.getPersonnelCompetencesRemoteService().findAllPersonnelCompetencesRemote());

	}

	/**
	 * Read personnel competence by id.
	 *
	 * @param personnelCompetenceID the personnel competence id
	 * @param pkiKey the pki key
	 * @return the personnel competence dto
	 */
	@GET
	@Path("/personnelCompetence/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelCompetenceDTO readPersonnelCompetenceById(
			@QueryParam("personnelCompetenceID") @NotNull(message="personnelCompetenceId may not be null") Long personnelCompetenceID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		PersonnelCompetenceDTO personnelCompetenceDTO = this.getPersonnelRemoteService().findPersonnelCompetenceByIdRemote(personnelCompetenceID);
		return personnelCompetenceDTO;
	}

	/**
	 * Read personnel by title.
	 *
	 * @param personnelTitle the personnel title
	 * @param pkiKey the pki key
	 * @return the personnel dto
	 */
	@GET
	@Path("/personnel/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelDTO readPersonnelByTitle(
			@QueryParam("personnelTitle") @NotNull(message="personnelTitle may not be null") String personnelTitle,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		PersonnelDTO personnelDTO = this.getPersonnelRemoteService().findPersonnelByTitleRemote(personnelTitle);
		return personnelDTO;
	}

	/**
	 * Read personnel competence by title.
	 *
	 * @param personnelCompetenceTitle the personnel competence title
	 * @param pkiKey the pki key
	 * @return the personnel competence dto
	 */
	@GET
	@Path("/personnelCompetence/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelCompetenceDTO readPersonnelCompetenceByTitle(
			@QueryParam("personnelCompetenceTitle") @NotNull(message="personnelCompetenceTitle may not be null") String personnelCompetenceTitle,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		PersonnelCompetenceDTO personnelCompetenceDTO = this.getPersonnelRemoteService().findPersonnelCompetenceByTitleRemote(personnelCompetenceTitle);
		return personnelCompetenceDTO;
	}

	/**
	 * Creates the personnel.
	 *
	 * @param personnelDTO the personnel dto
	 * @param pkiKey the pki key
	 * @return the personnel dto
	 */
	@POST
	@Path("/personnel/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelDTO createPersonnel(
			@NotNull(message="Personnel object may not be null") PersonnelDTO personnelDTO,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		PersonnelDTO PersonnelDTOPersisted = this.getPersonnelRemoteService().createPersonnelRemote(personnelDTO, userID);
		return PersonnelDTOPersisted;
	}

	/**
	 * Creates the personnel competence.
	 *
	 * @param personnelCompetenceDTO the personnel competence dto
	 * @param pkiKey the pki key
	 * @return the personnel competence dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@POST
	@Path("/personnelCompetence/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelCompetenceDTO createPersonnelCompetence(
			@NotNull(message="PersonnelCompetence object may not be null") PersonnelCompetenceDTO personnelCompetenceDTO,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws ClassNotFoundException {

		Long userID=SecurityCheck(pkiKey);

		PersonnelCompetenceDTO PersonnelCompetenceDTOPersisted = this.getPersonnelRemoteService().createPersonnelCompetenceRemote(personnelCompetenceDTO, userID);
		return PersonnelCompetenceDTOPersisted;
	}

	/**
	 * Update personnel.
	 *
	 * @param personnelDTO the personnel dto
	 * @param pkiKey the pki key
	 * @return the personnel dto
	 */
	@PUT
	@Path("/personnel/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelDTO updatePersonnel(
			@NotNull(message="Personnel object may not be null") PersonnelDTO personnelDTO,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		PersonnelDTO PersonnelDTOPersisted = this.getPersonnelRemoteService().updatePersonnelRemote(personnelDTO, userID);
		return PersonnelDTOPersisted;
	}

	/**
	 * Update personnel competence.
	 *
	 * @param personnelCompetenceDTO the personnel competence dto
	 * @param szuserID the szuser id
	 * @return the personnel competence dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@PUT
	@Path("/personnelCompetence/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public PersonnelCompetenceDTO updatePersonnelCompetence(
			@NotNull(message="PersonnelCompetence object may not be null") PersonnelCompetenceDTO personnelCompetenceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) throws ClassNotFoundException {

		Long userID=SecurityCheck(szuserID);

		PersonnelCompetenceDTO PersonnelCompetenceDTOPersisted = this.getPersonnelRemoteService().updatePersonnelCompetenceRemote(personnelCompetenceDTO, userID);
		return PersonnelCompetenceDTOPersisted;
	}

	/**
	 * Delete personnel by id.
	 *
	 * @param personnelID the personnel id
	 * @param pkiKey the pki key
	 * @return the long
	 */
	@DELETE
	@Path("/personnel/delete")
	@Produces({MediaType.APPLICATION_JSON})
	public Long deletePersonnelById(
			@QueryParam("personnelID") @NotNull(message="personnelId may not be null") Long personnelID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		this.getPersonnelRemoteService().deletePersonnelByIdRemote(personnelID);
		return personnelID;
	}

	/**
	 * Delete personnel competence by id.
	 *
	 * @param personnelCompetenceID the personnel competence id
	 * @param pkiKey the pki key
	 * @return the long
	 */
	@DELETE
	@Path("/personnelCompetence/delete")
	@Produces({MediaType.APPLICATION_JSON})
	public Long deletePersonnelCompetenceById(
			@QueryParam("personnelCompetenceID") @NotNull(message="personnelCompetenceId may not be null") Long personnelCompetenceID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		this.getPersonnelRemoteService().deletePersonnelCompetenceByIdRemote(personnelCompetenceID);
		return personnelCompetenceID;
	}

}
