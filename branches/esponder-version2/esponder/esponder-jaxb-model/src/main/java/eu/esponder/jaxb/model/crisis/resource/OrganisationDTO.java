package eu.esponder.jaxb.model.crisis.resource;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.snapshot.location.AddressDTO;
import eu.esponder.jaxb.model.snapshot.location.LocationAreaDTO;
import eu.esponder.jaxb.model.snapshot.location.PointDTO;
import eu.esponder.jaxb.model.type.DisciplineTypeDTO;
import eu.esponder.jaxb.model.type.OrganisationTypeDTO;

@XmlRootElement(name="Organisation")
@XmlType(name="Organisation")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "firstName",
					"lastName", "rank", "availability", "organisation"})
public class OrganisationDTO {
	
	private Long id;
	
	private DisciplineTypeDTO disciplineType;
	
	private OrganisationTypeDTO organizationType;
	
	private OrganisationDTO parent;
	
	private Set<OrganisationDTO> children;
	
	private AddressDTO address;
	
	private PointDTO location;
	
	private LocationAreaDTO responsibilityArea;
	
	public DisciplineTypeDTO getDisciplineType() {
		return disciplineType;
	}

	public void setDisciplineType(DisciplineTypeDTO disciplineType) {
		this.disciplineType = disciplineType;
	}

	public OrganisationTypeDTO getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(OrganisationTypeDTO organizationType) {
		this.organizationType = organizationType;
	}

	public OrganisationDTO getParent() {
		return parent;
	}

	public void setParent(OrganisationDTO parent) {
		this.parent = parent;
	}

	public Set<OrganisationDTO> getChildren() {
		return children;
	}

	public void setChildren(Set<OrganisationDTO> children) {
		this.children = children;
	}
	
	public AddressDTO getAddress() {
		return address;
	}

	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	public PointDTO getLocation() {
		return location;
	}

	public void setLocation(PointDTO location) {
		this.location = location;
	}

	public LocationAreaDTO getResponsibilityArea() {
		return responsibilityArea;
	}

	public void setResponsibilityArea(LocationAreaDTO responsibilityArea) {
		this.responsibilityArea = responsibilityArea;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

}
