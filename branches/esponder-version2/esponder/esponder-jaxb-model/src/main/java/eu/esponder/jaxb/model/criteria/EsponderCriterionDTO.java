package eu.esponder.jaxb.model.criteria;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="EsponderCriterionDTO")
@XmlType(name="EsponderCriterionDTO")
//@JsonPropertyOrder({"fieldName", "expression", "fieldValue"})
public class EsponderCriterionDTO extends EsponderQueryRestrictionDTO {

	private static final long serialVersionUID = -4927714081578656589L;

	private String field;
	
	private EsponderCriterionExpressionEnumDTO expression;
	
	private String value;
	
	public EsponderCriterionDTO() { }

	public EsponderCriterionExpressionEnumDTO getExpression() {
		return expression;
	}

	public void setExpression(EsponderCriterionExpressionEnumDTO expression) {
		this.expression = expression;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
