/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import org.dozer.CustomConverter;

import eu.esponder.model.type.EmergencyOperationsCentreType;
import eu.esponder.model.type.MobileEmergencyOperationsCentreType;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreCategoryOCTypeFieldConverter.
 */
public class OperationsCentreCategoryOCTypeFieldConverter implements CustomConverter {

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert(Object destination,
			Object source, Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == EmergencyOperationsCentreType.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == MobileEmergencyOperationsCentreType.class) {
			destination = source;
			return destination;
		}
		
		return null;
	}

}
