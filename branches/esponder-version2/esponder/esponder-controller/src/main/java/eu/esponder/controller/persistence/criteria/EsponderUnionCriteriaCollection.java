/*
 * 
 */
package eu.esponder.controller.persistence.criteria;

import java.util.Collection;

// TODO: Auto-generated Javadoc
/**
 * The Class EsponderUnionCriteriaCollection.
 */
public class EsponderUnionCriteriaCollection extends EsponderCriteriaCollection {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7938678292927863036L;

	/**
	 * Instantiates a new esponder union criteria collection.
	 */
	public EsponderUnionCriteriaCollection() {}

	/**
	 * Instantiates a new esponder union criteria collection.
	 *
	 * @param restrictions the restrictions
	 */
	public EsponderUnionCriteriaCollection(
			Collection<EsponderQueryRestriction> restrictions) {
		super(restrictions);
	}

}
