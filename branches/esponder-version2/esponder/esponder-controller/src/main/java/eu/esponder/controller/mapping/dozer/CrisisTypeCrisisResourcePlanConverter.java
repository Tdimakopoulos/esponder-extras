/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.type.CrisisDisasterTypeDTO;
import eu.esponder.dto.model.type.CrisisFeatureTypeDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;
import eu.esponder.model.type.CrisisDisasterType;
import eu.esponder.model.type.CrisisFeatureType;
import eu.esponder.model.type.CrisisType;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisTypeCrisisResourcePlanConverter.
 */
public class CrisisTypeCrisisResourcePlanConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if(source instanceof CrisisType && source != null) {
			if(source.getClass() == CrisisDisasterType.class){
				destination = this.getMappingService().mapObject(source, CrisisDisasterTypeDTO.class);
			}
			else if (source.getClass() == CrisisFeatureType.class) {
				destination = this.getMappingService().mapObject(source, CrisisFeatureTypeDTO.class);
			}
		}
		else
			if(source instanceof CrisisTypeDTO && source != null) {
				if(source.getClass() == CrisisDisasterTypeDTO.class){
					destination = this.getMappingService().mapObject(source, CrisisDisasterType.class);
				}
				else if (source.getClass() == CrisisFeatureTypeDTO.class) {
					destination = this.getMappingService().mapObject(source, CrisisFeatureTypeDTO.class);
				}
			}
			else {
				//FIXME Implement custom ESponderException
			}
		return destination;
	}

}
