/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.resource.RegisteredOperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.crisis.view.SketchPOI;
import eu.esponder.model.snapshot.ReferencePOISnapshot;
import eu.esponder.model.snapshot.SketchPOISnapshot;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;

// TODO: Auto-generated Javadoc
/**
 * The Interface OperationsCentreService.
 */
@Local
public interface OperationsCentreService extends OperationsCentreRemoteService {

	/**
	 * Find operation centre by id.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the operations centre
	 */
	public OperationsCentre findOperationCentreById(Long operationsCentreID);

	/**
	 * Find operations centre by title.
	 *
	 * @param title the title
	 * @return the operations centre
	 */
	public OperationsCentre findOperationsCentreByTitle(String title);

	/**
	 * Find operations centre by user.
	 *
	 * @param operationsCentreID the operations centre id
	 * @param userID the user id
	 * @return the operations centre
	 */
	public OperationsCentre findOperationsCentreByUser(Long operationsCentreID, Long userID);

	/**
	 * Sets the supervisor.
	 *
	 * @param operationsCentre the operations centre
	 * @param supervisingOperationsCentre the supervising operations centre
	 * @param userID the user id
	 * @return the operations centre
	 */
	public OperationsCentre setSupervisor(OperationsCentre operationsCentre, OperationsCentre supervisingOperationsCentre, Long userID);

	/**
	 * Creates the operations centre.
	 *
	 * @param operationsCentre the operations centre
	 * @param userID the user id
	 * @return the operations centre
	 */
	public OperationsCentre createOperationsCentre(OperationsCentre operationsCentre, Long userID);

	/**
	 * Update operations centre.
	 *
	 * @param operationsCentre the operations centre
	 * @param userID the user id
	 * @return the operations centre
	 */
	public OperationsCentre updateOperationsCentre(OperationsCentre operationsCentre, Long userID);

	/**
	 * Delete operations centre.
	 *
	 * @param operationsCentreId the operations centre id
	 * @param userID the user id
	 */
	public void deleteOperationsCentre(Long operationsCentreId, Long userID);

	/**
	 * Find operations centre snapshot by date.
	 *
	 * @param operationsCentreID the operations centre id
	 * @param maxDate the max date
	 * @return the operations centre snapshot
	 */
	public OperationsCentreSnapshot findOperationsCentreSnapshotByDate(Long operationsCentreID, Date maxDate);

	/**
	 * Creates the operations centre snapshot.
	 *
	 * @param operationsCentre the operations centre
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the operations centre snapshot
	 */
	public OperationsCentreSnapshot createOperationsCentreSnapshot(OperationsCentre operationsCentre, OperationsCentreSnapshot snapshot, Long userID);
	
	/**
	 * Update operations centre snapshot.
	 *
	 * @param operationsCentre the operations centre
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the operations centre snapshot
	 */
	public OperationsCentreSnapshot updateOperationsCentreSnapshot(OperationsCentre operationsCentre, OperationsCentreSnapshot snapshot, Long userID);
	
	/**
	 * Delete operations centre snapshot.
	 *
	 * @param operationsCentreSnapshotID the operations centre snapshot id
	 */
	public void deleteOperationsCentreSnapshot(Long operationsCentreSnapshotID);
	
	/**
	 * Find sketch poi by id.
	 *
	 * @param sketchPOIId the sketch poi id
	 * @return the sketch poi
	 */
	public SketchPOI findSketchPOIById(Long sketchPOIId);

	/**
	 * Find sketch poi by title.
	 *
	 * @param title the title
	 * @return the sketch poi
	 */
	public SketchPOI findSketchPOIByTitle(String title);
	
	/**
	 * Find sketch poi.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the list
	 */
	public List<SketchPOI> findSketchPOI(Long operationsCentreID);

	/**
	 * Creates the sketch poi.
	 *
	 * @param operationsCentre the operations centre
	 * @param sketch the sketch
	 * @param userID the user id
	 * @return the sketch poi
	 */
	public SketchPOI createSketchPOI(OperationsCentre operationsCentre, SketchPOI sketch, Long userID);
	
	/**
	 * Update sketch poi.
	 *
	 * @param operationsCentre the operations centre
	 * @param sketch the sketch
	 * @param userID the user id
	 * @return the sketch poi
	 */
	public SketchPOI updateSketchPOI(OperationsCentre operationsCentre, SketchPOI sketch, Long userID);
	
	/**
	 * Delete sketch poi.
	 *
	 * @param sketchPOIId the sketch poi id
	 * @param userID the user id
	 */
	public void deleteSketchPOI(Long sketchPOIId, Long userID);

	/**
	 * Find reference poi by title.
	 *
	 * @param title the title
	 * @return the reference poi
	 */
	public ReferencePOI findReferencePOIByTitle(String title);

	/**
	 * Find reference poi by id.
	 *
	 * @param referencePOIId the reference poi id
	 * @return the reference poi
	 */
	public ReferencePOI findReferencePOIById(Long referencePOIId);
	
	/**
	 * Find reference po is.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the list
	 */
	public List<ReferencePOI> findReferencePOIs(Long operationsCentreID);

	/**
	 * Creates the reference poi.
	 *
	 * @param operationsCentre the operations centre
	 * @param referencePOI the reference poi
	 * @param userID the user id
	 * @return the reference poi
	 */
	public ReferencePOI createReferencePOI(OperationsCentre operationsCentre, ReferencePOI referencePOI, Long userID);

	/**
	 * Update reference poi.
	 *
	 * @param operationsCentre the operations centre
	 * @param referencePOI the reference poi
	 * @param userID the user id
	 * @return the reference poi
	 */
	public ReferencePOI updateReferencePOI(OperationsCentre operationsCentre, ReferencePOI referencePOI, Long userID);
	
	/**
	 * Delete reference poi.
	 *
	 * @param referencePOIId the reference poi id
	 * @param userID the user id
	 */
	public void deleteReferencePOI(Long referencePOIId, Long userID);

	/**
	 * Find user operations centres.
	 *
	 * @param userID the user id
	 * @return the list
	 */
	public List<OperationsCentre> findUserOperationsCentres(Long userID);

	/**
	 * Find all operations centres.
	 *
	 * @return the list
	 */
	public List<OperationsCentre> findAllOperationsCentres();

	/**
	 * Find registered operation centre by id.
	 *
	 * @param registeredOperationsCentreID the registered operations centre id
	 * @return the registered operations centre
	 */
	public RegisteredOperationsCentre findRegisteredOperationCentreById(Long registeredOperationsCentreID);

	/**
	 * Find registered operations centre by title.
	 *
	 * @param title the title
	 * @return the registered operations centre
	 */
	public RegisteredOperationsCentre findRegisteredOperationsCentreByTitle(String title);

	/**
	 * Find all registered operations centres.
	 *
	 * @return the list
	 */
	public List<RegisteredOperationsCentre> findAllRegisteredOperationsCentres();

	/**
	 * Creates the registered operations centre.
	 *
	 * @param registeredOperationsCentre the registered operations centre
	 * @param userID the user id
	 * @return the registered operations centre
	 */
	public RegisteredOperationsCentre createRegisteredOperationsCentre(
			RegisteredOperationsCentre registeredOperationsCentre, Long userID);

	/**
	 * Delete registered operations centre.
	 *
	 * @param registeredOperationsCentreId the registered operations centre id
	 * @param userID the user id
	 */
	public void deleteRegisteredOperationsCentre(Long registeredOperationsCentreId, Long userID);

	/**
	 * Update registered operations centre.
	 *
	 * @param registeredOperationsCentre the registered operations centre
	 * @param userID the user id
	 * @return the registered operations centre
	 */
	public RegisteredOperationsCentre updateRegisteredOperationsCentre(
			RegisteredOperationsCentre registeredOperationsCentre, Long userID);

	/**
	 * Find all operations centres by crisis context.
	 *
	 * @param crisisContextID the crisis context id
	 * @return the list
	 */
	public List<OperationsCentre> findAllOperationsCentresByCrisisContext(Long crisisContextID);

	/**
	 * Find sketch poi snapshot by id.
	 *
	 * @param sensorID the sensor id
	 * @return the sketch poi snapshot
	 */
	public SketchPOISnapshot findSketchPOISnapshotById(Long sensorID);
	
	/**
	 * Find all sketch poi snapshots.
	 *
	 * @return the list
	 */
	public List<SketchPOISnapshot> findAllSketchPOISnapshots();
	
	/**
	 * Find sketch poi snapshot by date.
	 *
	 * @param sensorID the sensor id
	 * @param maxDate the max date
	 * @return the sketch poi snapshot
	 */
	public SketchPOISnapshot findSketchPOISnapshotByDate(Long sensorID, Date maxDate);
	
	/**
	 * Find previous sketch poi snapshot.
	 *
	 * @param sensorID the sensor id
	 * @return the sketch poi snapshot
	 */
	public SketchPOISnapshot findPreviousSketchPOISnapshot(Long sensorID);
	
	/**
	 * Creates the sketch poi snapshot.
	 *
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the sketch poi snapshot
	 */
	public SketchPOISnapshot createSketchPOISnapshot(SketchPOISnapshot snapshot, Long userID);
	
	/**
	 * Update sketch poi snapshot.
	 *
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the sketch poi snapshot
	 */
	public SketchPOISnapshot updateSketchPOISnapshot(SketchPOISnapshot snapshot, Long userID);
	
	/**
	 * Delete sketch poi snapshot.
	 *
	 * @param sensorSnapshotId the sensor snapshot id
	 * @param userID the user id
	 */
	public void deleteSketchPOISnapshot(Long sensorSnapshotId, Long userID);

	/**
	 * Find reference poi snapshot by id.
	 *
	 * @param referencePOIID the reference poiid
	 * @return the reference poi snapshot
	 */
	ReferencePOISnapshot findReferencePOISnapshotById(Long referencePOIID);

	/**
	 * Find all reference poi snapshots.
	 *
	 * @return the list
	 */
	List<ReferencePOISnapshot> findAllReferencePOISnapshots();

	

	/**
	 * Find reference poi snapshot by date.
	 *
	 * @param referencePOIID the reference poiid
	 * @param maxDate the max date
	 * @return the reference poi snapshot
	 */
	ReferencePOISnapshot findReferencePOISnapshotByDate(Long referencePOIID,
			Date maxDate);

	/**
	 * Find previous reference poi snapshot.
	 *
	 * @param referencePOIID the reference poiid
	 * @return the reference poi snapshot
	 */
	ReferencePOISnapshot findPreviousReferencePOISnapshot(Long referencePOIID);

	/**
	 * Creates the reference poi snapshot.
	 *
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the reference poi snapshot
	 */
	ReferencePOISnapshot createReferencePOISnapshot(
			ReferencePOISnapshot snapshot, Long userID);

	/**
	 * Update reference poi snapshot.
	 *
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the reference poi snapshot
	 */
	ReferencePOISnapshot updateReferencePOISnapshot(
			ReferencePOISnapshot snapshot, Long userID);

	/**
	 * Delete reference poi snapshot.
	 *
	 * @param referencePOISnapshotId the reference poi snapshot id
	 * @param userID the user id
	 */
	void deleteReferencePOISnapshot(Long referencePOISnapshotId, Long userID);

	/**
	 * Find all reference poi snapshots by reference poi.
	 *
	 * @param referencePOI the reference poi
	 * @param resultLimit the result limit
	 * @return the list
	 */
	List<ReferencePOISnapshot> findAllReferencePOISnapshotsByReferencePOI(
			Long referencePOI, int resultLimit);

	/**
	 * Find all sketch poi snapshots by poi.
	 *
	 * @param sensor the sensor
	 * @param resultLimit the result limit
	 * @return the list
	 */
	List<SketchPOISnapshot> findAllSketchPOISnapshotsByPoi(Long sensor,
			int resultLimit);

	/**
	 * Find meocs by crisis.
	 *
	 * @param crisisContextID the crisis context id
	 * @return the list
	 */
	public List<OperationsCentre> findMeocsByCrisis(Long crisisContextID);

	/**
	 * Find eoc by crisis.
	 *
	 * @param crisisContextID the crisis context id
	 * @return the operations centre
	 */
	public OperationsCentre findEocByCrisis(Long crisisContextID);

	/**
	 * Find meoc supervisor.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the actor
	 */
	public Actor findMeocSupervisor(Long operationsCentreID);

	/**
	 * Find eoc supervisor.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the actor
	 */
	public Actor findEocSupervisor(Long operationsCentreID);
}
