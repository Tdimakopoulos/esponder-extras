/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Interface SensorRemoteService.
 */
@Remote
public interface SensorRemoteService {
	
	/**
	 * Find sensor by id remote.
	 *
	 * @param sensorId the sensor id
	 * @return the sensor dto
	 */
	public SensorDTO findSensorByIdRemote(Long sensorId);
	
	/**
	 * Find sensor by title remote.
	 *
	 * @param title the title
	 * @return the sensor dto
	 */
	public SensorDTO findSensorByTitleRemote(String title);
	
	/**
	 * Creates the sensor remote.
	 *
	 * @param sensor the sensor
	 * @param userID the user id
	 * @return the sensor dto
	 */
	public SensorDTO createSensorRemote(SensorDTO sensor, Long userID);
	
	/**
	 * Update sensor remote.
	 *
	 * @param sensorDTO the sensor dto
	 * @param userID the user id
	 * @return the sensor dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public SensorDTO updateSensorRemote(SensorDTO sensorDTO, Long userID) throws ClassNotFoundException;
	
	/**
	 * Delete sensor remote.
	 *
	 * @param sensorId the sensor id
	 * @param userID the user id
	 */
	public void deleteSensorRemote(Long sensorId, Long userID);
	
	/**
	 * Find sensor snapshot by date remote.
	 *
	 * @param sensorID the sensor id
	 * @param dateTo the date to
	 * @return the sensor snapshot dto
	 */
	public SensorSnapshotDTO findSensorSnapshotByDateRemote(Long sensorID, Date dateTo);
	
	/**
	 * Find sensor snapshot by id remote.
	 *
	 * @param sensorID the sensor id
	 * @return the sensor snapshot dto
	 */
	public SensorSnapshotDTO findSensorSnapshotByIdRemote(Long sensorID);
	
	/**
	 * Find previous sensor snapshot remote.
	 *
	 * @param sensorID the sensor id
	 * @return the sensor snapshot dto
	 */
	public SensorSnapshotDTO findPreviousSensorSnapshotRemote(Long sensorID);
	
	/**
	 * Creates the sensor snapshot remote.
	 *
	 * @param snapshotDTO the snapshot dto
	 * @param userID the user id
	 * @return the sensor snapshot dto
	 */
	public SensorSnapshotDTO createSensorSnapshotRemote(SensorSnapshotDTO snapshotDTO, Long userID);
	
	/**
	 * Update sensor snapshot remote.
	 *
	 * @param sensorSnapshotDTO the sensor snapshot dto
	 * @param userID the user id
	 * @return the sensor snapshot dto
	 */
	public SensorSnapshotDTO updateSensorSnapshotRemote(SensorSnapshotDTO sensorSnapshotDTO, Long userID);
	
	/**
	 * Delete sensor snapshot remote.
	 *
	 * @param sensorSnapshotId the sensor snapshot id
	 * @param userID the user id
	 */
	public void deleteSensorSnapshotRemote(Long sensorSnapshotId, Long userID);
	
	/**
	 * Find config by id remote.
	 *
	 * @param configID the config id
	 * @return the statistics config dto
	 */
	public StatisticsConfigDTO findConfigByIdRemote(Long configID);
	
	/**
	 * Creates the statistic config remote.
	 *
	 * @param statisticConfigDTO the statistic config dto
	 * @param userID the user id
	 * @return the statistics config dto
	 */
	public StatisticsConfigDTO createStatisticConfigRemote(StatisticsConfigDTO statisticConfigDTO, Long userID);
	
	/**
	 * Update statistic config remote.
	 *
	 * @param statisticConfigDTO the statistic config dto
	 * @param userID the user id
	 * @return the statistics config dto
	 */
	public StatisticsConfigDTO updateStatisticConfigRemote(StatisticsConfigDTO statisticConfigDTO, Long userID);
	
	/**
	 * Delete statistic config remote.
	 *
	 * @param statisticsConfigId the statistics config id
	 * @param userID the user id
	 */
	public void deleteStatisticConfigRemote(Long statisticsConfigId, Long userID);

	/**
	 * Find all sensor snapshots remote.
	 *
	 * @return the list
	 */
	public List<SensorSnapshotDTO> findAllSensorSnapshotsRemote();

	/**
	 * Find all sensor snapshots by sensor remote.
	 *
	 * @param sensorDTO the sensor dto
	 * @param resultLimit the result limit
	 * @return the list
	 * @throws ClassNotFoundException the class not found exception
	 */
	public List<SensorSnapshotDTO> findAllSensorSnapshotsBySensorRemote(SensorDTO sensorDTO, int resultLimit) throws ClassNotFoundException;

	/**
	 * Find all sensor snapshots from to remote.
	 *
	 * @param sensorID the sensor id
	 * @param dateFrom the date from
	 * @param dateTo the date to
	 * @return the list
	 */
	List<SensorSnapshotDTO> findAllSensorSnapshotsFromToRemote(Long sensorID,
			Long dateFrom, Long dateTo);

	/**
	 * Find all sensor snapshots from to and type remote.
	 *
	 * @param sensorID the sensor id
	 * @param dateFrom the date from
	 * @param dateTo the date to
	 * @param statisticType the statistic type
	 * @return the list
	 */
	List<SensorSnapshotDTO> findAllSensorSnapshotsFromToAndTypeRemote(
			Long sensorID, Long dateFrom, Long dateTo,
			MeasurementStatisticTypeEnumDTO statisticType);

	/**
	 * Find all sensor snapshots by sensor and type remote.
	 *
	 * @param sensorDTO the sensor dto
	 * @param statisticType the statistic type
	 * @param resultLimit the result limit
	 * @return the list
	 * @throws ClassNotFoundException the class not found exception
	 */
	List<SensorSnapshotDTO> findAllSensorSnapshotsBySensorAndTypeRemote(
			SensorDTO sensorDTO, MeasurementStatisticTypeEnum statisticType,
			int resultLimit) throws ClassNotFoundException;
	
	
}
