/*
 * 
 */
package eu.esponder.controller.crisis.resource.bean;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import eu.esponder.controller.crisis.resource.ESponderResourceService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.model.crisis.resource.Resource;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderResourceBean.
 */
@Stateless
public class ESponderResourceBean implements ESponderResourceService {

	
	/** The resource crud bean. */
	@EJB
	private CrudService<Resource> resourceCrudBean;
	
	/** The e sponder mapping service. */
	@EJB
	private ESponderMappingService eSponderMappingService;
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ESponderResourceRemoteService#findDTOById(java.lang.Class, java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ResourceDTO findDTOById(Class<? extends ResourceDTO> clz, Long id) {
		try {
			Class<? extends Resource> targetClass = (Class<? extends Resource>) eSponderMappingService.getManagedEntityClass(clz);
			Resource resource = findByID(targetClass, id);
			return (ResourceDTO) eSponderMappingService.mapESponderEntity(resource, clz);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ESponderResourceRemoteService#findDTOByTitle(java.lang.Class, java.lang.String)
	 */
	@Override
	public ResourceDTO findDTOByTitle(Class<? extends ResourceDTO> clz,
			String title) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ESponderResourceService#findByID(java.lang.Class, java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Resource findByID(Class<? extends Resource> clz, Long id) {
		return resourceCrudBean.find((Class<Resource>) clz, id);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ESponderResourceService#findByTitle(java.lang.Class, java.lang.String)
	 */
	@Override
	public Resource findByTitle(Class<? extends Resource> clz,
			String title) {
		// TODO Auto-generated method stub
		return null;
	}

}
