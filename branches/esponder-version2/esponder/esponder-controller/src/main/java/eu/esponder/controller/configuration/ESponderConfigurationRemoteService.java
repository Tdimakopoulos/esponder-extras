/*
 * 
 */
package eu.esponder.controller.configuration;

import javax.ejb.Remote;

import eu.esponder.dto.model.config.ESponderConfigParameterDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ESponderConfigurationRemoteService.
 */
@Remote
public interface ESponderConfigurationRemoteService {
	
	/**
	 * Find e sponder config by name remote.
	 *
	 * @param configDTOTitle the config dto title
	 * @return the e sponder config parameter dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderConfigParameterDTO findESponderConfigByNameRemote(String configDTOTitle) throws ClassNotFoundException;

	/**
	 * Find e sponder config by id remote.
	 *
	 * @param configDTOId the config dto id
	 * @return the e sponder config parameter dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderConfigParameterDTO findESponderConfigByIdRemote(Long configDTOId) throws ClassNotFoundException;

	/**
	 * Creates the e sponder config remote.
	 *
	 * @param configDTO the config dto
	 * @param userID the user id
	 * @return the e sponder config parameter dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderConfigParameterDTO createESponderConfigRemote(ESponderConfigParameterDTO configDTO, Long userID) throws ClassNotFoundException;
	
	/**
	 * Update e sponder config remote.
	 *
	 * @param configDTO the config dto
	 * @param userID the user id
	 * @return the e sponder config parameter dto
	 */
	public ESponderConfigParameterDTO updateESponderConfigRemote(ESponderConfigParameterDTO configDTO, Long userID);
	
	/**
	 * Delete e sponder config remote.
	 *
	 * @param ESponderConfigDTOID the e sponder config dtoid
	 * @param userID the user id
	 */
	public void deleteESponderConfigRemote(Long ESponderConfigDTOID, Long userID);

}
