/*
 * 
 */
package eu.esponder.controller.persistence.criteria;

// TODO: Auto-generated Javadoc
/**
 * The Enum EsponderCriterionExpressionEnum.
 */
public enum EsponderCriterionExpressionEnum {

	/** The equal. */
	EQUAL,
	
	/** The greater than or equal. */
	GREATER_THAN_OR_EQUAL,
	
	/** The greater than. */
	GREATER_THAN,
	
	/** The less than. */
	LESS_THAN,
	
	/** The less than or equal. */
	LESS_THAN_OR_EQUAL,
	
	/** The not. */
	NOT
}
