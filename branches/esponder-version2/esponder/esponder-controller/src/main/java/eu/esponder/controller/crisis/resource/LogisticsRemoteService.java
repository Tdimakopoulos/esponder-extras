/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface LogisticsRemoteService.
 */
@Remote
public interface LogisticsRemoteService {
	
	/**
	 * Find reusable resource by id remote.
	 *
	 * @param reusableID the reusable id
	 * @return the reusable resource dto
	 */
	public ReusableResourceDTO findReusableResourceByIdRemote(Long reusableID);
	
	/**
	 * Find all reusable resources remote.
	 *
	 * @return the list
	 */
	public List<ReusableResourceDTO> findAllReusableResourcesRemote();
	
	/**
	 * Find reusable resource by title remote.
	 *
	 * @param title the title
	 * @return the reusable resource dto
	 */
	public ReusableResourceDTO findReusableResourceByTitleRemote(String title);
	
	/**
	 * Creates the reusable resource remote.
	 *
	 * @param reusableResourceDTO the reusable resource dto
	 * @param userID the user id
	 * @return the reusable resource dto
	 */
	public ReusableResourceDTO createReusableResourceRemote(ReusableResourceDTO reusableResourceDTO, Long userID);
	
	/**
	 * Update reusable resource remote.
	 *
	 * @param reusableResourceDTO the reusable resource dto
	 * @param userID the user id
	 * @return the reusable resource dto
	 */
	public ReusableResourceDTO updateReusableResourceRemote(ReusableResourceDTO reusableResourceDTO, Long userID);
	
	/**
	 * Delete reusable resource remote.
	 *
	 * @param reusableResourceDTOID the reusable resource dtoid
	 * @param userID the user id
	 */
	public void deleteReusableResourceRemote(Long reusableResourceDTOID, Long userID);
	
	/**
	 * Find consumable resource by id remote.
	 *
	 * @param consumableID the consumable id
	 * @return the consumable resource dto
	 */
	public ConsumableResourceDTO findConsumableResourceByIdRemote(Long consumableID);
	
	/**
	 * Find all consumable resources remote.
	 *
	 * @return the list
	 */
	public List<ConsumableResourceDTO> findAllConsumableResourcesRemote();
	
	/**
	 * Find consumable resource by title remote.
	 *
	 * @param title the title
	 * @return the consumable resource dto
	 */
	public ConsumableResourceDTO findConsumableResourceByTitleRemote(String title);
	
	/**
	 * Creates the consumable resource remote.
	 *
	 * @param consumableResourceDTO the consumable resource dto
	 * @param userID the user id
	 * @return the consumable resource dto
	 */
	public ConsumableResourceDTO createConsumableResourceRemote(ConsumableResourceDTO consumableResourceDTO, Long userID);
	
	/**
	 * Update consumable resource remote.
	 *
	 * @param consumableResourceDTO the consumable resource dto
	 * @param userID the user id
	 * @return the consumable resource dto
	 */
	public ConsumableResourceDTO updateConsumableResourceRemote(ConsumableResourceDTO consumableResourceDTO, Long userID);
	
	/**
	 * Delete consumable resource remote.
	 *
	 * @param consumableResourceDTOID the consumable resource dtoid
	 * @param userID the user id
	 */
	public void deleteConsumableResourceRemote(Long consumableResourceDTOID, Long userID);

	/**
	 * Find registered reusable resource by id remote.
	 *
	 * @param registeredReusableID the registered reusable id
	 * @return the registered reusable resource dto
	 */
	public RegisteredReusableResourceDTO findRegisteredReusableResourceByIdRemote(
			Long registeredReusableID);

	/**
	 * Find registered consumable resource by id remote.
	 *
	 * @param registeredConsumableID the registered consumable id
	 * @return the registered consumable resource dto
	 */
	public RegisteredConsumableResourceDTO findRegisteredConsumableResourceByIdRemote(Long registeredConsumableID);

	/**
	 * Find registered reusable resource by title remote.
	 *
	 * @param title the title
	 * @return the registered reusable resource dto
	 */
	public RegisteredReusableResourceDTO findRegisteredReusableResourceByTitleRemote(String title);
	
	/**
	 * Find all registered reusable resources remote.
	 *
	 * @return the list
	 */
	public List<RegisteredReusableResourceDTO> findAllRegisteredReusableResourcesRemote();

	/**
	 * Find registered consumable resource by title remote.
	 *
	 * @param title the title
	 * @return the registered consumable resource dto
	 */
	public RegisteredConsumableResourceDTO findRegisteredConsumableResourceByTitleRemote(String title);

	/**
	 * Creates the registered consumable resource remote.
	 *
	 * @param registeredConsumableResourceDTO the registered consumable resource dto
	 * @param userID the user id
	 * @return the registered consumable resource dto
	 */
	public RegisteredConsumableResourceDTO createRegisteredConsumableResourceRemote(
			RegisteredConsumableResourceDTO registeredConsumableResourceDTO, Long userID);

	/**
	 * Creates the registered reusable resource remote.
	 *
	 * @param registeredReusableResourceDTO the registered reusable resource dto
	 * @param userID the user id
	 * @return the registered reusable resource dto
	 */
	public RegisteredReusableResourceDTO createRegisteredReusableResourceRemote(
			RegisteredReusableResourceDTO registeredReusableResourceDTO, Long userID);

	/**
	 * Delete registered consumable resource remote.
	 *
	 * @param registeredReusableResourceDTOID the registered reusable resource dtoid
	 * @param userID the user id
	 */
	public void deleteRegisteredConsumableResourceRemote(Long registeredReusableResourceDTOID, Long userID);

	/**
	 * Delete registered reusable resource remote.
	 *
	 * @param registeredReusableResourceDTOID the registered reusable resource dtoid
	 * @param userID the user id
	 */
	public void deleteRegisteredReusableResourceRemote(Long registeredReusableResourceDTOID, Long userID);

	/**
	 * Update registered reusable resource remote.
	 *
	 * @param registeredReusableResourceDTO the registered reusable resource dto
	 * @param userID the user id
	 * @return the registered reusable resource dto
	 */
	public RegisteredReusableResourceDTO updateRegisteredReusableResourceRemote(
			RegisteredReusableResourceDTO registeredReusableResourceDTO, Long userID);

	/**
	 * Update registered consumable resource remote.
	 *
	 * @param registeredConsumableResourceDTO the registered consumable resource dto
	 * @param userID the user id
	 * @return the registered consumable resource dto
	 */
	public RegisteredConsumableResourceDTO updateRegisteredConsumableResourceRemote(
			RegisteredConsumableResourceDTO registeredConsumableResourceDTO, Long userID);
	
	/**
	 * Find all registered consumable resources remote.
	 *
	 * @return the list
	 */
	public List<RegisteredConsumableResourceDTO> findAllRegisteredConsumableResourcesRemote();

}
