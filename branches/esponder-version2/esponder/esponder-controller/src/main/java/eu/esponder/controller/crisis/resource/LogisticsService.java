/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.RegisteredConsumableResource;
import eu.esponder.model.crisis.resource.RegisteredReusableResource;
import eu.esponder.model.crisis.resource.ReusableResource;

// TODO: Auto-generated Javadoc
/**
 * The Interface LogisticsService.
 */
@Local
public interface LogisticsService extends LogisticsRemoteService {
	
	/**
	 * Find reusable resource by id.
	 *
	 * @param reusableID the reusable id
	 * @return the reusable resource
	 */
	public ReusableResource findReusableResourceById(Long reusableID);

	/**
	 * Find reusable resource by title.
	 *
	 * @param title the title
	 * @return the reusable resource
	 */
	public ReusableResource findReusableResourceByTitle(String title);
	
	/**
	 * Creates the reusable resource.
	 *
	 * @param reusableResource the reusable resource
	 * @param userID the user id
	 * @return the reusable resource
	 */
	public ReusableResource createReusableResource(ReusableResource reusableResource,Long userID);
	
	/**
	 * Update reusable resource.
	 *
	 * @param reusableResource the reusable resource
	 * @param userID the user id
	 * @return the reusable resource
	 */
	public ReusableResource updateReusableResource(ReusableResource reusableResource, Long userID);
	
	/**
	 * Delete reusable resource.
	 *
	 * @param reusableResourceID the reusable resource id
	 * @param userID the user id
	 */
	public void deleteReusableResource(Long reusableResourceID, Long userID);
	
	/**
	 * Find consumable resource by id.
	 *
	 * @param consumableID the consumable id
	 * @return the consumable resource
	 */
	public ConsumableResource findConsumableResourceById(Long consumableID);

	/**
	 * Find consumable resource by title.
	 *
	 * @param title the title
	 * @return the consumable resource
	 */
	public ConsumableResource findConsumableResourceByTitle(String title);

	/**
	 * Creates the consumable resource.
	 *
	 * @param consumableResource the consumable resource
	 * @param userID the user id
	 * @return the consumable resource
	 */
	public ConsumableResource createConsumableResource(ConsumableResource consumableResource, Long userID);

	/**
	 * Update consumable resource.
	 *
	 * @param consumableResource the consumable resource
	 * @param userID the user id
	 * @return the consumable resource
	 */
	public ConsumableResource updateConsumableResource(ConsumableResource consumableResource, Long userID);
	
	/**
	 * Delete consumable resource.
	 *
	 * @param consumableResourceID the consumable resource id
	 * @param userID the user id
	 */
	public void deleteConsumableResource(Long consumableResourceID, Long userID);

	/**
	 * Find registered reusable resource by id.
	 *
	 * @param registeredReusableID the registered reusable id
	 * @return the registered reusable resource
	 */
	public RegisteredReusableResource findRegisteredReusableResourceById(
			Long registeredReusableID);

	/**
	 * Find registered consumable resource by id.
	 *
	 * @param registeredConsumableID the registered consumable id
	 * @return the registered consumable resource
	 */
	public RegisteredConsumableResource findRegisteredConsumableResourceById(Long registeredConsumableID);

	/**
	 * Find registered reusable resource by title.
	 *
	 * @param title the title
	 * @return the registered reusable resource
	 */
	public RegisteredReusableResource findRegisteredReusableResourceByTitle(String title);

	/**
	 * Find registered consumable resource by title.
	 *
	 * @param title the title
	 * @return the registered consumable resource
	 */
	public RegisteredConsumableResource findRegisteredConsumableResourceByTitle(String title);

	/**
	 * Creates the registered consumable resource.
	 *
	 * @param registeredConsumableResource the registered consumable resource
	 * @param userID the user id
	 * @return the registered consumable resource
	 */
	public RegisteredConsumableResource createRegisteredConsumableResource(
			RegisteredConsumableResource registeredConsumableResource, Long userID);

	/**
	 * Creates the registered reusable resource.
	 *
	 * @param registeredReusableResource the registered reusable resource
	 * @param userID the user id
	 * @return the registered reusable resource
	 */
	public RegisteredReusableResource createRegisteredReusableResource(
			RegisteredReusableResource registeredReusableResource, Long userID);

	/**
	 * Delete registered consumable resource.
	 *
	 * @param registeredConsumableResourceID the registered consumable resource id
	 * @param userID the user id
	 */
	public void deleteRegisteredConsumableResource(Long registeredConsumableResourceID, Long userID);

	/**
	 * Delete registered reusable resource.
	 *
	 * @param registeredReusableResourceID the registered reusable resource id
	 * @param userID the user id
	 */
	public void deleteRegisteredReusableResource(Long registeredReusableResourceID, Long userID);

	/**
	 * Update registered reusable resource.
	 *
	 * @param registeredReusableResource the registered reusable resource
	 * @param userID the user id
	 * @return the registered reusable resource
	 */
	public RegisteredReusableResource updateRegisteredReusableResource(
			RegisteredReusableResource registeredReusableResource, Long userID);

	/**
	 * Update registered consumable resource.
	 *
	 * @param registeredConsumableResource the registered consumable resource
	 * @param userID the user id
	 * @return the registered consumable resource
	 */
	public RegisteredConsumableResource updateRegisteredConsumableResource(
			RegisteredConsumableResource registeredConsumableResource, Long userID);

	/**
	 * Find all reusable resources.
	 *
	 * @return the list
	 */
	public List<ReusableResource> findAllReusableResources();

	/**
	 * Find all consumable resources.
	 *
	 * @return the list
	 */
	public List<ConsumableResource> findAllConsumableResources();

	/**
	 * Find all registered reusable resources.
	 *
	 * @return the list
	 */
	public List<RegisteredReusableResource> findAllRegisteredReusableResources();

	/**
	 * Find all registered consumable resources.
	 *
	 * @return the list
	 */
	public List<RegisteredConsumableResource> findAllRegisteredConsumableResources();
	
}
