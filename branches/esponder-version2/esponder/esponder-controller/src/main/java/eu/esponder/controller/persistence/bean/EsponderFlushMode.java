/*
 * 
 */
package eu.esponder.controller.persistence.bean;

// TODO: Auto-generated Javadoc
/**
 * The Class EsponderFlushMode.
 */
public class EsponderFlushMode extends DOMParser{

	/** The My custom flush mode. */
	int MyCustomFlushMode=1034;
	
	/** The breturn. */
	boolean breturn=false;
	
	/** The multithreat flush mode. */
	int MULTITHREAT_FLUSH_MODE = 1023;


	/**
	 * Gets the my custom flush mode.
	 *
	 * @return the int
	 */
	public int GetMyCustomFlushMode()
	{
		return MyCustomFlushMode;
	}

	/**
	 * My custom flush mode.
	 *
	 * @return true, if successful
	 */
	public boolean MyCustomFlushMode()
	{
		return breturn;
	}
}
