/*
 * 
 */
package eu.esponder.controller.configuration.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.controller.configuration.ESponderConfigurationService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
import eu.esponder.model.config.ESponderConfigParameter;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderConfigurationBean.
 */
@Stateless
public class ESponderConfigurationBean implements ESponderConfigurationService, ESponderConfigurationRemoteService {
	
	/** The config crud service. */
	@EJB
	private CrudService<ESponderConfigParameter> configCrudService;

	/** The mapping service. */
	@EJB 
	private ESponderMappingService mappingService;

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.configuration.ESponderConfigurationRemoteService#findESponderConfigByNameRemote(java.lang.String)
	 */
	@Override
	public ESponderConfigParameterDTO findESponderConfigByNameRemote(String configDTOTitle) throws ClassNotFoundException {
		ESponderConfigParameter configParameter = findESponderConfigByName(configDTOTitle);
		if(configParameter != null)
			return (ESponderConfigParameterDTO) mappingService.mapObject(configParameter, ESponderConfigParameterDTO.class);
		else
			return null;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.configuration.ESponderConfigurationService#findESponderConfigByName(java.lang.String)
	 */
	@Override
	public ESponderConfigParameter findESponderConfigByName(String configName)	throws ClassNotFoundException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("parameterName", configName);
		return (ESponderConfigParameter) configCrudService.findSingleWithNamedQuery("ESponderConfigParameter.findByName", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.configuration.ESponderConfigurationRemoteService#findESponderConfigByIdRemote(java.lang.Long)
	 */
	@Override
	public ESponderConfigParameterDTO findESponderConfigByIdRemote(Long configDTOId) throws ClassNotFoundException {
		ESponderConfigParameter configParameter = findESponderConfigById(configDTOId);
		if(configParameter != null)
			return (ESponderConfigParameterDTO) mappingService.mapObject(configParameter, ESponderConfigParameterDTO.class);
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.configuration.ESponderConfigurationService#findESponderConfigById(java.lang.Long)
	 */
	@Override
	public ESponderConfigParameter findESponderConfigById(Long configId)throws ClassNotFoundException {
		return (ESponderConfigParameter) configCrudService.find(ESponderConfigParameter.class, configId);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.configuration.ESponderConfigurationRemoteService#createESponderConfigRemote(eu.esponder.dto.model.config.ESponderConfigParameterDTO, java.lang.Long)
	 */
	@Override
	public ESponderConfigParameterDTO createESponderConfigRemote(ESponderConfigParameterDTO configDTO, Long userID) throws ClassNotFoundException {
		ESponderConfigParameter configParameter = (ESponderConfigParameter) mappingService.mapObject(configDTO, ESponderConfigParameter.class); 
		configParameter = createESponderConfig(configParameter, userID);
		configDTO = (ESponderConfigParameterDTO) mappingService.mapObject(configParameter, ESponderConfigParameterDTO.class);
		return configDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.configuration.ESponderConfigurationService#createESponderConfig(eu.esponder.model.config.ESponderConfigParameter, java.lang.Long)
	 */
	@Override
	public ESponderConfigParameter createESponderConfig(ESponderConfigParameter config, Long userID)	throws ClassNotFoundException {
		return configCrudService.create(config);
	}

	// -------------------------------------------------------------------------
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.configuration.ESponderConfigurationRemoteService#updateESponderConfigRemote(eu.esponder.dto.model.config.ESponderConfigParameterDTO, java.lang.Long)
	 */
	@Override
	public ESponderConfigParameterDTO updateESponderConfigRemote(ESponderConfigParameterDTO configDTO, Long userID) {
		ESponderConfigParameter configParameter = (ESponderConfigParameter) mappingService.mapObject(configDTO, ESponderConfigParameter.class);
		configParameter = updateESponderConfig(configParameter, userID);
		configDTO = (ESponderConfigParameterDTO) mappingService.mapObject(configParameter, ESponderConfigParameterDTO.class);
		return configDTO;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.configuration.ESponderConfigurationService#updateESponderConfig(eu.esponder.model.config.ESponderConfigParameter, java.lang.Long)
	 */
	@Override
	
	public ESponderConfigParameter updateESponderConfig(ESponderConfigParameter config, Long userID) {
		return configCrudService.update(config);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.configuration.ESponderConfigurationRemoteService#deleteESponderConfigRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteESponderConfigRemote(Long ESponderConfigDTOID, Long userID) {
		deleteESponderConfig(ESponderConfigDTOID, userID);
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.configuration.ESponderConfigurationService#deleteESponderConfig(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteESponderConfig(Long ESponderConfigID, Long userID) {
		configCrudService.delete(ESponderConfigParameter.class, ESponderConfigID);
	}
	
	
	
}
