/*
 * 
 */
package eu.esponder.controller.persistence.bean;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileLock;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import eu.esponder.controller.persistence.CrudRemoteService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.controller.persistence.criteria.EsponderCriteriaCollection;
import eu.esponder.controller.persistence.criteria.EsponderCriterion;
import eu.esponder.controller.persistence.criteria.EsponderIntersectionCriteriaCollection;
import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.controller.persistence.criteria.EsponderUnionCriteriaCollection;
import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
/**
 * The Class CrudBean.
 *
 * @param <T> the generic type
 */
@Stateless
public class CrudBean<T> implements CrudService<T>, CrudRemoteService<T> {

	/** The em. */
	@PersistenceContext
	private EntityManager em;

	/**
	 * Flushes the persistence context.
	 */
	public void flush() {
		em.flush();
	}

	/*
	 * This function should never be called but as a FailSave we implemented, will be called by the em
	 * 
	 * 
	 */
	/**
	 * Persistance wait for unlock.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	protected void PersistanceWaitForUnlock() throws IOException, InterruptedException
	{
		 FileOutputStream fos= new FileOutputStream("persistance.lock");
		    FileLock fl = fos.getChannel().tryLock();
		    if(fl != null) {
		      System.out.println("Locked File from Persistance wait until unlock to do refresh");
		      Thread.sleep(100);
		      fl.release();
		      System.out.println("Released Lock from persistance to refresh");
		    }
		    fos.close();
	}
	
	/**
	 * Perform an initial save of a previously unsaved T entity. All subsequent
	 * persist actions of this entity should use the #update() method. This
	 * operation must be performed within the a database transaction context for
	 * the entity's data to be permanently saved to the persistence store, i.e.,
	 * database. This method uses the
	 *
	 * @param t T entity to persist
	 * @return the t
	 * {@link javax.persistence.EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 */
	public T create(T t) {
		EsponderFlushMode pMode = new EsponderFlushMode();
		if(pMode.MyCustomFlushMode())
		{
			this.em.persist(t);
			this.em.flush();
		}else
		{
			this.em.persist(t);
			this.em.flush();
			this.em.refresh(t);
		}
		return t;

	}

	/**
	 * Perform an initial save of a previously unsaved T entity. All subsequent
	 * persist actions of this entity should use the #update() method. This
	 * operation must be performed within the a database transaction context for
	 * the entity's data to be permanently saved to the persistence store, i.e.,
	 * database. This method uses the
	 *
	 * @param t T entity to persist
	 * @return the t
	 * {@link javax.persistence.EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 */
	public T createnorefresh(T t) {
		this.em.persist(t);
		this.em.flush();
		return t;

	}

	/**
	 * Find a T entity based on its id.
	 *
	 * @param type the type
	 * @param id The T id.
	 * @return A T entity.
	 */
	public T find(Class<T> type, Object id) {
		return this.em.find(type, id);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#delete(java.lang.Class, java.lang.Object)
	 */
	public void delete(Class<T> type, Object id) {
		Object ref = this.em.getReference(type, id);
		this.em.remove(ref);
	}

	/**
	 * Delete a persistent T entity. This operation must be
	 * performed within the a database transaction context for the entity's data
	 * to be permanently deleted from the persistence store, i.e., database.
	 * This method uses the
	 *
	 * @param entity entity to delete
	 * {@link javax.persistence.EntityManager#remove(Object) operation.
	 */
	public void delete(T entity) {
		em.remove(entity);
	}


	//	public void delete(Class<?> entityClass, Long entityID) {
	//		em.remove(em.find(entityClass, entityID));
	//	}

	/**
	 * Persist a previously saved T entity and return it or a copy of it to the
	 * sender. A copy of the T entity parameter is returned when the JPA
	 * persistence mechanism has not previously been tracking the updated
	 * entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 *
	 * @param t T entity to update
	 * @return T the persisted T entity instance, may not be the same
	 * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 */
	public T update(T t) {
		return this.em.merge(t);
	}

	/**
	 * Refresh.
	 *
	 * @param t T entity to refresh
	 */
	public void refresh(T t) {
		this.em.refresh(t);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#findWithNamedQuery(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public List<T> findWithNamedQuery(String namedQueryName) {
		return this.em.createNamedQuery(namedQueryName).getResultList();
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#findWithNamedQuery(java.lang.String, int)
	 */
	@SuppressWarnings("unchecked")
	public List<T> findWithNamedQuery(String queryName, int resultLimit) {
		return this.em.createNamedQuery(queryName).setMaxResults(resultLimit)
				.getResultList();
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#findWithNamedQuery(java.lang.String, java.util.Map)
	 */
	public List<T> findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters) {
		return this.findWithNamedQuery(namedQueryName, parameters, 0);

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#findWithNamedQuery(java.lang.String, java.util.Map, int)
	 */
	@SuppressWarnings("unchecked")
	public List<T> findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters, int resultLimit) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createNamedQuery(namedQueryName);
		if (resultLimit > 0)
			query.setMaxResults(resultLimit);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#findSingleWithNamedQuery(java.lang.String, java.util.Map)
	 */
	public Object findSingleWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters) {
		Object result = null;

		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createNamedQuery(namedQueryName);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		try {
			result = query.getSingleResult();
		}
		catch (NoResultException nre) {
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#findWithQuery(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public List<T> findWithQuery(String query) {
		return this.em.createQuery(query).getResultList();

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#findWithQuery(java.lang.String, int)
	 */
	@SuppressWarnings("unchecked")
	public List<T> findWithQuery(String query, int resultLimit) {
		return this.em.createQuery(query).setMaxResults(resultLimit)
				.getResultList();
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#findWithQuery(java.lang.String, java.util.Map)
	 */
	public List<T> findWithQuery(String query, Map<String, Object> parameters) {
		return this.findWithQuery(query, parameters, 0);

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#findWithQuery(java.lang.String, java.util.Map, int)
	 */
	@SuppressWarnings("unchecked")
	public List<T> findWithQuery(String queryStr,
			Map<String, Object> parameters, int resultLimit) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createQuery(queryStr);
		if (resultLimit > 0)
			query.setMaxResults(resultLimit);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#findWithCriteriaQuery(java.lang.Class, eu.esponder.controller.persistence.criteria.EsponderQueryRestriction, int, int)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<T> findWithCriteriaQuery(
			Class<? extends ESponderEntity> queriedClass,
			EsponderQueryRestriction criteria, int pageSize, int pageNumber) {

		org.hibernate.Session session = (Session) em.getDelegate();

		Criteria crit = session.createCriteria(queriedClass);
		Criterion criterion = null;

		if (criteria instanceof EsponderCriterion) {
			EsponderCriterion esponderCriterion = (EsponderCriterion) criteria;
			// if(complementaryMapping(queriedClass, esponderCriterion)) {
			criterion = (Criterion) createSimpleExpression(esponderCriterion);
			// }
		} else {
			EsponderCriteriaCollection esponderCriteriaCollection = (EsponderCriteriaCollection) criteria;
			Junction junction = getJunction(esponderCriteriaCollection);
			for (EsponderQueryRestriction restriction : esponderCriteriaCollection.getRestrictions()) {
				handleEsponderRestrictionCollection(queriedClass, junction, restriction);
			}
			criterion = junction;
		}

		crit.add(criterion);

		if (pageSize != 0) {
			crit.setMaxResults(pageSize);
		}

		return crit.list();
	}

	/**
	 * Gets the junction.
	 *
	 * @param collection the collection
	 * @return the junction
	 */
	private Junction getJunction(EsponderCriteriaCollection collection) {
		if (collection.getClass().equals(EsponderUnionCriteriaCollection.class)) {
			return Restrictions.disjunction();
		} else if (collection.getClass().equals(EsponderIntersectionCriteriaCollection.class)) {
			return Restrictions.conjunction();
		} else {
			return null;
		}
	}

	/**
	 * Handle esponder restriction collection.
	 *
	 * @param queriedClass the queried class
	 * @param junction the junction
	 * @param restriction the restriction
	 */
	@SuppressWarnings("rawtypes")
	private void handleEsponderRestrictionCollection(
			Class<? extends ESponderEntity> queriedClass, Junction junction,
			EsponderQueryRestriction restriction) {

		if (restriction instanceof EsponderCriterion) {
			EsponderCriterion esponderCriterion = (EsponderCriterion) restriction;
			// System.out.println("Selected Class is : "+queriedClass);
			// if(complementaryMapping(queriedClass, restriction)) {
			addCriterion(junction, esponderCriterion);
			// }
		} else {
			EsponderCriteriaCollection collection = (EsponderCriteriaCollection) restriction;
			for (EsponderQueryRestriction innerRestriction : collection.getRestrictions()) {
				// junction.add(handleEsponderRestrictionCollection(junction,
				// innerRestriction));
				handleEsponderRestrictionCollection(queriedClass, junction,
						innerRestriction);
			}
		}
	}

	/**
	 * Adds the criterion.
	 *
	 * @param junction the junction
	 * @param esponderCriterion the esponder criterion
	 */
	private void addCriterion(Junction junction,
			EsponderCriterion esponderCriterion) {
		junction.add(createSimpleExpression(esponderCriterion));

	}

	/**
	 * Creates the simple expression.
	 *
	 * @param esponderCriterion the esponder criterion
	 * @return the simple expression
	 */
	private SimpleExpression createSimpleExpression(
			EsponderCriterion esponderCriterion) {

		String fieldStr = esponderCriterion.getField();
		Object valueObj = esponderCriterion.getValue();
		SimpleExpression simpleExpression = null;
		switch (esponderCriterion.getExpression()) {
		case EQUAL:
			simpleExpression = getRestrictionEquals(fieldStr, valueObj);
			break;
		case GREATER_THAN:
			simpleExpression = getRestrictionGT(fieldStr, valueObj);
			break;
		case GREATER_THAN_OR_EQUAL:
			simpleExpression = getRestrictionGTE(fieldStr, valueObj);
			break;
		case LESS_THAN:
			simpleExpression = getRestrictionLT(fieldStr, valueObj);
			break;
		case LESS_THAN_OR_EQUAL:
			simpleExpression = getRestrictionLTE(fieldStr, valueObj);
			break;
		default:
			System.out.println("BUG in simpleExpression creation");
		}
		return simpleExpression;
	}

	/**
	 * Gets the restriction equals.
	 *
	 * @param field the field
	 * @param value the value
	 * @return the restriction equals
	 */
	private SimpleExpression getRestrictionEquals(String field, Object value) {
		return Restrictions.eq(field, value);
	}

	/**
	 * Gets the restriction gt.
	 *
	 * @param field the field
	 * @param value the value
	 * @return the restriction gt
	 */
	private SimpleExpression getRestrictionGT(String field, Object value) {
		return Restrictions.gt(field, value);
	}

	/**
	 * Gets the restriction gte.
	 *
	 * @param field the field
	 * @param value the value
	 * @return the restriction gte
	 */
	private SimpleExpression getRestrictionGTE(String field, Object value) {
		return Restrictions.ge(field, value);
	}

	/**
	 * Gets the restriction lt.
	 *
	 * @param field the field
	 * @param value the value
	 * @return the restriction lt
	 */
	private SimpleExpression getRestrictionLT(String field, Object value) {
		return Restrictions.lt(field, value);
	}

	/**
	 * Gets the restriction lte.
	 *
	 * @param field the field
	 * @param value the value
	 * @return the restriction lte
	 */
	private SimpleExpression getRestrictionLTE(String field, Object value) {
		return Restrictions.le(field, value);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#getReference(java.lang.Class, java.lang.Object)
	 */
	public T getReference(Class<T> type, Object id) {
		return this.em.getReference(type, id);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.persistence.CrudRemoteService#getEntityManager()
	 */
	public EntityManager getEntityManager() {
		return em;
	}

	

}
