/*
 * 
 */
package eu.esponder.controller.persistence.criteria;

// TODO: Auto-generated Javadoc
/**
 * The Class EsponderCriterion.
 */
public class EsponderCriterion extends EsponderQueryRestriction  {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6667733307605361832L;

	/** The field. */
	private String field;
	
	/** The expression. */
	private EsponderCriterionExpressionEnum expression;
	
	/** The value. */
	private Object value;

	/**
	 * Gets the field.
	 *
	 * @return the field
	 */
	public String getField() {
		return field;
	}

	/**
	 * Sets the field.
	 *
	 * @param field the new field
	 */
	public void setField(String field) {
		this.field = field;
	}

	/**
	 * Gets the expression.
	 *
	 * @return the expression
	 */
	public EsponderCriterionExpressionEnum getExpression() {
		return expression;
	}

	/**
	 * Sets the expression.
	 *
	 * @param expression the new expression
	 */
	public void setExpression(EsponderCriterionExpressionEnum expression) {
		this.expression = expression;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(Object value) {
		this.value = value;
	}
	
}
