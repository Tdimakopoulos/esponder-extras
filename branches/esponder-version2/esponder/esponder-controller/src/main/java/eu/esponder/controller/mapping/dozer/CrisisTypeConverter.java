/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import org.dozer.CustomConverter;

import eu.esponder.model.type.CrisisDisasterType;
import eu.esponder.model.type.CrisisFeatureType;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisTypeConverter.
 */
public class CrisisTypeConverter implements CustomConverter {

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert(Object destination,
			Object source, Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == CrisisDisasterType.class) {
			destination = source;
			return destination;
		}
		if (sourceClass == CrisisFeatureType.class) {
			destination = source;
			return destination;
		}
		
		return null;
	}

}
