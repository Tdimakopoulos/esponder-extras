/*
 * 
 */
package eu.esponder.controller.crisis.action.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.action.ActionService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.crisis.action.ActionObjective;
import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.model.crisis.action.ActionPartObjective;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.action.ActionPartSnapshot;
import eu.esponder.model.snapshot.action.ActionSnapshot;
import eu.esponder.model.type.ActionType;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionBean.
 *
 * @param <T> the generic type
 */
@Stateless
public class ActionBean<T> implements ActionService, ActionRemoteService {

	

	/** The action crud service. */
	@EJB
	private CrudService<Action> actionCrudService;

	/** The action part crud service. */
	@EJB
	private CrudService<ActionPart> actionPartCrudService;

	/** The action objective crud service. */
	@EJB
	private CrudService<ActionObjective> actionObjectiveCrudService;

	/** The action snapshot crud service. */
	@EJB
	private CrudService<ActionSnapshot> actionSnapshotCrudService;

	/** The action part snapshot crud service. */
	@EJB
	private CrudService<ActionPartSnapshot> actionPartSnapshotCrudService;

	/** The action part objective crud service. */
	@EJB
	private CrudService<ActionPartObjective> actionPartObjectiveCrudService;

	/** The type service. */
	@EJB
	private TypeService typeService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;


	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#findActionDTOById(java.lang.Long)
	 */
	@Override
	public ActionDTO findActionDTOById(Long actionID) {
		Action action = findActionById(actionID);
		return (ActionDTO) mappingService.mapESponderEntity(action, ActionDTO.class);	
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#findActionById(java.lang.Long)
	 */
	@Override
	public Action findActionById(Long actionID) {
		return (Action) actionCrudService.find(Action.class, actionID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#findActionDTOByTitle(java.lang.String)
	 */
	@Override
	public ActionDTO findActionDTOByTitle(String title) {
		Action action = findActionByTitle(title);
		return (ActionDTO) mappingService.mapESponderEntity(action, ActionDTO.class);
	}


	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#findActionByTitle(java.lang.String)
	 */
	@Override
	public Action findActionByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Action) actionCrudService.findSingleWithNamedQuery(
				"Action.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#findActionPartDTOById(java.lang.Long)
	 */
	@Override
	public ActionPartDTO findActionPartDTOById(Long actionPartID) {
		ActionPart actionPart = findActionPartById(actionPartID);
		return (ActionPartDTO) mappingService.mapESponderEntity(actionPart, ActionPartDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#findActionPartById(java.lang.Long)
	 */
	@Override
	public ActionPart findActionPartById(Long actionPartID) {
		return (ActionPart) actionPartCrudService.find(ActionPart.class, actionPartID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#findActionPartDTOByTitle(java.lang.String)
	 */
	@Override
	public ActionPartDTO findActionPartDTOByTitle(String title) {
		ActionPart actionPart = findActionPartByTitle(title);
		return (ActionPartDTO) mappingService.mapESponderEntity(actionPart, ActionPartDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#findActionPartByTitle(java.lang.String)
	 */
	@Override
	public ActionPart findActionPartByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ActionPart) actionPartCrudService.findSingleWithNamedQuery(
				"ActionPart.findByTitle", params);
	}


	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#findActionObjectiveDTOById(java.lang.Long)
	 */
	@Override
	public ActionObjectiveDTO findActionObjectiveDTOById(Long actionID) {
		ActionObjective actionObjective = findActionObjectiveById(actionID);
		return (ActionObjectiveDTO) mappingService.mapESponderEntity(actionObjective, ActionObjectiveDTO.class);	
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#findActionObjectiveById(java.lang.Long)
	 */
	@Override
	public ActionObjective findActionObjectiveById(Long actionID) {
		return (ActionObjective) actionObjectiveCrudService.find(ActionObjective.class, actionID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#findActionSnapshotDTOById(java.lang.Long)
	 */
	@Override
	public ActionSnapshotDTO findActionSnapshotDTOById(Long actionSnapshotID) {
		ActionSnapshot actionSnapshot = findActionSnapshotById(actionSnapshotID);
		return (ActionSnapshotDTO) mappingService.mapESponderEntity(actionSnapshot, ActionSnapshotDTO.class);	
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#findActionSnapshotById(java.lang.Long)
	 */
	@Override
	public ActionSnapshot findActionSnapshotById(Long actionSnapshotID) {
		return (ActionSnapshot) actionSnapshotCrudService.find(ActionSnapshot.class, actionSnapshotID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#findActionPartSnapshotDTOById(java.lang.Long)
	 */
	@Override
	public ActionPartSnapshotDTO findActionPartSnapshotDTOById(Long actionPartSnapshotID) {
		ActionPartSnapshot actionPartSnapshot = findActionPartSnapshotById(actionPartSnapshotID);
		return (ActionPartSnapshotDTO) mappingService.mapESponderEntity(actionPartSnapshot, ActionPartSnapshotDTO.class);	
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#findActionPartSnapshotById(java.lang.Long)
	 */
	@Override
	public ActionPartSnapshot findActionPartSnapshotById(Long actionPartSnapshotID) {
		return (ActionPartSnapshot) actionPartSnapshotCrudService.find(ActionPartSnapshot.class, actionPartSnapshotID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#findActionObjectiveDTOByTitle(java.lang.String)
	 */
	@Override
	public ActionObjectiveDTO findActionObjectiveDTOByTitle(String title) {
		ActionObjective actionObjective = findActionObjectiveByTitle(title);
		return (ActionObjectiveDTO) mappingService.mapESponderEntity(actionObjective, ActionObjectiveDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#findActionObjectiveByTitle(java.lang.String)
	 */
	@Override
	public ActionObjective findActionObjectiveByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ActionObjective) actionObjectiveCrudService.findSingleWithNamedQuery(
				"ActionObjective.findByTitle", params);
	}


	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#findActionPartObjectiveDTOById(java.lang.Long)
	 */
	@Override
	public ActionPartObjectiveDTO findActionPartObjectiveDTOById(Long actionID) {
		ActionPartObjective actionPartObjective = findActionPartObjectiveById(actionID);
		return (ActionPartObjectiveDTO) mappingService.mapESponderEntity(actionPartObjective, ActionPartObjectiveDTO.class);	
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#findActionPartObjectiveById(java.lang.Long)
	 */
	@Override
	public ActionPartObjective findActionPartObjectiveById(Long actionID) {
		return (ActionPartObjective) actionPartObjectiveCrudService.find(ActionPartObjective.class, actionID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#findActionPartObjectiveDTOByTitle(java.lang.String)
	 */
	@Override
	public ActionPartObjectiveDTO findActionPartObjectiveDTOByTitle(String title) {
		ActionPartObjective actionPartObjective = findActionPartObjectiveByTitle(title);
		return (ActionPartObjectiveDTO) mappingService.mapESponderEntity(actionPartObjective, ActionPartObjectiveDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#findActionPartObjectiveByTitle(java.lang.String)
	 */
	@Override
	public ActionPartObjective findActionPartObjectiveByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ActionPartObjective) actionPartObjectiveCrudService.findSingleWithNamedQuery(
				"ActionPartObjective.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#createActionRemote(eu.esponder.dto.model.crisis.action.ActionDTO, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionDTO createActionRemote(ActionDTO actionDTO, Long userID) {
		Action action = (Action) mappingService.mapESponderEntityDTO(actionDTO, Action.class);
		action = createAction(action, userID);
		return (ActionDTO) mappingService.mapESponderEntity(action, ActionDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#createAction(eu.esponder.model.crisis.action.Action, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Action createAction(Action action, Long userID) {
		ActionType actionType = (ActionType) typeService.findById(action.getActionType().getId());
		action.setActionType((actionType));
		actionCrudService.create(action);
		return action;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#updateActionRemote(eu.esponder.dto.model.crisis.action.ActionDTO, java.lang.Long)
	 */
	@Override
	public ActionDTO updateActionRemote(ActionDTO actionDTO, Long userID) {
		Action action = (Action) mappingService.mapESponderEntityDTO(actionDTO, Action.class);
		action = updateAction(action, userID);
		return (ActionDTO) mappingService.mapESponderEntity(action, ActionDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#updateAction(eu.esponder.model.crisis.action.Action, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Action updateAction(Action action, Long userID) {
		ActionType actionType = (ActionType) typeService.findById(action.getActionType().getId());
		action.setActionType((actionType));
		actionCrudService.update(action);
		return action;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#createActionSnapshotRemote(eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionSnapshotDTO createActionSnapshotRemote(ActionSnapshotDTO actionSnapshotDTO, Long userID) {
		ActionSnapshot actionSnapshot = (ActionSnapshot) mappingService.mapESponderEntityDTO(actionSnapshotDTO, ActionSnapshot.class);
		actionSnapshot = createActionSnapshot(actionSnapshot, userID);
		return (ActionSnapshotDTO) mappingService.mapESponderEntity(actionSnapshot, ActionSnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#createActionSnapshot(eu.esponder.model.snapshot.action.ActionSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionSnapshot createActionSnapshot(ActionSnapshot actionSnapshot, Long userID) {
		actionSnapshotCrudService.create(actionSnapshot);
		return actionSnapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#updateActionSnapshotRemote(eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO, java.lang.Long)
	 */
	@Override
	public ActionSnapshotDTO updateActionSnapshotRemote(ActionSnapshotDTO actionSnapshotDTO, Long userID) {
		ActionSnapshot actionSnapshot = (ActionSnapshot) mappingService.mapESponderEntityDTO(actionSnapshotDTO, ActionSnapshot.class);
		actionSnapshot = updateActionSnapshot(actionSnapshot, userID);
		return (ActionSnapshotDTO) mappingService.mapESponderEntity(actionSnapshot, ActionSnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#updateActionSnapshot(eu.esponder.model.snapshot.action.ActionSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionSnapshot updateActionSnapshot(ActionSnapshot actionSnapshot, Long userID) {
		actionSnapshotCrudService.update(actionSnapshot);
		return actionSnapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#createActionPartRemote(eu.esponder.dto.model.crisis.action.ActionPartDTO, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionPartDTO createActionPartRemote(ActionPartDTO actionPartDTO, Long userID) {
		ActionPart actionPart = (ActionPart) mappingService.mapESponderEntityDTO(actionPartDTO, ActionPart.class);
		actionPart = createActionPart(actionPart, userID);
		return (ActionPartDTO) mappingService.mapESponderEntity(actionPart, ActionPartDTO.class); 
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#createActionPart(eu.esponder.model.crisis.action.ActionPart, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionPart createActionPart(ActionPart actionPart, Long userID) {
		ActionPart actionPartPersisted = actionPartCrudService.create(actionPart);
		return actionPartPersisted;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#updateActionPartRemote(eu.esponder.dto.model.crisis.action.ActionPartDTO, java.lang.Long)
	 */
	@Override
	public ActionPartDTO updateActionPartRemote(ActionPartDTO actionPartDTO, Long userID) {
		ActionPart actionPart = (ActionPart) mappingService.mapESponderEntityDTO(actionPartDTO, ActionPart.class);
		actionPart = updateActionPart(actionPart, userID);
		return (ActionPartDTO) mappingService.mapESponderEntity(actionPart, ActionPartDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#updateActionPart(eu.esponder.model.crisis.action.ActionPart, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionPart updateActionPart(ActionPart actionPart, Long userID) {
		Action action = actionCrudService.find(Action.class, actionPart.getAction().getId());
		if( action !=  null) {
			actionPart.setAction(action);
		}
		ActionPart actionPartPersisted = findActionPartById(actionPart.getId());
		mappingService.mapEntityToEntity(actionPart, actionPartPersisted);
		actionPartPersisted = actionPartCrudService.update(actionPartPersisted);
		return actionPartPersisted;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#createActionPartSnapshotRemote(eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO, java.lang.Long)
	 */
	@Override
	public ActionPartSnapshotDTO createActionPartSnapshotRemote(ActionPartSnapshotDTO actionPartSnapshotDTO, Long userID) {
		ActionPartSnapshot actionPartSnapshot = (ActionPartSnapshot) mappingService.mapESponderEntityDTO(actionPartSnapshotDTO, ActionPartSnapshot.class);
		actionPartSnapshot = createActionPartSnapshot(actionPartSnapshot, userID);
		return (ActionPartSnapshotDTO) mappingService.mapESponderEntity(actionPartSnapshot, ActionPartSnapshotDTO.class); 
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#createActionPartSnapshot(eu.esponder.model.snapshot.action.ActionPartSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionPartSnapshot createActionPartSnapshot(ActionPartSnapshot actionPartSnapshot, Long userID) {
		ActionPartSnapshot actionPartSnapshotPersisted = actionPartSnapshotCrudService.create(actionPartSnapshot);
		return actionPartSnapshotPersisted;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#updateActionPartSnapshotRemote(eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO, java.lang.Long)
	 */
	@Override
	public ActionPartSnapshotDTO updateActionPartSnapshotRemote(ActionPartSnapshotDTO actionPartSnapshotDTO, Long userID) {
		ActionPartSnapshot actionPartSnapshot = (ActionPartSnapshot) mappingService.mapESponderEntityDTO(actionPartSnapshotDTO, ActionPartSnapshot.class);
		actionPartSnapshot = updateActionPartSnapshot(actionPartSnapshot, userID);
		return (ActionPartSnapshotDTO) mappingService.mapESponderEntity(actionPartSnapshot, ActionPartSnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#updateActionPartSnapshot(eu.esponder.model.snapshot.action.ActionPartSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionPartSnapshot updateActionPartSnapshot(ActionPartSnapshot actionPartSnapshot, Long userID) {
		actionPartSnapshot = actionPartSnapshotCrudService.update(actionPartSnapshot);
		return actionPartSnapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#deleteActionRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActionRemote(Long actionDTOID, Long userID) {
		deleteAction(actionDTOID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#deleteAction(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteAction(Long actionID, Long userID) {
		actionCrudService.delete(Action.class, actionID);

	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#deleteActionPartRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActionPartRemote(Long actionPartDTOID, Long userID) {
		deleteActionPart(actionPartDTOID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#deleteActionPart(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActionPart(Long actionPartID, Long userID) {
		actionPartCrudService.delete(ActionPart.class, actionPartID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#deleteActionSnapshotRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActionSnapshotRemote(Long actionSnapshotDTOID, Long userID) {
		deleteActionSnapshot(actionSnapshotDTOID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#deleteActionSnapshot(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActionSnapshot(Long actionSnapshotID, Long userID) {
		actionSnapshotCrudService.delete(ActionSnapshot.class, actionSnapshotID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#deleteActionPartSnapshotRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActionPartSnapshotRemote(Long actionPartSnapshotDTOID, Long userID) {
		deleteActionPartSnapshot(actionPartSnapshotDTOID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#deleteActionPartSnapshot(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActionPartSnapshot(Long actionPartSnapshotID, Long userID) {
		actionPartSnapshotCrudService.delete(ActionPartSnapshot.class, actionPartSnapshotID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#createActionObjectiveRemote(eu.esponder.dto.model.crisis.action.ActionObjectiveDTO, java.lang.Long)
	 */
	@Override
	public ActionObjectiveDTO createActionObjectiveRemote(ActionObjectiveDTO actionObjectiveDTO, Long userID) {
		ActionObjective actionObjective = (ActionObjective) mappingService.mapESponderEntityDTO(actionObjectiveDTO, ActionObjective.class);
		actionObjective = createActionObjective(actionObjective, userID);
		actionObjectiveDTO = (ActionObjectiveDTO) mappingService.mapESponderEntity(actionObjective, ActionObjectiveDTO.class);
		return actionObjectiveDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#createActionObjective(eu.esponder.model.crisis.action.ActionObjective, java.lang.Long)
	 */
	@Override
	public ActionObjective createActionObjective(ActionObjective actionObjective, Long userID) {
		Action action = actionCrudService.find(Action.class, actionObjective.getAction().getId());
		if(action != null){
			actionObjective.setAction(action);
			actionObjective = actionObjectiveCrudService.create(actionObjective);
			return actionObjective;
		}
		return null;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#updateActionObjectiveRemote(eu.esponder.dto.model.crisis.action.ActionObjectiveDTO, java.lang.Long)
	 */
	@Override
	public ActionObjectiveDTO updateActionObjectiveRemote(ActionObjectiveDTO actionObjectiveDTO, Long userID) {
		ActionObjective actionObjective = (ActionObjective) mappingService.mapESponderEntityDTO(actionObjectiveDTO, ActionObjective.class);
		actionObjective = updateActionObjective(actionObjective, userID);
		actionObjectiveDTO = (ActionObjectiveDTO) mappingService.mapESponderEntity(actionObjective, ActionObjectiveDTO.class);
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#updateActionObjective(eu.esponder.model.crisis.action.ActionObjective, java.lang.Long)
	 */
	@Override
	public ActionObjective updateActionObjective( ActionObjective actionObjective, Long userID) {
		Action action = actionCrudService.find(Action.class, actionObjective.getAction().getId());
		if(action != null){
			actionObjective.setAction(action);
			actionObjective = actionObjectiveCrudService.update(actionObjective);
			return actionObjective;
		}
		return null;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#deleteActionObjectiveRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActionObjectiveRemote(Long actionObjectiveDTOId, Long userID) {
		deleteActionObjective(actionObjectiveDTOId, userID);
	}	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#deleteActionObjective(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActionObjective(Long actionObjectiveId, Long userID) {
		actionObjectiveCrudService.delete(ActionObjective.class, actionObjectiveId);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#createActionPartObjectiveRemote(eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO, java.lang.Long)
	 */
	@Override
	public ActionPartObjectiveDTO createActionPartObjectiveRemote( ActionPartObjectiveDTO actionPartObjectiveDTO, Long userID) {
		ActionPartObjective actionPartObjective = (ActionPartObjective) mappingService.mapESponderEntityDTO(actionPartObjectiveDTO, ActionPartObjective.class);
		actionPartObjective = createActionPartObjective(actionPartObjective, userID);
		actionPartObjectiveDTO = (ActionPartObjectiveDTO) mappingService.mapESponderEntity(actionPartObjective, ActionPartObjectiveDTO.class);
		return actionPartObjectiveDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#createActionPartObjective(eu.esponder.model.crisis.action.ActionPartObjective, java.lang.Long)
	 */
	@Override
	public ActionPartObjective createActionPartObjective( ActionPartObjective actionPartObjective, Long userID) {
		ActionPart actionPart = actionPartCrudService.find(ActionPart.class, actionPartObjective.getActionPart().getId());
		if(actionPart != null){
			actionPartObjective.setActionPart(actionPart);
			actionPartObjective = actionPartObjectiveCrudService.create(actionPartObjective);
			return actionPartObjective;
		}
		return null;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#updateActionPartObjectiveRemote(eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO, java.lang.Long)
	 */
	@Override
	public ActionPartObjectiveDTO updateActionPartObjectiveRemote( ActionPartObjectiveDTO actionPartObjectiveDTO, Long userID) {
		ActionPartObjective actionPartObjective = (ActionPartObjective) mappingService.mapESponderEntityDTO(actionPartObjectiveDTO, ActionPartObjective.class);
		actionPartObjective = updateActionPartObjective(actionPartObjective, userID);
		actionPartObjectiveDTO = (ActionPartObjectiveDTO) mappingService.mapESponderEntity(actionPartObjective, ActionPartObjectiveDTO.class);
		return actionPartObjectiveDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#updateActionPartObjective(eu.esponder.model.crisis.action.ActionPartObjective, java.lang.Long)
	 */
	@Override
	public ActionPartObjective updateActionPartObjective(ActionPartObjective actionPartObjective, Long userID) {
		ActionPart actionPart = actionPartCrudService.find(ActionPart.class, actionPartObjective.getActionPart().getId());
		if(actionPart != null){
			actionPartObjective.setActionPart(actionPart);
			actionPartObjective = actionPartObjectiveCrudService.update(actionPartObjective);
			return actionPartObjective;
		}
		return null;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionRemoteService#deleteActionPartObjectiveRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActionPartObjectiveRemote(Long actionPartObjectiveDTOId, Long userID) {
		deleteActionPartObjective(actionPartObjectiveDTOId, userID);
	}	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.action.ActionService#deleteActionPartObjective(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActionPartObjective(Long actionPartObjectiveId, Long userID) {
		actionPartObjectiveCrudService.delete(ActionPartObjective.class, actionPartObjectiveId);
	}

	// TODO Mechanism for safe persisting of snapshots, regardless of
	// when they were issued and arrived at the backend for persisting.

	/**
	 * Safe action snapshot persistence.
	 *
	 * @param actionSnapshot the action snapshot
	 */
	public void safeActionSnapshotPersistence(ActionSnapshot actionSnapshot) {
		Period consPeriod = actionSnapshot.getPeriod();



	}


}


