/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Resource;

// TODO: Auto-generated Javadoc
/**
 * The Interface ESponderResourceService.
 */
@Local
public interface ESponderResourceService extends ESponderResourceRemoteService {
	
	/**
	 * Find by id.
	 *
	 * @param clz the clz
	 * @param id the id
	 * @return the resource
	 */
	public Resource findByID(Class<? extends Resource> clz, Long id);

	/**
	 * Find by title.
	 *
	 * @param clz the clz
	 * @param title the title
	 * @return the resource
	 */
	public Resource findByTitle(Class<? extends Resource> clz, String title);

}
