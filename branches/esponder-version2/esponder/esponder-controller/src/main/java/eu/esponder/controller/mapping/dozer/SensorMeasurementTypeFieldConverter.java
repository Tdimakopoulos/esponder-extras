/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;
import eu.esponder.model.snapshot.status.MeasurementUnitEnum;
import eu.esponder.util.ejb.ServiceLocator;
import eu.esponder.util.logger.ESponderLogger;


// TODO: Auto-generated Javadoc
/**
 * The Class SensorMeasurementTypeFieldConverter.
 */
public class SensorMeasurementTypeFieldConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert(Object destination, 
			Object source, 
			Class<?> destinationClass,
			Class<?> sourceClass) {

		
		ESponderLogger.warn(this.getClass(), "SensorMeasurementUnitFieldConverter reached");
		//SensorDTO -> Sensor
		if (sourceClass == MeasurementUnitEnumDTO.class && source != null) {
			//TODO Implement this conversion
			MeasurementUnitEnum unit = (MeasurementUnitEnum) source;
			MeasurementUnitEnumDTO destUnitDTO = (MeasurementUnitEnumDTO) this.getMappingService().mapESponderEnumDTO(unit, destinationClass);
			destination = destUnitDTO;
		}
		//	measurementUnitEnumDTO --> SensorTypeDTO.measurementUnit
		else if(sourceClass == MeasurementUnitEnum.class && source != null) {
			MeasurementUnitEnumDTO unitDTO = (MeasurementUnitEnumDTO) source;
			MeasurementUnitEnum destUnit = (MeasurementUnitEnum) this.getMappingService().mapESponderEnumDTO(unitDTO, destinationClass);
			destination = destUnit;
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}