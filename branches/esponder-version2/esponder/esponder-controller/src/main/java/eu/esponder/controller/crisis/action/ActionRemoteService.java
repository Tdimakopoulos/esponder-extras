/*
 * 
 */
package eu.esponder.controller.crisis.action;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ActionRemoteService.
 */
@Remote
public interface ActionRemoteService {

	/**
	 * Find action dto by id.
	 *
	 * @param actionID the action id
	 * @return the action dto
	 */
	public ActionDTO findActionDTOById(Long actionID);

	/**
	 * Find action dto by title.
	 *
	 * @param title the title
	 * @return the action dto
	 */
	public ActionDTO findActionDTOByTitle(String title);

	/**
	 * Creates the action remote.
	 *
	 * @param actionDTO the action dto
	 * @param userID the user id
	 * @return the action dto
	 */
	public ActionDTO createActionRemote(ActionDTO actionDTO, Long userID);

	/**
	 * Update action remote.
	 *
	 * @param actionDTO the action dto
	 * @param userID the user id
	 * @return the action dto
	 */
	public ActionDTO updateActionRemote(ActionDTO actionDTO, Long userID);

	/**
	 * Delete action remote.
	 *
	 * @param actionDTOID the action dtoid
	 * @param userID the user id
	 */
	public void deleteActionRemote(Long actionDTOID, Long userID);

	/**
	 * Find action part dto by id.
	 *
	 * @param actionPartID the action part id
	 * @return the action part dto
	 */
	public ActionPartDTO findActionPartDTOById(Long actionPartID);

	/**
	 * Find action part dto by title.
	 *
	 * @param title the title
	 * @return the action part dto
	 */
	public ActionPartDTO findActionPartDTOByTitle(String title);

	/**
	 * Creates the action part remote.
	 *
	 * @param actionPartDTO the action part dto
	 * @param userID the user id
	 * @return the action part dto
	 */
	public ActionPartDTO createActionPartRemote(ActionPartDTO actionPartDTO, Long userID);

	/**
	 * Update action part remote.
	 *
	 * @param actionPartDTO the action part dto
	 * @param userID the user id
	 * @return the action part dto
	 */
	public ActionPartDTO updateActionPartRemote(ActionPartDTO actionPartDTO, Long userID);

	/**
	 * Delete action part remote.
	 *
	 * @param actionPartDTOID the action part dtoid
	 * @param userID the user id
	 */
	public void deleteActionPartRemote(Long actionPartDTOID, Long userID);

	/**
	 * Find action objective dto by title.
	 *
	 * @param title the title
	 * @return the action objective dto
	 */
	public ActionObjectiveDTO findActionObjectiveDTOByTitle(String title);

	/**
	 * Find action objective dto by id.
	 *
	 * @param actionID the action id
	 * @return the action objective dto
	 */
	public ActionObjectiveDTO findActionObjectiveDTOById(Long actionID);

	/**
	 * Creates the action objective remote.
	 *
	 * @param actionObjectiveDTO the action objective dto
	 * @param userID the user id
	 * @return the action objective dto
	 */
	public ActionObjectiveDTO createActionObjectiveRemote(ActionObjectiveDTO actionObjectiveDTO, Long userID);

	/**
	 * Update action objective remote.
	 *
	 * @param actionObjectiveDTO the action objective dto
	 * @param userID the user id
	 * @return the action objective dto
	 */
	public ActionObjectiveDTO updateActionObjectiveRemote(ActionObjectiveDTO actionObjectiveDTO, Long userID);

	/**
	 * Delete action objective remote.
	 *
	 * @param actionObjectiveDTOId the action objective dto id
	 * @param userID the user id
	 */
	public void deleteActionObjectiveRemote(Long actionObjectiveDTOId, Long userID);

	/**
	 * Find action part objective dto by id.
	 *
	 * @param actionID the action id
	 * @return the action part objective dto
	 */
	public ActionPartObjectiveDTO findActionPartObjectiveDTOById(Long actionID);

	/**
	 * Find action part objective dto by title.
	 *
	 * @param title the title
	 * @return the action part objective dto
	 */
	public ActionPartObjectiveDTO findActionPartObjectiveDTOByTitle(String title);

	/**
	 * Creates the action part objective remote.
	 *
	 * @param actionPartObjectiveDTO the action part objective dto
	 * @param userID the user id
	 * @return the action part objective dto
	 */
	public ActionPartObjectiveDTO createActionPartObjectiveRemote(ActionPartObjectiveDTO actionPartObjectiveDTO, Long userID);

	/**
	 * Update action part objective remote.
	 *
	 * @param actionPartObjectiveDTO the action part objective dto
	 * @param userID the user id
	 * @return the action part objective dto
	 */
	public ActionPartObjectiveDTO updateActionPartObjectiveRemote(ActionPartObjectiveDTO actionPartObjectiveDTO, Long userID);

	/**
	 * Delete action part objective remote.
	 *
	 * @param actionPartObjectiveDTOId the action part objective dto id
	 * @param userID the user id
	 */
	public void deleteActionPartObjectiveRemote(Long actionPartObjectiveDTOId, Long userID);

	/**
	 * Find action snapshot dto by id.
	 *
	 * @param actionSnapshotID the action snapshot id
	 * @return the action snapshot dto
	 */
	public ActionSnapshotDTO findActionSnapshotDTOById(Long actionSnapshotID);

	/**
	 * Find action part snapshot dto by id.
	 *
	 * @param actionPartSnapshotID the action part snapshot id
	 * @return the action part snapshot dto
	 */
	public ActionPartSnapshotDTO findActionPartSnapshotDTOById(Long actionPartSnapshotID);

	/**
	 * Creates the action snapshot remote.
	 *
	 * @param actionSnapshotDTO the action snapshot dto
	 * @param userID the user id
	 * @return the action snapshot dto
	 */
	public ActionSnapshotDTO createActionSnapshotRemote(ActionSnapshotDTO actionSnapshotDTO, Long userID);

	/**
	 * Creates the action part snapshot remote.
	 *
	 * @param actionPartSnapshotDTO the action part snapshot dto
	 * @param userID the user id
	 * @return the action part snapshot dto
	 */
	public ActionPartSnapshotDTO createActionPartSnapshotRemote(ActionPartSnapshotDTO actionPartSnapshotDTO, Long userID);

	/**
	 * Update action snapshot remote.
	 *
	 * @param actionSnapshotDTO the action snapshot dto
	 * @param userID the user id
	 * @return the action snapshot dto
	 */
	public ActionSnapshotDTO updateActionSnapshotRemote(ActionSnapshotDTO actionSnapshotDTO, Long userID);

	/**
	 * Update action part snapshot remote.
	 *
	 * @param actionPartSnapshotDTO the action part snapshot dto
	 * @param userID the user id
	 * @return the action part snapshot dto
	 */
	public ActionPartSnapshotDTO updateActionPartSnapshotRemote(ActionPartSnapshotDTO actionPartSnapshotDTO, Long userID);

	/**
	 * Delete action snapshot remote.
	 *
	 * @param actionSnapshotDTOID the action snapshot dtoid
	 * @param userID the user id
	 */
	public void deleteActionSnapshotRemote(Long actionSnapshotDTOID, Long userID);

	/**
	 * Delete action part snapshot remote.
	 *
	 * @param actionPartSnapshotDTOID the action part snapshot dtoid
	 * @param userID the user id
	 */
	public void deleteActionPartSnapshotRemote(Long actionPartSnapshotDTOID, Long userID);
}
