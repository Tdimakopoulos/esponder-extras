/*
 * 
 */
package eu.esponder.controller.mapping.bean;

import java.util.List;

import javax.ejb.Stateless;

import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.util.mapping.MappingService;
import eu.esponder.util.mapping.MappingServiceFactory;
import eu.esponder.util.mapping.MappingServiceFactory.MappingServiceType;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderMappingBean.
 */
@Stateless
public class ESponderMappingBean implements ESponderMappingService, ESponderRemoteMappingService {

	/** The Constant MAPPER. */
	private final static MappingService MAPPER = MappingServiceFactory.getMappingService(MappingServiceType.CONFIG);

	//	@EJB
	//	private ActorService actorService;

//	@EJB
//	private GenericService genericService;

	//	@Override
	//	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID) {
	//		OperationsCentre operationsCentre = operationsCentreService.findOperationCentreById(operationsCentreID);
	//		return null != operationsCentre ? MAPPER.transform(operationsCentre, OperationsCentreDTO.class) : null;
	//	}


	//	@Override
	//	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID,	Long userID) {
	//
	//		OperationsCentre operationsCentre = operationsCentreService.findOperationsCentreByUser(
	//				operationsCentreID, userID);
	//		return null != operationsCentre ? MAPPER.transform(operationsCentre,
	//				OperationsCentreDTO.class) : null;
	//	}

	//	@Override
	//	public List<OperationsCentreDTO> mapUserOperationsCentres(Long userID) {
	//
	//		ESponderUser user = userService.findUserById(userID);
	//		return null != user ? MAPPER.transform(user.getOperationsCentres(),
	//				OperationsCentreDTO.class) : null;
	//	}

	// NOTUSED
	//	@Override
	//	public List<SketchPOIDTO> mapOperationsCentreSketches(
	//			Long operationsCentreID, Long userID) {
	//
	//		return MAPPER.transform(
	//				operationsCentreService.findSketchPOI(operationsCentreID),
	//				SketchPOIDTO.class);
	//	}

	//	@Override
	//	public List<ReferencePOIDTO> mapOperationsCentreReferencePOIs(Long operationsCentreID, Long userID) {
	//
	//		return MAPPER.transform(operationsCentreService.findReferencePOIs(operationsCentreID),	ReferencePOIDTO.class);
	//	}

	//	@Override
	//	public OperationsCentreSnapshotDTO mapOperationsCentreSnapshot(Long operationsCentreID, Date maxDate) {
	//
	//		OperationsCentreSnapshot snapshot = operationsCentreService.findOperationsCentreSnapshotByDate(operationsCentreID, maxDate);
	//		return null != snapshot ? MAPPER.transform(snapshot,
	//				OperationsCentreSnapshotDTO.class) : null;
	//	}

	//	@Override
	//	public ActorSnapshotDTO mapActorSnapshot(Long actorID, Date maxDate) {
	//
	//		ActorSnapshot snapshot = actorService.findActorSnapshotByDate(actorID,	maxDate);
	//		return null != snapshot ? MAPPER.transform(snapshot, ActorSnapshotDTO.class) : null;
	//	}

	//	@Override
	//	public EquipmentSnapshotDTO mapEquipmentSnapshot(Long equipmentID,Date maxDate) {
	//
	//		EquipmentSnapshot snapshot = equipmentService.findEquipmentSnapshotByDate(
	//				equipmentID, maxDate);
	//		return null != snapshot ? MAPPER.transform(snapshot,
	//				EquipmentSnapshotDTO.class) : null;
	//	}

	//	@Override
	//	public SensorSnapshotDTO mapLatestSensorSnapshot(Long sensorID, Date maxDate) {
	//
	//		SensorSnapshot snapshot = sensorService.findSensorSnapshotByDate(sensorID, maxDate);
	//		return null != snapshot ? MAPPER.transform(snapshot,
	//				SensorSnapshotDTO.class) : null;
	//	}

	//	@Override
	//	public SketchPOI mapSketch(SketchPOIDTO sketchDTO) {
	//		return null != sketchDTO ? MAPPER.transform(sketchDTO, SketchPOI.class)	: null;
	//	}
	//
	//	@Override
	//	public SketchPOIDTO mapSketch(SketchPOI sketch) {
	//		return null != sketch ? MAPPER.transform(sketch, SketchPOIDTO.class) : null;
	//	}
	//
	//	@Override
	//	public SketchPOIDTO mapCreateSketch(OperationsCentre opCentre, SketchPOIDTO sketchDTO, Long userID) {
	//		SketchPOI sketch = mapSketch(sketchDTO);
	//		SketchPOIDTO sketchPOIDTO = MAPPER.transform(operationsCentreService.createSketchPOI(opCentre, sketch, userID), SketchPOIDTO.class);
	//		return sketchPOIDTO;
	//	}

	//	@Override
	//	public SketchPOIDTO mapSketchById(Long sketchPOIId) {
	//		return MAPPER.transform(operationsCentreService.findSketchPOIById(sketchPOIId), SketchPOIDTO.class);
	//	}

	//	@Override
	//	public SketchPOIDTO mapSketchByTitle(String sketchPOITitle) {
	//		return MAPPER.transform(operationsCentreService.findSketchPOIByTitle(sketchPOITitle), SketchPOIDTO.class);
	//		//		return null != sketch ? MAPPER.transform(sketch, SketchPOIDTO.class) : null;
	//	}

	//	@Override
	//	public ReferencePOI mapReferencePOI(ReferencePOIDTO referencePOIDTO) {
	//		return null != referencePOIDTO ? MAPPER.transform(referencePOIDTO,
	//				ReferencePOI.class) : null;
	//	}

	//	@Override
	//	public ReferencePOIDTO mapReferencePOIDTO(ReferencePOI referencePOI) {
	//		return null != referencePOI ? MAPPER.transform(referencePOI,
	//				ReferencePOIDTO.class) : null;
	//	}

	//	@Override
	//	public ActorDTO mapActor(Long actorID) {
	//		Actor actor = actorService.findById(actorID);
	//		return null != actor ? MAPPER.transform(actor, ActorDTO.class) : null;
	//	}

	//	@Override
	//	public List<ActorDTO> mapActorSubordinates(Long actorID) {
	//		List<Actor> actorSubordinates = actorService.findSubordinatesById(actorID);			
	//		return null != actorSubordinates ? MAPPER.transform(actorSubordinates, ActorDTO.class) : null;
	//	}

	//	@Override
	//	public ActorDTO mapActor(String actorTitle) {
	//		Actor actor = actorService.findByTitle(actorTitle);
	//		return null != actor ? MAPPER.transform(actor, ActorDTO.class) : null;
	//	}

	// create Actor
	//	@Override
	//	public ActorDTO createActor(Actor actor, Long userID) {
	//		actor = actorService.createActor(actor, userID);
	//		return null != actor ? MAPPER.transform(actor, ActorDTO.class) : null;
	//	}

	// update Actor
	//	@Override
	//	public ActorDTO updateActor(Actor actor, Long userID) {
	//		actor = actorService.updateActor(actor, userID);
	//		return null != actor ? MAPPER.transform(actor, ActorDTO.class) : null;
	//	}


	//	@Override
	//	public void deleteActor(Long actorID, Long userID) {
	//		// TODO Auto-generated method stub
	//
	//	}



	//FIXME TRANSFERRED TO GENERIC BEAN
	//		REMOVE WHEN ALL IS TESTED TO BE WORKING
//	@Override
//	public EsponderQueryRestriction mapCriteriaCollection(EsponderQueryRestrictionDTO criteria) {
//		if (criteria instanceof EsponderCriterionDTO) {
//			return null != criteria ? MAPPER.transform(criteria, EsponderCriterion.class) : null;
//		} else if (criteria instanceof EsponderIntersectionCriteriaCollectionDTO) {
//			return null != criteria ? MAPPER.transform(criteria, EsponderIntersectionCriteriaCollection.class) : null;
//		} else if (criteria instanceof EsponderUnionCriteriaCollectionDTO) {
//			return null != criteria ? MAPPER.transform(criteria, EsponderUnionCriteriaCollection.class) : null;
//		} else if (criteria instanceof EsponderNegationCriteriaCollectionDTO) {
//			return null != criteria ? MAPPER.transform(criteria, EsponderNegationCriteriaCollection.class) : null;
//		} else {
//			ESponderLogger.debug(this.getClass(), "Unknown Criteria Collection, cannot proceed with mapping");
//			return null;
//		}
//	}


	//FIXME TRANSFERRED TO GENERIC BEAN
	//		REMOVE WHEN ALL IS TESTED TO BE WORKING
//	public EsponderCriterion mapSimpleCriterion(EsponderCriterionDTO criteria) {
//		return null != criteria ? MAPPER.transform(criteria,
//				EsponderCriterion.class) : null;
//	}
	
	
//	USED in CrisisEntityResource previously
	
//	@SuppressWarnings("unchecked")
//	public ESponderEntityDTO mapGenericCreateEntity(ESponderEntityDTO entityDTO, Long userID) throws ClassNotFoundException {
//
//		Class<? extends ESponderEntityDTO> baseClass = entityDTO.getClass();
//		String targetClassStr = getManagedEntityName(baseClass.getName());
//		Class<? extends ESponderEntity<Long>> targetClass = (Class<? extends ESponderEntity<Long>>) Class.forName(targetClassStr);
//		ESponderEntity<Long> entity = (ESponderEntity<Long>) this.mapESponderEntityDTO(entityDTO, targetClass);
//		if (entity == null) {
//			ESponderLogger.debug(this.getClass(), "The entity returned was null");
//			return null;
//		} else {
//			entity = (ESponderEntity<Long>) genericService.createEntity(entity,	userID);
//			ESponderEntityDTO createdEntityDTO = this.mapESponderEntity((ESponderEntity<Long>) entity, baseClass);
//			return createdEntityDTO;
//		}
//	}


	
	
//		USED in CrisisEntityResource previously
	
	//	@SuppressWarnings("unchecked")
//	public ESponderEntityDTO mapGenericUpdateEntity( ESponderEntityDTO entityDTO, Long userID) throws ClassNotFoundException {
//
//		Class<? extends ESponderEntityDTO> baseClass = entityDTO.getClass();
//		String targetClassStr = getManagedEntityName(baseClass.getName());
//		long entityDTOID = entityDTO.getId();
//		Class<? extends ESponderEntity<Long>> targetClass = (Class<? extends ESponderEntity<Long>>) Class.forName(targetClassStr);
//		/*
//		 * We retrieve the persisted Object once again so as to map the dto
//		 * object to domain object directly and retrieve all the ActionAudit
//		 * fields from the persisted entity
//		 */
//		ESponderEntity<Long> dest = genericService.getEntity(targetClass, entityDTOID);
//		ESponderEntity<Long> entity = (ESponderEntity<Long>) this.mapESponderEntityDTOWithObject(entityDTO, dest);
//
//		if (entity == null) {
//			System.out.println("The entity returned was null");
//			return null;
//		} else {
//			entity = (ESponderEntity<Long>) genericService.updateEntity(entity, userID);
//			ESponderEntityDTO createdEntityDTO = this.mapESponderEntity( (ESponderEntity<Long>) entity, baseClass);
//			return createdEntityDTO;
//		}
//	}

	
//	USED in CrisisEntityResource previously
	
//	@SuppressWarnings("unchecked")
//	public void mapGenericDeleteEntity(String entityClassStr, Long entityID)
//			throws ClassNotFoundException {
//		Class<? extends ESponderEntity<Long>> entityClass = (Class<? extends ESponderEntity<Long>>) Class.forName(entityClassStr);
//		genericService.deleteEntity(entityClass, entityID);
//	}
	
	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapESponderEntity(eu.esponder.model.ESponderEntity, java.lang.Class)
	 */
	public ESponderEntityDTO mapESponderEntity(ESponderEntity<Long> entity,	Class<? extends ESponderEntityDTO> targetClass) {
		if (targetClass != null) {
			ESponderEntityDTO e = (null != entity ? MAPPER.transform(entity, targetClass) : null);
			return  e;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapESponderEntity(java.util.List, java.lang.Class)
	 */
	public List<? extends ESponderEntityDTO> mapESponderEntity( List<? extends ESponderEntity<Long>> resultsList, Class<? extends ESponderEntityDTO> targetClass) {
		if (targetClass != null) {
			return (List<? extends ESponderEntityDTO>) (null != resultsList ? MAPPER.transform(resultsList, targetClass) : null);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapESponderEntityDTO(java.util.List, java.lang.Class)
	 */
	public List<? extends ESponderEntity<?>> mapESponderEntityDTO(List<? extends ESponderEntityDTO> resultsList, Class<? extends ESponderEntity<?>> targetClass) {
		if (targetClass != null) {
			return (List<? extends ESponderEntity<?>>) (null != resultsList ? MAPPER.transform(resultsList, targetClass) : null);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapESponderEntityDTO(eu.esponder.dto.model.ESponderEntityDTO, java.lang.Class)
	 */
	public ESponderEntity<?> mapESponderEntityDTO(ESponderEntityDTO entity,	Class<? extends ESponderEntity<?>> targetClass) {
		if (targetClass != null) {
			return (null != entity ? MAPPER.transform(entity, targetClass) : null);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapObject(java.lang.Object, java.lang.Class)
	 */
	@Override
	public Object mapObject(Object obj, Class<?> targetClass) {
		if (targetClass != null) {
			return (null != obj ? MAPPER.transform(obj, targetClass) : null);
		}
		return null;
	}


	/**
	 * Map e sponder entity dto with object.
	 *
	 * @param src the src
	 * @param dest the dest
	 * @return the e sponder entity
	 */
	public ESponderEntity<?> mapESponderEntityDTOWithObject(ESponderEntityDTO src, ESponderEntity<Long> dest) {
		MAPPER.transform(src, dest);
		return dest;
	}



	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#getEntityClass(java.lang.String)
	 */
	public Class<?> getEntityClass(String className) throws ClassNotFoundException {
		return Class.forName(className);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#getManagedEntityName(java.lang.String)
	 */
	public String getManagedEntityName(String queriedEntityDTO) {
		String queriedEntity = queriedEntityDTO.replaceAll("dto.", "");
		queriedEntity = queriedEntity.replaceAll("DTO", "");
		return queriedEntity;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#getDTOEntityName(java.lang.String)
	 */
	public String getDTOEntityName(String entityName) {
		String dtoEntityName = entityName.replaceAll("eu.esponder", "eu.esponder.dto");
		dtoEntityName = dtoEntityName.concat("DTO");
		return dtoEntityName;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapESponderEnumDTO(java.lang.Object, java.lang.Class)
	 */
	@Override
	public Object mapESponderEnumDTO(Object entity, Class<?> targetClass) {
		if (targetClass != null) {
			return (null != entity ? MAPPER.transform(entity, targetClass) : null);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#getManagedEntityClass(java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	public Class<? extends ESponderEntity<Long>> getManagedEntityClass(Class<? extends ESponderEntityDTO> clz) throws ClassNotFoundException{
		String className = this.getManagedEntityName(clz.getName()); 
		return (Class<? extends ESponderEntity<Long>>) this.getEntityClass(className);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#getDTOEntityClass(java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	public Class<? extends ESponderEntityDTO> getDTOEntityClass(Class<? extends ESponderEntity<Long>> clz) throws ClassNotFoundException{
		String className = this.getDTOEntityName(clz.getName()); 
		return (Class<? extends ESponderEntityDTO>) this.getEntityClass(className);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapEntityToEntity(eu.esponder.model.ESponderEntity, eu.esponder.model.ESponderEntity)
	 */
	@Override
	public void mapEntityToEntity(ESponderEntity<Long> src, ESponderEntity<Long> dest) {
		MAPPER.transform(src, dest);
	}

}
