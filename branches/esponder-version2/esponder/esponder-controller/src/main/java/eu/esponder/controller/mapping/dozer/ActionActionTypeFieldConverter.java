/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import org.dozer.CustomConverter;

import eu.esponder.model.type.OperationalActionType;
import eu.esponder.model.type.StrategicActionType;
import eu.esponder.model.type.TacticalActionType;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionActionTypeFieldConverter.
 */
public class ActionActionTypeFieldConverter implements CustomConverter {

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert(Object destination,
			Object source, Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == OperationalActionType.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == StrategicActionType.class) {
			destination = source;
			return destination;
		}
		
		if (sourceClass == TacticalActionType.class) {
			destination = source;
			return destination;
		}
		
		return null;
	}

}
