/*
 * 
 */
package eu.esponder.controller.crisis.resource.category.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.OrganisationService;
import eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService;
import eu.esponder.controller.crisis.resource.category.ResourceCategoryService;
import eu.esponder.controller.generic.GenericService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.EquipmentCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OperationsCentreCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PlannableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.dto.model.type.OperationsCentreTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.dto.model.type.RankTypeDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.Organisation;
import eu.esponder.model.crisis.resource.PersonnelCompetence;
import eu.esponder.model.crisis.resource.category.ConsumableResourceCategory;
import eu.esponder.model.crisis.resource.category.EquipmentCategory;
import eu.esponder.model.crisis.resource.category.OperationsCentreCategory;
import eu.esponder.model.crisis.resource.category.OrganisationCategory;
import eu.esponder.model.crisis.resource.category.PersonnelCategory;
import eu.esponder.model.crisis.resource.category.PlannableResourceCategory;
import eu.esponder.model.crisis.resource.category.ResourceCategory;
import eu.esponder.model.crisis.resource.category.ReusableResourceCategory;
import eu.esponder.model.type.ConsumableResourceType;
import eu.esponder.model.type.DisciplineType;
import eu.esponder.model.type.EquipmentType;
import eu.esponder.model.type.OperationsCentreType;
import eu.esponder.model.type.OrganisationType;
import eu.esponder.model.type.RankType;
import eu.esponder.model.type.ReusableResourceType;
import eu.esponder.util.logger.ESponderLogger;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceCategoryBean.
 */
@Stateless
public class ResourceCategoryBean implements ResourceCategoryService, ResourceCategoryRemoteService {

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;

	/** The crud service. */
	@EJB
	private CrudService<ResourceCategory> crudService;

	/** The personnel crud service. */
	@EJB
	private CrudService<PersonnelCategory> personnelCrudService;
	
	/** The organisation service. */
	@EJB
	private OrganisationService organisationService;
	
	/** The type service. */
	@EJB
	private TypeService typeService;
	
	/** The generic service. */
	@EJB
	private GenericService genericService;

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#create(eu.esponder.dto.model.crisis.resource.category.ResourceCategoryDTO, java.lang.Long)
	 */
	@Override
	public ResourceCategoryDTO create(ResourceCategoryDTO resourceCategoryDTO, Long userID) throws ClassNotFoundException {
		Class<? extends ESponderEntity<Long>> targetClass = (Class<? extends ESponderEntity<Long>>) mappingService.getManagedEntityClass(resourceCategoryDTO.getClass());
		ResourceCategory resourceCategory = (ResourceCategory) mappingService.mapESponderEntityDTO(resourceCategoryDTO, targetClass);
		ResourceCategory resourceCategoryPersisted = this.findByCategory(resourceCategory); 
		if (null == resourceCategoryPersisted) {
			resourceCategoryPersisted = this.create(resourceCategory, userID);
		}
		Class<? extends ESponderEntityDTO> targetDTOClass = resourceCategoryDTO.getClass();
		resourceCategoryDTO = (ResourceCategoryDTO) mappingService.mapESponderEntity(resourceCategoryPersisted, targetDTOClass);
		return resourceCategoryDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#create(eu.esponder.model.crisis.resource.category.ResourceCategory, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ResourceCategory create(ResourceCategory resourceCategory, Long userID) {
		return crudService.create(resourceCategory);
	}

	/**
	 * Find by category.
	 *
	 * @param resourceCategory the resource category
	 * @return the resource category
	 */
	public ResourceCategory findByCategory(ResourceCategory resourceCategory) {
		
		if (resourceCategory instanceof OrganisationCategory) {
			return findOrganisationCategoryByType(((OrganisationCategory) resourceCategory).getDisciplineType(), ((OrganisationCategory) resourceCategory).getOrganisationType());
		}
		else if (resourceCategory instanceof OperationsCentreCategory) {
			return findOperationsCentreCategoryByType(((OperationsCentreCategory) resourceCategory).getOperationsCentreType());
		}
		else if (resourceCategory instanceof PersonnelCategory) {
			return findPersonnelCategoryByType(((PersonnelCategory) resourceCategory).getRank(), ((PersonnelCategory) resourceCategory).getPersonnelCompetences(),
					((PersonnelCategory) resourceCategory).getOrganisationCategory());

		}
		else if (resourceCategory instanceof EquipmentCategory) {
			return findEquipmentCategoryByType(((EquipmentCategory) resourceCategory).getEquipmentType());
		}
		else if (resourceCategory instanceof ConsumableResourceCategory) {
			return findConsumableCategoryByType(((ConsumableResourceCategory) resourceCategory).getConsumableResourceType());
		}
		else if (resourceCategory instanceof ReusableResourceCategory) {
			return findReusableCategoryByType(((ReusableResourceCategory) resourceCategory).getReusableResourceType());
		}
		else
			throw new RuntimeException("Cannot handle this type. BUG!!!");
	}

	//TODO Remove it, when the other used instead is tested to work...
//	public OperationsCentreCategory findByOperationsCentreType(OperationsCentreType operationsCentreType) {
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		parameters.put("operationsCentreType", operationsCentreType);
//		List<ResourceCategory> list = crudService.findWithNamedQuery("OperationsCentreCategory.findByType", parameters);
//		/*
//		 * TODO: refactor the following based on the instructions included in findByType(OPerationsCentreType)
//		 */
//		if (list.size() == 0) {
//			return null;
//		} else if ((list.size() == 1)) {
//			return (OperationsCentreCategory) list.get(0);
//		} else {
//			throw new RuntimeException("Cannot handle this type. BUG!!!");
//		}
//	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#findByIdRemote(java.lang.Class, java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public PlannableResourceCategoryDTO findByIdRemote(Class<? extends ResourceCategoryDTO> clz, Long resourceCategoryID) {

		try {
			Class<? extends ResourceCategory> targetClass = (Class<? extends ResourceCategory>) mappingService.getManagedEntityClass(clz);
			PlannableResourceCategory resultCategory = (PlannableResourceCategory) findById(targetClass, resourceCategoryID);
			if(resultCategory != null)
				return (PlannableResourceCategoryDTO) mappingService.mapESponderEntity(resultCategory, mappingService.getDTOEntityClass(resultCategory.getClass()));
			else
				return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#findOrganisationCategoryDTOByType(eu.esponder.dto.model.type.DisciplineTypeDTO, eu.esponder.dto.model.type.OrganisationTypeDTO)
	 */
	@Override
	public OrganisationCategoryDTO findOrganisationCategoryDTOByType(DisciplineTypeDTO disciplineType, OrganisationTypeDTO organizationType) {

		DisciplineType discipType = (DisciplineType) mappingService.mapESponderEntityDTO(disciplineType, DisciplineType.class);
		OrganisationType organType = (OrganisationType) mappingService.mapESponderEntityDTO(organizationType, OrganisationType.class);
		System.out.println("\n\nCriteria: "+disciplineType.getTitle()+" : "+organType.getTitle());
		OrganisationCategory targetOrganisationCategory = findOrganisationCategoryByType(discipType, organType);
		if(targetOrganisationCategory != null) {
			System.out.println("\n\nOrganisationCategoryFound\n\n");
			return (OrganisationCategoryDTO) mappingService.mapESponderEntity(targetOrganisationCategory, OrganisationCategoryDTO.class);
		}
		else {
			System.out.println("\n\n ***** NO ***** OrganisationCategoryFound\n\n");
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#findEquipmentCategoryDTOByType(eu.esponder.dto.model.type.EquipmentTypeDTO)
	 */
	@Override
	public EquipmentCategoryDTO findEquipmentCategoryDTOByType(EquipmentTypeDTO equipmentTypeDTO) {
		EquipmentType equipmentType  = (EquipmentType) mappingService.mapESponderEntityDTO(equipmentTypeDTO, EquipmentType.class);
		EquipmentCategory equipmentCategory = findEquipmentCategoryByType(equipmentType);
		if(equipmentCategory != null)
			return (EquipmentCategoryDTO) mappingService.mapESponderEntity(equipmentCategory, EquipmentCategoryDTO.class);
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#findConsumableCategoryDTOByType(eu.esponder.dto.model.type.ConsumableResourceTypeDTO)
	 */
	@Override
	public ConsumableResourceCategoryDTO findConsumableCategoryDTOByType(
			ConsumableResourceTypeDTO consumablesType) {

		ConsumableResourceType consumableResourceType  = (ConsumableResourceType) mappingService.mapESponderEntityDTO(consumablesType, ConsumableResourceType.class);
		ConsumableResourceCategory consumableCategory = findConsumableCategoryByType(consumableResourceType);
		if(consumableCategory != null)
			return (ConsumableResourceCategoryDTO) mappingService.mapESponderEntity(consumableCategory, ConsumableResourceCategoryDTO.class);
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#findReusableCategoryDTOByType(eu.esponder.dto.model.type.ReusableResourceTypeDTO)
	 */
	@Override
	public ReusableResourceCategoryDTO findReusableCategoryDTOByType(ReusableResourceTypeDTO reusablesType) {
		/*
		 * 1. Transform
		 * 2. call @Local
		 * 3. Transform to DTO
		 */
		ReusableResourceType reusableResourceType  = (ReusableResourceType) mappingService.mapESponderEntityDTO(reusablesType, ReusableResourceType.class);
		ReusableResourceCategory reusableCategory = findReusableCategoryByType(reusableResourceType);
		if(reusableCategory != null)
			return (ReusableResourceCategoryDTO) mappingService.mapESponderEntity(reusableCategory, ReusableResourceCategoryDTO.class);
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#findOperationsCentreCategoryDTOByType(eu.esponder.dto.model.type.OperationsCentreTypeDTO)
	 */
	@Override
	public OperationsCentreCategoryDTO findOperationsCentreCategoryDTOByType(OperationsCentreTypeDTO operationsCentreTypeDTO) {
		/*
		 * 1. Transform
		 * 2. call @Local
		 * 3. Transform to DTO
		 */
		OperationsCentreType operationsCentreType  = (OperationsCentreType) mappingService.mapESponderEntityDTO(operationsCentreTypeDTO, OperationsCentreType.class);
		OperationsCentreCategory operationsCentreCategory = findOperationsCentreCategoryByType(operationsCentreType);
		if(operationsCentreCategory != null)
			return (OperationsCentreCategoryDTO) mappingService.mapESponderEntity(operationsCentreCategory, OperationsCentreCategoryDTO.class);
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#findPersonnelCategoryDTOByType(eu.esponder.dto.model.type.RankTypeDTO, java.util.Set, eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public PersonnelCategoryDTO findPersonnelCategoryDTOByType(RankTypeDTO rankType,	Set<PersonnelCompetenceDTO> competence,	OrganisationCategoryDTO organisationCategory) {
		RankType rankTypeModel = (RankType) mappingService.mapESponderEntityDTO(rankType, RankType.class);
		Set<PersonnelCompetence> competenceModel = (Set<PersonnelCompetence>) mappingService.mapESponderEntityDTO((List<? extends ESponderEntityDTO>) competence, PersonnelCompetence.class);
		OrganisationCategory organCategory = (OrganisationCategory) mappingService.mapESponderEntityDTO(organisationCategory, OrganisationCategory.class);
		PersonnelCategory personnelCategory = findPersonnelCategoryByType(rankTypeModel, competenceModel, organCategory);
		if(personnelCategory != null)
			return (PersonnelCategoryDTO) mappingService.mapESponderEntity(personnelCategory, PersonnelCategoryDTO.class);
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#findById(java.lang.Class, java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ResourceCategory findById(Class<? extends ResourceCategory> clz, Long resourceCategoryID) {
		return crudService.find((Class<ResourceCategory>) clz, resourceCategoryID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#findOrganisationCategoryByType(eu.esponder.model.type.DisciplineType, eu.esponder.model.type.OrganisationType)
	 */
	@Override
	public OrganisationCategory findOrganisationCategoryByType(DisciplineType disciplineType, OrganisationType organisationType) {

		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("disciplineType", disciplineType);
		parameters.put("organisationType", organisationType);
		OrganisationCategory organisationCategory= (OrganisationCategory) crudService.findSingleWithNamedQuery("OrganisationCategory.findByType", parameters);
		if(organisationCategory != null)
			return organisationCategory;
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#findEquipmentCategoryByType(eu.esponder.model.type.EquipmentType)
	 */
	@Override
	public EquipmentCategory findEquipmentCategoryByType(EquipmentType equipmentType) {
		/*
		 * find EquipmentCategory with equipmentType 
		 * otherwise return null 
		 * (create NamedQuery)
		 */
		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("equipmentType", equipmentType);
		EquipmentCategory equipmentCategory= (EquipmentCategory) crudService.findSingleWithNamedQuery("EquipmentCategory.findByType", parameters);
		if(equipmentCategory != null)
			return equipmentCategory;
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#findConsumableCategoryByType(eu.esponder.model.type.ConsumableResourceType)
	 */
	@Override
	public ConsumableResourceCategory findConsumableCategoryByType(ConsumableResourceType consumablesType) {

		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("consumableResourceType", consumablesType);
		ConsumableResourceCategory consumableCategory = (ConsumableResourceCategory) crudService.findSingleWithNamedQuery("ConsumableResourceCategory.findByType", parameters);
		if(consumableCategory != null)
			return consumableCategory;
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#findReusableCategoryByType(eu.esponder.model.type.ReusableResourceType)
	 */
	@Override
	public ReusableResourceCategory findReusableCategoryByType(ReusableResourceType reusablesType) {

		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("reusableResourceType", reusablesType);
		ReusableResourceCategory reusableCategory = (ReusableResourceCategory) crudService.findSingleWithNamedQuery("ReusableResourceCategory.findByType", parameters);
		if(reusableCategory != null)
			return reusableCategory;
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#findOperationsCentreCategoryByType(eu.esponder.model.type.OperationsCentreType)
	 */
	@Override
	public OperationsCentreCategory findOperationsCentreCategoryByType(OperationsCentreType operationsCentreType) {
		/*OperationsCentreCategory.findByType
		 * find ReusableResourceCategory with reusablesType 
		 * otherwise return null 
		 * (create NamedQuery) -> see findByOperationsCentreType()
		 */
		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("operationsCentreType", operationsCentreType);
		OperationsCentreCategory operationsCentreCategory = (OperationsCentreCategory) crudService.findSingleWithNamedQuery("OperationsCentreCategory.findByType", parameters);
		if(operationsCentreCategory != null)
			return operationsCentreCategory;
		else
			return null;
	}

	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#findPersonnelCategoryByType(eu.esponder.model.type.RankType, java.util.Set, eu.esponder.model.crisis.resource.category.OrganisationCategory)
	 */
	@Override
	public PersonnelCategory findPersonnelCategoryByType(RankType rankType, Set<PersonnelCompetence> personnelCompetence, OrganisationCategory organisationCategory) {
		/*
		 * find exact match for PersonnelCategory with rankType AND personnelCompetences and organizationCategory
		 * otherwise return null (if null is returned then a new personnel Category will be created by the application
		 */
		List<PersonnelCategory> categoryList = new ArrayList<PersonnelCategory>();
		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("rankType", rankType);
		parameters.put("organisationCategory", organisationCategory);
		List<PersonnelCategory> intermediateResults = personnelCrudService.findWithNamedQuery("PersonnelCategory.findByType", parameters);
		
		
		for (PersonnelCategory resultCategory : intermediateResults) {
			
			if(resultCategory.getPersonnelCompetences().size() == personnelCompetence.size()) {
				
				boolean exists = false;
				for(PersonnelCompetence testCompetence : resultCategory.getPersonnelCompetences()) {
					if(ContainsPersonnelCompetence(testCompetence, personnelCompetence)) {
						exists = true;
					}
					else{
						exists = false;
						break;
					}
				}
				if(exists) {
					categoryList.add(resultCategory);
				}
			}
			else {
				ESponderLogger.info(this.getClass(), "Category does not have same size as the requested.");
			}
		}
		
		if(categoryList.size() == 0) {
			ESponderLogger.info(this.getClass(), "No category has been found.");
		}
		else if(categoryList.size() > 1) {
			ESponderLogger.info(this.getClass(), "Multiple categories found.");
			return categoryList.get(0);
		}
		else if(categoryList.size() == 1) {
			return categoryList.get(0);
		}
		return null;
	}
	
	
	/**
	 * Contains personnel competence.
	 *
	 * @param competence the competence
	 * @param competenceList the competence list
	 * @return true, if successful
	 */
	private boolean ContainsPersonnelCompetence(PersonnelCompetence competence, Set<PersonnelCompetence> competenceList) {
		boolean exists = false;
		for(PersonnelCompetence p : competenceList) {
			System.out.println("ListID : "+p.getId()+"\nComp: "+competence.getId());
			if(p.getId().toString().equalsIgnoreCase(competence.getId().toString())) {
				return true;
			}
		}
		
		return false;
	}
	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#updateOrganizationCategory(java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public OrganisationCategoryDTO updateOrganizationCategory(Long organisationCategoryId, Long disciplineTypeId, Long organisationTypeId,  Long organisationID, Long userId) {
	//FIXME add support for organisatition	
		OrganisationCategory organisationCategory = (OrganisationCategory) findById(OrganisationCategory.class, organisationCategoryId);
		if(organisationCategory != null) {
			DisciplineType disciplineType = (DisciplineType) typeService.findById(disciplineTypeId);
			OrganisationType organisationType = (OrganisationType) typeService.findById(organisationTypeId);
			Organisation organisation = organisationService.findById(organisationID);
			OrganisationCategory organisationCategoryPersisted = updateOrganizationCategory(organisationCategory, disciplineType, organisationType, organisation, userId);
			return (OrganisationCategoryDTO) mappingService.mapESponderEntity(organisationCategoryPersisted, OrganisationCategoryDTO.class);
		}
		else
			return null;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#updateOperationsCentreCategory(java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public OperationsCentreCategoryDTO updateOperationsCentreCategory(Long operationsCentreCategoryID, Long operationsCentreTypeId, Long userID) {
	//FIXME add support for organisatition	
		OperationsCentreCategory operationsCentreCategory = (OperationsCentreCategory) findById(OperationsCentreCategory.class, operationsCentreCategoryID);
		if(operationsCentreCategory != null) {
			OperationsCentreType operationsCentreType = (OperationsCentreType) typeService.findById(operationsCentreTypeId);
			OperationsCentreCategory operationsCentreCategoryPersisted = updateOperationsCentreCategory(operationsCentreCategory, userID);
			return (OperationsCentreCategoryDTO) mappingService.mapESponderEntity(operationsCentreCategoryPersisted, OperationsCentreCategoryDTO.class);
		}
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#updateEquipmentCategory(java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public EquipmentCategoryDTO updateEquipmentCategory(Long equipmentCategoryId, Long equipmentTypeId, Long userId) {
		
		EquipmentCategory equipmentCategory = (EquipmentCategory) findById(EquipmentCategory.class, equipmentCategoryId);
		if(equipmentCategory != null){
			EquipmentType equipmentType = (EquipmentType) typeService.findById(equipmentTypeId);
			EquipmentCategory equipmentCategoryPersisted = updateEquipmentCategory(equipmentCategory, equipmentType, userId);
			return (EquipmentCategoryDTO) mappingService.mapESponderEntity(equipmentCategoryPersisted, EquipmentCategoryDTO.class);
		}
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#updateConsumableResourceCategory(java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public ConsumableResourceCategoryDTO updateConsumableResourceCategory( Long consumableCategoryId, Long consumableTypeId, Long userId) {
		
		ConsumableResourceCategory consumableResourceCategory = (ConsumableResourceCategory) findById(ConsumableResourceCategory.class, consumableCategoryId);
		if(consumableResourceCategory != null) {
			ConsumableResourceType consumableResourceType = (ConsumableResourceType) typeService.findById(consumableTypeId);
			ConsumableResourceCategory consumableResourceCategoryPersisted = updateConsumableResourceCategory(consumableResourceCategory, consumableResourceType, userId);
			return (ConsumableResourceCategoryDTO) mappingService.mapESponderEntity(consumableResourceCategoryPersisted, ConsumableResourceCategoryDTO.class);			
		}
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#updateReusableResourceCategory(java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public ReusableResourceCategoryDTO updateReusableResourceCategory(Long reusableCategoryId, Long reusableTypeId, Long userId) {
		
		ReusableResourceCategory reusableresourceCategory = (ReusableResourceCategory) findById(ReusableResourceCategory.class, reusableCategoryId);
		if(reusableresourceCategory != null) {
			ReusableResourceType reusableResourceType = (ReusableResourceType) typeService.findById(reusableTypeId);
			ReusableResourceCategory reusableResourceCategoryPersisted = updateReusableResourceCategory(reusableresourceCategory, reusableResourceType, userId);
			return (ReusableResourceCategoryDTO) mappingService.mapESponderEntity(reusableResourceCategoryPersisted, ReusableResourceCategoryDTO.class);			
		}
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#updatePersonnelCategory(java.lang.Long, java.util.Set, java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public PersonnelCategoryDTO updatePersonnelCategory( Long personnelCategoryId, Set<Long> competenceId, Long rankId, Long organisationCategoryId, Long userId) throws InstantiationException, IllegalAccessException {
		
		PersonnelCategory personnelCategory = (PersonnelCategory) findById(PersonnelCategory.class, personnelCategoryId);
		if(personnelCategory != null) {
			RankType rankType = (RankType) typeService.findById(rankId);
			OrganisationCategory organisationCategory = (OrganisationCategory) findById(OrganisationCategory.class, organisationCategoryId);
			
			EsponderQueryRestriction restriction = genericService.createCriteriaforBulkFindById(competenceId);
			Set<PersonnelCompetence> competenceSet =  (Set<PersonnelCompetence>) genericService.getEntities(PersonnelCompetence.class, restriction, 0, 0);
			
			PersonnelCategory personnelCategoryPersisted = updatePersonnelCategory(personnelCategory, competenceSet, rankType, organisationCategory, userId);
			return (PersonnelCategoryDTO) mappingService.mapESponderEntity(personnelCategoryPersisted, PersonnelCategoryDTO.class);
		}
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#updateOrganizationCategory(eu.esponder.model.crisis.resource.category.OrganisationCategory, eu.esponder.model.type.DisciplineType, eu.esponder.model.type.OrganisationType, eu.esponder.model.crisis.resource.Organisation, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OrganisationCategory updateOrganizationCategory(OrganisationCategory organisationCategory, DisciplineType disciplineType, OrganisationType organizationType,Organisation organisation, Long userId) {
		OrganisationCategory organisationCategoryTarget = (OrganisationCategory) findById(OrganisationCategory.class, organisationCategory.getId());
		organisationCategory.setDisciplineType(disciplineType);
		organisationCategory.setOrganisationType(organizationType);
		organisationCategory.setOrganisation(organisation);
		mappingService.mapEntityToEntity(organisationCategory, organisationCategoryTarget);
		return (OrganisationCategory) crudService.update(organisationCategoryTarget);
	}
	
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#updateOperationsCentreCategory(eu.esponder.model.crisis.resource.category.OperationsCentreCategory, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OperationsCentreCategory updateOperationsCentreCategory(OperationsCentreCategory operationsCentreCategory, Long userId) {
		OperationsCentreCategory operationsCentreCategoryTarget = (OperationsCentreCategory) findById(OperationsCentreCategory.class, operationsCentreCategory.getId());
		mappingService.mapEntityToEntity(operationsCentreCategory, operationsCentreCategoryTarget);
		return (OperationsCentreCategory) crudService.update(operationsCentreCategoryTarget);
	}
	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#updateEquipmentCategory(eu.esponder.model.crisis.resource.category.EquipmentCategory, eu.esponder.model.type.EquipmentType, java.lang.Long)
	 */
	@Override
	public EquipmentCategory updateEquipmentCategory(EquipmentCategory equipmentCategory, EquipmentType equipmentType, Long userId) {
		EquipmentCategory equipmentCategoryTarget = (EquipmentCategory) findById(EquipmentCategory.class, equipmentCategory.getId());
		equipmentCategory.setEquipmentType(equipmentType);
		mappingService.mapEntityToEntity(equipmentCategory, equipmentCategoryTarget);
		return (EquipmentCategory) crudService.update(equipmentCategoryTarget);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#updateConsumableResourceCategory(eu.esponder.model.crisis.resource.category.ConsumableResourceCategory, eu.esponder.model.type.ConsumableResourceType, java.lang.Long)
	 */
	@Override
	public ConsumableResourceCategory updateConsumableResourceCategory(	ConsumableResourceCategory consumableCategory,	ConsumableResourceType consumableType, Long userId) {
		
		ConsumableResourceCategory consumableResourceCategoryTarget = (ConsumableResourceCategory) findById(ConsumableResourceCategory.class, consumableCategory.getId());
		consumableCategory.setConsumableResourceType(consumableType);
		mappingService.mapEntityToEntity(consumableCategory, consumableResourceCategoryTarget);
		return (ConsumableResourceCategory) crudService.update(consumableResourceCategoryTarget);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#updateReusableResourceCategory(eu.esponder.model.crisis.resource.category.ReusableResourceCategory, eu.esponder.model.type.ReusableResourceType, java.lang.Long)
	 */
	@Override
	public ReusableResourceCategory updateReusableResourceCategory(	ReusableResourceCategory reusableCategory, ReusableResourceType reusableType, Long userId) {
		
		ReusableResourceCategory reusableResourceCategoryTarget = (ReusableResourceCategory) findById(ReusableResourceCategory.class, reusableCategory.getId());
		reusableCategory.setReusableResourceType(reusableType);
		mappingService.mapEntityToEntity(reusableCategory, reusableResourceCategoryTarget);
		return (ReusableResourceCategory) crudService.update(reusableResourceCategoryTarget);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#updatePersonnelCategory(eu.esponder.model.crisis.resource.category.PersonnelCategory, java.util.Set, eu.esponder.model.type.RankType, eu.esponder.model.crisis.resource.category.OrganisationCategory, java.lang.Long)
	 */
	@Override
	public PersonnelCategory updatePersonnelCategory( PersonnelCategory personnelCategory, Set<PersonnelCompetence> competences, RankType rank, OrganisationCategory organizationCategory, Long userId) {
		PersonnelCategory personnelCategoryTarget = (PersonnelCategory) findById(PersonnelCategory.class, personnelCategory.getId());
		personnelCategory.setOrganisationCategory(organizationCategory);
		personnelCategory.setRank(rank);
		personnelCategory.setPersonnelCompetences(competences);
		mappingService.mapEntityToEntity(personnelCategory, personnelCategoryTarget);
		return (PersonnelCategory) crudService.update(personnelCategoryTarget);
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService#deleteResourceCategoryRemote(java.lang.Long)
	 */
	@Override
	public Long deleteResourceCategoryRemote(Long resourceCategoryID) {
		return deleteResourceCategory(resourceCategoryID);
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.category.ResourceCategoryService#deleteResourceCategory(java.lang.Long)
	 */
	@Override
	public Long deleteResourceCategory(Long resourceCategoryID) {
		crudService.delete(ResourceCategory.class, resourceCategoryID);
		return resourceCategoryID;
	}

}
