/*
 * 
 */
package eu.esponder.controller.mapping;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
/**
 * The Interface ESponderRemoteMappingService.
 */
@Remote
public interface ESponderRemoteMappingService {

	//	public List<OperationsCentreDTO> mapUserOperationsCentres(Long userID);

	//	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID);

	//	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID, Long userID);


	//	public List<SketchPOIDTO> mapOperationsCentreSketches(Long operationsCentreID, Long userID);

	//	public List<ReferencePOIDTO> mapOperationsCentreReferencePOIs(Long operationsCentreID, Long userID);

	//	public OperationsCentreSnapshotDTO mapOperationsCentreSnapshot(Long operationsCentreID, Date maxDate);

	//	public ActorSnapshotDTO mapActorSnapshot(Long actorID, Date maxDate);

	//	public EquipmentSnapshotDTO mapEquipmentSnapshot(Long equipmentID, Date maxDate);

	//	public SensorSnapshotDTO mapLatestSensorSnapshot(Long sensorID, Date maxDate);

	//	public SketchPOI mapSketch(SketchPOIDTO sketchDTO);

	//	public ReferencePOI mapReferencePOI(ReferencePOIDTO referencePOIDTO);

	//	public ActorDTO mapActor(Long actorID);

	//	public ActorDTO mapActor(String actorTitle);

	//	public ActorDTO updateActor(Actor actor, Long userID);

	//	public ActorDTO createActor(Actor actor, Long userID);

	//	public void deleteActor(Long actorID, Long userID);

	//	public List<ActorDTO> mapActorSubordinates(Long actorID);

	//	public SketchPOIDTO mapSketch(SketchPOI sketch);

	//	public ReferencePOIDTO mapReferencePOIDTO(ReferencePOI referencePOI);

	//	public SketchPOIDTO mapSketchById(Long sketchPOIId);

	//	public SketchPOIDTO mapSketchByTitle(String sketchPOITitle);

	//	public SketchPOIDTO mapCreateSketch(OperationsCentre opCentre,	SketchPOIDTO sketchDTO, Long userID);


	//	public List<? extends ESponderEntityDTO> mapESponderEntity(List<? extends ESponderEntity<Long>> resultsList, Class<? extends ESponderEntityDTO> targetClass);
	//
	//	public List<? extends ESponderEntity<?>> mapESponderEntityDTO(List<? extends ESponderEntityDTO> resultsList, Class<? extends ESponderEntity<?>> targetClass);
	//	
	//	public ESponderEntityDTO mapESponderEntity(ESponderEntity<Long> entity, Class<? extends ESponderEntityDTO> targetClass);
	//
	//	public ESponderEntity<?> mapESponderEntityDTO(ESponderEntityDTO entity, Class<? extends ESponderEntity<?>> targetClass);

	//***************************************************************************************************************

	//	public EsponderQueryRestriction mapCriteriaCollection(EsponderQueryRestrictionDTO criteriaDTO);

	//	public EsponderCriterion mapSimpleCriterion(EsponderCriterionDTO criteria);

	//	public ESponderEntityDTO mapGenericCreateEntity(ESponderEntityDTO entityDTO, Long userID) throws ClassNotFoundException;

	//	public ESponderEntityDTO mapGenericUpdateEntity(ESponderEntityDTO entityDTO, Long userID) throws ClassNotFoundException;

	//	public void mapGenericDeleteEntity(String entityClass, Long entityID) throws ClassNotFoundException;


	/**
	 * Map e sponder entity.
	 *
	 * @param resultsList the results list
	 * @param targetClass the target class
	 * @return the list<? extends e sponder entity dt o>
	 */
	public List<? extends ESponderEntityDTO> mapESponderEntity(List<? extends ESponderEntity<Long>> resultsList, Class<? extends ESponderEntityDTO> targetClass);

	/**
	 * Map e sponder entity.
	 *
	 * @param entity the entity
	 * @param targetClass the target class
	 * @return the e sponder entity dto
	 */
	public ESponderEntityDTO mapESponderEntity(ESponderEntity<Long> entity, Class<? extends ESponderEntityDTO> targetClass);

	/**
	 * Map e sponder entity dto.
	 *
	 * @param resultsList the results list
	 * @param targetClass the target class
	 * @return the list<? extends e sponder entity<?>>
	 */
	public List<? extends ESponderEntity<?>> mapESponderEntityDTO(List<? extends ESponderEntityDTO> resultsList, Class<? extends ESponderEntity<?>> targetClass);

	/**
	 * Map e sponder entity dto.
	 *
	 * @param entity the entity
	 * @param targetClass the target class
	 * @return the e sponder entity
	 */
	public ESponderEntity<?> mapESponderEntityDTO(ESponderEntityDTO entity, Class<? extends ESponderEntity<?>> targetClass);

	//***************************************************************************************************************	

	/**
	 * Map e sponder enum dto.
	 *
	 * @param entity the entity
	 * @param targetClass the target class
	 * @return the object
	 */
	public Object mapESponderEnumDTO(Object entity, Class<?> targetClass);

	/**
	 * Gets the managed entity class.
	 *
	 * @param clz the clz
	 * @return the managed entity class
	 * @throws ClassNotFoundException the class not found exception
	 */
	public Class<? extends ESponderEntity<Long>> getManagedEntityClass(Class<? extends ESponderEntityDTO> clz) throws ClassNotFoundException;

	/**
	 * Gets the dto entity class.
	 *
	 * @param clz the clz
	 * @return the DTO entity class
	 * @throws ClassNotFoundException the class not found exception
	 */
	public Class<? extends ESponderEntityDTO> getDTOEntityClass(Class<? extends ESponderEntity<Long>> clz) throws ClassNotFoundException;

	/**
	 * Gets the entity class.
	 *
	 * @param className the class name
	 * @return the entity class
	 * @throws ClassNotFoundException the class not found exception
	 */
	public Class<?> getEntityClass(String className) throws ClassNotFoundException;

	/**
	 * Gets the managed entity name.
	 *
	 * @param queriedEntityDTO the queried entity dto
	 * @return the managed entity name
	 */
	public String getManagedEntityName(String queriedEntityDTO);

	/**
	 * Gets the dTO entity name.
	 *
	 * @param entityName the entity name
	 * @return the dTO entity name
	 */
	public String getDTOEntityName(String entityName);

	/**
	 * Map entity to entity.
	 *
	 * @param src the src
	 * @param dest the dest
	 */
	public void mapEntityToEntity(ESponderEntity<Long> src, ESponderEntity<Long> dest);

	/**
	 * Map object.
	 *
	 * @param obj the obj
	 * @param targetClass the target class
	 * @return the object
	 */
	public Object mapObject(Object obj, Class<?> targetClass);



}
