/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.OrganisationDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface OrganisationRemoteService.
 */
@Remote
public interface OrganisationRemoteService {

	/**
	 * Find dto by id.
	 *
	 * @param organisationID the organisation id
	 * @return the organisation dto
	 */
	public OrganisationDTO findDTOById(Long organisationID);

	/**
	 * Find dto by title.
	 *
	 * @param organisationTitle the organisation title
	 * @return the organisation dto
	 */
	public OrganisationDTO findDTOByTitle(String organisationTitle);

	/**
	 * Creates the organisation dto.
	 *
	 * @param organisationDTO the organisation dto
	 * @return the organisation dto
	 */
	public OrganisationDTO createOrganisationDTO(OrganisationDTO organisationDTO);

	/**
	 * Update organisation dto.
	 *
	 * @param organisationDTO the organisation dto
	 * @return the organisation dto
	 */
	public OrganisationDTO updateOrganisationDTO(OrganisationDTO organisationDTO);

	/**
	 * Delete organisation dto.
	 *
	 * @param organisationDTO the organisation dto
	 */
	public void deleteOrganisationDTO(OrganisationDTO organisationDTO);

	/**
	 * Delete organisation dto.
	 *
	 * @param organisationDTOId the organisation dto id
	 */
	public void deleteOrganisationDTO(Long organisationDTOId);

	/**
	 * Find allorganisations remote.
	 *
	 * @return the list
	 */
	public List<OrganisationDTO> findAllorganisationsRemote();
	
}
