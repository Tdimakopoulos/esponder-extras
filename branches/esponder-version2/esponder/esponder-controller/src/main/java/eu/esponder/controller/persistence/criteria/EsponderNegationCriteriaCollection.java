/*
 * 
 */
package eu.esponder.controller.persistence.criteria;

import java.util.Collection;

// TODO: Auto-generated Javadoc
/**
 * The Class EsponderNegationCriteriaCollection.
 */
public class EsponderNegationCriteriaCollection extends EsponderCriteriaCollection {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1754280753704823556L;

	/**
	 * Instantiates a new esponder negation criteria collection.
	 *
	 * @param restrictions the restrictions
	 */
	public EsponderNegationCriteriaCollection(
			Collection<EsponderQueryRestriction> restrictions) {
		super(restrictions);
	}

}
