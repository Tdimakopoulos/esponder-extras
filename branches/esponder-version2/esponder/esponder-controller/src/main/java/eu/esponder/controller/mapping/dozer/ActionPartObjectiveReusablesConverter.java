/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.LogisticsService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.model.crisis.resource.ReusableResource;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionPartObjectiveReusablesConverter.
 */
public class ActionPartObjectiveReusablesConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected LogisticsService getLogisticsService() {
		try {
			return ServiceLocator.getResource("esponder/LogisticsBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				if(it.next().getClass() == ReusableResource.class) {

					Set<ReusableResource> sourceReusablesSet = (Set<ReusableResource>) source;
					Set<ReusableResourceDTO> destReusablesDTOSet = new HashSet<ReusableResourceDTO>();
					for(ReusableResource cResource : sourceReusablesSet) {
						ReusableResourceDTO destResourceDTO = new ReusableResourceDTO();
						destResourceDTO.setId(cResource.getId());
						destReusablesDTOSet.add(destResourceDTO);
					}
					destination = destReusablesDTOSet;
				}
				else
					if(it.next().getClass() == ReusableResourceDTO.class) {

						Set<ReusableResourceDTO> sourceReusablesDTOSet = (Set<ReusableResourceDTO>) source;
						Set<ReusableResource> destReusablesSet = new HashSet<ReusableResource>();
						for(ReusableResourceDTO cResourceDTO : sourceReusablesDTOSet) {
							ReusableResource destResource = this.getLogisticsService().findReusableResourceById(cResourceDTO.getId());
							destReusablesSet.add(destResource);
						}
						destination = destReusablesSet;
					}
					else
						destination = null;
			}
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
