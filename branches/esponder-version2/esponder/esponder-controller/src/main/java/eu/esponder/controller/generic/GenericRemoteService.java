/*
 * 
 */
package eu.esponder.controller.generic;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.criteria.EsponderQueryRestrictionDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface GenericRemoteService.
 */
@Remote
public interface GenericRemoteService {

	/**
	 * Gets the dto entities.
	 *
	 * @param queriedDTOClassName the queried dto class name
	 * @param criteriaDTO the criteria dto
	 * @param pageSize the page size
	 * @param pageNumber the page number
	 * @return the DTO entities
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws Exception the exception
	 */
	public List<? extends ESponderEntityDTO> getDTOEntities(
			String queriedDTOClassName,EsponderQueryRestrictionDTO criteriaDTO, int pageSize, int pageNumber) 
			throws InstantiationException, IllegalAccessException, Exception;
	
	/**
	 * Creates the entity remote.
	 *
	 * @param entity the entity
	 * @param userID the user id
	 * @return the e sponder entity dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderEntityDTO createEntityRemote(ESponderEntityDTO entity, Long userID) throws ClassNotFoundException;
	
	/**
	 * Update entity remote.
	 *
	 * @param entityDTO the entity dto
	 * @param userID the user id
	 * @return the e sponder entity dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderEntityDTO updateEntityRemote(ESponderEntityDTO entityDTO, Long userID) throws ClassNotFoundException;
	
	/**
	 * Gets the entity dto.
	 *
	 * @param clz the clz
	 * @param objectID the object id
	 * @return the entity dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderEntityDTO getEntityDTO( Class<? extends ESponderEntityDTO> clz, Long objectID) throws ClassNotFoundException;
	
	/**
	 * Delete entity remote.
	 *
	 * @param clz the clz
	 * @param entityID the entity id
	 * @throws ClassNotFoundException the class not found exception
	 */
	public void deleteEntityRemote(Class<? extends ESponderEntityDTO> clz, Long entityID) throws ClassNotFoundException;
	
}