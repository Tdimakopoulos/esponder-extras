/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;
import org.hibernate.collection.PersistentSet;

import eu.esponder.controller.persistence.CrudService;
import eu.esponder.model.crisis.resource.PersonnelCompetence;
import eu.esponder.model.crisis.resource.PersonnelSkill;
import eu.esponder.model.crisis.resource.PersonnelTraining;
import eu.esponder.util.ejb.ServiceLocator;


// TODO: Auto-generated Javadoc
/**
 * The Class EquipmentSensorsFieldConverter.
 */
public class PesonnelCompetenceSetConverter implements CustomConverter {
	
	
	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected CrudService getCrudService() {
		try {
			return ServiceLocator.getResource("esponder/CrudBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert(Object destination,
			Object source, Class<?> destinationClass,
			Class<?> sourceClass) {


		if(sourceClass == PersistentSet.class) {
			
			if(!((Set)source).isEmpty()) {
				
				Object[] list = ((Set)source).toArray();
				if(list[0].getClass() == PersonnelSkill.class || list[0].getClass() == PersonnelTraining.class) {
					Set destSet = new HashSet<Integer>();
					for(PersonnelCompetence i : ((Set<PersonnelCompetence>)source)) {
						destSet.add(i.getId());
					}
					return destSet;
				}
				else if(list[0].getClass().isAssignableFrom(Long.class)) {
					Set destSet = new HashSet<PersonnelCompetence>();
					for(Long i : ((Set<Long>)source)) {
						destSet.add(getCrudService().find(PersonnelCompetence.class, i));
					}
					return destSet;
				}
				
				
			}
			return null;
		}	
		return null;
	}

}
