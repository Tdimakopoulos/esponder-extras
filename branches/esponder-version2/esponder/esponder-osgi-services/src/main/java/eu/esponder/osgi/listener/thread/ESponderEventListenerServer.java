/*
 * 
 */
package eu.esponder.osgi.listener.thread;

import java.io.IOException;
import java.util.Date;

import com.prosyst.mprm.common.ManagementException;

import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.crisis.action.CreateActionPartEvent;
import eu.esponder.event.model.snapshot.CreateCrisisContextSnapshotEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionPartSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.osgi.service.event.ESponderEventListener;
import eu.esponder.osgi.settings.OsgiSettings;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderEventListenerServer.
 */
public class ESponderEventListenerServer extends Thread {

	/** The listener. */
	ESponderEventListener<CreateActionSnapshotEvent> listener = null;
	
	/** The listener2. */
	ESponderEventListener<CreateCrisisContextEvent> listener2 = null;
	
	/** The listener3. */
	ESponderEventListener<CreateActionEvent> listener3 = null;
	
	/** The listener4. */
	ESponderEventListener<CreateActionPartEvent> listener4 = null;
	
	/** The listener5. */
	ESponderEventListener<CreateSensorMeasurementEnvelopeEvent> listener5 = null;
	
	/** The listener6. */
	ESponderEventListener<UpdateActionSnapshotEvent> listener6 = null;
	
	/** The listener7. */
	ESponderEventListener<UpdateActionPartSnapshotEvent> listener7 = null;
	
	/** The listener8. */
	ESponderEventListener<CreateSensorMeasurementStatisticEvent> listener8 = null;
	
	/** The listener9. */
	ESponderEventListener<CreateCrisisContextSnapshotEvent> listener9 = null;
	
	/** The b connection. */
	public boolean bConnection = true;
	
	/** The b listen. */
	public boolean bListen = true;
	
	/** The szgfilename. */
	String szgfilename = null;
	
	/** The i run times. */
	int iRunTimes = 0;

	/**
	 * Creates the listeners connections.
	 *
	 * @throws ManagementException the management exception
	 */
	public void CreateListenersConnections() throws ManagementException {
		System.out.println("Event Server Listeners OK !");
		if (listener == null)
			listener = new ESponderEventListener<CreateActionSnapshotEvent>(
					CreateActionSnapshotEvent.class);

		if (listener2 == null)
			listener2 = new ESponderEventListener<CreateCrisisContextEvent>(
					CreateCrisisContextEvent.class);

		if (listener3 == null)
			listener3 = new ESponderEventListener<CreateActionEvent>(
					CreateActionEvent.class);

		if (listener4 == null)
			listener4 = new ESponderEventListener<CreateActionPartEvent>(
					CreateActionPartEvent.class);

		if (listener5 == null)
			listener5 = new ESponderEventListener<CreateSensorMeasurementEnvelopeEvent>(
					CreateSensorMeasurementEnvelopeEvent.class);

		if (listener6 == null)
			listener6 = new ESponderEventListener<UpdateActionSnapshotEvent>(
					UpdateActionSnapshotEvent.class);

		if (listener7 == null)
			listener7 = new ESponderEventListener<UpdateActionPartSnapshotEvent>(
					UpdateActionPartSnapshotEvent.class);

		if (listener8 == null)
			listener8 = new ESponderEventListener<CreateSensorMeasurementStatisticEvent>(
					CreateSensorMeasurementStatisticEvent.class);

		if (listener9 == null)
			listener9 = new ESponderEventListener<CreateCrisisContextSnapshotEvent>(
					CreateCrisisContextSnapshotEvent.class);
	}

	/**
	 * Close server.
	 *
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String CloseServer() throws IOException {
		
		ServerStatus pStatus = new ServerStatus();
		String szFilename = null;

		if (szgfilename == null) {
			OsgiSettings pSettings = new OsgiSettings();
			pSettings.LoadSettings();
			szFilename = pSettings.getSzPropertiesFileName();
			szgfilename = szFilename;
		} else {
			szFilename = szgfilename;
		}
		String sz=pStatus.readFile(pStatus.AppandFileOnPath(pStatus
				.RemoveFilename(szFilename)));
		
		return sz;
	}

	/**
	 * Write status on file.
	 *
	 * @param running the running
	 */
	public void WriteStatusOnFile(boolean running) {
		ServerStatus pStatus = new ServerStatus();
		String szFilename;
		if (szgfilename == null) {
			OsgiSettings pSettings = new OsgiSettings();
			pSettings.LoadSettings();
			szFilename = pSettings.getSzPropertiesFileName();
			szgfilename = szFilename;
		} else {
			szFilename = szgfilename;
		}
		if (running)
			pStatus.UpdateServerRunning(szFilename,
					"Server is Running, Last Check : " + new Date());
		if (!running)
			pStatus.UpdateServerRunning(szFilename,
					"Server is Stopped, Last Check : " + new Date());
	}

	/**
	 * Stopserver.
	 */
	public void stopserver() {
		System.out.println("Event Server is Stopped");
		WriteStatusOnFile(false);
		if (listener != null)
			listener.CloseConnection();
		if (listener2 != null)
			listener2.CloseConnection();
		if (listener3 != null)
			listener3.CloseConnection();
		if (listener4 != null)
			listener4.CloseConnection();
		if (listener5 != null)
			listener5.CloseConnection();
		if (listener6 != null)
			listener6.CloseConnection();
		if (listener7 != null)
			listener7.CloseConnection();
		if (listener8 != null)
			listener8.CloseConnection();
		if (listener9 != null)
			listener9.CloseConnection();
		listener = null;
		listener2 = null;
		listener3 = null;
		listener4 = null;
		listener5 = null;
		listener6 = null;
		listener7 = null;
		listener8 = null;
		listener9 = null;
		bConnection = false;
	}

	/**
	 * Startserver.
	 */
	public void startserver() {
		System.out.println("Event Server is Running");
		WriteStatusOnFile(true);
		try {
			CreateListenersConnections();
			
			listener.subscribe();
			listener2.subscribe();
			listener3.subscribe();
			listener4.subscribe();
			listener5.subscribe();
			listener6.subscribe();
			listener7.subscribe();
			listener8.subscribe();
			listener9.subscribe();
		} catch (ManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		bConnection = true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run() {

		boolean bRun = true;
		try {
			startserver();

			while (true) {

				Thread.sleep(30000);
				WriteStatusOnFile(true);
				if (CloseServer().equalsIgnoreCase("0")) {
					bListen = false;
					stopserver();
					bRun = false;
				}else
				{
					if (bRun==false)
					{
						startserver();
						bRun=true;
						bListen = true;
					}
				}

			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}