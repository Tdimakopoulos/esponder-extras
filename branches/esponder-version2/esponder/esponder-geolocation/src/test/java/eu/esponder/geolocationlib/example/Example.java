/* Geodesy by Mike Gavaghan
 * 
 * http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula-java/
 * 
 * This code may be freely used and modified on any personal or professional
 * project.  It comes with no warranty.
 */
package eu.esponder.geolocationlib.example;

import eu.esponder.geolocation.calculations.DirectLineCalculations2D;
import eu.esponder.geolocation.lib.Ellipsoid;
import eu.esponder.geolocation.lib.GeodeticCalculator;
import eu.esponder.geolocation.lib.GlobalCoordinates;

public class Example
{
   /**
    * Calculate the destination if we start at:
    *    Lincoln Memorial in Washington, D.C --> 38.8892N, 77.04978W
    *         and travel at
    *    51.7679 degrees for 6179.016136 kilometers
    * 
    *    WGS84 reference ellipsoid
    */
   static void TwoDimensionalDirectCalculation(double lat1,double lon1,double lat2,double lon2, double step)
   {
     // instantiate the calculator
     GeodeticCalculator geoCalc = new GeodeticCalculator();

     // select a reference elllipsoid
     Ellipsoid reference = Ellipsoid.WGS84;

     // set Lincoln Memorial coordinates
     GlobalCoordinates lincolnMemorial;
     lincolnMemorial = new GlobalCoordinates( lat1, lon1 );

     // set the direction and distance
     double startBearing = bearing(lat1,lon1,lat2,lon2);
//     double startBearing  = 225;
     double distance = step;

     // find the destination
     double[] endBearing = new double[1];
     GlobalCoordinates dest = geoCalc.calculateEndingGlobalCoordinates(reference, lincolnMemorial, startBearing, distance, endBearing);
     
//     double p[] = offset( lat1, lon1, startBearing, step);
//     double latFinal = p[0] * 180 / Math.PI;
//     double lngFinal = p[1] * 180 / Math.PI;

     System.out.println("Travel from Lincoln Memorial at "+startBearing+" deg for 6179.016 km");
     System.out.printf("   Destination: %1.4f%s", dest.getLatitude(), (dest.getLatitude() > 0) ? "N" : "S" );
     System.out.printf(", %1.4f%s\n", dest.getLongitude(), (dest.getLongitude() > 0) ? "E" : "W");
     System.out.printf("   End Bearing: %1.2f degrees\n", endBearing[0]);
     
//     System.out.println("\n\nResults\n\n"+latFinal+", "+lngFinal);
   }


	protected static double bearing(double lat1, double lon1, double lat2, double lon2){
  double longitude1 = lon1;
  double longitude2 = lon2;
  double latitude1 = Math.toRadians(lat1);
//  double latitude1 = lat1;
  double latitude2 = Math.toRadians(lat2);
//  double latitude2 = lat2;
  double longDiff= Math.toRadians(longitude2-longitude1);
//  double longDiff= longitude2-longitude1;
  double y= Math.sin(longDiff)*Math.cos(latitude2);
  double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);

  return (Math.toDegrees(Math.atan2(y, x))+360)%360;
//  return Math.atan2(y, x);
}

	static public void main(String[] args)
	{
		DirectLineCalculations2D calc = new DirectLineCalculations2D();
		GlobalCoordinates point = new GlobalCoordinates(0, 0);
		point = calc.TwoDimensionalDirectCalculation(37.959146, 23.719933, 37.940263, 23.6956, 1000);
		
		
	     System.out.printf("   Destination: %1.4f%s", point.getLatitude(), (point.getLatitude() > 0) ? "N" : "S" );
	     System.out.printf(", %1.4f%s\n", point.getLongitude(), (point.getLongitude() > 0) ? "E" : "W");
	}
}
