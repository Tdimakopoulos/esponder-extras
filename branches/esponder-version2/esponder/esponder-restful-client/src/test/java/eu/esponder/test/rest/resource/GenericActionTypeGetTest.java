package eu.esponder.test.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.type.ActionTypeDTO;
import eu.esponder.rest.client.ResteasyClient;

public class GenericActionTypeGetTest {
	private String GENERIC_ENTITY_URI = "http://localhost:8080/esponder-restful/crisis/generic";

	@Test
	public void getEntity() throws ClassNotFoundException, Exception {

		String serviceName = GENERIC_ENTITY_URI + "/get";

		ResteasyClient getClient = new ResteasyClient(serviceName, "application/json");
		Map<String, String> params =  getGenericEntityServiceParameters();
		String result = getClient.get(params);
		System.out.println("\n"+result+"\n");
		if(result!=null) {
			ObjectMapper mapper = new ObjectMapper();
			ResultListDTO resultsList = mapper.readValue(result, ResultListDTO.class);
			ActionTypeDTO actionTypeDTO = (eu.esponder.dto.model.type.ActionTypeDTO) resultsList.getResultList().get(0);
		}

	}
	
	@Test
	public void getEntityByTitle() throws ClassNotFoundException, Exception {

		String serviceName = GENERIC_ENTITY_URI + "/getByTitle";

		ResteasyClient getClient = new ResteasyClient(serviceName, "application/json");
		Map<String, String> params =  getGenericEntityTitleServiceParameters();
		String result = getClient.get(params);
		System.out.println("\n"+result+"\n");
		if(result!=null) {
			ObjectMapper mapper = new ObjectMapper();
			ResultListDTO resultsList = mapper.readValue(result, ResultListDTO.class);
			ActionTypeDTO actionTypeDTO = (eu.esponder.dto.model.type.ActionTypeDTO) resultsList.getResultList().get(0);
		}

	}


	private Map<String, String> getGenericEntityServiceParameters() {
		Map<String, String> params = new HashMap<String, String>();

		ArrayList<Long> idList = new ArrayList<Long>();
		idList.add(new Long(12));		
		params.put("userID", "1");
		params.put("queriedEntityDTO",ActionTypeDTO.class.getName());
		params.put("idList", idList.toString());
		params.put("pageNumber", "0");
		params.put("pageSize", "10");
		return params;
	}
	
	private Map<String, String> getGenericEntityTitleServiceParameters() {
		Map<String, String> params = new HashMap<String, String>();

		ArrayList<String> titleList = new ArrayList<String>();
		titleList.add("StratActionType");
		titleList.add("TactActionType");
		titleList.add("OpActionType");
		params.put("userID", "1");
		params.put("queriedEntityDTO",ActionTypeDTO.class.getName());
		params.put("titleList", titleList.toString());
		params.put("pageNumber", "0");
		params.put("pageSize", "10");
		return params;
	}

}


