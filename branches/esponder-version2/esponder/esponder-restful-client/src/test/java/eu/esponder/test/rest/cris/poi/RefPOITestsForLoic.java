package eu.esponder.test.rest.cris.poi;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.ReferencePOISnapshotDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.rest.client.ResteasyClient;
import eu.esponder.util.jaxb.Parser;

public class RefPOITestsForLoic {

	private String REFPOI_SNAPSHOT_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/view/ref/snapshot";
	private String GENERIC_CREATE_ENTITY_URI = "http://localhost:8080/esponder-restful/crisis/generic";


	//TODO NOT DONE
	public void createRefPOISnapshot() throws RuntimeException, Exception {

		Parser parser = new Parser(new Class[] {ReferencePOISnapshotDTO.class, SphereDTO.class});

		ReferencePOISnapshotDTO refSnapshot = new ReferencePOISnapshotDTO();
		refSnapshot.setAverageSize(new Float("5"));
		refSnapshot.setTitle("new Ref POI for Loic");

		PointDTO point = new PointDTO(new BigDecimal("1"), new BigDecimal("1"), new BigDecimal("1"));
		SphereDTO sphere = new SphereDTO(point, new BigDecimal(1), "Sphere For Loic Test");

		ResteasyClient postClient = new ResteasyClient(GENERIC_CREATE_ENTITY_URI+"/create", "application/json");
		String jsonPayload = parser.marshall(sphere);
		Map<String, String> params =  getCreateEntityServiceParameters();
		String resultJSON = postClient.post(params, jsonPayload);

		sphere = (SphereDTO) parser.unmarshal(resultJSON);

		refSnapshot.setLocationArea(sphere);

		refSnapshot.setOperationsCentre(null);

		refSnapshot.setPeriod(new PeriodDTO(new Date().getTime(), new Date().getTime()+1000000));

		refSnapshot.setReferenceFile("New Ref File for Loic");

		refSnapshot.setReferencePOI(null);

		String serviceName = REFPOI_SNAPSHOT_SERVICE_URI + "/create";

		postClient = new ResteasyClient(serviceName, "application/json");
		System.out.println("Client for createGenericEntity created successfully...");
		ObjectMapper mapper = new ObjectMapper();

		jsonPayload = mapper.writeValueAsString(refSnapshot);

		printJSON(jsonPayload);

		params =  CreateRefServiceParameters(new Long(8));

		resultJSON = postClient.post(params, jsonPayload);

		printJSON(resultJSON);

		ReferencePOISnapshotDTO refDTO = mapper.readValue(resultJSON, ReferencePOISnapshotDTO.class);

		System.out.println("\n*****************REFPOI**************************");
		System.out.println(refDTO.toString());
		System.out.println("\n************************************************");

	}

	@Test
	public void createReferencePOI() throws RuntimeException, Exception {

		String CREATE_REFPOI_URI = "http://rdocs.exodussa.com:8080/esponder-restful/crisis/view/ref/create";

		ObjectMapper mapper = new ObjectMapper();

		ReferencePOIDTO refPOIDTO = new ReferencePOIDTO();
		refPOIDTO.setAverageSize(new Float(1.5));
		refPOIDTO.setReferenceFile("ReferenceFileForLoic");
		refPOIDTO.setTitle("Reference POI for Loic 1");

		Map<String,String> params = new HashMap<String, String>();
		params.put("", "");

		String strBody = mapper.writeValueAsString(refPOIDTO);
		printJSON(strBody);
	}


	@Test
	public void updateReferencePOI() throws JsonGenerationException, JsonMappingException, IOException, RuntimeException, Exception {

		String FINDBYID_REFPOI_URI = "http://localhost:8080/esponder-restful/crisis/view/ref/findByID";
		ObjectMapper mapper = new ObjectMapper();
		ResteasyClient client = new ResteasyClient(FINDBYID_REFPOI_URI, "application/json");
		Map<String,String> params = new HashMap<String, String>();
		params.put("referencePOIId", "1");
		params.put("pkiKey", "2");
		printJSON(client.get(params));
	}

	private Map<String, String> getCreateEntityServiceParameters() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		return params;
	}


	private Map<String, String> CreateRefServiceParameters(Long operationsCentreID) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		params.put("operationsCentreID", operationsCentreID.toString());
		return params;
	}


	private void printJSON(String jsonStr) {
		System.out.println("\n\n******* JSON * START *******");
		System.out.println(jsonStr);
		System.out.println("******** JSON * END ********\n\n");
	}

}
