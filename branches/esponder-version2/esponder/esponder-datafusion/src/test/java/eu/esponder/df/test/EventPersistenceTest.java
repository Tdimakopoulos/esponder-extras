package eu.esponder.df.test;

import javax.ejb.EJB;

import org.testng.annotations.Test;

import eu.esponder.controller.persistence.CrudRemoteService;
import eu.esponder.model.events.entity.OsgiEventsEntity;
import eu.esponder.test.ResourceLocator;

public class EventPersistenceTest {
	
	
	private CrudRemoteService<OsgiEventsEntity> crudService = ResourceLocator.lookup("esponder/CrudBean/remote");
	
	@Test
	public void testEventPersistence() {
		
		OsgiEventsEntity dEntity = new OsgiEventsEntity();

//		dEntity.setJournalMsg(event.getJournalMessage());
//		dEntity.setJournalMsgInfo(event.getJournalMessageInfo());
	//
//		dEntity.setSeverity(event.getEventSeverity().toString());
	//
//		dEntity.setSourceid(event.getEventSource().getId());
	//
//		dEntity.setTimeStamp(event.getEventTimestamp());

		dEntity.setAttachment("dddd");
		dEntity.setJournalMsg("ddddd");
		
		System.out.println("\n\nBefore persist"+dEntity.getAttachment()+"\n\n");
		OsgiEventsEntity entityPersisted = crudService.create(dEntity);
		
		System.out.println("\n\nAfter persist"+entityPersisted.getAttachment()+"\n\n");

		System.out.println("******** Save Event On DB End ******");

		
	}
}
