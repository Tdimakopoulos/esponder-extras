/**
 * State
 * 
 * This Java class implement a simple state variable, each state variable have a name ( string type )
 * and state, state can be finished or notrun
 *
 * @Project   esponder
 * @package   Datafusion
*/
package eu.esponder.df.ruleengine.core;

import java.beans.PropertyChangeSupport;

// TODO: Auto-generated Javadoc
/**
 * The Class State.
 *
 * @author tdim
 */
public class State {

	/** The Constant NOTRUN. */
	public static final int NOTRUN   = 0;
    
    /** The Constant FINISHED. */
    public static final int FINISHED = 1;

    /** The name. */
    private String name;
	
	/** The state. */
	private int state;

	/** The changes. */
	private final PropertyChangeSupport changes =
	        new PropertyChangeSupport( this );

    /**
     * Instantiates a new state.
     *
     * @param szName the name of the current state variable
     */
    public State(final String szName)
    {
    	this.name=szName;
    }

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param newName set the name
	 */
	public void setName(final String newName) {
		String oldName = this.name;
	    this.name = newName;
	    //execute rule rescan if any variables changes in name
	    this.changes.firePropertyChange( "name",oldName,newName );
	}

	/**
	 * Gets the state.
	 *
	 * @return the current state of the state variable
	 */
	public int getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param newState the state
	 */
	public void setState(final int newState) {
		int oldState = this.state;
	    this.state = newState;
	    //execute rule rescan if any variables changes in state
	    this.changes.firePropertyChange( "state",oldState,newState );
	}
    
}
