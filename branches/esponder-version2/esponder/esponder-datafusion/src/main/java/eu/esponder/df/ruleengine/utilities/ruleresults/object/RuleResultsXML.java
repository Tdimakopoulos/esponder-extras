/*
 * 
 */
package eu.esponder.df.ruleengine.utilities.ruleresults.object;

import org.simpleframework.xml.Element;

// TODO: Auto-generated Javadoc
/**
 * The Class RuleResultsXML.
 */
public class RuleResultsXML {

	/** The sz rule name. */
	@Element
	String szRuleName;
	
	/** The sz results text1. */
	@Element
	String szResultsText1;
	
	/** The sz results text2. */
	@Element
	String szResultsText2;
	
	/** The sz results text3. */
	@Element
	String szResultsText3;
	
	/** The sz results text4. */
	@Element
	private
	String szResultsText4;
	
	/**
	 * Gets the sz rule name.
	 *
	 * @return the sz rule name
	 */
	public String getSzRuleName() {
		return szRuleName;
	}
	
	/**
	 * Sets the sz rule name.
	 *
	 * @param szRuleName the new sz rule name
	 */
	public void setSzRuleName(String szRuleName) {
		this.szRuleName = szRuleName;
	}
	
	/**
	 * Gets the sz results text1.
	 *
	 * @return the sz results text1
	 */
	public String getSzResultsText1() {
		return szResultsText1;
	}
	
	/**
	 * Sets the sz results text1.
	 *
	 * @param szResultsText1 the new sz results text1
	 */
	public void setSzResultsText1(String szResultsText1) {
		this.szResultsText1 = szResultsText1;
	}
	
	/**
	 * Gets the sz results text2.
	 *
	 * @return the sz results text2
	 */
	public String getSzResultsText2() {
		return szResultsText2;
	}
	
	/**
	 * Sets the sz results text2.
	 *
	 * @param szResultsText2 the new sz results text2
	 */
	public void setSzResultsText2(String szResultsText2) {
		this.szResultsText2 = szResultsText2;
	}
	
	/**
	 * Gets the sz results text3.
	 *
	 * @return the sz results text3
	 */
	public String getSzResultsText3() {
		return szResultsText3;
	}
	
	/**
	 * Sets the sz results text3.
	 *
	 * @param szResultsText3 the new sz results text3
	 */
	public void setSzResultsText3(String szResultsText3) {
		this.szResultsText3 = szResultsText3;
	}
	
	/**
	 * Gets the sz results text4.
	 *
	 * @return the sz results text4
	 */
	public String getSzResultsText4() {
		return szResultsText4;
	}
	
	/**
	 * Sets the sz results text4.
	 *
	 * @param szResultsText4 the new sz results text4
	 */
	public void setSzResultsText4(String szResultsText4) {
		this.szResultsText4 = szResultsText4;
	}
}
