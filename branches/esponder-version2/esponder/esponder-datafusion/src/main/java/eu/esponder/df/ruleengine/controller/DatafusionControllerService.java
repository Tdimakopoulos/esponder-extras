/**
 * Controller
 * 
 * This Java class implement a controller for the rules engine, this is the class that must be called to run 
 * the rule engine, support DRL, DSL and DSLR type of assets. And Functions but need to be inline the rule.
 * 
 * 
 * @Project   esponder
 * @package   Datafusion
 */
package eu.esponder.df.ruleengine.controller;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;
import eu.esponder.event.model.ESponderEvent;


// TODO: Auto-generated Javadoc
/**
 * The Interface DatafusionControllerService.
 */
@Local
public interface DatafusionControllerService {

	/**
	 * Esponder event received handler.
	 *
	 * @param pEvent the event
	 */
	public void EsponderEventReceivedHandler(ESponderEvent<?> pEvent);
	
	/**
	 * Reinitialize repository.
	 */
	public void ReinitializeRepository();
	
	/**
	 * Delete repository.
	 */
	public void DeleteRepository();
	
	/**
	 * Sets the to live.
	 */
	public void SetToLive();
	
	/**
	 * Sets the to local.
	 */
	public void SetToLocal();
	
	/**
	 * Gets the rule results.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<RuleResultsXML> GetRuleResults() throws Exception;
}

