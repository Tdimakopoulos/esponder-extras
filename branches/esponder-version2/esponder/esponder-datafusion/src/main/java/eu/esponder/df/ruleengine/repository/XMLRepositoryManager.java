/*
 * 
 */
package eu.esponder.df.ruleengine.repository;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import eu.esponder.df.ruleengine.settings.DroolSettings;

// TODO: Auto-generated Javadoc
/**
 * The Class XMLRepositoryManager.
 */
public class XMLRepositoryManager {

	/** The d settings. */
	DroolSettings dSettings = new DroolSettings();
	
	/**
	 * Save category.
	 *
	 * @param szCategory the sz category
	 * @param XML the xml
	 */
	public void SaveCategory(String szCategory,String XML)
	{
		SaveXML(dSettings.getSzTmpRepositoryDirectoryPath()+szCategory+".xml",XML);
	}
	
	/**
	 * Save package.
	 *
	 * @param szPackage the sz package
	 * @param XML the xml
	 */
	public void SavePackage(String szPackage,String XML)
	{
		SaveXML(dSettings.getSzTmpRepositoryDirectoryPath()+szPackage+".xml",XML);
	}
	
	/**
	 * Save asset.
	 *
	 * @param szAsset the sz asset
	 * @param szPAsset the sz p asset
	 */
	public void SaveAsset(String szAsset,String szPAsset)
	{
		SaveXML(dSettings.getSzTmpRepositoryDirectoryPath()+szAsset+".ast",szPAsset);
	}
	
	/**
	 * Load package.
	 *
	 * @param szPackage the sz package
	 * @return the string
	 */
	public String LoadPackage(String szPackage)
	{
		return LoadXML(dSettings.getSzTmpRepositoryDirectoryPath()+szPackage+".xml");
	}
	
	/**
	 * Load asset.
	 *
	 * @param szAsset the sz asset
	 * @return the string
	 */
	public String LoadAsset(String szAsset)
	{
		return LoadXML(dSettings.getSzTmpRepositoryDirectoryPath()+szAsset+".ast");
	}
	
	/**
	 * Load category.
	 *
	 * @param szCategory the sz category
	 * @return the string
	 */
	public String LoadCategory(String szCategory)
	{
		return LoadXML(dSettings.getSzTmpRepositoryDirectoryPath()+szCategory+".xml");
	}
	
	/**
	 * Save xml.
	 *
	 * @param szFilename the sz filename
	 * @param szContents the sz contents
	 */
	private void SaveXML(String szFilename,String szContents)
	{
		//System.out.println("Save XML Filename : " + szFilename+" Content : "+ szContents);
		try {
		    BufferedWriter out = new BufferedWriter(new FileWriter(szFilename));
		    out.write(szContents);
		    out.close();
		} catch (IOException e) {
		}
	}
	
	/**
	 * Load xml.
	 *
	 * @param szFilename the sz filename
	 * @return the string
	 */
	private String LoadXML(String szFilename)
	{
		BufferedReader reader = null;
		try {
			reader = new BufferedReader( new FileReader (szFilename));
		} catch (FileNotFoundException e) {
			System.out.println("Error on read XML Stream: error 10001");
		}
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    String         ls = System.getProperty("line.separator");

	    try {
			while( ( line = reader.readLine() ) != null ) {
			    stringBuilder.append( line );
			    stringBuilder.append( ls );
			}
		} catch (IOException e) {
			System.out.println("Error on read XML Stream : error 10002");
			e.printStackTrace();
		}

	    //System.out.println("SzReturn : "+stringBuilder.toString());
	    String szReturn=stringBuilder.toString();
	    try {
			reader.close();
		} catch (IOException e) {
			System.out.println("Error on Close XML Stream : error 10003");
			e.printStackTrace();
		}
	    //System.out.println("SzReturn : "+szReturn);
	    return szReturn;
	}
}
