/*
 * 
 */
package eu.esponder.df.ruleengine.repository;

import java.io.File;

import eu.esponder.df.ruleengine.settings.DroolSettings;

// TODO: Auto-generated Javadoc
/**
 * The Class RepositoryController.
 */
public class RepositoryController {
	
	/** The d settings. */
	DroolSettings dSettings = new DroolSettings();
	
	/**
	 * Delete repository.
	 */
	public void DeleteRepository()
	{
		EmptyRepository();
	}
	
	/**
	 * Sets the repository to guvnor.
	 */
	public void SetRepositoryToGuvnor()
	{
		dSettings.setSzDroolRepo("guvnor");
	}
	
	/**
	 * Sets the repository to local.
	 */
	public void SetRepositoryToLocal()
	{
		dSettings.setSzDroolRepo("local");
	}
	
	/**
	 * Refresh request.
	 *
	 * @param szAssetName the sz asset name
	 * @param bXML the b xml
	 * @return true, if successful
	 */
	public boolean RefreshRequest(String szAssetName,boolean bXML)
	{
		//System.out.println("********** Update Request ********");
		if (dSettings.getSzDroolRepo().equalsIgnoreCase("guvnor"))
		{
			//System.out.println("********** Delete Repo ********");
			EmptyRepository();
			dSettings.setSzDroolRepo("local");
			//System.out.println("********** Delete Repo End ********");
		}
		String szFilename=null;
		if(bXML)
			szFilename=dSettings.getSzTmpRepositoryDirectoryPath()+szAssetName+".xml";
		else
			szFilename=dSettings.getSzTmpRepositoryDirectoryPath()+szAssetName+".ast";
		
		 File file=new File(szFilename);
		 if(file.exists())
		 {
			 //System.out.println("********** Update Request - Use Local********");
			 return false;
		 }
		 else
		 {
			 //System.out.println("********** Update Request - Get from guvnor ********");
			 //file is not exists get from guvnor
			 return true;
		 }
	}


	/**
	 * Empty local repository.
	 */
	private void EmptyRepository() {
		DeleteDirectory(dSettings.getSzTmpRepositoryDirectoryPath());
	}

	/**
	 * Delete directory.
	 *
	 * @param directoryName the directory to delete all files inside
	 */
	private void DeleteDirectory(String directoryName) {
		File directory = new File(directoryName);
		// Get all files in directory
		File[] files = directory.listFiles();
		for (File file : files) {
			// Delete each file
			if (!file.delete()) {
				// Failed to delete file
				System.out.println("Failed to delete " + file);
			}
		}
	}
}
