/*
 * 
 */
package eu.esponder.df.ruleengine.utilities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import eu.esponder.dto.model.crisis.resource.sensor.ActivitySensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BreathRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.EnvironmentTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.GasSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HeartBeatRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class RuleUtilities.
 */
public class RuleUtilities implements Serializable {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1499265521873811994L;


//	ActivitySensorDTO
//	BodyTemperatureSensorDTO
//	BreathRateSensorDTO
//	HeartBeatRateSensorDTO
//	EnvironmentTemperatureSensorDTO
//	GasSensorDTO
//	LocationSensorDTO

	/**
 * Check sensor type.
 *
 * @param pCheckType the check type
 * @return the int
 */
public int CheckSensorType(SensorDTO pCheckType)
	{
		int itype=-1;
		
		if (pCheckType instanceof ActivitySensorDTO)
			itype=1;
		else if (pCheckType instanceof BodyTemperatureSensorDTO)
			itype=2;
		else if (pCheckType instanceof BreathRateSensorDTO)
			itype=3;
		else if (pCheckType instanceof HeartBeatRateSensorDTO)
			itype=4;
		else if (pCheckType instanceof EnvironmentTemperatureSensorDTO)
			itype=5;
		else if (pCheckType instanceof GasSensorDTO)
			itype=6;
		else if (pCheckType instanceof LocationSensorDTO)
			itype=7;
		
		return itype;
	}
	
	/**
	 * Checks if is activity sensor.
	 *
	 * @param pCheck the check
	 * @return true, if successful
	 */
	public boolean IsActivitySensor(SensorDTO pCheck)
	{
		if (CheckSensorType(pCheck)==1)
			return true;
		else
			return false;
	}
	
	/**
	 * Checks if is body temperature sensor.
	 *
	 * @param pCheck the check
	 * @return true, if successful
	 */
	public boolean IsBodyTemperatureSensor(SensorDTO pCheck)
	{
		if (CheckSensorType(pCheck)==2)
			return true;
		else
			return false;
	}
	
	/**
	 * Checks if is breath rate sensor.
	 *
	 * @param pCheck the check
	 * @return true, if successful
	 */
	public boolean IsBreathRateSensor(SensorDTO pCheck)
	{
		if (CheckSensorType(pCheck)==3)
			return true;
		else
			return false;
	}
	
	/**
	 * Checks if is heart beat rate sensor.
	 *
	 * @param pCheck the check
	 * @return true, if successful
	 */
	public boolean IsHeartBeatRateSensor(SensorDTO pCheck)
	{
		if (CheckSensorType(pCheck)==4)
			return true;
		else
			return false;
	}
	
	/**
	 * Checks if is environment temperature sensor.
	 *
	 * @param pCheck the check
	 * @return true, if successful
	 */
	public boolean IsEnvironmentTemperatureSensor(SensorDTO pCheck)
	{
		if (CheckSensorType(pCheck)==5)
			return true;
		else
			return false;
	}
	
	/**
	 * Checks if is gas sensor sensor.
	 *
	 * @param pCheck the check
	 * @return true, if successful
	 */
	public boolean IsGasSensorSensor(SensorDTO pCheck)
	{
		if (CheckSensorType(pCheck)==6)
			return true;
		else
			return false;
	}
	
	/**
	 * Checks if is location sensor.
	 *
	 * @param pCheck the check
	 * @return true, if successful
	 */
	public boolean IsLocationSensor(SensorDTO pCheck)
	{
		if (CheckSensorType(pCheck)==7)
			return true;
		else
			return false;
	}
	
	/**
	 * Convert string to bd.
	 *
	 * @param szvalue the szvalue
	 * @return the big decimal
	 */
	public BigDecimal ConvertStringToBD(String szvalue)
	{
		return new BigDecimal(szvalue);		
	}
	
	/**
	 * Convert string toint.
	 *
	 * @param szvalue the szvalue
	 * @return the int
	 */
	public int ConvertStringToint(String szvalue)
	{
		return Integer.valueOf(szvalue);		
	}
	
	/**
	 * This String utility or util method can be used to merge 2 arrays of
	 * string values. If the input arrays are like this array1 = {"a", "b" ,
	 * "c"} array2 = {"c", "d", "e"} Then the output array will have {"a", "b" ,
	 * "c", "d", "e"}
	 * 
	 * This takes care of eliminating duplicates and checks null values.
	 *
	 * @param array1 the array1
	 * @param array2 the array2
	 * @return the string[]
	 */
	 @SuppressWarnings({ "unchecked", "rawtypes" })
	public  String[] mergeStringArrays(String array1[], String array2[]) {
	 
	  if (array1 == null || array1.length == 0)
	   return array2;
	  if (array2 == null || array2.length == 0)
	   return array1;
	  List array1List = Arrays.asList(array1);
	  List array2List = Arrays.asList(array2);
	  List result = new ArrayList(array1List);  
	  List tmp = new ArrayList(array1List);
	  tmp.retainAll(array2List);
	  result.removeAll(tmp);
	  result.addAll(array2List);  
	  return ((String[]) result.toArray(new String[result.size()]));
	 }
	 
	 /**
 	 * This String utility or util method can be used to trim all the String
 	 * values in list of Strings. For input [" a1 ", "b1 ", " c1"] the output
 	 * will be {"a1", "b1", "c1"} Method takes care of null values. This method
 	 * is collections equivalent of the trim method for String array.
 	 *
 	 * @param values the values
 	 * @return the list
 	 */
	 @SuppressWarnings({ "rawtypes", "unchecked" })
	public  List trim(final List values) {

	  List newValues = new ArrayList();
	  for (int i = 0, length = values.size(); i < length; i++) {
	   String v = (String) values.get(i);
	   if (v != null) {
	    v = v.trim();
	   }
	   newValues.add(v);
	  }
	  return newValues;
	 }
	 
	 /**
 	 * This String utility or util method can be used to
 	 * trim all the String values in the string array.
 	 * For input {" a1 ", "b1 ", " c1"}
 	 * the output will be {"a1", "b1", "c1"}
 	 * Method takes care of null values.
 	 *
 	 * @param values the values
 	 * @return the string[]
 	 */
	 public  String[] trim(final String[] values) {

	  for (int i = 0, length = values.length; i < length; i++) {
	   if (values[i] != null) {
	    values[i] = values[i].trim();                                
	   }
	  }
	  return values;

	 }
	 
	 
	 /**
 	 * This String util method removes single or double quotes
 	 * from a string if its quoted.
 	 * for input string = "mystr1" output will be = mystr1
 	 * for input string = 'mystr2' output will be = mystr2
 	 *
 	 * @param s the s
 	 * @return value unquoted, null if input is null.
 	 */
	 public  String unquote(String s) {

	  if (s != null
	    && ((s.startsWith("\"") && s.endsWith("\""))
	    || (s.startsWith("'") && s.endsWith("'")))) {

	   s = s.substring(1, s.length() - 1);
	  }
	  return s;
	 }
	 
 	/**
 	 * Same method as above but using the ?: syntax which is shorter. You can use whichever you prefer.
 	 * This String util method removes single or double quotes from a string if
 	 * its quoted. for input string = "mystr1" output will be = mystr1 for input
 	 * 
 	 * string = 'mystr2' output will be = mystr2
 	 *
 	 * @param s the s
 	 * @return value unquoted, null if input is null.
 	 */
	 public  String unquote2(String s) {

	  return (s != null && ((s.startsWith("\"") && s.endsWith("\"")) || (s
	    .startsWith("'") && s.endsWith("'")))) ? s = s.substring(1, s
	    .length() - 1) : s;

	 } 

	 /**
 	 * This method is used to split the given string into different tokens at
 	 * the occurrence of specified delimiter
 	 * An example :
 	 * <code>"abcdzefghizlmnop"</code> and using a delimiter <code>"z"</code>
 	 * would give following output
 	 * <code>"abcd" "efghi" "lmnop"</code>.
 	 *
 	 * @param str The string that needs to be broken
 	 * @param delimeter The delimiter used to break the string
 	 * @return a string array
 	 */

	 public  String[] getTokensArray(String str, String delimeter) {

	 String[] data;
	 if(str == null){
	     return null;
	 }

	 if (delimeter == null || "".equals(delimeter)
	 || "".equals(str)) {
	     data = new String[1];
	     data[0] = str;
	     return data;
	 } else {
	     StringTokenizer st = new StringTokenizer(str, delimeter);
	     int tokenCount = st.countTokens();
	     data = new String[tokenCount];
	     for (int i = 0; st.hasMoreTokens(); i++) {
	     data[i] = st.nextToken();
	 }
	 return data;
	 }
	 }

	 
	 /**
 	 * Check a String ends with another string ignoring the case.
 	 *
 	 * @param str the str
 	 * @param suffix the suffix
 	 * @return true, if successful
 	 */
	 public  boolean endsWithIgnoreCase(String str, String suffix) {

	     if (str == null || suffix == null) {
	         return false;
	     }
	     if (str.endsWith(suffix)) {
	         return true;
	     }
	     if (str.length() < suffix.length()) {
	         return false;
	     } else {
	         return str.toLowerCase().endsWith(suffix.toLowerCase());
	     }
	 }

	 /**
 	 * Check a String starts with another string ignoring the case.
 	 *
 	 * @param str the str
 	 * @param prefix the prefix
 	 * @return true, if successful
 	 */

	 public  boolean startsWithIgnoreCase(String str, String prefix) {

	     if (str == null || prefix == null) {
	         return false;
	     }
	     if (str.startsWith(prefix)) {
	         return true;
	     }
	     if (str.length() < prefix.length()) {
	         return false;
	     } else {
	         return str.toLowerCase().startsWith(prefix.toLowerCase());
	     }
	 }
	 
	
	 
	 /**
	 * Return a not null string.
	 *
	 * @param s String
	 * @return empty string if it is null otherwise the string passed in as
	 * parameter.
	 */

	 public  String nonNull(String s) {

	 if (s == null) {
	     return "";
	 }
	 return s;
	 }
	 

}
