/*
 * 
 */
package eu.esponder.df.eventhandler;

import eu.esponder.event.model.ESponderEvent;

// TODO: Auto-generated Javadoc
/**
 * The Interface ActionEventHandlerRemoteService.
 */
public interface ActionEventHandlerRemoteService extends
ActionEventHandlerService {
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.eventhandler.ActionEventHandlerService#ProcessEvent(eu.esponder.event.model.ESponderEvent)
	 */
	public void ProcessEvent(ESponderEvent<?> pEvent);
}
