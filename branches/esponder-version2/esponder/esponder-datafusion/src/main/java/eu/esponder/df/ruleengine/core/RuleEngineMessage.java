/**
 * RuleEngineMessage
 * 
 * This Java class implement a simple rule message. Only type and text is supported. Using this class we can pass
 * a fact type of this.type and message value type of this.msgtext to rule engine.
 * 
 * 
 * @Project   esponder
 * @package   Datafusion
*/
package eu.esponder.df.ruleengine.core;

// TODO: Auto-generated Javadoc
/**
 * The Class RuleEngineMessage.
 *
 * @author tdim
 */
public class RuleEngineMessage {
	
	/** The type. */
	private String type;

	/** The msgtext. */
	private String msgtext;

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {

		return type;

	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {

		this.type = type;

	}

	/**
	 * Gets the msgtext.
	 *
	 * @return the msgtext
	 */
	public String getMsgtext() {

		return msgtext;

	}

	/**
	 * Sets the msgtext.
	 *
	 * @param msgtext the new msgtext
	 */
	public void setMsgtext(String msgtext) {

		this.msgtext = msgtext;

	}
}
