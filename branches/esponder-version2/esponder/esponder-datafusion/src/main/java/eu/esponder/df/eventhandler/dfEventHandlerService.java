/*
 * 
 */
package eu.esponder.df.eventhandler;


import java.util.List;

import javax.ejb.Local;

import eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType;
import eu.esponder.event.model.ESponderEvent;

// TODO: Auto-generated Javadoc
/**
 * The Interface dfEventHandlerService.
 */
@Local
public interface dfEventHandlerService {

	/**
	 * Adds the dto objects.
	 *
	 * @param fact the fact
	 */
	public void AddDTOObjects(Object fact);
	
	/**
	 * Adds the objects.
	 *
	 * @param fact the fact
	 */
	public void AddObjects(Object fact);
	
	/**
	 * Load knowledge.
	 */
	public void LoadKnowledge();
	
	/**
	 * Process rules.
	 */
	public void ProcessRules();
	
	/**
	 * Sets the rule engine type.
	 *
	 * @param dType the d type
	 * @param szPackageNameOrFlowName the sz package name or flow name
	 */
	public void SetRuleEngineType(RuleEngineType dType,String szPackageNameOrFlowName);
	
	/**
	 * Sets the rule engine type.
	 *
	 * @param dType the d type
	 * @param szDSLName the sz dsl name
	 * @param szDSLRName the sz dslr name
	 */
	public void SetRuleEngineType(RuleEngineType dType,String szDSLName,String szDSLRName);
	
	/**
	 * Creates the sensor snapshot.
	 *
	 * @param pEvent the event
	 * @return the list
	 */
	public List<Object> CreateSensorSnapshot(ESponderEvent<?> pEvent);
}


