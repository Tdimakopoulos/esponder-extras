/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelSkillType.
 */
@Entity
@DiscriminatorValue("SKILL_TYPE")
public class PersonnelSkillType extends ESponderType {

	private static final long serialVersionUID = -8445175631131821568L;

}
