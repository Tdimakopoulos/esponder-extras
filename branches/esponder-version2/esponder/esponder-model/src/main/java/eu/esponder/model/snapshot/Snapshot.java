/*
 * 
 */
package eu.esponder.model.snapshot;

import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;

import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
/**
 * The Class Snapshot.
 *
 * @param <T> the generic type
 */
@MappedSuperclass
public abstract class Snapshot<T> extends ESponderEntity<T> {
	
	private static final long serialVersionUID = 6928365958293895105L;
	
	/** The period. */
	@Embedded
	protected Period period;

	/**
	 * Gets the period.
	 *
	 * @return the period
	 */
	public Period getPeriod() {
		return period;
	}

	/**
	 * Sets the period.
	 *
	 * @param period the new period
	 */
	public void setPeriod(Period period) {
		this.period = period;
	}

}
