/*
 * 
 */
package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class HeartBeatRateSensor.
 */
@Entity
@DiscriminatorValue("BIOMED_HEARTBEAT")
public class HeartBeatRateSensor extends BiomedicalSensor implements ArithmeticMeasurementSensor {

	private static final long serialVersionUID = 9129567500478272162L;

}
