/*
 * 
 */
package eu.esponder.model.snapshot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;

// TODO: Auto-generated Javadoc
/**
 * The Class ReferencePOISnapshot.
 */
@Entity
@Table(name="reference_poi_snapshot")
@NamedQueries({
	@NamedQuery(name="ReferencePOISnapshot.findByOperationsCentre", query="select s from ReferencePOISnapshot s where s.operationsCentre.id=:operationsCentreID"),
	@NamedQuery(name="ReferencePOISnapshot.findByReferencePOIId", query="select s from ReferencePOISnapshot s where s.referencePOI=:ReferencePOIId"),
	@NamedQuery(name="ReferencePOISnapshot.findByTitle", query="select s from ReferencePOISnapshot s where s.title=:ReferencePOIsnapshotTitle"),
	@NamedQuery(name = "ReferencePOISnapshot.findByReferenceAndDate", query = "SELECT s FROM ReferencePOISnapshot s WHERE s.referencePOI.id = :ReferenceID AND s.period.dateTo <= :maxDate AND s.period.dateTo = "
			+ "(SELECT max(s.period.dateTo) FROM ReferencePOISnapshot s WHERE s.referencePOI.id = :referenceID)"),
	@NamedQuery(name = "ReferencePOISnapshot.findPreviousSnapshot", query = "Select s from ReferencePOISnapshot s where s.id=(select max(snapshot.id) from ReferencePOISnapshot snapshot where snapshot.referencePOI.id = :referenceID)"),
	@NamedQuery(name = "ReferencePOISnapshot.findAll", query = "Select s from ReferencePOISnapshot s")
})
public class ReferencePOISnapshot extends SpatialSnapshot<Long> {
	
	private static final long serialVersionUID = -1729481522641331340L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="POI_ID_SNAPSHOT")
	protected Long id;
	
	/** The reference file. */
	@Column(name="REF_FILE_SNAPSHOT")
	protected String referenceFile;
	
	/** The average size. */
	@Column(name="AVERAGE_SIZE_SNAPSHOT")
	protected Float averageSize;
	
	/** The title. Not Null,Unique*/
	@Column(name="TITLE_SNAPSHOT", nullable=false, unique=true, length=255)
	protected String title;
	
	/** The operations centre. */
	@ManyToOne
	@JoinColumn(name="OPERATIONS_CENTRE_ID_SNAPSHOT")
	protected OperationsCentre operationsCentre;
	
	/** The reference poi. Not Null*/
	@ManyToOne
	@JoinColumn(name="REFERENCE_POI_ID", nullable=false)
	protected ReferencePOI referencePOI;
	
	/** The previous. */
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private ReferencePOISnapshot previous;
	
	/** The next. */
	@OneToOne(mappedBy="previous")
	private ReferencePOISnapshot next;
	
	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public ReferencePOISnapshot getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previous the new previous
	 */
	public void setPrevious(ReferencePOISnapshot previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public ReferencePOISnapshot getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(ReferencePOISnapshot next) {
		this.next = next;
	}

	/**
	 * Gets the reference file.
	 *
	 * @return the reference file
	 */
	public String getReferenceFile() {
		return referenceFile;
	}

	/**
	 * Sets the reference file.
	 *
	 * @param referenceFile the new reference file
	 */
	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

	/**
	 * Gets the average size.
	 *
	 * @return the average size
	 */
	public Float getAverageSize() {
		return averageSize;
	}

	/**
	 * Sets the average size.
	 *
	 * @param averageSize the new average size
	 */
	public void setAverageSize(Float averageSize) {
		this.averageSize = averageSize;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OperationsCentre getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OperationsCentre operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	/**
	 * Gets the reference poi.
	 *
	 * @return the reference poi
	 */
	public ReferencePOI getReferencePOI() {
		return referencePOI;
	}

	/**
	 * Sets the reference poi.
	 *
	 * @param referencePOI the new reference poi
	 */
	public void setReferencePOI(ReferencePOI referencePOI) {
		this.referencePOI = referencePOI;
	}
}



