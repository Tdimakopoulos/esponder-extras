/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class TacticalActorType.
 */
@Entity
@DiscriminatorValue("TCL_ACTOR")
public final class TacticalActorType extends ActorType {

	private static final long serialVersionUID = -5463150247445092220L;
	
}
