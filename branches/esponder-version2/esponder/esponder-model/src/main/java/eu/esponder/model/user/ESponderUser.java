/*
 * 
 */
package eu.esponder.model.user;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.OperationsCentre;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderUser.
 * Entity that manage the esponderuser, esponder user is system users, for portal, optimizer etc.
 */
@Entity
@Table(name="esponder_user")
@NamedQueries({
	@NamedQuery(name="ESponderUser.findByUserName", query="select u from ESponderUser u where u.userName=:userName"),
	@NamedQuery(name="ESponderUser.findAll", query="select u from ESponderUser u")
})
public class ESponderUser extends ESponderEntity<Long> {
	
	private static final long serialVersionUID = -2512583407793541959L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ESPONDER_USER_ID")
	private Long id;

	/** The user name. Not Null, Unique*/
	@Column(name="USER_NAME", nullable=false, unique=true)
	private String userName;
	
	/** The role. */
	@Column(name="ROLE")
	private String role;
	
	/** The actor that represent the user*/
	@OneToOne
	@JoinColumn(name="ACTOR_ID")
	private Actor actor;
	
	/**
	 * Gets the actor.
	 *
	 * @return the actor
	 */
	public Actor getActor() {
		return actor;
	}

	/**
	 * Sets the actor.
	 *
	 * @param actor the new actor
	 */
	public void setActor(Actor actor) {
		this.actor = actor;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the operations centres.
	 *
	 * @return the operations centres
	 */
	//public Set<OperationsCentre> getOperationsCentres() {
		//return operationsCentres;
	//}

	/**
	 * Sets the operations centres.
	 *
	 * @param operationsCentres the new operations centres
	 */
	//public void setOperationsCentres(Set<OperationsCentre> operationsCentres) {
		//this.operationsCentres = operationsCentres;
	//}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ESponderUser [id=" + id + ", userName=" + userName + "]";
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(String role) {
		this.role = role;
	}

}
