/*
 * 
 */
package eu.esponder.model.snapshot.action;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.model.snapshot.SpatialSnapshot;
import eu.esponder.model.snapshot.status.ActionPartSnapshotStatus;


// TODO: Auto-generated Javadoc
/**
 * The Class ActionPartSnapshot.
 */
@Entity
@Table(name="action_part_snapshot")
public class ActionPartSnapshot extends SpatialSnapshot<Long> {
	
	private static final long serialVersionUID = -433163591190712580L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_PART_SNAPSHOT_ID")
	private Long id;
	
	/** The action part. Not Null*/
	@ManyToOne
	@JoinColumn(name="ACTION_PART_ID", nullable=false)
	private ActionPart actionPart;
	
	/** The status. Not Null*/
	@Enumerated(EnumType.STRING)
	@Column(name="ACTION_PART_SNAPSHOT_STATUS", nullable=false)
	private ActionPartSnapshotStatus status;
	
	/** The previous. */
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private ActionPartSnapshot previous;
	
	/** The next. */
	@OneToOne(mappedBy="previous")
	private ActionPartSnapshot next;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the action part.
	 *
	 * @return the action part
	 */
	public ActionPart getActionPart() {
		return actionPart;
	}

	/**
	 * Sets the action part.
	 *
	 * @param actionPart the new action part
	 */
	public void setActionPart(ActionPart actionPart) {
		this.actionPart = actionPart;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ActionPartSnapshotStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ActionPartSnapshotStatus status) {
		this.status = status;
	}

	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public ActionPartSnapshot getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previous the new previous
	 */
	public void setPrevious(ActionPartSnapshot previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public ActionPartSnapshot getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(ActionPartSnapshot next) {
		this.next = next;
	}
	
}
