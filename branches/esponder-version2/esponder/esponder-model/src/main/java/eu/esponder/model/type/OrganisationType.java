/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

// TODO: Auto-generated Javadoc
/**
 * The Class OrganisationType.
 */
@Entity
@DiscriminatorValue("ORGANIZATION")
public class OrganisationType extends ESponderType {

	private static final long serialVersionUID = -3273246628570470484L;

}
