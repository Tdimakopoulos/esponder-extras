/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.category.EquipmentCategory;
import eu.esponder.model.crisis.resource.plan.PlannableResource;
import eu.esponder.model.crisis.resource.sensor.Sensor;


// TODO: Auto-generated Javadoc
/**
 * The Class Equipment.
 * Entity class the manage the Equipments
 */
@Entity
@Table(name="equipment")
@NamedQueries({
	@NamedQuery(name="Equipment.findByTitle", query="select e from Equipment e where e.title=:title")
})
public class Equipment extends PlannableResource {

	private static final long serialVersionUID = -4705569532455885376L;

	/** The actor. */
	@ManyToOne
	@JoinColumn(name="ACTOR_ID")
	private Actor actor;
	
	/** The container. */
	@ManyToOne
	@JoinColumn(name="CONTAINER_ID")
	private Equipment container;
	
	/** The parts. */
	@OneToMany(mappedBy="container")
	private Set<Equipment> parts;
	
	/** The sensors. */
	@OneToMany(mappedBy="equipment")
	private Set<Sensor> sensors;
	
	/** The equipment category. */
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private EquipmentCategory equipmentCategory;

	/**
	 * Gets the actor.
	 *
	 * @return the actor
	 */
	public Actor getActor() {
		return actor;
	}

	/**
	 * Sets the actor.
	 *
	 * @param actor the new actor
	 */
	public void setActor(Actor actor) {
		this.actor = actor;
	}

	/**
	 * Gets the container.
	 *
	 * @return the container
	 */
	public Equipment getContainer() {
		return container;
	}

	/**
	 * Sets the container.
	 *
	 * @param container the new container
	 */
	public void setContainer(Equipment container) {
		this.container = container;
	}

	/**
	 * Gets the parts.
	 *
	 * @return the parts
	 */
	public Set<Equipment> getParts() {
		return parts;
	}

	/**
	 * Sets the parts.
	 *
	 * @param parts the new parts
	 */
	public void setParts(Set<Equipment> parts) {
		this.parts = parts;
	}

	/**
	 * Gets the sensors.
	 *
	 * @return the sensors
	 */
	public Set<Sensor> getSensors() {
		return sensors;
	}

	/**
	 * Sets the sensors.
	 *
	 * @param sensors the new sensors
	 */
	public void setSensors(Set<Sensor> sensors) {
		this.sensors = sensors;
	}

	/**
	 * Gets the equipment category.
	 *
	 * @return the equipment category
	 */
	public EquipmentCategory getEquipmentCategory() {
		return equipmentCategory;
	}

	/**
	 * Sets the equipment category.
	 *
	 * @param equipmentCategory the new equipment category
	 */
	public void setEquipmentCategory(EquipmentCategory equipmentCategory) {
		this.equipmentCategory = equipmentCategory;
	}
	
}
