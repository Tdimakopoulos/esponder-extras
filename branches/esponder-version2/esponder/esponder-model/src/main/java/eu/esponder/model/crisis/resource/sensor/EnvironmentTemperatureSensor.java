/*
 * 
 */
package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class EnvironmentTemperatureSensor.
 */
@Entity
@DiscriminatorValue("ENV_TEMP")
public class EnvironmentTemperatureSensor extends EnvironmentalSensor implements ArithmeticMeasurementSensor {

	private static final long serialVersionUID = 738201853393625000L;


}
