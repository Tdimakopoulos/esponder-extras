/*
 * 
 */
package eu.esponder.model.snapshot.status;


// TODO: Auto-generated Javadoc
/**
 * The Enum ActionSnapshotStatus.
 */
public enum ActionSnapshotStatus {
	
	/** The started. */
	STARTED,
	
	/** The inprogress. */
	INPROGRESS,
	
	/** The finalized. */
	FINALIZED,
	
	/** The paused. */
	PAUSED,
	
	/** The completed. */
	COMPLETED,
	
	/** The canceled. */
	CANCELED;
	
	/**
	 * Gets the action snapshot status enum.
	 *
	 * @param value the value
	 * @return the action snapshot status enum
	 */
	public static ActionSnapshotStatus getActionSnapshotStatusEnum(int value) {
		switch (value) {
		case 0:
			return STARTED;
		case 1:
			return INPROGRESS;
		case 2:
			return FINALIZED;
		case 3:
			return PAUSED;
		case 4:
			return COMPLETED;
		case 5:
			return CANCELED;
		default:
			return null;
		}
	}
}
