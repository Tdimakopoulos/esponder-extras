/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.Entity;

import eu.esponder.model.crisis.resource.sensor.Sensor;


// TODO: Auto-generated Javadoc
/**
 * The Class CrisisType.
 */
@Entity
public abstract class CrisisType extends ESponderType {

	private static final long serialVersionUID = -5814308761452031478L;
	
	/* (non-Javadoc)
	 * @see eu.esponder.model.type.ESponderType#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    final Sensor other = (Sensor) obj;
	    if (id != other.getId()) {
	        return false;
	    }
	    return true;
	}

}