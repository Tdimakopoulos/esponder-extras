/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.snapshot.resource.FRTeamSnapshot;

// TODO: Auto-generated Javadoc
/**
 * The Class FRTeam.
 * This is the entity class that define the Team. 
 * If the operation center is Meoc , then cheifs are associated with the team. 
 */
@Entity
@Table(name="frteam")
@NamedQueries({
	@NamedQuery(name="FRTeam.findTeamsByMeoc", query="select t from FRTeam t where t.meoc= :meoc"),
	@NamedQuery(name="FRTeam.findTeamsByChief", query="select t from FRTeam t where t.frchief= :frchief")
})
public class FRTeam extends ESponderEntity<Long> {

	private static final long serialVersionUID = 1911200656062999563L;
	
	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="FRTEAM_ID")
	protected Long id;
	
	/** The title. Not Null, Unique, this store the name of the team*/
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	protected String title;

	/** The FR Chief. */
	@OneToOne
	@JoinColumn(name="CHIF_ID")
	private Actor frchief;
	
	
	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<FRTeamSnapshot> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<FRTeamSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	/** The MEOC. */
	@OneToOne
	@JoinColumn(name="MEOC_ID")
	private OperationsCentre meoc;
	
	/** The snapshots. */
	@OneToMany(mappedBy="frteam")
	private Set<FRTeamSnapshot> snapshots;
	
	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Gets the frcheif.
	 *
	 * @return the frcheif
	 */
	public Actor getFrchief() {
		return frchief;
	}

	/**
	 * Sets the frcheif.
	 *
	 * @param frcheif the new frcheif
	 */
	public void setFrcheif(Actor frchief) {
		this.frchief = frchief;
	}

	/**
	 * Gets the meoc.
	 *
	 * @return the meoc
	 */
	public OperationsCentre getMeoc() {
		return meoc;
	}

	/**
	 * Sets the meoc.
	 *
	 * @param meoc the new meoc
	 */
	public void setMeoc(OperationsCentre meoc) {
		this.meoc = meoc;
	}

	
}
