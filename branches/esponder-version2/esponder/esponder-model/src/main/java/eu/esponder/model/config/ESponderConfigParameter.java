/*
 * 
 */
package eu.esponder.model.config;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.Identifiable;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderConfigParameter.
 * Abstract class to store the configuration settings. 
 * At the moment we have two types of settings, Drools and OSGI.
 * Configuration is stored as a parameter-value pair
 */
@Entity
@Table(name="configuration")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="DISCRIMINATOR")
@NamedQueries({
	@NamedQuery(name="ESponderConfigParameter.findByName", query="select t from ESponderConfigParameter t where t.parameterName=:parameterName")
})public abstract class ESponderConfigParameter implements Identifiable<Long> {
	
	private static final long serialVersionUID = 5619634457839448917L;

	/** The id of the entity. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CONFIGURATION_ID")
	private Long id;
	
	/** The configuration parameter name. Not null, Unique*/
	@Column(name="PARAMETER_NAME", nullable=false, unique=true, length=255)
	private String parameterName;
	
	/** The configuration parameter value. Not null*/
	@Column(name="PARAMETER_VALUE", nullable=false, length=255)
	private String parameterValue;
	
	/** An additional description of the configuration parameter and/or value. */
	@Column(name="DESCRIPTION")
	private String description;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the parameter name.
	 *
	 * @return the parameter name
	 */
	public String getParameterName() {
		return parameterName;
	}

	/**
	 * Sets the parameter name.
	 *
	 * @param parameterName the new parameter name
	 */
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	/**
	 * Gets the parameter value.
	 *
	 * @return the parameter value
	 */
	public String getParameterValue() {
		return parameterValue;
	}

	/**
	 * Sets the parameter value.
	 *
	 * @param parameterValue the new parameter value
	 */
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
		
}
