/*
 * 
 */
package eu.esponder.model.config;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

// TODO: Auto-generated Javadoc
/**
 * The Class OSGIConfiguration.
 * This is a class based on the EsponderConfigParameter and store information about the OSGI settings
 * OSGI is used by the EventAdmin module
 */
@Entity
@DiscriminatorValue("OSGI")
public class OSGIConfiguration extends ESponderConfigParameter {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1472802157259993847L;
	
}
