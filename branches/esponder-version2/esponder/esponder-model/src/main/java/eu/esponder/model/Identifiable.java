/*
 * 
 */
package eu.esponder.model;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Interface Identifiable.
 * Internal used class to control the identifier of each managed (i.e. persisted) entity.
 *
 * @param <T> the generic type
 */
public interface Identifiable<T> extends Serializable {
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public T getId();
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(T id);

}
