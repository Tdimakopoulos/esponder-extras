/*
 * 
 */
package eu.esponder.model.crisis.view;

// TODO: Auto-generated Javadoc
/**
 * The Class VoIPURL.
 */
public class VoIPURL extends URL {

	private static final long serialVersionUID = 3193388189632590549L;

	/**
	 * Instantiates a new VoIP URL.
	 */
	public VoIPURL() {
		this.setProtocol("sip");
	}
}
