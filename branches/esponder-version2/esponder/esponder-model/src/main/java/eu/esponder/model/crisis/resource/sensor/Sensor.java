/*
 * 
 */
package eu.esponder.model.crisis.resource.sensor;

import java.util.Set;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.crisis.resource.Resource;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.snapshot.sensor.config.StatisticsConfig;
import eu.esponder.model.type.SensorType;

// TODO: Auto-generated Javadoc
/**
 * The Class Sensor.
 * This is the main sensor class.
 */
@Entity
@Table(name="sensor")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="DISCRIMINATOR")
@NamedQueries({
	@NamedQuery(name="Sensor.findByTitle", query="select s from Sensor s where s.title=:title")
})
public abstract class Sensor extends Resource {

	private static final long serialVersionUID = -5390561449137637770L; 

	/** The label. */
	private String label;
	
	/** The sensor type. */
	@ManyToOne
	@JoinColumn(name="SENSOR_TYPE_ID", nullable=false)
	private SensorType sensorType;
	
	/** The equipment. */
	@ManyToOne
	@JoinColumn(name="EQUIPMENT_ID")
	private Equipment equipment;
	
	/** The snapshots. */
	@OneToMany(mappedBy="sensor")
	private Set<SensorSnapshot> snapshots;
	
	/** The configuration. */
	@OneToMany(mappedBy="sensor")
	private Set<StatisticsConfig> configuration;

	/**
	 * Gets the sensor type.
	 *
	 * @return the sensor type
	 */
	public SensorType getSensorType() {
		return sensorType;
	}

	/**
	 * Sets the sensor type.
	 *
	 * @param sensorType the new sensor type
	 */
	public void setSensorType(SensorType sensorType) {
		this.sensorType = sensorType;
	}

	/**
	 * Gets the equipment.
	 *
	 * @return the equipment
	 */
	public Equipment getEquipment() {
		return equipment;
	}

	/**
	 * Sets the equipment.
	 *
	 * @param equipment the new equipment
	 */
	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<SensorSnapshot> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<SensorSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	/**
	 * Gets the configuration.
	 *
	 * @return the configuration
	 */
	public Set<StatisticsConfig> getConfiguration() {
		return configuration;
	}

	/**
	 * Sets the configuration.
	 *
	 * @param configuration the new configuration
	 */
	public void setConfiguration(Set<StatisticsConfig> configuration) {
		this.configuration = configuration;
	}

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Sets the label.
	 *
	 * @param label the new label
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    final Sensor other = (Sensor) obj;
	    if (id != other.id) {
	        return false;
	    }
	    return true;
	}
}
