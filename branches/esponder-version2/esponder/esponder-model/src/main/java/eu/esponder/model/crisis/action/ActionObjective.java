/*
 * 
 */
package eu.esponder.model.crisis.action;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.location.LocationArea;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionObjective.
 * A entity class to manage the action objectives
 */
@Entity
@Table(name="action_objective")
@NamedQueries({
	@NamedQuery(name="ActionObjective.findByTitle", query="select ap from ActionObjective ap where ap.title=:title")
})
public class ActionObjective extends ESponderEntity<Long> {

	private static final long serialVersionUID = 284141105295838636L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_OBJECTIVE_ID")
	private Long id;
	
	/** The title. Not Null, Unique*/
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	/** The period. Define when this part start and when is finished*/
	@Embedded
	private Period period;
	
	/** The location area.*/
	@ManyToOne
	@JoinColumn(name="LOCATION_AREA_ID")
	private LocationArea locationArea;
	
	/** The action. The parent action , Not Null*/
	@ManyToOne
	@JoinColumn(name="ACTION_ID", nullable=false)
	private Action action;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the period.
	 *
	 * @return the period
	 */
	public Period getPeriod() {
		return period;
	}

	/**
	 * Sets the period.
	 *
	 * @param period the new period
	 */
	public void setPeriod(Period period) {
		this.period = period;
	}

	/**
	 * Gets the location area.
	 *
	 * @return the location area
	 */
	public LocationArea getLocationArea() {
		return locationArea;
	}

	/**
	 * Sets the location area.
	 *
	 * @param locationArea the new location area
	 */
	public void setLocationArea(LocationArea locationArea) {
		this.locationArea = locationArea;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public Action getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(Action action) {
		this.action = action;
	}
	
}
