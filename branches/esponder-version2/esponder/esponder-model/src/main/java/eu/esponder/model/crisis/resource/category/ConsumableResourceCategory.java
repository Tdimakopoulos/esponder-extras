/*
 * 
 */
package eu.esponder.model.crisis.resource.category;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.type.ConsumableResourceType;

// TODO: Auto-generated Javadoc
/**
 * The Class ConsumableResourceCategory.
 * Manage information about a Consumable resource category
 */
@Entity
@Table(name="consumables_category")
@NamedQueries({
	@NamedQuery(name="ConsumableResourceCategory.findByType", query="select a from ConsumableResourceCategory a where a.consumableResourceType=:consumableResourceType") })
public class ConsumableResourceCategory extends LogisticsCategory {

	private static final long serialVersionUID = -7110133452197870817L;

	/** The consumable resource type. */
	@OneToOne
	@JoinColumn(name="CONSUMABLE_RESOURCE_TYPE_ID", nullable=false)
	private ConsumableResourceType consumableResourceType;
	
	/**
 * Gets the consumable resource type.
 *
 * @return the consumable resource type
 */
public ConsumableResourceType getConsumableResourceType() {
		return consumableResourceType;
	}

	/**
	 * Sets the consumable resource type.
	 *
	 * @param consumableResourceType the new consumable resource type
	 */
	public void setConsumableResourceType(
			ConsumableResourceType consumableResourceType) {
		this.consumableResourceType = consumableResourceType;
	}

}
