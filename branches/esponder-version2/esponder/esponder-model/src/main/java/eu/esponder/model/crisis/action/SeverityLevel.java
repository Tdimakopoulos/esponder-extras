/*
 * 
 */
package eu.esponder.model.crisis.action;

// TODO: Auto-generated Javadoc
/**
 * The Enum SeverityLevel. Each action have a severity level, this at the moment is define as Minimal, Medium and Serious.
 */
public enum SeverityLevel {
	
	/** The minimal. */
	MINIMAL,
	
	/** The medium. */
	MEDIUM,
	
	/** The serious. */
	SERIOUS
}
