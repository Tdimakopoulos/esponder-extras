package eu.esponder.test.crisis;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OperationsCentreCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.crisis.resource.plan.PlannableResourcePowerDTO;
import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionExpressionEnumDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;
import eu.esponder.dto.model.snapshot.status.ActionSnapshotStatusDTO;
import eu.esponder.dto.model.snapshot.status.CrisisContextSnapshotStatusDTO;
import eu.esponder.dto.model.type.ActionTypeDTO;
import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;
import eu.esponder.model.snapshot.Period;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class ActionServiceTest extends ControllerServiceTest {
	private static double RADIUS = 1;
	private static int SECONDS = 30;


	@Test(groups = "createBasics")
	public void CreateCrisisContext() throws ClassNotFoundException {

		CrisisTypeDTO crisisType1 = (CrisisTypeDTO) typeService.findDTOByTitle("Fire");
		CrisisTypeDTO crisisType2 = (CrisisTypeDTO) typeService.findDTOByTitle("Injury");
		PointDTO pointDTO = new PointDTO(new BigDecimal(3), new BigDecimal(3), new BigDecimal(3)); 
		SphereDTO sphereDTO = new SphereDTO(pointDTO, new BigDecimal(2), "Fire Brigade Drill Location Area");
		sphereDTO = (SphereDTO) genericService.createEntityRemote(sphereDTO, this.userID);
		CrisisContextDTO crisisContextDTO = new CrisisContextDTO();
		crisisContextDTO.setCrisisLocation(sphereDTO);
		crisisContextDTO.setTitle("Fire Brigade Drill");
		crisisContextDTO.setCrisisTypes(new HashSet<CrisisTypeDTO>());
		crisisContextDTO.getCrisisTypes().add(crisisType1);
		crisisContextDTO.getCrisisTypes().add(crisisType2);
		crisisService.createCrisisContextRemote(crisisContextDTO, this.userID);
	}

	@Test(groups = "createBasics")
	public void CreateCrisisResourceplan() throws ClassNotFoundException {

		CrisisTypeDTO crisisTypeFire = (CrisisTypeDTO) typeService.findDTOByTitle("Fire");
		CrisisTypeDTO crisisTypeEarthquake = (CrisisTypeDTO) typeService.findDTOByTitle("Earthquake");

		CrisisResourcePlanDTO crisisResourcePlanFire = new CrisisResourcePlanDTO();
		crisisResourcePlanFire.setCrisisType(crisisTypeFire);
		crisisResourcePlanFire.setTitle("Fire Plan");


		CrisisResourcePlanDTO crisisResourcePlanEarthquake = new CrisisResourcePlanDTO();
		crisisResourcePlanEarthquake.setCrisisType(crisisTypeEarthquake);
		crisisResourcePlanEarthquake.setTitle("Earthquake Plan");

		crisisService.createCrisisResourcePlanRemote(crisisResourcePlanFire, this.userID);
		crisisService.createCrisisResourcePlanRemote(crisisResourcePlanEarthquake, this.userID);

	}

	/***********************************************************************************/

	@Test(groups = "createBasics")
	public void createPlannableResourcePower() throws ClassNotFoundException {

		CrisisResourcePlanDTO firePlan = this.crisisService.findCrisisResourcePlanByTitleRemote("Fire Plan", this.userID);

		ReusableResourceTypeDTO reusType = (ReusableResourceTypeDTO) this.typeService.findDTOByTitle("Medical Kit");
		ReusableResourceCategoryDTO reusableCategory = this.resourceCategoryService.findReusableCategoryDTOByType(reusType);

		ConsumableResourceTypeDTO consType = (ConsumableResourceTypeDTO) this.typeService.findDTOByTitle("Medical Resource");
		ConsumableResourceCategoryDTO consCategory = this.resourceCategoryService.findConsumableCategoryDTOByType(consType);

		PersonnelCategoryDTO persCategory = (PersonnelCategoryDTO) this.resourceCategoryService.findByIdRemote(PersonnelCategoryDTO.class, new Long(12));

		OperationsCentreCategoryDTO ocCategoryEOC = (OperationsCentreCategoryDTO) this.resourceCategoryService.findByIdRemote(OperationsCentreCategoryDTO.class, new Long(1));
		OperationsCentreCategoryDTO ocCategoryMEOC = (OperationsCentreCategoryDTO) this.resourceCategoryService.findByIdRemote(OperationsCentreCategoryDTO.class, new Long(2));

		// Actors
		PlannableResourcePowerDTO power1 = new PlannableResourcePowerDTO();
		power1.setCrisisResourcePlanId(firePlan.getId());
		power1.setPower(2);
		power1.setPlanableResourceCategoryId(persCategory.getId());
		crisisService.createPlannableResourcePowerRemote(power1, userID);


		//Reusables
		PlannableResourcePowerDTO power2 = new PlannableResourcePowerDTO();
		power2.setCrisisResourcePlanId(firePlan.getId());
		power2.setPower(1);
		power2.setPlanableResourceCategoryId(reusableCategory.getId());
		crisisService.createPlannableResourcePowerRemote(power2, userID);


		//Consumables
		PlannableResourcePowerDTO power3 = new PlannableResourcePowerDTO();
		power3.setCrisisResourcePlanId(firePlan.getId());
		power3.setPower(1);
		power3.setPlanableResourceCategoryId(consCategory.getId());
		crisisService.createPlannableResourcePowerRemote(power3, userID);

		// OC
		PlannableResourcePowerDTO power4 = new PlannableResourcePowerDTO();
		power4.setCrisisResourcePlanId(firePlan.getId());
		power4.setPower(1);
		power4.setPlanableResourceCategoryId(ocCategoryEOC.getId());
		crisisService.createPlannableResourcePowerRemote(power4, userID);
		
		PlannableResourcePowerDTO power5 = new PlannableResourcePowerDTO();
		power5.setCrisisResourcePlanId(firePlan.getId());
		power5.setPower(1);
		power5.setPlanableResourceCategoryId(ocCategoryMEOC.getId());
		crisisService.createPlannableResourcePowerRemote(power5, userID);
	}


	@Test(groups = "createBasics")
	public void CreateRegisteredResources() throws ClassNotFoundException {

		RegisteredConsumableResourceDTO regConsumableResource1 = new RegisteredConsumableResourceDTO();
		RegisteredConsumableResourceDTO regConsumableResource2 = new RegisteredConsumableResourceDTO();

		RegisteredReusableResourceDTO regReusableResource1 = new RegisteredReusableResourceDTO();
		RegisteredReusableResourceDTO regReusableResource2 = new RegisteredReusableResourceDTO();

		RegisteredOperationsCentreDTO regOperationsCentre1 = new RegisteredOperationsCentreDTO();

		regConsumableResource1.setQuantity(new BigDecimal(20));
		regConsumableResource1.setStatus(ResourceStatusDTO.AVAILABLE);
		regConsumableResource1.setTitle("RegisteredConsumable1");
		ConsumableResourceTypeDTO consumableResourceType1 = (ConsumableResourceTypeDTO) typeService.findDTOByTitle("Medical Resource");
		ConsumableResourceCategoryDTO consumableCategoryDTO1 = resourceCategoryService.findConsumableCategoryDTOByType(consumableResourceType1);
		regConsumableResource1.setConsumableResourceCategoryId(consumableCategoryDTO1.getId());

		regConsumableResource2.setQuantity(new BigDecimal(20));
		regConsumableResource2.setStatus(ResourceStatusDTO.AVAILABLE);
		regConsumableResource2.setTitle("RegisteredConsumable2");
		ConsumableResourceTypeDTO consumableResourceType2 = (ConsumableResourceTypeDTO) typeService.findDTOByTitle("Food");
		ConsumableResourceCategoryDTO consumableCategoryDTO2 = resourceCategoryService.findConsumableCategoryDTOByType(consumableResourceType2);
		regConsumableResource2.setConsumableResourceCategoryId(consumableCategoryDTO2.getId());

		logisticsService.createRegisteredConsumableResourceRemote(regConsumableResource1, userID);
		logisticsService.createRegisteredConsumableResourceRemote(regConsumableResource2, userID);

		System.out.println("\n\nRegistered Consumables created successfully\n");

		/****************************************************************************/

		regReusableResource1.setQuantity(new BigDecimal(20));
		ReusableResourceTypeDTO reusType1 = (ReusableResourceTypeDTO) this.typeService.findDTOByTitle("Medical Kit");
		ReusableResourceCategoryDTO reusableCategory1 = this.resourceCategoryService.findReusableCategoryDTOByType(reusType1);
		regReusableResource1.setReusableResourceCategoryId(reusableCategory1.getId());
		regReusableResource1.setStatus(ResourceStatusDTO.AVAILABLE);
		regReusableResource1.setTitle("RegisteredReusable1");

		regReusableResource2.setQuantity(new BigDecimal(20));
		ReusableResourceTypeDTO reusType2 = (ReusableResourceTypeDTO) this.typeService.findDTOByTitle("Food package");
		ReusableResourceCategoryDTO reusableCategory2 = this.resourceCategoryService.findReusableCategoryDTOByType(reusType2);
		regReusableResource2.setReusableResourceCategoryId(reusableCategory2.getId());
		regReusableResource2.setStatus(ResourceStatusDTO.AVAILABLE);
		regReusableResource2.setTitle("RegisteredReusable2");

		logisticsService.createRegisteredReusableResourceRemote(regReusableResource1, userID);
		logisticsService.createRegisteredReusableResourceRemote(regReusableResource2, userID);

		System.out.println("Registered Reusables created successfully");


		/****************************************************************************/

		OperationsCentreCategoryDTO ocCategory = (OperationsCentreCategoryDTO) this.resourceCategoryService
				.findByIdRemote(OperationsCentreCategoryDTO.class, new Long(1));


		regOperationsCentre1.setOperationsCentreCategoryId(ocCategory.getId());
		regOperationsCentre1.setStatus(ResourceStatusDTO.AVAILABLE);
		regOperationsCentre1.setTitle("RegisteredOperationsCentre1");
		VoIPURLDTO voipURL = new VoIPURLDTO();
		voipURL.setHost("192.168.1.176");
		voipURL.setPath("/path");
		voipURL.setProtocol("SIP");
		regOperationsCentre1.setVoIPURL(voipURL);

		operationsCentreService.createRegisteredOperationsCentreRemote(regOperationsCentre1, userID);

		System.out.println("Registered Operations Centre created successfully");
	}


	@Test(groups = "createSnapshots")
	public void CreateCrisisContextSnapshots()  throws ClassNotFoundException {

		CrisisContextSnapshotDTO crisisContextSnapshotDTO=  new CrisisContextSnapshotDTO();
		CrisisContextSnapshotStatusDTO SnapshotStatus=   CrisisContextSnapshotStatusDTO.STARTED;
		PeriodDTO period = createPeriodDTO(10);


		crisisContextSnapshotDTO.setCrisisContext(this.crisisService.findCrisisContextDTOByTitle("Fire Brigade Drill"));
PointDTO point1 = new PointDTO(new BigDecimal(38.025334), new BigDecimal(23.802717), null);
		

		SphereDTO sphere1 = new SphereDTO();
		sphere1.setCentre(point1);
		sphere1.setRadius(new BigDecimal(0));

		sphere1.setTitle("SnapshotLoc3");
		sphere1=(SphereDTO)genericService.createEntityRemote(sphere1, new Long(1));
		
		crisisContextSnapshotDTO.setLocationArea(sphere1);
		crisisContextSnapshotDTO.setPeriod(period);
		crisisContextSnapshotDTO.setStatus(SnapshotStatus);
		crisisContextSnapshotDTO.setNext(null);
		crisisContextSnapshotDTO.setPrevious(null);

		crisisService.createCrisisContextSnapshotRemote(crisisContextSnapshotDTO, userID);


	}

	/************************************************************************************/

	@Test(groups = "createResources")
	public void CreateAction() throws ClassNotFoundException {

		CrisisContextDTO crisisContextDTO = crisisService.findCrisisContextDTOByTitle("Fire Brigade Drill");
		ActionTypeDTO actionTypeDTO = (ActionTypeDTO) typeService.findDTOByTitle("OpActionType");

		if (crisisContextDTO != null && actionTypeDTO != null) {

			ActionDTO actionDTO1 = new ActionDTO();
			actionDTO1.setActionOperation(ActionOperationEnumDTO.FIX);
			actionDTO1.setType(actionTypeDTO.getTitle());
			actionDTO1.setCrisisContext(crisisContextDTO);
			actionDTO1.setSeverityLevel(SeverityLevelDTO.MEDIUM);
			actionDTO1.setTitle("Extinguish Fire");

			ActionDTO actionDTO2 = new ActionDTO();
			actionDTO2.setActionOperation(ActionOperationEnumDTO.FIX);
			actionDTO2.setType(actionTypeDTO.getTitle());
			actionDTO2.setCrisisContext(crisisContextDTO);
			actionDTO2.setSeverityLevel(SeverityLevelDTO.MEDIUM);
			actionDTO2.setTitle("Transfer Supplies");

			actionService.createActionRemote(actionDTO1, this.userID);
			actionService.createActionRemote(actionDTO2, this.userID);

		}

	}

	/***********************************************************************************/

	@Test(groups = "createBasics")
	public void CreateActionObjectivesLocationAreas() throws ClassNotFoundException {

		PointDTO point1 = new PointDTO(new BigDecimal(10), new BigDecimal(11), new BigDecimal(12));
		PointDTO point2 = new PointDTO(new BigDecimal(13), new BigDecimal(14), new BigDecimal(15));

		SphereDTO sphere1 = new SphereDTO();
		sphere1.setCentre(point1);
		sphere1.setRadius(new BigDecimal(5));

		sphere1.setTitle("LocationArea1");
		genericService.createEntityRemote(sphere1, userID);

		SphereDTO sphere2 = new SphereDTO();
		sphere2.setCentre(point2);
		sphere2.setRadius(new BigDecimal(5));

		sphere2.setTitle("LocationArea2");
		genericService.createEntityRemote(sphere2, userID);
	}

	/************************************************************************************/

	@SuppressWarnings("unchecked")
	@Test(groups = "createResources")
	public void CreateActionObjectives() throws InstantiationException, IllegalAccessException, Exception {

		LocationAreaDTO areaDTO1 = null;

		List<LocationAreaDTO> results = (List<LocationAreaDTO>) genericService.getDTOEntities(SphereDTO.class.getName(), 
				new EsponderCriterionDTO("title", EsponderCriterionExpressionEnumDTO.EQUAL, "LocationArea1"), 10, 0);

		if(!results.isEmpty() && results.size()<2) {
			areaDTO1 = results.get(0);
		}

		ActionDTO actionDTO = actionService.findActionDTOByTitle("Extinguish Fire");

		if( areaDTO1 != null && actionDTO != null ) {

			PeriodDTO period1 = createPeriodDTO(10);
			PeriodDTO period2 = createPeriodDTO(10);

			ActionObjectiveDTO objective1 = new ActionObjectiveDTO();
			objective1.setAction(actionDTO);
			objective1.setLocationArea(areaDTO1);
			objective1.setPeriod(period1);
			objective1.setTitle("Extinguish Fire Objective");

			ActionObjectiveDTO objective2 = new ActionObjectiveDTO();
			objective2.setAction(actionDTO);
			objective2.setLocationArea(areaDTO1);
			objective2.setPeriod(period2);
			objective2.setTitle("Create firewalls");

			actionService.createActionObjectiveRemote(objective1, userID);
			actionService.createActionObjectiveRemote(objective2, userID);
		}

	}

	/************************************************************************************/

	@Test(groups = "createResources")
	public void CreateActionParts() throws ClassNotFoundException {

		ActionDTO parentActionDTO = actionService.findActionDTOByTitle("Extinguish Fire");
		if (parentActionDTO != null) {

			ActionPartDTO actionPart1 = new ActionPartDTO();
			actionPart1.setActionId(parentActionDTO.getId());
			actionPart1.setActionOperation(ActionOperationEnumDTO.FIX);
			ActorDTO actorDTO = actorService.findByTitleRemote("FR #1.1");
			actionPart1.setActor(actorDTO);
			actionPart1.setSeverityLevel(SeverityLevelDTO.MEDIUM);
			actionPart1.setTitle("Handle water pipe");

			ActionPartDTO actionPart2 = new ActionPartDTO();
			actionPart2.setActionId(parentActionDTO.getId());
			actionPart2.setActionOperation(ActionOperationEnumDTO.FIX);
			actorDTO = actorService.findByTitleRemote("FR #1.2");
			actionPart2.setActor(actorDTO);
			actionPart2.setSeverityLevel(SeverityLevelDTO.MEDIUM);
			actionPart2.setTitle("Use shovel and axe");

			ActionPartDTO actionPart3 = new ActionPartDTO();
			actionPart3.setActionId(parentActionDTO.getId());
			actionPart3.setActionOperation(ActionOperationEnumDTO.TRANSPORT);
			actorDTO = actorService.findByTitleRemote("FRC #1");
			actionPart3.setActor(actorDTO);
			actionPart3.setSeverityLevel(SeverityLevelDTO.MINIMAL);
			actionPart3.setTitle("Oversee Rescue Team members");

			actionService.createActionPartRemote(actionPart1, userID);
			actionService.createActionPartRemote(actionPart2, userID);
			actionService.createActionPartRemote(actionPart3, userID);

		}

	}

	/************************************************************************************/

	@SuppressWarnings("unchecked")
	@Test(groups = "createResources")
	public void CreateActionPartsObjectives() throws InstantiationException, IllegalAccessException, Exception {

		LocationAreaDTO areaDTO = null;

		List<LocationAreaDTO> results = (List<LocationAreaDTO>) genericService.getDTOEntities(SphereDTO.class.getName(), 
				new EsponderCriterionDTO("title", EsponderCriterionExpressionEnumDTO.EQUAL, "LocationArea1"), 10, 0);

		if(!results.isEmpty() && results.size()<2) {
			areaDTO = results.get(0);
		}

		PeriodDTO period1 = createPeriodDTO(10);
		PeriodDTO period2 = createPeriodDTO(10);
		PeriodDTO period3 = createPeriodDTO(10);

		if(areaDTO != null) {

			ActionPartObjectiveDTO objective1 = new ActionPartObjectiveDTO();
			objective1.setActionPart(actionService.findActionPartDTOByTitle("Handle water pipe"));
			objective1.setTitle("Extinguish fire at Ymmitos");
			objective1.setLocationArea(areaDTO);
			objective1.setPeriod(period1);

			ActionPartObjectiveDTO objective2 = new ActionPartObjectiveDTO();
			objective2.setActionPart(actionService.findActionPartDTOByTitle("Use shovel and axe"));
			objective2.setTitle("Create firewall at Ymmitos");
			objective2.setLocationArea(areaDTO);
			objective2.setPeriod(period2);

			ActionPartObjectiveDTO objective3 = new ActionPartObjectiveDTO();
			objective3.setActionPart(actionService.findActionPartDTOByTitle("Oversee Rescue Team members"));
			objective3.setTitle("Oversee Ymmitos Operation");
			objective3.setLocationArea(areaDTO);
			objective3.setPeriod(period3);	

			actionService.createActionPartObjectiveRemote(objective1, userID);
			actionService.createActionPartObjectiveRemote(objective2, userID);
			actionService.createActionPartObjectiveRemote(objective3, userID);
		}
	}

	/***********************************************************************************/

	@Test(groups = "createResources")
	public void CreateConsumableResources() throws ClassNotFoundException {
		//FIXME
		ConsumableResourceTypeDTO food = (ConsumableResourceTypeDTO) typeService.findDTOByTitle("Food");
		ConsumableResourceTypeDTO drink = (ConsumableResourceTypeDTO) typeService.findDTOByTitle("Drink");
		ConsumableResourceTypeDTO medical = (ConsumableResourceTypeDTO) typeService.findDTOByTitle("Medical Resource");

		ActionPartDTO actionPart1 = actionService.findActionPartDTOByTitle("Handle water pipe");
		ActionPartDTO actionPart2 = actionService.findActionPartDTOByTitle("Use shovel and axe");
		ActionPartDTO actionPart3 = actionService.findActionPartDTOByTitle("Oversee Rescue Team members");

		ConsumableResourceCategoryDTO consumableCategoryDTO1 = resourceCategoryService.findConsumableCategoryDTOByType(food);
		ConsumableResourceCategoryDTO consumableCategoryDTO2 = resourceCategoryService.findConsumableCategoryDTOByType(drink);
		ConsumableResourceCategoryDTO consumableCategoryDTO3 = resourceCategoryService.findConsumableCategoryDTOByType(medical);


		// Actor1
		ConsumableResourceDTO consumableResource1 = new ConsumableResourceDTO();
		consumableResource1.setConsumableResourceCategory(consumableCategoryDTO2);
		consumableResource1.setQuantity(new BigDecimal(10));
		consumableResource1.setStatus(ResourceStatusDTO.ALLOCATED);
		consumableResource1.setTitle("Water_Bottle_1");
		consumableResource1.setActionPart(actionPart1);

		ConsumableResourceDTO consumableResource2 = new ConsumableResourceDTO();
		consumableResource2.setConsumableResourceCategory(consumableCategoryDTO1);
		consumableResource2.setQuantity(new BigDecimal(10));
		consumableResource2.setStatus(ResourceStatusDTO.ALLOCATED);
		consumableResource2.setTitle("Sandwich_1");
		consumableResource2.setActionPart(actionPart1);

		ConsumableResourceDTO consumableResource3 = new ConsumableResourceDTO();
		consumableResource3.setConsumableResourceCategory(consumableCategoryDTO3);
		consumableResource3.setQuantity(new BigDecimal(10));
		consumableResource3.setStatus(ResourceStatusDTO.ALLOCATED);
		consumableResource3.setTitle("Bandages_1");
		consumableResource3.setActionPart(actionPart1);

		// Actor2
		ConsumableResourceDTO consumableResource4 = new ConsumableResourceDTO();
		consumableResource4.setConsumableResourceCategory(consumableCategoryDTO2);
		consumableResource4.setQuantity(new BigDecimal(10));
		consumableResource4.setStatus(ResourceStatusDTO.ALLOCATED);
		consumableResource4.setTitle("Water_Bottle_2");
		consumableResource4.setActionPart(actionPart2);

		ConsumableResourceDTO consumableResource5 = new ConsumableResourceDTO();
		consumableResource5.setConsumableResourceCategory(consumableCategoryDTO2);
		consumableResource5.setQuantity(new BigDecimal(10));
		consumableResource5.setStatus(ResourceStatusDTO.ALLOCATED);
		consumableResource5.setTitle("Sandwich_2");
		consumableResource5.setActionPart(actionPart2);

		ConsumableResourceDTO consumableResource6 = new ConsumableResourceDTO();
		consumableResource6.setConsumableResourceCategory(consumableCategoryDTO3);
		consumableResource6.setQuantity(new BigDecimal(10));
		consumableResource6.setStatus(ResourceStatusDTO.ALLOCATED);
		consumableResource6.setTitle("Bandages_2");
		consumableResource6.setActionPart(actionPart2);

		// Actor3
		ConsumableResourceDTO consumableResource7 = new ConsumableResourceDTO();
		consumableResource7.setConsumableResourceCategory(consumableCategoryDTO2);
		consumableResource7.setQuantity(new BigDecimal(10));
		consumableResource7.setStatus(ResourceStatusDTO.ALLOCATED);
		consumableResource7.setTitle("Water_Bottle_3");
		consumableResource7.setActionPart(actionPart3);

		ConsumableResourceDTO consumableResource8 = new ConsumableResourceDTO();
		consumableResource8.setConsumableResourceCategory(consumableCategoryDTO1);
		consumableResource8.setQuantity(new BigDecimal(10));
		consumableResource8.setStatus(ResourceStatusDTO.ALLOCATED);
		consumableResource8.setTitle("Sandwich_3");
		consumableResource8.setActionPart(actionPart3);

		ConsumableResourceDTO consumableResource9 = new ConsumableResourceDTO();
		consumableResource9.setConsumableResourceCategory(consumableCategoryDTO3);
		consumableResource9.setQuantity(new BigDecimal(10));
		consumableResource9.setStatus(ResourceStatusDTO.ALLOCATED);
		consumableResource9.setTitle("Bandages_3");
		consumableResource9.setActionPart(actionPart3);


		logisticsService.createConsumableResourceRemote(consumableResource1, userID);
		logisticsService.createConsumableResourceRemote(consumableResource2, userID);
		logisticsService.createConsumableResourceRemote(consumableResource3, userID);
		logisticsService.createConsumableResourceRemote(consumableResource4, userID);
		logisticsService.createConsumableResourceRemote(consumableResource5, userID);
		logisticsService.createConsumableResourceRemote(consumableResource6, userID);
		logisticsService.createConsumableResourceRemote(consumableResource7, userID);
		logisticsService.createConsumableResourceRemote(consumableResource8, userID);
		logisticsService.createConsumableResourceRemote(consumableResource9, userID);

	}

	/***********************************************************************************/

	@Test(groups = "createResources")
	public void CreateReusableResources() throws ClassNotFoundException {
		//FIXME
		ReusableResourceTypeDTO food = (ReusableResourceTypeDTO) typeService.findDTOByTitle("Water Container");
		ReusableResourceTypeDTO drink = (ReusableResourceTypeDTO) typeService.findDTOByTitle("Food package");
		ReusableResourceTypeDTO medical = (ReusableResourceTypeDTO) typeService.findDTOByTitle("Medical Kit");

		ActionPartDTO actionPart1 = actionService.findActionPartDTOByTitle("Handle water pipe");
		ActionPartDTO actionPart2 = actionService.findActionPartDTOByTitle("Use shovel and axe");
		ActionPartDTO actionPart3 = actionService.findActionPartDTOByTitle("Oversee Rescue Team members");

		ReusableResourceCategoryDTO reusableCategoryDTO1 = resourceCategoryService.findReusableCategoryDTOByType(food);
		ReusableResourceCategoryDTO reusableCategoryDTO2 = resourceCategoryService.findReusableCategoryDTOByType(drink);
		ReusableResourceCategoryDTO reusableCategoryDTO3 = resourceCategoryService.findReusableCategoryDTOByType(medical);


		// Actor1

		ReusableResourceDTO reusableResource1 = new ReusableResourceDTO();
		reusableResource1.setReusableResourceCategory(reusableCategoryDTO2);
		reusableResource1.setQuantity(new BigDecimal(10));
		reusableResource1.setStatus(ResourceStatusDTO.ALLOCATED);
		reusableResource1.setTitle("Water Bottle_1");
		reusableResource1.setActionPart(actionPart1);

		ReusableResourceDTO reusableResource2 = new ReusableResourceDTO();
		reusableResource2.setReusableResourceCategory(reusableCategoryDTO1);
		reusableResource2.setQuantity(new BigDecimal(10));
		reusableResource2.setStatus(ResourceStatusDTO.ALLOCATED);
		reusableResource2.setTitle("Basket_1");
		reusableResource2.setActionPart(actionPart1);

		ReusableResourceDTO reusableResource3 = new ReusableResourceDTO();
		reusableResource3.setReusableResourceCategory(reusableCategoryDTO3);
		reusableResource3.setQuantity(new BigDecimal(10));
		reusableResource3.setStatus(ResourceStatusDTO.ALLOCATED);
		reusableResource3.setTitle("Medical Kit_1");
		reusableResource3.setActionPart(actionPart1);

		// Actor2

		ReusableResourceDTO reusableResource4 = new ReusableResourceDTO();
		reusableResource4.setReusableResourceCategory(reusableCategoryDTO2);
		reusableResource4.setQuantity(new BigDecimal(10));
		reusableResource4.setStatus(ResourceStatusDTO.ALLOCATED);
		reusableResource4.setTitle("Water Bottle_2");
		reusableResource4.setActionPart(actionPart2);

		ReusableResourceDTO reusableResource5 = new ReusableResourceDTO();
		reusableResource5.setReusableResourceCategory(reusableCategoryDTO1);
		reusableResource5.setQuantity(new BigDecimal(10));
		reusableResource5.setStatus(ResourceStatusDTO.ALLOCATED);
		reusableResource5.setTitle("Basket_2");
		reusableResource5.setActionPart(actionPart2);

		ReusableResourceDTO reusableResource6 = new ReusableResourceDTO();
		reusableResource6.setReusableResourceCategory(reusableCategoryDTO3);
		reusableResource6.setQuantity(new BigDecimal(10));
		reusableResource6.setStatus(ResourceStatusDTO.ALLOCATED);
		reusableResource6.setTitle("Medical Kit_2");
		reusableResource6.setActionPart(actionPart2);

		// Actor3

		ReusableResourceDTO reusableResource7 = new ReusableResourceDTO();
		reusableResource7.setReusableResourceCategory(reusableCategoryDTO2);
		reusableResource7.setQuantity(new BigDecimal(10));
		reusableResource7.setStatus(ResourceStatusDTO.ALLOCATED);
		reusableResource7.setTitle("Water Bottle_3");
		reusableResource7.setActionPart(actionPart3);

		ReusableResourceDTO reusableResource8 = new ReusableResourceDTO();
		reusableResource8.setReusableResourceCategory(reusableCategoryDTO1);
		reusableResource8.setQuantity(new BigDecimal(10));
		reusableResource8.setStatus(ResourceStatusDTO.ALLOCATED);
		reusableResource8.setTitle("Basket_3");
		reusableResource8.setActionPart(actionPart3);

		ReusableResourceDTO reusableResource9 = new ReusableResourceDTO();
		reusableResource9.setReusableResourceCategory(reusableCategoryDTO3);
		reusableResource9.setQuantity(new BigDecimal(10));
		reusableResource9.setStatus(ResourceStatusDTO.ALLOCATED);
		reusableResource9.setTitle("Medical Kit_3");
		reusableResource9.setActionPart(actionPart3);


		logisticsService.createReusableResourceRemote(reusableResource1, userID);
		logisticsService.createReusableResourceRemote(reusableResource2, userID);
		logisticsService.createReusableResourceRemote(reusableResource3, userID);
		logisticsService.createReusableResourceRemote(reusableResource4, userID);
		logisticsService.createReusableResourceRemote(reusableResource5, userID);
		logisticsService.createReusableResourceRemote(reusableResource6, userID);
		logisticsService.createReusableResourceRemote(reusableResource7, userID);
		logisticsService.createReusableResourceRemote(reusableResource8, userID);
		logisticsService.createReusableResourceRemote(reusableResource9, userID);

	}

	/***********************************************************************************/


	@Test(groups = "createSnapshots")
	public void CreateActionSnapshots() throws ClassNotFoundException {

		ActionSnapshotDTO action = new ActionSnapshotDTO();
		PeriodDTO period = this.createPeriodDTO(SECONDS);
		ActionSnapshotStatusDTO status=ActionSnapshotStatusDTO.STARTED;

PointDTO point1 = new PointDTO(new BigDecimal(38.025334), new BigDecimal(23.802717), null);
		

		SphereDTO sphere1 = new SphereDTO();
		sphere1.setCentre(point1);
		sphere1.setRadius(new BigDecimal(0));

		sphere1.setTitle("Sphere 001");
		sphere1=(SphereDTO)genericService.createEntityRemote(sphere1, new Long(1));
		
		action.setAction(this.actionService.findActionDTOByTitle("Extinguish Fire"));
		action.setLocationArea(sphere1);
		action.setPeriod(period);
		action.setStatus(status);

		//		genericService.createEntityRemote(action, userID);
		actionService.createActionSnapshotRemote(action, userID);

	}

	/***********************************************************************************/

	@Test(groups = "createSnapshots")
	public void CreateActionPartSnapshots() throws ClassNotFoundException {
		ActionPartSnapshotDTO actionPartSnapshot = new ActionPartSnapshotDTO();
		PeriodDTO period = this.createPeriodDTO(SECONDS);
		ActionPartSnapshotStatusDTO status=ActionPartSnapshotStatusDTO.STARTED;
		PointDTO point1 = new PointDTO(new BigDecimal(38.025334), new BigDecimal(23.802717), null);
		SphereDTO sphere1 = new SphereDTO();
		sphere1.setCentre(point1);
		sphere1.setRadius(new BigDecimal(0));

		sphere1.setTitle("Sphere 005");
		sphere1=(SphereDTO)genericService.createEntityRemote(sphere1, new Long(1));
		actionPartSnapshot.setLocationArea(sphere1);
		actionPartSnapshot.setPeriod(period);
		actionPartSnapshot.setStatus(status);
		actionPartSnapshot.setActionPartId(actionService.findActionPartDTOByTitle("Handle water pipe").getId());

		//		genericService.createEntityRemote(actionPartSnapshot, userID);
		actionService.createActionPartSnapshotRemote(actionPartSnapshot, userID);


	}

	/***********************************************************************************/

	public XMLGregorianCalendar getXMLGregorianCalendarNow() throws DatatypeConfigurationException {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
		XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
		return now;
	}


	@SuppressWarnings("unused")
	private Period getPeriod() {
		Period period = new Period();
		period.setDateFrom(new Date().getTime());
		period.setDateTo(getDate().getTime());
		return period;
	}

	private Date getDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.MINUTE, 1);
		return c.getTime();
	}


}

