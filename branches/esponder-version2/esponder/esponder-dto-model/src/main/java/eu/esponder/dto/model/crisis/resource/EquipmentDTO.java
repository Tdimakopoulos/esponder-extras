/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.plan.PlannableResourceDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class EquipmentDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "sensors", "snapshot"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EquipmentDTO extends PlannableResourceDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3973978134313792671L;

	/**
	 * Instantiates a new equipment dto.
	 */
	public EquipmentDTO() { }
	
	/** The sensors. */
	private Set<SensorDTO> sensors;
	
	/** The snapshot. */
	private  EquipmentSnapshotDTO snapshot;
	
	/** The equipment category id. */
	private Long equipmentCategoryId;
	
	/** The actor. */
	private ActorDTO actor;

	/**
	 * Gets the actor.
	 *
	 * @return the actor
	 */
	public ActorDTO getActor() {
		return actor;
	}

	/**
	 * Sets the actor.
	 *
	 * @param actor the new actor
	 */
	public void setActor(ActorDTO actor) {
		this.actor = actor;
	}

	/**
	 * Gets the sensors.
	 *
	 * @return the sensors
	 */
	public Set<SensorDTO> getSensors() {
		return sensors;
	}

	/**
	 * Sets the sensors.
	 *
	 * @param sensors the new sensors
	 */
	public void setSensors(Set<SensorDTO> sensors) {
		this.sensors = sensors;
	}

	/**
	 * Gets the snapshot.
	 *
	 * @return the snapshot
	 */
	public EquipmentSnapshotDTO getSnapshot() {
		return snapshot;
	}

	/**
	 * Sets the snapshot.
	 *
	 * @param snapshot the new snapshot
	 */
	public void setSnapshot(EquipmentSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.ResourceDTO#toString()
	 */
	@Override
	public String toString() {
		return "EquipmentDTO [type=" + type + ", status=" + status + ", id="
				+ id + ", title=" + title + "]";
	}

	/**
	 * Gets the equipment category id.
	 *
	 * @return the equipment category id
	 */
	public Long getEquipmentCategoryId() {
		return equipmentCategoryId;
	}

	/**
	 * Sets the equipment category id.
	 *
	 * @param equipmentCategoryId the new equipment category id
	 */
	public void setEquipmentCategoryId(Long equipmentCategoryId) {
		this.equipmentCategoryId = equipmentCategoryId;
	}
	
}
