/*
 * 
 */
package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationalActionTypeDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "parent", "children"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OperationalActionTypeDTO extends ActionTypeDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8445886879200324066L;
	
	/** The parent. */
	private TacticalActionTypeDTO parent;
	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.type.ESponderTypeDTO#getParent()
	 */
	public TacticalActionTypeDTO getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(TacticalActionTypeDTO parent) {
		this.parent = parent;
	}
	
	

}
