/*
 * 
 */
package eu.esponder.dto.model.snapshot.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.OperationsCentreSnapshotStatusDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreSnapshotDTO.
 */
@JsonPropertyOrder({"id", "status", "locationArea", "period"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OperationsCentreSnapshotDTO extends SpatialSnapshotDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6058516655453683876L;

	/**
	 * Instantiates a new operations centre snapshot dto.
	 */
	public OperationsCentreSnapshotDTO() { }
	
	/** The status. */
	private OperationsCentreSnapshotStatusDTO status;

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public OperationsCentreSnapshotStatusDTO getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(OperationsCentreSnapshotStatusDTO status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.snapshot.SpatialSnapshotDTO#toString()
	 */
	@Override
	public String toString() {
		return "OperationsCentreSnapshotDTO [status=" + status + ", locationArea=" + locationArea + ", id=" + id + "]";
	}
	
}
