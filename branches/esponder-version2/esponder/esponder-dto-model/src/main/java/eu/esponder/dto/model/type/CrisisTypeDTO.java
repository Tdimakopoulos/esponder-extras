/*
 * 
 */
package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonTypeInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisTypeDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class CrisisTypeDTO extends ESponderTypeDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1697829503734490391L;

}
