/*
 * 
 */
package eu.esponder.dto.model.snapshot;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class SpatialSnapshotDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class SpatialSnapshotDTO extends SnapshotDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8816608444526837797L;
	
	/** The location area. */
	protected LocationAreaDTO locationArea;

	/**
	 * Gets the location area.
	 *
	 * @return the location area
	 */
	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	/**
	 * Sets the location area.
	 *
	 * @param locationArea the new location area
	 */
	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SpatialSnapshotDTO [locationArea=" + locationArea + ", id=" + id + "]";
	}

}
