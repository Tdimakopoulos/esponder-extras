/*
 * 
 */
package eu.esponder.dto.model.crisis.view;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

// TODO: Auto-generated Javadoc
/**
 * The Class VoIPURLDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"protocol", "hostName", "path"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class VoIPURLDTO extends URLDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3608367404473258930L;

	/**
	 * Instantiates a new vo ipurldto.
	 */
	public VoIPURLDTO() {
		this.setProtocol("sip");
	}
}
