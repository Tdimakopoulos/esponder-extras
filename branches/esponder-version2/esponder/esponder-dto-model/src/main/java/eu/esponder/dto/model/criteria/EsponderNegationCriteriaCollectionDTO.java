/*
 * 
 */
package eu.esponder.dto.model.criteria;

import java.util.Collection;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class EsponderNegationCriteriaCollectionDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"restrictions"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EsponderNegationCriteriaCollectionDTO extends EsponderCriteriaCollectionDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -786849464712579302L;

	/**
	 * Instantiates a new esponder negation criteria collection dto.
	 *
	 * @param restrictions the restrictions
	 */
	public EsponderNegationCriteriaCollectionDTO(
			Collection<EsponderQueryRestrictionDTO> restrictions) {
		super(restrictions);
	}
	
	/**
	 * Instantiates a new esponder negation criteria collection dto.
	 */
	public EsponderNegationCriteriaCollectionDTO() { }

}
