/*
 * 
 */
package eu.esponder.dto.model.snapshot.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.dto.model.snapshot.status.EquipmentSnapshotStatusDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class EquipmentSnapshotDTO.
 */
@JsonPropertyOrder({"status", "period"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EquipmentSnapshotDTO extends SnapshotDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7338449002977646358L;

	/**
	 * Instantiates a new equipment snapshot dto.
	 */
	public EquipmentSnapshotDTO() { }
	
	/** The status. */
	private EquipmentSnapshotStatusDTO status;
	
	/** The equipment. */
	private EquipmentDTO equipment;

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public EquipmentSnapshotStatusDTO getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(EquipmentSnapshotStatusDTO status) {
		this.status = status;
	}

	/**
	 * Gets the equipment.
	 *
	 * @return the equipment
	 */
	public EquipmentDTO getEquipment() {
		return equipment;
	}

	/**
	 * Sets the equipment.
	 *
	 * @param equipment the new equipment
	 */
	public void setEquipment(EquipmentDTO equipment) {
		this.equipment = equipment;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EquipmentSnapshotDTO [status=" + status + ", period=" + period + ", id=" + id + "]";
	}

}
