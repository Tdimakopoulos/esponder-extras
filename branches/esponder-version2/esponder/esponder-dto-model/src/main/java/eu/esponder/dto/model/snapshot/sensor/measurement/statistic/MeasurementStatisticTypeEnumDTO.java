/*
 * 
 */
package eu.esponder.dto.model.snapshot.sensor.measurement.statistic;


// TODO: Auto-generated Javadoc
/**
 * The Enum MeasurementStatisticTypeEnumDTO.
 */
public enum MeasurementStatisticTypeEnumDTO {

	/** The mean. */
	MEAN,
	
	/** The stdev. */
	STDEV,

	/** The autocorrelation. */
	AUTOCORRELATION,

	/** The maximum. */
	MAXIMUM,
	
	/** The minimum. */
	MINIMUM,

	/** The first. */
	FIRST,
	
	/** The last. */
	LAST,
	
	/** The median. */
	MEDIAN,
	
	/** The oldest. */
	OLDEST,
	
	/** The newest. */
	NEWEST,
	
	/** The rate. */
	RATE
	
}
