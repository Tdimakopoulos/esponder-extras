/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.category;

import org.codehaus.jackson.annotate.JsonTypeInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class PlannableResourceCategoryDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class PlannableResourceCategoryDTO extends ResourceCategoryDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4386538513769849871L;


}
