/*
 * 
 */
package eu.esponder.dto.model.crisis.action;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ActionScheduleDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "action", "prerequisiteAction", "criteria"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionScheduleDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -703599359640439512L;

	/** The title. */
	private String title;
	
	/** The action. */
	private ActionDTO action;
	
	/** The prerequisite action. */
	private ActionDTO prerequisiteAction;
	
	/** The criteria. */
	private ActionScheduleCriteriaDTO criteria;

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public ActionDTO getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(ActionDTO action) {
		this.action = action;
	}

	/**
	 * Gets the prerequisite action.
	 *
	 * @return the prerequisite action
	 */
	public ActionDTO getPrerequisiteAction() {
		return prerequisiteAction;
	}

	/**
	 * Sets the prerequisite action.
	 *
	 * @param prerequisiteAction the new prerequisite action
	 */
	public void setPrerequisiteAction(ActionDTO prerequisiteAction) {
		this.prerequisiteAction = prerequisiteAction;
	}

	/**
	 * Gets the criteria.
	 *
	 * @return the criteria
	 */
	public ActionScheduleCriteriaDTO getCriteria() {
		return criteria;
	}

	/**
	 * Sets the criteria.
	 *
	 * @param criteria the new criteria
	 */
	public void setCriteria(ActionScheduleCriteriaDTO criteria) {
		this.criteria = criteria;
	}

}


