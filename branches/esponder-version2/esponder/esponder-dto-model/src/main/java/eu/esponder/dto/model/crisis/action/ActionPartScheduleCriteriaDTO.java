/*
 * 
 */
package eu.esponder.dto.model.crisis.action;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ActionPartScheduleCriteriaDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "status", "dateAfter"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionPartScheduleCriteriaDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -832456284608428226L;

	/** The status. */
	private ActionPartSnapshotStatusDTO status;

	/** The date after. */
	private Long dateAfter;

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ActionPartSnapshotStatusDTO getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ActionPartSnapshotStatusDTO status) {
		this.status = status;
	}

	/**
	 * Gets the date after.
	 *
	 * @return the date after
	 */
	public Long getDateAfter() {
		return dateAfter;
	}

	/**
	 * Sets the date after.
	 *
	 * @param dateAfter the new date after
	 */
	public void setDateAfter(Long dateAfter) {
		this.dateAfter = dateAfter;
	}

}
