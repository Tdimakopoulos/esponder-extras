/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.plan;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class PlannableResourcePowerDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "crisisResourcePlan", "planableResource", "power", "constraint"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PlannableResourcePowerDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7083280767020268843L;

	/** The crisis resource plan id. */
	private Long crisisResourcePlanId;

	/** The planable resource category id. */
	private Long planableResourceCategoryId;

	/** The power. */
	private Integer power;

	/** FIXME: Refactor this to an Enum indicating <  -> Less than <= -> Less than or Equal == -> Equal => -> Greater than or Equal >  -> Greater than. */
	private String constraint;

	/**
	 * Gets the power.
	 *
	 * @return the power
	 */
	public Integer getPower() {
		return power;
	}

	/**
	 * Sets the power.
	 *
	 * @param power the new power
	 */
	public void setPower(Integer power) {
		this.power = power;
	}

	/**
	 * Gets the constraint.
	 *
	 * @return the constraint
	 */
	public String getConstraint() {
		return constraint;
	}

	/**
	 * Sets the constraint.
	 *
	 * @param constraint the new constraint
	 */
	public void setConstraint(String constraint) {
		this.constraint = constraint;
	}

	/**
	 * Gets the crisis resource plan id.
	 *
	 * @return the crisis resource plan id
	 */
	public Long getCrisisResourcePlanId() {
		return crisisResourcePlanId;
	}

	/**
	 * Sets the crisis resource plan id.
	 *
	 * @param crisisResourcePlanId the new crisis resource plan id
	 */
	public void setCrisisResourcePlanId(Long crisisResourcePlanId) {
		this.crisisResourcePlanId = crisisResourcePlanId;
	}

	/**
	 * Gets the planable resource category id.
	 *
	 * @return the planable resource category id
	 */
	public Long getPlanableResourceCategoryId() {
		return planableResourceCategoryId;
	}

	/**
	 * Sets the planable resource category id.
	 *
	 * @param planableResourceCategoryId the new planable resource category id
	 */
	public void setPlanableResourceCategoryId(Long planableResourceCategoryId) {
		this.planableResourceCategoryId = planableResourceCategoryId;
	}

}
