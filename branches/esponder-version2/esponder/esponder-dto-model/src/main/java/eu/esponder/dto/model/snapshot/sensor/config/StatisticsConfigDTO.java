/*
 * 
 */
package eu.esponder.dto.model.snapshot.sensor.config;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class StatisticsConfigDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "measurementStatisticType", "samplingPeriodMilliseconds"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class StatisticsConfigDTO extends ESponderEntityDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5641881403083027088L;

	/**
	 * Instantiates a new statistics config dto.
	 */
	public StatisticsConfigDTO() { }
	
	/** The measurement statistic type. */
	private MeasurementStatisticTypeEnumDTO measurementStatisticType;
	
	/** The sampling period milliseconds. */
	private long samplingPeriodMilliseconds;
	
	/** The sensor id. */
	private Long sensorId;
	
	/**
	 * Gets the measurement statistic type.
	 *
	 * @return the measurement statistic type
	 */
	public MeasurementStatisticTypeEnumDTO getMeasurementStatisticType() {
		return measurementStatisticType;
	}

	/**
	 * Sets the measurement statistic type.
	 *
	 * @param measurementStatisticType the new measurement statistic type
	 */
	public void setMeasurementStatisticType(
			MeasurementStatisticTypeEnumDTO measurementStatisticType) {
		this.measurementStatisticType = measurementStatisticType;
	}

	/**
	 * Gets the sampling period milliseconds.
	 *
	 * @return the sampling period milliseconds
	 */
	public long getSamplingPeriodMilliseconds() {
		return samplingPeriodMilliseconds;
	}

	/**
	 * Sets the sampling period milliseconds.
	 *
	 * @param samplingPeriodMilliseconds the new sampling period milliseconds
	 */
	public void setSamplingPeriodMilliseconds(long samplingPeriodMilliseconds) {
		this.samplingPeriodMilliseconds = samplingPeriodMilliseconds;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[Type : " + measurementStatisticType + ", sampling Period (ms): " + samplingPeriodMilliseconds + " ]";
	}

	/**
	 * Gets the sensor id.
	 *
	 * @return the sensor id
	 */
	public Long getSensorId() {
		return sensorId;
	}

	/**
	 * Sets the sensor id.
	 *
	 * @param sensorId the new sensor id
	 */
	public void setSensorId(Long sensorId) {
		this.sensorId = sensorId;
	}
	
}
