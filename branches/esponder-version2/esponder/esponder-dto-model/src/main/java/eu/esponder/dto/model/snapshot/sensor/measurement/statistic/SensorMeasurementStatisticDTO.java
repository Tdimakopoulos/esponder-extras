/*
 * 
 */
package eu.esponder.dto.model.snapshot.sensor.measurement.statistic;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class SensorMeasurementStatisticDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "samplingPeriod", "statistic", "period"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SensorMeasurementStatisticDTO extends ESponderEntityDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1389639486796682119L;

	/** The statistic type. */
	private MeasurementStatisticTypeEnumDTO statisticType;
	
	/** Sampling period in msec. */
	private long samplingPeriod;
	
	/** The statistic. */
	private SensorMeasurementDTO statistic;
	
	/** The period. */
	private PeriodDTO period;
	
	/**
	 * Gets the period.
	 *
	 * @return the period
	 */
	public PeriodDTO getPeriod() {
		return period;
	}

	/**
	 * Sets the period.
	 *
	 * @param period the new period
	 */
	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

	/**
	 * Gets the sampling period.
	 *
	 * @return the sampling period
	 */
	public long getSamplingPeriod() {
		return samplingPeriod;
	}

	/**
	 * Sets the sampling period.
	 *
	 * @param samplingPeriod the new sampling period
	 */
	public void setSamplingPeriod(long samplingPeriod) {
		this.samplingPeriod = samplingPeriod;
	}

	/**
	 * Gets the statistic.
	 *
	 * @return the statistic
	 */
	public SensorMeasurementDTO getStatistic() {
		return statistic;
	}

	/**
	 * Sets the statistic.
	 *
	 * @param statistic the new statistic
	 */
	public void setStatistic(SensorMeasurementDTO statistic) {
		this.statistic = statistic;
	}

	/**
	 * Gets the statistic type.
	 *
	 * @return the statistic type
	 */
	public MeasurementStatisticTypeEnumDTO getStatisticType() {
		return statisticType;
	}

	/**
	 * Sets the statistic type.
	 *
	 * @param statisticType the new statistic type
	 */
	public void setStatisticType(MeasurementStatisticTypeEnumDTO statisticType) {
		this.statisticType = statisticType;
	}
	
}
