/*
 * 
 */
package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonTypeInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class UserActorTypeDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class UserActorTypeDTO extends ESponderTypeDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4092646127121534860L;

}
