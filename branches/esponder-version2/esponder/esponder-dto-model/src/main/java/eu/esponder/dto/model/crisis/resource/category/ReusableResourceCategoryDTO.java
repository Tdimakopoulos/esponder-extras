/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.category;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.type.ReusableResourceTypeDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ReusableResourceCategoryDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "reusableResourceType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ReusableResourceCategoryDTO extends LogisticsCategoryDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7231628727141775176L;

	/** The reusable resource type. */
	private ReusableResourceTypeDTO reusableResourceType;

//	private Set<ReusableResourceDTO> reusableResources;

	/**
 * Gets the reusable resource type.
 *
 * @return the reusable resource type
 */
public ReusableResourceTypeDTO getReusableResourceType() {
		return reusableResourceType;
	}

	/**
	 * Sets the reusable resource type.
	 *
	 * @param reusableResourceType the new reusable resource type
	 */
	public void setReusableResourceType(ReusableResourceTypeDTO reusableResourceType) {
		this.reusableResourceType = reusableResourceType;
	}

//	public Set<ReusableResourceDTO> getReusableResources() {
//		return reusableResources;
//	}
//
//	public void setReusableResources(Set<ReusableResourceDTO> reusableResources) {
//		this.reusableResources = reusableResources;
//	}

}
