/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.type.PersonnelTrainingTypeDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelTrainingDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "type", "shortTitle", "personnelCategory", "personnelTrainingType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PersonnelTrainingDTO extends PersonnelCompetenceDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1572773899518146208L;
	
	/** The personnel training type. */
	private PersonnelTrainingTypeDTO personnelTrainingType;

	/**
	 * Gets the personnel training type.
	 *
	 * @return the personnel training type
	 */
	public PersonnelTrainingTypeDTO getPersonnelTrainingType() {
		return personnelTrainingType;
	}

	/**
	 * Sets the personnel training type.
	 *
	 * @param personnelTrainingType the new personnel training type
	 */
	public void setPersonnelTrainingType(
			PersonnelTrainingTypeDTO personnelTrainingType) {
		this.personnelTrainingType = personnelTrainingType;
	}

}
