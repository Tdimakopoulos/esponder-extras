/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.view.VoIPURLDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class RegisteredOperationsCentreDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "operationsCentreCategoryId", "title", "status", "voIPURL"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class RegisteredOperationsCentreDTO extends ResourceDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 443517368669208349L;
	
	/**
	 * Instantiates a new registered operations centre dto.
	 */
	public RegisteredOperationsCentreDTO() { }

	/** The operations centre category id. */
	private Long operationsCentreCategoryId;

	/** The vo ipurl. */
	private VoIPURLDTO voIPURL;

	/**
	 * Gets the operations centre category id.
	 *
	 * @return the operations centre category id
	 */
	public Long getOperationsCentreCategoryId() {
		return operationsCentreCategoryId;
	}

	/**
	 * Sets the operations centre category id.
	 *
	 * @param operationsCentreCategoryId the new operations centre category id
	 */
	public void setOperationsCentreCategoryId(Long operationsCentreCategoryId) {
		this.operationsCentreCategoryId = operationsCentreCategoryId;
	}

	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}

	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}

	
}
