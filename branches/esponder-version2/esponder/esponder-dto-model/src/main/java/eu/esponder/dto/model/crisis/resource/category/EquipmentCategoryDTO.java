/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.category;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.type.EquipmentTypeDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class EquipmentCategoryDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "equipmentType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EquipmentCategoryDTO extends PlannableResourceCategoryDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4964121325750331976L;

	/** The equipment type. */
	private EquipmentTypeDTO equipmentType;
	
//	private Set<EquipmentDTO> equipment;

	/**
 * Gets the equipment type.
 *
 * @return the equipment type
 */
public EquipmentTypeDTO getEquipmentType() {
		return equipmentType;
	}

	/**
	 * Sets the equipment type.
	 *
	 * @param equipmentType the new equipment type
	 */
	public void setEquipmentType(EquipmentTypeDTO equipmentType) {
		this.equipmentType = equipmentType;
	}

//	public Set<EquipmentDTO> getEquipment() {
//		return equipment;
//	}
//
//	public void setEquipment(Set<EquipmentDTO> equipment) {
//		this.equipment = equipment;
//	}
	
}
