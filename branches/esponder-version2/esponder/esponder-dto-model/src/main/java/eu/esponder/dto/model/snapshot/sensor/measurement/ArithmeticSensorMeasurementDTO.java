/*
 * 
 */
package eu.esponder.dto.model.snapshot.sensor.measurement;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

// TODO: Auto-generated Javadoc
/**
 * The Class ArithmeticSensorMeasurementDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "sensor", "timestamp", "measurement"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ArithmeticSensorMeasurementDTO extends SensorMeasurementDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8231515158702428532L;
	
	/** The measurement. */
	private BigDecimal measurement;

	/**
	 * Gets the measurement.
	 *
	 * @return the measurement
	 */
	public BigDecimal getMeasurement() {
		return measurement;
	}

	/**
	 * Sets the measurement.
	 *
	 * @param measurement the new measurement
	 */
	public void setMeasurement(BigDecimal measurement) {
		this.measurement = measurement;
	}

}
