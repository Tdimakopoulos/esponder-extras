/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.plan;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class PlannableResourceDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class PlannableResourceDTO extends ResourceDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -650765106596246416L;

}
