/*
 * 
 */
package eu.esponder.dto.model.config;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonTypeInfo;


// TODO: Auto-generated Javadoc
/**
 * The Class ESponderConfigParameterDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ESponderConfigParameterDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7734211039532779515L;
	
	/**
	 * Instantiates a new e sponder config parameter dto.
	 */
	public ESponderConfigParameterDTO() {}
	
	/** The Id. */
	private Long Id;
	
	/** The parameter name. */
	private String parameterName;
	
	/** The parameter value. */
	private String parameterValue;
	
	/** The description. */
	private String description;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return Id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		Id = id;
	}

	/**
	 * Gets the parameter name.
	 *
	 * @return the parameter name
	 */
	public String getParameterName() {
		return parameterName;
	}

	/**
	 * Sets the parameter name.
	 *
	 * @param parameterName the new parameter name
	 */
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	/**
	 * Gets the parameter value.
	 *
	 * @return the parameter value
	 */
	public String getParameterValue() {
		return parameterValue;
	}

	/**
	 * Sets the parameter value.
	 *
	 * @param parameterValue the new parameter value
	 */
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
