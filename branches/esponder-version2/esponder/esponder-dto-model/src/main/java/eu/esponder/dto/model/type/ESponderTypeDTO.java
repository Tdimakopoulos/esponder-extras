/*
 * 
 */
package eu.esponder.dto.model.type;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderTypeDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ESponderTypeDTO extends ESponderEntityDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4437636967611059143L;

	/**
	 * Instantiates a new e sponder type dto.
	 */
	public ESponderTypeDTO() {}
	
	/** The title. */
	protected String title;
	
	/** The parent. */
	protected ESponderTypeDTO parent;
	
	/** The children. */
	protected Set<ESponderTypeDTO> children;

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public ESponderTypeDTO getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(ESponderTypeDTO parent) {
		this.parent = parent;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public Set<ESponderTypeDTO> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the new children
	 */
	public void setChildren(Set<ESponderTypeDTO> children) {
		this.children = children;
	}
	
}
