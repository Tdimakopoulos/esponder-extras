/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.category;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ConsumableResourceCategoryDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "consumableResourceType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ConsumableResourceCategoryDTO extends LogisticsCategoryDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8169469202498003577L;

	/** The consumable resource type. */
	private ConsumableResourceTypeDTO consumableResourceType;

	//	private Set<ConsumableResourceDTO> consumableResources;

	/**
	 * Gets the consumable resource type.
	 *
	 * @return the consumable resource type
	 */
	public ConsumableResourceTypeDTO getConsumableResourceType() {
		return consumableResourceType;
	}

	/**
	 * Sets the consumable resource type.
	 *
	 * @param consumableResourceType the new consumable resource type
	 */
	public void setConsumableResourceType(
			ConsumableResourceTypeDTO consumableResourceType) {
		this.consumableResourceType = consumableResourceType;
	}

	//	public Set<ConsumableResourceDTO> getConsumableResources() {
	//		return consumableResources;
	//	}
	//
	//	public void setConsumableResources(
	//			Set<ConsumableResourceDTO> consumableResources) {
	//		this.consumableResources = consumableResources;
	//	}

}
