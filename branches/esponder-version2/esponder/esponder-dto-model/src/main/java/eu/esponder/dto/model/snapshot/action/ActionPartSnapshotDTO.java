/*
 * 
 */
package eu.esponder.dto.model.snapshot.action;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActionPartSnapshotStatusDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionPartSnapshotDTO.
 */
@JsonPropertyOrder({"id", "status","locationArea", "period", "actionPartId"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionPartSnapshotDTO extends SpatialSnapshotDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6629496009765832542L;

	/**
	 * Instantiates a new action part snapshot dto.
	 */
	public ActionPartSnapshotDTO() { }
	
	/** The action part id. */
	private Long actionPartId;
	
	/** The status. */
	private ActionPartSnapshotStatusDTO status;

	/**
	 * Gets the action part id.
	 *
	 * @return the action part id
	 */
	public Long getActionPartId() {
		return actionPartId;
	}

	/**
	 * Sets the action part id.
	 *
	 * @param actionPartId the new action part id
	 */
	public void setActionPartId(Long actionPartId) {
		this.actionPartId = actionPartId;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ActionPartSnapshotStatusDTO getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ActionPartSnapshotStatusDTO status) {
		this.status = status;
	}
	
		
}
