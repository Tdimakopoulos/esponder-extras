/*
 * 
 */
package eu.esponder.dto.model.snapshot.location;

import java.io.Serializable;
import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

// TODO: Auto-generated Javadoc
/**
 * The Class PointDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "latitude", "longitude", "altitude" })
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PointDTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -692299111596526761L;

	/**
	 * Instantiates a new point dto.
	 */
	public PointDTO() { }
	
	/** The latitude. */
	private BigDecimal latitude;

	/** The longitude. */
	private BigDecimal longitude;

	/** The altitude. */
	private BigDecimal altitude;
	
	/**
	 * Instantiates a new point dto.
	 *
	 * @param latitude the latitude
	 * @param longitude the longitude
	 * @param altitude the altitude
	 */
	public PointDTO(BigDecimal latitude, BigDecimal longitude, BigDecimal altitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}

	/**
	 * Gets the latitude.
	 *
	 * @return the latitude
	 */
	public BigDecimal getLatitude() {
		return latitude;
	}

	/**
	 * Sets the latitude.
	 *
	 * @param latitude the new latitude
	 */
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	/**
	 * Gets the longitude.
	 *
	 * @return the longitude
	 */
	public BigDecimal getLongitude() {
		return longitude;
	}

	/**
	 * Sets the longitude.
	 *
	 * @param longitude the new longitude
	 */
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	/**
	 * Gets the altitude.
	 *
	 * @return the altitude
	 */
	public BigDecimal getAltitude() {
		return altitude;
	}

	/**
	 * Sets the altitude.
	 *
	 * @param altitude the new altitude
	 */
	public void setAltitude(BigDecimal altitude) {
		this.altitude = altitude;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Point [latitude=" + latitude + ", longitude=" + longitude
				+ ", altitude=" + altitude + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (null == this.getAltitude() ? 0 : this.getAltitude().hashCode());
		hash = 31 * hash + (null == this.getLongitude() ? 0 : this.getLongitude().hashCode());
		hash = 31 * hash + (null == this.getLatitude() ? 0 : this.getLatitude().hashCode());
		return hash;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if ((obj == null) || (obj.getClass() != this.getClass())) {
			return false;
		}
		PointDTO pointDTO = (PointDTO) obj;
		if (compareCoordinates(this.getAltitude(), pointDTO.getAltitude())
				&& compareCoordinates(this.getLatitude(),
						pointDTO.getLatitude())
				&& compareCoordinates(this.getLongitude(),
						pointDTO.getLongitude())) {
			return true;
		}
		return false;
	}

	/**
	 * Compare coordinates.
	 *
	 * @param c1 the c1
	 * @param c2 the c2
	 * @return true, if successful
	 */
	private boolean compareCoordinates(BigDecimal c1, BigDecimal c2) {
		if (c1 == null && c2 == null) {
			return true;
		} else if ((c1 != null && c2 == null) || (c1 == null && c2 != null)) {
			return false;
		} else {
			return c1.compareTo(c2) == 0 ? true : false;
		}

	}

}
