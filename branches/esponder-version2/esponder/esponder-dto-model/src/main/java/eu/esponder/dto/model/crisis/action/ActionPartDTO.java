/*
 * 
 */
package eu.esponder.dto.model.crisis.action;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionPartDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "action", "snapshot", "actionPartObjectives","usedConsumableResources",
					"usedReusuableResources", "actionOperation", "actor"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionPartDTO extends ESponderEntityDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9041261126833929085L;

	/** The title. */
	private String title;
	
	/** The action id. */
	private Long actionId;
	
	/** Latest snapshot of this ActionPart instance. */
	private ActionPartSnapshotDTO snapshot;
	
	/** The action part objectives. */
	private Set<ActionPartObjectiveDTO> actionPartObjectives;
	
	/** The used consumable resources. */
	private Set<ConsumableResourceDTO> usedConsumableResources;
	
	/** The used reusuable resources. */
	private Set<ReusableResourceDTO> usedReusuableResources;
	
	/** The action operation. */
	private ActionOperationEnumDTO actionOperation;
	
	/** The actor. */
	private ActorDTO actor;
	
	/** The severity level. */
	private SeverityLevelDTO severityLevel;

	/**
	 * Gets the actor.
	 *
	 * @return the actor
	 */
	public ActorDTO getActor() {
		return actor;
	}

	/**
	 * Sets the actor.
	 *
	 * @param actor the new actor
	 */
	public void setActor(ActorDTO actor) {
		this.actor = actor;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the snapshot.
	 *
	 * @return the snapshot
	 */
	public ActionPartSnapshotDTO getSnapshot() {
		return snapshot;
	}

	/**
	 * Sets the snapshot.
	 *
	 * @param snapshot the new snapshot
	 */
	public void setSnapshot(ActionPartSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	/**
	 * Gets the action part objectives.
	 *
	 * @return the action part objectives
	 */
	public Set<ActionPartObjectiveDTO> getActionPartObjectives() {
		return actionPartObjectives;
	}

	/**
	 * Sets the action part objectives.
	 *
	 * @param actionPartObjectives the new action part objectives
	 */
	public void setActionPartObjectives(Set<ActionPartObjectiveDTO> actionPartObjectives) {
		this.actionPartObjectives = actionPartObjectives;
	}

	/**
	 * Gets the used consumable resources.
	 *
	 * @return the used consumable resources
	 */
	public Set<ConsumableResourceDTO> getUsedConsumableResources() {
		return usedConsumableResources;
	}

	/**
	 * Sets the used consumable resources.
	 *
	 * @param usedConsumableResources the new used consumable resources
	 */
	public void setUsedConsumableResources(Set<ConsumableResourceDTO> usedConsumableResources) {
		this.usedConsumableResources = usedConsumableResources;
	}

	/**
	 * Gets the used reusuable resources.
	 *
	 * @return the used reusuable resources
	 */
	public Set<ReusableResourceDTO> getUsedReusuableResources() {
		return usedReusuableResources;
	}

	/**
	 * Sets the used reusuable resources.
	 *
	 * @param usedReusuableResources the new used reusuable resources
	 */
	public void setUsedReusuableResources(Set<ReusableResourceDTO> usedReusuableResources) {
		this.usedReusuableResources = usedReusuableResources;
	}

	/**
	 * Gets the action operation.
	 *
	 * @return the action operation
	 */
	public ActionOperationEnumDTO getActionOperation() {
		return actionOperation;
	}

	/**
	 * Sets the action operation.
	 *
	 * @param actionOperation the new action operation
	 */
	public void setActionOperation(ActionOperationEnumDTO actionOperation) {
		this.actionOperation = actionOperation;
	}

	/**
	 * Gets the action id.
	 *
	 * @return the action id
	 */
	public Long getActionId() {
		return actionId;
	}

	/**
	 * Sets the action id.
	 *
	 * @param actionId the new action id
	 */
	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}

	/**
	 * Gets the severity level.
	 *
	 * @return the severity level
	 */
	public SeverityLevelDTO getSeverityLevel() {
		return severityLevel;
	}

	/**
	 * Sets the severity level.
	 *
	 * @param severityLevel the new severity level
	 */
	public void setSeverityLevel(SeverityLevelDTO severityLevel) {
		this.severityLevel = severityLevel;
	}

}
