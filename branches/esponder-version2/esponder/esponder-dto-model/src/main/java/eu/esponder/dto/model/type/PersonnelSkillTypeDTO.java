/*
 * 
 */
package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelSkillTypeDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "parent", "children"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PersonnelSkillTypeDTO extends ESponderTypeDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4497475878163337691L;

}
