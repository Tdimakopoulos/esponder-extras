/*
 * 
 */
package eu.esponder.dto.model.crisis;

// TODO: Auto-generated Javadoc
/**
 * The Enum CrisisContextAlertEnumDTO.
 */
public enum CrisisContextAlertEnumDTO {
	
	/** The Level1. */
	Level1,
	
	/** The Level2. */
	Level2,
	
	/** The Level3. */
	Level3,
	
	/** The Level4. */
	Level4,
	
	/** The Level5. */
	Level5
}
