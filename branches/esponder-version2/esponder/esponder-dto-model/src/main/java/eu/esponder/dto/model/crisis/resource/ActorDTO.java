/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status","operationsCentre",  "equipmentSet", "subordinates", "snapshot", "supervisor", "voIPURL"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActorDTO extends ResourceDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 660813303586822307L;

	/**
	 * Instantiates a new actor dto.
	 */
	public ActorDTO() { }

	/** The equipment set. */
	private Set<EquipmentDTO> equipmentSet;

	/** The subordinates. */
	private Set<ActorDTO> subordinates;

	/** The snapshot. */
	private ActorSnapshotDTO snapshot;

	/** The supervisor. */
	private ActorDTO supervisor;
	
	/** The vo ipurl. */
	private VoIPURLDTO voIPURL;
	
	/** The operations centre id. */
	private Long operationsCentreId;
	
	/** The personnel. */
	private PersonnelDTO personnel;

	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}

	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}

	/**
	 * Gets the equipment set.
	 *
	 * @return the equipment set
	 */
	public Set<EquipmentDTO> getEquipmentSet() {
		return equipmentSet;
	}

	/**
	 * Sets the equipment set.
	 *
	 * @param equipmentSet the new equipment set
	 */
	public void setEquipmentSet(Set<EquipmentDTO> equipmentSet) {
		this.equipmentSet = equipmentSet;
	}

	/**
	 * Gets the subordinates.
	 *
	 * @return the subordinates
	 */
	public Set<ActorDTO> getSubordinates() {
		return subordinates;
	}

	/**
	 * Sets the subordinates.
	 *
	 * @param subordinates the new subordinates
	 */
	public void setSubordinates(Set<ActorDTO> subordinates) {
		this.subordinates = subordinates;
	}

	/**
	 * Gets the snapshot.
	 *
	 * @return the snapshot
	 */
	public ActorSnapshotDTO getSnapshot() {
		return snapshot;
	}

	/**
	 * Sets the snapshot.
	 *
	 * @param snapshot the new snapshot
	 */
	public void setSnapshot(ActorSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	/**
	 * Gets the supervisor.
	 *
	 * @return the supervisor
	 */
	public ActorDTO getSupervisor() {
		return supervisor;
	}

	/**
	 * Sets the supervisor.
	 *
	 * @param supervisor the new supervisor
	 */
	public void setSupervisor(ActorDTO supervisor) {
		this.supervisor = supervisor;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.ResourceDTO#toString()
	 */
	@Override
	public String toString() {
		return "ActorDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}

	/**
	 * Gets the personnel.
	 *
	 * @return the personnel
	 */
	public PersonnelDTO getPersonnel() {
		return personnel;
	}

	/**
	 * Sets the personnel.
	 *
	 * @param personnel the new personnel
	 */
	public void setPersonnel(PersonnelDTO personnel) {
		this.personnel = personnel;
	}

	/**
	 * Gets the operations centre id.
	 *
	 * @return the operations centre id
	 */
	public Long getOperationsCentreId() {
		return operationsCentreId;
	}

	/**
	 * Sets the operations centre id.
	 *
	 * @param operationsCentreId the new operations centre id
	 */
	public void setOperationsCentreId(Long operationsCentreId) {
		this.operationsCentreId = operationsCentreId;
	}

}
