/*
 * 
 */
package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonTypeInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreTypeDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class OperationsCentreTypeDTO extends ESponderTypeDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4180901117994144848L;

}
