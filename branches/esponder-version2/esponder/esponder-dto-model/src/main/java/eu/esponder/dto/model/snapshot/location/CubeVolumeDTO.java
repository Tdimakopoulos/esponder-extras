/*
 * 
 */
package eu.esponder.dto.model.snapshot.location;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

// TODO: Auto-generated Javadoc
/**
 * The Class CubeVolumeDTO.
 */
@JsonPropertyOrder({"id", "point", "acme"})
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CubeVolumeDTO extends LocationAreaDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1716194673074175161L;

	/**
	 * Instantiates a new cube volume dto.
	 */
	public CubeVolumeDTO() { }
	
	/** The point. */
	private PointDTO point;

	/** The acme. */
	private BigDecimal acme;

	/**
	 * Gets the point.
	 *
	 * @return the point
	 */
	public PointDTO getPoint() {
		return point;
	}

	/**
	 * Sets the point.
	 *
	 * @param point the new point
	 */
	public void setPoint(PointDTO point) {
		this.point = point;
	}

	/**
	 * Gets the acme.
	 *
	 * @return the acme
	 */
	public BigDecimal getAcme() {
		return acme;
	}

	/**
	 * Sets the acme.
	 *
	 * @param acme the new acme
	 */
	public void setAcme(BigDecimal acme) {
		this.acme = acme;
	}

}
