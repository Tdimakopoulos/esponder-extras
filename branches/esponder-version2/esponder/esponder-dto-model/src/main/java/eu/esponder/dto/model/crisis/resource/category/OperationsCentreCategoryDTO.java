/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.category;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.type.OperationsCentreTypeDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreCategoryDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "operationsCentreType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OperationsCentreCategoryDTO extends PlannableResourceCategoryDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2966454872810486900L;

	/**
	 * Instantiates a new operations centre category dto.
	 */
	public OperationsCentreCategoryDTO() { }
	
	/** The operations centre type. */
	private OperationsCentreTypeDTO operationsCentreType;
	
//	private Set<OperationsCentreDTO> operationsCentres;
	
	/**
 * Gets the operations centre type.
 *
 * @return the operations centre type
 */
public OperationsCentreTypeDTO getOperationsCentreType() {
		return operationsCentreType;
	}

	/**
	 * Sets the operations centre type.
	 *
	 * @param operationsCentreType the new operations centre type
	 */
	public void setOperationsCentreType(OperationsCentreTypeDTO operationsCentreType) {
		this.operationsCentreType = operationsCentreType;
	}

//	public Set<OperationsCentreDTO> getOperationsCentres() {
//		return operationsCentres;
//	}
//
//	public void setOperationsCentres(Set<OperationsCentreDTO> operationsCentres) {
//		this.operationsCentres = operationsCentres;
//	}
}
