/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.event.model.snapshot.SnapshotEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class ResourceSnapshotEvent.
 *
 * @param <T> the generic type
 */
public abstract class ResourceSnapshotEvent<T extends SnapshotDTO> extends SnapshotEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6302091892935250120L;
	
}
