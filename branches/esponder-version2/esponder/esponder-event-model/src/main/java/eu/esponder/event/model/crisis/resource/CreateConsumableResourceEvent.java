/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.event.model.CreateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class CreateConsumableResourceEvent.
 */
public class CreateConsumableResourceEvent extends ConsumableResourceEvent<ConsumableResourceDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4348849414526185747L;

}
