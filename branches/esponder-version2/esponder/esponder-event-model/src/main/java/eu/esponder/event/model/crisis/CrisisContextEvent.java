/*
 * 
 */
package eu.esponder.event.model.crisis;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.event.model.ESponderEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class CrisisContextEvent.
 *
 * @param <T> the generic type
 */
public abstract class CrisisContextEvent<T extends CrisisContextDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2806433512434184568L;

}
