/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.SnapshotDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ConsumableResourceSnapshotEvent.
 *
 * @param <T> the generic type
 */
public abstract class ConsumableResourceSnapshotEvent<T extends SnapshotDTO> extends ResourceSnapshotEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2544414366303024642L;

}
