/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.event.model.UpdateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateConsumableResourceSnapshotEvent.
 */
public class UpdateConsumableResourceSnapshotEvent extends ConsumableResourceSnapshotEvent<SnapshotDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3471927216194630496L;
	
	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
