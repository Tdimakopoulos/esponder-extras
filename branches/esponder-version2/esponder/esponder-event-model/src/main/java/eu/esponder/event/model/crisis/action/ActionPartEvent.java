/*
 * 
 */
package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.event.model.ESponderEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class ActionPartEvent.
 *
 * @param <T> the generic type
 */
public abstract class ActionPartEvent<T extends ActionPartDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9001909010882055239L;
	
}
