/*
 * 
 */
package eu.esponder.event.model;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class ESponderTypeEvent.
 *
 * @param <T> the generic type
 */
public abstract class ESponderTypeEvent<T extends ESponderEntityDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9036535870972195826L;
	
}
