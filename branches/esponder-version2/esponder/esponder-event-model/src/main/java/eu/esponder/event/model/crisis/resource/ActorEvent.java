/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ActorDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorEvent.
 *
 * @param <T> the generic type
 */
public abstract class ActorEvent<T extends ActorDTO> extends ResourceEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8357301175649628740L;

}
