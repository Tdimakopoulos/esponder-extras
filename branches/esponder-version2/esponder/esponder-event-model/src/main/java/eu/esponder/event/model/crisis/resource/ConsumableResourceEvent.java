/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ConsumableResourceEvent.
 *
 * @param <T> the generic type
 */
public abstract class ConsumableResourceEvent<T extends ConsumableResourceDTO> extends ResourceEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4847055252359575844L;

}
