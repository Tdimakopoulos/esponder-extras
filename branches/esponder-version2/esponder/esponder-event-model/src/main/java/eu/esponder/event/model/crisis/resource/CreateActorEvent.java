/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.event.model.CreateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class CreateActorEvent.
 */
public class CreateActorEvent extends ActorEvent<ActorDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6318868022859370822L;
	
}
