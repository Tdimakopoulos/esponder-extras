/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreEvent.
 *
 * @param <T> the generic type
 */
public abstract class OperationsCentreEvent<T extends OperationsCentreDTO> extends ResourceEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4415380713125894915L;

}
