/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorSnapshotEvent.
 *
 * @param <T> the generic type
 */
public abstract class ActorSnapshotEvent<T extends ActorSnapshotDTO> extends ResourceSnapshotEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1475358159029683283L;
	
}
