/*
 * 
 */
package eu.esponder.event.model.snapshot.action;

import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.event.model.snapshot.SnapshotEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class ActionSnapshotEvent.
 *
 * @param <T> the generic type
 */
public abstract class ActionSnapshotEvent<T extends ActionSnapshotDTO> extends SnapshotEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3799871246977498985L;

}
