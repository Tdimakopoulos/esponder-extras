/*
 * 
 */
package eu.esponder.event.model.snapshot.measurement;

import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ArithmeticSensorMeasurementEvent.
 *
 * @param <T> the generic type
 */
public abstract class ArithmeticSensorMeasurementEvent<T extends ArithmeticSensorMeasurementDTO> extends SensorMeasurementEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3365711964069162650L;

}
