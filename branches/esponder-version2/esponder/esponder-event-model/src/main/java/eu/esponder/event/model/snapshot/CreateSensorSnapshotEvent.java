/*
 * 
 */
package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.event.model.CreateEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateSensorSnapshotEvent.
 */
public class CreateSensorSnapshotEvent extends SnapshotEvent<SensorSnapshotDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4737435535926991762L;

	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
