/*
 * 
 */
package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.event.model.CreateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class CreateCrisisContextSnapshotEvent.
 */
public class CreateCrisisContextSnapshotEvent extends CrisisContextSnapshotEvent<CrisisContextSnapshotDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4658214780264993947L;
	
}
