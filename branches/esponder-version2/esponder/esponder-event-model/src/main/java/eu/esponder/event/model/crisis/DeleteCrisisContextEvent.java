/*
 * 
 */
package eu.esponder.event.model.crisis;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.event.model.DeleteEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class DeleteCrisisContextEvent.
 */
public class DeleteCrisisContextEvent extends CrisisContextEvent<CrisisContextDTO> implements DeleteEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1139683152265684159L;
		
	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo="CrisisContextEvent Journal Info";
		return JournalMessageInfo;
	}
}
