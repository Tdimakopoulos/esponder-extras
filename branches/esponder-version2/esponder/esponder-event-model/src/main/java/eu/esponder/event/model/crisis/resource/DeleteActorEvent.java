/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.event.model.DeleteEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class DeleteActorEvent.
 */
public class DeleteActorEvent extends ActorEvent<ActorDTO> implements DeleteEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 451689214673500716L;
	
}
