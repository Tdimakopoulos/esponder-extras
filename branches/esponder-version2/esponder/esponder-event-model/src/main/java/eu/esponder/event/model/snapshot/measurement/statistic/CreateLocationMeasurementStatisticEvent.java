/*
 * 
 */
package eu.esponder.event.model.snapshot.measurement.statistic;

import java.util.List;

import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.CreateEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateLocationMeasurementStatisticEvent.
 */
public class CreateLocationMeasurementStatisticEvent extends SensorMeasurementStatisticEvent<SensorMeasurementStatisticEnvelopeDTO> implements CreateEvent { 
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2994208980374328457L;

	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		List<SensorMeasurementStatisticDTO> locationMeasurements = this.getEventAttachment().getMeasurementStatistics();
		
		String journalMessageInfo = "Received Location Measurement Statistics Envelope including: \n";
		for (SensorMeasurementStatisticDTO sensorMeasurement : locationMeasurements) {
			LocationSensorMeasurementDTO locMeasurement = (LocationSensorMeasurementDTO) sensorMeasurement.getStatistic();
			journalMessageInfo += locMeasurement.toString();
		}
		return journalMessageInfo;
	}
}
