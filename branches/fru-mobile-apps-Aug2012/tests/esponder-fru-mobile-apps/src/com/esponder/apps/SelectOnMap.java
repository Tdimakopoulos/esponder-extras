package com.esponder.apps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.Projection;


public class SelectOnMap extends MapActivity {

	private TextView currentAddress;
	private Button confirmAddress;

	private MapView myMapView;
	private MapController myMapController;
	Geocoder myGeocoder; 
	int Mode;
	GeoPoint temploc;

	@Override
	protected void onCreate(Bundle icicle) {
		// TODO Auto-generated method stub
		super.onCreate(icicle);
		setContentView(R.layout.mymapview);
		myGeocoder = new Geocoder(this);
		Bundle bundle = this.getIntent().getExtras();
		 Mode = bundle.getInt("Mode");

		myMapView = (MapView) findViewById(R.id.mapview);
		myMapController = myMapView.getController();
		myMapView.setBuiltInZoomControls(true);

		currentAddress = (TextView) findViewById(R.id.currentAddress); 
		confirmAddress = (Button) findViewById(R.id.confirmAddress); 
     	confirmAddress.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				String address=currentAddress.getText().toString();
				AlertDialog.Builder dialog = new AlertDialog.Builder(view.getContext());
				if(!address.equals("")){
	 		      dialog.setTitle("Confirm Change");
			      dialog.setMessage(address);
			      dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			          public void onClick(DialogInterface dialog, int id) {
			        	  if (Mode == 0) {
			        	  RoutingSelection.GeoPointFrom = temploc;
			        	  }
			        	  else{
			        		  RoutingSelection.GeoPointTo = temploc;
			        	  }
			        	  finish();
			          }
			      });
			      dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
			          public void onClick(DialogInterface dialog, int id) {
			       	    
			          }
			      });
			       
				}else{
					dialog.setMessage("Please Select an Address");
					dialog.setPositiveButton("Ok", null);
				}
				dialog.show(); 
			}

		});

		if (Mode == 0) {
			 
			int intLatitude = bundle.getInt("Latitude");
			int intLongitude = bundle.getInt("Longitude");
			if((intLatitude!=0) && (intLongitude!=0)){
			  GeoPoint initGeoPoint = new GeoPoint(intLatitude, intLongitude);
			  CenterLocation(initGeoPoint);
			 } else{
			  GeoPoint initGeoPoint = myMapView.getMapCenter();
		   	  CenterLocation(initGeoPoint);
			 }
			
		} else if (Mode == 1) {
			int intLatitude = bundle.getInt("Latitude");
			int intLongitude = bundle.getInt("Longitude");
 	    	GeoPoint initGeoPoint = myMapView.getMapCenter();
			Drawable drawable = this.getResources().getDrawable(R.drawable.marker2);
			CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
					drawable, this);

			GeoPoint point1 = new GeoPoint(intLatitude, intLongitude);

			OverlayItem overlayitem = new OverlayItem(point1, "Fr1", "Fr1");
			itemizedOverlay.addOverlay(overlayitem);
			myMapView.getOverlays().add(itemizedOverlay);
			CenterLocation(initGeoPoint);
		}
	}

	private void placeMarker(int markerLatitude, int markerLongitude) {
		Drawable marker = getResources().getDrawable(
				android.R.drawable.ic_menu_myplaces);
		marker.setBounds(0, 0, marker.getIntrinsicWidth(),
				marker.getIntrinsicHeight());
		myMapView.getOverlays().add(
				new InterestingLocations(marker, markerLatitude,
						markerLongitude,this));
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	private void CenterLocation(GeoPoint centerGeoPoint) {
		myMapController.animateTo(centerGeoPoint);

	/*	myLongitude
		 		.setText("Longitude: "
			 		+ String.valueOf((float) centerGeoPoint
				 				.getLongitudeE6() / 1000000));
		myLatitude
				.setText("Latitude: "
						+ String.valueOf((float) centerGeoPoint.getLatitudeE6() / 1000000));  */
		
		placeMarker(centerGeoPoint.getLatitudeE6(),
				centerGeoPoint.getLongitudeE6());
		 
	};

 

	class InterestingLocations extends ItemizedOverlay<OverlayItem> {

		private List<OverlayItem> locations = new ArrayList<OverlayItem>();
		private Drawable marker;
		private Context context;
		private OverlayItem myOverlayItem;

		boolean MoveMap;

		public InterestingLocations(Drawable defaultMarker, int LatitudeE6,
				int LongitudeE6,Context context) {
			super(defaultMarker);
			// TODO Auto-generated constructor stub
			this.marker = defaultMarker;
			// create locations of interest
			GeoPoint myPlace = new GeoPoint(LatitudeE6, LongitudeE6);
			myOverlayItem = new OverlayItem(myPlace, "My Place", "My Place");
			locations.add(myOverlayItem);
			this.context = context;
			populate();
		}

		@Override
		protected OverlayItem createItem(int i) {
			// TODO Auto-generated method stub
			return locations.get(i);
		}

		@Override
		public int size() {
			// TODO Auto-generated method stub
			return locations.size();
		}

		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			// TODO Auto-generated method stub
			super.draw(canvas, mapView, shadow);

			boundCenterBottom(marker);
		}

		public void addOverlay(OverlayItem overlay) {
			locations.add(overlay);
		       this.populate();
		   }
		
		@Override
		public boolean onTouchEvent(MotionEvent arg0, MapView arg1) {
			// TODO Auto-generated method stub
			// super.onTouchEvent(arg0, arg1);

			int Action = arg0.getAction();
			if (Action == MotionEvent.ACTION_UP) {

				if (!MoveMap) {
					Projection proj = myMapView.getProjection();
					GeoPoint loc = proj.fromPixels((int) arg0.getX(),
							(int) arg0.getY());

					List<Address> addresses = null;
					try {
						addresses = myGeocoder.getFromLocation(
								(double) loc.getLatitudeE6() / 1E6,
								(double) loc.getLongitudeE6() / 1E6, 1);
					//	myGeocoder.get
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	 
	 			      currentAddress
						.setText(addresses.get(0).getAddressLine(0)+" "+addresses.get(0).getAddressLine(1)+" "+addresses.get(0).getAddressLine(2));
					if(Mode==0){ 
					// remove the last marker
						myMapView.getOverlays().remove(0);
						temploc =loc;
		 			}
					else{
						myMapView.getOverlays().remove(1);
						temploc =loc;
					}
					CenterLocation(loc);
				}

			} else if (Action == MotionEvent.ACTION_DOWN) {

				MoveMap = false;

			} else if (Action == MotionEvent.ACTION_MOVE) {
				MoveMap = true;
			}

			return super.onTouchEvent(arg0, arg1);
			// return false;
		}
		
 
	}
}