package com.esponder.apps;
 
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.event.model.config.UpdateConfigurationEvent;
import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.event.model.crisis.UpdateCrisisContextEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionPartSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateLocationMeasurementStatisticEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.jsonrcp.JSONRPCClient;
import eu.esponder.jsonrcp.JSONRPCException;

public class Receiver extends BroadcastReceiver {
 
	private String action;
	private JSONRPCClient  client = JSONRPCClient.create("http://localhost:8080/remote/json-rpc");
	private Long myID;
	private boolean isFATAL = false;
	
	public Receiver(Long myID){
		this.myID = myID;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) { 
		
		// Get the event
		  String incomingEvent = intent.getStringExtra("event");
		  ObjectMapper	mapper = new ObjectMapper();
		  mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
		  JsonFactory jsonFactory = new JsonFactory(); 
			  
		//get action
	  	action = intent.getAction();
		
		if (action.contains(CreateSensorMeasurementStatisticEvent.class.getName())) {
			 Log.d(" 11111111111 "," CreateSensorMeasurementStatisticEvent ");
			  CreateSensorMeasurementStatisticEvent event = null;
			  // deserialize incoming Event
			 try {
			      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
			      // get the event
			       event = mapper.readValue(jp, CreateSensorMeasurementStatisticEvent.class);
			       if(  event.getEventSeverity() == SeverityLevelDTO.FATAL){
			    	   isFATAL = true;
			       }
			  } catch (Exception e) {
			      e.printStackTrace();
			    }
			 
			 
             try {
            	 // Save incoming Event
            	 Log.d(" before "," before CreateSensorMeasurementStatisticEvent");
				  client.call("esponderdb/addESponderEvent", null, event);
				 Log.d(" after "," after CreateSensorMeasurementStatisticEvent");
				 
				 if(event.getEventSource().getId() != myID){
				   // Save Statistics DTO
				   List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticList =  event.getEventAttachment().getMeasurementStatistics();
				   for(int i =0 ; i < sensorMeasurementStatisticList.size() ; i++){
					  Log.d(" before "," before adding sensorMeasurementStatisticDTO ");
		              client.call("esponderdb/addESponderDTO", null, sensorMeasurementStatisticList.get(i));
		              Log.d(" after "," after adding sensorMeasurementStatisticDTO  ");
				   }
				 }
				 
				 
	 		} catch (JSONRPCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
             
             
			
		} else if(action.contains(CreateLocationMeasurementStatisticEvent.class.getName())){
			 Log.d(" 22222222222 "," CreateLocationMeasurementStatisticEvent ");
			 CreateLocationMeasurementStatisticEvent event = null;
			  // deserialize incoming Event
			 try {
			      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
			      // get the event
			      event = mapper.readValue(jp, CreateLocationMeasurementStatisticEvent.class);
			      if(  event.getEventSeverity() == SeverityLevelDTO.FATAL){
			    	   isFATAL = true;
			       }
			  } catch (Exception e) {
			      e.printStackTrace();
			    }
			 
			 // Save incoming Event
             try {
            	 Log.d(" before "," before CreateLocationMeasurementStatisticEvent");
				  client.call("esponderdb/addESponderEvent", null, event);
				 Log.d(" after "," after CreateLocationMeasurementStatisticEvent");
			} catch (JSONRPCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
		} else if(action.contains(CreateActionEvent.class.getName())){
			 Log.d(" 33333333333 "," CreateActionEvent ");
			 CreateActionEvent event = null;
			  // deserialize incoming Event
			 try {
			      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
			      // get the event
			       event = mapper.readValue(jp, CreateActionEvent.class);
			       if(  event.getEventSeverity() == SeverityLevelDTO.FATAL){
			    	   isFATAL = true;
			       }
			  } catch (Exception e) {
			      e.printStackTrace();
			    }
			 
			 
             try {
            	 // Save incoming Event
            	 Log.d(" before "," before CreateActionEvent");
				 client.call("esponderdb/addESponderEvent", null, event);
				 Log.d(" after "," after CreateActionEvent");
				 
				 // Save ActionDTO
				 Log.d(" before "," before adding CreateActionEvent ");
	             client.call("esponderdb/addESponderDTO", null, event.getEventAttachment());
	             Log.d(" after "," after adding CreateActionEvent  ");
				 
			} catch (JSONRPCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			 
		} else if(action.contains(UpdateActionPartSnapshotEvent.class.getName())){
			 Log.d(" 44444444444 "," UpdateActionPartSnapshotEvent ");
			 UpdateActionPartSnapshotEvent event = null;
			  // deserialize incoming Event
			 try {
			      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
			      // get the event
			       event = mapper.readValue(jp, UpdateActionPartSnapshotEvent.class);
			       if(  event.getEventSeverity() == SeverityLevelDTO.FATAL){
			    	   isFATAL = true;
			       }
			  } catch (Exception e) {
			      e.printStackTrace();
			    } 
			 
             try {
            	 // Save incoming Event
            	 Log.d(" before "," before UpdateActionPartSnapshotEvent");
				 client.call("esponderdb/addESponderEvent", null, event);
				 Log.d(" after "," after UpdateActionPartSnapshotEvent");
			 // Update ActionPart Snapshot in db
		ActionDTO action = (ActionDTO) Menu.client.call("esponderdb/getESponderDTO",
						         ESponderEntityDTO.class,  event.getEventAttachment().getActionPartId(), ActionDTO.class.getName());
			     
				  HashSet<ActionPartDTO> actionPartsSet =  (HashSet<ActionPartDTO>) action.getActionParts();
    	    	  Iterator actionPartIterator = actionPartsSet.iterator(); 
    		      while (actionPartIterator.hasNext()){
    		    	  ActionPartDTO actionPartTemp =  ((ActionPartDTO) actionPartIterator.next());
    		    	    if(actionPartTemp.getId() == event.getEventAttachment().getActionPartId()){
    		    	    	actionPartTemp.setSnapshot(event.getEventAttachment());
    		    	    
    		    	    	Log.d(" before "," before UpdateActionPartSnapshotDTO ");
    						Menu.client.call("esponderdb/updateESponderDTO", null, action);
    					    Log.d(" after "," after UpdateActionPartSnapshotDTO ");
    		    	      }
    		    	  } 
 		 		    
			} catch (JSONRPCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
		} else if(action.contains(UpdateActionSnapshotEvent.class.getName())){
			 Log.d(" 55555555555 "," UpdateActionSnapshotEvent ");
			 UpdateActionSnapshotEvent event = null;
			  // deserialize incoming Event
			 try {
			      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
			      // get the event
			      event = mapper.readValue(jp, UpdateActionSnapshotEvent.class);
			      if(  event.getEventSeverity() == SeverityLevelDTO.FATAL){
			    	   isFATAL = true;
			       }
			  } catch (Exception e) {
			      e.printStackTrace();
			    }
			 
			    try {
	            	 // Save incoming Event
	            	 Log.d(" before "," before UpdateActionSnapshotEvent ");
					 client.call("esponderdb/addESponderEvent", null, event);
					 Log.d(" after "," after UpdateActionSnapshotEvent ");
					 
					 // Update ActionPart Snapshot in db
			ActionDTO action = (ActionDTO) Menu.client.call("esponderdb/getESponderDTO",
							         ESponderEntityDTO.class, event.getEventAttachment().getAction().getId(), ActionDTO.class.getName());
				     
			          action.setSnapshot(event.getEventAttachment()); 
					  HashSet<ActionPartDTO>  actionPartsSet =  (HashSet<ActionPartDTO>) action.getActionParts();
	    	    	  Iterator actionPartIterator = actionPartsSet.iterator(); 
	    	    	   while (actionPartIterator.hasNext()){
			    	    	  ActionPartDTO actionPartTemp =  ((ActionPartDTO) actionPartIterator.next());
			    	    	//  actionPartTemp.getSnapshot().setStatus(action.getSnapshot().getStatus());  // Add get sto ActionSnapshotStatusDTO
			    	      	  } 
	    		
	 		 		    
				} catch (JSONRPCException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			        
		} else if(action.contains(CreateCrisisContextEvent.class.getName())){
		
			 Log.d(" 666666666 "," CreateCrisisContextEvent ");
			 CreateCrisisContextEvent event = null;
			  // deserialize incoming Event
			 try {
			      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
			      // get the event
			       event = mapper.readValue(jp, CreateCrisisContextEvent.class);
			       if(  event.getEventSeverity() == SeverityLevelDTO.FATAL){
			    	   isFATAL = true;
			       }
			  } catch (Exception e) {
			      e.printStackTrace();
			    }
			 
			 
            try {
           	 // Save incoming Event
           	 Log.d(" before "," before CreateCrisisContextEvent");
				  client.call("esponderdb/addESponderEvent", null, event);
				 Log.d(" after "," after CreateCrisisContextEvent");	
				} catch (JSONRPCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

            
            
		} else if(action.contains(UpdateCrisisContextEvent.class.getName())){
			
			 Log.d(" 777777777 "," UpdateCrisisContextEvent ");
			  UpdateCrisisContextEvent event = null;
			  // deserialize incoming Event
			 try {
			      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
			      // get the event
			       event = mapper.readValue(jp, UpdateCrisisContextEvent.class);
			       if(  event.getEventSeverity() == SeverityLevelDTO.FATAL){
			    	   isFATAL = true;
			       }
			  } catch (Exception e) {
			      e.printStackTrace();
			    }
			 
			 
           try {
          	 // Save incoming Event
          	 Log.d(" before "," before UpdateCrisisContextEvent");
				  client.call("esponderdb/addESponderEvent", null, event);
				 Log.d(" after "," after UpdateCrisisContextEvent");	
				} catch (JSONRPCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

           
           
		}  else if(action.contains(UpdateConfigurationEvent.class.getName())){
			String jsonActor = null;
			try {
				JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
				// get the event
				UpdateConfigurationEvent event = mapper.readValue(jp,
						UpdateConfigurationEvent.class);
			    if(  event.getEventSeverity() == SeverityLevelDTO.FATAL){
			    	   isFATAL = true;
			       }
				ActorDTO actorDTO = (ActorDTO) event.getEventAttachment(); 

				mapper = new ObjectMapper();
				mapper.getSerializationConfig().setSerializationInclusion(
						JsonSerialize.Inclusion.NON_NULL);
				mapper.configure(
						DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,
						false);
				jsonActor = mapper.writeValueAsString(actorDTO);

			} catch (Exception e) {
				e.printStackTrace();
			}

			// Write To SD Card
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File(sdCard.getAbsolutePath() + "/esponder");
			dir.mkdirs();
			File file = new File(dir, "config.json");
			try {
				FileOutputStream f = new FileOutputStream(file);
				OutputStreamWriter myOutWriter = new OutputStreamWriter(f);
				myOutWriter.write(jsonActor);

				myOutWriter.close();
				f.close();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// if Event Fatal Show Message
		if(isFATAL){
		    Intent trIntent = new Intent("android.intent.action.MAIN");
     		trIntent.setClass(context, MyAlertDialog.class);
     		trIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);        
     		trIntent.putExtra("alertMessage","  A Fatal Event Arrived ");
     	    context.startActivity(trIntent);       
		}
	}
	
	/*private ActionDTO getAction(Long actionPartId) {
		try {
			ActionDTO[] allActions = (ActionDTO[]) Menu.client.call("esponderdb/getESponderDTO",
			         ActionDTO[].class, ActionDTO.class.getName());
			for (ActionDTO action : allActions) {
				for (ActionPartDTO actionPart : action.getActionParts()) {
					System.out.println("To id = " + actionPart.getId());
					if (actionPart.getId().equals(actionPartId)) {
						return action;
					}
				}
			}
			
		} catch (JSONRPCException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}*/

}

