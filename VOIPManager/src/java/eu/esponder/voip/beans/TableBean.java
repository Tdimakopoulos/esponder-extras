/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.voip.beans;


import eu.esponder.net.voip.voipcalls;
import eu.esponder.voip.db.voipdb;
import eu.esponder.voip.db.voipset;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author MEOC-WS1
 */
@ManagedBean
@SessionScoped
public class TableBean {

   
    private List<VOIPEntries> carsSmall;
    private List<VOIPEntries> carsSmallarc;
    private VOIPEntries selectedCar;

    public TableBean() {
        
        carsSmall = new ArrayList<VOIPEntries>();
        carsSmallarc = new ArrayList<VOIPEntries>();
        populateRandomCars();
    }

    public void populateRandomCars() {
        carsSmall.clear();
        System.out.println("Load");
         voipdb pdb= new voipdb();
        try {
            pdb.LoadFronFile();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        }
                   System.out.println("Position "+pdb.getpos());
                   System.out.println("ID "+pdb.getItem(0).getId());
          int iposs=pdb.getpos()+1;
          for (int i=0;i<iposs-1;i++)
          {
          System.out.println(pdb.getItem(i).getId().toString()+ pdb.getItem(i).getSp1()+pdb.getItem(i).getSp2()+ pdb.getItem(i).getId().toString());
          Date pdate=new Date();
          pdate.setTime(pdb.getItem(i).getId());
          carsSmall.add(new VOIPEntries(pdb.getItem(i).getId().toString(),  pdb.getItem(i).getSp1()+" - "+pdb.getItem(i).getSp2(), pdate.toString()));
          }
                populateRandomCars2();
    }
    
        public void populateRandomCars2() {
        carsSmallarc.clear();
        System.out.println("Load");
         voipdb pdb= new voipdb();
        try {
            pdb.LoadFronFile();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        }
                   System.out.println("Position "+pdb.getposa());
                   System.out.println("ID "+pdb.getItema(0).getId());
          int iposs=pdb.getposa()+1;
          for (int i=0;i<iposs-1;i++)
          {
       //   System.out.println(pdb.getItem(i).getId().toString()+ pdb.getItem(i).getSp1()+pdb.getItem(i).getSp2()+ pdb.getItem(i).getId().toString());
          Date pdate=new Date();
          pdate.setTime(pdb.getItema(i).getId());
          carsSmallarc.add(new VOIPEntries(pdb.getItema(i).getId().toString(),  pdb.getItema(i).getSp1()+" - "+pdb.getItema(i).getSp2(), pdate.toString()));
          }
                
    }

    public VOIPEntries getSelectedCar() {
        return selectedCar;
    }

    public void setSelectedCar(VOIPEntries selectedCar) {
        this.selectedCar = selectedCar;
    }

    public List<VOIPEntries> getCarsSmall() {
        return carsSmall;
    }

    public void deleteCar() {
         voipdb pdb= new voipdb();
        try {
            pdb.LoadFronFile();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        voipset pitem=new voipset();
        pitem.setId(Long.valueOf(selectedCar.getCallid()));
        pdb.Delete(pitem);
        voipcalls pcc= new voipcalls();
        try {
            pcc.DeleteCall(Integer.valueOf(String.valueOf(selectedCar.getCallid())), selectedCar.getAttendants());
        } catch (Exception ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            pdb.SaveOnFile();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        carsSmall.remove(selectedCar);
    }

    /**
     * @return the carsSmallarc
     */
    public List<VOIPEntries> getCarsSmallarc() {
        return carsSmallarc;
    }

    /**
     * @param carsSmallarc the carsSmallarc to set
     */
    public void setCarsSmallarc(List<VOIPEntries> carsSmallarc) {
        this.carsSmallarc = carsSmallarc;
    }
}
