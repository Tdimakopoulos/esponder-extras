/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.voip.beans;

/**
 *
 * @author MEOC-WS1
 */
public class VOIPEntries {
   private String callid;
       
        private String attendants;
        private String starttime;
        
        public VOIPEntries(String model,  String manufacturer, String color) {
                this.callid = model;
       
                this.attendants = manufacturer;
                this.starttime = color;
        }

    /**
     * @return the callid
     */
    public String getCallid() {
        return callid;
    }

    /**
     * @param callid the callid to set
     */
    public void setCallid(String callid) {
        this.callid = callid;
    }

    /**
     * @return the attendants
     */
    public String getAttendants() {
        return attendants;
    }

    /**
     * @param attendants the attendants to set
     */
    public void setAttendants(String attendants) {
        this.attendants = attendants;
    }

    /**
     * @return the starttime
     */
    public String getStarttime() {
        return starttime;
    }

    /**
     * @param starttime the starttime to set
     */
    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

      
}
