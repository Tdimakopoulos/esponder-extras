/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.net.voip;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author MEOC-WS1
 */
public class voipcalls {
    
    public void DeleteCall(int ii,String sz1) throws Exception
    {
     if (sz1.contains("MEOC"))
         if (sz1.contains("FRC"))
         {MeocToFrcCall(ii);}
     if (sz1.contains("FRC"))
         if (sz1.contains("MEOC"))
         {MeocToFrcCall(ii);}
     
     if (sz1.contains("MEOC"))
         if (sz1.contains("EOC"))
         {EocToMeocCall(ii);}
     if (sz1.contains("EOC"))
         if (sz1.contains("MEOC"))
         {EocToMeocCall(ii);}
    }
    public void EocToMeocCall(int ii) throws Exception {

		String url = "http://147.102.25.221:8080/esponder-comm-manager/rest/"+ii;
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("DELETE");
		con.setRequestProperty("Content-Type", "application/json");

		String urlParameters = "{\"eoc\":\"eoc1\",\"ian\":[{\"meoc2eoc\":\"meoc1\",\"frt\":null,\"meoc\":null}]}";
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println(response.toString());
	}
    
    public void MeocToFrcCall(int ii) throws Exception {

		String url = "http://147.102.25.221:8080/esponder-comm-manager/rest/"+ii;
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("DELETE");
		con.setRequestProperty("Content-Type", "application/json");

		String urlParameters = "{\"eoc\":null,\"ian\":[{\"meoc2eoc\":null,\"frt\":[{\"frc\": \"frc1\", \"fru\": null}],\"meoc\":[\"meoc1\", \"meoc2\"]}]}"; 
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println(response.toString());
	}
}
