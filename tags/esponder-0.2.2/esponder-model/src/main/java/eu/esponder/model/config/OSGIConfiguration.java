package eu.esponder.model.config;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("OSGI")
public class OSGIConfiguration extends ESponderConfigParameter {

	private static final long serialVersionUID = 1472802157259993847L;
	
}
