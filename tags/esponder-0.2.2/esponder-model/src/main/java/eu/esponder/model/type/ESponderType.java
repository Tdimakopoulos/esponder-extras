package eu.esponder.model.type;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;

@Entity
@Table(name="entity_type")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="DISCRIMINATOR")
@NamedQueries({
	@NamedQuery(name="ESponderType.findByTitle", query="select t from ESponderType t where t.title=:title")
})
public abstract class ESponderType extends ESponderEntity<Long> {
	
	private static final long serialVersionUID = 5092746393389044558L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ENTITY_TYPE_ID")
	protected Long id;
	
	/**
	 * TODO: Rename the "title" field as "description" 
	 */
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	protected String title;
	
	@ManyToOne
	@JoinColumn(name="PARENT_ID")
	protected ESponderType parent;
	
	@OneToMany(mappedBy="parent")
	protected Set<ESponderType> children;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ESponderType getParent() {
		return parent;
	}

	public void setParent(ESponderType parent) {
		this.parent = parent;
	}

	public Set<ESponderType> getChildren() {
		return children;
	}

	public void setChildren(Set<ESponderType> children) {
		this.children = children;
	}
	
	@Override
	public int hashCode() {
	    final int PRIME = 31;
	    int result = 1;
	    result = PRIME * result + id.intValue();
	    return result;
	}
	 
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    final ESponderType other = (ESponderType) obj;
	    if (id != other.id) {
	        return false;
	    }
	    if (!this.getTitle().equals(other.getTitle())) {
	    	return false;
	    } if (!this.getParent().equals(other.getParent())) {
	    	return false;
	    }
	    return true;
	}
	
}

