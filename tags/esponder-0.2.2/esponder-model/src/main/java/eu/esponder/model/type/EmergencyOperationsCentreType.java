package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("EOC")
public final class EmergencyOperationsCentreType extends OperationsCentreType {

	private static final long serialVersionUID = 4216857910645487846L;

	
}
