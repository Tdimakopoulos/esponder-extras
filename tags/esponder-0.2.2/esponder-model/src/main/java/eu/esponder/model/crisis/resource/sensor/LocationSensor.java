package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("LOCATION")
public class LocationSensor extends Sensor implements LocationMeasurementSensor {

	private static final long serialVersionUID = 3111283126456291581L;

}
