package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("STR_ACTOR")
public final class StrategicActorType extends ActorType {

	private static final long serialVersionUID = -1734043841975502816L;

}
