package eu.esponder.model.snapshot.status;

public enum MeasurementStatisticTypeEnum {

	MEAN,
	STDEV,

	AUTOCORRELATION,

	MAXIMUM,
	MINIMUM,

	FIRST,
	LAST,
	MEDIAN,
	
	OLDEST,
	NEWEST
	
}
