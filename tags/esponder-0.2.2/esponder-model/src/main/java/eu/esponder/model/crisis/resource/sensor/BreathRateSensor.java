package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("BIOMED_BREATH_RATE")
public class BreathRateSensor extends BiomedicalSensor implements ArithmeticMeasurementSensor {

	private static final long serialVersionUID = 5211712220701237629L;

}
