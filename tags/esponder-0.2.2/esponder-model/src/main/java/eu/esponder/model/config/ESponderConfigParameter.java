package eu.esponder.model.config;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.Identifiable;

@Entity
@Table(name="configuration")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="DISCRIMINATOR")
@NamedQueries({
	@NamedQuery(name="ESponderConfigParameter.findByName", query="select t from ESponderConfigParameter t where t.parameterName=:parameterName")
})public abstract class ESponderConfigParameter implements Identifiable<Long> {
	
	private static final long serialVersionUID = 5619634457839448917L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CONFIGURATION_ID")
	private Long id;
	
	@Column(name="PARAMETER_NAME", nullable=false, unique=true, length=255)
	private String parameterName;
	
	@Column(name="PARAMETER_VALUE", nullable=false, length=255)
	private String parameterValue;
	
	@Column(name="DESCRIPTION")
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getParameterValue() {
		return parameterValue;
	}

	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
		
}
