package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("BIOMED")
public abstract class BiomedicalSensor extends Sensor {

	private static final long serialVersionUID = 6993139817263562576L;

}
