package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateActorEvent extends ActorEvent<ActorDTO> implements UpdateEvent {

	private static final long serialVersionUID = -9062099773119274785L;
	
}
