package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.event.model.CreateEvent;


public class CreateEquipmentSnapshotEvent extends EquipmentSnapshotEvent<EquipmentSnapshotDTO> implements CreateEvent {

	private static final long serialVersionUID = 6363754485823085786L;
	
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
