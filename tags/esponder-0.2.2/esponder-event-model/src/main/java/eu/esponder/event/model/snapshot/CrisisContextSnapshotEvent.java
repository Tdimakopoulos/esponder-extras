package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;


public class CrisisContextSnapshotEvent<T extends CrisisContextSnapshotDTO> extends SnapshotEvent<T> {

	private static final long serialVersionUID = 5971268269850771662L;

	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
