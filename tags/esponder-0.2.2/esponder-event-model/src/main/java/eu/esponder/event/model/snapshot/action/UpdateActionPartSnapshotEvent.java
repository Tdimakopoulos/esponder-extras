package eu.esponder.event.model.snapshot.action;

import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateActionPartSnapshotEvent extends ActionPartSnapshotEvent<ActionPartSnapshotDTO> implements UpdateEvent {

	private static final long serialVersionUID = 6948219527460346540L;
	
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
