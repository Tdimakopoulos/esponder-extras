package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.event.model.DeleteEvent;


public class DeleteResourceEquipmentEvent extends ResourceEquipmentEvent<EquipmentDTO> implements DeleteEvent {

	private static final long serialVersionUID = -8961408917425131483L;
	
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
