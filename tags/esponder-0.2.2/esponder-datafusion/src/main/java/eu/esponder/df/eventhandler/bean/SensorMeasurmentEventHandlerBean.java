package eu.esponder.df.eventhandler.bean;

import eu.esponder.df.eventhandler.SensorMeasurmentEventHandlerRemoteService;
import eu.esponder.df.eventhandler.SensorMeasurmentEventHandlerService;
import eu.esponder.df.rules.profile.ProfileManager;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.event.model.ESponderEvent;

public class SensorMeasurmentEventHandlerBean extends dfEventHandlerBean
		implements SensorMeasurmentEventHandlerService,
		SensorMeasurmentEventHandlerRemoteService {

	public void ProcessEvent(ESponderEvent<?> pEvent)
	{
		ProfileManager pProfileManager=new ProfileManager();
		
		SensorSnapshotDTO pSensorSnapshot=CreateSensorSnapshot(pEvent);
		
		//Set rule profile for sensor measurement
		SetRuleEngineType(pProfileManager.GetProfileNameForSensor().getSzProfileType(),pProfileManager.GetProfileNameForSensor().getSzProfileName());

		//Load Rules
		LoadKnowledge();
		//Add Measurements object list
		AddDTOObjects(pSensorSnapshot);
		//Run Rules
		ProcessRules();
	}
}
