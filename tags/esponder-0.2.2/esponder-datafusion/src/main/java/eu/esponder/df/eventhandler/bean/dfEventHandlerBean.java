package eu.esponder.df.eventhandler.bean;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.df.eventhandler.dfEventHandlerRemoteService;
import eu.esponder.df.eventhandler.dfEventHandlerService;
import eu.esponder.df.ruleengine.core.DomainSpecificKnowledgeParser;
import eu.esponder.df.ruleengine.core.RuleEngineGuvnorAssets;
import eu.esponder.df.ruleengine.utilities.RuleStatistics;
import eu.esponder.df.ruleengine.utilities.RuleUtilities;
import eu.esponder.df.ruleengine.utilities.events.RuleEvents;
import eu.esponder.df.ruleengine.utilities.ruleresults.RuleResults;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.dto.model.snapshot.status.ActorSnapshotStatusDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.test.ResourceLocator;


@Stateless
public class dfEventHandlerBean implements dfEventHandlerRemoteService, dfEventHandlerService {

	private String szFlowName=null;
	private String szRulePackageName=null;
	private String szDSLName=null;
	@SuppressWarnings("unused")
	private String szDSLRName=null;
	private RuleEngineType dInferenceType=RuleEngineType.NONE;

	ActorRemoteService actorService = ResourceLocator.lookup("esponder/ActorBean/remote");
	EquipmentRemoteService equipmentService = ResourceLocator.lookup("esponder/EquipmentBean/remote");
	SensorRemoteService sensorService = ResourceLocator.lookup("esponder/SensorBean/remote");

	public enum RuleEngineType {
		NONE,DRL_RULES, DSL_RULES, FLOW_RULES, ALL_PACKAGE, MIX
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Rule Engine Calls and Variables
	private RuleEngineGuvnorAssets dEngine = new RuleEngineGuvnorAssets();
	private DomainSpecificKnowledgeParser ddsl = new DomainSpecificKnowledgeParser();

	public void AddDTOObjects(Object fact) {
		dEngine.InferenceEngineAddDTOObject(fact);
		ddsl.AddDTOObject(fact);
	}

	public void AddObjects(Object fact) {
		dEngine.InferenceEngineAddObject(fact);
		ddsl.AddObject(fact);
	}

	private void AddKnowledgeByCategory(String szCategory) {
		try {
			dEngine.InferenceEngineAddKnowledgeForCategory(szCategory);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Category");
			e.printStackTrace();
		}
		ddsl.AddKnowledgeForCategory(szCategory);
	}

	private void AddKnowledgeByPackage(String szPackage) {
		try {
			dEngine.InferenceEngineAddKnowledgeForPackage(szPackage);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Package");
			e.printStackTrace();
		}
		ddsl.AddKnowledgeForPackage(szPackage);
	}

	private void ExecuteRules() {
		// run simple drl rules
		dEngine.InferenceEngineRunAssets();

	}

	private void ExecuteFlow() {
		// run all processes
		ddsl.ExecuteFlow();

		// run dslr rules based on dsl and
		ddsl.RunRules();

		// close session only if all processes has finished
		ddsl.Dispose();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void LoadKnowledge()
	{
		if(this.dInferenceType==RuleEngineType.ALL_PACKAGE)
		{
			AddKnowledgeByPackage(szRulePackageName);
		}

		if(this.dInferenceType==RuleEngineType.DRL_RULES)
		{
			AddKnowledgeByCategory(szRulePackageName);
		}

		if(this.dInferenceType==RuleEngineType.FLOW_RULES)
		{
			AddKnowledgeByCategory(szFlowName);
		}

		if(this.dInferenceType==RuleEngineType.DSL_RULES)
		{
			AddKnowledgeByCategory(szDSLName);
		}


	}

	public void ProcessRules()
	{
		////////////////////////////////////////////////////////////////////////
		//Load Default Objects into drools
		// RuleUtils
		// MathUtils
		// EventUtils
		// RuleResultsUtils
		RuleStatistics pStatistics = new RuleStatistics();
		RuleUtilities pUtils = new RuleUtilities();
		RuleResults pResults = new RuleResults();
		RuleEvents pEvents = new RuleEvents();

		AddObjects(pStatistics);
		AddObjects(pUtils);
		AddObjects(pResults);
		AddObjects(pEvents);
		////////////////////////////////////////////////////////////////////////

		//Execute Rules using package, DRL,DSL,DSLR,Flow
		if (this.dInferenceType==RuleEngineType.ALL_PACKAGE)
		{
			ExecuteFlow();
			ExecuteRules();
		}

		//Execute Rules using Mix, DRL,DSL,DSLR,Flow
		if (this.dInferenceType==RuleEngineType.MIX)
		{
			ExecuteFlow();
			ExecuteRules();
		}

		//Execute Rules using  DRL
		if (this.dInferenceType==RuleEngineType.DRL_RULES)
		{
			ExecuteRules();
		}

		//Execute Rules using  DSL
		if (this.dInferenceType==RuleEngineType.DSL_RULES)
		{
			ExecuteFlow();
		}

		//Execute Rules using  Flow or/and DRSR
		if (this.dInferenceType==RuleEngineType.FLOW_RULES)
		{
			ExecuteFlow();
		}
	}

	/*	// What kind of event type ??
	public void PublishEvent()
	{

	}

	 */

	//TODO Check Snapshot Transformation Status (working or not)
	private List<Object> createSnapshotsFromEnvelope(SensorMeasurementStatisticEnvelopeDTO envelope, Long userID) {
		if(sensorService != null) {
			List<Object> createdSnapshots = new ArrayList<Object>();
			for(SensorMeasurementStatisticDTO statistic : envelope.getMeasurementStatistics()) {
				if(statistic.getStatistic() instanceof ArithmeticSensorMeasurementDTO) {
					SensorSnapshotDTO snapshotDTO = transformArithmeticMeasurementToSnapshot(statistic);
					if(snapshotDTO != null){
						snapshotDTO = sensorService.createSensorSnapshotRemote(snapshotDTO, userID);
						if(snapshotDTO !=null)
							createdSnapshots.add(snapshotDTO);
					}
					else
						System.out.println("ArithmeticSensorSnapshot has not been persisted, empty snapshot found...");
				}
				else if (statistic.getStatistic() instanceof LocationSensorMeasurementDTO) {
					ActorSnapshotDTO actorSnapshotDTO = transformLocationMeasurementToSnapshot(statistic);
					if(actorSnapshotDTO != null) {
						actorSnapshotDTO = actorService.createActorSnapshotRemote(actorSnapshotDTO, userID);
						if(actorSnapshotDTO !=null)
							createdSnapshots.add(actorSnapshotDTO);
					}
					else
						System.out.println("SensorSnapshot has not been persisted, empty snapshot found...");
				}
			}
			return createdSnapshots;
		}
		else {
			System.out.println("Cannot access SensorRemoteService...");
			return null;
		}

	}

	private SensorSnapshotDTO transformArithmeticMeasurementToSnapshot(SensorMeasurementStatisticDTO statistic) {
		ArithmeticSensorMeasurementDTO arithmeticMeasurementDTO = (ArithmeticSensorMeasurementDTO) statistic.getStatistic();
		SensorSnapshotDTO snapshot = new SensorSnapshotDTO();
		snapshot.setPeriod(statistic.getPeriod());
		snapshot.setSensor(statistic.getStatistic().getSensor());
		snapshot.setStatisticType(statistic.getStatisticType());
		//TODO 	Decide whether each working snapshot automatically takes a "WORKING" status or
		//		it is derived from the (missing yet) sensor status
		snapshot.setStatus(SensorSnapshotStatusDTO.WORKING);
		snapshot.setValue(arithmeticMeasurementDTO.getMeasurement().toString());
		return snapshot;
	}

	private ActorSnapshotDTO transformLocationMeasurementToSnapshot(SensorMeasurementStatisticDTO statistic) {
		LocationSensorMeasurementDTO locationMeasurementDTO = (LocationSensorMeasurementDTO) statistic.getStatistic();
		ActorSnapshotDTO snapshot = new ActorSnapshotDTO();
		snapshot.setPeriod(statistic.getPeriod());
		snapshot.setLocationArea(new SphereDTO(locationMeasurementDTO.getPoint(), new BigDecimal(0), new Date().toString()));
		//TODO 	Decide whether each working snapshot automatically takes a "WORKING" status or
		//		it is derived from the (missing yet) sensor status
		ActorDTO actor = equipmentService.findByIdRemote(statistic.getStatistic().getSensor().getEquipmentId()).getActor();
		snapshot.setActor(actor);
		snapshot.setStatus(ActorSnapshotStatusDTO.ACTIVE);
		return snapshot;
	}

	public SensorSnapshotDTO CreateSensorSnapshot(ESponderEvent<?> pEvent)
	{
		//		System.out.println("CSSE # " + event.toString());
		//		System.out.println("CSSE attachment # " + event.getEventAttachment().toString());
		//		System.out.println("CSSE severity # " + event.getEventSeverity());
		//		System.out.println("CSSE source # " + event.getEventSource());
		//		System.out.println("CSSE timestamp# " + event.getEventTimestamp());

		//createSnapshotsFromEnvelope
		//ObjectMapper mapper = new ObjectMapper();
		//mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		SensorMeasurementStatisticEnvelopeDTO pobject= (SensorMeasurementStatisticEnvelopeDTO)pEvent.getEventAttachment();
		System.out.println("==========> ID : "+pobject.getId());
		System.out.println("==========> Size of Measurments : "+pobject.getMeasurementStatistics().size());
		createSnapshotsFromEnvelope(pobject,new Long(1));
		return null;
	}



	public void SetRuleEngineType(RuleEngineType dType,String szPackageNameOrFlowName)
	{
		SetRuleType(dType,szPackageNameOrFlowName,null,null);
	}

	public void SetRuleEngineType(RuleEngineType dType,String szDSLName,String szDSLRName)
	{
		SetRuleType(dType,null,szDSLName,szDSLRName);
	}

	private void SetRuleType(RuleEngineType dType,String szPackageNameOrFlowName,String szDSLNamep,String szDSLRNamep)
	{
		if (dType==RuleEngineType.DRL_RULES)
		{
			szRulePackageName=szPackageNameOrFlowName;
			SetInferenceType(dType);
		}
		if (dType==RuleEngineType.ALL_PACKAGE)
		{
			szRulePackageName=szPackageNameOrFlowName;
			dInferenceType=RuleEngineType.ALL_PACKAGE;
		}
		if (dType==RuleEngineType.FLOW_RULES)
		{
			szFlowName=szPackageNameOrFlowName;
			SetInferenceType(dType);
		}
		if(dType==RuleEngineType.DSL_RULES)
		{
			szDSLName=szDSLNamep;
			szDSLRName=szDSLRNamep;
			SetInferenceType(dType);
		}
	}

	private void SetInferenceType(RuleEngineType dType)
	{
		if (dInferenceType==RuleEngineType.ALL_PACKAGE)
		{}else{
			if (dInferenceType==RuleEngineType.NONE)
				dInferenceType=dType;
			else
				dInferenceType=RuleEngineType.MIX;
		}
	}
}

