/**
 * RuleEngineMessage
 * 
 * This Java class implement a simple rule message. Only type and text is supported. Using this class we can pass
 * a fact type of this.type and message value type of this.msgtext to rule engine.
 * 
 * 
 * @Project   esponder
 * @package   Datafusion
*/
package eu.esponder.df.ruleengine.core;

/**
 * @author tdim
 *
 */
public class RuleEngineMessage {
	private String type;

	private String msgtext;

	public String getType() {

		return type;

	}

	public void setType(String type) {

		this.type = type;

	}

	public String getMsgtext() {

		return msgtext;

	}

	public void setMsgtext(String msgtext) {

		this.msgtext = msgtext;

	}
}
