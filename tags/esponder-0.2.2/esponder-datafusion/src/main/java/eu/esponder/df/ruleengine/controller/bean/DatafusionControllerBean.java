package eu.esponder.df.ruleengine.controller.bean;

import org.codehaus.jackson.map.ObjectMapper;
//ObjectMapper mapper = new ObjectMapper();
//CrisisContextDTO crisisContextPersisted = mapper.readValue(crisisResult, CrisisContextDTO.class);


import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.df.eventhandler.bean.ActionEventHandlerBean;
import eu.esponder.df.eventhandler.bean.SensorMeasurmentEventHandlerBean;
import eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService;
import eu.esponder.df.ruleengine.controller.DatafusionControllerService;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.test.ResourceLocator;



public class DatafusionControllerBean implements DatafusionControllerRemoteService,DatafusionControllerService {

	private String szName;

	private String szEventTypeSensorMeasurment="CreateSensorMeasurementStatisticEvent";
	private String szEventTypeAction="CreateActionEvent";

	@Override
	public void EsponderEventReceivedHandler(ESponderEvent<?> pEvent) {
		System.out.println("########### DF EVENT Handler - EVENT RECEIVED #############");
		printEvent(pEvent);
		szName = pEvent.getClass().getSimpleName();

		System.out.println("=====> Event Type (CreateSensorMeasurementStatisticEvent) DF : --- "+szName);

		if (szName.compareTo(szEventTypeSensorMeasurment)==0)
		{
			SensorMeasurmentEventHandlerBean pHandler = new SensorMeasurmentEventHandlerBean();
			pHandler.ProcessEvent(pEvent);
		}

		if (szName.compareTo(szEventTypeAction)==0)
		{
			ActionEventHandlerBean pHandler = new ActionEventHandlerBean();
			pHandler.ProcessEvent(pEvent);
		}
	}

	private void printEvent(ESponderEvent<? extends ESponderEntityDTO> event) {
		System.out.println("########### DF EVENT Handler - EVENT RECEIVED #############");
		System.out.println("########### EVENT DETAILS START #############");
		System.out.println("CSSE # " + event.toString());
		System.out.println("CSSE attachment # " + event.getEventAttachment().toString());
		System.out.println("CSSE severity # " + event.getEventSeverity());
		System.out.println("CSSE source # " + event.getEventSource());
		System.out.println("CSSE timestamp# " + event.getEventTimestamp());
		System.out.println("########### EVENT DETAILS END #############");

	}
	

	// functions for enveloped -> snapshots moved to dfEventHandlerBean.java


}
