package eu.esponder.controller.crisis.resource;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.ReusableResource;

@Local
public interface LogisticsService extends LogisticsRemoteService {
	
	public ReusableResource findReusableResourceById(Long reusableID);

	public ReusableResource findReusableResourceByTitle(String title);
	
	public ReusableResource createReusableResource(ReusableResource reusableResource,Long userID);
	
	public ReusableResource updateReusableResource(ReusableResource reusableResource, Long userID);
	
	public void deleteReusableResource(Long reusableResourceID, Long userID);
	
	public ConsumableResource findConsumableResourceById(Long consumableID);

	public ConsumableResource findConsumableResourceByTitle(String title);

	public ConsumableResource createConsumableResource(ConsumableResource consumableResource, Long userID);

	public ConsumableResource updateConsumableResource(ConsumableResource consumableResource, Long userID);
	
	public void deleteConsumableResource(Long consumableResourceID, Long userID);
	
}
