package eu.esponder.controller.crisis.resource.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.LogisticsRemoteService;
import eu.esponder.controller.crisis.resource.LogisticsService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.LogisticsResource;
import eu.esponder.model.crisis.resource.ReusableResource;
import eu.esponder.model.type.ConsumableResourceType;

@Stateless
public class LogisticsBean implements LogisticsService, LogisticsRemoteService {

	@EJB
	private CrudService<LogisticsResource> logisticsCrudService;

	@EJB
	private CrudService<ConsumableResource> consumablesCrudService;

	@EJB
	private CrudService<ReusableResource> reusablesCrudService;

	@EJB
	private TypeService typeService;

	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	public LogisticsResource findLogisticsById(Long logisticsID) {
		return (LogisticsResource) logisticsCrudService.find(LogisticsResource.class, logisticsID);
	}

	//-------------------------------------------------------------------------

	@Override
	public ReusableResourceDTO findReusableResourceByIdRemote(Long reusableID) {
		ReusableResource reusableResource = findReusableResourceById(reusableID);
		ReusableResourceDTO reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	@Override
	public ReusableResource findReusableResourceById(Long reusableID) {
		return (ReusableResource) reusablesCrudService.find(ReusableResource.class, reusableID);
	}

	//-------------------------------------------------------------------------
	@Override
	public ConsumableResourceDTO findConsumableResourceByIdRemote(Long consumableID) {
		ConsumableResource consumableResource = findConsumableResourceById(consumableID);
		ConsumableResourceDTO consumableResourceDTO = (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ConsumableResourceDTO.class);
		return consumableResourceDTO;
	}

	@Override
	public ConsumableResource findConsumableResourceById(Long consumableID) {
		return (ConsumableResource) consumablesCrudService.find(ConsumableResource.class, consumableID);
	}

	//-------------------------------------------------------------------------

	@Override
	public ReusableResourceDTO findReusableResourceByTitleRemote(String title) {
		ReusableResource reusableResource = findReusableResourceByTitle(title);
		ReusableResourceDTO reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	@Override
	public ReusableResource findReusableResourceByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ReusableResource) reusablesCrudService.findSingleWithNamedQuery("ReusableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@Override
	public ConsumableResourceDTO findConsumableResourceByTitleRemote(String title) {
		ConsumableResource consumableResource = findConsumableResourceByTitle(title);
		return (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ConsumableResourceDTO.class);
	}

	@Override
	public ConsumableResource findConsumableResourceByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ConsumableResource) consumablesCrudService.findSingleWithNamedQuery("ConsumableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@Override
	public ConsumableResourceDTO createConsumableResourceRemote( ConsumableResourceDTO consumableResourceDTO, Long userID) {
		ConsumableResource consumableResource = (ConsumableResource) mappingService.mapESponderEntityDTO(consumableResourceDTO, ConsumableResource.class);
		consumableResource = createConsumableResource(consumableResource, userID);
		consumableResourceDTO = (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ConsumableResourceDTO.class);
		return consumableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ConsumableResource createConsumableResource(ConsumableResource consumableResource, Long userID) {
		consumableResource.getConsumableResourceCategory().setConsumableResourceType((ConsumableResourceType) typeService.findById(consumableResource.getConsumableResourceCategory().getConsumableResourceType().getId()));
		if(null != consumableResource.getContainer())
		{
			consumableResource.setContainer(this.findReusableResourceById(consumableResource.getContainer().getId()));
		}
		consumablesCrudService.create(consumableResource);
		return consumableResource;
	}

	//-------------------------------------------------------------------------

	@Override
	public ReusableResourceDTO createReusableResourceRemote(ReusableResourceDTO reusableResourceDTO, Long userID) {
		ReusableResource reusableResource = (ReusableResource) mappingService.mapESponderEntityDTO(reusableResourceDTO, ReusableResource.class);
		reusableResource = createReusableResource(reusableResource, userID);
		reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReusableResource createReusableResource(ReusableResource reusableResource, Long userID) {
		reusablesCrudService.create(reusableResource);
		return reusableResource;
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteConsumableResourceRemote(Long reusableResourceDTOID, Long userID) {
		deleteConsumableResource(reusableResourceDTOID, userID);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteConsumableResource(Long consumableResourceID, Long userID) {
		consumablesCrudService.delete(ConsumableResource.class, consumableResourceID);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteReusableResourceRemote(Long reusableResourceDTOID, Long userID) {
		deleteReusableResource(reusableResourceDTOID, userID);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteReusableResource(Long reusableResourceID, Long userID) {
		reusablesCrudService.delete(ReusableResource.class, reusableResourceID);
	}

	//-------------------------------------------------------------------------

	@Override
	public ReusableResourceDTO updateReusableResourceDTO(ReusableResourceDTO reusableResourceDTO, Long userID) {
		ReusableResource reusableResource = (ReusableResource) mappingService.mapESponderEntityDTO(reusableResourceDTO, ReusableResource.class);
		reusableResource = updateReusableResource(reusableResource, userID);
		reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReusableResource updateReusableResource(ReusableResource reusableResource, Long userID) {
		return (ReusableResource) reusablesCrudService.update(reusableResource);
	}

	//-------------------------------------------------------------------------

	@Override
	public ConsumableResourceDTO updateConsumableResourceRemote(ConsumableResourceDTO consumableResourceDTO, Long userID) {
		ConsumableResource consumableResource = (ConsumableResource) mappingService.mapESponderEntityDTO(consumableResourceDTO, ConsumableResource.class);
		consumableResource = updateConsumableResource(consumableResource, userID);
		consumableResourceDTO = (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ReusableResourceDTO.class);
		return consumableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ConsumableResource updateConsumableResource(ConsumableResource consumableResource, Long userID) {
		return (ConsumableResource) consumablesCrudService.update(consumableResource);
	}

}
