package eu.esponder.controller.generic;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.criteria.EsponderQueryRestrictionDTO;

@Remote
public interface GenericRemoteService {

	public List<? extends ESponderEntityDTO> getDTOEntities(
			String queriedDTOClassName,EsponderQueryRestrictionDTO criteriaDTO, int pageSize, int pageNumber) 
			throws InstantiationException, IllegalAccessException, Exception;
	
	public ESponderEntityDTO createEntityRemote(ESponderEntityDTO entity, Long userID) throws ClassNotFoundException;
	
	public ESponderEntityDTO updateEntityRemote(ESponderEntityDTO entityDTO, Long userID) throws ClassNotFoundException;
	
	public ESponderEntityDTO getEntityDTO( Class<? extends ESponderEntityDTO> clz, Long objectID) throws ClassNotFoundException;
	
	public void deleteEntityRemote(Class<? extends ESponderEntityDTO> clz, Long entityID) throws ClassNotFoundException;
	
}