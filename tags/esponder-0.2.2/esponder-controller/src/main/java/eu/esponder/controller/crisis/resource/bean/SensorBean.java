package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.ESponderResourceService;
import eu.esponder.controller.crisis.resource.EquipmentService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.resource.SensorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.snapshot.sensor.config.StatisticsConfig;
import eu.esponder.model.type.SensorType;

@Stateless
public class SensorBean implements SensorService, SensorRemoteService {

	@EJB
	private CrudService<Sensor> sensorCrudService;

	@EJB
	private CrudService<SensorSnapshot> sensorSnapshotCrudService;

	@EJB
	private CrudService<StatisticsConfig> statisticsConfigCrudService;

	@EJB
	private TypeService typeService;

	@EJB
	private EquipmentService equipmentService;
	
	@EJB
	private ESponderMappingService eSponderMappingService;
	
	@EJB
	private ESponderResourceService resourceService;

	//-------------------------------------------------------------------------

	@Override
	public SensorDTO findSensorByIdRemote(Long sensorId) {
		Sensor sensor = findSensorById(sensorId);
		if(sensor != null) {
			try {
				return (SensorDTO) eSponderMappingService.mapESponderEntity(sensor, eSponderMappingService.getDTOEntityClass(sensor.getClass()));
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}	
		}
		else
			return null;
	}
	
	@Override
	public Sensor findSensorById(Long sensorID) {
		return (Sensor) resourceService.findByID(Sensor.class, sensorID);
	}

	//-------------------------------------------------------------------------

	@Override
	public StatisticsConfigDTO findConfigByIdRemote(Long configID) {
		StatisticsConfig statisticsConfig = findConfigById(configID);
		if(statisticsConfig != null)
			return (StatisticsConfigDTO) eSponderMappingService.mapESponderEntity(statisticsConfig, StatisticsConfigDTO.class);
		else
			return null;
	}
	
	@Override
	public StatisticsConfig findConfigById(Long configID) {
		return (StatisticsConfig) statisticsConfigCrudService.find(StatisticsConfig.class, configID);
	}

	//-------------------------------------------------------------------------

	@Override
	public SensorDTO findSensorByTitleRemote(String title) {
		Sensor sensor = findSensorByTitle(title);
		if(sensor != null) {
			try {
				return (SensorDTO) eSponderMappingService.mapESponderEntity(sensor, eSponderMappingService.getDTOEntityClass(sensor.getClass()));
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}	
		}
		else
			return null;
	}
	
	@Override
	public Sensor findSensorByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Sensor) sensorCrudService.findSingleWithNamedQuery("Sensor.findByTitle", params);
	}	

	//-------------------------------------------------------------------------

	@Override
	public SensorSnapshotDTO findSensorSnapshotByIdRemote(Long sensorID) {
		SensorSnapshot  sensorSnapshot = findSensorSnapshotById(sensorID);
		if(sensorSnapshot != null) {
			return (SensorSnapshotDTO) eSponderMappingService.mapESponderEntity(sensorSnapshot, SensorSnapshotDTO.class);
		}
		return null;
	}
	
	@Override
	public SensorSnapshot findSensorSnapshotById(Long sensorID) {
		return (SensorSnapshot) sensorSnapshotCrudService.find(SensorSnapshot.class, sensorID);
	}

	//-------------------------------------------------------------------------

	@Override
	public SensorSnapshotDTO findSensorSnapshotByDateRemote(Long sensorID, Date dateTo) {
		SensorSnapshot  sensorSnapshot = findSensorSnapshotByDate(sensorID, dateTo);
		if(sensorSnapshot != null) {
			return (SensorSnapshotDTO) eSponderMappingService.mapESponderEntity(sensorSnapshot, SensorSnapshotDTO.class);
		}
		return null;
		
	}
	
	@Override
	public SensorSnapshot findSensorSnapshotByDate(Long sensorID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sensorID", sensorID);
		params.put("maxDate", maxDate);
		SensorSnapshot snapshot =
				(SensorSnapshot) sensorSnapshotCrudService.findSingleWithNamedQuery("SensorSnapshot.findBySensorAndDate", params);
		return snapshot;
	}
	
	//-------------------------------------------------------------------------

	@Override
	public SensorSnapshotDTO findPreviousSensorSnapshotRemote(Long sensorID) {
		SensorSnapshot  sensorSnapshot = findPreviousSensorSnapshot(sensorID);
		if(sensorSnapshot != null) {
			return (SensorSnapshotDTO) eSponderMappingService.mapESponderEntity(sensorSnapshot, SensorSnapshotDTO.class);
		}
		return null;
	}
	
	@Override
	public SensorSnapshot findPreviousSensorSnapshot(Long sensorID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sensorID", sensorID);
		SensorSnapshot snapshot = (SensorSnapshot) sensorSnapshotCrudService.findSingleWithNamedQuery("SensorSnapshot.findPreviousSnapshot", params);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	@Override
	public SensorDTO createSensorRemote(SensorDTO sensorDTO, Long userID) {
		try {
			Sensor sensor = (Sensor) eSponderMappingService.mapESponderEntityDTO(sensorDTO, eSponderMappingService.getManagedEntityClass(sensorDTO.getClass()));
			this.createSensor(sensor, userID);
			sensorDTO = (SensorDTO) eSponderMappingService.mapESponderEntity(sensor, eSponderMappingService.getDTOEntityClass(sensor.getClass()));
			return sensorDTO;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}	
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Sensor createSensor(Sensor sensor, Long userID) {
		SensorType sensorType = (SensorType)typeService.findById(sensor.getSensorType().getId());
		Equipment equipment = equipmentService.findById(sensor.getEquipment().getId());
		sensor.setSensorType(sensorType);
		sensor.setEquipment(equipment);
		sensor = sensorCrudService.create(sensor);
		return sensor;
	}

	//-------------------------------------------------------------------------

	@Override
	public StatisticsConfigDTO createStatisticConfigRemote(StatisticsConfigDTO statisticConfigDTO, Long userID) {
		try {
			StatisticsConfig statisticConfig = (StatisticsConfig) eSponderMappingService.mapESponderEntityDTO(statisticConfigDTO, eSponderMappingService.getManagedEntityClass(statisticConfigDTO.getClass()));
			statisticConfig = this.createStatisticConfig(statisticConfig, userID);
			statisticConfigDTO = (StatisticsConfigDTO) eSponderMappingService.mapESponderEntity(statisticConfig, eSponderMappingService.getDTOEntityClass(statisticConfig.getClass()));
			return statisticConfigDTO;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public StatisticsConfig createStatisticConfig(StatisticsConfig statisticConfig, Long userID) {
		statisticConfig.setSensor(findSensorById(statisticConfig.getSensor().getId()));
		statisticsConfigCrudService.create(statisticConfig);
		return statisticConfig;
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteSensorRemote(Long sensorId, Long userID) {
		deleteSensor(sensorId, userID);
	}
	
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteSensor(Long sensorId, Long userID) {
		Sensor sensor = findSensorById(sensorId);
		if(sensor != null) {
			sensorCrudService.delete(Sensor.class, sensorId);
		}
		else
			throw new RuntimeException();
		
	}

	//-------------------------------------------------------------------------

	@Override
	public SensorDTO updateSensorRemote(SensorDTO sensorDTO, Long userID) throws ClassNotFoundException {
		Sensor sensor = (Sensor) eSponderMappingService.mapESponderEntityDTO(sensorDTO, Sensor.class);
		sensor = updateSensor(sensor, userID);
		sensorDTO = (SensorDTO) eSponderMappingService.mapESponderEntity(sensor, SensorDTO.class);
		return sensorDTO;
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Sensor updateSensor(Sensor sensor, Long userID) {
		return (Sensor) sensorCrudService.update(sensor);
	}

	//-------------------------------------------------------------------------

	@Override
	public SensorSnapshotDTO createSensorSnapshotRemote(SensorSnapshotDTO snapshotDTO, Long userID) {
		try {
			SensorSnapshot snapshot = (SensorSnapshot) eSponderMappingService.mapESponderEntityDTO(snapshotDTO, eSponderMappingService.getManagedEntityClass(snapshotDTO.getClass()));
			this.createSensorSnapshot(snapshot, userID);
			snapshotDTO = (SensorSnapshotDTO) eSponderMappingService.mapESponderEntity(snapshot, eSponderMappingService.getDTOEntityClass(snapshot.getClass()));
			return snapshotDTO;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SensorSnapshot createSensorSnapshot(SensorSnapshot snapshot, Long userID) {
		snapshot.setSensor((Sensor)this.findSensorById(snapshot.getSensor().getId()));
		SensorSnapshot previous = this.findPreviousSensorSnapshot(snapshot.getSensor().getId());
		if (previous!=null)
			snapshot.setPrevious(previous);
		snapshot = (SensorSnapshot) sensorSnapshotCrudService.create(snapshot);
//		if (previous!=null)
//			sensorSnapshotCrudService.update(previous);
		return snapshot;
	}
	
	//-------------------------------------------------------------------------
		
	@Override
	public SensorSnapshotDTO updateSensorSnapshotRemote( SensorSnapshotDTO sensorSnapshotDTO, Long userID) {
		SensorSnapshot sensorSnapshot = (SensorSnapshot) eSponderMappingService.mapESponderEntityDTO(sensorSnapshotDTO, SensorSnapshot.class);
		sensorSnapshot = updateSensorSnapshot(sensorSnapshot, userID);
		return (SensorSnapshotDTO) eSponderMappingService.mapESponderEntity(sensorSnapshot, SensorSnapshotDTO.class); 
	}
		
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SensorSnapshot updateSensorSnapshot(SensorSnapshot snapshot, Long userID) {
		return (SensorSnapshot) sensorSnapshotCrudService.update(snapshot); 
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public void deleteSensorSnapshotRemote(Long sensorSnapshotId, Long userID) {
		deleteSensorSnapshot(sensorSnapshotId, userID);
		
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteSensorSnapshot(Long sensorSnapshotId, Long userID) {
		SensorSnapshot sensorSnapshot = findSensorSnapshotById(sensorSnapshotId);
		if(sensorSnapshot != null) {
			sensorSnapshotCrudService.delete(SensorSnapshot.class, sensorSnapshotId);
		}
		else
			throw new RuntimeException();
	}

	//-------------------------------------------------------------------------
	
	@Override
	public StatisticsConfigDTO updateStatisticConfigRemote(StatisticsConfigDTO statisticsConfigDTO, Long userID) {
		StatisticsConfig statisticsConfig = (StatisticsConfig) eSponderMappingService.mapESponderEntityDTO(statisticsConfigDTO, StatisticsConfig.class);
		statisticsConfig = updateStatisticConfig(statisticsConfig, userID);
		return (StatisticsConfigDTO) eSponderMappingService.mapESponderEntity(statisticsConfig, StatisticsConfigDTO.class);
	}

	@Override
	public StatisticsConfig updateStatisticConfig(StatisticsConfig statisticConfig, Long userID) {
		return statisticsConfigCrudService.update(statisticConfig);
	}

	//-------------------------------------------------------------------------
	
	@Override
	public void deleteStatisticConfigRemote(Long statisticsConfigId, Long userID) {
		deleteStatisticConfig(statisticsConfigId, userID);
	}

	@Override
	public void deleteStatisticConfig(Long statisticsConfigId, Long userID) {
		StatisticsConfig statisticsConfig= findConfigById(statisticsConfigId);
		if(statisticsConfig != null) {
			statisticsConfigCrudService.delete(StatisticsConfig.class, statisticsConfigId);
		}
		else
			throw new RuntimeException();
	}
	
	@SuppressWarnings("unused")
	private SensorSnapshot placeSensorSnapshot(SensorSnapshot sensorSnapshot, Long userID) {
		
		SensorSnapshot sensorSnapshotCreated = null;
		SensorSnapshot currentPreviousSnapshot= null;
		SensorSnapshot currentSnapshot= null;
		SensorSnapshot latestSensorSnapshot = this.findPreviousSensorSnapshot(sensorSnapshot.getSensor().getId());
		if(latestSensorSnapshot == null) {
			sensorSnapshotCreated = createSensorSnapshot(sensorSnapshot, userID);
			return sensorSnapshotCreated;
		}else {
			// Perform date test for latest sensor snapshot
			// if latest is older, set previous and create sensor snapshot
			if(latestSensorSnapshot.getPeriod().getDateFrom().before(sensorSnapshot.getPeriod().getDateFrom())) {
				sensorSnapshot.setPrevious(latestSensorSnapshot);
				sensorSnapshotCreated = createSensorSnapshot(sensorSnapshot, userID);
				return sensorSnapshotCreated;
			}
			// if latest is not older, then this incoming sensor snapshot is older and
			// rearrangement of snapshots has to occur...
			else {
				// Case when only one snapshot is persisted already and no more
				if(latestSensorSnapshot.getPrevious() == null) {
					sensorSnapshotCreated = createSensorSnapshot(sensorSnapshot, userID);
					latestSensorSnapshot.setPrevious(sensorSnapshotCreated);
					updateSensorSnapshot(latestSensorSnapshot, userID);
					return sensorSnapshotCreated;
				}
				// Case when more than one snapshot is persisted
				else {
					
					
				}
				
				
				return null;
			}
			
		}
		
		
		
	}
	
}
