package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.crisis.resource.OperationsCentreService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.snapshot.resource.ActorSnapshot;
import eu.esponder.model.type.ActorType;

@Stateless
public class ActorBean implements ActorService, ActorRemoteService {

	@EJB
	private CrudService<Actor> actorCrudService;

	@EJB
	private CrudService<ActorSnapshot> actorSnapshotCrudService;

	@EJB
	private ESponderMappingService mappingService;
	
	@EJB
	private OperationsCentreService operationsCentreService;
	
	@EJB
	private TypeService typeService;

	//-------------------------------------------------------------------------

	@Override
	public ActorDTO findByIdRemote(Long actorID) {
		return (ActorDTO) mappingService.mapESponderEntity(findById(actorID), ActorDTO.class);
	}

	@Override
	public Actor findById(Long actorID) {
		return (Actor) actorCrudService.find(Actor.class, actorID);
	}

	//-------------------------------------------------------------------------

	@Override
	public ActorDTO findByTitleRemote(String title) {
		return (ActorDTO) mappingService.mapESponderEntity(findByTitle(title), ActorDTO.class);
	}

	@Override
	public Actor findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Actor) actorCrudService.findSingleWithNamedQuery("Actor.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	@Override
	public List<ActorDTO> findSubordinatesByIdRemote(Long actorID) {
		return (List<ActorDTO>) mappingService.mapESponderEntity(findSubordinatesById(actorID), ActorDTO.class);
	}

	@Override
	public List<Actor> findSubordinatesById(Long actorID) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("actorID", actorID);
		return  actorCrudService.findWithNamedQuery("Actor.findSubordinatesForActor", parameters); 
	}

	//-------------------------------------------------------------------------

	@Override
	public ActorSnapshotDTO findActorSnapshotByDateRemote(Long actorID, Date maxDate) {
		return (ActorSnapshotDTO) mappingService.mapESponderEntity(findActorSnapshotByDate(actorID, maxDate), ActorSnapshotDTO.class);
	}

	@Override
	public ActorSnapshot findActorSnapshotByDate(Long actorID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("actorID", actorID);
		params.put("maxDate", maxDate);
		ActorSnapshot snapshot =
				(ActorSnapshot) actorSnapshotCrudService.findSingleWithNamedQuery("ActorSnapshot.findByActorAndDate", params);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorDTO createActorRemote(ActorDTO actorDTO, Long userID) {
		Actor actor = (Actor) mappingService.mapESponderEntityDTO(actorDTO, Actor.class);
		return (ActorDTO) mappingService.mapESponderEntity(createActor(actor, userID), ActorDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Actor createActor(Actor actor, Long userID) {
		actor.setActorType((ActorType)typeService.findById(actor.getActorType().getId()));
		if (null != actor.getSupervisor()) {
			actor.setSupervisor(this.findById(actor.getSupervisor().getId()));
		}
		if (null != actor.getOperationsCentre()) {
			actor.setOperationsCentre(operationsCentreService.findOperationCentreById(actor.getOperationsCentre().getId()));
		}
		return actorCrudService.create(actor);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteActorRemote(Long actorID, Long userID) {
		this.deleteActor(actorID, userID);
	}

	@Override
	public void deleteActor(Long actorID, Long userID) {
		actorCrudService.delete(Actor.class, actorID);
	}

	//-------------------------------------------------------------------------

	@Override
	public ActorDTO updateActorRemote(ActorDTO actorDTO, Long userID) {
		Actor actor = (Actor) mappingService.mapESponderEntityDTO(actorDTO, Actor.class);
		return (ActorDTO) mappingService.mapESponderEntity(updateActor(actor, userID), ActorDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Actor updateActor(Actor actor, Long userID) {
		return (Actor) actorCrudService.update(actor);
	}

	//-------------------------------------------------------------------------		

	@Override
	public ActorSnapshotDTO createActorSnapshotRemote(ActorSnapshotDTO snapshotDTO, Long userID) {
		ActorSnapshot actorSnapshot = (ActorSnapshot) mappingService.mapESponderEntityDTO(snapshotDTO, ActorSnapshot.class);
		return (ActorSnapshotDTO) mappingService.mapESponderEntity(createActorSnapshot(actorSnapshot, userID), ActorSnapshotDTO.class);
	}		

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorSnapshot createActorSnapshot(ActorSnapshot snapshot, Long userID) {
		Actor actor = findById(snapshot.getActor().getId());
		if(actor != null) {
			snapshot.setActor(actor);
			snapshot = actorSnapshotCrudService.create(snapshot);
			return snapshot;
		}
		return null;
	}

	//-------------------------------------------------------------------------

	@Override
	public ActorSnapshotDTO findActorSnapshotByIdRemote(Long actorID) {
		ActorSnapshot actorSnapshot = findActorSnapshotById(actorID);
		ActorSnapshotDTO actorSnapshotDTO = (ActorSnapshotDTO) mappingService.mapESponderEntity(actorSnapshot, ActorSnapshotDTO.class);
		return actorSnapshotDTO;
	}

	@Override
	public ActorSnapshot findActorSnapshotById(Long actorID) {
		ActorSnapshot actorSnapshot = (ActorSnapshot) actorSnapshotCrudService.find(ActorSnapshot.class, actorID);
		return actorSnapshot;
	}
	
	//-------------------------------------------------------------------------

	@Override
	public ActorSnapshotDTO updateActorSnapshotRemote(ActorSnapshotDTO actorSnapshotDTO, Long userID) {
		ActorSnapshot actorSnapshot = (ActorSnapshot) mappingService.mapESponderEntityDTO(actorSnapshotDTO, ActorSnapshot.class);
		return (ActorSnapshotDTO) mappingService.mapESponderEntity(updateActorSnapshot(actorSnapshot, userID), ActorSnapshotDTO.class);
	}

	@Override
	public ActorSnapshot updateActorSnapshot(ActorSnapshot actorSnapshot, Long userID) {
		return (ActorSnapshot) actorSnapshotCrudService.update(actorSnapshot);
	}

	//-------------------------------------------------------------------------
	
	@Override
	public void deleteActorSnapshotRemote(Long actorSnapshotDTOID, Long userID) {
		deleteActorSnapshot(actorSnapshotDTOID, userID);
	}

	@Override
	public void deleteActorSnapshot(Long actorSnapshotID, Long userID) {
		actorSnapshotCrudService.delete(ActorSnapshot.class, userID);
		
	}
	
}
