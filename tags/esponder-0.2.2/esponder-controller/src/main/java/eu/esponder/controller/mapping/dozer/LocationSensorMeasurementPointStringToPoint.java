package eu.esponder.controller.mapping.dozer;

import java.math.BigDecimal;

import org.dozer.CustomConverter;

import eu.esponder.dto.model.snapshot.location.PointDTO;

public class LocationSensorMeasurementPointStringToPoint implements CustomConverter {

//	@Test
//	public static void main() {
//		
//		String pointString = "Point [latitude=23.876398, longitude=34.269357, altitude=0]";
//		int equalsIndex =1 + pointString.indexOf("=");
//		int equalsIndex2 =pointString.indexOf(",", equalsIndex);
//		String latitude = pointString.substring(equalsIndex, equalsIndex2);
//		
//		int equalsIndex3 =pointString.indexOf("=", equalsIndex2) + 1;
//		int equalsIndex4 =pointString.indexOf(",", equalsIndex3);
//		
//		String longitude = pointString.substring(equalsIndex3, equalsIndex4);
//		
//		
//		int equalsIndex5 =pointString.indexOf("=", equalsIndex4) + 1;
//		int equalsIndex6 =pointString.indexOf("]", equalsIndex5);
//		
//		String altitude = pointString.substring(equalsIndex5, equalsIndex6);
//		
//		System.out.println("latitude = "+latitude+", longtude = "+longitude+", altitude = "+altitude);
//		
//	}
	
	
	@Override
	public Object convert(Object destination,
			Object source, Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == String.class) {
			if(source.toString().contains("Point")) {
				
				String pointString = (String) source;
				int equalsIndex =1 + pointString.indexOf("=");
				int equalsIndex2 =pointString.indexOf(",", equalsIndex);
				String latitude = pointString.substring(equalsIndex, equalsIndex2);
				
				int equalsIndex3 =pointString.indexOf("=", equalsIndex2) + 1;
				int equalsIndex4 =pointString.indexOf(",", equalsIndex3);
				
				String longitude = pointString.substring(equalsIndex3, equalsIndex4);
				
				
				int equalsIndex5 =pointString.indexOf("=", equalsIndex4) + 1;
				int equalsIndex6 =pointString.indexOf("]", equalsIndex5);
				
				String altitude = pointString.substring(equalsIndex5, equalsIndex6);
				
				PointDTO point = new PointDTO();
				point.setLatitude(new BigDecimal(latitude));
				point.setLongitude(new BigDecimal(longitude));
				point.setAltitude(new BigDecimal(altitude));
				
				destination = (PointDTO) point;
				
//				System.out.println("latitude = "+latitude+", longtude = "+longitude+", altitude = "+altitude);
				
			}
			else
				destination = new BigDecimal(source.toString());
		}
		else if(sourceClass == BigDecimal.class) {
			
		}
			
		
		return destination;
	}

}
