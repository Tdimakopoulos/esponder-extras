package eu.esponder.controller.crisis.user.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.crisis.user.UserService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.model.user.ESponderUser;

@Stateless
public class UserBean implements UserService, UserRemoteService {
	
	@EJB
	private CrudService<ESponderUser> userCrudService;
	
	@EJB
	private ESponderMappingService mappingService;
	
	
	//-------------------------------------------------------------------------
	
	@Override
	public ESponderUserDTO findUserByIdRemote(Long userID) {
		ESponderUser user = findUserById(userID);
		if(user != null)
			return (ESponderUserDTO) mappingService.mapESponderEntity(user, ESponderUserDTO.class);
		else
			return null;
	}
	
	@Override
	public ESponderUser findUserById(Long userID) {
		return (ESponderUser) userCrudService.find(ESponderUser.class, userID);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public ESponderUserDTO findUserByNameRemote(String userName) {
		ESponderUser user = findUserByName(userName);
		if(user != null)
			return (ESponderUserDTO) mappingService.mapESponderEntity(user, ESponderUserDTO.class);
		else
			return null;
	}
	
	@Override
	public ESponderUser findUserByName(String userName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userName", userName);
		return (ESponderUser) userCrudService.findSingleWithNamedQuery("ESponderUser.findByUserName", params);
	}
	
	//-------------------------------------------------------------------------

	@Override
	public ESponderUserDTO createUserRemote(ESponderUserDTO userDTO, Long userID) {
		ESponderUser user = (ESponderUser) mappingService.mapESponderEntityDTO(userDTO, ESponderUser.class);
		user = createUser(user, userID);
		return (ESponderUserDTO) mappingService.mapESponderEntity(user, ESponderUserDTO.class);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderUser createUser(ESponderUser user, Long userID) {
		userCrudService.create(user);
		return user;
	}
	
	//-------------------------------------------------------------------------

	@Override
	public ESponderUserDTO updateUserRemote(ESponderUserDTO userDTO, Long userID) {
		ESponderUser user = (ESponderUser) mappingService.mapESponderEntityDTO(userDTO, ESponderUser.class);
		user = updateUser(user, userID);
		return (ESponderUserDTO) mappingService.mapESponderEntity(user, ESponderUserDTO.class);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderUser updateUser(ESponderUser user, Long userID) {
		ESponderUser userPersisted = findUserById(user.getId());
		mappingService.mapEntityToEntity(user, userPersisted);
		return (ESponderUser) userCrudService.update(userPersisted);
	}

}
