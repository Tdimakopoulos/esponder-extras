package eu.esponder.controller.persistence.criteria;

import java.util.Collection;

public class EsponderUnionCriteriaCollection extends EsponderCriteriaCollection {

	private static final long serialVersionUID = 7938678292927863036L;

	public EsponderUnionCriteriaCollection() {}

	public EsponderUnionCriteriaCollection(
			Collection<EsponderQueryRestriction> restrictions) {
		super(restrictions);
	}

}
