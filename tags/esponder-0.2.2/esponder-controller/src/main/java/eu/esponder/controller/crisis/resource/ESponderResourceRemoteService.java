package eu.esponder.controller.crisis.resource;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;

@Remote
public interface ESponderResourceRemoteService {

	public ResourceDTO findDTOById(Class<? extends ResourceDTO> clz, Long id);

	public ResourceDTO findDTOByTitle(Class<? extends ResourceDTO> clz, String title);
}
