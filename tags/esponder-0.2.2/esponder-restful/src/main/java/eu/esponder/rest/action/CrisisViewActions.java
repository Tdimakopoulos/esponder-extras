package eu.esponder.rest.action;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;
import eu.esponder.rest.ESponderResource;

@Path("/crisis/view")
public class CrisisViewActions extends ESponderResource {

	
	@GET
	@Path("/crisisContext/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextDTO readCrisisContextById(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextId may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		CrisisContextDTO crisisContextDTO = this.getActionRemoteService().findCrisisContextDTOById(crisisContextID);
		return crisisContextDTO;
	}
	
	@GET
	@Path("/crisisContextSnapshot/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextSnapshotDTO readCrisisContextSnapshotById(
			@QueryParam("crisisContextSnapshotID") @NotNull(message="crisisContextSnapshotId may not be null") Long crisisContextSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		CrisisContextSnapshotDTO crisisContextSnapshotDTO = this.getActionRemoteService().findCrisisContextSnapshotDTOById(crisisContextSnapshotID);
		return crisisContextSnapshotDTO;
	}
	
	@GET
	@Path("/action/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionDTO readActionById(
			@QueryParam("actionID") @NotNull(message="actionId may not be null") Long actionID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionDTO actionDTO = this.getActionRemoteService().findActionDTOById(actionID);
		return actionDTO;
	}
	
	@GET
	@Path("/actionSnapshot/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionSnapshotDTO readActionSnapshotById(
			@QueryParam("actionSnapshotID") @NotNull(message="actionSnapshotId may not be null") Long actionSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionSnapshotDTO actionSnapshotDTO = this.getActionRemoteService().findActionSnapshotDTOById(actionSnapshotID);
		return actionSnapshotDTO;
	}
	
	@GET
	@Path("/actionObjective/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionObjectiveDTO readActionObjectiveById(
			@QueryParam("actionObjectiveID") @NotNull(message="actionObjectiveId may not be null") Long actionObjectiveID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionObjectiveDTO actionObjectiveDTO = this.getActionRemoteService().findActionObjectiveDTOById(actionObjectiveID);
		return actionObjectiveDTO;
	}
	
	@GET
	@Path("/actionPart/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartDTO readActionPartById(
			@QueryParam("actionPartID") @NotNull(message="actionPartId may not be null") Long actionPartID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionPartDTO actionPartDTO = this.getActionRemoteService().findActionPartDTOById(actionPartID);
		return actionPartDTO;
	}
	
	@GET
	@Path("/actionPartSnapshot/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartSnapshotDTO readActionPartSnapshotById(
			@QueryParam("actionPartSnapshotID") @NotNull(message="actionPartSnapshotId may not be null") Long actionPartSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionPartSnapshotDTO actionPartSnapshotDTO = this.getActionRemoteService().findActionPartSnapshotDTOById(actionPartSnapshotID);
		return actionPartSnapshotDTO;
	}
	
	@GET
	@Path("/actionPartObjective/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartObjectiveDTO readActionPartObjectiveById(
			@QueryParam("actionPartObjectiveID") @NotNull(message="actionPartObjectiveId may not be null") Long actionPartObjectiveID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionPartObjectiveDTO actionPartObjectiveDTO = this.getActionRemoteService().findActionPartObjectiveDTOById(actionPartObjectiveID);
		return actionPartObjectiveDTO;
	}
	
	
	@GET
	@Path("/crisisContext/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextDTO readCrisisContextByTitle(
			@QueryParam("crisisContextTitle") @NotNull(message="crisisContextId may not be null") String crisisContextTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		CrisisContextDTO crisisContextDTO = this.getActionRemoteService().findCrisisContextDTOByTitle(crisisContextTitle);
		return crisisContextDTO;
	}
	
	@GET
	@Path("/action/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionDTO readActionByTitle(
			@QueryParam("actionTitle") @NotNull(message="actionTitle may not be null") String actionTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionDTO actionDTO = this.getActionRemoteService().findActionDTOByTitle(actionTitle);
		return actionDTO;
	}
	
	@GET
	@Path("/actionObjective/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionObjectiveDTO readActionObjectiveByTitle(
			@QueryParam("actionObjectiveTitle") @NotNull(message="actionObjectiveTitle may not be null") String actionObjectiveTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionObjectiveDTO actionObjectiveDTO = this.getActionRemoteService().findActionObjectiveDTOByTitle(actionObjectiveTitle);
		return actionObjectiveDTO;
	}
	
	@GET
	@Path("/actionPart/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartDTO readActionPartByTitle(
			@QueryParam("actionPartTitle") @NotNull(message="actionPartTitle may not be null") String actionPartTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionPartDTO actionPartDTO = this.getActionRemoteService().findActionPartDTOByTitle(actionPartTitle);
		return actionPartDTO;
	}
	
	@GET
	@Path("/actionPartObjective/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartObjectiveDTO readActionPartObjectiveByTitle(
			@QueryParam("actionPartObjectiveTitle") @NotNull(message="actionPartObjectiveTitle may not be null") String actionPartObjectiveTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionPartObjectiveDTO actionPartObjectiveDTO = this.getActionRemoteService().findActionPartObjectiveDTOByTitle(actionPartObjectiveTitle);
		return actionPartObjectiveDTO;
	}
	
	@POST
	@Path("/crisisContext/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextDTO createCrisisContext(
			@NotNull(message="crisisContext object may not be null") CrisisContextDTO crisisContextDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		CrisisContextDTO crisisContextDTOPersisted = this.getActionRemoteService().createCrisisContextRemote(crisisContextDTO, userID);
		
		CreateCrisisContextEvent crisisContextEvent = new CreateCrisisContextEvent();
		crisisContextEvent.setEventAttachment(crisisContextDTOPersisted);
		crisisContextEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
		crisisContextEvent.setEventTimestamp(new Date());
		crisisContextEvent.setJournalMessage(crisisContextEvent.getJournalMessageInfo());
		ActorDTO subActorDTO = this.getActorRemoteService().findByIdRemote(userID);
		crisisContextEvent.setEventSource(subActorDTO);
		
		ESponderEventPublisher<CreateCrisisContextEvent> publisher = new ESponderEventPublisher<CreateCrisisContextEvent>(CreateCrisisContextEvent.class);
		try {
			publisher.publishEvent(crisisContextEvent);
		} catch (EventListenerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return crisisContextDTOPersisted;
	}
	
	@POST
	@Path("/crisisContextSnapshot/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextSnapshotDTO createCrisisContextSnapshot(
			@NotNull(message="crisisContextSnapshot object may not be null") CrisisContextSnapshotDTO crisisContextSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		CrisisContextDTO crisisContextPersisted = this.getActionRemoteService().findCrisisContextDTOById(crisisContextSnapshotDTO.getCrisisContext().getId());
		if(crisisContextPersisted != null) {
			CrisisContextSnapshotDTO crisisContextSnapshotDTOPersisted = this.getActionRemoteService().createCrisisContextSnapshotRemote(crisisContextSnapshotDTO, userID);
			return crisisContextSnapshotDTOPersisted;
		}
		else
			return null;
	}
	
	@POST
	@Path("/action/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionDTO createAction(
			@NotNull(message="Action object may not be null") ActionDTO actionDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionDTO actionDTOPersisted = this.getActionRemoteService().createActionRemote(actionDTO, userID);
		return actionDTOPersisted;
	}
	
	@POST
	@Path("/actionSnapshot/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionSnapshotDTO createActionSnapshot(
			@NotNull(message="ActionSnapshot object may not be null") ActionSnapshotDTO actionSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionSnapshotDTO actionSnapshotDTOPersisted = this.getActionRemoteService().createActionSnapshotRemote(actionSnapshotDTO, userID);
		return actionSnapshotDTOPersisted;
	}
	
	@POST
	@Path("/actionObjective/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionObjectiveDTO createActionObjective(
			@NotNull(message="ActionObjective object may not be null") ActionObjectiveDTO actionObjectiveDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionObjectiveDTO actionObjectiveDTOPersisted = this.getActionRemoteService().createActionObjectiveRemote(actionObjectiveDTO, userID);
		return actionObjectiveDTOPersisted;
	}
	
	@POST
	@Path("/actionPart/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartDTO createActionPart(
			@NotNull(message="ActionPart object may not be null") ActionPartDTO actionPartDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionPartDTO actionPartDTOPersisted = this.getActionRemoteService().createActionPartRemote(actionPartDTO, userID);
		return actionPartDTOPersisted;
	}
	
	@POST
	@Path("/actionPartSnapshot/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartSnapshotDTO createActionPartSnapshot(
			@NotNull(message="ActionPartSnapshot object may not be null") ActionPartSnapshotDTO actionPartSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionPartSnapshotDTO actionPartSnapshotDTOPersisted = this.getActionRemoteService().createActionPartSnapshotRemote(actionPartSnapshotDTO, userID);
		return actionPartSnapshotDTOPersisted;
	}
	
	@POST
	@Path("/actionPartObjective/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartObjectiveDTO createActionPartObjective(
			@NotNull(message="ActionPartObjective object may not be null") ActionPartObjectiveDTO actionPartObjectiveDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionPartObjectiveDTO actionPartObjectiveDTOPersisted = this.getActionRemoteService().createActionPartObjectiveRemote(actionPartObjectiveDTO, userID);
		return actionPartObjectiveDTOPersisted;
	}
	
	@PUT
	@Path("/crisisContext/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextDTO updateCrisisContext(
			@NotNull(message="CrisisContext object may not be null") CrisisContextDTO crisisContextDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		CrisisContextDTO crisisContextDTOUpdated = this.getActionRemoteService().updateCrisisContextRemote(crisisContextDTO, userID);
		return crisisContextDTOUpdated;
	}
	
	@PUT
	@Path("/crisisContextSnapshot/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextSnapshotDTO updateCrisisContextSnapshot(
			@NotNull(message="CrisisContextSnapshot object may not be null") CrisisContextSnapshotDTO crisisContextSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		CrisisContextSnapshotDTO crisisContextSnapshotDTOUpdated = this.getActionRemoteService().updateCrisisContextSnapshotRemote(crisisContextSnapshotDTO, userID);
		return crisisContextSnapshotDTOUpdated;
	}
	
	@PUT
	@Path("/action/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionDTO updateAction(
			@NotNull(message="Action object may not be null") ActionDTO actionDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionDTO actionDTOUpdated = this.getActionRemoteService().updateActionRemote(actionDTO, userID);
		return actionDTOUpdated;
	}
	
	@PUT
	@Path("/actionSnapshot/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionSnapshotDTO updateActionSnapshot(
			@NotNull(message="ActionSnapshot object may not be null") ActionSnapshotDTO actionSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionSnapshotDTO actionSnapshotDTOUpdated = this.getActionRemoteService().updateActionSnapshotRemote(actionSnapshotDTO, userID);
		return actionSnapshotDTOUpdated;
	}
	
	@PUT
	@Path("/actionObjective/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionObjectiveDTO updateActionObjective(
			@NotNull(message="ActionObjective object may not be null") ActionObjectiveDTO actionObjectiveDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionObjectiveDTO actionObjectiveDTOUpdated = this.getActionRemoteService().updateActionObjectiveRemote(actionObjectiveDTO, userID);
		return actionObjectiveDTOUpdated;
	}
	
	@PUT
	@Path("/actionPart/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartDTO updateActionPart(
			@NotNull(message="ActionPart object may not be null") ActionPartDTO actionPartDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionPartDTO actionPartDTOUpdated = this.getActionRemoteService().updateActionPartRemote(actionPartDTO, userID);
		return actionPartDTOUpdated;
	}
	
	@PUT
	@Path("/actionPartSnapshot/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartSnapshotDTO updateActionPartSnapshot(
			@NotNull(message="ActionPartSnapshot object may not be null") ActionPartSnapshotDTO actionPartSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionPartSnapshotDTO actionPartSnapshotDTOUpdated = this.getActionRemoteService().updateActionPartSnapshotRemote(actionPartSnapshotDTO, userID);
		return actionPartSnapshotDTOUpdated;
	}
	
	@PUT
	@Path("/actionPartObjective/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartObjectiveDTO updateActionPartObjective(
			@NotNull(message="ActionPartObjective object may not be null") ActionPartObjectiveDTO actionPartObjectiveDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ActionPartObjectiveDTO actionPartObjectiveDTOUpdated = this.getActionRemoteService().updateActionPartObjectiveRemote(actionPartObjectiveDTO, userID);
		return actionPartObjectiveDTOUpdated;
	}
	
	@DELETE
	@Path("/crisisContext/delete")
	public void deleteCrisisContext(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextId may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getActionRemoteService().deleteCrisisContextRemote(crisisContextID, userID);
	}
	
	@DELETE
	@Path("/crisisContextSnapshot/delete")
	public void deleteCrisisContextSnapshot(
			@QueryParam("crisisContextSnapshotID") @NotNull(message="crisisContextSnapshotId may not be null") Long crisisContextSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getActionRemoteService().deleteCrisisContextSnapshotRemote(crisisContextSnapshotID, userID);
	}
	
	@DELETE
	@Path("/action/delete")
	public void deleteAction(
			@QueryParam("actionID") @NotNull(message="actionId may not be null") Long actionID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getActionRemoteService().deleteActionRemote(actionID, userID);
	}
	
	@DELETE
	@Path("/actionSnapshot/delete")
	public void deleteActionSnapshot(
			@QueryParam("actionSnapshotID") @NotNull(message="actionSnapshotId may not be null") Long actionSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getActionRemoteService().deleteActionRemote(actionSnapshotID, userID);
	}
	
	@DELETE
	@Path("/actionObjective/delete")
	public void deleteActionObjective(
			@QueryParam("actionObjectiveID") @NotNull(message="actionObjectiveId may not be null") Long actionObjectiveID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getActionRemoteService().deleteActionObjectiveRemote(actionObjectiveID, userID);
	}
	
	
	@DELETE
	@Path("/actionPart/delete")
	public void deleteActionPart(
			@QueryParam("actionPartID") @NotNull(message="actionPartId may not be null") Long actionPartID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getActionRemoteService().deleteActionPartRemote(actionPartID, userID);
	}
	
	@DELETE
	@Path("/actionPartSnapshot/delete")
	public void deleteActionPartSnapshot(
			@QueryParam("actionPartSnapshotID") @NotNull(message="actionPartSnapshotId may not be null") Long actionPartSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getActionRemoteService().deleteActionPartRemote(actionPartSnapshotID, userID);
	}
	
	@DELETE
	@Path("/actionPartObjective/delete")
	public void deleteActionPartObjective(
			@QueryParam("actionPartObjectiveID") @NotNull(message="actionPartObjectiveId may not be null") Long actionPartObjectiveID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getActionRemoteService().deleteActionPartObjectiveRemote(actionPartObjectiveID, userID);
	}
}