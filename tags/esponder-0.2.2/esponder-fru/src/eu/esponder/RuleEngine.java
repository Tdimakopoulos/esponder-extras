package eu.esponder;

import java.math.BigDecimal;

public class RuleEngine {

	public static boolean process(BigDecimal value,String Sensor,String type){
		
		if(Sensor=="Temperature"){
			
			  if(type=="AVG"){
				  return  (Operator.GREATER_THAN.apply(value,Controls.MAX_AVG_TEMP));
			    }
			  if(type=="MAX"){
				  return  (Operator.GREATER_THAN.apply(value,Controls.MIN_MAX_TEMP));
			    }
			  if(type=="MIN"){
				  return  (Operator.EQUAL.apply(value,Controls.MAX_MIN_TEMP));
			    }
			
		}
		
		
		
		
		return false;
		
	}
}
