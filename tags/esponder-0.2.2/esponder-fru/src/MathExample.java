 
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math.stat.descriptive.SummaryStatistics;
import org.apache.commons.math.stat.descriptive.moment.GeometricMean;
import org.apache.commons.math.stat.descriptive.moment.Kurtosis;
import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.moment.Skewness;
import org.apache.commons.math.stat.descriptive.moment.Variance;
import org.apache.commons.math.stat.descriptive.rank.Max;
import org.apache.commons.math.stat.descriptive.rank.Min;
import org.apache.commons.math.stat.descriptive.rank.Percentile;
import org.apache.commons.math.stat.descriptive.summary.Sum;
import org.apache.commons.math.stat.descriptive.summary.SumOfSquares;

import eu.esponder.Operator;

public class MathExample{
	public static void main(String[] args){
		
		DescriptiveStatistics desc= new DescriptiveStatistics();
		desc.addValue(5.5);
		desc.addValue(3.4);
		desc.addValue(2.1);
		desc.addValue(3.3);
		desc.addValue(4.5);
		desc.addValue(2.2);
	System.out.println(desc.getMean());
	
	SummaryStatistics as= new SummaryStatistics();
	as.addValue(2.2);
	as.addValue(1.4);
	as.addValue(3.2);
	System.out.println(as.getMean());
	
	
	 BigDecimal b = new BigDecimal("6.5");
	 BigDecimal b2 = new BigDecimal("6.5");
	 
	 
	
	 boolean foo = (Operator.EQUAL.apply(b2,b));
	 System.out.println("Einai "+foo);
	 BigDecimal re= new BigDecimal( "80.34"); 
	 BigDecimal re2= new BigDecimal("4.111421341253252314321235645725432532"); 
	// BigDecimal newBD = bd.setScale(5, BigDecimal.ROUND_HALF_UP);
	 System.out.println( re.setScale(2, BigDecimal.ROUND_HALF_UP));
	  
	 try {
		 
		 Map<String,Map> sensor=new HashMap<String, Map>();
		 Map<String,Integer> temp_command=new HashMap<String, Integer>();
		 Map<String,Integer> carbon_command=new HashMap<String, Integer>();
		 
		 temp_command.put("AVG", 4 );
		 temp_command.put("MAX", 5 );
		 carbon_command.put("MAX", 5 );
		 
		 
		 sensor.put("Temp", temp_command );
		 sensor.put("Carbon", carbon_command ); 

			Iterator iter = sensor.entrySet().iterator();

			while (iter.hasNext()) {
				Map.Entry mEntry = (Map.Entry) iter.next();
				System.out.println(mEntry.getKey() + " : " + mEntry.getValue());
			}

			 

		} catch (Exception e) {
			System.out.println(e.toString());
		}
	 
	}
	
	 
}
