package eu.esponder.jaxb.model.crisis.action;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.snapshot.PeriodDTO;
import eu.esponder.jaxb.model.snapshot.location.LocationAreaDTO;


@XmlRootElement(name="actionObjective")
@XmlType(name="ActionObjective")
@JsonSerialize(include=Inclusion.NON_NULL)
public class ActionObjectiveDTO extends ESponderEntityDTO {

	private String title;
	
	private PeriodDTO period;
	
	private LocationAreaDTO locationArea;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}
	
}
