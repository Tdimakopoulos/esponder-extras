package eu.esponder.jaxb.model.crisis.view;

import javax.xml.bind.annotation.XmlSeeAlso;

import eu.esponder.jaxb.model.ESponderEntityDTO;


@XmlSeeAlso({
	ReferencePOIDTO.class,
	SketchPOIDTO.class
})
public abstract class ResourcePOIDTO extends ESponderEntityDTO {
	
	protected String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
