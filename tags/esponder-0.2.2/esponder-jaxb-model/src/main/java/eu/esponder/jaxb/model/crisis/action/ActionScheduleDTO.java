package eu.esponder.jaxb.model.crisis.action;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.ESponderEntityDTO;


@XmlRootElement(name="actionSchedule")
@XmlType(name="ActionSchedule")
@JsonSerialize(include=Inclusion.NON_NULL)
public class ActionScheduleDTO extends ESponderEntityDTO {

	private String title;
	
	private ActionDTO action;
	
	private ActionDTO prerequisiteAction;
	
	private ActionScheduleCriteriaDTO criteria;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ActionDTO getAction() {
		return action;
	}

	public void setAction(ActionDTO action) {
		this.action = action;
	}

	public ActionDTO getPrerequisiteAction() {
		return prerequisiteAction;
	}

	public void setPrerequisiteAction(ActionDTO prerequisiteAction) {
		this.prerequisiteAction = prerequisiteAction;
	}

	public ActionScheduleCriteriaDTO getCriteria() {
		return criteria;
	}

	public void setCriteria(ActionScheduleCriteriaDTO criteria) {
		this.criteria = criteria;
	}

}


