package eu.esponder.jaxb.model.snapshot.sensor.config;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;

@XmlRootElement(name="Statistics_Configuration")
@XmlType(name="StatisticsConfig")
@JsonSerialize(include=Inclusion.ALWAYS)
@JsonPropertyOrder({"resourceId", "measurementStatisticType", "samplingPeriodMilliseconds"})
public class StatisticsConfigDTO extends ESponderEntityDTO {
	
	private MeasurementStatisticTypeEnumDTO measurementStatisticType;
	
	private long samplingPeriodMilliseconds;
	
	public MeasurementStatisticTypeEnumDTO getMeasurementStatisticType() {
		return measurementStatisticType;
	}

	public void setMeasurementStatisticType(
			MeasurementStatisticTypeEnumDTO measurementStatisticType) {
		this.measurementStatisticType = measurementStatisticType;
	}

	public long getSamplingPeriodMilliseconds() {
		return samplingPeriodMilliseconds;
	}

	public void setSamplingPeriodMilliseconds(long samplingPeriodMilliseconds) {
		this.samplingPeriodMilliseconds = samplingPeriodMilliseconds;
	}
	
	public String toString() {
		return "[Type : " + measurementStatisticType + ", sampling Period (ms): " + samplingPeriodMilliseconds + " ]";
	}
	
}
