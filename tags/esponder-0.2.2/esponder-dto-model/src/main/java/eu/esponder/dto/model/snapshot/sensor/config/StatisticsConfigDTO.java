package eu.esponder.dto.model.snapshot.sensor.config;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "measurementStatisticType", "samplingPeriodMilliseconds"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class StatisticsConfigDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = -5641881403083027088L;

	public StatisticsConfigDTO() { }
	
	private MeasurementStatisticTypeEnumDTO measurementStatisticType;
	
	private long samplingPeriodMilliseconds;
	
	private Long sensorId;
	
	public MeasurementStatisticTypeEnumDTO getMeasurementStatisticType() {
		return measurementStatisticType;
	}

	public void setMeasurementStatisticType(
			MeasurementStatisticTypeEnumDTO measurementStatisticType) {
		this.measurementStatisticType = measurementStatisticType;
	}

	public long getSamplingPeriodMilliseconds() {
		return samplingPeriodMilliseconds;
	}

	public void setSamplingPeriodMilliseconds(long samplingPeriodMilliseconds) {
		this.samplingPeriodMilliseconds = samplingPeriodMilliseconds;
	}
	
	public String toString() {
		return "[Type : " + measurementStatisticType + ", sampling Period (ms): " + samplingPeriodMilliseconds + " ]";
	}

	public Long getSensorId() {
		return sensorId;
	}

	public void setSensorId(Long sensorId) {
		this.sensorId = sensorId;
	}
	
}
