package eu.esponder.dto.model.snapshot.sensor.measurement;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "measurements"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SensorMeasurementEnvelopeDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = -6429356749216017827L;

	private List<SensorMeasurementDTO> measurements;

	public List<SensorMeasurementDTO> getMeasurements() {
		return measurements;
	}

	public void setMeasurements(List<SensorMeasurementDTO> measurements) {
		this.measurements = measurements;
	}
	
	public void add(SensorMeasurementDTO measurement) {
		if (null == this.measurements) {
			this.measurements = new ArrayList<SensorMeasurementDTO>();
		}
		this.measurements.add(measurement);
	}
	
	public void clear() {
		this.measurements.clear();
	}
	
	
}
