package eu.esponder.dto.model.snapshot.resource;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;

@JsonPropertyOrder({"status", "value", "statisticType", "period", "sensor"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SensorSnapshotDTO extends SnapshotDTO {

	private static final long serialVersionUID = -6909950557250928422L;

	public SensorSnapshotDTO() { }
	
	private SensorSnapshotStatusDTO status;
	
	private MeasurementStatisticTypeEnumDTO statisticType;
	
	private String value;
	
	private SensorDTO sensor;

	public BigDecimal GetValueAsBigDecimal()
	{
		return new BigDecimal(this.getValue());
	}
	
	public int GetValueAsInt()
	{
		return Integer.valueOf(this.getValue());
	}
	
	public long GetValueAsLong()
	{
		return Long.valueOf(this.getValue());
	}
	
	public SensorSnapshotStatusDTO getStatus() {
		return status;
	}

	public SensorDTO getSensor() {
		return sensor;
	}

	public void setSensor(SensorDTO sensor) {
		this.sensor = sensor;
	}

	public void setStatus(SensorSnapshotStatusDTO status) {
		this.status = status;
	}

	public MeasurementStatisticTypeEnumDTO getStatisticType() {
		return statisticType;
	}

	public void setStatisticType(MeasurementStatisticTypeEnumDTO statisticType) {
		this.statisticType = statisticType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "SensorSnapshotDTO [status=" + status 
				+ ", statisticType=" + statisticType + ", value=" + value 
				+ ", period=" + period + ", id=" + id + "]";
	}
	
}
