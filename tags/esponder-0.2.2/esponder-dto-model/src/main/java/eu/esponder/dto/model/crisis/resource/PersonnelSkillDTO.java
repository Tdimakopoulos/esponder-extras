package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.type.PersonnelSkillTypeDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "type", "shortTitle", "personnelCategory", "personnelSkillType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PersonnelSkillDTO extends PersonnelCompetenceDTO {

	private static final long serialVersionUID = -4287360040986460222L;
	
	private PersonnelSkillTypeDTO personnelSkillType;

	public PersonnelSkillTypeDTO getPersonnelSkillType() {
		return personnelSkillType;
	}

	public void setPersonnelSkillType(PersonnelSkillTypeDTO personnelSkillType) {
		this.personnelSkillType = personnelSkillType;
	}
	
}