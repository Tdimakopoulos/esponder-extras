package eu.esponder.dto.model.crisis.resource.plan;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "plannableResources", "crisisType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CrisisResourcePlanDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = -2277820597919890333L;

	private Set<PlannableResourcePowerDTO> plannableResources;
	
	/**
	 * This field indicates either the CrisisDisasterType or the CrisisFeatureType for the plan.
	 * It is for further study whether the combination of them (i.e DisasterType and FeatureType) 
	 * should mandate that an additive exclusive result of the plannableResourcePower should be applied.
	 * 
	 */
	private CrisisTypeDTO crisisType;

	public Set<PlannableResourcePowerDTO> getPlannableResources() {
		return plannableResources;
	}

	public void setPlannableResources(
			Set<PlannableResourcePowerDTO> plannableResources) {
		this.plannableResources = plannableResources;
	}

	public CrisisTypeDTO getCrisisType() {
		return crisisType;
	}

	public void setCrisisType(CrisisTypeDTO crisisType) {
		this.crisisType = crisisType;
	}
	
}
