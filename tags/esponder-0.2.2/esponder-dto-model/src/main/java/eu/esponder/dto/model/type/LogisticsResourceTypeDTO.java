package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class LogisticsResourceTypeDTO extends ESponderTypeDTO {

	private static final long serialVersionUID = 9131656810548564674L;

}
