package eu.esponder.dto.model.crisis.view;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"protocol", "hostName", "path"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class HttpURLDTO extends URLDTO {

	private static final long serialVersionUID = 7908917079873002927L;

	public HttpURLDTO() {
		this.setProtocol("http");
	}
}
