package eu.esponder.dto.model.crisis.resource.category;

import org.codehaus.jackson.annotate.JsonTypeInfo;


@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class LogisticsCategoryDTO extends PlannableResourceCategoryDTO {

	private static final long serialVersionUID = 1077502768919499655L;


}