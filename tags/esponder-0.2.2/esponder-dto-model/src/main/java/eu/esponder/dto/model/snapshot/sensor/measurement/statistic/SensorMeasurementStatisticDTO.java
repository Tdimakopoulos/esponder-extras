package eu.esponder.dto.model.snapshot.sensor.measurement.statistic;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "samplingPeriod", "statistic", "period"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SensorMeasurementStatisticDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = -1389639486796682119L;

	private MeasurementStatisticTypeEnumDTO statisticType;
	
	/**
	 * Sampling period in msec
	 */
	private long samplingPeriod;
	
	private SensorMeasurementDTO statistic;
	
	private PeriodDTO period;
	
	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

	public long getSamplingPeriod() {
		return samplingPeriod;
	}

	public void setSamplingPeriod(long samplingPeriod) {
		this.samplingPeriod = samplingPeriod;
	}

	public SensorMeasurementDTO getStatistic() {
		return statistic;
	}

	public void setStatistic(SensorMeasurementDTO statistic) {
		this.statistic = statistic;
	}

	public MeasurementStatisticTypeEnumDTO getStatisticType() {
		return statisticType;
	}

	public void setStatisticType(MeasurementStatisticTypeEnumDTO statisticType) {
		this.statisticType = statisticType;
	}
	
}
