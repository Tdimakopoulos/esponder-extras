package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.crisis.resource.plan.PlannableResourceDTO;


@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class LogisticsResourceDTO extends PlannableResourceDTO {

	private static final long serialVersionUID = -2308376552416218285L;

}
