package eu.esponder.osgi.service.event;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.osgi.listener.thread.ESponderEventListenerServer;

@Stateless
public class EventServiceManagerBean implements EventServiceManager {

	private static Map<Class<? extends ESponderEvent<? extends ESponderEntityDTO>>, ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>>> publishers 
			= new HashMap<Class<? extends ESponderEvent<? extends ESponderEntityDTO>>, ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>>>();

	static {
		
		ESponderEventPublisher<CreateActionSnapshotEvent> publisher = new ESponderEventPublisher<CreateActionSnapshotEvent>(CreateActionSnapshotEvent.class);
		createPublisher(publisher);
		
		(new Thread(new ESponderEventListenerServer())).start();
		
	}

	public static void createPublisher(ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>> publisher) {
		publishers.put(publisher.getEventTopicClass(), publisher);
	}
	
	public ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>> getPublisher(Class<? extends ESponderEvent<? extends ESponderEntityDTO>> eventClass) {
		return publishers.get(eventClass);
	}
	
}
