package eu.esponder.osgi.dbmanager;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceUnit;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import eu.esponder.osgi.DB.OsgiEventsEntity;


public class OsgiEventsManager {

	
	
	
  	public OsgiEventsEntity persistEntity(OsgiEventsEntity entity) {
  		System.out.println("########### Persist #############");
  		try 
  		{
  		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory( "ESponderEvent-Persistence-Unit" );
  		EntityManager entityManager = entityManagerFactory.createEntityManager();
  		//entityManager.getTransaction().begin();
  		entityManager.persist( entity );
  		//entityManager.getTransaction().commit();
  		entityManager.close();
  		}
  		catch (Exception e)
  		{
  			System.out.println(" =====>  Exception : "+e.getMessage());
  			e.printStackTrace();
  		}
  		System.out.println("########### Persist OK !!! #############");
    	return entity;
	}
  	
  	
//	public OsgiEventsEntity SaveEvent(OsgiEventsEntity dEvent)
//	{
//		return persistEntity(dEvent);
//	}
//
//	public OsgiEventsEntity EditEvent(OsgiEventsEntity dEvent)
//	{
//		return mergeEntity(dEvent);
//	}
//	
//	public List<OsgiEventsEntity> findbyID() {
//		@SuppressWarnings("unchecked")
//		List<OsgiEventsEntity> resultList = em.createNamedQuery("OsgiEventsEntity.findByID").getResultList();
//		return resultList;
//	}
//	
//	public List<OsgiEventsEntity> findBySeverity() {
//		@SuppressWarnings("unchecked")
//		List<OsgiEventsEntity> resultList = em.createNamedQuery("OsgiEventsEntity.findBySeverity").getResultList();
//		return resultList;
//	}
//	
//	public List<OsgiEventsEntity> findBySourceid() {
//		@SuppressWarnings("unchecked")
//		List<OsgiEventsEntity> resultList = em.createNamedQuery("OsgiEventsEntity.findBySourceid").getResultList();
//		return resultList;
//	}
}
