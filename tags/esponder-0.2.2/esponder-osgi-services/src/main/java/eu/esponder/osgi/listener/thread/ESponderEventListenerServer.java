package eu.esponder.osgi.listener.thread;

import com.prosyst.mprm.common.ManagementException;
import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.crisis.action.CreateActionPartEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionPartSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.osgi.service.event.ESponderEventListener;

public class ESponderEventListenerServer implements Runnable {

	public void run() {
		try {

			ESponderEventListener<CreateActionSnapshotEvent> listener = new ESponderEventListener<CreateActionSnapshotEvent>(
					CreateActionSnapshotEvent.class);
			ESponderEventListener<CreateCrisisContextEvent> listener2 = new ESponderEventListener<CreateCrisisContextEvent>(
					CreateCrisisContextEvent.class);
			ESponderEventListener<CreateActionEvent> listener3 = new ESponderEventListener<CreateActionEvent>(
					CreateActionEvent.class);
			ESponderEventListener<CreateActionPartEvent> listener4 = new ESponderEventListener<CreateActionPartEvent>(
					CreateActionPartEvent.class);
			ESponderEventListener<CreateSensorMeasurementEnvelopeEvent> listener5 = new ESponderEventListener<CreateSensorMeasurementEnvelopeEvent>(
					CreateSensorMeasurementEnvelopeEvent.class);
			ESponderEventListener<UpdateActionSnapshotEvent> listener6 = new ESponderEventListener<UpdateActionSnapshotEvent>(
					UpdateActionSnapshotEvent.class);
			ESponderEventListener<UpdateActionPartSnapshotEvent> listener7 = new ESponderEventListener<UpdateActionPartSnapshotEvent>(
					UpdateActionPartSnapshotEvent.class);
			ESponderEventListener<CreateSensorMeasurementStatisticEvent> listener8 = new ESponderEventListener<CreateSensorMeasurementStatisticEvent>(
					CreateSensorMeasurementStatisticEvent.class);

			listener.subscribe();
			listener2.subscribe();
			listener3.subscribe();
			listener4.subscribe();
			listener5.subscribe();
			listener6.subscribe();
			listener7.subscribe();
			listener8.subscribe();

			while (true) {
				try {
					Thread.sleep(10000000);
				} catch (InterruptedException e) {
				}
			}

		} catch (ManagementException me) {
			me.printStackTrace();

		} finally {

		}

	}

}
