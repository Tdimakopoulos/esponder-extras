package eu.esponder.test.crisis;

import java.math.BigDecimal;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HeartBeatRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;
import eu.esponder.dto.model.type.SensorTypeDTO;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class SensorServiceTest extends ControllerServiceTest {

	private static int SECONDS = 10;

	@Test(groups="createResources")
	public void testCreateSensorDTO() throws ClassNotFoundException {

		SensorTypeDTO temperatureSensorType = (SensorTypeDTO) typeService.findDTOByTitle("TEMP");
		SensorTypeDTO heartbeatSensorType = (SensorTypeDTO) typeService.findDTOByTitle("HB");
		SensorTypeDTO locationSensorType = (SensorTypeDTO) typeService.findDTOByTitle("LOCATION");


		EquipmentDTO firstFRCEquipment = equipmentService.findByTitleRemote("FRU #1");
		EquipmentDTO firstFREquipment = equipmentService.findByTitleRemote("FRU #1.1");
		EquipmentDTO secondFREquipment = equipmentService.findByTitleRemote("FRU #1.2");
		EquipmentDTO secondFRCEquipment = equipmentService.findByTitleRemote("FRU #2");
		EquipmentDTO thirdFREquipment = equipmentService.findByTitleRemote("FRU #2.1");
		EquipmentDTO fourthFREquipment = equipmentService.findByTitleRemote("FRU #2.2");


		createSensorDTO(temperatureSensorType, "FRU #1 TEMP.", firstFRCEquipment, "BdTemp");
		createSensorDTO(heartbeatSensorType, "FRU #1 HB.", firstFRCEquipment, "HB");
		createSensorDTO(locationSensorType, "FRU #1 LOC.", firstFRCEquipment, "LOC");

		createSensorDTO(temperatureSensorType, "FRU #1.1 TEMP.", firstFREquipment, "BdTemp");
		createSensorDTO(heartbeatSensorType, "FRU #1.1 HB.", firstFREquipment, "HB");
		createSensorDTO(locationSensorType, "FRU #1.1 LOC.", firstFREquipment, "LOC");

		createSensorDTO(temperatureSensorType, "FRU #1.2 TEMP.", secondFREquipment, "BdTemp");
		createSensorDTO(heartbeatSensorType, "FRU #1.2 HB.", secondFREquipment, "HB");
		createSensorDTO(locationSensorType, "FRU #1.2 LOC.", secondFREquipment, "LOC");

		createSensorDTO(temperatureSensorType, "FRU #2 TEMP.", secondFRCEquipment, "BdTemp");
		createSensorDTO(heartbeatSensorType, "FRU #2 HB.", secondFRCEquipment, "HB");
		createSensorDTO(locationSensorType, "FRU #2 LOC.", secondFRCEquipment, "LOC");

		createSensorDTO(temperatureSensorType, "FRU #2.1 TEMP.", thirdFREquipment, "BdTemp");
		createSensorDTO(heartbeatSensorType, "FRU #2.1 HB.", thirdFREquipment, "HB");
		createSensorDTO(locationSensorType, "FRU #2.1 LOC.", thirdFREquipment, "LOC");

		createSensorDTO(temperatureSensorType, "FRU #2.2 TEMP.", fourthFREquipment, "BdTemp");
		createSensorDTO(heartbeatSensorType, "FRU #2.2 HB.", fourthFREquipment, "HB");
		createSensorDTO(locationSensorType, "FRU #2.2 LOC.", fourthFREquipment, "LOC");

	}

	@SuppressWarnings("static-access")
	@Test(groups="createSnapshots")
	public void testCreateSensorSnapshots() throws ClassNotFoundException, InterruptedException {	

		SensorDTO firstFRCTemperature = sensorService.findSensorByTitleRemote("FRU #1 TEMP.");
		//		SensorDTO firstFRCHeartbeat = sensorService.findByTitleRemote("FRU #1 HB.");
		//		SensorDTO firstFRTemperature = sensorService.findByTitleRemote("FRU #1.1 TEMP.");
		//		SensorDTO firstFRHeartbeat = sensorService.findByTitleRemote("FRU #1.1 HB.");
		//		SensorDTO secondFRTemperature = sensorService.findByTitleRemote("FRU #1.2 TEMP.");
		//		SensorDTO secondFRHeartbeat = sensorService.findByTitleRemote("FRU #1.2 HB.");
		//		SensorDTO secondFRCTemperature = sensorService.findByTitleRemote("FRU #2 TEMP.");
		//		SensorDTO secondFRCHeartbeat = sensorService.findByTitleRemote("FRU #2 HB.");
		//		SensorDTO thirdFRTemperature = sensorService.findByTitleRemote("FRU #2.1 TEMP.");
		//		SensorDTO thirdFRHeartbeat = sensorService.findByTitleRemote("FRU #2.1 HB.");
		//		SensorDTO fourthFRTemperature = sensorService.findByTitleRemote("FRU #2.2 TEMP.");
		//		SensorDTO fourthFRHeartbeat = sensorService.findByTitleRemote("FRU #2.2 HB.");
		//		
		//		Period period = this.createPeriod(SECONDS);


		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("30"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("32"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("33"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("32"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("30"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("32"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		
		
		//		createSensorSnapshot(firstFRCHeartbeat, new BigDecimal("80"), MeasurementStatisticTypeEnum.MEAN, period);
		//		createSensorSnapshot(firstFRTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnum.MEAN, period);
		//		createSensorSnapshot(firstFRHeartbeat, new BigDecimal("70"), MeasurementStatisticTypeEnum.MEAN, period);
		//		createSensorSnapshot(secondFRTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnum.MEAN, period);
		//		createSensorSnapshot(secondFRHeartbeat, new BigDecimal("85"), MeasurementStatisticTypeEnum.MEAN, period);
		//		createSensorSnapshot(secondFRCTemperature, new BigDecimal("35"), MeasurementStatisticTypeEnum.MEAN, period);
		//		createSensorSnapshot(secondFRCHeartbeat, new BigDecimal("79"), MeasurementStatisticTypeEnum.MEAN, period);
		//		createSensorSnapshot(thirdFRTemperature, new BigDecimal("36"), MeasurementStatisticTypeEnum.MEAN, period);
		//		createSensorSnapshot(thirdFRHeartbeat, new BigDecimal("74"), MeasurementStatisticTypeEnum.MEAN, period);
		//		createSensorSnapshot(fourthFRTemperature, new BigDecimal("34"), MeasurementStatisticTypeEnum.MEAN, period);
		//		createSensorSnapshot(fourthFRHeartbeat, new BigDecimal("84"), MeasurementStatisticTypeEnum.MEAN, period);
	}

	@Test(groups="createSnapshots")
	public void testCreateSensorSnapshotsDTO() throws ClassNotFoundException {		

		SensorDTO firstFRCTemperature = sensorService.findSensorByTitleRemote("FRU #1 TEMP.");
		SensorDTO firstFRCHeartbeat = sensorService.findSensorByTitleRemote("FRU #1 HB.");
		SensorDTO firstFRTemperature = sensorService.findSensorByTitleRemote("FRU #1.1 TEMP.");
		SensorDTO firstFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #1.1 HB.");
		SensorDTO secondFRTemperature = sensorService.findSensorByTitleRemote("FRU #1.2 TEMP.");
		SensorDTO secondFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #1.2 HB.");
		SensorDTO secondFRCTemperature = sensorService.findSensorByTitleRemote("FRU #2 TEMP.");
		SensorDTO secondFRCHeartbeat = sensorService.findSensorByTitleRemote("FRU #2 HB.");
		SensorDTO thirdFRTemperature = sensorService.findSensorByTitleRemote("FRU #2.1 TEMP.");
		SensorDTO thirdFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #2.1 HB.");
		SensorDTO fourthFRTemperature = sensorService.findSensorByTitleRemote("FRU #2.2 TEMP.");
		SensorDTO fourthFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #2.2 HB.");

		PeriodDTO period = this.createPeriodDTO(SECONDS);

		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("30"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("80"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(firstFRTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(firstFRHeartbeat, new BigDecimal("70"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(secondFRTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(secondFRHeartbeat, new BigDecimal("85"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("35"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("79"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(thirdFRTemperature, new BigDecimal("36"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(thirdFRHeartbeat, new BigDecimal("74"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(fourthFRTemperature, new BigDecimal("34"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(fourthFRHeartbeat, new BigDecimal("84"), MeasurementStatisticTypeEnumDTO.MEAN, period);
	}

	@Test(groups="CreateConfiguration")
	public void testCreateSensorConfigurationDTO() {		

		SensorDTO firstFRCTemperature = sensorService.findSensorByTitleRemote("FRU #1 TEMP.");
		SensorDTO firstFRCHeartbeat = sensorService.findSensorByTitleRemote("FRU #1 HB.");
		SensorDTO firstFRCLocation = sensorService.findSensorByTitleRemote("FRU #1 LOC.");

		SensorDTO firstFRTemperature = sensorService.findSensorByTitleRemote("FRU #1.1 TEMP.");
		SensorDTO firstFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #1.1 HB.");
		SensorDTO firstFRLocation = sensorService.findSensorByTitleRemote("FRU #1.1 LOC.");

		SensorDTO secondFRTemperature = sensorService.findSensorByTitleRemote("FRU #1.2 TEMP.");
		SensorDTO secondFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #1.2 HB.");
		SensorDTO secondFRLocation = sensorService.findSensorByTitleRemote("FRU #1.2 LOC.");

		SensorDTO secondFRCTemperature = sensorService.findSensorByTitleRemote("FRU #2 TEMP.");
		SensorDTO secondFRCHeartbeat = sensorService.findSensorByTitleRemote("FRU #2 HB.");
		SensorDTO secondFRCLocation = sensorService.findSensorByTitleRemote("FRU #2 LOC.");

		SensorDTO thirdFRTemperature = sensorService.findSensorByTitleRemote("FRU #2.1 TEMP.");
		SensorDTO thirdFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #2.1 HB.");
		SensorDTO thirdFRLocation = sensorService.findSensorByTitleRemote("FRU #2.1 LOC.");

		SensorDTO fourthFRTemperature = sensorService.findSensorByTitleRemote("FRU #2.2 TEMP.");
		SensorDTO fourthFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #2.2 HB.");
		SensorDTO fourthFRLocation = sensorService.findSensorByTitleRemote("FRU #2.2 LOC.");


		/***************************************************************************************************/
		createStatisticConfigDTO(firstFRCTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(firstFRCHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(firstFRCLocation, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);
		
		/***************************************************************************************************/

		createStatisticConfigDTO(firstFRTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(firstFRHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(firstFRLocation, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);
		
		/***************************************************************************************************/

		createStatisticConfigDTO(secondFRTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(secondFRHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(secondFRLocation, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);
		
		/***************************************************************************************************/

		createStatisticConfigDTO(secondFRCTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(secondFRCHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(secondFRCLocation, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);

		/***************************************************************************************************/
		
		createStatisticConfigDTO(thirdFRTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(thirdFRHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(thirdFRLocation, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);

		/***************************************************************************************************/
		
		createStatisticConfigDTO(fourthFRTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(fourthFRHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(fourthFRLocation, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);
		
	}

	private SensorDTO createSensorDTO(SensorTypeDTO type, String title, EquipmentDTO equipment, String label) throws ClassNotFoundException {
		SensorDTO sensor = null;

		if(type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("TEMP")).getTitle())) {
			sensor = new BodyTemperatureSensorDTO();
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("HB")).getTitle())) {
			sensor = new HeartBeatRateSensorDTO();
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("LOCATION")).getTitle())) {
			sensor = new LocationSensorDTO();
		}
		sensor.setType(type.getTitle());
		sensor.setTitle(title);
		sensor.setStatus(ResourceStatusDTO.ALLOCATED);
		sensor.setEquipmentId(equipment.getId());
		sensor.setLabel(label);
		return sensorService.createSensorRemote(sensor, this.userID);
	}

	private SensorSnapshotDTO createSensorSnapshotDTO(SensorDTO sensor, BigDecimal value, MeasurementStatisticTypeEnumDTO statisticType, PeriodDTO period) throws ClassNotFoundException {
		SensorSnapshotDTO snapshot = new SensorSnapshotDTO();

		snapshot.setSensor(sensor);
		snapshot.setStatisticType(statisticType);
		snapshot.setValue(value.toString());
		snapshot.setPeriod(period);
		snapshot.setStatus(SensorSnapshotStatusDTO.WORKING);

		return (SensorSnapshotDTO) sensorService.createSensorSnapshotRemote(snapshot, userID);
	}

	private StatisticsConfigDTO createStatisticConfigDTO(SensorDTO sensor, Long samplingPeriodMs, MeasurementStatisticTypeEnumDTO type) {
		StatisticsConfigDTO statisticsConfig = new StatisticsConfigDTO();
		statisticsConfig.setMeasurementStatisticType(type);
		statisticsConfig.setSamplingPeriodMilliseconds(samplingPeriodMs);
		statisticsConfig.setSensorId(sensor.getId());
		return sensorService.createStatisticConfigRemote(statisticsConfig, this.userID);
	}

}
