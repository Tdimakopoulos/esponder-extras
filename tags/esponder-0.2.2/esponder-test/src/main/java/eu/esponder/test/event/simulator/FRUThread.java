package eu.esponder.test.event.simulator;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.ArithmeticMeasurementSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationMeasurementSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;

public class FRUThread extends Thread {

	private Long fruId;

	private ActorDTO eventSource;

	private long threadPeriodFRU;

	private List<Thread> threadsList;

	private List<SensorDTO> sensors;

	private ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher;

	public List<SensorMeasurementStatisticDTO> statisticsList;

	private int rangeTemp;

	private int minTemp;

	private PointDTO startPoint;

	private PointDTO destPoint;

	public FRUThread(ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher, 
			List<SensorDTO> sensors, ActorDTO eventSource, long threadPeriodFRU, int range,
			int min, PointDTO startingPoint, PointDTO destinationPoint) {
		this.publisher = publisher;
		this.sensors = sensors;
		this.threadPeriodFRU = threadPeriodFRU;
		this.eventSource = eventSource;
		statisticsList = new ArrayList<SensorMeasurementStatisticDTO>();
		this.rangeTemp = range;
		this.minTemp = min;
		this.setStartPoint(startingPoint);
		this.setDestPoint(destinationPoint);
	}

	@Override
	public void run() {

		int counter = 10;
		int threadCounter = 0;

		this.setThreadsList(CreateSensorThreads());

		for(Thread thread : threadsList) {
			thread.start();
		}

		while(counter > 0) {
			try {
				// 1. Get measurements from SensorThreads
				if(getStatisticsList().size() != 0)
					// 2. Put them into a SensorMeasurementStatisticEnvelopeDTO
					accessStatistics(this.getPublisher());
				else
					System.out.println("\nEnvelope is empty \n");
				counter--;
				Thread.sleep(threadPeriodFRU);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}


		// Stops the sensorThreads based on the shared variable threadKill
		//		setThreadKill(true);
		for(Thread thread : threadsList)
			thread.interrupt();

		for(Thread thread : threadsList) {
			try {
				System.out.println("Waiting for Thread "+thread.getName()+"/"+thread.getId()+" to die...");
				thread.join(500);
			} catch (InterruptedException e) {
				System.out.println("Interrupted while waiting for Thread "+thread.getName()+"/"+thread.getId()+" to die...");
			}
		}

		System.out.println("\n\n********************	Threads State	********************");
		System.out.println(Thread.currentThread().getName()+"/"+Thread.currentThread().getId());
		for(Thread thread : threadsList) {
			System.out.println(thread.getName()+":"+thread.getId()+":"+ thread.getState()+":"+ thread.isAlive());
		}


		System.out.println(Thread.currentThread().getName()+"/"+Thread.currentThread().getId());
		if(getStatisticsList().size() != 0) {
			accessStatistics(publisher);
		}
	}


	private List<Thread> CreateSensorThreads() {

		List<Thread> tempThreadsList = new ArrayList<Thread>();

		System.out.println("Thread executing : " + Thread.currentThread().getName() + " with id : "+Thread.currentThread().getId());
		for(SensorDTO sensor : this.sensors) {
			for(StatisticsConfigDTO config : sensor.getConfiguration()) {
				SensorThread sensorThread = null;
				if (sensor instanceof ArithmeticMeasurementSensorDTO) { 
					sensorThread = new ArithmeticSensorThread(new Long(config.getSamplingPeriodMilliseconds()), sensor, config.getMeasurementStatisticType(), minTemp, rangeTemp, statisticsList);
				} else if (sensor instanceof LocationMeasurementSensorDTO) {
					sensorThread = new LocationSensorThread(new Long(config.getSamplingPeriodMilliseconds()), sensor, config.getMeasurementStatisticType(), this.getStartPoint(), this.getDestPoint(), statisticsList);
				}
				tempThreadsList.add(sensorThread);
			}
		}
		return tempThreadsList;
	}

	private void accessStatistics(ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher) {

		SensorMeasurementStatisticEnvelopeDTO envelope = new SensorMeasurementStatisticEnvelopeDTO();
		envelope.setMeasurementStatistics(new ArrayList<SensorMeasurementStatisticDTO>());
		synchronized (this.statisticsList) {

			for( Iterator< SensorMeasurementStatisticDTO > it = statisticsList.iterator(); it.hasNext();)
			{
				SensorMeasurementStatisticDTO statisticMeasurement = it.next();
				envelope.getMeasurementStatistics().add(statisticMeasurement);
				it.remove();
			}
			System.out.println(Thread.currentThread().getClass() + " / " + Thread.currentThread().getName() + "with id : "
					+Thread.currentThread().getId()+"\nEnvelope Contents size is : " + envelope.getMeasurementStatistics().size());

			// 3. Publish a CreateSensorMeasurementStatisticEvent
			@SuppressWarnings("unused")
			CreateSensorMeasurementStatisticEvent event = createSensorMeasurementStatisticEvent(envelope);
			try {
				publisher.publishEvent(event);
			} catch (EventListenerException e) {
				e.printStackTrace();
			}
		}

	}

	private CreateSensorMeasurementStatisticEvent createSensorMeasurementStatisticEvent(SensorMeasurementStatisticEnvelopeDTO envelope) {
		CreateSensorMeasurementStatisticEvent event = new CreateSensorMeasurementStatisticEvent();
		event.setEventAttachment(envelope);
		event.setEventSeverity(SeverityLevelDTO.UNDEFINED);
		event.setEventSource(this.eventSource);
		event.setJournalMessage("Test Journal Message");
		event.setEventTimestamp(new Date());
		return event;
	}


	public synchronized List<SensorMeasurementStatisticDTO> getStatisticsList() {
		return statisticsList;
	}

	public synchronized void setStatisticsList(List<SensorMeasurementStatisticDTO> statisticsList) {
		this.statisticsList = statisticsList;
	}

	public Long getFruId() {
		return fruId;
	}

	public void setFruId(Long fruId) {
		this.fruId = fruId;
	}

	public long getThreadPeriodFRU() {
		return threadPeriodFRU;
	}

	public void setThreadPeriodFRU(long threadPeriod) {
		this.threadPeriodFRU = threadPeriod;
	}

	public List<SensorDTO> getSensors() {
		return sensors;
	}

	public void setSensors(List<SensorDTO> sensors) {
		this.sensors = sensors;
	}

	public List<Thread> getThreadsList() {
		return threadsList;
	}

	public void setThreadsList(List<Thread> threadsList) {
		this.threadsList = threadsList;
	}

	public ActorDTO getEventSource() {
		return eventSource;
	}

	public void setEventSource(ActorDTO eventSource) {
		this.eventSource = eventSource;
	}

	public PointDTO getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(PointDTO startPoint) {
		this.startPoint = startPoint;
	}

	public PointDTO getDestPoint() {
		return destPoint;
	}

	public void setDestPoint(PointDTO destPoint) {
		this.destPoint = destPoint;
	}

	public ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> getPublisher() {
		return publisher;
	}

	public void setPublisher(
			ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher) {
		this.publisher = publisher;
	}

}
