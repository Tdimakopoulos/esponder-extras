package eu.esponder.EventAdminTest;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;

import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.test.ResourceLocator;

public class CreateArithmeticSensorMeasurementStatisticDTO implements Runnable {

		SensorRemoteService sensorService= ResourceLocator.lookup("esponder/SensorBean/remote");
		SensorDTO sensor1 = sensorService.findSensorByTitleRemote("FRU #1.1 TEMP.");
		SensorDTO sensor2 = sensorService.findSensorByTitleRemote("FRU #1.2 TEMP.");

		ArrayList<SensorMeasurementStatisticDTO> sensorMeasurementStatisticsList = new ArrayList<SensorMeasurementStatisticDTO>();
	
	
	@Override
	public void run() {
	
		ArrayList<SensorMeasurementStatisticDTO> sensorMeasurementStatisticsList = new ArrayList<SensorMeasurementStatisticDTO>();
		int counter = 100;
		while( counter > 0 ) {
			
			try {
				SensorMeasurementStatisticDTO sensorMeasurementTemp = createSensorMeasurementsStatistics();
				sensorMeasurementStatisticsList.add(sensorMeasurementTemp);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			counter--;
		}
		
	}

	
	
	
	private SensorMeasurementStatisticDTO createSensorMeasurementsStatistics() throws Exception {
		/************************************************/
		double index = 0.5;
		// #1
		ArithmeticSensorMeasurementDTO arithmeticMeasurement1 = new ArithmeticSensorMeasurementDTO();
		arithmeticMeasurement1.setSensor(sensor1);
		arithmeticMeasurement1.setTimestamp(new Date(0));
		arithmeticMeasurement1.setMeasurement(new BigDecimal(35+index));

		SensorMeasurementStatisticDTO measurementStatistic1 = new SensorMeasurementStatisticDTO();
		measurementStatistic1.setStatistic(arithmeticMeasurement1);
		measurementStatistic1.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
		measurementStatistic1.setPeriod(new PeriodDTO(unmarshal(new Date((long) (index*5))), unmarshal(new Date((long) (index*6)))));
		measurementStatistic1.setSamplingPeriod(0);
		return measurementStatistic1;
		
	}
	
	
	/******************************************************************************************************/

	public java.util.Date marshal(java.sql.Date sqlDate) throws Exception {
		if(null == sqlDate) {
			return null;
		}
		return new java.util.Date(sqlDate.getTime());
	}


	public static java.sql.Date unmarshal(java.util.Date utilDate) throws Exception {
		if(null == utilDate) {
			return null;
		}
		return new java.sql.Date(utilDate.getTime());
	}
	
	
	
	
	
	
}
