package eu.esponder.EventAdminTest;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;
import eu.esponder.test.ResourceLocator;

public class SensorMeasurementEnvelopeToEventAdminTest {

	/**@author dkar
	 * @param args
	 * @throws InterruptedException 
	 * @throws EventListenerException 
	 */

	static SensorRemoteService sensorService= ResourceLocator.lookup("esponder/SensorBean/remote");
	static SensorDTO sensor = sensorService.findSensorByIdRemote(new Long(1));

	static int condition = 20;
	static int seconds = 4;
	static double index = 0.5;

	public static void main() throws Exception {

		while(condition>0) {
			ESponderEventPublisher<CreateSensorMeasurementEnvelopeEvent> publisher = new ESponderEventPublisher<CreateSensorMeasurementEnvelopeEvent>(CreateSensorMeasurementEnvelopeEvent.class);

			CreateSensorMeasurementEnvelopeEvent event = createEvent(index);


			publisher.publishEvent(event);
			index = index + 0.5;
			condition = condition - 1;
			Thread.sleep( seconds * 1000 );
		}

	}

	/**
	 * @throws Exception ****************************************************************************************************/

	private static CreateSensorMeasurementEnvelopeEvent createEvent(double index) throws Exception {
		CreateSensorMeasurementEnvelopeEvent event = new CreateSensorMeasurementEnvelopeEvent();

		SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticEnvelopeDTO = createSensorMeasurementStatisticEnvelope(index);


		return null;
	}

	/**
	 * @throws Exception ****************************************************************************************************/

	private static SensorMeasurementStatisticEnvelopeDTO createSensorMeasurementStatisticEnvelope(double index) throws Exception {

		List<SensorMeasurementStatisticDTO> measurements = createSensorMeasurementsStatistics(index);
		SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticEnvelopeDTO = new SensorMeasurementStatisticEnvelopeDTO();
		sensorMeasurementStatisticEnvelopeDTO.setMeasurementStatistics(measurements);
		return null;
	}

	/**
	 * @throws Exception ****************************************************************************************************/

	private static List<SensorMeasurementStatisticDTO> createSensorMeasurementsStatistics(double index) throws Exception {

		List<SensorMeasurementStatisticDTO> measurementsList = new ArrayList<SensorMeasurementStatisticDTO>();
		/************************************************/

		// #1
		ArithmeticSensorMeasurementDTO arithmeticMeasurement1 = new ArithmeticSensorMeasurementDTO();
		arithmeticMeasurement1.setSensor(sensor);
		arithmeticMeasurement1.setTimestamp(new Date(0));
		arithmeticMeasurement1.setMeasurement(new BigDecimal(35+index));

		SensorMeasurementStatisticDTO measurementStatistic1 = new SensorMeasurementStatisticDTO();
		measurementStatistic1.setStatistic(arithmeticMeasurement1);
		measurementStatistic1.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
		measurementStatistic1.setPeriod(new PeriodDTO(unmarshal(new Date((long) (index*5))), unmarshal(new Date((long) (index*6)))));
//		measurementStatistic1.setSamplingPeriod(0);


		// #2
		ArithmeticSensorMeasurementDTO arithmeticMeasurement2 = new ArithmeticSensorMeasurementDTO();
		arithmeticMeasurement2.setSensor(sensor);
		arithmeticMeasurement2.setTimestamp(new Date(0));
		arithmeticMeasurement2.setMeasurement(new BigDecimal(35+index));

		SensorMeasurementStatisticDTO measurementStatistic2 = new SensorMeasurementStatisticDTO();
		measurementStatistic2.setStatistic(arithmeticMeasurement2);
		measurementStatistic2.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
		measurementStatistic2.setPeriod(new PeriodDTO(unmarshal(new Date((long) (index*5))), unmarshal(new Date((long) (index*6)))));
//		measurementStatistic2.setSamplingPeriod(0);


		// #3
		ArithmeticSensorMeasurementDTO arithmeticMeasurement3 = new ArithmeticSensorMeasurementDTO();
		arithmeticMeasurement3.setSensor(sensor);
		arithmeticMeasurement3.setTimestamp(new Date(0));
		arithmeticMeasurement3.setMeasurement(new BigDecimal(35+index));

		SensorMeasurementStatisticDTO measurementStatistic3 = new SensorMeasurementStatisticDTO();
		measurementStatistic3.setStatistic(arithmeticMeasurement3);
		measurementStatistic3.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
		measurementStatistic3.setPeriod(new PeriodDTO(unmarshal(new Date((long) (index*5))), unmarshal(new Date((long) (index*6)))));
		measurementStatistic3.setSamplingPeriod(0);

		
		measurementsList.add(measurementStatistic1);
		measurementsList.add(measurementStatistic2);
		measurementsList.add(measurementStatistic3);

		return measurementsList;
	}
	

	/******************************************************************************************************/

	public java.util.Date marshal(java.sql.Date sqlDate) throws Exception {
		if(null == sqlDate) {
			return null;
		}
		return new java.util.Date(sqlDate.getTime());
	}


	public static java.sql.Date unmarshal(java.util.Date utilDate) throws Exception {
		if(null == utilDate) {
			return null;
		}
		return new java.sql.Date(utilDate.getTime());
	}




}
