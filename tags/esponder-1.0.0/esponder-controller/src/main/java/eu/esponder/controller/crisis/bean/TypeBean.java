package eu.esponder.controller.crisis.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.type.ESponderType;

@Stateless
@SuppressWarnings("unchecked")
public class TypeBean implements TypeService, TypeRemoteService {
	
	@EJB
	private CrudService<ESponderType> typeCrudService;

	@EJB 
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------
	@Override
	public ESponderTypeDTO findDTOById(Long typeId) throws ClassNotFoundException {
		ESponderType type = findById(typeId);
		ESponderTypeDTO typeDTO = (ESponderTypeDTO) mappingService.mapESponderEntity(type, mappingService.getDTOEntityClass(type.getClass()));
		return typeDTO;
	}


	@Override
	public ESponderType findById(Long typeId) {
		return (ESponderType) typeCrudService.find(ESponderType.class, typeId);
	}

	//-------------------------------------------------------------------------

	@Override
	public ESponderTypeDTO findDTOByTitle(String title) throws ClassNotFoundException {
		ESponderType type = findByTitle(title);
		Class<? extends ESponderTypeDTO> typeDTOClass = (Class<? extends ESponderTypeDTO>) mappingService.getEntityClass(mappingService.getDTOEntityName(type.getClass().getName()));
		ESponderTypeDTO esponderTypeDTO = (ESponderTypeDTO) mappingService.mapESponderEntity((ESponderEntity<Long>) type, typeDTOClass);
		return esponderTypeDTO;
	}

	@Override
	public ESponderType findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ESponderType) typeCrudService.findSingleWithNamedQuery("ESponderType.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@Override
	public List<ESponderTypeDTO> findDTOAllTypes() throws ClassNotFoundException {
		List<ESponderTypeDTO> typeResultsDTO = new ArrayList<ESponderTypeDTO>();
		List<ESponderType> typeResults = new ArrayList<ESponderType>();
		typeResults = findAllTypes();
		for(ESponderType t : typeResults) {
			typeResultsDTO.add((ESponderTypeDTO) mappingService.mapESponderEntity(t, mappingService.getDTOEntityClass(t.getClass())));
		}
//		return (List<ESponderTypeDTO>) mappingService.mapESponderEntity(findAllTypes(), ESponderTypeDTO.class);
		return typeResultsDTO;
	}

	@Override
	public List<ESponderType> findAllTypes() {
		
		return (List<ESponderType>) typeCrudService.findWithNamedQuery("ESponderType.findAll");
	}
	
	//-------------------------------------------------------------------------
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderTypeDTO createTypeRemote(ESponderTypeDTO typeDTO, Long userID) throws ClassNotFoundException {
		ESponderType type = (ESponderType) mappingService.mapESponderEntityDTO(typeDTO, mappingService.getManagedEntityClass(typeDTO.getClass()));
		type = createType(type, userID);
		return (ESponderTypeDTO) mappingService.mapESponderEntity(type, mappingService.getDTOEntityClass(type.getClass()));

	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderType createType(ESponderType type, Long userID) {
		return (ESponderType) typeCrudService.create(type);
	}
	
	// -------------------------------------------------------------------------

	@Override
	public ESponderTypeDTO updateTypeRemote(ESponderTypeDTO typeDTO, Long userID) {
		ESponderType type = (ESponderType) mappingService.mapESponderEntityDTO(typeDTO, ESponderType.class);
		type = updateType(type, userID);
		typeDTO = (ESponderTypeDTO) mappingService.mapESponderEntity(type, ESponderTypeDTO.class);
		return typeDTO;
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderType updateType(ESponderType type, Long userID) {
		type = typeCrudService.update(type);
		return type;
	}
	
	// -------------------------------------------------------------------------

	@Override
	public void deleteTypeRemote(Long ESponderTypeDTOID, Long userID) {
		deleteType(ESponderTypeDTOID, userID);
	}
	
	@Override
	public void deleteType(Long ESponderTypeID, Long userID) {
		typeCrudService.delete(ESponderType.class, ESponderTypeID);
	}
	
	// -------------------------------------------------------------------------

}
