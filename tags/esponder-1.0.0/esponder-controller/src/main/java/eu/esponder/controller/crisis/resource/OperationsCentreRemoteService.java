package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;

@Remote
public interface OperationsCentreRemoteService {

	public OperationsCentreDTO findOperationCentreByIdRemote(Long operationsCentreID);

	public OperationsCentreDTO findOperationsCentreByTitleRemote(String title);

	public OperationsCentreDTO findOperationsCentreByUserRemote(Long operationsCentreID, Long userID);

	public OperationsCentreDTO setSupervisor(Long operationsCentreID, Long supervisingOpearationsCentreID, Long userID);

	public OperationsCentreDTO createOperationsCentreRemote(OperationsCentreDTO operationsCentreDTO, Long userID);

	public OperationsCentreDTO updateOperationsCentreRemote(OperationsCentreDTO operationsCentreDTO, Long userID);

	public void deleteOperationsCentreRemote(Long operationsCentreId, Long userID);

	public OperationsCentreSnapshotDTO findOperationsCentreSnapshotByDateRemote(Long operationsCentreID, Date maxDate);

	public OperationsCentreSnapshotDTO createOperationsCentreSnapshotRemote(OperationsCentreDTO operationsCentreDTO,
			OperationsCentreSnapshotDTO snapshotDTO, Long userID);
	
	public OperationsCentreSnapshotDTO updateOperationsCentreSnapshotDTO(OperationsCentreDTO operationsCentreDTO, OperationsCentreSnapshotDTO snapshotDTO, Long userID);

	public void deleteOperationsCentreSnapshotRemote(Long operationsCentreSnapshotDTOID);
	
	public SketchPOIDTO findSketchPOIByIdRemote(Long sketchPOIId);

	public SketchPOIDTO findSketchPOIByTitleRemote(String title);
	
	public List<SketchPOIDTO> findSketchPOIRemote(Long operationsCentreID);

	public SketchPOIDTO createSketchPOIRemote(OperationsCentreDTO operationsCentreDTO, SketchPOIDTO sketchDTO, Long userID);

	public SketchPOIDTO updateSketchPOIRemote(OperationsCentreDTO operationsCentreDTO, SketchPOIDTO sketchDTO, Long userID);
	
	public void deleteSketchPOIRemote(Long sketchPOIId, Long userID);
	
	public ReferencePOIDTO findReferencePOIByTitleRemote(String title);

	public ReferencePOIDTO findReferencePOIByIdRemote(Long referencePOIId);

	public List<ReferencePOIDTO> findReferencePOIsRemote(Long operationsCentreID);

	public ReferencePOIDTO createReferencePOIRemote(OperationsCentreDTO operationsCentreDTO,ReferencePOIDTO referencePOIDTO, Long userID);

	public ReferencePOIDTO updateReferencePOIRemote(OperationsCentreDTO operationsCentreDTO,ReferencePOIDTO referencePOIDTO, Long userID);
	
	public void deleteReferencePOIRemote(Long referencePOIId, Long userID);

	public List<OperationsCentreDTO> findUserOperationsCentresRemote(Long userID);

	public List<OperationsCentreDTO> findAllOperationsCentresRemote();

	public RegisteredOperationsCentreDTO findRegisteredOperationCentreByIdRemote(Long registeredOperationsCentreID);

	public RegisteredOperationsCentreDTO findRegisteredOperationsCentreByTitleRemote(String title);

	public List<RegisteredOperationsCentreDTO> findAllRegisteredOperationsCentresRemote();

	public RegisteredOperationsCentreDTO createRegisteredOperationsCentreRemote(
			RegisteredOperationsCentreDTO registeredOperationsCentreDTO, Long userID);

	public void deleteRegisteredOperationsCentreRemote(Long registeredOperationsCentreId, Long userID);

	public RegisteredOperationsCentreDTO updateRegisteredOperationsCentreRemote(
			RegisteredOperationsCentreDTO registeredOperationsCentreDTO, Long userID);

}
