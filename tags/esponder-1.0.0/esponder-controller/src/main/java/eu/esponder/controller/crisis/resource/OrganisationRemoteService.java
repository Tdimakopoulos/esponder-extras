package eu.esponder.controller.crisis.resource;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.OrganisationDTO;

@Remote
public interface OrganisationRemoteService {

	public OrganisationDTO findDTOById(Long organisationID);

	public OrganisationDTO findDTOByTitle(String organisationTitle);

	public OrganisationDTO createOrganisationDTO(OrganisationDTO organisationDTO);

	public OrganisationDTO updateOrganisationDTO(OrganisationDTO organisationDTO);

	public void deleteOrganisationDTO(OrganisationDTO organisationDTO);

	public void deleteOrganisationDTO(Long organisationDTOId);
	
}
