package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.TypeService;
import eu.esponder.model.type.EquipmentType;
import eu.esponder.util.ejb.ServiceLocator;


public class EquipmentTypeFieldConverter implements CustomConverter {

	protected TypeService getTypeService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Object convert(Object destination,
			Object source, 
			Class<?> destinationClass,
			Class<?> sourceClass) {
		
		if (sourceClass == String.class && source != null) {
			String type = (String) source;
			EquipmentType equipmentType = (EquipmentType) getTypeService().findByTitle(type);
			destination = equipmentType;
		}
		else if(EquipmentType.class.isAssignableFrom(sourceClass)) {
			EquipmentType equipmentType = (EquipmentType) source;
			destination = (String) equipmentType.getTitle();
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}