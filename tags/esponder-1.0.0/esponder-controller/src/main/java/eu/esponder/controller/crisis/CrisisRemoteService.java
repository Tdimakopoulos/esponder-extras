package eu.esponder.controller.crisis;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.crisis.resource.plan.PlannableResourcePowerDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;

@Remote
public interface CrisisRemoteService {

	public PlannableResourcePowerDTO findPlannableResourcePowerByIdRemote(Long powerID);

	public PlannableResourcePowerDTO createPlannableResourcePowerRemote(
			PlannableResourcePowerDTO plannableResourcePowerDTO, Long userID);

	public void deletePlannableResourcePowerRemote(Long powerID, Long userID);

	public PlannableResourcePowerDTO updatePlannableResourcePowerRemote(
			PlannableResourcePowerDTO plannableResourcePowerDTO, Long userID);

	public CrisisResourcePlanDTO findCrisisResourcePlanByIdRemote(
			Long crisisResourcePlanId, Long userId);

	public CrisisResourcePlanDTO findCrisisResourcePlanByTitleRemote(
			String crisisResourcePlanTitle, Long userId);

	public List<CrisisResourcePlanDTO> findAllCrisisResourcePlansRemote();

	public CrisisResourcePlanDTO createCrisisResourcePlanRemote(
			CrisisResourcePlanDTO crisisResourcePlanDTO, Long userID);

	public void deleteCrisisResourcePlanRemote(Long crisisResourcePlanId, Long userID);

	public CrisisResourcePlanDTO updateCrisisResourcePlanRemote(
			CrisisResourcePlanDTO crisisResourcePlanDTO, Long userID);

	public CrisisContextDTO findCrisisContextDTOById(Long crisisContextID);

	public List<CrisisContextDTO> findAllCrisisContextsRemote();

	public CrisisContextDTO findCrisisContextDTOByTitle(String title);

	public CrisisContextDTO createCrisisContextRemote(CrisisContextDTO crisisContextDTO, Long userID);

	public void deleteCrisisContextRemote(Long crisisContextId, Long userID);

	public CrisisContextDTO updateCrisisContextRemote(CrisisContextDTO crisisContextDTO, Long userID);

	public CrisisContextSnapshotDTO findCrisisContextSnapshotDTOById(Long crisisContextSnapshotID);

	public CrisisContextSnapshotDTO createCrisisContextSnapshotRemote(
			CrisisContextSnapshotDTO crisisContextSnapshotDTO, Long userID);

	public void deleteCrisisContextSnapshotRemote(Long crisisContextSnapshotId,
			Long userID);

	public CrisisContextSnapshotDTO updateCrisisContextSnapshotRemote(
			CrisisContextSnapshotDTO crisisContextSnapshotDTO, Long userID);

}
