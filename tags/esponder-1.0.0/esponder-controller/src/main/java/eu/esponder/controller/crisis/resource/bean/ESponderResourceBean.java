package eu.esponder.controller.crisis.resource.bean;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import eu.esponder.controller.crisis.resource.ESponderResourceService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.model.crisis.resource.Resource;

@Stateless
public class ESponderResourceBean implements ESponderResourceService {

	
	@EJB
	private CrudService<Resource> resourceCrudBean;
	
	@EJB
	private ESponderMappingService eSponderMappingService;
	
	@SuppressWarnings("unchecked")
	@Override
	public ResourceDTO findDTOById(Class<? extends ResourceDTO> clz, Long id) {
		try {
			Class<? extends Resource> targetClass = (Class<? extends Resource>) eSponderMappingService.getManagedEntityClass(clz);
			Resource resource = findByID(targetClass, id);
			return (ResourceDTO) eSponderMappingService.mapESponderEntity(resource, clz);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ResourceDTO findDTOByTitle(Class<? extends ResourceDTO> clz,
			String title) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Resource findByID(Class<? extends Resource> clz, Long id) {
		return resourceCrudBean.find((Class<Resource>) clz, id);
	}

	@Override
	public Resource findByTitle(Class<? extends Resource> clz,
			String title) {
		// TODO Auto-generated method stub
		return null;
	}

}
