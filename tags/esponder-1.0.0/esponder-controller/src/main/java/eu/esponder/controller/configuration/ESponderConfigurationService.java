package eu.esponder.controller.configuration;

import javax.ejb.Local;

import eu.esponder.model.config.ESponderConfigParameter;

@Local
public interface ESponderConfigurationService extends ESponderConfigurationRemoteService {
	
	public ESponderConfigParameter findESponderConfigByName(String configName) throws ClassNotFoundException;

	public ESponderConfigParameter findESponderConfigById(Long configId) throws ClassNotFoundException;

	public ESponderConfigParameter createESponderConfig(ESponderConfigParameter config, Long userID) throws ClassNotFoundException;
	
	public ESponderConfigParameter updateESponderConfig(ESponderConfigParameter config, Long userID);
	
	public void deleteESponderConfig(Long ESponderConfigID, Long userID);

}
