package eu.esponder.controller.crisis.resource.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.LogisticsRemoteService;
import eu.esponder.controller.crisis.resource.LogisticsService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.LogisticsResource;
import eu.esponder.model.crisis.resource.RegisteredConsumableResource;
import eu.esponder.model.crisis.resource.RegisteredReusableResource;
import eu.esponder.model.crisis.resource.ReusableResource;
import eu.esponder.model.type.ConsumableResourceType;

@Stateless
public class LogisticsBean implements LogisticsService, LogisticsRemoteService {

	@EJB
	private CrudService<LogisticsResource> logisticsCrudService;

	@EJB
	private CrudService<ConsumableResource> consumablesCrudService;

	@EJB
	private CrudService<ReusableResource> reusablesCrudService;

	@EJB
	private CrudService<RegisteredReusableResource> registeredReusablesCrudService;

	@EJB
	private CrudService<RegisteredConsumableResource> registeredConsumablesCrudService;

	@EJB
	private TypeService typeService;

	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	public LogisticsResource findLogisticsById(Long logisticsID) {
		return (LogisticsResource) logisticsCrudService.find(LogisticsResource.class, logisticsID);
	}

	//-------------------------------------------------------------------------

	@Override
	public ReusableResourceDTO findReusableResourceByIdRemote(Long reusableID) {
		ReusableResource reusableResource = findReusableResourceById(reusableID);
		ReusableResourceDTO reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	@Override
	public ReusableResource findReusableResourceById(Long reusableID) {
		return (ReusableResource) reusablesCrudService.find(ReusableResource.class, reusableID);
	}

	//-------------------------------------------------------------------------

	@Override
	public List<ReusableResourceDTO> findAllReusableResourcesRemote() {
		List<ReusableResource> resultsReusables = findAllReusableResources();
		List<ReusableResourceDTO> reusableResourcesDTO = (List<ReusableResourceDTO>) mappingService.mapESponderEntity(resultsReusables, ReusableResourceDTO.class);
		return reusableResourcesDTO;
	}

	@Override
	public List<ReusableResource> findAllReusableResources() {
		return (List<ReusableResource>) reusablesCrudService.findWithNamedQuery("ReusableResource.findAll");
	}

	//-------------------------------------------------------------------------

	@Override
	public RegisteredReusableResourceDTO findRegisteredReusableResourceByIdRemote(Long registeredReusableID) {
		RegisteredReusableResource registeredReusableResource = findRegisteredReusableResourceById(registeredReusableID);
		RegisteredReusableResourceDTO registeredReusableResourceDTO = (RegisteredReusableResourceDTO) mappingService.mapESponderEntity(registeredReusableResource, RegisteredReusableResourceDTO.class);
		return registeredReusableResourceDTO;
	}

	@Override
	public RegisteredReusableResource findRegisteredReusableResourceById(Long registeredReusableID) {
		return (RegisteredReusableResource) registeredReusablesCrudService.find(RegisteredReusableResource.class, registeredReusableID);
	}

	//-------------------------------------------------------------------------

	@Override
	public ConsumableResourceDTO findConsumableResourceByIdRemote(Long consumableID) {
		ConsumableResource consumableResource = findConsumableResourceById(consumableID);
		ConsumableResourceDTO consumableResourceDTO = (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ConsumableResourceDTO.class);
		return consumableResourceDTO;
	}

	@Override
	public ConsumableResource findConsumableResourceById(Long consumableID) {
		return (ConsumableResource) consumablesCrudService.find(ConsumableResource.class, consumableID);
	}

	//-------------------------------------------------------------------------

	@Override
	public List<ConsumableResourceDTO> findAllConsumableResourcesRemote() {
		List<ConsumableResource> resultsConsumables = findAllConsumableResources();
		List<ConsumableResourceDTO> consumableResourcesDTO = (List<ConsumableResourceDTO>) mappingService.mapESponderEntity(resultsConsumables, ConsumableResourceDTO.class);
		return consumableResourcesDTO;
	}

	@Override
	public List<ConsumableResource> findAllConsumableResources() {
		return (List<ConsumableResource>) consumablesCrudService.findWithNamedQuery("ConsumableResource.findAll");
	}

	//-------------------------------------------------------------------------

	@Override
	public RegisteredConsumableResourceDTO findRegisteredConsumableResourceByIdRemote(Long registeredConsumableID) {
		RegisteredConsumableResource registeredConsumableResource = findRegisteredConsumableResourceById(registeredConsumableID);
		RegisteredConsumableResourceDTO registeredConsumableResourceDTO = (RegisteredConsumableResourceDTO) mappingService.mapESponderEntity(registeredConsumableResource, RegisteredConsumableResourceDTO.class);
		return registeredConsumableResourceDTO;
	}

	@Override
	public RegisteredConsumableResource findRegisteredConsumableResourceById(Long registeredConsumableID) {
		return (RegisteredConsumableResource) registeredConsumablesCrudService.find(RegisteredConsumableResource.class, registeredConsumableID);
	}

	//-------------------------------------------------------------------------

	@Override
	public List<RegisteredConsumableResourceDTO> findAllRegisteredConsumableResourcesRemote() {
		List<RegisteredConsumableResource> resultsRegisteredConsumables = findAllRegisteredConsumableResources();
		List<RegisteredConsumableResourceDTO> registeredConsumableResourcesDTO = (List<RegisteredConsumableResourceDTO>) 
				mappingService.mapESponderEntity(resultsRegisteredConsumables, RegisteredConsumableResourceDTO.class);
		return registeredConsumableResourcesDTO;
	}

	@Override
	public List<RegisteredConsumableResource> findAllRegisteredConsumableResources() {
		return (List<RegisteredConsumableResource>) registeredConsumablesCrudService.findWithNamedQuery("RegisteredConsumableResource.findAll");
	}

	//-------------------------------------------------------------------------

	@Override
	public List<RegisteredReusableResourceDTO> findAllRegisteredReusableResourcesRemote() {
		List<RegisteredReusableResource> resultsRegisteredReusables = findAllRegisteredReusableResources();
		List<RegisteredReusableResourceDTO> reusableRegisteredResourcesDTO = 
				(List<RegisteredReusableResourceDTO>) mappingService.mapESponderEntity(resultsRegisteredReusables, RegisteredReusableResourceDTO.class);
		return reusableRegisteredResourcesDTO;
	}

	@Override
	public List<RegisteredReusableResource> findAllRegisteredReusableResources() {
		return (List<RegisteredReusableResource>) registeredReusablesCrudService.findWithNamedQuery("RegisteredReusableResource.findAll");
	}

	//-------------------------------------------------------------------------

	@Override
	public ReusableResourceDTO findReusableResourceByTitleRemote(String title) {
		ReusableResource reusableResource = findReusableResourceByTitle(title);
		ReusableResourceDTO reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	@Override
	public ReusableResource findReusableResourceByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ReusableResource) reusablesCrudService.findSingleWithNamedQuery("ReusableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@Override
	public RegisteredReusableResourceDTO findRegisteredReusableResourceByTitleRemote(String title) {
		RegisteredReusableResource registeredReusableResource = findRegisteredReusableResourceByTitle(title);
		RegisteredReusableResourceDTO registeredReusableResourceDTO = (RegisteredReusableResourceDTO) mappingService.mapESponderEntity(registeredReusableResource, RegisteredReusableResourceDTO.class);
		return registeredReusableResourceDTO;
	}

	@Override
	public RegisteredReusableResource findRegisteredReusableResourceByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (RegisteredReusableResource) registeredReusablesCrudService.findSingleWithNamedQuery("RegisteredReusableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@Override
	public ConsumableResourceDTO findConsumableResourceByTitleRemote(String title) {
		ConsumableResource consumableResource = findConsumableResourceByTitle(title);
		return (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ConsumableResourceDTO.class);
	}

	@Override
	public ConsumableResource findConsumableResourceByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ConsumableResource) consumablesCrudService.findSingleWithNamedQuery("ConsumableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@Override
	public RegisteredConsumableResourceDTO findRegisteredConsumableResourceByTitleRemote(String title) {
		RegisteredConsumableResource registeredConsumableResource = findRegisteredConsumableResourceByTitle(title);
		RegisteredConsumableResourceDTO registeredConsumableResourceDTO = (RegisteredConsumableResourceDTO) mappingService.mapESponderEntity(registeredConsumableResource, RegisteredConsumableResourceDTO.class);
		return registeredConsumableResourceDTO;
	}

	@Override
	public RegisteredConsumableResource findRegisteredConsumableResourceByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (RegisteredConsumableResource) registeredConsumablesCrudService.findSingleWithNamedQuery("RegisteredConsumableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@Override
	public ConsumableResourceDTO createConsumableResourceRemote( ConsumableResourceDTO consumableResourceDTO, Long userID) {
		ConsumableResource consumableResource = (ConsumableResource) mappingService.mapESponderEntityDTO(consumableResourceDTO, ConsumableResource.class);
		consumableResource = createConsumableResource(consumableResource, userID);
		consumableResourceDTO = (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ConsumableResourceDTO.class);
		return consumableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ConsumableResource createConsumableResource(ConsumableResource consumableResource, Long userID) {
		consumableResource.getConsumableResourceCategory().setConsumableResourceType((ConsumableResourceType) typeService.findById(consumableResource.getConsumableResourceCategory().getConsumableResourceType().getId()));
		if(null != consumableResource.getContainer())
		{
			consumableResource.setContainer(this.findReusableResourceById(consumableResource.getContainer().getId()));
		}
		consumablesCrudService.create(consumableResource);
		return consumableResource;
	}

	//-------------------------------------------------------------------------

	@Override
	public RegisteredConsumableResourceDTO createRegisteredConsumableResourceRemote( RegisteredConsumableResourceDTO registeredConsumableResourceDTO, Long userID) {
		RegisteredConsumableResource registeredConsumableResource = (RegisteredConsumableResource) mappingService.mapESponderEntityDTO(registeredConsumableResourceDTO, RegisteredConsumableResource.class);
		registeredConsumableResource = createRegisteredConsumableResource(registeredConsumableResource, userID);
		registeredConsumableResourceDTO = (RegisteredConsumableResourceDTO) mappingService.mapESponderEntity(registeredConsumableResource, RegisteredConsumableResourceDTO.class);
		return registeredConsumableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredConsumableResource createRegisteredConsumableResource(RegisteredConsumableResource registeredConsumableResource, Long userID) {
		//		registeredConsumableResource.getConsumableResourceCategory().setConsumableResourceType((ConsumableResourceType) typeService.findById(registeredConsumableResource.getConsumableResourceCategory().getConsumableResourceType().getId()));
		if(null != registeredConsumableResource.getContainer())
		{
			registeredConsumableResource.setContainer(this.findRegisteredReusableResourceById(registeredConsumableResource.getContainer().getId()));
		}
		registeredConsumablesCrudService.create(registeredConsumableResource);
		return registeredConsumableResource;
	}

	//-------------------------------------------------------------------------

	@Override
	public ReusableResourceDTO createReusableResourceRemote(ReusableResourceDTO reusableResourceDTO, Long userID) {
		ReusableResource reusableResource = (ReusableResource) mappingService.mapESponderEntityDTO(reusableResourceDTO, ReusableResource.class);
		reusableResource = createReusableResource(reusableResource, userID);
		reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReusableResource createReusableResource(ReusableResource reusableResource, Long userID) {
		reusablesCrudService.create(reusableResource);
		return reusableResource;
	}

	//-------------------------------------------------------------------------

	@Override
	public RegisteredReusableResourceDTO createRegisteredReusableResourceRemote(RegisteredReusableResourceDTO registeredReusableResourceDTO, Long userID) {
		RegisteredReusableResource registeredReusableResource = (RegisteredReusableResource) mappingService.mapESponderEntityDTO(registeredReusableResourceDTO, RegisteredReusableResource.class);
		registeredReusableResource = createRegisteredReusableResource(registeredReusableResource, userID);
		registeredReusableResourceDTO = (RegisteredReusableResourceDTO) mappingService.mapESponderEntity(registeredReusableResource, RegisteredReusableResourceDTO.class);
		return registeredReusableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredReusableResource createRegisteredReusableResource(RegisteredReusableResource registeredReusableResource, Long userID) {
		registeredReusablesCrudService.create(registeredReusableResource);
		return registeredReusableResource;
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteConsumableResourceRemote(Long reusableResourceDTOID, Long userID) {
		deleteConsumableResource(reusableResourceDTOID, userID);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteConsumableResource(Long consumableResourceID, Long userID) {
		consumablesCrudService.delete(ConsumableResource.class, consumableResourceID);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteRegisteredConsumableResourceRemote(Long registeredReusableResourceDTOID, Long userID) {
		deleteRegisteredConsumableResource(registeredReusableResourceDTOID, userID);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteRegisteredConsumableResource(Long registeredConsumableResourceID, Long userID) {
		registeredConsumablesCrudService.delete(RegisteredConsumableResource.class, registeredConsumableResourceID);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteReusableResourceRemote(Long reusableResourceDTOID, Long userID) {
		deleteReusableResource(reusableResourceDTOID, userID);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteReusableResource(Long reusableResourceID, Long userID) {
		reusablesCrudService.delete(ReusableResource.class, reusableResourceID);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteRegisteredReusableResourceRemote(Long registeredReusableResourceDTOID, Long userID) {
		deleteRegisteredReusableResource(registeredReusableResourceDTOID, userID);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteRegisteredReusableResource(Long registeredReusableResourceID, Long userID) {
		registeredReusablesCrudService.delete(RegisteredReusableResource.class, registeredReusableResourceID);
	}

	//-------------------------------------------------------------------------

	@Override
	public ReusableResourceDTO updateReusableResourceRemote(ReusableResourceDTO reusableResourceDTO, Long userID) {
		ReusableResource reusableResource = (ReusableResource) mappingService.mapESponderEntityDTO(reusableResourceDTO, ReusableResource.class);
		reusableResource = updateReusableResource(reusableResource, userID);
		reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReusableResource updateReusableResource(ReusableResource reusableResource, Long userID) {
		ReusableResource resourcePersisted = findReusableResourceById(reusableResource.getId());
		mappingService.mapEntityToEntity(reusableResource, resourcePersisted);
		return (ReusableResource) reusablesCrudService.update(resourcePersisted);
	}

	//-------------------------------------------------------------------------

	@Override
	public RegisteredReusableResourceDTO updateRegisteredReusableResourceRemote(RegisteredReusableResourceDTO registeredReusableResourceDTO, Long userID) {
		RegisteredReusableResource registeredReusableResource = (RegisteredReusableResource) mappingService.mapESponderEntityDTO(registeredReusableResourceDTO, RegisteredReusableResource.class);
		registeredReusableResource = updateRegisteredReusableResource(registeredReusableResource, userID);
		registeredReusableResourceDTO = (RegisteredReusableResourceDTO) mappingService.mapESponderEntity(registeredReusableResource, RegisteredReusableResourceDTO.class);
		return registeredReusableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredReusableResource updateRegisteredReusableResource(RegisteredReusableResource registeredReusableResource, Long userID) {
		RegisteredReusableResource reusablePersisted = findRegisteredReusableResourceById(registeredReusableResource.getId());
		mappingService.mapEntityToEntity(registeredReusableResource, reusablePersisted);
		return (RegisteredReusableResource) registeredReusablesCrudService.update(reusablePersisted);
	}

	//-------------------------------------------------------------------------

	@Override
	public ConsumableResourceDTO updateConsumableResourceRemote(ConsumableResourceDTO consumableResourceDTO, Long userID) {
		ConsumableResource consumableResource = (ConsumableResource) mappingService.mapESponderEntityDTO(consumableResourceDTO, ConsumableResource.class);
		consumableResource = updateConsumableResource(consumableResource, userID);
		consumableResourceDTO = (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ReusableResourceDTO.class);
		return consumableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ConsumableResource updateConsumableResource(ConsumableResource consumableResource, Long userID) {
		return (ConsumableResource) consumablesCrudService.update(consumableResource);
	}

	//-------------------------------------------------------------------------

	@Override
	public RegisteredConsumableResourceDTO updateRegisteredConsumableResourceRemote(RegisteredConsumableResourceDTO registeredConsumableResourceDTO, Long userID) {
		RegisteredConsumableResource registeredConsumableResource = (RegisteredConsumableResource) mappingService.mapESponderEntityDTO(registeredConsumableResourceDTO, RegisteredConsumableResource.class);
		registeredConsumableResource = updateRegisteredConsumableResource(registeredConsumableResource, userID);
		registeredConsumableResourceDTO = (RegisteredConsumableResourceDTO) mappingService.mapESponderEntity(registeredConsumableResource, RegisteredConsumableResourceDTO.class);
		return registeredConsumableResourceDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredConsumableResource updateRegisteredConsumableResource(RegisteredConsumableResource registeredConsumableResource, Long userID) {
		return (RegisteredConsumableResource) registeredConsumablesCrudService.update(registeredConsumableResource);
	}

}
