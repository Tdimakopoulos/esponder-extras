package eu.esponder.controller.crisis.resource;

import java.util.Date;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.snapshot.resource.EquipmentSnapshot;

@Local
public interface EquipmentService extends EquipmentRemoteService {
	
	public Equipment findById(Long equipmentID);
	
	public Equipment findByTitle(String title);
	
	public Equipment createEquipment(Equipment equipment, Long userID);
	
	public Equipment updateEquipment(Equipment equipment, Long userID);
	
	public void deleteEquipment(Long equipmentId, Long userID);
	
	public EquipmentSnapshot findEquipmentSnapshotByDate(Long equipmentID, Date dateTo);
	
	public EquipmentSnapshot createEquipmentSnapshot(EquipmentSnapshot snapshot, Long userID);

	public EquipmentSnapshot updateEquipmentSnapshot(EquipmentSnapshot snapshot, Long userID);

	public void deleteEquipmentSnapshot(Long equipmentSnapshotId, Long userID);
	
}
