package eu.esponder.controller.crisis.resource.bean;

import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.model.crisis.resource.Personnel;
import eu.esponder.model.crisis.resource.PersonnelCompetence;

@Stateless
public class PersonnelBean implements PersonnelService, PersonnelRemoteService {

	@EJB
	private CrudService<Personnel> personnelCrudService;

	@EJB
	private CrudService<PersonnelCompetence> personnelCompetenceCrudService;

	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	@Override
	public PersonnelDTO findPersonnelByIdRemote(Long personnelID) {
		Personnel personnel = findPersonnelById(personnelID);
		PersonnelDTO personnelDTO = (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
		return  personnelDTO;
	}

	@Override
	public Personnel findPersonnelById(Long personnelID) {
		return (Personnel) personnelCrudService.find(Personnel.class, personnelID);
	}

	//-------------------------------------------------------------------------
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PersonnelDTO> findAllPersonnelRemote() {
		return (List<PersonnelDTO>) mappingService.mapESponderEntity(findAllPersonnel(), PersonnelDTO.class);
	}

	@Override
	public List<Personnel> findAllPersonnel() {
		return (List<Personnel>) personnelCrudService.findWithNamedQuery("Personnel.findAll");
	}
	
	//-------------------------------------------------------------------------

	@Override
	public PersonnelCompetenceDTO findPersonnelCompetenceByIdRemote(Long personnelCompetenceID) {
		PersonnelCompetence personnelCompetence = findPersonnelCompetenceById(personnelCompetenceID);
		PersonnelCompetenceDTO personnelCompetenceDTO = (PersonnelCompetenceDTO) mappingService.mapESponderEntity(personnelCompetence, PersonnelCompetenceDTO.class);
		return  personnelCompetenceDTO;
	}

	@Override
	public PersonnelCompetence findPersonnelCompetenceById(Long personnelCompetenceID) {
		return (PersonnelCompetence) personnelCompetenceCrudService.find(PersonnelCompetence.class, personnelCompetenceID);
	}

	//-------------------------------------------------------------------------

	@Override
	public PersonnelDTO findPersonnelByTitleRemote(String personnelTitle) {
		Personnel personnel = findPersonnelByTitle(personnelTitle);
		PersonnelDTO personnelDTO = (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
		return personnelDTO;
	}

	@Override
	public Personnel findPersonnelByTitle(String personnelTitle) {
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("title", personnelTitle);
		return (Personnel) personnelCrudService.findSingleWithNamedQuery("Personnel.findByTitle", parameters);
	}

	//-------------------------------------------------------------------------

	@Override
	public PersonnelCompetenceDTO findPersonnelCompetenceByTitleRemote(String personnelCompetenceTitle) {
		PersonnelCompetence personnelCompetence = findPersonnelCompetenceByTitle(personnelCompetenceTitle);
		PersonnelCompetenceDTO personnelCompetenceDTO = (PersonnelCompetenceDTO) mappingService.mapESponderEntity(personnelCompetence, PersonnelCompetenceDTO.class);
		return personnelCompetenceDTO;
	}

	@Override
	public PersonnelCompetence findPersonnelCompetenceByTitle(String personnelCompetenceTitle) {
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("shortTitle", personnelCompetenceTitle);
		return (PersonnelCompetence) personnelCompetenceCrudService.findSingleWithNamedQuery("PersonnelCompetence.findByTitle", parameters);
	}

	//-------------------------------------------------------------------------

	@Override
	public PersonnelDTO createPersonnelRemote(PersonnelDTO personnelDTO, Long userID) {
		Personnel personnel = (Personnel) mappingService.mapESponderEntityDTO(personnelDTO, Personnel.class);
		personnel = createPersonnel(personnel, userID);
		return (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
	}


	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Personnel createPersonnel(Personnel personnel, Long userID) {
		return (Personnel) personnelCrudService.create(personnel);
	}

	//-------------------------------------------------------------------------

	//FIXME
	@Override
	public PersonnelCompetenceDTO createPersonnelCompetenceRemote(PersonnelCompetenceDTO personnelCompetenceDTO, Long userID) throws ClassNotFoundException {
		PersonnelCompetence personnelCompetence;
		personnelCompetence = (PersonnelCompetence) mappingService.mapESponderEntityDTO(personnelCompetenceDTO, mappingService.getManagedEntityClass(personnelCompetenceDTO.getClass()));
		personnelCompetence = createPersonnelCompetence(personnelCompetence, userID);
		return (PersonnelCompetenceDTO) mappingService.mapESponderEntity(personnelCompetence, personnelCompetenceDTO.getClass());
	}


	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public PersonnelCompetence createPersonnelCompetence(PersonnelCompetence personnelCompetence, Long userID) {
		return (PersonnelCompetence) personnelCompetenceCrudService.create(personnelCompetence);
	}

	//-------------------------------------------------------------------------

	@Override
	public PersonnelDTO updatePersonnelRemote(PersonnelDTO personnelDTO, Long userID) {
		Personnel personnel = (Personnel) mappingService.mapESponderEntityDTO(personnelDTO, Personnel.class);
		personnel = updatePersonnel(personnel, userID);
		return (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Personnel updatePersonnel(Personnel personnel, Long userID) {
		Personnel personnelPersisted = findPersonnelById(personnel.getId());
		mappingService.mapEntityToEntity(personnel, personnelPersisted);
		return (Personnel) personnelCrudService.update(personnelPersisted);
	}

	//-------------------------------------------------------------------------

	@Override
	public PersonnelCompetenceDTO updatePersonnelCompetenceRemote(PersonnelCompetenceDTO personnelCompetenceDTO, Long userID) throws ClassNotFoundException {
		PersonnelCompetence personnelCompetence;
		personnelCompetence = (PersonnelCompetence) mappingService.mapESponderEntityDTO(personnelCompetenceDTO,  mappingService.getManagedEntityClass(personnelCompetenceDTO.getClass()));
		personnelCompetence = updatePersonnelCompetence(personnelCompetence, userID);
		return (PersonnelCompetenceDTO) mappingService.mapESponderEntity(personnelCompetence, personnelCompetenceDTO.getClass());
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public PersonnelCompetence updatePersonnelCompetence(PersonnelCompetence personnelCompetence, Long userID) {
		return (PersonnelCompetence) personnelCompetenceCrudService.update(personnelCompetence);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deletePersonnelRemote(PersonnelDTO personnelDTO) {
		Personnel personnel = (Personnel) mappingService.mapESponderEntityDTO(personnelDTO, Personnel.class);
		deletePersonnel(personnel);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deletePersonnel(Personnel personnel) {
		personnelCrudService.delete(personnel);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deletePersonnelByIdRemote(Long personnelDTOId) {
		deletePersonnelByID(personnelDTOId);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deletePersonnelByID(Long personnelID) {
		personnelCrudService.delete(Personnel.class, personnelID);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deletePersonnelCompetenceByIdRemote(Long personnelCompetenceDTOId) {
		deletePersonnelCompetenceByID(personnelCompetenceDTOId);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deletePersonnelCompetenceByID(Long personnelCompetenceID) {
		personnelCompetenceCrudService.delete(PersonnelCompetence.class, personnelCompetenceID);
	}

}
