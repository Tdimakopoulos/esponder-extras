package eu.esponder.controller.events;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.events.entity.OsgiEventsEntityDTO;

@Remote
public interface EventsEntityRemoteService {

	public OsgiEventsEntityDTO findOsgiEventsEntityByIdRemote(Long osgiEventsEntityID);

	public List<OsgiEventsEntityDTO> findAllOsgiEventsEntitiesRemote();

	public List<OsgiEventsEntityDTO> findOsgiEventsEntitiesBySeverityRemote(
			String severity);

	public List<OsgiEventsEntityDTO> findOsgiEventsEntitiesBySourceIdRemote(
			Long sourceId);

	public OsgiEventsEntityDTO createOsgiEventsEntityRemote(
			OsgiEventsEntityDTO osgiEventsEntityDTO, Long userID);

	public void deleteOsgiEventsEntityRemote(Long osgiEventsEntityID, Long userID);

	public OsgiEventsEntityDTO updateOsgiEventsEntityRemote(
			OsgiEventsEntityDTO osgiEventsEntityDTO, Long userID);
	

}
