package eu.esponder.controller.crisis.user;

import javax.ejb.Local;

import eu.esponder.model.user.ESponderUser;

@Local
public interface UserService extends UserRemoteService {

	public ESponderUser findUserById(Long userID);

	public ESponderUser findUserByName(String userName);

	public ESponderUser createUser(ESponderUser user, Long userID);

	public ESponderUser updateUser(ESponderUser user, Long userID);

}
