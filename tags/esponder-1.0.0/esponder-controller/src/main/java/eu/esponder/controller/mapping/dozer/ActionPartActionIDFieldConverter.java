package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.action.ActionService;
import eu.esponder.model.crisis.action.Action;
import eu.esponder.util.ejb.ServiceLocator;

public class ActionPartActionIDFieldConverter implements CustomConverter {
	
	protected ActionService getActionService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if (sourceClass == Action.class && source != null) {
			Action sourceAction = (Action) source;
			Long actionID = sourceAction.getId();
			destination = actionID;
		}
		else if(sourceClass == Long.class && source!=null) { 
			Long actionID = (Long) source;
			Action destAction = (Action) this.getActionService().findActionById(actionID);
			destination = destAction;
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
