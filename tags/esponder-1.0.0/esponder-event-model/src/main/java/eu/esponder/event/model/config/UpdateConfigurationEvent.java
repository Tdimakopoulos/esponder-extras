package eu.esponder.event.model.config;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;

public class UpdateConfigurationEvent extends ConfigurationEvent<ResourceDTO> {

	private static final long serialVersionUID = 9215480992618803998L;

	@Override
	public String getJournalMessageInfo() {
		String journalMessageInfo = "Received Update Configuration Event including: \n";
		return journalMessageInfo;
	}
}
