package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.event.model.CreateEvent;


public class CreateCrisisContextSnapshotEvent extends CrisisContextSnapshotEvent<CrisisContextSnapshotDTO> implements CreateEvent {

	private static final long serialVersionUID = 4658214780264993947L;
	
}
