package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;

public abstract class SensorSnapshotEvent<T extends EquipmentSnapshotDTO> extends EquipmentSnapshotEvent<T> {

	private static final long serialVersionUID = -1369626844210367218L;

}
