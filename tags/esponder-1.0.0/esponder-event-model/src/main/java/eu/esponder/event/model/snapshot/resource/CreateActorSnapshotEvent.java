package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.event.model.CreateEvent;


public class CreateActorSnapshotEvent extends ActorSnapshotEvent<ActorSnapshotDTO> implements CreateEvent {

	private static final long serialVersionUID = -7149492311767628355L;
	
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
