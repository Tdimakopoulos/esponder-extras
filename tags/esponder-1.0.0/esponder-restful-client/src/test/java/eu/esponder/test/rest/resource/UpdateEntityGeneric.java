package eu.esponder.test.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.rest.client.ResteasyClient;
import eu.esponder.util.jaxb.Parser;
import eu.esponder.util.logger.ESponderLogger;

public class UpdateEntityGeneric {
	private String GENERIC_CREATE_ENTITY_URI = "http://localhost:8080/esponder-restful/crisis/generic";
	private String GETID_GENERIC_QUERY_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/generic/get";

	
	@Test
	public void UpdateSketch() throws ClassNotFoundException, Exception {

		System.out.println("***************************************************");
		System.out.println("Update Method for Generic Entity beginning");
		System.out.println("***************************************************");
		Parser parser = new Parser(new Class[] {SketchPOIDTO.class});
		SketchPOIDTO sketch = getSketchByIDXml(new Long (8));
		sketch.setTitle(sketch.getTitle() + "test2");
		String serviceName = GENERIC_CREATE_ENTITY_URI + "/update";

		ResteasyClient putClient = new ResteasyClient(serviceName, "application/xml");
		String xmlPayload = parser.marshall(sketch);
		Map<String, String> params =  getUpdateEntityServiceParameters();
		String resultXML = putClient.put(params, xmlPayload);

		System.out.println("\n*********FULL_XML*******************************\n");
		System.out.println(resultXML+"\n\n");
		System.out.println("\n*********FULL_XML_END***************************\n");
	}
	
	
	@Test
	public void UpdateEntity () throws ClassNotFoundException, Exception {

		System.out.println("***************************************************");
		System.out.println("Update Method for Generic Entity beginning");
		System.out.println("***************************************************");
		Parser parser = new Parser(new Class[] {ActorDTO.class});
		ActorDTO actor = getActorByIDXml(new Long (11));
		actor.setTitle(actor.getTitle() + "test2");
		actor.setEquipmentSet(null);
		String serviceName = GENERIC_CREATE_ENTITY_URI + "/update";

		ResteasyClient putClient = new ResteasyClient(serviceName, "application/xml");
		String xmlPayload = parser.marshall(actor);
		Map<String, String> params =  getUpdateEntityServiceParameters();
		String resultXML = putClient.put(params, xmlPayload);

		System.out.println("\n*********FULL_XML*******************************\n");
		System.out.println(resultXML+"\n\n");
		System.out.println("\n*********FULL_XML_END***************************\n");
	}


	@Test
	public void UpdateEntityWithJson () throws ClassNotFoundException, Exception {

		System.out.println("***************************************************");
		System.out.println("Update Method for Generic Entity beginning");
		System.out.println("***************************************************");
		ObjectMapper mapper = new ObjectMapper();
		ActorDTO actor = getActorByIDJson(new Long (10));
		actor.setTitle(actor.getTitle() + "test2");
		actor.setEquipmentSet(null);
		String serviceName = GENERIC_CREATE_ENTITY_URI + "/update";

		ResteasyClient putClient = new ResteasyClient(serviceName, "application/json");
		String payload = mapper.writeValueAsString(actor);
		
		printJSON(payload);
		
		Map<String, String> params =  getUpdateEntityServiceParameters();
		String result = putClient.put(params, payload);
		
		ActorDTO actorDTO = mapper.readValue(result, ActorDTO.class);
		
		actorDTO.toString();

	}


	private Map<String, String> getUpdateEntityServiceParameters() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		return params;
	}


	public ActorDTO getActorByIDXml(Long actorID) throws RuntimeException, Exception{

		Parser parser = new Parser(new Class[] {ActorDTO.class});

		ResteasyClient getClient = new ResteasyClient(GETID_GENERIC_QUERY_SERVICE_URI, "application/xml");
		Map<String, String> params = this.getIDServiceParameters(actorID);

		String resultXML = getClient.get(params);
		printXML(resultXML);

		ActorDTO actorDTO = (ActorDTO) parser.unmarshal(resultXML);
		System.out.println("\n*****************ACTOR**************************");
		System.out.println(actorDTO.toString());
		System.out.println("\n************************************************");

		return actorDTO;
	}

	public ActorDTO getActorByIDJson(Long actorID) throws RuntimeException, Exception{


		ObjectMapper mapper = new ObjectMapper();
		ResteasyClient getClient = new ResteasyClient(GETID_GENERIC_QUERY_SERVICE_URI, "application/json");
		Map<String, String> params = this.getIDServiceParameters(actorID);

		ESponderLogger.debug(this.getClass(), "fetching initial actor object");
		String resultJson = getClient.get(params);
		printJSON(resultJson);

		ActorDTO actorDTO = mapper.readValue(resultJson, ActorDTO.class);

		System.out.println("\n*****************ACTOR**************************");
		System.out.println(actorDTO.toString());
		System.out.println("\n************************************************");

		return actorDTO;
	}
	
	
	
	public SketchPOIDTO getSketchByIDXml(Long sketchID) throws RuntimeException, Exception{

		Parser parser = new Parser(new Class[] {SketchPOIDTO.class, ResultListDTO.class});

		ResteasyClient getClient = new ResteasyClient(GETID_GENERIC_QUERY_SERVICE_URI, "application/xml");
		Map<String, String> params = this.getIDServiceParameters(sketchID);

		String resultXML = getClient.get(params);
		if(!resultXML.isEmpty()) {
			printXML(resultXML);

			ResultListDTO resultsList = (ResultListDTO) parser.unmarshal(resultXML);
//			SketchPOIDTO sketchDTO = (SketchPOIDTO) parser.unmarshal(resultXML);
			SketchPOIDTO sketchDTO = (SketchPOIDTO) resultsList.getResultList().get(0);
			System.out.println("\n*****************ACTOR**************************");
			System.out.println(sketchDTO.toString());
			System.out.println("\n************************************************");

			return sketchDTO;
		}
		return null;	
	}

	private Map<String, String> getIDServiceParameters(Long actorID) {
		ArrayList<Long> idList = new ArrayList<Long>();
		idList.add(new Long(8)); 
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		params.put("queriedEntityDTO", SketchPOIDTO.class.getName());
		params.put("pageNumber", "0");
		params.put("pageSize", "10");
		params.put("idList", idList.toString());
		return params;
	}

	private void printJSON(String jsonStr) {
		System.out.println("\n\n******* JSON * START *******");
		System.out.println(jsonStr);
		System.out.println("******** JSON * END ********\n\n");
	}

	private void printXML(String jsonStr) {
		System.out.println("\n\n******* XML * START *******");
		System.out.println(jsonStr);
		System.out.println("******** XML * END ********\n\n");
	}

}
