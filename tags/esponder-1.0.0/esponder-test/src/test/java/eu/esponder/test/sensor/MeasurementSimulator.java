package eu.esponder.test.sensor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;
import eu.esponder.test.ResourceLocator;
import eu.esponder.test.event.simulator.FRUThread;

public class MeasurementSimulator {

	SensorRemoteService sensorService = ResourceLocator.lookup("esponder/SensorBean/remote");
	ActorRemoteService actorService = ResourceLocator.lookup("esponder/ActorBean/remote");

	Long threadPeriodFRU = (long) 3000;

	List<Thread> FRUThreads = new ArrayList<Thread>();

	@Test
	public void runScenario1() throws InterruptedException {

		int counter = 4;
		int minTemp = 35;
		int rangeTemp = 3;
		
		ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher1 = null;
		ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher2 = null;
		try {
			publisher1 = new ESponderEventPublisher<CreateSensorMeasurementStatisticEvent>(CreateSensorMeasurementStatisticEvent.class);
			publisher2 = new ESponderEventPublisher<CreateSensorMeasurementStatisticEvent>(CreateSensorMeasurementStatisticEvent.class);
		} catch (Exception e1) {
			System.out.println("\n\nUnable to get a connection to EventAdmin, simulation will be terminated\n\n");
			return;
		}
		
		// Syggrou-Fix -----> Faliriko Delta, along Syggrou Avenue
		PointDTO startingPointFRU1 = new PointDTO(new BigDecimal(37.959655), new BigDecimal(23.720305), null);
		PointDTO destinationPointFRU1 = new PointDTO(new BigDecimal(37.940703), new BigDecimal(23.696158), null);
		
		// Plateia Neas Smirnis -----> Axilleos, along Eleftheriou Venizelou Avenue
		PointDTO startingPointFRU2 = new PointDTO(new BigDecimal(37.946997), new BigDecimal(23.715169), null);
		PointDTO destinationPointFRU2 = new PointDTO(new BigDecimal(37.929668), new BigDecimal(23.709848), null);
		
		ActorDTO actor1 = actorService.findByTitleRemote("FR #1.1");
		ActorDTO actor2 = actorService.findByTitleRemote("FR #1.2");
		
		/* instantiate fru1Sensors, fru2Sensors	 */
		List<SensorDTO> fru1Sensors = createListOfSensors(Arrays.asList("FRU #1 TEMP.", "FRU #1 LOC.", "FRU #1.1 TEMP.", "FRU #1.1 LOC."));
		List<SensorDTO> fru2Sensors = createListOfSensors(Arrays.asList("FRU #2 TEMP.", "FRU #2 LOC.", "FRU #2.1 TEMP.", "FRU #2.1 LOC."));
		
		FRUThread fru1Thread = new FRUThread(publisher1, fru1Sensors, actor1, threadPeriodFRU, rangeTemp, minTemp, startingPointFRU1, destinationPointFRU1);
		FRUThreads.add(new Thread(fru1Thread, "FRU Thread 1"));
		
		FRUThread fru2Thread = new FRUThread(publisher2, fru2Sensors, actor2, threadPeriodFRU, rangeTemp, minTemp, startingPointFRU2, destinationPointFRU2);
		FRUThreads.add(new Thread(fru2Thread, "FRU Thread 2"));

		// Start Threads
		for(Thread thread : FRUThreads)
			thread.start();

		while (true) {
			for(Thread thread : FRUThreads)
				if(!thread.isAlive()) {
					System.out.println("\n\n************************************");
					System.out.println("\tSimulation has ended successfully");
					System.out.println("************************************");
					publisher1.CloseConnection();
					publisher2.CloseConnection();
					return;
				}
		}
		
	}

	private List<SensorDTO> createListOfSensors(List<String> sensorTitles) {
		List<SensorDTO> fruSensors = new ArrayList<SensorDTO>();
		for(String title: sensorTitles) {
			fruSensors.add(sensorService.findSensorByTitleRemote(title));
		}
		return fruSensors;
	}
}
