package eu.esponder.osgi.service.event;

import java.io.IOException;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.prosyst.mprm.backend.event.Event;
import com.prosyst.mprm.backend.event.EventListener;
import com.prosyst.mprm.backend.event.EventListenerException;
import com.prosyst.mprm.common.ManagementException;
import com.prosyst.mprm.data.DictionaryInfo;

import eu.esponder.df.ruleengine.controller.bean.DatafusionControllerBean;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.osgi.service.connection.ConnectionHandler;

public class ESponderEventListener<T extends ESponderEvent<? extends ESponderEntityDTO>>
		implements EventListener {

	private static ConnectionHandler connectionHandler = new ConnectionHandler();

	private static ObjectMapper mapper = initialiseMapper();

	private String eventTopic;

	private Class<T> eventTopicClass;

	public ESponderEventListener(Class<T> eventClass)
			throws ManagementException {

		this.eventTopicClass = eventClass;
		this.setEventTopic(eventClass.getCanonicalName());

		if (connectionHandler.getRac() == null) {
			connectionHandler.connect();
		}
	}

	public void CloseConnection()
	{
		connectionHandler.CloseConnection();
	}
	
	public void subscribe() throws ManagementException {
		connectionHandler.getRac().addEventListener(this.getEventTopic(), this);
	}

	@Override
	public void event(Event event) throws EventListenerException {

		//System.out.println("Event: " + event.getEventData());

		/*
		 * Get the event data from the EventAdmin
		 */
		DictionaryInfo data = (DictionaryInfo) event.getEventData();
		String esponderEventData = (String) data
				.get(ConnectionHandler.EVENT_PROPERTY_NAME);
		parseEventData(esponderEventData);

	}

	private ESponderEvent<? extends ESponderEntityDTO> parseEventData(
			String eventData) {
		ESponderEvent<? extends ESponderEntityDTO> cssEvent = null;
		System.out.println("########### OSGI EVENT Listener - EVENT RECEIVED #############");
		try {
			/*
			 * Parse the event data
			 */
			JsonFactory jsonFactory = new JsonFactory();
			JsonParser jp;

			jp = jsonFactory.createJsonParser(eventData);

			Class<? extends ESponderEvent<? extends ESponderEntityDTO>> clz = (Class<? extends ESponderEvent<? extends ESponderEntityDTO>>) eventTopicClass;

			cssEvent = mapper.readValue(jp, clz);

			/*
			 * and just print them
			 */
			//printEvent(cssEvent);

			/*
			 * Passing event to DF
			 */
			RegisterDatafusionEvent(cssEvent);

			

		} catch (JsonParseException e) {
			System.out.println("******** OSGI EVENT Listener -  Parse Data JPARERROR ******"
					+ e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			System.out.println("******** OSGI EVENT Listener - Parse Data err MAPEX******"
					+ e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("******** OSGI EVENT Listener - Parse Data IOERR ******"
					+ e.getMessage());
			e.printStackTrace();
		}
		return cssEvent;
	}

	private void RegisterDatafusionEvent(
			ESponderEvent<? extends ESponderEntityDTO> event) {
		DatafusionControllerBean pEventManager = new DatafusionControllerBean();
		pEventManager.EsponderEventReceivedHandler(event);
	}

	

	private void printEvent(ESponderEvent<? extends ESponderEntityDTO> event) {
//		System.out
//				.println("########### OSGI EVENT Handler - EVENT RECEIVED #############");
//		System.out.println("########### EVENT DETAILS START #############");
//		System.out.println("CSSE # " + event.toString());
//		System.out.println("CSSE attachment # "
//				+ event.getEventAttachment().toString());
//		System.out.println("CSSE severity # " + event.getEventSeverity());
//		System.out.println("CSSE source # " + event.getEventSource());
//		System.out.println("CSSE timestamp# " + event.getEventTimestamp());
//		System.out.println("########### EVENT DETAILS END #############");

	}

	public String getEventTopic() {
		return eventTopic;
	}

	public void setEventTopic(String eventTopic) {
		this.eventTopic = eventTopic;
	}

	public static ObjectMapper initialiseMapper() {
		/*
		 * Create Jackson deserialization mapper
		 */
		ObjectMapper objMapper = new ObjectMapper();
		objMapper
				.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
		objMapper
				.configure(
						DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,
						false);
		return objMapper;
	}

	public Class<T> getEventTopicClass() {
		return eventTopicClass;
	}

	public void setEventTopicClass(Class<T> eventTopicClass) {
		this.eventTopicClass = eventTopicClass;
	}

}
