package eu.esponder.osgi.listener.thread;

import java.io.IOException;
import java.util.Date;

import com.prosyst.mprm.common.ManagementException;

import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.crisis.action.CreateActionPartEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionPartSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.osgi.service.event.ESponderEventListener;
import eu.esponder.osgi.settings.OsgiSettings;

public class ESponderEventListenerServer extends Thread {

	ESponderEventListener<CreateActionSnapshotEvent> listener = null;
	ESponderEventListener<CreateCrisisContextEvent> listener2 = null;
	ESponderEventListener<CreateActionEvent> listener3 = null;
	ESponderEventListener<CreateActionPartEvent> listener4 = null;
	ESponderEventListener<CreateSensorMeasurementEnvelopeEvent> listener5 = null;
	ESponderEventListener<UpdateActionSnapshotEvent> listener6 = null;
	ESponderEventListener<UpdateActionPartSnapshotEvent> listener7 = null;
	ESponderEventListener<CreateSensorMeasurementStatisticEvent> listener8 = null;
	public boolean bConnection = true;
	public boolean bListen = true;
	String szgfilename = null;
	int iRunTimes = 0;

	public void CreateListenersConnections() throws ManagementException {
		if (listener == null)
			listener = new ESponderEventListener<CreateActionSnapshotEvent>(
					CreateActionSnapshotEvent.class);

		if (listener2 == null)
			listener2 = new ESponderEventListener<CreateCrisisContextEvent>(
					CreateCrisisContextEvent.class);

		if (listener3 == null)
			listener3 = new ESponderEventListener<CreateActionEvent>(
					CreateActionEvent.class);

		if (listener4 == null)
			listener4 = new ESponderEventListener<CreateActionPartEvent>(
					CreateActionPartEvent.class);

		if (listener5 == null)
			listener5 = new ESponderEventListener<CreateSensorMeasurementEnvelopeEvent>(
					CreateSensorMeasurementEnvelopeEvent.class);

		if (listener6 == null)
			listener6 = new ESponderEventListener<UpdateActionSnapshotEvent>(
					UpdateActionSnapshotEvent.class);

		if (listener7 == null)
			listener7 = new ESponderEventListener<UpdateActionPartSnapshotEvent>(
					UpdateActionPartSnapshotEvent.class);

		if (listener8 == null)
			listener8 = new ESponderEventListener<CreateSensorMeasurementStatisticEvent>(
					CreateSensorMeasurementStatisticEvent.class);
	}

	public String CloseServer() throws IOException {
		ServerStatus pStatus = new ServerStatus();
		String szFilename = null;

		if (szgfilename == null) {
			OsgiSettings pSettings = new OsgiSettings();
			pSettings.LoadSettings();
			szFilename = pSettings.getSzPropertiesFileName();
			szgfilename = szFilename;
		} else {
			szFilename = szgfilename;
		}
		return pStatus.readFile(pStatus.AppandFileOnPath(pStatus
				.RemoveFilename(szFilename)));
	}

	public void WriteStatusOnFile(boolean running) {
		ServerStatus pStatus = new ServerStatus();
		String szFilename;
		if (szgfilename == null) {
			OsgiSettings pSettings = new OsgiSettings();
			pSettings.LoadSettings();
			szFilename = pSettings.getSzPropertiesFileName();
			szgfilename = szFilename;
		} else {
			szFilename = szgfilename;
		}
		if (running)
			pStatus.UpdateServerRunning(szFilename,
					"Server is Running, Last Check : " + new Date());
		if (!running)
			pStatus.UpdateServerRunning(szFilename,
					"Server is Stopped, Last Check : " + new Date());
	}

	public void stopserver() {
		WriteStatusOnFile(false);
		if (listener != null)
			listener.CloseConnection();
		if (listener2 != null)
			listener2.CloseConnection();
		if (listener3 != null)
			listener3.CloseConnection();
		if (listener4 != null)
			listener4.CloseConnection();
		if (listener5 != null)
			listener5.CloseConnection();
		if (listener6 != null)
			listener6.CloseConnection();
		if (listener7 != null)
			listener7.CloseConnection();
		if (listener8 != null)
			listener8.CloseConnection();
		listener = null;
		listener2 = null;
		listener3 = null;
		listener4 = null;
		listener5 = null;
		listener6 = null;
		listener7 = null;
		listener8 = null;
		bConnection = false;
	}

	public void startserver() {
		WriteStatusOnFile(true);
		try {
			CreateListenersConnections();
			listener.subscribe();
			listener2.subscribe();
			listener3.subscribe();
			listener4.subscribe();
			listener5.subscribe();
			listener6.subscribe();
			listener7.subscribe();
			listener8.subscribe();
		} catch (ManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		bConnection = true;
	}

	public void run() {

		try {
			startserver();
			while (true) {

				Thread.sleep(600000);

				if (CloseServer().equalsIgnoreCase("0")) {
					bListen = false;
				} else {
					bListen = true;
				}

				if (bListen) {
					if (!bConnection)
						startserver();
				} else {
					stopserver();
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}
	// public void run() {
	// if (iRunTimes == 0) {
	// try {
	// Thread.sleep(60000);
	// iRunTimes = 1;
	// System.out.println("Listener Server is Running");
	// WriteStatusOnFile(true);
	// } catch (InterruptedException e) {
	//
	// }
	// }
	//
	// try {
	//
	// CreateListenersConnections();
	// listener.subscribe();
	// listener2.subscribe();
	// listener3.subscribe();
	// listener4.subscribe();
	// listener5.subscribe();
	// listener6.subscribe();
	// listener7.subscribe();
	// listener8.subscribe();
	//
	// while (true) {
	// try {
	// Thread.sleep(600000);
	// if (bListen) {
	// WriteStatusOnFile(true);
	// if (listener != null)
	// listener.CloseConnection();
	// if (listener2 != null)
	// listener2.CloseConnection();
	// if (listener3 != null)
	// listener3.CloseConnection();
	// if (listener4 != null)
	// listener4.CloseConnection();
	// if (listener5 != null)
	// listener5.CloseConnection();
	// if (listener6 != null)
	// listener6.CloseConnection();
	// if (listener7 != null)
	// listener7.CloseConnection();
	// if (listener8 != null)
	// listener8.CloseConnection();
	// listener = null;
	// listener2 = null;
	// listener3 = null;
	// listener4 = null;
	// listener5 = null;
	// listener6 = null;
	// listener7 = null;
	// listener8 = null;
	// CreateListenersConnections();
	// listener.subscribe();
	// listener2.subscribe();
	// listener3.subscribe();
	// listener4.subscribe();
	// listener5.subscribe();
	// listener6.subscribe();
	// listener7.subscribe();
	// listener8.subscribe();
	// } else {
	// WriteStatusOnFile(false);
	// if (listener != null)
	// listener.CloseConnection();
	// if (listener2 != null)
	// listener2.CloseConnection();
	// if (listener3 != null)
	// listener3.CloseConnection();
	// if (listener4 != null)
	// listener4.CloseConnection();
	// if (listener5 != null)
	// listener5.CloseConnection();
	// if (listener6 != null)
	// listener6.CloseConnection();
	// if (listener7 != null)
	// listener7.CloseConnection();
	// if (listener8 != null)
	// listener8.CloseConnection();
	// listener = null;
	// listener2 = null;
	// listener3 = null;
	// listener4 = null;
	// listener5 = null;
	// listener6 = null;
	// listener7 = null;
	// listener8 = null;
	// }
	// try {
	// if (CloseServer().equalsIgnoreCase("0")) {
	// bListen = false;
	// } else {
	// bListen = true;
	// }
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// } catch (InterruptedException e) {
	// }
	// }
	//
	// } catch (ManagementException me) {
	//
	// } finally {
	//
	// }
	//
	// }

}
