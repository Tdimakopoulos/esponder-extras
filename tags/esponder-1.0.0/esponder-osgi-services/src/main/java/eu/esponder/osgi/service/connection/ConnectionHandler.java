package eu.esponder.osgi.service.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import com.prosyst.mprm.common.ManagementException;
import com.prosyst.mprm.rac.RemoteAccessClient;

import eu.esponder.osgi.settings.OsgiSettings;
import eu.esponder.util.properties.ESponderPropertyLoader;

public class ConnectionHandler {

	private static Properties prop = new Properties();

	private static Hashtable<String, String> credentials = new Hashtable<String, String>();

	private static String userName;
	private static String password;
	private static String server_URI;

	private static final String USERNAME_PROPERTY = "username";
	private static final String PASSWORD_PROPERTY = "password";
	private static final String SERVER_IPADDR_PROPERTY = "OSGi_Server_IP_Address";
	private static final String SERVER_PORT_PROPERTY = "OSGi_Server_Port";
	private static final String SERVER_PROTOCOL_PROPERTY = "OSGi_Server_Protocol";
	private String szStatusServer="";
	OsgiSettings dSettings = new OsgiSettings();

	@SuppressWarnings("unused")
	private static final String OSGI_SERVER_URI_PROPERTY = "OSGi_Server_URI";

	public static final String EVENT_PROPERTY_NAME = "event";

	private RemoteAccessClient rac;
	private boolean ConnectionActive;

	public ConnectionHandler() {

		ConnectionActive = false;
		String szPropertiesFile = dSettings.getSzPropertiesFileName();

		try {
			prop = ESponderPropertyLoader.getProperties(szPropertiesFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		userName = prop.getProperty(USERNAME_PROPERTY);
		password = prop.getProperty(PASSWORD_PROPERTY);
		server_URI = prop.getProperty(SERVER_PROTOCOL_PROPERTY) + "://"
				+ prop.getProperty(SERVER_IPADDR_PROPERTY) + ":"
				+ prop.getProperty(SERVER_PORT_PROPERTY);
		credentials.put(RemoteAccessClient.USER_PASSWORD, password);
	}

	public ConnectionHandler(String userName, String password, String serverURI) {
		ConnectionActive = false;
		ConnectionHandler.userName = userName;
		ConnectionHandler.password = password;
		ConnectionHandler.server_URI = serverURI;

		credentials.put(RemoteAccessClient.USER_PASSWORD, password);
	}

	public RemoteAccessClient connect() throws ManagementException {
		rac = RemoteAccessClient.connect(server_URI, userName, credentials,
				null);
		ConnectionActive = true;
		return rac;
	}

	public RemoteAccessClient getRac() {
		return rac;
	}

	public void setRac(RemoteAccessClient rac) {
		this.rac = rac;
	}

	public void CloseConnection() {
		if (ConnectionActive) {
			rac.close();
			ConnectionActive = false;
		}
	}

}
