package eu.esponder.osgi.listener.thread;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

public class ServerStatus {

	public String RemoveFilename(String szInput) {
		File f = new File(szInput);
		String path = f.getParent();
		return path;
	}

	public String AppandFileOnPath(String szInput) {
		String szFullPath = null;
		szFullPath = szInput + File.separator + "server.status";
		return szFullPath;
	}

	public String AppandServerFileOnPath(String szInput) {
		String szFullPath = null;
		szFullPath = szInput + File.separator + "serverinfo.status";
		return szFullPath;
	}

	public String readFile(String path) throws IOException {
		CreateFileIfNotExists(path);
		FileInputStream stream = new FileInputStream(new File(path));
		try {
			FileChannel fc = stream.getChannel();
			MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0,
					fc.size());
			/* Instead of using default, pass in a decoder. */
			return Charset.defaultCharset().decode(bb).toString();
		} finally {
			stream.close();
		}
	}

	public void CreateFileIfNotExists(String filename) throws IOException {
		File f;
		f = new File(filename);
		if (!f.exists()) {
			f.createNewFile();
			WriteOnFile(filename, "1");
		}
	}

	public void WriteOnFile(String filename, String content) throws IOException {

		String text = content;
		BufferedWriter out = new BufferedWriter(new FileWriter(filename));
		out.write(text);
		out.close();

	}

	public void UpdateServerRunning(String szFilename, String info) {
		String NewFile = AppandServerFileOnPath(RemoveFilename(szFilename));
		try {
			WriteOnFile(NewFile, info);
		} catch (IOException e) {
			System.out.println("Write server status info error : "
					+ e.getMessage());
		}
	}
}
