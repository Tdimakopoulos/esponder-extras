package eu.esponder.rest.portal;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.rest.ESponderResource;

@Path("/portal/types")
public class EsponderTypes extends ESponderResource {

	@GET
	@Path("/findAll")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO readAllPersonnel(
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID) throws ClassNotFoundException {

		List<ESponderTypeDTO> pResults = this.getTypeRemoteService().findDTOAllTypes();
		return new ResultListDTO(pResults);

	}
	
}