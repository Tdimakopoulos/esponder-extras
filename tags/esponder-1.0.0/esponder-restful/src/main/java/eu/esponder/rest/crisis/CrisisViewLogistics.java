package eu.esponder.rest.crisis;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.rest.ESponderResource;

@Path("/crisis/view")
public class CrisisViewLogistics extends ESponderResource {
	
	@GET
	@Path("/consumables/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceDTO readConsumableById(
			@QueryParam("consumableResourceId") @NotNull(message="consumableResourceId may not be null") Long consumableResourceId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ConsumableResourceDTO consumableResourceDTO = this.getLogisticsRemoteService().findConsumableResourceByIdRemote(consumableResourceId);
		return consumableResourceDTO;
	}
	
	@GET
	@Path("/consumables/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllConsumables(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		return new ResultListDTO(this.getLogisticsRemoteService().findAllConsumableResourcesRemote());
		
	}
	
	@GET
	@Path("/consumables/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceDTO readConsumableByTitle(
			@QueryParam("consumableResourceTitle") @NotNull(message="consumableResourceTitle may not be null") String consumableResourceTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ConsumableResourceDTO consumableResourceDTO = this.getLogisticsRemoteService().findConsumableResourceByTitleRemote(consumableResourceTitle);
		return consumableResourceDTO;
	}
	
	@POST
	@Path("/consumables/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceDTO createConsumableResource(
			@NotNull(message="Consumable Resource object may not be null") ConsumableResourceDTO consumableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		consumableResourceDTO = this.getLogisticsRemoteService().createConsumableResourceRemote(consumableResourceDTO, userID);
		return consumableResourceDTO;
	}
	
	@PUT
	@Path("/consumables/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceDTO updateConsumableResource(
			@NotNull(message="Consumable Resource object may not be null") ConsumableResourceDTO consumableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		consumableResourceDTO = this.getLogisticsRemoteService().updateConsumableResourceRemote(consumableResourceDTO, userID);
		return consumableResourceDTO;
	}
	
	@DELETE
	@Path("/consumables/delete")
	public Long deleteConsumableResource(
			@QueryParam("consumableResourceID") @NotNull(message="Consumable Resource ID may not be null") Long consumableResourceID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getLogisticsRemoteService().deleteConsumableResourceRemote(consumableResourceID, userID);
		return consumableResourceID;
	}
	
	@GET
	@Path("/regConsumables/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredConsumableResourceDTO readRegisteredConsumableById(
			@QueryParam("registeredConsumableResourceId") @NotNull(message="registeredConsumableResourceId may not be null") Long registeredConsumableResourceId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		RegisteredConsumableResourceDTO registeredConsumableResourceDTO = this.getLogisticsRemoteService().findRegisteredConsumableResourceByIdRemote(registeredConsumableResourceId);
		return registeredConsumableResourceDTO;
	}
	
	@GET
	@Path("/regConsumables/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllRegisteredConsumables(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		return new ResultListDTO(this.getLogisticsRemoteService().findAllRegisteredConsumableResourcesRemote());
		
	}
	
	@GET
	@Path("/regConsumables/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredConsumableResourceDTO readRegisteredConsumableByTitle(
			@QueryParam("registeredConsumableResourceTitle") @NotNull(message="registeredConsumableResourceTitle may not be null") String registeredConsumableResourceTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		RegisteredConsumableResourceDTO registeredConsumableResourceDTO = this.getLogisticsRemoteService().findRegisteredConsumableResourceByTitleRemote(registeredConsumableResourceTitle);
		return registeredConsumableResourceDTO;
	}
	
	@POST
	@Path("/regConsumables/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredConsumableResourceDTO createRegisteredConsumableResource(
			@NotNull(message="Registered Consumable Resource object may not be null") RegisteredConsumableResourceDTO registeredConsumableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		registeredConsumableResourceDTO = this.getLogisticsRemoteService().createRegisteredConsumableResourceRemote(registeredConsumableResourceDTO, userID);
		return registeredConsumableResourceDTO;
	}
	
	@PUT
	@Path("/regConsumables/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredConsumableResourceDTO updateRegisteredConsumableResource(
			@NotNull(message="Registered Consumable Resource object may not be null") RegisteredConsumableResourceDTO registeredConsumableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		registeredConsumableResourceDTO = this.getLogisticsRemoteService().updateRegisteredConsumableResourceRemote(registeredConsumableResourceDTO, userID);
		return registeredConsumableResourceDTO;
	}
	
	@DELETE
	@Path("/regConsumables/delete")
	public Long deleteRegisteredConsumableResource(
			@QueryParam("registeredConsumableResourceID") @NotNull(message="Registered Consumable Resource ID may not be null") Long registeredConsumableResourceID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getLogisticsRemoteService().deleteRegisteredConsumableResourceRemote(registeredConsumableResourceID, userID);
		return registeredConsumableResourceID;
	}
	
	@GET
	@Path("/regReusables/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredReusableResourceDTO readRegisteredReusableById(
			@QueryParam("registeredReusableResourceId") @NotNull(message="registeredReusableResourceId may not be null") Long registeredReusableResourceId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		RegisteredReusableResourceDTO registeredReusableResourceDTO = this.getLogisticsRemoteService().findRegisteredReusableResourceByIdRemote(registeredReusableResourceId);
		return registeredReusableResourceDTO;
	}
	
	@GET
	@Path("/regReusables/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllRegisteredReusables(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		return new ResultListDTO(this.getLogisticsRemoteService().findAllRegisteredReusableResourcesRemote());
		
	}
	
	@GET
	@Path("/regReusables/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredReusableResourceDTO readRegisteredReusableByTitle(
			@QueryParam("registeredResourceTitle") @NotNull(message="registeredReusableResourceTitle may not be null") String registeredReusableResourceTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		RegisteredReusableResourceDTO registeredReusableResourceDTO = this.getLogisticsRemoteService().findRegisteredReusableResourceByTitleRemote(registeredReusableResourceTitle);
		return registeredReusableResourceDTO;
	}
	
	@POST
	@Path("/regReusables/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredReusableResourceDTO createRegisteredReusableResource(
			@NotNull(message="Registered Reusable Resource object may not be null") RegisteredReusableResourceDTO registeredReusableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		registeredReusableResourceDTO = this.getLogisticsRemoteService().createRegisteredReusableResourceRemote(registeredReusableResourceDTO, userID);
		return registeredReusableResourceDTO;
	}
	
	@PUT
	@Path("/regReusables/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredReusableResourceDTO updateRegisteredReusableResource(
			@NotNull(message="Registered Reusable Resource object may not be null") RegisteredReusableResourceDTO registeredReusableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		registeredReusableResourceDTO = this.getLogisticsRemoteService().updateRegisteredReusableResourceRemote(registeredReusableResourceDTO, userID);
		return registeredReusableResourceDTO;
	}
	
	@DELETE
	@Path("/regReusables/delete")
	public Long deleteRegisteredReusableResource(
			@QueryParam("registeredReusableResourceID") @NotNull(message="Registered Reusable Resource ID may not be null") Long registeredReusableResourceID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getLogisticsRemoteService().deleteRegisteredReusableResourceRemote(registeredReusableResourceID, userID);
		return registeredReusableResourceID;
	}
	
	@GET
	@Path("/reusables/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceDTO readReusableById(
			@QueryParam("reusableResourceId") @NotNull(message="reusableResourceId may not be null") Long reusableResourceId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ReusableResourceDTO reusableResourceDTO = this.getLogisticsRemoteService().findReusableResourceByIdRemote(reusableResourceId);
		return reusableResourceDTO;
	}
	
	@GET
	@Path("/reusables/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllReusables(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		return new ResultListDTO(this.getLogisticsRemoteService().findAllReusableResourcesRemote());
		
	}
	
	@GET
	@Path("/reusables/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceDTO readReusableByTitle(
			@QueryParam("reusableResourceTitle") @NotNull(message="reusableResourceTitle may not be null") String reusableResourceTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		ReusableResourceDTO reusableResourceDTO = this.getLogisticsRemoteService().findReusableResourceByTitleRemote(reusableResourceTitle);
		return reusableResourceDTO;
	}
	
	@POST
	@Path("/reusables/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceDTO createReusableResource(
			@NotNull(message="Reusable Resource object may not be null") ReusableResourceDTO reusableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		reusableResourceDTO = this.getLogisticsRemoteService().createReusableResourceRemote(reusableResourceDTO, userID);
		return reusableResourceDTO;
	}
	
	@PUT
	@Path("/reusables/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceDTO updateReusableResource(
			@NotNull(message="Reusable Resource object may not be null") ReusableResourceDTO reusableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		reusableResourceDTO = this.getLogisticsRemoteService().updateReusableResourceRemote(reusableResourceDTO, userID);
		return reusableResourceDTO;
	}
	
	@DELETE
	@Path("/reusables/delete")
	public Long deleteReusableResource(
			@QueryParam("reusableResourceID") @NotNull(message="Reusable Resource ID may not be null") Long reusableResourceID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getLogisticsRemoteService().deleteReusableResourceRemote(reusableResourceID, userID);
		return reusableResourceID;
	}
	
	@GET
	@Path("/equipments/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public EquipmentDTO readReusableByTitle(
			@QueryParam("equipmentID") @NotNull(message="userID may not be null") Long equipmentID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		EquipmentDTO pFind=this.getEquipmentRemoteService().findByIdRemote(equipmentID);
				
		return pFind;
	}

}
