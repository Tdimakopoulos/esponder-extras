package eu.esponder.rest.business.logic;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.rest.ESponderResource;

@Path("/business/logic")
public class WorkFlows extends ESponderResource{

	@PUT
	@Path("/crisis/associateActor")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO associateActor(
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("ActorType") @NotNull(message="ActorType may not be null") String actorType,
			@QueryParam("ActorTitle") @NotNull(message="ActorTitle may not be null") String actorTitle,
			@QueryParam("ActionPartID") @NotNull(message="ActionPartID may not be null") Long actionPartID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {


		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.RESERVED);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorDTO actor = new ActorDTO();
		actor.setPersonnel(personnelUpdated);
		actor.setStatus(ResourceStatusDTO.AVAILABLE);
		actor.setType(actorType);
		actor.setTitle(actorTitle);
		actor = this.getActorRemoteService().createActorRemote(actor, userID);

		// Associate with CrisisContext
		ActionPartDTO actionPart = this.getActionRemoteService().findActionPartDTOById(actionPartID);
		actionPart.setActor(actor);
		actionPart = this.getActionRemoteService().updateActionPartRemote(actionPart, userID);

		return actor;
	}


	@PUT
	@Path("/crisis/deassociateActor")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO deassociateActor(
			@QueryParam("actorID") @NotNull(message="actorID may not be null") Long actorID,
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {


		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.AVAILABLE);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorDTO actor = this.getActorRemoteService().findByIdRemote(actorID);
		actor.setPersonnel(personnelUpdated);
		actor.setStatus(ResourceStatusDTO.UNAVAILABLE);
		actor = this.getActorRemoteService().updateActorRemote(actor, userID);
		return actor;
	}


	@PUT
	@Path("/crisis/associateReusable")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceDTO associateReusable(
			@QueryParam("crisisContextID") @NotNull(message="Crisis Context ID may not be null") Long crisisContextID,
			@QueryParam("regReusableID") @NotNull(message="Registered Reusable Resource ID may not be null") Long regreusableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {

		RegisteredReusableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredReusableResourceByIdRemote(regreusableID);

		ReusableResourceDTO reusableResource = new ReusableResourceDTO();
		reusableResource.setCrisisContextId(crisisContextID);
		reusableResource.setQuantity(regResourceDTO.getQuantity());
		reusableResource.setTitle(regResourceDTO.getTitle());
		reusableResource.setStatus(ResourceStatusDTO.AVAILABLE);
		ReusableResourceCategoryDTO reusableCategoryDTO = (ReusableResourceCategoryDTO) this.getResourceCategoryRemoteService().
				findByIdRemote(ReusableResourceCategoryDTO.class, regResourceDTO.getReusableResourceCategoryId()); 
		reusableResource.setReusableResourceCategory(reusableCategoryDTO);
		this.getLogisticsRemoteService().updateReusableResourceRemote(reusableResource, userID);


		regResourceDTO.setStatus(ResourceStatusDTO.RESERVED);
		this.getLogisticsRemoteService().updateRegisteredReusableResourceRemote(regResourceDTO, userID);

		return reusableResource;
	}

	@PUT
	@Path("/crisis/deassociateReusable")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredReusableResourceDTO deassociateReusable(
			@QueryParam("reusableID") @NotNull(message="Reusable Resource ID may not be null") Long reusableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {

		ReusableResourceDTO reusableResource = this.getLogisticsRemoteService().findReusableResourceByIdRemote(reusableID);
		RegisteredReusableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredReusableResourceByTitleRemote(reusableResource.getTitle());

		regResourceDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		this.getLogisticsRemoteService().updateRegisteredReusableResourceRemote(regResourceDTO, userID);
		this.getLogisticsRemoteService().deleteReusableResourceRemote(reusableID, userID);
		return regResourceDTO;
	}

	@PUT
	@Path("/crisis/associateConsumable")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceDTO associateConsumable(
			@QueryParam("crisisContextID") @NotNull(message="Crisis Context ID may not be null") Long crisisContextID,
			@QueryParam("regConsumableID") @NotNull(message="Registered Consumable Resource ID may not be null") Long regconsumableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {

		RegisteredConsumableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredConsumableResourceByIdRemote(regconsumableID);

		ConsumableResourceDTO consumableResource = new ConsumableResourceDTO();
		consumableResource.setCrisisContextId(crisisContextID);
		consumableResource.setQuantity(regResourceDTO.getQuantity());
		consumableResource.setTitle(regResourceDTO.getTitle());
		consumableResource.setStatus(ResourceStatusDTO.AVAILABLE);
		ConsumableResourceCategoryDTO consumableCategoryDTO = (ConsumableResourceCategoryDTO) this.getResourceCategoryRemoteService().
				findByIdRemote(ConsumableResourceCategoryDTO.class, regResourceDTO.getConsumableResourceCategoryId()); 
		consumableResource.setConsumableResourceCategory(consumableCategoryDTO);
		this.getLogisticsRemoteService().updateConsumableResourceRemote(consumableResource, userID);


		regResourceDTO.setStatus(ResourceStatusDTO.RESERVED);
		this.getLogisticsRemoteService().updateRegisteredConsumableResourceRemote(regResourceDTO, userID);

		return consumableResource;
	}

	@PUT
	@Path("/crisis/deassociateConsumable")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredConsumableResourceDTO deassociateConsumable(
			@QueryParam("consumableID") @NotNull(message="Consumable Resource ID may not be null") Long consumableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {

		ConsumableResourceDTO consumableResource = this.getLogisticsRemoteService().findConsumableResourceByIdRemote(consumableID);
		RegisteredConsumableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredConsumableResourceByTitleRemote(consumableResource.getTitle());

		regResourceDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		this.getLogisticsRemoteService().updateRegisteredConsumableResourceRemote(regResourceDTO, userID);
		this.getLogisticsRemoteService().deleteConsumableResourceRemote(consumableID, userID);
		return regResourceDTO;
	}

	@PUT
	@Path("/crisis/associateoc")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreDTO associateOC(
			@QueryParam("ccID") @NotNull(message="Crisis Context Id may not be null") Long ccID,
			@QueryParam("regocID") @NotNull(message="Registerd Operations Centre ID may not be null") Long regocID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {

		RegisteredOperationsCentreDTO regOC = this.getOperationsCentreRemoteService().findRegisteredOperationCentreByIdRemote(regocID);

		OperationsCentreDTO oc = new OperationsCentreDTO();
		oc.setCrisisContextId(ccID);
		oc.setTitle(regOC.getTitle());
		oc.setType(regOC.getType());
		oc.setOperationsCentreCategoryId(regOC.getOperationsCentreCategoryId());
		oc.setVoIPURL(regOC.getVoIPURL());
		oc.setStatus(ResourceStatusDTO.AVAILABLE);

		oc = this.getOperationsCentreRemoteService().createOperationsCentreRemote(oc, userID);

		regOC.setStatus(ResourceStatusDTO.RESERVED);
		this.getOperationsCentreRemoteService().updateRegisteredOperationsCentreRemote(regOC, userID);

		return oc;
	}

	@PUT
	@Path("/crisis/deassociateoc")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredOperationsCentreDTO deassociateOC(
			@QueryParam("ocID") @NotNull(message="Operations Centre ID may not be null") Long ocID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {

		OperationsCentreDTO oc = this.getOperationsCentreRemoteService().findOperationCentreByIdRemote(ocID);
		RegisteredOperationsCentreDTO regOC = this.getOperationsCentreRemoteService().findRegisteredOperationsCentreByTitleRemote(oc.getTitle());
		regOC.setStatus(ResourceStatusDTO.AVAILABLE);
		regOC = this.getOperationsCentreRemoteService().updateRegisteredOperationsCentreRemote(regOC, userID);
		this.getOperationsCentreRemoteService().deleteOperationsCentreRemote(ocID, userID);
		return regOC;
	}
	
}