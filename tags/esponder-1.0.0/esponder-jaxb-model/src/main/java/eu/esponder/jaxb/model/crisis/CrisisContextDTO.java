package eu.esponder.jaxb.model.crisis;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import eu.esponder.jaxb.model.ESponderEntityDTO;

@XmlRootElement(name="crisisContext")
@XmlType(name="CrisisContext")
@JsonPropertyOrder({"title"})
public class CrisisContextDTO extends ESponderEntityDTO {
	
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String toString() {
		return "CrisisContextDTO [id=" + id + ", title=" + title + "]";
	}
		
}
