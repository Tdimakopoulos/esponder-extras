package eu.esponder.jaxb.model.crisis.action;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.snapshot.status.ActionSnapshotStatusDTO;


@XmlRootElement(name="actionScheduleCriteria")
@XmlType(name="ActionScheduleCriteria")
@JsonSerialize(include=Inclusion.NON_NULL)
public class ActionScheduleCriteriaDTO extends ESponderEntityDTO {

	private ActionSnapshotStatusDTO status;

	private XMLGregorianCalendar dateAfter;

	public ActionSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActionSnapshotStatusDTO status) {
		this.status = status;
	}

	public XMLGregorianCalendar getDateAfter() {
		return dateAfter;
	}

	public void setDateAfter(XMLGregorianCalendar dateAfter) {
		this.dateAfter = dateAfter;
	}

}
