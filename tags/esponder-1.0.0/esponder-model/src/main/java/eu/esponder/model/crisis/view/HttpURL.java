package eu.esponder.model.crisis.view;

import javax.persistence.Embeddable;

@Embeddable
public class HttpURL extends URL {

	private static final long serialVersionUID = -2776045310365314316L;

	public HttpURL() {
		this.setProtocol("http");
	}
}
