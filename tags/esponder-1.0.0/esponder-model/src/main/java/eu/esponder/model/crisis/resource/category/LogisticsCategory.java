package eu.esponder.model.crisis.resource.category;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class LogisticsCategory extends PlannableResourceCategory {

	private static final long serialVersionUID = 4667107943033401089L;

}