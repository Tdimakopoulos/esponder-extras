package eu.esponder.model.snapshot.action;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.snapshot.SpatialSnapshot;
import eu.esponder.model.snapshot.status.ActionSnapshotStatus;

@Entity
@Table(name="action_snapshot")
@NamedQueries({
	@NamedQuery(
		name="ActionSnapshot.findByActionAndDate",
		query="SELECT s FROM ActionSnapshot s WHERE s.action.id = :actionID AND s.period.dateTo <= :maxDate AND s.period.dateTo = " +
				"(SELECT max(s.period.dateTo) FROM ActionSnapshot s WHERE s.action.id = :actionID)"),
	@NamedQuery(
		name="ActionSnapshot.findPreviousSnapshot",
		query="Select s from ActionSnapshot s where s.id=(select max(snapshot.id) from ActionSnapshot snapshot where snapshot.action.id = :sensorID)")
})
public class ActionSnapshot extends SpatialSnapshot<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3858107002910361432L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_SNAPSHOT_ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="ACTION_ID", nullable=false)
	private Action action;
	
	@Enumerated(EnumType.STRING)
	@Column(name="ACTION_SNAPSHOT_STATUS", nullable=false)
	private ActionSnapshotStatus status;
	
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private ActionSnapshot previous;
	
	@OneToOne(mappedBy="previous")
	private ActionSnapshot next;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public ActionSnapshotStatus getStatus() {
		return status;
	}

	public void setStatus(ActionSnapshotStatus status) {
		this.status = status;
	}

	public ActionSnapshot getPrevious() {
		return previous;
	}

	public void setPrevious(ActionSnapshot previous) {
		this.previous = previous;
	}

	public ActionSnapshot getNext() {
		return next;
	}

	public void setNext(ActionSnapshot next) {
		this.next = next;
	}
	
}
