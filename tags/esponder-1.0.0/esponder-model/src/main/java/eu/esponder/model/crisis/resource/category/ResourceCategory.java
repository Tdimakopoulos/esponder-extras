package eu.esponder.model.crisis.resource.category;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import eu.esponder.model.ESponderEntity;

@MappedSuperclass
public abstract class ResourceCategory extends ESponderEntity<Long> {

	private static final long serialVersionUID = -2401622867316452380L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="CATEGORY_ID")
	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
