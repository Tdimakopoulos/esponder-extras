package eu.esponder.model.crisis.resource.category;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.Organisation;
import eu.esponder.model.type.DisciplineType;
import eu.esponder.model.type.OrganisationType;

@Entity
@Table(name="organisation_category")
@NamedQueries({
	@NamedQuery(name="OrganisationCategory.findByType", query="select a from OrganisationCategory a where a.disciplineType=:disciplineType and a.organisationType=:organisationType") })
public class OrganisationCategory extends ResourceCategory {

	private static final long serialVersionUID = -1406195628572785186L;
	
	@OneToOne
	@JoinColumn(name="DISCIPLINE_TYPE", nullable=false)
	private DisciplineType disciplineType;
	
	/**
	 * Models the hierarchy of internal organisation elements of the particular organisation, 
	 * e.g. for FireFighters: Headquarters, Fire Stations, Garages etc., 
	 * thus advanced searching is enabled.
	 */
	@OneToOne
	@JoinColumn(name="ORG_TYPE", nullable=false)
	private OrganisationType organisationType;
	
	@ManyToOne
	@JoinColumn(name="ORGANISATION_ID")
	private Organisation organisation;
	
//	@OneToMany(mappedBy="organisationCategory")
//	private Set<PersonnelCategory> personnelCategories;

//	public Set<PersonnelCategory> getPersonnelCategories() {
//		return personnelCategories;
//	}
//
//	public void setPersonnelCategories(Set<PersonnelCategory> personnelCategories) {
//		this.personnelCategories = personnelCategories;
//	}

	public DisciplineType getDisciplineType() {
		return disciplineType;
	}

	public Organisation getOrganisation() {
		return organisation;
	}

	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}

	public void setDisciplineType(DisciplineType disciplineType) {
		this.disciplineType = disciplineType;
	}

	public OrganisationType getOrganisationType() {
		return organisationType;
	}

	public void setOrganisationType(OrganisationType organisationType) {
		this.organisationType = organisationType;
	}
	

}
