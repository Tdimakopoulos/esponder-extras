package eu.esponder.model.crisis.resource;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.crisis.view.VoIPURL;

/**
 * @author gleo
 *
 */
@Entity
@Table(name="registered_operations_centre")
@NamedQueries({
	@NamedQuery(name="RegisteredOperationsCentre.findByTitle", query="select roc from RegisteredOperationsCentre roc where roc.title=:title"),
	@NamedQuery(name="RegisteredOperationsCentre.findAll", query="select roc from RegisteredOperationsCentre roc")
})
public class RegisteredOperationsCentre extends Resource {

	private static final long serialVersionUID = 3363141808683972817L;
	
	/**
	 * TODO: Fix this to point to OperationsCentreCategory. For now we just keep the correct OperationsCentreCategoryId
	 */
//	@ManyToOne
//	@JoinColumn(name="CATEGORY_ID")
//	private OperationsCentreCategory operationsCentreCategory;
	@Column(name="OC_CATEGORY_ID")
	private Long operationsCentreCategoryId;

	@Embedded
	private VoIPURL voIPURL;

	public Long getOperationsCentreCategoryId() {
		return operationsCentreCategoryId;
	}

	public void setOperationsCentreCategoryId(Long operationsCentreCategoryId) {
		this.operationsCentreCategoryId = operationsCentreCategoryId;
	}

	public VoIPURL getVoIPURL() {
		return voIPURL;
	}

	public void setVoIPURL(VoIPURL voIPURL) {
		this.voIPURL = voIPURL;
	}

	
}
