package eu.esponder.model.crisis.resource;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="registered_consumable_resource")
@NamedQueries({
	@NamedQuery(name="RegisteredConsumableResource.findByTitle", query="select rcr from RegisteredConsumableResource rcr where rcr.title=:title"),
	@NamedQuery(name="RegisteredConsumableResource.findAll", query="select rcr from RegisteredConsumableResource rcr")
})
public class RegisteredConsumableResource extends Resource {
	
	
	private static final long serialVersionUID = -3763207365585553160L;

	@Column(name="QUANTITY")
	private BigDecimal quantity;
	
	@ManyToOne
	@JoinColumn(name="REUSABLE_RESOURCE_ID")
	private RegisteredReusableResource container;
	
	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public RegisteredReusableResource getContainer() {
		return container;
	}

	public void setContainer(RegisteredReusableResource container) {
		this.container = container;
	}

	public Long getConsumableResourceCategoryId() {
		return consumableResourceCategoryId;
	}

	public void setConsumableResourceCategoryId(Long consumableResourceCategoryId) {
		this.consumableResourceCategoryId = consumableResourceCategoryId;
	}

	/**
	 * TODO: Fix this to point to OperationsCentreCategory. For now we just keep the correct OperationsCentreCategoryId
	 */
//	@ManyToOne
//	@JoinColumn(name="CATEGORY_ID")
//	private ConsumableResourceCategory consumableResourceCategory;
	@Column(name="CATEGORY_ID")
	private Long consumableResourceCategoryId;
	
}
