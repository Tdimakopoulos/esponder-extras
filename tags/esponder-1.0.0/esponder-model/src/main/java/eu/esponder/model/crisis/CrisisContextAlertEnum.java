package eu.esponder.model.crisis;

public enum CrisisContextAlertEnum {
	
	Level1,
	Level2,
	Level3,
	Level4,
	Level5
}
