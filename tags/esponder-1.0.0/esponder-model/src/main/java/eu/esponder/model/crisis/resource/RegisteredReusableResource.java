package eu.esponder.model.crisis.resource;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="registered_reusable_resource")
@NamedQueries({
	@NamedQuery(name="RegisteredReusableResource.findByTitle", query="select rrr from RegisteredReusableResource rrr where rrr.title=:title"),
	@NamedQuery(name="RegisteredReusableResource.findAll", query="select rrr from RegisteredReusableResource rrr")
})
public class RegisteredReusableResource extends Resource {
	
	private static final long serialVersionUID = -2211872166654303250L;

	@Column(name="QUANTITY")
	private BigDecimal quantity;
	
	@OneToMany(mappedBy="container")
	private Set<RegisteredConsumableResource> registeredConsumables;
	
	@ManyToOne
	@JoinColumn(name="PARENT_ID")
	private RegisteredReusableResource parent;
	
	@OneToMany(mappedBy="parent")
	private Set<RegisteredReusableResource> children;
	
	/**
	 * TODO: Fix this to point to OperationsCentreCategory. For now we just keep the correct OperationsCentreCategoryId
	 */
//	@ManyToOne
//	@JoinColumn(name="CATEGORY_ID")
//	private ReusableResourceCategory reusableResourceCategory;
	@Column(name="CATEGORY_ID")
	private Long reusableResourceCategoryId;

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Set<RegisteredConsumableResource> getRegisteredConsumables() {
		return registeredConsumables;
	}

	public void setRegisteredConsumables(
			Set<RegisteredConsumableResource> registeredConsumables) {
		this.registeredConsumables = registeredConsumables;
	}

	public RegisteredReusableResource getParent() {
		return parent;
	}

	public void setParent(RegisteredReusableResource parent) {
		this.parent = parent;
	}

	public Set<RegisteredReusableResource> getChildren() {
		return children;
	}

	public void setChildren(Set<RegisteredReusableResource> children) {
		this.children = children;
	}

	public Long getReusableResourceCategoryId() {
		return reusableResourceCategoryId;
	}

	public void setReusableResourceCategoryId(Long reusableResourceCategoryId) {
		this.reusableResourceCategoryId = reusableResourceCategoryId;
	}

	
}
