package eu.esponder.model.snapshot.status;

public enum ActorSnapshotStatus {
	READY,
	ACTIVE,
	INACTIVE
}
