package eu.esponder.model.crisis.resource.plan;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import eu.esponder.model.crisis.resource.Resource;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class PlannableResource extends Resource {

	private static final long serialVersionUID = 6210270086779113742L;

}
