package eu.esponder.model.snapshot.status;

public enum MeasurementUnitEnum {
	
	//Temperature
	DEGREES_CELCIUS,
	DEGREES_FAHRENHEIT,
	
	//Time
	SECONDS,
	MILLISECONDS,
	
	//Gas Concentration
	PPM,
	
	//Position (for each of lon, lat, alt)
	DEGREES,
	
	//Hearbeat Rate
	BBM

}
