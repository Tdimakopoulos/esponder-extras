package eu.esponder.model.crisis.resource.category;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.type.ReusableResourceType;

@Entity
@Table(name="reusables_category")
@NamedQueries({
	@NamedQuery(name="ReusableResourceCategory.findByType", query="select a from ReusableResourceCategory a where a.reusableResourceType=:reusableResourceType") })
public class ReusableResourceCategory extends LogisticsCategory {

	private static final long serialVersionUID = 145276614065666955L;

	@OneToOne
	@JoinColumn(name="REUSABLE_RESOURCE_TYPE_ID", nullable=false)
	private ReusableResourceType reusableResourceType;

//	@OneToMany(mappedBy="reusableResourceCategory")
//	private Set<ReusableResource> reusableResources;

	public ReusableResourceType getReusableResourceType() {
		return reusableResourceType;
	}

	public void setReusableResourceType(ReusableResourceType reusableResourceType) {
		this.reusableResourceType = reusableResourceType;
	}

//	public Set<ReusableResource> getReusableResources() {
//		return reusableResources;
//	}
//
//	public void setReusableResources(Set<ReusableResource> reusableResources) {
//		this.reusableResources = reusableResources;
//	} 

}
