package eu.esponder.model.crisis.resource;

public enum ResourceStatus {
	AVAILABLE,
	UNAVAILABLE,
	RESERVED,
	ALLOCATED
}
