package eu.esponder.model.crisis.resource;

import javax.persistence.MappedSuperclass;

import eu.esponder.model.crisis.resource.plan.PlannableResource;

@MappedSuperclass
public abstract class LogisticsResource extends PlannableResource {

	private static final long serialVersionUID = -4407982840446904207L;

}
