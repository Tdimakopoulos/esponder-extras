package eu.esponder.fru.helper;

import java.util.Calendar;

public class Utils {
	
	public static String getTimeString() {
		Calendar cal = Calendar.getInstance();

		int hour = cal.get(Calendar.HOUR);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		
		return new String(hour + ":" + minute + ":" + second);
	}

}
