package eu.esponder.dto.model.crisis.resource.category;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.type.EquipmentTypeDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "equipmentType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EquipmentCategoryDTO extends PlannableResourceCategoryDTO {

	private static final long serialVersionUID = -4964121325750331976L;

	private EquipmentTypeDTO equipmentType;
	
//	private Set<EquipmentDTO> equipment;

	public EquipmentTypeDTO getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(EquipmentTypeDTO equipmentType) {
		this.equipmentType = equipmentType;
	}

//	public Set<EquipmentDTO> getEquipment() {
//		return equipment;
//	}
//
//	public void setEquipment(Set<EquipmentDTO> equipment) {
//		this.equipment = equipment;
//	}
	
}
