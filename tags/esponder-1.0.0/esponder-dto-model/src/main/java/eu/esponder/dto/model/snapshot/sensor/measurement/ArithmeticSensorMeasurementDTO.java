package eu.esponder.dto.model.snapshot.sensor.measurement;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "sensor", "timestamp", "measurement"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ArithmeticSensorMeasurementDTO extends SensorMeasurementDTO {

	private static final long serialVersionUID = 8231515158702428532L;
	
	private BigDecimal measurement;

	public BigDecimal getMeasurement() {
		return measurement;
	}

	public void setMeasurement(BigDecimal measurement) {
		this.measurement = measurement;
	}

}
