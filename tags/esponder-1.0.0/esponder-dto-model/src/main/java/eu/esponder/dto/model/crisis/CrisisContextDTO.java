package eu.esponder.dto.model.crisis;

import java.sql.Date;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "crisisLocation", "operationsCentres", "additionalInfo","startDate", "endDate", "crisiContextAlert"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CrisisContextDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = -7067439377510286686L;

	private String title;
	
	private SphereDTO crisisLocation;
	
	private Set<OperationsCentreDTO> operationsCentres; 

    private String additionalInfo;
	
	private Date startDate;
	
	private Date endDate;
	
	private Set<CrisisTypeDTO> crisisTypes;
	
	private CrisisContextAlertEnumDTO crisisContextAlert;
	
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	
	
	public String getTitle() {
		return title;
	}

	public Set<OperationsCentreDTO> getOperationsCentres() {
		return operationsCentres;
	}

	public void setOperationsCentres(Set<OperationsCentreDTO> operationsCentres) {
		this.operationsCentres = operationsCentres;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String toString() {
		return "CrisisContextDTO [id=" + id + ", title=" + title + "]";
	}

	public SphereDTO getCrisisLocation() {
		return crisisLocation;
	}

	public void setCrisisLocation(SphereDTO crisisLocation) {
		this.crisisLocation = crisisLocation;
	}

	public CrisisContextAlertEnumDTO getCrisisContextAlert() {
		return crisisContextAlert;
	}

	public void setCrisisContextAlert(CrisisContextAlertEnumDTO crisisContextAlert) {
		this.crisisContextAlert = crisisContextAlert;
	}

	public Set<CrisisTypeDTO> getCrisisTypes() {
		return crisisTypes;
	}

	public void setCrisisTypes(Set<CrisisTypeDTO> crisisTypes) {
		this.crisisTypes = crisisTypes;
	}
	
}
