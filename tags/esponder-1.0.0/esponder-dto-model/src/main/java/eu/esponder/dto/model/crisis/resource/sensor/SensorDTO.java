package eu.esponder.dto.model.crisis.resource.sensor;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class SensorDTO extends ResourceDTO {
	
	private static final long serialVersionUID = 7077059831872243408L;

	public SensorDTO() { }
	
	private String name;
	
	private MeasurementUnitEnumDTO measurementUnit;
	
	private Set<StatisticsConfigDTO> configuration;
	
	private String label;
	
	private Long equipmentId;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MeasurementUnitEnumDTO getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(MeasurementUnitEnumDTO measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(null == obj || obj instanceof SensorDTO)) return false;
		
		SensorDTO sensor = (SensorDTO) obj;
		if (!(sensor.getClass() == SensorDTO.class)) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "SensorDTO [name=" + name + ", type=" + type + ", measurementUnit="
				+ measurementUnit + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}
	
	public Set<StatisticsConfigDTO> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Set<StatisticsConfigDTO> configuration) {
		this.configuration = configuration;
	}

	public Long getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(Long equipmentId) {
		this.equipmentId = equipmentId;
	}
	
}
