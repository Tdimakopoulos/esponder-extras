package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "parent", "children"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OperationalActionTypeDTO extends ActionTypeDTO {
	
	private static final long serialVersionUID = -8445886879200324066L;
	
	private TacticalActionTypeDTO parent;
	
	public TacticalActionTypeDTO getParent() {
		return parent;
	}

	public void setParent(TacticalActionTypeDTO parent) {
		this.parent = parent;
	}
	
	

}
