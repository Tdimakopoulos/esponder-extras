package eu.esponder.dto.model.crisis.resource.plan;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "crisisResourcePlan", "planableResource", "power", "constraint"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PlannableResourcePowerDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = -7083280767020268843L;
	
	private Long crisisResourcePlanId;
	
//	private PlannableResourceCategoryDTO planableResourceCategory;
	
	private Long planableResourceCategoryId;
	
	private Integer power;
	
	/**
	 * FIXME: Refactor this to an Enum indicating
	 * <  -> Less than
	 * <= -> Less than or Equal
	 * == -> Equal
	 * => -> Greater than or Equal
	 * >  -> Greater than
	 */
	private String constraint;

	public Integer getPower() {
		return power;
	}

	public void setPower(Integer power) {
		this.power = power;
	}

	public String getConstraint() {
		return constraint;
	}

	public void setConstraint(String constraint) {
		this.constraint = constraint;
	}

	public Long getCrisisResourcePlanId() {
		return crisisResourcePlanId;
	}

	public void setCrisisResourcePlanId(Long crisisResourcePlanId) {
		this.crisisResourcePlanId = crisisResourcePlanId;
	}

	public Long getPlanableResourceCategoryId() {
		return planableResourceCategoryId;
	}

	public void setPlanableResourceCategoryId(Long planableResourceCategoryId) {
		this.planableResourceCategoryId = planableResourceCategoryId;
	}
	
}
