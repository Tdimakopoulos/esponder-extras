package eu.esponder.dto.model.snapshot;

import java.io.Serializable;
import java.sql.Timestamp;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"dateFrom", "dateTo"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PeriodDTO implements Serializable {
	
	private static final long serialVersionUID = -2870132067764797692L;
	
//	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Timestamp dateFrom;

//	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Timestamp dateTo;
	
	public PeriodDTO() { }
	
	public PeriodDTO(Timestamp dateFrom, Timestamp dateTo) {
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}
	
	//FIXME Find a solution for serialization of date fields in PeriodDTO
	
//	public PeriodDTO(XMLGregorianCalendar dateFrom, XMLGregorianCalendar dateTo) {
//		super();
//		this.dateFrom = dateFrom;
//		this.dateTo = dateTo;
//	}
	
//	public XMLGregorianCalendar getDateFrom() {
//		return dateFrom;
//	}
//
//	public void setDateFrom(XMLGregorianCalendar dateFrom) {
//		this.dateFrom = dateFrom;
//	}
//
//	public XMLGregorianCalendar getDateTo() {
//		return dateTo;
//	}
//
//	public void setDateTo(XMLGregorianCalendar dateTo) {
//		this.dateTo = dateTo;
//	}

	public Timestamp getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Timestamp dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Timestamp getDateTo() {
		return dateTo;
	}

	public void setDateTo(Timestamp dateTo) {
		this.dateTo = dateTo;
	}

	@Override
	public String toString() {
		return "Period [dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}

}
