package eu.esponder.dto.model.config;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "parameterName", "parameterValue", "description"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OSGIConfigurationDTO extends ESponderConfigParameterDTO {

	private static final long serialVersionUID = -3533725912580499884L;
	
	public OSGIConfigurationDTO() {};

}
