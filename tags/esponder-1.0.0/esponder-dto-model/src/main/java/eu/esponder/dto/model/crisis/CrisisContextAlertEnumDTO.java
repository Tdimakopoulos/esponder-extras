package eu.esponder.dto.model.crisis;

public enum CrisisContextAlertEnumDTO {
	
	Level1,
	Level2,
	Level3,
	Level4,
	Level5
}
