package eu.esponder.dto.model.crisis.action;



public enum SeverityLevelDTO {
	/*MINIMAL,
	MEDIUM,
	SERIOUS,
	SEVERE,
	FATAL,
	UNDEFINED*/
	
	MINIMAL((byte) 0),
	MEDIUM((byte) 1),
	SERIOUS((byte) 2),
	SEVERE((byte) 3),
	FATAL((byte) 4),
	UNDEFINED((byte) -1);
	
	public static int getSeverityColor(byte value) {
		switch (value) {
		case (byte)-1:
			return 0xFFBBD1F0; 
		case (byte)0:
			return 0xFFBBF0CD;
		case (byte)1:
			return 0xFFEBF765;
		case (byte)2:
			return 0xFFF0BBBC; 
		case (byte)3:
			return 0xFFF2686B; 
		case (byte)4:
			return 0xFFF0373B; 
		}
		return 0;
	}
	
	private byte value;

	private SeverityLevelDTO(byte value) {
		this.value = value;
	}

	public byte getValue() {
		return value;
	}

	public void setValue(byte value) {
		this.value = value;
	}
}
