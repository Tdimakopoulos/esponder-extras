package eu.esponder.df.actorlookup.test;

import org.testng.annotations.Test;

import eu.esponder.df.ruleengine.utilities.RuleLookup;
import eu.esponder.df.ruleengine.utilities.RuleLookup.RuleLookupType;
import eu.esponder.df.ruleengine.utilities.actorlookup.ActorXMLFileEntry;

public class ActorLookupTest {

	@Test
	public void TestWriteFile() {
		RuleLookup pLookup = new RuleLookup();
		pLookup.Initialize();
		ActorXMLFileEntry pEntry = new ActorXMLFileEntry();
		ActorXMLFileEntry pEntry2 = new ActorXMLFileEntry();
		pEntry.setdDouble1(37.000);
		pEntry.setdDouble2(37.000);
		pEntry.setdDouble3(39.000);
		pEntry.setdDouble4(32.000);
		pEntry.setSzString1("Operation");
		pEntry.setSzString2("Move");
		pEntry.setSzString3("FR1");
		pEntry.setSzString4("Acropoli");
		pEntry.setSzString5("N/A");
		pEntry.setSzString6("N/A");

		pLookup.AddXMLEntry(pEntry);

		pEntry2.setdDouble1(39.000);
		pEntry2.setdDouble2(39.000);
		pEntry2.setdDouble3(39.000);
		pEntry2.setdDouble4(39.000);
		pEntry2.setSzString1("Operation");
		pEntry2.setSzString2("Move");
		pEntry2.setSzString3("FR2");
		pEntry2.setSzString4("Acropoli");
		pEntry2.setSzString5("N/A");
		pEntry2.setSzString6("N/A");
		pLookup.AddXMLEntry(pEntry2);

		try {
			pLookup.WriteXMLFile(RuleLookupType.DLU_ACTORS);
		} catch (Exception e) {
			System.out
					.println("Error - Writing Action XML FIle - Rule Lookup - Msg : "
							+ e.getMessage());
			e.printStackTrace();
		}

	}

	@Test
	public void TestReadFile() {
		RuleLookup pLookup = new RuleLookup();
		pLookup.Initialize();
		try {
			pLookup.ReadXMLFile(RuleLookupType.DLU_ACTORS);
			System.out.println("Total Read : " + pLookup.pEntries.size());
		} catch (Exception e) {
			System.out
					.println("Error - Read Action XML FIle - Rule Lookup - Msg : "
							+ e.getMessage());
			e.printStackTrace();
		}
	}

	@Test
	public void TestActorFindOnLookup() {
		RuleLookup pLookup = new RuleLookup();
		pLookup.Initialize();
		System.out.println("Check for FR1 exact match (letter case) "
				+ pLookup.NewActorLocationMeasurment("Actor Moving","Moving",
						"FR1", "n/a", 0.989, 0.32543, 0.23423, 10.1));
		System.out.println("Check for FR1 no letter case match "
				+ pLookup.NewActorLocationMeasurment("Actor Moving","Moving",
						"Fr1", "n/a", 0.989, 0.32543, 0.23423, 10.1));
	}

}
