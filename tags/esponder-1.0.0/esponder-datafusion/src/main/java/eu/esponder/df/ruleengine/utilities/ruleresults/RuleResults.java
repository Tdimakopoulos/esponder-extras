package eu.esponder.df.ruleengine.utilities.ruleresults;

import java.io.Serializable;

import eu.esponder.df.ruleengine.utilities.RuleLookup;
import eu.esponder.df.ruleengine.utilities.RuleLookup.RuleLookupType;
import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;

public class RuleResults implements Serializable {
	
	private static final long serialVersionUID = 5287233281894129154L;

	public void RuleResult(String szRuleName,String szResults1,String szResults2,String szResults3)
	{
		RuleLookup pLookup= new RuleLookup();
		try {
			pLookup.ReadXMLFile(RuleLookupType.DLU_RULERESULTS);
		} catch (Exception e) {
			System.out.println("Rule Results Error (Read XML File) : "+e.getMessage());
		}
		
		RuleResultsXML pNewResults= new RuleResultsXML();
		pNewResults.setSzRuleName(szRuleName);
		pNewResults.setSzResultsText1(szResults1);
		pNewResults.setSzResultsText2(szResults2);
		pNewResults.setSzResultsText3(szResults3);
		pLookup.AddRuleResults(pNewResults);
		try {
			pLookup.WriteXMLFile(RuleLookupType.DLU_RULERESULTS);
		} catch (Exception e) {
			System.out.println("Rule Results Error (Update XML File) : "+e.getMessage());
		}
	}
}
