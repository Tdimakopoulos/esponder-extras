package eu.esponder.df.eventhandler.bean;

import eu.esponder.df.eventhandler.ActionEventHandlerRemoteService;
import eu.esponder.df.eventhandler.ActionEventHandlerService;
import eu.esponder.df.rules.profile.ProfileManager;
import eu.esponder.event.model.ESponderEvent;

public class ActionEventHandlerBean extends dfEventHandlerBean
		implements ActionEventHandlerService,
		ActionEventHandlerRemoteService {

	public void ProcessEvent(ESponderEvent<?> pEvent)
	{
		ProfileManager pProfileManager=new ProfileManager();
		
		
		//Set rule profile for action event
		SetRuleEngineType(pProfileManager.GetProfileNameForAction().getSzProfileType(),pProfileManager.GetProfileNameForAction().getSzProfileName());

		//Load Rules
		LoadKnowledge();
		
		//Add Measurements object list
		//AddDTOObjects(pSensorSnapshot);

		//Run Rules
		ProcessRules();
	}
}
