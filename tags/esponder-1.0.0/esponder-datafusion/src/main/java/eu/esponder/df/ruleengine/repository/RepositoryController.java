package eu.esponder.df.ruleengine.repository;

import java.io.File;

import eu.esponder.df.ruleengine.settings.DroolSettings;

public class RepositoryController {
	DroolSettings dSettings = new DroolSettings();
	
	public boolean RefreshRequest(String szAssetName,boolean bXML)
	{
		//System.out.println("********** Update Request ********");
		if (dSettings.getSzDroolRepo().equalsIgnoreCase("guvnor"))
		{
			//System.out.println("********** Delete Repo ********");
			EmptyRepository();
			dSettings.setSzDroolRepo("local");
		}
		String szFilename=null;
		if(bXML)
			szFilename=dSettings.getSzTmpRepositoryDirectoryPath()+szAssetName+".xml";
		else
			szFilename=dSettings.getSzTmpRepositoryDirectoryPath()+szAssetName+".ast";
		
		 File file=new File(szFilename);
		 if(file.exists())
		 {
			 //System.out.println("********** Update Request - Use Local********");
			 return false;
		 }
		 else
		 {
			 //System.out.println("********** Update Request - Get from guvnor ********");
			 //file is not exists get from guvnor
			 return true;
		 }
	}


	/**
	 * Empty local repository
	 */
	private void EmptyRepository() {
		DeleteDirectory(dSettings.getSzTmpRepositoryDirectoryPath());
	}

	/**
	 * @param directoryName
	 *            the directory to delete all files inside
	 */
	private void DeleteDirectory(String directoryName) {
		File directory = new File(directoryName);
		// Get all files in directory
		File[] files = directory.listFiles();
		for (File file : files) {
			// Delete each file
			if (!file.delete()) {
				// Failed to delete file
				System.out.println("Failed to delete " + file);
			}
		}
	}
}
