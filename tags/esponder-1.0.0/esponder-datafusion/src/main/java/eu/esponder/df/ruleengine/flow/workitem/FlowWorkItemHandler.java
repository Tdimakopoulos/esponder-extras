package eu.esponder.df.ruleengine.flow.workitem;

import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemHandler;
import org.drools.runtime.process.WorkItemManager;

public class FlowWorkItemHandler implements WorkItemHandler{
	@Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        //System.out.println("Executing work item " + workItem);
        manager.completeWorkItem(workItem.getId(), null);
    }
    
    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        // Do nothing here
    }
}
