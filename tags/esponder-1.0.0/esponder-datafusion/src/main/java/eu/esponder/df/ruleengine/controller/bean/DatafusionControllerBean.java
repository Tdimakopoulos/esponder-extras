package eu.esponder.df.ruleengine.controller.bean;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import eu.esponder.controller.persistence.CrudRemoteService;
import eu.esponder.df.eventhandler.bean.ActionEventHandlerBean;
import eu.esponder.df.eventhandler.bean.SensorMeasurmentEventHandlerBean;
import eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService;
import eu.esponder.df.ruleengine.controller.DatafusionControllerService;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.model.events.entity.OsgiEventsEntity;
import eu.esponder.test.ResourceLocator;



public class DatafusionControllerBean implements DatafusionControllerRemoteService,DatafusionControllerService {
	
	private CrudRemoteService<OsgiEventsEntity> crudService = ResourceLocator.lookup("esponder/CrudBean/remote");

	private String szName;

	private String szEventTypeSensorMeasurment="CreateSensorMeasurementStatisticEvent";
	private String szEventTypeAction="CreateActionEvent";

	@Override
	public void EsponderEventReceivedHandler(ESponderEvent<?> pEvent) {
		//System.out.println("########### DF EVENT Handler - EVENT RECEIVED #############");

		
		szName = pEvent.getClass().getSimpleName();

		//System.out.println("=====> Event Type (CreateSensorMeasurementStatisticEvent) DF : --- "+szName);

		PersistEvent(pEvent);
		
		if (szName.equalsIgnoreCase(szEventTypeSensorMeasurment))
		{
			//System.out.println("=====> Start the process of Sensor Measurment Event");
			SensorMeasurmentEventHandlerBean pHandler = new SensorMeasurmentEventHandlerBean();
			pHandler.ProcessEvent(pEvent);
		}

		if (szName.equalsIgnoreCase(szEventTypeAction))
		{
			//System.out.println("=====> Start the process of Action Event");
			ActionEventHandlerBean pHandler = new ActionEventHandlerBean();
			pHandler.ProcessEvent(pEvent);
		}
	}
	
	
	private void PersistEvent(ESponderEvent<? extends ESponderEntityDTO> event) {
		
		OsgiEventsEntity dEntity = new OsgiEventsEntity();

		dEntity.setJournalMsg(event.getJournalMessage());
		
		//dEntity.setJournalMsgInfo(event.getJournalMessageInfo());

		dEntity.setSeverity(event.getEventSeverity().toString());

		dEntity.setSourceid(event.getEventSource().getId());

		dEntity.setTimeStamp(event.getEventTimestamp());
		
		//////////////////////////////////////////////////////////////
		//FIXME convert object to string and save it into the database
		ObjectMapper mapper = new ObjectMapper();
		String attachment = null;
		try {
			attachment = mapper.writeValueAsString(event.getEventAttachment());
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		dEntity.setAttachment(event.getEventAttachment().toString());
		dEntity.setAttachment(attachment);
		//////////////////////////////////////////////////////////////
		
		dEntity.setSource(event.getEventSource().toString());
		
		
		//System.out.println("----> Lengths : "+event.getJournalMessage().length()+"  -- "+event.getEventAttachment().toString().length()+" -- "+event.getEventSource().toString().length());
		
		
		if(crudService != null){
			OsgiEventsEntity entityPersisted = crudService.create(dEntity);
			//System.out.println("Persisted Event id : "+entityPersisted.getId());
		}
		else
			System.out.println("Null error, cannot persist event...");
	}
	
	@SuppressWarnings("unused")
	private void printEvent(ESponderEvent<? extends ESponderEntityDTO> event) {
		System.out.println("########### DF EVENT Handler - EVENT RECEIVED #############");
		System.out.println("########### EVENT DETAILS START #############");
		System.out.println("CSSE # " + event.toString());
		System.out.println("CSSE attachment # " + event.getEventAttachment().toString());
		System.out.println("CSSE severity # " + event.getEventSeverity());
		System.out.println("CSSE source # " + event.getEventSource());
		System.out.println("CSSE timestamp# " + event.getEventTimestamp());
		System.out.println("########### EVENT DETAILS END #############");

	}
}
