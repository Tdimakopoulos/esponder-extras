package eu.esponder.df.ruleengine.utilities.actorlookup;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class ActorXMLFileEntry {

	// operation
	@Element
	private String szString1;

	// action
	@Element
	private String szString2;

	// actor
	@Element
	private String szString3;

	// place name
	@Element
	private String szString4;

	// date and time
	@Element
	private String szString5;

	@Element
	private String szString6;

	@Element
	private int iInt1;
	@Element
	private int iInt2;
	@Element
	private int iInt3;
	@Element
	private int iInt4;
	@Element
	private int iInt5;
	@Element
	private int iInt6;

	// lat
	@Element
	private double dDouble1;

	// lot
	@Element
	private double dDouble2;

	// alt
	@Element
	private double dDouble3;

	// radius
	@Element
	private double dDouble4;
	@Element
	private double dDouble5;
	@Element
	private double dDouble6;

	public String getSzString3() {
		return szString3;
	}

	public void setSzString3(String szString3) {
		this.szString3 = szString3;
	}

	public String getSzString4() {
		return szString4;
	}

	public void setSzString4(String szString4) {
		this.szString4 = szString4;
	}

	public String getSzString5() {
		return szString5;
	}

	public void setSzString5(String szString5) {
		this.szString5 = szString5;
	}

	public String getSzString6() {
		return szString6;
	}

	public void setSzString6(String szString6) {
		this.szString6 = szString6;
	}

	public int getiInt3() {
		return iInt3;
	}

	public void setiInt3(int iInt3) {
		this.iInt3 = iInt3;
	}

	public int getiInt4() {
		return iInt4;
	}

	public void setiInt4(int iInt4) {
		this.iInt4 = iInt4;
	}

	public int getiInt5() {
		return iInt5;
	}

	public void setiInt5(int iInt5) {
		this.iInt5 = iInt5;
	}

	public int getiInt6() {
		return iInt6;
	}

	public void setiInt6(int iInt6) {
		this.iInt6 = iInt6;
	}

	public double getdDouble1() {
		return dDouble1;
	}

	public void setdDouble1(double dDouble1) {
		this.dDouble1 = dDouble1;
	}

	public double getdDouble2() {
		return dDouble2;
	}

	public void setdDouble2(double dDouble2) {
		this.dDouble2 = dDouble2;
	}

	public double getdDouble3() {
		return dDouble3;
	}

	public void setdDouble3(double dDouble3) {
		this.dDouble3 = dDouble3;
	}

	public double getdDouble4() {
		return dDouble4;
	}

	public void setdDouble4(double dDouble4) {
		this.dDouble4 = dDouble4;
	}

	public double getdDouble5() {
		return dDouble5;
	}

	public void setdDouble5(double dDouble5) {
		this.dDouble5 = dDouble5;
	}

	public double getdDouble6() {
		return dDouble6;
	}

	public void setdDouble6(double dDouble6) {
		this.dDouble6 = dDouble6;
	}

	public String getSzString1() {
		return szString1;
	}

	public void setSzString1(String szString1) {
		this.szString1 = szString1;
	}

	public String getSzString2() {
		return szString2;
	}

	public void setSzString2(String szString2) {
		this.szString2 = szString2;
	}

	public int getiInt1() {
		return iInt1;
	}

	public void setiInt1(int iInt1) {
		this.iInt1 = iInt1;
	}

	public int getiInt2() {
		return iInt2;
	}

	public void setiInt2(int iInt2) {
		this.iInt2 = iInt2;
	}

	public String getOperation() {
		return this.getSzString1();
	}

	public String getAction() {
		return this.getSzString2();
	}

	public String getActor() {
		return this.getSzString3();
	}

	public String getPlaceName() {
		return this.getSzString4();
	}

	public String getTimeStamp() {
		return this.getSzString5();
	}

	public double getLat() {
		return this.getdDouble1();
	}

	public double getLot() {
		return this.getdDouble2();
	}

	public double getAlt() {
		return this.getdDouble3();
	}

	public double getRadius() {
		return this.getdDouble4();
	}
}
