package eu.esponder.df.eventhandler;

import eu.esponder.event.model.ESponderEvent;

public interface ActionEventHandlerRemoteService extends
ActionEventHandlerService {
	public void ProcessEvent(ESponderEvent<?> pEvent);
}
