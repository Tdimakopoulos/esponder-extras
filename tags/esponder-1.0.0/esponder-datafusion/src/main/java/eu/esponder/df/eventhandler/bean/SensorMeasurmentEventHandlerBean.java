package eu.esponder.df.eventhandler.bean;

import java.util.List;

import eu.esponder.df.eventhandler.SensorMeasurmentEventHandlerRemoteService;
import eu.esponder.df.eventhandler.SensorMeasurmentEventHandlerService;
import eu.esponder.df.rules.profile.ProfileManager;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.event.model.ESponderEvent;

public class SensorMeasurmentEventHandlerBean extends dfEventHandlerBean
		implements SensorMeasurmentEventHandlerService,
		SensorMeasurmentEventHandlerRemoteService {

	public void ProcessEvent(ESponderEvent<?> pEvent)
	{
		System.out.println("******* Sensor Snapshot Event Handler ***********");
		ProfileManager pProfileManager=new ProfileManager();
		
		
		@SuppressWarnings("unused")
		List<Object> pSensorSnapshot=CreateSensorSnapshot(pEvent);
		
		//System.out.println("******* Sensor Snapshot Created Size : "+pSensorSnapshot.size()+" Sensor Snapshots Size : "+ createdSensorSnapshots.size()+" Location Snapshots Size : "+createdActorSnapshots.size());
		
		for (int i=0;i<createdSensorSnapshots.size();i++)
		{
			//System.out.println("******* Run Rules for Snapshot : "+i);
			
			//System.out.println("******* DF : Setup Profiles ");
			
			//Set rule profile for sensor measurement
			SetRuleEngineType(pProfileManager.GetProfileNameForSensor().getSzProfileType(),pProfileManager.GetProfileNameForSensor().getSzProfileName());
	
			//System.out.println("******* DF : Load Knowledge ");
			//Load Rules
			LoadKnowledge();

			//System.out.println("******* DF : Create Single snapshot ");
			
			//Get current snapshot
			SensorSnapshotDTO createdSensorSnapshot = new SensorSnapshotDTO();
			createdSensorSnapshot.setId(createdSensorSnapshots.get(i).getId());
			createdSensorSnapshot.setPeriod(createdSensorSnapshots.get(i).getPeriod());
			createdSensorSnapshot.setSensor(createdSensorSnapshots.get(i).getSensor());
			createdSensorSnapshot.setStatisticType(createdSensorSnapshots.get(i).getStatisticType());
			createdSensorSnapshot.setStatus(createdSensorSnapshots.get(i).getStatus());
			createdSensorSnapshot.setValue(createdSensorSnapshots.get(i).getValue());
			
					//createdSensorSnapshots.get(i);
			
//			System.out.println("******* Run Rules for Snapshot Value : "+createdSensorSnapshot.GetValueAsLong());

			//System.out.println("******* DF : Add Single snapshot in working memory");
			
			String dtest="test";
			//Add current snapshot
			AddDTOObjects(createdSensorSnapshot);
			
			
			//System.out.println("******* DF : Process Rules");
			
			//Run Rules, this function automatically load the utility classes
			ProcessRules();
			
			//System.out.println("******* DF : Erase Working Memory");
			
			//Empty the session
			ReinitializeEngine();
			//System.out.println("******* Run Rules for Snapshot : "+i+" Finished ");
		}
	}
}
