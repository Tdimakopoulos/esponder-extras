package eu.esponder.df.ruleengine.controller;

import javax.ejb.Remote;

import eu.esponder.event.model.ESponderEvent;



@Remote
public interface DatafusionControllerRemoteService extends DatafusionControllerService {

	public void EsponderEventReceivedHandler(ESponderEvent<?> pEvent);
}

