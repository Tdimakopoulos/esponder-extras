package eu.esponder.df.eventhandler;

import eu.esponder.event.model.ESponderEvent;

public interface SensorMeasurmentEventHandlerService extends
		dfEventHandlerService {
	public void ProcessEvent(ESponderEvent<?> pEvent);
}
