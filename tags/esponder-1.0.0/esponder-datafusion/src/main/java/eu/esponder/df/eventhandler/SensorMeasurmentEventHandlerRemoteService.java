package eu.esponder.df.eventhandler;

import eu.esponder.event.model.ESponderEvent;

public interface SensorMeasurmentEventHandlerRemoteService extends
		SensorMeasurmentEventHandlerService {
	public void ProcessEvent(ESponderEvent<?> pEvent);
}
