/*
 * 
 */
package eu.esponder.event.model.crisis;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.event.model.UpdateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateCrisisContextEvent.
 */
public class UpdateCrisisContextEvent extends CrisisContextEvent<CrisisContextDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6676309937264842792L;

	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo="CrisisContextEvent Journal Info";
		return JournalMessageInfo;
	}
	
}
