/*
 * 
 */
package eu.esponder.event.model.snapshot.action;

import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.event.model.CreateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionSnapshotEvent.
 */
public class CreateActionSnapshotEvent extends ActionSnapshotEvent<ActionSnapshotDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5037252074821484071L;

	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
