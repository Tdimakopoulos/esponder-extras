/*
 * 
 */
package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.event.model.ESponderEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class SnapshotEvent.
 *
 * @param <T> the generic type
 */
public abstract class SnapshotEvent<T extends SnapshotDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1146740015260871634L;
	
}
