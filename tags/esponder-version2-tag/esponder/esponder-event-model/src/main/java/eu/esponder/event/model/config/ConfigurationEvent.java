/*
 * 
 */
package eu.esponder.event.model.config;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.event.model.ESponderEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class ConfigurationEvent.
 *
 * @param <T> the generic type
 */
public abstract class ConfigurationEvent<T extends ResourceDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3867440182566407274L;

}
