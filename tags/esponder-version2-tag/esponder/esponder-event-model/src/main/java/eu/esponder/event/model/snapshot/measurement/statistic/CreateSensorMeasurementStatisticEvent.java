/*
 * 
 */
package eu.esponder.event.model.snapshot.measurement.statistic;

import java.util.List;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.CreateEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateSensorMeasurementStatisticEvent.
 */
public class CreateSensorMeasurementStatisticEvent extends SensorMeasurementStatisticEvent<SensorMeasurementStatisticEnvelopeDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8358352435469079669L;

	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		List<SensorMeasurementStatisticDTO> arithmeticMeasurements = this.getEventAttachment().getMeasurementStatistics();
		
		String journalMessageInfo = "Received Sensor Measurement Statistics Envelope including: \n";
		for (SensorMeasurementStatisticDTO sensorMeasurement : arithmeticMeasurements) {
			ArithmeticSensorMeasurementDTO arithmeticMeasurement =  (ArithmeticSensorMeasurementDTO) sensorMeasurement.getStatistic();
			journalMessageInfo += arithmeticMeasurement.getSensor().getName() + " : " + arithmeticMeasurement.toString() + " " + arithmeticMeasurement.getSensor().getMeasurementUnit().toString()+"\n" ;
		}
		return journalMessageInfo;
	}
}
