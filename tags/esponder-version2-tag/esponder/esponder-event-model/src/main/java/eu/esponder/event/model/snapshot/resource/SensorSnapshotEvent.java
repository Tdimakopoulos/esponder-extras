/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class SensorSnapshotEvent.
 *
 * @param <T> the generic type
 */
public abstract class SensorSnapshotEvent<T extends EquipmentSnapshotDTO> extends EquipmentSnapshotEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1369626844210367218L;

}
