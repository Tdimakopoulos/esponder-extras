/*
 * 
 */
package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.event.model.UpdateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateActionPartEvent.
 */
public class UpdateActionPartEvent extends ActionPartEvent<ActionPartDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 161564239638577602L;
	
	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
