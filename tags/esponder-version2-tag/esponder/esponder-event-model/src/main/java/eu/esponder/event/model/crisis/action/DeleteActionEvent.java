/*
 * 
 */
package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.event.model.DeleteEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class DeleteActionEvent.
 */
public class DeleteActionEvent extends ActionEvent<ActionDTO> implements DeleteEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5675665888890540687L;
	
	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
