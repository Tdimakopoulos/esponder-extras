/*
 * 
 */
package eu.esponder.event.model.snapshot.measurement;

import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class LocationSensorMeasurementEvent.
 *
 * @param <T> the generic type
 */
public abstract class LocationSensorMeasurementEvent<T extends LocationSensorMeasurementDTO> extends SensorMeasurementEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 293860720150680105L;

}
