/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.event.model.UpdateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateOperationsCentreSnapshotEvent.
 */
public class UpdateOperationsCentreSnapshotEvent extends OperationsCentreSnapshotEvent<OperationsCentreSnapshotDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 819365363519771841L;

}
