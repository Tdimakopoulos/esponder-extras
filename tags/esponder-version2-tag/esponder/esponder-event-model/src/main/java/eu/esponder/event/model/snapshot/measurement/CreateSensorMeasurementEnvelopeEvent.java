/*
 * 
 */
package eu.esponder.event.model.snapshot.measurement;

import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.event.model.CreateEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateSensorMeasurementEnvelopeEvent.
 */
public class CreateSensorMeasurementEnvelopeEvent extends SensorMeasurementEnvelopeEvent<SensorMeasurementEnvelopeDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -722332256961688586L;

	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		String journalMessageInfo = "Received Sensor Measurement Envelope including: \n";
		return journalMessageInfo;
	}
}
