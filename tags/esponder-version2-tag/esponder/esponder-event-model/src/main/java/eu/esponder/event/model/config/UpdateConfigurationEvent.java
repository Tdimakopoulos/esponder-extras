/*
 * 
 */
package eu.esponder.event.model.config;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class UpdateConfigurationEvent.
 */
public class UpdateConfigurationEvent extends ConfigurationEvent<ResourceDTO> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9215480992618803998L;

	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		String journalMessageInfo = "Received Update Configuration Event including: \n";
		return journalMessageInfo;
	}
}
