/*
 * 
 */
package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class CrisisContextSnapshotEvent.
 *
 * @param <T> the generic type
 */
public class CrisisContextSnapshotEvent<T extends CrisisContextSnapshotDTO> extends SnapshotEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5971268269850771662L;

	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
