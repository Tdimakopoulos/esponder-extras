/*
 * 
 */
package eu.esponder.event.model.snapshot.measurement.statistic;

import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.ESponderEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class SensorMeasurementStatisticEvent.
 *
 * @param <T> the generic type
 */
public abstract class SensorMeasurementStatisticEvent<T extends SensorMeasurementStatisticEnvelopeDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1827678852021067614L;

}
