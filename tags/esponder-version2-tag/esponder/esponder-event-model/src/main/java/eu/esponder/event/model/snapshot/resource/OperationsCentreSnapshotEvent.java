/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreSnapshotEvent.
 *
 * @param <T> the generic type
 */
public class OperationsCentreSnapshotEvent<T extends OperationsCentreSnapshotDTO> extends ResourceSnapshotEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7163057308021392749L;

	/* (non-Javadoc)
	 * @see eu.esponder.event.model.ESponderEvent#getJournalMessageInfo()
	 */
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
