/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.event.model.snapshot.SnapshotEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class EquipmentSnapshotEvent.
 *
 * @param <T> the generic type
 */
public abstract class EquipmentSnapshotEvent<T extends EquipmentSnapshotDTO> extends SnapshotEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4318763503799378366L;
	
}
