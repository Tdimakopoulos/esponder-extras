/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.event.model.UpdateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateConsumableResourceEvent.
 */
public class UpdateConsumableResourceEvent extends ConsumableResourceEvent<ConsumableResourceDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7576724843419044712L;

}
