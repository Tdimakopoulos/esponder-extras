package eu.esponder.fru;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import eu.esponder.fru.data.DataSender;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;

public class CopyOfMain {

	public static SensorMeasurementEnvelopeDTO sensorMeasurementsEnvelope;

	public static List<Queue<SensorMeasurementDTO>> sensorTempQueue;
	
	public static List<Queue<SensorMeasurementDTO>> sensorCarbonQueue;
	
	public static List<Thread> processorThreads;

	public static void main(String args[]) {

		sensorMeasurementsEnvelope = new SensorMeasurementEnvelopeDTO();

		final List<String> Sensor_Temp_Commands = new ArrayList<String>();
		sensorTempQueue = new ArrayList<Queue<SensorMeasurementDTO>>();

		final List<String> Sensor_Carbon_Commands = new ArrayList<String>();
		sensorCarbonQueue = new ArrayList<Queue<SensorMeasurementDTO>>();

		String[] temp = null;

		try {
			// Open the file that is the first command line parameter
			FileInputStream fstream = new FileInputStream("config/config.txt");
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				// Print the content on the console
				temp = strLine.split("=");
				// System.out.println (temp[0]);
				if (temp[0].equals("Sensor_Temperature")) {
					Sensor_Temp_Commands.add(temp[1]);
				} else if (temp[0].equals("Sensor_Carbon")) {
					Sensor_Carbon_Commands.add(temp[1]);
				}
			}
			// Close the input stream
			in.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

		for (int i = 0; i < Sensor_Temp_Commands.size(); i++) {
			Queue<SensorMeasurementDTO> qe = new LinkedList<SensorMeasurementDTO>();
			sensorTempQueue.add(qe);
		}

		for (int i = 0; i < Sensor_Temp_Commands.size(); i++) {
			Queue<SensorMeasurementDTO> qe = new LinkedList<SensorMeasurementDTO>();
			sensorCarbonQueue.add(qe);
		}

		Thread dataSenderThread = new DataSender();
		dataSenderThread.start();
		
		/**
		 * FIXME:
		 * What's the purpose of sleeping for 1ms??? Is it necessary?
		 * 
		 */
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread dataHandlerThread = null; //new DataHandler(sensorMeasurementsEnvelopeList);
		dataHandlerThread.start();

		/**
		 * FIXME:
		 * What's the purpose of sleeping for 2sec??? Is it necessary?
		 * 
		 */
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		for (int i = 0; i < Sensor_Temp_Commands.size(); i++) {
//			String[] temp2;
//			temp2 = Sensor_Temp_Commands.get(i).split("_");
//			Thread temp_thread = new DataFusionProcessorThread(i, temp2[0],
//					Integer.parseInt(temp2[1]));
//			temp_thread.start();
//		}

	}
}
