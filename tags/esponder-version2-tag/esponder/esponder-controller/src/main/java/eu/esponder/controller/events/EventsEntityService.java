/*
 * 
 */
package eu.esponder.controller.events;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.events.entity.OsgiEventsEntity;

// TODO: Auto-generated Javadoc
/**
 * The Interface EventsEntityService.
 */
@Local
public interface EventsEntityService {

	/**
	 * Find osgi events entity by id.
	 *
	 * @param osgiEventsEntityID the osgi events entity id
	 * @return the osgi events entity
	 */
	public OsgiEventsEntity findOsgiEventsEntityById(Long osgiEventsEntityID);

	/**
	 * Find all osgi events entities.
	 *
	 * @return the list
	 */
	public List<OsgiEventsEntity> findAllOsgiEventsEntities();

	/**
	 * Find osgi events entities by severity.
	 *
	 * @param severity the severity
	 * @return the list
	 */
	public List<OsgiEventsEntity> findOsgiEventsEntitiesBySeverity(String severity);

	/**
	 * Find osgi events entities by source id.
	 *
	 * @param sourceId the source id
	 * @return the list
	 */
	public List<OsgiEventsEntity> findOsgiEventsEntitiesBySourceId(Long sourceId);

	/**
	 * Creates the osgi events entity.
	 *
	 * @param osgiEventsEntity the osgi events entity
	 * @param userID the user id
	 * @return the osgi events entity
	 */
	public OsgiEventsEntity createOsgiEventsEntity(OsgiEventsEntity osgiEventsEntity,
			Long userID);

	/**
	 * Delete osgi events entity.
	 *
	 * @param osgiEventsEntityID the osgi events entity id
	 * @param userID the user id
	 */
	public void deleteOsgiEventsEntity(Long osgiEventsEntityID, Long userID);

	/**
	 * Update osgi events entity.
	 *
	 * @param osgiEventsEntity the osgi events entity
	 * @param userID the user id
	 * @return the osgi events entity
	 */
	public OsgiEventsEntity updateOsgiEventsEntity(OsgiEventsEntity osgiEventsEntity,
			Long userID);

}
