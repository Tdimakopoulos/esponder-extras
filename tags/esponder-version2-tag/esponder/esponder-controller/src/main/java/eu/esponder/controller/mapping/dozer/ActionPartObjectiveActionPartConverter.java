/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.action.ActionService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionPartObjectiveActionPartConverter.
 */
public class ActionPartObjectiveActionPartConverter implements CustomConverter {
	
	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Gets the action service.
	 *
	 * @return the action service
	 */
	protected ActionService getActionService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if (sourceClass == ActionPart.class && source != null) {
			ActionPart sourceActionPart = (ActionPart) source;
			ActionPartDTO destActionPartDTO = new ActionPartDTO();
			destActionPartDTO.setId(sourceActionPart.getId());
			destActionPartDTO.setTitle(sourceActionPart.getTitle());
			destination = destActionPartDTO;
		}
		else if(sourceClass == ActionPartDTO.class && source!=null) { 
			ActionPartDTO sourceActionPartDTO = (ActionPartDTO) source;
			ActionPart destActionPart = this.getActionService().findActionPartById(sourceActionPartDTO.getId());
			destination = destActionPart;
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
