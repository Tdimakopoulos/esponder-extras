/*
 * 
 */
package eu.esponder.controller.crisis.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.type.ESponderType;

// TODO: Auto-generated Javadoc
/**
 * The Class TypeBean.
 */
@Stateless
@SuppressWarnings("unchecked")
public class TypeBean implements TypeService, TypeRemoteService {
	
	/** The type crud service. */
	@EJB
	private CrudService<ESponderType> typeCrudService;

	/** The mapping service. */
	@EJB 
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeRemoteService#findDTOById(java.lang.Long)
	 */
	@Override
	public ESponderTypeDTO findDTOById(Long typeId) throws ClassNotFoundException {
		ESponderType type = findById(typeId);
		ESponderTypeDTO typeDTO = (ESponderTypeDTO) mappingService.mapESponderEntity(type, mappingService.getDTOEntityClass(type.getClass()));
		return typeDTO;
	}


	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeService#findById(java.lang.Long)
	 */
	@Override
	public ESponderType findById(Long typeId) {
		return (ESponderType) typeCrudService.find(ESponderType.class, typeId);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeRemoteService#findDTOByTitle(java.lang.String)
	 */
	@Override
	public ESponderTypeDTO findDTOByTitle(String title) throws ClassNotFoundException {
		ESponderType type = findByTitle(title);
		Class<? extends ESponderTypeDTO> typeDTOClass = (Class<? extends ESponderTypeDTO>) mappingService.getEntityClass(mappingService.getDTOEntityName(type.getClass().getName()));
		ESponderTypeDTO esponderTypeDTO = (ESponderTypeDTO) mappingService.mapESponderEntity((ESponderEntity<Long>) type, typeDTOClass);
		return esponderTypeDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeService#findByTitle(java.lang.String)
	 */
	@Override
	public ESponderType findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ESponderType) typeCrudService.findSingleWithNamedQuery("ESponderType.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeRemoteService#findDTOAllTypes()
	 */
	@Override
	public List<ESponderTypeDTO> findDTOAllTypes() throws ClassNotFoundException {
		List<ESponderTypeDTO> typeResultsDTO = new ArrayList<ESponderTypeDTO>();
		List<ESponderType> typeResults = new ArrayList<ESponderType>();
		typeResults = findAllTypes();
		for(ESponderType t : typeResults) {
			typeResultsDTO.add((ESponderTypeDTO) mappingService.mapESponderEntity(t, mappingService.getDTOEntityClass(t.getClass())));
		}
//		return (List<ESponderTypeDTO>) mappingService.mapESponderEntity(findAllTypes(), ESponderTypeDTO.class);
		return typeResultsDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeService#findAllTypes()
	 */
	@Override
	public List<ESponderType> findAllTypes() {
		
		return (List<ESponderType>) typeCrudService.findWithNamedQuery("ESponderType.findAll");
	}
	
	//-------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeRemoteService#createTypeRemote(eu.esponder.dto.model.type.ESponderTypeDTO, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderTypeDTO createTypeRemote(ESponderTypeDTO typeDTO, Long userID) throws ClassNotFoundException {
		ESponderType type = (ESponderType) mappingService.mapESponderEntityDTO(typeDTO, mappingService.getManagedEntityClass(typeDTO.getClass()));
		type = createType(type, userID);
		return (ESponderTypeDTO) mappingService.mapESponderEntity(type, mappingService.getDTOEntityClass(type.getClass()));

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeService#createType(eu.esponder.model.type.ESponderType, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderType createType(ESponderType type, Long userID) {
		return (ESponderType) typeCrudService.create(type);
	}
	
	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeRemoteService#updateTypeRemote(eu.esponder.dto.model.type.ESponderTypeDTO, java.lang.Long)
	 */
	@Override
	public ESponderTypeDTO updateTypeRemote(ESponderTypeDTO typeDTO, Long userID) {
		ESponderType type = (ESponderType) mappingService.mapESponderEntityDTO(typeDTO, ESponderType.class);
		type = updateType(type, userID);
		typeDTO = (ESponderTypeDTO) mappingService.mapESponderEntity(type, ESponderTypeDTO.class);
		return typeDTO;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeService#updateType(eu.esponder.model.type.ESponderType, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderType updateType(ESponderType type, Long userID) {
		type = typeCrudService.update(type);
		return type;
	}
	
	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeRemoteService#deleteTypeRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteTypeRemote(Long ESponderTypeDTOID, Long userID) {
		deleteType(ESponderTypeDTOID, userID);
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.TypeService#deleteType(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteType(Long ESponderTypeID, Long userID) {
		typeCrudService.delete(ESponderType.class, ESponderTypeID);
	}
	
	// -------------------------------------------------------------------------

}
