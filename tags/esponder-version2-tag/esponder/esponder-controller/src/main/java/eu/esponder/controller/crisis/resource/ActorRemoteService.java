/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ActorRemoteService.
 */
@Remote
public interface ActorRemoteService {

	/**
	 * Find by id remote.
	 *
	 * @param actorID the actor id
	 * @return the actor dto
	 */
	public ActorDTO findByIdRemote(Long actorID);

	/**
	 * Find by title remote.
	 *
	 * @param title the title
	 * @return the actor dto
	 */
	public ActorDTO findByTitleRemote(String title);
	
	/**
	 * Find subordinates by id remote.
	 *
	 * @param actorID the actor id
	 * @return the list
	 */
	public List<ActorDTO> findSubordinatesByIdRemote(Long actorID);

	/**
	 * Creates the actor remote.
	 *
	 * @param actorDTO the actor dto
	 * @param userID the user id
	 * @return the actor dto
	 */
	public ActorDTO createActorRemote(ActorDTO actorDTO, Long userID);

	/**
	 * Update actor remote.
	 *
	 * @param actorDTO the actor dto
	 * @param userID the user id
	 * @return the actor dto
	 */
	public ActorDTO updateActorRemote(ActorDTO actorDTO, Long userID);
	
	/**
	 * Delete actor remote.
	 *
	 * @param actorID the actor id
	 * @param userID the user id
	 */
	public void deleteActorRemote(Long actorID, Long userID);
	
	/**
	 * Find actor snapshot by date remote.
	 *
	 * @param actorID the actor id
	 * @param maxDate the max date
	 * @return the actor snapshot dto
	 */
	public ActorSnapshotDTO findActorSnapshotByDateRemote(Long actorID, Date maxDate);
	
	/**
	 * Find actor snapshot by id remote.
	 *
	 * @param actorID the actor id
	 * @return the actor snapshot dto
	 */
	public ActorSnapshotDTO findActorSnapshotByIdRemote(Long actorID);

	/**
	 * Creates the actor snapshot remote.
	 *
	 * @param snapshotDTO the snapshot dto
	 * @param userID the user id
	 * @return the actor snapshot dto
	 */
	public ActorSnapshotDTO createActorSnapshotRemote(ActorSnapshotDTO snapshotDTO, Long userID);
	
	/**
	 * Update actor snapshot remote.
	 *
	 * @param actorSnapshotDTO the actor snapshot dto
	 * @param userID the user id
	 * @return the actor snapshot dto
	 */
	public ActorSnapshotDTO updateActorSnapshotRemote(ActorSnapshotDTO actorSnapshotDTO, Long userID);
	
	/**
	 * Delete actor snapshot remote.
	 *
	 * @param actorSnapshotDTOID the actor snapshot dtoid
	 * @param userID the user id
	 */
	public void deleteActorSnapshotRemote(Long actorSnapshotDTOID, Long userID);

	/**
	 * Find all actors remote.
	 *
	 * @return the list
	 */
	public List<ActorDTO> findAllActorsRemote();

	/**
	 * Find supervisor for actor remote.
	 *
	 * @param actorID the actor id
	 * @return the actor dto
	 */
	public ActorDTO findSupervisorForActorRemote(Long actorID);

	/**
	 * Find fr team by id remote.
	 *
	 * @param frTeamID the fr team id
	 * @return the fR team dto
	 */
	public FRTeamDTO findFRTeamByIdRemote(Long frTeamID);

	/**
	 * Find fr team chief remote.
	 *
	 * @param frTeamID the fr team id
	 * @return the actor dto
	 */
	public ActorDTO findFRTeamChiefRemote(Long frTeamID);

	/**
	 * Find fr teams for meoc remote.
	 *
	 * @param meocID the meoc id
	 * @return the list
	 */
	public List<FRTeamDTO> findFRTeamsForMeocRemote(Long meocID);

	/**
	 * Find fr team for chief remote.
	 *
	 * @param frchiefID the frchief id
	 * @return the fR team dto
	 */
	public FRTeamDTO findFRTeamForChiefRemote(Long frchiefID);
	
}
