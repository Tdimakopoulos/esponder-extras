/*
 * 
 */
package eu.esponder.controller.crisis;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.crisis.resource.plan.PlannableResourcePowerDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface CrisisRemoteService.
 */
@Remote
public interface CrisisRemoteService {

	/**
	 * Find plannable resource power by id remote.
	 *
	 * @param powerID the power id
	 * @return the plannable resource power dto
	 */
	public PlannableResourcePowerDTO findPlannableResourcePowerByIdRemote(Long powerID);

	/**
	 * Creates the plannable resource power remote.
	 *
	 * @param plannableResourcePowerDTO the plannable resource power dto
	 * @param userID the user id
	 * @return the plannable resource power dto
	 */
	public PlannableResourcePowerDTO createPlannableResourcePowerRemote(
			PlannableResourcePowerDTO plannableResourcePowerDTO, Long userID);

	/**
	 * Delete plannable resource power remote.
	 *
	 * @param powerID the power id
	 * @param userID the user id
	 */
	public void deletePlannableResourcePowerRemote(Long powerID, Long userID);

	/**
	 * Update plannable resource power remote.
	 *
	 * @param plannableResourcePowerDTO the plannable resource power dto
	 * @param userID the user id
	 * @return the plannable resource power dto
	 */
	public PlannableResourcePowerDTO updatePlannableResourcePowerRemote(
			PlannableResourcePowerDTO plannableResourcePowerDTO, Long userID);

	/**
	 * Find crisis resource plan by id remote.
	 *
	 * @param crisisResourcePlanId the crisis resource plan id
	 * @param userId the user id
	 * @return the crisis resource plan dto
	 */
	public CrisisResourcePlanDTO findCrisisResourcePlanByIdRemote(
			Long crisisResourcePlanId, Long userId);

	/**
	 * Find crisis resource plan by title remote.
	 *
	 * @param crisisResourcePlanTitle the crisis resource plan title
	 * @param userId the user id
	 * @return the crisis resource plan dto
	 */
	public CrisisResourcePlanDTO findCrisisResourcePlanByTitleRemote(
			String crisisResourcePlanTitle, Long userId);

	/**
	 * Find all crisis resource plans remote.
	 *
	 * @return the list
	 */
	public List<CrisisResourcePlanDTO> findAllCrisisResourcePlansRemote();

	/**
	 * Creates the crisis resource plan remote.
	 *
	 * @param crisisResourcePlanDTO the crisis resource plan dto
	 * @param userID the user id
	 * @return the crisis resource plan dto
	 */
	public CrisisResourcePlanDTO createCrisisResourcePlanRemote(
			CrisisResourcePlanDTO crisisResourcePlanDTO, Long userID);

	/**
	 * Delete crisis resource plan remote.
	 *
	 * @param crisisResourcePlanId the crisis resource plan id
	 * @param userID the user id
	 */
	public void deleteCrisisResourcePlanRemote(Long crisisResourcePlanId, Long userID);

	/**
	 * Update crisis resource plan remote.
	 *
	 * @param crisisResourcePlanDTO the crisis resource plan dto
	 * @param userID the user id
	 * @return the crisis resource plan dto
	 */
	public CrisisResourcePlanDTO updateCrisisResourcePlanRemote(
			CrisisResourcePlanDTO crisisResourcePlanDTO, Long userID);

	/**
	 * Find crisis context dto by id.
	 *
	 * @param crisisContextID the crisis context id
	 * @return the crisis context dto
	 */
	public CrisisContextDTO findCrisisContextDTOById(Long crisisContextID);

	/**
	 * Find all crisis contexts remote.
	 *
	 * @return the list
	 */
	public List<CrisisContextDTO> findAllCrisisContextsRemote();

	/**
	 * Find crisis context dto by title.
	 *
	 * @param title the title
	 * @return the crisis context dto
	 */
	public CrisisContextDTO findCrisisContextDTOByTitle(String title);

	/**
	 * Creates the crisis context remote.
	 *
	 * @param crisisContextDTO the crisis context dto
	 * @param userID the user id
	 * @return the crisis context dto
	 */
	public CrisisContextDTO createCrisisContextRemote(CrisisContextDTO crisisContextDTO, Long userID);

	/**
	 * Delete crisis context remote.
	 *
	 * @param crisisContextId the crisis context id
	 * @param userID the user id
	 */
	public void deleteCrisisContextRemote(Long crisisContextId, Long userID);

	/**
	 * Update crisis context remote.
	 *
	 * @param crisisContextDTO the crisis context dto
	 * @param userID the user id
	 * @return the crisis context dto
	 */
	public CrisisContextDTO updateCrisisContextRemote(CrisisContextDTO crisisContextDTO, Long userID);

	/**
	 * Find crisis context snapshot dto by id.
	 *
	 * @param crisisContextSnapshotID the crisis context snapshot id
	 * @return the crisis context snapshot dto
	 */
	public CrisisContextSnapshotDTO findCrisisContextSnapshotDTOById(Long crisisContextSnapshotID);

	/**
	 * Creates the crisis context snapshot remote.
	 *
	 * @param crisisContextSnapshotDTO the crisis context snapshot dto
	 * @param userID the user id
	 * @return the crisis context snapshot dto
	 */
	public CrisisContextSnapshotDTO createCrisisContextSnapshotRemote(
			CrisisContextSnapshotDTO crisisContextSnapshotDTO, Long userID);

	/**
	 * Delete crisis context snapshot remote.
	 *
	 * @param crisisContextSnapshotId the crisis context snapshot id
	 * @param userID the user id
	 */
	public void deleteCrisisContextSnapshotRemote(Long crisisContextSnapshotId,
			Long userID);

	/**
	 * Update crisis context snapshot remote.
	 *
	 * @param crisisContextSnapshotDTO the crisis context snapshot dto
	 * @param userID the user id
	 * @return the crisis context snapshot dto
	 */
	public CrisisContextSnapshotDTO updateCrisisContextSnapshotRemote(
			CrisisContextSnapshotDTO crisisContextSnapshotDTO, Long userID);

}
