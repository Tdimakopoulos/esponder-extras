/*
 * 
 */
package eu.esponder.controller.crisis;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.resource.plan.CrisisResourcePlan;
import eu.esponder.model.crisis.resource.plan.PlannableResourcePower;
import eu.esponder.model.snapshot.CrisisContextSnapshot;

// TODO: Auto-generated Javadoc
/**
 * The Interface CrisisService.
 */
@Local
public interface CrisisService {

	/**
	 * Find plannable resource power by id.
	 *
	 * @param powerID the power id
	 * @return the plannable resource power
	 */
	public PlannableResourcePower findPlannableResourcePowerById(Long powerID);

	/**
	 * Creates the plannable resource power.
	 *
	 * @param plannableResourcePower the plannable resource power
	 * @param userID the user id
	 * @return the plannable resource power
	 */
	public PlannableResourcePower createPlannableResourcePower(
			PlannableResourcePower plannableResourcePower, Long userID);

	/**
	 * Delete plannable resource power.
	 *
	 * @param powerID the power id
	 * @param userID the user id
	 */
	public void deletePlannableResourcePower(Long powerID, Long userID);

	/**
	 * Update plannable resource power.
	 *
	 * @param plannableResourcePower the plannable resource power
	 * @param userID the user id
	 * @return the plannable resource power
	 */
	public PlannableResourcePower updatePlannableResourcePower(
			PlannableResourcePower plannableResourcePower, Long userID);

	/**
	 * Find crisis resource plan by id.
	 *
	 * @param crisisResourcePlanId the crisis resource plan id
	 * @param userId the user id
	 * @return the crisis resource plan
	 */
	public CrisisResourcePlan findCrisisResourcePlanById(Long crisisResourcePlanId,
			Long userId);

	/**
	 * Find crisis resource plan by title.
	 *
	 * @param crisisResourcePlanTitle the crisis resource plan title
	 * @param userId the user id
	 * @return the crisis resource plan
	 */
	public CrisisResourcePlan findCrisisResourcePlanByTitle(
			String crisisResourcePlanTitle, Long userId);

	/**
	 * Find all crisis resource plans.
	 *
	 * @return the list
	 */
	public List<CrisisResourcePlan> findAllCrisisResourcePlans();

	/**
	 * Creates the crisis resource plan.
	 *
	 * @param crisisResourcePlan the crisis resource plan
	 * @param userID the user id
	 * @return the crisis resource plan
	 */
	public CrisisResourcePlan createCrisisResourcePlan(
			CrisisResourcePlan crisisResourcePlan, Long userID);

	/**
	 * Delete crisis resource plan.
	 *
	 * @param crisisResourcePlanId the crisis resource plan id
	 * @param userID the user id
	 */
	public void deleteCrisisResourcePlan(Long crisisResourcePlanId, Long userID);

	/**
	 * Update crisis resource plan.
	 *
	 * @param crisisResourcePlan the crisis resource plan
	 * @param userID the user id
	 * @return the crisis resource plan
	 */
	public CrisisResourcePlan updateCrisisResourcePlan(
			CrisisResourcePlan crisisResourcePlan, Long userID);

	/**
	 * Find crisis context by id.
	 *
	 * @param crisisContextID the crisis context id
	 * @return the crisis context
	 */
	public CrisisContext findCrisisContextById(Long crisisContextID);

	/**
	 * Find all crisis contexts.
	 *
	 * @return the list
	 */
	public List<CrisisContext> findAllCrisisContexts();

	/**
	 * Find crisis context by title.
	 *
	 * @param title the title
	 * @return the crisis context
	 */
	public CrisisContext findCrisisContextByTitle(String title);

	/**
	 * Creates the crisis context.
	 *
	 * @param crisisContext the crisis context
	 * @param userID the user id
	 * @return the crisis context
	 */
	public CrisisContext createCrisisContext(CrisisContext crisisContext, Long userID);

	/**
	 * Delete crisis context.
	 *
	 * @param crisisContextId the crisis context id
	 * @param userID the user id
	 */
	public void deleteCrisisContext(Long crisisContextId, Long userID);

	/**
	 * Update crisis context.
	 *
	 * @param crisisContext the crisis context
	 * @param userID the user id
	 * @return the crisis context
	 */
	public CrisisContext updateCrisisContext(CrisisContext crisisContext, Long userID);

	/**
	 * Find crisis context snapshot by id.
	 *
	 * @param crisisContextSnapshotID the crisis context snapshot id
	 * @return the crisis context snapshot
	 */
	public CrisisContextSnapshot findCrisisContextSnapshotById(Long crisisContextSnapshotID);

	/**
	 * Creates the crisis context snapshot.
	 *
	 * @param crisisContextSnapshot the crisis context snapshot
	 * @param userID the user id
	 * @return the crisis context snapshot
	 */
	public CrisisContextSnapshot createCrisisContextSnapshot(
			CrisisContextSnapshot crisisContextSnapshot, Long userID);

	/**
	 * Delete crisis context snapshot.
	 *
	 * @param crisisContextSnapshotId the crisis context snapshot id
	 * @param userID the user id
	 */
	public void deleteCrisisContextSnapshot(Long crisisContextSnapshotId, Long userID);

	/**
	 * Update crisis context snapshot.
	 *
	 * @param crisisContextSnapshot the crisis context snapshot
	 * @param userID the user id
	 * @return the crisis context snapshot
	 */
	public CrisisContextSnapshot updateCrisisContextSnapshot(
			CrisisContextSnapshot crisisContextSnapshot, Long userID);
	
}
