/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.snapshot.sensor.config.StatisticsConfig;
import eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Interface SensorService.
 */
@Local
public interface SensorService extends SensorRemoteService {
	
	/**
	 * Find sensor by id.
	 *
	 * @param sensorID the sensor id
	 * @return the sensor
	 */
	public Sensor findSensorById(Long sensorID);
	
	/**
	 * Find sensor by title.
	 *
	 * @param title the title
	 * @return the sensor
	 */
	public Sensor findSensorByTitle(String title);
	
	/**
	 * Creates the sensor.
	 *
	 * @param sensor the sensor
	 * @param userID the user id
	 * @return the sensor
	 */
	public Sensor createSensor(Sensor sensor, Long userID);
	
	/**
	 * Update sensor.
	 *
	 * @param sensor the sensor
	 * @param userID the user id
	 * @return the sensor
	 */
	public Sensor updateSensor(Sensor sensor, Long userID);
	
	/**
	 * Delete sensor.
	 *
	 * @param sensorId the sensor id
	 * @param userID the user id
	 */
	public void deleteSensor(Long sensorId, Long userID);
	
	/**
	 * Find sensor snapshot by date.
	 *
	 * @param sensorID the sensor id
	 * @param dateTo the date to
	 * @return the sensor snapshot
	 */
	public SensorSnapshot findSensorSnapshotByDate(Long sensorID, Date dateTo);
	
	/**
	 * Find sensor snapshot by id.
	 *
	 * @param sensorID the sensor id
	 * @return the sensor snapshot
	 */
	public SensorSnapshot findSensorSnapshotById(Long sensorID);
	
	/**
	 * Find previous sensor snapshot.
	 *
	 * @param sensorID the sensor id
	 * @return the sensor snapshot
	 */
	public SensorSnapshot findPreviousSensorSnapshot(Long sensorID);
	
	/**
	 * Creates the sensor snapshot.
	 *
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the sensor snapshot
	 */
	public SensorSnapshot createSensorSnapshot(SensorSnapshot snapshot, Long userID);
	
	/**
	 * Update sensor snapshot.
	 *
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the sensor snapshot
	 */
	public SensorSnapshot updateSensorSnapshot(SensorSnapshot snapshot, Long userID);
	
	/**
	 * Delete sensor snapshot.
	 *
	 * @param sensorSnapshotId the sensor snapshot id
	 * @param userID the user id
	 */
	public void deleteSensorSnapshot(Long sensorSnapshotId, Long userID);
	
	/**
	 * Find config by id.
	 *
	 * @param configID the config id
	 * @return the statistics config
	 */
	public StatisticsConfig findConfigById(Long configID);

	/**
	 * Creates the statistic config.
	 *
	 * @param statisticConfig the statistic config
	 * @param userID the user id
	 * @return the statistics config
	 */
	public StatisticsConfig createStatisticConfig(StatisticsConfig statisticConfig, Long userID);
	
	/**
	 * Update statistic config.
	 *
	 * @param statisticConfig the statistic config
	 * @param userID the user id
	 * @return the statistics config
	 */
	public StatisticsConfig updateStatisticConfig(StatisticsConfig statisticConfig, Long userID);
	
	/**
	 * Delete statistic config.
	 *
	 * @param statisticsConfigId the statistics config id
	 * @param userID the user id
	 */
	public void deleteStatisticConfig(Long statisticsConfigId, Long userID);

	/**
	 * Find all sensor snapshots.
	 *
	 * @return the list
	 */
	public List<SensorSnapshot> findAllSensorSnapshots();

	/**
	 * Find all sensor snapshots by sensor.
	 *
	 * @param sensor the sensor
	 * @param resultLimit the result limit
	 * @return the list
	 */
	public List<SensorSnapshot> findAllSensorSnapshotsBySensor(Sensor sensor,	int resultLimit);

	/**
	 * Find all sensor snapshots from to.
	 *
	 * @param sensorID the sensor id
	 * @param dateFrom the date from
	 * @param dateTo the date to
	 * @return the list
	 */
	List<SensorSnapshot> findAllSensorSnapshotsFromTo(Long sensorID,
			Long dateFrom, Long dateTo);

	/**
	 * Find all sensor snapshots from to and type.
	 *
	 * @param sensorID the sensor id
	 * @param dateFrom the date from
	 * @param dateTo the date to
	 * @param statisticType the statistic type
	 * @return the list
	 */
	List<SensorSnapshot> findAllSensorSnapshotsFromToAndType(Long sensorID,
			Long dateFrom, Long dateTo,
			MeasurementStatisticTypeEnum statisticType);

	/**
	 * Find all sensor snapshots by sensor and type.
	 *
	 * @param sensor the sensor
	 * @param statisticType the statistic type
	 * @param resultLimit the result limit
	 * @return the list
	 */
	List<SensorSnapshot> findAllSensorSnapshotsBySensorAndType(Sensor sensor,
			MeasurementStatisticTypeEnum statisticType, int resultLimit);
	
}
