/*
 * 
 */
package eu.esponder.controller.crisis.resource.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.LogisticsRemoteService;
import eu.esponder.controller.crisis.resource.LogisticsService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.LogisticsResource;
import eu.esponder.model.crisis.resource.RegisteredConsumableResource;
import eu.esponder.model.crisis.resource.RegisteredReusableResource;
import eu.esponder.model.crisis.resource.ReusableResource;
import eu.esponder.model.type.ConsumableResourceType;

// TODO: Auto-generated Javadoc
/**
 * The Class LogisticsBean.
 */
@Stateless
public class LogisticsBean implements LogisticsService, LogisticsRemoteService {

	/** The logistics crud service. */
	@EJB
	private CrudService<LogisticsResource> logisticsCrudService;

	/** The consumables crud service. */
	@EJB
	private CrudService<ConsumableResource> consumablesCrudService;

	/** The reusables crud service. */
	@EJB
	private CrudService<ReusableResource> reusablesCrudService;

	/** The registered reusables crud service. */
	@EJB
	private CrudService<RegisteredReusableResource> registeredReusablesCrudService;

	/** The registered consumables crud service. */
	@EJB
	private CrudService<RegisteredConsumableResource> registeredConsumablesCrudService;

	/** The type service. */
	@EJB
	private TypeService typeService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	/**
	 * Find logistics by id.
	 *
	 * @param logisticsID the logistics id
	 * @return the logistics resource
	 */
	public LogisticsResource findLogisticsById(Long logisticsID) {
		return (LogisticsResource) logisticsCrudService.find(LogisticsResource.class, logisticsID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findReusableResourceByIdRemote(java.lang.Long)
	 */
	@Override
	public ReusableResourceDTO findReusableResourceByIdRemote(Long reusableID) {
		ReusableResource reusableResource = findReusableResourceById(reusableID);
		ReusableResourceDTO reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findReusableResourceById(java.lang.Long)
	 */
	@Override
	public ReusableResource findReusableResourceById(Long reusableID) {
		return (ReusableResource) reusablesCrudService.find(ReusableResource.class, reusableID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findAllReusableResourcesRemote()
	 */
	@Override
	public List<ReusableResourceDTO> findAllReusableResourcesRemote() {
		List<ReusableResource> resultsReusables = findAllReusableResources();
		List<ReusableResourceDTO> reusableResourcesDTO = (List<ReusableResourceDTO>) mappingService.mapESponderEntity(resultsReusables, ReusableResourceDTO.class);
		return reusableResourcesDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findAllReusableResources()
	 */
	@Override
	public List<ReusableResource> findAllReusableResources() {
		return (List<ReusableResource>) reusablesCrudService.findWithNamedQuery("ReusableResource.findAll");
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findRegisteredReusableResourceByIdRemote(java.lang.Long)
	 */
	@Override
	public RegisteredReusableResourceDTO findRegisteredReusableResourceByIdRemote(Long registeredReusableID) {
		RegisteredReusableResource registeredReusableResource = findRegisteredReusableResourceById(registeredReusableID);
		RegisteredReusableResourceDTO registeredReusableResourceDTO = (RegisteredReusableResourceDTO) mappingService.mapESponderEntity(registeredReusableResource, RegisteredReusableResourceDTO.class);
		return registeredReusableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findRegisteredReusableResourceById(java.lang.Long)
	 */
	@Override
	public RegisteredReusableResource findRegisteredReusableResourceById(Long registeredReusableID) {
		return (RegisteredReusableResource) registeredReusablesCrudService.find(RegisteredReusableResource.class, registeredReusableID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findConsumableResourceByIdRemote(java.lang.Long)
	 */
	@Override
	public ConsumableResourceDTO findConsumableResourceByIdRemote(Long consumableID) {
		ConsumableResource consumableResource = findConsumableResourceById(consumableID);
		ConsumableResourceDTO consumableResourceDTO = (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ConsumableResourceDTO.class);
		return consumableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findConsumableResourceById(java.lang.Long)
	 */
	@Override
	public ConsumableResource findConsumableResourceById(Long consumableID) {
		return (ConsumableResource) consumablesCrudService.find(ConsumableResource.class, consumableID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findAllConsumableResourcesRemote()
	 */
	@Override
	public List<ConsumableResourceDTO> findAllConsumableResourcesRemote() {
		List<ConsumableResource> resultsConsumables = findAllConsumableResources();
		List<ConsumableResourceDTO> consumableResourcesDTO = (List<ConsumableResourceDTO>) mappingService.mapESponderEntity(resultsConsumables, ConsumableResourceDTO.class);
		return consumableResourcesDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findAllConsumableResources()
	 */
	@Override
	public List<ConsumableResource> findAllConsumableResources() {
		return (List<ConsumableResource>) consumablesCrudService.findWithNamedQuery("ConsumableResource.findAll");
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findRegisteredConsumableResourceByIdRemote(java.lang.Long)
	 */
	@Override
	public RegisteredConsumableResourceDTO findRegisteredConsumableResourceByIdRemote(Long registeredConsumableID) {
		RegisteredConsumableResource registeredConsumableResource = findRegisteredConsumableResourceById(registeredConsumableID);
		RegisteredConsumableResourceDTO registeredConsumableResourceDTO = (RegisteredConsumableResourceDTO) mappingService.mapESponderEntity(registeredConsumableResource, RegisteredConsumableResourceDTO.class);
		return registeredConsumableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findRegisteredConsumableResourceById(java.lang.Long)
	 */
	@Override
	public RegisteredConsumableResource findRegisteredConsumableResourceById(Long registeredConsumableID) {
		return (RegisteredConsumableResource) registeredConsumablesCrudService.find(RegisteredConsumableResource.class, registeredConsumableID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findAllRegisteredConsumableResourcesRemote()
	 */
	@Override
	public List<RegisteredConsumableResourceDTO> findAllRegisteredConsumableResourcesRemote() {
		List<RegisteredConsumableResource> resultsRegisteredConsumables = findAllRegisteredConsumableResources();
		List<RegisteredConsumableResourceDTO> registeredConsumableResourcesDTO = (List<RegisteredConsumableResourceDTO>) 
				mappingService.mapESponderEntity(resultsRegisteredConsumables, RegisteredConsumableResourceDTO.class);
		return registeredConsumableResourcesDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findAllRegisteredConsumableResources()
	 */
	@Override
	public List<RegisteredConsumableResource> findAllRegisteredConsumableResources() {
		return (List<RegisteredConsumableResource>) registeredConsumablesCrudService.findWithNamedQuery("RegisteredConsumableResource.findAll");
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findAllRegisteredReusableResourcesRemote()
	 */
	@Override
	public List<RegisteredReusableResourceDTO> findAllRegisteredReusableResourcesRemote() {
		List<RegisteredReusableResource> resultsRegisteredReusables = findAllRegisteredReusableResources();
		List<RegisteredReusableResourceDTO> reusableRegisteredResourcesDTO = 
				(List<RegisteredReusableResourceDTO>) mappingService.mapESponderEntity(resultsRegisteredReusables, RegisteredReusableResourceDTO.class);
		return reusableRegisteredResourcesDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findAllRegisteredReusableResources()
	 */
	@Override
	public List<RegisteredReusableResource> findAllRegisteredReusableResources() {
		return (List<RegisteredReusableResource>) registeredReusablesCrudService.findWithNamedQuery("RegisteredReusableResource.findAll");
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findReusableResourceByTitleRemote(java.lang.String)
	 */
	@Override
	public ReusableResourceDTO findReusableResourceByTitleRemote(String title) {
		ReusableResource reusableResource = findReusableResourceByTitle(title);
		ReusableResourceDTO reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findReusableResourceByTitle(java.lang.String)
	 */
	@Override
	public ReusableResource findReusableResourceByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ReusableResource) reusablesCrudService.findSingleWithNamedQuery("ReusableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findRegisteredReusableResourceByTitleRemote(java.lang.String)
	 */
	@Override
	public RegisteredReusableResourceDTO findRegisteredReusableResourceByTitleRemote(String title) {
		RegisteredReusableResource registeredReusableResource = findRegisteredReusableResourceByTitle(title);
		RegisteredReusableResourceDTO registeredReusableResourceDTO = (RegisteredReusableResourceDTO) mappingService.mapESponderEntity(registeredReusableResource, RegisteredReusableResourceDTO.class);
		return registeredReusableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findRegisteredReusableResourceByTitle(java.lang.String)
	 */
	@Override
	public RegisteredReusableResource findRegisteredReusableResourceByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (RegisteredReusableResource) registeredReusablesCrudService.findSingleWithNamedQuery("RegisteredReusableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findConsumableResourceByTitleRemote(java.lang.String)
	 */
	@Override
	public ConsumableResourceDTO findConsumableResourceByTitleRemote(String title) {
		ConsumableResource consumableResource = findConsumableResourceByTitle(title);
		return (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ConsumableResourceDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findConsumableResourceByTitle(java.lang.String)
	 */
	@Override
	public ConsumableResource findConsumableResourceByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ConsumableResource) consumablesCrudService.findSingleWithNamedQuery("ConsumableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#findRegisteredConsumableResourceByTitleRemote(java.lang.String)
	 */
	@Override
	public RegisteredConsumableResourceDTO findRegisteredConsumableResourceByTitleRemote(String title) {
		RegisteredConsumableResource registeredConsumableResource = findRegisteredConsumableResourceByTitle(title);
		RegisteredConsumableResourceDTO registeredConsumableResourceDTO = (RegisteredConsumableResourceDTO) mappingService.mapESponderEntity(registeredConsumableResource, RegisteredConsumableResourceDTO.class);
		return registeredConsumableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#findRegisteredConsumableResourceByTitle(java.lang.String)
	 */
	@Override
	public RegisteredConsumableResource findRegisteredConsumableResourceByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (RegisteredConsumableResource) registeredConsumablesCrudService.findSingleWithNamedQuery("RegisteredConsumableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#createConsumableResourceRemote(eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO, java.lang.Long)
	 */
	@Override
	public ConsumableResourceDTO createConsumableResourceRemote( ConsumableResourceDTO consumableResourceDTO, Long userID) {
		ConsumableResource consumableResource = (ConsumableResource) mappingService.mapESponderEntityDTO(consumableResourceDTO, ConsumableResource.class);
		consumableResource = createConsumableResource(consumableResource, userID);
		consumableResourceDTO = (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ConsumableResourceDTO.class);
		return consumableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#createConsumableResource(eu.esponder.model.crisis.resource.ConsumableResource, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ConsumableResource createConsumableResource(ConsumableResource consumableResource, Long userID) {
		consumableResource.getConsumableResourceCategory().setConsumableResourceType((ConsumableResourceType) typeService.findById(consumableResource.getConsumableResourceCategory().getConsumableResourceType().getId()));
		if(null != consumableResource.getContainer())
		{
			consumableResource.setContainer(this.findReusableResourceById(consumableResource.getContainer().getId()));
		}
		consumablesCrudService.create(consumableResource);
		return consumableResource;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#createRegisteredConsumableResourceRemote(eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO, java.lang.Long)
	 */
	@Override
	public RegisteredConsumableResourceDTO createRegisteredConsumableResourceRemote( RegisteredConsumableResourceDTO registeredConsumableResourceDTO, Long userID) {
		RegisteredConsumableResource registeredConsumableResource = (RegisteredConsumableResource) mappingService.mapESponderEntityDTO(registeredConsumableResourceDTO, RegisteredConsumableResource.class);
		registeredConsumableResource = createRegisteredConsumableResource(registeredConsumableResource, userID);
		registeredConsumableResourceDTO = (RegisteredConsumableResourceDTO) mappingService.mapESponderEntity(registeredConsumableResource, RegisteredConsumableResourceDTO.class);
		return registeredConsumableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#createRegisteredConsumableResource(eu.esponder.model.crisis.resource.RegisteredConsumableResource, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredConsumableResource createRegisteredConsumableResource(RegisteredConsumableResource registeredConsumableResource, Long userID) {
		//		registeredConsumableResource.getConsumableResourceCategory().setConsumableResourceType((ConsumableResourceType) typeService.findById(registeredConsumableResource.getConsumableResourceCategory().getConsumableResourceType().getId()));
		if(null != registeredConsumableResource.getContainer())
		{
			registeredConsumableResource.setContainer(this.findRegisteredReusableResourceById(registeredConsumableResource.getContainer().getId()));
		}
		registeredConsumablesCrudService.create(registeredConsumableResource);
		return registeredConsumableResource;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#createReusableResourceRemote(eu.esponder.dto.model.crisis.resource.ReusableResourceDTO, java.lang.Long)
	 */
	@Override
	public ReusableResourceDTO createReusableResourceRemote(ReusableResourceDTO reusableResourceDTO, Long userID) {
		ReusableResource reusableResource = (ReusableResource) mappingService.mapESponderEntityDTO(reusableResourceDTO, ReusableResource.class);
		reusableResource = createReusableResource(reusableResource, userID);
		reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#createReusableResource(eu.esponder.model.crisis.resource.ReusableResource, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReusableResource createReusableResource(ReusableResource reusableResource, Long userID) {
		reusablesCrudService.create(reusableResource);
		return reusableResource;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#createRegisteredReusableResourceRemote(eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO, java.lang.Long)
	 */
	@Override
	public RegisteredReusableResourceDTO createRegisteredReusableResourceRemote(RegisteredReusableResourceDTO registeredReusableResourceDTO, Long userID) {
		RegisteredReusableResource registeredReusableResource = (RegisteredReusableResource) mappingService.mapESponderEntityDTO(registeredReusableResourceDTO, RegisteredReusableResource.class);
		registeredReusableResource = createRegisteredReusableResource(registeredReusableResource, userID);
		registeredReusableResourceDTO = (RegisteredReusableResourceDTO) mappingService.mapESponderEntity(registeredReusableResource, RegisteredReusableResourceDTO.class);
		return registeredReusableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#createRegisteredReusableResource(eu.esponder.model.crisis.resource.RegisteredReusableResource, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredReusableResource createRegisteredReusableResource(RegisteredReusableResource registeredReusableResource, Long userID) {
		registeredReusablesCrudService.create(registeredReusableResource);
		return registeredReusableResource;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#deleteConsumableResourceRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteConsumableResourceRemote(Long consumableResourceDTOID, Long userID) {
		deleteConsumableResource(consumableResourceDTOID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#deleteConsumableResource(java.lang.Long, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteConsumableResource(Long consumableResourceID, Long userID) {
		ConsumableResource resource = consumablesCrudService.find(ConsumableResource.class, consumableResourceID);
		consumablesCrudService.delete(resource);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#deleteRegisteredConsumableResourceRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteRegisteredConsumableResourceRemote(Long registeredReusableResourceDTOID, Long userID) {
		deleteRegisteredConsumableResource(registeredReusableResourceDTOID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#deleteRegisteredConsumableResource(java.lang.Long, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteRegisteredConsumableResource(Long registeredConsumableResourceID, Long userID) {
		RegisteredConsumableResource regResource = registeredConsumablesCrudService.find(RegisteredConsumableResource.class, registeredConsumableResourceID);
		registeredConsumablesCrudService.delete(regResource);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#deleteReusableResourceRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteReusableResourceRemote(Long reusableResourceDTOID, Long userID) {
		deleteReusableResource(reusableResourceDTOID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#deleteReusableResource(java.lang.Long, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteReusableResource(Long reusableResourceID, Long userID) {
		ReusableResource resource = reusablesCrudService.find(ReusableResource.class, reusableResourceID);
//		reusablesCrudService.delete(ReusableResource.class, reusableResourceID);
		reusablesCrudService.delete(resource);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#deleteRegisteredReusableResourceRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteRegisteredReusableResourceRemote(Long registeredReusableResourceDTOID, Long userID) {
		deleteRegisteredReusableResource(registeredReusableResourceDTOID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#deleteRegisteredReusableResource(java.lang.Long, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteRegisteredReusableResource(Long registeredReusableResourceID, Long userID) {
		RegisteredReusableResource regResource = registeredReusablesCrudService.find(RegisteredReusableResource.class, registeredReusableResourceID);
		registeredReusablesCrudService.delete(regResource);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#updateReusableResourceRemote(eu.esponder.dto.model.crisis.resource.ReusableResourceDTO, java.lang.Long)
	 */
	@Override
	public ReusableResourceDTO updateReusableResourceRemote(ReusableResourceDTO reusableResourceDTO, Long userID) {
		ReusableResource reusableResource = (ReusableResource) mappingService.mapESponderEntityDTO(reusableResourceDTO, ReusableResource.class);
		reusableResource = updateReusableResource(reusableResource, userID);
		reusableResourceDTO = (ReusableResourceDTO) mappingService.mapESponderEntity(reusableResource, ReusableResourceDTO.class);
		return reusableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#updateReusableResource(eu.esponder.model.crisis.resource.ReusableResource, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReusableResource updateReusableResource(ReusableResource reusableResource, Long userID) {
		ReusableResource resourcePersisted = findReusableResourceById(reusableResource.getId());
		mappingService.mapEntityToEntity(reusableResource, resourcePersisted);
		return (ReusableResource) reusablesCrudService.update(resourcePersisted);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#updateRegisteredReusableResourceRemote(eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO, java.lang.Long)
	 */
	@Override
	public RegisteredReusableResourceDTO updateRegisteredReusableResourceRemote(RegisteredReusableResourceDTO registeredReusableResourceDTO, Long userID) {
		RegisteredReusableResource registeredReusableResource = (RegisteredReusableResource) mappingService.mapESponderEntityDTO(registeredReusableResourceDTO, RegisteredReusableResource.class);
		registeredReusableResource = updateRegisteredReusableResource(registeredReusableResource, userID);
		registeredReusableResourceDTO = (RegisteredReusableResourceDTO) mappingService.mapESponderEntity(registeredReusableResource, RegisteredReusableResourceDTO.class);
		return registeredReusableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#updateRegisteredReusableResource(eu.esponder.model.crisis.resource.RegisteredReusableResource, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredReusableResource updateRegisteredReusableResource(RegisteredReusableResource registeredReusableResource, Long userID) {
		RegisteredReusableResource reusablePersisted = findRegisteredReusableResourceById(registeredReusableResource.getId());
		mappingService.mapEntityToEntity(registeredReusableResource, reusablePersisted);
		return (RegisteredReusableResource) registeredReusablesCrudService.update(reusablePersisted);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#updateConsumableResourceRemote(eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO, java.lang.Long)
	 */
	@Override
	public ConsumableResourceDTO updateConsumableResourceRemote(ConsumableResourceDTO consumableResourceDTO, Long userID) {
		ConsumableResource consumableResource = (ConsumableResource) mappingService.mapESponderEntityDTO(consumableResourceDTO, ConsumableResource.class);
		consumableResource = updateConsumableResource(consumableResource, userID);
		consumableResourceDTO = (ConsumableResourceDTO) mappingService.mapESponderEntity(consumableResource, ReusableResourceDTO.class);
		return consumableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#updateConsumableResource(eu.esponder.model.crisis.resource.ConsumableResource, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ConsumableResource updateConsumableResource(ConsumableResource consumableResource, Long userID) {
		ConsumableResource resourcePersisted = consumablesCrudService.find(ConsumableResource.class, consumableResource.getId());
		mappingService.mapEntityToEntity(consumableResource, resourcePersisted);
		return (ConsumableResource) consumablesCrudService.update(resourcePersisted);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsRemoteService#updateRegisteredConsumableResourceRemote(eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO, java.lang.Long)
	 */
	@Override
	public RegisteredConsumableResourceDTO updateRegisteredConsumableResourceRemote(RegisteredConsumableResourceDTO registeredConsumableResourceDTO, Long userID) {
		RegisteredConsumableResource registeredConsumableResource = (RegisteredConsumableResource) mappingService.mapESponderEntityDTO(registeredConsumableResourceDTO, RegisteredConsumableResource.class);
		registeredConsumableResource = updateRegisteredConsumableResource(registeredConsumableResource, userID);
		registeredConsumableResourceDTO = (RegisteredConsumableResourceDTO) mappingService.mapESponderEntity(registeredConsumableResource, RegisteredConsumableResourceDTO.class);
		return registeredConsumableResourceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.LogisticsService#updateRegisteredConsumableResource(eu.esponder.model.crisis.resource.RegisteredConsumableResource, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredConsumableResource updateRegisteredConsumableResource(RegisteredConsumableResource registeredConsumableResource, Long userID) {
		RegisteredConsumableResource regResourcePersisted = registeredConsumablesCrudService.find(RegisteredConsumableResource.class, registeredConsumableResource.getId());
		mappingService.mapEntityToEntity(registeredConsumableResource, regResourcePersisted);
		return (RegisteredConsumableResource) registeredConsumablesCrudService.update(regResourcePersisted);
	}

}
