/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface PersonnelRemoteService.
 */
@Remote
public interface PersonnelRemoteService {

	/**
	 * Find personnel by id remote.
	 *
	 * @param personnelID the personnel id
	 * @return the personnel dto
	 */
	public PersonnelDTO findPersonnelByIdRemote(Long personnelID);

	/**
	 * Find personnel by title remote.
	 *
	 * @param personnelTitle the personnel title
	 * @return the personnel dto
	 */
	public PersonnelDTO findPersonnelByTitleRemote(String personnelTitle);

	/**
	 * Creates the personnel remote.
	 *
	 * @param personnelDTO the personnel dto
	 * @param userID the user id
	 * @return the personnel dto
	 */
	public PersonnelDTO createPersonnelRemote(PersonnelDTO personnelDTO, Long userID);

	/**
	 * Update personnel remote.
	 *
	 * @param personnelDTO the personnel dto
	 * @param userID the user id
	 * @return the personnel dto
	 */
	public PersonnelDTO updatePersonnelRemote(PersonnelDTO personnelDTO, Long userID);

	/**
	 * Delete personnel remote.
	 *
	 * @param personnelDTO the personnel dto
	 */
	public void deletePersonnelRemote(PersonnelDTO personnelDTO);

	/**
	 * Delete personnel by id remote.
	 *
	 * @param personnelDTOId the personnel dto id
	 */
	public void deletePersonnelByIdRemote(Long personnelDTOId);

	/**
	 * Find personnel competence by id remote.
	 *
	 * @param personnelCompetenceID the personnel competence id
	 * @return the personnel competence dto
	 */
	public PersonnelCompetenceDTO findPersonnelCompetenceByIdRemote(Long personnelCompetenceID);

	/**
	 * Find personnel competence by title remote.
	 *
	 * @param personnelCompetenceTitle the personnel competence title
	 * @return the personnel competence dto
	 */
	public PersonnelCompetenceDTO findPersonnelCompetenceByTitleRemote(String personnelCompetenceTitle);

	/**
	 * Creates the personnel competence remote.
	 *
	 * @param personnelCompetenceDTO the personnel competence dto
	 * @param userID the user id
	 * @return the personnel competence dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public PersonnelCompetenceDTO createPersonnelCompetenceRemote(PersonnelCompetenceDTO personnelCompetenceDTO, Long userID) throws ClassNotFoundException;

	/**
	 * Update personnel competence remote.
	 *
	 * @param personnelCompetenceDTO the personnel competence dto
	 * @param userID the user id
	 * @return the personnel competence dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public PersonnelCompetenceDTO updatePersonnelCompetenceRemote(PersonnelCompetenceDTO personnelCompetenceDTO, Long userID) throws ClassNotFoundException;

	/**
	 * Delete personnel competence by id remote.
	 *
	 * @param personnelCompetenceDTOId the personnel competence dto id
	 */
	public void deletePersonnelCompetenceByIdRemote(Long personnelCompetenceDTOId);

	/**
	 * Find all personnel remote.
	 *
	 * @return the list
	 */
	public List<PersonnelDTO> findAllPersonnelRemote();

	/**
	 * Find all personnel competences remote.
	 *
	 * @return the list
	 */
	public List<PersonnelCompetenceDTO> findAllPersonnelCompetencesRemote();

}
