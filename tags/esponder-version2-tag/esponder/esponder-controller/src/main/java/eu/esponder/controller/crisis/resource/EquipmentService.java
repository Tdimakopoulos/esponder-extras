/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.Date;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.snapshot.resource.EquipmentSnapshot;

// TODO: Auto-generated Javadoc
/**
 * The Interface EquipmentService.
 */
@Local
public interface EquipmentService extends EquipmentRemoteService {
	
	/**
	 * Find by id.
	 *
	 * @param equipmentID the equipment id
	 * @return the equipment
	 */
	public Equipment findById(Long equipmentID);
	
	/**
	 * Find by title.
	 *
	 * @param title the title
	 * @return the equipment
	 */
	public Equipment findByTitle(String title);
	
	/**
	 * Creates the equipment.
	 *
	 * @param equipment the equipment
	 * @param userID the user id
	 * @return the equipment
	 */
	public Equipment createEquipment(Equipment equipment, Long userID);
	
	/**
	 * Update equipment.
	 *
	 * @param equipment the equipment
	 * @param userID the user id
	 * @return the equipment
	 */
	public Equipment updateEquipment(Equipment equipment, Long userID);
	
	/**
	 * Delete equipment.
	 *
	 * @param equipmentId the equipment id
	 * @param userID the user id
	 */
	public void deleteEquipment(Long equipmentId, Long userID);
	
	/**
	 * Find equipment snapshot by date.
	 *
	 * @param equipmentID the equipment id
	 * @param dateTo the date to
	 * @return the equipment snapshot
	 */
	public EquipmentSnapshot findEquipmentSnapshotByDate(Long equipmentID, Date dateTo);
	
	/**
	 * Creates the equipment snapshot.
	 *
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the equipment snapshot
	 */
	public EquipmentSnapshot createEquipmentSnapshot(EquipmentSnapshot snapshot, Long userID);

	/**
	 * Update equipment snapshot.
	 *
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the equipment snapshot
	 */
	public EquipmentSnapshot updateEquipmentSnapshot(EquipmentSnapshot snapshot, Long userID);

	/**
	 * Delete equipment snapshot.
	 *
	 * @param equipmentSnapshotId the equipment snapshot id
	 * @param userID the user id
	 */
	public void deleteEquipmentSnapshot(Long equipmentSnapshotId, Long userID);
	
}
