/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Personnel;
import eu.esponder.model.crisis.resource.PersonnelCompetence;

// TODO: Auto-generated Javadoc
/**
 * The Interface PersonnelService.
 */
@Local
public interface PersonnelService {

	/**
	 * Find personnel by id.
	 *
	 * @param personnelID the personnel id
	 * @return the personnel
	 */
	public Personnel findPersonnelById(Long personnelID);

	/**
	 * Find personnel by title.
	 *
	 * @param personnelTitle the personnel title
	 * @return the personnel
	 */
	public Personnel findPersonnelByTitle(String personnelTitle);

	/**
	 * Creates the personnel.
	 *
	 * @param personnel the personnel
	 * @param userID the user id
	 * @return the personnel
	 */
	public Personnel createPersonnel(Personnel personnel, Long userID);

	/**
	 * Update personnel.
	 *
	 * @param personnel the personnel
	 * @param userID the user id
	 * @return the personnel
	 */
	public Personnel updatePersonnel(Personnel personnel, Long userID);

	/**
	 * Delete personnel.
	 *
	 * @param personnel the personnel
	 */
	public void deletePersonnel(Personnel personnel);

	/**
	 * Delete personnel by id.
	 *
	 * @param personnelID the personnel id
	 */
	public void deletePersonnelByID(Long personnelID);

	/**
	 * Find personnel competence by id.
	 *
	 * @param personnelCompetenceID the personnel competence id
	 * @return the personnel competence
	 */
	public PersonnelCompetence findPersonnelCompetenceById(Long personnelCompetenceID);

	/**
	 * Find personnel competence by title.
	 *
	 * @param personnelCompetenceTitle the personnel competence title
	 * @return the personnel competence
	 */
	public PersonnelCompetence findPersonnelCompetenceByTitle(String personnelCompetenceTitle);

	/**
	 * Creates the personnel competence.
	 *
	 * @param personnelCompetence the personnel competence
	 * @param userID the user id
	 * @return the personnel competence
	 */
	public PersonnelCompetence createPersonnelCompetence(PersonnelCompetence personnelCompetence, Long userID);

	/**
	 * Update personnel competence.
	 *
	 * @param personnelCompetence the personnel competence
	 * @param userID the user id
	 * @return the personnel competence
	 */
	public PersonnelCompetence updatePersonnelCompetence(PersonnelCompetence personnelCompetence, Long userID);

	/**
	 * Delete personnel competence by id.
	 *
	 * @param personnelCompetenceID the personnel competence id
	 */
	public void deletePersonnelCompetenceByID(Long personnelCompetenceID);

	/**
	 * Find all personnel.
	 *
	 * @return the list
	 */
	public List<Personnel> findAllPersonnel();

	/**
	 * Find all personnel competences.
	 *
	 * @return the list
	 */
	public List<PersonnelCompetence> findAllPersonnelCompetences();
	
}
