/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.FRTeam;
import eu.esponder.model.snapshot.resource.ActorSnapshot;

// TODO: Auto-generated Javadoc
/**
 * The Interface ActorService.
 */
@Local
public interface ActorService extends ActorRemoteService {
	
	/**
	 * Find by id.
	 *
	 * @param actorID the actor id
	 * @return the actor
	 */
	public Actor findById(Long actorID);
	
	/**
	 * Find by title.
	 *
	 * @param title the title
	 * @return the actor
	 */
	public Actor findByTitle(String title);
	
	/**
	 * Find subordinates by id.
	 *
	 * @param actorID the actor id
	 * @return the list
	 */
	public List<Actor> findSubordinatesById(Long actorID);
	
	/**
	 * Creates the actor.
	 *
	 * @param actor the actor
	 * @param userID the user id
	 * @return the actor
	 */
	public Actor createActor(Actor actor, Long userID);
	
	/**
	 * Update actor.
	 *
	 * @param actor the actor
	 * @param userID the user id
	 * @return the actor
	 */
	public Actor updateActor(Actor actor, Long userID);
	
	/**
	 * Delete actor.
	 *
	 * @param actorID the actor id
	 * @param userID the user id
	 */
	public void deleteActor(Long actorID, Long userID);
	
	/**
	 * Find actor snapshot by date.
	 *
	 * @param actorID the actor id
	 * @param dateTo the date to
	 * @return the actor snapshot
	 */
	public ActorSnapshot findActorSnapshotByDate(Long actorID, Date dateTo);
	
	/**
	 * Find actor snapshot by id.
	 *
	 * @param actorID the actor id
	 * @return the actor snapshot
	 */
	public ActorSnapshot findActorSnapshotById(Long actorID);
	
	/**
	 * Creates the actor snapshot.
	 *
	 * @param snapshot the snapshot
	 * @param userID the user id
	 * @return the actor snapshot
	 */
	public ActorSnapshot createActorSnapshot(ActorSnapshot snapshot, Long userID);
	
	/**
	 * Update actor snapshot.
	 *
	 * @param actorSnapshot the actor snapshot
	 * @param userID the user id
	 * @return the actor snapshot
	 */
	public ActorSnapshot updateActorSnapshot(ActorSnapshot actorSnapshot, Long userID);
	
	/**
	 * Delete actor snapshot.
	 *
	 * @param actorSnapshotID the actor snapshot id
	 * @param userID the user id
	 */
	public void deleteActorSnapshot(Long actorSnapshotID, Long userID);

	/**
	 * Find all actors.
	 *
	 * @return the list
	 */
	public List<Actor> findAllActors();

	/**
	 * Find supervisor for actor.
	 *
	 * @param actorID the actor id
	 * @return the actor
	 */
	public Actor findSupervisorForActor(Long actorID);

	/**
	 * Find fr team by id.
	 *
	 * @param frTeamID the fr team id
	 * @return the fR team
	 */
	public FRTeam findFRTeamById(Long frTeamID);

	/**
	 * Find fr team chief by id.
	 *
	 * @param frTeamID the fr team id
	 * @return the actor
	 */
	public Actor findFRTeamChiefById(Long frTeamID);

	/**
	 * Find fr teams for meoc.
	 *
	 * @param meocID the meoc id
	 * @return the list
	 */
	public List<FRTeam> findFRTeamsForMeoc(Long meocID);

	/**
	 * Find fr team for chief.
	 *
	 * @param frchiefID the frchief id
	 * @return the fR team
	 */
	public FRTeam findFRTeamForChief(Long frchiefID);
	
}
