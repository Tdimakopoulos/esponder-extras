/*
 * 
 */
package eu.esponder.controller.crisis.resource.category;

import java.util.Set;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.EquipmentCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OperationsCentreCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.dto.model.type.OperationsCentreTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.dto.model.type.RankTypeDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ResourceCategoryRemoteService.
 */
@Remote
public interface ResourceCategoryRemoteService {

	/**
	 * Creates the.
	 *
	 * @param resourceCategoryDTO the resource category dto
	 * @param UserID the user id
	 * @return the resource category dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ResourceCategoryDTO create(ResourceCategoryDTO resourceCategoryDTO, Long UserID) throws ClassNotFoundException;
	
	/**
	 * Update organization category.
	 *
	 * @param organizationCategory the organization category
	 * @param disciplineTypeId the discipline type id
	 * @param organizationTypeId the organization type id
	 * @param organisationID the organisation id
	 * @param userId the user id
	 * @return the organisation category dto
	 */
	public OrganisationCategoryDTO updateOrganizationCategory(Long organizationCategory, Long disciplineTypeId, Long organizationTypeId,  Long organisationID, Long userId);
	
	/**
	 * Update equipment category.
	 *
	 * @param equipmentCategoryId the equipment category id
	 * @param equipmentTypeId the equipment type id
	 * @param userId the user id
	 * @return the equipment category dto
	 */
	public EquipmentCategoryDTO updateEquipmentCategory(Long equipmentCategoryId, Long equipmentTypeId, Long userId);
	
	/**
	 * Update consumable resource category.
	 *
	 * @param consumableCategoryId the consumable category id
	 * @param consumableTypeId the consumable type id
	 * @param userId the user id
	 * @return the consumable resource category dto
	 */
	public ConsumableResourceCategoryDTO updateConsumableResourceCategory(Long consumableCategoryId, Long consumableTypeId, Long userId);
	
	/**
	 * Update reusable resource category.
	 *
	 * @param reusableCategoryId the reusable category id
	 * @param reusableTypeId the reusable type id
	 * @param userId the user id
	 * @return the reusable resource category dto
	 */
	public ReusableResourceCategoryDTO updateReusableResourceCategory(Long reusableCategoryId, Long reusableTypeId,	Long userId);
	
	/**
	 * Update personnel category.
	 *
	 * @param personnelCategoryId the personnel category id
	 * @param competenceId the competence id
	 * @param rankId the rank id
	 * @param organizationCategoryId the organization category id
	 * @param userId the user id
	 * @return the personnel category dto
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	public PersonnelCategoryDTO updatePersonnelCategory(Long personnelCategoryId, Set<Long> competenceId, Long rankId, Long organizationCategoryId, Long userId) throws InstantiationException, IllegalAccessException;

	/**
	 * Find by id remote.
	 *
	 * @param clz the clz
	 * @param resourceCategoryID the resource category id
	 * @return the resource category dto
	 */
	public ResourceCategoryDTO findByIdRemote(Class<? extends ResourceCategoryDTO> clz, Long resourceCategoryID);

	/**
	 * Find organisation category dto by type.
	 *
	 * @param disciplineType the discipline type
	 * @param organizationType the organization type
	 * @return the organisation category dto
	 */
	public OrganisationCategoryDTO findOrganisationCategoryDTOByType(DisciplineTypeDTO disciplineType, OrganisationTypeDTO organizationType);

	/**
	 * Find equipment category dto by type.
	 *
	 * @param equipmentTypeDTO the equipment type dto
	 * @return the equipment category dto
	 */
	public EquipmentCategoryDTO findEquipmentCategoryDTOByType(EquipmentTypeDTO equipmentTypeDTO);

	/**
	 * Find consumable category dto by type.
	 *
	 * @param consumablesType the consumables type
	 * @return the consumable resource category dto
	 */
	public ConsumableResourceCategoryDTO findConsumableCategoryDTOByType(ConsumableResourceTypeDTO consumablesType);

	/**
	 * Find reusable category dto by type.
	 *
	 * @param reusablesType the reusables type
	 * @return the reusable resource category dto
	 */
	public ReusableResourceCategoryDTO findReusableCategoryDTOByType(ReusableResourceTypeDTO reusablesType);

	/**
	 * Find operations centre category dto by type.
	 *
	 * @param operationsCentreTypeDTO the operations centre type dto
	 * @return the operations centre category dto
	 */
	public OperationsCentreCategoryDTO findOperationsCentreCategoryDTOByType(OperationsCentreTypeDTO operationsCentreTypeDTO);

	/**
	 * Find personnel category dto by type.
	 *
	 * @param rankType the rank type
	 * @param competence the competence
	 * @param organisationCategory the organisation category
	 * @return the personnel category dto
	 */
	public PersonnelCategoryDTO findPersonnelCategoryDTOByType(RankTypeDTO rankType,
			Set<PersonnelCompetenceDTO> competence,
			OrganisationCategoryDTO organisationCategory);

	/**
	 * Update operations centre category.
	 *
	 * @param operationsCentreCategoryID the operations centre category id
	 * @param operationsCentreTypeId the operations centre type id
	 * @param userID the user id
	 * @return the operations centre category dto
	 */
	public OperationsCentreCategoryDTO updateOperationsCentreCategory(Long operationsCentreCategoryID, Long operationsCentreTypeId, Long userID);

	/**
	 * Delete resource category remote.
	 *
	 * @param resourceCategoryID the resource category id
	 * @return the long
	 */
	public Long deleteResourceCategoryRemote(Long resourceCategoryID);
	
}
