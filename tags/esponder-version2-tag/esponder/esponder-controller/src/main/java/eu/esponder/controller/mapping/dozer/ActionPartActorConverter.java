/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionPartActorConverter.
 */
public class ActionPartActorConverter implements CustomConverter {
	
	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Gets the actor service.
	 *
	 * @return the actor service
	 */
	protected ActorService getActorService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if (sourceClass == Actor.class && source != null) {
			Actor sourceActor = (Actor) source;
			ActorDTO destActorDTO = new ActorDTO();
			destActorDTO.setId(sourceActor.getId());
			destActorDTO.setTitle(sourceActor.getTitle());
			destActorDTO.setType(sourceActor.getActorType().getTitle());
			destination = destActorDTO;
		}
		else if(sourceClass == ActorDTO.class && source!=null) { 
			ActorDTO sourceActorDTO = (ActorDTO) source;
			Actor destActor = this.getActorService().findById(sourceActorDTO.getId());
			destination = destActor;
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
