/*
 * 
 */
package eu.esponder.controller.crisis.resource.bean;

import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.model.crisis.resource.Personnel;
import eu.esponder.model.crisis.resource.PersonnelCompetence;

// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelBean.
 */
@Stateless
public class PersonnelBean implements PersonnelService, PersonnelRemoteService {

	/** The personnel crud service. */
	@EJB
	private CrudService<Personnel> personnelCrudService;

	/** The personnel competence crud service. */
	@EJB
	private CrudService<PersonnelCompetence> personnelCompetenceCrudService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#findPersonnelByIdRemote(java.lang.Long)
	 */
	@Override
	public PersonnelDTO findPersonnelByIdRemote(Long personnelID) {
		Personnel personnel = findPersonnelById(personnelID);
		PersonnelDTO personnelDTO = (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
		return  personnelDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#findPersonnelById(java.lang.Long)
	 */
	@Override
	public Personnel findPersonnelById(Long personnelID) {
		return (Personnel) personnelCrudService.find(Personnel.class, personnelID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#findAllPersonnelRemote()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<PersonnelDTO> findAllPersonnelRemote() {
		return (List<PersonnelDTO>) mappingService.mapESponderEntity(findAllPersonnel(), PersonnelDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#findAllPersonnel()
	 */
	@Override
	public List<Personnel> findAllPersonnel() {
		return (List<Personnel>) personnelCrudService.findWithNamedQuery("Personnel.findAll");
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#findAllPersonnelCompetencesRemote()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<PersonnelCompetenceDTO> findAllPersonnelCompetencesRemote() {
		return (List<PersonnelCompetenceDTO>) mappingService.mapESponderEntity(findAllPersonnelCompetences(), PersonnelCompetenceDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#findAllPersonnelCompetences()
	 */
	@Override
	public List<PersonnelCompetence> findAllPersonnelCompetences() {
		return (List<PersonnelCompetence>) personnelCompetenceCrudService.findWithNamedQuery("PersonnelCompetence.findAll");
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#findPersonnelCompetenceByIdRemote(java.lang.Long)
	 */
	@Override
	public PersonnelCompetenceDTO findPersonnelCompetenceByIdRemote(Long personnelCompetenceID) {
		PersonnelCompetence personnelCompetence = findPersonnelCompetenceById(personnelCompetenceID);
		PersonnelCompetenceDTO personnelCompetenceDTO = (PersonnelCompetenceDTO) mappingService.mapESponderEntity(personnelCompetence, PersonnelCompetenceDTO.class);
		return  personnelCompetenceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#findPersonnelCompetenceById(java.lang.Long)
	 */
	@Override
	public PersonnelCompetence findPersonnelCompetenceById(Long personnelCompetenceID) {
		return (PersonnelCompetence) personnelCompetenceCrudService.find(PersonnelCompetence.class, personnelCompetenceID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#findPersonnelByTitleRemote(java.lang.String)
	 */
	@Override
	public PersonnelDTO findPersonnelByTitleRemote(String personnelTitle) {
		Personnel personnel = findPersonnelByTitle(personnelTitle);
		PersonnelDTO personnelDTO = (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
		return personnelDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#findPersonnelByTitle(java.lang.String)
	 */
	@Override
	public Personnel findPersonnelByTitle(String personnelTitle) {
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("title", personnelTitle);
		return (Personnel) personnelCrudService.findSingleWithNamedQuery("Personnel.findByTitle", parameters);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#findPersonnelCompetenceByTitleRemote(java.lang.String)
	 */
	@Override
	public PersonnelCompetenceDTO findPersonnelCompetenceByTitleRemote(String personnelCompetenceTitle) {
		PersonnelCompetence personnelCompetence = findPersonnelCompetenceByTitle(personnelCompetenceTitle);
		PersonnelCompetenceDTO personnelCompetenceDTO = (PersonnelCompetenceDTO) mappingService.mapESponderEntity(personnelCompetence, PersonnelCompetenceDTO.class);
		return personnelCompetenceDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#findPersonnelCompetenceByTitle(java.lang.String)
	 */
	@Override
	public PersonnelCompetence findPersonnelCompetenceByTitle(String personnelCompetenceTitle) {
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("shortTitle", personnelCompetenceTitle);
		return (PersonnelCompetence) personnelCompetenceCrudService.findSingleWithNamedQuery("PersonnelCompetence.findByTitle", parameters);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#createPersonnelRemote(eu.esponder.dto.model.crisis.resource.PersonnelDTO, java.lang.Long)
	 */
	@Override
	public PersonnelDTO createPersonnelRemote(PersonnelDTO personnelDTO, Long userID) {
		Personnel personnel = (Personnel) mappingService.mapESponderEntityDTO(personnelDTO, Personnel.class);
		personnel = createPersonnel(personnel, userID);
		return (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
	}


	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#createPersonnel(eu.esponder.model.crisis.resource.Personnel, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Personnel createPersonnel(Personnel personnel, Long userID) {
		return (Personnel) personnelCrudService.create(personnel);
	}

	//-------------------------------------------------------------------------

	//FIXME
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#createPersonnelCompetenceRemote(eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO, java.lang.Long)
	 */
	@Override
	public PersonnelCompetenceDTO createPersonnelCompetenceRemote(PersonnelCompetenceDTO personnelCompetenceDTO, Long userID) throws ClassNotFoundException {
		PersonnelCompetence personnelCompetence;
		personnelCompetence = (PersonnelCompetence) mappingService.mapESponderEntityDTO(personnelCompetenceDTO, mappingService.getManagedEntityClass(personnelCompetenceDTO.getClass()));
		personnelCompetence = createPersonnelCompetence(personnelCompetence, userID);
		return (PersonnelCompetenceDTO) mappingService.mapESponderEntity(personnelCompetence, personnelCompetenceDTO.getClass());
	}


	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#createPersonnelCompetence(eu.esponder.model.crisis.resource.PersonnelCompetence, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public PersonnelCompetence createPersonnelCompetence(PersonnelCompetence personnelCompetence, Long userID) {
		return (PersonnelCompetence) personnelCompetenceCrudService.create(personnelCompetence);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#updatePersonnelRemote(eu.esponder.dto.model.crisis.resource.PersonnelDTO, java.lang.Long)
	 */
	@Override
	public PersonnelDTO updatePersonnelRemote(PersonnelDTO personnelDTO, Long userID) {
		Personnel personnel = (Personnel) mappingService.mapESponderEntityDTO(personnelDTO, Personnel.class);
		personnel = updatePersonnel(personnel, userID);
		return (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#updatePersonnel(eu.esponder.model.crisis.resource.Personnel, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Personnel updatePersonnel(Personnel personnel, Long userID) {
		Personnel personnelPersisted = findPersonnelById(personnel.getId());
		mappingService.mapEntityToEntity(personnel, personnelPersisted);
		return (Personnel) personnelCrudService.update(personnelPersisted);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#updatePersonnelCompetenceRemote(eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO, java.lang.Long)
	 */
	@Override
	public PersonnelCompetenceDTO updatePersonnelCompetenceRemote(PersonnelCompetenceDTO personnelCompetenceDTO, Long userID) throws ClassNotFoundException {
		PersonnelCompetence personnelCompetence;
		personnelCompetence = (PersonnelCompetence) mappingService.mapESponderEntityDTO(personnelCompetenceDTO,  mappingService.getManagedEntityClass(personnelCompetenceDTO.getClass()));
		personnelCompetence = updatePersonnelCompetence(personnelCompetence, userID);
		return (PersonnelCompetenceDTO) mappingService.mapESponderEntity(personnelCompetence, personnelCompetenceDTO.getClass());
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#updatePersonnelCompetence(eu.esponder.model.crisis.resource.PersonnelCompetence, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public PersonnelCompetence updatePersonnelCompetence(PersonnelCompetence personnelCompetence, Long userID) {
		return (PersonnelCompetence) personnelCompetenceCrudService.update(personnelCompetence);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#deletePersonnelRemote(eu.esponder.dto.model.crisis.resource.PersonnelDTO)
	 */
	@Override
	public void deletePersonnelRemote(PersonnelDTO personnelDTO) {
		Personnel personnel = (Personnel) mappingService.mapESponderEntityDTO(personnelDTO, Personnel.class);
		deletePersonnel(personnel);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#deletePersonnel(eu.esponder.model.crisis.resource.Personnel)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deletePersonnel(Personnel personnel) {
		personnelCrudService.delete(personnel);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#deletePersonnelByIdRemote(java.lang.Long)
	 */
	@Override
	public void deletePersonnelByIdRemote(Long personnelDTOId) {
		deletePersonnelByID(personnelDTOId);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#deletePersonnelByID(java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deletePersonnelByID(Long personnelID) {
		personnelCrudService.delete(Personnel.class, personnelID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelRemoteService#deletePersonnelCompetenceByIdRemote(java.lang.Long)
	 */
	@Override
	public void deletePersonnelCompetenceByIdRemote(Long personnelCompetenceDTOId) {
		deletePersonnelCompetenceByID(personnelCompetenceDTOId);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.PersonnelService#deletePersonnelCompetenceByID(java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deletePersonnelCompetenceByID(Long personnelCompetenceID) {
		personnelCompetenceCrudService.delete(PersonnelCompetence.class, personnelCompetenceID);
	}

}
