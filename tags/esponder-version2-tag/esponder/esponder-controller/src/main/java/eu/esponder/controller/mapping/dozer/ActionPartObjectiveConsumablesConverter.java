/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.LogisticsService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class ActionPartObjectiveConsumablesConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected LogisticsService getLogisticsService() {
		try {
			return ServiceLocator.getResource("esponder/LogisticsBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				if(it.next().getClass() == ConsumableResource.class) {

					Set<ConsumableResource> sourceConsumablesSet = (Set<ConsumableResource>) source;
					Set<ConsumableResourceDTO> destConsumablesDTOSet = new HashSet<ConsumableResourceDTO>();
					for(ConsumableResource cResource : sourceConsumablesSet) {
						ConsumableResourceDTO destResourceDTO = new ConsumableResourceDTO();
						destResourceDTO.setId(cResource.getId());
						destConsumablesDTOSet.add(destResourceDTO);
					}
					destination = destConsumablesDTOSet;
				}
				else
					if(it.next().getClass() == ConsumableResourceDTO.class) {

						Set<ConsumableResourceDTO> sourceConsumablesDTOSet = (Set<ConsumableResourceDTO>) source;
						Set<ConsumableResource> destConsumablesSet = new HashSet<ConsumableResource>();
						for(ConsumableResourceDTO cResourceDTO : sourceConsumablesDTOSet) {
							ConsumableResource destResource = this.getLogisticsService().findConsumableResourceById(cResourceDTO.getId());
							destConsumablesSet.add(destResource);
						}
						destination = destConsumablesSet;
					}
					else
						destination = null;
			}
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
