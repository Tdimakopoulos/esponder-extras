/*
 * 
 */
package eu.esponder.controller.generic;

import java.util.List;
import java.util.Set;

import javax.ejb.Local;

import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
/**
 * The Interface GenericService.
 */
@Local
public interface GenericService extends GenericRemoteService {

	/**
	 * Gets the entities.
	 *
	 * @param clz the clz
	 * @param ecriterion the ecriterion
	 * @param pageSize the page size
	 * @param pageNumber the page number
	 * @return the entities
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	public List<? extends ESponderEntity<Long>> getEntities(
			Class<? extends ESponderEntity<Long>> clz, EsponderQueryRestriction ecriterion, int pageSize, int pageNumber) 
			throws InstantiationException, IllegalAccessException;

	/**
	 * Creates the entity.
	 *
	 * @param entity the entity
	 * @param userID the user id
	 * @return the e sponder entity
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderEntity<Long> createEntity(ESponderEntity<Long> entity, Long userID) throws ClassNotFoundException;
	
	/**
	 * Update entity.
	 *
	 * @param entity the entity
	 * @param userID the user id
	 * @return the e sponder entity
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderEntity<Long> updateEntity(ESponderEntity<Long> entity, Long userID) throws ClassNotFoundException;
	
	/**
	 * Gets the entity.
	 *
	 * @param clz the clz
	 * @param objectID the object id
	 * @return the entity
	 */
	public ESponderEntity<Long> getEntity( Class<? extends ESponderEntity<Long>> clz, Long objectID);
	
	/**
	 * Delete entity.
	 *
	 * @param targetClass the target class
	 * @param entityID the entity id
	 * @throws ClassNotFoundException the class not found exception
	 */
	public void deleteEntity(Class<? extends ESponderEntity<Long>> targetClass, Long entityID) throws ClassNotFoundException;
	
	/**
	 * Creates the criteriafor bulk find by id.
	 *
	 * @param competenceId the competence id
	 * @return the esponder query restriction
	 */
	public EsponderQueryRestriction createCriteriaforBulkFindById(Set<Long> competenceId);
	
}
