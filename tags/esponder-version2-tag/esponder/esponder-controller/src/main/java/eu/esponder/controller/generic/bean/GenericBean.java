/*
 * 
 */
package eu.esponder.controller.generic.bean;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.generic.GenericService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.controller.persistence.criteria.EsponderCriteriaCollection;
import eu.esponder.controller.persistence.criteria.EsponderCriterion;
import eu.esponder.controller.persistence.criteria.EsponderCriterionExpressionEnum;
import eu.esponder.controller.persistence.criteria.EsponderIntersectionCriteriaCollection;
import eu.esponder.controller.persistence.criteria.EsponderNegationCriteriaCollection;
import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.controller.persistence.criteria.EsponderUnionCriteriaCollection;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.criteria.EsponderCriteriaCollectionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderIntersectionCriteriaCollectionDTO;
import eu.esponder.dto.model.criteria.EsponderNegationCriteriaCollectionDTO;
import eu.esponder.dto.model.criteria.EsponderQueryRestrictionDTO;
import eu.esponder.dto.model.criteria.EsponderUnionCriteriaCollectionDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.util.logger.ESponderLogger;

// TODO: Auto-generated Javadoc
/**
 * The Class GenericBean.
 */
@Stateless
public class GenericBean implements GenericRemoteService, GenericService {
	
	/** The crud service. */
	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;
	
	/**
	 * ************************************************************************************************************************.
	 *
	 * @param queriedDTOClassName the queried dto class name
	 * @param criteriaDTO the criteria dto
	 * @param pageSize the page size
	 * @param pageNumber the page number
	 * @return the DTO entities
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws Exception the exception
	 */

	@SuppressWarnings("unchecked")
	public List<? extends ESponderEntityDTO> getDTOEntities(String queriedDTOClassName,	EsponderQueryRestrictionDTO criteriaDTO, int pageSize, int pageNumber) 
											 throws InstantiationException, IllegalAccessException, Exception {
		
		Class<? extends ESponderEntity<Long>> queriedClass = (Class<? extends ESponderEntity<Long>>) Class.forName(getManagedEntityName(queriedDTOClassName));
//		Class<? extends ESponderEntityDTO> queriedDTOClass = (Class<? extends ESponderEntityDTO>) Class.forName(queriedDTOClassName);
		EsponderQueryRestriction criteria = transformCriteria(criteriaDTO, queriedDTOClassName);
		if (criteria != null) {
			List<? extends ESponderEntity<Long>> resultsList = getEntities(queriedClass, criteria, pageSize, pageNumber);
			if(!resultsList.isEmpty()) {
				String resultDTOClassName = getManagedEntityDTOName(resultsList.get(0).getClass().getName());
				Class<? extends ESponderEntityDTO> resultDTOClass = (Class<? extends ESponderEntityDTO>) Class.forName(resultDTOClassName);
				List<? extends ESponderEntityDTO> resultListDTO = this.mappingService.mapESponderEntity(resultsList, resultDTOClass);
				return resultListDTO;
			}
			else
				return null;
		}
		else
			return null;
	}
	
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.generic.GenericService#getEntities(java.lang.Class, eu.esponder.controller.persistence.criteria.EsponderQueryRestriction, int, int)
	 */
	@SuppressWarnings({ "unchecked" })
	public List<? extends ESponderEntity<Long>> getEntities(Class<? extends ESponderEntity<Long>> clz, EsponderQueryRestriction criteria, int pageSize, int pageNumber) 
					throws InstantiationException, IllegalAccessException {
		return (List<ESponderEntity<Long>>) crudService.findWithCriteriaQuery(clz, criteria, pageSize, pageNumber);
	}
	
	/**
	 * ************************************************************************************************************************.
	 *
	 * @param entityDTO the entity dto
	 * @param userID the user id
	 * @return the e sponder entity dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public ESponderEntityDTO createEntityRemote( ESponderEntityDTO entityDTO, Long userID) throws ClassNotFoundException {
		Class<? extends ESponderEntity<Long>> targetClass = mappingService.getManagedEntityClass(entityDTO.getClass());
		ESponderEntity<Long> entity = (ESponderEntity<Long>) mappingService.mapESponderEntityDTO(entityDTO, targetClass);
		entity = createEntity(entity, userID);
		return mappingService.mapESponderEntity(entity, entityDTO.getClass());
	}
	
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.generic.GenericService#createEntity(eu.esponder.model.ESponderEntity, java.lang.Long)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderEntity createEntity(ESponderEntity<Long> entity, Long userID) throws ClassNotFoundException {
		return (ESponderEntity) crudService.create(entity);
	}
	
	/**
	 * ************************************************************************************************************************.
	 *
	 * @param idList the id list
	 * @return the esponder query restriction
	 */
	
	public EsponderQueryRestriction createCriteriaforBulkFindById(Set<Long> idList) {
		
		EsponderIntersectionCriteriaCollection criteriaCollection = new EsponderIntersectionCriteriaCollection();
		for(Long id : idList) {
			EsponderCriterion criterion = new EsponderCriterion();
			criterion.setExpression(EsponderCriterionExpressionEnum.EQUAL);
			criterion.setField("id");
			criterion.setValue(id);
			criteriaCollection.add(criterion);
		}
		return criteriaCollection;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.generic.GenericRemoteService#getEntityDTO(java.lang.Class, java.lang.Long)
	 */
	@Override
	public ESponderEntityDTO getEntityDTO( Class<? extends ESponderEntityDTO> clz, Long objectID) throws ClassNotFoundException {
		Class<? extends ESponderEntity<Long>> targetClass = mappingService.getManagedEntityClass(clz);
		ESponderEntity<Long> entity = getEntity(targetClass, objectID);
		return mappingService.mapESponderEntity(entity, clz);
		
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.generic.GenericService#getEntity(java.lang.Class, java.lang.Long)
	 */
	@SuppressWarnings({ "unchecked" })
	public ESponderEntity<Long> getEntity(
			Class<? extends ESponderEntity<Long>> clz, Long objectID) {
		return (ESponderEntity<Long>) crudService.find(clz, objectID);
	}
	
	/**
	 * ************************************************************************************************************************.
	 *
	 * @param entityDTO the entity dto
	 * @param userID the user id
	 * @return the e sponder entity dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ESponderEntityDTO updateEntityRemote(ESponderEntityDTO entityDTO, Long userID) throws ClassNotFoundException {
		Class<? extends ESponderEntity<Long>> targetClass = mappingService.getManagedEntityClass(entityDTO.getClass());
		ESponderEntity<Long> entity = (ESponderEntity<Long>) mappingService.mapESponderEntityDTO(entityDTO, targetClass);
		entity = updateEntity(entity, userID);
		if(entity!= null)
			return mappingService.mapESponderEntity(entity, entityDTO.getClass());
		else
			return null;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.generic.GenericService#updateEntity(eu.esponder.model.ESponderEntity, java.lang.Long)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderEntity<Long> updateEntity(ESponderEntity<Long> entity, Long userID) throws ClassNotFoundException {
		ESponderEntity<Long> persistedEntity = (ESponderEntity<Long>) crudService.find(entity.getClass(), entity.getId());
		if (null != persistedEntity) {
			this.mappingService.mapEntityToEntity(entity, persistedEntity);
			return (ESponderEntity) crudService.update(persistedEntity);
		}
		return null;
	}
	
	/**
	 * ************************************************************************************************************************.
	 *
	 * @param clz the clz
	 * @param entityID the entity id
	 * @throws ClassNotFoundException the class not found exception
	 */
	
	/**
	 * TODO: What happens if the delete fails for a reason? An exception should be thrown
	 */
	
	public void deleteEntityRemote(Class<? extends ESponderEntityDTO> clz, Long entityID) throws ClassNotFoundException {
		Class<? extends ESponderEntity<Long>> targetModelClass = mappingService.getManagedEntityClass(clz);
		deleteEntity(targetModelClass, entityID);
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.generic.GenericService#deleteEntity(java.lang.Class, java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	public void deleteEntity(Class<? extends ESponderEntity<Long>> targetClass, Long entityID) {
		crudService.delete(targetClass, entityID);
	}
	
	/**
	 * ************************************************************************************************************************.
	 *
	 * @param criteria the criteria
	 * @return the esponder query restriction
	 */
	
	
	//FIXME TRANSFERRED FROM ESPONDERMAPPINGBEAN
	public EsponderQueryRestriction mapCriteriaCollection(EsponderQueryRestrictionDTO criteria) {
		if (criteria instanceof EsponderCriterionDTO) {
//			return null != criteria ? MAPPER.transform(criteria, EsponderCriterion.class) : null;
			return (EsponderQueryRestriction) mappingService.mapObject(criteria, EsponderCriterion.class);
		} else if (criteria instanceof EsponderIntersectionCriteriaCollectionDTO) {
//			return null != criteria ? MAPPER.transform(criteria, EsponderIntersectionCriteriaCollection.class) : null;
			return (EsponderQueryRestriction) mappingService.mapObject(criteria, EsponderIntersectionCriteriaCollection.class);
		} else if (criteria instanceof EsponderUnionCriteriaCollectionDTO) {
//			return null != criteria ? MAPPER.transform(criteria, EsponderUnionCriteriaCollection.class) : null;
			return (EsponderQueryRestriction) mappingService.mapObject(criteria, EsponderUnionCriteriaCollection.class);
		} else if (criteria instanceof EsponderNegationCriteriaCollectionDTO) {
//			return null != criteria ? MAPPER.transform(criteria, EsponderNegationCriteriaCollection.class) : null;
			return (EsponderQueryRestriction) mappingService.mapObject(criteria, EsponderNegationCriteriaCollection.class);
		} else {
			ESponderLogger.debug(this.getClass(), "Unknown Criteria Collection, cannot proceed with mapping");
			return null;
		}
	}
	
	//FIXME TRANSFERRED FROM ESPONDERMAPPINGBEAN
	/**
	 * Map simple criterion.
	 *
	 * @param criteria the criteria
	 * @return the esponder criterion
	 */
	public EsponderCriterion mapSimpleCriterion(EsponderCriterionDTO criteria) {
//		return null != criteria ? MAPPER.transform(criteria,EsponderCriterion.class) : null;
		return (EsponderCriterion) mappingService.mapObject(criteria, EsponderCriterion.class);
	}
	
	
	/**
	 * Gets the managed entity name.
	 *
	 * @param queriedEntityDTO the queried entity dto
	 * @return the managed entity name
	 */
	private String getManagedEntityName(String queriedEntityDTO) {
		String queriedEntity = queriedEntityDTO.replaceAll("dto.", "");
		queriedEntity = queriedEntity.replaceAll("DTO", "");
		return queriedEntity;
	}
	
	/**
	 * Gets the managed entity dto name.
	 *
	 * @param queriedEntity the queried entity
	 * @return the managed entity dto name
	 */
	private String getManagedEntityDTOName(String queriedEntity) {
		String queriedEntityDTO = queriedEntity.replaceAll("eu.esponder.model", "eu.esponder.dto.model");
		queriedEntityDTO = queriedEntityDTO.concat("DTO");
		return queriedEntityDTO;
	}
	
	/**
	 * Transform criteria.
	 *
	 * @param criteriaRestrictionsDTO the criteria restrictions dto
	 * @param queriedEntityStr the queried entity str
	 * @return the esponder query restriction
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	private EsponderQueryRestriction transformCriteria(EsponderQueryRestrictionDTO criteriaRestrictionsDTO, String queriedEntityStr) throws Exception {
		Class<? extends ESponderEntity<Long>> queriedClass = (Class<? extends ESponderEntity<Long>>) Class.forName(queriedEntityStr);
		EsponderQueryRestriction criteriaRestrictions = null;

		if (criteriaRestrictionsDTO instanceof EsponderCriteriaCollectionDTO) {
//			criteriaRestrictions = mappingService.mapCriteriaCollection(criteriaRestrictionsDTO);
			criteriaRestrictions = mapCriteriaCollection(criteriaRestrictionsDTO);
			fixEsponderEntityTypes(queriedClass, criteriaRestrictions);
		}
		else if (criteriaRestrictionsDTO instanceof EsponderCriterionDTO) {
//			criteriaRestrictions = mappingService.mapSimpleCriterion((EsponderCriterionDTO) criteriaRestrictionsDTO);
			criteriaRestrictions = mapSimpleCriterion((EsponderCriterionDTO) criteriaRestrictionsDTO);
			fixEsponderEntityTypes(queriedClass, criteriaRestrictions);
		}
		else {
			return null;
		}
		return criteriaRestrictions;
	}
	
	/**
	 * Fix esponder entity types.
	 *
	 * @param queriedClass the queried class
	 * @param restriction the restriction
	 * @throws Exception the exception
	 */
	@SuppressWarnings("rawtypes")
	private void fixEsponderEntityTypes(Class<? extends ESponderEntity> queriedClass, EsponderQueryRestriction restriction) throws Exception {

		if (restriction instanceof EsponderCriterion) {
			fieldTypeFixes(queriedClass, (EsponderCriterion) restriction);
		} else {
			EsponderCriteriaCollection collection = (EsponderCriteriaCollection) restriction;
			for (EsponderQueryRestriction innerRestriction : collection.getRestrictions()) {
				fixEsponderEntityTypes(queriedClass, innerRestriction);
			}
		}
	}

	/**
	 * Field type fixes.
	 *
	 * @param queriedClass the queried class
	 * @param criterion the criterion
	 * @throws Exception the exception
	 */
	@SuppressWarnings("rawtypes")
	private void fieldTypeFixes(
			Class<? extends ESponderEntity> queriedClass,
			EsponderCriterion criterion) throws Exception {
		String targetFieldName = ((EsponderCriterion) criterion).getField();
		Field testField = null;
		
		for (Field field : queriedClass.getDeclaredFields()) {
			if (targetFieldName.equals(field.getName())) {
				testField = field;
				break;
			}
		}

		if (testField == null) {
			Field[] arrayGetField = queriedClass.getFields();
			for (Field field : arrayGetField) {
				if (targetFieldName.equals(field.getName())) {
					testField = field;
					break;
				}
			}
		}

		if (testField == null) {
			List<Field> fieldsList = getAllFields(queriedClass);
			if (fieldsList != null) {
				for (Field field : fieldsList) {
					if (targetFieldName.equals(field.getName())) {
						testField = field;
						break;
					}
				}
			}
		}

		if (testField != null) {
			Class<?> fieldClass = testField.getType();
			Constructor<?> constructor = null;
			constructor = fieldClass.getConstructor(String.class);
			criterion.setValue(constructor.newInstance(criterion.getValue().toString()));
		} else {
			throw new Exception("Field not found");
		}
	}

	/**
	 * Gets the all fields.
	 *
	 * @param clz the clz
	 * @return the all fields
	 */
	private List<Field> getAllFields(Class<?> clz) {
		List<Field> fields = new ArrayList<Field>();
		Class<?> superclass = clz.getSuperclass();
		while (superclass !=null) {
			for (Field field : superclass.getFields()) {
				fields.add(field);
			}
			for (Field field : superclass.getDeclaredFields()) {
				fields.add(field);
			}
			superclass = superclass.getSuperclass();
		}
		return fields;
	}

	
}
