/*
 * 
 */
package eu.esponder.controller.crisis.resource.bean;

import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.resource.OrganisationRemoteService;
import eu.esponder.controller.crisis.resource.OrganisationService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.OrganisationDTO;
import eu.esponder.model.crisis.resource.Organisation;

// TODO: Auto-generated Javadoc
/**
 * The Class OrganisationBean.
 */
@Stateless
public class OrganisationBean implements OrganisationService, OrganisationRemoteService {

	/** The organisation crud service. */
	@EJB
	private CrudService<Organisation> organisationCrudService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationRemoteService#findDTOById(java.lang.Long)
	 */
	@Override
	public OrganisationDTO findDTOById(Long organisationID) {
		Organisation organisation = findById(organisationID);
		OrganisationDTO organisationDTO = (OrganisationDTO) mappingService.mapESponderEntity(organisation, OrganisationDTO.class);
		return  organisationDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationService#findById(java.lang.Long)
	 */
	@Override
	public Organisation findById(Long organisationID) {
		return (Organisation) organisationCrudService.find(Organisation.class, organisationID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationRemoteService#findDTOByTitle(java.lang.String)
	 */
	@Override
	public OrganisationDTO findDTOByTitle(String organisationTitle) {
		Organisation organisation = findByTitle(organisationTitle);
		OrganisationDTO organisationDTO = (OrganisationDTO) mappingService.mapESponderEntity(organisation, OrganisationDTO.class);
		return organisationDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationService#findByTitle(java.lang.String)
	 */
	@Override
	public Organisation findByTitle(String organisationTitle) {
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("title", organisationTitle);
		return (Organisation) organisationCrudService.findSingleWithNamedQuery("Organisation.findByTitle", parameters);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationRemoteService#findAllorganisationsRemote()
	 */
	@Override
	public List<OrganisationDTO> findAllorganisationsRemote() {
		List<Organisation> organisations = findAllorganisations();
		return (List<OrganisationDTO>) mappingService.mapESponderEntity(organisations, OrganisationDTO.class);
		
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationService#findAllorganisations()
	 */
	@Override
	public List<Organisation> findAllorganisations() {
		return (List<Organisation>) organisationCrudService.findWithNamedQuery("Organisation.findAll");
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationRemoteService#createOrganisationDTO(eu.esponder.dto.model.crisis.resource.OrganisationDTO)
	 */
	@Override
	public OrganisationDTO createOrganisationDTO(OrganisationDTO organisationDTO) {
		Organisation organisation = (Organisation) mappingService.mapESponderEntityDTO(organisationDTO, Organisation.class);
		organisation = createOrganisation(organisation);
		return (OrganisationDTO) mappingService.mapESponderEntity(organisation, OrganisationDTO.class);
	}


	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationService#createOrganisation(eu.esponder.model.crisis.resource.Organisation)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Organisation createOrganisation(Organisation organisation) {
		return (Organisation) organisationCrudService.create(organisation);
	}


	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationRemoteService#updateOrganisationDTO(eu.esponder.dto.model.crisis.resource.OrganisationDTO)
	 */
	@Override
	public OrganisationDTO updateOrganisationDTO(OrganisationDTO organisationDTO) {
		Organisation organisation = (Organisation) mappingService.mapESponderEntityDTO(organisationDTO, Organisation.class);
		organisation = updateOrganisation(organisation);
		return (OrganisationDTO) mappingService.mapESponderEntity(organisation, OrganisationDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationService#updateOrganisation(eu.esponder.model.crisis.resource.Organisation)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Organisation updateOrganisation(Organisation organisation) {
		return (Organisation) organisationCrudService.update(organisation);
	}

	//-------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationRemoteService#deleteOrganisationDTO(eu.esponder.dto.model.crisis.resource.OrganisationDTO)
	 */
	@Override
	public void deleteOrganisationDTO(OrganisationDTO organisationDTO) {
		Organisation organisation = (Organisation) mappingService.mapESponderEntityDTO(organisationDTO, Organisation.class);
		deleteOrganisation(organisation);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationService#deleteOrganisation(eu.esponder.model.crisis.resource.Organisation)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteOrganisation(Organisation organisation) {
		organisationCrudService.delete(organisation);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationRemoteService#deleteOrganisationDTO(java.lang.Long)
	 */
	@Override
	public void deleteOrganisationDTO(Long organisationDTOId) {
		deleteOrganisationByID(organisationDTOId);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OrganisationService#deleteOrganisationByID(java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteOrganisationByID(Long organisationID) {
		organisationCrudService.delete(Organisation.class, organisationID);
	}

	//-------------------------------------------------------------------------

}
