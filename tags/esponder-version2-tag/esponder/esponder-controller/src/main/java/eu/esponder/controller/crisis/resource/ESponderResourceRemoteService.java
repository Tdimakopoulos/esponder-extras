/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ESponderResourceRemoteService.
 */
@Remote
public interface ESponderResourceRemoteService {

	/**
	 * Find dto by id.
	 *
	 * @param clz the clz
	 * @param id the id
	 * @return the resource dto
	 */
	public ResourceDTO findDTOById(Class<? extends ResourceDTO> clz, Long id);

	/**
	 * Find dto by title.
	 *
	 * @param clz the clz
	 * @param title the title
	 * @return the resource dto
	 */
	public ResourceDTO findDTOByTitle(Class<? extends ResourceDTO> clz, String title);
}
