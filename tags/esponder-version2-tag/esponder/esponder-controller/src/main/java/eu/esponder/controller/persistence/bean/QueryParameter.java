/*
 * 
 */
package eu.esponder.controller.persistence.bean;

import java.util.HashMap;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class QueryParameter.
 */
public class QueryParameter {

	/** The parameters. */
	private Map<String,Object> parameters = null;

	/**
	 * Instantiates a new query parameter.
	 *
	 * @param name the name
	 * @param value the value
	 */
	private QueryParameter(String name,Object value){
		this.parameters = new HashMap<String,Object>();
		this.parameters.put(name, value);
	}
	
	/**
	 * With.
	 *
	 * @param name the name
	 * @param value the value
	 * @return the query parameter
	 */
	public static QueryParameter with(String name,Object value){
		return new QueryParameter(name, value);
	}
	
	/**
	 * And.
	 *
	 * @param name the name
	 * @param value the value
	 * @return the query parameter
	 */
	public QueryParameter and(String name,Object value){
		this.parameters.put(name, value);
		return this;
	}
	
	/**
	 * Parameters.
	 *
	 * @return the map
	 */
	public Map<String,Object> parameters(){
		return this.parameters;
	}
	
}

