/*
 * 
 */
package eu.esponder.controller.crisis.resource.category;

import java.util.Set;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Organisation;
import eu.esponder.model.crisis.resource.PersonnelCompetence;
import eu.esponder.model.crisis.resource.category.ConsumableResourceCategory;
import eu.esponder.model.crisis.resource.category.EquipmentCategory;
import eu.esponder.model.crisis.resource.category.OperationsCentreCategory;
import eu.esponder.model.crisis.resource.category.OrganisationCategory;
import eu.esponder.model.crisis.resource.category.PersonnelCategory;
import eu.esponder.model.crisis.resource.category.ResourceCategory;
import eu.esponder.model.crisis.resource.category.ReusableResourceCategory;
import eu.esponder.model.type.ConsumableResourceType;
import eu.esponder.model.type.DisciplineType;
import eu.esponder.model.type.EquipmentType;
import eu.esponder.model.type.OperationsCentreType;
import eu.esponder.model.type.OrganisationType;
import eu.esponder.model.type.RankType;
import eu.esponder.model.type.ReusableResourceType;

// TODO: Auto-generated Javadoc
/**
 * The Interface ResourceCategoryService.
 */
@Local
public interface ResourceCategoryService extends ResourceCategoryRemoteService {

	/**
	 * Creates the.
	 *
	 * @param resourceCategory the resource category
	 * @param UserID the user id
	 * @return the resource category
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ResourceCategory create(ResourceCategory resourceCategory, Long UserID) throws ClassNotFoundException;

	/**
	 * Find by id.
	 *
	 * @param clz the clz
	 * @param resourceCategoryID the resource category id
	 * @return the resource category
	 */
	public ResourceCategory findById(Class<? extends ResourceCategory> clz, Long resourceCategoryID);
	
	/**
	 * Update organization category.
	 *
	 * @param organisationCategory the organisation category
	 * @param disciplineType the discipline type
	 * @param organizationType the organization type
	 * @param organisation the organisation
	 * @param userId the user id
	 * @return the organisation category
	 * @Local updates are performed onto managed entities
	 * nullUpdates 	true: set values to null
	 * false: ignore null values
	 */
	

	public OrganisationCategory updateOrganizationCategory(OrganisationCategory organisationCategory, DisciplineType disciplineType, OrganisationType organizationType,Organisation organisation, Long userId);

	/**
	 * Update equipment category.
	 *
	 * @param equipmentCategory the equipment category
	 * @param equipmentType the equipment type
	 * @param userId the user id
	 * @return the equipment category
	 */
	public EquipmentCategory updateEquipmentCategory(EquipmentCategory equipmentCategory, EquipmentType equipmentType, Long userId);

	/**
	 * Update consumable resource category.
	 *
	 * @param consumableCategory the consumable category
	 * @param consumableType the consumable type
	 * @param userId the user id
	 * @return the consumable resource category
	 */
	public ConsumableResourceCategory updateConsumableResourceCategory(ConsumableResourceCategory consumableCategory, ConsumableResourceType consumableType, Long userId);

	/**
	 * Update reusable resource category.
	 *
	 * @param reusableCategory the reusable category
	 * @param reusableType the reusable type
	 * @param userId the user id
	 * @return the reusable resource category
	 */
	public ReusableResourceCategory updateReusableResourceCategory(ReusableResourceCategory reusableCategory, ReusableResourceType reusableType, Long userId);

	/**
	 * Update personnel category.
	 *
	 * @param personnelCategory the personnel category
	 * @param competences the competences
	 * @param rank the rank
	 * @param organizationCategory the organization category
	 * @param userId the user id
	 * @return the personnel category
	 */
	public PersonnelCategory updatePersonnelCategory(PersonnelCategory personnelCategory, Set<PersonnelCompetence> competences, RankType rank, OrganisationCategory organizationCategory, Long userId);

	/**
	 * Find organisation category by type.
	 *
	 * @param disciplineType the discipline type
	 * @param organisationType the organisation type
	 * @return the organisation category
	 */
	public OrganisationCategory findOrganisationCategoryByType(DisciplineType disciplineType, OrganisationType organisationType);

	/**
	 * Find equipment category by type.
	 *
	 * @param equipmentType the equipment type
	 * @return the equipment category
	 */
	public EquipmentCategory findEquipmentCategoryByType(EquipmentType equipmentType);

	/**
	 * Find consumable category by type.
	 *
	 * @param consumablesType the consumables type
	 * @return the consumable resource category
	 */
	public ConsumableResourceCategory findConsumableCategoryByType(ConsumableResourceType consumablesType);

	/**
	 * Find reusable category by type.
	 *
	 * @param reusablesType the reusables type
	 * @return the reusable resource category
	 */
	public ReusableResourceCategory findReusableCategoryByType(ReusableResourceType reusablesType);

	/**
	 * Find operations centre category by type.
	 *
	 * @param operationsCentreType the operations centre type
	 * @return the operations centre category
	 */
	public OperationsCentreCategory findOperationsCentreCategoryByType(OperationsCentreType operationsCentreType);

	/**
	 * Find personnel category by type.
	 *
	 * @param rankType the rank type
	 * @param personnelCompetence the personnel competence
	 * @param organisationCategory the organisation category
	 * @return the personnel category
	 */
	public PersonnelCategory findPersonnelCategoryByType(RankType rankType,
			Set<PersonnelCompetence> personnelCompetence,
			OrganisationCategory organisationCategory);

	/**
	 * Update operations centre category.
	 *
	 * @param operationsCentreCategory the operations centre category
	 * @param userId the user id
	 * @return the operations centre category
	 */
	public OperationsCentreCategory updateOperationsCentreCategory(OperationsCentreCategory operationsCentreCategory, Long userId);

	/**
	 * Delete resource category.
	 *
	 * @param resourceCategoryID the resource category id
	 * @return the long
	 */
	public Long deleteResourceCategory(Long resourceCategoryID);
	
}
