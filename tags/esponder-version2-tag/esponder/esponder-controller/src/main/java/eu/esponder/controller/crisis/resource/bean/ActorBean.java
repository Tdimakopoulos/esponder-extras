/*
 * 
 */
package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.crisis.resource.OperationsCentreService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.FRTeam;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.snapshot.resource.ActorSnapshot;
import eu.esponder.model.type.ActorType;

// TODO: Auto-generated Javadoc
/**
 * The Class ActorBean.
 */
@Stateless
public class ActorBean implements ActorService, ActorRemoteService {

	/** The actor crud service. */
	@EJB
	private CrudService<Actor> actorCrudService;

	/** The fr team crud service. */
	@EJB
	private CrudService<FRTeam> frTeamCrudService;

	/** The actor snapshot crud service. */
	@EJB
	private CrudService<ActorSnapshot> actorSnapshotCrudService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;

	/** The operations centre service. */
	@EJB
	private OperationsCentreService operationsCentreService;

	/** The type service. */
	@EJB
	private TypeService typeService;

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRTeamByIdRemote(java.lang.Long)
	 */
	@Override
	public FRTeamDTO findFRTeamByIdRemote(Long frTeamID) {
		return (FRTeamDTO) mappingService.mapESponderEntity(findById(frTeamID), FRTeamDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRTeamById(java.lang.Long)
	 */
	@Override
	public FRTeam findFRTeamById(Long frTeamID) {
		return (FRTeam) frTeamCrudService.find(FRTeam.class, frTeamID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRTeamChiefRemote(java.lang.Long)
	 */
	@Override
	public ActorDTO findFRTeamChiefRemote(Long frTeamID) {
		return (ActorDTO) mappingService.mapESponderEntity(findFRTeamChiefById(frTeamID), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRTeamChiefById(java.lang.Long)
	 */
	@Override
	public Actor findFRTeamChiefById(Long frTeamID) {
		return frTeamCrudService.find(FRTeam.class, frTeamID).getFrchief();
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRTeamsForMeocRemote(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FRTeamDTO> findFRTeamsForMeocRemote(Long meocID) {
		return (List<FRTeamDTO>) mappingService.mapESponderEntity(findFRTeamsForMeoc(meocID), FRTeamDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRTeamsForMeoc(java.lang.Long)
	 */
	@Override
	public List<FRTeam> findFRTeamsForMeoc(Long meocID) {
		OperationsCentre meoc = operationsCentreService.findOperationCentreById(meocID);
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("meoc", meoc);
		return frTeamCrudService.findWithNamedQuery("FRTeam.findTeamsByMeoc", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findFRTeamForChiefRemote(java.lang.Long)
	 */
	@Override
	public FRTeamDTO findFRTeamForChiefRemote(Long frchiefID) {
		return (FRTeamDTO) mappingService.mapESponderEntity(findFRTeamsForMeoc(frchiefID), FRTeamDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findFRTeamForChief(java.lang.Long)
	 */
	@Override
	public FRTeam findFRTeamForChief(Long frchiefID) {
		Actor chief = findById(frchiefID);
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("frchief", chief);
		return (FRTeam) frTeamCrudService.findSingleWithNamedQuery("FRTeam.findTeamsByChief", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findByIdRemote(java.lang.Long)
	 */
	@Override
	public ActorDTO findByIdRemote(Long actorID) {
		return (ActorDTO) mappingService.mapESponderEntity(findById(actorID), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findById(java.lang.Long)
	 */
	@Override
	public Actor findById(Long actorID) {
		return (Actor) actorCrudService.find(Actor.class, actorID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findAllActorsRemote()
	 */
	@Override
	public List<ActorDTO> findAllActorsRemote() {
		return (List<ActorDTO>) mappingService.mapESponderEntity(findAllActors(), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findAllActors()
	 */
	@Override
	public List<Actor> findAllActors() {
		return (List<Actor>) actorCrudService.findWithNamedQuery("Actor.findAll");
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findByTitleRemote(java.lang.String)
	 */
	@Override
	public ActorDTO findByTitleRemote(String title) {
		return (ActorDTO) mappingService.mapESponderEntity(findByTitle(title), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findByTitle(java.lang.String)
	 */
	@Override
	public Actor findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Actor) actorCrudService.findSingleWithNamedQuery("Actor.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findSupervisorForActorRemote(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ActorDTO findSupervisorForActorRemote(Long actorID) {
		return (ActorDTO) mappingService.mapESponderEntity(findSupervisorForActor(actorID), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findSupervisorForActor(java.lang.Long)
	 */
	@Override
	public Actor findSupervisorForActor(Long actorID) {
		return  findById(actorID).getSupervisor(); 
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findSubordinatesByIdRemote(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ActorDTO> findSubordinatesByIdRemote(Long actorID) {
		return (List<ActorDTO>) mappingService.mapESponderEntity(findSubordinatesById(actorID), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findSubordinatesById(java.lang.Long)
	 */
	@Override
	public List<Actor> findSubordinatesById(Long actorID) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("actorID", actorID);
		return  actorCrudService.findWithNamedQuery("Actor.findSubordinatesForActor", parameters); 
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findActorSnapshotByDateRemote(java.lang.Long, java.util.Date)
	 */
	@Override
	public ActorSnapshotDTO findActorSnapshotByDateRemote(Long actorID, Date maxDate) {
		return (ActorSnapshotDTO) mappingService.mapESponderEntity(findActorSnapshotByDate(actorID, maxDate), ActorSnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findActorSnapshotByDate(java.lang.Long, java.util.Date)
	 */
	@Override
	public ActorSnapshot findActorSnapshotByDate(Long actorID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("actorID", actorID);
		params.put("maxDate", maxDate.getTime());
		ActorSnapshot snapshot =
				(ActorSnapshot) actorSnapshotCrudService.findSingleWithNamedQuery("ActorSnapshot.findByActorAndDate", params);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#createActorRemote(eu.esponder.dto.model.crisis.resource.ActorDTO, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorDTO createActorRemote(ActorDTO actorDTO, Long userID) {
		Actor actor = (Actor) mappingService.mapESponderEntityDTO(actorDTO, Actor.class);
		return (ActorDTO) mappingService.mapESponderEntity(createActor(actor, userID), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#createActor(eu.esponder.model.crisis.resource.Actor, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Actor createActor(Actor actor, Long userID) {
		actor.setActorType((ActorType)typeService.findById(actor.getActorType().getId()));
		if (null != actor.getSupervisor()) {
			actor.setSupervisor(this.findById(actor.getSupervisor().getId()));
		}
		if (null != actor.getOperationsCentre()) {
			actor.setOperationsCentre(operationsCentreService.findOperationCentreById(actor.getOperationsCentre().getId()));
		}
		return actorCrudService.create(actor);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#deleteActorRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActorRemote(Long actorID, Long userID) {
		this.deleteActor(actorID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#deleteActor(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActor(Long actorID, Long userID) {
		Actor actor = actorCrudService.find(Actor.class, actorID);
		actorCrudService.delete(actor);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#updateActorRemote(eu.esponder.dto.model.crisis.resource.ActorDTO, java.lang.Long)
	 */
	@Override
	public ActorDTO updateActorRemote(ActorDTO actorDTO, Long userID) {
		Actor actor = (Actor) mappingService.mapESponderEntityDTO(actorDTO, Actor.class);
		return (ActorDTO) mappingService.mapESponderEntity(updateActor(actor, userID), ActorDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#updateActor(eu.esponder.model.crisis.resource.Actor, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Actor updateActor(Actor actor, Long userID) {
		Actor pactor = findById(actor.getId());
		mappingService.mapEntityToEntity(actor, pactor);
		return (Actor) actorCrudService.update(pactor);
	}

	//-------------------------------------------------------------------------		

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#createActorSnapshotRemote(eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO, java.lang.Long)
	 */
	@Override
	public ActorSnapshotDTO createActorSnapshotRemote(ActorSnapshotDTO snapshotDTO, Long userID) {
		ActorSnapshot actorSnapshot = (ActorSnapshot) mappingService.mapESponderEntityDTO(snapshotDTO, ActorSnapshot.class);
		return (ActorSnapshotDTO) mappingService.mapESponderEntity(createActorSnapshot(actorSnapshot, userID), ActorSnapshotDTO.class);
	}		

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#createActorSnapshot(eu.esponder.model.snapshot.resource.ActorSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorSnapshot createActorSnapshot(ActorSnapshot snapshot, Long userID) {
		Actor actor = findById(snapshot.getActor().getId());
		if(actor != null) {
			snapshot.setActor(actor);
			snapshot = actorSnapshotCrudService.create(snapshot);
			return snapshot;
		}
		return null;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#findActorSnapshotByIdRemote(java.lang.Long)
	 */
	@Override
	public ActorSnapshotDTO findActorSnapshotByIdRemote(Long actorID) {
		ActorSnapshot actorSnapshot = findActorSnapshotById(actorID);
		ActorSnapshotDTO actorSnapshotDTO = (ActorSnapshotDTO) mappingService.mapESponderEntity(actorSnapshot, ActorSnapshotDTO.class);
		return actorSnapshotDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#findActorSnapshotById(java.lang.Long)
	 */
	@Override
	public ActorSnapshot findActorSnapshotById(Long actorID) {
		ActorSnapshot actorSnapshot = (ActorSnapshot) actorSnapshotCrudService.find(ActorSnapshot.class, actorID);
		return actorSnapshot;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#updateActorSnapshotRemote(eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO, java.lang.Long)
	 */
	@Override
	public ActorSnapshotDTO updateActorSnapshotRemote(ActorSnapshotDTO actorSnapshotDTO, Long userID) {
		ActorSnapshot actorSnapshot = (ActorSnapshot) mappingService.mapESponderEntityDTO(actorSnapshotDTO, ActorSnapshot.class);
		return (ActorSnapshotDTO) mappingService.mapESponderEntity(updateActorSnapshot(actorSnapshot, userID), ActorSnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#updateActorSnapshot(eu.esponder.model.snapshot.resource.ActorSnapshot, java.lang.Long)
	 */
	@Override
	public ActorSnapshot updateActorSnapshot(ActorSnapshot actorSnapshot, Long userID) {
		return (ActorSnapshot) actorSnapshotCrudService.update(actorSnapshot);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorRemoteService#deleteActorSnapshotRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActorSnapshotRemote(Long actorSnapshotDTOID, Long userID) {
		deleteActorSnapshot(actorSnapshotDTOID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.ActorService#deleteActorSnapshot(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteActorSnapshot(Long actorSnapshotID, Long userID) {
		actorSnapshotCrudService.delete(ActorSnapshot.class, userID);

	}

}
