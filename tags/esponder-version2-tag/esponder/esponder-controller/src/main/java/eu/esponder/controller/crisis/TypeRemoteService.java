/*
 * 
 */
package eu.esponder.controller.crisis;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.type.ESponderTypeDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface TypeRemoteService.
 */
@Remote
public interface TypeRemoteService {
	
	/**
	 * Find dto by title.
	 *
	 * @param title the title
	 * @return the e sponder type dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderTypeDTO findDTOByTitle(String title) throws ClassNotFoundException;

	/**
	 * Find dto by id.
	 *
	 * @param typeId the type id
	 * @return the e sponder type dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderTypeDTO findDTOById(Long typeId) throws ClassNotFoundException;

	/**
	 * Creates the type remote.
	 *
	 * @param typeDTO the type dto
	 * @param userID the user id
	 * @return the e sponder type dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	public ESponderTypeDTO createTypeRemote(ESponderTypeDTO typeDTO, Long userID) throws ClassNotFoundException;
	
	/**
	 * Update type remote.
	 *
	 * @param typeDTO the type dto
	 * @param userID the user id
	 * @return the e sponder type dto
	 */
	public ESponderTypeDTO updateTypeRemote(ESponderTypeDTO typeDTO, Long userID);
	
	/**
	 * Delete type remote.
	 *
	 * @param ESponderTypeDTOID the e sponder type dtoid
	 * @param userID the user id
	 */
	public void deleteTypeRemote(Long ESponderTypeDTOID, Long userID);

	/**
	 * Find dto all types.
	 *
	 * @return the list
	 * @throws ClassNotFoundException the class not found exception
	 */
	public List<ESponderTypeDTO> findDTOAllTypes() throws ClassNotFoundException;

}
