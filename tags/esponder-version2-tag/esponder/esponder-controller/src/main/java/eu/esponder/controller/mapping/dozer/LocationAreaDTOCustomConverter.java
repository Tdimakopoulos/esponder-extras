/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import org.dozer.CustomConverter;

import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.model.snapshot.location.Point;
import eu.esponder.model.snapshot.location.Sphere;

// TODO: Auto-generated Javadoc
/**
 * The Class LocationAreaDTOCustomConverter.
 */
public class LocationAreaDTOCustomConverter implements CustomConverter {

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	public Object convert(Object destination,
			Object source, Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == SphereDTO.class && source != null) {
			SphereDTO sphereDTO = (SphereDTO) source;
			Point centre = new Point(sphereDTO.getCentre().getLatitude(), sphereDTO.getCentre().getLongitude(), sphereDTO.getCentre().getAltitude());
			Sphere sphere = new Sphere(centre, sphereDTO.getRadius());
			sphere.setTitle(sphereDTO.getTitle());
			sphere.setId(sphereDTO.getId());
			return destination = sphere;
		}
		
		if (sourceClass == Sphere.class && source != null) {
			Sphere sphere = (Sphere) source;
			PointDTO centre = new PointDTO(sphere.getCentre().getLatitude(), sphere.getCentre().getLongitude(), sphere.getCentre().getAltitude());
			SphereDTO sphereDTO = new SphereDTO(centre, sphere.getRadius(), sphere.getTitle());
			sphereDTO.setTitle(sphere.getTitle());
			sphereDTO.setId(sphere.getId());
			return destination = sphereDTO;
		}
		
		return null;
	}

}
