/*
 * 
 */
package eu.esponder.controller.persistence.criteria;

import java.util.Collection;


// TODO: Auto-generated Javadoc
/**
 * The Class EsponderCriteriaCollection.
 */
public class EsponderCriteriaCollection extends EsponderQueryRestriction {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5707952404179111086L;

	/** The restrictions. */
	private Collection<EsponderQueryRestriction> restrictions;
	
	/**
	 * Instantiates a new esponder criteria collection.
	 */
	public EsponderCriteriaCollection() { }
	
	/**
	 * Instantiates a new esponder criteria collection.
	 *
	 * @param restrictions the restrictions
	 */
	public EsponderCriteriaCollection(Collection<EsponderQueryRestriction> restrictions) {
		this.restrictions = restrictions;
	}
	
	/**
	 * Gets the restrictions.
	 *
	 * @return the restrictions
	 */
	public Collection<EsponderQueryRestriction> getRestrictions() {
		return restrictions;
	}
	
	/**
	 * Sets the restrictions.
	 *
	 * @param restrictions the new restrictions
	 */
	public void setRestrictions(Collection<EsponderQueryRestriction> restrictions) {
		this.restrictions = restrictions;
	}
	
	/**
	 * Adds the.
	 *
	 * @param restriction the restriction
	 */
	public void add(EsponderQueryRestriction restriction) {
		this.getRestrictions().add(restriction);
	}
	
	/**
	 * Removes the.
	 *
	 * @param restriction the restriction
	 */
	public void remove(EsponderQueryRestriction restriction) {
		this.getRestrictions().remove(restriction);
	}
	
}
