/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class EquipmentActorFieldConverter.
 */
public class EquipmentActorFieldConverter implements CustomConverter {
	
	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if (sourceClass == Actor.class && source != null) {
			Actor sourceActor = (Actor) source;
			ActorDTO destActorDTO = new ActorDTO();
			destActorDTO.setId(sourceActor.getId());
			destination = destActorDTO;
		}
		else if(sourceClass == ActorDTO.class && source!=null) { 
			ActorDTO actorDTO = (ActorDTO) source;
			Actor destActor = (Actor) this.getMappingService().mapESponderEntityDTO((ESponderEntityDTO) actorDTO, (Class<? extends ESponderEntity<Long>>) destinationClass );
			destination = destActor;
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
