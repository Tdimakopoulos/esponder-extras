/*
 * 
 */
package eu.esponder.eventadmin;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import com.prosyst.mprm.common.ManagementException;
import com.prosyst.mprm.rac.RemoteAccessClient;

//import eu.esponder.osgi.settings.OsgiSettings;
import eu.esponder.util.properties.ESponderPropertyLoader;

// TODO: Auto-generated Javadoc
/**
 * The Class ConnectionHandler.
 */
public class ConnectionHandler {

	/** The prop. */
	private static Properties prop = new Properties();

	/** The credentials. */
	private static Hashtable<String, String> credentials = new Hashtable<String, String>();

	/** The user name. */
	private static String userName;
	
	/** The password. */
	private static String password;
	
	/** The server_ uri. */
	private static String server_URI;

	/** The Constant USERNAME_PROPERTY. */
	private static final String USERNAME_PROPERTY = "username";
	
	/** The Constant PASSWORD_PROPERTY. */
	private static final String PASSWORD_PROPERTY = "password";
	
	/** The Constant SERVER_IPADDR_PROPERTY. */
	private static final String SERVER_IPADDR_PROPERTY = "OSGi_Server_IP_Address";
	
	/** The Constant SERVER_PORT_PROPERTY. */
	private static final String SERVER_PORT_PROPERTY = "OSGi_Server_Port";
	
	/** The Constant SERVER_PROTOCOL_PROPERTY. */
	private static final String SERVER_PROTOCOL_PROPERTY = "OSGi_Server_Protocol";
	
	/** The sz status server. */
	private String szStatusServer="";
	
	/** The d settings. */
	OsgiSettings dSettings = new OsgiSettings();

	/** The Constant OSGI_SERVER_URI_PROPERTY. */
	@SuppressWarnings("unused")
	private static final String OSGI_SERVER_URI_PROPERTY = "OSGi_Server_URI";

	/** The Constant EVENT_PROPERTY_NAME. */
	public static final String EVENT_PROPERTY_NAME = "event";

	/** The rac. */
	private RemoteAccessClient rac;
	
	/** The Connection active. */
	private boolean ConnectionActive;

	/**
	 * Instantiates a new connection handler.
	 */
	public ConnectionHandler() {

		ConnectionActive = false;
		String szPropertiesFile = dSettings.getSzPropertiesFileName();

		try {
			prop = ESponderPropertyLoader.getProperties(szPropertiesFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		userName = prop.getProperty(USERNAME_PROPERTY);
		password = prop.getProperty(PASSWORD_PROPERTY);
		server_URI = prop.getProperty(SERVER_PROTOCOL_PROPERTY) + "://"
				+ prop.getProperty(SERVER_IPADDR_PROPERTY) + ":"
				+ prop.getProperty(SERVER_PORT_PROPERTY);
		credentials.put(RemoteAccessClient.USER_PASSWORD, password);
	}

	/**
	 * Instantiates a new connection handler.
	 *
	 * @param userName the user name
	 * @param password the password
	 * @param serverURI the server uri
	 */
	public ConnectionHandler(String userName, String password, String serverURI) {
		ConnectionActive = false;
		ConnectionHandler.userName = userName;
		ConnectionHandler.password = password;
		ConnectionHandler.server_URI = serverURI;

		credentials.put(RemoteAccessClient.USER_PASSWORD, password);
	}

	/**
	 * Connect.
	 *
	 * @return the remote access client
	 * @throws ManagementException the management exception
	 */
	public RemoteAccessClient connect() throws ManagementException {
		rac = RemoteAccessClient.connect(server_URI, userName, credentials,
				null);
		ConnectionActive = true;
		return rac;
	}

	/**
	 * Gets the rac.
	 *
	 * @return the rac
	 */
	public RemoteAccessClient getRac() {
		if (!ConnectionActive)
			return null;
		return rac;
	}

	/**
	 * Sets the rac.
	 *
	 * @param rac the new rac
	 */
	public void setRac(RemoteAccessClient rac) {
		this.rac = rac;
	}

	/**
	 * Close connection.
	 */
	public void CloseConnection() {
		if (ConnectionActive) {
			rac.close();
			ConnectionActive = false;
			
		}
	}

}
