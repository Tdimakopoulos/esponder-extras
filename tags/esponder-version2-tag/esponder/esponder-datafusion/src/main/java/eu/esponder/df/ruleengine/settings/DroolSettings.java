/**
 * DroolSettings
 * 
 * This Java class store all settings need by the project to access the drool repository
 * and identify the temporary local repository using a file path 
 *
 * @Project   esponder
 * @package   Datafusion
 */
package eu.esponder.df.ruleengine.settings;

import java.io.File;

import javax.naming.NamingException;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
//import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.test.ResourceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class DroolSettings.
 *
 * @author tdim
 */
public class DroolSettings {

	/** The szinitial path. */
	String szinitialPath = "c:\\tmprepo\\";
	
	// Variable to store the local repository (file path)
	/** The sz tmp repository directory path. */
	String szTmpRepositoryDirectoryPath = "c:\\tmprepo\\";
	// Drool server URL
	// String szDroolApplicationServerURL="http://localhost:8080/";
	/** The sz drool application server url. */
	String szDroolApplicationServerURL = "http://localhost:8181/";
	// Drool deployment name
	// String szDroolWarDeploymentPath="guvnor-5.4.0.Final-tomcat-6.0";
	/** The sz drool war deployment path. */
	String szDroolWarDeploymentPath = "guvnor";
	// Service entry point
	/** The sz drool service entry point. */
	String szDroolServiceEntryPoint = "rest";
	// Drool username
	/** The sz drool username. */
	String szDroolUsername = "guest";
	// Drool password
	/** The sz drool password. */
	String szDroolPassword = "";
	// Reset repo
	/** The sz drool repo. */
	String szDroolRepo = "local";

	/**
	 * Instantiates a new drool settings.
	 */
	public DroolSettings() {
		try {
			LoadAllOptions();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Instantiates a new drool settings.
	 *
	 * @param bLoad the b load
	 */
	public DroolSettings(boolean bLoad) {
		if (bLoad) {
			try {
				LoadAllOptions();
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Load all options.
	 *
	 * @throws NamingException the naming exception
	 * @throws ClassNotFoundException the class not found exception
	 */
	public void LoadAllOptions() throws NamingException, ClassNotFoundException {
		// System.out.println("Getting Options from DB");
		ESponderConfigurationRemoteService cfgService = ResourceLocator
				.lookup("esponder/ESponderConfigurationBean/remote");

		// Get DroolsTempRep
		ESponderConfigParameterDTO dTempRep = cfgService
				.findESponderConfigByNameRemote("DroolTempRep");
		szTmpRepositoryDirectoryPath = dTempRep.getParameterValue();

		// Get DroolsURL
		ESponderConfigParameterDTO dAppURL = cfgService
				.findESponderConfigByNameRemote("DroolURL");
		szDroolApplicationServerURL = dAppURL.getParameterValue();

		// Get DroolsWarDeployment
		ESponderConfigParameterDTO dWarName = cfgService
				.findESponderConfigByNameRemote("DroolWARNAME");
		szDroolWarDeploymentPath = dWarName.getParameterValue();

		// Get DroolsWarDeployment
		ESponderConfigParameterDTO dSPoint = cfgService
				.findESponderConfigByNameRemote("DroolSPoint");
		szDroolServiceEntryPoint = dSPoint.getParameterValue();

		// Get Droolsusername
		ESponderConfigParameterDTO dDUser = cfgService
				.findESponderConfigByNameRemote("DroolUsername");
		szDroolUsername = dDUser.getParameterValue();

		// Get Droolspassword
		ESponderConfigParameterDTO dDPassword = cfgService
				.findESponderConfigByNameRemote("DroolPassword");
		szDroolPassword = dDPassword.getParameterValue();

		// Get DroolLocalRepoRefresh
		ESponderConfigParameterDTO dDLRepo = cfgService
				.findESponderConfigByNameRemote("DroolRefresh");
		szDroolRepo = dDLRepo.getParameterValue();
		// System.out.println("Getting Options from DB --- "+szDroolRepo);

	}

	/**
	 * Gets the sz drool repo.
	 *
	 * @return the sz drool repo
	 */
	public String getSzDroolRepo() {
		return szDroolRepo;
	}

	/**
	 * Sets the sz drool repo.
	 *
	 * @param szDroolRepo the new sz drool repo
	 */
	public void setSzDroolRepo(String szDroolRepo) {
		//System.out.println("Update Option To DB");
		ESponderConfigurationRemoteService cfgService = ResourceLocator
				.lookup("esponder/ESponderConfigurationBean/remote");

		// Get DroolsTempRep
		// ESponderConfigParameterDTO
		// dTempRep=cfgService.findESponderConfigByNameRemote("DroolTempRep");
		// szTmpRepositoryDirectoryPath=dTempRep.getParameterValue();
		try {
			ESponderConfigParameterDTO dDLRepo = cfgService
					.findESponderConfigByNameRemote("DroolRefresh");
			dDLRepo.setParameterValue(szDroolRepo);
			cfgService.updateESponderConfigRemote(dDLRepo, (long) 1);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.szDroolRepo = szDroolRepo;
	}

	/**
	 * Checks if is unix.
	 *
	 * @return true, if is unix
	 */
	private boolean isUnix() {
		if (File.separatorChar == '/')
			return true;
		else
			return false;
	}
	
	/**
	 * Gets the sz drool username.
	 *
	 * @return drool username (user account manage by drool web interface)
	 */
	public String getSzDroolUsername() {
		return szDroolUsername;
	}

	/**
	 * Sets the sz drool username.
	 *
	 * @param szDroolUsername the new sz drool username
	 */
	public void setSzDroolUsername(String szDroolUsername) {
		this.szDroolUsername = szDroolUsername;
	}

	/**
	 * Gets the sz drool password.
	 *
	 * @return drool password
	 */
	public String getSzDroolPassword() {
		return szDroolPassword;
	}

	/**
	 * Sets the sz drool password.
	 *
	 * @param szDroolPassword the new sz drool password
	 */
	public void setSzDroolPassword(String szDroolPassword) {
		this.szDroolPassword = szDroolPassword;
	}

	/**
	 * Gets the sz tmp repository directory path.
	 *
	 * @return temporary local repository file path
	 */
	public String getSzTmpRepositoryDirectoryPath() {
		//check the path
		//check if unix
		//make correction if need
		// //home//exodus//droolsrepo//
		if (szTmpRepositoryDirectoryPath.equalsIgnoreCase(szinitialPath))
		{
			if (isUnix())
			{
				szTmpRepositoryDirectoryPath="//home//exodus//droolsrepo//";
			}
			
		}
		return szTmpRepositoryDirectoryPath;
	}

	/**
	 * Sets the sz tmp repository directory path.
	 *
	 * @param szTmpRepositoryDirectoryPath the new sz tmp repository directory path
	 */
	public void setSzTmpRepositoryDirectoryPath(
			String szTmpRepositoryDirectoryPath) {
		this.szTmpRepositoryDirectoryPath = szTmpRepositoryDirectoryPath;
	}

	/**
	 * Gets the sz drool application server url.
	 *
	 * @return the url where drool and all sub components are install
	 */
	public String getSzDroolApplicationServerURL() {
		return szDroolApplicationServerURL;
	}

	/**
	 * Sets the sz drool application server url.
	 *
	 * @param szDroolApplicationServerURL the new sz drool application server url
	 */
	public void setSzDroolApplicationServerURL(
			String szDroolApplicationServerURL) {
		this.szDroolApplicationServerURL = szDroolApplicationServerURL;
	}

	/**
	 * Gets the sz drool war deployment path.
	 *
	 * @return drool guvnor deployment path
	 */
	public String getSzDroolWarDeploymentPath() {
		return szDroolWarDeploymentPath;
	}

	/**
	 * Sets the sz drool war deployment path.
	 *
	 * @param szDroolWarDeploymentPath the new sz drool war deployment path
	 */
	public void setSzDroolWarDeploymentPath(String szDroolWarDeploymentPath) {
		this.szDroolWarDeploymentPath = szDroolWarDeploymentPath;
	}

	/**
	 * Gets the sz drool service entry point.
	 *
	 * @return service entry point ( default is rest )
	 */
	public String getSzDroolServiceEntryPoint() {
		return szDroolServiceEntryPoint;
	}

	/**
	 * Sets the sz drool service entry point.
	 *
	 * @param szDroolServiceEntryPoint the new sz drool service entry point
	 */
	public void setSzDroolServiceEntryPoint(String szDroolServiceEntryPoint) {
		this.szDroolServiceEntryPoint = szDroolServiceEntryPoint;
	}
}
