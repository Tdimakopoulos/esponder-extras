/*
 * 
 */
package eu.esponder.df.ruleengine.utilities.ruleresults.object;

import java.util.List;

import org.simpleframework.xml.ElementList;

// TODO: Auto-generated Javadoc
/**
 * The Class RuleResultsXMLConfiguration.
 */
public class RuleResultsXMLConfiguration {

	/** The entries. */
	@ElementList(inline = true)
	private List<RuleResultsXML> entries;

	/**
	 * Gets the entries.
	 *
	 * @return the entries
	 */
	public List<RuleResultsXML> getEntries() {
		return entries;
	}

	/**
	 * Sets the entries.
	 *
	 * @param entries the new entries
	 */
	public void setEntries(List<RuleResultsXML> entries) {
		this.entries = entries;
	}
	
	
}
