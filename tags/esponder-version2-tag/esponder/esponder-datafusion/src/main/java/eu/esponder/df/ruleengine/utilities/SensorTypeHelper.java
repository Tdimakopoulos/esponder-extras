/*
 * 
 */
package eu.esponder.df.ruleengine.utilities;

// TODO: Auto-generated Javadoc
/**
 * The Class SensorTypeHelper.
 */
public class SensorTypeHelper {

	/** The itype. */
	int itype;
	
	/** The iworking. */
	int iworking;
	
	/** The i sensor id. */
	Long iSensorID;
	
	/**
	 * Sets the sensor id.
	 *
	 * @param sensorid the sensorid
	 */
	public void SetSensorID(Long sensorid)
	{
		iSensorID=sensorid;
	}
	
	/**
	 * Gets the sensor id.
	 *
	 * @return the long
	 */
	public Long GetSensorID()
	{
		return iSensorID;
	}
	
	/**
	 * Sets the working.
	 *
	 * @param piworking the piworking
	 */
	public void SetWorking ( int piworking)
	{
		iworking=piworking;
	}
	
	/**
	 * Gets the sensor status.
	 *
	 * @return the int
	 */
	public int GetSensorStatus()
	{
		return iworking;
	}
	
	/**
	 * Sets the type.
	 *
	 * @param pitype the pitype
	 */
	public void SetType(int pitype)
	{
		itype=pitype;
	}
	
	/**
	 * Gets the sensor type.
	 *
	 * @return the int
	 */
	public int GetSensorType()
	{
		return itype;
	}
}
