/*
 * 
 */
package eu.esponder.df.ruleengine.utilities;

// TODO: Auto-generated Javadoc
/**
 * The Class ActorMovementValues.
 */
public class ActorMovementValues {

	/** The p actorname. */
	String pActorname;
	
	/** The plog. */
	Double plog;
	
	/** The plat. */
	Double plat;
	
	/** The palt. */
	Double palt;
	
	/** The pradius. */
	Double pradius;
	
	/**
	 * Gets the p actorname.
	 *
	 * @return the p actorname
	 */
	public String getpActorname() {
		return pActorname;
	}
	
	/**
	 * Sets the p actorname.
	 *
	 * @param pActorname the new p actorname
	 */
	public void setpActorname(String pActorname) {
		this.pActorname = pActorname;
	}
	
	/**
	 * Gets the plog.
	 *
	 * @return the plog
	 */
	public Double getPlog() {
		return plog;
	}
	
	/**
	 * Sets the plog.
	 *
	 * @param plog the new plog
	 */
	public void setPlog(Double plog) {
		this.plog = plog;
	}
	
	/**
	 * Gets the plat.
	 *
	 * @return the plat
	 */
	public Double getPlat() {
		return plat;
	}
	
	/**
	 * Sets the plat.
	 *
	 * @param plat the new plat
	 */
	public void setPlat(Double plat) {
		this.plat = plat;
	}
	
	/**
	 * Gets the palt.
	 *
	 * @return the palt
	 */
	public Double getPalt() {
		return palt;
	}
	
	/**
	 * Sets the palt.
	 *
	 * @param palt the new palt
	 */
	public void setPalt(Double palt) {
		this.palt = palt;
	}
	
	/**
	 * Gets the pradius.
	 *
	 * @return the pradius
	 */
	public Double getPradius() {
		return pradius;
	}
	
	/**
	 * Sets the pradius.
	 *
	 * @param pradius the new pradius
	 */
	public void setPradius(Double pradius) {
		this.pradius = pradius;
	}
	
}
