/*
 * 
 */
package eu.esponder.df.rules.profile;

import eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType;

// TODO: Auto-generated Javadoc
/**
 * The Class ProfileData.
 */
public class ProfileData {

	/** The sz profile name. */
	private String szProfileName;
	
	/** The sz profile type. */
	private RuleEngineType szProfileType;
	
	/** The sz decision table. */
	String szDecisionTable;
	
	/** The has decision table. */
	boolean hasDecisionTable;
	
	/**
	 * Gets the sz decision table.
	 *
	 * @return the sz decision table
	 */
	public String getSzDecisionTable() {
		return szDecisionTable;
	}
	
	/**
	 * Sets the sz decision table.
	 *
	 * @param szDecisionTable the new sz decision table
	 */
	public void setSzDecisionTable(String szDecisionTable) {
		this.szDecisionTable = szDecisionTable;
	}
	
	/**
	 * Checks if is checks for decision table.
	 *
	 * @return true, if is checks for decision table
	 */
	public boolean isHasDecisionTable() {
		return hasDecisionTable;
	}
	
	/**
	 * Sets the checks for decision table.
	 *
	 * @param hasDecisionTable the new checks for decision table
	 */
	public void setHasDecisionTable(boolean hasDecisionTable) {
		this.hasDecisionTable = hasDecisionTable;
	}
	
	/**
	 * Gets the sz profile name.
	 *
	 * @return the sz profile name
	 */
	public String getSzProfileName() {
		return szProfileName;
	}
	
	/**
	 * Sets the sz profile name.
	 *
	 * @param szProfileName the new sz profile name
	 */
	public void setSzProfileName(String szProfileName) {
		this.szProfileName = szProfileName;
	}
	
	/**
	 * Gets the sz profile type.
	 *
	 * @return the sz profile type
	 */
	public RuleEngineType getSzProfileType() {
		return szProfileType;
	}
	
	/**
	 * Sets the sz profile type.
	 *
	 * @param szProfileType the new sz profile type
	 */
	public void setSzProfileType(RuleEngineType szProfileType) {
		this.szProfileType = szProfileType;
	}
	
	
}
