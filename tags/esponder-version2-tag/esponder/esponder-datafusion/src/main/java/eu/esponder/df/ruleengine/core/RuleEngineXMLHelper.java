/**
 * RuleEngineXMLHelper
 * 
 * This Java class implement a simple xml parser for the return of assets web service. The return is a list of
 * urls for all assets
 * 
 * 
 * @Project   esponder
 * @package   Datafusion
*/
package eu.esponder.df.ruleengine.core;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

// TODO: Auto-generated Javadoc
/**
 * The Class RuleEngineXMLHelper.
 *
 * @author tdim
 */
public class RuleEngineXMLHelper {

	/** The bdebug. */
	private boolean bdebug=false;

	/**
	 * Sets the debugging.
	 *
	 * @param bldebug is this set to true a debug log is recorded in system output or server log
	 */
	public void SetDebugging(boolean bldebug) {
		bdebug = bldebug;
	}

	/**
	 * Only handle type drl, we need to handle function as well ? dsl? dslr ?.
	 *
	 * @param szAssets this is the return string from the assets web service
	 * @return list of all assets url
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List ProcessAssets(String szAssets) {

		List dRulesSource = new LinkedList();
		String szState;
		String szSourceLink;
		String szType;
		
		dRulesSource = new ArrayList();
		
		try {

			org.w3c.dom.Document doc = loadXMLFromString(szAssets);
			doc.getDocumentElement().normalize();

			if (bdebug)
				System.out.println("Process Assets Root element :"
						+ doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("asset");

			if (bdebug)
				System.out.println("-----------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					szType=getTagValue("format", eElement);
					szState=getTagValue("state", eElement);
					szSourceLink=getTagValue("sourceLink", eElement);
					if (szState.compareTo("FINISHED")==0)
						if (szType.compareTo("drl")==0)
							dRulesSource.add(szSourceLink);
					
					if (bdebug)
						System.out.println("state : "
								+szState );
					if (bdebug)
						System.out.println("Source link : "
								+szSourceLink );

				}
			}
		} catch (Exception e) {
			if (bdebug)
				e.printStackTrace();
						
			return dRulesSource;
		}
		return dRulesSource;
	}

	/**
	 * Gets the tag value.
	 *
	 * @param sTag the tag name
	 * @param eElement the element
	 * @return the value of tag
	 */
	private String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0)
				.getChildNodes();

		Node nValue = (Node) nlList.item(0);

		return nValue.getNodeValue();
	}

	/**
	 * Load xml from string.
	 *
	 * @param xml the string which holds the xml contents
	 * @return document type of w3c.dom
	 * @throws Exception w3c.dom exception type
	 */
	private org.w3c.dom.Document loadXMLFromString(String xml) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		return builder.parse(is);
	}
}
