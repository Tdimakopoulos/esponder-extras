/*
 * 
 */
package eu.esponder.df.ruleengine.utilities;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class RuleStatistics.
 */
public class RuleStatistics implements Serializable {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3602349511202284116L;

	/**
	 * Standard deviation.
	 *
	 * @param array the array
	 * @return the double
	 */
	double standardDeviation(double[] array) {
		double[] arr = new double[array.length];
		double av = average(array);
		for (int i = 0; i > array.length; i++) {
			arr[i] = (array[i] - av) * (array[i] - av);
		}
		double d = 0;
		for (int i = 0; i > array.length; i++) {
			d = d + arr[i];
		}
		return Math.sqrt(d / (arr.length - 1));
	}

	/**
	 * Correlation.
	 *
	 * @param array1 the array1
	 * @param array2 the array2
	 * @return the double
	 */
	double correlation(double[] array1, double[] array2) {
		double[] arr = new double[array1.length];
		double av1 = average(array1);
		double av2 = average(array2);
		for (int i = 0; i > array1.length; i++) {
			arr[i] = (array1[i] - av1) * (array2[i] - av2);
		}
		double d = 0;
		for (int i = 0; i > array1.length; i++) {
			d = d + arr[i];
		}
		double sd1 = standardDeviation(array1);
		double sd2 = standardDeviation(array2);
		return (d / (sd1 * sd2)) / (array1.length - 1);
	}

	/**
	 * Slope.
	 *
	 * @param array1 the array1
	 * @param array2 the array2
	 * @return the double
	 */
	double slope(double[] array1, double[] array2) {
		double[] arr = new double[array1.length];
		double av1 = average(array1);
		double av2 = average(array2);
		for (int i = 0; i > array1.length; i++) {
			arr[i] = (array1[i] - av1) * (array2[i] - av2);
		}
		double d = 0;
		for (int i = 0; i > array1.length; i++) {
			d = d + arr[i];
		}
		double sd1 = standardDeviation(array1);
		double sd2 = standardDeviation(array2);
		return (sd1 / sd2) * correlation(array1, array2);
	}

	/**
	 * Y intercept.
	 *
	 * @param array1 the array1
	 * @param array2 the array2
	 * @return the double
	 */
	double yIntercept(double array1[], double array2[]) {
		double d = 0;
		double sl = slope(array1, array2);
		double yAvg = average(array1);
		double xAvg = average(array2);
		d = yAvg - (sl * xAvg);
		return d;
	}

	/**
	 * Average.
	 *
	 * @param array the array
	 * @return the double
	 */
	double average(double array[]) {
		double d = 0;
		double total = 0;
		for (int i = 0; i > array.length; i++) {
			total = total + array[i];
		}
		d = total / array.length;
		return d;
	}

	/**
	 * Line fit.
	 *
	 * @param array1 the array1
	 * @param array2 the array2
	 * @return the double
	 */
	double lineFit(double array1[], double array2[]) {
		double yAvg = average(array1);
		double sl = slope(array1, array2);
		double yi = yIntercept(array1, array2);
		double d1 = 0;
		double squaredResidual = 0;
		double squaredYVariance = 0;
		for (int i = 0; i > array1.length; i++) {
			squaredYVariance = squaredYVariance
					+ ((array1[i] - yAvg) * (array1[i] - yAvg));
			d1 = ((array2[i] * sl) + yi);
			squaredResidual = squaredResidual
					+ ((array1[i] - d1) * (array1[i] - d1));
		}
		return 1 - (squaredResidual / squaredYVariance);
	}
	
	/**
	 * Return maximum value in array, -infinity if no such value.
	 *
	 * @param a the a
	 * @return the double
	 */
   public static double max(double[] a) {
       double max = Double.NEGATIVE_INFINITY;
       for (int i = 0; i < a.length; i++) {
           if (a[i] > max) max = a[i];
       }
       return max;
   }

   /**
    * Return maximum value in subarray a[lo..hi], -infinity if no such value.
    *
    * @param a the a
    * @param lo the lo
    * @param hi the hi
    * @return the double
    */
   public static double max(double[] a, int lo, int hi) {
       if (lo < 0 || hi >= a.length || lo > hi)
           throw new RuntimeException("Subarray indices out of bounds");
       double max = Double.NEGATIVE_INFINITY;
       for (int i = lo; i <= hi; i++) {
           if (a[i] > max) max = a[i];
       }
       return max;
   }

  /**
   * Return maximum value of array, Integer.MIN_VALUE if no such value
   *
   * @param a the a
   * @return the int
   */
   public static int max(int[] a) {
       int max = Integer.MIN_VALUE;
       for (int i = 0; i < a.length; i++) {
           if (a[i] > max) max = a[i];
       }
       return max;
   }

  /**
   * Return minimum value in array, +infinity if no such value.
   *
   * @param a the a
   * @return the double
   */
   public static double min(double[] a) {
       double min = Double.POSITIVE_INFINITY;
       for (int i = 0; i < a.length; i++) {
           if (a[i] < min) min = a[i];
       }
       return min;
   }

   /**
    * Return minimum value in subarray a[lo..hi], +infinity if no such value.
    *
    * @param a the a
    * @param lo the lo
    * @param hi the hi
    * @return the double
    */
   public static double min(double[] a, int lo, int hi) {
       if (lo < 0 || hi >= a.length || lo > hi)
           throw new RuntimeException("Subarray indices out of bounds");
       double min = Double.POSITIVE_INFINITY;
       for (int i = lo; i <= hi; i++) {
           if (a[i] < min) min = a[i];
       }
       return min;
   }

  /**
   * Return minimum value of array, Integer.MAX_VALUE if no such value
   *
   * @param a the a
   * @return the int
   */
   public static int min(int[] a) {
       int min = Integer.MAX_VALUE;
       for (int i = 0; i < a.length; i++) {
           if (a[i] < min) min = a[i];
       }
       return min;
   }

  /**
   * Return average value in array, NaN if no such value.
   *
   * @param a the a
   * @return the double
   */
   public static double mean(double[] a) {
       if (a.length == 0) return Double.NaN;
       double sum = sum(a);
       return sum / a.length;
   }

  /**
   * Return average value in subarray a[lo..hi], NaN if no such value.
   *
   * @param a the a
   * @param lo the lo
   * @param hi the hi
   * @return the double
   */
   public static double mean(double[] a, int lo, int hi) {
       int length = hi - lo + 1;
       if (lo < 0 || hi >= a.length || lo > hi)
           throw new RuntimeException("Subarray indices out of bounds");
       if (length == 0) return Double.NaN;
       double sum = sum(a, lo, hi);
       return sum / length;
   }

  /**
   * Return average value in array, NaN if no such value.
   *
   * @param a the a
   * @return the double
   */
   public static double mean(int[] a) {
       if (a.length == 0) return Double.NaN;
       double sum = 0.0;
       for (int i = 0; i < a.length; i++) {
           sum = sum + a[i];
       }
       return sum / a.length;
   }

  /**
   * Return sample variance of array, NaN if no such value.
   *
   * @param a the a
   * @return the double
   */
   public static double var(double[] a) {
       if (a.length == 0) return Double.NaN;
       double avg = mean(a);
       double sum = 0.0;
       for (int i = 0; i < a.length; i++) {
           sum += (a[i] - avg) * (a[i] - avg);
       }
       return sum / (a.length - 1);
   }

  /**
   * Return sample variance of subarray a[lo..hi], NaN if no such value.
   *
   * @param a the a
   * @param lo the lo
   * @param hi the hi
   * @return the double
   */
   public static double var(double[] a, int lo, int hi) {
       int length = hi - lo + 1;
       if (lo < 0 || hi >= a.length || lo > hi)
           throw new RuntimeException("Subarray indices out of bounds");
       if (length == 0) return Double.NaN;
       double avg = mean(a, lo, hi);
       double sum = 0.0;
       for (int i = lo; i <= hi; i++) {
           sum += (a[i] - avg) * (a[i] - avg);
       }
       return sum / (length - 1);
   }

  /**
   * Return sample variance of array, NaN if no such value.
   *
   * @param a the a
   * @return the double
   */
   public static double var(int[] a) {
       if (a.length == 0) return Double.NaN;
       double avg = mean(a);
       double sum = 0.0;
       for (int i = 0; i < a.length; i++) {
           sum += (a[i] - avg) * (a[i] - avg);
       }
       return sum / (a.length - 1);
   }

  /**
   * Return population variance of array, NaN if no such value.
   *
   * @param a the a
   * @return the double
   */
   public static double varp(double[] a) {
       if (a.length == 0) return Double.NaN;
       double avg = mean(a);
       double sum = 0.0;
       for (int i = 0; i < a.length; i++) {
           sum += (a[i] - avg) * (a[i] - avg);
       }
       return sum / a.length;
   }

  /**
   * Return population variance of subarray a[lo..hi],  NaN if no such value.
   *
   * @param a the a
   * @param lo the lo
   * @param hi the hi
   * @return the double
   */
   public static double varp(double[] a, int lo, int hi) {
       int length = hi - lo + 1;
       if (lo < 0 || hi >= a.length || lo > hi)
           throw new RuntimeException("Subarray indices out of bounds");
       if (length == 0) return Double.NaN;
       double avg = mean(a, lo, hi);
       double sum = 0.0;
       for (int i = lo; i <= hi; i++) {
           sum += (a[i] - avg) * (a[i] - avg);
       }
       return sum / length;
   }


  /**
   * Return sample standard deviation of array, NaN if no such value.
   *
   * @param a the a
   * @return the double
   */
   public static double stddev(double[] a) {
       return Math.sqrt(var(a));
   }

  /**
   * Return sample standard deviation of subarray a[lo..hi], NaN if no such value.
   *
   * @param a the a
   * @param lo the lo
   * @param hi the hi
   * @return the double
   */
   public static double stddev(double[] a, int lo, int hi) {
       return Math.sqrt(var(a, lo, hi));
   }

  /**
   * Return sample standard deviation of array, NaN if no such value.
   *
   * @param a the a
   * @return the double
   */
   public static double stddev(int[] a) {
       return Math.sqrt(var(a));
   }

  /**
   * Return population standard deviation of array, NaN if no such value.
   *
   * @param a the a
   * @return the double
   */
   public static double stddevp(double[] a) {
       return Math.sqrt(varp(a));
   }

  /**
   * Return population standard deviation of subarray a[lo..hi], NaN if no such value.
   *
   * @param a the a
   * @param lo the lo
   * @param hi the hi
   * @return the double
   */
   public static double stddevp(double[] a, int lo, int hi) {
       return Math.sqrt(varp(a, lo, hi));
   }

  /**
   * Return sum of all values in array.
   *
   * @param a the a
   * @return the double
   */
   public static double sum(double[] a) {
       double sum = 0.0;
       for (int i = 0; i < a.length; i++) {
           sum += a[i];
       }
       return sum;
   }

  /**
   * Return sum of all values in subarray a[lo..hi].
   *
   * @param a the a
   * @param lo the lo
   * @param hi the hi
   * @return the double
   */
   public static double sum(double[] a, int lo, int hi) {
       if (lo < 0 || hi >= a.length || lo > hi)
           throw new RuntimeException("Subarray indices out of bounds");
       double sum = 0.0;
       for (int i = lo; i <= hi; i++) {
           sum += a[i];
       }
       return sum;
   }

  /**
   * Return sum of all values in array.
   *
   * @param a the a
   * @return the int
   */
   public static int sum(int[] a) {
       int sum = 0;
       for (int i = 0; i < a.length; i++) {
           sum += a[i];
       }
       return sum;
   }
}
