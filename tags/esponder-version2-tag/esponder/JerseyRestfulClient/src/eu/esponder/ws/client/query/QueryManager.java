package eu.esponder.ws.client.query;

import java.io.IOException;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MultivaluedMap;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.events.entity.OsgiEventsEntityDTO;
import eu.esponder.ws.client.urlmanager.UrlManager;

public class QueryManager {
	ObjectMapper mapper = new ObjectMapper();
	UrlManager URL = new UrlManager();

	public ActorDTO getActorID(String userID, String actorID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ActorDTO actorDTO = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzGetActorByID());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		queryParams.add("actorID", actorID);

		// Call Webservice
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		actorDTO = mapper.readValue(szReturn, ActorDTO.class);

		return actorDTO;
	}

	public ResultListDTO getRegisterdOCAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzGetAllRegisteredOC());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getRegisterdConsumablesAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL
				.getSzRegisterConsumablesFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getRegisterdReusablesAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL
				.getSzRegisterReusablesFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getConsumablesAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzConsumablesFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getCrisisContextAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL
				.getSzCrisisContextFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getCrisisResourcePlanAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL
				.getSzCrisisResourcePlanFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getOperationCentersAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL
				.getSzOperationCenterFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getPersonnelAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzPersonnelFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getRusablesAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzReusablesFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}
	
	public ResultListDTO getAllSensorSnapshots(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzGetAllSensorSnapshosts());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}
	
	public ResultListDTO getAllTypes(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzGetAllEsponderTypes());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

    public EquipmentDTO getEquipment(String userID,Long eid)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		EquipmentDTO pResults = null;
		Client client = Client.create();

		WebResource webResource = client.resource(URL.getSzgetAllEquipments());
		// Initialize Parameters
				MultivaluedMap queryParams = new MultivaluedMapImpl();
				queryParams.add("equipmentID", eid.toString());
				queryParams.add("userID", userID);
				String szReturn = webResource.queryParams(queryParams)
						.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, EquipmentDTO.class);

		return pResults;
	}
    
    public ResultListDTO getEventsBySeverity(String userID,String Severity)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();

		WebResource webResource = client.resource(URL.getSzgetEventsBySeverity());
		// Initialize Parameters
				MultivaluedMap queryParams = new MultivaluedMapImpl();
				queryParams.add("osgiEventsEntitySeverity", Severity);
				queryParams.add("userID", userID);
				String szReturn = webResource.queryParams(queryParams)
						.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}
    
    public OsgiEventsEntityDTO getEventsByid(String userID,String eventid)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
    	OsgiEventsEntityDTO pResults = null;
		Client client = Client.create();

		WebResource webResource = client.resource(URL.getSzgetEventsByID());
		// Initialize Parameters
				MultivaluedMap queryParams = new MultivaluedMapImpl();
				queryParams.add("osgiEventsEntityDTOID", eventid);
				queryParams.add("userID", userID);
				String szReturn = webResource.queryParams(queryParams)
						.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, OsgiEventsEntityDTO.class);

		return pResults;
	}
    
   
    
}
