package eu.esponder.fru.thread;

public class Threado_Helper {

	public static void main(String args[]) {

		System.out.println("dw");
		
		@SuppressWarnings("unused")
		Runnable r1 = new Runnable() {
			public void run() {
				try {
					while (true) {
						System.out.println("Hello, world!");
						Thread.sleep(1000L);
					}
				} catch (InterruptedException iex) {
				}
			}
		};
		
		@SuppressWarnings("unused")
		Runnable r2 = new Runnable() {
			public void run() {
				try {
					while (true) {
						System.out.println("Goodbye, " + "cruel world!");
						Thread.sleep(2000L);
					}
				} catch (InterruptedException iex) {
				}
			}
		};

		/*
		 * Thread thr1 = new Thread(r1); Thread thr2 = new Thread(r2);
		 * thr1.start(); thr2.start();
		 * 
		 * 
		 * Thread thr1 = new ExtraThread(2); thr1.start(); Thread thr4 = new
		 * ExtraThread(4); thr4.start();
		 */
		for (int i = 0; i < 2; i++) {
			Thread thr1 = new ExtraThread(i);
			thr1.start();
		}

	}

}
