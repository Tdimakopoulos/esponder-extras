package eu.esponder.fru;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import eu.esponder.jaxb.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.jaxb.model.crisis.resource.sensor.GasSensorDTO;
import eu.esponder.jaxb.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnum;

public class ConfigReader {

	public static Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Long>> readMeasurementStatisticsConfig(
			String fileName) {

		Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Long>> configMap = new HashMap<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Long>>();

		String[] temp = null;

		Class<? extends SensorDTO> sensorClass;
		MeasurementStatisticTypeEnum statisticType;
		Long samplingPeriod;
		Map<MeasurementStatisticTypeEnum, Long> tempStatisticConfigMap = null;

		try {
			// Open the file that is the first command line parameter
			FileInputStream fstream = new FileInputStream(
					FruConstants.FRU_CONFIGURATION_FILE);

			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				// Print the content on the console
				temp = strLine.split("=");
//				System.out.println(temp[0]);
				String sensorName = temp[0];

				if (sensorName.startsWith("#"))
					continue;

				String[] temp2;
				temp2 = temp[1].split("_");
				String operation = temp2[0];
				String samplingPeriodStr = temp2[1];

				if (sensorName.equals("Sensor_Temperature")) {
					sensorClass = BodyTemperatureSensorDTO.class;
				} else if (sensorName.equals("Sensor_Carbon")) {
					sensorClass = GasSensorDTO.class;
				} else {
					/**
					 * FIXME: Bug in case of not identified Sensor from config
					 */
					sensorClass = null;
				}

				if (operation.equals("AVG")) {
					statisticType = MeasurementStatisticTypeEnum.MEAN;
				}
				if (operation.equals("MAX")) {
					statisticType = MeasurementStatisticTypeEnum.MAXIMUM;
				}

				else {
					/**
					 * FIXME: Bug in case of not identified operator from config
					 */
					statisticType = MeasurementStatisticTypeEnum.MEAN;
				}

				// sensorMeasurementStatistic.setStatisticType(statisticType);
				// sensorMeasurementStatistic.setSamplingPeriod((Long.parseLong(samplingPeriodStr)));

				samplingPeriod = Long.parseLong(samplingPeriodStr);

				if (configMap.containsKey(sensorClass)) {
					tempStatisticConfigMap = configMap.get(sensorClass);
					tempStatisticConfigMap.put(statisticType, samplingPeriod);
				} else {
					tempStatisticConfigMap = new HashMap<MeasurementStatisticTypeEnum, Long>();
					configMap.put(sensorClass, tempStatisticConfigMap);
					tempStatisticConfigMap.put(statisticType, samplingPeriod);
				}
				// sensorMeasurementStatisticsList.add(sensorMeasurementStatistic);
			}

			// for (int i = 0; i < Sensor_Temp_Commands.size(); i++) {
			// String[] temp2;
			// temp2 = Sensor_Temp_Commands.get(i).split("_");
			// Thread temp_thread = new DataFusionProcessorThread(i, temp2[0],
			// Integer.parseInt(temp2[1]));
			// temp_thread.start();
			// }

			// Close the input stream
			in.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

		return configMap;
	}
}
