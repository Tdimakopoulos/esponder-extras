/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ConsumableResourceDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "quantity", "actionPart","configuration", "consumableResourceCategoryId"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ConsumableResourceDTO extends LogisticsResourceDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3549004897135122493L;

	/**
	 * Instantiates a new consumable resource dto.
	 */
	public ConsumableResourceDTO() { }
	
	/** The quantity. */
	private BigDecimal quantity;
	
	/** The consumable resource category. */
	private ConsumableResourceCategoryDTO consumableResourceCategory;
	
	/** The action part. */
	private ActionPartDTO actionPart;
	
	/** The container. */
	private ReusableResourceDTO container;

	/** The crisis context id. */
	private Long crisisContextId;
	
	/**
	 * Gets the crisis context id.
	 *
	 * @return the crisis context id
	 */
	public Long getCrisisContextId() {
		return crisisContextId;
	}

	/**
	 * Sets the crisis context id.
	 *
	 * @param crisisContextId the new crisis context id
	 */
	public void setCrisisContextId(Long crisisContextId) {
		this.crisisContextId = crisisContextId;
	}

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Gets the container.
	 *
	 * @return the container
	 */
	public ReusableResourceDTO getContainer() {
		return container;
	}

	/**
	 * Sets the container.
	 *
	 * @param container the new container
	 */
	public void setContainer(ReusableResourceDTO container) {
		this.container = container;
	}

	/**
	 * Gets the action part.
	 *
	 * @return the action part
	 */
	public ActionPartDTO getActionPart() {
		return actionPart;
	}

	/**
	 * Sets the action part.
	 *
	 * @param actionPart the new action part
	 */
	public void setActionPart(ActionPartDTO actionPart) {
		this.actionPart = actionPart;
	}

	/**
	 * Gets the consumable resource category.
	 *
	 * @return the consumable resource category
	 */
	public ConsumableResourceCategoryDTO getConsumableResourceCategory() {
		return consumableResourceCategory;
	}

	/**
	 * Sets the consumable resource category.
	 *
	 * @param consumableResourceCategory the new consumable resource category
	 */
	public void setConsumableResourceCategory(ConsumableResourceCategoryDTO consumableResourceCategory) {
		this.consumableResourceCategory = consumableResourceCategory;
	}

}
