/*
 * 
 */
package eu.esponder.dto.model.crisis.action;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ActionObjectiveDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "period", "locationArea"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionObjectiveDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3290261377684343769L;

	/** The title. */
	private String title;
	
	/** The period. */
	private PeriodDTO period;
	
	/** The location area. */
	private LocationAreaDTO locationArea;

	/** The action. */
	private ActionDTO action;
	
	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the period.
	 *
	 * @return the period
	 */
	public PeriodDTO getPeriod() {
		return period;
	}

	/**
	 * Sets the period.
	 *
	 * @param period the new period
	 */
	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

	/**
	 * Gets the location area.
	 *
	 * @return the location area
	 */
	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	/**
	 * Sets the location area.
	 *
	 * @param locationArea the new location area
	 */
	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public ActionDTO getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(ActionDTO action) {
		this.action = action;
	}
	
}
