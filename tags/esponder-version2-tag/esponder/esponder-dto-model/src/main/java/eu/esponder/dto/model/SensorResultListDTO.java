/*
 * 
 */
package eu.esponder.dto.model;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

// TODO: Auto-generated Javadoc
/**
 * The Class SensorResultListDTO.
 * Custom ResultList used by the portal
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({ "resultList" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class SensorResultListDTO  implements Serializable  {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5545699205753830677L;
	
	/** The result list. */
	private List<SensorSnapshotDetailsList> resultList;

	/**
	 * Instantiates a new sensor result list dto.
	 */
	public SensorResultListDTO()
	{
		
	}
	
	/**
	 * Instantiates a new sensor result list dto.
	 *
	 * @param resultsList the results list
	 */
	public SensorResultListDTO(List<SensorSnapshotDetailsList> resultsList) {
		this.resultList = resultsList;
	}
	
	/**
	 * Gets the result list.
	 *
	 * @return the result list
	 */
	public List<SensorSnapshotDetailsList> getResultList() {
		return resultList;
	}

	/**
	 * Sets the result list.
	 *
	 * @param resultList the new result list
	 */
	public void setResultList(List<SensorSnapshotDetailsList> resultList) {
		this.resultList = resultList;
	}
	
	
}
