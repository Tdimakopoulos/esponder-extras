/*
 * 
 */
package eu.esponder.dto.model.snapshot;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ReferencePOISnapshotDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "referenceFile", "averageSize","operationsCentre","referencePOI"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ReferencePOISnapshotDTO extends SpatialSnapshotDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1729481522641331340L;

	/** The id. */
	protected Long id;
	
	/** The reference file. */
	protected String referenceFile;
	
	/** The average size. */
	protected Float averageSize;
	
	/** The title. */
	protected String title;
	
	/** The operations centre. */
	protected OperationsCentreDTO operationsCentre;
	
	/** The reference poi. */
	protected ReferencePOIDTO referencePOI;
	
	/** The previous. */
	private ReferencePOISnapshotDTO previous;
	
	/** The next. */
	private ReferencePOISnapshotDTO next;
	
	/**
	 * Gets the reference file.
	 *
	 * @return the reference file
	 */
	public String getReferenceFile() {
		return referenceFile;
	}

	/**
	 * Sets the reference file.
	 *
	 * @param referenceFile the new reference file
	 */
	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

	/**
	 * Gets the average size.
	 *
	 * @return the average size
	 */
	public Float getAverageSize() {
		return averageSize;
	}

	/**
	 * Sets the average size.
	 *
	 * @param averageSize the new average size
	 */
	public void setAverageSize(Float averageSize) {
		this.averageSize = averageSize;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#setId(java.lang.Long)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OperationsCentreDTO getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OperationsCentreDTO operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	/**
	 * Gets the reference poi.
	 *
	 * @return the reference poi
	 */
	public ReferencePOIDTO getReferencePOI() {
		return referencePOI;
	}

	/**
	 * Sets the reference poi.
	 *
	 * @param referencePOI the new reference poi
	 */
	public void setReferencePOI(ReferencePOIDTO referencePOI) {
		this.referencePOI = referencePOI;
	}

	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public ReferencePOISnapshotDTO getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previous the new previous
	 */
	public void setPrevious(ReferencePOISnapshotDTO previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public ReferencePOISnapshotDTO getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(ReferencePOISnapshotDTO next) {
		this.next = next;
	}
}



