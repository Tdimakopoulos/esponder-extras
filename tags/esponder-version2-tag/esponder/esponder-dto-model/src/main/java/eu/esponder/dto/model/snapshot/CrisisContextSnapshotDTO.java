/*
 * 
 */
package eu.esponder.dto.model.snapshot;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.snapshot.status.CrisisContextSnapshotStatusDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisContextSnapshotDTO.
 */
@JsonPropertyOrder({"id", "status","crisisContext", "previous", "next",  "locationArea", "period"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CrisisContextSnapshotDTO extends SpatialSnapshotDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2780925199500331932L;

	/** The id. */
	private Long id;
	
	/** The crisis context. */
	private CrisisContextDTO crisisContext;
	
	/** The status. */
	private CrisisContextSnapshotStatusDTO status;
	
	/** The previous. */
	private CrisisContextSnapshotDTO previous;
	
	/** The next. */
	private CrisisContextSnapshotDTO next;
	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#setId(java.lang.Long)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the crisis context.
	 *
	 * @return the crisis context
	 */
	public CrisisContextDTO getCrisisContext() {
		return crisisContext;
	}

	/**
	 * Sets the crisis context.
	 *
	 * @param crisisContext the new crisis context
	 */
	public void setCrisisContext(CrisisContextDTO crisisContext) {
		this.crisisContext = crisisContext;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public CrisisContextSnapshotStatusDTO getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(CrisisContextSnapshotStatusDTO status) {
		this.status = status;
	}

	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public CrisisContextSnapshotDTO getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previous the new previous
	 */
	public void setPrevious(CrisisContextSnapshotDTO previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public CrisisContextSnapshotDTO getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(CrisisContextSnapshotDTO next) {
		this.next = next;
	}

}
