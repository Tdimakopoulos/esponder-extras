/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;



import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class FRTeamDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"title", "frcheif", "meoc","snapshots"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class FRTeamDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6418102891138209607L;

	/** The title. */
	protected String title;

	/** The frcheif. */
	private ActorDTO frcheif;

	/** The meoc. */
	private OperationsCentreDTO meoc;

	
	private Set<FRTeamSnapshotDTO> snapshots;
	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the frcheif.
	 *
	 * @return the frcheif
	 */
	public ActorDTO getFrcheif() {
		return frcheif;
	}

	/**
	 * Sets the frcheif.
	 *
	 * @param frcheif the new frcheif
	 */
	public void setFrcheif(ActorDTO frcheif) {
		this.frcheif = frcheif;
	}

	/**
	 * Gets the meoc.
	 *
	 * @return the meoc
	 */
	public OperationsCentreDTO getMeoc() {
		return meoc;
	}

	/**
	 * Sets the meoc.
	 *
	 * @param meoc the new meoc
	 */
	public void setMeoc(OperationsCentreDTO meoc) {
		this.meoc = meoc;
	}

	public Set<FRTeamSnapshotDTO> getFRTeamSnapshots() {
		return snapshots;
	}

	public void setFRTeamSnapshots(Set<FRTeamSnapshotDTO> snapshots) {
		this.snapshots = snapshots;
	}

}
