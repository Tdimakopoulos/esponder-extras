/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.category;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.type.RankTypeDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelCategoryDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "rank", "personnelCompetences","organisationCategory" })
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PersonnelCategoryDTO extends PlannableResourceCategoryDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9187806862545991852L;

	/**
	 * Models the discipline-specific (RankType) rank, i.e. General, Colonel etc. 
	 */
	private RankTypeDTO rank;

	//	private PersonnelDTO personnel;

	/** The personnel competences. */
	private Set<PersonnelCompetenceDTO> personnelCompetences;

	/** The organisation category. */
	private OrganisationCategoryDTO organisationCategory;

	/**
	 * Gets the rank.
	 *
	 * @return the rank
	 */
	public RankTypeDTO getRank() {
		return rank;
	}

	/**
	 * Sets the rank.
	 *
	 * @param rank the new rank
	 */
	public void setRank(RankTypeDTO rank) {
		this.rank = rank;
	}

	//	public PersonnelDTO getPersonnel() {
	//		return personnel;
	//	}
	//
	//	public void setPersonnel(PersonnelDTO personnel) {
	//		this.personnel = personnel;
	//	}

	/**
	 * Gets the personnel competences.
	 *
	 * @return the personnel competences
	 */
	public Set<PersonnelCompetenceDTO> getPersonnelCompetences() {
		return personnelCompetences;
	}

	/**
	 * Sets the personnel competences.
	 *
	 * @param personnelCompetences the new personnel competences
	 */
	public void setPersonnelCompetences(
			Set<PersonnelCompetenceDTO> personnelCompetences) {
		this.personnelCompetences = personnelCompetences;
	}

	/**
	 * Gets the organisation category.
	 *
	 * @return the organisation category
	 */
	public OrganisationCategoryDTO getOrganisationCategory() {
		return organisationCategory;
	}

	/**
	 * Sets the organisation category.
	 *
	 * @param organisationCategory the new organisation category
	 */
	public void setOrganisationCategory(OrganisationCategoryDTO organisationCategory) {
		this.organisationCategory = organisationCategory;
	}

}
