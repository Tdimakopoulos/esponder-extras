/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.math.BigDecimal;
import java.util.Set;



// TODO: Auto-generated Javadoc
/**
 * The Class RegisteredReusableResourceDTO.
 */
public class RegisteredReusableResourceDTO extends ResourceDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1249171957154114291L;


	/** The quantity. */
	private BigDecimal quantity;
	

	/** The registered consumables. */
	private Set<RegisteredConsumableResourceDTO> registeredConsumables;
	
	/** The parent. */
	private RegisteredReusableResourceDTO parent;
	

	/** The children. */
	private Set<RegisteredReusableResourceDTO> children;
	
	/**
	 * TODO: Fix this to point to OperationsCentreCategory. For now we just keep the correct OperationsCentreCategoryId
	 */
//	@ManyToOne
//	@JoinColumn(name="CATEGORY_ID")
//	private ReusableResourceCategory reusableResourceCategory;
	private Long reusableResourceCategoryId;

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Gets the registered consumables.
	 *
	 * @return the registered consumables
	 */
	public Set<RegisteredConsumableResourceDTO> getRegisteredConsumables() {
		return registeredConsumables;
	}

	/**
	 * Sets the registered consumables.
	 *
	 * @param registeredConsumables the new registered consumables
	 */
	public void setRegisteredConsumables(
			Set<RegisteredConsumableResourceDTO> registeredConsumables) {
		this.registeredConsumables = registeredConsumables;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public RegisteredReusableResourceDTO getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(RegisteredReusableResourceDTO parent) {
		this.parent = parent;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public Set<RegisteredReusableResourceDTO> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the new children
	 */
	public void setChildren(Set<RegisteredReusableResourceDTO> children) {
		this.children = children;
	}

	/**
	 * Gets the reusable resource category id.
	 *
	 * @return the reusable resource category id
	 */
	public Long getReusableResourceCategoryId() {
		return reusableResourceCategoryId;
	}

	/**
	 * Sets the reusable resource category id.
	 *
	 * @param reusableResourceCategoryId the new reusable resource category id
	 */
	public void setReusableResourceCategoryId(Long reusableResourceCategoryId) {
		this.reusableResourceCategoryId = reusableResourceCategoryId;
	}

	
}
