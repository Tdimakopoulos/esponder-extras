/*
 * 
 */
package eu.esponder.dto.model.crisis;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisContextDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "crisisLocation", "operationsCentres", "additionalInfo","startDate", "endDate", "crisiContextAlert"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CrisisContextDTO extends ESponderEntityDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7067439377510286686L;

	/** The title. */
	private String title;
	
	/** The crisis location. */
	private SphereDTO crisisLocation;
	
	/** The operations centres. */
	private Set<OperationsCentreDTO> operationsCentres; 

    /** The additional info. */
    private String additionalInfo;
	
	/** The start date. */
	private Long startDate;
	
	/** The end date. */
	private Long endDate;
	
	/** The crisis types. */
	private Set<CrisisTypeDTO> crisisTypes;
	
	/** The crisis context alert. */
	private CrisisContextAlertEnumDTO crisisContextAlert;
	
	/**
	 * Gets the additional info.
	 *
	 * @return the additional info
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	/**
	 * Sets the additional info.
	 *
	 * @param additionalInfo the new additional info
	 */
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Long getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Long getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	
	
	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Gets the operations centres.
	 *
	 * @return the operations centres
	 */
	public Set<OperationsCentreDTO> getOperationsCentres() {
		return operationsCentres;
	}

	/**
	 * Sets the operations centres.
	 *
	 * @param operationsCentres the new operations centres
	 */
	public void setOperationsCentres(Set<OperationsCentreDTO> operationsCentres) {
		this.operationsCentres = operationsCentres;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "CrisisContextDTO [id=" + id + ", title=" + title + "]";
	}

	/**
	 * Gets the crisis location.
	 *
	 * @return the crisis location
	 */
	public SphereDTO getCrisisLocation() {
		return crisisLocation;
	}

	/**
	 * Sets the crisis location.
	 *
	 * @param crisisLocation the new crisis location
	 */
	public void setCrisisLocation(SphereDTO crisisLocation) {
		this.crisisLocation = crisisLocation;
	}

	/**
	 * Gets the crisis context alert.
	 *
	 * @return the crisis context alert
	 */
	public CrisisContextAlertEnumDTO getCrisisContextAlert() {
		return crisisContextAlert;
	}

	/**
	 * Sets the crisis context alert.
	 *
	 * @param crisisContextAlert the new crisis context alert
	 */
	public void setCrisisContextAlert(CrisisContextAlertEnumDTO crisisContextAlert) {
		this.crisisContextAlert = crisisContextAlert;
	}

	/**
	 * Gets the crisis types.
	 *
	 * @return the crisis types
	 */
	public Set<CrisisTypeDTO> getCrisisTypes() {
		return crisisTypes;
	}

	/**
	 * Sets the crisis types.
	 *
	 * @param crisisTypes the new crisis types
	 */
	public void setCrisisTypes(Set<CrisisTypeDTO> crisisTypes) {
		this.crisisTypes = crisisTypes;
	}
	
}
