/*
 * 
 */
package eu.esponder.dto.model.snapshot.sensor.measurement.statistic;

import java.util.List;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class SensorMeasurementStatisticEnvelopeDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "measurementStatistics", "fRId"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SensorMeasurementStatisticEnvelopeDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4109736575559482473L;

	/** The measurement statistics. */
	private List<SensorMeasurementStatisticDTO> measurementStatistics;
	
	/** The r id. */
	private String fRId;

	/**
	 * Gets the f r id.
	 *
	 * @return the f r id
	 */
	public String getfRId() {
		return fRId;
	}

	/**
	 * Sets the f r id.
	 *
	 * @param fRId the new f r id
	 */
	public void setfRId(String fRId) {
		this.fRId = fRId;
	}

	/**
	 * Gets the measurement statistics.
	 *
	 * @return the measurement statistics
	 */
	public List<SensorMeasurementStatisticDTO> getMeasurementStatistics() {
		return measurementStatistics;
	}

	/**
	 * Sets the measurement statistics.
	 *
	 * @param measurementStatistics the new measurement statistics
	 */
	public void setMeasurementStatistics(
			List<SensorMeasurementStatisticDTO> measurementStatistics) {
		this.measurementStatistics = measurementStatistics;
	}
	
}
