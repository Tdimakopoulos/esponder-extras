/*
 * 
 */
package eu.esponder.dto.model.snapshot.sensor.measurement;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class SensorMeasurementDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class SensorMeasurementDTO extends ESponderEntityDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6461648755883151217L;

	/** The sensor. */
	private SensorDTO sensor;
	
	/** The timestamp. */
	private Long timestamp;

	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public Long getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the timestamp.
	 *
	 * @param timestamp the new timestamp
	 */
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Gets the sensor.
	 *
	 * @return the sensor
	 */
	public SensorDTO getSensor() {
		return sensor;
	}

	/**
	 * Sets the sensor.
	 *
	 * @param sensor the new sensor
	 */
	public void setSensor(SensorDTO sensor) {
		this.sensor = sensor;
	}


}
