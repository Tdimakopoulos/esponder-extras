/*
 * 
 */
package eu.esponder.dto.model.snapshot.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class SensorSnapshotDTO.
 */
@JsonPropertyOrder({"status", "value", "statisticType", "period", "sensor"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SensorSnapshotDTO extends SnapshotDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6909950557250928422L;

	/**
	 * Instantiates a new sensor snapshot dto.
	 */
	public SensorSnapshotDTO() { }
	
	/** The status. */
	private SensorSnapshotStatusDTO status;
	
	/** The statistic type. */
	private MeasurementStatisticTypeEnumDTO statisticType;
	
	/** The value. */
	private String value;
	
	/** The sensor. */
	private SensorDTO sensor;
	
	/**
	 * Gets the value as double.
	 *
	 * @return the double
	 */
	public double GetValueAsDouble()
	{
	  double lReturn = 0;
	  try {
		  lReturn = Double.parseDouble(this.value.trim());
	         System.out.println("Double lReturn = " + lReturn);
	      } catch (NumberFormatException nfe) {
	         System.out.println("NumberFormatException: " + nfe.getMessage());
	      }
	  
	  return lReturn;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public SensorSnapshotStatusDTO getStatus() {
		return status;
	}

	/**
	 * Gets the sensor.
	 *
	 * @return the sensor
	 */
	public SensorDTO getSensor() {
		return sensor;
	}

	/**
	 * Sets the sensor.
	 *
	 * @param sensor the new sensor
	 */
	public void setSensor(SensorDTO sensor) {
		this.sensor = sensor;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(SensorSnapshotStatusDTO status) {
		this.status = status;
	}

	/**
	 * Gets the statistic type.
	 *
	 * @return the statistic type
	 */
	public MeasurementStatisticTypeEnumDTO getStatisticType() {
		return statisticType;
	}

	/**
	 * Sets the statistic type.
	 *
	 * @param statisticType the new statistic type
	 */
	public void setStatisticType(MeasurementStatisticTypeEnumDTO statisticType) {
		this.statisticType = statisticType;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SensorSnapshotDTO [status=" + status 
				+ ", statisticType=" + statisticType + ", value=" + value 
				+ ", period=" + period + ", id=" + id + "]";
	}
	
}
