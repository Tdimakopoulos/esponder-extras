/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.crisis.resource.plan.PlannableResourceDTO;
import eu.esponder.dto.model.type.RankTypeDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "type", "title", "status", "firstName", "lastName", "rank", "availability", "organisation"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PersonnelDTO extends PlannableResourceDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5687655161402727780L;

	/**
	 * Instantiates a new personnel dto.
	 */
	public PersonnelDTO() { }
	
	/** The id. */
	private Long id;
	
	/** The first name. */
	private String firstName;
	
	/** The last name. */
	private String lastName;
	
	/** The rank. */
	private RankTypeDTO rank;
	
	/** The availability. */
	private boolean availability;
	
	/** The organisation. */
	private OrganisationDTO organisation;
	
	/** The personnel category. */
	private PersonnelCategoryDTO personnelCategory;
	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#setId(java.lang.Long)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the rank.
	 *
	 * @return the rank
	 */
	public RankTypeDTO getRank() {
		return rank;
	}

	/**
	 * Sets the rank.
	 *
	 * @param rank the new rank
	 */
	public void setRank(RankTypeDTO rank) {
		this.rank = rank;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Checks if is availability.
	 *
	 * @return true, if is availability
	 */
	public boolean isAvailability() {
		return availability;
	}

	/**
	 * Sets the availability.
	 *
	 * @param availability the new availability
	 */
	public void setAvailability(boolean availability) {
		this.availability = availability;
	}

	/**
	 * Gets the organisation.
	 *
	 * @return the organisation
	 */
	public OrganisationDTO getOrganisation() {
		return organisation;
	}

	/**
	 * Sets the organisation.
	 *
	 * @param organisation the new organisation
	 */
	public void setOrganisation(OrganisationDTO organisation) {
		this.organisation = organisation;
	}

	/**
	 * Gets the personnel category.
	 *
	 * @return the personnel category
	 */
	public PersonnelCategoryDTO getPersonnelCategory() {
		return personnelCategory;
	}

	/**
	 * Sets the personnel category.
	 *
	 * @param personnelCategory the new personnel category
	 */
	public void setPersonnelCategory(PersonnelCategoryDTO personnelCategory) {
		this.personnelCategory = personnelCategory;
	}

}
