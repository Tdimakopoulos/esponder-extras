/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.plan;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisResourcePlanDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "plannableResources", "crisisType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CrisisResourcePlanDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2277820597919890333L;

	/** The plannable resources. */
	private Set<PlannableResourcePowerDTO> plannableResources;
	
	/**
	 * This field indicates either the CrisisDisasterType or the CrisisFeatureType for the plan.
	 * It is for further study whether the combination of them (i.e DisasterType and FeatureType) 
	 * should mandate that an additive exclusive result of the plannableResourcePower should be applied.
	 * 
	 */
	private CrisisTypeDTO crisisType;
	
	/** The title. */
	private String title;

	/**
	 * Gets the plannable resources.
	 *
	 * @return the plannable resources
	 */
	public Set<PlannableResourcePowerDTO> getPlannableResources() {
		return plannableResources;
	}

	/**
	 * Sets the plannable resources.
	 *
	 * @param plannableResources the new plannable resources
	 */
	public void setPlannableResources(
			Set<PlannableResourcePowerDTO> plannableResources) {
		this.plannableResources = plannableResources;
	}

	/**
	 * Gets the crisis type.
	 *
	 * @return the crisis type
	 */
	public CrisisTypeDTO getCrisisType() {
		return crisisType;
	}

	/**
	 * Sets the crisis type.
	 *
	 * @param crisisType the new crisis type
	 */
	public void setCrisisType(CrisisTypeDTO crisisType) {
		this.crisisType = crisisType;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
}
