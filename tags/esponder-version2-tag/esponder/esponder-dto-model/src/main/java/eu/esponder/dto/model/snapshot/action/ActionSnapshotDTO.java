/*
 * 
 */
package eu.esponder.dto.model.snapshot.action;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActionSnapshotStatusDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionSnapshotDTO.
 */
@JsonPropertyOrder({"id", "status","action", "locationArea", "period"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionSnapshotDTO extends SpatialSnapshotDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6768351283306416107L;

	/**
	 * Instantiates a new action snapshot dto.
	 */
	public ActionSnapshotDTO() { }
	
	/** The action. */
	private ActionDTO action;
	
	/** The status. */
	private ActionSnapshotStatusDTO status;
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ActionSnapshotStatusDTO getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ActionSnapshotStatusDTO status) {
		this.status = status;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public ActionDTO getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(ActionDTO action) {
		this.action = action;
	}

}
