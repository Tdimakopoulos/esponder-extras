/*
 * 
 */
package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class SensorTypeDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "measurementUnit", "parent", "children"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SensorTypeDTO extends ESponderTypeDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1374104680044702555L;

	/**
	 * Instantiates a new sensor type dto.
	 */
	public SensorTypeDTO() { }
	
	/** The measurement unit. */
	private MeasurementUnitEnumDTO measurementUnit;
	
	/**
	 * Gets the measurement unit.
	 *
	 * @return the measurement unit
	 */
	public MeasurementUnitEnumDTO getMeasurementUnit() {
		return measurementUnit;
	}

	/**
	 * Sets the measurement unit.
	 *
	 * @param measurementUnit the new measurement unit
	 */
	public void setMeasurementUnit(MeasurementUnitEnumDTO measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

}
