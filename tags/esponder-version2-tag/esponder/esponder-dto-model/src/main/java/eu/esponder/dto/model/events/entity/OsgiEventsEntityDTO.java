/*
 * 
 */
package eu.esponder.dto.model.events.entity;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import eu.esponder.dto.model.ESponderEntityDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class OsgiEventsEntityDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "journalMsg", "journalMsgInfo", "timestamp", "severity", "attachment", "source", "sourceid"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OsgiEventsEntityDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6940684176472034542L;

	/** The id. */
	private Long id;

	/** The journal msg. */
	private String journalMsg;

	/** The journal msg info. */
	private String journalMsgInfo;

	/** The time stamp. */
	private Long timeStamp;

	/** The severity. */
	private String severity;

	/** The attachment. */
	private String attachment;

	/** The source. */
	private String source;

	/** The sourceid. */
	private Long sourceid;

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.ESponderEntityDTO#setId(java.lang.Long)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the journal msg.
	 *
	 * @return the journal msg
	 */
	public String getJournalMsg() {
		return journalMsg;
	}

	/**
	 * Sets the journal msg.
	 *
	 * @param journalMsg the new journal msg
	 */
	public void setJournalMsg(String journalMsg) {
		this.journalMsg = journalMsg;
	}

	/**
	 * Gets the journal msg info.
	 *
	 * @return the journal msg info
	 */
	public String getJournalMsgInfo() {
		return journalMsgInfo;
	}

	/**
	 * Sets the journal msg info.
	 *
	 * @param journalMsgInfo the new journal msg info
	 */
	public void setJournalMsgInfo(String journalMsgInfo) {
		this.journalMsgInfo = journalMsgInfo;
	}

	/**
	 * Gets the time stamp.
	 *
	 * @return the time stamp
	 */
	public Long getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Sets the time stamp.
	 *
	 * @param timeStamp the new time stamp
	 */
	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * Gets the severity.
	 *
	 * @return the severity
	 */
	public String getSeverity() {
		return severity;
	}

	/**
	 * Sets the severity.
	 *
	 * @param severity the new severity
	 */
	public void setSeverity(String severity) {
		this.severity = severity;
	}

	/**
	 * Gets the attachment.
	 *
	 * @return the attachment
	 */
	public String getAttachment() {
		return attachment;
	}

	/**
	 * Sets the attachment.
	 *
	 * @param attachment the new attachment
	 */
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	/**
	 * Gets the source.
	 *
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Sets the source.
	 *
	 * @param source the new source
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Gets the sourceid.
	 *
	 * @return the sourceid
	 */
	public Long getSourceid() {
		return sourceid;
	}

	/**
	 * Sets the sourceid.
	 *
	 * @param sourceid the new sourceid
	 */
	public void setSourceid(Long sourceid) {
		this.sourceid = sourceid;
	}

}
