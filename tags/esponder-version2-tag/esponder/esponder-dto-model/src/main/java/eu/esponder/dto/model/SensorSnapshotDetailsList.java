/*
 * 
 */
package eu.esponder.dto.model;
import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class SensorSnapshotDetailsList.
 * Custom resultList return type used by the portal
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({ "pSensorSnapshot,pActor,pEquipment" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class SensorSnapshotDetailsList implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9013625599098496480L;
	
	/** The p sensor snapshot. */
	SensorSnapshotDTO pSensorSnapshot= new SensorSnapshotDTO();
	
	/** The p actor. */
	ActorDTO pActor = new ActorDTO();
	
	/** The p equipment. */
	EquipmentDTO pEquipment = new EquipmentDTO();
    
	/**
	 * Instantiates a new sensor snapshot details list.
	 */
	public SensorSnapshotDetailsList()
	{
		
	}
	
	/**
	 * Instantiates a new sensor snapshot details list.
	 *
	 * @param pSensorSnapshots the sensor snapshots
	 * @param pActors the actors
	 * @param pEquipments the equipments
	 */
	public SensorSnapshotDetailsList(SensorSnapshotDTO pSensorSnapshots,ActorDTO pActors,EquipmentDTO pEquipments)
	{
		pSensorSnapshot=pSensorSnapshots;
		pActor=pActors;
		pEquipment=pEquipments;
	}
	
	/**
	 * Gets the p sensor snapshot.
	 *
	 * @return the p sensor snapshot
	 */
	public SensorSnapshotDTO getpSensorSnapshot() {
		return pSensorSnapshot;
	}
	
	/**
	 * Sets the p sensor snapshot.
	 *
	 * @param pSensorSnapshot the new p sensor snapshot
	 */
	public void setpSensorSnapshot(SensorSnapshotDTO pSensorSnapshot) {
		this.pSensorSnapshot = pSensorSnapshot;
	}
	
	/**
	 * Gets the p actor.
	 *
	 * @return the p actor
	 */
	public ActorDTO getpActor() {
		return pActor;
	}
	
	/**
	 * Sets the p actor.
	 *
	 * @param pActor the new p actor
	 */
	public void setpActor(ActorDTO pActor) {
		this.pActor = pActor;
	}
	
	/**
	 * Gets the p equipment.
	 *
	 * @return the p equipment
	 */
	public EquipmentDTO getpEquipment() {
		return pEquipment;
	}
	
	/**
	 * Sets the p equipment.
	 *
	 * @param pEquipment the new p equipment
	 */
	public void setpEquipment(EquipmentDTO pEquipment) {
		this.pEquipment = pEquipment;
	}
}
