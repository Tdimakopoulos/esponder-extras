/*
 * 
 */
package eu.esponder.dto.model.crisis.action;



// TODO: Auto-generated Javadoc
/**
 * The Enum SeverityLevelDTO.
 */
public enum SeverityLevelDTO {
	/*MINIMAL,
	MEDIUM,
	SERIOUS,
	SEVERE,
	FATAL,
	UNDEFINED*/
	
	/** The minimal. */
	MINIMAL((byte) 0),
	
	/** The medium. */
	MEDIUM((byte) 1),
	
	/** The serious. */
	SERIOUS((byte) 2),
	
	/** The severe. */
	SEVERE((byte) 3),
	
	/** The fatal. */
	FATAL((byte) 4),
	
	/** The undefined. */
	UNDEFINED((byte) -1);
	
	/**
	 * Gets the severity color.
	 *
	 * @param value the value
	 * @return the severity color
	 */
	public static int getSeverityColor(byte value) {
		switch (value) {
		case (byte)-1:
			return 0xFFBBD1F0; 
		case (byte)0:
			return 0xFFBBF0CD;
		case (byte)1:
			return 0xFFEBF765;
		case (byte)2:
			return 0xFFF0BBBC; 
		case (byte)3:
			return 0xFFF2686B; 
		case (byte)4:
			return 0xFFF0373B; 
		}
		return 0;
	}
	
	/** The value. */
	private byte value;

	/**
	 * Instantiates a new severity level dto.
	 *
	 * @param value the value
	 */
	private SeverityLevelDTO(byte value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public byte getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(byte value) {
		this.value = value;
	}
}
