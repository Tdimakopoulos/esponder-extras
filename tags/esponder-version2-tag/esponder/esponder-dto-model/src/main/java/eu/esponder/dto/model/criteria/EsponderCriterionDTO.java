/*
 * 
 */
package eu.esponder.dto.model.criteria;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class EsponderCriterionDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"field", "expression", "value"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EsponderCriterionDTO extends EsponderQueryRestrictionDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4927714081578656589L;
	
	/**
	 * Instantiates a new esponder criterion dto.
	 */
	public EsponderCriterionDTO() {}
	
	/**
	 * Instantiates a new esponder criterion dto.
	 *
	 * @param fieldName the field name
	 * @param expression the expression
	 * @param fieldValue the field value
	 */
	public EsponderCriterionDTO(String fieldName, EsponderCriterionExpressionEnumDTO expression, String fieldValue) {
		this.field = fieldName;
		this.expression = expression;
		this.value = fieldValue;
	}

	/** The field. */
	private String field;
	
	/** The expression. */
	private EsponderCriterionExpressionEnumDTO expression;
	
	/** The value. */
	private String value;

	/**
	 * Gets the field.
	 *
	 * @return the field
	 */
	public String getField() {
		return field;
	}

	/**
	 * Sets the field.
	 *
	 * @param fieldName the new field
	 */
	public void setField(String fieldName) {
		this.field = fieldName;
	}

	/**
	 * Gets the expression.
	 *
	 * @return the expression
	 */
	public EsponderCriterionExpressionEnumDTO getExpression() {
		return expression;
	}

	/**
	 * Sets the expression.
	 *
	 * @param expression the new expression
	 */
	public void setExpression(EsponderCriterionExpressionEnumDTO expression) {
		this.expression = expression;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param fieldValue the new value
	 */
	public void setValue(String fieldValue) {
		this.value = fieldValue;
	}

}
