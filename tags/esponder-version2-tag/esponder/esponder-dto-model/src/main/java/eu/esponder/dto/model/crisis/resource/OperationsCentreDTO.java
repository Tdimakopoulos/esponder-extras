/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.plan.PlannableResourceDTO;
import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "users","operationsCentreCategoryId", "title", "status", "actors","supervisor", "subordinates", "snapshot", "voIPURL"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OperationsCentreDTO extends PlannableResourceDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1138167138418691550L;

	/**
	 * Instantiates a new operations centre dto.
	 */
	public OperationsCentreDTO() { }
	
	/** The actors. */
	private Set<ActorDTO> actors;
	
	/** The supervisor. */
	private OperationsCentreDTO supervisor;
	
	/** The subordinates. */
	private Set<OperationsCentreDTO> subordinates;
	
	/** The snapshot. */
	private OperationsCentreSnapshotDTO snapshot;
	
	/** The operations centre category id. */
	private Long operationsCentreCategoryId;
	
	/** The vo ipurl. */
	private VoIPURLDTO voIPURL;
	
	/** The crisis context id. */
	private Long crisisContextId;

	/**
	 * Gets the crisis context id.
	 *
	 * @return the crisis context id
	 */
	public Long getCrisisContextId() {
		return crisisContextId;
	}

	/**
	 * Sets the crisis context id.
	 *
	 * @param crisisContextId the new crisis context id
	 */
	public void setCrisisContextId(Long crisisContextId) {
		this.crisisContextId = crisisContextId;
	}

	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}

	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}
	
	/**
	 * Gets the actors.
	 *
	 * @return the actors
	 */
	public Set<ActorDTO> getActors() {
		return actors;
	}
	
	/**
	 * Sets the actors.
	 *
	 * @param actors the new actors
	 */
	public void setActors(Set<ActorDTO> actors) {
		this.actors = actors;
	}
	
	/**
	 * Gets the subordinates.
	 *
	 * @return the subordinates
	 */
	public Set<OperationsCentreDTO> getSubordinates() {
		return subordinates;
	}

	/**
	 * Sets the subordinates.
	 *
	 * @param subordinates the new subordinates
	 */
	public void setSubordinates(Set<OperationsCentreDTO> subordinates) {
		this.subordinates = subordinates;
	}

	/**
	 * Gets the snapshot.
	 *
	 * @return the snapshot
	 */
	public OperationsCentreSnapshotDTO getSnapshot() {
		return snapshot;
	}

	/**
	 * Sets the snapshot.
	 *
	 * @param snapshot the new snapshot
	 */
	public void setSnapshot(OperationsCentreSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	/**
	 * Gets the operations centre category id.
	 *
	 * @return the operations centre category id
	 */
	public Long getOperationsCentreCategoryId() {
		return operationsCentreCategoryId;
	}

	/**
	 * Sets the operations centre category id.
	 *
	 * @param operationsCentreCategoryId the new operations centre category id
	 */
	public void setOperationsCentreCategoryId(Long operationsCentreCategoryId) {
		this.operationsCentreCategoryId = operationsCentreCategoryId;
	}

	/**
	 * Gets the supervisor.
	 *
	 * @return the supervisor
	 */
	public OperationsCentreDTO getSupervisor() {
		return supervisor;
	}

	/**
	 * Sets the supervisor.
	 *
	 * @param supervisor the new supervisor
	 */
	public void setSupervisor(OperationsCentreDTO supervisor) {
		this.supervisor = supervisor;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.ResourceDTO#toString()
	 */
	@Override
	public String toString() {
		return "OperationsCentreDTO [type=" + type + ", status=" + status
				+ ", id=" + id + ", title=" + title + "]";
	}
	
}
