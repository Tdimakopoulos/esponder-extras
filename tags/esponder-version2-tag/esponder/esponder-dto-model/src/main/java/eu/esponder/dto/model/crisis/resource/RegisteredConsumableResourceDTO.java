/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.math.BigDecimal;




// TODO: Auto-generated Javadoc
/**
 * The Class RegisteredConsumableResourceDTO.
 */
public class RegisteredConsumableResourceDTO extends ResourceDTO {
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1419531858035816761L;


	/** The quantity. */
	private BigDecimal quantity;
	
	
	/** The container. */
	private RegisteredReusableResourceDTO container;

	/** The consumable resource category id. */
	private Long consumableResourceCategoryId;
	
	
	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Gets the container.
	 *
	 * @return the container
	 */
	public RegisteredReusableResourceDTO getContainer() {
		return container;
	}

	/**
	 * Sets the container.
	 *
	 * @param container the new container
	 */
	public void setContainer(RegisteredReusableResourceDTO container) {
		this.container = container;
	}

	/**
	 * Gets the consumable resource category id.
	 *
	 * @return the consumable resource category id
	 */
	public Long getConsumableResourceCategoryId() {
		return consumableResourceCategoryId;
	}

	/**
	 * Sets the consumable resource category id.
	 *
	 * @param consumableResourceCategoryId the new consumable resource category id
	 */
	public void setConsumableResourceCategoryId(Long consumableResourceCategoryId) {
		this.consumableResourceCategoryId = consumableResourceCategoryId;
	}



	
}
