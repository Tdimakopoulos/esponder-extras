/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.category;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class OrganisationCategoryDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "disciplineType", "organisationType", "organisationId", "personnelCategories"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OrganisationCategoryDTO extends ResourceCategoryDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8982078577957843565L;

	/** The discipline type. */
	private DisciplineTypeDTO disciplineType;
	
	/**
	 * Models the hierarchy of internal organisation elements of the particular organisation,
	 * e.g. for FireFighters: Headquarters, Fire Stations, Garages etc., 
	 * thus advanced searching is enabled.
	 */
	private OrganisationTypeDTO organisationType;
	
	/** The organisation id. */
	private Long organisationId;

	/**
 * Gets the discipline type.
 *
 * @return the discipline type
 */
public DisciplineTypeDTO getDisciplineType() {
		return disciplineType;
	}

	/**
	 * Sets the discipline type.
	 *
	 * @param disciplineType the new discipline type
	 */
	public void setDisciplineType(DisciplineTypeDTO disciplineType) {
		this.disciplineType = disciplineType;
	}

	/**
	 * Gets the organisation type.
	 *
	 * @return the organisation type
	 */
	public OrganisationTypeDTO getOrganisationType() {
		return organisationType;
	}

	/**
	 * Sets the organisation type.
	 *
	 * @param organisationType the new organisation type
	 */
	public void setOrganisationType(OrganisationTypeDTO organisationType) {
		this.organisationType = organisationType;
	}

	/**
 * Gets the organisation id.
 *
 * @return the organisation id
 */
public Long getOrganisationId() {
		return organisationId;
	}

	/**
	 * Sets the organisation id.
	 *
	 * @param organisationId the new organisation id
	 */
	public void setOrganisationId(Long organisationId) {
		this.organisationId = organisationId;
	}

	

}
