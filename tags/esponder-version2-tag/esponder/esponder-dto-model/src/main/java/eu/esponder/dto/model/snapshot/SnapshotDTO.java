/*
 * 
 */
package eu.esponder.dto.model.snapshot;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class SnapshotDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class SnapshotDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3358588722380975179L;
	
	/** The period. */
	protected PeriodDTO period;

	/**
	 * Gets the period.
	 *
	 * @return the period
	 */
	public PeriodDTO getPeriod() {
		return period;
	}

	/**
	 * Sets the period.
	 *
	 * @param period the new period
	 */
	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

}
