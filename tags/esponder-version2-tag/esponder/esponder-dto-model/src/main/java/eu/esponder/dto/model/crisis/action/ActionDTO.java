/*
 * 
 */
package eu.esponder.dto.model.crisis.action;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "type", "title", "actionParts", "snapshot", "children", "actionObjectives", "severityLevel", "actionOperation"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionDTO extends ESponderEntityDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4499447146637844779L;

	/** The title. */
	private String title;

	/** The type. */
	private String type;
	
	/** The action parts. */
	private Set<ActionPartDTO> actionParts;
	
	/** The crisis context. */
	private CrisisContextDTO crisisContext;
	
	/** Latest ActionSnapshot of this Action instance. */
	private ActionSnapshotDTO snapshot;
	
	/** The children. */
	private Set<ActionDTO> children;
	
	/** The action objectives. */
	private Set<ActionObjectiveDTO> actionObjectives;
	
	/** The severity level. */
	private SeverityLevelDTO severityLevel;
	
	/** The action operation. */
	private ActionOperationEnumDTO actionOperation;

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the action parts.
	 *
	 * @return the action parts
	 */
	public Set<ActionPartDTO> getActionParts() {
		return actionParts;
	}

	/**
	 * Sets the action parts.
	 *
	 * @param actionParts the new action parts
	 */
	public void setActionParts(Set<ActionPartDTO> actionParts) {
		this.actionParts = actionParts;
	}

	/**
	 * Gets the snapshot.
	 *
	 * @return the snapshot
	 */
	public ActionSnapshotDTO getSnapshot() {
		return snapshot;
	}

	/**
	 * Sets the snapshot.
	 *
	 * @param snapshot the new snapshot
	 */
	public void setSnapshot(ActionSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public Set<ActionDTO> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the new children
	 */
	public void setChildren(Set<ActionDTO> children) {
		this.children = children;
	}

	/**
	 * Gets the action objectives.
	 *
	 * @return the action objectives
	 */
	public Set<ActionObjectiveDTO> getActionObjectives() {
		return actionObjectives;
	}

	/**
	 * Sets the action objectives.
	 *
	 * @param actionObjectives the new action objectives
	 */
	public void setActionObjectives(Set<ActionObjectiveDTO> actionObjectives) {
		this.actionObjectives = actionObjectives;
	}

	/**
	 * Gets the severity level.
	 *
	 * @return the severity level
	 */
	public SeverityLevelDTO getSeverityLevel() {
		return severityLevel;
	}

	/**
	 * Sets the severity level.
	 *
	 * @param severityLevel the new severity level
	 */
	public void setSeverityLevel(SeverityLevelDTO severityLevel) {
		this.severityLevel = severityLevel;
	}

	/**
	 * Gets the action operation.
	 *
	 * @return the action operation
	 */
	public ActionOperationEnumDTO getActionOperation() {
		return actionOperation;
	}

	/**
	 * Sets the action operation.
	 *
	 * @param actionOperation the new action operation
	 */
	public void setActionOperation(ActionOperationEnumDTO actionOperation) {
		this.actionOperation = actionOperation;
	}

	/**
	 * Gets the crisis context.
	 *
	 * @return the crisis context
	 */
	public CrisisContextDTO getCrisisContext() {
		return crisisContext;
	}

	/**
	 * Sets the crisis context.
	 *
	 * @param crisisContext the new crisis context
	 */
	public void setCrisisContext(CrisisContextDTO crisisContext) {
		this.crisisContext = crisisContext;
	}
	
}
