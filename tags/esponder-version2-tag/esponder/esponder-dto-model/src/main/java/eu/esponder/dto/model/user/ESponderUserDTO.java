/*
 * 
 */
package eu.esponder.dto.model.user;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderUserDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"userName", "role","actor"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ESponderUserDTO extends ESponderEntityDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6015093808353080103L;

	/** The user name. */
	private String userName;
	
	/** The role. */
	private String role;
	
	//private Set<OperationsCentreDTO> operationsCentres;

	/** The actor. */
	private ActorDTO actor;
	
	/**
	 * Gets the actor.
	 *
	 * @return the actor
	 */
	public ActorDTO getActor() {
		return actor;
	}

	/**
	 * Sets the actor.
	 *
	 * @param actor the new actor
	 */
	public void setActor(ActorDTO actor) {
		this.actor = actor;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	//public Set<OperationsCentreDTO> getOperationsCentres() {
		//return operationsCentres;
	//}

	//public void setOperationsCentres(Set<OperationsCentreDTO> operationsCentres) {
		//this.operationsCentres = operationsCentres;
	//}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ESponderUserDTO [userName=" + userName + ", id=" + id + "]";
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(String role) {
		this.role = role;
	}

}
