/*
 * 
 */
package eu.esponder.dto.model;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

// TODO: Auto-generated Javadoc
/**
 * The Class ResultListDTO.
 * When a service return a list of DTOs, we use the ResultListDTO return type.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({ "resultList" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class ResultListDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -363245325332031836L;

	/** The result list. */
	private List<? extends ESponderEntityDTO> resultList;

	/**
	 * Instantiates a new result list dto.
	 */
	public ResultListDTO() {
	}


	/**
	 * Instantiates a new result list dto.
	 *
	 * @param resultsList the results list
	 */
	public ResultListDTO(List<? extends ESponderEntityDTO> resultsList) {
		this.resultList = resultsList;
	}

	/**
	 * Gets the result list.
	 *
	 * @return the result list
	 */
	public List<? extends ESponderEntityDTO> getResultList() {
		return resultList;
	}

	/**
	 * Sets the result list.
	 *
	 * @param resultList the new result list
	 */
	public void setResultList(List<? extends ESponderEntityDTO> resultList) {
		this.resultList = resultList;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String result = "[ResultsList:";
		for (ESponderEntityDTO entity : this.getResultList()) {
			result += entity.toString();
		}
		result += "]";
		return result;
	}

}
