/*
 * 
 */
package eu.esponder.dto.model.criteria;

import java.util.Collection;

import org.codehaus.jackson.annotate.JsonTypeInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class EsponderCriteriaCollectionDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class EsponderCriteriaCollectionDTO extends EsponderQueryRestrictionDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7905934732438391487L;

	/** The restrictions. */
	private Collection<EsponderQueryRestrictionDTO> restrictions;

	/**
	 * Instantiates a new esponder criteria collection dto.
	 */
	public EsponderCriteriaCollectionDTO() {}

	/**
	 * Instantiates a new esponder criteria collection dto.
	 *
	 * @param restrictions the restrictions
	 */
	public EsponderCriteriaCollectionDTO(Collection<EsponderQueryRestrictionDTO> restrictions) {
		this.restrictions = restrictions;
	}

	/**
	 * Sets the restrictions.
	 *
	 * @param restrictions the new restrictions
	 */
	public void setRestrictions(Collection<EsponderQueryRestrictionDTO> restrictions) {
		this.restrictions = restrictions;
	}

	/**
	 * Gets the restrictions.
	 *
	 * @return the restrictions
	 */
	public Collection<EsponderQueryRestrictionDTO> getRestrictions() {
		return restrictions;
	}

	/**
	 * Adds the.
	 *
	 * @param restriction the restriction
	 */
	public void add(EsponderQueryRestrictionDTO restriction) {
		this.getRestrictions().add(restriction);
	}

	/**
	 * Removes the.
	 *
	 * @param restriction the restriction
	 */
	public void remove(EsponderQueryRestrictionDTO restriction) {
		this.getRestrictions().remove(restriction);
	}

}
