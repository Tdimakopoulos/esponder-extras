/*
 * 
 */
package eu.esponder.dto.model.snapshot.status;



// TODO: Auto-generated Javadoc
/**
 * The Enum ActionPartSnapshotStatusDTO.
 */
public enum ActionPartSnapshotStatusDTO {
	
	/** The started. */
	STARTED,
	
	/** The paused. */
	PAUSED,
	
	/** The completed. */
	COMPLETED,
	
	/** The canceled. */
	CANCELED;
	
	/**
	 * Gets the action part snapshot status enum.
	 *
	 * @param value the value
	 * @return the action part snapshot status enum
	 */
	public static ActionPartSnapshotStatusDTO getActionPartSnapshotStatusEnum(int value) {
		switch (value) {
		case 0:
			return STARTED;
		case 1:
			return PAUSED;
		case 2:
			return COMPLETED;
		case 3:
			return CANCELED;
		default:
			return null;
		}
	}
}
