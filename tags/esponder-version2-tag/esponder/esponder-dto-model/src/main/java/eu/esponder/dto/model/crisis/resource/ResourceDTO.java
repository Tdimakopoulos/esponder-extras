/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ResourceDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6714622659532068332L;

	/** The type. */
	protected String type;

	/** The title. */
	protected String title;

	/** The status. */
	protected ResourceStatusDTO status;

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ResourceStatusDTO getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ResourceStatusDTO status) {
		this.status = status;
	}

	/**
	 * Gets the resource id.
	 *
	 * @return the resource id
	 */
	public String getResourceId() {
		return id+":"+this.type;
	}

	/**
	 * Sets the resource id.
	 *
	 * @param resourceId the new resource id
	 */
	public void setResourceId(String resourceId) {
		String id[];
		id = resourceId.split(":");
		if(id[0].equals("null")) {
			this.setId(null);
		}
		else {
			this.setId(new Long(id[0]).longValue());	
		}
		this.setType(id[1]);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResourceDTO [status=" + status + ", id=" + id + ", title="
				+ title + "]";
	}

}
