/*
 * 
 */
package eu.esponder.dto.model.crisis.view;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

// TODO: Auto-generated Javadoc
/**
 * The Class ReferencePOIDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "referenceFile", "averageSize"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ReferencePOIDTO extends ResourcePOIDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 698766046297897727L;

	/** The reference file. */
	private String referenceFile;
	
	/** The average size. */
	private Float averageSize;

	/**
	 * Gets the reference file.
	 *
	 * @return the reference file
	 */
	public String getReferenceFile() {
		return referenceFile;
	}

	/**
	 * Sets the reference file.
	 *
	 * @param referenceFile the new reference file
	 */
	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

	/**
	 * Gets the average size.
	 *
	 * @return the average size
	 */
	public Float getAverageSize() {
		return averageSize;
	}

	/**
	 * Sets the average size.
	 *
	 * @param averageSize the new average size
	 */
	public void setAverageSize(Float averageSize) {
		this.averageSize = averageSize;
	}

}
