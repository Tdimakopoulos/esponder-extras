/*
 * 
 */
package eu.esponder.rest.portal;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class RunSimulator.
 */
@Path("/portal/simulator")
public class RunSimulator extends ESponderResource {

	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}
	
	/**
	 * Run simulation.
	 *
	 * @param pkiKey the pki key
	 * @return the string
	 */
	@GET
	@Path("/run")
	@Produces({ MediaType.APPLICATION_JSON })
	public String RunSimulation(
@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {
		
		Long userID=SecurityCheck(pkiKey);
		
		MeasurementSimulator simulator = new MeasurementSimulator();
		try {
			simulator.runSimulation();
		} catch (InterruptedException e) {
			return "Simulation failed";
		}

		return "Simulation succeeded";
	}
	
}