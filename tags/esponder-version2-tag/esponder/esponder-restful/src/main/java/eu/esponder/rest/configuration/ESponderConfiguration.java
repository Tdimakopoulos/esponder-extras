/*
 * 
 */
package eu.esponder.rest.configuration;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderConfiguration.
 */
@Path("/system/configuration")
public class ESponderConfiguration extends ESponderResource {

	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}


	/**
	 * Gets the configuration by id.
	 *
	 * @param configurationId the configuration id
	 * @param pkiKey the pki key
	 * @return the configuration by id
	 * @throws ClassNotFoundException the class not found exception
	 */
	@GET
	@Path("/getByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO getConfigurationById (
			@QueryParam("configurationId") @NotNull(message="ConfigurationId may not be null") Long configurationId,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws ClassNotFoundException {

		Long userID=SecurityCheck(pkiKey);

		return this.getConfigurationRemoteService().findESponderConfigByIdRemote(configurationId);
	}


	/**
	 * Gets the configuration by name.
	 *
	 * @param configurationName the configuration name
	 * @param pkiKey the pki key
	 * @return the configuration by name
	 * @throws ClassNotFoundException the class not found exception
	 */
	@GET
	@Path("/getByName")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO getConfigurationByName (
			@QueryParam("configurationName") @NotNull(message="ConfigurationId may not be null") String configurationName,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws ClassNotFoundException {

		Long userID=SecurityCheck(pkiKey);

		return this.getConfigurationRemoteService().findESponderConfigByNameRemote(configurationName);
	}


	/**
	 * Creates the e sponder configuration.
	 *
	 * @param configurationDTO the configuration dto
	 * @param pkiKey the pki key
	 * @return the e sponder config parameter dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@POST
	@Path("/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO createESponderConfiguration(
			@NotNull(message="entity to be created may not be null") ESponderConfigParameterDTO configurationDTO,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws ClassNotFoundException {

		Long userID=SecurityCheck(pkiKey);

		ESponderConfigParameterDTO configDTO = this.getConfigurationRemoteService().createESponderConfigRemote(configurationDTO, userID); 
		return configDTO;
	}


	/**
	 * Update configuration.
	 *
	 * @param configurationDTO the configuration dto
	 * @param pkiKey the pki key
	 * @return the e sponder config parameter dto
	 */
	@PUT
	@Path("/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO updateConfiguration( 
			@NotNull(message="entity to be updated may not be null") ESponderConfigParameterDTO configurationDTO,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return this.getConfigurationRemoteService().updateESponderConfigRemote(configurationDTO, userID);
	}


	/**
	 * Delete configuration.
	 *
	 * @param configurationId the configuration id
	 * @param pkiKey the pki key
	 * @return the long
	 */
	@DELETE
	@Path("/delete")
	public Long deleteConfiguration(
			@QueryParam("configurationId") @NotNull(message="ConfigurationId may not be null") Long configurationId,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		this.getConfigurationRemoteService().deleteESponderConfigRemote(configurationId, userID);
		return configurationId;
	}


}