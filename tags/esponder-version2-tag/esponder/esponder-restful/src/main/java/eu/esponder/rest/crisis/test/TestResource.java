/*
 * 
 */
package eu.esponder.rest.crisis.test;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.rest.ESponderResource;


// TODO: Auto-generated Javadoc
/**
 * The Class TestResource.
 */
@Path("/crisis/contextTest")
public class TestResource extends ESponderResource {

	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}

	/**
	 * Gets the rest test.
	 *
	 * @param pkiKey the pki key
	 * @return the rest test
	 */
	@GET
	@Path("/test")
	@Produces({MediaType.APPLICATION_JSON})
	public PointDTO getRestTest(
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		PointDTO point = new PointDTO();

		point.setAltitude(new BigDecimal(1));
		point.setLongitude(new BigDecimal(2));
		point.setLatitude(new BigDecimal(3));

		return point; 
	}


	/**
	 * Post rest test.
	 *
	 * @param point the point
	 * @param pkiKey the pki key
	 * @return the point dto
	 */
	@POST
	@Path("/testPost")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public PointDTO postRestTest(
			@NotNull(message="point may not be null") PointDTO point, 
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		//		PointDTO anotherPoint = new PointDTO();
		System.out.println("Server: POST operation received");
		System.out.println(point.toString());
		point.setAltitude(new BigDecimal(55L));
		System.out.println("Server: POST operation returning");
		return point;
	}

}
