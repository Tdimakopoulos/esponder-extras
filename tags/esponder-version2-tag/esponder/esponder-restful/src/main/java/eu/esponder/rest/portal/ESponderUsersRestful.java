/*
 * 
 */
package eu.esponder.rest.portal;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderUsersRestful.
 */
@Path("/portal/users")
public class ESponderUsersRestful extends ESponderResource {
	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}

	/**
	 * Read a user by id.
	 *
	 * @param pkiKey the pki key
	 * @return the e sponder user dto
	 */
	@GET
	@Path("/findById")
	@Produces({ MediaType.APPLICATION_JSON })
	public ESponderUserDTO readAUserById(
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return this.getUserRemoteService().findUserByIdRemote(userID);
	}

	/**
	 * Read a user byuser name.
	 *
	 * @param userName the user name
	 * @param pkiKey the pki key
	 * @return the e sponder user dto
	 */
	@GET
	@Path("/findByTitle")
	@Produces({ MediaType.APPLICATION_JSON })
	public ESponderUserDTO readAUserByuserName(
			@QueryParam("userName") @NotNull(message = "userID may not be null") String userName,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);
		return this.getUserRemoteService().findUserByNameRemote(userName);
	}

	/**
	 * Read all users.
	 *
	 * @param pkiKey the pki key
	 * @return the result list dto
	 */
	@GET
	@Path("/findAll")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO readAllUsers(
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		List<ESponderUserDTO> pResults = this.getUserRemoteService().findAllUsersRemote();
		return new ResultListDTO(pResults);

	}

	/**
	 * Creates the user.
	 *
	 * @param esponderUSer the esponder u ser
	 * @param pkiKey the pki key
	 * @return the e sponder user dto
	 */
	@POST
	@Path("/createUser")
	@Produces({ MediaType.APPLICATION_JSON })
	public ESponderUserDTO createUser(
			@NotNull(message = "ESponder User may not be null") ESponderUserDTO esponderUSer,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);
		return this.getUserRemoteService().createUserRemote(esponderUSer, userID);
	}

	/**
	 * Update user.
	 *
	 * @param esponderUSer the esponder u ser
	 * @param pkiKey the pki key
	 * @return the e sponder user dto
	 */
	@PUT
	@Path("/updateUser")
	@Produces({ MediaType.APPLICATION_JSON })
	public ESponderUserDTO updateUser(
			@NotNull(message = "ESponder User may not be null") ESponderUserDTO esponderUSer,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);

		return this.getUserRemoteService().updateUserRemote(esponderUSer, userID);
	}

	/**
	 * Delete user.
	 *
	 * @param deletedUserID the deleted user id
	 * @param pkiKey the pki key
	 * @return the long
	 */
	@DELETE
	@Path("/deleteUser")
	@Produces({ MediaType.APPLICATION_JSON })
	public Long deleteUser(
			@QueryParam("deletedUserID") @NotNull(message = "ESponder User id to be deleted may not be null") Long deletedUserID,
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {

		Long userID=SecurityCheck(pkiKey);
		this.getUserRemoteService().deleteUserRemote(deletedUserID, userID);
		return deletedUserID; 
	}

}