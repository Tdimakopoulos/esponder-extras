/*
 * 
 */
package eu.esponder.rest;

import javax.naming.NamingException;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.LogisticsRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.events.EventsEntityRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderResource.
 */
public abstract class ESponderResource {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the sensor remote service.
	 *
	 * @return the sensor remote service
	 */
	protected SensorRemoteService getSensorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the actor remote service.
	 *
	 * @return the actor remote service
	 */
	protected ActorRemoteService getActorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the equipment remote service.
	 *
	 * @return the equipment remote service
	 */
	protected EquipmentRemoteService getEquipmentRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EquipmentBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the mapping remote service.
	 *
	 * @return the mapping remote service
	 */
	protected ESponderRemoteMappingService getMappingRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the operations centre remote service.
	 *
	 * @return the operations centre remote service
	 */
	protected OperationsCentreRemoteService getOperationsCentreRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/OperationsCentreBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Gets the action remote service.
	 *
	 * @return the action remote service
	 */
	protected ActionRemoteService getActionRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Gets the generic remote service.
	 *
	 * @return the generic remote service
	 */
	protected GenericRemoteService getGenericRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the configuration remote service.
	 *
	 * @return the configuration remote service
	 */
	protected ESponderConfigurationRemoteService getConfigurationRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderConfigurationBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the user remote service.
	 *
	 * @return the user remote service
	 */
	protected UserRemoteService getUserRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/UserBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the personnel remote service.
	 *
	 * @return the personnel remote service
	 */
	protected PersonnelRemoteService getPersonnelRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/PersonnelBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the personnel remote service.
	 *
	 * @return the personnel remote service
	 */
	protected PersonnelRemoteService getPersonnelCompetencesRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/PersonnelBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the logistics remote service.
	 *
	 * @return the logistics remote service
	 */
	protected LogisticsRemoteService getLogisticsRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/LogisticsBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the osgi events entity remote service.
	 *
	 * @return the osgi events entity remote service
	 */
	protected EventsEntityRemoteService getOsgiEventsEntityRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EventsEntityBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the resource category remote service.
	 *
	 * @return the resource category remote service
	 */
	protected ResourceCategoryRemoteService getResourceCategoryRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ResourceCategoryBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the type remote service.
	 *
	 * @return the type remote service
	 */
	protected TypeRemoteService getTypeRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	/**
	 * Gets the crisis remote service.
	 *
	 * @return the crisis remote service
	 */
	protected CrisisRemoteService getCrisisRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/CrisisBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

}
