/*
 * 
 */
package eu.esponder.rest.CoordinatesForAddress;

// TODO: Auto-generated Javadoc
/**
 * The Class Coordinates.
 */
public class Coordinates {

	 /** The Latitude. */
 	private float Latitude ;

	    /** The Longitude. */
    	private float Longitude ;
	    
	    /** The Formated address. */
    	private String FormatedAddress;
	    
    	/** The Formated city. */
    	private String FormatedCity;
	    
	    /**
    	 * Instantiates a new coordinates.
    	 *
    	 * @param latitude the latitude
    	 * @param longitude the longitude
    	 */
    	public Coordinates(float latitude, float longitude)
	    {
	        Latitude = latitude;
	        Longitude = longitude;
	    }

	    /**
    	 * Gets the latitude.
    	 *
    	 * @return the Latitude
    	 */
	    public float getLatitude() {
	        return Latitude;
	    }

	    /**
    	 * Sets the latitude.
    	 *
    	 * @param Latitude the Latitude to set
    	 */
	    public void setLatitude(float Latitude) {
	        this.Latitude = Latitude;
	    }

	    /**
    	 * Gets the longitude.
    	 *
    	 * @return the Longitude
    	 */
	    public float getLongitude() {
	        return Longitude;
	    }

	    /**
    	 * Sets the longitude.
    	 *
    	 * @param Longitude the Longitude to set
    	 */
	    public void setLongitude(float Longitude) {
	        this.Longitude = Longitude;
	    }

	    /**
    	 * Gets the formated address.
    	 *
    	 * @return the FormatedAddress
    	 */
	    public String getFormatedAddress() {
	        return FormatedAddress;
	    }

	    /**
    	 * Sets the formated address.
    	 *
    	 * @param FormatedAddress the FormatedAddress to set
    	 */
	    public void setFormatedAddress(String FormatedAddress) {
	        this.FormatedAddress = FormatedAddress;
	    }

	    /**
    	 * Gets the formated city.
    	 *
    	 * @return the FormatedCity
    	 */
	    public String getFormatedCity() {
	        return FormatedCity;
	    }

	    /**
    	 * Sets the formated city.
    	 *
    	 * @param FormatedCity the FormatedCity to set
    	 */
	    public void setFormatedCity(String FormatedCity) {
	        this.FormatedCity = FormatedCity;
	    }

}
