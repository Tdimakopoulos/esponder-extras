/*
 * 
 */
package eu.esponder.rest.datafusion;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService;
import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;
import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.rest.ESponderResource;
import eu.esponder.test.ResourceLocator;



// TODO: Auto-generated Javadoc
/**
 * The Class DatafusionRepositoryManager.
 */
@Path("/datafusion/repository")
public class DatafusionRepositoryManager extends ESponderResource {

	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}
	
	/**
	 * Remotetolocal.
	 *
	 * @param pkiKey the pki key
	 * @return the string
	 */
	@GET
	@Path("/remotetolocal")
	public String remotetolocal (@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {
		
		Long userID=SecurityCheck(pkiKey);
		
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.ReinitializeRepository();
		

		return szReturnStatus;
	}

	/**
	 * Ruleresults.
	 *
	 * @param pkiKey the pki key
	 * @return the rule results
	 * @throws Exception the exception
	 */
	@GET
	@Path("/ruleresults")
	@Produces({ MediaType.APPLICATION_JSON })
	public RuleResults ruleresults (@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws Exception {
		
		Long userID=SecurityCheck(pkiKey);
		
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		
		List<RuleResultsXML> pResults=pservice.GetRuleResults();
		return new RuleResults(pResults);
	}
	
	/**
	 * Deleterepo.
	 *
	 * @param pkiKey the pki key
	 * @return the string
	 */
	@GET
	@Path("/deleterepo")
	public String deleterepo (@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {
		
		Long userID=SecurityCheck(pkiKey);
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.DeleteRepository();
		

		return szReturnStatus;
	}

	/**
	 * Switchtolocal.
	 *
	 * @param pkiKey the pki key
	 * @return the string
	 */
	@GET
	@Path("/switchtolocal")
	public String switchtolocal (@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {
		
		Long userID=SecurityCheck(pkiKey);
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.SetToLocal();
		

		return szReturnStatus;
	}

	/**
	 * Switchtoguvnor.
	 *
	 * @param pkiKey the pki key
	 * @return the string
	 */
	@GET
	@Path("/switchtoguvnor")
	public String switchtoguvnor (@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) {
		
		Long userID=SecurityCheck(pkiKey);
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.SetToLive();
		

		return szReturnStatus;
	}
}
