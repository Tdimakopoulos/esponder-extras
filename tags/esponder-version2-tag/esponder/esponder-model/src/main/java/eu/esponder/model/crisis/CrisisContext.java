/*
 * 
 */
package eu.esponder.model.crisis;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.snapshot.CrisisContextSnapshot;
import eu.esponder.model.snapshot.location.Sphere;
import eu.esponder.model.type.CrisisType;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisContext.
 * This entity includes all root (top-level) information associated with a crisis incident managed by the E-SPONDER platform. Different incidents should be associated with different crisis context. Discrimination among incidents should be based on time and/or location (i.e. incidents monitored at different time periods may be considered as different, or parallel executed (in time) incidents that refer to different or even the same location could be considered as being different. Selection of one incident over the other is simply a matter of the End-User (no automation will be applied in this).
 */
@Entity
@Table(name="crisis_context")
@NamedQueries({
	@NamedQuery(name="CrisisContext.findByTitle", query="select cc from CrisisContext cc where cc.title=:title"),
	@NamedQuery(name="CrisisContext.findAll", query="select cc from CrisisContext cc")
})
public class CrisisContext extends ESponderEntity<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2118773143338642719L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CRISIS_CONTEXT_ID")
	private Long id;
	
	/** The incident title. Will be provided by the user on creation of the entity. Not null , Unique */
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	/** The set of actions associated with this CrisisContext. */
	@OneToMany(mappedBy="crisisContext")
	private Set<Action> actions;
	
	/** The crisis location. This field provides . Type Sphere */
	@OneToOne
	@JoinColumn(name="LOCATION_ID")
	private Sphere crisisLocation;
	
	/** The incident entity's snapshots (including the status of the incident). */
	@OneToMany(mappedBy="crisisContext")
	private Set<CrisisContextSnapshot> snapshots;
	
	/** The set of operations centres associated with this incident (i.e. Crisis Context). */
	@OneToMany(mappedBy="crisisContext")
	private Set<OperationsCentre> operationsCentres;
	
	/** Additional information inserted by the Operator for this crisis Context. Max length 10000 characters*/
	@Column(name = "CRISIS_CONTEXT_ADDITIONAL_INFO", length = 10000)
    private String additionalInfo;
	 
	/** The start date of this crisis incident. */
	@Column(name = "CRISIS_CONTEXT_START_DATE")
	private Long startDate;
	
	/** The end date of this crisis incident. */
	@Column(name = "CRISIS_CONTEXT_END_DATE")
	private Long endDate;
	
	/** The set of the incident's crisis types. */
	@ManyToMany/*(cascade={CascadeType.PERSIST,CascadeType.MERGE, CascadeType.REMOVE})*/
	@JoinTable(name="crisis_context_types",
			joinColumns=@JoinColumn(name="CONTEXT_ID"),
			inverseJoinColumns=@JoinColumn(name="CRISIS_TYPE_ID"))
	private Set<CrisisType> crisisTypes;
	
	/** The crisis context alert. Enum type of CrisisContextAlertEnum */
	@Column(name="ALERT")
	private CrisisContextAlertEnum crisisContextAlert;
	
	/**
	 * Gets the additional info.
	 *
	 * @return the additional info
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	/**
	 * Sets the additional info.
	 *
	 * @param additionalInfo the new additional info
	 */
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Long getStartDate() {
		return startDate;
	}

	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Long getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the operations centres.
	 *
	 * @return the operations centres
	 */
	public Set<OperationsCentre> getOperationsCentres() {
		return operationsCentres;
	}

	/**
	 * Sets the operations centres.
	 *
	 * @param operationsCentres the new operations centres
	 */
	public void setOperationsCentres(Set<OperationsCentre> operationsCentres) {
		this.operationsCentres = operationsCentres;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the actions.
	 *
	 * @return the actions
	 */
	public Set<Action> getActions() {
		return actions;
	}

	/**
	 * Sets the actions.
	 *
	 * @param actions the new actions
	 */
	public void setActions(Set<Action> actions) {
		this.actions = actions;
	}

	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<CrisisContextSnapshot> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<CrisisContextSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	/**
	 * Gets the crisis location.
	 *
	 * @return the crisis location
	 */
	public Sphere getCrisisLocation() {
		return crisisLocation;
	}

	/**
	 * Sets the crisis location.
	 *
	 * @param crisisLocation the new crisis location
	 */
	public void setCrisisLocation(Sphere crisisLocation) {
		this.crisisLocation = crisisLocation;
	}

	/**
	 * Gets the crisis context alert.
	 *
	 * @return the crisis context alert
	 */
	public CrisisContextAlertEnum getCrisisContextAlert() {
		return crisisContextAlert;
	}

	/**
	 * Sets the crisis context alert.
	 *
	 * @param crisisContextAlert the new crisis context alert
	 */
	public void setCrisisContextAlert(CrisisContextAlertEnum crisisContextAlert) {
		this.crisisContextAlert = crisisContextAlert;
	}

	/**
	 * Gets the crisis types.
	 *
	 * @return the crisis types
	 */
	public Set<CrisisType> getCrisisTypes() {
		return crisisTypes;
	}

	/**
	 * Sets the crisis types.
	 *
	 * @param crisisTypes the new crisis types
	 */
	public void setCrisisTypes(Set<CrisisType> crisisTypes) {
		this.crisisTypes = crisisTypes;
	}
	
}
