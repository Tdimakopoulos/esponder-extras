/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelTrainingType.
 */
@Entity
@DiscriminatorValue("TRAINING")
public class PersonnelTrainingType extends ESponderType {

	private static final long serialVersionUID = 7268016199798192551L;

}
