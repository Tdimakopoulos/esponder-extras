/*
 * 
 */
package eu.esponder.model.snapshot.status;

// TODO: Auto-generated Javadoc
/**
 * The Enum EquipmentSnapshotStatus.
 */
public enum EquipmentSnapshotStatus {
	
	/** The working. */
	WORKING,
	
	/** The malfunctioning. */
	MALFUNCTIONING,
	
	/** The damaged. */
	DAMAGED
}
