/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.collections.CollectionUtils;

import eu.esponder.model.crisis.view.VoIPURL;
import eu.esponder.model.snapshot.resource.ActorSnapshot;
import eu.esponder.model.type.ActorType;

// TODO: Auto-generated Javadoc
/**
 * The Class Actor.
 * This is a entity class which manage information about a actor, a platform user is also a actor
 */
@Entity
@Table(name="actor")
@NamedQueries({
	@NamedQuery(name="Actor.findByTitle", query="select a from Actor a where a.title=:title"),
	@NamedQuery(name="Actor.findAll", query="select a from Actor a"),
	@NamedQuery( name="Actor.findActorsWithSubordinates", query="SELECT s FROM Actor s WHERE s.operationsCentre.id = :operationsCentreID AND " +
			"(s.subordinates IS NOT NULL OR (s.subordinates IS NULL and s.supervisor IS NULL))"),
	@NamedQuery( name="Actor.findSubordinatesForActor", query="SELECT s FROM Actor s WHERE s.supervisor.id = :actorID")
})

public class Actor extends Resource {

	private static final long serialVersionUID = 2224027092652397963L;

	/**
	 * Models the E-SPONDER-specific ActorType, i.e. Strategic, Tactical, Operational
	 * Not Null
	 */
	@ManyToOne
	@JoinColumn(name="ACTOR_TYPE_ID", nullable=false)
	private ActorType actorType;

	/** The supervisor. This is a supervisor for Meoc or EOC*/
	@ManyToOne
	@JoinColumn(name="SUPERVISOR_ID")
	private Actor supervisor;

	/** The subordinates. If we are in meoc level this set describes the FR cheifs, If we are in EOC level this is the supervisors of Meoc*/
	@OneToMany(mappedBy="supervisor")
	private Set<Actor> subordinates;

	/** The operations centre. The operations center that the actor belong too*/
	@ManyToOne
	@JoinColumn(name="OPERATIONS_CENTRE_ID")
	private OperationsCentre operationsCentre;

	/** The equipment set. */
	@OneToMany(mappedBy="actor", cascade=CascadeType.PERSIST)
	private Set<Equipment> equipmentSet;

	/** The snapshots. */
	@OneToMany(mappedBy="actor")
	private Set<ActorSnapshot> snapshots;

	/** The personnel. Used to map the personnel to actor*/
	@OneToOne
	@JoinColumn(name="PERSON_ID")
	private Personnel personnel;
	
	/** The voip url. */
	@Embedded
	private VoIPURL voIPURL;

	/* (non-Javadoc)
	 * @see eu.esponder.model.crisis.resource.Resource#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.crisis.resource.Resource#setId(java.lang.Long)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the actor type.
	 *
	 * @return the actor type
	 */
	public ActorType getActorType() {
		return actorType;
	}

	/**
	 * Sets the actor type.
	 *
	 * @param actorType the new actor type
	 */
	public void setActorType(ActorType actorType) {
		this.actorType = actorType;
	}

	/**
	 * Gets the supervisor.
	 *
	 * @return the supervisor
	 */
	public Actor getSupervisor() {
		return supervisor;
	}

	/**
	 * Sets the supervisor.
	 *
	 * @param supervisor the new supervisor
	 */
	public void setSupervisor(Actor supervisor) {
		this.supervisor = supervisor;
	}

	/**
	 * Gets the subordinates.
	 *
	 * @return the subordinates
	 */
	public Set<Actor> getSubordinates() {
		return subordinates;
	}

	/**
	 * Sets the subordinates.
	 *
	 * @param subordinates the new subordinates
	 */
	public void setSubordinates(Set<Actor> subordinates) {
		this.subordinates = subordinates;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OperationsCentre getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OperationsCentre operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	/**
	 * Gets the equipment set.
	 *
	 * @return the equipment set
	 */
	public Set<Equipment> getEquipmentSet() {

		return equipmentSet;
	}

	/**
	 * Sets the equipment set.
	 *
	 * @param equipmentSet the new equipment set
	 */
	public void setEquipmentSet(Set<Equipment> equipmentSet) {
		this.equipmentSet = equipmentSet;
	}

	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<ActorSnapshot> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<ActorSnapshot> snapshots) {
		this.snapshots = snapshots;
	}
	
	/**
	 * Gets the personnel.
	 *
	 * @return the personnel
	 */
	public Personnel getPersonnel() {
		return personnel;
	}

	/**
	 * Sets the personnel.
	 *
	 * @param personnel the new personnel
	 */
	public void setPersonnel(Personnel personnel) {
		this.personnel = personnel;
	}

	/**
	 * Traverse subordinates.
	 */
	public void traverseSubordinates() {
		if (CollectionUtils.isNotEmpty(this.subordinates)) {
			for (Actor actor : this.subordinates) {
				actor.traverseSubordinates();
			}
		}
	}

	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURL getVoIPURL() {
		return voIPURL;
	}

	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURL voIPURL) {
		this.voIPURL = voIPURL;
	}

}
