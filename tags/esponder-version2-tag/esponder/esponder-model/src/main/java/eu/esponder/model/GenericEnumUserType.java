/*
 * 
 */
package eu.esponder.model;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.type.BasicType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TypeResolver;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

// TODO: Auto-generated Javadoc
/**
 * The Class GenericEnumUserType.
 * Internaly used class for Enums
 */
public class GenericEnumUserType implements UserType, ParameterizedType {

	/** The enum class. */
	@SuppressWarnings("rawtypes")
	private Class<? extends Enum> enumClass;
	
	/** The identifier type. */
	private Class<?> identifierType;
	
	/** The identifier method. */
	private Method identifierMethod;
	
	/** The value of method. */
	private Method valueOfMethod;
	
	/** The Constant defaultIdentifierMethodName. */
	private static final String defaultIdentifierMethodName = "name";
	
	/** The Constant defaultValueOfMethodName. */
	private static final String defaultValueOfMethodName = "valueOf";
	
	/** The type. */
	private BasicType type;
	
	/** The sql types. */
	private int[] sqlTypes;

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.ParameterizedType#setParameterValues(java.util.Properties)
	 */
	public void setParameterValues(Properties parameters) {
		String enumClassName = parameters.getProperty("enumClass");
		try {
			enumClass = Class.forName(enumClassName).asSubclass(Enum.class);
		} catch (ClassNotFoundException exception) {
			throw new HibernateException("Enum class not found", exception);
		}

		String identifierMethodName = parameters.getProperty(
				"identifierMethod", defaultIdentifierMethodName);

		try {
			identifierMethod = enumClass.getMethod(identifierMethodName,
					new Class[0]);
			identifierType = identifierMethod.getReturnType();
		} catch (Exception exception) {
			throw new HibernateException("Failed to optain identifier method",
					exception);
		}

		TypeResolver tr = new TypeResolver();
		type = tr.basic(identifierType.getName());
		if (type == null) {
			throw new HibernateException("Unsupported identifier type "
					+ identifierType.getName());
		}
		if (type instanceof IntegerType) {
			sqlTypes = new int[] { ((IntegerType)type).sqlType() };
		} else if (type instanceof StringType) {
			sqlTypes = new int[] { ((StringType)type).sqlType() };
		} else {
			throw new HibernateException("Unsupported identifier type "
					+ identifierType.getName());
		}

		String valueOfMethodName = parameters.getProperty("valueOfMethod",
				defaultValueOfMethodName);

		try {
			valueOfMethod = enumClass.getMethod(valueOfMethodName,
					new Class[] { identifierType });
		} catch (Exception exception) {
			throw new HibernateException("Failed to optain valueOf method",
					exception);
		}
	}

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.UserType#returnedClass()
	 */
	@SuppressWarnings("rawtypes")
	public Class returnedClass() {
		return enumClass;
	}

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.UserType#nullSafeGet(java.sql.ResultSet, java.lang.String[], java.lang.Object)
	 */
	@SuppressWarnings("deprecation")
	public Object nullSafeGet(ResultSet rs, String[] names, Object owner)
			throws HibernateException, SQLException {
		Object identifier = null;
		if (type instanceof IntegerType) {
			identifier = ((IntegerType)type).get(rs, names[0]);
		} else if (type instanceof StringType) {
			identifier = ((StringType)type).get(rs, names[0]);
		} else {
			throw new HibernateException("Unsupported identifier type "
					+ identifierType.getName());
		}
		try {
			return valueOfMethod.invoke(enumClass, new Object[] { identifier });
		} catch (Exception exception) {
			throw new HibernateException("Exception while"
					+ " invoking valueOfMethod of enumeration class: ",
					exception);
		}
	}

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.UserType#nullSafeSet(java.sql.PreparedStatement, java.lang.Object, int)
	 */
	public void nullSafeSet(PreparedStatement st, Object value, int index)
			throws HibernateException, SQLException {
		try {
			Object identifier = value != null ? identifierMethod.invoke(value,
					new Object[0]) : null;
			st.setObject(index, identifier);
		} catch (Exception exception) {
			throw new HibernateException("Exception while"
					+ " invoking identifierMethod of enumeration class: ",
					exception);

		}
	}

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.UserType#sqlTypes()
	 */
	public int[] sqlTypes() {
		return sqlTypes;
		// There was a logical bug within the set-up phase of any user type
		// I reported the issue and it got instantly solved (Thanks again
		// Garvin!)
		// But it might still exist in your Hibernate version. So if you are
		// facing any null-pointer exceptions, use the return statement below.
		// Note: INTEGER works even for String based mappings...
		// return new int [] {Types.INTEGER};
	}

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.UserType#assemble(java.io.Serializable, java.lang.Object)
	 */
	public Object assemble(Serializable cached, Object owner)
			throws HibernateException {
		return cached;
	}

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.UserType#deepCopy(java.lang.Object)
	 */
	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.UserType#disassemble(java.lang.Object)
	 */
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.UserType#equals(java.lang.Object, java.lang.Object)
	 */
	public boolean equals(Object x, Object y) throws HibernateException {
		return x == y;
	}

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.UserType#hashCode(java.lang.Object)
	 */
	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.UserType#isMutable()
	 */
	public boolean isMutable() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.hibernate.usertype.UserType#replace(java.lang.Object, java.lang.Object, java.lang.Object)
	 */
	public Object replace(Object original, Object target, Object owner)
			throws HibernateException {
		return original;
	}
}
