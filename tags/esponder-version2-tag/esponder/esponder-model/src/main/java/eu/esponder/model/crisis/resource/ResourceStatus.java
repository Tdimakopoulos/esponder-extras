/*
 * 
 */
package eu.esponder.model.crisis.resource;

// TODO: Auto-generated Javadoc
/**
 * The Enum ResourceStatus.
 * Enum that control the status of a resource
 */
public enum ResourceStatus {
	
	/** The available. */
	AVAILABLE,
	
	/** The unavailable. */
	UNAVAILABLE,
	
	/** The reserved. */
	RESERVED,
	
	/** The allocated. */
	ALLOCATED
}
