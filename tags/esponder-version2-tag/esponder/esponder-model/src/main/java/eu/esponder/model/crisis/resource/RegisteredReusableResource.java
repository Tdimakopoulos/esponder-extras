/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class RegisteredReusableResource.
 * Entity class that manage information about  Registered Reusable Resource
 * The Consumable Resource before a crisis is Registered Reusable Resource, when  a crisis start a copy of  Registered Reusable Resource is created as Reusable Resource
 */
@Entity
@Table(name="registered_reusable_resource")
@NamedQueries({
	@NamedQuery(name="RegisteredReusableResource.findByTitle", query="select rrr from RegisteredReusableResource rrr where rrr.title=:title"),
	@NamedQuery(name="RegisteredReusableResource.findAll", query="select rrr from RegisteredReusableResource rrr")
})
public class RegisteredReusableResource extends Resource {
	
	private static final long serialVersionUID = -2211872166654303250L;

	/** The quantity. */
	@Column(name="QUANTITY")
	private BigDecimal quantity;
	
	/** The registered consumables. */
	@OneToMany(mappedBy="container")
	private Set<RegisteredConsumableResource> registeredConsumables;
	
	/** The parent. */
	@ManyToOne
	@JoinColumn(name="PARENT_ID")
	private RegisteredReusableResource parent;
	
	/** The children. */
	@OneToMany(mappedBy="parent")
	private Set<RegisteredReusableResource> children;
	
	/**
	 * TODO: Fix this to point to OperationsCentreCategory. For now we just keep the correct OperationsCentreCategoryId
	 */
//	@ManyToOne
//	@JoinColumn(name="CATEGORY_ID")
//	private ReusableResourceCategory reusableResourceCategory;
	@Column(name="CATEGORY_ID")
	private Long reusableResourceCategoryId;

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Gets the registered consumables.
	 *
	 * @return the registered consumables
	 */
	public Set<RegisteredConsumableResource> getRegisteredConsumables() {
		return registeredConsumables;
	}

	/**
	 * Sets the registered consumables.
	 *
	 * @param registeredConsumables the new registered consumables
	 */
	public void setRegisteredConsumables(
			Set<RegisteredConsumableResource> registeredConsumables) {
		this.registeredConsumables = registeredConsumables;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public RegisteredReusableResource getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(RegisteredReusableResource parent) {
		this.parent = parent;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public Set<RegisteredReusableResource> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the new children
	 */
	public void setChildren(Set<RegisteredReusableResource> children) {
		this.children = children;
	}

	/**
	 * Gets the reusable resource category id.
	 *
	 * @return the reusable resource category id
	 */
	public Long getReusableResourceCategoryId() {
		return reusableResourceCategoryId;
	}

	/**
	 * Sets the reusable resource category id.
	 *
	 * @param reusableResourceCategoryId the new reusable resource category id
	 */
	public void setReusableResourceCategoryId(Long reusableResourceCategoryId) {
		this.reusableResourceCategoryId = reusableResourceCategoryId;
	}

	
}
