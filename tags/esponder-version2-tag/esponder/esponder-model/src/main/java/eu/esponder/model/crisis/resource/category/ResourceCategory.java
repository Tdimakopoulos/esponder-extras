/*
 * 
 */
package eu.esponder.model.crisis.resource.category;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceCategory.
 * ResourceCategory is the root of the hierarchy used to manage all type information associated with each EsponderEntity.
 * Practically each EsponderEntity may have one or more EsponderTypes associated with it, namely an Actor has an ActorType, or a Personnel has a PersonnelCompetenceType and a PersonnelTrainingType.
 * As such, ResourceCategories and respective subclasses model this aspect and thus enable query execution based on EsponderTypes (e.g. fetch all Personnel that has a particular training or skill etc. or all equipment with a particular feature)
 */
@MappedSuperclass
public abstract class ResourceCategory extends ESponderEntity<Long> {

	private static final long serialVersionUID = -2401622867316452380L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="CATEGORY_ID")
	protected Long id;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

}
