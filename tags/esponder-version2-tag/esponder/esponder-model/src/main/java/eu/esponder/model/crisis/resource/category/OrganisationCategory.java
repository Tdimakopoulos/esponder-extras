/*
 * 
 */
package eu.esponder.model.crisis.resource.category;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.Organisation;
import eu.esponder.model.type.DisciplineType;
import eu.esponder.model.type.OrganisationType;

// TODO: Auto-generated Javadoc
/**
 * The Class OrganisationCategory.
 * Manage information associate with the Organization category
 */
@Entity
@Table(name="organisation_category")
@NamedQueries({
	@NamedQuery(name="OrganisationCategory.findByType", query="select a from OrganisationCategory a where a.disciplineType=:disciplineType and a.organisationType=:organisationType") })
public class OrganisationCategory extends ResourceCategory {

	private static final long serialVersionUID = -1406195628572785186L;
	
	/** The discipline type. */
	@OneToOne
	@JoinColumn(name="DISCIPLINE_TYPE", nullable=false)
	private DisciplineType disciplineType;
	
	/**
	 * Models the hierarchy of internal organisation elements of the particular organisation, 
	 * e.g. for FireFighters: Headquarters, Fire Stations, Garages etc., 
	 * thus advanced searching is enabled.
	 */
	@OneToOne
	@JoinColumn(name="ORG_TYPE", nullable=false)
	private OrganisationType organisationType;
	
	/** The organisation. */
	@ManyToOne
	@JoinColumn(name="ORGANISATION_ID")
	private Organisation organisation;

	/**
 * Gets the discipline type.
 *
 * @return the discipline type
 */
public DisciplineType getDisciplineType() {
		return disciplineType;
	}

	/**
	 * Gets the organisation.
	 *
	 * @return the organisation
	 */
	public Organisation getOrganisation() {
		return organisation;
	}

	/**
	 * Sets the organisation.
	 *
	 * @param organisation the new organisation
	 */
	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}

	/**
	 * Sets the discipline type.
	 *
	 * @param disciplineType the new discipline type
	 */
	public void setDisciplineType(DisciplineType disciplineType) {
		this.disciplineType = disciplineType;
	}

	/**
	 * Gets the organisation type.
	 *
	 * @return the organisation type
	 */
	public OrganisationType getOrganisationType() {
		return organisationType;
	}

	/**
	 * Sets the organisation type.
	 *
	 * @param organisationType the new organisation type
	 */
	public void setOrganisationType(OrganisationType organisationType) {
		this.organisationType = organisationType;
	}
	

}
