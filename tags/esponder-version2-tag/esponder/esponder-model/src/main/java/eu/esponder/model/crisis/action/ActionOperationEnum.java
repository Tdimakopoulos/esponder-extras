/*
 * 
 */
package eu.esponder.model.crisis.action;

// TODO: Auto-generated Javadoc
/**
 * The Enum ActionOperationEnum.
 * This class is Enum and define the type of Action Operations
 */
public enum ActionOperationEnum {

	/** The move. */
	MOVE,
	
	/** The transport. */
	TRANSPORT,
	
	/** The fix. */
	FIX
}
