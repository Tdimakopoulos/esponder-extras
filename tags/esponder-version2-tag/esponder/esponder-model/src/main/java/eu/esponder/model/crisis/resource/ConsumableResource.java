/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.model.crisis.resource.category.ConsumableResourceCategory;


// TODO: Auto-generated Javadoc
/**
 * The Class ConsumableResource.
 * Entity class that manage the Consumable Resources for a crisis 
 */
@Entity
@Table(name="consumable_resource")
@NamedQueries({
	@NamedQuery(name="ConsumableResource.findByTitle", query="select cr from ConsumableResource cr where cr.title=:title"),
	@NamedQuery(name="ConsumableResource.findAll", query="select cr from ConsumableResource cr")
})
public class ConsumableResource extends LogisticsResource {

	private static final long serialVersionUID = -7626364119871726847L;

	/** The quantity. */
	@Column(name="QUANTITY")
	private BigDecimal quantity;
	
	/** The container. */
	@ManyToOne
	@JoinColumn(name="REUSABLE_RESOURCE_ID")
	private ReusableResource container;
	
	/** The action part. */
	@ManyToOne
	@JoinColumn(name="ACTION_PART_ID")
	private ActionPart actionPart;

	/** The consumable resource category. */
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private ConsumableResourceCategory consumableResourceCategory;

	/** The crisis context. */
	@ManyToOne
	@JoinColumn(name="CRISIS_CONTEXT_ID")
	private CrisisContext crisisContext;
	
	/**
	 * Gets the crisis context.
	 *
	 * @return the crisis context
	 */
	public CrisisContext getCrisisContext() {
		return crisisContext;
	}

	/**
	 * Sets the crisis context.
	 *
	 * @param crisisContext the new crisis context
	 */
	public void setCrisisContext(CrisisContext crisisContext) {
		this.crisisContext = crisisContext;
	}

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Gets the container.
	 *
	 * @return the container
	 */
	public ReusableResource getContainer() {
		return container;
	}

	/**
	 * Sets the container.
	 *
	 * @param container the new container
	 */
	public void setContainer(ReusableResource container) {
		this.container = container;
	}

	/**
	 * Gets the action part.
	 *
	 * @return the action part
	 */
	public ActionPart getActionPart() {
		return actionPart;
	}

	/**
	 * Sets the action part.
	 *
	 * @param actionPart the new action part
	 */
	public void setActionPart(ActionPart actionPart) {
		this.actionPart = actionPart;
	}

	/**
	 * Gets the consumable resource category.
	 *
	 * @return the consumable resource category
	 */
	public ConsumableResourceCategory getConsumableResourceCategory() {
		return consumableResourceCategory;
	}

	/**
	 * Sets the consumable resource category.
	 *
	 * @param consumableResourceCategory the new consumable resource category
	 */
	public void setConsumableResourceCategory(
			ConsumableResourceCategory consumableResourceCategory) {
		this.consumableResourceCategory = consumableResourceCategory;
	}

}
