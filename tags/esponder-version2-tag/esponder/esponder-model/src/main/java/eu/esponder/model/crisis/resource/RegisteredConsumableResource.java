/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class RegisteredConsumableResource.
 * Entity class that manage information about  Registered Consumable Resource
 * The Consumable Resource before a crisis is Registered Consumable Resource, when  a crisis start a copy of  Registered Consumable Resource is created as Consumable Resource  
 */
@Entity
@Table(name="registered_consumable_resource")
@NamedQueries({
	@NamedQuery(name="RegisteredConsumableResource.findByTitle", query="select rcr from RegisteredConsumableResource rcr where rcr.title=:title"),
	@NamedQuery(name="RegisteredConsumableResource.findAll", query="select rcr from RegisteredConsumableResource rcr")
})
public class RegisteredConsumableResource extends Resource {
	
	private static final long serialVersionUID = -3763207365585553160L;

	/** The quantity. */
	@Column(name="QUANTITY")
	private BigDecimal quantity;
	
	/** The container. */
	@ManyToOne
	@JoinColumn(name="REUSABLE_RESOURCE_ID")
	private RegisteredReusableResource container;
	
	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Gets the container.
	 *
	 * @return the container
	 */
	public RegisteredReusableResource getContainer() {
		return container;
	}

	/**
	 * Sets the container.
	 *
	 * @param container the new container
	 */
	public void setContainer(RegisteredReusableResource container) {
		this.container = container;
	}

	/**
	 * Gets the consumable resource category id.
	 *
	 * @return the consumable resource category id
	 */
	public Long getConsumableResourceCategoryId() {
		return consumableResourceCategoryId;
	}

	/**
	 * Sets the consumable resource category id.
	 *
	 * @param consumableResourceCategoryId the new consumable resource category id
	 */
	public void setConsumableResourceCategoryId(Long consumableResourceCategoryId) {
		this.consumableResourceCategoryId = consumableResourceCategoryId;
	}

	/**
	 * TODO: Fix this to point to OperationsCentreCategory. For now we just keep the correct OperationsCentreCategoryId
	 */
//	@ManyToOne
//	@JoinColumn(name="CATEGORY_ID")
//	private ConsumableResourceCategory consumableResourceCategory;
	@Column(name="CATEGORY_ID")
	private Long consumableResourceCategoryId;
	
}
