/*
 * 
 */
package eu.esponder.model.crisis.resource.category;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.PersonnelCompetence;
import eu.esponder.model.type.RankType;

// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelCategory.
 * Manage information associated with the Personnel category
 */
@Entity
@Table(name="personnel_category")
@NamedQueries({
	@NamedQuery(name="PersonnelCategory.findByType", query="select a from PersonnelCategory a where a.rank=:rankType and a.organisationCategory=:organisationCategory") })
public class PersonnelCategory extends PlannableResourceCategory {

	private static final long serialVersionUID = 8088945215121398846L;

	/**
	 * Models the discipline-specific (RankType) rank, i.e. General, Colonel etc.
	 */
	@OneToOne
	@JoinColumn(name="DISCIPLINE_TYPE_ID")
	private RankType rank;
	
//	@OneToMany(mappedBy="personnelCategory")
//	private Set<Personnel> personnel;
	
	/** The personnel competences. */
	@ManyToMany
	@JoinTable(name="PersonnelCategories_competences",
	joinColumns=@JoinColumn(name="CATEGORY_ID"),
	inverseJoinColumns=@JoinColumn(name="COMPETENCE_ID"))
	private Set<PersonnelCompetence> personnelCompetences;
	
	/** The organisation category. */
	@ManyToOne
	@JoinColumn(name="ORGANISATION_CATEGORY_ID")
	private OrganisationCategory organisationCategory;
	
	/**
	 * Gets the organisation category.
	 *
	 * @return the organisation category
	 */
	public OrganisationCategory getOrganisationCategory() {
		return organisationCategory;
	}

	/**
	 * Sets the organisation category.
	 *
	 * @param organisationCategory the new organisation category
	 */
	public void setOrganisationCategory(OrganisationCategory organisationCategory) {
		this.organisationCategory = organisationCategory;
	}

	/**
	 * Gets the rank.
	 *
	 * @return the rank
	 */
	public RankType getRank() {
		return rank;
	}

	/**
	 * Gets the personnel competences.
	 *
	 * @return the personnel competences
	 */
	public Set<PersonnelCompetence> getPersonnelCompetences() {
		return personnelCompetences;
	}

	/**
	 * Sets the personnel competences.
	 *
	 * @param personnelCompetences the new personnel competences
	 */
	public void setPersonnelCompetences(
			Set<PersonnelCompetence> personnelCompetences) {
		this.personnelCompetences = personnelCompetences;
	}

	/**
	 * Sets the rank.
	 *
	 * @param rank the new rank
	 */
	public void setRank(RankType rank) {
		this.rank = rank;
	}

//	public Set<Personnel> getPersonnel() {
//		return personnel;
//	}
//
//	public void setPersonnel(Set<Personnel> personnel) {
//		this.personnel = personnel;
//	}

}
