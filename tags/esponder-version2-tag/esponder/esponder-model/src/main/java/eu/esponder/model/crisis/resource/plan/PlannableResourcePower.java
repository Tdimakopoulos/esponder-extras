/*
 * 
 */
package eu.esponder.model.crisis.resource.plan;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.category.PlannableResourceCategory;

// TODO: Auto-generated Javadoc
/**
 * The Class PlannableResourcePower.
 */
@Entity
@Table(name="plannable_resource_power")
public class PlannableResourcePower extends ESponderEntity<Long> {
	
	private static final long serialVersionUID = -4062662804969524602L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="RESOURCE_POWER_ID")
	protected Long id;
	
	/** The crisis resource plan. */
	@ManyToOne
	@JoinColumn(name="CRISIS_RESOURCE_PLAN_ID")
	private CrisisResourcePlan crisisResourcePlan;
	
	/** The planable resource category. */
	@OneToOne
	@JoinColumn(name="CATEGORY_ID")
	private PlannableResourceCategory planableResourceCategory;
	
	/** The power. */
	@Column(name="POWER")
	private Integer power;
	
	/** FIXME: Refactor this to an Enum indicating <  -> Less than <= -> Less than or Equal == -> Equal => -> Greater than or Equal >  -> Greater than. */
	@Column(name="CONSTR")
	private String constraint;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the crisis resource plan.
	 *
	 * @return the crisis resource plan
	 */
	public CrisisResourcePlan getCrisisResourcePlan() {
		return crisisResourcePlan;
	}

	/**
	 * Sets the crisis resource plan.
	 *
	 * @param crisisResourcePlan the new crisis resource plan
	 */
	public void setCrisisResourcePlan(CrisisResourcePlan crisisResourcePlan) {
		this.crisisResourcePlan = crisisResourcePlan;
	}

	/**
	 * Gets the planable resource category.
	 *
	 * @return the planable resource category
	 */
	public PlannableResourceCategory getPlanableResourceCategory() {
		return planableResourceCategory;
	}

	/**
	 * Sets the planable resource category.
	 *
	 * @param planableResourceCategory the new planable resource category
	 */
	public void setPlanableResourceCategory(
			PlannableResourceCategory planableResourceCategory) {
		this.planableResourceCategory = planableResourceCategory;
	}

	/**
	 * Gets the power.
	 *
	 * @return the power
	 */
	public Integer getPower() {
		return power;
	}

	/**
	 * Sets the power.
	 *
	 * @param power the new power
	 */
	public void setPower(Integer power) {
		this.power = power;
	}

	/**
	 * Gets the constraint.
	 *
	 * @return the constraint
	 */
	public String getConstraint() {
		return constraint;
	}

	/**
	 * Sets the constraint.
	 *
	 * @param constraint the new constraint
	 */
	public void setConstraint(String constraint) {
		this.constraint = constraint;
	}

}
