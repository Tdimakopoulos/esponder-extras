/*
 * 
 */
package eu.esponder.model.crisis.view;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

// TODO: Auto-generated Javadoc
/**
 * The Class SketchPOI.
 */
@Entity
@DiscriminatorValue("SKETCH")
@NamedQueries({
	@NamedQuery(name="SketchPOI.findByOperationsCentre", query="select s from SketchPOI s where s.operationsCentre.id=:operationsCentreID"),
	@NamedQuery(name="SketchPOI.findBySketchPOIId", query="select s from SketchPOI s where s.id=:sketchPOIId"),
	@NamedQuery(name="SketchPOI.findByTitle", query="select s from SketchPOI s where s.title=:sketchPOITitle")
})
public class SketchPOI extends ResourcePOI {
	
	private static final long serialVersionUID = 3958925970048521465L;

	
	/** The points. */
	@ManyToMany(cascade={CascadeType.PERSIST,CascadeType.MERGE, CascadeType.REMOVE})
	@JoinTable(name="sketch_points",
			joinColumns=@JoinColumn(name="SKETCH_ID"),
			inverseJoinColumns=@JoinColumn(name="POINT_ID"))
	private Set<MapPoint> points;
	
	/** The http url. */
	@Embedded
	private HttpURL httpURL;
	
	/** The sketch type. */
	@Column(name="sketchType")
	private String sketchType;

	/**
	 * Gets the points.
	 *
	 * @return the points
	 */
	public Set<MapPoint> getPoints() {
		return points;
	}

	/**
	 * Sets the points.
	 *
	 * @param points the new points
	 */
	public void setPoints(Set<MapPoint> points) {
		this.points = points;
	}

	/**
	 * Gets the http url.
	 *
	 * @return the http url
	 */
	public HttpURL getHttpURL() {
		return httpURL;
	}

	/**
	 * Sets the http url.
	 *
	 * @param httpURL the new http url
	 */
	public void setHttpURL(HttpURL httpURL) {
		this.httpURL = httpURL;
	}

	/**
	 * Gets the sketch type.
	 *
	 * @return the sketch type
	 */
	public String getSketchType() {
		return sketchType;
	}

	/**
	 * Sets the sketch type.
	 *
	 * @param sketchType the new sketch type
	 */
	public void setSketchType(String sketchType) {
		this.sketchType = sketchType;
	}
	
}
