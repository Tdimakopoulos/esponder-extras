/*
 * 
 */
package eu.esponder.model.snapshot.status;

// TODO: Auto-generated Javadoc
/**
 * The Enum MeasurementStatisticTypeEnum.
 */
public enum MeasurementStatisticTypeEnum {

	/** The mean. */
	MEAN,
	
	/** The stdev. */
	STDEV,

	/** The autocorrelation. */
	AUTOCORRELATION,

	/** The maximum. */
	MAXIMUM,
	
	/** The minimum. */
	MINIMUM,

	/** The first. */
	FIRST,
	
	/** The last. */
	LAST,
	
	/** The median. */
	MEDIAN,
	
	/** The oldest. */
	OLDEST,
	
	/** The newest. */
	NEWEST,
	
	/** The Rate. */
	RATE
	
}
