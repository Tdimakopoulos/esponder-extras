/*
 * 
 */
package eu.esponder.model.snapshot.status;

// TODO: Auto-generated Javadoc
/**
 * The Enum ActionPartSnapshotStatus.
 */
public enum ActionPartSnapshotStatus {
	
	/** The started. */
	STARTED,
	
	/** The paused. */
	PAUSED,
	
	/** The completed. */
	COMPLETED,
	
	/** The canceled. */
	CANCELED
}
