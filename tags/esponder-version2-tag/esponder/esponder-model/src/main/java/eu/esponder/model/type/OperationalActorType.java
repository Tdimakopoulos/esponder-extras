/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class OperationalActorType.
 */
@Entity
@DiscriminatorValue("OP_ACTOR")
public final class OperationalActorType extends ActorType {

	private static final long serialVersionUID = -231655312265592161L;
	
}
