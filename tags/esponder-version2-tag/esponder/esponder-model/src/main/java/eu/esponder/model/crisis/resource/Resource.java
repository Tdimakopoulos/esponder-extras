/*
 * 
 */
package eu.esponder.model.crisis.resource;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
/**
 * The Class Resource.
 * Abstract class for resource, each resource inherit this class
 */
@MappedSuperclass
public abstract class Resource extends ESponderEntity<Long> {

	private static final long serialVersionUID = 351245075327159975L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="RESOURCE_ID")
	protected Long id;
	
	/** The title. Not Null,Unique*/
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	protected String title;

	/** The status. Not Null*/
	@Enumerated(EnumType.STRING)
	@Column(name="RESOURCE_STATUS", nullable=false)
	protected ResourceStatus status;
	
	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ResourceStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ResourceStatus status) {
		this.status = status;
	}

}
