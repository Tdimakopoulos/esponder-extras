/*
 * 
 */
package eu.esponder.model.snapshot.location;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

// TODO: Auto-generated Javadoc
/**
 * The Class Point.
 */
@Embeddable
public class Point implements Serializable {

	private static final long serialVersionUID = 5884204669933882287L;

	/**
	 * Instantiates a new point.
	 */
	public Point() {
	}

	/**
	 * Instantiates a new point.
	 *
	 * @param latitude the latitude
	 * @param longitude the longitude
	 * @param altitude the altitude
	 */
	public Point(BigDecimal latitude, BigDecimal longitude, BigDecimal altitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}

	/** The latitude. Not Null decimal*/
	@Column(name="LATITUDE", nullable=false, columnDefinition="decimal(19,6)")
	private BigDecimal latitude;
	
	/** The longitude. Not Null decimal*/
	@Column(name="LONGITUDE", nullable=false, columnDefinition="decimal(19,6)")
	private BigDecimal longitude;
	
	/** The altitude. decimal can be null*/
	@Column(name="ALTITUDE", columnDefinition="decimal(19,6)")
	private BigDecimal altitude;

	/**
	 * Gets the latitude.
	 *
	 * @return the latitude
	 */
	public BigDecimal getLatitude() {
		return latitude;
	}

	/**
	 * Sets the latitude.
	 *
	 * @param latitude the new latitude
	 */
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	/**
	 * Gets the longitude.
	 *
	 * @return the longitude
	 */
	public BigDecimal getLongitude() {
		return longitude;
	}

	/**
	 * Sets the longitude.
	 *
	 * @param longitude the new longitude
	 */
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	/**
	 * Gets the altitude.
	 *
	 * @return the altitude
	 */
	public BigDecimal getAltitude() {
		return altitude;
	}

	/**
	 * Sets the altitude.
	 *
	 * @param altitude the new altitude
	 */
	public void setAltitude(BigDecimal altitude) {
		this.altitude = altitude;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Point [latitude=" + latitude + ", longitude=" + longitude
				+ ", altitude=" + altitude + "]";
	}
	
}
