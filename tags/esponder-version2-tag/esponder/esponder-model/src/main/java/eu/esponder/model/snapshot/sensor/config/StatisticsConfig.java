/*
 * 
 */
package eu.esponder.model.snapshot.sensor.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class StatisticsConfig.
 */
@Entity
@Table(name="statistics_config")
public class StatisticsConfig extends ESponderEntity<Long> {

	private static final long serialVersionUID = -788744673417526492L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SENSOR_CONFIG_ID")
	private Long id;

	/** The measurement statistic type.Not Null*/
	@Enumerated(EnumType.STRING)
	@Column(name="MEAS_STATISTIC_TYPE", nullable=false)
	private MeasurementStatisticTypeEnum measurementStatisticType;

	/** The sampling period milliseconds.Not Null */
	@Column(name="SAMPLING_PERIOD_MS", nullable=false)
	private long samplingPeriodMilliseconds;
	
	/** The sensor. Not Null*/
	@ManyToOne
	@JoinColumn(name="SENSOR_ID", nullable=false)
	private Sensor sensor;
	
	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the measurement statistic type.
	 *
	 * @return the measurement statistic type
	 */
	public MeasurementStatisticTypeEnum getMeasurementStatisticType() {
		return measurementStatisticType;
	}

	/**
	 * Sets the measurement statistic type.
	 *
	 * @param measurementStatisticType the new measurement statistic type
	 */
	public void setMeasurementStatisticType(MeasurementStatisticTypeEnum measurementStatisticType) {
		this.measurementStatisticType = measurementStatisticType;
	}

	/**
	 * Gets the sampling period milliseconds.
	 *
	 * @return the sampling period milliseconds
	 */
	public long getSamplingPeriodMilliseconds() {
		return samplingPeriodMilliseconds;
	}

	/**
	 * Sets the sampling period milliseconds.
	 *
	 * @param samplingPeriodMilliseconds the new sampling period milliseconds
	 */
	public void setSamplingPeriodMilliseconds(long samplingPeriodMilliseconds) {
		this.samplingPeriodMilliseconds = samplingPeriodMilliseconds;
	}

	/**
	 * Gets the sensor.
	 *
	 * @return the sensor
	 */
	public Sensor getSensor() {
		return sensor;
	}

	/**
	 * Sets the sensor.
	 *
	 * @param sensor the new sensor
	 */
	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[Type : " + measurementStatisticType + ", sampling Period (ms): " + samplingPeriodMilliseconds + " ]";
	}

}
