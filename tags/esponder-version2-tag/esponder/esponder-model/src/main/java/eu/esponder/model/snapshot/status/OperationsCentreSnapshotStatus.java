/*
 * 
 */
package eu.esponder.model.snapshot.status;

// TODO: Auto-generated Javadoc
/**
 * The Enum OperationsCentreSnapshotStatus.
 */
public enum OperationsCentreSnapshotStatus {
	
	/** The stationary. */
	STATIONARY,
	
	/** The moving. */
	MOVING
}
