/*
 * 
 */
package eu.esponder.model.crisis.action;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.ReusableResource;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.location.LocationArea;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionPartObjective.
 * Each action part have a objective or a set of objectives. This entity class manage this objectives
 */
@Entity
@Table(name="action_part_objective")
@NamedQueries({
	@NamedQuery(name="ActionPartObjective.findByTitle", query="select ap from ActionPartObjective ap where ap.title=:title")
})
public class ActionPartObjective extends ESponderEntity<Long> {

	private static final long serialVersionUID = -3441505897036501908L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_PART_OBJECTIVE_ID")
	private Long id;
	
	/** The title. Not NUll, Unique*/
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	/** The period. The period that the objective start and finish*/
	@Embedded
	private Period period;
	
	/** The location area. */
	@ManyToOne
	@JoinColumn(name="LOCATION_AREA_ID", nullable=false)
	private LocationArea locationArea;
	
	/** The action part. */
	@ManyToOne
	@JoinColumn(name="ACTION_PART_ID", nullable=false)
	private ActionPart actionPart;

	
	 
	 
	/** The consumable resources.  These resources are part of the action part's target. For example the "food packages" 
	  in the following sentence: Move these food-packages using a truck*/
	@OneToMany(mappedBy="actionPart")
	private Set<ConsumableResource> consumableResources;

	
	   
	 
	/** The reusable resources. These resources are part of the action part's target. For example the "telecom equipment" 
	   in the following sentence: Move this telecom equipment using a truck*/
	@OneToMany(mappedBy="actionPart")
	private Set<ReusableResource> reusableResources;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the period.
	 *
	 * @return the period
	 */
	public Period getPeriod() {
		return period;
	}

	/**
	 * Sets the period.
	 *
	 * @param period the new period
	 */
	public void setPeriod(Period period) {
		this.period = period;
	}

	/**
	 * Gets the location area.
	 *
	 * @return the location area
	 */
	public LocationArea getLocationArea() {
		return locationArea;
	}

	/**
	 * Sets the location area.
	 *
	 * @param locationArea the new location area
	 */
	public void setLocationArea(LocationArea locationArea) {
		this.locationArea = locationArea;
	}

	/**
	 * Gets the action part.
	 *
	 * @return the action part
	 */
	public ActionPart getActionPart() {
		return actionPart;
	}

	/**
	 * Sets the action part.
	 *
	 * @param actionPart the new action part
	 */
	public void setActionPart(ActionPart actionPart) {
		this.actionPart = actionPart;
	}

	/**
	 * Gets the consumable resources.
	 *
	 * @return the consumable resources
	 */
	public Set<ConsumableResource> getConsumableResources() {
		return consumableResources;
	}

	/**
	 * Sets the consumable resources.
	 *
	 * @param consumableResources the new consumable resources
	 */
	public void setConsumableResources(Set<ConsumableResource> consumableResources) {
		this.consumableResources = consumableResources;
	}

	/**
	 * Gets the reusable resources.
	 *
	 * @return the reusable resources
	 */
	public Set<ReusableResource> getReusableResources() {
		return reusableResources;
	}

	/**
	 * Sets the reusable resources.
	 *
	 * @param reusableResources the new reusable resources
	 */
	public void setReusableResources(Set<ReusableResource> reusableResources) {
		this.reusableResources = reusableResources;
	}

}
