package eu.esponder.jaxb.model.crisis.resource;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.annotate.JsonTypeInfo.As;
import org.codehaus.jackson.annotate.JsonTypeInfo.Id;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.snapshot.resource.ActorSnapshotDTO;

@XmlRootElement(name="actor")
@XmlType(name="Actor")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "equipmentSet", "subordinates", "snapshot", "supervisor"})
@JsonTypeInfo(use=Id.NAME, include=As.WRAPPER_OBJECT)
public class ActorDTO extends ResourceDTO {

	public ActorDTO() {}

	private Set<EquipmentDTO> equipmentSet;
	
	private Set<ActorDTO> subordinates;
	
	private ActorSnapshotDTO snapshot;
	
	private ActorDTO supervisor;

	public Set<EquipmentDTO> getEquipmentSet() {
		return equipmentSet;
	}

	public void setEquipmentSet(Set<EquipmentDTO> equipmentSet) {
		this.equipmentSet = equipmentSet;
	}

	public Set<ActorDTO> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(Set<ActorDTO> subordinates) {
		this.subordinates = subordinates;
	}

	public ActorSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(ActorSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}
	
	public ActorDTO getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(ActorDTO supervisor) {
		this.supervisor = supervisor;
	}
	
	@Override
	public String toString() {
		return "ActorDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "supervisor.id=" + (null==supervisor ? "no supervisor" : supervisor.getId()) +"]";
	}

}
