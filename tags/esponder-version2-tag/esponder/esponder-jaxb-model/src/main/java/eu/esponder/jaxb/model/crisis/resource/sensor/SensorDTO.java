package eu.esponder.jaxb.model.crisis.resource.sensor;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.crisis.resource.ResourceDTO;
import eu.esponder.jaxb.model.snapshot.sensor.config.StatisticsConfigDTO;

@XmlRootElement(name="sensor")
@XmlType(name="Sensor")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "measurementUnit", "configuration"})
public class SensorDTO extends ResourceDTO {
	
	private String name;
	
	private String measurementUnit;
	
	private StatisticsConfigDTO configuration;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(null == obj || obj instanceof SensorDTO)) return false;
		
		SensorDTO sensor = (SensorDTO) obj;
		if (!(sensor.getClass() == SensorDTO.class)) return false;
		return true;
	}
	@Override
	public String toString() {
		return "SensorDTO [name=" + name + ", type=" + type + ", measurementUnit="
				+ measurementUnit + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}

	public StatisticsConfigDTO getConfiguration() {
		return configuration;
	}

	public void setConfiguration(StatisticsConfigDTO configuration) {
		this.configuration = configuration;
	}
	
}
