package eu.esponder.event.model;

import java.io.Serializable;
import java.util.Date;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ResourceDTO;


public abstract class ESponderEvent<T extends ESponderEntityDTO> implements Serializable {

	private static final long serialVersionUID = 3165213086735540677L;
	
	private ResourceDTO eventSource;
	
	private T eventAttachment;
	
	private Date eventTimestamp;
	
	private SeverityLevelDTO eventSeverity;
	
	public ESponderEvent() {
		this.setEventSeverity(SeverityLevelDTO.UNDEFINED);
	}

	public T getEventAttachment() {
		return eventAttachment;
	}

	public ResourceDTO getEventSource() {
		return eventSource;
	}

	public void setEventSource(ResourceDTO eventSource) {
		this.eventSource = eventSource;
	}

	public void setEventAttachment(T eventAttachment) {
		this.eventAttachment = eventAttachment;
	}

	public Date getEventTimestamp() {
		return eventTimestamp;
	}

	public void setEventTimestamp(Date eventTimestamp) {
		this.eventTimestamp = eventTimestamp;
	}

	public SeverityLevelDTO getEventSeverity() {
		return eventSeverity;
	}

	public void setEventSeverity(SeverityLevelDTO eventSeverity) {
		this.eventSeverity = eventSeverity;
	}
	
}

