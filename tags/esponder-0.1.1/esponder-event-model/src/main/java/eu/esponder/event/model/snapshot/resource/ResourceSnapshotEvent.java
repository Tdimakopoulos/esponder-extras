package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.event.model.snapshot.SnapshotEvent;


public class ResourceSnapshotEvent<T extends ResourceDTO> extends SnapshotEvent<T> {

	private static final long serialVersionUID = -6302091892935250120L;
	
}
