package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateOperationsCentreSnapshotEvent extends OperationsCentreSnapshotEvent<OperationsCentreDTO> implements UpdateEvent {

	private static final long serialVersionUID = 819365363519771841L;

}
