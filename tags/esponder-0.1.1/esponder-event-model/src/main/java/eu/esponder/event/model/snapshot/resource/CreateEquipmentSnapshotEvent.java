package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.CreateEvent;


public class CreateEquipmentSnapshotEvent extends EquipmentSnapshotEvent<ESponderEntityDTO> implements CreateEvent {

	private static final long serialVersionUID = 6363754485823085786L;
	
}
