package eu.esponder.event.model.crisis.resource.type;

import eu.esponder.event.model.DeleteEvent;


public class DeleteOperationsCentreTypeEvent extends OperationsCentreTypeEvent implements DeleteEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1039825825796701255L;
	
}
