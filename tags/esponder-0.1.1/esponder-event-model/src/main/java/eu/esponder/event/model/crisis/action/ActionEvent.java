package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.event.model.ESponderEvent;


public abstract class ActionEvent<T extends ActionDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = -345759696434734591L;
	
}
