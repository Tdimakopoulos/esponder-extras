package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.TypeService;
import eu.esponder.model.type.OperationsCentreType;
import eu.esponder.util.ejb.ServiceLocator;


public class OperationsCentreTypeFieldConverter implements CustomConverter {

	protected TypeService getTypeService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Object convert(Object destination, 
			Object source, 
			Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == String.class && source !=null ) {
			String type = (String) source;
			OperationsCentreType operationsCentreType = (OperationsCentreType) getTypeService().findByTitle(type);
			destination = operationsCentreType;
		}
		else if(OperationsCentreType.class.isAssignableFrom(sourceClass)) {
			OperationsCentreType operationsCentreType = (OperationsCentreType) source;
			destination = (String) operationsCentreType.getTitle();
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}