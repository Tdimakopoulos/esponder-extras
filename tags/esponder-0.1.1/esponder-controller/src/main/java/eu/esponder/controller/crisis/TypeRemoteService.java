package eu.esponder.controller.crisis;

import javax.ejb.Remote;

import eu.esponder.dto.model.type.ESponderTypeDTO;

@Remote
public interface TypeRemoteService {
	
	public ESponderTypeDTO findDTOByTitle(String title) throws ClassNotFoundException;

	public ESponderTypeDTO findDTOById(Long typeId) throws ClassNotFoundException;

	public ESponderTypeDTO createRemote(ESponderTypeDTO typeDTO, Long userID) throws ClassNotFoundException;

}
