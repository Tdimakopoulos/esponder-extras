package eu.esponder.controller.crisis.resource;

import java.util.Date;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.snapshot.sensor.config.StatisticsConfig;

@Remote
public interface SensorRemoteService {
	
	public Sensor findById(Long sensorID);
	
	public Sensor findByTitle(String title);
	
	public SensorDTO findByTitleRemote(String title);
	
	public SensorSnapshot findSnapshotByDate(Long sensorID, Date dateTo);
	
	public Sensor create(Sensor sensor, Long userID);
	
	public SensorDTO createRemote(SensorDTO sensor, Long userID);
	
	public SensorSnapshot createSnapshot(SensorSnapshot snapshot, Long userID);

	public SensorSnapshotDTO createSnapshotRemote(SensorSnapshotDTO snapshot, Long userID);

	public void deleteLogical(Sensor sensor, Long userID);

	public Sensor update(Sensor sensor, Long userID);

	public SensorSnapshot findPreviousSnapshot(Long sensorID);

	public SensorSnapshot updateSnapshot(SensorSnapshot snapshot, Long userID);

	public void deleteSnapshot(SensorSnapshot snapshot, Long userID);

	public SensorSnapshot findSnapshotById(Long sensorID);

	public StatisticsConfig findConfigById(Long configID);

	public StatisticsConfig createConfig(StatisticsConfig statisticConfig, Long userID);
	
	public StatisticsConfigDTO createConfigRemote(StatisticsConfigDTO statisticConfig, Long userID);
	
}
