package eu.esponder.controller.crisis.resource.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.LogisticsRemoteService;
import eu.esponder.controller.crisis.resource.LogisticsService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.LogisticsResource;
import eu.esponder.model.crisis.resource.ReusableResource;
import eu.esponder.model.type.ConsumableResourceType;

@Stateless
public class LogisticsBean implements LogisticsService, LogisticsRemoteService {

	@EJB
	private CrudService<LogisticsResource> logisticsCrudService;

	@EJB
	private CrudService<ConsumableResource> consumablesCrudService;

	@EJB
	private CrudService<ReusableResource> reusablesCrudService;

	@EJB
	private TypeService typeService;

	//-------------------------------------------------------------------------

	public LogisticsResource findLogisticsById(Long logisticsID) {
		return (LogisticsResource) logisticsCrudService.find(LogisticsResource.class, logisticsID);
	}

	public ReusableResource findReusableById(Long reusableID) {
		return (ReusableResource) reusablesCrudService.find(ReusableResource.class, reusableID);
	}
	//-------------------------------------------------------------------------

	public ConsumableResource findConsumableById(Long consumableID) {
		return (ConsumableResource) consumablesCrudService.find(ConsumableResource.class, consumableID);
	}

	//-------------------------------------------------------------------------

	@Override
	public ReusableResource findReusableByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ReusableResource) reusablesCrudService.findSingleWithNamedQuery("ReusableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@Override
	public ConsumableResource findConsumableByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ConsumableResource) consumablesCrudService.findSingleWithNamedQuery("ConsumableResource.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ConsumableResource createConsumableResource(ConsumableResource consumableResource, Long userID) {
		consumableResource.getConsumableResourceCategory().setConsumableResourceType((ConsumableResourceType) typeService.findById(consumableResource.getConsumableResourceCategory().getConsumableResourceType().getId()));
		if(null != consumableResource.getContainer())
		{
			consumableResource.setContainer(this.findReusableById(consumableResource.getContainer().getId()));
		}
		consumablesCrudService.create(consumableResource);
		return consumableResource;
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReusableResource createReusableResource(ReusableResource reusableResource, Long userID) {
		reusablesCrudService.create(reusableResource);
		return reusableResource;
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteConsumable(ConsumableResource consumableResource, Long userID) {
		consumablesCrudService.delete(consumableResource);
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteReusable(ReusableResource reusableResource, Long userID) {
		reusablesCrudService.delete(reusableResource);
	}

	//-------------------------------------------------------------------------


	//TODO
	//	Delete Logicals for Consumable and reusable resources
	//	Update Logicals for Consumable and reusable resources

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReusableResource updateReusable(ReusableResource reusableResource, Long userID) {
		return (ReusableResource) reusablesCrudService.update(reusableResource);
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ConsumableResource updateConsumable(ConsumableResource consumableResource, Long userID) {
		return (ConsumableResource) consumablesCrudService.update(consumableResource);
	}


}
