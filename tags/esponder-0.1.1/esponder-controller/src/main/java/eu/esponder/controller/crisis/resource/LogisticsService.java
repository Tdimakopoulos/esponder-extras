package eu.esponder.controller.crisis.resource;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.ReusableResource;

@Local
public interface LogisticsService extends LogisticsRemoteService {
	
	public ReusableResource findReusableById(Long reusableID);

	public ConsumableResource findConsumableById(Long consumableID);

	public ReusableResource findReusableByTitle(String title);

	public ConsumableResource findConsumableByTitle(String title);

	public ConsumableResource createConsumableResource(ConsumableResource consumableResource, Long userID);

	public ReusableResource createReusableResource(ReusableResource reusableResource,Long userID);

	public void deleteConsumable(ConsumableResource consumableResource, Long userID);

	public void deleteReusable(ReusableResource reusableResource, Long userID);

	public ReusableResource updateReusable(ReusableResource reusableResource, Long userID);

	public ConsumableResource updateConsumable(ConsumableResource consumableResource, Long userID);
	
}
