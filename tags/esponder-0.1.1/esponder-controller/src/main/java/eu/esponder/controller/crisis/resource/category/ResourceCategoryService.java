package eu.esponder.controller.crisis.resource.category;

import java.util.Set;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.PersonnelCompetence;
import eu.esponder.model.crisis.resource.category.ConsumableResourceCategory;
import eu.esponder.model.crisis.resource.category.EquipmentCategory;
import eu.esponder.model.crisis.resource.category.OperationsCentreCategory;
import eu.esponder.model.crisis.resource.category.OrganisationCategory;
import eu.esponder.model.crisis.resource.category.PersonnelCategory;
import eu.esponder.model.crisis.resource.category.ResourceCategory;
import eu.esponder.model.crisis.resource.category.ReusableResourceCategory;
import eu.esponder.model.type.ConsumableResourceType;
import eu.esponder.model.type.DisciplineType;
import eu.esponder.model.type.EquipmentType;
import eu.esponder.model.type.OperationsCentreType;
import eu.esponder.model.type.OrganisationType;
import eu.esponder.model.type.RankType;
import eu.esponder.model.type.ReusableResourceType;

@Local
public interface ResourceCategoryService extends ResourceCategoryRemoteService {

	public ResourceCategory create(ResourceCategory resourceCategory, Long UserID) throws ClassNotFoundException;

	public ResourceCategory findById(Class<? extends ResourceCategory> clz, Long resourceCategoryID);

	public OrganisationCategory findByType(DisciplineType disciplineType, OrganisationType organizationType);

	public EquipmentCategory findByType(EquipmentType equipmentType);

	public ConsumableResourceCategory findByType(ConsumableResourceType consumablesType);

	public ReusableResourceCategory findByType(ReusableResourceType reusablesType);

	public OperationsCentreCategory findByType(OperationsCentreType operationsCentreType);

	public PersonnelCategory findByType(RankType rankType, Set<PersonnelCompetence> competence, OrganisationCategory organizationCategory);
	
	/**
	 * @Local updates are performed onto managed entities
	 * nullUpdates 	true: set values to null
	 * 				false: ignore null values
	 */
	

	public OrganisationCategory updateOrganizationCategory(OrganisationCategory organizationCategory, DisciplineType disciplineType, OrganisationType organizationType, Long userId);

	public EquipmentCategory updateEquipmentCategory(EquipmentCategory equipmentCategory, EquipmentType equipmentType, Long userId);

	public ConsumableResourceCategory updateConsumableResourceCategory(ConsumableResourceCategory consumableCategory, ConsumableResourceType consumableType, Long userId);

	public ReusableResourceCategory updateReusableResourceCategory(ReusableResourceCategory reusableCategory, ReusableResourceType reusableType, Long userId);

	public PersonnelCategory updatePersonnelCategory(PersonnelCategory personnelCategory, Set<PersonnelCompetence> competences, RankType rank, OrganisationCategory organizationCategory, Long userId);
}
