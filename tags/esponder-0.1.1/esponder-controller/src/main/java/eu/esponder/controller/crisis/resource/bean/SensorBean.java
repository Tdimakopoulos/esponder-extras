package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.ESponderResourceService;
import eu.esponder.controller.crisis.resource.EquipmentService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.resource.SensorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.crisis.resource.ResourceStatus;
import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.snapshot.sensor.config.StatisticsConfig;
import eu.esponder.model.type.SensorType;

@Stateless
public class SensorBean implements SensorService, SensorRemoteService {

	@EJB
	private CrudService<Sensor> sensorCrudService;

	@EJB
	private CrudService<SensorSnapshot> sensorSnapshotCrudService;

	@EJB
	private CrudService<StatisticsConfig> statisticsCofnigCrudService;

	@EJB
	private TypeService typeService;

	@EJB
	private EquipmentService equipmentService;
	
	@EJB
	private ESponderMappingService eSponderMappingService;
	
	@EJB
	private ESponderResourceService resourceService;

	//-------------------------------------------------------------------------

	@Override
	public Sensor findById(Long sensorID) {
		return (Sensor) resourceService.findByID(Sensor.class, sensorID);
	}

	//-------------------------------------------------------------------------

	@Override
	public StatisticsConfig findConfigById(Long configID) {
		return (StatisticsConfig) statisticsCofnigCrudService.find(StatisticsConfig.class, configID);
	}

	//-------------------------------------------------------------------------

	@Override
	public Sensor findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Sensor) sensorCrudService.findSingleWithNamedQuery("Sensor.findByTitle", params);
	}	

	//-------------------------------------------------------------------------

	@Override
	public SensorSnapshot findSnapshotById(Long sensorID) {
		return (SensorSnapshot) sensorSnapshotCrudService.find(SensorSnapshot.class, sensorID);
	}

	//-------------------------------------------------------------------------

	@Override
	public SensorSnapshot findSnapshotByDate(Long sensorID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sensorID", sensorID);
		params.put("maxDate", maxDate);
		SensorSnapshot snapshot =
				(SensorSnapshot) sensorSnapshotCrudService.findSingleWithNamedQuery("SensorSnapshot.findBySensorAndDate", params);
		return snapshot;
	}
	
	//-------------------------------------------------------------------------

	@Override
	public SensorSnapshot findPreviousSnapshot(Long sensorID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sensorID", sensorID);
		SensorSnapshot snapshot =
				(SensorSnapshot) sensorSnapshotCrudService.findSingleWithNamedQuery("SensorSnapshot.findPreviousSnapshot", params);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Sensor create(Sensor sensor, Long userID) {
		SensorType sensorType = (SensorType)typeService.findById(sensor.getSensorType().getId());
		Equipment equipment = equipmentService.findById(sensor.getEquipment().getId());
		sensor.setSensorType(sensorType);
		sensor.setEquipment(equipment);
		sensor = sensorCrudService.create(sensor);
		return sensor;
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public StatisticsConfig createConfig(StatisticsConfig statisticConfig, Long userID) {
		statisticConfig.setSensor(findById(statisticConfig.getSensor().getId()));
		statisticsCofnigCrudService.create(statisticConfig);
		return statisticConfig;
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteLogical(Sensor sensor, Long userID) {
		sensor.setEquipment(null);
		sensor.setStatus(ResourceStatus.UNAVAILABLE);
		//SensorSnapshots are left as is, because of history
		//and cause sensor is not actually deleted
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Sensor update(Sensor sensor, Long userID) {
		return (Sensor) sensorCrudService.update(sensor);
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SensorSnapshot createSnapshot(SensorSnapshot snapshot, Long userID) {
		snapshot.setSensor((Sensor)this.findById(snapshot.getSensor().getId()));
		SensorSnapshot previous = this.findPreviousSnapshot(snapshot.getSensor().getId());
		if (previous!=null)
			snapshot.setPrevious(previous);
		snapshot = (SensorSnapshot) sensorSnapshotCrudService.create(snapshot);
		//previous.setNext(snapshot);
		if (previous!=null)
			sensorSnapshotCrudService.update(previous);
		return snapshot;
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SensorSnapshot updateSnapshot(SensorSnapshot snapshot, Long userID) {
		return (SensorSnapshot) sensorSnapshotCrudService.update(snapshot); 
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteSnapshot(SensorSnapshot snapshot, Long userID) {
		sensorSnapshotCrudService.delete(snapshot);
	}

	@Override
	public SensorDTO findByTitleRemote(String title) {
		Sensor sensor = this.findByTitle(title);
		try {
			return (SensorDTO) eSponderMappingService.mapESponderEntity(sensor, eSponderMappingService.getDTOEntityClass(sensor.getClass()));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public SensorDTO createRemote(SensorDTO sensorDTO, Long userID) {
		try {
			Sensor sensor = (Sensor) eSponderMappingService.mapESponderEntityDTO(sensorDTO, eSponderMappingService.getManagedEntityClass(sensorDTO.getClass()));
			this.create(sensor, userID);
			sensorDTO = (SensorDTO) eSponderMappingService.mapESponderEntity(sensor, eSponderMappingService.getDTOEntityClass(sensor.getClass()));
			return sensorDTO;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public SensorSnapshotDTO createSnapshotRemote(SensorSnapshotDTO snapshotDTO, Long userID) {
		try {
			SensorSnapshot snapshot = (SensorSnapshot) eSponderMappingService.mapESponderEntityDTO(snapshotDTO, eSponderMappingService.getManagedEntityClass(snapshotDTO.getClass()));
			this.createSnapshot(snapshot, userID);
			snapshotDTO = (SensorSnapshotDTO) eSponderMappingService.mapESponderEntity(snapshot, eSponderMappingService.getDTOEntityClass(snapshot.getClass()));
			return snapshotDTO;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public StatisticsConfigDTO createConfigRemote(StatisticsConfigDTO statisticConfigDTO, Long userID) {
		try {
			StatisticsConfig statisticConfig = (StatisticsConfig) eSponderMappingService.mapESponderEntityDTO(statisticConfigDTO, eSponderMappingService.getManagedEntityClass(statisticConfigDTO.getClass()));
			this.createConfig(statisticConfig, userID);
			statisticConfigDTO = (StatisticsConfigDTO) eSponderMappingService.mapESponderEntity(statisticConfig, eSponderMappingService.getDTOEntityClass(statisticConfig.getClass()));
			return statisticConfigDTO;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
}
