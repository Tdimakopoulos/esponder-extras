package eu.esponder.controller.crisis.resource;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Personnel;

@Local
public interface PersonnelService {

	public Personnel findById(Long personnelID);

	public Personnel findByTitle(String personnelTitle);

	public Personnel createPersonnel(Personnel personnel);

	public Personnel updatePersonnel(Personnel personnel);

	public void deletePersonnel(Personnel personnel);

	public void deletePersonnelByID(Long personnelID);
	
	

}
