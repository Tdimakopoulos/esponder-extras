package eu.esponder.controller.generic;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.criteria.EsponderQueryRestrictionDTO;
import eu.esponder.model.ESponderEntity;

@Remote
public interface GenericRemoteService {

	public List<? extends ESponderEntityDTO> getDTOEntities(
			String queriedDTOClassName,EsponderQueryRestrictionDTO criteriaDTO, int pageSize, int pageNumber) 
					throws InstantiationException, IllegalAccessException, Exception;
	
	public ESponderEntityDTO createEntityDTO(ESponderEntityDTO entity, Long userID) throws ClassNotFoundException;
	
	/***************************************************************************************************************************/
	
	public ESponderEntity<Long> updateEntity(ESponderEntity<Long> entity, Long userID) throws ClassNotFoundException;
	
	public ESponderEntityDTO getEntityDTO( Class<? extends ESponderEntityDTO> clz, Long objectID) throws ClassNotFoundException;
	
	public void deleteEntity(Class<? extends ESponderEntity<Long>> targetClass, Long entityID) throws ClassNotFoundException;
}
