package eu.esponder.controller.crisis.resource.bean;

import java.util.HashMap;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.model.crisis.resource.Personnel;

@Stateless
public class PersonnelBean implements PersonnelService, PersonnelRemoteService {

	@EJB
	private CrudService<Personnel> personnelCrudService;
	
	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	@Override
	public PersonnelDTO findDTOById(Long personnelID) {
		Personnel personnel = findById(personnelID);
		PersonnelDTO personnelDTO = (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
		return  personnelDTO;
	}
	
	@Override
	public Personnel findById(Long personnelID) {
		return (Personnel) personnelCrudService.find(Personnel.class, personnelID);
	}

	//-------------------------------------------------------------------------
	
	@Override
	public PersonnelDTO findDTOByTitle(String personnelTitle) {
		Personnel personnel = findByTitle(personnelTitle);
		PersonnelDTO personnelDTO = (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
		return personnelDTO;
	}
	
	@Override
	public Personnel findByTitle(String personnelTitle) {
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("title", personnelTitle);
		return (Personnel) personnelCrudService.findSingleWithNamedQuery("Personnel.findByTitle", parameters);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public PersonnelDTO createPersonnelDTO(PersonnelDTO personnelDTO) {
		Personnel personnel = (Personnel) mappingService.mapESponderEntityDTO(personnelDTO, Personnel.class);
		personnel = createPersonnel(personnel);
		return (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
	}
	
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Personnel createPersonnel(Personnel personnel) {
		return (Personnel) personnelCrudService.create(personnel);
	}


	//-------------------------------------------------------------------------

	@Override
	public PersonnelDTO updatePersonnelDTO(PersonnelDTO personnelDTO) {
		Personnel personnel = (Personnel) mappingService.mapESponderEntityDTO(personnelDTO, Personnel.class);
		personnel = updatePersonnel(personnel);
		return (PersonnelDTO) mappingService.mapESponderEntity(personnel, PersonnelDTO.class);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Personnel updatePersonnel(Personnel personnel) {
		return (Personnel) personnelCrudService.update(personnel);
	}
	
	//-------------------------------------------------------------------------
	@Override
	public void deletePersonnelDTO(PersonnelDTO personnelDTO) {
		Personnel personnel = (Personnel) mappingService.mapESponderEntityDTO(personnelDTO, Personnel.class);
		deletePersonnel(personnel);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deletePersonnel(Personnel personnel) {
		personnelCrudService.delete(personnel);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deletePersonnelDTOById(Long personnelDTOId) {
		deletePersonnelByID(personnelDTOId);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deletePersonnelByID(Long personnelID) {
		personnelCrudService.delete(Personnel.class, personnelID);
	}

	//-------------------------------------------------------------------------

}
