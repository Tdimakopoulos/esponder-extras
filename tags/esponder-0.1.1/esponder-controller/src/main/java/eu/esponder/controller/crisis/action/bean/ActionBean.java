package eu.esponder.controller.crisis.action.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.action.ActionService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.model.type.ActionType;

@Stateless
public class ActionBean<T> implements ActionService, ActionRemoteService {

	@EJB
	private CrudService<CrisisContext> crisisCrudService;

	@EJB
	private CrudService<Action> actionCrudService;

	@EJB
	private CrudService<ActionPart> actionPartCrudService;

	@EJB
	private TypeService typeService;

	@EJB
	private ESponderMappingService mappingService;


	/*
	 *  TODO ADD "UPDATE" AND "DELETE" @LOCAL AND @REMOTE Interfaces and Bean Implementations
	 */


	// -------------------------------------------------------------------------

	@Override
	public ActionDTO findActionDTOById(Long actionID) {
		Action action = findActionById(actionID);
		return (ActionDTO) mappingService.mapESponderEntity(action, ActionDTO.class);	
	}

	@Override
	public Action findActionById(Long actionID) {
		return (Action) actionCrudService.find(Action.class, actionID);
	}

	// -------------------------------------------------------------------------

	@Override
	public ActionDTO findActionDTOByTitle(String title) {
		Action action = findActionByTitle(title);
		return (ActionDTO) mappingService.mapESponderEntity(action, ActionDTO.class);
	}


	@Override
	public Action findActionByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Action) actionCrudService.findSingleWithNamedQuery(
				"Action.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	@Override
	public ActionPartDTO findActionPartDTOById(Long actionPartID) {
		ActionPart actionPart = findActionPartById(actionPartID);
		return (ActionPartDTO) mappingService.mapESponderEntity(actionPart, ActionPartDTO.class);
	}

	@Override
	public ActionPart findActionPartById(Long actionPartID) {
		return (ActionPart) actionPartCrudService.find(ActionPart.class, actionPartID);
	}

	// -------------------------------------------------------------------------

	@Override
	public ActionPartDTO findActionPartDTOByTitle(String title) {
		ActionPart actionPart = findActionPartByTitle(title);
		return (ActionPartDTO) mappingService.mapESponderEntity(actionPart, ActionPartDTO.class);
	}

	@Override
	public ActionPart findActionPartByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ActionPart) actionPartCrudService.findSingleWithNamedQuery(
				"ActionPart.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextDTO findCrisisContextDTOById(Long actionID) {
		CrisisContext crisisContext = findCrisisContextById(actionID);
		return (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
	}	

	@Override
	public CrisisContext findCrisisContextById(Long actionID) {
		return (CrisisContext) crisisCrudService.find(CrisisContext.class, actionID);
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextDTO findCrisisContextDTOByTitle(String title) {
		CrisisContext crisisContext = findCrisisContextByTitle(title);
		return (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
	}	

	@Override
	public CrisisContext findCrisisContextByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (CrisisContext) crisisCrudService.findSingleWithNamedQuery("CrisisContext.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionDTO createActionRemote(ActionDTO actionDTO, Long userID) {
		Action action = (Action) mappingService.mapESponderEntityDTO(actionDTO, Action.class);
		action = createAction(action, userID);
		return (ActionDTO) mappingService.mapESponderEntity(action, ActionDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Action createAction(Action action, Long userID) {
		ActionType actionType = (ActionType) typeService.findById(action.getActionType().getId());
		action.setActionType((actionType));
		actionCrudService.create(action);
		return action;
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionPartDTO createActionPartRemote(ActionPartDTO actionPartDTO, Long userID) {
		ActionPart actionPart = (ActionPart) mappingService.mapESponderEntityDTO(actionPartDTO, ActionPart.class);
		actionPart = createActionPart(actionPart, userID);
		return (ActionPartDTO) mappingService.mapESponderEntity(actionPart, ActionPartDTO.class); 
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActionPart createActionPart(ActionPart actionPart, Long userID) {
		ActionPart actionPartPersisted = actionPartCrudService.create(actionPart);
		return actionPartPersisted;
	}

	// -------------------------------------------------------------------------

	@Override
	public CrisisContextDTO createCrisisContextRemote(CrisisContextDTO crisisContextDTO, Long userID) {
		CrisisContext crisisContext = (CrisisContext) mappingService.mapESponderEntityDTO(crisisContextDTO, CrisisContext.class);
		crisisContext = createCrisisContext(crisisContext, userID);
		return (CrisisContextDTO) mappingService.mapESponderEntity(crisisContext, CrisisContextDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public CrisisContext createCrisisContext(CrisisContext crisisContext, Long userID) {
		return (CrisisContext) crisisCrudService.create(crisisContext);
	}

	// -------------------------------------------------------------------------

}
