package eu.esponder.controller.crisis.user;

import javax.ejb.Remote;

import eu.esponder.model.user.ESponderUser;

@Remote
public interface UserRemoteService {
	
	public ESponderUser findById(Long userID);
	
	public ESponderUser findByUserName(String userName);
	
	public ESponderUser create(ESponderUser user, Long userID);
	
	public ESponderUser update(ESponderUser user, Long userID);
	
}
