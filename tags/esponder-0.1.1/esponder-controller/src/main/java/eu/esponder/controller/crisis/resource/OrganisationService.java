package eu.esponder.controller.crisis.resource;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Organisation;

@Local
public interface OrganisationService {
	
	public Organisation findById(Long organisationID);
	
	public Organisation findByTitle(String organisationTitle);

	public Organisation createOrganisation(Organisation organisation);

	public Organisation updateOrganisation(Organisation organisation);

	public void deleteOrganisation(Organisation organisation);
	
	public void deleteOrganisationByID(Long organisationID);

}
