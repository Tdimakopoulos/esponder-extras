package eu.esponder.controller.crisis.resource;

import java.util.Date;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.snapshot.resource.EquipmentSnapshot;

@Local
public interface EquipmentService extends EquipmentRemoteService {
	
	public Equipment findById(Long equipmentID);
	
	public Equipment findByTitle(String title);
	
	public EquipmentSnapshot findSnapshotByDate(Long equipmentID, Date dateTo);
	
	public Equipment create(Equipment equipment, Long userID);
	
	public EquipmentSnapshot createSnapshot(EquipmentSnapshot snapshot, Long userID);
	
	public void deleteEquipment(Long equipmentId, Long userID);
	
	public Equipment updateEquipment(Equipment equipment, Long userID);

	public EquipmentSnapshot updateSnapshot(EquipmentSnapshot snapshot, Long userID);

	public void deleteSnapshot(Long equipmentSnapshotId, Long userID);

	
}
