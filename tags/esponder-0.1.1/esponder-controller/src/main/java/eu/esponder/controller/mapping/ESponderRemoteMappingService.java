package eu.esponder.controller.mapping;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.esponder.controller.persistence.criteria.EsponderCriterion;
import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderQueryRestrictionDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.crisis.view.SketchPOI;

@Remote
public interface ESponderRemoteMappingService {

	public List<OperationsCentreDTO> mapUserOperationsCentres(Long userID);
	
	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID);
	
	public OperationsCentreDTO mapOperationsCentre(Long operationsCentreID, Long userID);
	
	
//	public List<SketchPOIDTO> mapOperationsCentreSketches(Long operationsCentreID, Long userID);
	
	public List<ReferencePOIDTO> mapOperationsCentreReferencePOIs(Long operationsCentreID, Long userID);
	
	public OperationsCentreSnapshotDTO mapOperationsCentreSnapshot(Long operationsCentreID, Date maxDate);
	
	public ActorSnapshotDTO mapActorSnapshot(Long actorID, Date maxDate);
	
	public EquipmentSnapshotDTO mapEquipmentSnapshot(Long equipmentID, Date maxDate);
	
	public SensorSnapshotDTO mapLatestSensorSnapshot(Long sensorID, Date maxDate);
	
	public SketchPOI mapSketch(SketchPOIDTO sketchDTO);
	
	public ReferencePOI mapReferencePOI(ReferencePOIDTO referencePOIDTO);
	
	public EsponderQueryRestriction mapCriteriaCollection(EsponderQueryRestrictionDTO criteriaDTO);
	
	public EsponderCriterion mapSimpleCriterion(EsponderCriterionDTO criteria);

	public List<? extends ESponderEntityDTO> mapESponderEntity(List<? extends ESponderEntity<Long>> resultsList, Class<? extends ESponderEntityDTO> targetClass);

	public List<? extends ESponderEntity<?>> mapESponderEntityDTO(List<? extends ESponderEntityDTO> resultsList, Class<? extends ESponderEntity<?>> targetClass);

	public ESponderEntityDTO mapESponderEntity(ESponderEntity<Long> entity, Class<? extends ESponderEntityDTO> targetClass);
	
	public ESponderEntity<?> mapESponderEntityDTO(ESponderEntityDTO entity, Class<? extends ESponderEntity<?>> targetClass);

	public ActorDTO mapActor(Long actorID);

	public ActorDTO mapActor(String actorTitle);

	public ActorDTO updateActor(Actor actor, Long userID);

	public ActorDTO createActor(Actor actor, Long userID);

	public void deleteActor(Long actorID, Long userID);
	
	public ESponderEntityDTO mapGenericCreateEntity(ESponderEntityDTO entityDTO, Long userID) throws ClassNotFoundException;
	
	public ESponderEntityDTO mapGenericUpdateEntity(ESponderEntityDTO entityDTO, Long userID) throws ClassNotFoundException;
	
	public void mapGenericDeleteEntity(String entityClass, Long entityID) throws ClassNotFoundException;

	public List<ActorDTO> mapActorSubordinates(Long actorID);

	public SketchPOIDTO mapSketch(SketchPOI sketch);

	public ReferencePOIDTO mapReferencePOIDTO(ReferencePOI referencePOI);

	public SketchPOIDTO mapSketchById(Long sketchPOIId);

	public SketchPOIDTO mapSketchByTitle(String sketchPOITitle);

	public SketchPOIDTO mapCreateSketch(OperationsCentre opCentre,	SketchPOIDTO sketchDTO, Long userID);

	public Object mapObject(Object obj, Class<?> targetClass);
	
}
