package eu.esponder.controller.generic.bean;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.generic.GenericService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.controller.persistence.criteria.EsponderCriteriaCollection;
import eu.esponder.controller.persistence.criteria.EsponderCriterion;
import eu.esponder.controller.persistence.criteria.EsponderCriterionExpressionEnum;
import eu.esponder.controller.persistence.criteria.EsponderIntersectionCriteriaCollection;
import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.criteria.EsponderCriteriaCollectionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderQueryRestrictionDTO;
import eu.esponder.model.ESponderEntity;

@Stateless
public class GenericBean implements GenericRemoteService, GenericService {
	
	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	@EJB
	private ESponderMappingService mappingService; 
	
	@SuppressWarnings({ "unchecked" })
	public List<? extends ESponderEntity<Long>> getEntities(Class<? extends ESponderEntity<Long>> clz, EsponderQueryRestriction criteria, int pageSize, int pageNumber) 
					throws InstantiationException, IllegalAccessException {
		return (List<ESponderEntity<Long>>) crudService.findWithCriteriaQuery(clz, criteria, pageSize, pageNumber);
	}
	
	
	public EsponderQueryRestriction createCriteriaforBulkFindById(Set<Long> idList) {
		
		EsponderIntersectionCriteriaCollection criteriaCollection = new EsponderIntersectionCriteriaCollection();
		for(Long id : idList) {
			EsponderCriterion criterion = new EsponderCriterion();
			criterion.setExpression(EsponderCriterionExpressionEnum.EQUAL);
			criterion.setField("id");
			criterion.setValue(id);
			criteriaCollection.add(criterion);
		}
		return criteriaCollection;
	}
	
	@Override
	public ESponderEntityDTO getEntityDTO( Class<? extends ESponderEntityDTO> clz, Long objectID) throws ClassNotFoundException {
		Class<? extends ESponderEntity<Long>> targetClass = mappingService.getManagedEntityClass(clz);
		ESponderEntity<Long> entity = getEntity(targetClass, objectID);
		return mappingService.mapESponderEntity(entity, clz);
		
	}
	
	@SuppressWarnings({ "unchecked" })
	public ESponderEntity<Long> getEntity(
			Class<? extends ESponderEntity<Long>> clz, Long objectID) {
		return (ESponderEntity<Long>) crudService.find(clz, objectID);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ESponderEntityDTO createEntityDTO( ESponderEntityDTO entityDTO, Long userID) throws ClassNotFoundException {
		Class<? extends ESponderEntity<Long>> targetClass = mappingService.getManagedEntityClass(entityDTO.getClass());
		ESponderEntity<Long> entity = (ESponderEntity<Long>) mappingService.mapESponderEntityDTO(entityDTO, targetClass);
		entity = createEntity(entity, userID);
		return mappingService.mapESponderEntity(entity, entityDTO.getClass());
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderEntity createEntity(ESponderEntity<Long> entity, Long userID) throws ClassNotFoundException {
		return (ESponderEntity) crudService.create(entity);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderEntity<Long> updateEntity(ESponderEntity<Long> entity, Long userID) throws ClassNotFoundException {
		ESponderEntity<Long> persistedEntity = (ESponderEntity<Long>) crudService.find(entity.getClass(), entity.getId());
		if (null != persistedEntity) {
			this.mappingService.mapEntityToEntity(entity, persistedEntity);
			return (ESponderEntity) crudService.update(persistedEntity);
		}
		return null;
		
	}
	
	/**
	 * TODO: What happens if the delete fails for a reason? An exception should be thrown
	 */
	@SuppressWarnings("unchecked")
	public void deleteEntity(Class<? extends ESponderEntity<Long>> targetClass, Long entityID) {
		crudService.delete(targetClass, entityID);
	}

	@SuppressWarnings("unchecked")
	public List<? extends ESponderEntityDTO> getDTOEntities(
			String queriedDTOClassName,
			EsponderQueryRestrictionDTO criteriaDTO, int pageSize, int pageNumber) throws InstantiationException, IllegalAccessException, Exception {
		
		Class<? extends ESponderEntity<Long>> queriedClass = (Class<? extends ESponderEntity<Long>>) Class.forName(getManagedEntityName(queriedDTOClassName));
		Class<? extends ESponderEntityDTO> queriedDTOClass = (Class<? extends ESponderEntityDTO>) Class.forName(queriedDTOClassName);
		EsponderQueryRestriction criteria = transformCriteria(criteriaDTO, queriedDTOClass.getName());
		if (criteria != null) {
			List<? extends ESponderEntityDTO> resultList = this.mappingService.mapESponderEntity(getEntities(queriedClass, criteria, pageSize, pageNumber), queriedDTOClass);
			return resultList;
		}
		else {
			return null;
		}
	}
	
	private String getManagedEntityName(String queriedEntityDTO) {
		String queriedEntity = queriedEntityDTO.replaceAll("dto.", "");
		queriedEntity = queriedEntity.replaceAll("DTO", "");
		return queriedEntity;
	}
	
	@SuppressWarnings("unchecked")
	private EsponderQueryRestriction transformCriteria(EsponderQueryRestrictionDTO criteriaRestrictionsDTO, String queriedEntityStr) throws Exception {
		Class<? extends ESponderEntity<Long>> queriedClass = (Class<? extends ESponderEntity<Long>>) Class.forName(queriedEntityStr);
		EsponderQueryRestriction criteriaRestrictions = null;

		if (criteriaRestrictionsDTO instanceof EsponderCriteriaCollectionDTO) {
			criteriaRestrictions = mappingService.mapCriteriaCollection(criteriaRestrictionsDTO);
			fixEsponderEntityTypes(queriedClass, criteriaRestrictions);
		}
		else if (criteriaRestrictionsDTO instanceof EsponderCriterionDTO) {
			criteriaRestrictions = mappingService.mapSimpleCriterion((EsponderCriterionDTO) criteriaRestrictionsDTO);
			fixEsponderEntityTypes(queriedClass, criteriaRestrictions);
		}
		else {
			return null;
		}
		return criteriaRestrictions;
	}
	
	@SuppressWarnings("rawtypes")
	private void fixEsponderEntityTypes(Class<? extends ESponderEntity> queriedClass, EsponderQueryRestriction restriction) throws Exception {

		if (restriction instanceof EsponderCriterion) {
			fieldTypeFixes(queriedClass, (EsponderCriterion) restriction);
		} else {
			EsponderCriteriaCollection collection = (EsponderCriteriaCollection) restriction;
			for (EsponderQueryRestriction innerRestriction : collection.getRestrictions()) {
				fixEsponderEntityTypes(queriedClass, innerRestriction);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private void fieldTypeFixes(
			Class<? extends ESponderEntity> queriedClass,
			EsponderCriterion criterion) throws Exception {
		String targetFieldName = ((EsponderCriterion) criterion).getField();
		Field testField = null;
		
		for (Field field : queriedClass.getDeclaredFields()) {
			if (targetFieldName.equals(field.getName())) {
				testField = field;
				break;
			}
		}

		if (testField == null) {
			Field[] arrayGetField = queriedClass.getFields();
			for (Field field : arrayGetField) {
				if (targetFieldName.equals(field.getName())) {
					testField = field;
					break;
				}
			}
		}

		if (testField == null) {
			List<Field> fieldsList = getAllFields(queriedClass);
			if (fieldsList != null) {
				for (Field field : fieldsList) {
					if (targetFieldName.equals(field.getName())) {
						testField = field;
						break;
					}
				}
			}
		}

		if (testField != null) {
			Class<?> fieldClass = testField.getType();
			Constructor<?> constructor = null;
			constructor = fieldClass.getConstructor(String.class);
			criterion.setValue(constructor.newInstance(criterion.getValue().toString()));
		} else {
			throw new Exception("Field not found");
		}
	}

	private List<Field> getAllFields(Class<?> clz) {
		List<Field> fields = new ArrayList<Field>();
		Class<?> superclass = clz.getSuperclass();
		while (superclass !=null) {
			for (Field field : superclass.getFields()) {
				fields.add(field);
			}
			for (Field field : superclass.getDeclaredFields()) {
				fields.add(field);
			}
			superclass = superclass.getSuperclass();
		}
		return fields;
	}

	
}
