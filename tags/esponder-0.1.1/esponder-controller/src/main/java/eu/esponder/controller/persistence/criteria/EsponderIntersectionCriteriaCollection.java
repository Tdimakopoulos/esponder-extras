package eu.esponder.controller.persistence.criteria;

import java.util.Collection;

public class EsponderIntersectionCriteriaCollection extends EsponderCriteriaCollection {

	private static final long serialVersionUID = -447493874297381400L;
	
	public EsponderIntersectionCriteriaCollection() {}
	
	public EsponderIntersectionCriteriaCollection(
			Collection<EsponderQueryRestriction> restrictions) {
		super(restrictions);
	}

}
