package eu.esponder.controller.crisis.resource;

import java.util.Date;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;

@Remote
public interface EquipmentRemoteService {

	public EquipmentDTO findByIdRemote(Long equipmentID);

	public EquipmentDTO findByTitleRemote(String title);

	public EquipmentSnapshotDTO findSnapshotByDateRemote(Long equipmentID, Date maxDate);

	public EquipmentDTO createRemote(EquipmentDTO equipmentDTO, Long userID);

	public EquipmentSnapshotDTO createSnapshotRemote(EquipmentSnapshotDTO snapshotDTO,
			Long userID);

	public void deleteEquipmentRemote(Long equipmentId, Long userID);

	public EquipmentDTO updateEquipmentRemote(EquipmentDTO equipmentDTO, Long userID);

	public EquipmentSnapshotDTO updateSnapshotRemote(EquipmentSnapshotDTO snapshotDTO,
			Long userID);

	public void deleteSnapshotRemote(Long equipmentSnapshotId, Long userID);
	
	
	
}
