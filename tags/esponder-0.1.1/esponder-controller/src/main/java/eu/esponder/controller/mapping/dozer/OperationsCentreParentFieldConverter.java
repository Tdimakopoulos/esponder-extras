package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.util.ejb.ServiceLocator;

public class OperationsCentreParentFieldConverter implements CustomConverter {
	
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if (sourceClass == OperationsCentre.class && source != null) {
			OperationsCentre sourceOC = (OperationsCentre) source;
			OperationsCentreDTO destOperationsCentreDTO = new OperationsCentreDTO();
			destOperationsCentreDTO.setId(sourceOC.getId());
			destination = destOperationsCentreDTO;
		}
		else if(sourceClass == OperationsCentreDTO.class && source!=null) { 
			OperationsCentreDTO sourceOC = (OperationsCentreDTO) source;
			OperationsCentre destOperationsCentre = (OperationsCentre) this.getMappingService().mapESponderEntityDTO((ESponderEntityDTO) sourceOC, (Class<? extends ESponderEntity<Long>>) destinationClass );
			destination = destOperationsCentre;
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
