package eu.esponder.controller.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;
import eu.esponder.dto.model.type.CrisisDisasterTypeDTO;
import eu.esponder.dto.model.type.CrisisFeatureTypeDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.dto.model.type.EmergencyOperationsCentreTypeDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.dto.model.type.MobileEmergencyOperationsCentreTypeDTO;
import eu.esponder.dto.model.type.OperationalActionTypeDTO;
import eu.esponder.dto.model.type.OperationalActorTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.dto.model.type.PersonnelSkillTypeDTO;
import eu.esponder.dto.model.type.PersonnelTrainingTypeDTO;
import eu.esponder.dto.model.type.RankTypeDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;
import eu.esponder.dto.model.type.SensorTypeDTO;
import eu.esponder.dto.model.type.StrategicActionTypeDTO;
import eu.esponder.dto.model.type.TacticalActionTypeDTO;
import eu.esponder.model.snapshot.status.MeasurementUnitEnum;
import eu.esponder.model.type.EmergencyOperationsCentreType;
import eu.esponder.model.type.EquipmentType;
import eu.esponder.model.type.MobileEmergencyOperationsCentreType;
import eu.esponder.model.type.OperationalActorType;
import eu.esponder.model.type.SensorType;
import eu.esponder.model.type.TacticalActorType;


public class TypeServiceTest extends ControllerServiceTest {

	@Test(groups="createBasics")
	public void testCreateTypes() throws ClassNotFoundException {

		// Actors
		TacticalActorType missionCommander = new TacticalActorType();
		missionCommander.setTitle("Mission Commander");
		ESponderTypeDTO typeDTO = (ESponderTypeDTO) mappingService.mapESponderEntity(missionCommander, ESponderTypeDTO.class); 
		typeService.createRemote(typeDTO, this.userID);

		OperationalActorType frc = new OperationalActorType();
		frc.setTitle("FRC");
		typeService.createRemote((ESponderTypeDTO) mappingService.mapESponderEntity(frc, OperationalActorTypeDTO.class), this.userID);

		OperationalActorType fr = new OperationalActorType();
		fr.setTitle("FR");
		typeService.createRemote((ESponderTypeDTO) mappingService.mapESponderEntity(fr, OperationalActorTypeDTO.class), this.userID);

		// Operations Centers 
		EmergencyOperationsCentreType eoc = new EmergencyOperationsCentreType();
		eoc.setTitle("EOC");
		typeService.createRemote((ESponderTypeDTO) mappingService.mapESponderEntity(eoc, EmergencyOperationsCentreTypeDTO.class), this.userID);

		MobileEmergencyOperationsCentreType meoc = new MobileEmergencyOperationsCentreType();
		meoc.setTitle("MEOC");
		typeService.createRemote((ESponderTypeDTO) mappingService.mapESponderEntity(meoc, MobileEmergencyOperationsCentreTypeDTO.class), this.userID);

		// Equipment
		EquipmentType wimax = new EquipmentType();
		wimax.setTitle("FRU WiMAX");
		typeService.createRemote((ESponderTypeDTO) mappingService.mapESponderEntity(wimax, EquipmentTypeDTO.class), this.userID);

		EquipmentType wifi = new EquipmentType();
		wifi.setTitle("FRU WiFi");
		typeService.createRemote((ESponderTypeDTO) mappingService.mapESponderEntity(wifi, EquipmentTypeDTO.class), this.userID);

		// Sensors
		SensorType heartbeat = new SensorType();
		heartbeat.setTitle("HB");
		heartbeat.setMeasurementUnit(MeasurementUnitEnum.BBM);
		typeService.createRemote((ESponderTypeDTO) mappingService.mapESponderEntity(heartbeat, SensorTypeDTO.class), this.userID);

		SensorType temperature = new SensorType();
		temperature.setTitle("TEMP");
		temperature.setMeasurementUnit(MeasurementUnitEnum.DEGREES_CELCIUS);
		typeService.createRemote((ESponderTypeDTO) mappingService.mapESponderEntity(temperature, SensorTypeDTO.class), this.userID);

		// Actions
		StrategicActionTypeDTO strategicActionTypeDTO = new StrategicActionTypeDTO();
		strategicActionTypeDTO.setTitle("StratActionType");
		typeService.createRemote(strategicActionTypeDTO, this.userID);

		TacticalActionTypeDTO tacticalActionTypeDTO = new TacticalActionTypeDTO();
		tacticalActionTypeDTO.setTitle("TactActionType");
		typeService.createRemote(tacticalActionTypeDTO, this.userID);

		OperationalActionTypeDTO opActionTypeDTO = new OperationalActionTypeDTO();
		opActionTypeDTO.setTitle("OpActionType");
		typeService.createRemote(opActionTypeDTO, this.userID);

		// Consumable Resources
		ConsumableResourceTypeDTO crType1 = new ConsumableResourceTypeDTO();
		crType1.setTitle("Drink");
		typeService.createRemote(crType1, this.userID);

		ConsumableResourceTypeDTO crType2 = new ConsumableResourceTypeDTO();
		crType2.setTitle("Food");
		typeService.createRemote(crType2, this.userID);

		ConsumableResourceTypeDTO crType3 = new ConsumableResourceTypeDTO();
		crType3.setTitle("Medical Resource");
		typeService.createRemote(crType3, this.userID);

		// Reusable Resources
		ReusableResourceTypeDTO rrType1 = new ReusableResourceTypeDTO();
		rrType1.setTitle("Water Container");
		typeService.createRemote(rrType1, this.userID);

		ReusableResourceTypeDTO rrType2 = new ReusableResourceTypeDTO();
		rrType2.setTitle("Food package");
		typeService.createRemote(rrType2, this.userID);

		ReusableResourceTypeDTO rrType3 = new ReusableResourceTypeDTO();
		rrType3.setTitle("Medical Kit");
		typeService.createRemote(rrType3, this.userID);

		ReusableResourceTypeDTO rrType4 = new ReusableResourceTypeDTO();
		rrType4.setTitle("Tools");
		typeService.createRemote(rrType4, this.userID);

		//Organisations
		OrganisationTypeDTO organisationTypeDTO1 = new OrganisationTypeDTO();
		organisationTypeDTO1.setTitle("Headquarters");
		typeService.createRemote(organisationTypeDTO1, this.userID);

		OrganisationTypeDTO organisationTypeDTO2 = new OrganisationTypeDTO();
		organisationTypeDTO2.setTitle("Local Station");
		typeService.createRemote(organisationTypeDTO2, this.userID);


		//Disciplines
		DisciplineTypeDTO disciplineTypeDTO1 = new DisciplineTypeDTO();
		disciplineTypeDTO1.setTitle("Police Force");
		typeService.createRemote(disciplineTypeDTO1, this.userID);

		DisciplineTypeDTO disciplineTypeDTO2 = new DisciplineTypeDTO();
		disciplineTypeDTO2.setTitle("Fire Brigade");
		typeService.createRemote(disciplineTypeDTO2, this.userID);

		DisciplineTypeDTO disciplineTypeDTO3 = new DisciplineTypeDTO();
		disciplineTypeDTO3.setTitle("Coast Guard");
		typeService.createRemote(disciplineTypeDTO3, this.userID);

		DisciplineTypeDTO disciplineTypeDTO4 = new DisciplineTypeDTO();
		disciplineTypeDTO4.setTitle("Army");
		typeService.createRemote(disciplineTypeDTO4, this.userID);

		// Rank Types
		RankTypeDTO rankTypeDTO1 = new RankTypeDTO();
		rankTypeDTO1.setTitle("RankType1");
		typeService.createRemote(rankTypeDTO1, this.userID);

		RankTypeDTO rankTypeDTO2 = new RankTypeDTO();
		rankTypeDTO2.setTitle("RankType2");
		typeService.createRemote(rankTypeDTO2, this.userID);

		RankTypeDTO rankTypeDTO3 = new RankTypeDTO();
		rankTypeDTO3.setTitle("RankType3");
		typeService.createRemote(rankTypeDTO3, this.userID);

		// Personnel Skill Types
		PersonnelSkillTypeDTO personnelSkillTypeDTO1 = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO1.setTitle("Fire Truck Driving");
		typeService.createRemote(personnelSkillTypeDTO1, this.userID);

		PersonnelSkillTypeDTO personnelSkillTypeDTO2 = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO2.setTitle("Crane Operation Handling");
		typeService.createRemote(personnelSkillTypeDTO2, this.userID);

		PersonnelSkillTypeDTO personnelSkillTypeDTO3 = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO3.setTitle("Firewall Planning");
		typeService.createRemote(personnelSkillTypeDTO3, this.userID);


		// Personnel Training Types
		PersonnelTrainingTypeDTO personnelTrainingTypeDTO1 = new PersonnelTrainingTypeDTO();
		personnelTrainingTypeDTO1.setTitle("Driving");
		typeService.createRemote(personnelTrainingTypeDTO1, this.userID);

		PersonnelTrainingTypeDTO personnelTrainingTypeDTO2 = new PersonnelTrainingTypeDTO();
		personnelTrainingTypeDTO2.setTitle("Tool Handling");
		typeService.createRemote(personnelTrainingTypeDTO2, this.userID);

		PersonnelTrainingTypeDTO personnelTrainingTypeDTO3 = new PersonnelTrainingTypeDTO();
		personnelTrainingTypeDTO3.setTitle("Administration");
		typeService.createRemote(personnelTrainingTypeDTO3, this.userID);


		// Crisis Disaster Types
		CrisisDisasterTypeDTO crisisDisasterTypeDTO1 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO1.setTitle("Fire");
		typeService.createRemote(crisisDisasterTypeDTO1, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO2 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO2.setTitle("Earthquake");
		typeService.createRemote(crisisDisasterTypeDTO2, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO3 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO3.setTitle("Flood");
		typeService.createRemote(crisisDisasterTypeDTO3, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO4 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO4.setTitle("Road accident");
		typeService.createRemote(crisisDisasterTypeDTO4, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO5 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO5.setTitle("Blizzard");
		typeService.createRemote(crisisDisasterTypeDTO5, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO6 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO6.setTitle("Storm");
		typeService.createRemote(crisisDisasterTypeDTO6, this.userID);

		// Crisis Feature Types
		CrisisFeatureTypeDTO crisisFeatureTypeDTO1 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO1.setTitle("Human Loss");
		typeService.createRemote(crisisFeatureTypeDTO1, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO2 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO2.setTitle("Injury");
		typeService.createRemote(crisisFeatureTypeDTO2, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO3 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO3.setTitle("People hemming");
		typeService.createRemote(crisisFeatureTypeDTO3, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO4 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO4.setTitle("Building damage");
		typeService.createRemote(crisisFeatureTypeDTO4, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO5 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO5.setTitle("Substance leak");
		typeService.createRemote(crisisFeatureTypeDTO5, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO6 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO6.setTitle("Inaccessible city");
		typeService.createRemote(crisisFeatureTypeDTO6, this.userID);

	}

}
