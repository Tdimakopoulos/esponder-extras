package eu.esponder.controller.test.crisis;

import java.math.BigDecimal;
import java.util.HashSet;

import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.category.OperationsCentreCategoryDTO;
import eu.esponder.dto.model.crisis.view.HttpURLDTO;
import eu.esponder.dto.model.crisis.view.MapPointDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.OperationsCentreSnapshotStatusDTO;
import eu.esponder.dto.model.type.OperationsCentreTypeDTO;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.user.ESponderUser;

public class OperationsCentreServiceTest extends ControllerServiceTest {
	
	private static int SECONDS = 30;
	private static double RADIUS = 1;

	@Test(groups="createResources")
	public void testCreateOperationsCentresDTO() throws ClassNotFoundException {
		
		//	Create Operations Centres and place them in hierarchy
		OperationsCentreDTO eocAttica = createOperationsCentreDTO("EOC", "EOC Attica", "sip", "192.168.10.1", "eocAttica");
		OperationsCentreDTO meocAthens = createOperationsCentreDTO("MEOC", "MEOC Athens", "sip", "192.168.10.1", "meocAthens");
		OperationsCentreDTO meocPiraeus = createOperationsCentreDTO("MEOC", "MEOC Piraeus",  "sip", "192.168.10.1", "meocPiraeus");
		
		operationsCentreService.setSupervisor(meocAthens.getId(), eocAttica.getId(), this.userID);
		operationsCentreService.setSupervisor(meocPiraeus.getId(), eocAttica.getId(), this.userID);
		
		// Assign ESponder Users to Operations Centers
		ESponderUser ctri = userService.findByUserName("ctri");
		ESponderUser gleo = userService.findByUserName("gleo");
		ESponderUser kotso = userService.findByUserName("kotso");
		
		HashSet<OperationsCentre> ctriChannels = new HashSet<OperationsCentre>();
		ctriChannels.add((OperationsCentre) mappingService.mapESponderEntityDTO(meocAthens, OperationsCentre.class));
		ctri.setOperationsCentres(ctriChannels);
		userService.update(ctri, this.userID);
		
		HashSet<OperationsCentre> gleoChannels = new HashSet<OperationsCentre>();
		gleoChannels.add((OperationsCentre) mappingService.mapESponderEntityDTO(meocPiraeus, OperationsCentre.class));
		gleo.setOperationsCentres(gleoChannels);
		userService.update(gleo, this.userID);
		
		HashSet<OperationsCentre> kotsoChannels = new HashSet<OperationsCentre>();
		kotsoChannels.add((OperationsCentre) mappingService.mapESponderEntityDTO(eocAttica, OperationsCentre.class));
		kotso.setOperationsCentres(kotsoChannels);
		userService.update(kotso, this.userID);

	}
	
	
//	@Test(groups="createSnapshots")
//	public void testCreateOperationsCentreSnapshotsDTO() {
//		
//		OperationsCentreDTO eocAttica = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("EOC Attica");
//		OperationsCentreDTO meocAthens = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("MEOC Athens");
//		OperationsCentreDTO meocPiraeus = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("MEOC Piraeus");
//
//		PeriodDTO period = this.createPeriodDTO(SECONDS);
//		
//		createOperationsCentreSnapshot(eocAttica, period, this.createSphereDTO(37.939577, 23.829346, null, RADIUS));
//		createOperationsCentreSnapshot(meocAthens, period, this.createSphereDTO(38.023069, 23.78643, null, RADIUS));
//		createOperationsCentreSnapshot(meocPiraeus, period, this.createSphereDTO(37.950623, 23.642406, null, RADIUS));
//	}
	
	
	@Test(groups="createSnapshots")
	public void testCreateOperationsCentreSnapshotsDTO() throws ClassNotFoundException {
		
		OperationsCentreDTO eocAttica = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("EOC Attica");
		OperationsCentreDTO meocAthens = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("MEOC Athens");
		OperationsCentreDTO meocPiraeus = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("MEOC Piraeus");

		PeriodDTO period = this.createPeriodDTO(SECONDS);
		
		createOperationsCentreSnapshot(eocAttica, period, this.createSphereDTO(38.025334, 23.802717, null, RADIUS, "Sphere 01"));
		createOperationsCentreSnapshot(meocAthens, period, this.createSphereDTO(38.025334, 23.802717, null, RADIUS, "Sphere 02"));
		createOperationsCentreSnapshot(meocPiraeus, period, this.createSphereDTO(38.025334, 23.802717, null, RADIUS, "Sphere 03"));
	}
	
	
	/*******************************************************************************************************************************************/
	//FIXME Transfer POI related stuff to a separate test
	
	@Test(groups="createPOIs")
	public void testCreateSketches() {
		
		OperationsCentreDTO meocAthens = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("MEOC Athens");
		OperationsCentreDTO meocPiraeus = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("MEOC Piraeus");
		
		SketchPOIDTO sketch1 = new SketchPOIDTO();
		sketch1.setTitle("Sketch 1");
		sketch1.setPoints(new HashSet<MapPointDTO>());
		sketch1.getPoints().add(createMapPointDTO("point11", 1, new BigDecimal(37.97219), new BigDecimal(23.72617)));
		sketch1.getPoints().add(createMapPointDTO("point12", 1, new BigDecimal(37.97569), new BigDecimal(23.73712))); 	
		sketch1.getPoints().add(createMapPointDTO("point13", 1, new BigDecimal(37.97499), new BigDecimal(23.72154))); 	
		sketch1.setHttpURL(createHttpURLDTO("192.168.1.1:8080", "testpath1", "http"));
		sketch1.setSketchType("SketchType1");
	
		SketchPOIDTO sketch2 = new SketchPOIDTO();
		sketch2.setTitle("Sketch 2");
		sketch2.setPoints(new HashSet<MapPointDTO>());
		sketch2.getPoints().add(createMapPointDTO("point21", 2,  new BigDecimal(37.98825), new BigDecimal(23.73879)));
		sketch2.getPoints().add(createMapPointDTO("point22", 2,  new BigDecimal(37.99265), new BigDecimal(23.73510)));
		sketch2.setHttpURL(createHttpURLDTO("192.168.1.2:8080", "testpath2", "http"));
		sketch2.setSketchType("SketchType2");
		
		operationsCentreService.createSketchPOIRemote(meocAthens, sketch1, this.userID);
		operationsCentreService.createSketchPOIRemote(meocAthens, sketch2, this.userID);
		
		SketchPOIDTO sketch3 = new SketchPOIDTO();
		sketch3.setTitle("Sketch 3");
		sketch3.setPoints(new HashSet<MapPointDTO>());
		sketch3.getPoints().add(createMapPointDTO("point31", 3,  new BigDecimal(37.96288), new BigDecimal(23.91827)));
		sketch3.getPoints().add(createMapPointDTO("point32", 3,  new BigDecimal(37.94718), new BigDecimal(23.96290)));
		sketch3.getPoints().add(createMapPointDTO("point33", 3,  new BigDecimal(37.92145), new BigDecimal(23.94779)));
		sketch3.getPoints().add(createMapPointDTO("point34", 3,  new BigDecimal(37.92416), new BigDecimal(23.92033)));
		sketch3.setHttpURL(createHttpURLDTO("192.168.1.3:8080", "testpath3", "http"));
		sketch3.setSketchType("SketchType3");
		
		operationsCentreService.createSketchPOIRemote(meocPiraeus, sketch3, this.userID);
	}
	
	@Test(groups="createPOIs")
	public void testCreateReferencePOIs() {
		
		OperationsCentreDTO meocAthens = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("MEOC Athens");
		OperationsCentreDTO meocPiraeus = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("MEOC Piraeus");
		
		ReferencePOIDTO ref1 = createReferencePOIDTO("file 1", "first file location", new Float(1.5));
		operationsCentreService.createReferencePOIRemote(meocAthens, ref1, this.userID);
		
		ReferencePOIDTO ref2 = createReferencePOIDTO("file 2", "second file location", new Float(2.5));
		operationsCentreService.createReferencePOIRemote(meocPiraeus, ref2, this.userID);
	}
	
	private OperationsCentreCategoryDTO createOperationsCentreCategoryDTO(String operationsCentreCategoryTitle) throws ClassNotFoundException {
		OperationsCentreCategoryDTO operationsCentreCategoryDTO = new OperationsCentreCategoryDTO();
		OperationsCentreTypeDTO operationsCentreTypeDTO = (OperationsCentreTypeDTO) typeService.findDTOByTitle(operationsCentreCategoryTitle);
		operationsCentreCategoryDTO.setOperationsCentreType(operationsCentreTypeDTO);
		operationsCentreCategoryDTO = (OperationsCentreCategoryDTO) resourceCategoryService.create(operationsCentreCategoryDTO, userID);
		return operationsCentreCategoryDTO;
	}

	/*
	 * TODO: Not Final. Some refactoring missing
	 */
//	@SuppressWarnings("unused")
//	private OperationsCentre createOperationsCentre( OperationsCentreType type, String title, OperationsCentre supervisor ) throws ClassNotFoundException {
//		
//		OperationsCentreCategoryDTO operationsCentreCategoryDTO = createOperationsCentreCategoryDTO("MEOC");
//		
//		OperationsCentreCategory operationCentreCategory = new OperationsCentreCategory();
//		
//		OperationsCentre operationsCentre = new OperationsCentre();
//		
//		operationCentreCategory.setOperationsCentreType(type);
//		operationsCentre.setOperationsCentreCategory(operationCentreCategory);
//		operationsCentre.setTitle(title);
//		operationsCentre.setStatus(ResourceStatus.AVAILABLE);
//		operationsCentre.setSupervisor(supervisor);
//		//FIXME
////		return operationsCentreService.createOperationsCentre(operationsCentre, this.userID);
//		return null;
//	}
	
	
	private OperationsCentreDTO createOperationsCentreDTO(String type, String title, String protocol, String host, String path) throws ClassNotFoundException {
		
		OperationsCentreCategoryDTO operationsCentreCategoryDTO = createOperationsCentreCategoryDTO(type);
		operationsCentreCategoryDTO = (OperationsCentreCategoryDTO) resourceCategoryService.create(operationsCentreCategoryDTO, this.userID); 
		
		OperationsCentreDTO operationsCentreDTO = new OperationsCentreDTO();
		
		operationsCentreDTO.setOperationsCentreCategoryId(operationsCentreCategoryDTO.getId());
		operationsCentreDTO.setTitle(title);
		operationsCentreDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		operationsCentreDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
		return operationsCentreService.createOperationsCentreRemote(operationsCentreDTO, this.userID);
	}
	
	private OperationsCentreSnapshotDTO createOperationsCentreSnapshot (OperationsCentreDTO operationsCentre, PeriodDTO period, LocationAreaDTO locationArea) {
		
		OperationsCentreSnapshotDTO snapshot = new OperationsCentreSnapshotDTO();
		snapshot.setLocationArea(locationArea);
		snapshot.setPeriod(period);
		snapshot.setStatus(OperationsCentreSnapshotStatusDTO.MOVING);
		OperationsCentreSnapshotDTO ocSnapshot = operationsCentreService.createOperationsCentreSnapshotRemote(operationsCentre, snapshot, this.userID);
		return ocSnapshot;
	}
	
	private MapPointDTO createMapPointDTO(String title, Integer icon, BigDecimal latitude, BigDecimal longitude ) {
		MapPointDTO mapPoint = new MapPointDTO();
		mapPoint.setTitle(title);
		mapPoint.setIcon(icon);
		PointDTO point = new PointDTO();
		point.setLongitude(longitude);
		point.setLatitude(latitude);
		mapPoint.setPoint(point);
		return mapPoint;
	}
	
	@SuppressWarnings("unused")
	private ReferencePOI createReferencePOI(String referenceFile, String title) {
		ReferencePOI ref = new ReferencePOI();
		ref.setReferenceFile(referenceFile);
		ref.setTitle(title);
		return ref;
	}
	
	private ReferencePOIDTO createReferencePOIDTO(String referenceFile, String title, Float averageSize) {
		ReferencePOIDTO ref = new ReferencePOIDTO();
		ref.setReferenceFile(referenceFile);
		ref.setTitle(title);
		ref.setAverageSize(averageSize);
		return ref;
	}
	
	private HttpURLDTO createHttpURLDTO(String host, String path, String protocol) {
		HttpURLDTO httpURL = new HttpURLDTO();
		httpURL.setHost(host);
		httpURL.setPath(path);
		httpURL.setProtocol(protocol);
		return httpURL;
	}
		
	private VoIPURLDTO createVoIPURLDTO(String host, String path, String protocol) {
		VoIPURLDTO voipURL = new VoIPURLDTO();
		voipURL.setHost(host);
		voipURL.setPath(path);
		voipURL.setProtocol(protocol);
		return voipURL;
	}
	
}