package eu.esponder.test.rest.cris.poi;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.rest.client.ResteasyClient;
import eu.esponder.util.jaxb.Parser;

public class CreateRefPOIs {

	private String REFPOI_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/view/ref";

	@Test
	public void CreateRefPOIWithXml () throws ClassNotFoundException, Exception {

		Parser parser = new Parser(new Class[] {ReferencePOIDTO.class});
		ReferencePOIDTO refPOIDTO = new ReferencePOIDTO();

		refPOIDTO.setTitle("new Test RefPOI");
		refPOIDTO.setAverageSize(null);
		refPOIDTO.setReferenceFile("test reference file");

		if(refPOIDTO !=null) {

			String serviceName = REFPOI_SERVICE_URI + "/createWithReturn";
			ResteasyClient postClient = new ResteasyClient(serviceName, "application/xml");
			System.out.println("Client for createGenericEntity created successfully...");
			String xmlPayload = parser.marshall(refPOIDTO);

			printXML(xmlPayload);

			Map<String, String> params =  CreateRefServiceParameters(new Long(2));
			String resultXML = postClient.post(params, xmlPayload);

			printXML(resultXML);

			ReferencePOIDTO refDTO = (ReferencePOIDTO) parser.unmarshal(resultXML);

			System.out.println("\n*****************REFPOI**************************");
			System.out.println(refDTO.toString());
			System.out.println("\n************************************************");

		}
	}


	@Test
	public void CreateRefPOIWithJson () throws ClassNotFoundException, Exception {

		ReferencePOIDTO refPOIDTO = new ReferencePOIDTO();

		refPOIDTO.setTitle("new Test RefPOI_2");
		refPOIDTO.setAverageSize(null);
		refPOIDTO.setReferenceFile("test reference file 2");

		if(refPOIDTO !=null) {

			String serviceName = REFPOI_SERVICE_URI + "/createWithReturn";
			ResteasyClient postClient = new ResteasyClient(serviceName, "application/json");
			System.out.println("Client for createGenericEntity created successfully...");
			ObjectMapper mapper = new ObjectMapper();

			String jsonPayload = mapper.writeValueAsString(refPOIDTO);

			printJSON(jsonPayload);

			Map<String, String> params =  CreateRefServiceParameters(new Long(2));
			String resultJSON = postClient.post(params, jsonPayload);

			printJSON(resultJSON);

			ReferencePOIDTO refDTO = mapper.readValue(resultJSON, ReferencePOIDTO.class);

			System.out.println("\n*****************REFPOI**************************");
			System.out.println(refDTO.toString());
			System.out.println("\n************************************************");

		}
	}


	private Map<String, String> CreateRefServiceParameters(Long operationsCentreID) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		params.put("operationsCentreID", operationsCentreID.toString());
		return params;
	}


	private void printJSON(String jsonStr) {
		System.out.println("\n\n******* JSON * START *******");
		System.out.println(jsonStr);
		System.out.println("******** JSON * END ********\n\n");
	}

	private void printXML(String xmlStr) {
		System.out.println("\n\n******* XML * START *******");
		System.out.println(xmlStr);
		System.out.println("******** XML * END ********\n\n");
	}

}
