package eu.esponder.osgi.service.event;

import com.prosyst.mprm.backend.event.EventListenerException;
import com.prosyst.mprm.backend.event.EventService;
import com.prosyst.mprm.common.ManagementException;
import com.prosyst.mprm.data.DictionaryInfo;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.osgi.service.connection.ConnectionHandler;

public class ESponderEventPublisher<T extends ESponderEvent<? extends ESponderEntityDTO>> {
	
	private static ConnectionHandler connectionHandler = new ConnectionHandler();

	private String eventTopic;
	
	private EventService eventService;
	
	private Class<T> eventTopicClass;
	
	public ESponderEventPublisher(Class<T> eventClass) {
		
		this.eventTopicClass = eventClass;
		this.setEventTopic(eventClass.getCanonicalName());
		
		try {
			if (connectionHandler.getRac() == null) {
				connectionHandler.connect();
			}
			eventService = (EventService) connectionHandler.getRac().getService(EventService.class.getName());
		}
		catch (ManagementException me) {
			me.printStackTrace();
		}
	}

	public String getEventTopic() {
		return eventTopic;
	}

	public void setEventTopic(String eventTopic) {
		this.eventTopic = eventTopic;
	}
	
	public void publishEvent(ESponderEvent<? extends ESponderEntityDTO> event) throws EventListenerException {
	      DictionaryInfo  data = new DictionaryInfo();
	      data.put(ConnectionHandler.EVENT_PROPERTY_NAME, event);
	      
	      eventService.event(eventTopic, data);
	      System.out.println("Data have been sent");
	}
	
	public Class<T> getEventTopicClass() {
		return eventTopicClass;
	}
	
	public void setEventTopicClass(Class<T> eventTopicClass) {
		this.eventTopicClass = eventTopicClass;
	}
	
//	public void sendSampleEvent() {
//		try {
//			// create simple event, it will be received from other connected
//			// clients
//			DictionaryInfo data = new DictionaryInfo();
//			data.put("event", createSampleEvent());
//
//			// send the event
//			String eventTopic = CreateActionEvent.class.getName();
//			eventService.event(eventTopic, data);
//			System.out.println("Event Sent: " + data);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

//	private ESponderEvent<? extends ESponderEntityDTO> createSampleEvent() {
//		ActionDTO action = new ActionDTO();
//		action.setTitle("Sample Action");
//		CreateActionEvent result = new CreateActionEvent();
//		result.setEventAttachment(action);
//		return result;
//	}
	
	
}
