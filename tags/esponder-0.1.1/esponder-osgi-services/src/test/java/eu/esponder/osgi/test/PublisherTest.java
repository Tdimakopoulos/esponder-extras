package eu.esponder.osgi.test;

import org.testng.annotations.Test;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActionSnapshotStatusDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;

public class PublisherTest {

	@Test
	public void genericPublisherTest() {
		ESponderEventPublisher<CreateActionSnapshotEvent> publisher = new ESponderEventPublisher<CreateActionSnapshotEvent>(CreateActionSnapshotEvent.class);

		try {
			publisher.publishEvent(createSampleEvent());
		} catch (EventListenerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	public void sendSampleEvent() {
//		try {
//			// create simple event, it will be received from other connected
//			// clients
//			DictionaryInfo data = new DictionaryInfo();
//			data.put("event", createSampleEvent());
//
//			// send the event
//			String eventTopic = CreateActionEvent.class.getName();
//			eventService.event(eventTopic, data);
//			System.out.println("Event Sent: " + data);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private ESponderEvent<? extends ESponderEntityDTO> createSampleEvent() {
		ActionSnapshotDTO actionSnapshot = new ActionSnapshotDTO();
		actionSnapshot.setStatus(ActionSnapshotStatusDTO.STARTED);
		CreateActionSnapshotEvent result = new CreateActionSnapshotEvent();
		result.setEventAttachment(actionSnapshot);
		return result;
	}
}
