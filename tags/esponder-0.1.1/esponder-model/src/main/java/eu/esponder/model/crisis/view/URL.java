package eu.esponder.model.crisis.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class URL implements Serializable {

	private static final long serialVersionUID = 5214691610570185146L;
	
	private static final String separator = "/";
	
	private static final String protocolSeparator = "://";
	
	@Column(name="PROTOCOL")
	private String protocol;
	
	@Column(name="HOST")
	private String hostName;
	
	@Column(name="PATH")
	private String path;
	
	public String getURL() {
		return protocol + protocolSeparator + hostName + separator + path; 
	}
	
	public String getProtocol() {
			return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
			return hostName;
	}

	public void setHost(String host) {
		this.hostName = host;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
