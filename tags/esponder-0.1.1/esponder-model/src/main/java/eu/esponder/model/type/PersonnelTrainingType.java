package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("TRAINING")
public class PersonnelTrainingType extends ESponderType {

	private static final long serialVersionUID = 7268016199798192551L;

}
