package eu.esponder.model.snapshot.status;

public enum ActionSnapshotStatus {
	STARTED,
	PAUSED,
	COMPLETED,
	CANCELED
}
