package eu.esponder.model.snapshot.location;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("SPHERE")
public class Sphere extends LocationArea {
	
	private static final long serialVersionUID = 8670528613518375245L;

	public Sphere() {
		super();
	}

	public Sphere(Point centre, BigDecimal radius) {
		super();
		this.centre = centre;
		this.radius = radius;
	}

	@Embedded
	private Point centre;
	
	@Column(name="RADIUS")
	private BigDecimal radius;

	public Point getCentre() {
		return centre;
	}

	public void setCentre(Point centre) {
		this.centre = centre;
	}

	public BigDecimal getRadius() {
		return radius;
	}

	public void setRadius(BigDecimal radius) {
		this.radius = radius;
	}
	
}
