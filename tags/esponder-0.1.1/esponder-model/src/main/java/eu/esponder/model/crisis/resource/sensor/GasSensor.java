package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("ENV_GAS")
public class GasSensor extends EnvironmentalSensor implements ArithmeticMeasurementSensor {

	private static final long serialVersionUID = 5760021964807956319L;

}
