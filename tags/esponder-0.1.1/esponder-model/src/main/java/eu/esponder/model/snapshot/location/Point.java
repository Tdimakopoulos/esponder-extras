package eu.esponder.model.snapshot.location;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Point implements Serializable {

	private static final long serialVersionUID = 5884204669933882287L;

	public Point() {
	}

	public Point(BigDecimal latitude, BigDecimal longitude, BigDecimal altitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}

	@Column(name="LATITUDE", nullable=false, columnDefinition="decimal(19,6)")
	private BigDecimal latitude;
	
	@Column(name="LONGITUDE", nullable=false, columnDefinition="decimal(19,6)")
	private BigDecimal longitude;
	
	@Column(name="ALTITUDE", columnDefinition="decimal(19,6)")
	private BigDecimal altitude;

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getAltitude() {
		return altitude;
	}

	public void setAltitude(BigDecimal altitude) {
		this.altitude = altitude;
	}

	@Override
	public String toString() {
		return "Point [latitude=" + latitude + ", longitude=" + longitude
				+ ", altitude=" + altitude + "]";
	}
	
}
