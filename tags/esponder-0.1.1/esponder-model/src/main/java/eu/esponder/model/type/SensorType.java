package eu.esponder.model.type;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import eu.esponder.model.snapshot.status.MeasurementUnitEnum;


@Entity
@DiscriminatorValue("SENSOR")
public class SensorType extends ESponderType {

	private static final long serialVersionUID = 2832452320920328627L;
	
	@Column(name="MEASUREMENT_UNIT")
	private MeasurementUnitEnum measurementUnit;
	
	public MeasurementUnitEnum getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(MeasurementUnitEnum measurementUnit) {
		this.measurementUnit = measurementUnit;
	}
	
}
