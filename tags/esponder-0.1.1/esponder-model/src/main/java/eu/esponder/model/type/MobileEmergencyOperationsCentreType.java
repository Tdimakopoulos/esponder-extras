package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("MEOC")
public final class MobileEmergencyOperationsCentreType extends OperationsCentreType {

	private static final long serialVersionUID = -9129311973994216522L;
	
}
