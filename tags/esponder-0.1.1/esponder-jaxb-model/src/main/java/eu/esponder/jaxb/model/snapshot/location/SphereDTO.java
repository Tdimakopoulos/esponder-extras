package eu.esponder.jaxb.model.snapshot.location;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

@XmlRootElement(name="sphere")
@XmlType(name="Sphere")
@JsonPropertyOrder({"centre", "radius"})
public class SphereDTO extends LocationAreaDTO {

	private PointDTO centre;
	
	private BigDecimal radius;

	public PointDTO getCentre() {
		return centre;
	}

	public void setCentre(PointDTO centre) {
		this.centre = centre;
	}

	public BigDecimal getRadius() {
		return radius;
	}

	public void setRadius(BigDecimal radius) {
		this.radius = radius;
	}

	@Override
	public String toString() {
		return "SphereDTO [centre=" + centre + ", radius=" + radius + "]";
	}
	
}
