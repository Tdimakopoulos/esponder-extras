package eu.esponder.rest.validation;

import java.util.Arrays;
import java.util.Set;

import javax.naming.NamingException;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.ValidatorFactory;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.method.MethodConstraintViolation;
import org.hibernate.validator.method.MethodValidator;

import eu.esponder.controller.crisis.user.UserService;
import eu.esponder.model.user.ESponderUser;
import eu.esponder.util.ejb.ServiceLocator;

@Aspect
public class ESponderResourceValidator {
	
	static private ValidatorFactory factory = 
			Validation.byProvider(HibernateValidator.class).configure().buildValidatorFactory();
	
	static private MethodValidator getMethodValidator() {
		return factory.getValidator().unwrap(MethodValidator.class);
	}

	@Pointcut("execution(public * eu.esponder.rest.ESponderResource+.*(..))")
	public final void restfulServicePointcut() { }
	
	/**
	 * Validates the method parameters.
	 */
	@Before(value="restfulServicePointcut()")
	public void before(JoinPoint thisJoinPoint) {
		
		Object[] args = thisJoinPoint.getArgs();
		MethodSignature methodSignature = (MethodSignature)thisJoinPoint.getSignature();
		
		System.out.println("Validating call: " +  methodSignature.getMethod() + " with args " + Arrays.toString(args));
		
		Set<? extends MethodConstraintViolation<?>> validationErrors = 
				getMethodValidator().validateAllParameters(thisJoinPoint.getTarget(), methodSignature.getMethod(), args);
 
		if (!validationErrors.isEmpty()) {
			throw new RuntimeException(buildValidationException(validationErrors));
		}
		
		Long userID = (Long)args[args.length-1];
		ESponderUser user = this.getUserService().findById(userID);
		if (null == user) {
			throw new RuntimeException("User with ID: " + userID + " not found");
		}
	}
	 
 
	/**
	 * @param validationErrors The errors detected in a method/constructor call.
	 * @return A RuntimeException with information about the detected validation errors. 
	 */
	private RuntimeException buildValidationException(Set<? extends MethodConstraintViolation<?>> validationErrors) {
		StringBuilder sb = new StringBuilder();
		for (MethodConstraintViolation<?> cv : validationErrors ) {
			sb.append("\n" + cv.getPropertyPath() + "{" + cv.getInvalidValue() + "} : " + cv.getMessage());
		}
		return new ValidationException(sb.toString());
	}
	
	protected UserService getUserService() {
		try {
			return ServiceLocator.getResource("esponder/UserBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
}