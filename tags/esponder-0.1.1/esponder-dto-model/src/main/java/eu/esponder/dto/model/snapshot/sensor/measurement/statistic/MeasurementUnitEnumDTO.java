package eu.esponder.dto.model.snapshot.sensor.measurement.statistic;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="MeasurementUnit")
public enum MeasurementUnitEnumDTO {
	
	//Temperature
	DEGREES_CELCIUS,
	DEGREES_FAHRENHEIT,
	
	//Time
	SECONDS,
	MILLISECONDS,
	
	//Gas Concentration
	PPM,
	
	//Position (for each of lon, lat, alt)
	DEGREES,
	
	//Hearbeat Rate
	BBM

}
