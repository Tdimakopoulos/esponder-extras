package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.plan.PlannableResourceDTO;
import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;

@XmlRootElement(name="operationsCentre")
@XmlType(name="OperationsCentre")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "operationsCentreCategoryId", "title", "status", "actors","supervisor", "subordinates", "snapshot", "voIPURL"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OperationsCentreDTO extends PlannableResourceDTO {
	
	private static final long serialVersionUID = -1138167138418691550L;

	public OperationsCentreDTO() { }
	
	private Set<ActorDTO> actors;
	
	private OperationsCentreDTO supervisor;
	
	private Set<OperationsCentreDTO> subordinates;
	
	private OperationsCentreSnapshotDTO snapshot;
	
//	private OperationsCentreCategoryDTO operationsCentreCategory;
	
	private Long operationsCentreCategoryId;
	
	private VoIPURLDTO voIPURL;

	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}

	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}
	public Set<ActorDTO> getActors() {
		return actors;
	}
	
	public void setActors(Set<ActorDTO> actors) {
		this.actors = actors;
	}
	
	public Set<OperationsCentreDTO> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(Set<OperationsCentreDTO> subordinates) {
		this.subordinates = subordinates;
	}

	public OperationsCentreSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(OperationsCentreSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

//	public OperationsCentreCategoryDTO getOperationsCentreCategory() {
//		return operationsCentreCategory;
//	}
//
//	public void setOperationsCentreCategory(
//			OperationsCentreCategoryDTO operationsCentreCategory) {
//		this.operationsCentreCategory = operationsCentreCategory;
//	}

	
//	@Override
//	public String toString() {
//		return "OperationsCentreDTO [type=" + type + ", status=" + status
//				+ ", id=" + id + ", title=" + title + "]";
//	}
	
	@Override
	public String toString() {
		return "OperationsCentreDTO [type=" + type + ", status=" + status
				+ ", id=" + id + ", title=" + title + "]";
	}

	public Long getOperationsCentreCategoryId() {
		return operationsCentreCategoryId;
	}

	public void setOperationsCentreCategoryId(Long operationsCentreCategoryId) {
		this.operationsCentreCategoryId = operationsCentreCategoryId;
	}

	public OperationsCentreDTO getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(OperationsCentreDTO supervisor) {
		this.supervisor = supervisor;
	}
	
}
