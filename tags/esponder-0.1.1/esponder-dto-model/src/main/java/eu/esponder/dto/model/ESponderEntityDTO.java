package eu.esponder.dto.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionScheduleCriteriaDTO;
import eu.esponder.dto.model.crisis.action.ActionScheduleDTO;
import eu.esponder.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.crisis.resource.plan.PlannableResourcePowerDTO;
import eu.esponder.dto.model.crisis.view.MapPointDTO;
import eu.esponder.dto.model.crisis.view.ResourcePOIDTO;
import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;


@XmlTransient
@XmlSeeAlso({
	ActionDTO.class,
	ActionObjectiveDTO.class,
	ActionPartDTO.class,
	ActionPartObjectiveDTO.class,
	ActionScheduleCriteriaDTO.class,
	ActionScheduleDTO.class,
	CrisisContextDTO.class,
	ESponderUserDTO.class,
	LocationAreaDTO.class,
	MapPointDTO.class,
	ResourceDTO.class,
	ResourcePOIDTO.class,
	SnapshotDTO.class,
	StatisticsConfigDTO.class,
	ESponderTypeDTO.class,
	SensorMeasurementDTO.class,
	SensorMeasurementEnvelopeDTO.class,
	SensorMeasurementStatisticDTO.class,
	SensorMeasurementStatisticEnvelopeDTO.class,
	CrisisResourcePlanDTO.class,
	PlannableResourcePowerDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ESponderEntityDTO implements Serializable {
	
	private static final long serialVersionUID = -1652821671452383819L;

	protected Long id;

	@JsonIgnore
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}

