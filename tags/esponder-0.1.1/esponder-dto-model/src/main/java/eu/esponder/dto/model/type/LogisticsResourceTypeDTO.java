package eu.esponder.dto.model.type;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@XmlSeeAlso ({
	ConsumableResourceTypeDTO.class,
	ReusableResourceTypeDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class LogisticsResourceTypeDTO extends ESponderTypeDTO {

	private static final long serialVersionUID = 9131656810548564674L;

}
