package eu.esponder.dto.model.criteria;

import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

//@XmlRootElement(name="EsponderUnionCriteriaCollectionDTO")
//@JsonTypeName(value="EsponderUnionCriteriaCollectionDTO")
//@JsonSerialize(as=HashSet.class)
//@JsonDeserialize(contentAs = HashSet.class)
@XmlRootElement(name="EsponderUnionCriteriaCollectionDTO")
@XmlType(name="EsponderUnionCriteriaCollectionDTO")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"restrictions"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EsponderUnionCriteriaCollectionDTO extends EsponderCriteriaCollectionDTO {

	private static final long serialVersionUID = -4963444702454465879L;

	public EsponderUnionCriteriaCollectionDTO() { }
	
	public EsponderUnionCriteriaCollectionDTO(
			Collection<EsponderQueryRestrictionDTO> restrictions) {
		super(restrictions);
	}

}
