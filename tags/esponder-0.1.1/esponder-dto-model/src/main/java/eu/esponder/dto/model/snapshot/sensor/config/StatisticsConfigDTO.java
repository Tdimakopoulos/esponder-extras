package eu.esponder.dto.model.snapshot.sensor.config;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;

@XmlRootElement(name="statisticsConfig")
@XmlType(name="StatisticsConfig")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "measurementStatisticType", "samplingPeriodMilliseconds"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class StatisticsConfigDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = -5641881403083027088L;

	public StatisticsConfigDTO() { }
	
	private MeasurementStatisticTypeEnumDTO measurementStatisticType;
	
	private long samplingPeriodMilliseconds;
	
//	private SensorDTO sensor;
	
	private Long sensorId;
	
	public MeasurementStatisticTypeEnumDTO getMeasurementStatisticType() {
		return measurementStatisticType;
	}

//	public SensorDTO getSensor() {
//		return sensor;
//	}
//
//	public void setSensor(SensorDTO sensor) {
//		this.sensor = sensor;
//	}

	public void setMeasurementStatisticType(
			MeasurementStatisticTypeEnumDTO measurementStatisticType) {
		this.measurementStatisticType = measurementStatisticType;
	}

	public long getSamplingPeriodMilliseconds() {
		return samplingPeriodMilliseconds;
	}

	public void setSamplingPeriodMilliseconds(long samplingPeriodMilliseconds) {
		this.samplingPeriodMilliseconds = samplingPeriodMilliseconds;
	}
	
	public String toString() {
		return "[Type : " + measurementStatisticType + ", sampling Period (ms): " + samplingPeriodMilliseconds + " ]";
	}

	public Long getSensorId() {
		return sensorId;
	}

	public void setSensorId(Long sensorId) {
		this.sensorId = sensorId;
	}
	
}
