package eu.esponder.dto.model.crisis.view;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;

@XmlRootElement(name="mapPoint")
@XmlType(name="MapPoint")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "point", "icon"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class MapPointDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = -1771566126967786747L;

	private String title;

	private PointDTO point;
	
	private Integer icon;
	
	public String getTitle() {
		return title;
	}

	public Integer getIcon() {
		return icon;
	}

	public void setIcon(Integer icon) {
		this.icon = icon;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public PointDTO getPoint() {
		return point;
	}

	public void setPoint(PointDTO point) {
		this.point = point;
	}

}
