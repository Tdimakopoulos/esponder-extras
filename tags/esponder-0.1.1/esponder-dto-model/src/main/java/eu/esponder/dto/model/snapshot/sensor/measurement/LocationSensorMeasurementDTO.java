package eu.esponder.dto.model.snapshot.sensor.measurement;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.location.PointDTO;

@XmlRootElement(name="locationSensorMeasurement")
@XmlType(name="LocationSensorMeasurement")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "sensor", "timestamp", "point"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class LocationSensorMeasurementDTO extends SensorMeasurementDTO {

	private static final long serialVersionUID = -6520366734281109172L;

	private PointDTO point;

	public PointDTO getPoint() {
		return point;
	}

	public void setPoint(PointDTO point) {
		this.point = point;
	}

}
