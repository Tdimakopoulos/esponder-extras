package eu.esponder.dto.model.type;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@XmlSeeAlso ({
	OperationalActionTypeDTO.class,
	StrategicActionTypeDTO.class,
	TacticalActionTypeDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionTypeDTO extends ESponderTypeDTO {

	private static final long serialVersionUID = -4116663244782312272L;
	
}
