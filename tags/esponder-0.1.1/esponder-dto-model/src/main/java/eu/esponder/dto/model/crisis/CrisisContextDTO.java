package eu.esponder.dto.model.crisis;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;

@XmlRootElement(name="crisisContext")
@XmlType(name="CrisisContext")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CrisisContextDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = -7067439377510286686L;

	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String toString() {
		return "CrisisContextDTO [id=" + id + ", title=" + title + "]";
	}
		
}
