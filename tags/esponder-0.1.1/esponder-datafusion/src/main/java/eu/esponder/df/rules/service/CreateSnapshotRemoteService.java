package eu.esponder.df.rules.service;

import eu.esponder.model.snapshot.Snapshot;

public interface CreateSnapshotRemoteService<T extends Snapshot<Long>> {

	public T createSnapshot(T snapshot);
	
}
