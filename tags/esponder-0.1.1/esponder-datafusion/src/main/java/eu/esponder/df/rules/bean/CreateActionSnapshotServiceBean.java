package eu.esponder.df.rules.bean;

import java.util.Date;

import javax.ejb.Stateless;

import eu.esponder.df.rules.service.CreateActionSnapshotService;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.action.ActionSnapshot;

@Stateless
public class CreateActionSnapshotServiceBean extends CreateSnapshotServiceBean<ActionSnapshot> implements CreateActionSnapshotService {

	public ActionSnapshot createActionSnapshot(ActionSnapshot actionSnapshot) {

		/*
		 * Do whatever necessary (process) with the new action snapshot, e.g.
		 * 1. pass from the rule engine
		 * 2. store to the database
		 * 3. ...
		 */
		
		/*
		 * 1. Invoke Rule engine
		 */
		
		Period period = new Period();
		period.setDateTo(new Date());
		actionSnapshot.setPeriod(period);
		
		/*
		 * 2. Store to the database the new snapshot
		 */
		
		return (ActionSnapshot) this.createSnapshot(actionSnapshot);
	}

}
