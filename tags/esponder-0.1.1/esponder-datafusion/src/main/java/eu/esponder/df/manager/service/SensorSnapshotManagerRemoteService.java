package eu.esponder.df.manager.service;

import javax.ejb.Remote;

import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.model.snapshot.resource.SensorSnapshot;

@Remote
public interface SensorSnapshotManagerRemoteService {

	public SensorSnapshot createSensorSnapshot(SensorMeasurementDTO measurement, Long userID);
}
