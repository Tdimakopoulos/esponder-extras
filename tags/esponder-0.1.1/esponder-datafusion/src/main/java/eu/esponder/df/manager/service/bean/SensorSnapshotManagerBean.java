package eu.esponder.df.manager.service.bean;

import javax.ejb.Stateless;
import javax.naming.NamingException;

import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.df.manager.service.SensorSnapshotManagerRemoteService;
import eu.esponder.df.manager.service.SensorSnapshotManagerService;
import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.util.ejb.ServiceLocator;

@Stateless
public class SensorSnapshotManagerBean implements SensorSnapshotManagerService, SensorSnapshotManagerRemoteService {

	public SensorSnapshot createSensorSnapshot(SensorMeasurementDTO measurement, Long userID) {
		System.out.println("Bean Invoked. Ready to invoke controller's beans");
		try {
			SensorRemoteService sensorService = ServiceLocator.getResource("esponder/SensorRemoteService/remote");
			sensorService.createSnapshot(createSensorSnapshot(measurement), userID);
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private SensorSnapshot createSensorSnapshot(SensorMeasurementDTO measurement) {
		return null;
	}

}
