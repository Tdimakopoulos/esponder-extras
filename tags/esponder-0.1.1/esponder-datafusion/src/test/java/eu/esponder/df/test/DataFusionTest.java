package eu.esponder.df.test;

import org.testng.annotations.BeforeClass;

import eu.esponder.controller.crisis.user.UserService;
import eu.esponder.df.manager.service.SensorSnapshotManagerRemoteService;
import eu.esponder.model.user.ESponderUser;
import eu.esponder.test.ResourceLocator;

public class DataFusionTest {

	protected static final String USER_NAME = "ctri";
	protected Long userID;
	
	protected SensorSnapshotManagerRemoteService sensorSnapshotService;
	
	protected UserService userService;
	
	@BeforeClass(alwaysRun = true)
	public void before() {
	
		sensorSnapshotService = ResourceLocator.lookup("esponder/SensorSnapshotManagerBean/remote");
		userService = ResourceLocator.lookup("esponder/UserBean/remote");
		
		ESponderUser user = userService.findByUserName(USER_NAME);
		if (null != user) {
			userID = user.getId();
		}
	}

}
