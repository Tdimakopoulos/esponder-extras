package eu.esponder.df.rules.service;

import javax.ejb.Remote;

import eu.esponder.model.snapshot.action.ActionSnapshot;

@Remote
public interface CreateActionSnapshotRemoteService extends CreateSnapshotRemoteService<ActionSnapshot> {

	public ActionSnapshot createActionSnapshot(ActionSnapshot actionSnapshot);
}
