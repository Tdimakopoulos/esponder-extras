package eu.esponder.df.rules.bean;

import javax.ejb.EJB;

import eu.esponder.controller.persistence.CrudService;
import eu.esponder.df.rules.service.CreateSnapshotService;
import eu.esponder.model.snapshot.Snapshot;

public abstract class CreateSnapshotServiceBean<T extends Snapshot<Long>> implements CreateSnapshotService<T> {

	@EJB
	CrudService<Snapshot<Long>> crudService;
	
	@SuppressWarnings("unchecked")
	public T createSnapshot(T snapshot) {
		/*
		 * Call backend services to store Snapshot to the database
		 */
		return (T) crudService.create(snapshot);
	}

}
