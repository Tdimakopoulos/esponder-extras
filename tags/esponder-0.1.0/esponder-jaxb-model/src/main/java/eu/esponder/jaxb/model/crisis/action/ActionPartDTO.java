package eu.esponder.jaxb.model.crisis.action;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.jaxb.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.jaxb.model.snapshot.action.ActionPartSnapshotDTO;


@XmlRootElement(name="actionPart")
@XmlType(name="ActionPart")
@JsonSerialize(include=Inclusion.NON_NULL)
public class ActionPartDTO extends ESponderEntityDTO {
	
	private String title;
	
	private ActionPartSnapshotDTO snapshot;
	
	private Set<ActionPartObjectiveDTO> actionPartObjectives;
	
	private Set<ConsumableResourceDTO> usedConsumableResources;
	
	private Set<ReusableResourceDTO> usedReusuableResources;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ActionPartSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(ActionPartSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	public Set<ActionPartObjectiveDTO> getActionPartObjectives() {
		return actionPartObjectives;
	}

	public void setActionPartObjectives(Set<ActionPartObjectiveDTO> actionPartObjectives) {
		this.actionPartObjectives = actionPartObjectives;
	}

	public Set<ConsumableResourceDTO> getUsedConsumableResources() {
		return usedConsumableResources;
	}

	public void setUsedConsumableResources(Set<ConsumableResourceDTO> usedConsumableResources) {
		this.usedConsumableResources = usedConsumableResources;
	}

	public Set<ReusableResourceDTO> getUsedReusuableResources() {
		return usedReusuableResources;
	}

	public void setUsedReusuableResources(Set<ReusableResourceDTO> usedReusuableResources) {
		this.usedReusuableResources = usedReusuableResources;
	}

}
