package eu.esponder.jaxb.model.crisis.view;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

@XmlRootElement(name="sketch")
@XmlType(name="Sketch")
@JsonPropertyOrder({"title", "points"})
public class SketchPOIDTO extends ResourcePOIDTO {	
	
	private Set<MapPointDTO> points;

	public Set<MapPointDTO> getPoints() {
		return points;
	}

	public void setPoints(Set<MapPointDTO> points) {
		this.points = points;
	}	
	
}
