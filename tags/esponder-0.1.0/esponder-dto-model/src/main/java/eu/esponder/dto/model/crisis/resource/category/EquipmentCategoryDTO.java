package eu.esponder.dto.model.crisis.resource.category;

import java.util.Set;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;

public class EquipmentCategoryDTO extends PlannableResourceCategoryDTO {

	private static final long serialVersionUID = -4964121325750331976L;

	private EquipmentTypeDTO equipmentType;
	
	private Set<EquipmentDTO> equipment;

	public EquipmentTypeDTO getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(EquipmentTypeDTO equipmentType) {
		this.equipmentType = equipmentType;
	}

	public Set<EquipmentDTO> getEquipment() {
		return equipment;
	}

	public void setEquipment(Set<EquipmentDTO> equipment) {
		this.equipment = equipment;
	}
	
}
