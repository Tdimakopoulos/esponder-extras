package eu.esponder.dto.model.crisis.resource.category;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.type.OperationsCentreTypeDTO;

@XmlRootElement(name="operationCentreCategory")
@XmlType(name="OperationsCentreCategory")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"operationsCentreType", "operationsCentres"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OperationsCentreCategoryDTO extends PlannableResourceCategoryDTO {

	private static final long serialVersionUID = 2966454872810486900L;

	public OperationsCentreCategoryDTO() { }
	
	private OperationsCentreTypeDTO operationsCentreType;
	
	private Set<OperationsCentreDTO> operationsCentres;
	
	public OperationsCentreTypeDTO getOperationsCentreType() {
		return operationsCentreType;
	}

	public void setOperationsCentreType(OperationsCentreTypeDTO operationsCentreType) {
		this.operationsCentreType = operationsCentreType;
	}

	public Set<OperationsCentreDTO> getOperationsCentres() {
		return operationsCentres;
	}

	public void setOperationsCentres(Set<OperationsCentreDTO> operationsCentres) {
		this.operationsCentres = operationsCentres;
	}
}
