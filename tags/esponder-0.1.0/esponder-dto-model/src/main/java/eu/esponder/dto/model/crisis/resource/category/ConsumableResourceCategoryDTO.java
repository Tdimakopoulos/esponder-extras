package eu.esponder.dto.model.crisis.resource.category;

import java.util.Set;

import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;

public class ConsumableResourceCategoryDTO extends LogisticsCategoryDTO {

	private static final long serialVersionUID = 8169469202498003577L;

	private ConsumableResourceTypeDTO consumableResourceType;

	private Set<ConsumableResourceDTO> consumableResources;

	public ConsumableResourceTypeDTO getConsumableResourceType() {
		return consumableResourceType;
	}

	public void setConsumableResourceType(
			ConsumableResourceTypeDTO consumableResourceType) {
		this.consumableResourceType = consumableResourceType;
	}

	public Set<ConsumableResourceDTO> getConsumableResources() {
		return consumableResources;
	}

	public void setConsumableResources(
			Set<ConsumableResourceDTO> consumableResources) {
		this.consumableResources = consumableResources;
	}

}
