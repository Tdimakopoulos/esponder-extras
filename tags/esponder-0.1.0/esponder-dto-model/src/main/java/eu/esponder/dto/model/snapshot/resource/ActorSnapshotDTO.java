package eu.esponder.dto.model.snapshot.resource;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActorSnapshotStatusDTO;

@XmlRootElement(name="actorSnapshot")
@XmlType(name="ActorSnapshot")
@JsonPropertyOrder({"id", "status", "locationArea", "period"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActorSnapshotDTO extends SpatialSnapshotDTO {
	
	private static final long serialVersionUID = -4264419745021840922L;

	public ActorSnapshotDTO() { }
	
	private ActorSnapshotStatusDTO status;
	
	private ActorDTO actor;

	public ActorSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActorSnapshotStatusDTO status) {
		this.status = status;
	}

	public ActorDTO getActor() {
		return actor;
	}

	public void setActor(ActorDTO actor) {
		this.actor = actor;
	}
	@Override
	public String toString() {
		return "ActorSnapshotDTO [status=" + status + ", locationArea=" + locationArea + ", id=" + id  + "]";
	}
	
}
