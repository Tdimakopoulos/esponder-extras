package eu.esponder.dto.model.type;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@XmlSeeAlso ({
	CrisisDisasterTypeDTO.class,
	CrisisFeatureTypeDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CrisisTypeDTO extends ESponderTypeDTO {

	private static final long serialVersionUID = -1697829503734490391L;

}
