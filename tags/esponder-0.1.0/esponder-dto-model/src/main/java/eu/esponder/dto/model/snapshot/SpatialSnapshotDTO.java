package eu.esponder.dto.model.snapshot;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;

@XmlSeeAlso({
	ActorSnapshotDTO.class,
	ActionSnapshotDTO.class,
	ActionPartSnapshotDTO.class,
	OperationsCentreSnapshotDTO.class,
	CrisisContextSnapshotDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class SpatialSnapshotDTO extends SnapshotDTO {
	
	private static final long serialVersionUID = -8816608444526837797L;
	
	protected LocationAreaDTO locationArea;

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

	@Override
	public String toString() {
		return "SpatialSnapshotDTO [locationArea=" + locationArea + ", id=" + id + "]";
	}

}
