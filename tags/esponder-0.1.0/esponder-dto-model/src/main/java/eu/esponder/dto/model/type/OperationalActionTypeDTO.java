package eu.esponder.dto.model.type;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@XmlRootElement(name="operationalActionType")
@XmlType(name="OperationalActionType")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "parent", "children"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OperationalActionTypeDTO extends ActionTypeDTO {
	
	private static final long serialVersionUID = -8445886879200324066L;
	
	private TacticalActionTypeDTO parent;
	
	public TacticalActionTypeDTO getParent() {
		return parent;
	}

	public void setParent(TacticalActionTypeDTO parent) {
		this.parent = parent;
	}
	
	

}
