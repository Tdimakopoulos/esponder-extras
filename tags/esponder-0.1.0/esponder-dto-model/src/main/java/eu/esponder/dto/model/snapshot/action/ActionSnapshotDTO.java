package eu.esponder.dto.model.snapshot.action;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActionSnapshotStatusDTO;

@XmlRootElement(name="actionSnapshot")
@XmlType(name="ActionSnapshot")
@JsonPropertyOrder({"id", "status","action", "locationArea", "period"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionSnapshotDTO extends SpatialSnapshotDTO {
	
	private static final long serialVersionUID = -6768351283306416107L;

	public ActionSnapshotDTO() { }
	
	private ActionDTO action;
	
	private ActionSnapshotStatusDTO status;
	
	public ActionSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActionSnapshotStatusDTO status) {
		this.status = status;
	}

	public ActionDTO getAction() {
		return action;
	}

	public void setAction(ActionDTO action) {
		this.action = action;
	}

}
