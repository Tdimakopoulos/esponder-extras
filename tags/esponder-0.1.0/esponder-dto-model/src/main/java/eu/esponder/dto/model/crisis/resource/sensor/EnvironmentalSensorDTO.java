package eu.esponder.dto.model.crisis.resource.sensor;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@XmlSeeAlso({ EnvironmentTemperatureSensorDTO.class, GasSensorDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class EnvironmentalSensorDTO extends SensorDTO {

	private static final long serialVersionUID = -3878737094263093312L;

}
