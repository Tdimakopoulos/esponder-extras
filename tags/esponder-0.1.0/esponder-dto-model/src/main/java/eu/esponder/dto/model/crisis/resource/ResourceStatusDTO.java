package eu.esponder.dto.model.crisis.resource;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="ResourceStatus")
public enum ResourceStatusDTO {
	AVAILABLE,
	UNAVAILABLE,
	RESERVED,
	ALLOCATED
}
