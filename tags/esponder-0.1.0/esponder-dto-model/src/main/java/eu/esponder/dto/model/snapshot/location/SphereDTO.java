package eu.esponder.dto.model.snapshot.location;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@XmlRootElement(name="sphere")
@XmlType(name="Sphere")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "centre", "radius"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SphereDTO extends LocationAreaDTO {

	private static final long serialVersionUID = -9091311951839192515L;

	public SphereDTO() { }
	
	public SphereDTO(PointDTO centre, BigDecimal radius, String title) {
		super();
		this.centre = centre;
		this.radius = radius;
		this.setTitle(title);
	}
	
	private PointDTO centre;
	
	private BigDecimal radius;

	public PointDTO getCentre() {
		return centre;
	}

	public void setCentre(PointDTO centre) {
		this.centre = centre;
	}

	public BigDecimal getRadius() {
		return radius;
	}

	public void setRadius(BigDecimal radius) {
		this.radius = radius;
	}

	@Override
	public String toString() {
		return "SphereDTO [centre=" + centre + ", radius=" + radius + "]";
	}
	
}
