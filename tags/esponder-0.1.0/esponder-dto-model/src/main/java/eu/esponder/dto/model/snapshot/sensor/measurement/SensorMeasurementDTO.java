package eu.esponder.dto.model.snapshot.sensor.measurement;

import java.util.Date;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
@XmlSeeAlso({
	ArithmeticSensorMeasurementDTO.class,
	LocationSensorMeasurementDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class SensorMeasurementDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = -6461648755883151217L;

	private SensorDTO sensor;
	
	private Date timestamp;

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public SensorDTO getSensor() {
		return sensor;
	}

	public void setSensor(SensorDTO sensor) {
		this.sensor = sensor;
	}


}
