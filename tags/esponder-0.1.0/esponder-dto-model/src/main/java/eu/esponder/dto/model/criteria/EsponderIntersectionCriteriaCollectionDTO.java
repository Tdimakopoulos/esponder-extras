package eu.esponder.dto.model.criteria;

import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@XmlRootElement(name="EsponderIntersectionCriteriaCollectionDTO")
@XmlType(name="EsponderIntersectionCriteriaCollectionDTO")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"restrictions"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EsponderIntersectionCriteriaCollectionDTO extends EsponderCriteriaCollectionDTO {

	private static final long serialVersionUID = -4395158116445919854L;
	
	public EsponderIntersectionCriteriaCollectionDTO() { }
		
	public EsponderIntersectionCriteriaCollectionDTO (Collection<EsponderQueryRestrictionDTO> restrictions) {		
		super(restrictions);
	}

}
