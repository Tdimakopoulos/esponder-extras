package eu.esponder.dto.model.snapshot.sensor.measurement.statistic;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="MeasurementStatistic")
public enum MeasurementStatisticTypeEnumDTO {

	MEAN,
	STDEV,

	AUTOCORRELATION,

	MAXIMUM,
	MINIMUM,

	FIRST,
	LAST,
	MEDIAN,
	
	OLDEST,
	NEWEST
	
}
