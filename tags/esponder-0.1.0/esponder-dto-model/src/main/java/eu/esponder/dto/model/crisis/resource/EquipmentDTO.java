package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.plan.PlannableResourceDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;

@XmlRootElement(name="equipment")
@XmlType(name="Equipment")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "sensors", "snapshot"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EquipmentDTO extends PlannableResourceDTO {
	
	private static final long serialVersionUID = -3973978134313792671L;

	public EquipmentDTO() { }
	
	private Set<SensorDTO> sensors;
	
	private  EquipmentSnapshotDTO snapshot;
	
//	private EquipmentCategoryDTO equipmentCategory;
	
	private Long equipmentCategoryId;
	
	private ActorDTO actor;

	public ActorDTO getActor() {
		return actor;
	}

	public void setActor(ActorDTO actor) {
		this.actor = actor;
	}

	public Set<SensorDTO> getSensors() {
		return sensors;
	}

//	public EquipmentCategoryDTO getEquipmentCategory() {
//		return equipmentCategory;
//	}
//
//	public void setEquipmentCategory(EquipmentCategoryDTO equipmentCategory) {
//		this.equipmentCategory = equipmentCategory;
//	}

	public void setSensors(Set<SensorDTO> sensors) {
		this.sensors = sensors;
	}

	public EquipmentSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(EquipmentSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	@Override
	public String toString() {
		return "EquipmentDTO [type=" + type + ", status=" + status + ", id="
				+ id + ", title=" + title + "]";
	}

	public Long getEquipmentCategoryId() {
		return equipmentCategoryId;
	}

	public void setEquipmentCategoryId(Long equipmentCategoryId) {
		this.equipmentCategoryId = equipmentCategoryId;
	}
	
}
