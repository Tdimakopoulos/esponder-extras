package eu.esponder.dto.model.crisis.resource.sensor;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@XmlSeeAlso({ ActivitySensorDTO.class,
	BodyTemperatureSensorDTO.class,
	BreathRateSensorDTO.class,
	HeartBeatRateSensorDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class BiomedicalSensorDTO extends SensorDTO {

	private static final long serialVersionUID = -4394240267122716480L;

}
