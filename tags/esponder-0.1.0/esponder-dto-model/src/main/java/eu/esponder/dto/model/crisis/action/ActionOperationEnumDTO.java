package eu.esponder.dto.model.crisis.action;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="ActionOperation")
public enum ActionOperationEnumDTO {

	MOVE,
	TRANSPORT,
	FIX
}
