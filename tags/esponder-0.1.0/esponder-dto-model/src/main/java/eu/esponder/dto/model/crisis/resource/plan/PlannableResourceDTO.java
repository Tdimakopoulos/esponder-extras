package eu.esponder.dto.model.crisis.resource.plan;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.LogisticsResourceDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.ResourceDTO;

@XmlSeeAlso({
	EquipmentDTO.class,
	LogisticsResourceDTO.class,
	OperationsCentreDTO.class,
	PersonnelDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class PlannableResourceDTO extends ResourceDTO {

	private static final long serialVersionUID = -650765106596246416L;

}
