package eu.esponder.dto.model.crisis.resource.category;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;

@XmlSeeAlso({
	PlannableResourceCategoryDTO.class,
	OrganisationCategoryDTO.class
})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ResourceCategoryDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = -429348562225638140L;
	
}
