package eu.esponder.dto.model.criteria;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonTypeInfo;

//@XmlTransient
//@XmlSeeAlso({ EsponderIntersectionCriteriaCollectionDTO.class,
//		EsponderUnionCriteriaCollectionDTO.class,
//		EsponderNegationCriteriaCollectionDTO.class, EsponderCriterionDTO.class })
//@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@XmlTransient
@XmlSeeAlso({ EsponderQueryRestrictionDTO.class, EsponderCriterionDTO.class })
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class EsponderQueryRestrictionDTO implements Serializable {

	private static final long serialVersionUID = -6325570244012530074L;

}