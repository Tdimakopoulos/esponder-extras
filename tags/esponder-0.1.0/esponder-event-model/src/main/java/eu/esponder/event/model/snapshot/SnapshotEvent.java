package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;


public class SnapshotEvent<T extends ESponderEntityDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = -1146740015260871634L;
	
}
