package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;


public class OperationsCentreSnapshotEvent<T extends OperationsCentreDTO> extends ResourceSnapshotEvent<T> {

	private static final long serialVersionUID = -7163057308021392749L;

}
