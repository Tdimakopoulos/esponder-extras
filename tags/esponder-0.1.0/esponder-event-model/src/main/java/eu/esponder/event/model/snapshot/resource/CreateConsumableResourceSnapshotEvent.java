package eu.esponder.event.model.snapshot.resource;

import eu.esponder.event.model.CreateEvent;


public class CreateConsumableResourceSnapshotEvent extends ConsumableResourceSnapshotEvent implements CreateEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4505854454250082213L;

}
