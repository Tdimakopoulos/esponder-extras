package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateEquipmentSnapshotEvent extends EquipmentSnapshotEvent<SnapshotDTO> implements UpdateEvent {

	private static final long serialVersionUID = 5506469989744092195L;
	
	
}
