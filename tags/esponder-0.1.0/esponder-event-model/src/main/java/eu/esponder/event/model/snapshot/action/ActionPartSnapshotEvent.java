package eu.esponder.event.model.snapshot.action;

import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.event.model.snapshot.SnapshotEvent;

public abstract class ActionPartSnapshotEvent<T extends ActionPartSnapshotDTO> extends SnapshotEvent<T> {

	private static final long serialVersionUID = -1431871320640276284L;
	
}
