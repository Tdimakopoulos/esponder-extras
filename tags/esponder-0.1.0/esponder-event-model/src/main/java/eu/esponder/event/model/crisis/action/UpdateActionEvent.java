package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateActionEvent extends ActionEvent<ActionDTO> implements UpdateEvent {

	private static final long serialVersionUID = 1950437603072013048L;
	
}
