package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.snapshot.SnapshotEvent;


public class EquipmentSnapshotEvent<T extends ESponderEntityDTO> extends SnapshotEvent<T> {

	private static final long serialVersionUID = 4318763503799378366L;
	
}
