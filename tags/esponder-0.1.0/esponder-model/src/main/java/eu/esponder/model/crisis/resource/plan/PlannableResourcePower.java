package eu.esponder.model.crisis.resource.plan;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.category.PlannableResourceCategory;

@Entity
@Table(name="plannable_resource_power")
public class PlannableResourcePower extends ESponderEntity<Long> {
	
	private static final long serialVersionUID = -4062662804969524602L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="RESOURCE_POWER_ID")
	protected Long id;
	
	@ManyToOne
	@JoinColumn(name="CRISIS_RESOURCE_PLAN_ID")
	private CrisisResourcePlan crisisResourcePlan;
	
	@OneToOne
	@JoinColumn(name="CATEGORY_ID")
	private PlannableResourceCategory planableResourceCategory;
	
	@Column(name="POWER")
	private Integer power;
	
	/**
	 * FIXME: Refactor this to an Enum indicating 
	 * <  -> Less than
	 * <= -> Less than or Equal
	 * == -> Equal
	 * => -> Greater than or Equal
	 * >  -> Greater than
	 */
	@Column(name="CONSTR")
	private String constraint;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CrisisResourcePlan getCrisisResourcePlan() {
		return crisisResourcePlan;
	}

	public void setCrisisResourcePlan(CrisisResourcePlan crisisResourcePlan) {
		this.crisisResourcePlan = crisisResourcePlan;
	}

	public PlannableResourceCategory getPlanableResourceCategory() {
		return planableResourceCategory;
	}

	public void setPlanableResourceCategory(
			PlannableResourceCategory planableResourceCategory) {
		this.planableResourceCategory = planableResourceCategory;
	}

	public Integer getPower() {
		return power;
	}

	public void setPower(Integer power) {
		this.power = power;
	}

	public String getConstraint() {
		return constraint;
	}

	public void setConstraint(String constraint) {
		this.constraint = constraint;
	}

}
