package eu.esponder.model.snapshot.status;

public enum SensorSnapshotStatus {
	WORKING,
	MALFUNCTIONING,
	DAMAGED
}
