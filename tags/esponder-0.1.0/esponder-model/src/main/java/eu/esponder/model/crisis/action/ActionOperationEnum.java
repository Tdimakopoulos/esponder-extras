package eu.esponder.model.crisis.action;

public enum ActionOperationEnum {

	MOVE,
	TRANSPORT,
	FIX
}
