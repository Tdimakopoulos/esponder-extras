package eu.esponder.model.snapshot.sensor.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum;

@Entity
@Table(name="statistics_config")
public class StatisticsConfig extends ESponderEntity<Long> {

	private static final long serialVersionUID = -788744673417526492L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SENSOR_CONFIG_ID")
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name="MEAS_STATISTIC_TYPE", nullable=false)
	private MeasurementStatisticTypeEnum measurementStatisticType;

	@Column(name="SAMPLING_PERIOD_MS", nullable=false)
	private long samplingPeriodMilliseconds;
	
	@OneToOne
	@JoinColumn(name="SENSOR_ID", nullable=false)
	private Sensor sensor;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MeasurementStatisticTypeEnum getMeasurementStatisticType() {
		return measurementStatisticType;
	}

	public void setMeasurementStatisticType(MeasurementStatisticTypeEnum measurementStatisticType) {
		this.measurementStatisticType = measurementStatisticType;
	}

	public long getSamplingPeriodMilliseconds() {
		return samplingPeriodMilliseconds;
	}

	public void setSamplingPeriodMilliseconds(long samplingPeriodMilliseconds) {
		this.samplingPeriodMilliseconds = samplingPeriodMilliseconds;
	}

	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}
	
	public String toString() {
		return "[Type : " + measurementStatisticType + ", sampling Period (ms): " + samplingPeriodMilliseconds + " ]";
	}

}
