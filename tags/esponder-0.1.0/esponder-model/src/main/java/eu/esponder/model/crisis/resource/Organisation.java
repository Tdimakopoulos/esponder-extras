package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.category.OrganisationCategory;
import eu.esponder.model.snapshot.location.Address;
import eu.esponder.model.snapshot.location.LocationArea;
import eu.esponder.model.snapshot.location.Point;

@Entity
@Table(name="organisation")
@NamedQueries({
	@NamedQuery(name="Organisation.findByTitle", query="select a from Organisation a where a.title=:title")
})
public class Organisation extends ESponderEntity<Long> {

	private static final long serialVersionUID = 126368864536261845L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ORGANISATION_ID")
	private Long id;
	
	@Column(name="TITLE")
	private String title;
	
	@ManyToOne
	@JoinColumn(name="PARENT_ID")
	protected Organisation parent;
	
	@OneToMany(mappedBy="parent")
	protected Set<Organisation> children;
	
	@Embedded
	private Address address;
	
	@Embedded
	private Point location;
	
	@OneToOne
	@JoinColumn(name="LOCATION_AREA_ID")
	private LocationArea responsibilityArea;
	
	@OneToMany(mappedBy="organisation")
	private Set<OrganisationCategory> organisationCategories;

	public Set<OrganisationCategory> getOrganisationCategories() {
		return organisationCategories;
	}

	public void setOrganisationCategories(
			Set<OrganisationCategory> organisationCategories) {
		this.organisationCategories = organisationCategories;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public LocationArea getResponsibilityArea() {
		return responsibilityArea;
	}

	public void setResponsibilityArea(LocationArea responsibilityArea) {
		this.responsibilityArea = responsibilityArea;
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}

	public Organisation getParent() {
		return parent;
	}

	public void setParent(Organisation parent) {
		this.parent = parent;
	}

	public Set<Organisation> getChildren() {
		return children;
	}

	public void setChildren(Set<Organisation> children) {
		this.children = children;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
