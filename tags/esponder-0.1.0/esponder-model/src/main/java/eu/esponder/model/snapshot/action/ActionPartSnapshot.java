package eu.esponder.model.snapshot.action;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.model.snapshot.SpatialSnapshot;
import eu.esponder.model.snapshot.status.ActionPartSnapshotStatus;


@Entity
@Table(name="action_part_snapshot")
public class ActionPartSnapshot extends SpatialSnapshot<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -433163591190712580L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_PART_SNAPSHOT_ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="ACTION_PART_ID", nullable=false)
	private ActionPart actionPart;
	
	@Enumerated(EnumType.STRING)
	@Column(name="ACTION_PART_SNAPSHOT_STATUS", nullable=false)
	private ActionPartSnapshotStatus status;
	
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private ActionPartSnapshot previous;
	
	@OneToOne(mappedBy="previous")
	private ActionPartSnapshot next;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ActionPart getActionPart() {
		return actionPart;
	}

	public void setActionPart(ActionPart actionPart) {
		this.actionPart = actionPart;
	}

	public ActionPartSnapshotStatus getStatus() {
		return status;
	}

	public void setStatus(ActionPartSnapshotStatus status) {
		this.status = status;
	}

	public ActionPartSnapshot getPrevious() {
		return previous;
	}

	public void setPrevious(ActionPartSnapshot previous) {
		this.previous = previous;
	}

	public ActionPartSnapshot getNext() {
		return next;
	}

	public void setNext(ActionPartSnapshot next) {
		this.next = next;
	}
	
}
