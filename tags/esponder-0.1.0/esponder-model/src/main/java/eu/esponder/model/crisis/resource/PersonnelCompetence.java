package eu.esponder.model.crisis.resource;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.category.PersonnelCategory;

@Entity
@Table(name="competence")
@DiscriminatorColumn(name="DISCRIMINATOR")
public abstract class PersonnelCompetence extends ESponderEntity<Long> {
	
	private static final long serialVersionUID = 7628260391266784842L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COMPETENCE_ID")
	private Long id;

	@Column(name="SHORT_DESCR")
	private String shortTitle;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private PersonnelCategory personnelCategory;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShortTitle() {
		return shortTitle;
	}

	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}

	public PersonnelCategory getPesonnelCategory() {
		return personnelCategory;
	}

	public void setPesonnelCategory(PersonnelCategory pesonnelCategory) {
		this.personnelCategory = pesonnelCategory;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    final PersonnelCompetence other = (PersonnelCompetence) obj;
	    if (id != other.id) {
	        return false;
	    }
	    if (!this.getPesonnelCategory().equals(other.getPesonnelCategory())) {
	    	return false;
	    }
	    if (!this.getShortTitle().equals(other.getShortTitle())) {
	    	return false;
	    }
	    if (!this.getDescription().equals(other.getDescription())) {
	    	return false;
	    }
	    if (!this.getRecordStatus().equals(other.getRecordStatus())) {
	    	return false;
	    }
	    return true;
	}

}
