package eu.esponder.model.snapshot;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class Period implements Serializable {
	
	private static final long serialVersionUID = -1943089649218847062L;
	
	public Period() { }

	public Period(Date dateFrom, Date dateTo) {
		super();
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}

	@Column(name="DATE_FROM", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateFrom;
	
	@Column(name="DATE_TO", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateTo;

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	@Override
	public String toString() {
		return "Period [dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}

}
