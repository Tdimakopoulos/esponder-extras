package eu.esponder.model.snapshot.resource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.snapshot.SpatialSnapshot;
import eu.esponder.model.snapshot.status.ActorSnapshotStatus;

@Entity
@Table(name="actor_snapshot")
@NamedQueries({
	@NamedQuery(
		name="ActorSnapshot.findByActorAndDate",
		query="SELECT s FROM ActorSnapshot s WHERE s.actor.id = :actorID AND s.period.dateTo <= :maxDate AND s.period.dateTo = " +
				"(SELECT max(s.period.dateTo) FROM ActorSnapshot s WHERE s.actor.id = :actorID)")
})
public class ActorSnapshot extends SpatialSnapshot<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1380880669505779496L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTOR_SNAPSHOT_ID")
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="ACTOR_SNAPSHOT_STATUS", nullable=false)
	private ActorSnapshotStatus status;
	
	@ManyToOne
	@JoinColumn(name="ACTOR_ID", nullable=false)
	private Actor actor;
	
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private ActorSnapshot previous;
	
	@OneToOne(mappedBy="previous")
	private ActorSnapshot next;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ActorSnapshotStatus getStatus() {
		return status;
	}

	public void setStatus(ActorSnapshotStatus status) {
		this.status = status;
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public ActorSnapshot getPrevious() {
		return previous;
	}

	public void setPrevious(ActorSnapshot previous) {
		this.previous = previous;
	}

	public ActorSnapshot getNext() {
		return next;
	}

	public void setNext(ActorSnapshot next) {
		this.next = next;
	}

}
