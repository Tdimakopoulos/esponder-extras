package eu.esponder.osgi.service.event;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import eu.esponder.df.manager.service.SensorSnapshotManagerRemoteService;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;

@Stateless
public abstract class EventHandlerBean<T extends ESponderEvent<? extends ESponderEntityDTO>> 
	implements EventHandlerService<T> {

	@EJB
	SensorSnapshotManagerRemoteService sensorSnapshotManager;
	
	/*
	 * Handle the event as appropriate for each event type T
	 */
	public void onReceive(T event) {
		if (event instanceof CreateActionEvent) {
			if (event.getEventAttachment() instanceof SensorSnapshotDTO) {
				/*
				 * FIXME: We need to update the hierarchy of events so that SensorMeasuremetnDTO is used in the SensorSnapshotEvent attachment
				 * I just make a trial here
				 */
				SensorMeasurementDTO measurement = new ArithmeticSensorMeasurementDTO();
				sensorSnapshotManager.createSensorSnapshot(measurement, new Long(1));
			}
			
		}
		
	}

}
