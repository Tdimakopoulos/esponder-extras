package eu.esponder.osgi.service.event;

import javax.ejb.Local;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;

@Local
public interface EventHandlerService<T extends ESponderEvent<? extends ESponderEntityDTO>> {

	public void onReceive(T event);
}
