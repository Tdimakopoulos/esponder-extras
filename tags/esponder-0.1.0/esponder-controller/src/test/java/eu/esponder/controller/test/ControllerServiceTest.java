package eu.esponder.controller.test;

import java.math.BigDecimal;
import java.sql.Date;

import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.BeforeClass;

import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.OrganisationRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.location.Point;
import eu.esponder.model.snapshot.location.Sphere;
import eu.esponder.model.user.ESponderUser;
import eu.esponder.test.ResourceLocator;

public class ControllerServiceTest {

	protected static final String USER_NAME = "ctri";
	protected Long userID;
	
	protected ActorRemoteService actorService;
	protected EquipmentRemoteService equipmentService;
	protected OperationsCentreRemoteService operationsCentreService;
	protected SensorRemoteService sensorService;
	protected TypeRemoteService typeService;
	protected UserRemoteService userService;
	protected ActionRemoteService actionService;
	protected ResourceCategoryRemoteService resourceCategoryService;
	protected GenericRemoteService genericService;
	protected ESponderRemoteMappingService mappingService;
	protected OrganisationRemoteService organisationService;
	protected PersonnelRemoteService personnelService;
		
	@BeforeClass(alwaysRun=true)
	public void before() {
		actorService = ResourceLocator.lookup("esponder/ActorBean/remote");
		equipmentService = ResourceLocator.lookup("esponder/EquipmentBean/remote");
		operationsCentreService = ResourceLocator.lookup("esponder/OperationsCentreBean/remote");
		sensorService = ResourceLocator.lookup("esponder/SensorBean/remote");
		typeService = ResourceLocator.lookup("esponder/TypeBean/remote");
		userService = ResourceLocator.lookup("esponder/UserBean/remote");
		actionService = ResourceLocator.lookup("esponder/ActionBean/remote");
		resourceCategoryService = ResourceLocator.lookup("esponder/ResourceCategoryBean/remote");
		genericService = ResourceLocator.lookup("esponder/GenericBean/remote");
		mappingService = ResourceLocator.lookup("esponder/ESponderMappingBean/remote");
		organisationService = ResourceLocator.lookup("esponder/OrganisationBean/remote");
		personnelService = ResourceLocator.lookup("esponder/PersonnelBean/remote");
		
		ESponderUser user = userService.findByUserName(USER_NAME);
		if (null != user) {
			userID = user.getId();
		}
	}
	
	protected Period createPeriod(int seconds) {
		java.util.Date now = new java.util.Date();
		Period period = new Period(now, DateUtils.addSeconds(now, seconds));
		return period; 
	}

	protected PeriodDTO createPeriodDTO(int seconds) {
		Date date = new Date(1000);
		PeriodDTO period = new PeriodDTO(date, date);
		return period;
		
//		try {
//			XMLGregorianCalendar now = DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar());
//			GregorianCalendar calendarTo = new GregorianCalendar();
//			calendarTo.setTime(DateUtils.addSeconds(new GregorianCalendar().getTime(), seconds));
//			XMLGregorianCalendar xmlGregorianCalendarTo = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendarTo);
//			PeriodDTO period = new PeriodDTO(now, xmlGregorianCalendarTo);
//			return period;
//		} catch (DatatypeConfigurationException e) {
//			e.printStackTrace();
//		}
//		return null;
	}
	
	

	protected Sphere createSphere(
			Double latitude, Double longitude, Double altitude, Double radius) {
		
		Point centre = new Point(
				new BigDecimal(latitude), new BigDecimal(longitude), null != altitude ? new BigDecimal(altitude) : null);
		Sphere sphere = new Sphere(centre, null != radius ? new BigDecimal(radius) : null);
		return sphere;
	}
	
	protected SphereDTO createSphereDTO(
			Double latitude, Double longitude, Double altitude, Double radius, String title) {
		
		PointDTO centre = new PointDTO(
				new BigDecimal(latitude), new BigDecimal(longitude), null != altitude ? new BigDecimal(altitude) : null);
		SphereDTO sphere = new SphereDTO(centre, null != radius ? new BigDecimal(radius) : null, title);
		return sphere;
	}
}
