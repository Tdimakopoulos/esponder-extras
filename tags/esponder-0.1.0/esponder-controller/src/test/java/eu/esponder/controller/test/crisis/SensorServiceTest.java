package eu.esponder.controller.test.crisis;

import java.math.BigDecimal;

import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HeartBeatRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;
import eu.esponder.dto.model.type.SensorTypeDTO;
import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.snapshot.sensor.config.StatisticsConfig;
import eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum;
import eu.esponder.model.snapshot.status.SensorSnapshotStatus;

public class SensorServiceTest extends ControllerServiceTest {
	
	private static int SECONDS = 10;
	
	@Test(groups="createResources")
	public void testCreateSensorDTO() throws ClassNotFoundException {
		
		SensorTypeDTO temperatureSensorType = (SensorTypeDTO) typeService.findDTOByTitle("TEMP");
		SensorTypeDTO heartbeatSensorType = (SensorTypeDTO) typeService.findDTOByTitle("HB");
		
		EquipmentDTO firstFRCEquipment = equipmentService.findByTitleRemote("FRU #1");
		EquipmentDTO firstFREquipment = equipmentService.findByTitleRemote("FRU #1.1");
		EquipmentDTO secondFREquipment = equipmentService.findByTitleRemote("FRU #1.2");
		EquipmentDTO secondFRCEquipment = equipmentService.findByTitleRemote("FRU #2");
		EquipmentDTO thirdFREquipment = equipmentService.findByTitleRemote("FRU #2.1");
		EquipmentDTO fourthFREquipment = equipmentService.findByTitleRemote("FRU #2.2");
		
		createSensorDTO(temperatureSensorType, "FRU #1 TEMP.", firstFRCEquipment);
		createSensorDTO(heartbeatSensorType, "FRU #1 HB.", firstFRCEquipment);
		createSensorDTO(temperatureSensorType, "FRU #1.1 TEMP.", firstFREquipment);
		createSensorDTO(heartbeatSensorType, "FRU #1.1 HB.", firstFREquipment);
		createSensorDTO(temperatureSensorType, "FRU #1.2 TEMP.", secondFREquipment);
		createSensorDTO(heartbeatSensorType, "FRU #1.2 HB.", secondFREquipment);
		createSensorDTO(temperatureSensorType, "FRU #2 TEMP.", secondFRCEquipment);
		createSensorDTO(heartbeatSensorType, "FRU #2 HB.", secondFRCEquipment);
		createSensorDTO(temperatureSensorType, "FRU #2.1 TEMP.", thirdFREquipment);
		createSensorDTO(heartbeatSensorType, "FRU #2.1 HB.", thirdFREquipment);
		createSensorDTO(temperatureSensorType, "FRU #2.2 TEMP.", fourthFREquipment);
		createSensorDTO(heartbeatSensorType, "FRU #2.2 HB.", fourthFREquipment);
	}
	
	@Test(groups="createSnapshots")
	public void testCreateSensorSnapshots() throws ClassNotFoundException {		
		
		SensorDTO firstFRCTemperature = sensorService.findByTitleRemote("FRU #1 TEMP.");
//		SensorDTO firstFRCHeartbeat = sensorService.findByTitleRemote("FRU #1 HB.");
//		SensorDTO firstFRTemperature = sensorService.findByTitleRemote("FRU #1.1 TEMP.");
//		SensorDTO firstFRHeartbeat = sensorService.findByTitleRemote("FRU #1.1 HB.");
//		SensorDTO secondFRTemperature = sensorService.findByTitleRemote("FRU #1.2 TEMP.");
//		SensorDTO secondFRHeartbeat = sensorService.findByTitleRemote("FRU #1.2 HB.");
//		SensorDTO secondFRCTemperature = sensorService.findByTitleRemote("FRU #2 TEMP.");
//		SensorDTO secondFRCHeartbeat = sensorService.findByTitleRemote("FRU #2 HB.");
//		SensorDTO thirdFRTemperature = sensorService.findByTitleRemote("FRU #2.1 TEMP.");
//		SensorDTO thirdFRHeartbeat = sensorService.findByTitleRemote("FRU #2.1 HB.");
//		SensorDTO fourthFRTemperature = sensorService.findByTitleRemote("FRU #2.2 TEMP.");
//		SensorDTO fourthFRHeartbeat = sensorService.findByTitleRemote("FRU #2.2 HB.");
//		
//		Period period = this.createPeriod(SECONDS);
		
		
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("30"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(12));
//		createSensorSnapshot(firstFRCHeartbeat, new BigDecimal("80"), MeasurementStatisticTypeEnum.MEAN, period);
//		createSensorSnapshot(firstFRTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnum.MEAN, period);
//		createSensorSnapshot(firstFRHeartbeat, new BigDecimal("70"), MeasurementStatisticTypeEnum.MEAN, period);
//		createSensorSnapshot(secondFRTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnum.MEAN, period);
//		createSensorSnapshot(secondFRHeartbeat, new BigDecimal("85"), MeasurementStatisticTypeEnum.MEAN, period);
//		createSensorSnapshot(secondFRCTemperature, new BigDecimal("35"), MeasurementStatisticTypeEnum.MEAN, period);
//		createSensorSnapshot(secondFRCHeartbeat, new BigDecimal("79"), MeasurementStatisticTypeEnum.MEAN, period);
//		createSensorSnapshot(thirdFRTemperature, new BigDecimal("36"), MeasurementStatisticTypeEnum.MEAN, period);
//		createSensorSnapshot(thirdFRHeartbeat, new BigDecimal("74"), MeasurementStatisticTypeEnum.MEAN, period);
//		createSensorSnapshot(fourthFRTemperature, new BigDecimal("34"), MeasurementStatisticTypeEnum.MEAN, period);
//		createSensorSnapshot(fourthFRHeartbeat, new BigDecimal("84"), MeasurementStatisticTypeEnum.MEAN, period);
	}
	
	@Test(groups="createSnapshots")
	public void testCreateSensorSnapshotsDTO() throws ClassNotFoundException {		
		
		SensorDTO firstFRCTemperature = sensorService.findByTitleRemote("FRU #1 TEMP.");
		SensorDTO firstFRCHeartbeat = sensorService.findByTitleRemote("FRU #1 HB.");
		SensorDTO firstFRTemperature = sensorService.findByTitleRemote("FRU #1.1 TEMP.");
		SensorDTO firstFRHeartbeat = sensorService.findByTitleRemote("FRU #1.1 HB.");
		SensorDTO secondFRTemperature = sensorService.findByTitleRemote("FRU #1.2 TEMP.");
		SensorDTO secondFRHeartbeat = sensorService.findByTitleRemote("FRU #1.2 HB.");
		SensorDTO secondFRCTemperature = sensorService.findByTitleRemote("FRU #2 TEMP.");
		SensorDTO secondFRCHeartbeat = sensorService.findByTitleRemote("FRU #2 HB.");
		SensorDTO thirdFRTemperature = sensorService.findByTitleRemote("FRU #2.1 TEMP.");
		SensorDTO thirdFRHeartbeat = sensorService.findByTitleRemote("FRU #2.1 HB.");
		SensorDTO fourthFRTemperature = sensorService.findByTitleRemote("FRU #2.2 TEMP.");
		SensorDTO fourthFRHeartbeat = sensorService.findByTitleRemote("FRU #2.2 HB.");
		
		PeriodDTO period = this.createPeriodDTO(SECONDS);
		
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("30"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("80"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(firstFRTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(firstFRHeartbeat, new BigDecimal("70"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(secondFRTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(secondFRHeartbeat, new BigDecimal("85"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("35"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("79"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(thirdFRTemperature, new BigDecimal("36"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(thirdFRHeartbeat, new BigDecimal("74"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(fourthFRTemperature, new BigDecimal("34"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(fourthFRHeartbeat, new BigDecimal("84"), MeasurementStatisticTypeEnumDTO.MEAN, period);
	}
	
	@Test(groups="CreateSensorConfiguration")
	public void testCreateSensorConfigurationDTO() {		
		
		SensorDTO firstFRCTemperature = sensorService.findByTitleRemote("FRU #1 TEMP.");
		SensorDTO firstFRCHeartbeat = sensorService.findByTitleRemote("FRU #1 HB.");
		SensorDTO firstFRTemperature = sensorService.findByTitleRemote("FRU #1.1 TEMP.");
		SensorDTO firstFRHeartbeat = sensorService.findByTitleRemote("FRU #1.1 HB.");
		SensorDTO secondFRTemperature = sensorService.findByTitleRemote("FRU #1.2 TEMP.");
		SensorDTO secondFRHeartbeat = sensorService.findByTitleRemote("FRU #1.2 HB.");
		SensorDTO secondFRCTemperature = sensorService.findByTitleRemote("FRU #2 TEMP.");
		SensorDTO secondFRCHeartbeat = sensorService.findByTitleRemote("FRU #2 HB.");
		SensorDTO thirdFRTemperature = sensorService.findByTitleRemote("FRU #2.1 TEMP.");
		SensorDTO thirdFRHeartbeat = sensorService.findByTitleRemote("FRU #2.1 HB.");
		SensorDTO fourthFRTemperature = sensorService.findByTitleRemote("FRU #2.2 TEMP.");
		SensorDTO fourthFRHeartbeat = sensorService.findByTitleRemote("FRU #2.2 HB.");
		
		createStatisticConfigDTO(firstFRCTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN); 
		createStatisticConfigDTO(firstFRCHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MAXIMUM);
		createStatisticConfigDTO(firstFRTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);
		createStatisticConfigDTO(firstFRHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);
		createStatisticConfigDTO(secondFRTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MAXIMUM);
		createStatisticConfigDTO(secondFRHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);
		createStatisticConfigDTO(secondFRCTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MAXIMUM);
		createStatisticConfigDTO(secondFRCHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);
		createStatisticConfigDTO(thirdFRTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MAXIMUM);
		createStatisticConfigDTO(thirdFRHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);
		createStatisticConfigDTO(fourthFRTemperature, new Long(2000), MeasurementStatisticTypeEnumDTO.MAXIMUM);
		createStatisticConfigDTO(fourthFRHeartbeat, new Long(2000), MeasurementStatisticTypeEnumDTO.MEAN);
	}
	
	private SensorDTO createSensorDTO(SensorTypeDTO type, String title, EquipmentDTO equipment) throws ClassNotFoundException {
		SensorDTO sensor = null;
		
		if(type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("TEMP")).getTitle())) {
			sensor = new BodyTemperatureSensorDTO();
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("HB")).getTitle())) {
			sensor = new HeartBeatRateSensorDTO();
		}
		sensor.setType(type.getTitle());
		sensor.setTitle(title);
		sensor.setStatus(ResourceStatusDTO.ALLOCATED);
		sensor.setEquipmentId(equipment.getId());
		return sensorService.createRemote(sensor, this.userID);
	}
	
	@SuppressWarnings("unused")
	private SensorSnapshot createSensorSnapshot(Sensor sensor, BigDecimal value, MeasurementStatisticTypeEnum statisticType, Period period) {
		SensorSnapshot snapshot = new SensorSnapshot();
		snapshot.setSensor(sensor);
		snapshot.setStatisticType(statisticType);
		snapshot.setValue(value);
		snapshot.setPeriod(period);
		snapshot.setStatus(SensorSnapshotStatus.WORKING);
		return sensorService.createSnapshot(snapshot, this.userID);
	}
	
	private SensorSnapshotDTO createSensorSnapshotDTO(SensorDTO sensor, BigDecimal value, MeasurementStatisticTypeEnumDTO statisticType, PeriodDTO period) throws ClassNotFoundException {
		SensorSnapshotDTO snapshot = new SensorSnapshotDTO();
		
		snapshot.setSensor(sensor);
		snapshot.setStatisticType(statisticType);
		snapshot.setValue(value.toString());
		snapshot.setPeriod(period);
		snapshot.setStatus(SensorSnapshotStatusDTO.WORKING);
		
		return (SensorSnapshotDTO) genericService.createEntityDTO(snapshot, userID);
	}
	
	@SuppressWarnings("unused")
	private StatisticsConfig createStatisticConfig(Sensor sensor, Long samplingPeriodMs, MeasurementStatisticTypeEnum type) {
		StatisticsConfig statisticsConfig = new StatisticsConfig();
		statisticsConfig.setMeasurementStatisticType(type);
		statisticsConfig.setSamplingPeriodMilliseconds(samplingPeriodMs);
		statisticsConfig.setSensor(sensor);
		return sensorService.createConfig(statisticsConfig, this.userID);
	}
	
	private StatisticsConfigDTO createStatisticConfigDTO(SensorDTO sensor, Long samplingPeriodMs, MeasurementStatisticTypeEnumDTO type) {
		StatisticsConfigDTO statisticsConfig = new StatisticsConfigDTO();
		statisticsConfig.setMeasurementStatisticType(type);
		statisticsConfig.setSamplingPeriodMilliseconds(samplingPeriodMs);
		statisticsConfig.setSensorId(sensor.getId());
		return sensorService.createConfigRemote(statisticsConfig, this.userID);
	}
	
}
