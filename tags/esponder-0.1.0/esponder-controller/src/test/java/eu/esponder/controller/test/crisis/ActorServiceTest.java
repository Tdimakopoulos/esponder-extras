package eu.esponder.controller.test.crisis;

import org.testng.annotations.Test;


import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActorSnapshotStatusDTO;
import eu.esponder.dto.model.type.ActorTypeDTO;
import eu.esponder.dto.model.type.OperationalActorTypeDTO;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.location.LocationArea;
import eu.esponder.model.snapshot.resource.ActorSnapshot;
import eu.esponder.model.snapshot.status.ActorSnapshotStatus;
import eu.esponder.util.logger.ESponderLogger;

public class ActorServiceTest extends ControllerServiceTest {

	private static int SECONDS = 30;
	private static double RADIUS = 1;

	@SuppressWarnings("unused")
	@Test(groups="createResources")
	public void testCreateActorsDTO() throws ClassNotFoundException {

		OperationalActorTypeDTO frc = (OperationalActorTypeDTO) typeService.findDTOByTitle("FRC");
		OperationalActorTypeDTO fr = (OperationalActorTypeDTO) typeService.findDTOByTitle("FR");

		OperationsCentreDTO meocAthens = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("MEOC Athens");
		OperationsCentreDTO meocPiraeus = (OperationsCentreDTO) operationsCentreService.findOperationsCentreByTitleRemote("MEOC Piraeus");

		ActorDTO firstChief = createActorDTO(frc, "FRC #1", null, meocAthens, "Employee No1");
		ActorDTO subordinate11 = createActorDTO(fr, "FR #1.1", firstChief, null, "Employee No2");
		ActorDTO subordinate12 = createActorDTO(fr, "FR #1.2", firstChief, null, "Employee No3");
		ActorDTO secondChief = createActorDTO(frc, "FRC #2", null, meocPiraeus, "Employee No4");
		ActorDTO subordinate21 = createActorDTO(fr, "FR #2.1", secondChief, null, "Employee No5");
		ActorDTO subordinate22 = createActorDTO(fr, "FR #2.2", secondChief, null, "Employee No6");

	}


	@Test(groups="createSnapshots")
	public void testCreateActorSnapshots() {
		
//		ActorDTO firstFRC = (ActorDTO) actorService.findByTitleRemote("FRC #1"); 

//		ActorDTO firstFR = (ActorDTO) actorService.findByTitleRemote("FR #1.1");
//		ActorDTO secondFR = (ActorDTO) actorService.findByTitleRemote("FR #1.2");
//		ActorDTO secondFRC = (ActorDTO) actorService.findByTitleRemote("FRC #2");
//		ActorDTO thirdFR = (ActorDTO) actorService.findByTitleRemote("FR #2.1");
//		ActorDTO fourthFR = (ActorDTO) actorService.findByTitleRemote("FR #2.2");
//
//		Period period = this.createPeriod(SECONDS);
//		createActorSnapshotDTO(firstFRC,period, this.createSphereDTO(38.025334, 23.802717, null, RADIUS));
//		createActorSnapshot(firstFR, period, this.createSphere(38.02732, 23.805871, null, RADIUS));
//		createActorSnapshot(secondFR, period, this.createSphere(38.024615, 23.799348, null, RADIUS));
//		createActorSnapshot(secondFRC, period, this.createSphere(37.950118, 23.650045, null, RADIUS));
//		createActorSnapshot(thirdFR, period, this.createSphere(37.948493, 23.648157, null, RADIUS));
//		createActorSnapshot(fourthFR, period, this.createSphere(37.951065, 23.639746, null, RADIUS));
	}

	@Test(groups="createSnapshots")
	public void testCreateActorSnapshotsDTO() {

		ActorDTO firstFRC = (ActorDTO) actorService.findByTitleRemote("FRC #1");
		ActorDTO firstFR = (ActorDTO) actorService.findByTitleRemote("FR #1.1");
		ActorDTO secondFR = (ActorDTO) actorService.findByTitleRemote("FR #1.2");
		ActorDTO secondFRC = (ActorDTO) actorService.findByTitleRemote("FRC #2");
		ActorDTO thirdFR = (ActorDTO) actorService.findByTitleRemote("FR #2.1");
		ActorDTO fourthFR = (ActorDTO) actorService.findByTitleRemote("FR #2.2");

		PeriodDTO period = this.createPeriodDTO(SECONDS);

		createActorSnapshotDTO(firstFRC,period, this.createSphereDTO(38.025334, 23.802717, null, RADIUS, "Sphere 1"));
		createActorSnapshotDTO(firstFR, period, this.createSphereDTO(38.02732, 23.805871, null, RADIUS, "Sphere 2"));
		createActorSnapshotDTO(secondFR, period, this.createSphereDTO(38.024615, 23.799348, null, RADIUS, "Sphere 3"));
		createActorSnapshotDTO(secondFRC, period, this.createSphereDTO(37.950118, 23.650045, null, RADIUS, "Sphere 4"));
		createActorSnapshotDTO(thirdFR, period, this.createSphereDTO(37.948493, 23.648157, null, RADIUS, "Sphere 5"));
		createActorSnapshotDTO(fourthFR, period, this.createSphereDTO(37.951065, 23.639746, null, RADIUS, "Sphere 6"));
	}


	private ActorDTO createActorDTO(ActorTypeDTO type, String title, ActorDTO supervisor, OperationsCentreDTO operationsCentre, String personnelTitle) {

		PersonnelDTO personnelDTO = personnelService.findDTOByTitle(personnelTitle);
		if(personnelDTO != null) {
			ActorDTO actorDTO = new ActorDTO();
			actorDTO.setType(type.getTitle());
			actorDTO.setTitle(title);
			actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
			actorDTO.setSupervisor(supervisor);
			if(operationsCentre != null)
				actorDTO.setOperationsCentreId(operationsCentre.getId());
			actorDTO.setPersonnel(personnelDTO);
			return actorService.createRemote(actorDTO, this.userID);
		}
		else {
			ESponderLogger.debug(this.getClass(), "Requested Personnel cannot be found.");
			return null;
		}
		
	}

	
	@SuppressWarnings("unused")
	private ActorDTO createActorDTO(
			ActorTypeDTO type, String title, ActorDTO supervisor, OperationsCentreDTO operationsCentre) {
		
		ActorDTO actorDTO = new ActorDTO();
		actorDTO.setType(type.getTitle());
		actorDTO.setTitle(title);
		actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		actorDTO.setSupervisor(supervisor);
		actorDTO.setOperationsCentreId(operationsCentre.getId());
		return actorService.createRemote(actorDTO, this.userID);
	}
	
	@SuppressWarnings("unused")
	private ActorSnapshot createActorSnapshot(Actor actor,Period period, LocationArea locationArea) {

		ActorSnapshot snapshot = new ActorSnapshot();
		snapshot.setActor(actor);
		snapshot.setLocationArea(locationArea);
		snapshot.setPeriod(period);
		snapshot.setStatus(ActorSnapshotStatus.ACTIVE);
		//		return actorService.createSnapshot(snapshot, this.userID);
		return null;
	}

	private ActorSnapshotDTO createActorSnapshotDTO(ActorDTO actor, PeriodDTO period, LocationAreaDTO locationArea) {

		ActorSnapshotDTO snapshot = new ActorSnapshotDTO();
		snapshot.setActor(actor);
		snapshot.setLocationArea(locationArea);
		snapshot.setPeriod(period);
		snapshot.setStatus(ActorSnapshotStatusDTO.ACTIVE);
		return actorService.createSnapshotRemote(snapshot, this.userID);
	}

}
