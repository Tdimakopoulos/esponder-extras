package eu.esponder.controller.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.category.EquipmentCategoryDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.EquipmentSnapshotStatusDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.resource.EquipmentSnapshot;
import eu.esponder.model.snapshot.status.EquipmentSnapshotStatus;

public class EquipmentServiceTest extends ControllerServiceTest {
	
	private static int SECONDS = 20;
	
	@Test(groups="createResources")
	public void testCreateEquipmentDTO() throws ClassNotFoundException {
		
		EquipmentTypeDTO wimax = (EquipmentTypeDTO) typeService.findDTOByTitle("FRU WiMAX");
		EquipmentTypeDTO wifi = (EquipmentTypeDTO) typeService.findDTOByTitle("FRU WiFi");
		
		ActorDTO firstFRC = actorService.findByTitleRemote("FRC #1");
		ActorDTO firstFR = actorService.findByTitleRemote("FR #1.1");
		ActorDTO secondFR = actorService.findByTitleRemote("FR #1.2");
		ActorDTO secondFRC = actorService.findByTitleRemote("FRC #2");
		ActorDTO thirdFR= actorService.findByTitleRemote("FR #2.1");
		ActorDTO fourthFR = actorService.findByTitleRemote("FR #2.2");
		
		createEquipmentDTO(wimax, "FRU #1", firstFRC);
		createEquipmentDTO(wifi, "FRU #1.1", firstFR);
		createEquipmentDTO(wifi, "FRU #1.2", secondFR);
		createEquipmentDTO(wimax, "FRU #2", secondFRC);
		createEquipmentDTO(wifi, "FRU #2.1", thirdFR);
		createEquipmentDTO(wifi, "FRU #2.2", fourthFR);
	}
	
	@Test(groups="createSnapshots")
	public void testCreateEquipmentSnapshots() throws ClassNotFoundException {		
		
		EquipmentDTO firstFRCEquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #1");
		EquipmentDTO firstFREquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #1.1");
		EquipmentDTO secondFREquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #1.2");
		EquipmentDTO secondFRCEquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #2");
		EquipmentDTO thirdFREquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #2.1");
		EquipmentDTO fourthFREquipment = (EquipmentDTO) equipmentService.findByTitleRemote("FRU #2.2");

		PeriodDTO period = this.createPeriodDTO(SECONDS);
		createEquipmentSnapshotDTO(firstFRCEquipment, period);
		createEquipmentSnapshotDTO(firstFREquipment, period);
		createEquipmentSnapshotDTO(secondFREquipment, period);
		createEquipmentSnapshotDTO(secondFRCEquipment, period);
		createEquipmentSnapshotDTO(thirdFREquipment, period);
		createEquipmentSnapshotDTO(fourthFREquipment, period);
	}
	
	private EquipmentDTO createEquipmentDTO(EquipmentTypeDTO type, String title, ActorDTO actor) {
		
		EquipmentDTO equipment = new EquipmentDTO();		
		EquipmentCategoryDTO equipmentCategory = resourceCategoryService.findDTOByType(type);
		equipment.setEquipmentCategoryId(equipmentCategory.getId());
		equipment.setTitle(title);
		equipment.setStatus(ResourceStatusDTO.AVAILABLE);
		equipment.setActor(actor);
		return equipmentService.createRemote(equipment, this.userID);
	}
	
	@SuppressWarnings("unused")
	private EquipmentSnapshot createEquipmentSnapshot(Equipment equipment, Period period) {
		
		EquipmentSnapshot snapshot = new EquipmentSnapshot();
		snapshot.setEquipment(equipment);
		snapshot.setPeriod(period);
		snapshot.setStatus(EquipmentSnapshotStatus.WORKING);
//		return equipmentService.createSnapshot(snapshot, this.userID);
		return null;
	}
	
	private EquipmentSnapshotDTO createEquipmentSnapshotDTO(EquipmentDTO equipment, PeriodDTO period) {
		
		EquipmentSnapshotDTO snapshot = new EquipmentSnapshotDTO();
		snapshot.setEquipment(equipment);
		snapshot.setPeriod(period);
		snapshot.setStatus(EquipmentSnapshotStatusDTO.WORKING);
		return equipmentService.createSnapshotRemote(snapshot, this.userID);
	}
	
}