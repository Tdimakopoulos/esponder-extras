package eu.esponder.controller.mapping.dozer;

import org.dozer.CustomConverter;

import eu.esponder.model.snapshot.location.Sphere;

public class LocationAreaCustomConverter implements CustomConverter {

	@Override
	public Object convert(Object destination,
			Object source, Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == Sphere.class) {
			destination = source;
			return destination;
		}
		
		return null;
	}

}
