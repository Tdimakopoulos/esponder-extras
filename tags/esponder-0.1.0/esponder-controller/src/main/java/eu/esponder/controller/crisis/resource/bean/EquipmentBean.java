package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.snapshot.resource.EquipmentSnapshot;
import eu.esponder.model.type.EquipmentType;

@Stateless
public class EquipmentBean implements EquipmentService, EquipmentRemoteService {

	@EJB
	private CrudService<Equipment> equipmentCrudService;

	@EJB
	private CrudService<EquipmentSnapshot> equipmentSnapshotCrudService;

	@EJB
	private TypeService typeService;

	@EJB
	private ActorService actorService;

	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	@Override
	public EquipmentDTO findByIdRemote(Long equipmentID) {
		return (EquipmentDTO) mappingService.mapESponderEntity(findById(equipmentID), EquipmentDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	public Equipment findById(Long equipmentID) {
		return equipmentCrudService.find(Equipment.class, equipmentID);
	}

	//-------------------------------------------------------------------------

	@Override
	public EquipmentDTO findByTitleRemote(String title) {
		return (EquipmentDTO) mappingService.mapESponderEntity(findByTitle(title), EquipmentDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	public Equipment findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Equipment) equipmentCrudService.findSingleWithNamedQuery("Equipment.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@Override
	public EquipmentSnapshotDTO findSnapshotByDateRemote(Long equipmentID, Date maxDate) {
		return (EquipmentSnapshotDTO) mappingService.mapESponderEntity(findSnapshotByDate(equipmentID, maxDate), EquipmentSnapshotDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	public EquipmentSnapshot findSnapshotByDate(Long equipmentID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("equipmentID", equipmentID);
		params.put("maxDate", maxDate);
		EquipmentSnapshot snapshot =
				(EquipmentSnapshot) equipmentCrudService.findSingleWithNamedQuery("EquipmentSnapshot.findByEquipmentAndDate", params);
		return snapshot;
	}


	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public EquipmentDTO createRemote(EquipmentDTO equipmentDTO, Long userID) {
		Equipment equipment = (Equipment) mappingService.mapESponderEntityDTO(equipmentDTO, Equipment.class);
		return (EquipmentDTO) mappingService.mapESponderEntity(create(equipment, userID), EquipmentDTO.class);

	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Equipment create(Equipment equipment, Long userID) {
		equipment.getEquipmentCategory().setEquipmentType((EquipmentType) typeService.findById(equipment.getEquipmentCategory().getEquipmentType().getId()));
		if (null != equipment.getActor()) {
			equipment.setActor(actorService.findById(equipment.getActor().getId()));
		}
		equipmentCrudService.create(equipment);
		return equipment;
	}

	//--------------------------------------------------------------------------

	@Override
	public void deleteEquipmentRemote(Long equipmentId, Long userID) {
		deleteEquipment(equipmentId, userID);
	}	

	//--------------------------------------------------------------------------

	@Override
	public void deleteEquipment(Long equipmentId, Long userID) {
		equipmentCrudService.delete(Equipment.class, equipmentId);
	}

	//-------------------------------------------------------------------------

	@Override
	public EquipmentDTO updateEquipmentRemote(EquipmentDTO equipmentDTO, Long userID) {
		Equipment equipment = (Equipment) mappingService.mapESponderEntityDTO(equipmentDTO, Equipment.class);
		return (EquipmentDTO) mappingService.mapESponderEntity(updateEquipment(equipment, userID), EquipmentDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Equipment updateEquipment(Equipment equipment, Long userID) {
		return equipmentCrudService.update(equipment);
	}

	//-------------------------------------------------------------------------

	@Override
	public EquipmentSnapshotDTO createSnapshotRemote(EquipmentSnapshotDTO snapshotDTO, Long userID) {
		EquipmentSnapshot equipmentSnapshot = (EquipmentSnapshot) mappingService.mapESponderEntityDTO(snapshotDTO, EquipmentSnapshot.class);
		return (EquipmentSnapshotDTO) mappingService.mapESponderEntity(createSnapshot(equipmentSnapshot, userID), EquipmentSnapshotDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public EquipmentSnapshot createSnapshot(EquipmentSnapshot snapshot, Long userID) {
		snapshot.setEquipment((Equipment)this.findById(snapshot.getEquipment().getId()));
		equipmentSnapshotCrudService.create(snapshot);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	@Override
	public EquipmentSnapshotDTO updateSnapshotRemote(EquipmentSnapshotDTO snapshotDTO, Long userID) {
		EquipmentSnapshot equipmentSnapshot = (EquipmentSnapshot) mappingService.mapESponderEntityDTO(snapshotDTO, EquipmentSnapshot.class);
		return (EquipmentSnapshotDTO) mappingService.mapESponderEntity(updateSnapshot(equipmentSnapshot, userID), EquipmentSnapshotDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public EquipmentSnapshot updateSnapshot(EquipmentSnapshot snapshot, Long userID) {
		equipmentSnapshotCrudService.update(snapshot);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteSnapshotRemote(Long equipmentSnapshotId, Long userID) {
		deleteSnapshot(equipmentSnapshotId, userID);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteSnapshot(Long equipmentSnapshotId, Long userID) {
		equipmentSnapshotCrudService.delete(EquipmentSnapshot.class, equipmentSnapshotId);
	}

}
