package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.util.ejb.ServiceLocator;

public class ActorSupervisorFieldConverter implements CustomConverter {
	
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if (sourceClass == Actor.class && source != null) {
			Actor sourceActor = (Actor) source;
			ActorDTO destActorDTO = new ActorDTO();
			destActorDTO.setId(sourceActor.getId());
			destination = destActorDTO;
		}
		else if(sourceClass == ActorDTO.class && source!=null) { 
			ActorDTO supervisor = (ActorDTO) source;
			Actor destActor = (Actor) this.getMappingService().mapESponderEntityDTO((ESponderEntityDTO) supervisor, (Class<? extends ESponderEntity<Long>>) destinationClass );
			destination = destActor;
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
