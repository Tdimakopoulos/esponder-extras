package eu.esponder.controller.crisis.action;

import javax.ejb.Local;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.crisis.action.ActionPart;

@Local
public interface ActionService {
	
	public Action findActionById(Long actionID);

	public Action findActionByTitle(String title);
	
	public Action createAction(Action action, Long userID);
	
	public ActionPart findActionPartById(Long actionID);
	
	public ActionPart findActionPartByTitle(String title);
	
	public ActionPart createActionPart(ActionPart actionPart, Long userID);
	
	public CrisisContext findCrisisContextById(Long actionID);
	
	public CrisisContext findCrisisContextByTitle(String title);
	
	public CrisisContext createCrisisContext(CrisisContext crisisContext, Long userID);

}
