package eu.esponder.controller.crisis.action;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;

@Remote
public interface ActionRemoteService {

	public ActionDTO findActionDTOById(Long actionID);

	public ActionDTO findActionDTOByTitle(String title);
	
	public ActionDTO createActionRemote(ActionDTO actionDTO, Long userID);
	
	public ActionPartDTO findActionPartDTOById(Long actionPartID);
	
	public ActionPartDTO findActionPartDTOByTitle(String title);
	
	public ActionPartDTO createActionPartRemote(ActionPartDTO actionPartDTO, Long userID);
	
	public CrisisContextDTO findCrisisContextDTOById(Long actionID);
	
	public CrisisContextDTO findCrisisContextDTOByTitle(String title);

	public CrisisContextDTO createCrisisContextRemote(CrisisContextDTO crisisContextDTO, Long userID);

	
	
	
	
	
	
	
	

	

	

	

	

	

	

	



	

}
