package eu.esponder.controller.crisis.user.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.crisis.user.UserService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.model.user.ESponderUser;

@Stateless
@SuppressWarnings("unchecked")
public class UserBean implements UserService, UserRemoteService {
	
	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	//-------------------------------------------------------------------------
	
	@Override
	public ESponderUser findById(Long userID) {
		return (ESponderUser) crudService.find(ESponderUser.class, userID);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public ESponderUser findByUserName(String userName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userName", userName);
		return (ESponderUser) crudService.findSingleWithNamedQuery("ESponderUser.findByUserName", params);
	}
	
	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderUser create(ESponderUser user, Long userID) {
		crudService.create(user);
		return user;
	}
	
	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderUser update(ESponderUser user, Long userID) {
		return (ESponderUser) crudService.update(user);
	}

}
