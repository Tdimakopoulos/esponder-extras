package eu.esponder.controller.crisis;

import javax.ejb.Local;

import eu.esponder.model.type.ESponderType;

@Local
public interface TypeService extends TypeRemoteService {
	
	public ESponderType findById(Long typeId);
	
	public ESponderType findByTitle(String title);
	
	public ESponderType create(ESponderType type, Long userID);
	
}
