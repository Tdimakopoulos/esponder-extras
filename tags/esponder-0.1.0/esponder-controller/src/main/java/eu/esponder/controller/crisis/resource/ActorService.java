package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.snapshot.resource.ActorSnapshot;

@Local
public interface ActorService extends ActorRemoteService {
	
	public Actor findById(Long actorID);
	
	public Actor findByTitle(String title);
	
	public ActorSnapshot findSnapshotByDate(Long actorID, Date dateTo);
	
	public Actor create(Actor actor, Long userID);
		
	public ActorSnapshot createSnapshot(ActorSnapshot snapshot, Long userID);

	public void delete(Actor actor, Long userID);
	
	public Actor update(Actor actor, Long userID);
	
	public List<Actor> findSubordinatesById(Long actorID);
	
}
