package eu.esponder.controller.crisis.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.type.ESponderType;

@Stateless
@SuppressWarnings("unchecked")
public class TypeBean implements TypeService, TypeRemoteService {
	
	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	@EJB 
	private ESponderMappingService mappingService;
	
	//-------------------------------------------------------------------------
	@Override
	public ESponderTypeDTO findDTOById(Long typeId) throws ClassNotFoundException {
		ESponderType type = findById(typeId);
		ESponderTypeDTO typeDTO = (ESponderTypeDTO) mappingService.mapESponderEntity(type, mappingService.getDTOEntityClass(type.getClass()));
		return typeDTO;
	}
	
	
	@Override
	public ESponderType findById(Long typeId) {
		return (ESponderType) crudService.find(ESponderType.class, typeId);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public ESponderTypeDTO findDTOByTitle(String title) throws ClassNotFoundException {
		ESponderType type = findByTitle(title);
		Class<? extends ESponderTypeDTO> typeDTOClass = (Class<? extends ESponderTypeDTO>) mappingService.getEntityClass(mappingService.getDTOEntityName(type.getClass().getName()));
		ESponderTypeDTO esponderTypeDTO = (ESponderTypeDTO) mappingService.mapESponderEntity((ESponderEntity<Long>) type, typeDTOClass);
		return esponderTypeDTO;
	}
	
	@Override
	public ESponderType findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (ESponderType) crudService.findSingleWithNamedQuery("ESponderType.findByTitle", params);
	}

	//-------------------------------------------------------------------------
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderTypeDTO createRemote(ESponderTypeDTO typeDTO, Long userID) throws ClassNotFoundException {
		ESponderType type = (ESponderType) mappingService.mapESponderEntityDTO(typeDTO, mappingService.getManagedEntityClass(typeDTO.getClass()));
		type = create(type, userID);
		return (ESponderTypeDTO) mappingService.mapESponderEntity(type, mappingService.getDTOEntityClass(type.getClass()));
		
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderType create(ESponderType type, Long userID) {
		return (ESponderType) crudService.create(type);
	}
	
	
	
	
}
