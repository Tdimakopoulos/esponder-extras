package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.snapshot.resource.ActorSnapshot;

@Stateless
public class ActorBean implements ActorService, ActorRemoteService {

	@EJB
	private CrudService<Actor> actorCrudService;

	@EJB
	private CrudService<ActorSnapshot> actorSnapshotCrudService;

	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	@Override
	public ActorDTO findByIdRemote(Long actorID) {
		return (ActorDTO) mappingService.mapESponderEntity(findById(actorID), ActorDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	public Actor findById(Long actorID) {
		return (Actor) actorCrudService.find(Actor.class, actorID);
	}

	//-------------------------------------------------------------------------

	@Override
	public ActorDTO findByTitleRemote(String title) {
		return (ActorDTO) mappingService.mapESponderEntity(findByTitle(title), ActorDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	public Actor findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Actor) actorCrudService.findSingleWithNamedQuery("Actor.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	@Override
	public List<ActorDTO> findSubordinatesByIdRemote(Long actorID) {
		return (List<ActorDTO>) mappingService.mapESponderEntity(findSubordinatesById(actorID), ActorDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	public List<Actor> findSubordinatesById(Long actorID) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("actorID", actorID);
		return  actorCrudService.findWithNamedQuery("Actor.findSubordinatesForActor", parameters); 
	}

	//-------------------------------------------------------------------------

	@Override
	public ActorSnapshotDTO findSnapshotByDateRemote(Long actorID, Date maxDate) {
		return (ActorSnapshotDTO) mappingService.mapESponderEntity(findSnapshotByDate(actorID, maxDate), ActorSnapshotDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	public ActorSnapshot findSnapshotByDate(Long actorID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("actorID", actorID);
		params.put("maxDate", maxDate);
		ActorSnapshot snapshot =
				(ActorSnapshot) actorSnapshotCrudService.findSingleWithNamedQuery("ActorSnapshot.findByActorAndDate", params);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorDTO createRemote(ActorDTO actorDTO, Long userID) {
		Actor actor = (Actor) mappingService.mapESponderEntityDTO(actorDTO, Actor.class);
		return (ActorDTO) mappingService.mapESponderEntity(create(actor, userID), ActorDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Actor create(Actor actor, Long userID) {
		/**
		 * TODO: Check whether the following code was actually useful. 
		 * Otherwise remove it!
		 */
//		actor.setActorType((ActorType)typeService.findById(actor.getActorType().getId()));
//		if (null != actor.getSupervisor()) {
//			actor.setSupervisor(this.findById(actor.getSupervisor().getId()));
//		}
//		if (null != actor.getOperationsCentre()) {
//			actor.setOperationsCentre(operationsCentreService.findById(actor.getOperationsCentre().getId()));
//		}
		
		return actorCrudService.create(actor);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteRemote(ActorDTO actorDTO, Long userID) {
		Actor actor = (Actor) mappingService.mapESponderEntityDTO(actorDTO, Actor.class);
		this.delete(actor, userID);
	}

	//-------------------------------------------------------------------------

	@Override
	public void delete(Actor actor, Long userID) {
		actorCrudService.delete(actor);
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorDTO updateRemote(ActorDTO actorDTO, Long userID) {
		Actor actor = (Actor) mappingService.mapESponderEntityDTO(actorDTO, Actor.class);
		return (ActorDTO) mappingService.mapESponderEntity(update(actor, userID), ActorDTO.class);
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Actor update(Actor actor, Long userID) {
		return (Actor) actorCrudService.update(actor);
	}

	//-------------------------------------------------------------------------		

	@Override
	public ActorSnapshotDTO createSnapshotRemote(ActorSnapshotDTO snapshotDTO, Long userID) {
		ActorSnapshot actorSnapshot = (ActorSnapshot) mappingService.mapESponderEntityDTO(snapshotDTO, ActorSnapshot.class);
		return (ActorSnapshotDTO) mappingService.mapESponderEntity(createSnapshot(actorSnapshot, userID), ActorSnapshotDTO.class);
	}

	//-------------------------------------------------------------------------		

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActorSnapshot createSnapshot(ActorSnapshot snapshot, Long userID) {
		snapshot.setActor((Actor)this.findById(snapshot.getActor().getId()));
		actorSnapshotCrudService.create(snapshot);
		return snapshot;
	}


}
