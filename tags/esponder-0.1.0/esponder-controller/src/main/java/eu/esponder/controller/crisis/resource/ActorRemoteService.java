package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;

@Remote
public interface ActorRemoteService {

	public ActorDTO findByIdRemote(Long actorID);

	public ActorDTO findByTitleRemote(String title);

	public ActorSnapshotDTO findSnapshotByDateRemote(Long actorID, Date maxDate);

	public ActorDTO createRemote(ActorDTO actorDTO, Long userID);

	public ActorSnapshotDTO createSnapshotRemote(ActorSnapshotDTO snapshotDTO,
			Long userID);

	public void deleteRemote(ActorDTO actorDTO, Long userID);

	public ActorDTO updateRemote(ActorDTO actorDTO, Long userID);

	public List<ActorDTO> findSubordinatesByIdRemote(Long actorID);
	
}
