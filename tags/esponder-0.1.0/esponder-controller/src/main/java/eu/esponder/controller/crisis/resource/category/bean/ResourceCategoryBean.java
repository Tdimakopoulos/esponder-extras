package eu.esponder.controller.crisis.resource.category.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService;
import eu.esponder.controller.crisis.resource.category.ResourceCategoryService;
import eu.esponder.controller.generic.GenericService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.EquipmentCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OperationsCentreCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.dto.model.type.RankTypeDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.PersonnelCompetence;
import eu.esponder.model.crisis.resource.category.ConsumableResourceCategory;
import eu.esponder.model.crisis.resource.category.EquipmentCategory;
import eu.esponder.model.crisis.resource.category.OperationsCentreCategory;
import eu.esponder.model.crisis.resource.category.OrganisationCategory;
import eu.esponder.model.crisis.resource.category.PersonnelCategory;
import eu.esponder.model.crisis.resource.category.ResourceCategory;
import eu.esponder.model.crisis.resource.category.ReusableResourceCategory;
import eu.esponder.model.type.ConsumableResourceType;
import eu.esponder.model.type.DisciplineType;
import eu.esponder.model.type.EquipmentType;
import eu.esponder.model.type.OperationsCentreType;
import eu.esponder.model.type.OrganisationType;
import eu.esponder.model.type.RankType;
import eu.esponder.model.type.ReusableResourceType;
import eu.esponder.util.logger.ESponderLogger;

@Stateless
public class ResourceCategoryBean implements ResourceCategoryService, ResourceCategoryRemoteService {

	@EJB
	private ESponderMappingService mappingService;

	@EJB
	private CrudService<ResourceCategory> crudService;

	@EJB
	private CrudService<PersonnelCategory> personnelCrudService;
	
	@EJB
	private TypeService typeService;
	
	@EJB
	private GenericService genericService;

	@Override
	public ResourceCategoryDTO create(ResourceCategoryDTO resourceCategoryDTO, Long userID) throws ClassNotFoundException {
		Class<? extends ESponderEntity<Long>> targetClass = (Class<? extends ESponderEntity<Long>>) mappingService.getManagedEntityClass(resourceCategoryDTO.getClass());
		ResourceCategory resourceCategory = (ResourceCategory) mappingService.mapESponderEntityDTO(resourceCategoryDTO, targetClass);
		ResourceCategory resourceCategoryPersisted = this.findByCategory(resourceCategory); 
		if (null == resourceCategoryPersisted) {
			resourceCategoryPersisted = this.create(resourceCategory, userID);
		}
		Class<? extends ESponderEntityDTO> targetDTOClass = resourceCategoryDTO.getClass();
		resourceCategoryDTO = (ResourceCategoryDTO) mappingService.mapESponderEntity(resourceCategoryPersisted, targetDTOClass);
		return resourceCategoryDTO;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ResourceCategory create(ResourceCategory resourceCategory, Long userID) {
		return crudService.create(resourceCategory);
	}

	public ResourceCategory findByCategory(ResourceCategory resourceCategory) {
		
		if (resourceCategory instanceof OrganisationCategory) {
			return findByType(((OrganisationCategory) resourceCategory).getDisciplineType(), ((OrganisationCategory) resourceCategory).getOrganisationType());

		}
		else if (resourceCategory instanceof OperationsCentreCategory) {
			return findByType(((OperationsCentreCategory) resourceCategory).getOperationsCentreType());
		}
		else if (resourceCategory instanceof PersonnelCategory) {
			return findByType(((PersonnelCategory) resourceCategory).getRank(), ((PersonnelCategory) resourceCategory).getPersonnelCompetences(), ((PersonnelCategory) resourceCategory).getOrganisationCategory());

		}
		else if (resourceCategory instanceof EquipmentCategory) {
			return findByType(((EquipmentCategory) resourceCategory).getEquipmentType());
		}
		else if (resourceCategory instanceof ConsumableResourceCategory) {
			return findByType(((ConsumableResourceCategory) resourceCategory).getConsumableResourceType());
		}
		else if (resourceCategory instanceof ReusableResourceCategory) {
			return findByType(((ReusableResourceCategory) resourceCategory).getReusableResourceType());
		}
		else
			throw new RuntimeException("Cannot handle this type. BUG!!!");
	}

	//TODO Remove it, when the other used instead is tested to work...
	public OperationsCentreCategory findByOperationsCentreType(OperationsCentreType operationsCentreType) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("operationsCentreType", operationsCentreType);
		List<ResourceCategory> list = crudService.findWithNamedQuery("OperationsCentreCategory.findByType", parameters);
		/*
		 * TODO: refactor the following based on the instructions included in findByType(OPerationsCentreType)
		 */
		if (list.size() == 0) {
			return null;
		} else if ((list.size() == 1)) {
			return (OperationsCentreCategory) list.get(0);
		} else {
			throw new RuntimeException("Cannot handle this type. BUG!!!");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResourceCategoryDTO findDTOById(Class<? extends ResourceCategoryDTO> clz, Long resourceCategoryID) {

		try {
			Class<? extends ResourceCategory> targetClass = (Class<? extends ResourceCategory>) mappingService.getManagedEntityClass(clz);
			ResourceCategory resultCategory = findById(targetClass, resourceCategoryID);
			if(resultCategory != null)
				return (ResourceCategoryDTO) mappingService.mapESponderEntity(resultCategory, clz);
			else
				return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public OrganisationCategoryDTO findDTOByType(DisciplineTypeDTO disciplineType, OrganisationTypeDTO organizationType) {

		DisciplineType discipType = (DisciplineType) mappingService.mapESponderEntityDTO(disciplineType, DisciplineType.class);
		OrganisationType organType = (OrganisationType) mappingService.mapESponderEntityDTO(organizationType, OrganisationType.class);
		OrganisationCategory targetOrganisationCategory = findByType(discipType, organType);
		if(targetOrganisationCategory != null)
			return (OrganisationCategoryDTO) mappingService.mapESponderEntity(targetOrganisationCategory, OrganisationCategoryDTO.class);
		else
			return null;
	}

	@Override
	public EquipmentCategoryDTO findDTOByType(EquipmentTypeDTO equipmentTypeDTO) {
		EquipmentType equipmentType  = (EquipmentType) mappingService.mapESponderEntityDTO(equipmentTypeDTO, EquipmentType.class);
		EquipmentCategory equipmentCategory = findByType(equipmentType);
		if(equipmentCategory != null)
			return (EquipmentCategoryDTO) mappingService.mapESponderEntity(equipmentCategory, EquipmentCategoryDTO.class);
		else
			return null;
	}

	@Override
	public ConsumableResourceCategoryDTO findDTOByType(
			ConsumableResourceTypeDTO consumablesType) {

		ConsumableResourceType consumableResourceType  = (ConsumableResourceType) mappingService.mapESponderEntityDTO(consumablesType, ConsumableResourceType.class);
		ConsumableResourceCategory consumableCategory = findByType(consumableResourceType);
		if(consumableCategory != null)
			return (ConsumableResourceCategoryDTO) mappingService.mapESponderEntity(consumableCategory, ConsumableResourceCategoryDTO.class);
		else
			return null;
	}

	@Override
	public ReusableResourceCategoryDTO findDTOByType(
			ReusableResourceTypeDTO reusablesType) {
		/*
		 * 1. Transform
		 * 2. call @Local
		 * 3. Transform to DTO
		 */
		ReusableResourceType reusableResourceType  = (ReusableResourceType) mappingService.mapESponderEntityDTO(reusablesType, ReusableResourceType.class);
		ReusableResourceCategory reusableCategory = findByType(reusableResourceType);
		if(reusableCategory != null)
			return (ReusableResourceCategoryDTO) mappingService.mapESponderEntity(reusableCategory, ReusableResourceCategoryDTO.class);
		else
			return null;
	}

	@Override
	public OperationsCentreCategoryDTO findDTOByType(
			OperationsCentreCategoryDTO operationsCentreCategoryDTO) {
		/*
		 * 1. Transform
		 * 2. call @Local
		 * 3. Transform to DTO
		 */
		OperationsCentreType operationsCentreType  = (OperationsCentreType) mappingService.mapESponderEntityDTO(operationsCentreCategoryDTO, OperationsCentreType.class);
		OperationsCentreCategory operationsCentreCategory = findByType(operationsCentreType);
		if(operationsCentreCategory != null)
			return (OperationsCentreCategoryDTO) mappingService.mapESponderEntity(operationsCentreCategory, OperationsCentreCategoryDTO.class);
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PersonnelCategoryDTO findDTOByType(RankTypeDTO rankType,
			Set<PersonnelCompetenceDTO> competence,
			OrganisationCategoryDTO organisationCategory) {
		RankType rankTypeModel = (RankType) mappingService.mapESponderEntityDTO(rankType, RankType.class);
		Set<PersonnelCompetence> competenceModel = (Set<PersonnelCompetence>) mappingService.mapESponderEntityDTO((List<? extends ESponderEntityDTO>) competence, PersonnelCompetence.class);
		OrganisationCategory organCategory = (OrganisationCategory) mappingService.mapESponderEntityDTO(organisationCategory, OrganisationCategory.class);
		PersonnelCategory personnelCategory = findByType(rankTypeModel, competenceModel, organCategory);
		if(personnelCategory != null)
			return (PersonnelCategoryDTO) mappingService.mapESponderEntity(personnelCategory, PersonnelCategoryDTO.class);
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResourceCategory findById(Class<? extends ResourceCategory> clz, Long resourceCategoryID) {
		return crudService.find((Class<ResourceCategory>) clz, resourceCategoryID);
	}

	@Override
	public OrganisationCategory findByType(DisciplineType disciplineType,
			OrganisationType organisationType) {

		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("disciplineType", disciplineType);
		parameters.put("organisationType", organisationType);
		OrganisationCategory organisationCategory= (OrganisationCategory) crudService.findSingleWithNamedQuery("OrganisationCategory.findByType", parameters);
		if(organisationCategory != null)
			return organisationCategory;
		else
			return null;
	}

	@Override
	public EquipmentCategory findByType(EquipmentType equipmentType) {
		/*
		 * find EquipmentCategory with equipmentType 
		 * otherwise return null 
		 * (create NamedQuery)
		 */
		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("equipmentType", equipmentType);
		EquipmentCategory equipmentCategory= (EquipmentCategory) crudService.findSingleWithNamedQuery("EquipmentCategory.findByType", parameters);
		if(equipmentCategory != null)
			return equipmentCategory;
		else
			return null;
	}

	@Override
	public ConsumableResourceCategory findByType(ConsumableResourceType consumablesType) {

		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("consumableResourceType", consumablesType);
		ConsumableResourceCategory consumableCategory = (ConsumableResourceCategory) crudService.findSingleWithNamedQuery("ConsumableResourceCategory.findByType", parameters);
		if(consumableCategory != null)
			return consumableCategory;
		else
			return null;
	}

	@Override
	public ReusableResourceCategory findByType(
			ReusableResourceType reusablesType) {

		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("reusableResourceType", reusablesType);
		ReusableResourceCategory reusableCategory = (ReusableResourceCategory) crudService.findSingleWithNamedQuery("ReusableResourceCategory.findByType", parameters);
		if(reusableCategory != null)
			return reusableCategory;
		else
			return null;
	}

	@Override
	public OperationsCentreCategory findByType(
			OperationsCentreType operationsCentreType) {
		/*OperationsCentreCategory.findByType
		 * find ReusableResourceCategory with reusablesType 
		 * otherwise return null 
		 * (create NamedQuery) -> see findByOperationsCentreType()
		 */
		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("operationsCentreType", operationsCentreType);
		OperationsCentreCategory operationsCentreCategory = (OperationsCentreCategory) crudService.findSingleWithNamedQuery("OperationsCentreCategory.findByType", parameters);
		if(operationsCentreCategory != null)
			return operationsCentreCategory;
		else
			return null;
	}

	
	@Override
	public PersonnelCategory findByType(RankType rankType, Set<PersonnelCompetence> personnelCompetence, OrganisationCategory organisationCategory) {
		/*
		 * find exact match for PersonnelCategory with rankType AND personnelCompetences and organizationCategory
		 * otherwise return null (if null is returned then a new personnel Category will be created by the application
		 */
		List<PersonnelCategory> categoryList = new ArrayList<PersonnelCategory>();
		HashMap<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("rankType", rankType);
		parameters.put("organisationCategory", organisationCategory);
		List<PersonnelCategory> intermediateResults = personnelCrudService.findWithNamedQuery("PersonnelCategory.findByType", parameters);

		for(PersonnelCategory category : intermediateResults) {

			if(personnelCompetence.size() == category.getPersonnelCompetences().size()) {
				
				boolean found = true;
				for(PersonnelCompetence icompParam : personnelCompetence) {
					if(found == true) {
						for(PersonnelCompetence icompResult : category.getPersonnelCompetences()) {
							if(icompResult.equals(icompParam)) {
								found = true;
								break;
							}
							else 
								found = false;
						}
					}
					else 
						break;
				}
				if(found == true){
					categoryList.add(category);
				}
				else
					break;
			}
			else {
				ESponderLogger.info(this.getClass(), "Category does not have same size as the requested.");
				break;
			}
			
		}
		
		
		if(categoryList.size() == 0) {
			throw new RuntimeException("No Personnel Category that matches the selected criteria has been found.");
		}
		else if(categoryList.size() > 1) {
			throw new RuntimeException("Two categories found matching the selected criteria, something is wrong.");
		}
		else if(categoryList.size() == 1) {
			return categoryList.get(0);
		}
		return null;
	}
	

	@Override
	public OrganisationCategoryDTO updateOrganizationCategory(Long organisationCategoryId, Long disciplineTypeId, Long organisationTypeId, Long userId) {
		
		OrganisationCategory organisationCategory = (OrganisationCategory) findById(OrganisationCategory.class, organisationCategoryId);
		if(organisationCategory != null) {
			DisciplineType disciplineType = (DisciplineType) typeService.findById(disciplineTypeId);
			OrganisationType organisationType = (OrganisationType) typeService.findById(organisationTypeId);
			OrganisationCategory organisationCategoryPersisted = updateOrganizationCategory(organisationCategory, disciplineType, organisationType, userId);
			return (OrganisationCategoryDTO) mappingService.mapESponderEntity(organisationCategoryPersisted, OrganisationCategoryDTO.class);
		}
		else
			return null;
	}

	@Override
	public EquipmentCategoryDTO updateEquipmentCategory(Long equipmentCategoryId, Long equipmentTypeId, Long userId) {
		
		EquipmentCategory equipmentCategory = (EquipmentCategory) findById(EquipmentCategory.class, equipmentCategoryId);
		if(equipmentCategory != null){
			EquipmentType equipmentType = (EquipmentType) typeService.findById(equipmentTypeId);
			EquipmentCategory equipmentCategoryPersisted = updateEquipmentCategory(equipmentCategory, equipmentType, userId);
			return (EquipmentCategoryDTO) mappingService.mapESponderEntity(equipmentCategoryPersisted, EquipmentCategoryDTO.class);
		}
		else
			return null;
	}

	@Override
	public ConsumableResourceCategoryDTO updateConsumableResourceCategory( Long consumableCategoryId, Long consumableTypeId, Long userId) {
		
		ConsumableResourceCategory consumableResourceCategory = (ConsumableResourceCategory) findById(ConsumableResourceCategory.class, consumableCategoryId);
		if(consumableResourceCategory != null) {
			ConsumableResourceType consumableResourceType = (ConsumableResourceType) typeService.findById(consumableTypeId);
			ConsumableResourceCategory consumableResourceCategoryPersisted = updateConsumableResourceCategory(consumableResourceCategory, consumableResourceType, userId);
			return (ConsumableResourceCategoryDTO) mappingService.mapESponderEntity(consumableResourceCategoryPersisted, ConsumableResourceCategoryDTO.class);			
		}
		else
			return null;
	}

	@Override
	public ReusableResourceCategoryDTO updateReusableResourceCategory(Long reusableCategoryId, Long reusableTypeId, Long userId) {
		
		ReusableResourceCategory reusableresourceCategory = (ReusableResourceCategory) findById(ReusableResourceCategory.class, reusableCategoryId);
		if(reusableresourceCategory != null) {
			ReusableResourceType reusableResourceType = (ReusableResourceType) typeService.findById(reusableTypeId);
			ReusableResourceCategory reusableResourceCategoryPersisted = updateReusableResourceCategory(reusableresourceCategory, reusableResourceType, userId);
			return (ReusableResourceCategoryDTO) mappingService.mapESponderEntity(reusableResourceCategoryPersisted, ReusableResourceCategoryDTO.class);			
		}
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PersonnelCategoryDTO updatePersonnelCategory( Long personnelCategoryId, Set<Long> competenceId, Long rankId, Long organisationCategoryId, Long userId) throws InstantiationException, IllegalAccessException {
		
		PersonnelCategory personnelCategory = (PersonnelCategory) findById(PersonnelCategory.class, personnelCategoryId);
		if(personnelCategory != null) {
			RankType rankType = (RankType) typeService.findById(rankId);
			OrganisationCategory organisationCategory = (OrganisationCategory) findById(OrganisationCategory.class, organisationCategoryId);
			
			EsponderQueryRestriction restriction = genericService.createCriteriaforBulkFindById(competenceId);
			Set<PersonnelCompetence> competenceSet =  (Set<PersonnelCompetence>) genericService.getEntities(PersonnelCompetence.class, restriction, 0, 0);
			
			PersonnelCategory personnelCategoryPersisted = updatePersonnelCategory(personnelCategory, competenceSet, rankType, organisationCategory, userId);
			return (PersonnelCategoryDTO) mappingService.mapESponderEntity(personnelCategoryPersisted, PersonnelCategoryDTO.class);
		}
		else
			return null;
	}

	@Override
	public OrganisationCategory updateOrganizationCategory(OrganisationCategory organisationCategory, DisciplineType disciplineType, OrganisationType organizationType, Long userId) {
		OrganisationCategory organisationCategoryTarget = (OrganisationCategory) findById(OrganisationCategory.class, organisationCategory.getId());
		organisationCategory.setDisciplineType(disciplineType);
		organisationCategory.setOrganisationType(organizationType);
		mappingService.mapEntityToEntity(organisationCategory, organisationCategoryTarget);
		return (OrganisationCategory) crudService.update(organisationCategoryTarget);
	}

	@Override
	public EquipmentCategory updateEquipmentCategory(EquipmentCategory equipmentCategory, EquipmentType equipmentType, Long userId) {
		EquipmentCategory equipmentCategoryTarget = (EquipmentCategory) findById(EquipmentCategory.class, equipmentCategory.getId());
		equipmentCategory.setEquipmentType(equipmentType);
		mappingService.mapEntityToEntity(equipmentCategory, equipmentCategoryTarget);
		return (EquipmentCategory) crudService.update(equipmentCategoryTarget);
	}

	@Override
	public ConsumableResourceCategory updateConsumableResourceCategory(	ConsumableResourceCategory consumableCategory,	ConsumableResourceType consumableType, Long userId) {
		
		ConsumableResourceCategory consumableResourceCategoryTarget = (ConsumableResourceCategory) findById(ConsumableResourceCategory.class, consumableCategory.getId());
		consumableCategory.setConsumableResourceType(consumableType);
		mappingService.mapEntityToEntity(consumableCategory, consumableResourceCategoryTarget);
		return (ConsumableResourceCategory) crudService.update(consumableResourceCategoryTarget);
	}

	@Override
	public ReusableResourceCategory updateReusableResourceCategory(	ReusableResourceCategory reusableCategory, ReusableResourceType reusableType, Long userId) {
		
		ReusableResourceCategory reusableResourceCategoryTarget = (ReusableResourceCategory) findById(ReusableResourceCategory.class, reusableCategory.getId());
		reusableCategory.setReusableResourceType(reusableType);
		mappingService.mapEntityToEntity(reusableCategory, reusableResourceCategoryTarget);
		return (ReusableResourceCategory) crudService.update(reusableResourceCategoryTarget);
	}

	@Override
	public PersonnelCategory updatePersonnelCategory( PersonnelCategory personnelCategory, Set<PersonnelCompetence> competences, RankType rank, OrganisationCategory organizationCategory, Long userId) {
		PersonnelCategory personnelCategoryTarget = (PersonnelCategory) findById(PersonnelCategory.class, personnelCategory.getId());
		personnelCategory.setOrganisationCategory(organizationCategory);
		personnelCategory.setRank(rank);
		personnelCategory.setPersonnelCompetences(competences);
		mappingService.mapEntityToEntity(personnelCategory, personnelCategoryTarget);
		return (PersonnelCategory) crudService.update(personnelCategoryTarget);
	}

}
