package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.generic.GenericService;
import eu.esponder.model.ESponderEntity;
import eu.esponder.util.ejb.ServiceLocator;

public class ESponderEntityDTOIdToESPonderEntityConverter implements CustomConverter {
	
	protected GenericService getGenericService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {
		
		// Model to DTO
		// ESponderEntity --> ESponderEntityId
		if (source instanceof ESponderEntity && source != null) {
			ESponderEntity<Long> sourceEntity = (ESponderEntity<Long>) source;
			Long destField = sourceEntity.getId();
			destination = destField;
		}
		else if(sourceClass == Long.class && source!=null) {
			Object destObject = this.getGenericService().getEntity((Class<? extends ESponderEntity<Long>>) destinationClass, (Long) source);
			if(destObject != null) {
				destination = destObject;
			}
			else
				throw new RuntimeException();
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
