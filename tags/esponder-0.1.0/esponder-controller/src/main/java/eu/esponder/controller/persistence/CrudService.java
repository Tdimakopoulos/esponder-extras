package eu.esponder.controller.persistence;

import javax.ejb.Local;

@Local
public interface CrudService<T> extends CrudRemoteService<T> {
    
}
