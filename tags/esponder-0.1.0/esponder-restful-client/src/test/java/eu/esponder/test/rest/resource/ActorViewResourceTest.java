package eu.esponder.test.rest.resource;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.rest.client.ResteasyClient;
import eu.esponder.util.jaxb.Parser;

public class ActorViewResourceTest {

	String GETID_GENERIC_QUERY_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/resource/actor/getID";
	String GETTITLE_GENERIC_QUERY_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/resource/actor/getTitle";
	String POST_GENERIC_QUERY_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/resource/actor/post";
	String PUT_GENERIC_QUERY_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/resource/actor/put";
	String DELETE_GENERIC_QUERY_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/resource/actor/delete";

	//Returns XML
	@Test
	public ActorDTO getActorByIDXml() throws RuntimeException, Exception{
		Parser parser = new Parser(new Class[] {ActorDTO.class});

		ResteasyClient getClient = new ResteasyClient(GETID_GENERIC_QUERY_SERVICE_URI, "application/xml");
		System.out.println("Client for getID created successfully...\n");

		Map<String, String> params = this.getIDServiceParameters();

		String resultXML = getClient.get(params);
		

		System.out.println("\n*********FULL_XML*******************************\n");
		System.out.println(resultXML+"\n\n");

		ActorDTO actorDTO = (ActorDTO) parser.unmarshal(resultXML);
		System.out.println("\n*****************ACTOR**************************\n");
		System.out.println(actorDTO.toString());
		System.out.println("\n************************************************\n");
		
		return actorDTO;
	}

	//returns JSON
	@SuppressWarnings("unused")
	@Test
	public void getActorByIDJson() throws RuntimeException, Exception{

		ResteasyClient getClient = new ResteasyClient(GETID_GENERIC_QUERY_SERVICE_URI, "application/json");
		System.out.println("Client for getID created successfully...");

		Map<String, String> params = this.getIDServiceParameters();

		String resultJSON = getClient.get(params);

		System.out.println("\n**************JSON******************************\n");
		System.out.println(resultJSON+"\n\n");

		ObjectMapper mapper = new ObjectMapper();
		ActorDTO actorDTO = mapper.readValue(resultJSON, ActorDTO.class);
	}

//	@Test
	public ActorDTO getActorByTitle() throws RuntimeException, Exception{
		Parser parser = new Parser(new Class[] {ActorDTO.class});

		ResteasyClient getClient = new ResteasyClient(GETTITLE_GENERIC_QUERY_SERVICE_URI, "application/xml");
		System.out.println("Client for getTitle created successfully...");

		Map<String, String> params = this.getTitleServiceParameters();

		String resultXML = getClient.get(params);
		ActorDTO actorDTO = (ActorDTO) parser.unmarshal(resultXML);

		System.out.println("\n*********FULL_XML*******************************\n");
		System.out.println(resultXML+"\n\n");

		System.out.println("\n*****************ACTOR**************************\n");
		System.out.println(actorDTO.toString());
		System.out.println("\n************************************************\n");

		return actorDTO;
	}

	@Test
	public void createActor() throws RuntimeException, Exception {
		ActorDTO actorDTO = getActorByIDXml();
//		actorDTO.setEquipmentSet(null);
		actorDTO.setSubordinates(null);
//		Parser parser = new Parser(new Class[] {ActorDTO.class});
//		System.out.println("1)\tafter marshall :");
//		System.out.println(parser.marshall(actorDTO));
		
		ObjectMapper mapper = new ObjectMapper();
		String entityStr = mapper.writeValueAsString(actorDTO);
		
		System.out.println("\n******* JSON START *******\n");
		System.out.println(entityStr);
		System.out.println("\n******** JSON END ********\n");
		
		
//		ResteasyClient postClient = new ResteasyClient(POST_GENERIC_QUERY_SERVICE_URI, "application/xml");
//		Map<String, String> serviceParams = postCreateServiceParameters();
//		String xmlResponse = postClient.post(serviceParams, parser.marshall(actorDTO));
//		System.out.println("\n\n" + xmlResponse + "\n\n");
	}

	private Map<String, String> getIDServiceParameters() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		params.put("actorID", "1");
		return params;
	}

	private Map<String, String> getTitleServiceParameters() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		params.put("actorTitle", "FR #1.1");
		return params;
	}

	@SuppressWarnings("unused")
	private Map<String, String> postCreateServiceParameters() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		return params;
	}
}
