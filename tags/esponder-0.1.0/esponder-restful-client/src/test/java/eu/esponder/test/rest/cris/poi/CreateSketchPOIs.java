package eu.esponder.test.rest.cris.poi;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.view.MapPointDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.rest.client.ResteasyClient;
import eu.esponder.util.jaxb.Parser;

public class CreateSketchPOIs {
	
	private String SKETCHPOI_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/view/sketch";

	@Test
	public void CreateSketchPOIWithXml () throws ClassNotFoundException, Exception {

		Parser parser = new Parser(new Class[] {SketchPOIDTO.class});
		SketchPOIDTO sketchPOIDTO = new SketchPOIDTO();
		sketchPOIDTO.setTitle("new Test SketchPOI");
		MapPointDTO mapPoint = new MapPointDTO();
		PointDTO point = new PointDTO();
		point.setLatitude(BigDecimal.TEN);
		point.setLongitude(BigDecimal.TEN);
		mapPoint.setPoint(point);
		mapPoint.setTitle("new map point test1");
		HashSet<MapPointDTO> set = new HashSet<MapPointDTO>();
		
		set.add(mapPoint);
		sketchPOIDTO.setPoints(set);
		
		if(sketchPOIDTO !=null) {
			
			String serviceName = SKETCHPOI_SERVICE_URI + "/createWithReturn";
			ResteasyClient postClient = new ResteasyClient(serviceName, "application/xml");
			System.out.println("Client for createGenericEntity created successfully...");
			String xmlPayload = parser.marshall(sketchPOIDTO);
			
			printXML(xmlPayload);
			
			Map<String, String> params =  CreateSketchServiceParameters(new Long(2));
			String resultXML = postClient.post(params, xmlPayload);
			
			printXML(resultXML);
			
			SketchPOIDTO sketchDTO = (SketchPOIDTO) parser.unmarshal(resultXML);

			System.out.println("\n*****************SKETCHPOI**************************");
			System.out.println(sketchDTO.toString());
			System.out.println("\n************************************************");
			
		}
	}
	
	
	@Test
	public void CreateSketchPOIWithJson () throws ClassNotFoundException, Exception {

		SketchPOIDTO sketchPOIDTO = new SketchPOIDTO();
		sketchPOIDTO.setTitle("new Test SketchPOI2");
		MapPointDTO mapPoint = new MapPointDTO();
		PointDTO point = new PointDTO();
		point.setLatitude(BigDecimal.TEN);
		point.setLongitude(BigDecimal.TEN);
		mapPoint.setPoint(point);
		mapPoint.setTitle("new map point test2");
		HashSet<MapPointDTO> set = new HashSet<MapPointDTO>();
		
		set.add(mapPoint);
		sketchPOIDTO.setPoints(set);
		
		if(sketchPOIDTO !=null) {
			
			String serviceName = SKETCHPOI_SERVICE_URI + "/createWithReturn";
			ResteasyClient postClient = new ResteasyClient(serviceName, "application/json");
			System.out.println("Client for createGenericEntity created successfully...");
			ObjectMapper mapper = new ObjectMapper();
			
			String jsonPayload = mapper.writeValueAsString(sketchPOIDTO);
			
			printJSON(jsonPayload);
			
			Map<String, String> params =  CreateSketchServiceParameters(new Long(2));
			String resultJSON = postClient.post(params, jsonPayload);
			
			printJSON(resultJSON);
			
			SketchPOIDTO sketchDTO = mapper.readValue(resultJSON, SketchPOIDTO.class);

			System.out.println("\n*****************SKETCHPOI**************************");
			System.out.println(sketchDTO.toString());
			System.out.println("\n************************************************");
			
		}
	}
	
	@SuppressWarnings("unused")
	private Map<String, String> getIDServiceParameters(Long sketchPOIId) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("sketchPOIId", sketchPOIId.toString());
		params.put("userID", "1");
		return params;
	}
	
	private Map<String, String> CreateSketchServiceParameters(Long operationsCentreID) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		params.put("operationsCentreID", operationsCentreID.toString());
		return params;
	}
	
	
	private void printJSON(String jsonStr) {
		System.out.println("\n\n******* JSON * START *******");
		System.out.println(jsonStr);
		System.out.println("******** JSON * END ********\n\n");
	}
	
	private void printXML(String xmlStr) {
		System.out.println("\n\n******* XML * START *******");
		System.out.println(xmlStr);
		System.out.println("******** XML * END ********\n\n");
	}

}
