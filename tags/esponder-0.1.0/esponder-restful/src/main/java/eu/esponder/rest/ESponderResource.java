package eu.esponder.rest;

import javax.naming.NamingException;

import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.generic.GenericService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.util.ejb.ServiceLocator;

public abstract class ESponderResource {
	
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected ESponderRemoteMappingService getMappingRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected OperationsCentreService getOperationsCentreService() {
		try {
			return ServiceLocator.getResource("esponder/OperationsCentreBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
		
	protected OperationsCentreRemoteService getOperationsCentreRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/OperationsCentreBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
		
	protected GenericService getGenericService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected GenericRemoteService getGenericRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
		
}
