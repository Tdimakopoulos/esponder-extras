package eu.esponder.datasync.db.postgresql;

import org.jumpmind.db.platform.IDatabasePlatform;
import org.jumpmind.symmetric.service.IParameterService;

public class GreenplumSymmetricDialect extends PostgreSqlSymmetricDialect {

    public GreenplumSymmetricDialect(IParameterService parameterService, IDatabasePlatform platform) {
        super(parameterService, platform);
        this.triggerTemplate = new GreenplumTriggerTemplate(this);
    }
    
}
