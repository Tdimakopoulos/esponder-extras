package eu.esponder.fru;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		DataFusionController.loadConfig();
		DataFusionController.init();

	}

}
