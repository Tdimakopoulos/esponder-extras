package eu.esponder.test.sensor;

import java.math.BigDecimal;

import org.testng.annotations.Test;

import com.javadocmd.simplelatlng.LatLng;
import com.javadocmd.simplelatlng.LatLngTool;
import com.javadocmd.simplelatlng.util.LengthUnit;

import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;

public class LocationSensorMeasurementTest {

	@Test
	public void testLocation() {
		getMeasurement();
	}

	public SensorMeasurementStatisticDTO getMeasurement() {

		LocationSensorMeasurementDTO measurement = new LocationSensorMeasurementDTO();

		/*
		 * Syggrou - Fix as starting point, end of Syggrou avenue as ending point
		 */

		PointDTO startPointDTO = new PointDTO(new BigDecimal(37.959146), new BigDecimal(23.719933), BigDecimal.ZERO);
		PointDTO endPointDTO = new PointDTO(new BigDecimal(37.940263), new BigDecimal(23.6956), BigDecimal.ZERO);


		/*
		 * Move for 10m on the line from 'start' to 'end' pointDTOs
		 */
		measurement.setPoint(getMeasurementValue(startPointDTO, endPointDTO, new Double(0.01)));

		SensorMeasurementStatisticDTO statistic = new SensorMeasurementStatisticDTO();
		statistic.setStatistic(measurement);
		return statistic;
	}


	private PointDTO getMeasurementValue(PointDTO startPointDTO, PointDTO endPointDTO, double step) {
		/*
		 * use range (choose proper type) to determine randomness of generated values
		 */
		LatLng startPoint = new LatLng(startPointDTO.getLatitude().doubleValue(), startPointDTO.getLatitude().doubleValue());
		LatLng endPoint = new LatLng(endPointDTO.getLatitude().doubleValue(), endPointDTO.getLatitude().doubleValue());
		
		System.out.println("\n\nDistance between points (in meters) is : "+LatLngTool.distance(startPoint, endPoint, LengthUnit.METER)+"\n");
		LatLng intermediatePoint = getNewPoint(startPoint, endPoint, step);

		return new PointDTO(new BigDecimal(intermediatePoint.getLatitude()), new BigDecimal(intermediatePoint.getLongitude()), BigDecimal.ZERO);

	}


	// Check if these refer to radians or degrees
	private LatLng getNewPoint(LatLng startPoint, LatLng destPoint, double step) {
		//           LatLng latlon = LatLng.random();

		/*
		 * Earth Radius -> Check if it should be in meters or Km
		 */
		//	double R = 3.781 * 10E3;
		double R = 6.371;
		double startLat = startPoint.getLatitude(), startLon = startPoint.getLongitude();
		double brng = getBearing(startPoint, destPoint, R);
		double d = step;

		double destLat = Math.asin( Math.sin(startLat)*Math.cos(d/R) + 
				Math.cos(startLat)*Math.sin(d/R)*Math.cos(brng) );
		double destLon = startLon + Math.atan2(Math.sin(brng)*Math.sin(d/R)*Math.cos(startLat), 
				Math.cos(d/R)-Math.sin(startLat)*Math.sin(destLat));

		LatLng destinationPoint = new LatLng(destLat, destLon);
		return destinationPoint;
		
	}

	// Check if these refer to radians or degrees

	@SuppressWarnings("unused")
	private double getBearing(LatLng startPoint, LatLng destPoint, double R) {

		double startLat = startPoint.getLatitude(), startLon = startPoint.getLongitude();
		double destLat = destPoint.getLatitude(), destLon = destPoint.getLongitude();

		double dLat = startLat - destLat;
		double dLon = startLon - destLon;

		double dPhi = Math.log(Math.tan(destLat/2+Math.PI/4)/Math.tan(startLat/2+Math.PI/4));
		double q = (dPhi != 0) ? dLat/dPhi : Math.cos(startLat);  // E-W line gives dPhi=0

		// if dLon over 180° take shorter rhumb across anti-meridian:
		if (Math.abs(dLon) > Math.PI) {
			dLon = dLon>0 ? -(2*Math.PI-dLon) : (2*Math.PI+dLon);
		}

		double d = Math.sqrt(dLat*dLat + q*q*dLon*dLon) * R;
		double brng = Math.atan2(dLon, dPhi);
		return brng;
	}


}
