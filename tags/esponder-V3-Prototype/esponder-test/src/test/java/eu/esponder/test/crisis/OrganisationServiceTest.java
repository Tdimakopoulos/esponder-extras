package eu.esponder.test.crisis;

import java.math.BigDecimal;
import java.util.List;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.resource.OrganisationDTO;
import eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionExpressionEnumDTO;
import eu.esponder.dto.model.snapshot.location.AddressDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class OrganisationServiceTest extends ControllerServiceTest {	

	@SuppressWarnings("unused")
	@Test(groups = "createResources")
	public void CreateOrganisations() throws InstantiationException, IllegalAccessException, Exception {

		//Addresses
		AddressDTO address1 = new AddressDTO();
		address1.setCountry("Greece");
		address1.setNumber("25");
		address1.setPrefecture("Attica");
		address1.setStreet("Axarnon Avenue");
		address1.setZipCode("17124");

		AddressDTO address2 = new AddressDTO();
		address2.setCountry("Greece");
		address2.setNumber("35");
		address2.setPrefecture("Attica");
		address2.setStreet("Alexandras Avenue");
		address2.setZipCode("13872");

		AddressDTO address3 = new AddressDTO();
		address3.setCountry("Greece");
		address3.setNumber("45");
		address3.setPrefecture("Attica");
		address3.setStreet("Syggrou Avenue");
		address3.setZipCode("14234");


		//	Points
		PointDTO point1 = new PointDTO();
		point1.setAltitude(new BigDecimal(1));
		point1.setLatitude(new BigDecimal(1));
		point1.setLongitude(new BigDecimal(1));

		PointDTO point2 = new PointDTO();
		point2.setAltitude(new BigDecimal(5));
		point2.setLatitude(new BigDecimal(5));
		point2.setLongitude(new BigDecimal(5));

		PointDTO point3 = new PointDTO();
		point3.setAltitude(new BigDecimal(10));
		point3.setLatitude(new BigDecimal(10));
		point3.setLongitude(new BigDecimal(10));


		// Responsibility Areas
		SphereDTO responsibilityArea1 = new SphereDTO();
		responsibilityArea1.setCentre(point1);
		responsibilityArea1.setRadius(new BigDecimal(10));
		responsibilityArea1.setTitle("Organisation1 Area");

		SphereDTO responsibilityArea2 = new SphereDTO();
		responsibilityArea2.setCentre(point2);
		responsibilityArea2.setRadius(new BigDecimal(10));
		responsibilityArea2.setTitle("Organisation2 Area");

		SphereDTO responsibilityArea3 = new SphereDTO();
		responsibilityArea3.setCentre(point3);
		responsibilityArea3.setRadius(new BigDecimal(10));
		responsibilityArea3.setTitle("Organisation3 Area");

		responsibilityArea1 = (SphereDTO) genericService.createEntityRemote(responsibilityArea1, this.userID);
		responsibilityArea2 = (SphereDTO) genericService.createEntityRemote(responsibilityArea2, this.userID);
		responsibilityArea3 = (SphereDTO) genericService.createEntityRemote(responsibilityArea3, this.userID);


		//	Organisation Categories

		//		DisciplineTypeDTO discipline1 = (DisciplineTypeDTO) typeService.findDTOByTitle("Fire Brigade");
		//		DisciplineTypeDTO discipline2 = (DisciplineTypeDTO) typeService.findDTOByTitle("Police Force");
		//		
		//		OrganisationTypeDTO organisationType1 = (OrganisationTypeDTO) typeService.findDTOByTitle("Headquarters");
		//		OrganisationTypeDTO organisationType2 = (OrganisationTypeDTO) typeService.findDTOByTitle("Local Station");
		//				
		//		OrganisationCategoryDTO organisationCategoryDTO1 = new OrganisationCategoryDTO();
		//		organisationCategoryDTO1 = resourceCategoryService.findDTOByType(discipline1, organisationType1);
		//		
		//		OrganisationCategoryDTO organisationCategoryDTO2 = new OrganisationCategoryDTO();
		//		organisationCategoryDTO2 = resourceCategoryService.findDTOByType(discipline1, organisationType2);
		//		
		//		OrganisationCategoryDTO organisationCategoryDTO3 = new OrganisationCategoryDTO();
		//		organisationCategoryDTO3 = resourceCategoryService.findDTOByType(discipline2, organisationType1);



		//	Organisation 1
		OrganisationDTO organisation1 = createOrganisations("Organisation1", address1, point1, "Organisation1 Area");

		//	Organisation 2
		OrganisationDTO organisation2 = createOrganisations("Organisation2", address2, point2, "Organisation2 Area");

		//	Organisation 3
		OrganisationDTO organisation3 = createOrganisations("Organisation3", address3, point3, "Organisation3 Area");
		
	}

	
	
	@Test(groups = "createResources")
	public void updateOrganisationCategories() throws ClassNotFoundException {

		DisciplineTypeDTO discipline1 = (DisciplineTypeDTO) typeService.findDTOByTitle("Fire Brigade");
		DisciplineTypeDTO discipline2 = (DisciplineTypeDTO) typeService.findDTOByTitle("Police Force");
//		DisciplineTypeDTO discipline3 = (DisciplineTypeDTO) typeService.findDTOByTitle("Coast Guard");
		
		OrganisationTypeDTO organisationType1 = (OrganisationTypeDTO) typeService.findDTOByTitle("Headquarters");
		OrganisationTypeDTO organisationType2 = (OrganisationTypeDTO) typeService.findDTOByTitle("Local Station");

		OrganisationCategoryDTO organisationCategoryDTO1 = resourceCategoryService.findOrganisationCategoryDTOByType(discipline1, organisationType1);
		OrganisationCategoryDTO organisationCategoryDTO2 = resourceCategoryService.findOrganisationCategoryDTOByType(discipline1, organisationType2);
//		OrganisationCategoryDTO organisationCategoryDTO3 = resourceCategoryService.findDTOByType(discipline2, organisationType1);

		OrganisationDTO organisationDTO1 = this.organisationService.findDTOByTitle("Organisation1");
		OrganisationDTO organisationDTO2 = this.organisationService.findDTOByTitle("Organisation2");
//		OrganisationDTO organisationDTO3 = this.organisationService.findDTOByTitle("Organisation3");

		organisationCategoryDTO1.setOrganisationId(organisationDTO1.getId());
		organisationCategoryDTO2.setOrganisationId(organisationDTO2.getId());
//		organisationCategoryDTO3.setOrganisationId(organisationDTO3.getId());

		this.resourceCategoryService.updateOrganizationCategory(organisationCategoryDTO1.getId(), discipline1.getId(), organisationType1.getId(), organisationDTO1.getId(), this.userID);
		this.resourceCategoryService.updateOrganizationCategory(organisationCategoryDTO2.getId(), discipline2.getId(), organisationType2.getId(), organisationDTO2.getId(),  this.userID);
//		this.resourceCategoryService.updateOrganizationCategory(organisationCategoryDTO3.getId(), discipline3.getId(), organisationType2.getId(), organisationDTO3.getId(),  this.userID);
	}

	
	@SuppressWarnings("unchecked")
	private OrganisationDTO createOrganisations(String title, AddressDTO address, PointDTO point, String responsibilityAreaTitle) throws InstantiationException, IllegalAccessException, Exception {

		LocationAreaDTO responsibilityArea = null;

		List<LocationAreaDTO> results = (List<LocationAreaDTO>) genericService.getDTOEntities(SphereDTO.class.getName(), 
				new EsponderCriterionDTO("title", EsponderCriterionExpressionEnumDTO.EQUAL, responsibilityAreaTitle), 10, 0);

		if(!results.isEmpty() && results.size()<2) {
			responsibilityArea = results.get(0);
		}

		if(responsibilityArea != null) {
			OrganisationDTO organisation = new OrganisationDTO();
			organisation.setAddress(address);
			organisation.setLocation(point);
			organisation.setResponsibilityArea(responsibilityArea);
			organisation.setTitle(title);
			return  organisationService.createOrganisationDTO(organisation);
		}
		else return null;
	}

}