package eu.esponder.optimizerscenarios.first.personnelmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;
import eu.esponder.optimizerscenarios.first.objects.person;
import eu.esponder.ws.client.logic.CrisisActionAndActionPartManager;
import eu.esponder.ws.client.logic.ResourcePlanInfo;
import eu.esponder.ws.client.logic.ResourcePlanInfoQueries;
import eu.esponder.ws.client.query.QueryManager;

public class PersonnelManager {

	private List<person> personnel;
	private List<ResourcePlanInfo> pPersonnelPlanInfo;
	Long crisisID;
	ActionDTO pAction;

	public PersonnelManager() {
		personnel = new ArrayList<person>();
		pPersonnelPlanInfo = new ArrayList<ResourcePlanInfo>();
		PopulatePersonnel();
	}

	public List<person> GetAllPersonnel() {
		return personnel;
	}

	// this function load all personnel from the database and add them into a
	// arraylist for the selection
	private void PopulatePersonnel() {
		try {
			ResourcePlanInfoQueries pFind = new ResourcePlanInfoQueries();
			QueryManager pQuery = new QueryManager();
			ResultListDTO pResults = pQuery.getPersonnelAll("1");
			for (int i = 0; i < pResults.getResultList().size(); i++) {
				PersonnelDTO pItem = (PersonnelDTO) pResults.getResultList()
						.get(i);
				QueryManager pqm = new QueryManager();

				person pperson = new person(pItem.getFirstName() + " "
						+ pItem.getLastName(), Integer.parseInt(pqm
						.getPersonnelTitleID("1", pItem.getTitle())),
						pFind.GetresourceCategoryforPersonnel(pItem
								.getPersonnelCategory().getId()));

				pperson.setLat(pItem.getOrganisation().getLocation()
						.getLatitude());
				pperson.setLon(pItem.getOrganisation().getLocation()
						.getLongitude());
				pperson.setTitle(pItem.getTitle());
				pperson.setOrganization(pItem.getOrganisation().getTitle());
				personnel.add(pperson);
			}
		} catch (JsonParseException ex) {
		} catch (JsonMappingException ex) {
		} catch (IOException ex) {
		}
	}

	public void PrintAllPersonnel() {
		System.out.println(" ");
		System.out.println(" ");
		for (int i = 0; i < personnel.size(); i++) {
			System.out.println("id: " + personnel.get(i).getNumber());
			System.out.println("Name: " + personnel.get(i).getName());
			System.out.println("Position: " + personnel.get(i).getPosition());
			System.out.println("Organization: "
					+ personnel.get(i).getOrganization());
			System.out
					.println("Organization Lon: " + personnel.get(i).getLon());
			System.out
					.println("Organization Lat: " + personnel.get(i).getLat());
			System.out.println(" ");
			System.out.println(" ");

		}
	}
}
