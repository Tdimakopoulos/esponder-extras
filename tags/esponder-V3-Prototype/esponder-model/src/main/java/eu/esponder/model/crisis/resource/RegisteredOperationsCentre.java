/*
 * 
 */
package eu.esponder.model.crisis.resource;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.crisis.view.VoIPURL;


// TODO: Auto-generated Javadoc
/**
 * The Class RegisteredOperationsCentre.
 * Entity class that manage information about  Registered Operations Centre
 * The Consumable Resource before a crisis is Registered Operations Centre, when  a crisis start a copy of  Registered Operations Centre is created as Operations Centre
 *
 * @author gleo
 */
@Entity
@Table(name="registered_operations_centre")
@NamedQueries({
	@NamedQuery(name="RegisteredOperationsCentre.findByTitle", query="select roc from RegisteredOperationsCentre roc where roc.title=:title"),
	@NamedQuery(name="RegisteredOperationsCentre.findAll", query="select roc from RegisteredOperationsCentre roc")
})
public class RegisteredOperationsCentre extends Resource {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3363141808683972817L;
	


	/** The operations centre category id. */
	@Column(name="OC_CATEGORY_ID")
	private Long operationsCentreCategoryId;

	/** The vo ipurl. */
	@Embedded
	private VoIPURL voIPURL;

	/**
	 * Gets the operations centre category id.
	 *
	 * @return the operations centre category id
	 */
	public Long getOperationsCentreCategoryId() {
		return operationsCentreCategoryId;
	}

	/**
	 * Sets the operations centre category id.
	 *
	 * @param operationsCentreCategoryId the new operations centre category id
	 */
	public void setOperationsCentreCategoryId(Long operationsCentreCategoryId) {
		this.operationsCentreCategoryId = operationsCentreCategoryId;
	}

	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURL getVoIPURL() {
		return voIPURL;
	}

	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURL voIPURL) {
		this.voIPURL = voIPURL;
	}

	
}
