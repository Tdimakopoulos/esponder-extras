/*
 * 
 */
package eu.esponder.model.snapshot;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


// TODO: Auto-generated Javadoc
/**
 * The Class Period.
 */
@Embeddable
public class Period implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1943089649218847062L;
	
	/** The date from. Not Null*/
	@Column(name="DATE_FROM", nullable=false)
	private Long dateFrom;
	
	/** The date to. Not Null*/
	@Column(name="DATE_TO", nullable=false)
	private Long dateTo;
	
	/**
	 * Instantiates a new period.
	 */
	public Period() { }

	/**
	 * Instantiates a new period.
	 *
	 * @param dateFrom the date from
	 * @param dateTo the date to
	 */
	public Period(Long dateFrom, Long dateTo) {
		super();
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}

	

	/**
	 * Gets the date from.
	 *
	 * @return the date from
	 */
	public Long getDateFrom() {
		return dateFrom;
	}

	/**
	 * Sets the date from.
	 *
	 * @param dateFrom the new date from
	 */
	public void setDateFrom(Long dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * Gets the date to.
	 *
	 * @return the date to
	 */
	public Long getDateTo() {
		return dateTo;
	}

	/**
	 * Sets the date to.
	 *
	 * @param dateTo the new date to
	 */
	public void setDateTo(Long dateTo) {
		this.dateTo = dateTo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Period [dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}

}
