/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import eu.esponder.model.crisis.CrisisContext;


// TODO: Auto-generated Javadoc
/**
 * The Class OCEoc.
 * Entity class that manage the Operations Centers EOC
 */
@Entity
@DiscriminatorValue("EOC")
@NamedQueries({
	@NamedQuery(name="OCEoc.findByTitle", query="select c from OCEoc c where c.title=:title"),
	@NamedQuery(name="OCEoc.findAll", query="select c from OCEoc c")
	
})
public class OCEoc extends OperationsCentre {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7620437338448445871L;

	/** The crisis manager. */
	@OneToOne
	@JoinColumn(name="CRISIS_MANAGER")
	private ActorCM crisisManager;
	
	

	/** The subordinate meo cs. */
	@OneToMany(mappedBy="supervisingOC")
	private Set<OCMeoc> subordinateMEOCs;
	
	/** The eoc crisis context. */
	@ManyToOne
	@JoinColumn(name="CRISIS_CONTEXT_FOR_EOC")
	private CrisisContext eocCrisisContext;

	/**
	 * Gets the crisis manager.
	 *
	 * @return the crisis manager
	 */
	public ActorCM getCrisisManager() {
		return crisisManager;
	}

	/**
	 * Sets the crisis manager.
	 *
	 * @param crisisManager the new crisis manager
	 */
	public void setCrisisManager(ActorCM crisisManager) {
		this.crisisManager = crisisManager;
	}

	/**
	 * Gets the subordinate meo cs.
	 *
	 * @return the subordinate meo cs
	 */
	public Set<OCMeoc> getSubordinateMEOCs() {
		return subordinateMEOCs;
	}

	/**
	 * Sets the subordinate meo cs.
	 *
	 * @param subordinateMEOCs the new subordinate meo cs
	 */
	public void setSubordinateMEOCs(Set<OCMeoc> subordinateMEOCs) {
		this.subordinateMEOCs = subordinateMEOCs;
	}

	/**
	 * Gets the eoc crisis context.
	 *
	 * @return the eoc crisis context
	 */
	public CrisisContext getEocCrisisContext() {
		return eocCrisisContext;
	}

	/**
	 * Sets the eoc crisis context.
	 *
	 * @param eocCrisisContext the new eoc crisis context
	 */
	public void setEocCrisisContext(CrisisContext eocCrisisContext) {
		this.eocCrisisContext = eocCrisisContext;
	}

	
	
}
