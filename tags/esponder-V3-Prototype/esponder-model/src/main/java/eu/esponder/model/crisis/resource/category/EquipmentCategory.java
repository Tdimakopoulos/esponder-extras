/*
 * 
 */
package eu.esponder.model.crisis.resource.category;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.type.EquipmentType;


// TODO: Auto-generated Javadoc
/**
 * The Class EquipmentCategory.
 * Manage the equipment category
 */
@Entity
@Table(name="equipment_category")
@NamedQueries({
	@NamedQuery(name="EquipmentCategory.findByType", query="select a from EquipmentCategory a where a.equipmentType=:equipmentType") })
public class EquipmentCategory extends PlannableResourceCategory {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5814307867860284042L;
	
	/** The equipment type. */
	@OneToOne
	@JoinColumn(name="EQUIPMENT_TYPE_ID", nullable=false)
	private EquipmentType equipmentType;
	

	
	/**
 * Gets the equipment type.
 *
 * @return the equipment type
 */
public EquipmentType getEquipmentType() {
		return equipmentType;
	}

	/**
	 * Sets the equipment type.
	 *
	 * @param equipmentType the new equipment type
	 */
	public void setEquipmentType(EquipmentType equipmentType) {
		this.equipmentType = equipmentType;
	}



}
