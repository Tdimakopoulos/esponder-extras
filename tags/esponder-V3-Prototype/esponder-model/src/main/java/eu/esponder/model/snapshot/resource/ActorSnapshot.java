/*
 * 
 */
package eu.esponder.model.snapshot.resource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.FirstResponderActor;
import eu.esponder.model.snapshot.SpatialSnapshot;
import eu.esponder.model.snapshot.status.ActorSnapshotStatus;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorSnapshot.
 */
@Entity
@Table(name="actor_snapshot")
@NamedQueries({
	@NamedQuery(
		name="ActorSnapshot.findByActorAndDate",
		query="SELECT s FROM ActorSnapshot s WHERE s.actor.id = :actorID AND s.period.dateTo <= :maxDate AND s.period.dateTo = " +
				"(SELECT max(s.period.dateTo) FROM ActorSnapshot s WHERE s.actor.id = :actorID)")
})
public class ActorSnapshot extends SpatialSnapshot<Long> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1380880669505779496L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTOR_SNAPSHOT_ID")
	private Long id;
	
	/** The status. Not Null*/
	@Enumerated(EnumType.STRING)
	@Column(name="ACTOR_SNAPSHOT_STATUS")
	private ActorSnapshotStatus status;
	
	/** The actor. Not Null*/
	@ManyToOne
	@JoinColumn(name="ACTOR_ID")
	private FirstResponderActor actor;
	
	/** The previous. */
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private ActorSnapshot previous;
	
	/** The next. */
	@OneToOne(mappedBy="previous")
	private ActorSnapshot next;

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ActorSnapshotStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ActorSnapshotStatus status) {
		this.status = status;
	}

	/**
	 * Gets the actor.
	 *
	 * @return the actor
	 */
	public FirstResponderActor getActor() {
		return actor;
	}

	/**
	 * Sets the actor.
	 *
	 * @param actor the new actor
	 */
	public void setActor(FirstResponderActor actor) {
		this.actor = actor;
	}

	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public ActorSnapshot getPrevious() {
		return previous;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previous the new previous
	 */
	public void setPrevious(ActorSnapshot previous) {
		this.previous = previous;
	}

	/**
	 * Gets the next.
	 *
	 * @return the next
	 */
	public ActorSnapshot getNext() {
		return next;
	}

	/**
	 * Sets the next.
	 *
	 * @param next the new next
	 */
	public void setNext(ActorSnapshot next) {
		this.next = next;
	}

}
