/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.category.OrganisationCategory;
import eu.esponder.model.snapshot.location.Address;
import eu.esponder.model.snapshot.location.LocationArea;
import eu.esponder.model.snapshot.location.Point;


// TODO: Auto-generated Javadoc
/**
 * The Class Organisation.
 * Entity class that manage the organization information
 */
@Entity
@Table(name="organisation")
@NamedQueries({
	@NamedQuery(name="Organisation.findByTitle", query="select a from Organisation a where a.title=:title"),
	@NamedQuery(name="Organisation.findAll", query="select o from Organisation o")
})
public class Organisation extends ESponderEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 126368864536261845L;
	
	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ORGANISATION_ID")
	private Long id;
	
	/** The title. */
	@Column(name="TITLE")
	private String title;
	
	/** The parent. If the organization is child this can be null*/
	@ManyToOne
	@JoinColumn(name="PARENT_ID")
	protected Organisation parent;
	
	/** The children. If the organization is children this can be null*/
	@OneToMany(mappedBy="parent")
	protected Set<Organisation> children;
	
	/** The address. */
	@Embedded
	private Address address;
	
	/** The location. */
	@Embedded
	private Point location;
	
	/** The responsibility area. The area that the operation center covers*/
	@OneToOne
	@JoinColumn(name="LOCATION_AREA_ID")
	private LocationArea responsibilityArea;
	
	/** The organisation categories. */
	@OneToMany(mappedBy="organisation")
	private Set<OrganisationCategory> organisationCategories;

	/**
	 * Gets the organisation categories.
	 *
	 * @return the organisation categories
	 */
	public Set<OrganisationCategory> getOrganisationCategories() {
		return organisationCategories;
	}

	/**
	 * Sets the organisation categories.
	 *
	 * @param organisationCategories the new organisation categories
	 */
	public void setOrganisationCategories(
			Set<OrganisationCategory> organisationCategories) {
		this.organisationCategories = organisationCategories;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * Gets the responsibility area.
	 *
	 * @return the responsibility area
	 */
	public LocationArea getResponsibilityArea() {
		return responsibilityArea;
	}

	/**
	 * Sets the responsibility area.
	 *
	 * @param responsibilityArea the new responsibility area
	 */
	public void setResponsibilityArea(LocationArea responsibilityArea) {
		this.responsibilityArea = responsibilityArea;
	}

	/**
	 * Gets the location.
	 *
	 * @return the location
	 */
	public Point getLocation() {
		return location;
	}

	/**
	 * Sets the location.
	 *
	 * @param location the new location
	 */
	public void setLocation(Point location) {
		this.location = location;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public Organisation getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(Organisation parent) {
		this.parent = parent;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public Set<Organisation> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the new children
	 */
	public void setChildren(Set<Organisation> children) {
		this.children = children;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

}
