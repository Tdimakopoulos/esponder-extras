/*
 * 
 */
package eu.esponder.model.config;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class DroolsConfiguration.
 * This is a class based on the EsponderConfigParameter and store information about the drool settings
 * Drools is used by the DataFusion module
 */
@Entity
@DiscriminatorValue("DROOLS")
public class DroolsConfiguration extends ESponderConfigParameter {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1472802157259896819L;
	
}
