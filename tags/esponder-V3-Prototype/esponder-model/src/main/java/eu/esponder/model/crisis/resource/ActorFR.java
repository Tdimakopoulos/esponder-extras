/*
 * 
 */
package eu.esponder.model.crisis.resource;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


// TODO: Auto-generated Javadoc
/**
 * The Class Actor First Responder.
 */
@Entity
@DiscriminatorValue("FR")
@NamedQueries({
	@NamedQuery(name="ActorFR.findByTitle", query="select a from ActorFR a where a.title=:title"),
	@NamedQuery(name="ActorFR.findAll", query="select a from ActorFR a")
})
public class ActorFR extends FirstResponderActor {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5482288965562933476L;
	
	/** The FR chief. */
	@ManyToOne
	@JoinColumn(name="FRChief")
	private ActorFRC FRChief;
	
	/**
	 * Gets the fR chief.
	 *
	 * @return the fR chief
	 */
	public ActorFRC getFRChief() {
		return FRChief;
	}

	/**
	 * Sets the fR chief.
	 *
	 * @param fRChief the new fR chief
	 */
	public void setFRChief(ActorFRC fRChief) {
		FRChief = fRChief;
	}
	
}
