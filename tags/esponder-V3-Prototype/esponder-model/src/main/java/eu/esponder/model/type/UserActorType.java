/*
 * 
 */
package eu.esponder.model.type;



import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;



// TODO: Auto-generated Javadoc
/**
 * The Class UserActorType.
 * If an actor has a UserActorType then it also has an associated EsponderUser object
 */
@Entity
@DiscriminatorValue("USER_ACTOR")
public class UserActorType extends ActorType {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -231655923475592938L;
	
}
