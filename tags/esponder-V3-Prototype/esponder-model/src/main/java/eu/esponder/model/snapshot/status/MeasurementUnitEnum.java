/*
 * 
 */
package eu.esponder.model.snapshot.status;


// TODO: Auto-generated Javadoc
/**
 * The Enum MeasurementUnitEnum.
 */
public enum MeasurementUnitEnum {
	
	//Temperature
	/** The degrees celcius. */
	DEGREES_CELCIUS,
	
	/** The degrees fahrenheit. */
	DEGREES_FAHRENHEIT,
	
	//Time
	/** The seconds. */
	SECONDS,
	
	/** The milliseconds. */
	MILLISECONDS,
	
	//Gas Concentration
	/** The ppm. */
	PPM,
	
	//Position (for each of lon, lat, alt)
	/** The degrees. */
	DEGREES,
	
	//Hearbeat Rate
	/** The bbm. */
	BBM

}
