/*
 * 
 */
package eu.esponder.model.snapshot.location;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


// TODO: Auto-generated Javadoc
// 
/**
 * The Class Address.
 */
@Embeddable
public class Address implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3274818026669019295L;

	/** The number. */
	@Column(name="ADDR_NUMBER")
	private String number;
	
	/** The street. */
	@Column(name="ADDR_STREET")
	private String street;
	
	/** The zip code. */
	@Column(name="ZIPCODE")
	private String zipCode;
	
	/** The prefecture. */
	@Column(name="PREFECTURE")
	private String prefecture;
	
	/** The country. */
	@Column(name="COUNTRY")
	private String country;

	/**
	 * Gets the number.
	 *
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * Sets the number.
	 *
	 * @param number the new number
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * Gets the street.
	 *
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Sets the street.
	 *
	 * @param street the new street
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Gets the zip code.
	 *
	 * @return the zip code
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * Sets the zip code.
	 *
	 * @param zipCode the new zip code
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * Gets the prefecture.
	 *
	 * @return the prefecture
	 */
	public String getPrefecture() {
		return prefecture;
	}

	/**
	 * Sets the prefecture.
	 *
	 * @param prefecture the new prefecture
	 */
	public void setPrefecture(String prefecture) {
		this.prefecture = prefecture;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
