/*
 * 
 */
package eu.esponder.model.snapshot;

import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

import eu.esponder.model.snapshot.location.LocationArea;


// TODO: Auto-generated Javadoc
/**
 * The Class SpatialSnapshot.
 *
 * @param <T> the generic type
 */
@MappedSuperclass
public abstract class SpatialSnapshot<T> extends Snapshot<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6994801801906839319L;
	
	/** The location area. */
	@OneToOne
	@JoinColumn(name="LOCATION_AREA_ID")
	protected LocationArea locationArea;

	/**
	 * Gets the location area.
	 *
	 * @return the location area
	 */
	public LocationArea getLocationArea() {
		return locationArea;
	}

	/**
	 * Sets the location area.
	 *
	 * @param locationArea the new location area
	 */
	public void setLocationArea(LocationArea locationArea) {
		this.locationArea = locationArea;
	}
	
}
