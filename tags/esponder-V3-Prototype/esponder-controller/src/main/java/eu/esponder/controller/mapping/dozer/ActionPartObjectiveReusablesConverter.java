/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.LogisticsService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.resource.ReusableResource;
import eu.esponder.util.ejb.ServiceLocator;
// TODO: Auto-generated Javadoc
// 
//import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;

// 
/**
 * The Class ActionPartObjectiveReusablesConverter.
 */
public class ActionPartObjectiveReusablesConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected LogisticsService getLogisticsService() {
		try {
			return ServiceLocator.getResource("esponder/LogisticsBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()) {
				if(it.next().getClass() == ReusableResource.class) {

					Set<ReusableResource> sourceReusablesSet = (Set<ReusableResource>) source;
					Set<Long> destReusablesDTOSet = new HashSet<Long>();
					for(ReusableResource rResource : sourceReusablesSet) {
						destReusablesDTOSet.add(rResource.getId());
					}
					destination = destReusablesDTOSet;
				}
				else
					if(it.next().getClass() == Long.class) {

						Set<Long> sourceReusablesDTOSet = (Set<Long>) source;
						Set<ReusableResource> destReusablesSet = new HashSet<ReusableResource>();
						for(Long rResourceID : sourceReusablesDTOSet) {
							ReusableResource destResource = this.getLogisticsService().findReusableResourceById(rResourceID);
							destReusablesSet.add(destResource);
						}
						destination = destReusablesSet;
					}
					else
						destination = null;
			}
		}
		else {
			new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;

	}

}
