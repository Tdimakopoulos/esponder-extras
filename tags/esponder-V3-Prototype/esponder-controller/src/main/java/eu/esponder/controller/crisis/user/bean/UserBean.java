/*
 * 
 */
package eu.esponder.controller.crisis.user.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.crisis.user.UserService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.model.user.ESponderUser;
// TODO: Auto-generated Javadoc
//import eu.esponder.model.crisis.resource.ReusableResource;

// 
/**
 * The Class UserBean.
 */
@Stateless
public class UserBean implements UserService, UserRemoteService {

	/** The user crud service. */
	@EJB
	private CrudService<ESponderUser> userCrudService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserRemoteService#findUserByIdRemote(java.lang.Long)
	 */
	@Override
	public ESponderUserDTO findUserByIdRemote(Long userID) {
		ESponderUser user = findUserById(userID);
		if (user != null)
			return (ESponderUserDTO) mappingService.mapESponderEntity(user,
					ESponderUserDTO.class);
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserService#findUserById(java.lang.Long)
	 */
	@Override
	public ESponderUser findUserById(Long userID) {
		return (ESponderUser) userCrudService.find(ESponderUser.class, userID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserRemoteService#findUserByNameRemote(java.lang.String)
	 */
	@Override
	public ESponderUserDTO findUserByNameRemote(String userName) {
		ESponderUser user = findUserByName(userName);
		if (user != null)
			return (ESponderUserDTO) mappingService.mapESponderEntity(user,
					ESponderUserDTO.class);
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserService#findUserByName(java.lang.String)
	 */
	@Override
	public ESponderUser findUserByName(String userName) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", userName);
		return (ESponderUser) userCrudService.findSingleWithNamedQuery(
				"ESponderUser.findByTitle", params);
		
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserRemoteService#findAllUsersRemote()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ESponderUserDTO> findAllUsersRemote() {
		return (List<ESponderUserDTO>) mappingService.mapESponderEntity(findAllUsers(), ESponderUserDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserService#findAllUsers()
	 */
	@Override
	public List<ESponderUser> findAllUsers() {
		return (List<ESponderUser>) userCrudService.findWithNamedQuery("ESponderUser.findAll");
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserRemoteService#createUserRemote(eu.esponder.dto.model.user.ESponderUserDTO, java.lang.Long)
	 */
	@Override
	public ESponderUserDTO createUserRemote(ESponderUserDTO userDTO, Long userID) {
		ESponderUser user = (ESponderUser) mappingService.mapESponderEntityDTO(
				userDTO, ESponderUser.class);
		user = createUser(user, userID);
		return (ESponderUserDTO) mappingService.mapESponderEntity(user,
				ESponderUserDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserService#createUser(eu.esponder.model.user.ESponderUser, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderUser createUser(ESponderUser user, Long userID) {
		userCrudService.create(user);
		return user;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserRemoteService#updateUserRemote(eu.esponder.dto.model.user.ESponderUserDTO, java.lang.Long)
	 */
	@Override
	public ESponderUserDTO updateUserRemote(ESponderUserDTO userDTO, Long userID) {
		ESponderUser user = (ESponderUser) mappingService.mapESponderEntityDTO(userDTO, ESponderUser.class);
		user = updateUser(user, userID);
		return (ESponderUserDTO) mappingService.mapESponderEntity(user, ESponderUserDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserService#updateUser(eu.esponder.model.user.ESponderUser, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ESponderUser updateUser(ESponderUser user, Long userID) {
		ESponderUser userPersisted = findUserById(user.getId());
		mappingService.mapEntityToEntity(user, userPersisted);
		return (ESponderUser) userCrudService.update(userPersisted);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserRemoteService#deleteUserRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public Long deleteUserRemote(Long deletedUserId, Long userID) {
		return deleteUser(deletedUserId, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserService#deleteUser(long, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Long deleteUser(long deletedUserId, Long userID) {
		ESponderUser resource = userCrudService.find(ESponderUser.class, deletedUserId);
//		reusablesCrudService.delete(ReusableResource.class, reusableResourceID);
		userCrudService.delete(resource);
		
		//userCrudService.delete(ESponderUser.class, deletedUserId);
		return deletedUserId;
		
	}

}
