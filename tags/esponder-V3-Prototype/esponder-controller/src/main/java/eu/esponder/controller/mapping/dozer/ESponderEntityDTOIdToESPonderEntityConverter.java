/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.generic.GenericService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.ESponderEntity;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ESponderEntityDTOIdToESPonderEntityConverter.
 */
public class ESponderEntityDTOIdToESPonderEntityConverter implements CustomConverter {
	
	/**
	 * Gets the generic service.
	 *
	 * @return the generic service
	 */
	protected GenericService getGenericService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {
		
		// Model to DTO
		// ESponderEntity --> ESponderEntityId
//		if(ESponderEntity.class.isAssignableFrom(sourceClass) && source != null) {
		if (source instanceof ESponderEntity<?> && source != null) {
			ESponderEntity<Long> sourceEntity = (ESponderEntity<Long>) source;
			Long destField = sourceEntity.getId();
			destination = destField;
		}
		else if(sourceClass == Long.class && source!=null) {
			Object destObject = this.getGenericService().getEntity((Class<? extends ESponderEntity<Long>>) destinationClass, (Long) source);
			if(destObject != null) {
				destination = destObject;
			}
			else
				new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
