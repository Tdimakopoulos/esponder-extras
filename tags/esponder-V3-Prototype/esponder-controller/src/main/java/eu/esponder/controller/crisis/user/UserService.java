/*
 * 
 */
package eu.esponder.controller.crisis.user;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.user.ESponderUser;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface UserService.
 */
@Local
public interface UserService extends UserRemoteService {

	/**
	 * Find user by id.
	 *
	 * @param userID the user id
	 * @return the e sponder user
	 */
	public ESponderUser findUserById(Long userID);

	/**
	 * Find user by name.
	 *
	 * @param userName the user name
	 * @return the e sponder user
	 */
	public ESponderUser findUserByName(String userName);

	/**
	 * Creates the user.
	 *
	 * @param user the user
	 * @param userID the user id
	 * @return the e sponder user
	 */
	public ESponderUser createUser(ESponderUser user, Long userID);

	/**
	 * Update user.
	 *
	 * @param user the user
	 * @param userID the user id
	 * @return the e sponder user
	 */
	public ESponderUser updateUser(ESponderUser user, Long userID);

	/**
	 * Find all users.
	 *
	 * @return the list
	 */
	public List<ESponderUser> findAllUsers();

	/**
	 * Delete user.
	 *
	 * @param deletedUserId the deleted user id
	 * @param userID the user id
	 * @return the long
	 */
	public Long deleteUser(long deletedUserId, Long userID);

}
