/*
 * 
 */
package eu.esponder.controller.persistence.criteria;

import java.util.Collection;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class EsponderIntersectionCriteriaCollection.
 */
public class EsponderIntersectionCriteriaCollection extends EsponderCriteriaCollection {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -447493874297381400L;
	
	/**
	 * Instantiates a new esponder intersection criteria collection.
	 */
	public EsponderIntersectionCriteriaCollection() {}
	
	/**
	 * Instantiates a new esponder intersection criteria collection.
	 *
	 * @param restrictions the restrictions
	 */
	public EsponderIntersectionCriteriaCollection(
			Collection<EsponderQueryRestriction> restrictions) {
		super(restrictions);
	}

}
