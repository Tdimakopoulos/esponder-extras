/*
 * 
 */
package eu.esponder.controller.crisis;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.type.ESponderType;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface TypeService.
 */
@Local
public interface TypeService extends TypeRemoteService {
	
	/**
	 * Find by id.
	 *
	 * @param typeId the type id
	 * @return the e sponder type
	 */
	public ESponderType findById(Long typeId);
	
	/**
	 * Find by title.
	 *
	 * @param title the title
	 * @return the e sponder type
	 */
	public ESponderType findByTitle(String title);
	
	/**
	 * Creates the type.
	 *
	 * @param type the type
	 * @param userID the user id
	 * @return the e sponder type
	 */
	public ESponderType createType(ESponderType type, Long userID);
	
	/**
	 * Update type.
	 *
	 * @param type the type
	 * @param userID the user id
	 * @return the e sponder type
	 */
	public ESponderType updateType(ESponderType type, Long userID);
	
	/**
	 * Delete type.
	 *
	 * @param ESponderTypeID the e sponder type id
	 * @param userID the user id
	 */
	public void deleteType(Long ESponderTypeID, Long userID);

	/**
	 * Find all types.
	 *
	 * @return the list
	 */
	List<ESponderType> findAllTypes();
	
}
