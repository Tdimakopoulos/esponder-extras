/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.OrganisationService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.resource.Organisation;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class SetOfChildrenInOrganisationConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected OrganisationService getOrganisationService() {
		try {
			return ServiceLocator.getResource("esponder/OrganisationBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				Object next = it.next();
				if(next.getClass() == Organisation.class) {

					Set<Organisation> sourceOrganisationSet = (Set<Organisation>) source;
					Set<Long> destOrganisationDTOSet = new HashSet<Long>();
					for(Organisation organisation : sourceOrganisationSet) {
						destOrganisationDTOSet.add(organisation.getId());
					}
					destination = destOrganisationDTOSet;
				}
				else
					if(next.getClass() == Long.class) {

						Set<Long> sourceOrganisationDTOSet = (Set<Long>) source;
						Set<Organisation> destOrganisationSet = new HashSet<Organisation>();
						for(Long organisationID : sourceOrganisationDTOSet) {
							Organisation destOrganisation = this.getOrganisationService().findById(organisationID);
							destOrganisationSet.add(destOrganisation);
						}
						destination = destOrganisationSet;
					}
					else
						destination = null;
			}
		}
		else {
			new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
