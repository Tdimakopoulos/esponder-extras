/*
 * 
 */
package eu.esponder.controller.crisis.resource.bean;

//import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.CrisisService;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.ActorCMDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.OCEocDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.dto.model.snapshot.ReferencePOISnapshotDTO;
import eu.esponder.dto.model.snapshot.SketchPOISnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.ActorCM;
import eu.esponder.model.crisis.resource.ActorFR;
import eu.esponder.model.crisis.resource.ActorFRC;
import eu.esponder.model.crisis.resource.ActorIC;
import eu.esponder.model.crisis.resource.OCEoc;
import eu.esponder.model.crisis.resource.OCMeoc;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.resource.RegisteredOperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.crisis.view.SketchPOI;
import eu.esponder.model.snapshot.ReferencePOISnapshot;
import eu.esponder.model.snapshot.SketchPOISnapshot;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;
import eu.esponder.model.type.OperationsCentreType;
import eu.esponder.model.user.ESponderUser;


// TODO: Auto-generated Javadoc
// 
/**
 * The Class OperationsCentreBean.
 */
@Stateless
@SuppressWarnings("unchecked")
public class OperationsCentreBean implements OperationsCentreService,
OperationsCentreRemoteService {

	/** The operations centre crud service. */
	@EJB
	private CrudService<OperationsCentre> operationsCentreCrudService;

	/** The eoc crud service. */
	@EJB
	private CrudService<OCEoc> eocCrudService;

	/** The meoc crud service. */
	@EJB
	private CrudService<OCMeoc> meocCrudService;

	/** The cm crud service. */
	@EJB
	private CrudService<ActorCM> cmCrudService;

	/** The registered operations centre crud service. */
	@EJB
	private CrudService<RegisteredOperationsCentre> registeredOperationsCentreCrudService;

	/** The operations centre snapshot crud service. */
	@EJB
	private CrudService<OperationsCentreSnapshot> operationsCentreSnapshotCrudService;

	/** The user crud service. */
	@EJB
	private CrudService<ESponderUser> userCrudService;

	/** The type service. */
	@EJB
	private TypeService typeService;

	/** The reference poi crud service. */
	@EJB
	private CrudService<ReferencePOI> referencePOICrudService;

	/** The sketch poi crud service. */
	@EJB
	private CrudService<SketchPOI> sketchPOICrudService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;

	/** The crisis service. */
	@EJB
	private CrisisService crisisService;

	/** The reference poi snapshot crud service. */
	@EJB
	private CrudService<ReferencePOISnapshot> referencePOISnapshotCrudService;

	/** The sketch poi snapshot crud service. */
	@EJB
	private CrudService<SketchPOISnapshot> sketchPOISnapshotCrudService;

	/** The crisis crud service. */
	@EJB
	private CrudService<CrisisContext> crisisCrudService;


	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findOperationsCentreClassByIdRemote(java.lang.Long)
	 */
	@Override
	public String findOperationsCentreClassByIdRemote(Long ocID) {

		Object oc = findOperationsCentreAsObjectById(ocID);

		if(oc != null ) {
			if(oc.getClass() == OCEoc.class)
				return OCEocDTO.class.getSimpleName();
			else if(oc.getClass() == OCMeoc.class)
				return OCMeocDTO.class.getSimpleName();
			else
				return null;
		}
		return null;
	}


	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findOperationsCentreAsObjectById(java.lang.Long)
	 */
	@Override
	public Object findOperationsCentreAsObjectById(Long ocID) {
		return (Object) operationsCentreCrudService.find(OperationsCentre.class, ocID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findEocSupervisorRemote(java.lang.Long)
	 */
	@Override
	public ActorCMDTO findEocSupervisorRemote(Long operationsCentreID) {
		ActorCM actorCM = findEocSupervisor(operationsCentreID);
		if(actorCM!=null)
			return (ActorCMDTO) this.mappingService.mapESponderEntity(actorCM, ActorCMDTO.class);
		else
			return null;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findEocSupervisor(java.lang.Long)
	 */
	@Override
	public ActorCM findEocSupervisor(Long operationsCentreID) {
		OCEoc eoc = (OCEoc) findOperationCentreById(operationsCentreID);
		if(eoc!=null) {
			return eoc.getCrisisManager();
		}
		return null;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findMeocSupervisorRemote(java.lang.Long)
	 */
	@Override
	public ActorICDTO findMeocSupervisorRemote(Long operationsCentreID) {
		ActorIC actorIC = findMeocSupervisor(operationsCentreID);
		if(actorIC!=null)
			return (ActorICDTO) this.mappingService.mapESponderEntity(actorIC, ActorICDTO.class);
		else
			return null;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findMeocSupervisor(java.lang.Long)
	 */
	@Override
	public ActorIC findMeocSupervisor(Long operationsCentreID) {
		OCMeoc meoc = (OCMeoc) findOperationCentreById(operationsCentreID);
		if(meoc!=null) {
			return meoc.getIncidentCommander();
		}
		return null;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findMeocsByCrisisRemote(java.lang.Long)
	 */
	@Override
	public List<OCMeocDTO> findMeocsByCrisisRemote(Long crisisContextID) {
		return (List<OCMeocDTO>) mappingService.mapESponderEntity(
				findMeocsByCrisis(crisisContextID), OCMeocDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findMeocsByCrisis(java.lang.Long)
	 */
	@Override
	public List<OCMeoc> findMeocsByCrisis(Long crisisContextID) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		CrisisContext crisisContext = crisisCrudService.find(CrisisContext.class, crisisContextID);
		if(crisisContext != null) {
			params.put("crisisContext", crisisContext);
			//FIXME UPDATE MODEL 3

			//			return (List<OCMeoc>) operationsCentreCrudService
			//					.findWithNamedQuery("OperationsCentre.findMeocsByCrisisContext", params);
			return null;
		}
		else
			return null;

	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findEocByCrisisRemote(java.lang.Long)
	 */
	@Override
	public OperationsCentreDTO findEocByCrisisRemote(Long  crisisContextID) {
		return (OperationsCentreDTO) mappingService.mapESponderEntity(
				findEocByCrisis(crisisContextID), OperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findEocByCrisis(java.lang.Long)
	 */
	@Override
	public OperationsCentre findEocByCrisis(Long crisisContextID) {
		OperationsCentreType meocType = (OperationsCentreType) typeService.findByTitle("EOC");
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("crisisContextID", crisisContextID);
		params.put("eocType", meocType);

		return (OperationsCentre) operationsCentreCrudService
				.findWithNamedQuery("OperationsCentre.findEocsByCrisisContext", params);
	}


	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findOperationCentreByIdRemote(java.lang.Long)
	 */
	@Override
	public Object findOCByIdRemote(Long operationsCentreID) throws ClassNotFoundException {

		Object oc = findOCById(operationsCentreID);
		return  mappingService.mapObject(oc,mappingService.getDTOEntityClass((Class<? extends ESponderEntity<Long>>) oc.getClass()));
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findOperationCentreById(java.lang.Long)
	 */
	@Override
	public Object findOCById(Long operationsCentreID) {
		return operationsCentreCrudService.find(OperationsCentre.class, operationsCentreID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findOperationCentreByIdRemote(java.lang.Long)
	 */
	@Override
	public OperationsCentreDTO findOperationCentreByIdRemote(
			Long operationsCentreID) {
		return (OperationsCentreDTO) mappingService.mapESponderEntity(
				findOperationCentreById(operationsCentreID),
				OperationsCentreDTO.class);
	}


	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findOperationCentreById(java.lang.Long)
	 */
	@Override
	public OperationsCentre findOperationCentreById(Long operationsCentreID) {
		return (OperationsCentre) operationsCentreCrudService.find(
				OperationsCentre.class, operationsCentreID);
	}


	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findEocByIdRemote(java.lang.Long)
	 */
	@Override
	public OCEocDTO findEocByIdRemote(Long operationsCentreID) {
		return (OCEocDTO) mappingService.mapESponderEntity(
				findEocById(operationsCentreID),OCEocDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findEocById(java.lang.Long)
	 */
	@Override
	public OCEoc findEocById(Long operationsCentreID) {
		return (OCEoc) eocCrudService.find(OCEoc.class, operationsCentreID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findEocIDBYMeocIDRemote(java.lang.Long)
	 */
	@Override
	public Long findEocIDBYMeocIDRemote(Long operationsCentreID) {
		return findEocIDBYMeocID(operationsCentreID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findEocIDBYMeocID(java.lang.Long)
	 */
	@Override
	public Long findEocIDBYMeocID(Long operationsCentreID) {
		OCMeoc meoc = meocCrudService.find(OCMeoc.class, operationsCentreID);
		if(meoc!= null)
			if(meoc.getSupervisingOC() !=null)
				return meoc.getSupervisingOC().getId();
			else
				return null;
		else
			return null;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findMeocByIdRemote(java.lang.Long)
	 */
	@Override
	public OCMeocDTO findMeocByIdRemote(Long operationsCentreID) {
		return (OCMeocDTO) mappingService.mapESponderEntity(
				findMeocById(operationsCentreID),OCMeocDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findMeocById(java.lang.Long)
	 */
	@Override
	public OCMeoc findMeocById(Long operationsCentreID) {
		return meocCrudService.find(OCMeoc.class, operationsCentreID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findRegisteredOperationCentreByIdRemote(java.lang.Long)
	 */
	@Override
	public RegisteredOperationsCentreDTO findRegisteredOperationCentreByIdRemote(
			Long registeredOperationsCentreID) {
		return (RegisteredOperationsCentreDTO) mappingService
				.mapESponderEntity(
						findRegisteredOperationCentreById(registeredOperationsCentreID),
						RegisteredOperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findRegisteredOperationCentreById(java.lang.Long)
	 */
	@Override
	public RegisteredOperationsCentre findRegisteredOperationCentreById(
			Long registeredOperationsCentreID) {
		return (RegisteredOperationsCentre) registeredOperationsCentreCrudService
				.find(RegisteredOperationsCentre.class,
						registeredOperationsCentreID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findOperationsCentreByTitleRemote(java.lang.String)
	 */
	@Override
	public OCEocDTO findEocByTitleRemote(String title) {
		return (OCEocDTO) mappingService.mapESponderEntity(findEOCByTitle(title), OCEocDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findOperationsCentreByTitle(java.lang.String)
	 */
	@Override
	public OCEoc findEOCByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (OCEoc) eocCrudService
				.findSingleWithNamedQuery("OCEoc.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findOperationsCentreByTitleRemote(java.lang.String)
	 */
	@Override
	public OCMeocDTO findMeocByTitleRemote(String title) {
		OCMeoc meoc = findMeocByTitle(title);
		return (OCMeocDTO) mappingService.mapESponderEntity(
				meoc, OCMeocDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findOperationsCentreByTitle(java.lang.String)
	 */
	@Override
	public OCMeoc findMeocByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (OCMeoc) operationsCentreCrudService
				.findSingleWithNamedQuery("OCMeoc.findByTitle",
						params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findRegisteredOperationsCentreByTitleRemote(java.lang.String)
	 */
	@Override
	public RegisteredOperationsCentreDTO findRegisteredOperationsCentreByTitleRemote(
			String title) {
		return (RegisteredOperationsCentreDTO) mappingService
				.mapESponderEntity(
						findRegisteredOperationsCentreByTitle(title),
						RegisteredOperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findRegisteredOperationsCentreByTitle(java.lang.String)
	 */
	@Override
	public RegisteredOperationsCentre findRegisteredOperationsCentreByTitle(
			String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (RegisteredOperationsCentre) registeredOperationsCentreCrudService
				.findSingleWithNamedQuery(
						"RegisteredOperationsCentre.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findAllOperationsCentresRemote()
	 */
	@Override
	public List<OperationsCentreDTO> findAllOperationsCentresRemote() {
		return (List<OperationsCentreDTO>) mappingService.mapESponderEntity(
				findAllOperationsCentres(), OperationsCentreDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findAllOperationsCentres()
	 */
	@Override
	public List<OperationsCentre> findAllOperationsCentres() {
		return (List<OperationsCentre>) operationsCentreCrudService
				.findWithNamedQuery("OperationsCentre.findAll");
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findAllOperationsCentresByCrisisContextRemote(java.lang.Long)
	 */
	@Override
	public List<OperationsCentreDTO> findAllOperationsCentresByCrisisContextRemote(
			Long crisisContextID) {
		return (List<OperationsCentreDTO>) mappingService.mapESponderEntity(
				findAllOperationsCentres(), OperationsCentreDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findAllOperationsCentresByCrisisContext(java.lang.Long)
	 */
	@Override
	public List<OperationsCentre> findAllOperationsCentresByCrisisContext(
			Long crisisContextID) {
		CrisisContext cc = crisisService.findCrisisContextById(crisisContextID);
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("crisisContext", cc);
		return (List<OperationsCentre>) operationsCentreCrudService
				.findWithNamedQuery("OperationsCentre.findByCrisisContext",
						params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findAllRegisteredOperationsCentresRemote()
	 */
	@Override
	public List<RegisteredOperationsCentreDTO> findAllRegisteredOperationsCentresRemote() {
		return (List<RegisteredOperationsCentreDTO>) mappingService
				.mapESponderEntity(findAllRegisteredOperationsCentres(),
						RegisteredOperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findAllRegisteredOperationsCentres()
	 */
	@Override
	public List<RegisteredOperationsCentre> findAllRegisteredOperationsCentres() {
		return (List<RegisteredOperationsCentre>) registeredOperationsCentreCrudService
				.findWithNamedQuery("RegisteredOperationsCentre.findAll");
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findOperationsCentreByUserRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public OperationsCentreDTO findOperationsCentreByUserRemote(
			Long operationsCentreID, Long userID) {
		return (OperationsCentreDTO) mappingService.mapESponderEntity(
				findOperationsCentreByUser(operationsCentreID, userID),
				OperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findOperationsCentreByUser(java.lang.Long, java.lang.Long)
	 */
	@Override
	public OperationsCentre findOperationsCentreByUser(Long operationsCentreID,
			Long userID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		params.put("userID", userID);
		return (OperationsCentre) operationsCentreCrudService
				.findSingleWithNamedQuery("OperationsCentre.findByIdAndUser",
						params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findUserOperationsCentresRemote(java.lang.Long)
	 */
	@Override
	public OperationsCentreDTO findUserOperationsCentreRemote(Long userID) {
		OperationsCentre operationsCentre = findUserOperationsCentre(userID);
		if (operationsCentre != null)
			return (OperationsCentreDTO) mappingService.mapESponderEntity(operationsCentre, OperationsCentreDTO.class);
		else
			return null;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findUserOperationsCentres(java.lang.Long)
	 */
	@Override
	public OperationsCentre findUserOperationsCentre(Long userID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userID", userID);
		ESponderUser user = userCrudService.find(ESponderUser.class, userID);
		if(user!=null) {
			OperationsCentre operationsCentre = user.getOperationsCentre();
			return operationsCentre;
		}
		else
			return null;

	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findOperationsCentreSnapshotByIdRemote(java.lang.Long)
	 */
	@Override
	public OperationsCentreSnapshotDTO findOperationsCentreSnapshotByIdRemote(Long operationsCentreID) {
		
		OperationsCentreSnapshot snapshot = findOperationsCentreSnapshotById(operationsCentreID);
		return (OperationsCentreSnapshotDTO) mappingService.mapESponderEntity(
						snapshot, OperationsCentreSnapshotDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findOperationsCentreSnapshotById(java.lang.Long)
	 */
	@Override
	public OperationsCentreSnapshot findOperationsCentreSnapshotById(Long operationsCentreID) {
		OperationsCentreSnapshot snapshot = (OperationsCentreSnapshot) operationsCentreSnapshotCrudService
				.find(OperationsCentreSnapshot.class, operationsCentreID);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findOperationsCentreSnapshotByDateRemote(java.lang.Long, java.util.Date)
	 */
	@Override
	public OperationsCentreSnapshotDTO findOperationsCentreSnapshotByDateRemote(
			Long operationsCentreID, Date maxDate) {
		return (OperationsCentreSnapshotDTO) mappingService
				.mapESponderEntity(
						findOperationsCentreSnapshotByDate(operationsCentreID,
								maxDate), OperationsCentreSnapshotDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findOperationsCentreSnapshotByDate(java.lang.Long, java.util.Date)
	 */
	@Override
	public OperationsCentreSnapshot findOperationsCentreSnapshotByDate(
			Long operationsCentreID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		params.put("maxDate", maxDate.getTime());
		OperationsCentreSnapshot snapshot = (OperationsCentreSnapshot) operationsCentreSnapshotCrudService
				.findSingleWithNamedQuery(
						"OperationsCentreSnapshot.findByOperationsCentreAndDate",
						params);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#createOperationsCentreRemote(eu.esponder.dto.model.crisis.resource.OperationsCentreDTO, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Object createOperationsCentreRemote(Object operationsCentreDTO, Long userID) {

		if(operationsCentreDTO instanceof OCEocDTO) {
			OCEoc operationsCentre = (OCEoc) mappingService.mapESponderEntityDTO((OCEocDTO)operationsCentreDTO, OCEoc.class);
			return mappingService.mapESponderEntity(
					createEoc(operationsCentre, userID), OCEocDTO.class);	
		}
		else if(operationsCentreDTO instanceof OCMeocDTO) {
			OCMeoc operationsCentre = (OCMeoc) mappingService.mapESponderEntityDTO((OCMeocDTO)operationsCentreDTO, OCMeoc.class);
			return mappingService.mapESponderEntity(
					createMeoc(operationsCentre, userID), OCMeocDTO.class);
		}
		return null;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#createOperationsCentre(eu.esponder.model.crisis.resource.OperationsCentre, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OCEoc createEoc(OCEoc operationsCentre, Long userID) {
		return (OCEoc) operationsCentreCrudService.create(operationsCentre);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#createOperationsCentre(eu.esponder.model.crisis.resource.OperationsCentre, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OCMeoc createMeoc(OCMeoc operationsCentre, Long userID) {
		return (OCMeoc) operationsCentreCrudService.create(operationsCentre);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#createRegisteredOperationsCentreRemote(eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO, java.lang.Long)
	 */
	@Override
	public RegisteredOperationsCentreDTO createRegisteredOperationsCentreRemote(
			RegisteredOperationsCentreDTO registeredOperationsCentreDTO,
			Long userID) {
		RegisteredOperationsCentre registeredOperationsCentre = (RegisteredOperationsCentre) mappingService
				.mapESponderEntityDTO(registeredOperationsCentreDTO,
						RegisteredOperationsCentre.class);
		return (RegisteredOperationsCentreDTO) mappingService
				.mapESponderEntity(
						createRegisteredOperationsCentre(
								registeredOperationsCentre, userID),
								RegisteredOperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#createRegisteredOperationsCentre(eu.esponder.model.crisis.resource.RegisteredOperationsCentre, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredOperationsCentre createRegisteredOperationsCentre(
			RegisteredOperationsCentre registeredOperationsCentre, Long userID) {
		return registeredOperationsCentreCrudService
				.create(registeredOperationsCentre);

	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#deleteOperationsCentreRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteOperationsCentreRemote(Long operationsCentreId,
			Long userID) {
		deleteOperationsCentre(operationsCentreId, userID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#deleteOperationsCentre(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteOperationsCentre(Long operationsCentreId, Long userID) {
		OperationsCentre operationsCentre = findOperationCentreById(operationsCentreId);
		if (operationsCentre != null)
			operationsCentreCrudService.delete(operationsCentre);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#deleteRegisteredOperationsCentreRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteRegisteredOperationsCentreRemote(
			Long registeredOperationsCentreId, Long userID) {
		deleteRegisteredOperationsCentre(registeredOperationsCentreId, userID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#deleteRegisteredOperationsCentre(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteRegisteredOperationsCentre(
			Long registeredOperationsCentreId, Long userID) {
		RegisteredOperationsCentre registeredOperationsCentre = findRegisteredOperationCentreById(registeredOperationsCentreId);
		if (registeredOperationsCentre != null)
			registeredOperationsCentreCrudService.delete(registeredOperationsCentre);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#updateOperationsCentreRemote(eu.esponder.dto.model.crisis.resource.OperationsCentreDTO, java.lang.Long)
	 */
	@Override
	public OCEocDTO updateEOCRemote(OCEocDTO operationsCentreDTO, Long userID) {
		OCEoc operationsCentre = (OCEoc) mappingService.mapESponderEntityDTO(operationsCentreDTO, OCEoc.class);
		return (OCEocDTO) mappingService.mapESponderEntity(
				updateEOC(operationsCentre, userID), OCEocDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#updateOperationsCentre(eu.esponder.model.crisis.resource.OperationsCentre, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OCEoc updateEOC(OCEoc operationsCentre, Long userID) {
		OCEoc persistedOC = eocCrudService.find(OCEoc.class, operationsCentre.getId());
		mappingService.mapEntityToEntity(operationsCentre, persistedOC);
		return (OCEoc) eocCrudService.update(persistedOC);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#updateOperationsCentreRemote(eu.esponder.dto.model.crisis.resource.OperationsCentreDTO, java.lang.Long)
	 */
	@Override
	public OCMeocDTO updateMEOCRemote(OCMeocDTO operationsCentreDTO, Long userID) {
		OCMeoc operationsCentre = (OCMeoc) mappingService.mapESponderEntityDTO(operationsCentreDTO, OCMeoc.class);
		OCMeoc pmeoc = updateMEOC(operationsCentre, userID);
		return (OCMeocDTO) mappingService.mapESponderEntity(
				pmeoc, OCMeocDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#updateOperationsCentre(eu.esponder.model.crisis.resource.OperationsCentre, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OCMeoc updateMEOC(OCMeoc operationsCentre, Long userID) {
		OCMeoc persistedOC = meocCrudService.find(OCMeoc.class, operationsCentre.getId());
		mappingService.mapEntityToEntity(operationsCentre, persistedOC);
		return (OCMeoc) meocCrudService.update(persistedOC);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#updateRegisteredOperationsCentreRemote(eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO, java.lang.Long)
	 */
	@Override
	public RegisteredOperationsCentreDTO updateRegisteredOperationsCentreRemote(
			RegisteredOperationsCentreDTO registeredOperationsCentreDTO,
			Long userID) {
		RegisteredOperationsCentre registeredOperationsCentre = (RegisteredOperationsCentre) mappingService
				.mapESponderEntityDTO(registeredOperationsCentreDTO,
						RegisteredOperationsCentre.class);
		return (RegisteredOperationsCentreDTO) mappingService
				.mapESponderEntity(
						updateRegisteredOperationsCentre(
								registeredOperationsCentre, userID),
								RegisteredOperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#updateRegisteredOperationsCentre(eu.esponder.model.crisis.resource.RegisteredOperationsCentre, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredOperationsCentre updateRegisteredOperationsCentre(
			RegisteredOperationsCentre registeredOperationsCentre, Long userID) {
		RegisteredOperationsCentre regOCPersisted = registeredOperationsCentreCrudService
				.find(RegisteredOperationsCentre.class,
						registeredOperationsCentre.getId());
		mappingService.mapEntityToEntity(registeredOperationsCentre,
				regOCPersisted);
		return (RegisteredOperationsCentre) registeredOperationsCentreCrudService
				.update(regOCPersisted);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#createOperationsCentreSnapshotRemote(eu.esponder.dto.model.crisis.resource.OperationsCentreDTO, eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO, java.lang.Long)
	 */
	@Override
	public OperationsCentreSnapshotDTO createOperationsCentreSnapshotRemote(
			OCMeocDTO operationsCentreDTO,
			OperationsCentreSnapshotDTO snapshotDTO, Long userID) {

		OCMeoc operationsCentre = (OCMeoc) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OCMeoc.class);
		OperationsCentreSnapshot operationsCentreSnapshot = (OperationsCentreSnapshot) mappingService
				.mapESponderEntityDTO(snapshotDTO,
						OperationsCentreSnapshot.class);
		operationsCentreSnapshot = createOperationsCentreSnapshot(
				operationsCentre, operationsCentreSnapshot, userID);
		OperationsCentreSnapshotDTO opCentreSnapshotDTO = (OperationsCentreSnapshotDTO) this.mappingService
				.mapESponderEntity(operationsCentreSnapshot,
						OperationsCentreSnapshotDTO.class);
		return opCentreSnapshotDTO;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#createOperationsCentreSnapshot(eu.esponder.model.crisis.resource.OperationsCentre, eu.esponder.model.snapshot.resource.OperationsCentreSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OperationsCentreSnapshot createOperationsCentreSnapshot(
			OCMeoc operationsCentre,
			OperationsCentreSnapshot snapshot, Long userID) {
		snapshot.setOperationsCentre(operationsCentre);
		operationsCentreSnapshotCrudService.create(snapshot);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findSketchPOIRemote(java.lang.Long)
	 */
	@Override
	public List<SketchPOIDTO> findSketchPOIRemote(Long operationsCentreID) {
		return (List<SketchPOIDTO>) mappingService.mapESponderEntity(
				findSketchPOI(operationsCentreID), SketchPOIDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findSketchPOI(java.lang.Long)
	 */
	@Override
	public List<SketchPOI> findSketchPOI(Long operationsCentreID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		return sketchPOICrudService.findWithNamedQuery(
				"SketchPOI.findByOperationsCentre", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findSketchPOIByIdRemote(java.lang.Long)
	 */
	@Override
	public SketchPOIDTO findSketchPOIByIdRemote(Long sketchPOIId) {
		return (SketchPOIDTO) mappingService.mapESponderEntity(
				findSketchPOIById(sketchPOIId), SketchPOIDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findSketchPOIById(java.lang.Long)
	 */
	@Override
	public SketchPOI findSketchPOIById(Long sketchPOIId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sketchPOIId", sketchPOIId);
		SketchPOI sketch = (SketchPOI) sketchPOICrudService
				.findSingleWithNamedQuery("SketchPOI.findBySketchPOIId", params);
		return sketch;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findSketchPOIByTitleRemote(java.lang.String)
	 */
	@Override
	public SketchPOIDTO findSketchPOIByTitleRemote(String title) {
		return (SketchPOIDTO) mappingService.mapESponderEntity(
				findSketchPOIByTitle(title), SketchPOIDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findSketchPOIByTitle(java.lang.String)
	 */
	@Override
	public SketchPOI findSketchPOIByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sketchPOITitle", title);
		return (SketchPOI) sketchPOICrudService.findSingleWithNamedQuery(
				"SketchPOI.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#createSketchPOIRemote(eu.esponder.dto.model.crisis.resource.OperationsCentreDTO, eu.esponder.dto.model.crisis.view.SketchPOIDTO, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SketchPOIDTO createSketchPOIRemote(
			OperationsCentreDTO operationsCentreDTO, SketchPOIDTO sketchDTO,
			Long userID) {
		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		SketchPOI sketchPOI = (SketchPOI) mappingService.mapESponderEntityDTO(
				sketchDTO, SketchPOI.class);
		return (SketchPOIDTO) mappingService.mapESponderEntity(
				createSketchPOI(operationsCentre, sketchPOI, userID),
				SketchPOIDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#createSketchPOI(eu.esponder.model.crisis.resource.OperationsCentre, eu.esponder.model.crisis.view.SketchPOI, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SketchPOI createSketchPOI(OperationsCentre operationsCentre,
			SketchPOI sketch, Long userID) {
		sketch.setOperationsCentre(operationsCentre);
		sketchPOICrudService.create(sketch);
		return sketch;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#updateSketchPOIRemote(eu.esponder.dto.model.crisis.resource.OperationsCentreDTO, eu.esponder.dto.model.crisis.view.SketchPOIDTO, java.lang.Long)
	 */
	@Override
	public SketchPOIDTO updateSketchPOIRemote(
			OperationsCentreDTO operationsCentreDTO, SketchPOIDTO sketchDTO,
			Long userID) {
		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		SketchPOI sketchPOI = (SketchPOI) mappingService.mapESponderEntityDTO(
				sketchDTO, SketchPOI.class);
		return (SketchPOIDTO) mappingService.mapESponderEntity(
				updateSketchPOI(operationsCentre, sketchPOI, userID),
				SketchPOIDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#updateSketchPOI(eu.esponder.model.crisis.resource.OperationsCentre, eu.esponder.model.crisis.view.SketchPOI, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SketchPOI updateSketchPOI(OperationsCentre operationsCentre,
			SketchPOI sketch, Long userID) {
		sketch.setOperationsCentre(operationsCentre);
		SketchPOI sketchPersisted = findSketchPOIById(sketch.getId());
		mappingService.mapEntityToEntity(sketch, sketchPersisted);
		sketchPersisted = (SketchPOI) sketchPOICrudService
				.update(sketchPersisted);
		return sketchPersisted;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#deleteSketchPOIRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteSketchPOIRemote(Long sketchPOIId, Long userID) {
		deleteSketchPOI(sketchPOIId, userID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#deleteSketchPOI(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteSketchPOI(Long sketchPOIId, Long userID) {
		SketchPOI sketchPOI = findSketchPOIById(sketchPOIId);
		if (sketchPOI != null) {
			sketchPOICrudService.delete(sketchPOI);
		}
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findReferencePOIsRemote(java.lang.Long)
	 */
	@Override
	public List<ReferencePOIDTO> findReferencePOIsRemote(Long operationsCentreID) {
		return (List<ReferencePOIDTO>) mappingService.mapESponderEntity(
				findReferencePOIs(operationsCentreID), ReferencePOIDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findReferencePOIs(java.lang.Long)
	 */
	@Override
	public List<ReferencePOI> findReferencePOIs(Long operationsCentreID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		return referencePOICrudService.findWithNamedQuery(
				"ReferencePOI.findByOperationsCentre", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findReferencePOIByIdRemote(java.lang.Long)
	 */
	@Override
	public ReferencePOIDTO findReferencePOIByIdRemote(Long referencePOIId) {
		return (ReferencePOIDTO) mappingService.mapESponderEntity(
				findReferencePOIById(referencePOIId), ReferencePOIDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findReferencePOIById(java.lang.Long)
	 */
	@Override
	public ReferencePOI findReferencePOIById(Long referencePOIId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("referencePOIId", referencePOIId);
		return (ReferencePOI) referencePOICrudService.findSingleWithNamedQuery(
				"ReferencePOI.findByReferencePOIId", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findReferencePOIByTitleRemote(java.lang.String)
	 */
	@Override
	public ReferencePOIDTO findReferencePOIByTitleRemote(String title) {
		return (ReferencePOIDTO) mappingService.mapESponderEntity(
				findReferencePOIByTitle(title), ReferencePOIDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findReferencePOIByTitle(java.lang.String)
	 */
	@Override
	public ReferencePOI findReferencePOIByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("referencePOITitle", title);
		return (ReferencePOI) referencePOICrudService.findSingleWithNamedQuery(
				"ReferencePOI.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#createReferencePOIRemote(eu.esponder.dto.model.crisis.resource.OperationsCentreDTO, eu.esponder.dto.model.crisis.view.ReferencePOIDTO, java.lang.Long)
	 */
	@Override
	public ReferencePOIDTO createReferencePOIRemote(
			OperationsCentreDTO operationsCentreDTO,
			ReferencePOIDTO referencePOIDTO, Long userID) {

		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		ReferencePOI referencePOI = (ReferencePOI) mappingService
				.mapESponderEntityDTO(referencePOIDTO, ReferencePOI.class);
		return (ReferencePOIDTO) mappingService.mapESponderEntity(
				createReferencePOI(operationsCentre, referencePOI, userID),
				ReferencePOIDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#createReferencePOI(eu.esponder.model.crisis.resource.OperationsCentre, eu.esponder.model.crisis.view.ReferencePOI, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReferencePOI createReferencePOI(OperationsCentre operationsCentre,
			ReferencePOI referencePOI, Long userID) {
		referencePOI.setOperationsCentre(operationsCentre);
		referencePOICrudService.create(referencePOI);
		return referencePOI;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#updateReferencePOIRemote(eu.esponder.dto.model.crisis.resource.OperationsCentreDTO, eu.esponder.dto.model.crisis.view.ReferencePOIDTO, java.lang.Long)
	 */
	@Override
	public ReferencePOIDTO updateReferencePOIRemote(
			OperationsCentreDTO operationsCentreDTO,
			ReferencePOIDTO referencePOIDTO, Long userID) {
		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		ReferencePOI referencePOI = (ReferencePOI) mappingService
				.mapESponderEntityDTO(referencePOIDTO, ReferencePOI.class);
		return (ReferencePOIDTO) mappingService.mapESponderEntity(
				updateReferencePOI(operationsCentre, referencePOI, userID),
				ReferencePOIDTO.class);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#updateReferencePOI(eu.esponder.model.crisis.resource.OperationsCentre, eu.esponder.model.crisis.view.ReferencePOI, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReferencePOI updateReferencePOI(OperationsCentre operationsCentre,
			ReferencePOI referencePOI, Long userID) {
		referencePOI.setOperationsCentre(operationsCentre);
		ReferencePOI referencePOIPersisted = findReferencePOIById(referencePOI
				.getId());
		mappingService.mapEntityToEntity(referencePOI, referencePOIPersisted);
		referencePOIPersisted = (ReferencePOI) referencePOICrudService
				.update(referencePOIPersisted);
		return referencePOIPersisted;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#deleteReferencePOIRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteReferencePOIRemote(Long sketchPOIId, Long userID) {
		deleteReferencePOI(sketchPOIId, userID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#deleteReferencePOI(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteReferencePOI(Long sketchPOIId, Long userID) {
		ReferencePOI referencePOI = findReferencePOIById(sketchPOIId);
		if (referencePOI != null) {
			referencePOICrudService.delete(referencePOI);
		}
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#updateOperationsCentreSnapshotDTO(eu.esponder.dto.model.crisis.resource.OperationsCentreDTO, eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO, java.lang.Long)
	 */
	@Override
	public OperationsCentreSnapshotDTO updateOperationsCentreSnapshotDTO(
			OperationsCentreDTO operationsCentreDTO,
			OperationsCentreSnapshotDTO snapshotDTO, Long userID) {
		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		OperationsCentreSnapshot operationsCentreSnapshot = (OperationsCentreSnapshot) mappingService
				.mapESponderEntityDTO(snapshotDTO,
						OperationsCentreSnapshot.class);
		operationsCentreSnapshot = updateOperationsCentreSnapshot(
				operationsCentre, operationsCentreSnapshot, userID);
		snapshotDTO = (OperationsCentreSnapshotDTO) mappingService
				.mapESponderEntity(operationsCentreSnapshot,
						OperationsCentreSnapshotDTO.class);
		return snapshotDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#updateOperationsCentreSnapshot(eu.esponder.model.crisis.resource.OperationsCentre, eu.esponder.model.snapshot.resource.OperationsCentreSnapshot, java.lang.Long)
	 */
	@Override
	public OperationsCentreSnapshot updateOperationsCentreSnapshot(
			OperationsCentre operationsCentre,
			OperationsCentreSnapshot snapshot, Long userID) {
		OperationsCentre operationsCentrePersisted = findOperationCentreById(operationsCentre
				.getId());
		if (operationsCentrePersisted != null) {
			snapshot.setOperationsCentre((OCMeoc)operationsCentrePersisted);
			snapshot = operationsCentreSnapshotCrudService.update(snapshot);
			return snapshot;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#deleteOperationsCentreSnapshotRemote(java.lang.Long)
	 */
	@Override
	public void deleteOperationsCentreSnapshotRemote(
			Long operationsCentreSnapshotDTOID) {
		deleteOperationsCentreSnapshot(operationsCentreSnapshotDTOID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#deleteOperationsCentreSnapshot(java.lang.Long)
	 */
	@Override
	public void deleteOperationsCentreSnapshot(Long operationsCentreSnapshotID) {
		operationsCentreSnapshotCrudService.delete(
				OperationsCentreSnapshot.class, operationsCentreSnapshotID);

	}

	// /////////////////////sketch
	// snapshots////////////////////////////////////////////////////////////////
	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findSketchPOISnapshotByIdRemote(java.lang.Long)
	 */
	@Override
	public SketchPOISnapshotDTO findSketchPOISnapshotByIdRemote(Long sensorID) {
		SketchPOISnapshot sensorSnapshot = findSketchPOISnapshotById(sensorID);
		if (sensorSnapshot != null) {
			return (SketchPOISnapshotDTO) mappingService.mapESponderEntity(
					sensorSnapshot, SketchPOISnapshotDTO.class);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findSketchPOISnapshotById(java.lang.Long)
	 */
	@Override
	public SketchPOISnapshot findSketchPOISnapshotById(Long sensorID) {
		return (SketchPOISnapshot) sketchPOISnapshotCrudService.find(
				SketchPOISnapshot.class, sensorID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findAllSketchPOISnapshotsRemote()
	 */

	@Override
	public List<SketchPOISnapshotDTO> findAllSketchPOISnapshotsRemote() {
		return (List<SketchPOISnapshotDTO>) mappingService.mapESponderEntity(
				findAllSketchPOISnapshots(), SketchPOISnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findAllSketchPOISnapshots()
	 */
	@Override
	public List<SketchPOISnapshot> findAllSketchPOISnapshots() {
		return (List<SketchPOISnapshot>) sketchPOISnapshotCrudService
				.findWithNamedQuery("SketchPOISnapshot.findAll");
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findAllSketchPOISnapshotsByPoiRemote(java.lang.Long, int)
	 */
	@SuppressWarnings({ "unused" })
	@Override
	public List<SketchPOISnapshotDTO> findAllSketchPOISnapshotsByPoiRemote(
			Long SketchPOIID , int resultLimit) {
		SketchPOI sensor;


		return (List<SketchPOISnapshotDTO>) mappingService
				.mapESponderEntity(
						findAllSketchPOISnapshotsByPoi(SketchPOIID, resultLimit),
						SketchPOISnapshotDTO.class);


	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findAllSketchPOISnapshotsByPoi(java.lang.Long, int)
	 */
	@Override
	public List<SketchPOISnapshot> findAllSketchPOISnapshotsByPoi(
			Long SketchPOIID, int resultLimit) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("sketchsnapshotPOIId", SketchPOIID);
		return (List<SketchPOISnapshot>) sketchPOISnapshotCrudService
				.findWithNamedQuery("SketchPOISnapshot.findBySketchPOIId",
						params, resultLimit);
	}

	// --------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findSketchPOISnapshotByDateRemote(java.lang.Long, java.util.Date)
	 */
	@Override
	public SketchPOISnapshotDTO findSketchPOISnapshotByDateRemote(
			Long sensorID, Date dateTo) {
		SketchPOISnapshot sensorSnapshot = findSketchPOISnapshotByDate(
				sensorID, dateTo);
		if (sensorSnapshot != null) {
			return (SketchPOISnapshotDTO) mappingService.mapESponderEntity(
					sensorSnapshot, SketchPOISnapshotDTO.class);
		}
		return null;

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findSketchPOISnapshotByDate(java.lang.Long, java.util.Date)
	 */
	@Override
	public SketchPOISnapshot findSketchPOISnapshotByDate(Long sensorID,
			Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sketchID", sensorID);
		params.put("maxDate", maxDate.getTime());
		SketchPOISnapshot snapshot = (SketchPOISnapshot) sketchPOISnapshotCrudService
				.findSingleWithNamedQuery(
						"SketchPOISnapshot.findBySketchPOIAndDate", params);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findPreviousSketchPOISnapshotRemote(java.lang.Long)
	 */
	@Override
	public SketchPOISnapshotDTO findPreviousSketchPOISnapshotRemote(
			Long sensorID) {
		SketchPOISnapshot sensorSnapshot = findPreviousSketchPOISnapshot(sensorID);
		if (sensorSnapshot != null) {
			return (SketchPOISnapshotDTO) mappingService.mapESponderEntity(
					sensorSnapshot, SketchPOISnapshotDTO.class);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findPreviousSketchPOISnapshot(java.lang.Long)
	 */
	@Override
	public SketchPOISnapshot findPreviousSketchPOISnapshot(Long sensorID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sketchID", sensorID);
		SketchPOISnapshot snapshot = (SketchPOISnapshot) sketchPOISnapshotCrudService
				.findSingleWithNamedQuery(
						"SketchPOISnapshot.findPreviousSnapshot", params);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#createSketchPOISnapshotRemote(eu.esponder.dto.model.snapshot.SketchPOISnapshotDTO, java.lang.Long)
	 */
	@Override
	public SketchPOISnapshotDTO createSketchPOISnapshotRemote(
			SketchPOISnapshotDTO snapshotDTO, Long userID) {
		try {
			SketchPOISnapshot snapshot = (SketchPOISnapshot) mappingService
					.mapESponderEntityDTO(snapshotDTO, mappingService
							.getManagedEntityClass(snapshotDTO.getClass()));
			SketchPOISnapshot snapshotPersisted = this.createSketchPOISnapshot(
					snapshot, userID);
			// SensorSnapshot snapshotPersisted =
			// this.placeSensorSnapshot(snapshot, userID);
			snapshotDTO = (SketchPOISnapshotDTO) mappingService
					.mapESponderEntity(snapshotPersisted, mappingService
							.getDTOEntityClass(snapshot.getClass()));
			return snapshotDTO;
		} catch (ClassNotFoundException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#createSketchPOISnapshot(eu.esponder.model.crisis.view.SketchPOISnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SketchPOISnapshot createSketchPOISnapshot(
			SketchPOISnapshot snapshot, Long userID) {

		snapshot = (SketchPOISnapshot) sketchPOISnapshotCrudService
				.create(snapshot);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#updateSketchPOISnapshotRemote(eu.esponder.dto.model.snapshot.SketchPOISnapshotDTO, java.lang.Long)
	 */
	@Override
	public SketchPOISnapshotDTO updateSketchPOISnapshotRemote(
			SketchPOISnapshotDTO sensorSnapshotDTO, Long userID) {
		SketchPOISnapshot sensorSnapshot = (SketchPOISnapshot) mappingService
				.mapESponderEntityDTO(sensorSnapshotDTO,
						SketchPOISnapshot.class);
		sensorSnapshot = updateSketchPOISnapshot(sensorSnapshot, userID);
		return (SketchPOISnapshotDTO) mappingService.mapESponderEntity(
				sensorSnapshot, SketchPOISnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#updateSketchPOISnapshot(eu.esponder.model.crisis.view.SketchPOISnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SketchPOISnapshot updateSketchPOISnapshot(
			SketchPOISnapshot snapshot, Long userID) {
		return (SketchPOISnapshot) sketchPOISnapshotCrudService
				.update(snapshot);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#deleteSketchPOISnapshotRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteSketchPOISnapshotRemote(Long sensorSnapshotId, Long userID) {
		deleteSketchPOISnapshot(sensorSnapshotId, userID);

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#deleteSketchPOISnapshot(java.lang.Long, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteSketchPOISnapshot(Long sensorSnapshotId, Long userID) {
		SketchPOISnapshot sensorSnapshot = findSketchPOISnapshotById(sensorSnapshotId);
		if (sensorSnapshot != null) {
			sketchPOISnapshotCrudService.delete(SketchPOISnapshot.class,
					sensorSnapshotId);
		} else
			throw new RuntimeException();
	}

	// -------------------------------------------------------------------------

	// /////////////////////////////referencePOI///////////////////////////////////
	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findReferencePOISnapshotByIdRemote(java.lang.Long)
	 */
	@Override
	public ReferencePOISnapshotDTO findReferencePOISnapshotByIdRemote(
			Long referencePOIID) {
		ReferencePOISnapshot referencePOISnapshot = findReferencePOISnapshotById(referencePOIID);
		if (referencePOISnapshot != null) {
			return (ReferencePOISnapshotDTO) mappingService.mapESponderEntity(
					referencePOISnapshot, ReferencePOISnapshotDTO.class);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findReferencePOISnapshotById(java.lang.Long)
	 */
	@Override
	public ReferencePOISnapshot findReferencePOISnapshotById(Long referencePOIID) {
		return (ReferencePOISnapshot) referencePOISnapshotCrudService.find(
				ReferencePOISnapshot.class, referencePOIID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findAllReferencePOISnapshotsRemote()
	 */

	@Override
	public List<ReferencePOISnapshotDTO> findAllReferencePOISnapshotsRemote() {
		return (List<ReferencePOISnapshotDTO>) mappingService
				.mapESponderEntity(findAllReferencePOISnapshots(),
						ReferencePOISnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findAllReferencePOISnapshots()
	 */
	@Override
	public List<ReferencePOISnapshot> findAllReferencePOISnapshots() {
		return (List<ReferencePOISnapshot>) referencePOISnapshotCrudService
				.findWithNamedQuery("ReferencePOISnapshot.findAll");
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findAllReferencePOISnapshotsByReferencePOIRemote(java.lang.Long, int)
	 */

	@Override
	public List<ReferencePOISnapshotDTO> findAllReferencePOISnapshotsByReferencePOIRemote(
			Long referencePOIDTO, int resultLimit)
					throws ClassNotFoundException {
		//ReferencePOI referencePOI = (ReferencePOI) mappingService
		//	.mapESponderEntityDTO(referencePOIDTO, mappingService
		//		.getManagedEntityClass(referencePOIDTO.getClass()));
		return (List<ReferencePOISnapshotDTO>) mappingService
				.mapESponderEntity(
						findAllReferencePOISnapshotsByReferencePOI(
								referencePOIDTO, resultLimit),
								ReferencePOISnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findAllReferencePOISnapshotsByReferencePOI(java.lang.Long, int)
	 */
	@Override
	public List<ReferencePOISnapshot> findAllReferencePOISnapshotsByReferencePOI(
			Long referencePOI, int resultLimit) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("ReferencePOIId", referencePOI);
		return (List<ReferencePOISnapshot>) referencePOISnapshotCrudService
				.findWithNamedQuery(
						"ReferencePOISnapshot.findByReferencePOIId", params,
						resultLimit);
	}

	// --------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findReferencePOISnapshotByDateRemote(java.lang.Long, java.util.Date)
	 */
	@Override
	public ReferencePOISnapshotDTO findReferencePOISnapshotByDateRemote(
			Long referencePOIID, Date dateTo) {
		ReferencePOISnapshot referencePOISnapshot = findReferencePOISnapshotByDate(
				referencePOIID, dateTo);
		if (referencePOISnapshot != null) {
			return (ReferencePOISnapshotDTO) mappingService.mapESponderEntity(
					referencePOISnapshot, ReferencePOISnapshotDTO.class);
		}
		return null;

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findReferencePOISnapshotByDate(java.lang.Long, java.util.Date)
	 */
	@Override
	public ReferencePOISnapshot findReferencePOISnapshotByDate(
			Long referencePOIID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("referenceID", referencePOIID);
		params.put("maxDate", maxDate.getTime());
		ReferencePOISnapshot snapshot = (ReferencePOISnapshot) referencePOISnapshotCrudService
				.findSingleWithNamedQuery(
						"ReferencePOISnapshot.findByReferenceAndDate",
						params);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#findPreviousReferencePOISnapshotRemote(java.lang.Long)
	 */
	@Override
	public ReferencePOISnapshotDTO findPreviousReferencePOISnapshotRemote(
			Long referencePOIID) {
		ReferencePOISnapshot referencePOISnapshot = findPreviousReferencePOISnapshot(referencePOIID);
		if (referencePOISnapshot != null) {
			return (ReferencePOISnapshotDTO) mappingService.mapESponderEntity(
					referencePOISnapshot, ReferencePOISnapshotDTO.class);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#findPreviousReferencePOISnapshot(java.lang.Long)
	 */
	@Override
	public ReferencePOISnapshot findPreviousReferencePOISnapshot(
			Long referencePOIID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("referenceID", referencePOIID);
		ReferencePOISnapshot snapshot = (ReferencePOISnapshot) referencePOISnapshotCrudService
				.findSingleWithNamedQuery(
						"ReferencePOISnapshot.findPreviousSnapshot", params);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#createReferencePOISnapshotRemote(eu.esponder.dto.model.snapshot.ReferencePOISnapshotDTO, java.lang.Long)
	 */
	@Override
	public ReferencePOISnapshotDTO createReferencePOISnapshotRemote(
			ReferencePOISnapshotDTO snapshotDTO, Long userID) {
		try {
			ReferencePOISnapshot snapshot = (ReferencePOISnapshot) mappingService
					.mapESponderEntityDTO(snapshotDTO, mappingService
							.getManagedEntityClass(snapshotDTO.getClass()));
			ReferencePOISnapshot snapshotPersisted = this
					.createReferencePOISnapshot(snapshot, userID);
			// ReferencePOISnapshot snapshotPersisted =
			// this.placeReferencePOISnapshot(snapshot, userID);
			snapshotDTO = (ReferencePOISnapshotDTO) mappingService
					.mapESponderEntity(snapshotPersisted, mappingService
							.getDTOEntityClass(snapshot.getClass()));
			return snapshotDTO;
		} catch (ClassNotFoundException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#createReferencePOISnapshot(eu.esponder.model.snapshot.ReferencePOISnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReferencePOISnapshot createReferencePOISnapshot(
			ReferencePOISnapshot snapshot, Long userID) {
		// snapshot.setReferencePOI((ReferencePOISnapshot)this.findReferencePOIById(snapshot.getReferencePOI().getId()));
		snapshot = (ReferencePOISnapshot) referencePOISnapshotCrudService
				.create(snapshot);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#updateReferencePOISnapshotRemote(eu.esponder.dto.model.snapshot.ReferencePOISnapshotDTO, java.lang.Long)
	 */
	@Override
	public ReferencePOISnapshotDTO updateReferencePOISnapshotRemote(
			ReferencePOISnapshotDTO referencePOISnapshotDTO, Long userID) {
		ReferencePOISnapshot referencePOISnapshot = (ReferencePOISnapshot) mappingService
				.mapESponderEntityDTO(referencePOISnapshotDTO,
						ReferencePOISnapshot.class);
		referencePOISnapshot = updateReferencePOISnapshot(referencePOISnapshot,
				userID);
		return (ReferencePOISnapshotDTO) mappingService.mapESponderEntity(
				referencePOISnapshot, ReferencePOISnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#updateReferencePOISnapshot(eu.esponder.model.snapshot.ReferencePOISnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReferencePOISnapshot updateReferencePOISnapshot(
			ReferencePOISnapshot snapshot, Long userID) {
		return (ReferencePOISnapshot) referencePOISnapshotCrudService
				.update(snapshot);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreRemoteService#deleteReferencePOISnapshotRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteReferencePOISnapshotRemote(Long referencePOISnapshotId,
			Long userID) {
		deleteReferencePOISnapshot(referencePOISnapshotId, userID);

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.OperationsCentreService#deleteReferencePOISnapshot(java.lang.Long, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteReferencePOISnapshot(Long referencePOISnapshotId,
			Long userID) {
		ReferencePOISnapshot referencePOISnapshot = findReferencePOISnapshotById(referencePOISnapshotId);
		if (referencePOISnapshot != null) {
			referencePOISnapshotCrudService.delete(ReferencePOISnapshot.class,
					referencePOISnapshotId);
		} else
			throw new RuntimeException();
	}

	// -------------------------------------------------------------------------

}
