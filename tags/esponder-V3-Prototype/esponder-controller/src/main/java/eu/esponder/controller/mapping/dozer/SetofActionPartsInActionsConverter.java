/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.action.ActionService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.exception.EsponderCheckedException;
//import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class SetofActionPartsInActionsConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected ActionService getActionService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				Object next = it.next();
				if(next.getClass() == ActionPart.class) {

					Set<ActionPart> sourceActionPartSet = (Set<ActionPart>) source;
					Set<Long> destActionPartDTOSet = new HashSet<Long>();
					for(ActionPart actionPart : sourceActionPartSet) {
						destActionPartDTOSet.add(actionPart.getId());
					}
					destination = destActionPartDTOSet;
				}
				else
					if(next.getClass() == Long.class) {

						Set<Long> sourceActionPartDTOSet = (Set<Long>) source;
						Set<ActionPart> destActionPartSet = new HashSet<ActionPart>();
						for(Long cActionPartID : sourceActionPartDTOSet) {
							ActionPart destActionPart = this.getActionService().findActionPartById(cActionPartID);
							destActionPartSet.add(destActionPart);
						}
						destination = destActionPartSet;
					}
					else
						destination = null;
			}
		}
		else {
			new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
