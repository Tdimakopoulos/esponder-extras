/*
 * 
 */
package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.ESponderResourceService;
import eu.esponder.controller.crisis.resource.EquipmentService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.resource.SensorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.snapshot.sensor.config.StatisticsConfig;
import eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum;
import eu.esponder.model.type.SensorType;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class SensorBean.
 */
@Stateless
public class SensorBean implements SensorService, SensorRemoteService {

	/** The sensor crud service. */
	@EJB
	private CrudService<Sensor> sensorCrudService;

	/** The sensor snapshot crud service. */
	@EJB
	private CrudService<SensorSnapshot> sensorSnapshotCrudService;

	/** The statistics config crud service. */
	@EJB
	private CrudService<StatisticsConfig> statisticsConfigCrudService;

	/** The type service. */
	@EJB
	private TypeService typeService;

	/** The equipment service. */
	@EJB
	private EquipmentService equipmentService;

	/** The e sponder mapping service. */
	@EJB
	private ESponderMappingService eSponderMappingService;

	/** The resource service. */
	@EJB
	private ESponderResourceService resourceService;

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#findSensorByIdRemote(java.lang.Long)
	 */
	@Override
	public SensorDTO findSensorByIdRemote(Long sensorId) {
		Sensor sensor = findSensorById(sensorId);
		if(sensor != null) {
			try {
				return (SensorDTO) eSponderMappingService.mapESponderEntity(sensor, eSponderMappingService.getDTOEntityClass(sensor.getClass()));
			} catch (ClassNotFoundException e) {
				new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
				return null;
			}	
		}
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#findSensorById(java.lang.Long)
	 */
	@Override
	public Sensor findSensorById(Long sensorID) {
		return (Sensor) resourceService.findByID(Sensor.class, sensorID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#findConfigByIdRemote(java.lang.Long)
	 */
	@Override
	public StatisticsConfigDTO findConfigByIdRemote(Long configID) {
		StatisticsConfig statisticsConfig = findConfigById(configID);
		if(statisticsConfig != null)
			return (StatisticsConfigDTO) eSponderMappingService.mapESponderEntity(statisticsConfig, StatisticsConfigDTO.class);
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#findConfigById(java.lang.Long)
	 */
	@Override
	public StatisticsConfig findConfigById(Long configID) {
		return (StatisticsConfig) statisticsConfigCrudService.find(StatisticsConfig.class, configID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#findSensorByTitleRemote(java.lang.String)
	 */
	@Override
	public SensorDTO findSensorByTitleRemote(String title) {
		Sensor sensor = findSensorByTitle(title);
		if(sensor != null) {
			try {
				return (SensorDTO) eSponderMappingService.mapESponderEntity(sensor, eSponderMappingService.getDTOEntityClass(sensor.getClass()));
			} catch (ClassNotFoundException e) {
				new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
				return null;
			}	
		}
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#findSensorByTitle(java.lang.String)
	 */
	@Override
	public Sensor findSensorByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Sensor) sensorCrudService.findSingleWithNamedQuery("Sensor.findByTitle", params);
	}	

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#findSensorSnapshotByIdRemote(java.lang.Long)
	 */
	@Override
	public SensorSnapshotDTO findSensorSnapshotByIdRemote(Long sensorID) {
		SensorSnapshot  sensorSnapshot = findSensorSnapshotById(sensorID);
		if(sensorSnapshot != null) {
			return (SensorSnapshotDTO) eSponderMappingService.mapESponderEntity(sensorSnapshot, SensorSnapshotDTO.class);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#findSensorSnapshotById(java.lang.Long)
	 */
	@Override
	public SensorSnapshot findSensorSnapshotById(Long sensorID) {
		return (SensorSnapshot) sensorSnapshotCrudService.find(SensorSnapshot.class, sensorID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#findAllSensorSnapshotsRemote()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SensorSnapshotDTO> findAllSensorSnapshotsRemote() {
		return (List<SensorSnapshotDTO>) eSponderMappingService.mapESponderEntity(findAllSensorSnapshots(), SensorSnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#findAllSensorSnapshots()
	 */
	@Override
	public List<SensorSnapshot> findAllSensorSnapshots() {
		return (List<SensorSnapshot>) sensorSnapshotCrudService.findWithNamedQuery("SensorSnapshot.findAll");
	}

	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#findAllSensorSnapshotsFromToRemote(java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SensorSnapshotDTO> findAllSensorSnapshotsFromToRemote(Long sensorID,Long dateFrom,Long dateTo) {
		return (List<SensorSnapshotDTO>) eSponderMappingService.mapESponderEntity(findAllSensorSnapshotsFromTo(sensorID,dateFrom,dateTo), SensorSnapshotDTO.class);
	
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#findAllSensorSnapshotsFromTo(java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<SensorSnapshot> findAllSensorSnapshotsFromTo(Long sensorID,Long dateFrom,Long dateTo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sensorID", sensorID);
		params.put("maxDate", dateTo);
		params.put("minDate", dateFrom);
		return (List<SensorSnapshot>) sensorSnapshotCrudService.findWithNamedQuery("SensorSnapshot.findBySensorAndPeriod");
	}

	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------

		/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#findAllSensorSnapshotsFromToAndTypeRemote(java.lang.Long, java.lang.Long, java.lang.Long, eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO)
	 */
	@SuppressWarnings("unchecked")
		@Override
		public List<SensorSnapshotDTO> findAllSensorSnapshotsFromToAndTypeRemote(Long sensorID,Long dateFrom,Long dateTo,MeasurementStatisticTypeEnumDTO statisticType) {
			MeasurementStatisticTypeEnum pType=(MeasurementStatisticTypeEnum)eSponderMappingService.mapESponderEnumDTO(statisticType, MeasurementStatisticTypeEnum.class);
			return (List<SensorSnapshotDTO>) eSponderMappingService.mapESponderEntity(findAllSensorSnapshotsFromToAndType(sensorID,dateFrom,dateTo,pType), SensorSnapshotDTO.class);
		
		}

		/* (non-Javadoc)
		 * @see eu.esponder.controller.crisis.resource.SensorService#findAllSensorSnapshotsFromToAndType(java.lang.Long, java.lang.Long, java.lang.Long, eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum)
		 */
		@Override
		public List<SensorSnapshot> findAllSensorSnapshotsFromToAndType(Long sensorID,Long dateFrom,Long dateTo,MeasurementStatisticTypeEnum statisticType) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("sensorID", sensorID);
			params.put("maxDate", dateTo);
			params.put("minDate", dateFrom);
			params.put("statisticTYPE", statisticType);
			return (List<SensorSnapshot>) sensorSnapshotCrudService.findWithNamedQuery("SensorSnapshot.findBySensorAndPeriodAndType");
		}

	//-------------------------------------------------------------------------
		
   //-------------------------------------------------------------------------		

		/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#findAllSensorSnapshotsBySensorAndTypeRemote(eu.esponder.dto.model.crisis.resource.sensor.SensorDTO, eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum, int)
	 */
	@SuppressWarnings("unchecked")
		@Override
		public List<SensorSnapshotDTO> findAllSensorSnapshotsBySensorAndTypeRemote(SensorDTO sensorDTO,MeasurementStatisticTypeEnum statisticType, int resultLimit) throws ClassNotFoundException {
			Sensor sensor = (Sensor) eSponderMappingService.mapESponderEntityDTO(sensorDTO,eSponderMappingService.getManagedEntityClass(sensorDTO.getClass()));
			MeasurementStatisticTypeEnum pType=(MeasurementStatisticTypeEnum)eSponderMappingService.mapESponderEnumDTO(statisticType, MeasurementStatisticTypeEnum.class);
			return (List<SensorSnapshotDTO>) eSponderMappingService.mapESponderEntity(findAllSensorSnapshotsBySensorAndType(sensor, pType,resultLimit), SensorSnapshotDTO.class);
		}

		/* (non-Javadoc)
		 * @see eu.esponder.controller.crisis.resource.SensorService#findAllSensorSnapshotsBySensorAndType(eu.esponder.model.crisis.resource.sensor.Sensor, eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum, int)
		 */
		@Override
		public List<SensorSnapshot> findAllSensorSnapshotsBySensorAndType(Sensor sensor,MeasurementStatisticTypeEnum statisticType, int resultLimit) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("sensor", sensor);
			params.put("statisticTYPE", statisticType);
			return (List<SensorSnapshot>) sensorSnapshotCrudService.findWithNamedQuery("SensorSnapshot.findAllBySensorAndType", params, resultLimit);
		}

	//--------------------------------------------------------------------------
		
    //-------------------------------------------------------------------------		

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#findAllSensorSnapshotsBySensorRemote(eu.esponder.dto.model.crisis.resource.sensor.SensorDTO, int)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SensorSnapshotDTO> findAllSensorSnapshotsBySensorRemote(SensorDTO sensorDTO, int resultLimit) throws ClassNotFoundException {
		Sensor sensor = (Sensor) eSponderMappingService.mapESponderEntityDTO(sensorDTO,eSponderMappingService.getManagedEntityClass(sensorDTO.getClass()));
		return (List<SensorSnapshotDTO>) eSponderMappingService.mapESponderEntity(findAllSensorSnapshotsBySensor(sensor, resultLimit), SensorSnapshotDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#findAllSensorSnapshotsBySensor(eu.esponder.model.crisis.resource.sensor.Sensor, int)
	 */
	@Override
	public List<SensorSnapshot> findAllSensorSnapshotsBySensor(Sensor sensor, int resultLimit) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("sensor", sensor);
		return (List<SensorSnapshot>) sensorSnapshotCrudService.findWithNamedQuery("SensorSnapshot.findAllBySensor", params, resultLimit);
	}

	//--------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#findSensorSnapshotByDateRemote(java.lang.Long, java.util.Date)
	 */
	@Override
	public SensorSnapshotDTO findSensorSnapshotByDateRemote(Long sensorID, Date dateTo) {
		SensorSnapshot  sensorSnapshot = findSensorSnapshotByDate(sensorID, dateTo);
		if(sensorSnapshot != null) {
			return (SensorSnapshotDTO) eSponderMappingService.mapESponderEntity(sensorSnapshot, SensorSnapshotDTO.class);
		}
		return null;

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#findSensorSnapshotByDate(java.lang.Long, java.util.Date)
	 */
	@Override
	public SensorSnapshot findSensorSnapshotByDate(Long sensorID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sensorID", sensorID);
		params.put("maxDate", maxDate.getTime());
		
		SensorSnapshot snapshot =
				(SensorSnapshot) sensorSnapshotCrudService.findSingleWithNamedQuery("SensorSnapshot.findBySensorAndDate", params);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#findPreviousSensorSnapshotRemote(java.lang.Long)
	 */
	@Override
	public SensorSnapshotDTO findPreviousSensorSnapshotRemote(Long sensorID) {
		SensorSnapshot  sensorSnapshot = findPreviousSensorSnapshot(sensorID);
		if(sensorSnapshot != null) {
			return (SensorSnapshotDTO) eSponderMappingService.mapESponderEntity(sensorSnapshot, SensorSnapshotDTO.class);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#findPreviousSensorSnapshot(java.lang.Long)
	 */
	@Override
	public SensorSnapshot findPreviousSensorSnapshot(Long sensorID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sensorID", sensorID);
		SensorSnapshot snapshot = (SensorSnapshot) sensorSnapshotCrudService.findSingleWithNamedQuery("SensorSnapshot.findPreviousSnapshot", params);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#createSensorRemote(eu.esponder.dto.model.crisis.resource.sensor.SensorDTO, java.lang.Long)
	 */
	@Override
	public SensorDTO createSensorRemote(SensorDTO sensorDTO, Long userID) {
		try {
			Sensor sensor = (Sensor) eSponderMappingService.mapESponderEntityDTO(sensorDTO, eSponderMappingService.getManagedEntityClass(sensorDTO.getClass()));
			this.createSensor(sensor, userID);
			sensorDTO = (SensorDTO) eSponderMappingService.mapESponderEntity(sensor, eSponderMappingService.getDTOEntityClass(sensor.getClass()));
			return sensorDTO;
		} catch (ClassNotFoundException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#createSensor(eu.esponder.model.crisis.resource.sensor.Sensor, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Sensor createSensor(Sensor sensor, Long userID) {
		SensorType sensorType = (SensorType)typeService.findById(sensor.getSensorType().getId());
		Equipment equipment = equipmentService.findById(sensor.getEquipment().getId());
		sensor.setSensorType(sensorType);
		sensor.setEquipment(equipment);
		sensor = sensorCrudService.create(sensor);
		return sensor;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#createStatisticConfigRemote(eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO, java.lang.Long)
	 */
	@Override
	public StatisticsConfigDTO createStatisticConfigRemote(StatisticsConfigDTO statisticConfigDTO, Long userID) {
		try {
			StatisticsConfig statisticConfig = (StatisticsConfig) eSponderMappingService.mapESponderEntityDTO(statisticConfigDTO, eSponderMappingService.getManagedEntityClass(statisticConfigDTO.getClass()));
			statisticConfig = this.createStatisticConfig(statisticConfig, userID);
			statisticConfigDTO = (StatisticsConfigDTO) eSponderMappingService.mapESponderEntity(statisticConfig, eSponderMappingService.getDTOEntityClass(statisticConfig.getClass()));
			return statisticConfigDTO;
		} catch (ClassNotFoundException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#createStatisticConfig(eu.esponder.model.snapshot.sensor.config.StatisticsConfig, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public StatisticsConfig createStatisticConfig(StatisticsConfig statisticConfig, Long userID) {
		statisticConfig.setSensor(findSensorById(statisticConfig.getSensor().getId()));
		statisticsConfigCrudService.create(statisticConfig);
		return statisticConfig;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#deleteSensorRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteSensorRemote(Long sensorId, Long userID) {
		deleteSensor(sensorId, userID);
	}


	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#deleteSensor(java.lang.Long, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteSensor(Long sensorId, Long userID) {
		Sensor sensor = findSensorById(sensorId);
		if(sensor != null) {
			sensorCrudService.delete(Sensor.class, sensorId);
		}
		else
			throw new RuntimeException();

	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#updateSensorRemote(eu.esponder.dto.model.crisis.resource.sensor.SensorDTO, java.lang.Long)
	 */
	@Override
	public SensorDTO updateSensorRemote(SensorDTO sensorDTO, Long userID) throws ClassNotFoundException {
		Sensor sensor = (Sensor) eSponderMappingService.mapESponderEntityDTO(sensorDTO, Sensor.class);
		sensor = updateSensor(sensor, userID);
		sensorDTO = (SensorDTO) eSponderMappingService.mapESponderEntity(sensor, SensorDTO.class);
		return sensorDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#updateSensor(eu.esponder.model.crisis.resource.sensor.Sensor, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Sensor updateSensor(Sensor sensor, Long userID) {
		return (Sensor) sensorCrudService.update(sensor);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#createSensorSnapshotRemote(eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO, java.lang.Long)
	 */
	@Override
	public SensorSnapshotDTO createSensorSnapshotRemote(SensorSnapshotDTO snapshotDTO, Long userID) {
		try {
			SensorSnapshot snapshot = (SensorSnapshot) eSponderMappingService.mapESponderEntityDTO(snapshotDTO, eSponderMappingService.getManagedEntityClass(snapshotDTO.getClass()));
			SensorSnapshot snapshotPersisted = this.createSensorSnapshot(snapshot, userID);
//			SensorSnapshot snapshotPersisted = this.placeSensorSnapshot(snapshot, userID);
			snapshotDTO = (SensorSnapshotDTO) eSponderMappingService.mapESponderEntity(snapshotPersisted, eSponderMappingService.getDTOEntityClass(snapshot.getClass()));
			return snapshotDTO;
		} catch (ClassNotFoundException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}


	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#createSensorSnapshot(eu.esponder.model.snapshot.resource.SensorSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SensorSnapshot createSensorSnapshot(SensorSnapshot snapshot, Long userID) {
		snapshot.setSensor((Sensor)this.findSensorById(snapshot.getSensor().getId()));
		snapshot = (SensorSnapshot) sensorSnapshotCrudService.create(snapshot);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#updateSensorSnapshotRemote(eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO, java.lang.Long)
	 */
	@Override
	public SensorSnapshotDTO updateSensorSnapshotRemote( SensorSnapshotDTO sensorSnapshotDTO, Long userID) {
		SensorSnapshot sensorSnapshot = (SensorSnapshot) eSponderMappingService.mapESponderEntityDTO(sensorSnapshotDTO, SensorSnapshot.class);
		sensorSnapshot = updateSensorSnapshot(sensorSnapshot, userID);
		return (SensorSnapshotDTO) eSponderMappingService.mapESponderEntity(sensorSnapshot, SensorSnapshotDTO.class); 
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#updateSensorSnapshot(eu.esponder.model.snapshot.resource.SensorSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SensorSnapshot updateSensorSnapshot(SensorSnapshot snapshot, Long userID) {
		return (SensorSnapshot) sensorSnapshotCrudService.update(snapshot); 
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#deleteSensorSnapshotRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteSensorSnapshotRemote(Long sensorSnapshotId, Long userID) {
		deleteSensorSnapshot(sensorSnapshotId, userID);

	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#deleteSensorSnapshot(java.lang.Long, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteSensorSnapshot(Long sensorSnapshotId, Long userID) {
		SensorSnapshot sensorSnapshot = findSensorSnapshotById(sensorSnapshotId);
		if(sensorSnapshot != null) {
			sensorSnapshotCrudService.delete(SensorSnapshot.class, sensorSnapshotId);
		}
		else
			throw new RuntimeException();
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#updateStatisticConfigRemote(eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO, java.lang.Long)
	 */
	@Override
	public StatisticsConfigDTO updateStatisticConfigRemote(StatisticsConfigDTO statisticsConfigDTO, Long userID) {
		StatisticsConfig statisticsConfig = (StatisticsConfig) eSponderMappingService.mapESponderEntityDTO(statisticsConfigDTO, StatisticsConfig.class);
		statisticsConfig = updateStatisticConfig(statisticsConfig, userID);
		return (StatisticsConfigDTO) eSponderMappingService.mapESponderEntity(statisticsConfig, StatisticsConfigDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#updateStatisticConfig(eu.esponder.model.snapshot.sensor.config.StatisticsConfig, java.lang.Long)
	 */
	@Override
	public StatisticsConfig updateStatisticConfig(StatisticsConfig statisticConfig, Long userID) {
		return statisticsConfigCrudService.update(statisticConfig);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorRemoteService#deleteStatisticConfigRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteStatisticConfigRemote(Long statisticsConfigId, Long userID) {
		deleteStatisticConfig(statisticsConfigId, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.SensorService#deleteStatisticConfig(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteStatisticConfig(Long statisticsConfigId, Long userID) {
		StatisticsConfig statisticsConfig= findConfigById(statisticsConfigId);
		if(statisticsConfig != null) {
			statisticsConfigCrudService.delete(StatisticsConfig.class, statisticsConfigId);
		}
		else
			throw new RuntimeException();
	}


	/**
	 * Place sensor snapshot.
	 *
	 * @param sensorSnapshot the sensor snapshot
	 * @param userID the user id
	 * @return the sensor snapshot
	 */
	@SuppressWarnings("unused")
	private SensorSnapshot placeSensorSnapshot(SensorSnapshot sensorSnapshot, Long userID) {

		SensorSnapshot sensorSnapshotCreated = null;
		SensorSnapshot latestSensorSnapshot = this.findPreviousSensorSnapshot(sensorSnapshot.getSensor().getId());
		if(latestSensorSnapshot == null) {
			sensorSnapshotCreated = createSensorSnapshot(sensorSnapshot, userID);
			return sensorSnapshotCreated;
		}else {
			// Perform date test for latest sensor snapshot
			// if latest is older, set previous and create sensor snapshot
			Date pdate= new Date();
			Date pdatecurrent= new Date();
			pdate.setTime(latestSensorSnapshot.getPeriod().getDateFrom());
			pdatecurrent.setTime(sensorSnapshot.getPeriod().getDateFrom());
			if(pdate.before(pdatecurrent)) {
				sensorSnapshot.setPrevious(latestSensorSnapshot);
				sensorSnapshotCreated = createSensorSnapshot(sensorSnapshot, userID);
				return sensorSnapshotCreated;
			}
			// if latest is not older, then this incoming sensor snapshot is older and
			// rearrangement of snapshots has to occur...
			else {
				// Case when only one snapshot is persisted already and no more
				if(latestSensorSnapshot.getPrevious() == null) {
					sensorSnapshotCreated = createSensorSnapshot(sensorSnapshot, userID);
					latestSensorSnapshot.setPrevious(sensorSnapshotCreated);
					updateSensorSnapshot(latestSensorSnapshot, userID);
					return sensorSnapshotCreated;
				}
				// Case when more than one snapshot is persisted
				// Traversing the Snapshot Hierarchy
				else {
					SensorSnapshot rightSensorSnapshot = latestSensorSnapshot;
					SensorSnapshot leftSensorSnapshot = latestSensorSnapshot.getPrevious();
					Date pdatelatesttmp= new Date();
					Date pdatecurrenttmp= new Date();
					pdatelatesttmp.setTime(leftSensorSnapshot.getPeriod().getDateFrom());
					pdatecurrenttmp.setTime(sensorSnapshot.getPeriod().getDateFrom());
					while(pdatelatesttmp.before(pdatecurrenttmp)) {
						rightSensorSnapshot = leftSensorSnapshot;
						leftSensorSnapshot = leftSensorSnapshot.getPrevious();
					}
					sensorSnapshot.setPrevious(leftSensorSnapshot);
					sensorSnapshotCreated = createSensorSnapshot(sensorSnapshot, userID);
					rightSensorSnapshot.setPrevious(sensorSnapshotCreated);
					rightSensorSnapshot = updateSensorSnapshot(rightSensorSnapshot, userID);
					return sensorSnapshotCreated;
				}
			}	
		}
	}

}
