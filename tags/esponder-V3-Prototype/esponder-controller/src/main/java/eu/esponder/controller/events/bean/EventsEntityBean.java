/*
 * 
 */
package eu.esponder.controller.events.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.events.EventsEntityRemoteService;
import eu.esponder.controller.events.EventsEntityService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.events.entity.OsgiEventsEntityDTO;
import eu.esponder.model.events.entity.OsgiEventsEntity;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class EventsEntityBean.
 */
@Stateless
public class EventsEntityBean implements EventsEntityService, EventsEntityRemoteService {

	/** The osgi events crud service. */
	@EJB
	private CrudService<OsgiEventsEntity> osgiEventsCrudService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityRemoteService#findOsgiEventsEntityByIdRemote(java.lang.Long)
	 */
	@Override
	public OsgiEventsEntityDTO findOsgiEventsEntityByIdRemote(Long osgiEventsEntityID) {
		return (OsgiEventsEntityDTO) mappingService.mapObject(findOsgiEventsEntityById(osgiEventsEntityID), OsgiEventsEntityDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityService#findOsgiEventsEntityById(java.lang.Long)
	 */
	@Override
	public OsgiEventsEntity findOsgiEventsEntityById(Long osgiEventsEntityID) {
		return (OsgiEventsEntity) osgiEventsCrudService.find(OsgiEventsEntity.class, osgiEventsEntityID);
	}

	//-------------------------------------------------------------------------
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityRemoteService#findAllOsgiEventsEntitiesRemote()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OsgiEventsEntityDTO> findAllOsgiEventsEntitiesRemote() {
		return (List<OsgiEventsEntityDTO>) mappingService.mapObject(findAllOsgiEventsEntities(), OsgiEventsEntityDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityService#findAllOsgiEventsEntities()
	 */
	@Override
	public List<OsgiEventsEntity> findAllOsgiEventsEntities() {
		return (List<OsgiEventsEntity>) osgiEventsCrudService.findWithNamedQuery("OsgiEventsEntity.findAll");
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityRemoteService#findOsgiEventsEntitiesBySeverityRemote(java.lang.String)
	 */
	@Override
	public List<OsgiEventsEntityDTO> findOsgiEventsEntitiesBySeverityRemote(String severity) {
		List<OsgiEventsEntityDTO> eventsDTOList = new ArrayList<OsgiEventsEntityDTO>();
		List<OsgiEventsEntity> eventsList = new ArrayList<OsgiEventsEntity>();
		eventsList = findOsgiEventsEntitiesBySeverity(severity);
		if(eventsList != null){
			for(OsgiEventsEntity e : eventsList) {
				eventsDTOList.add((OsgiEventsEntityDTO) mappingService.mapESponderEntity(e, OsgiEventsEntityDTO.class));
			}
			return eventsDTOList;
		}
		else
			return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityService#findOsgiEventsEntitiesBySeverity(java.lang.String)
	 */
	@Override
	public List<OsgiEventsEntity> findOsgiEventsEntitiesBySeverity(String severity) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("severity", severity);
		return (List<OsgiEventsEntity>) osgiEventsCrudService.findWithNamedQuery("OsgiEventsEntity.findBySeverity", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityRemoteService#findOsgiEventsEntitiesBySourceIdRemote(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OsgiEventsEntityDTO> findOsgiEventsEntitiesBySourceIdRemote(Long sourceId) {
		return (List<OsgiEventsEntityDTO>) mappingService.mapObject(findOsgiEventsEntitiesBySourceId(sourceId), OsgiEventsEntityDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityService#findOsgiEventsEntitiesBySourceId(java.lang.Long)
	 */
	@Override
	public List<OsgiEventsEntity> findOsgiEventsEntitiesBySourceId(Long sourceId) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("sourceid", sourceId);
		return (List<OsgiEventsEntity>) osgiEventsCrudService.findWithNamedQuery("OsgiEventsEntity.findBySourceid", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityRemoteService#createOsgiEventsEntityRemote(eu.esponder.dto.model.events.entity.OsgiEventsEntityDTO, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OsgiEventsEntityDTO createOsgiEventsEntityRemote(OsgiEventsEntityDTO osgiEventsEntityDTO, Long userID) {
		OsgiEventsEntity osgiEventsEntity = (OsgiEventsEntity) mappingService.mapObject(osgiEventsEntityDTO, OsgiEventsEntity.class);
		return (OsgiEventsEntityDTO) mappingService.mapObject(createOsgiEventsEntity(osgiEventsEntity, userID), OsgiEventsEntityDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityService#createOsgiEventsEntity(eu.esponder.model.events.entity.OsgiEventsEntity, java.lang.Long)
	 */
	@Override
	public OsgiEventsEntity createOsgiEventsEntity(OsgiEventsEntity osgiEventsEntity, Long userID) {
		return osgiEventsCrudService.create(osgiEventsEntity);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityRemoteService#deleteOsgiEventsEntityRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteOsgiEventsEntityRemote(Long osgiEventsEntityID, Long userID) {
		this.deleteOsgiEventsEntity(osgiEventsEntityID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityService#deleteOsgiEventsEntity(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteOsgiEventsEntity(Long osgiEventsEntityID, Long userID) {
		osgiEventsCrudService.delete(OsgiEventsEntity.class, osgiEventsEntityID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityRemoteService#updateOsgiEventsEntityRemote(eu.esponder.dto.model.events.entity.OsgiEventsEntityDTO, java.lang.Long)
	 */
	@Override
	public OsgiEventsEntityDTO updateOsgiEventsEntityRemote(OsgiEventsEntityDTO osgiEventsEntityDTO, Long userID) {
		OsgiEventsEntity osgiEventsEntity = (OsgiEventsEntity) mappingService.mapObject(osgiEventsEntityDTO, OsgiEventsEntity.class);
		return (OsgiEventsEntityDTO) mappingService.mapObject(updateOsgiEventsEntity(osgiEventsEntity, userID), OsgiEventsEntityDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.events.EventsEntityService#updateOsgiEventsEntity(eu.esponder.model.events.entity.OsgiEventsEntity, java.lang.Long)
	 */
	@Override
	public OsgiEventsEntity updateOsgiEventsEntity(OsgiEventsEntity osgiEventsEntity, Long userID) {
		return (OsgiEventsEntity) osgiEventsCrudService.update(osgiEventsEntity);
	}

}
