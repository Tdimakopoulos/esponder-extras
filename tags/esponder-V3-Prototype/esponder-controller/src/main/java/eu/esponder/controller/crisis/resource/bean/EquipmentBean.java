/*
 * 
 */
package eu.esponder.controller.crisis.resource.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.TypeService;
import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.snapshot.resource.EquipmentSnapshot;
// TODO: Auto-generated Javadoc
//import eu.esponder.model.type.EquipmentType;

// 
/**
 * The Class EquipmentBean.
 */
@Stateless
public class EquipmentBean implements EquipmentService, EquipmentRemoteService {

	/** The equipment crud service. */
	@EJB
	private CrudService<Equipment> equipmentCrudService;

	/** The equipment snapshot crud service. */
	@EJB
	private CrudService<EquipmentSnapshot> equipmentSnapshotCrudService;

	/** The type service. */
	@EJB
	private TypeService typeService;

	/** The actor service. */
	@EJB
	private ActorService actorService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentRemoteService#findByIdRemote(java.lang.Long)
	 */
	@Override
	public EquipmentDTO findByIdRemote(Long equipmentID) {
		return (EquipmentDTO) mappingService.mapESponderEntity(findById(equipmentID), EquipmentDTO.class);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentService#findById(java.lang.Long)
	 */
	@Override
	public Equipment findById(Long equipmentID) {
		return equipmentCrudService.find(Equipment.class, equipmentID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentRemoteService#findByTitleRemote(java.lang.String)
	 */
	@Override
	public EquipmentDTO findByTitleRemote(String title) {
		return (EquipmentDTO) mappingService.mapESponderEntity(findByTitle(title), EquipmentDTO.class);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentService#findByTitle(java.lang.String)
	 */
	@Override
	public Equipment findByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (Equipment) equipmentCrudService.findSingleWithNamedQuery("Equipment.findByTitle", params);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentRemoteService#findEquipmentSnapshotByDateRemote(java.lang.Long, java.util.Date)
	 */
	@Override
	public EquipmentSnapshotDTO findEquipmentSnapshotByDateRemote(Long equipmentID, Date maxDate) {
		return (EquipmentSnapshotDTO) mappingService.mapESponderEntity(findEquipmentSnapshotByDate(equipmentID, maxDate), EquipmentSnapshotDTO.class);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentService#findEquipmentSnapshotByDate(java.lang.Long, java.util.Date)
	 */
	@Override
	public EquipmentSnapshot findEquipmentSnapshotByDate(Long equipmentID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("equipmentID", equipmentID);
		params.put("maxDate", maxDate.getTime());
		EquipmentSnapshot snapshot =
				(EquipmentSnapshot) equipmentCrudService.findSingleWithNamedQuery("EquipmentSnapshot.findByEquipmentAndDate", params);
		return snapshot;
	}


	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentRemoteService#createEquipmentRemote(eu.esponder.dto.model.crisis.resource.EquipmentDTO, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public EquipmentDTO createEquipmentRemote(EquipmentDTO equipmentDTO, Long userID) {
		Equipment equipment = (Equipment) mappingService.mapESponderEntityDTO(equipmentDTO, Equipment.class);
		return (EquipmentDTO) mappingService.mapESponderEntity(createEquipment(equipment, userID), EquipmentDTO.class);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentService#createEquipment(eu.esponder.model.crisis.resource.Equipment, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Equipment createEquipment(Equipment equipment, Long userID) {
		return equipmentCrudService.create(equipment);
	}

	//--------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentRemoteService#deleteEquipmentRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteEquipmentRemote(Long equipmentId, Long userID) {
		deleteEquipment(equipmentId, userID);
	}	

	//--------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentService#deleteEquipment(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteEquipment(Long equipmentId, Long userID) {
		equipmentCrudService.delete(Equipment.class, equipmentId);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentRemoteService#updateEquipmentRemote(eu.esponder.dto.model.crisis.resource.EquipmentDTO, java.lang.Long)
	 */
	@Override
	public EquipmentDTO updateEquipmentRemote(EquipmentDTO equipmentDTO, Long userID) {
		Equipment equipment = (Equipment) mappingService.mapESponderEntityDTO(equipmentDTO, Equipment.class);
		return (EquipmentDTO) mappingService.mapESponderEntity(updateEquipment(equipment, userID), EquipmentDTO.class);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentService#updateEquipment(eu.esponder.model.crisis.resource.Equipment, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Equipment updateEquipment(Equipment equipment, Long userID) {
		return equipmentCrudService.update(equipment);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentRemoteService#createEquipmentSnapshotRemote(eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO, java.lang.Long)
	 */
	@Override
	public EquipmentSnapshotDTO createEquipmentSnapshotRemote(EquipmentSnapshotDTO snapshotDTO, Long userID) {
		EquipmentSnapshot equipmentSnapshot = (EquipmentSnapshot) mappingService.mapESponderEntityDTO(snapshotDTO, EquipmentSnapshot.class);
		return (EquipmentSnapshotDTO) mappingService.mapESponderEntity(createEquipmentSnapshot(equipmentSnapshot, userID), EquipmentSnapshotDTO.class);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentService#createEquipmentSnapshot(eu.esponder.model.snapshot.resource.EquipmentSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public EquipmentSnapshot createEquipmentSnapshot(EquipmentSnapshot snapshot, Long userID) {
		snapshot.setEquipment((Equipment)this.findById(snapshot.getEquipment().getId()));
		equipmentSnapshotCrudService.create(snapshot);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentRemoteService#updateEquipmentSnapshotRemote(eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO, java.lang.Long)
	 */
	@Override
	public EquipmentSnapshotDTO updateEquipmentSnapshotRemote(EquipmentSnapshotDTO snapshotDTO, Long userID) {
		EquipmentSnapshot equipmentSnapshot = (EquipmentSnapshot) mappingService.mapESponderEntityDTO(snapshotDTO, EquipmentSnapshot.class);
		return (EquipmentSnapshotDTO) mappingService.mapESponderEntity(updateEquipmentSnapshot(equipmentSnapshot, userID), EquipmentSnapshotDTO.class);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentService#updateEquipmentSnapshot(eu.esponder.model.snapshot.resource.EquipmentSnapshot, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public EquipmentSnapshot updateEquipmentSnapshot(EquipmentSnapshot snapshot, Long userID) {
		equipmentSnapshotCrudService.update(snapshot);
		return snapshot;
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentRemoteService#deleteEquipmentSnapshotRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteEquipmentSnapshotRemote(Long equipmentSnapshotId, Long userID) {
		deleteEquipmentSnapshot(equipmentSnapshotId, userID);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.resource.EquipmentService#deleteEquipmentSnapshot(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteEquipmentSnapshot(Long equipmentSnapshotId, Long userID) {
		equipmentSnapshotCrudService.delete(EquipmentSnapshot.class, equipmentSnapshotId);
	}

}
