/*
 * 
 */
package eu.esponder.controller.datafusion;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.datafusion.DatafusionResultsDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface DatafusionRemoteService.
 */
@Remote
public interface DatafusionRemoteService {

	/**
	 * Find df resutls by id remote.
	 *
	 * @param DFResultsID the dF results id
	 * @return the datafusion results dto
	 */
	public DatafusionResultsDTO findDFResutlsByIdRemote(Long DFResultsID);

	/**
	 * Find df results by rulename remote.
	 *
	 * @param Rulename the rulename
	 * @return the datafusion results dto
	 */
	public DatafusionResultsDTO findDFResultsByRulenameRemote(String Rulename);

	/**
	 * Creates the df results remote.
	 *
	 * @param DFResults the dF results
	 * @param userID the user id
	 * @return the datafusion results dto
	 */
	public DatafusionResultsDTO createDFResultsRemote(DatafusionResultsDTO DFResults, Long userID);
	
	/**
	 * Find all df results remote.
	 *
	 * @return the list
	 */
	public List<DatafusionResultsDTO> findAllDFResultsRemote();

	
}





