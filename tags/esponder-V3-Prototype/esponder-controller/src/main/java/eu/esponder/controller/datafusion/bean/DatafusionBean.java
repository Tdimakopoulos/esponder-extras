/*
 * 
 */
package eu.esponder.controller.datafusion.bean;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import eu.esponder.controller.datafusion.DatafusionRemoteService;
import eu.esponder.controller.datafusion.DatafusionService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.datafusion.DatafusionResultsDTO;
import eu.esponder.model.datafusion.DatafusionResults;

// TODO: Auto-generated Javadoc
/**
 * The Class DatafusionBean.
 */
@Stateless
public class DatafusionBean implements DatafusionService, DatafusionRemoteService{

	/** The user crud service. */
	@EJB
	private CrudService<DatafusionResults> userCrudService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;

	/* (non-Javadoc)
	 * @see eu.esponder.controller.datafusion.DatafusionRemoteService#findDFResutlsByIdRemote(java.lang.Long)
	 */
	@Override
	public DatafusionResultsDTO findDFResutlsByIdRemote(Long DFResultsID) {
		// 
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.datafusion.DatafusionService#findDFResutlsById(java.lang.Long)
	 */
	@Override
	public DatafusionResults findDFResutlsById(Long DFResultsID) {
		// 
		return null;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.datafusion.DatafusionRemoteService#findDFResultsByRulenameRemote(java.lang.String)
	 */
	@Override
	public DatafusionResultsDTO findDFResultsByRulenameRemote(String Rulename) {
		// 
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.datafusion.DatafusionService#findDFResultsByRulename(java.lang.String)
	 */
	@Override
	public DatafusionResults findDFResultsByRulename(String Rulename) {
		// 
		return null;
	}

	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.datafusion.DatafusionRemoteService#findAllDFResultsRemote()
	 */
	@Override
	public List<DatafusionResultsDTO> findAllDFResultsRemote() {
		// 
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.datafusion.DatafusionService#findAllDFResults()
	 */
	@Override
	public List<DatafusionResults> findAllDFResults() {
		// 
		return null;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.controller.datafusion.DatafusionRemoteService#createDFResultsRemote(eu.esponder.dto.model.datafusion.DatafusionResultsDTO, java.lang.Long)
	 */
	@Override
	public DatafusionResultsDTO createDFResultsRemote(
			DatafusionResultsDTO DFResultsDTO, Long userID) {
		DatafusionResults DFResults = (DatafusionResults) mappingService.mapESponderEntityDTO(
				DFResultsDTO, DatafusionResults.class);
		DFResults = createDFResults(DFResults, userID);
		return (DatafusionResultsDTO) mappingService.mapESponderEntity(DFResults,
				DatafusionResultsDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.datafusion.DatafusionService#createDFResults(eu.esponder.model.datafusion.DatafusionResults, java.lang.Long)
	 */
	@Override
	public DatafusionResults createDFResults(DatafusionResults DFResults,
			Long userID) {
		userCrudService.create(DFResults);
		return DFResults;
	}


	
}


	