/*
 * 
 */
package eu.esponder.df.actionmanager.location;



import eu.esponder.df.ruleengine.utilities.RuleLookup;
import eu.esponder.df.ruleengine.utilities.RuleLookup.RuleLookupType;
import eu.esponder.df.ruleengine.utilities.actorlookup.ActorXMLFileEntry;
import eu.esponder.exception.EsponderCheckedException;


// TODO: Auto-generated Javadoc
/**
 * The Class ActionLocationManager.
 * Manage the if the actor had reached a specific location
 */
public class ActionLocationManager {

	
	/**
	 * Action movement request.
	 *
	 * @param log the log
	 * @param lat the lat
	 * @param alt the alt
	 * @param radius the radius
	 * @param actor the actor
	 */
	public void ActionMovementRequest(Double log,Double lat,Double alt,Double radius,String actor) {

		RuleLookup pLookup = new RuleLookup();
		pLookup.Initialize();
		ActorXMLFileEntry pEntry = new ActorXMLFileEntry();
		pEntry.setdDouble1(lat);
		pEntry.setdDouble2(log);
		pEntry.setdDouble3(alt);
		pEntry.setdDouble4(radius);
		pEntry.setSzString1("Operation");
		pEntry.setSzString2("Move");
		pEntry.setSzString3(actor);
		pEntry.setSzString4("Esponder Location");
		pEntry.setSzString5("N/A");
		pEntry.setSzString6("N/A");

		
		pLookup.AddXMLEntry(pEntry);

		try {
			pLookup.WriteXMLFile(RuleLookupType.DLU_ACTORS);
		} catch (Exception e) {
			System.out
					.println("Error - Writing Action XML FIle - ActionLocationManager - Msg : "
							+ e.getMessage());
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			
		}

	}

	
	/**
	 * Test read file.
	 */
	public void TestReadFile() {
		RuleLookup pLookup = new RuleLookup();
		pLookup.Initialize();
		try {
			pLookup.ReadXMLFile(RuleLookupType.DLU_ACTORS);
			System.out.println("Total Read : " + pLookup.pEntries.size());
		} catch (Exception e) {
			System.out
					.println("Error - Read Action XML FIle - ActionLocationManager - Msg : "
							+ e.getMessage());
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			
		}
	}

	
	/**
	 * Actor moved.
	 *
	 * @param log the log
	 * @param lat the lat
	 * @param alt the alt
	 * @param radius the radius
	 * @param actor the actor
	 */
	public void ActorMoved(Double log,Double lat,Double alt,Double radius,String actor) {
		RuleLookup pLookup = new RuleLookup();
		pLookup.Initialize();
		System.out.println("(ActionLocationManager) Check for "+actor+" : "
				+ pLookup.NewActorLocationMeasurment("Actor Moving","Moving",
						actor, "n/a", lat, log, alt, radius));
	}
}
