/**
 * RuleEngine
 * 
 * This Java class is the implementation of the actual rule engine. This is the parent class and manage the
 * execution of the rules in facts.
 * The rule engine support states, 10 state points are supported and are exposed to use by the rules. Using
 * states we can control which rules can be executed and when, to add support of state on your rules the variables
 * StatePoint-x can be used and set to NOTRUN or FINISHED. 
 * Example of state
 * Rule 1
 * If x10=100 then
 *  StatePoint-1.setState(NOTRUN)
 *  do something else like event
 *  end
 *  
 * Rule 2
 * If x20=200 and StatePoint-1.getState()!=NOTRUN then
 *   do something else like event
 * end
 *
 * @Project   esponder
 * @package   Datafusion
*/
package eu.esponder.df.ruleengine.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.StatefulSession;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.definition.KnowledgePackage;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;

import eu.esponder.exception.EsponderCheckedException;


// TODO: Auto-generated Javadoc
/**
 * The Class RuleEngine.
 *
 * @author tdim
 */
public class RuleEngine {

	/** The kbase. */
	private KnowledgeBase kbase = null;
	
	/** The kbuilder. */
	private KnowledgeBuilder kbuilder = null;
	
	/** The ksession. */
	private StatefulKnowledgeSession ksession = null;
	
	/** The session object. */
	private  StatefulSession sessionObject;
	
	/** The b debug. */
	private boolean bDebug=false;
	
	/** The b dynamic. */
	private boolean bDynamic=true;    
	
	/** The Session created. */
	private boolean SessionCreated=false;
	
	/** The state point1. */
	public State statePoint1=new State("StatePoint-1");
	
	/** The state point2. */
	public State statePoint2=new State("StatePoint-2");
	
	/** The state point3. */
	public State statePoint3=new State("StatePoint-3");
	
	/** The state point4. */
	public State statePoint4=new State("StatePoint-4");
	
	/** The state point5. */
	public State statePoint5=new State("StatePoint-5");
	
	/** The state point6. */
	public State statePoint6=new State("StatePoint-6");
	
	/** The state point7. */
	public State statePoint7=new State("StatePoint-7");
	
	/** The state point8. */
	public State statePoint8=new State("StatePoint-8");
	
	/** The state point9. */
	public State statePoint9=new State("StatePoint-9");
	
	/** The state point10. */
	public State statePoint10=new State("StatePoint-10");
	
	
	/**
	 * Constructor.
	 */
	public RuleEngine() { 
		//System.out.println("Initialize rule engine - Support for DRL and Decision Table Initialized");
	}

	/**
	 * Initialize the 10 state variables.
	 */
	public void AddStateControl()
	{
		AddStateVariable(statePoint1);
		AddStateVariable(statePoint2);
		AddStateVariable(statePoint3);
		AddStateVariable(statePoint4);
		AddStateVariable(statePoint5);
		AddStateVariable(statePoint6);
		AddStateVariable(statePoint7);
		AddStateVariable(statePoint8);
		AddStateVariable(statePoint9);
		AddStateVariable(statePoint10);
	}
	
	/**
	 * Sets the dynamic.
	 *
	 * @param bzDynamic This variable control the dynamic changes of the states, see state.java for more
	 */
	public void SetDynamic(boolean bzDynamic)
	{
		bDynamic=bzDynamic;
	}
	
	
    /**
     * Sets the debug.
     *
     * @param blDebug if true provide debug infromation on system log or eclipse output
     */
    public void SetDebug(boolean blDebug)
    {
    	bDebug=blDebug;
    }
    
	/**
	 * Adds the rules.
	 *
	 * @param rules add rules into the rule engine, rules is string array of filenames stored into the local rep
	 * @throws Exception Drool knowledgebase exception
	 */
	public void AddRules(String[] rules) throws Exception {
		kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		
		for (int i = 0; i < rules.length; i++) {
			String ruleFile = rules[i];
			if(bDebug)
				System.out.println("Loading file: " + ruleFile);
			
			///////////////////////////////////////////////////////////////////////////
			//Generate resource
			//Since drool 5.0.1 kbuilder look into project resources for the rules.
			//so we need to implement the following resource and inputstream to load
			//the rule from a file stored in a general file system
			Reader myStringReader = null;
			File f = new File(ruleFile);
			InputStream ruleFilestr = new FileInputStream(f);
			myStringReader= new InputStreamReader(ruleFilestr);
			Resource res = ResourceFactory.newReaderResource( myStringReader );
			//Generate Resource ends.
			///////////////////////////////////////////////////////////////////////////
			
			
			kbuilder.add( res, ResourceType.DRL );
			if (kbuilder.hasErrors()) {  
	            //throw new RuntimeException(kbuilder.getErrors().toString());
				new EsponderCheckedException(this.getClass(),"KBuilder Error : "+kbuilder.getErrors().toString());
	        } 
			
		}

		Collection<KnowledgePackage> pkgs = kbuilder.getKnowledgePackages();
		kbase.addKnowledgePackages(pkgs);
	}

	/**
	 * Adds the decision table.
	 *
	 * @param fullpath the fullpath
	 * @throws FileNotFoundException the file not found exception
	 */
	public void AddDecisionTable(String fullpath) throws FileNotFoundException
	{
		DecisionTableConfiguration config = KnowledgeBuilderFactory.newDecisionTableConfiguration();
        config.setInputType(DecisionTableInputType.XLS);
        
        Reader myStringReader = null;
		File f = new File(fullpath);
		InputStream ruleFilestr = new FileInputStream(f);
		myStringReader= new InputStreamReader(ruleFilestr);
		Resource res = ResourceFactory.newReaderResource( myStringReader );
        kbuilder.add(res, ResourceType.DTABLE, config);
      
		kbuilder.add( res, ResourceType.DRL );
		if (kbuilder.hasErrors()) {  
			new EsponderCheckedException(this.getClass(),"KBuilder Error : "+kbuilder.getErrors().toString());  
        }
	}
	
	/**
	 * Create the ksession which can be used to pass facts into the rule execution.
	 */
	public void CreateSession() {
		//System.out.println("Try to create session");
		ksession = kbase.newStatefulKnowledgeSession();
		//System.out.println("Create session OK !!!!");
		SessionCreated=true;
	}

	/**
	 * Adds the objects.
	 *
	 * @param facts this is facts that the rules used to get data, any kind of object can be used
	 */
	public void AddObjects(Object[] facts) {
		for (int i = 0; i < facts.length; i++) {
			Object fact = facts[i];
			if(bDebug)
				System.out.println("Inserting fact: " + fact);
			if (SessionCreated==false)
			{
				//System.out.println("hmmm, session is null why ? lets make a new one");
				ksession = kbase.newStatefulKnowledgeSession();
			}
			ksession.insert(fact);
		}
	}
	
	/**
	 * Adds the object.
	 *
	 * @param fact the fact
	 */
	public void AddObject(Object fact) {
		
			if(bDebug)
				System.out.println("Inserting fact: " + fact);
			//System.out.println("Adding DTO Object RE - Ksession add object start");
			if (SessionCreated==false)
			{
				//System.out.println("hmmm, session is null why ? lets make a new one");
				ksession = kbase.newStatefulKnowledgeSession();
			}
			//System.out.println("Ok we have session, lets add the snapshot : "+fact);
			
			
			
			try
			{
			ksession.insert(fact);
			}
			catch (Exception e)
			{
				System.out.println("Add Object Exception error : "+e.getMessage());
				new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			}
			//System.out.println("Adding DTO Objetc RE - Ksession add object End");
		
	}
	
	/**
	 * Adds the state variable.
	 *
	 * @param bName this is the state variable or any other global session variable
	 */
	public void AddStateVariable(State bName)
	{
		
		sessionObject.insert(bName,bDynamic);
	}
	
	/**
	 * close session when rule engine finish.
	 */
	public void CloseSession() {
		ksession.dispose();
	}

	/**
	 * this execute all rules in facts.
	 *
	 * @throws Exception the exception
	 */
	public void RunRules() throws Exception {
		
		ksession.fireAllRules();
		if ( kbuilder.hasErrors() ) {

		    System.err.println( kbuilder.getErrors().toString() );

		}
	}

}
