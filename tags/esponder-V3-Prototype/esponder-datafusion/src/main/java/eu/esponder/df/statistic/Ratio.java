/*
 * 
 */
package eu.esponder.df.statistic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The Class Ratio.
 */
public class Ratio extends SensorStatistics {

	/**
	 * Calculate rate.
	 *
	 * @param SensorID the sensor id
	 * @param dFrom the d from
	 * @param dTo the d to
	 * @return the double
	 */
	public Double CalculateRate(Long SensorID, Date dFrom, Date dTo) {
		List<Double> pList=LoadSensorSnapshots(SensorID,dFrom,dTo);

		return CalculateRate(pList);
	}

	/**
	 * Calculate rate latest.
	 *
	 * @param SensorID the sensor id
	 * @param dTo the d to
	 * @return the double
	 */
	public Double CalculateRateLatest(Long SensorID, Date dTo) {
		List<Double> pList=LoadSensorSnapshots(SensorID,dTo);

		return CalculateRate(pList);
	}

	/**
	 * Calculate rate using measurements.
	 *
	 * @param SensorID the sensor id
	 * @param iNumber the i number
	 * @return the double
	 */
	public Double CalculateRateUsingMeasurements(Long SensorID, int iNumber) {
		List<Double> pList=LoadSensorSnapshots(SensorID,iNumber);

		return CalculateRate(pList);
	}

	/**
	 * Calculate rate.
	 *
	 * @param pList the list
	 * @return the double
	 */
	private Double CalculateRate(List<Double> pList) {
		Double dreturn = null;
		
		List<Double> pChangesInTime = new ArrayList<Double>();
		
		//Calculate values changes between two measurements
		for (int i=0;i<pList.size()-1;i++)
		{
			pChangesInTime.add(pList.get(i)-pList.get(i+1));
		}
		
		//Calculate the Rate
		for (int j=0;j<pChangesInTime.size();j++)
		{
			dreturn=dreturn+pChangesInTime.get(j);
		}
		dreturn=dreturn/pChangesInTime.size();
		return dreturn;
	}
}
