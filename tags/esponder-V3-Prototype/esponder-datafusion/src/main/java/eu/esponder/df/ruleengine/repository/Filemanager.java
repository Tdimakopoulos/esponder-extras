/*
 * 
 */
package eu.esponder.df.ruleengine.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import eu.esponder.exception.EsponderCheckedException;


// TODO: Auto-generated Javadoc
/**
 * The Class Filemanager.
 */
public class Filemanager {

	/**
	 * Gets the files.
	 *
	 * @param szdirectory the szdirectory
	 */
	public void GetFiles(String szdirectory)
	{
		File dir = new File(szdirectory);

		String[] children = null;
		
		// It is also possible to filter the list of returned files.
		// This example does not return any files that start with `.'.
		FilenameFilter filter = new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return !name.startsWith(".drl");
		    }
		};
		children = dir.list(filter);
		if (children == null) {
		    // Either dir does not exist or is not a directory
		} else {
		    for (int i=0; i<children.length; i++) {
		        // Get filename of file or directory
		        String filename = children[i];
		        System.out.println(filename);
		    }
		}
	}
	
	/**
	 * Copyfile.
	 *
	 * @param srFile the sr file
	 * @param dtFile the dt file
	 */
	public void copyfile(String srFile, String dtFile) {
		try {
			File f1 = new File(srFile);
			File f2 = new File(dtFile);
			InputStream in = new FileInputStream(f1);

			// For Append the file.
			// OutputStream out = new FileOutputStream(f2,true);

			// For Overwrite the file.
			OutputStream out = new FileOutputStream(f2);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
			System.out.println("File copied.");
		} catch (FileNotFoundException ex) {
			System.out
					.println(ex.getMessage() + " in the specified directory.");
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+ex.getMessage());
			System.exit(0);
		} catch (IOException e) {
			System.out.println(e.getMessage());
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
		}
	}
}
