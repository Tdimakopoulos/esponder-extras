/*
 * 
 */
package eu.esponder.df.rules.profile;

import eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType;


// TODO: Auto-generated Javadoc
/**
 * The Class ProfileManager.
 */
public class ProfileManager {

	/**
	 * Gets the profile name for sensor.
	 *
	 * @return the profile data
	 */
	public ProfileData GetProfileNameForSensor() {
		ProfileData pReturn = new ProfileData();

		pReturn.setSzProfileName("Esponder.Sensors");
		pReturn.setSzProfileType(RuleEngineType.DRL_RULES);
		pReturn.setSzDecisionTable("NA");
		pReturn.setHasDecisionTable(false);
		
		return pReturn;
	}
	
	/**
	 * Gets the profile name for action.
	 *
	 * @return the profile data
	 */
	public ProfileData GetProfileNameForAction() {
		ProfileData pReturn = new ProfileData();

		pReturn.setSzProfileName("Esponder.Action");
		pReturn.setSzProfileType(RuleEngineType.DRL_RULES);
		pReturn.setSzDecisionTable("NA");
		pReturn.setHasDecisionTable(false);
		
		return pReturn;
	}
	
	/**
	 * Gets the profile name for esponder.
	 *
	 * @return the profile data
	 */
	public ProfileData GetProfileNameForEsponder() {
		ProfileData pReturn = new ProfileData();

		pReturn.setSzProfileName("esponder");
		pReturn.setSzProfileType(RuleEngineType.ALL_PACKAGE);
		pReturn.setSzDecisionTable("NA");
		pReturn.setHasDecisionTable(false);
		
		return pReturn;
	}
	
	
}
