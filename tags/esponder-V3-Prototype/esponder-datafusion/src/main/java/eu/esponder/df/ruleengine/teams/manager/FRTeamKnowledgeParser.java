/**
 * DomainSpecificKnowledgeParser
 * 
 * This Java class implement a DomainSpecificKnowledgeParser to work with DSL and DSLR.
 * 
 *  Example : http://www.softwarepassion.com/getting-started-with-drools-flow/
 *  AddFlowHandler - add handler for each block name
 *  ExecuteFlow - execute flow
 * 
 * @Project   esponder
 * @package   Datafusion
 */
package eu.esponder.df.ruleengine.teams.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.process.ProcessInstance;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import eu.esponder.df.ruleengine.core.EngineKnowledgeLocalRepository;
import eu.esponder.df.ruleengine.core.RuleEngineGuvnorAssets;
import eu.esponder.df.ruleengine.flow.workitem.FlowWorkItemHandler;
import eu.esponder.df.ruleengine.settings.DroolSettings;
import eu.esponder.exception.EsponderCheckedException;


// TODO: Auto-generated Javadoc
/**
 * The Class DomainSpecificKnowledgeParser.
 */
public class FRTeamKnowledgeParser {

	/** The d settings. */
	DroolSettings dSettings = new DroolSettings();
	
	/** The d guvnor. */
	RuleEngineGuvnorAssets dGuvnor = new RuleEngineGuvnorAssets();
	
	/** The d local pero. */
	EngineKnowledgeLocalRepository dLocalPero = new EngineKnowledgeLocalRepository();
	
	/** The dknowledge base. */
	@SuppressWarnings("unused")
	private KnowledgeBase dknowledgeBase = null;
	
	/** The ksession. */
	StatefulKnowledgeSession ksession = null;
	
	/** The d rules dslr source. */
	@SuppressWarnings("rawtypes")
	List dRulesDSLRSource = new LinkedList();
	
	/** The d dsl source. */
	@SuppressWarnings("rawtypes")
	List dDSLSource = new LinkedList();
	
	/** The d rf source. */
	@SuppressWarnings("rawtypes")
	List dRFSource = new LinkedList();
	
	/** The d r fids. */
	@SuppressWarnings("rawtypes")
	List dRFids = new LinkedList();
	
	/** The b has process. */
	boolean bHasProcess = false;
	
	/** The builder. */
	KnowledgeBuilder builder;
	
	/** The d list. */
	@SuppressWarnings("rawtypes")
	ArrayList dList = new ArrayList();

	/**
	 * Creates the knowledge base from dsl.
	 *
	 * @throws Exception the exception
	 */
	private void createKnowledgeBaseFromDSL() throws Exception {

		// //////////////DO NOT REMOVE////////////////////////////////
		// if this processBuilder is remove we have a runtime error.
		// Jboss Drools known Bug recorded in Drools JIRA
		@SuppressWarnings("unused")
		ProcessBuilder processBuilder = new ProcessBuilder();
		// ///////////////////////////////////////////////////////////

		builder = KnowledgeBuilderFactory
				.newKnowledgeBuilder();
		bHasProcess = false;
		for (int i = 0; i < dLocalPero.ReturnDSL().length; i++) {
			Reader myStringReader = null;
			File f = new File(dLocalPero.ReturnDSL()[i]);
			InputStream ruleFilestr = new FileInputStream(f);
			myStringReader = new InputStreamReader(ruleFilestr);
			Resource res = ResourceFactory.newReaderResource(myStringReader);

			builder.add(res, ResourceType.DSL);
		}
		for (int i = 0; i < dLocalPero.ReturnRules().length; i++) {
			Reader myStringReader2 = null;
			File f2 = new File(dLocalPero.ReturnRules()[i]);
			InputStream ruleFilestr2 = new FileInputStream(f2);
			myStringReader2 = new InputStreamReader(ruleFilestr2);
			Resource res2 = ResourceFactory.newReaderResource(myStringReader2);

			builder.add(res2, ResourceType.DSLR);
		}

		for (int i = 0; i < dLocalPero.ReturnRF().length; i++) {

			Reader myStringReader3 = null;
			File f3 = new File(dLocalPero.ReturnRF()[i]);
			InputStream ruleFilestr3 = new FileInputStream(f3);
			myStringReader3 = new InputStreamReader(ruleFilestr3);
			Resource res3 = ResourceFactory.newReaderResource(myStringReader3);

			builder.add(res3, ResourceType.DRF);
			bHasProcess = true;
		}

		if (builder.hasErrors()) {
			throw new RuntimeException(builder.getErrors().toString());
		}

		KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
		knowledgeBase.addKnowledgePackages(builder.getKnowledgePackages());
		ksession = knowledgeBase.newStatefulKnowledgeSession();

		dknowledgeBase = knowledgeBase;

	}

	/**
	 * Adds the flow handler.
	 *
	 * @param szName the sz name
	 * @param handler the handler
	 */
	public void AddFlowHandler(String szName, FlowWorkItemHandler handler) {
		if (bHasProcess) {
			
			ksession.getWorkItemManager().registerWorkItemHandler(szName,
					handler);
		}
	}

	/**
	 * Adds the decision table.
	 *
	 * @param fullpath the fullpath
	 * @throws FileNotFoundException the file not found exception
	 */
	public void AddDecisionTable(String fullpath) throws FileNotFoundException
	{
		DecisionTableConfiguration config = KnowledgeBuilderFactory.newDecisionTableConfiguration();
        config.setInputType(DecisionTableInputType.XLS);
        
        Reader myStringReader = null;
		File f = new File(fullpath);
		InputStream ruleFilestr = new FileInputStream(f);
		myStringReader= new InputStreamReader(ruleFilestr);
		Resource res = ResourceFactory.newReaderResource( myStringReader );
        builder.add(res, ResourceType.DTABLE, config);
	}
	
	/**
	 * Gets the process instances.
	 *
	 * @return the array list
	 */
	@SuppressWarnings("rawtypes")
	public ArrayList GetProcessInstances() {
		return dList;
	}

	/**
	 * Execute flow.
	 */
	@SuppressWarnings("unchecked")
	public void ExecuteFlow() {
		if (bHasProcess) {
			for(int i=0;i<dRFids.size();i++)
			dList.add(ksession.startProcess(dRFids.get(i).toString(), null));

		}
	}

	/**
	 * Adds the objects.
	 *
	 * @param facts the facts
	 */
	public void AddObjects(Object[] facts) {
		for (int i = 0; i < facts.length; i++) {
			Object fact = facts[i];
			ksession.insert(fact);
		}
	}

	/**
	 * Adds the dto object.
	 *
	 * @param fact the fact
	 */
	public void AddDTOObject(Object fact) {
		//System.out.println("Adding DTO Objetc - Ksession add object start");
		ksession.insert(fact);
		//System.out.println("Adding DTO Objetc - Ksession add object end");

	}

	/**
	 * Adds the object.
	 *
	 * @param fact the fact
	 */
	public void AddObject(Object fact) {

		ksession.insert(fact);

	}

	/**
	 * Run rules.
	 */
	public void RunRules() {
		ksession.fireAllRules();

	}

	/**
	 * Dispose.
	 */
	@SuppressWarnings("static-access")
	public void Dispose() {
		boolean bAlive = false;
		if (bHasProcess) {
			ProcessInstance pProcessInstance;
			for (int i = 0; i < this.GetProcessInstances().size(); i++) {
				pProcessInstance = (ProcessInstance) GetProcessInstances().get(
						i);
				if (pProcessInstance.getState() == pProcessInstance.STATE_ACTIVE) {
					bAlive = true;
				}
			}
			if (bAlive == false) {
				ksession.dispose();
			}

		} else {
			ksession.dispose();
		}
	}

	/**
	 * Adds the knowledge for package.
	 *
	 * @param szPackage the sz package
	 */
	public void AddKnowledgeForPackage(String szPackage) {

		ProcessKnowledge(GetDSLAndDSLRForPackage(szPackage));
	}

	/**
	 * Adds the knowledge for category.
	 *
	 * @param szCategory the sz category
	 */
	public void AddKnowledgeForCategory(String szCategory) {
		ProcessKnowledge(GetDSLAndDSLRForCategory(szCategory));
	}

	/**
	 * Gets the dsl and dslr for package.
	 *
	 * @param szPackage the sz package
	 * @return the string
	 */
	private String GetDSLAndDSLRForPackage(String szPackage) {
		return dGuvnor.GetAssetsXMLForPackage(szPackage);
	}

	/**
	 * Gets the dsl and dslr for category.
	 *
	 * @param szCategory the sz category
	 * @return the string
	 */
	private String GetDSLAndDSLRForCategory(String szCategory) {
		return dGuvnor.GetAssetsXMLForCategory(szCategory);
	}

	/**
	 * Process knowledge.
	 *
	 * @param assets the assets
	 */
	private void ProcessKnowledge(String assets) {
		ProcessAssets(assets);
		for (int i = 0; i < this.dDSLSource.size(); i++) {
			dLocalPero.AddNewRuleDSLOnLocalRepository(dDSLSource.get(i)
					.toString(), dSettings.getSzTmpRepositoryDirectoryPath(),
					dGuvnor.GetAssetsTextForURL(dDSLSource.get(i).toString()),
					"dsl");
		}
		for (int i = 0; i < this.dRulesDSLRSource.size(); i++) {
			dLocalPero.AddNewRuleDSLOnLocalRepository(dRulesDSLRSource.get(i)
					.toString(), dSettings.getSzTmpRepositoryDirectoryPath(),
					dGuvnor.GetAssetsTextForURL(dRulesDSLRSource.get(i)
							.toString()), "dslr");
		}

		for (int i = 0; i < this.dRFSource.size(); i++) {
			dLocalPero.AddNewRuleDSLOnLocalRepository(dRFSource.get(i)
					.toString(), dSettings.getSzTmpRepositoryDirectoryPath(),
					dGuvnor.GetAssetsTextForURL(dRFSource.get(i).toString()),
					"rf");
		}

		try {
			createKnowledgeBaseFromDSL();
		} catch (Exception e) {
			System.out.println("Error on Add Kwnoledge - " + e.getMessage()
					+ " Class DomainSpecificKnowledge");
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
		}
	}

	/**
	 * Process assets.
	 *
	 * @param szAssets the sz assets
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void ProcessAssets(String szAssets) {

		String szState;
		String szSourceLink;
		String szType;
        String szTitle;
		dRulesDSLRSource = new ArrayList();
		dDSLSource = new ArrayList();
		dRFSource = new ArrayList();
		dRFids = new ArrayList();
		try {

			org.w3c.dom.Document doc = loadXMLFromString(szAssets);
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("asset");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					szType = getTagValue("format", eElement);
					szState = getTagValue("state", eElement);
					szTitle=getTagValue("title", eElement);
					szSourceLink = getTagValue("sourceLink", eElement);
					if (szState.compareTo("FINISHED") == 0) {
						if (szType.compareTo("dsl") == 0) {
							dDSLSource.add(szSourceLink);

						}
						if (szType.compareTo("dslr") == 0) {
							dRulesDSLRSource.add(szSourceLink);
						}
						if (szType.compareTo("rf") == 0) {
							dRFSource.add(szSourceLink);
							dRFids.add(szTitle);
						}
					}

				}
			}
		} catch (Exception e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
		}
	}

	/**
	 * Gets the tag value.
	 *
	 * @param sTag the tag name
	 * @param eElement the element
	 * @return the value of tag
	 */
	private String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0)
				.getChildNodes();

		Node nValue = (Node) nlList.item(0);

		return nValue.getNodeValue();
	}

	/**
	 * Load xml from string.
	 *
	 * @param xml the string which holds the xml contents
	 * @return document type of w3c.dom
	 * @throws Exception w3c.dom exception type
	 */
	private org.w3c.dom.Document loadXMLFromString(String xml) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		return builder.parse(is);
	}
}
