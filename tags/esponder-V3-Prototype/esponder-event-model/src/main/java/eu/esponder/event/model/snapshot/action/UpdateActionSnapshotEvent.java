/*
 * 
 */
package eu.esponder.event.model.snapshot.action;

import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.event.model.UpdateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateActionSnapshotEvent.
 */
public class UpdateActionSnapshotEvent extends ActionSnapshotEvent<ActionSnapshotDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3628927070794827267L;

	/**
	 * Instantiates a new update action snapshot event.
	 */
	public UpdateActionSnapshotEvent(){
		setJournalMessageInfo("UpdateActionSnapshotEvent");
	}
}
