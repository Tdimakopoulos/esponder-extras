/*
 * 
 */
package eu.esponder.event.model.login;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.crisis.resource.ActorEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class LoginEsponderUserEvent.
 */
public class LoginEsponderUserEvent extends ActorEvent<ESponderUserDTO> implements LoginEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5408128404989456038L;
	
	/**
	 * Instantiates a new login esponder user event.
	 */
	public LoginEsponderUserEvent(){
		setJournalMessageInfo("LoginEsponderUserEvent");
	}

}
