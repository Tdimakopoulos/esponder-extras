/*
 * 
 */
package eu.esponder.event.model.snapshot.measurement;

import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.event.model.ESponderEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class SensorMeasurementEnvelopeEvent.
 *
 * @param <T> the generic type
 */
public abstract class SensorMeasurementEnvelopeEvent<T extends SensorMeasurementEnvelopeDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8470374826798188512L;

}
