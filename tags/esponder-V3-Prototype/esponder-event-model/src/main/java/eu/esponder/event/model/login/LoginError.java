/*
 * 
 */
package eu.esponder.event.model.login;

import eu.esponder.event.model.ESponderOperationEvent;

/**
 * The Interface LoginError.
 */
public abstract interface  LoginError  extends ESponderOperationEvent{

}
