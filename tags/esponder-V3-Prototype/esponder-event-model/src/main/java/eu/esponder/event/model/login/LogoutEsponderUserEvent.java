/*
 * 
 */
package eu.esponder.event.model.login;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.crisis.resource.ActorEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class LogoutEsponderUserEvent.
 */
public class LogoutEsponderUserEvent extends ActorEvent<ESponderUserDTO> implements LogoutEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5408128404989456038L;
	
	/**
	 * Instantiates a new logout esponder user event.
	 */
	public LogoutEsponderUserEvent(){
		setJournalMessageInfo("LogoutEsponderUserEvent");
	}

}
