/*
 * 
 */
package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.event.model.ESponderEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class ActionEvent.
 *
 * @param <T> the generic type
 */
public abstract class ActionEvent<T extends ActionDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -345759696434734591L;
	
}
