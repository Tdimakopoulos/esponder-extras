/*
 * 
 */
package eu.esponder.event.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.codehaus.jackson.annotate.*;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ResourceDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ESponderEvent.
 * 
 * @param <T>
 *            the generic type
 */
public abstract class ESponderEvent<T extends ESponderEntityDTO> implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3165213086735540677L;

	// private static SimpleDateFormat formatter = new SimpleDateFormat
	// ("yyyy.MM.dd ww 'at' hh:mm:ss a zzz");
	/** The formatter. */
	private static SimpleDateFormat formatter = new SimpleDateFormat(
			"yyyy.MM.dd kk:mm:ss");

	/** The event source. */
	private ResourceDTO eventSource;

	/** The journal message info. */
	private String JournalMessageInfo;
	
	/** The event attachment. */
	private T eventAttachment;

	/** The event timestamp. */
	private Date eventTimestamp;

	/** The event severity. */
	private SeverityLevelDTO eventSeverity;

	

	/** The journal message. */
	private String journalMessage;

	/**
	 * Gets the event source str.
	 * 
	 * @return the event source str
	 */
	@JsonIgnore
	public String getEventSourceStr() {
		if (this.getEventSource().getResourceId() == null)
			return "";
		else
			return this.getEventSource().getResourceId();
	}

	/**
	 * Gets the journal message info.
	 * 
	 * @return the journal message info
	 */
	public String getJournalMessageInfo()
	{
		return JournalMessageInfo;
	}

	/**
	 * Sets the journal message info.
	 *
	 * @param journalMessageInfo the new journal message info
	 */
	public void setJournalMessageInfo(String journalMessageInfo) {
		JournalMessageInfo = journalMessageInfo;
	}
	
	/**
	 * Gets the event timestamp str.
	 * 
	 * @return the event timestamp str
	 */
	private String getEventTimestampStr() {
		return formatter.format(eventTimestamp);
	}

	/**
	 * Gets the event attachment.
	 * 
	 * @return the event attachment
	 */
	public T getEventAttachment() {
		return eventAttachment;
	}

	/**
	 * Gets the event source.
	 * 
	 * @return the event source
	 */
	public ResourceDTO getEventSource() {
		return eventSource;
	}

	/**
	 * Sets the event source.
	 * 
	 * @param eventSource
	 *            the new event source
	 */
	public void setEventSource(ResourceDTO eventSource) {
		this.eventSource = eventSource;
	}

	/**
	 * Sets the event attachment.
	 * 
	 * @param eventAttachment
	 *            the new event attachment
	 */
	public void setEventAttachment(T eventAttachment) {
		this.eventAttachment = eventAttachment;
	}

	/**
	 * Gets the event timestamp.
	 * 
	 * @return the event timestamp
	 */
	public Date getEventTimestamp() {
		return eventTimestamp;
	}

	/**
	 * Sets the event timestamp.
	 * 
	 * @param eventTimestamp
	 *            the new event timestamp
	 */
	public void setEventTimestamp(Date eventTimestamp) {
		this.eventTimestamp = eventTimestamp;
	}

	/**
	 * Gets the event severity.
	 * 
	 * @return the event severity
	 */
	public SeverityLevelDTO getEventSeverity() {
		return eventSeverity;
	}

	/**
	 * Sets the event severity.
	 * 
	 * @param eventSeverity
	 *            the new event severity
	 */
	public void setEventSeverity(SeverityLevelDTO eventSeverity) {
		this.eventSeverity = eventSeverity;
	}

	/**
	 * Gets the journal message.
	 * 
	 * @return the journal message
	 */
	public String getJournalMessage() {
		this.journalMessage =
		"TIME: " + getEventTimestampStr() + "\n" + "SOURCE: "
				+ getEventSourceStr() + "\n" + "SEVERITY: "
				+ getEventSeverity().toString();
		if (this.journalMessage == null)
			this.journalMessage = "";
		return this.journalMessage;
	}

	/**
	 * Sets the journal message.
	 * 
	 * @param journalMessage
	 *            the new journal message
	 */
	public void setJournalMessage(String journalMessage) {
		this.journalMessage = journalMessage;
	}

}
