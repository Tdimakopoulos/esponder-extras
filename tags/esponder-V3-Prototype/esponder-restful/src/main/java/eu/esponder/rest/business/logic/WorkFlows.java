/*
 * 
 */
package eu.esponder.rest.business.logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.SensorResultListDTO;
import eu.esponder.dto.model.SensorSnapshotDetailsList;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorCMDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionExpressionEnumDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.CrisisContextSnapshotStatusDTO;
import eu.esponder.dto.model.type.ActorTypeDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.dto.osgi.service.event.ESponderEventPublisher;
import eu.esponder.event.model.snapshot.CreateCrisisContextSnapshotEvent;
import eu.esponder.model.snapshot.Period;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class WorkFlows.
 */
@Path("/business/logic")
public class WorkFlows extends ESponderResource{

	

	/**
	 * Associate actor.
	 *
	 * @param personnelID the personnel id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@GET
	@Path("/crisis/associateCrisisManager")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorCMDTO associateCrisisManager(
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.RESERVED);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorCMDTO actorCM = new ActorCMDTO();
		actorCM.setPersonnel(personnelUpdated);
		actorCM.setStatus(ResourceStatusDTO.AVAILABLE);
		ActorTypeDTO actorType;
		try {
			actorType = (ActorTypeDTO) this.getTypeRemoteService().findDTOByTitle("INIT_FR");
			actorCM.setType(actorType.getTitle());
		} catch (ClassNotFoundException e) {
			System.out.println("Error getting Initial Actor Type");
		}

		actorCM.setTitle(personnel.getTitle());
		actorCM = this.getActorRemoteService().createCrisisManagerRemote(actorCM, userID);

		return actorCM;
	}
	
	/**
	 * Associate actor.
	 *
	 * @param personnelID the personnel id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@GET
	@Path("/crisis/associateIncidentCommander")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorICDTO associateIncidentCommander(
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.RESERVED);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorICDTO actorIC = new ActorICDTO();
		actorIC.setPersonnel(personnelUpdated);
		actorIC.setStatus(ResourceStatusDTO.AVAILABLE);
		ActorTypeDTO actorType;
		try {
			actorType = (ActorTypeDTO) this.getTypeRemoteService().findDTOByTitle("INIT_FR");
			actorIC.setType(actorType.getTitle());
		} catch (ClassNotFoundException e) {
			System.out.println("Error getting Initial Actor Type");
		}

		actorIC.setTitle(personnel.getTitle());
		actorIC = this.getActorRemoteService().createIncidentCommanderRemote(actorIC, userID);

		return actorIC;
	}
	
	/**
	 * Associate actor.
	 *
	 * @param personnelID the personnel id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@GET
	@Path("/crisis/associateFRC")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRCDTO associateActorFRC(
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.RESERVED);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorFRCDTO actorFRC = new ActorFRCDTO();
		actorFRC.setPersonnel(personnelUpdated);
		actorFRC.setStatus(ResourceStatusDTO.AVAILABLE);
		ActorTypeDTO actorType;
		try {
			actorType = (ActorTypeDTO) this.getTypeRemoteService().findDTOByTitle("INIT_FR");
			actorFRC.setType(actorType.getTitle());
		} catch (ClassNotFoundException e) {
			System.out.println("Error getting Initial Actor Type");
		}

		actorFRC.setTitle(personnel.getTitle());
		actorFRC = this.getActorRemoteService().createFRChiefRemote(actorFRC, userID);

		return actorFRC;
	}
	

	/**
	 * Associate actor.
	 *
	 * @param personnelID the personnel id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@GET
	@Path("/crisis/associateFRC")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRDTO associateActorFR(
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.RESERVED);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorFRDTO actorFR = new ActorFRDTO();
		actorFR.setPersonnel(personnelUpdated);
		actorFR.setStatus(ResourceStatusDTO.AVAILABLE);
		ActorTypeDTO actorType;
		try {
			actorType = (ActorTypeDTO) this.getTypeRemoteService().findDTOByTitle("INIT_FR");
			actorFR.setType(actorType.getTitle());
		} catch (ClassNotFoundException e) {
			System.out.println("Error getting Initial Actor Type");
		}

		actorFR.setTitle(personnel.getTitle());
		actorFR = this.getActorRemoteService().createFRRemote(actorFR, userID);

		return actorFR;
	}
	
	
	
	/**
	 * Associate actor.
	 *
	 * @param personnelID the personnel id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@GET
	@Path("/crisis/associateESpondeUser")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderUserDTO associateESponderUser(
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.RESERVED);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ESponderUserDTO user = new ESponderUserDTO();
		user.setPersonnel(personnelUpdated);
		user.setStatus(ResourceStatusDTO.AVAILABLE);
		ActorTypeDTO actorType;
		try {
			actorType = (ActorTypeDTO) this.getTypeRemoteService().findDTOByTitle("INIT_FR");
			user.setType(actorType.getTitle());
		} catch (ClassNotFoundException e) {
			System.out.println("Error getting Initial Actor Type");
		}

		user.setTitle(personnel.getTitle());
		user = this.getActorRemoteService().createESponderUseremote(user, userID);

		return user;
	}
	

	/**
	 * Deassociate actor.
	 *
	 * @param actorID the actor id
	 * @param personnelID the personnel id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@GET
	@Path("/crisis/deassociateCrisisManager")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorCMDTO deassociateCrisisManager(
			@QueryParam("actorID") @NotNull(message="actorID may not be null") Long actorID,
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.AVAILABLE);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorCMDTO actorCM = this.getActorRemoteService().findCrisisManagerByIdRemote(actorID);
		actorCM.setPersonnel(personnelUpdated);
		actorCM.setStatus(ResourceStatusDTO.UNAVAILABLE);
		actorCM = this.getActorRemoteService().updateCrisisManagerRemote(actorCM, userID);
		return actorCM;
	}
	
	/**
	 * Deassociate incident commander.
	 *
	 * @param actorID the actor id
	 * @param personnelID the personnel id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor icdto
	 */
	@GET
	@Path("/crisis/deassociateIncidentCommander")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorICDTO deassociateIncidentCommander(
			@QueryParam("actorID") @NotNull(message="actorID may not be null") Long actorID,
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.AVAILABLE);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorICDTO actorIC = this.getActorRemoteService().findIncidentCommanderByIdRemote(actorID);
		actorIC.setPersonnel(personnelUpdated);
		actorIC.setStatus(ResourceStatusDTO.UNAVAILABLE);
		actorIC = this.getActorRemoteService().updateIncidentCommanderRemote(actorIC, userID);
		return actorIC;
	}
	
	/**
	 * Deassociate e sponder user.
	 *
	 * @param actorID the actor id
	 * @param personnelID the personnel id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the e sponder user dto
	 */
	@GET
	@Path("/crisis/deassociateESponderUser")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderUserDTO deassociateESponderUser(
			@QueryParam("actorID") @NotNull(message="actorID may not be null") Long actorID,
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.AVAILABLE);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ESponderUserDTO user = this.getActorRemoteService().findESponderUserByIdRemote(actorID);
		user.setPersonnel(personnelUpdated);
		user.setStatus(ResourceStatusDTO.UNAVAILABLE);
		user = this.getActorRemoteService().updateEsponderUserRemote(user, userID);
		return user;
	}
	
	
	/**
	 * Deassociate actor frc.
	 *
	 * @param actorID the actor id
	 * @param personnelID the personnel id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor frcdto
	 */
	@GET
	@Path("/crisis/deassociateActorFRC")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRCDTO deassociateActorFRC(
			@QueryParam("actorID") @NotNull(message="actorID may not be null") Long actorID,
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.AVAILABLE);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorFRCDTO actorFRC = this.getActorRemoteService().findFRChiefByIdRemote(userID);
		actorFRC.setPersonnel(personnelUpdated);
		actorFRC.setStatus(ResourceStatusDTO.UNAVAILABLE);
		actorFRC = this.getActorRemoteService().updateFRCRemote(actorFRC, userID);
		return actorFRC;
	}
	
	
	/**
	 * Deassociate actor fr.
	 *
	 * @param actorID the actor id
	 * @param personnelID the personnel id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor frdto
	 */
	@GET
	@Path("/crisis/deassociateActorFR")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorFRDTO deassociateActorFR(
			@QueryParam("actorID") @NotNull(message="actorID may not be null") Long actorID,
			@QueryParam("personnelID") @NotNull(message="personnelID may not be null") Long personnelID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		// Set Personnel to RESERVED
		PersonnelDTO personnel = this.getPersonnelRemoteService().findPersonnelByIdRemote(personnelID);
		personnel.setStatus(ResourceStatusDTO.AVAILABLE);
		PersonnelDTO personnelUpdated = this.getPersonnelRemoteService().updatePersonnelRemote(personnel, userID);

		//Create actor
		ActorFRDTO actorFR = this.getActorRemoteService().findFRByIdRemote(userID);
		actorFR.setPersonnel(personnelUpdated);
		actorFR.setStatus(ResourceStatusDTO.UNAVAILABLE);
		actorFR = this.getActorRemoteService().updateFRRemote(actorFR, userID);
		return actorFR;
	}


	/**
	 * Associate reusable.
	 *
	 * @param crisisContextID the crisis context id
	 * @param regreusableID the regreusable id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reusable resource dto
	 */
	@GET
	@Path("/crisis/associateReusable")
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceDTO associateReusable(
			@QueryParam("crisisContextID") @NotNull(message="Crisis Context ID may not be null") Long crisisContextID,
			@QueryParam("regReusableID") @NotNull(message="Registered Reusable Resource ID may not be null") Long regreusableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		RegisteredReusableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredReusableResourceByIdRemote(regreusableID);

		ReusableResourceDTO reusableResource = new ReusableResourceDTO();
		reusableResource.setCrisisContextId(crisisContextID);
		reusableResource.setQuantity(regResourceDTO.getQuantity());
		reusableResource.setTitle(regResourceDTO.getTitle());
		reusableResource.setStatus(ResourceStatusDTO.AVAILABLE);
		ReusableResourceCategoryDTO reusableCategoryDTO = (ReusableResourceCategoryDTO) this.getResourceCategoryRemoteService().
				findByIdRemote(ReusableResourceCategoryDTO.class, regResourceDTO.getReusableResourceCategoryId()); 
		reusableResource.setReusableResourceCategory(reusableCategoryDTO);
		ReusableResourceDTO reusableResourcep = this.getLogisticsRemoteService().createReusableResourceRemote(reusableResource, userID);


		regResourceDTO.setStatus(ResourceStatusDTO.RESERVED);
		this.getLogisticsRemoteService().updateRegisteredReusableResourceRemote(regResourceDTO, userID);

		return reusableResourcep;
	}

	/**
	 * Deassociate reusable.
	 *
	 * @param reusableID the reusable id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered reusable resource dto
	 */
	@GET
	@Path("/crisis/deassociateReusable")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredReusableResourceDTO deassociateReusable(
			@QueryParam("reusableID") @NotNull(message="Reusable Resource ID may not be null") Long reusableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ReusableResourceDTO reusableResource = this.getLogisticsRemoteService().findReusableResourceByIdRemote(reusableID);
		RegisteredReusableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredReusableResourceByTitleRemote(reusableResource.getTitle());

		regResourceDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		this.getLogisticsRemoteService().updateRegisteredReusableResourceRemote(regResourceDTO, userID);
		this.getLogisticsRemoteService().deleteReusableResourceRemote(reusableID, userID);
		return regResourceDTO;
	}

	/**
	 * Associate consumable.
	 *
	 * @param crisisContextID the crisis context id
	 * @param regconsumableID the regconsumable id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the consumable resource dto
	 */
	@GET
	@Path("/crisis/associateConsumable")
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceDTO associateConsumable(
			@QueryParam("crisisContextID") @NotNull(message="Crisis Context ID may not be null") Long crisisContextID,
			@QueryParam("regConsumableID") @NotNull(message="Registered Consumable Resource ID may not be null") Long regconsumableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		RegisteredConsumableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredConsumableResourceByIdRemote(regconsumableID);

		ConsumableResourceDTO consumableResource = new ConsumableResourceDTO();
		consumableResource.setCrisisContextId(crisisContextID);
		consumableResource.setQuantity(regResourceDTO.getQuantity());
		consumableResource.setTitle(regResourceDTO.getTitle());
		consumableResource.setStatus(ResourceStatusDTO.AVAILABLE);
		ConsumableResourceCategoryDTO consumableCategoryDTO = (ConsumableResourceCategoryDTO) this.getResourceCategoryRemoteService().
				findByIdRemote(ConsumableResourceCategoryDTO.class, regResourceDTO.getConsumableResourceCategoryId()); 
		consumableResource.setConsumableResourceCategory(consumableCategoryDTO);
		ConsumableResourceDTO consumableResourcep=this.getLogisticsRemoteService().createConsumableResourceRemote(consumableResource, userID);


		regResourceDTO.setStatus(ResourceStatusDTO.RESERVED);
		this.getLogisticsRemoteService().updateRegisteredConsumableResourceRemote(regResourceDTO, userID);

		return consumableResourcep;
	}

	/**
	 * Deassociate consumable.
	 *
	 * @param consumableID the consumable id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered consumable resource dto
	 */
	@GET
	@Path("/crisis/deassociateConsumable")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredConsumableResourceDTO deassociateConsumable(
			@QueryParam("consumableID") @NotNull(message="Consumable Resource ID may not be null") Long consumableID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ConsumableResourceDTO consumableResource = this.getLogisticsRemoteService().findConsumableResourceByIdRemote(consumableID);
		RegisteredConsumableResourceDTO regResourceDTO = this.getLogisticsRemoteService().findRegisteredConsumableResourceByTitleRemote(consumableResource.getTitle());

		regResourceDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		this.getLogisticsRemoteService().updateRegisteredConsumableResourceRemote(regResourceDTO, userID);
		this.getLogisticsRemoteService().deleteConsumableResourceRemote(consumableID, userID);
		return regResourceDTO;
	}

	/**
	 * Associate oc.
	 *
	 * @param ccID the cc id
	 * @param regocID the regoc id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the operations centre dto
	 */
	@GET
	@Path("/crisis/associateoc")
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreDTO associateOC(
			@QueryParam("ccID") @NotNull(message="Crisis Context Id may not be null") Long ccID,
			@QueryParam("regocID") @NotNull(message="Registered Operations Centre ID may not be null") Long regocID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		//FIXME UPDATE TO MODEL v3
//
//		RegisteredOperationsCentreDTO regOC = this.getOperationsCentreRemoteService().findRegisteredOperationCentreByIdRemote(regocID);
//
//		OperationsCentreDTO oc = new OperationsCentreDTO();
////		oc.setCrisisContextId(ccID);
//		oc.setTitle(regOC.getTitle());
//		oc.setType(regOC.getType());
//		oc.setOperationsCentreCategoryId(regOC.getOperationsCentreCategoryId());
//		oc.setVoIPURL(regOC.getVoIPURL());
//		oc.setStatus(ResourceStatusDTO.AVAILABLE);
//
//		OperationsCentreDTO ocp = this.getOperationsCentreRemoteService().createOperationsCentreRemote(oc, userID);
//
//		regOC.setStatus(ResourceStatusDTO.RESERVED);
//		this.getOperationsCentreRemoteService().updateRegisteredOperationsCentreRemote(regOC, userID);
//
//		return ocp;
		return null;
	}

	/**
	 * Deassociate oc.
	 *
	 * @param ocID the oc id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered operations centre dto
	 */
	@GET
	@Path("/crisis/deassociateoc")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredOperationsCentreDTO deassociateOC(
			@QueryParam("ocID") @NotNull(message="Operations Centre ID may not be null") Long ocID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		OperationsCentreDTO oc = this.getOperationsCentreRemoteService().findOperationCentreByIdRemote(ocID);
		RegisteredOperationsCentreDTO regOC = this.getOperationsCentreRemoteService().findRegisteredOperationsCentreByTitleRemote(oc.getTitle());
		regOC.setStatus(ResourceStatusDTO.AVAILABLE);
		regOC = this.getOperationsCentreRemoteService().updateRegisteredOperationsCentreRemote(regOC, userID);
		this.getOperationsCentreRemoteService().deleteOperationsCentreRemote(ocID, userID);
		return regOC;
	}



	/**
	 * Creates the crisis context snapshots.
	 *
	 * @param ccID the cc id
	 * @param szLocationAreaTitle the sz location area title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context snapshot dto
	 */
	@POST
	@Path("/crisis/createcrisissnapshot")
	@Produces({ MediaType.APPLICATION_JSON })
	public CrisisContextSnapshotDTO CreateCrisisContextSnapshots(

			@QueryParam("ccID") @NotNull(message = "Crisis Context Id may not be null") Long ccID,
			@QueryParam("LocationTitle") @NotNull(message = "Location area Title may not be null") String szLocationAreaTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		CrisisContextSnapshotDTO crisisContextSnapshotPersisted = this.getCrisisRemoteService().createCrisisContextSnapshotRemote(
				CreateContextSnap(ccID, szLocationAreaTitle), userID);

		CreateCrisisContextSnapshotEvent crisisContextSnapshotEvent = new CreateCrisisContextSnapshotEvent();
		crisisContextSnapshotEvent.setEventAttachment(crisisContextSnapshotPersisted);
		crisisContextSnapshotEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
		crisisContextSnapshotEvent.setEventTimestamp(new Date());
		crisisContextSnapshotEvent.setJournalMessage(crisisContextSnapshotEvent.getJournalMessageInfo());
		ActorDTO subActorDTO = this.getActorRemoteService().findByIdRemote(userID);
		crisisContextSnapshotEvent.setEventSource(subActorDTO);

		ESponderEventPublisher<CreateCrisisContextSnapshotEvent> publisher = new ESponderEventPublisher<CreateCrisisContextSnapshotEvent>(CreateCrisisContextSnapshotEvent.class);
		try {
			publisher.publishEvent(crisisContextSnapshotEvent);
		} catch (EventListenerException e) {
			e.printStackTrace();
		}
		publisher.CloseConnection();

		return crisisContextSnapshotPersisted;

	}

//	/**
//	 * Load sensors snapshots.
//	 *
//	 * @param pkiKey the pki key
//	 * @return the sensor result list dto
//	 * @throws JsonParseException the json parse exception
//	 * @throws JsonMappingException the json mapping exception
//	 * @throws IOException Signals that an I/O exception has occurred.
//	 * @throws ClassNotFoundException the class not found exception
//	 */
//	@GET
//	@Path("/sensorsnapshots/getAllSnapshotsByAllActors")
//	@Produces({ MediaType.APPLICATION_JSON })
//	public SensorResultListDTO LoadSensorsSnapshots(
//			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey)
//					throws JsonParseException, JsonMappingException, IOException, ClassNotFoundException {
//FIXME Update model v3	-->	
		//		Long userID=SecurityCheck(pkiKey);
//		// intialize return variable
//		List<SensorSnapshotDetailsList> pSensorsSnapshots = new ArrayList<SensorSnapshotDetailsList>();
//
//		// Get all actors
//		List<ActorDTO> actorsList = this.getActorRemoteService().findAllActorsRemote();
//
//		for(ActorDTO actor : actorsList) {
//			for(EquipmentDTO equipment : actor.getEquipmentSet()) {
//				for(Long sensorID : equipment.getSensors()) {
//					SensorDTO sensorDTO = this.getSensorRemoteService().findSensorByIdRemote(sensorID);
//					List<SensorSnapshotDTO> sensorSnapshots = (List<SensorSnapshotDTO>) this.getSensorRemoteService().findAllSensorSnapshotsBySensorRemote(sensorDTO, 10);
//					for(SensorSnapshotDTO sensorSnapshotDTO : sensorSnapshots) {
//						SensorSnapshotDetailsList pItem = new SensorSnapshotDetailsList();
//						pItem.setpActor(actor);
//						pItem.setpEquipment(equipment);
//						pItem.setpSensorSnapshot(sensorSnapshotDTO);
//						pSensorsSnapshots.add(pItem);
//					}
//				}
//			}
//		}
//		return new SensorResultListDTO(pSensorsSnapshots);
//		return null;
//	}

	/**
 * Load sensors snapshots.
 *
 * @param actorID the actor id
 * @param userID the user id
 * @param sessionID the session id
 * @return the sensor result list dto
 * @throws ClassNotFoundException the class not found exception
 */
	@GET
	@Path("/sensorsnapshots/getAllSnapshotsByActor")
	@Produces({ MediaType.APPLICATION_JSON })
	public SensorResultListDTO LoadSensorsSnapshots(
			@QueryParam("actorID") @NotNull(message = "actorID may not be null") Long actorID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws ClassNotFoundException {
		
		// intialize return variable
		List<SensorSnapshotDetailsList> pSensorsSnapshots = new ArrayList<SensorSnapshotDetailsList>();
		FirstResponderActorDTO actorDTO = (FirstResponderActorDTO) this.getActorRemoteService().findByIdRemote(actorID);
		for(EquipmentDTO equipment : actorDTO.getEquipmentSet()) {
			for(Long sensorID : equipment.getSensors()) {
				SensorDTO sensorDTO = this.getSensorRemoteService().findSensorByIdRemote(sensorID);
				List<SensorSnapshotDTO> sensorSnapshots = (List<SensorSnapshotDTO>) this.getSensorRemoteService().findAllSensorSnapshotsBySensorRemote(sensorDTO, 10);
				for(SensorSnapshotDTO sensorSnapshotDTO : sensorSnapshots) {
					SensorSnapshotDetailsList pItem = new SensorSnapshotDetailsList();
					pItem.setpActor(actorDTO);
					pItem.setpEquipment(equipment);
					pItem.setpSensorSnapshot(sensorSnapshotDTO);
					pSensorsSnapshots.add(pItem);
				}
			}
		}
		return new SensorResultListDTO(pSensorsSnapshots);
	}

	/**
	 * Load actors by crisis context.
	 *
	 * @param crisisContextID the crisis context id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the result list dto
	 */
	@GET
	@Path("/sensorsnapshots/getAllActorsByCrisisContext")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO LoadActorsByCrisisContext(
			@QueryParam("crisisContextID") @NotNull(message = "crisisContextID may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
//		CrisisContextDTO crisisContextDTO = this.getCrisisRemoteService().findCrisisContextDTOById(crisisContextID);
//		List<ActorDTO> actorsList = new ArrayList<ActorDTO>();
//		for(OperationsCentreDTO oc : crisisContextDTO.getOperationsCentres()) {
//			if(oc instanceof OCMeocDTO) {
//				for(FRTeamDTO team : ((OCMeocDTO)oc).getSubordinates())
//					actorsList.add(team.getFrchief());
//			}
//		}
//
//		return new ResultListDTO(actorsList);
		return null;
	}


	/**
	 * Creates the context snap.
	 *
	 * @param ccID the cc id
	 * @param szLocationAreaTitle the sz location area title
	 * @return the crisis context snapshot dto
	 */
	private CrisisContextSnapshotDTO CreateContextSnap(Long ccID,
			String szLocationAreaTitle) {
		CrisisContextSnapshotDTO crisisContextSnapshotDTO = new CrisisContextSnapshotDTO();
		CrisisContextSnapshotStatusDTO SnapshotStatus = CrisisContextSnapshotStatusDTO.STARTED;
		PeriodDTO period = createPeriodDTO(10);

		
		crisisContextSnapshotDTO.setCrisisContext(this.getCrisisRemoteService()
				.findCrisisContextDTOById(ccID));// .crisisService.findCrisisContextDTOByTitle("Fire Brigade Drill"));
		// crisisContextSnapshotDTO.setLocationArea(this.createSphereDTO(38.025334,
		// 23.802717, null, RADIUS, "SnapshotLoc3"));

		LocationAreaDTO plocation = GetLocation(szLocationAreaTitle);
		crisisContextSnapshotDTO.setLocationArea(plocation);
		crisisContextSnapshotDTO.setPeriod(period);
		crisisContextSnapshotDTO.setStatus(SnapshotStatus);
		return crisisContextSnapshotDTO;

	}

	/**
	 * Gets the location.
	 *
	 * @param szLocationAreaTitle the sz location area title
	 * @return the location area dto
	 */
	@SuppressWarnings("unchecked")
	private LocationAreaDTO GetLocation(String szLocationAreaTitle) {
		LocationAreaDTO areaDTO1 = null;

		List<LocationAreaDTO> results = null;
		try {
			results = (List<LocationAreaDTO>) this.getGenericRemoteService().getDTOEntities(LocationAreaDTO.class.getName(),new EsponderCriterionDTO("title",	EsponderCriterionExpressionEnumDTO.EQUAL,szLocationAreaTitle), 10, 0);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!results.isEmpty() && results.size() < 2) {
			areaDTO1 = results.get(0);
		}
		return areaDTO1;
	}

	/**
	 * Creates the period dto.
	 *
	 * @param seconds the seconds
	 * @return the period dto
	 */
	protected PeriodDTO createPeriodDTO(int seconds) {
		java.util.Date now = new java.util.Date();
		Period period = new Period(now.getTime(), now.getTime()+new Long(seconds));
		PeriodDTO periodDTO = (PeriodDTO) this.getMappingRemoteService().mapObject(period, PeriodDTO.class);
		return periodDTO;
	}

}