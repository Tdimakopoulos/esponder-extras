/*
 * 
 */
package eu.esponder.rest.portal;

import java.util.List;

import org.codehaus.enunciate.XmlTransient;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class SensorThread.
 */
@XmlTransient
public abstract class SensorThread extends Thread {

	/*
	 * The period at which sensor measurement statistics are generated in msec
	 * 
	 *  Also, the Sensor.StatisticsConfig.sampling period is the time the sensor thread will be sleeping
	 *  (rate of generating measurements)
	 */

	/** The period. */
	private Long period;

	/** The sensor. */
	private SensorDTO sensor;

	/** The statistic type. */
	private MeasurementStatisticTypeEnumDTO statisticType;

	/** The statistic measurement. */
	private SensorMeasurementStatisticDTO statisticMeasurement;

	/** The statistics list. */
	private List<SensorMeasurementStatisticDTO> statisticsList;

	/**
	 * Instantiates a new sensor thread.
	 *
	 * @param period the period
	 * @param sensor the sensor
	 * @param statisticType the statistic type
	 * @param statisticsList the statistics list
	 */
	public SensorThread(Long period, SensorDTO sensor,
			MeasurementStatisticTypeEnumDTO statisticType, List<SensorMeasurementStatisticDTO> statisticsList) {
		super();
		this.period = period;
		this.sensor = sensor;
		this.statisticType = statisticType;
		this.statisticsList = statisticsList;
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run() {

		setStatisticMeasurement(this.getInitialMeasurementParameters());

		/*
		 * Set all fields of the measurement that are thread-specific and common for all measurements, e.g. measurement collection period -> 10:00:00 - 10:00:10
		 */
	}

	/**
	 * Gets the initial measurement parameters.
	 *
	 * @return the initial measurement parameters
	 */
	public SensorMeasurementStatisticDTO getInitialMeasurementParameters() {
		SensorMeasurementStatisticDTO statistic = new SensorMeasurementStatisticDTO();

		/*
		 * Set all fields of the measurement that are known in advance, e.g. sensor object etc.
		 */

		//		statistic.getStatistic().setTimestamp(new Date());
		//		statistic.getStatistic().setSensor(sensor);
		statistic.setStatisticType(statisticType);

		/*
		 * +++ ki o,ti allo xreiazetai
		 */
		return statistic;
	}

	/**
	 * Gets the period.
	 *
	 * @return the period
	 */
	public Long getPeriod() {
		return period;
	}

	/**
	 * Sets the period.
	 *
	 * @param period the new period
	 */
	public void setPeriod(Long period) {
		this.period = period;
	}

	/**
	 * Gets the sensor.
	 *
	 * @return the sensor
	 */
	public SensorDTO getSensor() {
		return sensor;
	}

	/**
	 * Sets the sensor.
	 *
	 * @param sensor the new sensor
	 */
	public void setSensor(SensorDTO sensor) {
		this.sensor = sensor;
	}

	/**
	 * Gets the statistic type.
	 *
	 * @return the statistic type
	 */
	public MeasurementStatisticTypeEnumDTO getStatisticType() {
		return statisticType;
	}

	/**
	 * Sets the statistic type.
	 *
	 * @param statisticType the new statistic type
	 */
	public void setStatisticType(MeasurementStatisticTypeEnumDTO statisticType) {
		this.statisticType = statisticType;
	}

	/**
	 * Gets the statistic measurement.
	 *
	 * @return the statistic measurement
	 */
	public SensorMeasurementStatisticDTO getStatisticMeasurement() {
		return statisticMeasurement;
	}

	/**
	 * Sets the statistic measurement.
	 *
	 * @param statistic the new statistic measurement
	 */
	public void setStatisticMeasurement(SensorMeasurementStatisticDTO statistic) {
		this.statisticMeasurement = statistic;
	}

	/**
	 * Gets the statistics list.
	 *
	 * @return the statistics list
	 */
	public synchronized List<SensorMeasurementStatisticDTO> getStatisticsList() {
		return statisticsList;
	}

	/**
	 * Sets the statistics list.
	 *
	 * @param statisticsList the new statistics list
	 */
	public synchronized void setStatisticsList(List<SensorMeasurementStatisticDTO> statisticsList) {
		this.statisticsList = statisticsList;
	}
	
}
