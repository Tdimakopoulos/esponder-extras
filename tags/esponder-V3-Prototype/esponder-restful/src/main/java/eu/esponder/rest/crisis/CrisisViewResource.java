/*
 * 
 */
package eu.esponder.rest.crisis;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.dto.model.snapshot.ReferencePOISnapshotDTO;
import eu.esponder.dto.model.snapshot.SketchPOISnapshotDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisViewResource.
 */
@Path("/crisis/view")
public class CrisisViewResource extends ESponderResource {

	//TODO Create OperationsCentres Web Services



	/**
	 * Read sketches.
	 *
	 * @param operationsCentreID the operations centre id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the list
	 */
	@GET
	@Path("/sketch/read")
	@Produces({MediaType.APPLICATION_JSON})
	public List<SketchPOIDTO> readSketches(
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		List<SketchPOIDTO> results = this.getOperationsCentreRemoteService().findSketchPOIRemote(operationsCentreID);
		if(results.isEmpty())
			return null;
		else
			return results;
	}

	/**
	 * Read sketch by id.
	 *
	 * @param sketchPOIId the sketch poi id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the sketch poidto
	 */
	@GET
	@Path("/sketch/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO readSketchById(
			@QueryParam("sketchPOIId") @NotNull(message="sketchPOIId may not be null") Long sketchPOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		SketchPOIDTO sketchPOIDTO = this.getOperationsCentreRemoteService().findSketchPOIByIdRemote(sketchPOIId);
		return sketchPOIDTO;
	}

	/**
	 * Read sketch by title.
	 *
	 * @param sketchPOITitle the sketch poi title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the sketch poidto
	 */
	@GET
	@Path("/sketch/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO readSketchByTitle(
			@QueryParam("sketchPOITitle") @NotNull(message="sketchPOITitle may not be null") String sketchPOITitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		SketchPOIDTO sketchPOIDTO = this.getOperationsCentreRemoteService().findSketchPOIByTitleRemote(sketchPOITitle);
		return sketchPOIDTO;
	}

	/**
	 * Creates the sketch returning.
	 *
	 * @param sketchDTO the sketch dto
	 * @param operationsCentreID the operations centre id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the sketch poidto
	 */
	@POST
	@Path("/sketch/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO createSketchReturning(
			@NotNull(message="sketch object may not be null") SketchPOIDTO sketchDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().createSketchPOIRemote(operationsCentreDTO, sketchDTO, userID);
		}
		return null;
	}


	/**
	 * Update sketch poi.
	 *
	 * @param sketchDTO the sketch dto
	 * @param operationsCentreID the operations centre id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the sketch poidto
	 */
	@PUT
	@Path("/sketch/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO updateSketchPOI(
			@NotNull(message="sketch object may not be null") SketchPOIDTO sketchDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().updateSketchPOIRemote(operationsCentreDTO, sketchDTO, userID);
		}
		return null;
	}


	/**
	 * Delete sketch poi.
	 *
	 * @param sketchPOIId the sketch poi id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/sketch/delete")
	public Long deleteSketchPOI(
			@QueryParam("sketchPOIId") @NotNull(message="SketchPOIId may not be null") Long sketchPOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getOperationsCentreRemoteService().deleteSketchPOIRemote(sketchPOIId, userID);
		return sketchPOIId;
	}


	/**
	 * Read reference po is.
	 *
	 * @param operationsCentreID the operations centre id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the list
	 */
	@GET
	@Path("/ref/read")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ReferencePOIDTO> readReferencePOIs(
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findReferencePOIsRemote(operationsCentreID);
	}


	/**
	 * Read reference by id.
	 *
	 * @param referencePOIId the reference poi id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reference poidto
	 */
	@GET
	@Path("/ref/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO readReferenceById(
			@QueryParam("referencePOIId") @NotNull(message="referencePOIId may not be null") Long referencePOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findReferencePOIByIdRemote(referencePOIId);
	}

	/**
	 * Read reference by title.
	 *
	 * @param referencePOITitle the reference poi title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reference poidto
	 */
	@GET
	@Path("/ref/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO readReferenceByTitle(
			@QueryParam("referencePOITitle") @NotNull(message="referencePOITitle may not be null") String referencePOITitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findReferencePOIByTitleRemote(referencePOITitle);
	}

	/**
	 * Creates the reference poi returning.
	 *
	 * @param referencePOIDTO the reference poidto
	 * @param operationsCentreID the operations centre id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reference poidto
	 */

	@POST
	@Path("/ref/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO createReferencePOIReturning(
			@NotNull(message="referencePOI object may not be null") ReferencePOIDTO referencePOIDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().createReferencePOIRemote(operationsCentreDTO, referencePOIDTO, userID);
		}
		return null;
	}


	/**
	 * Update reference poi.
	 *
	 * @param referencePOIDTO the reference poidto
	 * @param operationsCentreID the operations centre id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reference poidto
	 */
	@PUT
	@Path("/ref/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO updateReferencePOI(
			@NotNull(message="sketch object may not be null") ReferencePOIDTO referencePOIDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().updateReferencePOIRemote(operationsCentreDTO, referencePOIDTO, userID);
		}
		return null;
	}


	/**
	 * Deletereference poi.
	 *
	 * @param referencePOIId the reference poi id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/ref/delete")
	public Long deletereferencePOI(
			@QueryParam("referencePOIId") @NotNull(message="referencePOIId may not be null") Long referencePOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getOperationsCentreRemoteService().deleteReferencePOIRemote(referencePOIId, userID);
		return referencePOIId;
	}

	//	//Sketch Snapshots
	/**
	 * Creates the sketch snapshot poi.
	 *
	 * @param snapshotDTO the snapshot dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the sketch poi snapshot dto
	 */
	@POST
	@Path("/sketch/snapshot/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOISnapshotDTO createSketchSnapshotPOI(
			@NotNull(message="sketchPOISnapshot object may not be null") SketchPOISnapshotDTO snapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		SketchPOISnapshotDTO sketchPOISnapshotDTO = this.getOperationsCentreRemoteService().createSketchPOISnapshotRemote(snapshotDTO, userID);
		return sketchPOISnapshotDTO;
	}

	/**
	 * Read sketch snapshot po ibyid.
	 *
	 * @param snapshotsketchID the snapshotsketch id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the sketch poi snapshot dto
	 */
	@GET
	@Path("/sketch/snapshot/findbyid")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOISnapshotDTO readSketchSnapshotPOIbyid(
			@QueryParam("snapshotsketchID") @NotNull(message="snapshotsketchID may not be null") Long snapshotsketchID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findSketchPOISnapshotByIdRemote(snapshotsketchID);
	}

	/**
	 * Read sketch snapshot po iall.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the list
	 */
	@GET
	@Path("/sketch/snapshot/findall")
	@Produces({MediaType.APPLICATION_JSON})
	public List<SketchPOISnapshotDTO> readSketchSnapshotPOIall(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findAllSketchPOISnapshotsRemote();
	}

	//causing problems in deployment
	/**
	 * Read sketch snapshot po iby poi.
	 *
	 * @param sketchPOIID the sketch poiid
	 * @param resultLimit the result limit
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the list
	 */
	@GET
	@Path("/sketch/snapshot/findbysketchpoi")
	@Produces({MediaType.APPLICATION_JSON})
	public List<SketchPOISnapshotDTO> readSketchSnapshotPOIbyPOI(
			@QueryParam("sketchPOI") @NotNull(message="sketchPOI may not be null") Long sketchPOIID,
			@QueryParam("resultLimit") @NotNull(message="resultLimit may not be null") int resultLimit,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findAllSketchPOISnapshotsByPoiRemote(sketchPOIID, resultLimit);
	}

	/**
	 * Read sketch snapshot po ibydate.
	 *
	 * @param sketchID the sketch id
	 * @param dDate the d date
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the sketch poi snapshot dto
	 */
	@GET
	@Path("/sketch/snapshot/findbydate")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOISnapshotDTO readSketchSnapshotPOIbydate(
			@QueryParam("sketchID") @NotNull(message="sketchPOI may not be null") Long sketchID,
			@QueryParam("dateTo") @NotNull(message="resultLimit may not be null") Long dDate,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		Date xDate=new Date();
		xDate.setTime(dDate);
		return this.getOperationsCentreRemoteService().findSketchPOISnapshotByDateRemote(sketchID, xDate);
	}

	/**
	 * Read sketch snapshot po ibydate.
	 *
	 * @param sketchID the sketch id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the sketch poi snapshot dto
	 */
	@GET
	@Path("/sketch/snapshot/findprevious")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOISnapshotDTO readPreviousSketchSnapshotPOI(
			@QueryParam("sketchID") @NotNull(message="sketchPOI may not be null") Long sketchID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findPreviousSketchPOISnapshotRemote(sketchID);
	}


	/**
	 * Update sketch snapshot poi.
	 *
	 * @param sensorSnapshotDTO the sensor snapshot dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the sketch poi snapshot dto
	 */
	@PUT
	@Path("/sketch/snapshot/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOISnapshotDTO updateSketchSnapshotPOI(
			@NotNull(message="sketch object may not be null") SketchPOISnapshotDTO sensorSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().updateSketchPOISnapshotRemote(sensorSnapshotDTO, userID);

	}

	/**
	 * Deletesketchsnapshot poi.
	 *
	 * @param sketchSnapshotId the sketch snapshot id
	 * @param userID the user id
	 * @param sessionID the session id
	 */
	@DELETE
	@Path("/sketch/snapshot/delete")
	public void deletesketchsnapshotPOI(
			@QueryParam("sketchSnapshotId") @NotNull(message="sketchSnapshotId may not be null") Long sketchSnapshotId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getOperationsCentreRemoteService().deleteSketchPOISnapshotRemote(sketchSnapshotId, userID);

	}

	//	
	//	//Ref Snapshots
	/**
	 * Creates the reference poi snapshot.
	 *
	 * @param snapshotDTO the snapshot dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reference poi snapshot dto
	 */
	@POST
	@Path("/ref/snapshot/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOISnapshotDTO createReferencePOISnapshot(
			@NotNull(message="snapshotDTO object may not be null") ReferencePOISnapshotDTO snapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ReferencePOISnapshotDTO sketchPOISnapshotDTO = this.getOperationsCentreRemoteService().createReferencePOISnapshotRemote(snapshotDTO, userID);
		return sketchPOISnapshotDTO;
	}

	/**
	 * Read reference poi snapshotbyid.
	 *
	 * @param snapshotrefID the snapshotref id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reference poi snapshot dto
	 */
	@GET
	@Path("/ref/snapshot/findbyid")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOISnapshotDTO readReferencePOISnapshotbyid(
			@QueryParam("snapshotRefID") @NotNull(message="snapshotsketchID may not be null") Long snapshotrefID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findReferencePOISnapshotByIdRemote(snapshotrefID);
	}

	/**
	 * Read reference poi snapshot poi all.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the list
	 */
	@GET
	@Path("/ref/snapshot/findall")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ReferencePOISnapshotDTO> readReferencePOISnapshotPOIall(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOperationsCentreRemoteService().findAllReferencePOISnapshotsRemote();
	}

	/**
	 * Read reference poi snapshotby poi.
	 *
	 * @param referencePOIId the reference poi id
	 * @param resultLimit the result limit
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the list
	 * @throws ClassNotFoundException the class not found exception
	 */
	@GET
	@Path("/ref/snapshot/findbypoi")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ReferencePOISnapshotDTO> readReferencePOISnapshotbyPOI(
			@QueryParam("referencePOIId") @NotNull(message="sketchPOI may not be null") Long referencePOIId,
			@QueryParam("resultLimit") @NotNull(message="resultLimit may not be null") int resultLimit,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws ClassNotFoundException {

		return this.getOperationsCentreRemoteService().findAllReferencePOISnapshotsByReferencePOIRemote(referencePOIId, resultLimit);
	}

	/**
	 * Read reference poi snapshotbydate.
	 *
	 * @param sketchID the sketch id
	 * @param dDate the d date
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reference poi snapshot dto
	 */
	@GET
	@Path("/ref/snapshot/findbydate")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOISnapshotDTO readReferencePOISnapshotbydate(
			@QueryParam("referencePOIID") @NotNull(message="sketchPOI may not be null") Long sketchID,
			@QueryParam("dateTo") @NotNull(message="resultLimit may not be null") Long dDate,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		
		Date xDate=new Date();
		xDate.setTime(dDate);
		return this.getOperationsCentreRemoteService().findReferencePOISnapshotByDateRemote(sketchID, xDate);
	}

	/**
	 * Read reference poi snapshotbydate.
	 *
	 * @param sketchID the sketch id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reference poi snapshot dto
	 */
	@GET
	@Path("/ref/snapshot/findprevious")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOISnapshotDTO readReferencePOISnapshotbydate(
			@QueryParam("referencePOIID") @NotNull(message="sketchPOI may not be null") Long sketchID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findPreviousReferencePOISnapshotRemote(sketchID);
	}


	/**
	 * Update reference poi snapshot.
	 *
	 * @param sensorSnapshotDTO the sensor snapshot dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reference poi snapshot dto
	 */
	@PUT
	@Path("/ref/snapshot/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOISnapshotDTO updateReferencePOISnapshot(
			@NotNull(message="referencePOI object may not be null") ReferencePOISnapshotDTO sensorSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {


		return this.getOperationsCentreRemoteService().updateReferencePOISnapshotRemote(sensorSnapshotDTO, userID);

	}

	/**
	 * Delete reference poi snapshot.
	 *
	 * @param sketchSnapshotId the sketch snapshot id
	 * @param userID the user id
	 * @param sessionID the session id
	 */
	@DELETE
	@Path("/ref/snapshot/delete")
	public void deleteReferencePOISnapshot(
			@QueryParam("referenceSnapshotId") @NotNull(message="sketchSnapshotId may not be null") Long sketchSnapshotId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getOperationsCentreRemoteService().deleteReferencePOISnapshotRemote(sketchSnapshotId, userID);
	}

}
