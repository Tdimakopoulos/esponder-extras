/*
 * 
 */
package eu.esponder.rest.PointsAndCircles;

import java.util.Vector;

import org.codehaus.enunciate.XmlTransient;

// TODO: Auto-generated Javadoc
/**
 * The Class Mec.
 */
@XmlTransient
public class Mec {
    
    /** The points. */
    private MovablePoint[] points;
    
    /** The boundary. */
    private MovablePoint[] boundary;

    
    /** The radsave. */
    double cxsave, cysave, radsave;
    
    /* Constructor */
    /**
     * Instantiates a new mec.
     *
     * @param p the p
     */
    public Mec(Vector<MovablePoint> p) {

        setPoints(p);
    }
    
    /*
     * Convert new points to array
     */
    /**
     * Sets the points.
     *
     * @param p the new points
     */
    public void setPoints(Vector<MovablePoint> p)
    {
        points = null;
        points = new MovablePoint[p.size()];
        p.copyInto(points);
    }
    
    
    /*
     * Calculates the minimal enclosing circle. Prepares data and calls the recursive function.
     */
    /**
     * Calc mec.
     */
    public void calcMec()
    {
        if (points.length == 0) return;
        
        // random permutation of point set
        int pos;
        MovablePoint temp;
        for (int i = 0; i < points.length; i++)
        {
            pos = (int)(Math.random() * points.length);
            temp = points[i];
            points[i] = points[pos];
            points[pos] = temp;
        }
        
        boundary = new MovablePoint[3];
        cxsave = 0;
        cysave = 0;
        radsave = 0;
        mec(points, points.length, boundary, 0);
        System.out.println("CxSave: " + cxsave + "  CySave: " + cysave + "  radsave: " + radsave);

    }
    

    /* 
     * The main recursive function to calculate the minmal enclosing circle.
     * Call it initially with boundary array empty and b = 0.
     */
    /**
     * Mec.
     *
     * @param points the points
     * @param n the n
     * @param boundary the boundary
     * @param b the b
     * @return the circle
     */
    private Circle mec(MovablePoint[] points, int n, MovablePoint[] boundary, int b)
    { 
        System.out.println("In Mec... points = " + n + "  boundary = " + b);
        
        Circle localCircle = null;
        
        // terminal cases
        if (b == 3) 
            localCircle = calcCircle3(boundary[0], boundary[1], boundary[2]);
        else if (n == 1 && b == 0)
            localCircle = new Circle(points[0].getMidX(), points[0].getMidY(), 0);
        else if (n == 0 && b == 2)
            localCircle = calcCircle2(boundary[0], boundary[1]);
        else if (n == 1 && b == 1)
            localCircle = calcCircle2(boundary[0], points[0]);
        else 
        {
            localCircle = mec(points, n-1, boundary, b);
            if (!localCircle.isInCircle(points[n-1]))
            {
                System.out.println("Not in circle");
                boundary[b++] = points[n-1];
                localCircle = mec(points, n-1, boundary, b);
            }
        }
        if (localCircle != null)
        {
            cxsave = localCircle.x;
            cysave = localCircle.y;
            radsave = localCircle.radius;
        }
        return localCircle;
    }
    
    
   
    /*
     * Calculates the circle through the given 3 points. They must not lie on a common line!
     */
    /**
     * Calc circle3.
     *
     * @param p1 the p1
     * @param p2 the p2
     * @param p3 the p3
     * @return the circle
     */
    public Circle calcCircle3(MovablePoint p1, MovablePoint p2, MovablePoint p3)
    {
        double p1x = p1.getMidX();
        double p1y = p1.getMidY();
        double p2x = p2.getMidX();
        double p2y = p2.getMidY();
        double p3x = p3.getMidX();
        double p3y = p3.getMidY();
        
        double a = p2x - p1x;
        double b = p2y - p1y;     
        double c = p3x - p1x;
        double d = p3y - p1y;
        double e = a * (p2x + p1x) * 0.5 + b * (p2y + p1y) * 0.5;
        double f = c * (p3x + p1x) * 0.5 + d * (p3y + p1y) * 0.5;
        double det = a*d - b*c;    

        double cx = (d*e - b*f)/det;
        double cy = (-c*e + a*f)/det;
        
        Circle circle = new Circle( (int)cx, (int)cy, Math.sqrt( (p1x-cx)*(p1x-cx) + (p1y-cy)*(p1y-cy)) );
        return circle;
    }

    /*
     * Calculates a circle through the given two points.
     */
    /**
     * Calc circle2.
     *
     * @param p1 the p1
     * @param p2 the p2
     * @return the circle
     */
    public Circle calcCircle2(MovablePoint p1, MovablePoint p2)
    {
        double p1x = p1.getMidX();
        double p1y = p1.getMidY();
        double p2x = p2.getMidX();
        double p2y = p2.getMidY();
        
        double cx = 0.5*(p1x + p2x);
        double cy = 0.5*(p1y + p2y);
        Circle circle = new Circle( (int)cx, (int)cy, Math.sqrt( (p1x-cx)*(p1x-cx) + (p1y-cy)*(p1y-cy) ));
        return circle;
    }
}

