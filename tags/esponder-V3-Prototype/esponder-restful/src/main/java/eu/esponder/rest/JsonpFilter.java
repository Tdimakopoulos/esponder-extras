/*
 * 
 */
package eu.esponder.rest;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.ws.rs.core.MediaType;

import org.codehaus.enunciate.XmlTransient;

// TODO: Auto-generated Javadoc
/**
 * * Adds padding to json responses when the 'jsonp' parameter is specified.
 */
@XmlTransient
public class JsonpFilter implements Filter {

	/** The querystring parameter that indicates the response should be padded. */
	public static final String CALLBACK_PARAM = "jsonp";

	/** The regular expression to ensure that the callback is safe for display to a browser. */
	public static final Pattern SAFE_PATTERN = Pattern.compile("[a-zA-Z0-9\\.]+");

	/** The default padding to use if the specified padding contains invalid characters. */
	public static final String DEFAULT_CALLBACK = "handleMatterhornData";

	/**
	 * The content type for jsonp is "application/x-javascript", not
	 * "application/json".
	 */
	public static final String JS_CONTENT_TYPE = "application/x-javascript";

	/** The character encoding. */
	public static final String CHARACTER_ENCODING = "UTF-8";

	/** The post padding, which is always ');' no matter what the pre-padding looks like. */
	public static final String POST_PADDING = ");";

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * Instantiates a new jsonp filter.
	 */
	public JsonpFilter()
	{
		
	}
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

		// Cast the request and response to HTTP versions
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse originalResponse = (HttpServletResponse) resp;

		// Determine whether the response must be wrapped
		String callbackValue = request.getParameter(CALLBACK_PARAM);
		if (callbackValue == null || callbackValue.isEmpty()) {
			chain.doFilter(request, originalResponse);
		} else {
			// Ensure the callback value contains only safe characters
			if (!SAFE_PATTERN.matcher(callbackValue).matches()) {
				callbackValue = DEFAULT_CALLBACK;
			}

			// Write the padded response
			String preWrapper = callbackValue + "(";
			HttpServletResponseContentWrapper wrapper = new HttpServletResponseContentWrapper(originalResponse, preWrapper);
			chain.doFilter(request, wrapper);
			wrapper.flushWrapper();
		}
	}

	/**
	 * A response wrapper that allows for json padding.
	 */
	@XmlTransient
	static class HttpServletResponseContentWrapper extends HttpServletResponseWrapper {

		/** The buffer. */
		
		protected ByteArrayServletOutputStream buffer;
		
		/** The buffer writer. */
		
		protected PrintWriter bufferWriter;
		
		/** The committed. */
		protected boolean committed = false;
		
		/** The enable wrapping. */
		protected boolean enableWrapping = true;
		
		/** The pre wrapper. */
		protected String preWrapper;

		/**
		 * Instantiates a new http servlet response content wrapper.
		 *
		 * @param response the response
		 * @param preWrapper the pre wrapper
		 */
		public HttpServletResponseContentWrapper(HttpServletResponse response, String preWrapper) {
			super(response);
			this.preWrapper = preWrapper;
			this.buffer = new ByteArrayServletOutputStream();
		}

		/**
		 * Flush wrapper.
		 *
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		public void flushWrapper() throws IOException {
			if (enableWrapping) {
				if (bufferWriter != null) {
					bufferWriter.close();
				}
				if (buffer != null) {
					buffer.close();
				}
				byte[] content = wrap(buffer.toByteArray());
				getResponse().setContentType(JS_CONTENT_TYPE);
				getResponse().setContentLength(content.length);
				getResponse().setCharacterEncoding(CHARACTER_ENCODING);
				getResponse().getOutputStream().write(content);
				getResponse().flushBuffer();
				committed = true;
			}
		}

		/**
		 * Wrap.
		 *
		 * @param content the content
		 * @return the byte[]
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		public byte[] wrap(byte[] content) throws IOException {
			StringBuilder sb = new StringBuilder(preWrapper);
			sb.append(new String(content, CHARACTER_ENCODING));
			sb.append(POST_PADDING);
			return sb.toString().getBytes(CHARACTER_ENCODING);
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletResponseWrapper#getContentType()
		 */
		@Override
		public String getContentType() {
			return enableWrapping ? JS_CONTENT_TYPE : getResponse().getContentType();
		}

		/**
		 * If the content type is set to JSON, we enable wrapping. Otherwise, we
		 * leave it disabled.
		 * 
		 * {@inheritDoc}
		 * 
		 * @see javax.servlet.ServletResponseWrapper#setContentType(java.lang.String)
		 */
		@Override
		public void setContentType(String type) {
			enableWrapping = MediaType.APPLICATION_JSON.equals(type);
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletResponseWrapper#getOutputStream()
		 */
		@Override
		public ServletOutputStream getOutputStream() throws IOException {
			return enableWrapping ? buffer : getResponse().getOutputStream();
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletResponseWrapper#getWriter()
		 */
		@Override
		public PrintWriter getWriter() throws IOException {
			if (enableWrapping) {
				if (bufferWriter == null) {
					bufferWriter = new PrintWriter(new OutputStreamWriter(buffer, this.getCharacterEncoding()));
				}
				return bufferWriter;
			} else {
				return getResponse().getWriter();
			}
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletResponseWrapper#setBufferSize(int)
		 */
		@Override
		public void setBufferSize(int size) {
			if (enableWrapping) {
				buffer.enlarge(size);
			} else {
				getResponse().setBufferSize(size);
			}
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletResponseWrapper#getBufferSize()
		 */
		@Override
		public int getBufferSize() {
			return enableWrapping ? buffer.size() : getResponse()
					.getBufferSize();
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletResponseWrapper#flushBuffer()
		 */
		@Override
		public void flushBuffer() throws IOException {
			if (!enableWrapping)
				getResponse().flushBuffer();
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletResponseWrapper#isCommitted()
		 */
		@Override
		public boolean isCommitted() {
			return enableWrapping ? committed : getResponse().isCommitted();
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletResponseWrapper#reset()
		 */
		@Override
		public void reset() {
			getResponse().reset();
			buffer.reset();
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletResponseWrapper#resetBuffer()
		 */
		@Override
		public void resetBuffer() {
			getResponse().resetBuffer();
			buffer.reset();
		}
	}

	/**
	 * The Class ByteArrayServletOutputStream.
	 */
	@XmlTransient
	static class ByteArrayServletOutputStream extends ServletOutputStream {

		/** The buf. */
		protected byte buf[];

		/** The count. */
		protected int count;

		/**
		 * Instantiates a new byte array servlet output stream.
		 */
		public ByteArrayServletOutputStream() {
			this(32);
		}

		/**
		 * Instantiates a new byte array servlet output stream.
		 *
		 * @param size the size
		 */
		public ByteArrayServletOutputStream(int size) {
			if (size < 0) {
				throw new IllegalArgumentException("Negative initial size: " + size);
			}
			buf = new byte[size];
		}

		/**
		 * To byte array.
		 *
		 * @return the byte[]
		 */
		public synchronized byte toByteArray()[] {
			return Arrays.copyOf(buf, count);
		}

		/**
		 * Reset.
		 */
		public synchronized void reset() {
			count = 0;
		}

		/**
		 * Size.
		 *
		 * @return the int
		 */
		public synchronized int size() {
			return count;
		}

		/**
		 * Enlarge.
		 *
		 * @param size the size
		 */
		public void enlarge(int size) {
	      if (size > buf.length) {
	        buf = Arrays.copyOf(buf, Math.max(buf.length << 1, size));
	      }
	    }

		/* (non-Javadoc)
		 * @see java.io.OutputStream#write(int)
		 */
		@Override
		public synchronized void write(int b) throws IOException {
			int newcount = count + 1;
			enlarge(newcount);
			buf[count] = (byte) b;
			count = newcount;
		}
	}
}
