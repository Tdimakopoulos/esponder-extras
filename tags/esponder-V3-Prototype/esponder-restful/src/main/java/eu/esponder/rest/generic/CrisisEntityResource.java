/*
 * 
 */
package eu.esponder.rest.generic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionExpressionEnumDTO;
import eu.esponder.dto.model.criteria.EsponderQueryRestrictionDTO;
import eu.esponder.dto.model.criteria.EsponderUnionCriteriaCollectionDTO;
import eu.esponder.dto.model.security.KeyStorageDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisEntityResource.
 */
@Path("/crisis/generic")
public class CrisisEntityResource extends ESponderResource {


	
	/**
	 * Gets the entity.
	 *
	 * @param idListStr the id list str
	 * @param queriedEntityDTO the queried entity dto
	 * @param pageNumber the page number
	 * @param pageSize the page size
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the entity
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws Exception the exception
	 */
	@GET
	@Path("/get")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getEntity (
			@QueryParam("idList") @NotNull(message="criteriaDTO may not be null") String idListStr,
			@QueryParam("queriedEntityDTO") @NotNull(message="queriedEntity may not be null") String queriedEntityDTO,
			@QueryParam("pageNumber") @NotNull(message="Page Number may not be null") int pageNumber,
			@QueryParam("pageSize") @NotNull(message="Page Size may not be null") int pageSize,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws InstantiationException, IllegalAccessException, Exception {
		
		ArrayList<Long> idList = StringToIDList(idListStr);	
		EsponderQueryRestrictionDTO restrictions = generateRestrictionsFromIdList(idList);
		if(restrictions!=null)
			return postEntity(restrictions, queriedEntityDTO, pageSize, pageNumber, userID, sessionID);
		else
			return null;
	}
	
	/**
	 * Gets the entity by title.
	 *
	 * @param titleList the title list
	 * @param queriedEntityDTO the queried entity dto
	 * @param pageNumber the page number
	 * @param pageSize the page size
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the entity by title
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws Exception the exception
	 */
	@GET
	@Path("/getByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getEntityByTitle (
			@QueryParam("titleList") @NotNull(message="criteriaDTO may not be null") String titleList,
			@QueryParam("queriedEntityDTO") @NotNull(message="queriedEntity may not be null") String queriedEntityDTO,
			@QueryParam("pageNumber") @NotNull(message="Page Number may not be null") int pageNumber,
			@QueryParam("pageSize") @NotNull(message="Page Size may not be null") int pageSize,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws InstantiationException, IllegalAccessException, Exception {
		
		ArrayList<String> idList = StringToStringList(titleList);	
		EsponderQueryRestrictionDTO restrictions = generateRestrictionsFromTitleList(idList);
		if(restrictions!=null)
			return postEntity(restrictions, queriedEntityDTO, pageSize, pageNumber, userID, sessionID);
		else
			return null;
	}

	
	/**
	 * Post entity.
	 *
	 * @param criteriaDTO the criteria dto
	 * @param queriedEntityDTO the queried entity dto
	 * @param pageNumber the page number
	 * @param pageSize the page size
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the result list dto
	 * @throws InstantiationException the instantiation exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws Exception the exception
	 */
	@POST
	@Path("/post")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO postEntity(
			EsponderQueryRestrictionDTO criteriaDTO,
			@QueryParam("queriedEntityDTO") @NotNull(message="queriedEntity may not be null") String queriedEntityDTO,  
			@QueryParam("pageNumber") @NotNull(message="Page Number may not be null") int pageNumber,
			@QueryParam("pageSize") @NotNull(message="Page Size may not be null") int pageSize,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws InstantiationException, IllegalAccessException, Exception {
		
		ResultListDTO resultsList = new ResultListDTO(this.getGenericRemoteService().getDTOEntities(queriedEntityDTO, criteriaDTO, pageSize, pageNumber));
		return resultsList;
	}
	
	
	/**
	 * Creates the entity.
	 *
	 * @param entityDTO the entity dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the e sponder entity dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@POST
	@Path("/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderEntityDTO createEntity(
			@NotNull(message="entity to be created may not be null") ESponderEntityDTO entityDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws ClassNotFoundException {
		
		ESponderEntityDTO createdEntity = this.getGenericRemoteService().createEntityRemote(entityDTO, userID);
		return createdEntity;
	}
	
	
	/**
	 * Update entity.
	 *
	 * @param entityDTO the entity dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the e sponder entity dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@PUT
	@Path("/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderEntityDTO updateEntity( 
			@NotNull(message="entity to be updated may not be null") ESponderEntityDTO entityDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws ClassNotFoundException {
		
		ESponderEntityDTO updatedEntity = this.getGenericRemoteService().updateEntityRemote(entityDTO, userID);
		return updatedEntity;
	}
	

	/**
	 * Delete entity.
	 *
	 * @param entityClass the entity class
	 * @param entityID the entity id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 * @throws ClassNotFoundException the class not found exception
	 */
	@SuppressWarnings("unchecked")
	@DELETE
	@Path("/delete")
	public Long deleteEntity(
			@QueryParam("entityClass") @NotNull(message="entityClass may not be null") String entityClass,
			@QueryParam("entityID") @NotNull(message="entityID may not be null") Long entityID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws ClassNotFoundException {
		
		Class<? extends ESponderEntityDTO> targetDTOClass = (Class<? extends ESponderEntityDTO>) this.getMappingService().getEntityClass(entityClass);
		if(targetDTOClass != null) {
			this.getGenericRemoteService().deleteEntityRemote(targetDTOClass, entityID);
			return entityID;
		}
		else {
			//FIXME Integrate custom ESponderException when implemented
			
		}
		return null;
	}
	

	/**
	 * Generate restrictions from id list.
	 *
	 * @param idList the id list
	 * @return the esponder query restriction dto
	 */
	private EsponderQueryRestrictionDTO generateRestrictionsFromIdList (List<Long> idList) {

		
		
		EsponderUnionCriteriaCollectionDTO criteriaDTO = new EsponderUnionCriteriaCollectionDTO(new HashSet<EsponderQueryRestrictionDTO>());
		if(!idList.isEmpty()) {
			for(Long id : idList) {
				criteriaDTO.add( generateCriterion( id ) );
			}
			return criteriaDTO;
		}
		else {
			/*
			 * TODO: integrate the User Exceptions Hierarchy that will be defined later
			 * i.e., list of ids is empty
			 */
			System.out.println("idList is Null, cannot continue...");
		}
		return null;
	}
	
	/**
	 * Generate restrictions from title list.
	 *
	 * @param titleList the title list
	 * @return the esponder query restriction dto
	 */
	private EsponderQueryRestrictionDTO generateRestrictionsFromTitleList (List<String> titleList) {

		EsponderUnionCriteriaCollectionDTO criteriaDTO = new EsponderUnionCriteriaCollectionDTO(new HashSet<EsponderQueryRestrictionDTO>());
		if(!titleList.isEmpty()) {
			for(String title : titleList) {
				criteriaDTO.add( generateCriterionFromTitle(title) );
			}
			return criteriaDTO;
		}
		else {
			/*
			 * TODO: integrate the User Exceptions Hierarchy that will be defined later
			 * i.e., list of ids is empty
			 */
			System.out.println("idList is Null, cannot continue...");
		}
		return null;
	}


	/**
	 * Generate criterion.
	 *
	 * @param id the id
	 * @return the esponder criterion dto
	 */
	private EsponderCriterionDTO generateCriterion (Long id) {
		EsponderCriterionDTO criterion = new EsponderCriterionDTO();
		criterion.setField("id");
		criterion.setExpression(EsponderCriterionExpressionEnumDTO.EQUAL);
		criterion.setValue(id.toString());
		return criterion;
	}
	
	/**
	 * Generate criterion from title.
	 *
	 * @param title the title
	 * @return the esponder criterion dto
	 */
	private EsponderCriterionDTO generateCriterionFromTitle (String title) {
		EsponderCriterionDTO criterion = new EsponderCriterionDTO();
		criterion.setField("title");
		criterion.setExpression(EsponderCriterionExpressionEnumDTO.EQUAL);
		criterion.setValue(title);
		return criterion;
	}

	/**
	 * String to id list.
	 *
	 * @param idListStr the id list str
	 * @return the array list
	 */
	private ArrayList<Long> StringToIDList(String idListStr) {
		idListStr = idListStr.replace("[", "").replace("]", "");
		String delims = ", ";
		String[] tokens = idListStr.split(delims);
		ArrayList<Long> idList = new ArrayList<Long>();
		for(String i : tokens) {
			idList.add(new Long(i));
		}
		return idList;
	}
	
	/**
	 * String to string list.
	 *
	 * @param titleListStr the title list str
	 * @return the array list
	 */
	private ArrayList<String> StringToStringList(String titleListStr) {
		titleListStr = titleListStr.replace("[", "").replace("]", "");
		String delims = ", ";
		String[] tokens = titleListStr.split(delims);
		ArrayList<String> titleList = new ArrayList<String>();
		for(String i : tokens) {
			titleList.add(i);
		}
		return titleList;
	}

}