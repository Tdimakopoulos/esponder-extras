/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class OCMeocALTDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "resourceId", "title", "supervisingOC",
		"incidentCommander", "subordinateTeams", "meocCrisisContext",
		"snapshots" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class OCMeocALTDTO extends OperationsCentreALTDTO {

	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6001309075526460044L;

	/** The supervising oc. */
	private Long supervisingOC;

	/** The incident commander. */
	private ActorICALTDTO incidentCommander;

	/** The meoc crisis context. */
	private Long meocCrisisContext;

	/** The snapshots. */
	private Set<OperationsCentreSnapshotDTO> snapshots;

	/**
	 * Gets the meoc crisis context.
	 *
	 * @return the meoc crisis context
	 */
	public Long getMeocCrisisContext() {
		return meocCrisisContext;
	}

	/**
	 * Gets the supervising oc.
	 *
	 * @return the supervising oc
	 */
	public Long getSupervisingOC() {
		return supervisingOC;
	}

	/**
	 * Sets the supervising oc.
	 *
	 * @param supervisingOC the new supervising oc
	 */
	public void setSupervisingOC(Long supervisingOC) {
		this.supervisingOC = supervisingOC;
	}

	/**
	 * Gets the incident commander.
	 *
	 * @return the incident commander
	 */
	public ActorICALTDTO getIncidentCommander() {
		return incidentCommander;
	}

	/**
	 * Sets the incident commander.
	 *
	 * @param incidentCommander the new incident commander
	 */
	public void setIncidentCommander(ActorICALTDTO incidentCommander) {
		this.incidentCommander = incidentCommander;
	}

	/**
	 * Sets the meoc crisis context.
	 *
	 * @param meocCrisisContext the new meoc crisis context
	 */
	public void setMeocCrisisContext(Long meocCrisisContext) {
		this.meocCrisisContext = meocCrisisContext;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.alternative.OperationsCentreALTDTO#getSnapshots()
	 */
	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<OperationsCentreSnapshotDTO> getSnapshots() {
		return snapshots;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.alternative.OperationsCentreALTDTO#setSnapshots(java.util.Set)
	 */
	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<OperationsCentreSnapshotDTO> snapshots) {
		this.snapshots = snapshots;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.alternative.OperationsCentreALTDTO#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getName()+" [id=" + id + ", title=" + title + "]";
	}

}
