/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.sensor;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class SensorDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class SensorDTO extends ResourceDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7077059831872243408L;

	/**
	 * Instantiates a new sensor dto.
	 */
	public SensorDTO() { }
	
	/** The name. */
	private String name;
	
	/** The measurement unit. */
	private MeasurementUnitEnumDTO measurementUnit;
	
	/** The configuration. */
	private Set<StatisticsConfigDTO> configuration;
	
	/** The label. */
	private String label;
	
	/** The equipment id. */
	private Long equipmentId;

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Sets the label.
	 *
	 * @param label the new label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the measurement unit.
	 *
	 * @return the measurement unit
	 */
	public MeasurementUnitEnumDTO getMeasurementUnit() {
		return measurementUnit;
	}

	/**
	 * Sets the measurement unit.
	 *
	 * @param measurementUnit the new measurement unit
	 */
	public void setMeasurementUnit(MeasurementUnitEnumDTO measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(null == obj || obj instanceof SensorDTO)) return false;
		
		SensorDTO sensor = (SensorDTO) obj;
		if (!(sensor.getClass() == SensorDTO.class)) return false;
		return true;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.ResourceDTO#toString()
	 */
	@Override
	public String toString() {
		return "SensorDTO [name=" + name + ", type=" + type + ", measurementUnit="
				+ measurementUnit + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}
	
	/**
	 * Gets the configuration.
	 *
	 * @return the configuration
	 */
	public Set<StatisticsConfigDTO> getConfiguration() {
		return configuration;
	}

	/**
	 * Sets the configuration.
	 *
	 * @param configuration the new configuration
	 */
	public void setConfiguration(Set<StatisticsConfigDTO> configuration) {
		this.configuration = configuration;
	}

	/**
	 * Gets the equipment id.
	 *
	 * @return the equipment id
	 */
	public Long getEquipmentId() {
		return equipmentId;
	}

	/**
	 * Sets the equipment id.
	 *
	 * @param equipmentId the new equipment id
	 */
	public void setEquipmentId(Long equipmentId) {
		this.equipmentId = equipmentId;
	}
	
}
