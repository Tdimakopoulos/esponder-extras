/*
 * 
 */
package eu.esponder.dto.model.crisis.action;



// TODO: Auto-generated Javadoc
// 
/**
 * The Enum ActionOperationEnumDTO.
 */
public enum ActionOperationEnumDTO {

	/** The move. */
	MOVE,
	
	/** The transport. */
	TRANSPORT,
	
	/** The fix. */
	FIX
}
