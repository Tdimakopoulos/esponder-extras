/*
 * 
 */
package eu.esponder.dto.model.snapshot.sensor.measurement.statistic;



// TODO: Auto-generated Javadoc
/**
 * The Enum MeasurementUnitEnumDTO.
 */
public enum MeasurementUnitEnumDTO {
	
	//Temperature
	/** The degrees celcius. */
	DEGREES_CELCIUS,
	
	/** The degrees fahrenheit. */
	DEGREES_FAHRENHEIT,
	
	//Time
	/** The seconds. */
	SECONDS,
	
	/** The milliseconds. */
	MILLISECONDS,
	
	//Gas Concentration
	/** The ppm. */
	PPM,
	
	//Position (for each of lon, lat, alt)
	/** The degrees. */
	DEGREES,
	
	//Hearbeat Rate
	/** The bbm. */
	BBM

}
