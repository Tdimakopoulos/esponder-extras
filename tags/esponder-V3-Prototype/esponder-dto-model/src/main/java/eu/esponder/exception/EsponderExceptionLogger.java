/*
 * 
 */
package eu.esponder.exception;

import java.io.File;
import java.util.Date;

import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.dto.osgi.service.event.ESponderEventPublisher;
import eu.esponder.event.exception.GeneratedExceptionEvent;
import eu.esponder.event.model.ESponderEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class EsponderExceptionLogger.
 */
public class EsponderExceptionLogger {

	/** The log. */
	public static Logger log; 

	/** The sz properties file name unix. */
	private static String szPropertiesFileNameUnix = "/home/exodus/log/exponderlog.log";

	/** The sz properties file name windows. */
	private static String szPropertiesFileNameWindows = "C:/esponderlog.log";
	
	/**
	 * Hourly rotation.
	 */
	public static void HourlyRotation()
	{
		DailyRollingFileAppender appender = new DailyRollingFileAppender();
		appender.setDatePattern("'.'yyyy-MM-dd-HH");
		appender.setName("FileLogger");
		appender.setFile(GetPath());
		appender.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
		appender.setThreshold(Level.DEBUG);
		appender.setAppend(true);
		appender.activateOptions();
		Logger.getRootLogger().addAppender(appender);
	}
	
	/**
	 * Midnight rotation.
	 */
	public static void MidnightRotation()
	{
		DailyRollingFileAppender appender = new DailyRollingFileAppender();
		appender.setDatePattern("'.'yyyy-MM-dd");
		appender.setName("FileLogger");
		appender.setFile(GetPath());
		appender.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
		appender.setThreshold(Level.DEBUG);
		appender.setAppend(true);
		appender.activateOptions();
		Logger.getRootLogger().addAppender(appender);
	}
	
	/**
	 * Week rotation.
	 */
	public static void WeekRotation()
	{
		DailyRollingFileAppender appender = new DailyRollingFileAppender();
		appender.setDatePattern("'.'yyyy-ww");
		appender.setName("FileLogger");
		appender.setFile(GetPath());
		appender.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
		appender.setThreshold(Level.DEBUG);
		appender.setAppend(true);
		appender.activateOptions();
		
		Logger.getRootLogger().addAppender(appender);
	}
	
	/**
	 * Size rotation.
	 */
	public static void SizeRotation()
	{
		RollingFileAppender  fa = new  RollingFileAppender ();
		fa.setName("FileLogger");
		fa.setFile(GetPath());
		fa.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
		fa.setThreshold(Level.DEBUG);
		fa.setAppend(true);
		fa.activateOptions();
		fa.setMaxFileSize("10MB");
		Logger.getRootLogger().addAppender(fa);
	}
	
	/**
	 * Setup.
	 */
	public static void Setup() {
		Logger.getRootLogger().removeAllAppenders();
		SizeRotation();
	}

	/**
	 * Gets the single instance of EsponderExceptionLogger.
	 *
	 * @param clz the clz
	 * @return single instance of EsponderExceptionLogger
	 */
	public static Logger getInstance(Class<?> clz) {
		Setup();
		return Logger.getLogger(clz);
	}
	
	/**
	 * Log info.
	 *
	 * @param clz the clz
	 * @param szMessage the sz message
	 */
	public static void LogInfo(Class<?> clz,String szMessage) {
		Logger log = Logger.getLogger(clz);
		Setup();
		log.info(szMessage);
		
		ESponderEventPublisher<GeneratedExceptionEvent> publisher = new ESponderEventPublisher<GeneratedExceptionEvent>(
				GeneratedExceptionEvent.class);

		try {
			// publisher.setEventTopic("createaction");
			publisher.publishEvent(GeneratedExceptionEvent("Name : "+clz.getName()+" Canonical Name :"+clz.getCanonicalName()+" Message : "+szMessage,SeverityLevelDTO.MINIMAL));
		} catch (EventListenerException e) {

			e.printStackTrace();
		}
	}

		
	/**
	 * Log warn.
	 *
	 * @param clz the clz
	 * @param szMessage the sz message
	 */
	public static void LogWarn(Class<?> clz,String szMessage) {
		Logger log = Logger.getLogger(clz);
		Setup();
		log.warn(szMessage);
		
		ESponderEventPublisher<GeneratedExceptionEvent> publisher = new ESponderEventPublisher<GeneratedExceptionEvent>(
				GeneratedExceptionEvent.class);

		try {
			// publisher.setEventTopic("createaction");
			publisher.publishEvent(GeneratedExceptionEvent("Name : "+clz.getName()+" Canonical Name :"+clz.getCanonicalName()+" Message : "+szMessage,SeverityLevelDTO.SERIOUS));
		} catch (EventListenerException e) {

			e.printStackTrace();
		}
	}

	/**
	 * Log error.
	 *
	 * @param clz the clz
	 * @param szMessage the sz message
	 */
	public static void LogError(Class<?> clz,String szMessage) {
		Logger log = Logger.getLogger(clz);
		Setup();
		log.error(szMessage);
		
		ESponderEventPublisher<GeneratedExceptionEvent> publisher = new ESponderEventPublisher<GeneratedExceptionEvent>(
				GeneratedExceptionEvent.class);

		try {
			// publisher.setEventTopic("createaction");
			publisher.publishEvent(GeneratedExceptionEvent("Name : "+clz.getName()+" Canonical Name :"+clz.getCanonicalName()+" Message : "+szMessage,SeverityLevelDTO.SEVERE));
		} catch (EventListenerException e) {

			e.printStackTrace();
		}
	}

	/**
	 * Log fatal.
	 *
	 * @param clz the clz
	 * @param szMessage the sz message
	 */
	public static void LogFatal(Class<?> clz,String szMessage) {
		Logger log = Logger.getLogger(clz);
		Setup();
		log.fatal(szMessage);
		
		ESponderEventPublisher<GeneratedExceptionEvent> publisher = new ESponderEventPublisher<GeneratedExceptionEvent>(
				GeneratedExceptionEvent.class);

		try {
			// publisher.setEventTopic("createaction");
			publisher.publishEvent(GeneratedExceptionEvent("Name : "+clz.getName()+" Canonical Name :"+clz.getCanonicalName()+" Message : "+szMessage,SeverityLevelDTO.FATAL));
		} catch (EventListenerException e) {

			e.printStackTrace();
		}
	}
	
	/**
	 * Generated exception event.
	 *
	 * @param ExceptionMessage the exception message
	 * @param Severity the severity
	 * @return the e sponder event<? extends e sponder entity dt o>
	 */
	private static ESponderEvent<? extends ESponderEntityDTO> GeneratedExceptionEvent(String ExceptionMessage,SeverityLevelDTO Severity) {
		ESponderUserDTO actionSnapshot = new ESponderUserDTO();
		actionSnapshot.setId(new Long(0));

		GeneratedExceptionEvent result = new GeneratedExceptionEvent();
		result.setEventAttachment(actionSnapshot);
		result.setJournalMessage(ExceptionMessage);
		result.setEventSeverity(Severity);
		result.setEventTimestamp(new Date());
		result.setEventSource(actionSnapshot);
		return result;
	}

	/**
	 * Gets the path.
	 *
	 * @return the string
	 */
	private static String GetPath() {
		if (File.separatorChar == '/')
			return szPropertiesFileNameUnix;
		else
			return szPropertiesFileNameWindows;
	}

}
