/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class OCEocALTDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "resourceId", "title",  "crisisManager",
		"subordinateMEOCs",
		"eocCrisisContext" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class OCEocALTDTO extends OperationsCentreALTDTO {

	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6738011571879473184L;

	/** The crisis manager. */
	private ActorCMALTDTO crisisManager;

	/** The subordinate meo cs. */
	private Set<OCMeocALTDTO> subordinateMEOCs;

	/** The eoc crisis context. */
	private Long eocCrisisContext;

	/**
	 * Gets the crisis manager.
	 *
	 * @return the crisis manager
	 */
	public ActorCMALTDTO getCrisisManager() {
		return crisisManager;
	}

	/**
	 * Sets the crisis manager.
	 *
	 * @param crisisManager the new crisis manager
	 */
	public void setCrisisManager(ActorCMALTDTO crisisManager) {
		this.crisisManager = crisisManager;
	}

	/**
	 * Gets the subordinate meo cs.
	 *
	 * @return the subordinate meo cs
	 */
	public Set<OCMeocALTDTO> getSubordinateMEOCs() {
		return subordinateMEOCs;
	}

	/**
	 * Sets the subordinate meo cs.
	 *
	 * @param subordinateMEOCs the new subordinate meo cs
	 */
	public void setSubordinateMEOCs(Set<OCMeocALTDTO> subordinateMEOCs) {
		this.subordinateMEOCs = subordinateMEOCs;
	}

	/**
	 * Gets the eoc crisis context.
	 *
	 * @return the eoc crisis context
	 */
	public Long getEocCrisisContext() {
		return eocCrisisContext;
	}

	/**
	 * Sets the eoc crisis context.
	 *
	 * @param eocCrisisContext the new eoc crisis context
	 */
	public void setEocCrisisContext(Long eocCrisisContext) {
		this.eocCrisisContext = eocCrisisContext;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.alternative.OperationsCentreALTDTO#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getName()+" [id=" + id + ", title=" + title + "]";
	}

}
