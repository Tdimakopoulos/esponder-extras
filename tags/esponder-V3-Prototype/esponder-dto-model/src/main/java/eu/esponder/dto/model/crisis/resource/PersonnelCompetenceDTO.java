/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import eu.esponder.dto.model.ESponderEntityDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class PersonnelCompetenceDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class PersonnelCompetenceDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 886664390428578001L;

	/** The short title. */
	private String shortTitle;


	/**
	 * Gets the short title.
	 *
	 * @return the short title
	 */
	public String getShortTitle() {
		return shortTitle;
	}

	/**
	 * Sets the short title.
	 *
	 * @param shortTitle the new short title
	 */
	public void setShortTitle(String shortTitle) {
		this.shortTitle = shortTitle;
	}




}
