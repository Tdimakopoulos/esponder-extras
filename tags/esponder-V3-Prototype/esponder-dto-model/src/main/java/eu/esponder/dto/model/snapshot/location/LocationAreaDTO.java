/*
 * 
 */
package eu.esponder.dto.model.snapshot.location;
 

import org.codehaus.jackson.annotate.JsonTypeInfo;
import eu.esponder.dto.model.ESponderEntityDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class LocationAreaDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class LocationAreaDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6264903057849400607L;

	/**
	 * Instantiates a new location area dto.
	 */
	public LocationAreaDTO() {}
	
	/** The title. */
	private String title;

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

}
