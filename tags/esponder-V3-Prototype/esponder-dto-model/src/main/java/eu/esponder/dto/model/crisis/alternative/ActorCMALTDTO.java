/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorCMALTDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "resourceId", "title", "personnel",
		"voIPURL", "subordinates", "operationsCentre" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class ActorCMALTDTO extends ActorALTDTO {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2851935917455150233L;

	/** The subordinates. */
	private Set<ActorICALTDTO> subordinates;

	/** The operations centre. */
	private Long operationsCentre;
//	private OCEocALTDTO operationsCentre;

	/**
	 * Gets the subordinates.
	 *
	 * @return the subordinates
	 */
	public Set<ActorICALTDTO> getSubordinates() {
		return subordinates;
	}

	/**
	 * Sets the subordinates.
	 *
	 * @param subordinates the new subordinates
	 */
	public void setSubordinates(Set<ActorICALTDTO> subordinates) {
		this.subordinates = subordinates;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public Long getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(Long operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.alternative.ActorALTDTO#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getName()+" [id=" + id + ", title=" + title + "]";
	}
}
