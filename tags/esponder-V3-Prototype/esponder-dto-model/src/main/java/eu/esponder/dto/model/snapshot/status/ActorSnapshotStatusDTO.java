/*
 * 
 */
package eu.esponder.dto.model.snapshot.status;


// TODO: Auto-generated Javadoc
/**
 * The Enum ActorSnapshotStatusDTO.
 */
public enum ActorSnapshotStatusDTO {
	
	/** The ready. */
	READY,
	
	/** The active. */
	ACTIVE,
	
	/** The inactive. */
	INACTIVE
}
