/*
 * 
 */
package eu.esponder.dto.model.criteria;

import java.util.Collection;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;



// TODO: Auto-generated Javadoc
/**
 * The Class EsponderIntersectionCriteriaCollectionDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"restrictions"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EsponderIntersectionCriteriaCollectionDTO extends EsponderCriteriaCollectionDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4395158116445919854L;
	
	/**
	 * Instantiates a new esponder intersection criteria collection dto.
	 */
	public EsponderIntersectionCriteriaCollectionDTO() { }
		
	/**
	 * Instantiates a new esponder intersection criteria collection dto.
	 *
	 * @param restrictions the restrictions
	 */
	public EsponderIntersectionCriteriaCollectionDTO (Collection<EsponderQueryRestrictionDTO> restrictions) {		
		super(restrictions);
	}

}
