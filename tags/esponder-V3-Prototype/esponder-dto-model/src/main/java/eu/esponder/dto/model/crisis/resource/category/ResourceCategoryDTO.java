/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.category;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ResourceCategoryDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ResourceCategoryDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -429348562225638140L;
	
}
