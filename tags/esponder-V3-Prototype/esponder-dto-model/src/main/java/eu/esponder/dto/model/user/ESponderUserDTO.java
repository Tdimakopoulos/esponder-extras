/*
 * 
 */
package eu.esponder.dto.model.user;



import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderUserDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"pkiID","actorID","eventadminID", "operationsCentre"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ESponderUserDTO extends ActorDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6015093808353080103L;
	
	/** The operations centre. */
	private OperationsCentreDTO operationsCentre;
	
	/** The actor id. */
	private Long actorID;
	
	/** The eventadmin id. */
	private Long eventadminID;	
	
	/** The pki id. */
	private Long pkiID;
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ESponderUserDTO [userName=" + actorID + ", id=" + id + "]";
	}

	/**
	 * Gets the actor id.
	 *
	 * @return the actor id
	 */
	public Long getActorID() {
		return actorID;
	}

	/**
	 * Sets the actor id.
	 *
	 * @param actorID the new actor id
	 */
	public void setActorID(Long actorID) {
		this.actorID = actorID;
	}

	/**
	 * Gets the eventadmin id.
	 *
	 * @return the eventadmin id
	 */
	public Long getEventadminID() {
		return eventadminID;
	}

	/**
	 * Sets the eventadmin id.
	 *
	 * @param eventadminID the new eventadmin id
	 */
	public void setEventadminID(Long eventadminID) {
		this.eventadminID = eventadminID;
	}

	/**
	 * Gets the pki id.
	 *
	 * @return the pki id
	 */
	public Long getPkiID() {
		return pkiID;
	}

	/**
	 * Sets the pki id.
	 *
	 * @param pkiID the new pki id
	 */
	public void setPkiID(Long pkiID) {
		this.pkiID = pkiID;
	}


	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OperationsCentreDTO getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OperationsCentreDTO operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

}
