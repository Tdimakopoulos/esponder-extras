/*
 * 
 */
package eu.esponder.exception;



// TODO: Auto-generated Javadoc
/**
 * The Class EsponderUncheckedException.
 * The code will stop excution and will throw a exception of Runtime type
 */
public class EsponderUncheckedException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4913259723869108745L;

	/**
	 * Instantiates a new esponder unchecked exception.
	 */
	public EsponderUncheckedException() {
	}

	/**
	 * Instantiates a new esponder unchecked exception.
	 *
	 * @param clz the clz
	 * @param message the message
	 */
	public EsponderUncheckedException(Class<?> clz, String message) {
		super(prepare(clz, message));
	}

	/**
	 * Instantiates a new esponder unchecked exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public EsponderUncheckedException(String message, Throwable cause) {
		super(prepare(cause, message), cause);
	}

	/**
	 * Prepare.
	 *
	 * @param cause the cause
	 * @param msg the msg
	 * @return the string
	 */
	private static String prepare(Throwable cause, String msg) {
		EsponderExceptionLogger.LogFatal(cause.getClass(), msg);
		return msg;
	}

	/**
	 * Prepare.
	 *
	 * @param cls the cls
	 * @param msg the msg
	 * @return the string
	 */
	private static String prepare(Class<?> cls, String msg) {
		EsponderExceptionLogger.LogFatal(cls, msg);
		return msg;
	}

}
