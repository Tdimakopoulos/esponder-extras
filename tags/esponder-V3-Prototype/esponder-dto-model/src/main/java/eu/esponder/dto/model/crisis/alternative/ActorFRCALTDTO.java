/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorFRCALTDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "resourceId", "title", "personnel",
		"voIPURL", "incidentCommander", "team", "subordinates" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class ActorFRCALTDTO extends FirstResponderActorALTDTO {

	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8100927169788855216L;

	/** The team. */
	private FRTeamALTDTO team;

	/** The subordinates. */
	private Set<ActorFRALTDTO> subordinates;

	/**
	 * The incident commander.
	 *
	 * @return the team
	 */
//	private Long incidentCommander;
	
	/**
	 * Gets the incident commander.
	 *
	 * @return the incident commander
	 */
//	public Long getIncidentCommander() {
//		return incidentCommander;
//	}

	/**
	 * Sets the incident commander.
	 *
	 * @param incidentCommander the new incident commander
	 */
//	public void setIncidentCommander(Long incidentCommander) {
//		this.incidentCommander = incidentCommander;
//	}

	/**
	 * Gets the team.
	 *
	 * @return the team
	 */
	public FRTeamALTDTO getTeam() {
		return team;
	}

	/**
	 * Sets the team.
	 *
	 * @param team the new team
	 */
	public void setTeam(FRTeamALTDTO team) {
		this.team = team;
	}

	/**
	 * Gets the subordinates.
	 *
	 * @return the subordinates
	 */
	public Set<ActorFRALTDTO> getSubordinates() {
		return subordinates;
	}

	/**
	 * Sets the subordinates.
	 *
	 * @param subordinates the new subordinates
	 */
	public void setSubordinates(Set<ActorFRALTDTO> subordinates) {
		this.subordinates = subordinates;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.alternative.FirstResponderActorALTDTO#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getName()+" [id=" + id + ", title=" + title + "]";
	}
}
