/*
 * 
 */
package eu.esponder.dto.model.crisis;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisContextParameterDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "paramName", "ParamValue", "crisisContext", "additionalInfo","startDate", "endDate", "crisiContextAlert"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CrisisContextParameterDTO extends ESponderEntityDTO {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7285118060235867701L;

	/** The id. */
	private Long id;
	
	/** The param name. */
	private String paramName;
	
	/** The param value. */
	private String paramValue;
	
	/** The crisis context. */
	private CrisisContextDTO crisisContext;
	
	/**
	 * Gets the crisis context.
	 *
	 * @return the crisis context
	 */
	public CrisisContextDTO getCrisisContext() {
		return crisisContext;
	}

	/**
	 * Sets the crisis context.
	 *
	 * @param crisisContext the new crisis context
	 */
	public void setCrisisContext(CrisisContextDTO crisisContext) {
		this.crisisContext = crisisContext;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Gets the param name.
	 *
	 * @return the param name
	 */
	public String getParamName() {
		return paramName;
	}

	/**
	 * Sets the param name.
	 *
	 * @param paramName the new param name
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	/**
	 * Gets the param value.
	 *
	 * @return the param value
	 */
	public String getParamValue() {
		return paramValue;
	}

	/**
	 * Sets the param value.
	 *
	 * @param paramValue the new param value
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

}
