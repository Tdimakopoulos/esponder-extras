package eu.esponder.CoordinatesForAddress;

public class Coordinates {

	private float Latitude;

	private float Longitude;

	private String FormatedAddress;
	
	private String FormatedCity;

	public Coordinates(float latitude, float longitude) {
		Latitude = latitude;
		Longitude = longitude;
	}

	/**
	 * @return the Latitude
	 */
	public float getLatitude() {
		return Latitude;
	}

	/**
	 * @param Latitude
	 *            the Latitude to set
	 */
	public void setLatitude(float Latitude) {
		this.Latitude = Latitude;
	}

	/**
	 * @return the Longitude
	 */
	public float getLongitude() {
		return Longitude;
	}

	/**
	 * @param Longitude
	 *            the Longitude to set
	 */
	public void setLongitude(float Longitude) {
		this.Longitude = Longitude;
	}

	/**
	 * @return the FormatedAddress
	 */
	public String getFormatedAddress() {
		return FormatedAddress;
	}

	/**
	 * @param FormatedAddress
	 *            the FormatedAddress to set
	 */
	public void setFormatedAddress(String FormatedAddress) {
		this.FormatedAddress = FormatedAddress;
	}

	/**
	 * @return the FormatedCity
	 */
	public String getFormatedCity() {
		return FormatedCity;
	}

	/**
	 * @param FormatedCity
	 *            the FormatedCity to set
	 */
	public void setFormatedCity(String FormatedCity) {
		this.FormatedCity = FormatedCity;
	}

}
