package eu.esponder.jaxb.model.type;


public class OperationalActionTypeDTO extends ActionTypeDTO {
	
	private TacticalActionTypeDTO parent;
	
	public TacticalActionTypeDTO getParent() {
		return parent;
	}

	public void setParent(TacticalActionTypeDTO parent) {
		this.parent = parent;
	}
	
	

}
