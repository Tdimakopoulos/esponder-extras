package eu.esponder.jaxb.model.crisis.resource;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.type.RankTypeDTO;

@XmlRootElement(name="Personnel")
@XmlType(name="Personnel")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "type", "title", "status", "firstName",
					"lastName", "rank", "availability", "organisation"})
public class PersonnelDTO {
	
	private Long id;
	
	private String firstName;
	
	private String lastName;
	
	private RankTypeDTO rank;
	
	private boolean availability;
	
	private OrganisationDTO organisation;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public RankTypeDTO getRank() {
		return rank;
	}

	public void setRank(RankTypeDTO rank) {
		this.rank = rank;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public boolean isAvailability() {
		return availability;
	}

	public void setAvailability(boolean availability) {
		this.availability = availability;
	}

	public OrganisationDTO getOrganisation() {
		return organisation;
	}

	public void setOrganisation(OrganisationDTO organisation) {
		this.organisation = organisation;
	}

}
