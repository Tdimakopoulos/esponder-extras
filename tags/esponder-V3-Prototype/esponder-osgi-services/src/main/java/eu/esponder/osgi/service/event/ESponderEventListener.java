/*
 * 
 */
package eu.esponder.osgi.service.event;

import java.io.IOException;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.prosyst.mprm.backend.event.Event;
import com.prosyst.mprm.backend.event.EventListener;
import com.prosyst.mprm.backend.event.EventListenerException;
import com.prosyst.mprm.common.ManagementException;
import com.prosyst.mprm.data.DictionaryInfo;

import eu.esponder.df.ruleengine.controller.bean.DatafusionControllerBean;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.osgi.service.connection.ConnectionHandler;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.exception.EsponderCheckedException;


// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving ESponderEvent events.
 * The class that is interested in processing a ESponderEvent
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addESponderEventListener<code> method. When
 * the ESponderEvent event occurs, that object's appropriate
 * method is invoked.
 *
 * @param <T> the generic type
 * @see ESponderEventEvent
 */
public class ESponderEventListener<T extends ESponderEvent<? extends ESponderEntityDTO>>
		implements EventListener {

	/** The connection handler. */
	private static ConnectionHandler connectionHandler = new ConnectionHandler();

	/** The mapper. */
	private static ObjectMapper mapper = initialiseMapper();

	/** The event topic. */
	private String eventTopic;

	/** The event topic class. */
	private Class<T> eventTopicClass;

	/**
	 * Instantiates a new e sponder event listener.
	 *
	 * @param eventClass the event class
	 * @throws ManagementException the management exception
	 */
	public ESponderEventListener(Class<T> eventClass)
			throws ManagementException {

		this.eventTopicClass = eventClass;
		this.setEventTopic(eventClass.getCanonicalName());

		if (connectionHandler.getRac() == null) {
			System.out.println("Event Listener Make New connection");
			connectionHandler.connect();
		}
	}

	/**
	 * Close connection.
	 */
	public void CloseConnection()
	{
		connectionHandler.CloseConnection();
	}
	
	/**
	 * Subscribe.
	 *
	 * @throws ManagementException the management exception
	 */
	public void subscribe() throws ManagementException {
		connectionHandler.getRac().addEventListener(this.getEventTopic(), this);
	}

	/* (non-Javadoc)
	 * @see com.prosyst.mprm.backend.event.EventListener#event(com.prosyst.mprm.backend.event.Event)
	 */
	@Override
	public void event(Event event) throws EventListenerException {

		//System.out.println("Event: " + event.getEventData());

		/*
		 * Get the event data from the EventAdmin
		 */
		DictionaryInfo data = (DictionaryInfo) event.getEventData();
		String esponderEventData = (String) data
				.get(ConnectionHandler.EVENT_PROPERTY_NAME);
		parseEventData(esponderEventData);

	}

	/**
	 * Parses the event data.
	 *
	 * @param eventData the event data
	 * @return the e sponder event<? extends e sponder entity dt o>
	 */
	private ESponderEvent<? extends ESponderEntityDTO> parseEventData(
			String eventData) {
		ESponderEvent<? extends ESponderEntityDTO> cssEvent = null;
		System.out.println("===> OSGI EVENT Listener - EVENT RECEIVED parsing .....");
		try {
			/*
			 * Parse the event data
			 */
			JsonFactory jsonFactory = new JsonFactory();
			JsonParser jp;

			jp = jsonFactory.createJsonParser(eventData);

			Class<? extends ESponderEvent<? extends ESponderEntityDTO>> clz = (Class<? extends ESponderEvent<? extends ESponderEntityDTO>>) eventTopicClass;

			cssEvent = mapper.readValue(jp, clz);

			System.out.println("===> EVENT :"+cssEvent.getClass().getName());
			if(cssEvent.getClass().getName().equalsIgnoreCase("eu.esponder.event.exception.GeneratedExceptionEvent"))
			{
				System.out.println("===> EVENT Exception Severity :"+cssEvent.getEventSeverity());
				System.out.println("===> EVENT Exception Message :"+cssEvent.getJournalMessage());
			}
			
			/*
			 * Passing event to DF
			 */
			RegisterDatafusionEvent(cssEvent);

			

		} catch (JsonParseException e) {
			System.out.println("******** OSGI EVENT Listener -  Parse Data JPARERROR ******"
					+ e.getMessage());
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
		} catch (JsonMappingException e) {
			System.out.println("******** OSGI EVENT Listener - Parse Data err MAPEX******"
					+ e.getMessage());
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
		} catch (IOException e) {
			System.out.println("******** OSGI EVENT Listener - Parse Data IOERR ******"
					+ e.getMessage());
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
		}
		return cssEvent;
	}

	/**
	 * Register datafusion event.
	 *
	 * @param event the event
	 */
	private void RegisterDatafusionEvent(
			ESponderEvent<? extends ESponderEntityDTO> event) {
		DatafusionControllerBean pEventManager = new DatafusionControllerBean();
		pEventManager.EsponderEventReceivedHandler(event);
	}

	

	/**
	 * Prints the event.
	 *
	 * @param event the event
	 */
	@SuppressWarnings("unused")
	private void printEvent(ESponderEvent<? extends ESponderEntityDTO> event) {
		System.out
				.println("########### OSGI EVENT Handler - EVENT RECEIVED #############");
		System.out.println("########### EVENT DETAILS START #############");
		System.out.println("CSSE # " + event.toString());
		System.out.println("CSSE attachment # "
				+ event.getEventAttachment().toString());
		System.out.println("CSSE severity # " + event.getEventSeverity());
		System.out.println("CSSE source # " + event.getEventSource());
		System.out.println("CSSE timestamp# " + event.getEventTimestamp());
		System.out.println("########### EVENT DETAILS END #############");
	}

	/**
	 * Gets the event topic.
	 *
	 * @return the event topic
	 */
	public String getEventTopic() {
		return eventTopic;
	}

	/**
	 * Sets the event topic.
	 *
	 * @param eventTopic the new event topic
	 */
	public void setEventTopic(String eventTopic) {
		this.eventTopic = eventTopic;
	}

	/**
	 * Initialise mapper.
	 *
	 * @return the object mapper
	 */
	public static ObjectMapper initialiseMapper() {
		/*
		 * Create Jackson deserialization mapper
		 */
		ObjectMapper objMapper = new ObjectMapper();
		objMapper
				.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
		objMapper
				.configure(
						DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,
						false);
		return objMapper;
	}

	/**
	 * Gets the event topic class.
	 *
	 * @return the event topic class
	 */
	public Class<T> getEventTopicClass() {
		return eventTopicClass;
	}

	/**
	 * Sets the event topic class.
	 *
	 * @param eventTopicClass the new event topic class
	 */
	public void setEventTopicClass(Class<T> eventTopicClass) {
		this.eventTopicClass = eventTopicClass;
	}

}
