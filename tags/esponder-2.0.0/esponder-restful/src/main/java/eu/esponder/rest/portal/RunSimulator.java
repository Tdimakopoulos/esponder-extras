package eu.esponder.rest.portal;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.rest.ESponderResource;

@Path("/portal/simulator")
public class RunSimulator extends ESponderResource {

	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}
	
	@GET
	@Path("/run")
	@Produces({ MediaType.APPLICATION_JSON })
	public String RunSimulation(
			@QueryParam("userID") @NotNull(message = "userID may not be null") final String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		MeasurementSimulator simulator = new MeasurementSimulator();
		try {
			simulator.runSimulation();
		} catch (InterruptedException e) {
			return "Simulation failed";
		}

		return "Simulation succeeded";
	}
	
}