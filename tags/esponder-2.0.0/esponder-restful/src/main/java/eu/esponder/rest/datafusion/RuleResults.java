package eu.esponder.rest.datafusion;


import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({ "ruleresultList" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class RuleResults implements Serializable {

	private static final long serialVersionUID = -363245325332031836L;

	private List<RuleResultsXML> resultList;

	public RuleResults() {
	}

	public RuleResults(List<RuleResultsXML> resultsList) {
		this.resultList = resultsList;
	}

	public List<RuleResultsXML> getResultList() {
		return resultList;
	}

	public void setResultList(List<RuleResultsXML> resultList) {
		this.resultList = resultList;
	}

	public String toString() {
		String result = "[ResultsList:";
		for (RuleResultsXML entity : this.getResultList()) {
			result += entity.toString();
		}
		result += "]";
		return result;
	}

}