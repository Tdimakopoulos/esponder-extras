package eu.esponder.rest.portal;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.crisis.resource.category.PlannableResourceCategoryDTO;
import eu.esponder.rest.ESponderResource;

@Path("/portal/categories")
public class ESponderCategories extends ESponderResource {

	@GET
	@Path("/findById")
	@Produces({MediaType.APPLICATION_JSON})
	public PlannableResourceCategoryDTO findPlannableResourceCategoryById(
			@QueryParam("categoryID") @NotNull(message = "categoryID may not be null") Long categoryID,
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID){

		PlannableResourceCategoryDTO categoryDTO = (PlannableResourceCategoryDTO) this.getResourceCategoryRemoteService().findByIdRemote(PlannableResourceCategoryDTO.class, categoryID);
		return categoryDTO;
	}
	
}
