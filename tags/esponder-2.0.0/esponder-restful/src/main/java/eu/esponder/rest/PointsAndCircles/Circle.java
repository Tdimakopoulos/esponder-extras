package eu.esponder.rest.PointsAndCircles;


public class Circle {
    
    public double radius;
    public double x,y; 
    
    /** Creates a new instance of Circle */
    public Circle() {
    }
    
    public Circle(double x, double y, double radius)
    {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }
    
    public boolean isInCircle(MovablePoint p)
    {
        return ( (x - p.getMidX())*(x - p.getMidX()) + (y - p.getMidY())*(y - p.getMidY()) <= radius * radius);
    }
    
}
