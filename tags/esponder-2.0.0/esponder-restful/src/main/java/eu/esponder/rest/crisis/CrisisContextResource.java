package eu.esponder.rest.crisis;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.rest.ESponderResource;


@Path("/crisis/context")
public class CrisisContextResource extends ESponderResource {
	
	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}
	
	@GET
	@Path("/oc")
	@Produces({MediaType.APPLICATION_JSON})
	public List<OperationsCentreDTO> getUserOperationsCentres(
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findUserOperationsCentresRemote(userID);
	}
	
	@GET
	@Path("/oc/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreDTO getOperationsCentreByID(
			@QueryParam("operationsCentreID") @NotNull(message="Operations Centre Id may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findOperationCentreByIdRemote(operationsCentreID);
	}
	
	@GET
	@Path("/oc/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreDTO getOperationsCentreByTitle(
			@QueryParam("operationsCentreTitle") @NotNull(message="Operations Centre Title may not be null") String operationsCentreTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findOperationsCentreByTitleRemote(operationsCentreTitle);
	}
	
	@GET
	@Path("/oc/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getAllOperationsCentres(
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return new ResultListDTO(this.getOperationsCentreRemoteService().findAllOperationsCentresRemote());
	}
	
	@GET
	@Path("/oc/findAllByCrisisContext")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getAllOperationsCentresByCrisisCOntext(
			@QueryParam("crisisContextID") @NotNull(message="Crisis Context ID may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return new ResultListDTO(this.getOperationsCentreRemoteService().findAllOperationsCentresByCrisisContextRemote(crisisContextID));
	}
	
	@POST
	@Path("/oc/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreDTO createOperationsCentre(
			@NotNull(message="Operations Centre object may not be null") OperationsCentreDTO operationsCentreDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().createOperationsCentreRemote(operationsCentreDTO, userID);
	}
	
	@PUT
	@Path("/oc/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreDTO updateOperationsCentre(
			@NotNull(message="Operations Centre object may not be null") OperationsCentreDTO operationsCentreDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().updateOperationsCentreRemote(operationsCentreDTO, userID);
	}
	
	@DELETE
	@Path("/oc/delete")
	public Long deleteOperationsCentre(
			@QueryParam("operationsCEntreID") @NotNull(message="Operations Centre ID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		this.getOperationsCentreRemoteService().deleteOperationsCentreRemote(operationsCentreID, userID);
		return operationsCentreID;
	}
	
	@GET
	@Path("/regoc/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredOperationsCentreDTO getRegisteredOperationsCentreByID(
			@QueryParam("registeredOperationsCentreID") @NotNull(message="Registered Operations Centre Id may not be null") Long registeredOperationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findRegisteredOperationCentreByIdRemote(registeredOperationsCentreID);
	}
	
	@GET
	@Path("/regoc/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredOperationsCentreDTO getRegisteredOperationsCentreByTitle(
			@QueryParam("registeredOperationsCentreTitle") @NotNull(message="Registered Operations Centre Title may not be null") String registeredOperationsCentreTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().findRegisteredOperationsCentreByTitleRemote(registeredOperationsCentreTitle);
	}
	
	@GET
	@Path("/regoc/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getAllRegisteredOperationsCentres(
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return new ResultListDTO(this.getOperationsCentreRemoteService().findAllRegisteredOperationsCentresRemote());
	}
	
	@POST
	@Path("/regoc/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredOperationsCentreDTO createRegisteredRegisteredOperationsCentre(
			@NotNull(message="Registered Operations Centre object may not be null") RegisteredOperationsCentreDTO registeredOperationsCentreDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().createRegisteredOperationsCentreRemote(registeredOperationsCentreDTO, userID);
	}
	
	@PUT
	@Path("/regoc/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredOperationsCentreDTO updateRegisteredOperationsCentre(
			@NotNull(message="Registered Operations Centre object may not be null") RegisteredOperationsCentreDTO registeredOperationsCentreDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		return this.getOperationsCentreRemoteService().updateRegisteredOperationsCentreRemote(registeredOperationsCentreDTO, userID);
	}
	
	@DELETE
	@Path("/regoc/delete")
	public Long deleteRegisteredOperationsCentre(
			@QueryParam("registeredOperationsCEntreID") @NotNull(message="Registered Operations Centre ID may not be null") Long registeredOperationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID) {
		
		Long userID=SecurityCheck(szuserID);
		
		this.getOperationsCentreRemoteService().deleteRegisteredOperationsCentreRemote(registeredOperationsCentreID, userID);
		return registeredOperationsCentreID;
	}
	
	
	
	
}
