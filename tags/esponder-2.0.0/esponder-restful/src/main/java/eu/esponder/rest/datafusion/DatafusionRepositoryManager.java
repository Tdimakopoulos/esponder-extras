package eu.esponder.rest.datafusion;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService;
import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;
import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.rest.ESponderResource;
import eu.esponder.test.ResourceLocator;



@Path("/datafusion/repository")
public class DatafusionRepositoryManager extends ESponderResource {

	private Long SecurityCheck(String userID)
	{
		return new Long(userID);
	}
	
	@GET
	@Path("/remotetolocal")
	public String remotetolocal (@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID)
			throws Exception {
		Long userID=SecurityCheck(szuserID);
		
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.ReinitializeRepository();
		

		return szReturnStatus;
	}

	@GET
	@Path("/ruleresults")
	@Produces({ MediaType.APPLICATION_JSON })
	public RuleResults ruleresults (@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID)
			throws Exception {
		Long userID=SecurityCheck(szuserID);
		
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		
		List<RuleResultsXML> pResults=pservice.GetRuleResults();
		return new RuleResults(pResults);
	}
	
	@GET
	@Path("/deleterepo")
	public String deleterepo (@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID)
			throws Exception {
		Long userID=SecurityCheck(szuserID);
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.DeleteRepository();
		

		return szReturnStatus;
	}

	@GET
	@Path("/switchtolocal")
	public String switchtolocal (@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID)
			throws Exception {
		Long userID=SecurityCheck(szuserID);
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.SetToLocal();
		

		return szReturnStatus;
	}

	@GET
	@Path("/switchtoguvnor")
	public String switchtoguvnor (@QueryParam("userID") @NotNull(message="userID may not be null") String szuserID)
			throws Exception {
		Long userID=SecurityCheck(szuserID);
		String szReturnStatus="NoErrors";

		DatafusionControllerRemoteService  pservice= ResourceLocator.lookup("esponder/DatafusionControllerBean/remote");
		
		pservice.SetToLive();
		

		return szReturnStatus;
	}
}
