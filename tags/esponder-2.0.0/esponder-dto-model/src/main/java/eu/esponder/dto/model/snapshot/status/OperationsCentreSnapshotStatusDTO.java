package eu.esponder.dto.model.snapshot.status;


public enum OperationsCentreSnapshotStatusDTO {
	STATIONARY,
	MOVING
}
