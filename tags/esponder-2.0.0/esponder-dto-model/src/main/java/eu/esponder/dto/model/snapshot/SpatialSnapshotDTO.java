package eu.esponder.dto.model.snapshot;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class SpatialSnapshotDTO extends SnapshotDTO {
	
	private static final long serialVersionUID = -8816608444526837797L;
	
	protected LocationAreaDTO locationArea;

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

	@Override
	public String toString() {
		return "SpatialSnapshotDTO [locationArea=" + locationArea + ", id=" + id + "]";
	}

}
