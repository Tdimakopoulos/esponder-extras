package eu.esponder.dto.model;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({ "resultList" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class SensorResultListDTO  implements Serializable  {

	
	private static final long serialVersionUID = -5545699205753830677L;
	private List<SensorSnapshotDetailsList> resultList;

	public SensorResultListDTO()
	{
		
	}
	public SensorResultListDTO(List<SensorSnapshotDetailsList> resultsList) {
		this.resultList = resultsList;
	}
	
	public List<SensorSnapshotDetailsList> getResultList() {
		return resultList;
	}

	public void setResultList(List<SensorSnapshotDetailsList> resultList) {
		this.resultList = resultList;
	}
	
	
}
