package eu.esponder.dto.model.snapshot.status;


public enum SensorSnapshotStatusDTO {
	WORKING,
	MALFUNCTIONING,
	DAMAGED
}
