package eu.esponder.dto.model.crisis.resource.sensor;

import org.codehaus.jackson.annotate.JsonTypeInfo;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class EnvironmentalSensorDTO extends SensorDTO {

	private static final long serialVersionUID = -3878737094263093312L;

}
