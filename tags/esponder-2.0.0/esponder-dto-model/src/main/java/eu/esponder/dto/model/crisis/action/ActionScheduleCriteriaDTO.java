package eu.esponder.dto.model.crisis.action;

import java.sql.Date;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.status.ActionSnapshotStatusDTO;


@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "status", "dateAfter"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionScheduleCriteriaDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = -832456284608428226L;

	private ActionSnapshotStatusDTO status;

//	private XMLGregorianCalendar dateAfter;
	
	private Long dateAfter;

	public ActionSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActionSnapshotStatusDTO status) {
		this.status = status;
	}

	public Long getDateAfter() {
		return dateAfter;
	}

	public void setDateAfter(Long dateAfter) {
		this.dateAfter = dateAfter;
	}

}
