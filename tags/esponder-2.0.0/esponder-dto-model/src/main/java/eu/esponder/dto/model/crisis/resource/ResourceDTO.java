package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ResourceDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = 6714622659532068332L;

	protected String type;

	protected String title;

	protected ResourceStatusDTO status;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ResourceStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ResourceStatusDTO status) {
		this.status = status;
	}

	public String getResourceId() {
		return id+":"+this.type;
	}

	public void setResourceId(String resourceId) {
		String id[];
		id = resourceId.split(":");
		if(id[0].equals("null")) {
			this.setId(null);
		}
		else {
			this.setId(new Long(id[0]).longValue());	
		}
		this.setType(id[1]);
	}

	@Override
	public String toString() {
		return "ResourceDTO [status=" + status + ", id=" + id + ", title="
				+ title + "]";
	}

}
