package eu.esponder.dto.model.snapshot;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.snapshot.status.CrisisContextSnapshotStatusDTO;

@JsonPropertyOrder({"id", "status","crisisContext", "previous", "next",  "locationArea", "period"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CrisisContextSnapshotDTO extends SpatialSnapshotDTO {
	
	private static final long serialVersionUID = 2780925199500331932L;

	private Long id;
	
	private CrisisContextDTO crisisContext;
	
	private CrisisContextSnapshotStatusDTO status;
	
	private CrisisContextSnapshotDTO previous;
	
	private CrisisContextSnapshotDTO next;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CrisisContextDTO getCrisisContext() {
		return crisisContext;
	}

	public void setCrisisContext(CrisisContextDTO crisisContext) {
		this.crisisContext = crisisContext;
	}

	public CrisisContextSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(CrisisContextSnapshotStatusDTO status) {
		this.status = status;
	}

	public CrisisContextSnapshotDTO getPrevious() {
		return previous;
	}

	public void setPrevious(CrisisContextSnapshotDTO previous) {
		this.previous = previous;
	}

	public CrisisContextSnapshotDTO getNext() {
		return next;
	}

	public void setNext(CrisisContextSnapshotDTO next) {
		this.next = next;
	}

}
