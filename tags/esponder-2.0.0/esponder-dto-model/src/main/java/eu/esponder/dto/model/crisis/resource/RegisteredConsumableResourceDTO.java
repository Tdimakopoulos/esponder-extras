package eu.esponder.dto.model.crisis.resource;

import java.math.BigDecimal;




public class RegisteredConsumableResourceDTO extends ResourceDTO {
	
	
	private static final long serialVersionUID = -1419531858035816761L;


	private BigDecimal quantity;
	
	
	private RegisteredReusableResourceDTO container;

	private Long consumableResourceCategoryId;
	
	
	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public RegisteredReusableResourceDTO getContainer() {
		return container;
	}

	public void setContainer(RegisteredReusableResourceDTO container) {
		this.container = container;
	}

	public Long getConsumableResourceCategoryId() {
		return consumableResourceCategoryId;
	}

	public void setConsumableResourceCategoryId(Long consumableResourceCategoryId) {
		this.consumableResourceCategoryId = consumableResourceCategoryId;
	}



	
}
