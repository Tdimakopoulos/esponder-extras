package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.type.PersonnelTrainingTypeDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "type", "shortTitle", "personnelCategory", "personnelTrainingType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PersonnelTrainingDTO extends PersonnelCompetenceDTO {

	private static final long serialVersionUID = 1572773899518146208L;
	
	private PersonnelTrainingTypeDTO personnelTrainingType;

	public PersonnelTrainingTypeDTO getPersonnelTrainingType() {
		return personnelTrainingType;
	}

	public void setPersonnelTrainingType(
			PersonnelTrainingTypeDTO personnelTrainingType) {
		this.personnelTrainingType = personnelTrainingType;
	}

}
