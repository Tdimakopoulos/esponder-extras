package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.action.ActionService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.model.crisis.action.Action;
import eu.esponder.util.ejb.ServiceLocator;

public class ActionObjectiveActionConverter implements CustomConverter {
	
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	protected ActionService getActionService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if (sourceClass == Action.class && source != null) {
			Action sourceAction = (Action) source;
			ActionDTO destActionDTO = new ActionDTO();
			destActionDTO.setId(sourceAction.getId());
			destActionDTO.setTitle(sourceAction.getTitle());
			destination = destActionDTO;
		}
		else if(sourceClass == ActionDTO.class && source!=null) { 
			ActionDTO sourceActionDTO = (ActionDTO) source;
			Action destAction= this.getActionService().findActionById(sourceActionDTO.getId());
			destination = destAction;
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
