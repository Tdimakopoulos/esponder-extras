package eu.esponder.controller.crisis.resource;

import java.util.Date;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;

@Remote
public interface EquipmentRemoteService {

	public EquipmentDTO findByIdRemote(Long equipmentID);

	public EquipmentDTO findByTitleRemote(String title);

	public EquipmentDTO createEquipmentRemote(EquipmentDTO equipmentDTO, Long userID);
	
	public EquipmentDTO updateEquipmentRemote(EquipmentDTO equipmentDTO, Long userID);
	
	public void deleteEquipmentRemote(Long equipmentId, Long userID);

	public EquipmentSnapshotDTO findEquipmentSnapshotByDateRemote(Long equipmentID, Date maxDate);
	
	public EquipmentSnapshotDTO createEquipmentSnapshotRemote(EquipmentSnapshotDTO snapshotDTO, Long userID);

	public EquipmentSnapshotDTO updateEquipmentSnapshotRemote(EquipmentSnapshotDTO snapshotDTO, Long userID);

	public void deleteEquipmentSnapshotRemote(Long equipmentSnapshotId, Long userID);
	
}
