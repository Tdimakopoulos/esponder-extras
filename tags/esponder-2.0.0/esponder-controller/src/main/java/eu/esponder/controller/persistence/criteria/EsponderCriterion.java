package eu.esponder.controller.persistence.criteria;

public class EsponderCriterion extends EsponderQueryRestriction  {

	private static final long serialVersionUID = -6667733307605361832L;

	private String field;
	
	private EsponderCriterionExpressionEnum expression;
	
	private Object value;

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public EsponderCriterionExpressionEnum getExpression() {
		return expression;
	}

	public void setExpression(EsponderCriterionExpressionEnum expression) {
		this.expression = expression;
	}

	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
}
