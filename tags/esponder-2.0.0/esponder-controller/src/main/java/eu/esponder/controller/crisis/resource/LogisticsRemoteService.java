package eu.esponder.controller.crisis.resource;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;

@Remote
public interface LogisticsRemoteService {
	
	public ReusableResourceDTO findReusableResourceByIdRemote(Long reusableID);
	
	public List<ReusableResourceDTO> findAllReusableResourcesRemote();
	
	public ReusableResourceDTO findReusableResourceByTitleRemote(String title);
	
	public ReusableResourceDTO createReusableResourceRemote(ReusableResourceDTO reusableResourceDTO, Long userID);
	
	public ReusableResourceDTO updateReusableResourceRemote(ReusableResourceDTO reusableResourceDTO, Long userID);
	
	public void deleteReusableResourceRemote(Long reusableResourceDTOID, Long userID);
	
	public ConsumableResourceDTO findConsumableResourceByIdRemote(Long consumableID);
	
	public List<ConsumableResourceDTO> findAllConsumableResourcesRemote();
	
	public ConsumableResourceDTO findConsumableResourceByTitleRemote(String title);
	
	public ConsumableResourceDTO createConsumableResourceRemote(ConsumableResourceDTO consumableResourceDTO, Long userID);
	
	public ConsumableResourceDTO updateConsumableResourceRemote(ConsumableResourceDTO consumableResourceDTO, Long userID);
	
	public void deleteConsumableResourceRemote(Long consumableResourceDTOID, Long userID);

	public RegisteredReusableResourceDTO findRegisteredReusableResourceByIdRemote(
			Long registeredReusableID);

	public RegisteredConsumableResourceDTO findRegisteredConsumableResourceByIdRemote(Long registeredConsumableID);

	public RegisteredReusableResourceDTO findRegisteredReusableResourceByTitleRemote(String title);
	
	public List<RegisteredReusableResourceDTO> findAllRegisteredReusableResourcesRemote();

	public RegisteredConsumableResourceDTO findRegisteredConsumableResourceByTitleRemote(String title);

	public RegisteredConsumableResourceDTO createRegisteredConsumableResourceRemote(
			RegisteredConsumableResourceDTO registeredConsumableResourceDTO, Long userID);

	public RegisteredReusableResourceDTO createRegisteredReusableResourceRemote(
			RegisteredReusableResourceDTO registeredReusableResourceDTO, Long userID);

	public void deleteRegisteredConsumableResourceRemote(Long registeredReusableResourceDTOID, Long userID);

	public void deleteRegisteredReusableResourceRemote(Long registeredReusableResourceDTOID, Long userID);

	public RegisteredReusableResourceDTO updateRegisteredReusableResourceRemote(
			RegisteredReusableResourceDTO registeredReusableResourceDTO, Long userID);

	public RegisteredConsumableResourceDTO updateRegisteredConsumableResourceRemote(
			RegisteredConsumableResourceDTO registeredConsumableResourceDTO, Long userID);
	
	public List<RegisteredConsumableResourceDTO> findAllRegisteredConsumableResourcesRemote();

}
