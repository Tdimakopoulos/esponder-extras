package eu.esponder.controller.persistence.criteria;

import java.util.Collection;

public class EsponderNegationCriteriaCollection extends EsponderCriteriaCollection {
	
	private static final long serialVersionUID = 1754280753704823556L;

	public EsponderNegationCriteriaCollection(
			Collection<EsponderQueryRestriction> restrictions) {
		super(restrictions);
	}

}
