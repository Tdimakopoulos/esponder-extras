package eu.esponder.controller.crisis.resource.bean;

import java.util.HashMap;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.resource.OrganisationRemoteService;
import eu.esponder.controller.crisis.resource.OrganisationService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.OrganisationDTO;
import eu.esponder.model.crisis.resource.Organisation;

@Stateless
public class OrganisationBean implements OrganisationService, OrganisationRemoteService {

	@EJB
	private CrudService<Organisation> organisationCrudService;
	
	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	@Override
	public OrganisationDTO findDTOById(Long organisationID) {
		Organisation organisation = findById(organisationID);
		OrganisationDTO organisationDTO = (OrganisationDTO) mappingService.mapESponderEntity(organisation, OrganisationDTO.class);
		return  organisationDTO;
	}
	
	@Override
	public Organisation findById(Long organisationID) {
		return (Organisation) organisationCrudService.find(Organisation.class, organisationID);
	}

	//-------------------------------------------------------------------------
	
	@Override
	public OrganisationDTO findDTOByTitle(String organisationTitle) {
		Organisation organisation = findByTitle(organisationTitle);
		OrganisationDTO organisationDTO = (OrganisationDTO) mappingService.mapESponderEntity(organisation, OrganisationDTO.class);
		return organisationDTO;
	}
	
	@Override
	public Organisation findByTitle(String organisationTitle) {
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("title", organisationTitle);
		return (Organisation) organisationCrudService.findSingleWithNamedQuery("Organisation.findByTitle", parameters);
	}
	
	//-------------------------------------------------------------------------
	
	@Override
	public OrganisationDTO createOrganisationDTO(OrganisationDTO organisationDTO) {
		Organisation organisation = (Organisation) mappingService.mapESponderEntityDTO(organisationDTO, Organisation.class);
		organisation = createOrganisation(organisation);
		return (OrganisationDTO) mappingService.mapESponderEntity(organisation, OrganisationDTO.class);
	}
	
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Organisation createOrganisation(Organisation organisation) {
		return (Organisation) organisationCrudService.create(organisation);
	}


	//-------------------------------------------------------------------------

	@Override
	public OrganisationDTO updateOrganisationDTO(OrganisationDTO organisationDTO) {
		Organisation organisation = (Organisation) mappingService.mapESponderEntityDTO(organisationDTO, Organisation.class);
		organisation = updateOrganisation(organisation);
		return (OrganisationDTO) mappingService.mapESponderEntity(organisation, OrganisationDTO.class);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public Organisation updateOrganisation(Organisation organisation) {
		return (Organisation) organisationCrudService.update(organisation);
	}
	
	//-------------------------------------------------------------------------
	@Override
	public void deleteOrganisationDTO(OrganisationDTO organisationDTO) {
		Organisation organisation = (Organisation) mappingService.mapESponderEntityDTO(organisationDTO, Organisation.class);
		deleteOrganisation(organisation);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteOrganisation(Organisation organisation) {
		organisationCrudService.delete(organisation);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteOrganisationDTO(Long organisationDTOId) {
		deleteOrganisationByID(organisationDTOId);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteOrganisationByID(Long organisationID) {
		organisationCrudService.delete(Organisation.class, organisationID);
	}

	//-------------------------------------------------------------------------

}
