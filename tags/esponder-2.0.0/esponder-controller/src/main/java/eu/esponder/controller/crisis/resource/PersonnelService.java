package eu.esponder.controller.crisis.resource;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Personnel;
import eu.esponder.model.crisis.resource.PersonnelCompetence;

@Local
public interface PersonnelService {

	public Personnel findPersonnelById(Long personnelID);

	public Personnel findPersonnelByTitle(String personnelTitle);

	public Personnel createPersonnel(Personnel personnel, Long userID);

	public Personnel updatePersonnel(Personnel personnel, Long userID);

	public void deletePersonnel(Personnel personnel);

	public void deletePersonnelByID(Long personnelID);

	public PersonnelCompetence findPersonnelCompetenceById(Long personnelCompetenceID);

	public PersonnelCompetence findPersonnelCompetenceByTitle(String personnelCompetenceTitle);

	public PersonnelCompetence createPersonnelCompetence(PersonnelCompetence personnelCompetence, Long userID);

	public PersonnelCompetence updatePersonnelCompetence(PersonnelCompetence personnelCompetence, Long userID);

	public void deletePersonnelCompetenceByID(Long personnelCompetenceID);

	public List<Personnel> findAllPersonnel();
	
}
