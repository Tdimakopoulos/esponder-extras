package eu.esponder.controller.events.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.events.EventsEntityRemoteService;
import eu.esponder.controller.events.EventsEntityService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.events.entity.OsgiEventsEntityDTO;
import eu.esponder.model.events.entity.OsgiEventsEntity;

@Stateless
public class EventsEntityBean implements EventsEntityService, EventsEntityRemoteService {

	@EJB
	private CrudService<OsgiEventsEntity> osgiEventsCrudService;

	@EJB
	private ESponderMappingService mappingService;

	//-------------------------------------------------------------------------

	@Override
	public OsgiEventsEntityDTO findOsgiEventsEntityByIdRemote(Long osgiEventsEntityID) {
		return (OsgiEventsEntityDTO) mappingService.mapObject(findOsgiEventsEntityById(osgiEventsEntityID), OsgiEventsEntityDTO.class);
	}

	@Override
	public OsgiEventsEntity findOsgiEventsEntityById(Long osgiEventsEntityID) {
		return (OsgiEventsEntity) osgiEventsCrudService.find(OsgiEventsEntity.class, osgiEventsEntityID);
	}

	//-------------------------------------------------------------------------
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OsgiEventsEntityDTO> findAllOsgiEventsEntitiesRemote() {
		return (List<OsgiEventsEntityDTO>) mappingService.mapObject(findAllOsgiEventsEntities(), OsgiEventsEntityDTO.class);
	}

	@Override
	public List<OsgiEventsEntity> findAllOsgiEventsEntities() {
		return (List<OsgiEventsEntity>) osgiEventsCrudService.findWithNamedQuery("OsgiEventsEntity.findAll");
	}

	//-------------------------------------------------------------------------

	@Override
	public List<OsgiEventsEntityDTO> findOsgiEventsEntitiesBySeverityRemote(String severity) {
		List<OsgiEventsEntityDTO> eventsDTOList = new ArrayList<OsgiEventsEntityDTO>();
		List<OsgiEventsEntity> eventsList = new ArrayList<OsgiEventsEntity>();
		eventsList = findOsgiEventsEntitiesBySeverity(severity);
		if(eventsList != null){
			for(OsgiEventsEntity e : eventsList) {
				eventsDTOList.add((OsgiEventsEntityDTO) mappingService.mapESponderEntity(e, OsgiEventsEntityDTO.class));
			}
			return eventsDTOList;
		}
		else
			return null;
	}

	@Override
	public List<OsgiEventsEntity> findOsgiEventsEntitiesBySeverity(String severity) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("severity", severity);
		return (List<OsgiEventsEntity>) osgiEventsCrudService.findWithNamedQuery("OsgiEventsEntity.findBySeverity", params);
	}

	//-------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	@Override
	public List<OsgiEventsEntityDTO> findOsgiEventsEntitiesBySourceIdRemote(Long sourceId) {
		return (List<OsgiEventsEntityDTO>) mappingService.mapObject(findOsgiEventsEntitiesBySourceId(sourceId), OsgiEventsEntityDTO.class);
	}

	@Override
	public List<OsgiEventsEntity> findOsgiEventsEntitiesBySourceId(Long sourceId) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("sourceid", sourceId);
		return (List<OsgiEventsEntity>) osgiEventsCrudService.findWithNamedQuery("OsgiEventsEntity.findBySourceid", params);
	}

	//-------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OsgiEventsEntityDTO createOsgiEventsEntityRemote(OsgiEventsEntityDTO osgiEventsEntityDTO, Long userID) {
		OsgiEventsEntity osgiEventsEntity = (OsgiEventsEntity) mappingService.mapObject(osgiEventsEntityDTO, OsgiEventsEntity.class);
		return (OsgiEventsEntityDTO) mappingService.mapObject(createOsgiEventsEntity(osgiEventsEntity, userID), OsgiEventsEntityDTO.class);
	}

	@Override
	public OsgiEventsEntity createOsgiEventsEntity(OsgiEventsEntity osgiEventsEntity, Long userID) {
		return osgiEventsCrudService.create(osgiEventsEntity);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteOsgiEventsEntityRemote(Long osgiEventsEntityID, Long userID) {
		this.deleteOsgiEventsEntity(osgiEventsEntityID, userID);
	}

	@Override
	public void deleteOsgiEventsEntity(Long osgiEventsEntityID, Long userID) {
		osgiEventsCrudService.delete(OsgiEventsEntity.class, osgiEventsEntityID);
	}

	//-------------------------------------------------------------------------

	@Override
	public OsgiEventsEntityDTO updateOsgiEventsEntityRemote(OsgiEventsEntityDTO osgiEventsEntityDTO, Long userID) {
		OsgiEventsEntity osgiEventsEntity = (OsgiEventsEntity) mappingService.mapObject(osgiEventsEntityDTO, OsgiEventsEntity.class);
		return (OsgiEventsEntityDTO) mappingService.mapObject(updateOsgiEventsEntity(osgiEventsEntity, userID), OsgiEventsEntityDTO.class);
	}

	@Override
	public OsgiEventsEntity updateOsgiEventsEntity(OsgiEventsEntity osgiEventsEntity, Long userID) {
		return (OsgiEventsEntity) osgiEventsCrudService.update(osgiEventsEntity);
	}

}
