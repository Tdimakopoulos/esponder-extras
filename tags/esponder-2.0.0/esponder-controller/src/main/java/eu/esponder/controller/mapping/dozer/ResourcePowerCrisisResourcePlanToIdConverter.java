package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.CrisisService;
import eu.esponder.model.crisis.resource.plan.CrisisResourcePlan;
import eu.esponder.util.ejb.ServiceLocator;

public class ResourcePowerCrisisResourcePlanToIdConverter implements CustomConverter {
	
	protected CrisisService getCrisisService() {
		try {
			return ServiceLocator.getResource("esponder/CrisisBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	private Long userID = new Long(1);
	
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {
		
		// Model to DTO
		// ESponderEntity --> ESponderEntityId
		if (sourceClass == CrisisResourcePlan.class && source != null) {
			destination = ((CrisisResourcePlan)source).getId();
		}
		else if(sourceClass == Long.class && source!=null) {
			Object destObject = this.getCrisisService().findCrisisResourcePlanById((Long) source, userID);
				destination = destObject;
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
