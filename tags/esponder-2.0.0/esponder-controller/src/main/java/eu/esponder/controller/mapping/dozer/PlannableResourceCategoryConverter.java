package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.category.ResourceCategoryService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.model.crisis.resource.category.PlannableResourceCategory;
import eu.esponder.model.crisis.resource.category.ResourceCategory;
import eu.esponder.util.ejb.ServiceLocator;

public class PlannableResourceCategoryConverter implements CustomConverter {

	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected ResourceCategoryService getCategoryService() {
		try {
			return ServiceLocator.getResource("esponder/ResourceCategoryBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if(source instanceof PlannableResourceCategory && source != null) {
//			if(source.getClass() == EquipmentCategory.class){
//				destination = this.getMappingService().mapObject(source, EquipmentCategoryDTO.class);
//			}
//			else if (source.getClass() == ConsumableResourceCategory.class) {
//				destination = this.getMappingService().mapObject(source, ConsumableResourceCategoryDTO.class);
//			}
//			else if (source.getClass() == ReusableResourceCategory.class) {
//				destination = this.getMappingService().mapObject(source, ReusableResourceCategoryDTO.class);
//			}
//			else if (source.getClass() == OperationsCentreCategory.class) {
//				destination = this.getMappingService().mapObject(source, OperationsCentreCategoryDTO.class);
//			}
//			else if (source.getClass() == PersonnelCategory.class) {
//				destination = this.getMappingService().mapObject(source, PersonnelCategoryDTO.class);
//			}
			destination = ((PlannableResourceCategory)source).getId();
		}
		else
//			if(source instanceof PlannableResourceCategoryDTO && source != null) {
//				if(source.getClass() == EquipmentCategoryDTO.class){
//					destination = this.getMappingService().mapObject(source, EquipmentCategory.class);
//				}
//				else if (source.getClass() == ConsumableResourceCategoryDTO.class) {
//					destination = this.getMappingService().mapObject(source, ConsumableResourceCategory.class);
//				}
//				else if (source.getClass() == ReusableResourceCategoryDTO.class) {
//					destination = this.getMappingService().mapObject(source, ReusableResourceCategory.class);
//				}
//				else if (source.getClass() == OperationsCentreCategoryDTO.class) {
//					destination = this.getMappingService().mapObject(source, OperationsCentreCategory.class);
//				}
//				else if (source.getClass() == PersonnelCategoryDTO.class) {
//					destination = this.getMappingService().mapObject(source, PersonnelCategory.class);
//				}
//			}
			if(sourceClass == Long.class && source != null) { 
				destination = this.getCategoryService().findById((Class<? extends ResourceCategory>) destinationClass, (Long) source);
			}
			else {
				//FIXME Implement custom ESponderException
			}
		return destination;
	}

}
