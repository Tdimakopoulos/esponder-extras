package eu.esponder.controller.crisis.action;

import javax.ejb.Local;

import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.crisis.action.ActionObjective;
import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.model.crisis.action.ActionPartObjective;
import eu.esponder.model.snapshot.action.ActionPartSnapshot;
import eu.esponder.model.snapshot.action.ActionSnapshot;

@Local
public interface ActionService {

	public Action findActionById(Long actionID);

	public Action findActionByTitle(String title);

	public Action createAction(Action action, Long userID);

	public Action updateAction(Action action, Long userID);

	public void deleteAction(Long actionDTOID, Long userID);

	public ActionPart findActionPartById(Long actionID);

	public ActionPart findActionPartByTitle(String title);

	public ActionPart createActionPart(ActionPart actionPart, Long userID);

	public ActionPart updateActionPart(ActionPart actionPart, Long userID);

	public void deleteActionPart(Long actionPartID, Long userID);

	public ActionObjective findActionObjectiveByTitle(String title);

	public ActionObjective findActionObjectiveById(Long actionID);

	public ActionObjective createActionObjective(ActionObjective actionObjective, Long userID);

	public ActionObjective updateActionObjective(ActionObjective actionObjective, Long userID);

	public void deleteActionObjective(Long actionObjectiveId, Long userID);

	public ActionPartObjective findActionPartObjectiveById(Long actionID);

	public ActionPartObjective findActionPartObjectiveByTitle(String title);

	public ActionPartObjective createActionPartObjective(ActionPartObjective actionPartObjective, Long userID);

	public ActionPartObjective updateActionPartObjective(ActionPartObjective actionPartObjective, Long userID);

	public void deleteActionPartObjective(Long actionPartObjectiveId, Long userID);

	public ActionSnapshot findActionSnapshotById(Long actionSnapshotID);

	public ActionSnapshot createActionSnapshot(ActionSnapshot actionSnapshot,Long userID);
	
	public ActionPartSnapshot findActionPartSnapshotById(Long actionPartSnapshotID);

	public ActionPartSnapshot createActionPartSnapshot(ActionPartSnapshot actionPartSnapshot, Long userID);

	public ActionSnapshot updateActionSnapshot(ActionSnapshot actionSnapshot, Long userID);

	public ActionPartSnapshot updateActionPartSnapshot(ActionPartSnapshot actionPartSnapshot, Long userID);

	public void deleteActionSnapshot(Long actionSnapshotID, Long userID);

	public void deleteActionPartSnapshot(Long actionPartSnapshotID, Long userID);
}
