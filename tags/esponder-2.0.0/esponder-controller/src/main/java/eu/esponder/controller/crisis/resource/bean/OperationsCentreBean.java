package eu.esponder.controller.crisis.resource.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.CrisisService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.dto.model.snapshot.ReferencePOISnapshotDTO;
import eu.esponder.dto.model.snapshot.SketchPOISnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.resource.RegisteredOperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.crisis.view.SketchPOI;
import eu.esponder.model.crisis.view.SketchPOISnapshot;
import eu.esponder.model.snapshot.ReferencePOISnapshot;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;
import eu.esponder.model.user.ESponderUser;

@Stateless
@SuppressWarnings("unchecked")
public class OperationsCentreBean implements OperationsCentreService,
		OperationsCentreRemoteService {

	@EJB
	private CrudService<OperationsCentre> operationsCentreCrudService;

	@EJB
	private CrudService<RegisteredOperationsCentre> registeredOperationsCentreCrudService;

	@EJB
	private CrudService<OperationsCentreSnapshot> operationsCentreSnapshotCrudService;

	@EJB
	private CrudService<ESponderUser> userCrudService;

	@EJB
	private CrudService<ReferencePOI> referencePOICrudService;

	@EJB
	private CrudService<SketchPOI> sketchPOICrudService;

	@EJB
	private ESponderMappingService mappingService;

	@EJB
	private CrisisService crisisService;

	@EJB
	private CrudService<ReferencePOISnapshot> referencePOISnapshotCrudService;

	@EJB
	private CrudService<SketchPOISnapshot> sketchPOISnapshotCrudService;

	// -------------------------------------------------------------------------

	@Override
	public OperationsCentreDTO findOperationCentreByIdRemote(
			Long operationsCentreID) {
		return (OperationsCentreDTO) mappingService.mapESponderEntity(
				findOperationCentreById(operationsCentreID),
				OperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public OperationsCentre findOperationCentreById(Long operationsCentreID) {
		return (OperationsCentre) operationsCentreCrudService.find(
				OperationsCentre.class, operationsCentreID);
	}

	// -------------------------------------------------------------------------

	@Override
	public RegisteredOperationsCentreDTO findRegisteredOperationCentreByIdRemote(
			Long registeredOperationsCentreID) {
		return (RegisteredOperationsCentreDTO) mappingService
				.mapESponderEntity(
						findRegisteredOperationCentreById(registeredOperationsCentreID),
						RegisteredOperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public RegisteredOperationsCentre findRegisteredOperationCentreById(
			Long registeredOperationsCentreID) {
		return (RegisteredOperationsCentre) registeredOperationsCentreCrudService
				.find(RegisteredOperationsCentre.class,
						registeredOperationsCentreID);
	}

	// -------------------------------------------------------------------------

	@Override
	public OperationsCentreDTO findOperationsCentreByTitleRemote(String title) {
		return (OperationsCentreDTO) mappingService.mapESponderEntity(
				findOperationsCentreByTitle(title), OperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public OperationsCentre findOperationsCentreByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (OperationsCentre) operationsCentreCrudService
				.findSingleWithNamedQuery("OperationsCentre.findByTitle",
						params);
	}

	// -------------------------------------------------------------------------

	@Override
	public RegisteredOperationsCentreDTO findRegisteredOperationsCentreByTitleRemote(
			String title) {
		return (RegisteredOperationsCentreDTO) mappingService
				.mapESponderEntity(
						findRegisteredOperationsCentreByTitle(title),
						RegisteredOperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public RegisteredOperationsCentre findRegisteredOperationsCentreByTitle(
			String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", title);
		return (RegisteredOperationsCentre) registeredOperationsCentreCrudService
				.findSingleWithNamedQuery(
						"RegisteredOperationsCentre.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	@Override
	public List<OperationsCentreDTO> findAllOperationsCentresRemote() {
		return (List<OperationsCentreDTO>) mappingService.mapESponderEntity(
				findAllOperationsCentres(), OperationsCentreDTO.class);
	}

	@Override
	public List<OperationsCentre> findAllOperationsCentres() {
		return (List<OperationsCentre>) operationsCentreCrudService
				.findWithNamedQuery("OperationsCentre.findAll");
	}

	// -------------------------------------------------------------------------

	@Override
	public List<OperationsCentreDTO> findAllOperationsCentresByCrisisContextRemote(
			Long crisisContextID) {
		return (List<OperationsCentreDTO>) mappingService.mapESponderEntity(
				findAllOperationsCentres(), OperationsCentreDTO.class);
	}

	@Override
	public List<OperationsCentre> findAllOperationsCentresByCrisisContext(
			Long crisisContextID) {
		CrisisContext cc = crisisService.findCrisisContextById(crisisContextID);
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("crisisContext", cc);
		return (List<OperationsCentre>) operationsCentreCrudService
				.findWithNamedQuery("OperationsCentre.findByCrisisContext",
						params);
	}

	// -------------------------------------------------------------------------

	@Override
	public List<RegisteredOperationsCentreDTO> findAllRegisteredOperationsCentresRemote() {
		return (List<RegisteredOperationsCentreDTO>) mappingService
				.mapESponderEntity(findAllRegisteredOperationsCentres(),
						RegisteredOperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public List<RegisteredOperationsCentre> findAllRegisteredOperationsCentres() {
		return (List<RegisteredOperationsCentre>) registeredOperationsCentreCrudService
				.findWithNamedQuery("RegisteredOperationsCentre.findAll");
	}

	// -------------------------------------------------------------------------

	@Override
	public OperationsCentreDTO findOperationsCentreByUserRemote(
			Long operationsCentreID, Long userID) {
		return (OperationsCentreDTO) mappingService.mapESponderEntity(
				findOperationsCentreByUser(operationsCentreID, userID),
				OperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public OperationsCentre findOperationsCentreByUser(Long operationsCentreID,
			Long userID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		params.put("userID", userID);
		return (OperationsCentre) operationsCentreCrudService
				.findSingleWithNamedQuery("OperationsCentre.findByIdAndUser",
						params);
	}

	// -------------------------------------------------------------------------

	@Override
	public List<OperationsCentreDTO> findUserOperationsCentresRemote(Long userID) {
		List<OperationsCentre> operationsCentres = findUserOperationsCentres(userID);
		if (operationsCentres != null)
			return (List<OperationsCentreDTO>) mappingService
					.mapESponderEntity(operationsCentres,
							OperationsCentreDTO.class);
		else
			return null;
	}

	// -------------------------------------------------------------------------

	@Override
	public List<OperationsCentre> findUserOperationsCentres(Long userID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userID", userID);
		ESponderUser user = userCrudService.find(ESponderUser.class, userID);
		// List<OperationsCentre> operationsCentres = (List<OperationsCentre>)
		// user.getOperationsCentres();
		Set<OperationsCentre> operationsCentresSet = (Set<OperationsCentre>) user
				.getOperationsCentres();
		List<OperationsCentre> operationsCentresList = new ArrayList<OperationsCentre>(
				operationsCentresSet);
		return operationsCentresList;
	}

	// -------------------------------------------------------------------------

	@Override
	public OperationsCentreSnapshotDTO findOperationsCentreSnapshotByDateRemote(
			Long operationsCentreID, Date maxDate) {
		return (OperationsCentreSnapshotDTO) mappingService
				.mapESponderEntity(
						findOperationsCentreSnapshotByDate(operationsCentreID,
								maxDate), OperationsCentreSnapshotDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public OperationsCentreSnapshot findOperationsCentreSnapshotByDate(
			Long operationsCentreID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		params.put("maxDate", maxDate.getTime());
		OperationsCentreSnapshot snapshot = (OperationsCentreSnapshot) operationsCentreSnapshotCrudService
				.findSingleWithNamedQuery(
						"OperationsCentreSnapshot.findByOperationsCentreAndDate",
						params);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OperationsCentreDTO createOperationsCentreRemote(
			OperationsCentreDTO operationsCentreDTO, Long userID) {
		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		return (OperationsCentreDTO) mappingService.mapESponderEntity(
				createOperationsCentre(operationsCentre, userID),
				OperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OperationsCentre createOperationsCentre(
			OperationsCentre operationsCentre, Long userID) {
		operationsCentreCrudService.create(operationsCentre);
		return operationsCentre;
	}

	// -------------------------------------------------------------------------

	@Override
	public RegisteredOperationsCentreDTO createRegisteredOperationsCentreRemote(
			RegisteredOperationsCentreDTO registeredOperationsCentreDTO,
			Long userID) {
		RegisteredOperationsCentre registeredOperationsCentre = (RegisteredOperationsCentre) mappingService
				.mapESponderEntityDTO(registeredOperationsCentreDTO,
						RegisteredOperationsCentre.class);
		return (RegisteredOperationsCentreDTO) mappingService
				.mapESponderEntity(
						createRegisteredOperationsCentre(
								registeredOperationsCentre, userID),
						RegisteredOperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredOperationsCentre createRegisteredOperationsCentre(
			RegisteredOperationsCentre registeredOperationsCentre, Long userID) {
		return registeredOperationsCentreCrudService
				.create(registeredOperationsCentre);

	}

	// -------------------------------------------------------------------------

	@Interceptors(ActionAuditInterceptor.class)
	public OperationsCentreDTO setSupervisor(Long operationsCentreID,
			Long supervisingOperationsCentreID, Long userID) {
		OperationsCentreDTO operationsCentreDTO = null;
		try {
			OperationsCentre operationsCentre = (OperationsCentre) operationsCentreCrudService
					.find(OperationsCentre.class, operationsCentreID);
			OperationsCentre supervisingOperationsCentre = (OperationsCentre) operationsCentreCrudService
					.find(OperationsCentre.class, supervisingOperationsCentreID);
			this.setSupervisor(operationsCentre, supervisingOperationsCentre,
					userID);
			operationsCentreDTO = (OperationsCentreDTO) mappingService
					.mapESponderEntity(operationsCentre, mappingService
							.getDTOEntityClass(operationsCentre.getClass()));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return operationsCentreDTO;
	}

	@Interceptors(ActionAuditInterceptor.class)
	public OperationsCentre setSupervisor(OperationsCentre operationsCentre,
			OperationsCentre supervisingOperationsCentre, Long userID) {
		operationsCentre.setSupervisor(supervisingOperationsCentre);
		return (OperationsCentre) operationsCentreCrudService
				.update(operationsCentre);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteOperationsCentreRemote(Long operationsCentreId,
			Long userID) {
		deleteOperationsCentre(operationsCentreId, userID);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteOperationsCentre(Long operationsCentreId, Long userID) {
		OperationsCentre operationsCentre = findOperationCentreById(operationsCentreId);
		if (operationsCentre != null)
			operationsCentreCrudService.delete(operationsCentre);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteRegisteredOperationsCentreRemote(
			Long registeredOperationsCentreId, Long userID) {
		deleteRegisteredOperationsCentre(registeredOperationsCentreId, userID);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteRegisteredOperationsCentre(
			Long registeredOperationsCentreId, Long userID) {
		RegisteredOperationsCentre registeredOperationsCentre = findRegisteredOperationCentreById(registeredOperationsCentreId);
		if (registeredOperationsCentre != null)
			registeredOperationsCentreCrudService
					.delete(registeredOperationsCentre);
	}

	// -------------------------------------------------------------------------

	@Override
	public OperationsCentreDTO updateOperationsCentreRemote(
			OperationsCentreDTO operationsCentreDTO, Long userID) {
		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		return (OperationsCentreDTO) mappingService.mapESponderEntity(
				updateOperationsCentre(operationsCentre, userID),
				OperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OperationsCentre updateOperationsCentre(
			OperationsCentre operationsCentre, Long userID) {
		return (OperationsCentre) operationsCentreCrudService
				.update(operationsCentre);
	}

	// -------------------------------------------------------------------------

	@Override
	public RegisteredOperationsCentreDTO updateRegisteredOperationsCentreRemote(
			RegisteredOperationsCentreDTO registeredOperationsCentreDTO,
			Long userID) {
		RegisteredOperationsCentre registeredOperationsCentre = (RegisteredOperationsCentre) mappingService
				.mapESponderEntityDTO(registeredOperationsCentreDTO,
						RegisteredOperationsCentre.class);
		return (RegisteredOperationsCentreDTO) mappingService
				.mapESponderEntity(
						updateRegisteredOperationsCentre(
								registeredOperationsCentre, userID),
						RegisteredOperationsCentreDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public RegisteredOperationsCentre updateRegisteredOperationsCentre(
			RegisteredOperationsCentre registeredOperationsCentre, Long userID) {
		RegisteredOperationsCentre regOCPersisted = registeredOperationsCentreCrudService
				.find(RegisteredOperationsCentre.class,
						registeredOperationsCentre.getId());
		mappingService.mapEntityToEntity(registeredOperationsCentre,
				regOCPersisted);
		return (RegisteredOperationsCentre) registeredOperationsCentreCrudService
				.update(regOCPersisted);
	}

	// -------------------------------------------------------------------------

	@Override
	public OperationsCentreSnapshotDTO createOperationsCentreSnapshotRemote(
			OperationsCentreDTO operationsCentreDTO,
			OperationsCentreSnapshotDTO snapshotDTO, Long userID) {

		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		OperationsCentreSnapshot operationsCentreSnapshot = (OperationsCentreSnapshot) mappingService
				.mapESponderEntityDTO(snapshotDTO,
						OperationsCentreSnapshot.class);
		operationsCentreSnapshot = createOperationsCentreSnapshot(
				operationsCentre, operationsCentreSnapshot, userID);
		OperationsCentreSnapshotDTO opCentreSnapshotDTO = (OperationsCentreSnapshotDTO) this.mappingService
				.mapESponderEntity(operationsCentreSnapshot,
						OperationsCentreSnapshotDTO.class);
		return opCentreSnapshotDTO;
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public OperationsCentreSnapshot createOperationsCentreSnapshot(
			OperationsCentre operationsCentre,
			OperationsCentreSnapshot snapshot, Long userID) {
		snapshot.setOperationsCentre(operationsCentre);
		operationsCentreSnapshotCrudService.create(snapshot);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	@Override
	public List<SketchPOIDTO> findSketchPOIRemote(Long operationsCentreID) {
		return (List<SketchPOIDTO>) mappingService.mapESponderEntity(
				findSketchPOI(operationsCentreID), SketchPOIDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public List<SketchPOI> findSketchPOI(Long operationsCentreID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		return sketchPOICrudService.findWithNamedQuery(
				"SketchPOI.findByOperationsCentre", params);
	}

	// -------------------------------------------------------------------------

	@Override
	public SketchPOIDTO findSketchPOIByIdRemote(Long sketchPOIId) {
		return (SketchPOIDTO) mappingService.mapESponderEntity(
				findSketchPOIById(sketchPOIId), SketchPOIDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public SketchPOI findSketchPOIById(Long sketchPOIId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sketchPOIId", sketchPOIId);
		SketchPOI sketch = (SketchPOI) sketchPOICrudService
				.findSingleWithNamedQuery("SketchPOI.findBySketchPOIId", params);
		return sketch;
	}

	// -------------------------------------------------------------------------

	@Override
	public SketchPOIDTO findSketchPOIByTitleRemote(String title) {
		return (SketchPOIDTO) mappingService.mapESponderEntity(
				findSketchPOIByTitle(title), SketchPOIDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public SketchPOI findSketchPOIByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sketchPOITitle", title);
		return (SketchPOI) sketchPOICrudService.findSingleWithNamedQuery(
				"SketchPOI.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SketchPOIDTO createSketchPOIRemote(
			OperationsCentreDTO operationsCentreDTO, SketchPOIDTO sketchDTO,
			Long userID) {
		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		SketchPOI sketchPOI = (SketchPOI) mappingService.mapESponderEntityDTO(
				sketchDTO, SketchPOI.class);
		return (SketchPOIDTO) mappingService.mapESponderEntity(
				createSketchPOI(operationsCentre, sketchPOI, userID),
				SketchPOIDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SketchPOI createSketchPOI(OperationsCentre operationsCentre,
			SketchPOI sketch, Long userID) {
		sketch.setOperationsCentre(operationsCentre);
		sketchPOICrudService.create(sketch);
		return sketch;
	}

	// -------------------------------------------------------------------------

	@Override
	public SketchPOIDTO updateSketchPOIRemote(
			OperationsCentreDTO operationsCentreDTO, SketchPOIDTO sketchDTO,
			Long userID) {
		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		SketchPOI sketchPOI = (SketchPOI) mappingService.mapESponderEntityDTO(
				sketchDTO, SketchPOI.class);
		return (SketchPOIDTO) mappingService.mapESponderEntity(
				updateSketchPOI(operationsCentre, sketchPOI, userID),
				SketchPOIDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SketchPOI updateSketchPOI(OperationsCentre operationsCentre,
			SketchPOI sketch, Long userID) {
		sketch.setOperationsCentre(operationsCentre);
		SketchPOI sketchPersisted = findSketchPOIById(sketch.getId());
		mappingService.mapEntityToEntity(sketch, sketchPersisted);
		sketchPersisted = (SketchPOI) sketchPOICrudService
				.update(sketchPersisted);
		return sketchPersisted;
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteSketchPOIRemote(Long sketchPOIId, Long userID) {
		deleteSketchPOI(sketchPOIId, userID);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteSketchPOI(Long sketchPOIId, Long userID) {
		SketchPOI sketchPOI = findSketchPOIById(sketchPOIId);
		if (sketchPOI != null) {
			sketchPOICrudService.delete(sketchPOI);
		}
	}

	// -------------------------------------------------------------------------

	@Override
	public List<ReferencePOIDTO> findReferencePOIsRemote(Long operationsCentreID) {
		return (List<ReferencePOIDTO>) mappingService.mapESponderEntity(
				findReferencePOIs(operationsCentreID), ReferencePOIDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public List<ReferencePOI> findReferencePOIs(Long operationsCentreID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("operationsCentreID", operationsCentreID);
		return referencePOICrudService.findWithNamedQuery(
				"ReferencePOI.findByOperationsCentre", params);
	}

	// -------------------------------------------------------------------------

	@Override
	public ReferencePOIDTO findReferencePOIByIdRemote(Long referencePOIId) {
		return (ReferencePOIDTO) mappingService.mapESponderEntity(
				findReferencePOIById(referencePOIId), ReferencePOIDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public ReferencePOI findReferencePOIById(Long referencePOIId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("referencePOIId", referencePOIId);
		return (ReferencePOI) referencePOICrudService.findSingleWithNamedQuery(
				"ReferencePOI.findByReferencePOIId", params);
	}

	// -------------------------------------------------------------------------

	@Override
	public ReferencePOIDTO findReferencePOIByTitleRemote(String title) {
		return (ReferencePOIDTO) mappingService.mapESponderEntity(
				findReferencePOIByTitle(title), ReferencePOIDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	public ReferencePOI findReferencePOIByTitle(String title) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("referencePOITitle", title);
		return (ReferencePOI) referencePOICrudService.findSingleWithNamedQuery(
				"ReferencePOI.findByTitle", params);
	}

	// -------------------------------------------------------------------------

	@Override
	public ReferencePOIDTO createReferencePOIRemote(
			OperationsCentreDTO operationsCentreDTO,
			ReferencePOIDTO referencePOIDTO, Long userID) {

		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		ReferencePOI referencePOI = (ReferencePOI) mappingService
				.mapESponderEntityDTO(referencePOIDTO, ReferencePOI.class);
		return (ReferencePOIDTO) mappingService.mapESponderEntity(
				createReferencePOI(operationsCentre, referencePOI, userID),
				ReferencePOIDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReferencePOI createReferencePOI(OperationsCentre operationsCentre,
			ReferencePOI referencePOI, Long userID) {
		referencePOI.setOperationsCentre(operationsCentre);
		referencePOICrudService.create(referencePOI);
		return referencePOI;
	}

	// -------------------------------------------------------------------------

	@Override
	public ReferencePOIDTO updateReferencePOIRemote(
			OperationsCentreDTO operationsCentreDTO,
			ReferencePOIDTO referencePOIDTO, Long userID) {
		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		ReferencePOI referencePOI = (ReferencePOI) mappingService
				.mapESponderEntityDTO(referencePOIDTO, ReferencePOI.class);
		return (ReferencePOIDTO) mappingService.mapESponderEntity(
				updateReferencePOI(operationsCentre, referencePOI, userID),
				ReferencePOIDTO.class);
	}

	// -------------------------------------------------------------------------

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReferencePOI updateReferencePOI(OperationsCentre operationsCentre,
			ReferencePOI referencePOI, Long userID) {
		referencePOI.setOperationsCentre(operationsCentre);
		ReferencePOI referencePOIPersisted = findReferencePOIById(referencePOI
				.getId());
		mappingService.mapEntityToEntity(referencePOI, referencePOIPersisted);
		referencePOIPersisted = (ReferencePOI) referencePOICrudService
				.update(referencePOIPersisted);
		return referencePOIPersisted;
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteReferencePOIRemote(Long sketchPOIId, Long userID) {
		deleteReferencePOI(sketchPOIId, userID);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteReferencePOI(Long sketchPOIId, Long userID) {
		ReferencePOI referencePOI = findReferencePOIById(sketchPOIId);
		if (referencePOI != null) {
			referencePOICrudService.delete(referencePOI);
		}
	}

	// -------------------------------------------------------------------------

	@Override
	public OperationsCentreSnapshotDTO updateOperationsCentreSnapshotDTO(
			OperationsCentreDTO operationsCentreDTO,
			OperationsCentreSnapshotDTO snapshotDTO, Long userID) {
		OperationsCentre operationsCentre = (OperationsCentre) mappingService
				.mapESponderEntityDTO(operationsCentreDTO,
						OperationsCentre.class);
		OperationsCentreSnapshot operationsCentreSnapshot = (OperationsCentreSnapshot) mappingService
				.mapESponderEntityDTO(snapshotDTO,
						OperationsCentreSnapshot.class);
		operationsCentreSnapshot = updateOperationsCentreSnapshot(
				operationsCentre, operationsCentreSnapshot, userID);
		snapshotDTO = (OperationsCentreSnapshotDTO) mappingService
				.mapESponderEntity(operationsCentreSnapshot,
						OperationsCentreSnapshotDTO.class);
		return snapshotDTO;
	}

	@Override
	public OperationsCentreSnapshot updateOperationsCentreSnapshot(
			OperationsCentre operationsCentre,
			OperationsCentreSnapshot snapshot, Long userID) {
		OperationsCentre operationsCentrePersisted = findOperationCentreById(operationsCentre
				.getId());
		if (operationsCentrePersisted != null) {
			snapshot.setOperationsCentre(operationsCentrePersisted);
			snapshot = operationsCentreSnapshotCrudService.update(snapshot);
			return snapshot;
		}
		return null;
	}

	@Override
	public void deleteOperationsCentreSnapshotRemote(
			Long operationsCentreSnapshotDTOID) {
		deleteOperationsCentreSnapshot(operationsCentreSnapshotDTOID);
	}

	@Override
	public void deleteOperationsCentreSnapshot(Long operationsCentreSnapshotID) {
		operationsCentreSnapshotCrudService.delete(
				OperationsCentreSnapshot.class, operationsCentreSnapshotID);

	}

	// /////////////////////sketch
	// snapshots////////////////////////////////////////////////////////////////
	// -------------------------------------------------------------------------

	@Override
	public SketchPOISnapshotDTO findSketchPOISnapshotByIdRemote(Long sensorID) {
		SketchPOISnapshot sensorSnapshot = findSketchPOISnapshotById(sensorID);
		if (sensorSnapshot != null) {
			return (SketchPOISnapshotDTO) mappingService.mapESponderEntity(
					sensorSnapshot, SketchPOISnapshotDTO.class);
		}
		return null;
	}

	@Override
	public SketchPOISnapshot findSketchPOISnapshotById(Long sensorID) {
		return (SketchPOISnapshot) sketchPOISnapshotCrudService.find(
				SketchPOISnapshot.class, sensorID);
	}

	// -------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	@Override
	public List<SketchPOISnapshotDTO> findAllSketchPOISnapshotsRemote() {
		return (List<SketchPOISnapshotDTO>) mappingService.mapESponderEntity(
				findAllSketchPOISnapshots(), SketchPOISnapshotDTO.class);
	}

	@Override
	public List<SketchPOISnapshot> findAllSketchPOISnapshots() {
		return (List<SketchPOISnapshot>) sketchPOISnapshotCrudService
				.findWithNamedQuery("SketchPOISnapshot.findAll");
	}

	// -------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	@Override
	public List<SketchPOISnapshotDTO> findAllSketchPOISnapshotsByPoiRemote(
			Long SketchPOIID , int resultLimit) {
		SketchPOI sensor;
		
			
			return (List<SketchPOISnapshotDTO>) mappingService
					.mapESponderEntity(
							findAllSketchPOISnapshotsByPoi(SketchPOIID, resultLimit),
							SketchPOISnapshotDTO.class);
		

	}

	@Override
	public List<SketchPOISnapshot> findAllSketchPOISnapshotsByPoi(
			Long SketchPOIID, int resultLimit) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("sketchsnapshotPOIId", SketchPOIID);
		return (List<SketchPOISnapshot>) sketchPOISnapshotCrudService
				.findWithNamedQuery("SketchPOISnapshot.findBySketchPOIId",
						params, resultLimit);
	}

	// --------------------------------------------------------------------------
	@Override
	public SketchPOISnapshotDTO findSketchPOISnapshotByDateRemote(
			Long sensorID, Date dateTo) {
		SketchPOISnapshot sensorSnapshot = findSketchPOISnapshotByDate(
				sensorID, dateTo);
		if (sensorSnapshot != null) {
			return (SketchPOISnapshotDTO) mappingService.mapESponderEntity(
					sensorSnapshot, SketchPOISnapshotDTO.class);
		}
		return null;

	}

	@Override
	public SketchPOISnapshot findSketchPOISnapshotByDate(Long sensorID,
			Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sketchID", sensorID);
		params.put("maxDate", maxDate.getTime());
		SketchPOISnapshot snapshot = (SketchPOISnapshot) sketchPOISnapshotCrudService
				.findSingleWithNamedQuery(
						"SketchPOISnapshot.findBySketchPOIAndDate", params);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	@Override
	public SketchPOISnapshotDTO findPreviousSketchPOISnapshotRemote(
			Long sensorID) {
		SketchPOISnapshot sensorSnapshot = findPreviousSketchPOISnapshot(sensorID);
		if (sensorSnapshot != null) {
			return (SketchPOISnapshotDTO) mappingService.mapESponderEntity(
					sensorSnapshot, SketchPOISnapshotDTO.class);
		}
		return null;
	}

	@Override
	public SketchPOISnapshot findPreviousSketchPOISnapshot(Long sensorID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sketchID", sensorID);
		SketchPOISnapshot snapshot = (SketchPOISnapshot) sketchPOISnapshotCrudService
				.findSingleWithNamedQuery(
						"SketchPOISnapshot.findPreviousSnapshot", params);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	@Override
	public SketchPOISnapshotDTO createSketchPOISnapshotRemote(
			SketchPOISnapshotDTO snapshotDTO, Long userID) {
		try {
			SketchPOISnapshot snapshot = (SketchPOISnapshot) mappingService
					.mapESponderEntityDTO(snapshotDTO, mappingService
							.getManagedEntityClass(snapshotDTO.getClass()));
			SketchPOISnapshot snapshotPersisted = this.createSketchPOISnapshot(
					snapshot, userID);
			// SensorSnapshot snapshotPersisted =
			// this.placeSensorSnapshot(snapshot, userID);
			snapshotDTO = (SketchPOISnapshotDTO) mappingService
					.mapESponderEntity(snapshotPersisted, mappingService
							.getDTOEntityClass(snapshot.getClass()));
			return snapshotDTO;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SketchPOISnapshot createSketchPOISnapshot(
			SketchPOISnapshot snapshot, Long userID) {

		snapshot = (SketchPOISnapshot) sketchPOISnapshotCrudService
				.create(snapshot);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	@Override
	public SketchPOISnapshotDTO updateSketchPOISnapshotRemote(
			SketchPOISnapshotDTO sensorSnapshotDTO, Long userID) {
		SketchPOISnapshot sensorSnapshot = (SketchPOISnapshot) mappingService
				.mapESponderEntityDTO(sensorSnapshotDTO,
						SketchPOISnapshot.class);
		sensorSnapshot = updateSketchPOISnapshot(sensorSnapshot, userID);
		return (SketchPOISnapshotDTO) mappingService.mapESponderEntity(
				sensorSnapshot, SketchPOISnapshotDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public SketchPOISnapshot updateSketchPOISnapshot(
			SketchPOISnapshot snapshot, Long userID) {
		return (SketchPOISnapshot) sketchPOISnapshotCrudService
				.update(snapshot);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteSketchPOISnapshotRemote(Long sensorSnapshotId, Long userID) {
		deleteSketchPOISnapshot(sensorSnapshotId, userID);

	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteSketchPOISnapshot(Long sensorSnapshotId, Long userID) {
		SketchPOISnapshot sensorSnapshot = findSketchPOISnapshotById(sensorSnapshotId);
		if (sensorSnapshot != null) {
			sketchPOISnapshotCrudService.delete(SketchPOISnapshot.class,
					sensorSnapshotId);
		} else
			throw new RuntimeException();
	}

	// -------------------------------------------------------------------------

	// /////////////////////////////referencePOI///////////////////////////////////
	// -------------------------------------------------------------------------

	@Override
	public ReferencePOISnapshotDTO findReferencePOISnapshotByIdRemote(
			Long referencePOIID) {
		ReferencePOISnapshot referencePOISnapshot = findReferencePOISnapshotById(referencePOIID);
		if (referencePOISnapshot != null) {
			return (ReferencePOISnapshotDTO) mappingService.mapESponderEntity(
					referencePOISnapshot, ReferencePOISnapshotDTO.class);
		}
		return null;
	}

	@Override
	public ReferencePOISnapshot findReferencePOISnapshotById(Long referencePOIID) {
		return (ReferencePOISnapshot) referencePOISnapshotCrudService.find(
				ReferencePOISnapshot.class, referencePOIID);
	}

	// -------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	@Override
	public List<ReferencePOISnapshotDTO> findAllReferencePOISnapshotsRemote() {
		return (List<ReferencePOISnapshotDTO>) mappingService
				.mapESponderEntity(findAllReferencePOISnapshots(),
						ReferencePOISnapshotDTO.class);
	}

	@Override
	public List<ReferencePOISnapshot> findAllReferencePOISnapshots() {
		return (List<ReferencePOISnapshot>) referencePOISnapshotCrudService
				.findWithNamedQuery("ReferencePOISnapshot.findAll");
	}

	// -------------------------------------------------------------------------

	@SuppressWarnings("unchecked")
	@Override
	public List<ReferencePOISnapshotDTO> findAllReferencePOISnapshotsByReferencePOIRemote(
			Long referencePOIDTO, int resultLimit)
			throws ClassNotFoundException {
		//ReferencePOI referencePOI = (ReferencePOI) mappingService
			//	.mapESponderEntityDTO(referencePOIDTO, mappingService
				//		.getManagedEntityClass(referencePOIDTO.getClass()));
		return (List<ReferencePOISnapshotDTO>) mappingService
				.mapESponderEntity(
						findAllReferencePOISnapshotsByReferencePOI(
								referencePOIDTO, resultLimit),
						ReferencePOISnapshotDTO.class);
	}

	@Override
	public List<ReferencePOISnapshot> findAllReferencePOISnapshotsByReferencePOI(
			Long referencePOI, int resultLimit) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("ReferencePOIId", referencePOI);
		return (List<ReferencePOISnapshot>) referencePOISnapshotCrudService
				.findWithNamedQuery(
						"ReferencePOISnapshot.findByReferencePOIId", params,
						resultLimit);
	}

	// --------------------------------------------------------------------------
	@Override
	public ReferencePOISnapshotDTO findReferencePOISnapshotByDateRemote(
			Long referencePOIID, Date dateTo) {
		ReferencePOISnapshot referencePOISnapshot = findReferencePOISnapshotByDate(
				referencePOIID, dateTo);
		if (referencePOISnapshot != null) {
			return (ReferencePOISnapshotDTO) mappingService.mapESponderEntity(
					referencePOISnapshot, ReferencePOISnapshotDTO.class);
		}
		return null;

	}

	@Override
	public ReferencePOISnapshot findReferencePOISnapshotByDate(
			Long referencePOIID, Date maxDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("referenceID", referencePOIID);
		params.put("maxDate", maxDate.getTime());
		ReferencePOISnapshot snapshot = (ReferencePOISnapshot) referencePOISnapshotCrudService
				.findSingleWithNamedQuery(
						"ReferencePOISnapshot.findByReferenceAndDate",
						params);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	@Override
	public ReferencePOISnapshotDTO findPreviousReferencePOISnapshotRemote(
			Long referencePOIID) {
		ReferencePOISnapshot referencePOISnapshot = findPreviousReferencePOISnapshot(referencePOIID);
		if (referencePOISnapshot != null) {
			return (ReferencePOISnapshotDTO) mappingService.mapESponderEntity(
					referencePOISnapshot, ReferencePOISnapshotDTO.class);
		}
		return null;
	}

	@Override
	public ReferencePOISnapshot findPreviousReferencePOISnapshot(
			Long referencePOIID) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("referenceID", referencePOIID);
		ReferencePOISnapshot snapshot = (ReferencePOISnapshot) referencePOISnapshotCrudService
				.findSingleWithNamedQuery(
						"ReferencePOISnapshot.findPreviousSnapshot", params);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	@Override
	public ReferencePOISnapshotDTO createReferencePOISnapshotRemote(
			ReferencePOISnapshotDTO snapshotDTO, Long userID) {
		try {
			ReferencePOISnapshot snapshot = (ReferencePOISnapshot) mappingService
					.mapESponderEntityDTO(snapshotDTO, mappingService
							.getManagedEntityClass(snapshotDTO.getClass()));
			ReferencePOISnapshot snapshotPersisted = this
					.createReferencePOISnapshot(snapshot, userID);
			// ReferencePOISnapshot snapshotPersisted =
			// this.placeReferencePOISnapshot(snapshot, userID);
			snapshotDTO = (ReferencePOISnapshotDTO) mappingService
					.mapESponderEntity(snapshotPersisted, mappingService
							.getDTOEntityClass(snapshot.getClass()));
			return snapshotDTO;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReferencePOISnapshot createReferencePOISnapshot(
			ReferencePOISnapshot snapshot, Long userID) {
		// snapshot.setReferencePOI((ReferencePOISnapshot)this.findReferencePOIById(snapshot.getReferencePOI().getId()));
		snapshot = (ReferencePOISnapshot) referencePOISnapshotCrudService
				.create(snapshot);
		return snapshot;
	}

	// -------------------------------------------------------------------------

	@Override
	public ReferencePOISnapshotDTO updateReferencePOISnapshotRemote(
			ReferencePOISnapshotDTO referencePOISnapshotDTO, Long userID) {
		ReferencePOISnapshot referencePOISnapshot = (ReferencePOISnapshot) mappingService
				.mapESponderEntityDTO(referencePOISnapshotDTO,
						ReferencePOISnapshot.class);
		referencePOISnapshot = updateReferencePOISnapshot(referencePOISnapshot,
				userID);
		return (ReferencePOISnapshotDTO) mappingService.mapESponderEntity(
				referencePOISnapshot, ReferencePOISnapshotDTO.class);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ReferencePOISnapshot updateReferencePOISnapshot(
			ReferencePOISnapshot snapshot, Long userID) {
		return (ReferencePOISnapshot) referencePOISnapshotCrudService
				.update(snapshot);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteReferencePOISnapshotRemote(Long referencePOISnapshotId,
			Long userID) {
		deleteReferencePOISnapshot(referencePOISnapshotId, userID);

	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void deleteReferencePOISnapshot(Long referencePOISnapshotId,
			Long userID) {
		ReferencePOISnapshot referencePOISnapshot = findReferencePOISnapshotById(referencePOISnapshotId);
		if (referencePOISnapshot != null) {
			referencePOISnapshotCrudService.delete(ReferencePOISnapshot.class,
					referencePOISnapshotId);
		} else
			throw new RuntimeException();
	}

	// -------------------------------------------------------------------------

}
