package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.util.ejb.ServiceLocator;

public class ActorSupervisorFieldConverter implements CustomConverter {
	
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected ActorService getActorService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if (sourceClass == Actor.class && source != null) {
			Actor sourceActor = (Actor) source;
			ActorDTO destActorDTO = new ActorDTO();
			destActorDTO.setId(sourceActor.getId());
			destination = destActorDTO;
		}
		else if(sourceClass == ActorDTO.class && source!=null) { 
			ActorDTO supervisor = (ActorDTO) source;
			Actor destSupervisor = this.getActorService().findById(supervisor.getId());
//			Actor destActor = (Actor) this.getMappingService().mapESponderEntityDTO((ESponderEntityDTO) destSupervisor, (Class<? extends ESponderEntity<Long>>) destinationClass );
			destination = destSupervisor;
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}
