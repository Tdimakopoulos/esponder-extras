package eu.esponder.controller.crisis.resource;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.RegisteredConsumableResource;
import eu.esponder.model.crisis.resource.RegisteredReusableResource;
import eu.esponder.model.crisis.resource.ReusableResource;

@Local
public interface LogisticsService extends LogisticsRemoteService {
	
	public ReusableResource findReusableResourceById(Long reusableID);

	public ReusableResource findReusableResourceByTitle(String title);
	
	public ReusableResource createReusableResource(ReusableResource reusableResource,Long userID);
	
	public ReusableResource updateReusableResource(ReusableResource reusableResource, Long userID);
	
	public void deleteReusableResource(Long reusableResourceID, Long userID);
	
	public ConsumableResource findConsumableResourceById(Long consumableID);

	public ConsumableResource findConsumableResourceByTitle(String title);

	public ConsumableResource createConsumableResource(ConsumableResource consumableResource, Long userID);

	public ConsumableResource updateConsumableResource(ConsumableResource consumableResource, Long userID);
	
	public void deleteConsumableResource(Long consumableResourceID, Long userID);

	public RegisteredReusableResource findRegisteredReusableResourceById(
			Long registeredReusableID);

	public RegisteredConsumableResource findRegisteredConsumableResourceById(Long registeredConsumableID);

	public RegisteredReusableResource findRegisteredReusableResourceByTitle(String title);

	public RegisteredConsumableResource findRegisteredConsumableResourceByTitle(String title);

	public RegisteredConsumableResource createRegisteredConsumableResource(
			RegisteredConsumableResource registeredConsumableResource, Long userID);

	public RegisteredReusableResource createRegisteredReusableResource(
			RegisteredReusableResource registeredReusableResource, Long userID);

	public void deleteRegisteredConsumableResource(Long registeredConsumableResourceID, Long userID);

	public void deleteRegisteredReusableResource(Long registeredReusableResourceID, Long userID);

	public RegisteredReusableResource updateRegisteredReusableResource(
			RegisteredReusableResource registeredReusableResource, Long userID);

	public RegisteredConsumableResource updateRegisteredConsumableResource(
			RegisteredConsumableResource registeredConsumableResource, Long userID);

	public List<ReusableResource> findAllReusableResources();

	public List<ConsumableResource> findAllConsumableResources();

	public List<RegisteredReusableResource> findAllRegisteredReusableResources();

	public List<RegisteredConsumableResource> findAllRegisteredConsumableResources();
	
}
