package eu.esponder.controller.crisis;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.resource.plan.CrisisResourcePlan;
import eu.esponder.model.crisis.resource.plan.PlannableResourcePower;
import eu.esponder.model.snapshot.CrisisContextSnapshot;

@Local
public interface CrisisService {

	public PlannableResourcePower findPlannableResourcePowerById(Long powerID);

	public PlannableResourcePower createPlannableResourcePower(
			PlannableResourcePower plannableResourcePower, Long userID);

	public void deletePlannableResourcePower(Long powerID, Long userID);

	public PlannableResourcePower updatePlannableResourcePower(
			PlannableResourcePower plannableResourcePower, Long userID);

	public CrisisResourcePlan findCrisisResourcePlanById(Long crisisResourcePlanId,
			Long userId);

	public CrisisResourcePlan findCrisisResourcePlanByTitle(
			String crisisResourcePlanTitle, Long userId);

	public List<CrisisResourcePlan> findAllCrisisResourcePlans();

	public CrisisResourcePlan createCrisisResourcePlan(
			CrisisResourcePlan crisisResourcePlan, Long userID);

	public void deleteCrisisResourcePlan(Long crisisResourcePlanId, Long userID);

	public CrisisResourcePlan updateCrisisResourcePlan(
			CrisisResourcePlan crisisResourcePlan, Long userID);

	public CrisisContext findCrisisContextById(Long crisisContextID);

	public List<CrisisContext> findAllCrisisContexts();

	public CrisisContext findCrisisContextByTitle(String title);

	public CrisisContext createCrisisContext(CrisisContext crisisContext, Long userID);

	public void deleteCrisisContext(Long crisisContextId, Long userID);

	public CrisisContext updateCrisisContext(CrisisContext crisisContext, Long userID);

	public CrisisContextSnapshot findCrisisContextSnapshotById(Long crisisContextSnapshotID);

	public CrisisContextSnapshot createCrisisContextSnapshot(
			CrisisContextSnapshot crisisContextSnapshot, Long userID);

	public void deleteCrisisContextSnapshot(Long crisisContextSnapshotId, Long userID);

	public CrisisContextSnapshot updateCrisisContextSnapshot(
			CrisisContextSnapshot crisisContextSnapshot, Long userID);
	
}
