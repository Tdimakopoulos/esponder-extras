package eu.esponder.df.ruleengine.utilities.events;

import java.io.Serializable;
import java.util.Date;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.df.ruleengine.utilities.ruleresults.RuleResults;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.CreateSensorSnapshotEvent;
import eu.esponder.event.model.snapshot.resource.UpdateActorSnapshotEvent;
import eu.esponder.eventadmin.ESponderEventPublisher;


public class RuleEvents implements Serializable {


	private static final long serialVersionUID = -5651145416673918122L;

	//FIXME 
	// Add rule events functions into the RuleEvents Class
	public void PublishEvent(ESponderEvent<ESponderEntityDTO> event)
	{
		

	}

	//Person mark = new Person();
    //mark.setName("Mark");

	public void publishUpdateSensorSnapshotEvent(SensorSnapshotDTO sensorSnapshot,String szmessage) {
		CreateSensorSnapshotEvent event = new CreateSensorSnapshotEvent();
		event.setEventAttachment(sensorSnapshot);
		event.setEventSeverity(SeverityLevelDTO.MEDIUM);
		event.setEventSource(sensorSnapshot.getSensor());
		event.setEventTimestamp(new Date());
		event.setJournalMessage(szmessage);
		System.out.println("DF : Publish Datafusion Event (SensorSnapshotUpdate)");
		RuleResults pResults = new RuleResults();
		pResults.RuleResult("Sensor Snapshot Rule Set", szmessage, sensorSnapshot.getValue(), sensorSnapshot.getSensor().getTitle());
		ESponderEventPublisher<CreateSensorSnapshotEvent> publisher = new ESponderEventPublisher<CreateSensorSnapshotEvent>(CreateSensorSnapshotEvent.class);
		try {
			publisher.publishEvent(event);
		} catch (EventListenerException e) {
			System.out.println("Error on EventAdmin on Publish SensorSnapshot Event - Event Type : UPDATE. Reason : "+e.getMessage());
		}
		publisher.CloseConnection();

	}

	public void publishUpdateActorSnasphotEvent(ActorSnapshotDTO actorSnapshot,String szmessage) {
		UpdateActorSnapshotEvent event = new UpdateActorSnapshotEvent();
		event.setEventAttachment(actorSnapshot);
		event.setEventSeverity(SeverityLevelDTO.MEDIUM);
		event.setEventSource(actorSnapshot.getActor());
		event.setEventTimestamp(new Date());
		event.setJournalMessage(szmessage);
		System.out.println("DF : Publish Datafusion Event (ActorSnapshotUpdate)");
		RuleResults pResults = new RuleResults();
		pResults.RuleResult("Actor Snapshot Rule Set", szmessage, actorSnapshot.getActor().getTitle(), actorSnapshot.getLocationArea().toString());
		ESponderEventPublisher<UpdateActorSnapshotEvent> publisher = new ESponderEventPublisher<UpdateActorSnapshotEvent>(UpdateActorSnapshotEvent.class);
		try {
			publisher.publishEvent(event);
		} catch (EventListenerException e) {
			System.out.println("Error on EventAdmin on Publish ActorSnapshot Event - Event Type : UPDATE. Reason : "+e.getMessage());
		}
		publisher.CloseConnection();
		
	}
}
