package eu.esponder.df.eventhandler.bean;

import javax.ejb.Stateless;

import eu.esponder.df.actionmanager.location.ActionLocationManager;
import eu.esponder.df.eventhandler.ActionEventHandlerRemoteService;
import eu.esponder.df.eventhandler.ActionEventHandlerService;
import eu.esponder.df.rules.profile.ProfileManager;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.ESponderEvent;
@Stateless
public class ActionEventHandlerBean extends dfEventHandlerBean
		implements ActionEventHandlerService,
		ActionEventHandlerRemoteService {

	public void ProcessEvent(ESponderEvent<?> pEvent)
	{
//		ProfileManager pProfileManager=new ProfileManager();
//		
//		
//		//Set rule profile for action event
//		SetRuleEngineType(pProfileManager.GetProfileNameForAction().getSzProfileType(),pProfileManager.GetProfileNameForAction().getSzProfileName());
//
//		//Load Rules
//		LoadKnowledge();
//		
//		//Add Measurements object list
//		//AddDTOObjects(pSensorSnapshot);
//
//		//Run Rules
//		ProcessRules();
		//new create action event
		// - If move then get actors
		//    - For each actor save title and new location
		
		ActionDTO pobject= (ActionDTO)pEvent.getEventAttachment();
		if ((pobject.getActionOperation()==ActionOperationEnumDTO.MOVE) ||(pobject.getActionOperation()==ActionOperationEnumDTO.TRANSPORT))
		{
			ActionPartDTO[] paparts=pobject.getActionParts().toArray(new ActionPartDTO[pobject.getActionParts().size()]);
			for (int i=0;i<paparts.length;i++)
			{
				ActorDTO pactor=paparts[i].getActor();
				ActionPartObjectiveDTO[] pobjectives=paparts[i].getActionPartObjectives().toArray(new ActionPartObjectiveDTO[paparts[i].getActionPartObjectives().size()]);
				for (int d=0;d<pobjectives.length;d++)
				{
					
					SphereDTO plocation=(SphereDTO)pobjectives[i].getLocationArea();
					ActionLocationManager pLocManager=new ActionLocationManager();
					System.out.println("Action event received : "+plocation.getCentre().getLongitude().doubleValue()+ plocation.getCentre().getLatitude().doubleValue()+
							plocation.getCentre().getAltitude().doubleValue()+ plocation.getRadius().doubleValue()+ pactor.getTitle());
					pLocManager.ActionMovementRequest(plocation.getCentre().getLongitude().doubleValue(), plocation.getCentre().getLatitude().doubleValue(), plocation.getCentre().getAltitude().doubleValue(), plocation.getRadius().doubleValue(), pactor.getTitle());
				}

			}
		}
	}
}
