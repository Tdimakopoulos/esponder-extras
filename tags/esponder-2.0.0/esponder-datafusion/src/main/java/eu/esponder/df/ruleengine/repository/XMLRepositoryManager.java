package eu.esponder.df.ruleengine.repository;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import eu.esponder.df.ruleengine.settings.DroolSettings;

public class XMLRepositoryManager {

	DroolSettings dSettings = new DroolSettings();
	
	public void SaveCategory(String szCategory,String XML)
	{
		SaveXML(dSettings.getSzTmpRepositoryDirectoryPath()+szCategory+".xml",XML);
	}
	
	public void SavePackage(String szPackage,String XML)
	{
		SaveXML(dSettings.getSzTmpRepositoryDirectoryPath()+szPackage+".xml",XML);
	}
	
	public void SaveAsset(String szAsset,String szPAsset)
	{
		SaveXML(dSettings.getSzTmpRepositoryDirectoryPath()+szAsset+".ast",szPAsset);
	}
	
	public String LoadPackage(String szPackage)
	{
		return LoadXML(dSettings.getSzTmpRepositoryDirectoryPath()+szPackage+".xml");
	}
	
	public String LoadAsset(String szAsset)
	{
		return LoadXML(dSettings.getSzTmpRepositoryDirectoryPath()+szAsset+".ast");
	}
	
	public String LoadCategory(String szCategory)
	{
		return LoadXML(dSettings.getSzTmpRepositoryDirectoryPath()+szCategory+".xml");
	}
	
	private void SaveXML(String szFilename,String szContents)
	{
		//System.out.println("Save XML Filename : " + szFilename+" Content : "+ szContents);
		try {
		    BufferedWriter out = new BufferedWriter(new FileWriter(szFilename));
		    out.write(szContents);
		    out.close();
		} catch (IOException e) {
		}
	}
	
	private String LoadXML(String szFilename)
	{
		BufferedReader reader = null;
		try {
			reader = new BufferedReader( new FileReader (szFilename));
		} catch (FileNotFoundException e) {
			System.out.println("Error on read XML Stream: error 10001");
		}
	    String         line = null;
	    StringBuilder  stringBuilder = new StringBuilder();
	    String         ls = System.getProperty("line.separator");

	    try {
			while( ( line = reader.readLine() ) != null ) {
			    stringBuilder.append( line );
			    stringBuilder.append( ls );
			}
		} catch (IOException e) {
			System.out.println("Error on read XML Stream : error 10002");
			e.printStackTrace();
		}

	    //System.out.println("SzReturn : "+stringBuilder.toString());
	    String szReturn=stringBuilder.toString();
	    try {
			reader.close();
		} catch (IOException e) {
			System.out.println("Error on Close XML Stream : error 10003");
			e.printStackTrace();
		}
	    //System.out.println("SzReturn : "+szReturn);
	    return szReturn;
	}
}
