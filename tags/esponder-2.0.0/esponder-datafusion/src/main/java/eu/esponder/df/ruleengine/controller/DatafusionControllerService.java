/**
 * Controller
 * 
 * This Java class implement a controller for the rules engine, this is the class that must be called to run 
 * the rule engine, support DRL, DSL and DSLR type of assets. And Functions but need to be inline the rule.
 * 
 * 
 * @Project   esponder
 * @package   Datafusion
 */
package eu.esponder.df.ruleengine.controller;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;
import eu.esponder.event.model.ESponderEvent;


@Local
public interface DatafusionControllerService {

	public void EsponderEventReceivedHandler(ESponderEvent<?> pEvent);
	public void ReinitializeRepository();
	public void DeleteRepository();
	public void SetToLive();
	public void SetToLocal();
	public List<RuleResultsXML> GetRuleResults() throws Exception;
}

