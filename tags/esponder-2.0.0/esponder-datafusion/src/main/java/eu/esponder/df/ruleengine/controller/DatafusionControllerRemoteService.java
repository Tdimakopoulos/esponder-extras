package eu.esponder.df.ruleengine.controller;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;
import eu.esponder.event.model.ESponderEvent;



@Remote
public interface DatafusionControllerRemoteService extends DatafusionControllerService {

	public void EsponderEventReceivedHandler(ESponderEvent<?> pEvent);
	public void ReinitializeRepository();
	public void DeleteRepository();
	public void SetToLive();
	public void SetToLocal();
	public List<RuleResultsXML> GetRuleResults() throws Exception;
}

