package eu.esponder.df.ruleengine.core;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.codec.EncoderException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class RestGetClient {

	private String XML_REQUEST_TYPE_HEADER = "application/xml";
	private String JSON_REQUEST_TYPE_HEADER = "application/json";
	//private String TEXT_REQUEST_TYPE_HEADER = "application/plaintext";
	private String TEXT_REQUEST_TYPE_HEADER = "text/plain";
	private String resourceURI = "";

	public RestGetClient(String resourceURI) {
		//System.out.println(resourceURI);
		this.resourceURI = resourceURI;
	}

	public String getXML() {
		return get(XML_REQUEST_TYPE_HEADER);
	}
	
	public String getText() {
		return get(TEXT_REQUEST_TYPE_HEADER);
	}

	public String getJSON() {
		return get(JSON_REQUEST_TYPE_HEADER);
	}

	public String get(String serviceType) {
		String output = "";
		String outputResponse = "";
		DefaultHttpClient httpClient = null;
		try {

			httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(resourceURI);
			
			//String szAuthorization = "guest" + ":" + "";
			//org.apache.commons.codec.binary.Base32 Base64Encoder = new org.apache.commons.codec.binary.Base32();
			
			String encoding = null;
			
				String ss="guest" + ":" + "guest";
				encoding =  org.apache.commons.codec.binary.Base64.encodeBase64(ss.getBytes()).toString();
			
			//System.out.println("-->>> Authorization"+"Basic "+encoding);
			
			/////////////////////////////////////////////////////////////////////////////////////////////
			//FIXME : DANGER 
			//DANGER ONLY WORKING WITH GUEST> NEED TO FIND DIFFERENCE BETWEEN CXF AND APACHE ENCODING
			getRequest.addHeader("Authorization","Basic Z3Vlc3Q6");
			////////////////////////////////////////////////////////////////////////////////////////////
			
			if (serviceType.equalsIgnoreCase(TEXT_REQUEST_TYPE_HEADER)){}else{
			getRequest.addHeader("accept", serviceType);
			}
			//System.out.println("URL - "+getRequest);
			HttpResponse response = httpClient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));

			//System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				//System.out.println(output);
				outputResponse += output+"\n"; 
			}
			//System.out.println("Output from Server FINISHED");


		} catch (ClientProtocolException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			if (httpClient != null) {
				httpClient.getConnectionManager().shutdown();
			}
		}
		return outputResponse;
	}

}
