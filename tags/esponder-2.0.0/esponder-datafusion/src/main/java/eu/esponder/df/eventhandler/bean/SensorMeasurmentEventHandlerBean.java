package eu.esponder.df.eventhandler.bean;

import java.util.List;

import javax.ejb.Stateless;

import eu.esponder.df.eventhandler.SensorMeasurmentEventHandlerRemoteService;
import eu.esponder.df.eventhandler.SensorMeasurmentEventHandlerService;
import eu.esponder.df.rules.profile.ProfileManager;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.event.model.ESponderEvent;

@Stateless
public class SensorMeasurmentEventHandlerBean extends dfEventHandlerBean
		implements SensorMeasurmentEventHandlerService,
		SensorMeasurmentEventHandlerRemoteService {

	public void ProcessEvent(ESponderEvent<?> pEvent)
	{
		System.out.println("******* Sensor Snapshot Event Handler ***********");
		ProfileManager pProfileManager=new ProfileManager();
		
		CreateSensorSnapshot(pEvent);
		
		//Arithmetic sensors snapshots
		for (int i=0;i<createdSensorSnapshots.size();i++)
		{
			//Set rule profile for sensor measurement
			SetRuleEngineType(pProfileManager.GetProfileNameForSensor().getSzProfileType(),pProfileManager.GetProfileNameForSensor().getSzProfileName());
			//Load Rules
			LoadKnowledge();
			//Get current snapshot
			SensorSnapshotDTO createdSensorSnapshot = new SensorSnapshotDTO();
			createdSensorSnapshot.setId(createdSensorSnapshots.get(i).getId());
			createdSensorSnapshot.setPeriod(createdSensorSnapshots.get(i).getPeriod());
			createdSensorSnapshot.setSensor(createdSensorSnapshots.get(i).getSensor());
			createdSensorSnapshot.setStatisticType(createdSensorSnapshots.get(i).getStatisticType());
			createdSensorSnapshot.setStatus(createdSensorSnapshots.get(i).getStatus());
			createdSensorSnapshot.setValue(createdSensorSnapshots.get(i).getValue());
			//Add current snapshot
			AddDTOObjects(createdSensorSnapshot);
			//Run Rules, this function automatically load the utility classes
			ProcessRules();
			//Empty the session
			ReinitializeEngine();
		}
		
		//Location Sensors snapshots
		for (int i=0;i<createdActorSnapshots.size();i++)
		{
			//Set rule profile for sensor measurement
			SetRuleEngineType(pProfileManager.GetProfileNameForAction().getSzProfileType(),pProfileManager.GetProfileNameForAction().getSzProfileName());
			//Load Rules
			LoadKnowledge();
			//Get current snapshot
			ActorSnapshotDTO createdActorSnapshot = new ActorSnapshotDTO();
			createdActorSnapshot.setId(createdActorSnapshots.get(i).getId());
			createdActorSnapshot.setActor(createdActorSnapshots.get(i).getActor());
			createdActorSnapshot.setLocationArea(createdActorSnapshots.get(i).getLocationArea());
			createdActorSnapshot.setPeriod(createdActorSnapshots.get(i).getPeriod());
			createdActorSnapshot.setStatus(createdActorSnapshots.get(i).getStatus());
			//Add current snapshot
			AddDTOObjects(createdActorSnapshot);
			//Run Rules, this function automatically load the utility classes
			ProcessRules();
			//Empty the session
			ReinitializeEngine();
		}
	}
}
