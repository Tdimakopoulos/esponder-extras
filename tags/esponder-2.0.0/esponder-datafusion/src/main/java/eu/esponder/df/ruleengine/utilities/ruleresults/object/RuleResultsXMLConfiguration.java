package eu.esponder.df.ruleengine.utilities.ruleresults.object;

import java.util.List;

import org.simpleframework.xml.ElementList;

public class RuleResultsXMLConfiguration {

	@ElementList(inline = true)
	private List<RuleResultsXML> entries;

	public List<RuleResultsXML> getEntries() {
		return entries;
	}

	public void setEntries(List<RuleResultsXML> entries) {
		this.entries = entries;
	}
	
	
}
