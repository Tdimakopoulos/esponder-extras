package eu.esponder.model.crisis.resource.category;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.type.EquipmentType;

@Entity
@Table(name="equipment_category")
@NamedQueries({
	@NamedQuery(name="EquipmentCategory.findByType", query="select a from EquipmentCategory a where a.equipmentType=:equipmentType") })
public class EquipmentCategory extends PlannableResourceCategory {

	private static final long serialVersionUID = 5814307867860284042L;
	
	@OneToOne
	@JoinColumn(name="EQUIPMENT_TYPE_ID", nullable=false)
	private EquipmentType equipmentType;
	
//	@OneToMany(mappedBy="equipmentCategory")
//	private Set<Equipment> equipment;
	
	public EquipmentType getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(EquipmentType equipmentType) {
		this.equipmentType = equipmentType;
	}

//	public Set<Equipment> getEquipment() {
//		return equipment;
//	}
//
//	public void setEquipment(Set<Equipment> equipment) {
//		this.equipment = equipment;
//	}

}
