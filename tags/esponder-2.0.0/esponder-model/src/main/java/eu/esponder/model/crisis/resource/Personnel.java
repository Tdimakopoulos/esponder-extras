package eu.esponder.model.crisis.resource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.category.PersonnelCategory;
import eu.esponder.model.crisis.resource.plan.PlannableResource;

@Entity
@Table(name="personnel")
@NamedQueries({
	@NamedQuery(name="Personnel.findByTitle", query="select p from Personnel p where p.title=:title"),
	@NamedQuery(name="Personnel.findAll", query="select p from Personnel p")
})
public class Personnel extends PlannableResource {

	private static final long serialVersionUID = -3981713854457369736L;

	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@ManyToOne
	@JoinColumn(name="ORGANISATION_ID")
	private Organisation organisation;
	
	public Organisation getOrganisation() {
		return organisation;
	}

	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}

	/**
	 * Indicates whether this person is available to be assigned to a crisis or not.
	 * A particular service needs to be developed to keep this indicator up to date
	 */
	@Column(name="AVAILABILITY")
	private boolean availability;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private PersonnelCategory personnelCategory;
	
	public PersonnelCategory getPersonnelCategory() {
		return personnelCategory;
	}

	public void setPersonnelCategory(PersonnelCategory personnelCategory) {
		this.personnelCategory = personnelCategory;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isAvailability() {
		return availability;
	}

	public void setAvailability(boolean availability) {
		this.availability = availability;
	}

}
