package eu.esponder.model.crisis.action;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eu.esponder.model.snapshot.status.ActionPartSnapshotStatus;
import eu.esponder.model.snapshot.status.ActionSnapshotStatus;

@Embeddable
public class ActionPartScheduleCriteria implements Serializable {

	private static final long serialVersionUID = 6947038776364763386L;
	
	@Enumerated(EnumType.STRING)
	@Column(name="PREREQUISITE_STATUS")
	private ActionPartSnapshotStatus status;

	
	@Column(name="DATE_AFTER")
	private Long dateAfter;

	public ActionPartSnapshotStatus getStatus() {
		return status;
	}

	public void setStatus(ActionPartSnapshotStatus status) {
		this.status = status;
	}

	public Long getDateAfter() {
		return dateAfter;
	}

	public void setDateAfter(Long dateAfter) {
		this.dateAfter = dateAfter;
	}

}
