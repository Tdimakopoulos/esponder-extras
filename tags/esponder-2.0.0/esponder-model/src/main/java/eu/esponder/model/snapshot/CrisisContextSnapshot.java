package eu.esponder.model.snapshot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.snapshot.status.CrisisContextSnapshotStatus;

@Entity
@Table(name="crisis_context_snapshot")
public class CrisisContextSnapshot extends SpatialSnapshot<Long> {
	
	private static final long serialVersionUID = -3318916280076239374L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CRISIS_CONTEXT_SNAPSHOT_ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="CRISIS_CONTEXT_ID", nullable=false)
	private CrisisContext crisisContext;
	
	@Enumerated(EnumType.STRING)
	@Column(name="CRISIS_CONTEXT_SNAPSHOT_STATUS", nullable=false)
	private CrisisContextSnapshotStatus status;
	
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private CrisisContextSnapshot previous;
	
	@OneToOne(mappedBy="previous")
	private CrisisContextSnapshot next;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CrisisContext getCrisisContext() {
		return crisisContext;
	}

	public void setCrisisContext(CrisisContext crisisContext) {
		this.crisisContext = crisisContext;
	}

	public CrisisContextSnapshotStatus getStatus() {
		return status;
	}

	public void setStatus(CrisisContextSnapshotStatus status) {
		this.status = status;
	}

	public CrisisContextSnapshot getPrevious() {
		return previous;
	}

	public void setPrevious(CrisisContextSnapshot previous) {
		this.previous = previous;
	}

	public CrisisContextSnapshot getNext() {
		return next;
	}

	public void setNext(CrisisContextSnapshot next) {
		this.next = next;
	}
		
}
