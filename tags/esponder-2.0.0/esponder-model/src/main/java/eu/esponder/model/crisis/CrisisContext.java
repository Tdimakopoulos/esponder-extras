package eu.esponder.model.crisis;

import java.sql.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.snapshot.CrisisContextSnapshot;
import eu.esponder.model.snapshot.location.Sphere;
import eu.esponder.model.type.CrisisType;

@Entity
@Table(name="crisis_context")
@NamedQueries({
	@NamedQuery(name="CrisisContext.findByTitle", query="select cc from CrisisContext cc where cc.title=:title"),
	@NamedQuery(name="CrisisContext.findAll", query="select cc from CrisisContext cc")
})
public class CrisisContext extends ESponderEntity<Long> {
	
	private static final long serialVersionUID = -2118773143338642719L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CRISIS_CONTEXT_ID")
	private Long id;
	
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	@OneToMany(mappedBy="crisisContext")
	private Set<Action> actions;
	
	@OneToOne
	@JoinColumn(name="LOCATION_ID")
	private Sphere crisisLocation;
	
	@OneToMany(mappedBy="crisisContext")
	private Set<CrisisContextSnapshot> snapshots;
	
	@OneToMany(mappedBy="crisisContext")
	private Set<OperationsCentre> operationsCentres;
	
	@Column(name = "CRISIS_CONTEXT_ADDITIONAL_INFO", length = 10000)
    private String additionalInfo;
	 
	@Column(name = "CRISIS_CONTEXT_START_DATE")
	private Long startDate;
	
	@Column(name = "CRISIS_CONTEXT_END_DATE")
	private Long endDate;
	
	@ManyToMany/*(cascade={CascadeType.PERSIST,CascadeType.MERGE, CascadeType.REMOVE})*/
	@JoinTable(name="crisis_context_types",
			joinColumns=@JoinColumn(name="CONTEXT_ID"),
			inverseJoinColumns=@JoinColumn(name="CRISIS_TYPE_ID"))
	private Set<CrisisType> crisisTypes;
	
	@Column(name="ALERT")
	private CrisisContextAlertEnum crisisContextAlert;
	
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public Set<OperationsCentre> getOperationsCentres() {
		return operationsCentres;
	}

	public void setOperationsCentres(Set<OperationsCentre> operationsCentres) {
		this.operationsCentres = operationsCentres;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<Action> getActions() {
		return actions;
	}

	public void setActions(Set<Action> actions) {
		this.actions = actions;
	}

	public Set<CrisisContextSnapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<CrisisContextSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	public Sphere getCrisisLocation() {
		return crisisLocation;
	}

	public void setCrisisLocation(Sphere crisisLocation) {
		this.crisisLocation = crisisLocation;
	}

	public CrisisContextAlertEnum getCrisisContextAlert() {
		return crisisContextAlert;
	}

	public void setCrisisContextAlert(CrisisContextAlertEnum crisisContextAlert) {
		this.crisisContextAlert = crisisContextAlert;
	}

	public Set<CrisisType> getCrisisTypes() {
		return crisisTypes;
	}

	public void setCrisisTypes(Set<CrisisType> crisisTypes) {
		this.crisisTypes = crisisTypes;
	}
	
}
