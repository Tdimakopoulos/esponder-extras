package eu.esponder.model.crisis.resource;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.model.crisis.resource.category.ConsumableResourceCategory;


@Entity
@Table(name="consumable_resource")
@NamedQueries({
	@NamedQuery(name="ConsumableResource.findByTitle", query="select cr from ConsumableResource cr where cr.title=:title"),
	@NamedQuery(name="ConsumableResource.findAll", query="select cr from ConsumableResource cr")
})
public class ConsumableResource extends LogisticsResource {

	private static final long serialVersionUID = -7626364119871726847L;

	@Column(name="QUANTITY")
	private BigDecimal quantity;
	
	@ManyToOne
	@JoinColumn(name="REUSABLE_RESOURCE_ID")
	private ReusableResource container;
	
	@ManyToOne
	@JoinColumn(name="ACTION_PART_ID")
	private ActionPart actionPart;

	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private ConsumableResourceCategory consumableResourceCategory;

	@ManyToOne
	@JoinColumn(name="CRISIS_CONTEXT_ID")
	private CrisisContext crisisContext;
	
	public CrisisContext getCrisisContext() {
		return crisisContext;
	}

	public void setCrisisContext(CrisisContext crisisContext) {
		this.crisisContext = crisisContext;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public ReusableResource getContainer() {
		return container;
	}

	public void setContainer(ReusableResource container) {
		this.container = container;
	}

	public ActionPart getActionPart() {
		return actionPart;
	}

	public void setActionPart(ActionPart actionPart) {
		this.actionPart = actionPart;
	}

	public ConsumableResourceCategory getConsumableResourceCategory() {
		return consumableResourceCategory;
	}

	public void setConsumableResourceCategory(
			ConsumableResourceCategory consumableResourceCategory) {
		this.consumableResourceCategory = consumableResourceCategory;
	}

}
