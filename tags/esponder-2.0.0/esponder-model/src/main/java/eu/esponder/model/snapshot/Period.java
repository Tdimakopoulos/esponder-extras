package eu.esponder.model.snapshot;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class Period implements Serializable {
	
	private static final long serialVersionUID = -1943089649218847062L;
	
	public Period() { }

	public Period(Long dateFrom, Long dateTo) {
		super();
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}

	@Column(name="DATE_FROM", nullable=false)
	private Long dateFrom;
	
	@Column(name="DATE_TO", nullable=false)
	private Long dateTo;

	public Long getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Long dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Long getDateTo() {
		return dateTo;
	}

	public void setDateTo(Long dateTo) {
		this.dateTo = dateTo;
	}

	@Override
	public String toString() {
		return "Period [dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}

}
