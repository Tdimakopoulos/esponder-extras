package eu.esponder.model.user;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.OperationsCentre;

@Entity
@Table(name="esponder_user")
@NamedQueries({
	@NamedQuery(name="ESponderUser.findByUserName", query="select u from ESponderUser u where u.userName=:userName"),
	@NamedQuery(name="ESponderUser.findAll", query="select u from ESponderUser u")
})
public class ESponderUser extends ESponderEntity<Long> {
	
	private static final long serialVersionUID = -2512583407793541959L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ESPONDER_USER_ID")
	private Long id;

	@Column(name="USER_NAME", nullable=false, unique=true)
	private String userName;
	
	@Column(name="ROLE")
	private String role;
	
	@ManyToMany
	@JoinTable(
		name="operations_centre_users",
		joinColumns=@JoinColumn(name="USER_ID"),
		inverseJoinColumns=@JoinColumn(name="OPERATIONS_CENTRE_ID"))
	private Set<OperationsCentre> operationsCentres;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Set<OperationsCentre> getOperationsCentres() {
		return operationsCentres;
	}

	public void setOperationsCentres(Set<OperationsCentre> operationsCentres) {
		this.operationsCentres = operationsCentres;
	}

	@Override
	public String toString() {
		return "ESponderUser [id=" + id + ", userName=" + userName + "]";
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
