package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("REUSABLE")
public final class ReusableResourceType extends LogisticsResourceType {

	private static final long serialVersionUID = 3416073165515169747L;
	
	
}
