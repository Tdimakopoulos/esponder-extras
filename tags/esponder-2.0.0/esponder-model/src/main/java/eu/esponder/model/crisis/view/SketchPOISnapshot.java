package eu.esponder.model.crisis.view;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.snapshot.Snapshot;

@Entity
@Table(name="sketch_poi_snapshot")
@NamedQueries({
	@NamedQuery(name="SketchPOISnapshot.findByOperationsCentre", query="select s from SketchPOISnapshot s where s.operationsCentre.id=:operationsCentreID"),
	@NamedQuery(name="SketchPOISnapshot.findBySketchPOIId", query="select s from SketchPOISnapshot s where s.id=:sketchsnapshotPOIId"),
	@NamedQuery(name="SketchPOISnapshot.findByTitle", query="select s from SketchPOISnapshot s where s.title=:sketchsnapshotPOITitle"),
	@NamedQuery(name = "SketchPOISnapshot.findBySketchPOIAndDate", query = "SELECT s FROM SketchPOISnapshot s WHERE s.sketchPOI.id = :sketchID AND s.period.dateTo <= :maxDate AND s.period.dateTo = "
			+ "(SELECT max(s.period.dateTo) FROM SketchPOISnapshot s WHERE s.sketchPOI.id = :sketchID)"),
	@NamedQuery(name = "SketchPOISnapshot.findPreviousSnapshot", query = "Select s from SketchPOISnapshot s where s.id=(select max(snapshot.id) from SketchPOISnapshot snapshot where snapshot.sketchPOI.id = :sketchID)"),
	@NamedQuery(name = "SketchPOISnapshot.findAll", query = "Select s from SketchPOISnapshot s")
})
public class SketchPOISnapshot extends Snapshot<Long> {
	

	private static final long serialVersionUID = -1734142629191263542L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SKETCH_POI_ID_SNAPSHOT")
	protected Long id;
	
	@ManyToMany(cascade={CascadeType.PERSIST,CascadeType.MERGE, CascadeType.REMOVE})
	@JoinTable(name="sketch_points_snapshots",
			joinColumns=@JoinColumn(name="SKETCH_POI_ID_SNAPSHOT"),
			inverseJoinColumns=@JoinColumn(name="POINT_ID"))
	private Set<MapPoint> points;
	
	@Embedded
	private HttpURL httpURL;
	
	@Column(name="sketchType_snapshot")
	private String sketchType;
	
	@Column(name="sketchpoi_snapshot")
	private SketchPOI sketchPOI;

	@Column(name="TITLE_SNAPSHOT", nullable=false, unique=true, length=255)
	protected String title;
	
	@ManyToOne
	@JoinColumn(name="OPERATIONS_CENTRE_ID_SNAPSHOT")
	protected OperationsCentre operationsCentre;
	
	public SketchPOI getSketchPOI() {
		return sketchPOI;
	}

	public void setSketchPOI(SketchPOI sketchPOI) {
		this.sketchPOI = sketchPOI;
	}


	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<MapPoint> getPoints() {
		return points;
	}

	public void setPoints(Set<MapPoint> points) {
		this.points = points;
	}

	public HttpURL getHttpURL() {
		return httpURL;
	}

	public void setHttpURL(HttpURL httpURL) {
		this.httpURL = httpURL;
	}

	public String getSketchType() {
		return sketchType;
	}

	public void setSketchType(String sketchType) {
		this.sketchType = sketchType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public OperationsCentre getOperationsCentre() {
		return operationsCentre;
	}

	public void setOperationsCentre(OperationsCentre operationsCentre) {
		this.operationsCentre = operationsCentre;
	}
	
}
