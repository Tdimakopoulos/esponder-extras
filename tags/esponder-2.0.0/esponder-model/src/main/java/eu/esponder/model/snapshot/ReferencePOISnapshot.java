package eu.esponder.model.snapshot;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;

@Entity
@Table(name="reference_poi_snapshot")
@NamedQueries({
	@NamedQuery(name="ReferencePOISnapshot.findByOperationsCentre", query="select s from ReferencePOISnapshot s where s.operationsCentre.id=:operationsCentreID"),
	@NamedQuery(name="ReferencePOISnapshot.findByReferencePOIId", query="select s from ReferencePOISnapshot s where s.referencePOI=:ReferencePOIId"),
	@NamedQuery(name="ReferencePOISnapshot.findByTitle", query="select s from ReferencePOISnapshot s where s.title=:ReferencePOIsnapshotTitle"),
	@NamedQuery(name = "ReferencePOISnapshot.findByReferenceAndDate", query = "SELECT s FROM ReferencePOISnapshot s WHERE s.referencePOI.id = :ReferenceID AND s.period.dateTo <= :maxDate AND s.period.dateTo = "
			+ "(SELECT max(s.period.dateTo) FROM ReferencePOISnapshot s WHERE s.referencePOI.id = :referenceID)"),
	@NamedQuery(name = "ReferencePOISnapshot.findPreviousSnapshot", query = "Select s from ReferencePOISnapshot s where s.id=(select max(snapshot.id) from ReferencePOISnapshot snapshot where snapshot.referencePOI.id = :referenceID)"),
	@NamedQuery(name = "ReferencePOISnapshot.findAll", query = "Select s from ReferencePOISnapshot s")
})
public class ReferencePOISnapshot extends Snapshot<Long> {
	
	private static final long serialVersionUID = -1729481522641331340L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="POI_ID_SNAPSHOT")
	protected Long id;
	
	@Column(name="REF_FILE_SNAPSHOT")
	protected String referenceFile;
	
	@Column(name="AVERAGE_SIZE_SNAPSHOT")
	protected Float averageSize;
	
	@Column(name="TITLE_SNAPSHOT", nullable=false, unique=true, length=255)
	protected String title;
	
	@ManyToOne
	@JoinColumn(name="OPERATIONS_CENTRE_ID_SNAPSHOT")
	protected OperationsCentre operationsCentre;
	
	@ManyToOne
	@JoinColumn(name="REFERENCE_POI_ID", nullable=false)
	protected ReferencePOI referencePOI;
	
	@OneToOne
	@JoinColumn(name="PREVIOUS_SNAPSHOT_ID")
	private ReferencePOISnapshot previous;
	
	@OneToOne(mappedBy="previous")
	private ReferencePOISnapshot next;
	
	public ReferencePOISnapshot getPrevious() {
		return previous;
	}

	public void setPrevious(ReferencePOISnapshot previous) {
		this.previous = previous;
	}

	public ReferencePOISnapshot getNext() {
		return next;
	}

	public void setNext(ReferencePOISnapshot next) {
		this.next = next;
	}

	public String getReferenceFile() {
		return referenceFile;
	}

	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

	public Float getAverageSize() {
		return averageSize;
	}

	public void setAverageSize(Float averageSize) {
		this.averageSize = averageSize;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public OperationsCentre getOperationsCentre() {
		return operationsCentre;
	}

	public void setOperationsCentre(OperationsCentre operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	public ReferencePOI getReferencePOI() {
		return referencePOI;
	}

	public void setReferencePOI(ReferencePOI referencePOI) {
		this.referencePOI = referencePOI;
	}
}



