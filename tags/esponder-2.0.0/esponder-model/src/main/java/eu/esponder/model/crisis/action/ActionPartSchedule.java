package eu.esponder.model.crisis.action;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;

@Entity
@Table(name="actionpart_schedule")
public class ActionPartSchedule extends ESponderEntity<Long> {

	private static final long serialVersionUID = 6440269024338454840L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTIONPART_SCHEDULE_ID")
	private Long id;
	
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	@ManyToOne
	@JoinColumn(name="ACTIONPART_ID", nullable=false)
	private ActionPart action;
	
	@ManyToOne
	@JoinColumn(name="PREREQUISITE_ACTIONPART_ID", nullable=false)
	private ActionPart prerequisiteAction;
	
	@Embedded
	private ActionPartScheduleCriteria criteria;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ActionPart getAction() {
		return action;
	}

	public void setAction(ActionPart action) {
		this.action = action;
	}

	public ActionPart getPrerequisiteAction() {
		return prerequisiteAction;
	}

	public void setPrerequisiteAction(ActionPart prerequisiteAction) {
		this.prerequisiteAction = prerequisiteAction;
	}

	public ActionPartScheduleCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(ActionPartScheduleCriteria criteria) {
		this.criteria = criteria;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

		
}


