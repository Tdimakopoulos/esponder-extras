package eu.esponder.model.crisis.resource.plan;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.type.CrisisType;

/**
 * This entity provides the information required for initialising the set of Resources than need to be deployed in the crisis field 
 * (at all levels of command, namely strategic, tactical, operational) based on the type of disaster or relevant features of the crisis.
 * 
 * @author gleo
 *
 */

@Entity
@Table(name="crisis_resource_plan")
@NamedQueries({
	@NamedQuery(name="CrisisResourcePlan.findAll", query="select crp from CrisisResourcePlan crp"),
	@NamedQuery(name="CrisisResourcePlan.findByTitle", query="select crp from CrisisResourcePlan crp where crp.title= :title")
})
public class CrisisResourcePlan extends ESponderEntity<Long> {

	private static final long serialVersionUID = 6886705125898043247L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CRISIS_RESOURCE_PLAN_ID")
	protected Long id;
	
	@Column(name="RESOURCE_PLAN_TITLE")
	private String title;
	
	@OneToMany(mappedBy="crisisResourcePlan")
	private Set<PlannableResourcePower> plannableResources;
	
	
	/**
	 * This field indicates either the CrisisDisasterType or the CrisisFeatureType for the plan.
	 * It is for further study whether the combination of them (i.e DisasterType and FeatureType) 
	 * should mandate that an additive exclusive result of the plannableResourcePower should be applied.
	 * 
	 */
	
	@OneToOne
	@JoinColumn(name="ACTION_TYPE_ID", nullable=false)
	private CrisisType crisisType;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<PlannableResourcePower> getPlannableResources() {
		return plannableResources;
	}

	public void setPlannableResources(Set<PlannableResourcePower> plannableResources) {
		this.plannableResources = plannableResources;
	}

	public CrisisType getCrisisType() {
		return crisisType;
	}

	public void setCrisisType(CrisisType crisisType) {
		this.crisisType = crisisType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
