package eu.esponder.model.snapshot.resource;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.model.snapshot.Snapshot;
import eu.esponder.model.snapshot.status.MeasurementStatisticTypeEnum;
import eu.esponder.model.snapshot.status.SensorSnapshotStatus;

@Entity
@Table(name = "sensor_snapshot")
@NamedQueries({
		@NamedQuery(name = "SensorSnapshot.findBySensorAndDate", query = "SELECT s FROM SensorSnapshot s WHERE s.sensor.id = :sensorID AND s.period.dateTo <= :maxDate AND s.period.dateTo = "
				+ "(SELECT max(s.period.dateTo) FROM SensorSnapshot s WHERE s.sensor.id = :sensorID)"),
		@NamedQuery(name = "SensorSnapshot.findPreviousSnapshot", query = "Select s from SensorSnapshot s where s.id=(select max(snapshot.id) from SensorSnapshot snapshot where snapshot.sensor.id = :sensorID)"),
		@NamedQuery(name = "SensorSnapshot.findAll", query = "Select s from SensorSnapshot s"),
		@NamedQuery(name = "SensorSnapshot.findAllBySensor", query = "Select s from SensorSnapshot s where s.sensor = :sensor")
		})
public class SensorSnapshot extends Snapshot<Long> {

	private static final long serialVersionUID = 7546751894098413109L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SENSOR_SNAPSHOT_ID")
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "SENSOR_SNAPSHOT_STATUS", nullable = false)
	private SensorSnapshotStatus status;

	@ManyToOne
	@JoinColumn(name = "SENSOR_ID", nullable = false)
	private Sensor sensor;

	@OneToOne
	@JoinColumn(name = "PREVIOUS_SNAPSHOT_ID")
	private SensorSnapshot previous;

	@OneToOne(mappedBy = "previous")
	private SensorSnapshot next;

	@Column(name = "STATISTIC_TYPE")
	private MeasurementStatisticTypeEnum statisticType;

	@Column(name = "VALUE")
	private BigDecimal value;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SensorSnapshotStatus getStatus() {
		return status;
	}

	public void setStatus(SensorSnapshotStatus status) {
		this.status = status;
	}

	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}

	public SensorSnapshot getPrevious() {
		return previous;
	}

	public void setPrevious(SensorSnapshot previous) {
		this.previous = previous;
	}

	public SensorSnapshot getNext() {
		return next;
	}

	public void setNext(SensorSnapshot next) {
		this.next = next;
	}

	public MeasurementStatisticTypeEnum getStatisticType() {
		return statisticType;
	}

	public void setStatisticType(MeasurementStatisticTypeEnum statisticType) {
		this.statisticType = statisticType;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
