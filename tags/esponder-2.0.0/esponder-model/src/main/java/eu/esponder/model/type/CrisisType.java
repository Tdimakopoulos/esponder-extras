package eu.esponder.model.type;

import javax.persistence.Entity;

import eu.esponder.model.crisis.resource.sensor.Sensor;


@Entity
public abstract class CrisisType extends ESponderType {

	private static final long serialVersionUID = -5814308761452031478L;
	
	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    final Sensor other = (Sensor) obj;
	    if (id != other.getId()) {
	        return false;
	    }
	    return true;
	}

}