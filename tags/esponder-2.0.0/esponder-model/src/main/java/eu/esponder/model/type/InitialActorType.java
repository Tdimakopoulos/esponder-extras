package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("INIT_ACTOR")
public final class InitialActorType extends ActorType {

	private static final long serialVersionUID = -231655312265592161L;
	
}
