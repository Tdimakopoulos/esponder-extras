package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("EQUIPMENT")
public final class EquipmentType extends ESponderType {

	private static final long serialVersionUID = -7218166530641914993L;
	
}
