package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("ENVIRONMENTAL")
public abstract class EnvironmentalSensor extends Sensor {

	private static final long serialVersionUID = 909177965785563306L;

}
