package eu.esponder.model.crisis.resource.category;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class PlannableResourceCategory extends ResourceCategory {

	private static final long serialVersionUID = 4443616080294927775L;

}
