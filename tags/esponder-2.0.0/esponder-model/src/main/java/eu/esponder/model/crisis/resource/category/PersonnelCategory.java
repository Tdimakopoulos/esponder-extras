package eu.esponder.model.crisis.resource.category;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.PersonnelCompetence;
import eu.esponder.model.type.RankType;

@Entity
@Table(name="personnel_category")
@NamedQueries({
	@NamedQuery(name="PersonnelCategory.findByType", query="select a from PersonnelCategory a where a.rank=:rankType and a.organisationCategory=:organisationCategory") })
public class PersonnelCategory extends PlannableResourceCategory {

	private static final long serialVersionUID = 8088945215121398846L;

	/**
	 * Models the discipline-specific (RankType) rank, i.e. General, Colonel etc.
	 */
	@OneToOne
	@JoinColumn(name="DISCIPLINE_TYPE_ID")
	private RankType rank;
	
//	@OneToMany(mappedBy="personnelCategory")
//	private Set<Personnel> personnel;
	
	@OneToMany(mappedBy="personnelCategory")
	private Set<PersonnelCompetence> personnelCompetences;
	
	@ManyToOne
	@JoinColumn(name="ORGANISATION_CATEGORY_ID")
	private OrganisationCategory organisationCategory;
	
	public OrganisationCategory getOrganisationCategory() {
		return organisationCategory;
	}

	public void setOrganisationCategory(OrganisationCategory organisationCategory) {
		this.organisationCategory = organisationCategory;
	}

	public RankType getRank() {
		return rank;
	}

	public Set<PersonnelCompetence> getPersonnelCompetences() {
		return personnelCompetences;
	}

	public void setPersonnelCompetences(
			Set<PersonnelCompetence> personnelCompetences) {
		this.personnelCompetences = personnelCompetences;
	}

	public void setRank(RankType rank) {
		this.rank = rank;
	}

//	public Set<Personnel> getPersonnel() {
//		return personnel;
//	}
//
//	public void setPersonnel(Set<Personnel> personnel) {
//		this.personnel = personnel;
//	}

}
