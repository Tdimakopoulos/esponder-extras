package eu.esponder.model.snapshot.status;

public enum CrisisContextSnapshotStatus {
	STARTED,
	RESOLVED,
	UNRESOLVED
}
