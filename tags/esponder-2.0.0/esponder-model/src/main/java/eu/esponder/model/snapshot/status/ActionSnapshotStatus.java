package eu.esponder.model.snapshot.status;


public enum ActionSnapshotStatus {
	STARTED,
	INPROGRESS,
	FINALIZED,
	PAUSED,
	COMPLETED,
	CANCELED;
	
	public static ActionSnapshotStatus getActionSnapshotStatusEnum(int value) {
		switch (value) {
		case 0:
			return STARTED;
		case 1:
			return INPROGRESS;
		case 2:
			return FINALIZED;
		case 3:
			return PAUSED;
		case 4:
			return COMPLETED;
		case 5:
			return CANCELED;
		default:
			return null;
		}
	}
}
