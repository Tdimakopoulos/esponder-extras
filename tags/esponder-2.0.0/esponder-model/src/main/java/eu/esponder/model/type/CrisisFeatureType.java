package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


/**
 * See D7.0.1 page 162
 * 
 * Arbitrary Types that are not following the hierarchy of CrisisTypes
 * 
 * Human Losses, Injuries, People Hemming, Building Damages, Infrastructure Damages
 * Leak of dangerous substances, Near Living Regions, Near Industrial Regions,
 * City or Village Inaccessible
 *
 */

@Entity
@DiscriminatorValue("CRISIS_FEATURE")
public class CrisisFeatureType extends CrisisType {

	private static final long serialVersionUID = -2517895504901732735L;

}
