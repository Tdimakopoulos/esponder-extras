package eu.esponder.model.crisis.resource.category;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.type.ConsumableResourceType;

@Entity
@Table(name="consumables_category")
@NamedQueries({
	@NamedQuery(name="ConsumableResourceCategory.findByType", query="select a from ConsumableResourceCategory a where a.consumableResourceType=:consumableResourceType") })
public class ConsumableResourceCategory extends LogisticsCategory {

	private static final long serialVersionUID = -7110133452197870817L;

	@OneToOne
	@JoinColumn(name="CONSUMABLE_RESOURCE_TYPE_ID", nullable=false)
	private ConsumableResourceType consumableResourceType;
	
//	@OneToMany(mappedBy="consumableResourceCategory")
//	private Set<ConsumableResource> consumableResources;

//	public Set<ConsumableResource> getConsumableResources() {
//		return consumableResources;
//	}
//
//	public void setConsumableResources(Set<ConsumableResource> consumableResources) {
//		this.consumableResources = consumableResources;
//	}

	public ConsumableResourceType getConsumableResourceType() {
		return consumableResourceType;
	}

	public void setConsumableResourceType(
			ConsumableResourceType consumableResourceType) {
		this.consumableResourceType = consumableResourceType;
	}

}
