package eu.esponder.jaxb.model.snapshot;

import javax.xml.bind.annotation.XmlSeeAlso;
import eu.esponder.jaxb.model.snapshot.location.LocationAreaDTO;
import eu.esponder.jaxb.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.resource.OperationsCentreSnapshotDTO;


@XmlSeeAlso({
	ActorSnapshotDTO.class,
	OperationsCentreSnapshotDTO.class,
	CrisisContextSnapshotDTO.class
})
public abstract class SpatialSnapshotDTO extends SnapshotDTO {
	
	protected LocationAreaDTO locationArea;

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

	@Override
	public String toString() {
		return "SpatialSnapshotDTO [locationArea=" + locationArea + ", id=" + id + "]";
	}

}
