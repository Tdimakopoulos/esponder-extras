package eu.esponder.jaxb.model.snapshot.location;

import javax.xml.bind.annotation.XmlSeeAlso;

import eu.esponder.jaxb.model.ESponderEntityDTO;


@XmlSeeAlso({
	SphereDTO.class
})
public abstract class LocationAreaDTO extends ESponderEntityDTO {

}
