package eu.esponder.jaxb.model.type;

public class SensorTypeDTO extends ESponderTypeDTO {
	
	private String measurementUnit;
	
	public String getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

}
