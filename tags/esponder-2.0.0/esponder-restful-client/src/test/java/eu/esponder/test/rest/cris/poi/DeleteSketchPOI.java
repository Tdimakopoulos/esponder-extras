package eu.esponder.test.rest.cris.poi;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import eu.esponder.rest.client.ResteasyClient;

public class DeleteSketchPOI {

	private String SKETCHPOI_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/view/sketch";

	@Test
	public void DeleteSketchPOIWithJson () throws ClassNotFoundException, Exception {

		String serviceName = SKETCHPOI_SERVICE_URI + "/delete";
		ResteasyClient deleteClient = new ResteasyClient(serviceName, "application/json");
		System.out.println("Client for createGenericEntity created successfully...");
		Map<String, String> params =  CreateServiceParameters(new Long(3));
		deleteClient.delete(params);

	}

	private Map<String, String> CreateServiceParameters(Long sketchPOIID) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		params.put("SketchPOIId", sketchPOIID.toString());
		return params;
	}

}