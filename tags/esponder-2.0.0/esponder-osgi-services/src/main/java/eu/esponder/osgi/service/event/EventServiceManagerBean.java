package eu.esponder.osgi.service.event;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.osgi.listener.thread.ESponderEventListenerServer;
import eu.esponder.server.EsponderServer;

@Stateless
public class EventServiceManagerBean implements EventServiceManager {
	static {
		// Create the thread supplying it with the runnable object
		Thread thread = new ESponderEventListenerServer();
		thread.start();
//		new EsponderServer(4444);
//		new EsponderServer(4445);
//		new EsponderServer(4446);
		}
	
}
