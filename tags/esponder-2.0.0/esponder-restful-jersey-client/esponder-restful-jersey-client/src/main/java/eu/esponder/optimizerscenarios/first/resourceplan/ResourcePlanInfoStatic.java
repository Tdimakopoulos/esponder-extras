package eu.esponder.optimizerscenarios.first.resourceplan;

public class ResourcePlanInfoStatic {

	String FireBrigade="Fire Brigade";
	String policeforce="Police Force";
	String eoc="EOC";
	
	String mm1="Medical Resource";
	String mm2="Medical Kit";
	
	Long CrisisID=new Long(1);
	
	public int GetMaxResources()
	{
		return 2;
	}
	
	public String Getmm1()
	{
		return mm1;
	}
	public String Getmm2()
	{
		return mm2;
	}
	public String GetEOCName()
	{
		return eoc;
	}
	
	public Long GetCrisisID()
	{
		return CrisisID;
	}
	public int GetPolice()
	{
		return 1;
	}
	
	public int GetFire()
	{
		return 1;
	}
	
	public int GetEOC()
	{
		return 1;
	}
	public String GetNameOFFirefighter()
	{
		return FireBrigade;
	}
	
	public String GetNameOfPolice()
	{
		return policeforce;
	}
}
