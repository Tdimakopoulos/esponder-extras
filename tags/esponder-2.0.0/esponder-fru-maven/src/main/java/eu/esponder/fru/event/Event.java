package eu.esponder.fru.event;
public class Event {
	
	private int id;
	
	private int value;
	
	private String type;

	Event(int idd, String tp, int valuee) {
		id = idd;
		type = tp;
		value = valuee;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
