package eu.esponder.ws.client.logic.objects;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.type.CrisisDisasterTypeDTO;
import eu.esponder.dto.model.type.CrisisFeatureTypeDTO;
import eu.esponder.ws.client.query.QueryManager;

public class CrisisTypeQueryManager {

	List<CrisisDisasterTypeDTO> pCrisisDisasterTypes = new ArrayList<CrisisDisasterTypeDTO>();
	List<CrisisFeatureTypeDTO> pCrisisFeatureTypes = new ArrayList<CrisisFeatureTypeDTO>();
	
	public void LoadAllCrisisTypes() throws JsonParseException, JsonMappingException, IOException {
		QueryManager pMan = new QueryManager();
		ResultListDTO pre = pMan.getAllTypes("1");
		
		for (int i = 0; i < pre.getResultList().size(); i++) {
			if (pre.getResultList().get(i) instanceof CrisisDisasterTypeDTO) {
				CrisisDisasterTypeDTO pitem=(CrisisDisasterTypeDTO)pre.getResultList().get(i);
				pCrisisDisasterTypes.add(pitem);
			}
//			if (pre.getResultList().get(i) instanceof CrisisFeatureTypeDTO) {
//				CrisisFeatureTypeDTO pitem=(CrisisFeatureTypeDTO)pre.getResultList().get(i);
//				pCrisisFeatureTypes.add(pitem);
//			}
		}
	}

	public List<CrisisDisasterTypeDTO> getpCrisisDisasterTypes() {
		return pCrisisDisasterTypes;
	}

	public void setpCrisisDisasterTypes(
			List<CrisisDisasterTypeDTO> pCrisisDisasterTypes) {
		this.pCrisisDisasterTypes = pCrisisDisasterTypes;
	}

	public List<CrisisFeatureTypeDTO> getpCrisisFeatureTypes() {
		return pCrisisFeatureTypes;
	}

	public void setpCrisisFeatureTypes(
			List<CrisisFeatureTypeDTO> pCrisisFeatureTypes) {
		this.pCrisisFeatureTypes = pCrisisFeatureTypes;
	}
}
