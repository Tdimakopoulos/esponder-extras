package eu.esponder.ws.client.urlmanager;

public class UrlManager {

	
	//String szURLHost="http://rdocs.exodussa.com:";
	String szURLHost="http://localhost:";
	
	String szURLPort="8080";
	
	String szGetAllSensorSnapshosts=szURLHost+szURLPort+"/esponder-restful/crisis/snapshot/sensorsnapshotfindall";

	String szGetAllEsponderTypes=szURLHost+szURLPort+"/esponder-restful/esponder/types/findAll";
	
	
	String szSpherePathCreate=szURLHost+szURLPort+"/esponder-restful/crisis/generic/create";

	String szGetActorByID=szURLHost+szURLPort+"/esponder-restful/crisis/resource/actor/getID";

	String szCreateCrisisContext=szURLHost+szURLPort+"/esponder-restful/crisis/generic/create";
	
	String szCreateRegisteredOC=szURLHost+szURLPort+"/esponder-restful/crisis/context/regoc/create";
	
	String szGetAllRegisteredOC=szURLHost+szURLPort+"/esponder-restful/crisis/context/regoc/findAll";

	String szCreateRegisterConsumableResource=szURLHost+szURLPort+"/esponder-restful/crisis/view/regconsumables/create";
	
	String szCreateRegisterReusableResource=szURLHost+szURLPort+"/esponder-restful/crisis/view/reusables/create";
	
	String szOperationCenterFindAll=szURLHost+szURLPort+"/esponder-restful/crisis/context/oc/findAll";
	
	String szPersonnelFindAll=szURLHost+szURLPort+"/esponder-restful/crisis/view/personnel/findAll";
	
	String szRegisterReusablesFindAll=szURLHost+szURLPort+"/esponder-restful/crisis/view/regreusables/findAll";
	
	String szRegisterConsumablesFindAll=szURLHost+szURLPort+"/esponder-restful/crisis/view/regconsumables/findAll";
	
	String szConsumablesFindAll=szURLHost+szURLPort+"/esponder-restful/crisis/view/consumables/findAll";
	
	String szCrisisResourcePlanFindAll=szURLHost+szURLPort+"/esponder-restful/crisis/view/crisisResourcePlan/findAll";
	
	String szCrisisContextFindAll=szURLHost+szURLPort+"/esponder-restful/crisis/view/crisisContext/findAll";
	
	String szReusablesFindAll=szURLHost+szURLPort+"/esponder-restful/crisis/view/reusables/findAll";
	
	String szCreateAction=szURLHost+szURLPort+"/esponder-restful/crisis/view/action/create";
	
	String szCreateActionPart=szURLHost+szURLPort+"/esponder-restful/crisis/view/actionPart/create";


	String szCrisisContentUpdate = szURLHost+szURLPort+"/esponder-restful/crisis/view/crisisContext/update";
	
	String szResourcePlanUpdate = szURLHost+szURLPort+"/esponder-restful/crisis/view/crisisResourcePlan/update";

	String szResourcePlanCreate = szURLHost+szURLPort+"/esponder-restful/crisis/view//crisisResourcePlan/create";
	
	String szUpdateRegisteredOC=szURLHost+szURLPort+"/esponder-restful/crisis/context/regoc/update";
	
	String szgetAllEquipments=szURLHost+szURLPort+"/esponder-restful/crisis/view/equipments/findByID";
	
	String szgetEventsBySeverity=szURLHost+szURLPort+"/esponder-restful/events/view/osgievents/getBySeverity";
	
	String szgetEventsByID=szURLHost+szURLPort+"/esponder-restful/events/view/osgievents/getByID";
	
	private String szCreateCrisisWithEvent=szURLHost+szURLPort+"/esponder-restful/crisis/view/crisisContext/createwithevent";
	
	public String getSzgetEventsByID() {
		return szgetEventsByID;
	}

	public void setSzgetEventsByID(String szgetEventsByID) {
		this.szgetEventsByID = szgetEventsByID;
	}

	public String getSzgetEventsBySeverity() {
		return szgetEventsBySeverity;
	}

	public void setSzgetEventsBySeverity(String szgetEventsBySeverity) {
		this.szgetEventsBySeverity = szgetEventsBySeverity;
	}

	public String getSzGetAllEsponderTypes() {
		return szGetAllEsponderTypes;
	}

	public String getSzgetAllEquipments() {
		return szgetAllEquipments;
	}

	public void setSzgetAllEquipments(String szgetAllEquipments) {
		this.szgetAllEquipments = szgetAllEquipments;
	}

	public void setSzGetAllEsponderTypes(String szGetAllEsponderTypes) {
		this.szGetAllEsponderTypes = szGetAllEsponderTypes;
	}

	public String getSzGetAllSensorSnapshosts() {
		return szGetAllSensorSnapshosts;
	}

	public void setSzGetAllSensorSnapshosts(String szGetAllSensorSnapshosts) {
		this.szGetAllSensorSnapshosts = szGetAllSensorSnapshosts;
	}
	
	public String getSzUpdateRegisteredOC() {
		return szUpdateRegisteredOC;
	}

	public void setSzUpdateRegisteredOC(String szUpdateRegisteredOC) {
		this.szUpdateRegisteredOC = szUpdateRegisteredOC;
	}

	public String getSzResourcePlanCreate() {
		return szResourcePlanCreate;
	}

	public void setSzResourcePlanCreate(String szResourcePlanCreate) {
		this.szResourcePlanCreate = szResourcePlanCreate;
	}

	public String getSzCreateAction() {
		return szCreateAction;
	}

	public String getSzCrisisContentUpdate() {
		return szCrisisContentUpdate;
	}

	public void setSzCrisisContentUpdate(String szCrisisContentUpdate) {
		this.szCrisisContentUpdate = szCrisisContentUpdate;
	}

	public String getSzResourcePlanUpdate() {
		return szResourcePlanUpdate;
	}

	public void setSzResourcePlanUpdate(String szResourcePlanUpdate) {
		this.szResourcePlanUpdate = szResourcePlanUpdate;
	}

	public void setSzCreateAction(String szCreateAction) {
		this.szCreateAction = szCreateAction;
	}

	public String getSzCreateActionPart() {
		return szCreateActionPart;
	}

	public void setSzCreateActionPart(String szCreateActionPart) {
		this.szCreateActionPart = szCreateActionPart;
	}

	public String getSzOperationCenterFindAll() {
		return szOperationCenterFindAll;
	}

	public void setSzOperationCenterFindAll(String szOperationCenterFindAll) {
		this.szOperationCenterFindAll = szOperationCenterFindAll;
	}

	public String getSzPersonnelFindAll() {
		return szPersonnelFindAll;
	}

	public void setSzPersonnelFindAll(String szPersonnelFindAll) {
		this.szPersonnelFindAll = szPersonnelFindAll;
	}

	public String getSzRegisterReusablesFindAll() {
		return szRegisterReusablesFindAll;
	}

	public void setSzRegisterReusablesFindAll(String szRegisterReusablesFindAll) {
		this.szRegisterReusablesFindAll = szRegisterReusablesFindAll;
	}

	public String getSzRegisterConsumablesFindAll() {
		return szRegisterConsumablesFindAll;
	}

	public void setSzRegisterConsumablesFindAll(String szRegisterConsumablesFindAll) {
		this.szRegisterConsumablesFindAll = szRegisterConsumablesFindAll;
	}

	public String getSzConsumablesFindAll() {
		return szConsumablesFindAll;
	}

	public void setSzConsumablesFindAll(String szConsumablesFindAll) {
		this.szConsumablesFindAll = szConsumablesFindAll;
	}

	public String getSzCrisisResourcePlanFindAll() {
		return szCrisisResourcePlanFindAll;
	}

	public void setSzCrisisResourcePlanFindAll(String szCrisisResourcePlanFindAll) {
		this.szCrisisResourcePlanFindAll = szCrisisResourcePlanFindAll;
	}

	public String getSzCrisisContextFindAll() {
		return szCrisisContextFindAll;
	}

	public void setSzCrisisContextFindAll(String szCrisisContextFindAll) {
		this.szCrisisContextFindAll = szCrisisContextFindAll;
	}

	public String getSzReusablesFindAll() {
		return szReusablesFindAll;
	}

	public void setSzReusablesFindAll(String szReusablesFindAll) {
		this.szReusablesFindAll = szReusablesFindAll;
	}

	public String getSzCreateRegisterReusableResource() {
		return szCreateRegisterReusableResource;
	}

	public void setSzCreateRegisterReusableResource(
			String szCreateRegisterReusableResource) {
		this.szCreateRegisterReusableResource = szCreateRegisterReusableResource;
	}

	public String getSzCreateRegisterConsumableResource() {
		return szCreateRegisterConsumableResource;
	}

	public void setSzCreateRegisterConsumableResource(
			String szCreateRegisterConsumableResource) {
		this.szCreateRegisterConsumableResource = szCreateRegisterConsumableResource;
	}

	public String getSzURLHost() {
		return szURLHost;
	}

	public void setSzURLHost(String szURLHost) {
		this.szURLHost = szURLHost;
	}

	public String getSzURLPort() {
		return szURLPort;
	}

	public void setSzURLPort(String szURLPort) {
		this.szURLPort = szURLPort;
	}

	public String getSzSpherePathCreate() {
		return szSpherePathCreate;
	}

	public void setSzSpherePathCreate(String szSpherePathCreate) {
		this.szSpherePathCreate = szSpherePathCreate;
	}

	public String getSzGetActorByID() {
		return szGetActorByID;
	}

	public void setSzGetActorByID(String szGetActorByID) {
		this.szGetActorByID = szGetActorByID;
	}

	public String getSzCreateCrisisContext() {
		return szCreateCrisisContext;
	}

	public void setSzCreateCrisisContext(String szCreateCrisisContext) {
		this.szCreateCrisisContext = szCreateCrisisContext;
	}

	public String getSzCreateRegisteredOC() {
		return szCreateRegisteredOC;
	}

	public void setSzCreateRegisteredOC(String szCreateRegisteredOC) {
		this.szCreateRegisteredOC = szCreateRegisteredOC;
	}

	public String getSzGetAllRegisteredOC() {
		return szGetAllRegisteredOC;
	}

	public void setSzGetAllRegisteredOC(String szGetAllRegisteredOC) {
		this.szGetAllRegisteredOC = szGetAllRegisteredOC;
	}

	String getSzCreateCrisisWithEvent() {
		return szCreateCrisisWithEvent;
	}

	void setSzCreateCrisisWithEvent(String szCreateCrisisWithEvent) {
		this.szCreateCrisisWithEvent = szCreateCrisisWithEvent;
	}
	
	

}
