package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.event.model.ESponderEvent;

public abstract class SnapshotEvent<T extends SnapshotDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = -1146740015260871634L;
	
}
