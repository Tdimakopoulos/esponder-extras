package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.event.model.DeleteEvent;


public class DeleteOperationsCentreEvent extends OperationsCentreEvent<OperationsCentreDTO> implements DeleteEvent {

	private static final long serialVersionUID = 2788080828499796379L;

}
