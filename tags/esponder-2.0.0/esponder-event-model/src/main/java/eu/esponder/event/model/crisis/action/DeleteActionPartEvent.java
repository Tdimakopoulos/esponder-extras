package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.event.model.DeleteEvent;


public class DeleteActionPartEvent extends ActionPartEvent<ActionPartDTO> implements DeleteEvent {

	private static final long serialVersionUID = -225309812850357319L;
	
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
