package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateActionPartEvent extends ActionPartEvent<ActionPartDTO> implements UpdateEvent {

	private static final long serialVersionUID = 161564239638577602L;
	
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
