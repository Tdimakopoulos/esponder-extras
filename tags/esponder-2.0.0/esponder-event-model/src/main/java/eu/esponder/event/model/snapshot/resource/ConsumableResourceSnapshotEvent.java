package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.SnapshotDTO;


public abstract class ConsumableResourceSnapshotEvent<T extends SnapshotDTO> extends ResourceSnapshotEvent<T> {

	private static final long serialVersionUID = 2544414366303024642L;

}
