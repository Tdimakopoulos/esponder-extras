package eu.esponder.event.model.snapshot.action;

import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.event.model.CreateEvent;


public class CreateActionPartSnapshotEvent extends ActionPartSnapshotEvent<ActionPartSnapshotDTO> implements CreateEvent {

	private static final long serialVersionUID = -8444980801237832581L;

	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
