package eu.esponder.event.model.crisis;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.event.model.DeleteEvent;


public class DeleteCrisisContextEvent extends CrisisContextEvent<CrisisContextDTO> implements DeleteEvent {

	private static final long serialVersionUID = -1139683152265684159L;
		
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo="CrisisContextEvent Journal Info";
		return JournalMessageInfo;
	}
}
