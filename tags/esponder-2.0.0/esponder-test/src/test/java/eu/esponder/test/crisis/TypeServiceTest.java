package eu.esponder.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;
import eu.esponder.dto.model.type.CrisisDisasterTypeDTO;
import eu.esponder.dto.model.type.CrisisFeatureTypeDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.dto.model.type.EmergencyOperationsCentreTypeDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.dto.model.type.InitialActorTypeDTO;
import eu.esponder.dto.model.type.MobileEmergencyOperationsCentreTypeDTO;
import eu.esponder.dto.model.type.OperationalActionTypeDTO;
import eu.esponder.dto.model.type.OperationalActorTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.dto.model.type.PersonnelSkillTypeDTO;
import eu.esponder.dto.model.type.PersonnelTrainingTypeDTO;
import eu.esponder.dto.model.type.RankTypeDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;
import eu.esponder.dto.model.type.SensorTypeDTO;
import eu.esponder.dto.model.type.StrategicActionTypeDTO;
import eu.esponder.dto.model.type.TacticalActionTypeDTO;
import eu.esponder.model.snapshot.status.MeasurementUnitEnum;
import eu.esponder.model.type.EmergencyOperationsCentreType;
import eu.esponder.model.type.EquipmentType;
import eu.esponder.model.type.InitialActorType;
import eu.esponder.model.type.MobileEmergencyOperationsCentreType;
import eu.esponder.model.type.OperationalActorType;
import eu.esponder.model.type.SensorType;
import eu.esponder.model.type.StrategicActorType;
import eu.esponder.model.type.TacticalActorType;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class TypeServiceTest extends ControllerServiceTest {

	@Test(groups="createBasics")
	public void testCreateTypes() throws ClassNotFoundException {

		// Actors
		TacticalActorType missionCommander = new TacticalActorType();
		missionCommander.setTitle("Mission Commander");
		ESponderTypeDTO typeDTO = (ESponderTypeDTO) mappingService.mapESponderEntity(missionCommander, ESponderTypeDTO.class); 
		typeService.createTypeRemote(typeDTO, this.userID);

		StrategicActorType frc = new StrategicActorType();
		frc.setTitle("FRC");
		typeService.createTypeRemote((ESponderTypeDTO) mappingService.mapESponderEntity(frc, OperationalActorTypeDTO.class), this.userID);

		OperationalActorType fr = new OperationalActorType();
		fr.setTitle("FR");
		typeService.createTypeRemote((ESponderTypeDTO) mappingService.mapESponderEntity(fr, OperationalActorTypeDTO.class), this.userID);
		
		InitialActorType initActor = new InitialActorType();
		initActor.setTitle("INIT_FR");
		typeService.createTypeRemote((ESponderTypeDTO) mappingService.mapESponderEntity(initActor, InitialActorTypeDTO.class), this.userID);

		// Operations Centers 
		EmergencyOperationsCentreType eoc = new EmergencyOperationsCentreType();
		eoc.setTitle("EOC");
		typeService.createTypeRemote((ESponderTypeDTO) mappingService.mapESponderEntity(eoc, EmergencyOperationsCentreTypeDTO.class), this.userID);

		MobileEmergencyOperationsCentreType meoc = new MobileEmergencyOperationsCentreType();
		meoc.setTitle("MEOC");
		typeService.createTypeRemote((ESponderTypeDTO) mappingService.mapESponderEntity(meoc, MobileEmergencyOperationsCentreTypeDTO.class), this.userID);

		// Equipment
		EquipmentType wimax = new EquipmentType();
		wimax.setTitle("FRU WiMAX");
		typeService.createTypeRemote((ESponderTypeDTO) mappingService.mapESponderEntity(wimax, EquipmentTypeDTO.class), this.userID);

		EquipmentType wifi = new EquipmentType();
		wifi.setTitle("FRU WiFi");
		typeService.createTypeRemote((ESponderTypeDTO) mappingService.mapESponderEntity(wifi, EquipmentTypeDTO.class), this.userID);

		// Sensors
		SensorType heartbeatSensorType = new SensorType();
		heartbeatSensorType.setTitle("HB");
		heartbeatSensorType.setMeasurementUnit(MeasurementUnitEnum.BBM);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService.mapESponderEntity(heartbeatSensorType, SensorTypeDTO.class), this.userID);

		SensorType temperatureSensorType = new SensorType();
		temperatureSensorType.setTitle("TEMP");
		temperatureSensorType.setMeasurementUnit(MeasurementUnitEnum.DEGREES_CELCIUS);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService.mapESponderEntity(temperatureSensorType, SensorTypeDTO.class), this.userID);
		
		SensorType locationSensorType = new SensorType();
		locationSensorType.setTitle("LOCATION");
		locationSensorType.setMeasurementUnit(MeasurementUnitEnum.DEGREES);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService.mapESponderEntity(locationSensorType, SensorTypeDTO.class), this.userID);

		// Actions
		StrategicActionTypeDTO strategicActionTypeDTO = new StrategicActionTypeDTO();
		strategicActionTypeDTO.setTitle("StratActionType");
		typeService.createTypeRemote(strategicActionTypeDTO, this.userID);

		TacticalActionTypeDTO tacticalActionTypeDTO = new TacticalActionTypeDTO();
		tacticalActionTypeDTO.setTitle("TactActionType");
		typeService.createTypeRemote(tacticalActionTypeDTO, this.userID);

		OperationalActionTypeDTO opActionTypeDTO = new OperationalActionTypeDTO();
		opActionTypeDTO.setTitle("OpActionType");
		typeService.createTypeRemote(opActionTypeDTO, this.userID);

		// Consumable Resources
		ConsumableResourceTypeDTO crType1 = new ConsumableResourceTypeDTO();
		crType1.setTitle("Drink");
		typeService.createTypeRemote(crType1, this.userID);

		ConsumableResourceTypeDTO crType2 = new ConsumableResourceTypeDTO();
		crType2.setTitle("Food");
		typeService.createTypeRemote(crType2, this.userID);

		ConsumableResourceTypeDTO crType3 = new ConsumableResourceTypeDTO();
		crType3.setTitle("Medical Resource");
		typeService.createTypeRemote(crType3, this.userID);

		// Reusable Resources
		ReusableResourceTypeDTO rrType1 = new ReusableResourceTypeDTO();
		rrType1.setTitle("Water Container");
		typeService.createTypeRemote(rrType1, this.userID);

		ReusableResourceTypeDTO rrType2 = new ReusableResourceTypeDTO();
		rrType2.setTitle("Food package");
		typeService.createTypeRemote(rrType2, this.userID);

		ReusableResourceTypeDTO rrType3 = new ReusableResourceTypeDTO();
		rrType3.setTitle("Medical Kit");
		typeService.createTypeRemote(rrType3, this.userID);

		ReusableResourceTypeDTO rrType4 = new ReusableResourceTypeDTO();
		rrType4.setTitle("Tools");
		typeService.createTypeRemote(rrType4, this.userID);

		//Organisations
		OrganisationTypeDTO organisationTypeDTO1 = new OrganisationTypeDTO();
		organisationTypeDTO1.setTitle("Headquarters");
		typeService.createTypeRemote(organisationTypeDTO1, this.userID);

		OrganisationTypeDTO organisationTypeDTO2 = new OrganisationTypeDTO();
		organisationTypeDTO2.setTitle("Local Station");
		typeService.createTypeRemote(organisationTypeDTO2, this.userID);


		//Disciplines
		DisciplineTypeDTO disciplineTypeDTO1 = new DisciplineTypeDTO();
		disciplineTypeDTO1.setTitle("Police Force");
		typeService.createTypeRemote(disciplineTypeDTO1, this.userID);

		DisciplineTypeDTO disciplineTypeDTO2 = new DisciplineTypeDTO();
		disciplineTypeDTO2.setTitle("Fire Brigade");
		typeService.createTypeRemote(disciplineTypeDTO2, this.userID);

		DisciplineTypeDTO disciplineTypeDTO3 = new DisciplineTypeDTO();
		disciplineTypeDTO3.setTitle("Coast Guard");
		typeService.createTypeRemote(disciplineTypeDTO3, this.userID);

		DisciplineTypeDTO disciplineTypeDTO4 = new DisciplineTypeDTO();
		disciplineTypeDTO4.setTitle("Army");
		typeService.createTypeRemote(disciplineTypeDTO4, this.userID);

		// Rank Types
		RankTypeDTO rankTypeDTO1 = new RankTypeDTO();
		rankTypeDTO1.setTitle("RankType1");
		typeService.createTypeRemote(rankTypeDTO1, this.userID);

		RankTypeDTO rankTypeDTO2 = new RankTypeDTO();
		rankTypeDTO2.setTitle("RankType2");
		typeService.createTypeRemote(rankTypeDTO2, this.userID);

		RankTypeDTO rankTypeDTO3 = new RankTypeDTO();
		rankTypeDTO3.setTitle("RankType3");
		typeService.createTypeRemote(rankTypeDTO3, this.userID);

		// Personnel Skill Types
		PersonnelSkillTypeDTO personnelSkillTypeDTO1 = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO1.setTitle("Fire Truck Driving");
		typeService.createTypeRemote(personnelSkillTypeDTO1, this.userID);

		PersonnelSkillTypeDTO personnelSkillTypeDTO2 = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO2.setTitle("Crane Operation Handling");
		typeService.createTypeRemote(personnelSkillTypeDTO2, this.userID);

		PersonnelSkillTypeDTO personnelSkillTypeDTO3 = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO3.setTitle("Firewall Planning");
		typeService.createTypeRemote(personnelSkillTypeDTO3, this.userID);


		// Personnel Training Types
		PersonnelTrainingTypeDTO personnelTrainingTypeDTO1 = new PersonnelTrainingTypeDTO();
		personnelTrainingTypeDTO1.setTitle("Driving");
		typeService.createTypeRemote(personnelTrainingTypeDTO1, this.userID);

		PersonnelTrainingTypeDTO personnelTrainingTypeDTO2 = new PersonnelTrainingTypeDTO();
		personnelTrainingTypeDTO2.setTitle("Tool Handling");
		typeService.createTypeRemote(personnelTrainingTypeDTO2, this.userID);

		PersonnelTrainingTypeDTO personnelTrainingTypeDTO3 = new PersonnelTrainingTypeDTO();
		personnelTrainingTypeDTO3.setTitle("Administration");
		typeService.createTypeRemote(personnelTrainingTypeDTO3, this.userID);


		// Crisis Disaster Types
		CrisisDisasterTypeDTO crisisDisasterTypeDTO1 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO1.setTitle("Fire");
		typeService.createTypeRemote(crisisDisasterTypeDTO1, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO2 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO2.setTitle("Earthquake");
		typeService.createTypeRemote(crisisDisasterTypeDTO2, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO3 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO3.setTitle("Flood");
		typeService.createTypeRemote(crisisDisasterTypeDTO3, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO4 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO4.setTitle("Road accident");
		typeService.createTypeRemote(crisisDisasterTypeDTO4, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO5 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO5.setTitle("Blizzard");
		typeService.createTypeRemote(crisisDisasterTypeDTO5, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO6 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO6.setTitle("Storm");
		typeService.createTypeRemote(crisisDisasterTypeDTO6, this.userID);

		// Crisis Feature Types
		CrisisFeatureTypeDTO crisisFeatureTypeDTO1 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO1.setTitle("Human Loss");
		typeService.createTypeRemote(crisisFeatureTypeDTO1, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO2 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO2.setTitle("Injury");
		typeService.createTypeRemote(crisisFeatureTypeDTO2, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO3 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO3.setTitle("People hemming");
		typeService.createTypeRemote(crisisFeatureTypeDTO3, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO4 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO4.setTitle("Building damage");
		typeService.createTypeRemote(crisisFeatureTypeDTO4, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO5 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO5.setTitle("Substance leak");
		typeService.createTypeRemote(crisisFeatureTypeDTO5, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO6 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO6.setTitle("Inaccessible city");
		typeService.createTypeRemote(crisisFeatureTypeDTO6, this.userID);

	}

}
