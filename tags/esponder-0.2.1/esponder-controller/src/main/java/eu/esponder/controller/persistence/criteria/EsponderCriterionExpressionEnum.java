package eu.esponder.controller.persistence.criteria;

public enum EsponderCriterionExpressionEnum {

	EQUAL,
	GREATER_THAN_OR_EQUAL,
	GREATER_THAN,
	LESS_THAN,
	LESS_THAN_OR_EQUAL,
	NOT
}
