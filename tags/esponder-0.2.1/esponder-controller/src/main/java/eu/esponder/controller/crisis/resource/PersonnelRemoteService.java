package eu.esponder.controller.crisis.resource;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.PersonnelCompetenceDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;

@Remote
public interface PersonnelRemoteService {

	public PersonnelDTO findPersonnelByIdRemote(Long personnelID);

	public PersonnelDTO findPersonnelByTitleRemote(String personnelTitle);

	public PersonnelDTO createPersonnelRemote(PersonnelDTO personnelDTO, Long userID);

	public PersonnelDTO updatePersonnelRemote(PersonnelDTO personnelDTO, Long userID);

	public void deletePersonnelRemote(PersonnelDTO personnelDTO);

	public void deletePersonnelByIdRemote(Long personnelDTOId);

	public PersonnelCompetenceDTO findPersonnelCompetenceByIdRemote(Long personnelCompetenceID);

	public PersonnelCompetenceDTO findPersonnelCompetenceByTitleRemote(String personnelCompetenceTitle);

	public PersonnelCompetenceDTO createPersonnelCompetenceRemote(PersonnelCompetenceDTO personnelCompetenceDTO, Long userID) throws ClassNotFoundException;

	public PersonnelCompetenceDTO updatePersonnelCompetenceRemote(PersonnelCompetenceDTO personnelCompetenceDTO, Long userID) throws ClassNotFoundException;

	public void deletePersonnelCompetenceByIdRemote(Long personnelCompetenceDTOId);

}
