package eu.esponder.controller.crisis.resource;

import java.util.Date;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;

@Remote
public interface SensorRemoteService {
	
	public SensorDTO findSensorByIdRemote(Long sensorId);
	
	public SensorDTO findSensorByTitleRemote(String title);
	
	public SensorDTO createSensorRemote(SensorDTO sensor, Long userID);
	
	public SensorDTO updateSensorRemote(SensorDTO sensorDTO, Long userID) throws ClassNotFoundException;
	
	public void deleteSensorRemote(Long sensorId, Long userID);
	
	public SensorSnapshotDTO findSensorSnapshotByDateRemote(Long sensorID, Date dateTo);
	
	public SensorSnapshotDTO findSensorSnapshotByIdRemote(Long sensorID);
	
	public SensorSnapshotDTO findPreviousSensorSnapshotRemote(Long sensorID);
	
	public SensorSnapshotDTO createSensorSnapshotRemote(SensorSnapshotDTO snapshotDTO, Long userID);
	
	public SensorSnapshotDTO updateSensorSnapshotRemote(SensorSnapshotDTO sensorSnapshotDTO, Long userID);
	
	public void deleteSensorSnapshotRemote(Long sensorSnapshotId, Long userID);
	
	public StatisticsConfigDTO findConfigByIdRemote(Long configID);
	
	public StatisticsConfigDTO createStatisticConfigRemote(StatisticsConfigDTO statisticConfigDTO, Long userID);
	
	public StatisticsConfigDTO updateStatisticConfigRemote(StatisticsConfigDTO statisticConfigDTO, Long userID);
	
	public void deleteStatisticConfigRemote(Long statisticsConfigId, Long userID);
	
	
}
