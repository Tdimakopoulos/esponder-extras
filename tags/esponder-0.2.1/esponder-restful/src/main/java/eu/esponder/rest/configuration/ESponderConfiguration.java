package eu.esponder.rest.configuration;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
import eu.esponder.rest.ESponderResource;

@Path("/system/configuration")
public class ESponderConfiguration extends ESponderResource {

	
	@GET
	@Path("/getByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO getConfigurationById (
			@QueryParam("configurationId") @NotNull(message="ConfigurationId may not be null") Long configurationId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {
		
		return this.getConfigurationRemoteService().findESponderConfigByIdRemote(configurationId);
	}
	
	
	@GET
	@Path("/getByName")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO getConfigurationByName (
			@QueryParam("configurationName") @NotNull(message="ConfigurationId may not be null") String configurationName,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {

		return this.getConfigurationRemoteService().findESponderConfigByNameRemote(configurationName);
	}
	
	
	@POST
	@Path("/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO createESponderConfiguration(
			@NotNull(message="entity to be created may not be null") ESponderConfigParameterDTO configurationDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {
		
		ESponderConfigParameterDTO configDTO = this.getConfigurationRemoteService().createESponderConfigRemote(configurationDTO, userID); 
		return configDTO;
	}

	
	@PUT
	@Path("/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO updateConfiguration( 
			@NotNull(message="entity to be updated may not be null") ESponderConfigParameterDTO configurationDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {

		return this.getConfigurationRemoteService().updateESponderConfigRemote(configurationDTO, userID);
	}
	
	
	@DELETE
	@Path("/delete")
	public void deleteConfiguration(
			@QueryParam("configurationId") @NotNull(message="ConfigurationId may not be null") Long configurationId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) throws Exception {

		this.getConfigurationRemoteService().deleteESponderConfigRemote(configurationId, userID);
	}


}