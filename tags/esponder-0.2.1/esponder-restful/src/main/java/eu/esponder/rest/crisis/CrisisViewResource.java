package eu.esponder.rest.crisis;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.rest.ESponderResource;

@Path("/crisis/view")
public class CrisisViewResource extends ESponderResource {

	//TODO Create OperationsCentres Web Services
	
	
	@GET
	@Path("/sketch/read")
	@Produces({MediaType.APPLICATION_JSON})
	public List<SketchPOIDTO> readSketches(
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		return this.getOperationsCentreRemoteService().findSketchPOIRemote(operationsCentreID);
	}
	
	@GET
	@Path("/sketch/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO readSketchById(
			@QueryParam("sketchPOIId") @NotNull(message="sketchPOIId may not be null") Long sketchPOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		SketchPOIDTO sketchPOIDTO = this.getOperationsCentreRemoteService().findSketchPOIByIdRemote(sketchPOIId);
		return sketchPOIDTO;
	}
	
	@GET
	@Path("/sketch/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO readSketchByTitle(
			@QueryParam("sketchPOITitle") @NotNull(message="sketchPOITitle may not be null") String sketchPOITitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		SketchPOIDTO sketchPOIDTO = this.getOperationsCentreRemoteService().findSketchPOIByTitleRemote(sketchPOITitle);
		return sketchPOIDTO;
	}
	
	@POST
	@Path("/sketch/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO createSketchReturning(
			@NotNull(message="sketch object may not be null") SketchPOIDTO sketchDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().createSketchPOIRemote(operationsCentreDTO, sketchDTO, userID);
		}
		return null;
	}
	
	
	@PUT
	@Path("/sketch/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SketchPOIDTO updateSketchPOI(
			@NotNull(message="sketch object may not be null") SketchPOIDTO sketchDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().updateSketchPOIRemote(operationsCentreDTO, sketchDTO, userID);
		}
		return null;
	}
	
	
	@DELETE
	@Path("/sketch/delete")
	public void deleteSketchPOI(
			@QueryParam("SketchPOIId") @NotNull(message="SketchPOIId may not be null") Long sketchPOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getOperationsCentreRemoteService().deleteSketchPOIRemote(sketchPOIId, userID);
		return;
	}
	
	
	@GET
	@Path("/ref/read")
	@Produces({MediaType.APPLICATION_JSON})
	public List<ReferencePOIDTO> readReferencePOIs(
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		return this.getOperationsCentreRemoteService().findReferencePOIsRemote(operationsCentreID);
	}
	
	
	@GET
	@Path("/ref/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO readReferenceById(
			@QueryParam("referencePOIId") @NotNull(message="referencePOIId may not be null") Long referencePOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		return this.getOperationsCentreRemoteService().findReferencePOIByIdRemote(referencePOIId);
	}
	
	@GET
	@Path("/ref/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO readReferenceByTitle(
			@QueryParam("referencePOITitle") @NotNull(message="referencePOITitle may not be null") String referencePOITitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		return this.getOperationsCentreRemoteService().findReferencePOIByTitleRemote(referencePOITitle);
	}
	
	@POST
	@Path("/ref/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO createReferencePOIReturning(
			@NotNull(message="referencePOI object may not be null") ReferencePOIDTO referencePOIDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().createReferencePOIRemote(operationsCentreDTO, referencePOIDTO, userID);
		}
		return null;
	}
	
	
	@PUT
	@Path("/ref/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReferencePOIDTO updateReferencePOI(
			@NotNull(message="sketch object may not be null") ReferencePOIDTO referencePOIDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		OperationsCentreDTO operationsCentreDTO = this.getOperationsCentreRemoteService().findOperationsCentreByUserRemote(operationsCentreID, userID);
		if (null != operationsCentreDTO) {
			return this.getOperationsCentreRemoteService().updateReferencePOIRemote(operationsCentreDTO, referencePOIDTO, userID);
		}
		return null;
	}
	
	
	@DELETE
	@Path("/ref/delete")
	public void deletereferencePOI(
			@QueryParam("ReferencePOIId") @NotNull(message="referencePOIId may not be null") Long referencePOIId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		this.getOperationsCentreRemoteService().deleteReferencePOIRemote(referencePOIId, userID);
		return;
	}
	
	
}
