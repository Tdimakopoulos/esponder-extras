package eu.esponder.dto.model.crisis.action;


public enum ActionOperationEnumDTO {

	MOVE,
	TRANSPORT,
	FIX
}
