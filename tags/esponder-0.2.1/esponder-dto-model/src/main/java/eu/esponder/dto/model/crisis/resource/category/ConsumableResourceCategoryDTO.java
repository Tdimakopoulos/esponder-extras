package eu.esponder.dto.model.crisis.resource.category;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "consumableResourceType", "consumableResources"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ConsumableResourceCategoryDTO extends LogisticsCategoryDTO {

	private static final long serialVersionUID = 8169469202498003577L;

	private ConsumableResourceTypeDTO consumableResourceType;

	private Set<ConsumableResourceDTO> consumableResources;

	public ConsumableResourceTypeDTO getConsumableResourceType() {
		return consumableResourceType;
	}

	public void setConsumableResourceType(
			ConsumableResourceTypeDTO consumableResourceType) {
		this.consumableResourceType = consumableResourceType;
	}

	public Set<ConsumableResourceDTO> getConsumableResources() {
		return consumableResources;
	}

	public void setConsumableResources(
			Set<ConsumableResourceDTO> consumableResources) {
		this.consumableResources = consumableResources;
	}

}
