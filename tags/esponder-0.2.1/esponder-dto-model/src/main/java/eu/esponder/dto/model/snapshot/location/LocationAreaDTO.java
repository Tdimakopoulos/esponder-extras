package eu.esponder.dto.model.snapshot.location;
 

import org.codehaus.jackson.annotate.JsonTypeInfo;
import eu.esponder.dto.model.ESponderEntityDTO;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class LocationAreaDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = -6264903057849400607L;

	public LocationAreaDTO() {}
	
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
