package eu.esponder.dto.model.type;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.ESponderEntityDTO;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ESponderTypeDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = 4437636967611059143L;

	public ESponderTypeDTO() {}
	
	protected String title;
	
	protected ESponderTypeDTO parent;
	
	protected Set<ESponderTypeDTO> children;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ESponderTypeDTO getParent() {
		return parent;
	}

	public void setParent(ESponderTypeDTO parent) {
		this.parent = parent;
	}

	public Set<ESponderTypeDTO> getChildren() {
		return children;
	}

	public void setChildren(Set<ESponderTypeDTO> children) {
		this.children = children;
	}
	
}
