package eu.esponder.dto.model.snapshot.status;



public enum ActionPartSnapshotStatusDTO {
	STARTED,
	PAUSED,
	COMPLETED,
	CANCELED;
	
	public static ActionPartSnapshotStatusDTO getActionPartSnapshotStatusEnum(int value) {
		switch (value) {
		case 0:
			return STARTED;
		case 1:
			return PAUSED;
		case 2:
			return COMPLETED;
		case 3:
			return CANCELED;
		default:
			return null;
		}
	}
}
