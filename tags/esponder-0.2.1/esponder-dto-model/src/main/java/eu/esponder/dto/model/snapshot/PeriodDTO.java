package eu.esponder.dto.model.snapshot;

import java.io.Serializable;
import java.sql.Date;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"dateFrom", "dateTo"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class PeriodDTO implements Serializable {
	
	private static final long serialVersionUID = -2870132067764797692L;
	
//	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Date dateFrom;

//	@XmlJavaTypeAdapter(SqlDateAdapter.class)
	private Date dateTo;
	
	public PeriodDTO() { }
	
	public PeriodDTO(Date dateFrom, Date dateTo) {
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}
	
	//FIXME Find a solution for serialization of date fields in PeriodDTO
	
//	public PeriodDTO(XMLGregorianCalendar dateFrom, XMLGregorianCalendar dateTo) {
//		super();
//		this.dateFrom = dateFrom;
//		this.dateTo = dateTo;
//	}
	
//	public XMLGregorianCalendar getDateFrom() {
//		return dateFrom;
//	}
//
//	public void setDateFrom(XMLGregorianCalendar dateFrom) {
//		this.dateFrom = dateFrom;
//	}
//
//	public XMLGregorianCalendar getDateTo() {
//		return dateTo;
//	}
//
//	public void setDateTo(XMLGregorianCalendar dateTo) {
//		this.dateTo = dateTo;
//	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	@Override
	public String toString() {
		return "Period [dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}

}
