package eu.esponder.dto.model.snapshot.status;


public enum EquipmentSnapshotStatusDTO {
	WORKING,
	MALFUNCTIONING,
	DAMAGED
}
