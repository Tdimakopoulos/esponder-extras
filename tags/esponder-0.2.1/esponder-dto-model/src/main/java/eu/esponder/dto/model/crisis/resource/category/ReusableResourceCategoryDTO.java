package eu.esponder.dto.model.crisis.resource.category;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "reusableResourceType", "reusableResources"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ReusableResourceCategoryDTO extends LogisticsCategoryDTO {

	private static final long serialVersionUID = 7231628727141775176L;

	private ReusableResourceTypeDTO reusableResourceType;

	private Set<ReusableResourceDTO> reusableResources;

	public ReusableResourceTypeDTO getReusableResourceType() {
		return reusableResourceType;
	}

	public void setReusableResourceType(ReusableResourceTypeDTO reusableResourceType) {
		this.reusableResourceType = reusableResourceType;
	}

	public Set<ReusableResourceDTO> getReusableResources() {
		return reusableResources;
	}

	public void setReusableResources(Set<ReusableResourceDTO> reusableResources) {
		this.reusableResources = reusableResources;
	}

}
