package eu.esponder.dto.model;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;


@JsonPropertyOrder({ "resultList" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class ResultListDTO implements Serializable {

	private static final long serialVersionUID = -363245325332031836L;

	private List<? extends ESponderEntityDTO> resultList;

	public ResultListDTO() {
	}

	public ResultListDTO(List<? extends ESponderEntityDTO> resultsList) {
		this.resultList = resultsList;
	}

	public List<? extends ESponderEntityDTO> getResultList() {
		return resultList;
	}

	public void setResultList(List<? extends ESponderEntityDTO> resultList) {
		this.resultList = resultList;
	}

	public String toString() {
		String result = "[ResultsList:";
		for (ESponderEntityDTO entity : this.getResultList()) {
			result += entity.toString();
		}
		result += "]";
		return result;
	}

}
