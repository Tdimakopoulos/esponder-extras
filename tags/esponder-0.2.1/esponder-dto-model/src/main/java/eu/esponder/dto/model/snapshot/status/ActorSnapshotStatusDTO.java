package eu.esponder.dto.model.snapshot.status;



public enum ActorSnapshotStatusDTO {
	READY,
	ACTIVE,
	INACTIVE
}
