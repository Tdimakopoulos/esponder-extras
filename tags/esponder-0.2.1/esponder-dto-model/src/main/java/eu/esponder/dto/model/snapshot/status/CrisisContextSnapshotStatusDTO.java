package eu.esponder.dto.model.snapshot.status;


public enum CrisisContextSnapshotStatusDTO {
	STARTED,
	RESOLVED,
	UNRESOLVED
}
