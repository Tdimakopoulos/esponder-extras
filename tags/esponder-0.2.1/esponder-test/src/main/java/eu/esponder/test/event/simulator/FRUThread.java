package eu.esponder.test.event.simulator;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.ArithmeticMeasurementSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationMeasurementSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;

/*
 * Add a shared variable as flag to have ability to kill threads called from here
 */

public class FRUThread extends Thread {

	private Long fruId;

	private ActorDTO eventSource;

	private long threadPeriodFRU;

	private List<Thread> threadsList;

	private List<SensorDTO> sensors;

	public List<SensorMeasurementStatisticDTO> statisticsList;

	public FRUThread(List<SensorDTO> sensors, ActorDTO eventSource, long threadPeriodFRU) {
		this.sensors = sensors;
		this.threadPeriodFRU = threadPeriodFRU;
		this.eventSource = eventSource;
		statisticsList = new ArrayList<SensorMeasurementStatisticDTO>();
	}

	private int range = 3;

	private int min = 35;

	@Override
	public void run() {

		int counter = 5;
		int threadCounter = 0;
		threadsList = new ArrayList<Thread>();

		ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher = null;
		try {
			publisher = new ESponderEventPublisher<CreateSensorMeasurementStatisticEvent>(CreateSensorMeasurementStatisticEvent.class);
		} catch (Exception e1) {
			System.out.println("\n\nError creating an event pulisher\n\n");
			return;
		}


		/*	Start SensorThreads based on sensors list	*/
		System.out.println("Thread executing : " + Thread.currentThread().getName() + " with id : "+Thread.currentThread().getId());
		for(SensorDTO sensor : this.sensors) {
			for(StatisticsConfigDTO config : sensor.getConfiguration()) {
				SensorThread sensorThread = null;
				if (sensor instanceof ArithmeticMeasurementSensorDTO) { 
					sensorThread = new ArithmeticSensorThread(new Long(config.getSamplingPeriodMilliseconds()), sensor, config.getMeasurementStatisticType(), min, range, statisticsList);
				} else if (sensor instanceof LocationMeasurementSensorDTO) {
					sensorThread = new LocationSensorThread(new Long(config.getSamplingPeriodMilliseconds()), sensor, config.getMeasurementStatisticType(), min, range, statisticsList);
				}
				threadCounter++;
				threadsList.add(sensorThread);
			}
		}


		for(Thread thread : threadsList) {
			thread.start();
		}

		while(counter > 0) {
			try {
				// 1. Get measurements from SensorThreads
				if(getStatisticsList().size() != 0)
					// 2. Put them into a SensorMeasurementStatisticEnvelopeDTO
					accessStatistics(publisher);
				else
					System.out.println("\nEnvelope is empty \n");
				counter--;
				Thread.sleep(threadPeriodFRU);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}


		// Stops the sensorThreads based on the shared variable threadKill
		//		setThreadKill(true);
		for(Thread thread : threadsList)
			thread.interrupt();
		
		for(Thread thread : threadsList) {
			try {
				System.out.println("Waiting for Thread "+thread.getName()+"/"+thread.getId()+" to die...");
				thread.join(500);
			} catch (InterruptedException e) {
				System.out.println("Interrupted while waiting for Thread "+thread.getName()+"/"+thread.getId()+" to die...");
			}
		}
			


		System.out.println("\n\n********************	Threads State	********************");
		System.out.println(Thread.currentThread().getName()+"/"+Thread.currentThread().getId());
		for(Thread thread : threadsList) {
			System.out.println(thread.getName()+":"+thread.getId()+":"+ thread.getState()+":"+ thread.isAlive());
		}


		System.out.println(Thread.currentThread().getName()+"/"+Thread.currentThread().getId());
		if(getStatisticsList().size() != 0) {
			System.out.println("\n\nEnvelope is not empty, measurements have been found... \n");
			accessStatistics(publisher);
		}
		else {
			System.out.println("\nEnvelope is empty, no remaining measurements found... \n");
		}

	}




	private void accessStatistics(ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher) {

		SensorMeasurementStatisticEnvelopeDTO envelope = new SensorMeasurementStatisticEnvelopeDTO();
		envelope.setMeasurementStatistics(new ArrayList<SensorMeasurementStatisticDTO>());
		synchronized (this.statisticsList) {

			for( Iterator< SensorMeasurementStatisticDTO > it = statisticsList.iterator(); it.hasNext();)
			{
				SensorMeasurementStatisticDTO statisticMeasurement = it.next();
				envelope.getMeasurementStatistics().add(statisticMeasurement);
				it.remove();
			}
			System.out.println(Thread.currentThread().getClass() + " / " + Thread.currentThread().getName() + "with id : "
							+Thread.currentThread().getId()+"\nEnvelope Contents size is : " + envelope.getMeasurementStatistics().size()
							+"\nStatisticsList contents size is : " + statisticsList.size() + "\n");

			// 3. Publish a CreateSensorMeasurementStatisticEvent
			CreateSensorMeasurementStatisticEvent event = createSensorMeasurementStatisticEvent(envelope);
			try {
				publisher.publishEvent(event);
			} catch (EventListenerException e) {
				e.printStackTrace();
			}
		}

	}
	
	

	private CreateSensorMeasurementStatisticEvent createSensorMeasurementStatisticEvent(SensorMeasurementStatisticEnvelopeDTO envelope) {
		CreateSensorMeasurementStatisticEvent event = new CreateSensorMeasurementStatisticEvent();
		event.setEventAttachment(envelope);
		event.setEventSeverity(SeverityLevelDTO.UNDEFINED);
		event.setEventSource(this.eventSource);
		event.setJournalMessage("Test Journal Message");
		event.setEventTimestamp(new Date());
		return event;
	}


	public synchronized List<SensorMeasurementStatisticDTO> getStatisticsList() {
		return statisticsList;
	}

	public synchronized void setStatisticsList(List<SensorMeasurementStatisticDTO> statisticsList) {
		this.statisticsList = statisticsList;
	}
	
	public Long getFruId() {
		return fruId;
	}

	public void setFruId(Long fruId) {
		this.fruId = fruId;
	}

	public long getThreadPeriodFRU() {
		return threadPeriodFRU;
	}

	public void setThreadPeriodFRU(long threadPeriod) {
		this.threadPeriodFRU = threadPeriod;
	}

	public List<SensorDTO> getSensors() {
		return sensors;
	}

	public void setSensors(List<SensorDTO> sensors) {
		this.sensors = sensors;
	}

	public List<Thread> getThreadsList() {
		return threadsList;
	}

	public void setThreadsList(List<Thread> threadsList) {
		this.threadsList = threadsList;
	}

	public ActorDTO getEventSource() {
		return eventSource;
	}

	public void setEventSource(ActorDTO eventSource) {
		this.eventSource = eventSource;
	}

}
