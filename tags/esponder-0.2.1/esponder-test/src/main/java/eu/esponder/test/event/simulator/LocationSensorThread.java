package eu.esponder.test.event.simulator;

import java.util.Date;
import java.util.List;
import java.util.Random;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;

public class LocationSensorThread extends SensorThread {

	public LocationSensorThread(Long period, SensorDTO sensor,
			MeasurementStatisticTypeEnumDTO statisticType, int min, int range, List<SensorMeasurementStatisticDTO> statisticsEnvelope) {
		super(period, sensor, statisticType, min, range, statisticsEnvelope);	// statisticsEnvelope --> List of statistics, not actual envelope
	}

	@Override
	public void run() {

		while(!this.isInterrupted()) {

			super.run();
			this.setStatisticMeasurement(getMeasurement());

			System.out.println("Measurement for "+Thread.currentThread().getName() + " with id : " + Thread.currentThread().getId()+" is : "
					+ ((LocationSensorMeasurementDTO)this.getStatisticMeasurement().getStatistic()).getPoint());

			synchronized (this.getStatisticsList()) {
				this.getStatisticsList().add(this.getStatisticMeasurement());
			}

			try {
				Thread.sleep(this.getPeriod());
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.out.println("\n\n"+Thread.currentThread().getName()+"/"+Thread.currentThread().getId()+" : "+"Caught me during Sleep\n\n");
				
			}
		}
		
	}


	public SensorMeasurementStatisticDTO getMeasurement() {

		LocationSensorMeasurementDTO measurement = new LocationSensorMeasurementDTO();
		measurement.setPoint(getLocationMeasurementValue(this.getMin(), this.getRange()));
		measurement.setSensor(getSensor());
		measurement.setTimestamp(new Date());
		this.getStatisticMeasurement().setStatistic(measurement);
		return getStatisticMeasurement();
	}


	private PointDTO getLocationMeasurementValue(int min, int range) {
		/*
		 * use range (choose proper type) to determine randomness of generated values
		 */
		Date seed = new Date();
		Random random = new Random(seed.getTime());
//		return new BigDecimal(min + (range * random.nextDouble()));
		return null;
	}
}
