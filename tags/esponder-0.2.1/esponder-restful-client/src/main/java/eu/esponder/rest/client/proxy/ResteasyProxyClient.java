package eu.esponder.rest.client.proxy;

import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.rest.generic.CrisisEntityResourceService;

public class ResteasyProxyClient {
	
	private static CrisisEntityResourceService client;
	
	static {
		/*
		 * this initialisation only needs to be done once per VM
		 */
		RegisterBuiltin.register(ResteasyProviderFactory.getInstance());


	}

	public ResteasyProxyClient() {
		
//		client = ProxyFactory.create(CrisisEntityResourceService.class, "http://localhost:8080/esponder-restful/crisis/generic/leoleis");
		client = ProxyFactory.create(CrisisEntityResourceService.class, "http://localhost:8080/esponder-restful");
    
	}

	public ResultListDTO invokeService(Long userID) {
		return client.testRestEasyProxy(new Long(1));
	}
}
