package eu.esponder.test.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.rest.client.ResteasyClient;

public class GenericCriteriaGetTest {
	private String GENERIC_ENTITY_URI = "http://localhost:8080/esponder-restful/crisis/generic";

	@Test
	public void getEntity() throws ClassNotFoundException, Exception {

		String serviceName = GENERIC_ENTITY_URI + "/get";
				
		ResteasyClient getClient = new ResteasyClient(serviceName, "application/xml");
		Map<String, String> params =  getCreateEntityServiceParameters();
		String resultXML = getClient.get(params);

		System.out.println("\n*********FULL_XML*******************************\n");
		System.out.println(resultXML+"\n\n");
	}


	private Map<String, String> getCreateEntityServiceParameters() {
		Map<String, String> params = new HashMap<String, String>();
		
		ArrayList<Long> idList = new ArrayList<Long>();
		idList.add(new Long(3));
		
		params.put("userID", "1");
		params.put("queriedEntityDTO",SketchPOIDTO.class.getName()); 
		params.put("idList", idList.toString());
		params.put("pageNumber", "0");
		params.put("pageSize", "10");
		return params;
	}

}
