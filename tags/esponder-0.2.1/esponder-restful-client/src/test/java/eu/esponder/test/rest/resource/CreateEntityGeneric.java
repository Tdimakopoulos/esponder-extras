package eu.esponder.test.rest.resource;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.view.HttpURLDTO;
import eu.esponder.dto.model.crisis.view.MapPointDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.rest.client.ResteasyClient;
import eu.esponder.util.jaxb.Parser;
import eu.esponder.util.logger.ESponderLogger;

public class CreateEntityGeneric {
	private String GENERIC_CREATE_ENTITY_URI = "http://localhost:8080/esponder-restful/crisis/generic";
	private String GETID_GENERIC_QUERY_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/resource/actor/getID";

	@Test
	public void CreateEntityWithXml () throws ClassNotFoundException, Exception {

		Parser parser = new Parser(new Class[] {SketchPOIDTO.class});
//		Parser parser = new Parser(new Class[] {ActorDTO.class});
//		ActorDTO actor = getActorByIDXml(new Long(2));
//		actor.setTitle("test"+Long.toHexString(Double.doubleToLongBits(Math.random())));
//		actor.setId(null);
		
		SketchPOIDTO sketch1 = new SketchPOIDTO();
		sketch1.setTitle("Sketch  test 1");
		sketch1.setPoints(new HashSet<MapPointDTO>());
		sketch1.getPoints().add(createMapPointDTO("pointtest11", 1, new BigDecimal(37.97219), new BigDecimal(23.72617)));
		sketch1.getPoints().add(createMapPointDTO("pointtest12", 1, new BigDecimal(37.97569), new BigDecimal(23.73712))); 	
		sketch1.getPoints().add(createMapPointDTO("pointtest13", 1, new BigDecimal(37.97499), new BigDecimal(23.72154))); 	
		sketch1.setHttpURL(createHttpURLDTO("192.168.1.1:8080", "testpath1", "http"));
		sketch1.setSketchType("SketchType1");
		System.out.println("Sketch created in client : " + sketch1.toString());
		
		String serviceName = GENERIC_CREATE_ENTITY_URI + "/create";

		ResteasyClient postClient = new ResteasyClient(serviceName, "application/xml");
		System.out.println("Client for createGenericEntity created successfully...");
		String xmlPayload = parser.marshall(sketch1);
		Map<String, String> params =  getCreateEntityServiceParameters();
		String resultXML = postClient.post(params, xmlPayload);
		printXML(resultXML);
		
		SketchPOIDTO sketchDTO = (SketchPOIDTO) parser.unmarshal(resultXML);

		System.out.println("\n*****************Sketch**************************");
		System.out.println(sketchDTO.toString());
		System.out.println("\n************************************************");
	}
	
	
	@Test
	public void CreateEntityWithJson () throws ClassNotFoundException, Exception {

		ActorDTO actor = getActorByIDJson(new Long(2));
		actor.setTitle("test"+Long.toHexString(Double.doubleToLongBits(Math.random())));
		actor.setId(null);
		actor.setEquipmentSet(null);
		
		String serviceName = GENERIC_CREATE_ENTITY_URI + "/create";

		ResteasyClient postClient = new ResteasyClient(serviceName, "application/json");
		System.out.println("Client for createGenericEntity created successfully...");
		
		ObjectMapper mapper = new ObjectMapper();
		String reqPayload = mapper.writeValueAsString(actor);
		printJSON(reqPayload);
		Map<String, String> params =  getCreateEntityServiceParameters();
		String resultXML = postClient.post(params, reqPayload);

		System.out.println("\n*********JSON*******************************\n");
		System.out.println(resultXML+"\n");
		System.out.println("\n*********JSON_END***************************\n");
	}


	private Map<String, String> getCreateEntityServiceParameters() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		return params;
	}
	
	@Test
	public ActorDTO getActorByIDXml(Long actorID) throws RuntimeException, Exception{

		ESponderLogger.debug(this.getClass(), "fetching initial actor object");
		Parser parser = new Parser(new Class[] {ActorDTO.class});

		ResteasyClient getClient = new ResteasyClient(GETID_GENERIC_QUERY_SERVICE_URI, "application/xml");
		Map<String, String> params = this.getIDServiceParameters(actorID);

		String resultXML = getClient.get(params);
		printXML(resultXML);

		ActorDTO actorDTO = (ActorDTO) parser.unmarshal(resultXML);
		System.out.println("\n*****************ACTOR**************************");
		System.out.println(actorDTO.toString());
		System.out.println("\n************************************************");
		
		return actorDTO;
	}
	
	@Test
	public ActorDTO getActorByIDJson(Long actorID) throws RuntimeException, Exception{


		ObjectMapper mapper = new ObjectMapper();
		ResteasyClient getClient = new ResteasyClient(GETID_GENERIC_QUERY_SERVICE_URI, "application/json");
		Map<String, String> params = this.getIDServiceParameters(actorID);

		ESponderLogger.debug(this.getClass(), "fetching initial actor object");
		String resultJson = getClient.get(params);
		printJSON(resultJson);

		ActorDTO actorDTO = mapper.readValue(resultJson, ActorDTO.class);
		
		System.out.println("\n*****************ACTOR**************************");
		System.out.println(actorDTO.toString());
		System.out.println("\n************************************************");
		
		return actorDTO;
	}
	
	private Map<String, String> getIDServiceParameters(Long actorID) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		params.put("actorID", actorID.toString());
		return params;
	}
	
	
	private void printJSON(String jsonStr) {
		System.out.println("\n\n******* JSON * START *******");
		System.out.println(jsonStr);
		System.out.println("******** JSON * END ********\n\n");
	}
	
	private void printXML(String jsonStr) {
		System.out.println("\n\n******* XML * START *******");
		System.out.println(jsonStr);
		System.out.println("******** XML * END ********\n\n");
	}

	
	private MapPointDTO createMapPointDTO(String title, Integer icon, BigDecimal latitude, BigDecimal longitude ) {
		MapPointDTO mapPoint = new MapPointDTO();
		mapPoint.setTitle(title);
		mapPoint.setIcon(icon);
		PointDTO point = new PointDTO();
		point.setLongitude(longitude);
		point.setLatitude(latitude);
		mapPoint.setPoint(point);
		return mapPoint;
	}
	
	private HttpURLDTO createHttpURLDTO(String host, String path, String protocol) {
		HttpURLDTO httpURL = new HttpURLDTO();
		httpURL.setHost(host);
		httpURL.setPath(path);
		httpURL.setProtocol(protocol);
		return httpURL;
	}
	
	
}
