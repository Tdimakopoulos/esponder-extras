package eu.esponder.osgi.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.annotations.Test;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.dto.model.snapshot.status.ActionSnapshotStatusDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;

public class PublisherTest {

	
	public static void main(String[] args) {
		ESponderEventPublisher<CreateActionSnapshotEvent> publisher = new ESponderEventPublisher<CreateActionSnapshotEvent>(CreateActionSnapshotEvent.class);

		try {
			//publisher.setEventTopic("createaction");
			publisher.publishEvent(createActionSnapshotEvent());
		} catch (EventListenerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void genericPublisherTest() {
		ESponderEventPublisher<CreateActionSnapshotEvent> publisher = new ESponderEventPublisher<CreateActionSnapshotEvent>(CreateActionSnapshotEvent.class);

		try {
			//publisher.setEventTopic("createaction");
			publisher.publishEvent(createActionSnapshotEvent());
		} catch (EventListenerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	public void sendSampleEvent() {
//		try {
//			// create simple event, it will be received from other connected
//			// clients
//			DictionaryInfo data = new DictionaryInfo();
//			data.put("event", createSampleEvent());
//
//			// send the event
//			String eventTopic = CreateActionEvent.class.getName();
//			eventService.event(eventTopic, data);
//			System.out.println("Event Sent: " + data);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/*public  ESponderEvent createActionSnapshotEvent() {
	    BodyTemperatureSensorDTO bodyTemperatureSensor = new BodyTemperatureSensorDTO();
	    bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
	    bodyTemperatureSensor.setName("Body Temperature");
	    bodyTemperatureSensor.setType("BodyTemp");
	    bodyTemperatureSensor.setLabel("BdTemp");
	    bodyTemperatureSensor.setTitle("Body Temperature Sensor");
	    bodyTemperatureSensor.setId(System.currentTimeMillis());
	    bodyTemperatureSensor.setStatus(ResourceStatusDTO.AVAILABLE);
	    bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);

	    ArithmeticSensorMeasurementDTO sensorMeasurement1 = new ArithmeticSensorMeasurementDTO();
	    sensorMeasurement1.setMeasurement(new BigDecimal(200));
	    sensorMeasurement1.setId(99L);
	    sensorMeasurement1.setTimestamp(new Date());
	    sensorMeasurement1.setSensor(bodyTemperatureSensor);

	    SensorMeasurementStatisticDTO statisticArethmetic = new SensorMeasurementStatisticDTO();
	    statisticArethmetic.setStatistic(sensorMeasurement1);
	    statisticArethmetic.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
	    sensorMeasurement1.setId(89L);

	    List<SensorMeasurementStatisticDTO> sensorMeasurementStatistic = new ArrayList<SensorMeasurementStatisticDTO>();
	    sensorMeasurementStatistic.add(statisticArethmetic);

	    SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
	    sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatistic);
	    sensorMeasurementStatisticEnvelope.setId(System.currentTimeMillis());

	    ActorDTO subactorDTO = new ActorDTO();
	    subactorDTO.setId(new Long(2));
	    subactorDTO.setType("FRC 2222");
	    subactorDTO.setTitle("FRC #2222");
	    subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);

	    ESponderEvent<SensorMeasurementStatisticEnvelopeDTO> statisticEvent2 = new CreateSensorMeasurementStatisticEvent();
	    statisticEvent2.setEventSeverity(SeverityLevelDTO.SERIOUS);
	    statisticEvent2.setEventTimestamp(new Date());
	    statisticEvent2.setEventAttachment(sensorMeasurementStatisticEnvelope);
	    statisticEvent2.setEventSource(subactorDTO);
	    
	    return statisticEvent2;
	  }*/
	private static ESponderEvent<? extends ESponderEntityDTO> createActionSnapshotEvent() {
		ActionSnapshotDTO actionSnapshot = new ActionSnapshotDTO();
		ActorDTO subactorDTO = new ActorDTO();
	    subactorDTO.setId(new Long(2));
	    subactorDTO.setType("FRC 2222");
	    subactorDTO.setTitle("FRC #2222");
	    subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		actionSnapshot.setStatus(ActionSnapshotStatusDTO.STARTED);
		actionSnapshot.setId(new Long(10));
		
		CreateActionSnapshotEvent result = new CreateActionSnapshotEvent();
		result.setEventAttachment(actionSnapshot);
		result.setJournalMessage("Test Event");
		result.setEventSeverity(SeverityLevelDTO.SERIOUS);
		result.setEventTimestamp(new Date());
		result.setEventSeverity(SeverityLevelDTO.UNDEFINED);
		result.setEventSource(subactorDTO);
		return result;
	}
}
