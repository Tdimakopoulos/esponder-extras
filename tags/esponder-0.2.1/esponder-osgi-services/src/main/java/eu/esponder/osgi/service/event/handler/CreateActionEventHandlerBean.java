package eu.esponder.osgi.service.event.handler;

import eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.event.model.snapshot.action.ActionSnapshotEvent;
import eu.esponder.osgi.service.event.EventHandlerBean;

public class CreateActionEventHandlerBean extends EventHandlerBean<ActionSnapshotEvent<? extends ActionSnapshotDTO>> {
	
	DatafusionControllerRemoteService pEventManager ;
	
	@Override
	public void onReceive(ActionSnapshotEvent<? extends ActionSnapshotDTO> event) {
		
		pEventManager.EsponderEventReceivedHandler(event);
		System.out.println("CreateActionEventHandlerBean has been invoked");
	}

}
