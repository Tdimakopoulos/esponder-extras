package eu.esponder.osgi.service.event;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.crisis.action.CreateActionPartEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionPartSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;

@Stateless
public class EventServiceManagerBean implements EventServiceManager {

	private static Map<Class<? extends ESponderEvent<? extends ESponderEntityDTO>>, ESponderEventListener<? extends ESponderEvent<? extends ESponderEntityDTO>>> listeners 
			= new HashMap<Class<? extends ESponderEvent<? extends ESponderEntityDTO>>, ESponderEventListener<? extends ESponderEvent<? extends ESponderEntityDTO>>>();

	private static Map<Class<? extends ESponderEvent<? extends ESponderEntityDTO>>, ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>>> publishers 
			= new HashMap<Class<? extends ESponderEvent<? extends ESponderEntityDTO>>, ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>>>();

	static {
		
		ESponderEventPublisher<CreateActionSnapshotEvent> publisher = new ESponderEventPublisher<CreateActionSnapshotEvent>(CreateActionSnapshotEvent.class);
		createPublisher(publisher);
		
		ESponderEventListener<CreateActionSnapshotEvent> listener = new ESponderEventListener<CreateActionSnapshotEvent>(CreateActionSnapshotEvent.class);
		createListener(listener);
		
		ESponderEventListener<CreateCrisisContextEvent> listener2 = new ESponderEventListener<CreateCrisisContextEvent>(CreateCrisisContextEvent.class);
		createListener(listener2);
		
		ESponderEventListener<CreateActionEvent> listener3 = new ESponderEventListener<CreateActionEvent>(CreateActionEvent.class);
		createListener(listener3);
		
		ESponderEventListener<CreateActionPartEvent> listener4 = new ESponderEventListener<CreateActionPartEvent>(CreateActionPartEvent.class);
		createListener(listener4);
		
		ESponderEventListener<CreateSensorMeasurementEnvelopeEvent> listener5 = new ESponderEventListener<CreateSensorMeasurementEnvelopeEvent>(CreateSensorMeasurementEnvelopeEvent.class);
		createListener(listener5);
		
		ESponderEventListener<UpdateActionSnapshotEvent> listener6 = new ESponderEventListener<UpdateActionSnapshotEvent>(UpdateActionSnapshotEvent.class);
		createListener(listener6);
		
		ESponderEventListener<UpdateActionPartSnapshotEvent> listener7 = new ESponderEventListener<UpdateActionPartSnapshotEvent>(UpdateActionPartSnapshotEvent.class);
		createListener(listener7);
		
		ESponderEventListener<CreateSensorMeasurementStatisticEvent> listener8 = new ESponderEventListener<CreateSensorMeasurementStatisticEvent>(CreateSensorMeasurementStatisticEvent.class);
		createListener(listener8);
	}

	public static void createListener(ESponderEventListener<? extends ESponderEvent<? extends ESponderEntityDTO>> listener) {
		listeners.put(listener.getEventTopicClass(), listener);
	}
	
	public static void createPublisher(ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>> publisher) {
		publishers.put(publisher.getEventTopicClass(), publisher);
	}
	
	public ESponderEventListener<? extends ESponderEvent<? extends ESponderEntityDTO>> getListener(Class<? extends ESponderEvent<? extends ESponderEntityDTO>> eventClass) {
		return listeners.get(eventClass);
	}

	public ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>> getPublisher(Class<? extends ESponderEvent<? extends ESponderEntityDTO>> eventClass) {
		return publishers.get(eventClass);
	}
	
}
