package eu.esponder.osgi.service.event;

import javax.ejb.Stateless;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;

@Stateless
public abstract class EventHandlerBean<T extends ESponderEvent<? extends ESponderEntityDTO>>
		implements EventHandlerService<T> {

	/*
	 * Handle the event as appropriate for each event type T
	 */
	public void onReceive(T event) {
		System.out.println("EventHandlerBean has been invoked");
	}

}
