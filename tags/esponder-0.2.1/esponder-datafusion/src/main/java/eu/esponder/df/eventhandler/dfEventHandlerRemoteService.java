package eu.esponder.df.eventhandler;



import javax.ejb.Remote;

import eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.event.model.ESponderEvent;

@Remote
public interface dfEventHandlerRemoteService extends dfEventHandlerService{

	public void AddDTOObjects(Object fact);
	public void AddObjects(Object fact);
	public void LoadKnowledge();
	public void ProcessRules();
	public void SetRuleEngineType(RuleEngineType dType,String szPackageNameOrFlowName);
	public void SetRuleEngineType(RuleEngineType dType,String szDSLName,String szDSLRName);
	public SensorSnapshotDTO CreateSensorSnapshot(ESponderEvent<?> pEvent);
}
