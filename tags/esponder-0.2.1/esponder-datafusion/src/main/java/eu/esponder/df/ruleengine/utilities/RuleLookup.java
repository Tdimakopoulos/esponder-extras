package eu.esponder.df.ruleengine.utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import eu.esponder.df.ruleengine.utilities.actorlookup.ActorConfigurationXML;
import eu.esponder.df.ruleengine.utilities.actorlookup.ActorXMLFileEntry;

public class RuleLookup {


	List<ActorXMLFileEntry> pEntries = new LinkedList<ActorXMLFileEntry>();
	
	public enum RuleLookupType {
	    DLU_ACTORS, DLU_ERRORS
	}
	
	public void Initialize()
	{
		pEntries = new ArrayList<ActorXMLFileEntry>();
	}
	
	public void AddXMLEntry(String szElement1,String szElement2,int iElement1,int iElement2)
	{
		ActorXMLFileEntry pEntry=new ActorXMLFileEntry();
		pEntry.setSzString1(szElement1);
		pEntry.setSzString2(szElement2);
		pEntry.setiInt1(iElement1);
		pEntry.setiInt2(iElement2);
		pEntries.add(pEntry);
	}
	
	public void ReadXMLFile(RuleLookupType LookupType,String szFilename) throws Exception
	{
		if (LookupType==RuleLookupType.DLU_ACTORS)
		{
			ReadActorXMLFile(szFilename);
		}
	}
	
	private void ReadActorXMLFile(String fileLocation) throws Exception
	{
		Serializer serializer = new Persister();
		File source = new File(fileLocation);
		ActorConfigurationXML configurator = serializer.read(ActorConfigurationXML.class, source);
	}

	private void WriteActorXMLFile(String fileLocation) throws Exception
	{
		Serializer serializer = new Persister();

		File result = new File("example.xml");
	
		serializer.write(pEntries, result);
	}
}
