package eu.esponder.df.ruleengine.utilities.actorlookup;

import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;


@Root
public class ActorConfigurationXML {

   @ElementList(inline=true)
   private List<ActorXMLFileEntry> entries;

   public void setEntries(List<ActorXMLFileEntry> entries) {
	this.entries = entries;
}

public List<ActorXMLFileEntry> getEntries() { 
      return entries;
   }
}