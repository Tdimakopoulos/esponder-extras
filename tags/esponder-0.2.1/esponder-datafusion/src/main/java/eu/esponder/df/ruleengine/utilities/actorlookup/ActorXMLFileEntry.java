package eu.esponder.df.ruleengine.utilities.actorlookup;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class ActorXMLFileEntry {

	@Element
	private String szString1;
	@Element
	private String szString2;
	@Element
	private int iInt1;
	@Element
	private int iInt2;
	public String getSzString1() {
		return szString1;
	}
	public void setSzString1(String szString1) {
		this.szString1 = szString1;
	}
	public String getSzString2() {
		return szString2;
	}
	public void setSzString2(String szString2) {
		this.szString2 = szString2;
	}
	public int getiInt1() {
		return iInt1;
	}
	public void setiInt1(int iInt1) {
		this.iInt1 = iInt1;
	}
	public int getiInt2() {
		return iInt2;
	}
	public void setiInt2(int iInt2) {
		this.iInt2 = iInt2;
	}

	

	
}
