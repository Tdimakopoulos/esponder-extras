package eu.esponder.df.rules.profile;

import eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType;

public class ProfileData {

	private String szProfileName;
	public String getSzProfileName() {
		return szProfileName;
	}
	public void setSzProfileName(String szProfileName) {
		this.szProfileName = szProfileName;
	}
	public RuleEngineType getSzProfileType() {
		return szProfileType;
	}
	public void setSzProfileType(RuleEngineType szProfileType) {
		this.szProfileType = szProfileType;
	}
	private RuleEngineType szProfileType;
	
}
