/**
 * Controller
 * 
 * This Java class implement a controller for the rules engine, this is the class that must be called to run 
 * the rule engine, support DRL, DSL and DSLR type of assets. And Functions but need to be inline the rule.
 * 
 * 
 * @Project   esponder
 * @package   Datafusion
 */
package eu.esponder.df.ruleengine.controller;

import javax.ejb.Local;

import eu.esponder.event.model.ESponderEvent;


@Local
public interface DatafusionControllerService {

	public void EsponderEventReceivedHandler(ESponderEvent<?> pEvent);
}

/*
	public static void main(String args[]) {

		
		
		
		Date date = new Date(1000);
		RuleUtilities dutils = new RuleUtilities();
		PeriodDTO period = new PeriodDTO(date, date);
		Controller dEngine = new Controller();
		SensorDTO sensor = null;

		sensor = new BodyTemperatureSensorDTO();

		sensor.setType("title");
		sensor.setTitle("title");
		sensor.setStatus(ResourceStatusDTO.ALLOCATED);
		sensor.setEquipmentId((long) 1);

		SensorSnapshotDTO snapshot = new SensorSnapshotDTO();
		snapshot.setSensor(sensor);
		snapshot.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
		snapshot.setValue("35");
		snapshot.setPeriod(period);
		snapshot.setStatus(SensorSnapshotStatusDTO.WORKING);

		//dEngine.AddKnowledgeByPackage("esponder");

		//dEngine.AddDTOObjects(dutils);
		//dEngine.AddDTOObjects(snapshot);

		//dEngine.ExecuteRules();

	}


*/