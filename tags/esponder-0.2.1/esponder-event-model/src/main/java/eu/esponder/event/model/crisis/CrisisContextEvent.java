package eu.esponder.event.model.crisis;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.event.model.ESponderEvent;


public abstract class CrisisContextEvent<T extends CrisisContextDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = 2806433512434184568L;

}
