package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateConsumableResourceEvent extends ConsumableResourceEvent<ConsumableResourceDTO> implements UpdateEvent {

	private static final long serialVersionUID = -7576724843419044712L;

}
