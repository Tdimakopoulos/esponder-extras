package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.event.model.UpdateEvent;

public class UpdateResourceEquipmentEvent extends ResourceEquipmentEvent<EquipmentDTO> implements UpdateEvent {

	private static final long serialVersionUID = 5017996688146629527L;
	
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
