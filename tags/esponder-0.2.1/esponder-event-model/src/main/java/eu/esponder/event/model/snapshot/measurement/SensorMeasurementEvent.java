package eu.esponder.event.model.snapshot.measurement;

import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.event.model.ESponderEvent;

public abstract class SensorMeasurementEvent<T extends SensorMeasurementDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = 737153233164428505L;

}
