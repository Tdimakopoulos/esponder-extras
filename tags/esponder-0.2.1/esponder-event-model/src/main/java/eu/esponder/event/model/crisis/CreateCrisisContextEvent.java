package eu.esponder.event.model.crisis;




import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.event.model.CreateEvent;


public class CreateCrisisContextEvent extends CrisisContextEvent<CrisisContextDTO> implements CreateEvent {

	private static final long serialVersionUID = 5728175601548401117L;

	@Override
	public String getJournalMessageInfo() {
		// TODO Auto-generated method stub
		String JournalMessageInfo="CrisisContextEvent Journal Info";
		return JournalMessageInfo;
	}
		
}
