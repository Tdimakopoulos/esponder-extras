package eu.esponder.event.model.snapshot.measurement;

import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;

public abstract class LocationSensorMeasurementEvent<T extends LocationSensorMeasurementDTO> extends SensorMeasurementEvent<T> {

	private static final long serialVersionUID = 293860720150680105L;

}
