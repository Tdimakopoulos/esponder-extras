package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.event.model.DeleteEvent;


public class DeleteConsumableResourceEvent extends ConsumableResourceEvent<ConsumableResourceDTO> implements DeleteEvent {

	private static final long serialVersionUID = 1886755992124247660L;

}
