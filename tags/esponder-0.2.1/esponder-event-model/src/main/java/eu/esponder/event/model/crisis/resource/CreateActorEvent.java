package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.event.model.CreateEvent;


public class CreateActorEvent extends ActorEvent<ActorDTO> implements CreateEvent {

	private static final long serialVersionUID = 6318868022859370822L;
	
}
