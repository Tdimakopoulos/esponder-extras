package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.event.model.CreateEvent;


public class CreateResourceEquipmentEvent extends ResourceEquipmentEvent<EquipmentDTO> implements CreateEvent {

	private static final long serialVersionUID = 1002815036463481211L;
	
	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
