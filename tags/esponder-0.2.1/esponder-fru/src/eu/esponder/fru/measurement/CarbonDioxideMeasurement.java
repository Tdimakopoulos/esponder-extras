package eu.esponder.fru.measurement;

public class CarbonDioxideMeasurement extends SensorMeasurement {

	private double value;

	public CarbonDioxideMeasurement(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	public void setTimestamp(double value) {
		this.value = value;
	}

}
