package eu.esponder.model.snapshot.status;

public enum EquipmentSnapshotStatus {
	WORKING,
	MALFUNCTIONING,
	DAMAGED
}
