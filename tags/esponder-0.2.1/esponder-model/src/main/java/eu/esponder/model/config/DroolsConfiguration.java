package eu.esponder.model.config;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("DROOLS")
public class DroolsConfiguration extends ESponderConfigParameter {

	private static final long serialVersionUID = 1472802157259896819L;
	
}
