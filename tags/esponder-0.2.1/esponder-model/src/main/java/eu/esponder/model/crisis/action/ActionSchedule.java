package eu.esponder.model.crisis.action;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;

@Entity
@Table(name="action_schedule")
public class ActionSchedule extends ESponderEntity<Long> {

	private static final long serialVersionUID = 6440269024338454840L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_SCHEDULE_ID")
	private Long id;
	
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	@ManyToOne
	@JoinColumn(name="ACTION_ID", nullable=false)
	private Action action;
	
	@ManyToOne
	@JoinColumn(name="PREREQUISITE_ACTION_ID", nullable=false)
	private Action prerequisiteAction;
	
	@Embedded
	private ActionScheduleCriteria criteria;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Action getPrerequisiteAction() {
		return prerequisiteAction;
	}

	public void setPrerequisiteAction(Action prerequisiteAction) {
		this.prerequisiteAction = prerequisiteAction;
	}

	public ActionScheduleCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(ActionScheduleCriteria criteria) {
		this.criteria = criteria;
	}
	
}


