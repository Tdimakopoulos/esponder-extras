package eu.esponder.model.crisis.view;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@DiscriminatorValue("REF")
@NamedQueries({
	@NamedQuery(name="ReferencePOI.findByOperationsCentre", query="select r from ReferencePOI r where r.operationsCentre.id=:operationsCentreID"),
	@NamedQuery(name="ReferencePOI.findByReferencePOIId", query="select s from ReferencePOI s where s.id=:referencePOIId"),
	@NamedQuery(name="ReferencePOI.findByTitle", query="select s from ReferencePOI s where s.title=:referencePOITitle")
})
public class ReferencePOI extends ResourcePOI {
	
	private static final long serialVersionUID = 7856655848723729892L;
	
	@Column(name="REF_FILE")
	private String referenceFile;
	
	@Column(name="AVERAGE_SIZE")
	private Float averageSize;

	public String getReferenceFile() {
		return referenceFile;
	}

	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

	public Float getAverageSize() {
		return averageSize;
	}

	public void setAverageSize(Float averageSize) {
		this.averageSize = averageSize;
	}

	
}
