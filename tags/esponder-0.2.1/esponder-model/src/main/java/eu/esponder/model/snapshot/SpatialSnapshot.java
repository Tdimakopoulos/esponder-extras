package eu.esponder.model.snapshot;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

import eu.esponder.model.snapshot.location.LocationArea;

@MappedSuperclass
public abstract class SpatialSnapshot<T> extends Snapshot<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6994801801906839319L;
	
	@OneToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="LOCATION_AREA_ID")
	protected LocationArea locationArea;

	public LocationArea getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationArea locationArea) {
		this.locationArea = locationArea;
	}
	
}
