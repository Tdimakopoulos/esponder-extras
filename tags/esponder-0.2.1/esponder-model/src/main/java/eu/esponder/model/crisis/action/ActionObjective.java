package eu.esponder.model.crisis.action;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.location.LocationArea;

@Entity
@Table(name="action_objective")
@NamedQueries({
	@NamedQuery(name="ActionObjective.findByTitle", query="select ap from ActionObjective ap where ap.title=:title")
})
public class ActionObjective extends ESponderEntity<Long> {

	private static final long serialVersionUID = 284141105295838636L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_OBJECTIVE_ID")
	private Long id;
	
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	@Embedded
	private Period period;
	
	@ManyToOne
	@JoinColumn(name="LOCATION_AREA_ID")
	private LocationArea locationArea;
	
	@ManyToOne
	@JoinColumn(name="ACTION_ID", nullable=false)
	private Action action;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public LocationArea getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationArea locationArea) {
		this.locationArea = locationArea;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}
	
}
