package eu.esponder.model.crisis.action;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.ReusableResource;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.location.LocationArea;

@Entity
@Table(name="action_part_objective")
@NamedQueries({
	@NamedQuery(name="ActionPartObjective.findByTitle", query="select ap from ActionPartObjective ap where ap.title=:title")
})
public class ActionPartObjective extends ESponderEntity<Long> {

	private static final long serialVersionUID = -3441505897036501908L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_PART_OBJECTIVE_ID")
	private Long id;
	
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	@Embedded
	private Period period;
	
	@ManyToOne
	@JoinColumn(name="LOCATION_AREA_ID", nullable=false)
	private LocationArea locationArea;
	
	@ManyToOne
	@JoinColumn(name="ACTION_PART_ID", nullable=false)
	private ActionPart actionPart;

	/*
	 * These resources are part of the action part's target. For example the "food packages" 
	 * in the following sentence: Move these food-packages using a truck
	 */
	@OneToMany(mappedBy="actionPart")
	private Set<ConsumableResource> consumableResources;

	/*
	 *  These resources are part of the action part's target. For example the "telecom equipment" 
	 *  in the following sentence: Move this telecom equipment using a truck
	 */
	@OneToMany(mappedBy="actionPart")
	private Set<ReusableResource> reusableResources;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public LocationArea getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationArea locationArea) {
		this.locationArea = locationArea;
	}

	public ActionPart getActionPart() {
		return actionPart;
	}

	public void setActionPart(ActionPart actionPart) {
		this.actionPart = actionPart;
	}

	public Set<ConsumableResource> getConsumableResources() {
		return consumableResources;
	}

	public void setConsumableResources(Set<ConsumableResource> consumableResources) {
		this.consumableResources = consumableResources;
	}

	public Set<ReusableResource> getReusableResources() {
		return reusableResources;
	}

	public void setReusableResources(Set<ReusableResource> reusableResources) {
		this.reusableResources = reusableResources;
	}

}
