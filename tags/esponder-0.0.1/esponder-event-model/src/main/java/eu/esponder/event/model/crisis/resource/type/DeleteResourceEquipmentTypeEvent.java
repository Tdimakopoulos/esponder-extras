package eu.esponder.event.model.crisis.resource.type;

import eu.esponder.event.model.DeleteEvent;


public class DeleteResourceEquipmentTypeEvent extends ResourceEquipmentTypeEvent implements DeleteEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7343879352291213669L;
	
}
