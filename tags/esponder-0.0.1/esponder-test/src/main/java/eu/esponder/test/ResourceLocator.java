package eu.esponder.test;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ResourceLocator {
	
	private static Context ctx;
	
	static {
		Properties properties = new Properties();
        properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
        properties.put("java.naming.factory.url.pkgs", "org.jboss.naming rg.jnp.interfaces");
        properties.put("java.naming.provider.url", "jnp://localhost:1099");
        try {
			ctx = new InitialContext(properties);
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T lookup(String name) {
        try {
        	Object obj  = ctx.lookup(name);
        	System.out.println("lookup returned " + obj);
            return (T) ctx.lookup(name);
        } catch(NamingException e) {
            throw new RuntimeException(e);
        }
    }
	
}
