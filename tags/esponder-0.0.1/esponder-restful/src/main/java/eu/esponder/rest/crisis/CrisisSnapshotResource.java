package eu.esponder.rest.crisis;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.collections.CollectionUtils;

import eu.esponder.jaxb.model.crisis.resource.ActorDTO;
import eu.esponder.jaxb.model.crisis.resource.EquipmentDTO;
import eu.esponder.jaxb.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.jaxb.model.crisis.resource.SensorDTO;
import eu.esponder.jaxb.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.rest.ESponderResource;
import eu.esponder.util.rest.StringParamUnmarshaller.DateFormat;


@Path("/crisis/snapshot")
public class CrisisSnapshotResource extends ESponderResource {
	
	@GET()
	@Path("/oc")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public OperationsCentreDTO getSnapshotByDate(
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null")  Long operationsCentreID, 
			@QueryParam("maxDate") @DateFormat("yyyy-MM-dd'T'HH:mm:ss") @NotNull(message="maxDate may not be null") Date maxDate, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		OperationsCentreDTO operationsCentre = this.getMappingService().mapOperationsCentre(operationsCentreID, userID);
		if (null != operationsCentre) {
			setOperationsCentreSnapshot(operationsCentre, maxDate);
			return operationsCentre;
		}
		return null;
	}
	
	//-------------------------------------------------------------------------
	
	private void setOperationsCentreSnapshot(OperationsCentreDTO operationsCentre, Date maxDate) {

		if (CollectionUtils.isNotEmpty(operationsCentre.getSubordinates())) {
			for (OperationsCentreDTO subordinate : operationsCentre.getSubordinates()) {
				setOperationsCentreSnapshot(subordinate, maxDate);
			}
		}
		OperationsCentreSnapshotDTO snapshot = this.getMappingService().mapOperationsCentreSnapshot(operationsCentre.getId(), maxDate);
		operationsCentre.setSnapshot(snapshot);
		for (ActorDTO actor : operationsCentre.getActors()) {
			setActorSnapshot(actor, maxDate);
		}
	}
	
	//-------------------------------------------------------------------------
	
	private void setActorSnapshot(ActorDTO actor, Date maxDate) {
		
		if (CollectionUtils.isNotEmpty(actor.getSubordinates())) {
			for (ActorDTO subordinate : actor.getSubordinates()) {
				setActorSnapshot(subordinate, maxDate);
			}
		}
		ActorSnapshotDTO snapshot = this.getMappingService().mapActorSnapshot(actor.getId(), maxDate);
		actor.setSnapshot(snapshot);
		for (EquipmentDTO equipment : actor.getEquipmentSet()) {
			setEquipmentSnapshot(equipment, maxDate);
		}
	}
	
	//-------------------------------------------------------------------------
	
	private void setEquipmentSnapshot(EquipmentDTO equipment, Date maxDate) {
		
		EquipmentSnapshotDTO snapshot = this.getMappingService().mapEquipmentSnapshot(equipment.getId(), maxDate);
		equipment.setSnapshot(snapshot);
		for (SensorDTO sensor : equipment.getSensors()) {
			setSensorSnapshot(sensor, maxDate);
		}
	}
	
	//-------------------------------------------------------------------------
	
	private void setSensorSnapshot(SensorDTO sensor, Date maxDate) {
		
		SensorSnapshotDTO snapshot =this.getMappingService().mapSensorSnapshot(sensor.getId(), maxDate);
		sensor.setSnapshot(snapshot);
	}
	
}
