package eu.esponder.rest.crisis;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.jaxb.model.crisis.view.ReferencePOIDTO;
import eu.esponder.jaxb.model.crisis.view.SketchPOIDTO;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.view.ReferencePOI;
import eu.esponder.model.crisis.view.SketchPOI;
import eu.esponder.rest.ESponderResource;


@Path("/crisis/view")
public class CrisisViewResource extends ESponderResource {

	@GET
	@Path("/sketch/read")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public List<SketchPOIDTO> readSketches(
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		return this.getMappingService().mapOperationsCentreSketches(operationsCentreID, userID);
	}
	
	@POST
	@Path("/sketch/create")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public void createSketch(
			@NotNull(message="sketch object may not be null") SketchPOIDTO sketchDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		OperationsCentre operationsCentre = this.getOperationsCentreService().findByUser(operationsCentreID, userID);
		if (null != operationsCentre) {
			SketchPOI sketch = this.getMappingService().mapSketch(sketchDTO);
			this.getOperationsCentreService().createSketch(operationsCentre, sketch, userID);
		}
	}
	
	@GET
	@Path("/ref/read")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public List<ReferencePOIDTO> readReferencePOIs(
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		return this.getMappingService().mapOperationsCentreReferencePOIs(operationsCentreID, userID);
	}
	
	@POST
	@Path("/ref/create")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public void createReferencePOI(
			@NotNull(message="referencePOI object may not be null") ReferencePOIDTO referencePOIDTO,
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID) {
		
		OperationsCentre operationsCentre = this.getOperationsCentreService().findByUser(operationsCentreID, userID);
		if (null != operationsCentre) {
			ReferencePOI referencePOI = this.getMappingService().mapReferencePOI(referencePOIDTO);
			this.getOperationsCentreService().createReferencePOI(operationsCentre, referencePOI, userID);
		}
	}
	
}
