package eu.esponder.jaxb.model.crisis.resource;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.snapshot.resource.ActorSnapshotDTO;

@XmlRootElement(name="actor")
@XmlType(name="Actor")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "equipmentSet", "subordinates", "snapshot"})
public class ActorDTO extends ResourceDTO {
	
	private Set<EquipmentDTO> equipmentSet;
	
	private Set<ActorDTO> subordinates;
	
	private ActorSnapshotDTO snapshot;

	public Set<EquipmentDTO> getEquipmentSet() {
		return equipmentSet;
	}

	public void setEquipmentSet(Set<EquipmentDTO> equipmentSet) {
		this.equipmentSet = equipmentSet;
	}

	public Set<ActorDTO> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(Set<ActorDTO> subordinates) {
		this.subordinates = subordinates;
	}

	public ActorSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(ActorSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	@Override
	public String toString() {
		return "ActorDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}
	
}
