package eu.esponder.jaxb.model.crisis.resource;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.snapshot.resource.SensorSnapshotDTO;

@XmlRootElement(name="sensor")
@XmlType(name="Sensor")
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "measurementUnit", "snapshot"})
public class SensorDTO extends ResourceDTO {
	
	private String measurementUnit;
	
	private SensorSnapshotDTO snapshot;

	public String getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	public SensorSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(SensorSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	@Override
	public String toString() {
		return "SensorDTO [type=" + type + ", measurementUnit="
				+ measurementUnit + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}
	
}
