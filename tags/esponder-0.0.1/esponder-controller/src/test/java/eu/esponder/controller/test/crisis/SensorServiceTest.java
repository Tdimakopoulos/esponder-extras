package eu.esponder.controller.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.model.crisis.resource.Equipment;
import eu.esponder.model.crisis.resource.ResourceStatus;
import eu.esponder.model.crisis.resource.Sensor;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.snapshot.status.SensorSnapshotStatus;
import eu.esponder.model.type.SensorType;

public class SensorServiceTest extends ControllerServiceTest {
	
	private static int SECONDS = 10;
	
	@Test(groups="createResources")
	public void testCreateSensor() {
		
		SensorType temperature = (SensorType) typeService.findByTitle("TEMP");
		SensorType heartbeat = (SensorType) typeService.findByTitle("HB");
		
		Equipment firstFRCEquipment = equipmentService.findByTitle("FRU #1");
		Equipment firstFREquipment = equipmentService.findByTitle("FRU #1.1");
		Equipment secondFREquipment = equipmentService.findByTitle("FRU #1.2");
		Equipment secondFRCEquipment = equipmentService.findByTitle("FRU #2");
		Equipment thirdFREquipment = equipmentService.findByTitle("FRU #2.1");
		Equipment fourthFREquipment = equipmentService.findByTitle("FRU #2.2");
		
		createSensor(temperature, "FRU #1 TEMP.", firstFRCEquipment);
		createSensor(heartbeat, "FRU #1 HB.", firstFRCEquipment);
		createSensor(temperature, "FRU #1.1 TEMP.", firstFREquipment);
		createSensor(heartbeat, "FRU #1.1 HB.", firstFREquipment);
		createSensor(temperature, "FRU #1.2 TEMP.", secondFREquipment);
		createSensor(heartbeat, "FRU #1.2 HB.", secondFREquipment);
		createSensor(temperature, "FRU #2 TEMP.", secondFRCEquipment);
		createSensor(heartbeat, "FRU #2 HB.", secondFRCEquipment);
		createSensor(temperature, "FRU #2.1 TEMP.", thirdFREquipment);
		createSensor(heartbeat, "FRU #2.1 HB.", thirdFREquipment);
		createSensor(temperature, "FRU #2.2 TEMP.", fourthFREquipment);
		createSensor(heartbeat, "FRU #2.2 HB.", fourthFREquipment);
	}
	
	@Test(groups="createSnapshots")
	public void testCreateSensorSnapshots() {		
		
		Sensor firstFRCTemperature = sensorService.findByTitle("FRU #1 TEMP.");
		Sensor firstFRCHeartbeat = sensorService.findByTitle("FRU #1 HB.");
		Sensor firstFRTemperature = sensorService.findByTitle("FRU #1.1 TEMP.");
		Sensor firstFRHeartbeat = sensorService.findByTitle("FRU #1.1 HB.");
		Sensor secondFRTemperature = sensorService.findByTitle("FRU #1.2 TEMP.");
		Sensor secondFRHeartbeat = sensorService.findByTitle("FRU #1.2 HB.");
		Sensor secondFRCTemperature = sensorService.findByTitle("FRU #2 TEMP.");
		Sensor secondFRCHeartbeat = sensorService.findByTitle("FRU #2 HB.");
		Sensor thirdFRTemperature = sensorService.findByTitle("FRU #2.1 TEMP.");
		Sensor thirdFRHeartbeat = sensorService.findByTitle("FRU #2.1 HB.");
		Sensor fourthFRTemperature = sensorService.findByTitle("FRU #2.2 TEMP.");
		Sensor fourthFRHeartbeat = sensorService.findByTitle("FRU #2.2 HB.");
		
		Period period = this.createPeriod(SECONDS);
		createSensorSnapshot(firstFRCTemperature, "30", period);
		createSensorSnapshot(firstFRCHeartbeat, "80", period);
		createSensorSnapshot(firstFRTemperature, "31", period);
		createSensorSnapshot(firstFRHeartbeat, "70", period);
		createSensorSnapshot(secondFRTemperature, "31", period);
		createSensorSnapshot(secondFRHeartbeat, "85", period);
		createSensorSnapshot(secondFRCTemperature, "35", period);
		createSensorSnapshot(secondFRCHeartbeat, "79", period);
		createSensorSnapshot(thirdFRTemperature, "36", period);
		createSensorSnapshot(thirdFRHeartbeat, "74", period);
		createSensorSnapshot(fourthFRTemperature, "34", period);
		createSensorSnapshot(fourthFRHeartbeat, "84", period);
	}
	
	private Sensor createSensor(SensorType type, String title, Equipment equipment) {
		Sensor sensor = new Sensor();
		sensor.setSensorType(type);
		sensor.setTitle(title);
		sensor.setStatus(ResourceStatus.ALLOCATED);
		sensor.setEquipment(equipment);
		return sensorService.create(sensor, this.userID);
	}
	
	private SensorSnapshot createSensorSnapshot(Sensor sensor, String meanValue, Period period) {
		SensorSnapshot snapshot = new SensorSnapshot();
		snapshot.setSensor(sensor);
		snapshot.setMeanValue(meanValue);
		snapshot.setPeriod(period);
		snapshot.setStatus(SensorSnapshotStatus.WORKING);
		return sensorService.createSnapshot(snapshot, this.userID);
	}
	
}
