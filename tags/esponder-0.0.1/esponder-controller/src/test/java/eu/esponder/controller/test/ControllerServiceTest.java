package eu.esponder.controller.test;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.BeforeClass;

import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.location.Point;
import eu.esponder.model.snapshot.location.Sphere;
import eu.esponder.model.user.ESponderUser;
import eu.esponder.test.ResourceLocator;

public class ControllerServiceTest {

	protected static final String USER_NAME = "ctri";
	protected Long userID;
	
	protected ActorRemoteService actorService;
	protected EquipmentRemoteService equipmentService;
	protected OperationsCentreRemoteService operationsCentreService;
	protected SensorRemoteService sensorService;
	protected TypeRemoteService typeService;
	protected UserRemoteService userService;
		
	@BeforeClass(alwaysRun=true)
	public void before() {
		actorService = ResourceLocator.lookup("esponder/ActorBean/remote");
		equipmentService = ResourceLocator.lookup("esponder/EquipmentBean/remote");
		operationsCentreService = ResourceLocator.lookup("esponder/OperationsCentreBean/remote");
		sensorService = ResourceLocator.lookup("esponder/SensorBean/remote");
		typeService = ResourceLocator.lookup("esponder/TypeBean/remote");
		userService = ResourceLocator.lookup("esponder/UserBean/remote");
		
		ESponderUser user = userService.findByUserName(USER_NAME);
		if (null != user) {
			userID = user.getId();
		}
	}
	
	protected Period createPeriod(int seconds) {
		Date now = new Date();
		Period period = new Period(now, DateUtils.addSeconds(now, seconds));
		return period;
	}
	
	protected Sphere createSphere(
			Double latitude, Double longitude, Double altitude, Double radius) {
		
		Point centre = new Point(
				new BigDecimal(latitude), new BigDecimal(longitude), null != altitude ? new BigDecimal(altitude) : null);
		Sphere sphere = new Sphere(centre, null != radius ? new BigDecimal(radius) : null);
		return sphere;
	}
}
