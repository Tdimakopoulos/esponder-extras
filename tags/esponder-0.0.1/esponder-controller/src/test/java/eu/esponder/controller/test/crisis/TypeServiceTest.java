package eu.esponder.controller.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.controller.test.ControllerServiceTest;
import eu.esponder.model.type.EmergencyOperationsCentre;
import eu.esponder.model.type.EquipmentType;
import eu.esponder.model.type.MobileEmergencyOperationsCentre;
import eu.esponder.model.type.OperationalActorType;
import eu.esponder.model.type.SensorType;
import eu.esponder.model.type.TacticalActorType;


public class TypeServiceTest extends ControllerServiceTest {
	
	@Test(groups="createBasics")
	public void testCreateTypes() {
		TacticalActorType missionCommander = new TacticalActorType();
		missionCommander.setTitle("Mission Commander");
		typeService.create(missionCommander, this.userID);
		
		OperationalActorType frc = new OperationalActorType();
		frc.setTitle("FRC");
		typeService.create(frc, this.userID);
		
		OperationalActorType fr = new OperationalActorType();
		fr.setTitle("FR");
		typeService.create(fr, this.userID);
		
		EmergencyOperationsCentre eoc = new EmergencyOperationsCentre();
		eoc.setTitle("EOC");
		typeService.create(eoc, this.userID);
		
		MobileEmergencyOperationsCentre meoc = new MobileEmergencyOperationsCentre();
		meoc.setTitle("MEOC");
		typeService.create(meoc, this.userID);
		
		EquipmentType wimax = new EquipmentType();
		wimax.setTitle("FRU WiMAX");
		typeService.create(wimax,  this.userID);
		
		EquipmentType wifi = new EquipmentType();
		wifi.setTitle("FRU WiFi");
		typeService.create(wifi,  this.userID);
		
		SensorType heartbeat = new SensorType();
		heartbeat.setTitle("HB");
		heartbeat.setMeasurementUnit("bpm");
		typeService.create(heartbeat,  this.userID);
		
		SensorType temperature = new SensorType();
		temperature.setTitle("TEMP");
		temperature.setMeasurementUnit("C");
		typeService.create(temperature,  this.userID);
	}
	
}