package eu.esponder.controller.persistence;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;
import javax.persistence.EntityManager;

@Remote
public interface CrudRemoteService<T> {

	public T create(T t);
    
	public T find(Class<T> type, Object id);
	
	public T getReference(Class<T> type, Object id);
    
	public T update(T t);
	
	public void refresh(T t);
    
	public void delete(Class<T> type, Object id);
	
    public void delete(T entity);
    
    public void flush();

    public List<T> findWithNamedQuery(String queryName);
    
    public List<T> findWithNamedQuery(String queryName, int resultLimit);
    
    public List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters);
    
    public List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit);
    
    public List<T> findWithQuery(String queryName);
    
    public List<T> findWithQuery(String queryName, int resultLimit);
    
    public List<T> findWithQuery(String namedQueryName, Map<String, Object> parameters);
    
    public List<T> findWithQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit);

	public Object findSingleWithNamedQuery(String namedQueryName, Map<String,Object> parameters);
	
	public EntityManager getEntityManager();
    
}
