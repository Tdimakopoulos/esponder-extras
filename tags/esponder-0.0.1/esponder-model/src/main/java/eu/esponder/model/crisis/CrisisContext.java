package eu.esponder.model.crisis;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.snapshot.CrisisContextSnapshot;

@Entity
@Table(name="crisis_context")
@NamedQueries({
	@NamedQuery(name="CrisisContext.findByTitle", query="select cc from CrisisContext cc where cc.title=:title")
})
public class CrisisContext extends ESponderEntity<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2118773143338642719L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CRISIS_CONTEXT_ID")
	private Long id;
	
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	@OneToMany(mappedBy="crisisContext")
	private Set<Action> actions;
	
	@OneToMany(mappedBy="crisisContext")
	private Set<CrisisContextSnapshot> snapshots;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<Action> getActions() {
		return actions;
	}

	public void setActions(Set<Action> actions) {
		this.actions = actions;
	}

	public Set<CrisisContextSnapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<CrisisContextSnapshot> snapshots) {
		this.snapshots = snapshots;
	}	
	
}
