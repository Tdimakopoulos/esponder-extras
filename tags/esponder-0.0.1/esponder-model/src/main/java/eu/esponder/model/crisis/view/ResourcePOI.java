package eu.esponder.model.crisis.view;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.OperationsCentre;

@Entity
@Table(name="resource_poi")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="DISCRIMINATOR")
public abstract class ResourcePOI extends ESponderEntity<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5584272232867252690L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="POI_ID")
	protected Long id;
	
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	protected String title;
	
	@ManyToOne
	@JoinColumn(name="OPERATIONS_CENTRE_ID")
	protected OperationsCentre operationsCentre;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public OperationsCentre getOperationsCentre() {
		return operationsCentre;
	}

	public void setOperationsCentre(OperationsCentre operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

}
