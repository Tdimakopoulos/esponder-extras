package eu.esponder.model.type;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("SENSOR")
public class SensorType extends ESponderType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2832452320920328627L;
	
	@Column(name="MEASUREMENT_UNIT")
	private String measurementUnit;

	public String getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}
	
}
