package eu.esponder.model.crisis.action;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.model.crisis.resource.ReusableResource;
import eu.esponder.model.snapshot.action.ActionPartSnapshot;


@Entity
@Table(name="action_part")
@NamedQueries({
	@NamedQuery(name="ActionPart.findByTitle", query="select ap from ActionPart ap where ap.title=:title")
})
public class ActionPart extends ESponderEntity<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -441973801978686457L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTION_PART_ID")
	private Long id;
	
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	@ManyToOne
	@JoinColumn(name="ACTOR_ID", nullable=false)
	private Actor actor;
	
	@ManyToOne
	@JoinColumn(name="ACTION_ID", nullable=false)
	private Action action;
	
	@OneToMany(mappedBy="actionPart")
	private Set<ActionPartSnapshot> snapshots;
	
	// These resources are *used by* the actor. For example the "truck" in the following sentence: Move this telecom equipment kits using a truck
	@OneToMany(mappedBy="actionPart")
	private Set<ReusableResource> usedReusableResources;
	
	// These resources are *used by* the actor. For example the "truck" in the following sentence: Move this telecom equipment kits using a truck
	@OneToMany(mappedBy="actionPart")
	private Set<ConsumableResource> usedConsumableResources;
	
	@Enumerated
	@Column(name="SEVERITY_LEVEL")
	private SeverityLevel severityLevel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Set<ActionPartSnapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<ActionPartSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	public Set<ReusableResource> getUsedReusableResources() {
		return usedReusableResources;
	}

	public void setUsedReusableResources(Set<ReusableResource> usedReusableResources) {
		this.usedReusableResources = usedReusableResources;
	}

	public Set<ConsumableResource> getUsedConsumableResources() {
		return usedConsumableResources;
	}

	public void setUsedConsumableResources(
			Set<ConsumableResource> usedConsumableResources) {
		this.usedConsumableResources = usedConsumableResources;
	}

	public SeverityLevel getSeverityLevel() {
		return severityLevel;
	}

	public void setSeverityLevel(SeverityLevel severityLevel) {
		this.severityLevel = severityLevel;
	}

}
