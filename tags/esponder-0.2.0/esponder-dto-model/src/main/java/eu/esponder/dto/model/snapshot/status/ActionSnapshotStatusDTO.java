package eu.esponder.dto.model.snapshot.status;



public enum ActionSnapshotStatusDTO {
	STARTED,
	PAUSED,
	COMPLETED,
	CANCELED
}
