package eu.esponder.dto.model.snapshot.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.dto.model.snapshot.status.EquipmentSnapshotStatusDTO;

@JsonPropertyOrder({"status", "period"})
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EquipmentSnapshotDTO extends SnapshotDTO {
	
	private static final long serialVersionUID = 7338449002977646358L;

	public EquipmentSnapshotDTO() { }
	
	private EquipmentSnapshotStatusDTO status;
	
	private EquipmentDTO equipment;

	public EquipmentSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(EquipmentSnapshotStatusDTO status) {
		this.status = status;
	}

	public EquipmentDTO getEquipment() {
		return equipment;
	}

	public void setEquipment(EquipmentDTO equipment) {
		this.equipment = equipment;
	}
	
	@Override
	public String toString() {
		return "EquipmentSnapshotDTO [status=" + status + ", period=" + period + ", id=" + id + "]";
	}

}
