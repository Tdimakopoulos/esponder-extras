package eu.esponder.dto.model.crisis.action;



public enum SeverityLevelDTO {
	MINIMAL,
	MEDIUM,
	SERIOUS,
	SEVERE,
	FATAL,
	UNDEFINED
}
