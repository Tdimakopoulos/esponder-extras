package eu.esponder.dto.model.snapshot.sensor.measurement.statistic;


public enum MeasurementUnitEnumDTO {
	
	//Temperature
	DEGREES_CELCIUS,
	DEGREES_FAHRENHEIT,
	
	//Time
	SECONDS,
	MILLISECONDS,
	
	//Gas Concentration
	PPM,
	
	//Position (for each of lon, lat, alt)
	DEGREES,
	
	//Hearbeat Rate
	BBM

}
