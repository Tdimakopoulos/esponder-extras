package eu.esponder.dto.model.crisis.action;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;


@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "action", "prerequisiteAction", "criteria"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionScheduleDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = -703599359640439512L;

	private String title;
	
	private ActionDTO action;
	
	private ActionDTO prerequisiteAction;
	
	private ActionScheduleCriteriaDTO criteria;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ActionDTO getAction() {
		return action;
	}

	public void setAction(ActionDTO action) {
		this.action = action;
	}

	public ActionDTO getPrerequisiteAction() {
		return prerequisiteAction;
	}

	public void setPrerequisiteAction(ActionDTO prerequisiteAction) {
		this.prerequisiteAction = prerequisiteAction;
	}

	public ActionScheduleCriteriaDTO getCriteria() {
		return criteria;
	}

	public void setCriteria(ActionScheduleCriteriaDTO criteria) {
		this.criteria = criteria;
	}

}


