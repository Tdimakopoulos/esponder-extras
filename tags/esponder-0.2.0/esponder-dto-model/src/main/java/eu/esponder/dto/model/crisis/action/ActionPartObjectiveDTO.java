package eu.esponder.dto.model.crisis.action;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;


@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "period", "locationArea", "consumableResources","reusableResources"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionPartObjectiveDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = 5435315087528431329L;

	private String title;
	
	private PeriodDTO period;
	
	private LocationAreaDTO locationArea;
	
	private Set<ConsumableResourceDTO> consumableResources;
	
	private Set<ReusableResourceDTO> reusuableResources;
	
	private ActionPartDTO actionPart;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

	public Set<ConsumableResourceDTO> getConsumableResources() {
		return consumableResources;
	}

	public void setConsumableResources(Set<ConsumableResourceDTO> consumableResources) {
		this.consumableResources = consumableResources;
	}

	public Set<ReusableResourceDTO> getReusuableResources() {
		return reusuableResources;
	}

	public void setReusuableResources(Set<ReusableResourceDTO> reusuableResources) {
		this.reusuableResources = reusuableResources;
	}

	public ActionPartDTO getActionPart() {
		return actionPart;
	}

	public void setActionPart(ActionPartDTO actionPart) {
		this.actionPart = actionPart;
	}
	
}
