package eu.esponder.dto.model.crisis.action;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "action", "snapshot", "actionPartObjectives","usedConsumableResources",
					"usedReusuableResources", "actionOperation", "actor"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionPartDTO extends ESponderEntityDTO {
	
	private static final long serialVersionUID = -9041261126833929085L;

	private String title;
	
	private Long actionId;
	
	private ActionPartSnapshotDTO snapshot;
	
	private Set<ActionPartObjectiveDTO> actionPartObjectives;
	
	private Set<ConsumableResourceDTO> usedConsumableResources;
	
	private Set<ReusableResourceDTO> usedReusuableResources;
	
	private ActionOperationEnumDTO actionOperation;
	
	private ActorDTO actor;
	
	private SeverityLevelDTO severityLevel;

	public ActorDTO getActor() {
		return actor;
	}

	public void setActor(ActorDTO actor) {
		this.actor = actor;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ActionPartSnapshotDTO getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(ActionPartSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	public Set<ActionPartObjectiveDTO> getActionPartObjectives() {
		return actionPartObjectives;
	}

	public void setActionPartObjectives(Set<ActionPartObjectiveDTO> actionPartObjectives) {
		this.actionPartObjectives = actionPartObjectives;
	}

	public Set<ConsumableResourceDTO> getUsedConsumableResources() {
		return usedConsumableResources;
	}

	public void setUsedConsumableResources(Set<ConsumableResourceDTO> usedConsumableResources) {
		this.usedConsumableResources = usedConsumableResources;
	}

	public Set<ReusableResourceDTO> getUsedReusuableResources() {
		return usedReusuableResources;
	}

	public void setUsedReusuableResources(Set<ReusableResourceDTO> usedReusuableResources) {
		this.usedReusuableResources = usedReusuableResources;
	}

	public ActionOperationEnumDTO getActionOperation() {
		return actionOperation;
	}

	public void setActionOperation(ActionOperationEnumDTO actionOperation) {
		this.actionOperation = actionOperation;
	}

	public Long getActionId() {
		return actionId;
	}

	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}

	public SeverityLevelDTO getSeverityLevel() {
		return severityLevel;
	}

	public void setSeverityLevel(SeverityLevelDTO severityLevel) {
		this.severityLevel = severityLevel;
	}

}
