package eu.esponder.dto.model.snapshot.status;


public enum ActionPartSnapshotStatusDTO {
	STARTED,
	PAUSED,
	COMPLETED,
	CANCELED
}
