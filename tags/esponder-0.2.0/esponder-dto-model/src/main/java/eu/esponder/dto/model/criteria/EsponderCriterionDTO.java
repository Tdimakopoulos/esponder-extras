package eu.esponder.dto.model.criteria;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"field", "expression", "value"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EsponderCriterionDTO extends EsponderQueryRestrictionDTO {

	private static final long serialVersionUID = -4927714081578656589L;
	
	public EsponderCriterionDTO() {}
	
	public EsponderCriterionDTO(String fieldName, EsponderCriterionExpressionEnumDTO expression, String fieldValue) {
		this.field = fieldName;
		this.expression = expression;
		this.value = fieldValue;
	}

	private String field;
	
	private EsponderCriterionExpressionEnumDTO expression;
	
	private String value;

	public String getField() {
		return field;
	}

	public void setField(String fieldName) {
		this.field = fieldName;
	}

	public EsponderCriterionExpressionEnumDTO getExpression() {
		return expression;
	}

	public void setExpression(EsponderCriterionExpressionEnumDTO expression) {
		this.expression = expression;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String fieldValue) {
		this.value = fieldValue;
	}

}
