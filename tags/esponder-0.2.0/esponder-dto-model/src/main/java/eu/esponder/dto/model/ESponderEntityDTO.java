package eu.esponder.dto.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonTypeInfo;


@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class ESponderEntityDTO implements Serializable {
	
	private static final long serialVersionUID = -1652821671452383819L;

	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}

