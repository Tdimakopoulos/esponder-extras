package eu.esponder.dto.model.crisis.resource.plan;

import org.codehaus.jackson.annotate.JsonTypeInfo;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class PlannableResourceDTO extends ResourceDTO {

	private static final long serialVersionUID = -650765106596246416L;

}
