package eu.esponder.model.crisis.resource;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import eu.esponder.model.ESponderEntity;

@MappedSuperclass
public abstract class Resource extends ESponderEntity<Long> {

	private static final long serialVersionUID = 351245075327159975L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="RESOURCE_ID")
	protected Long id;
	
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	protected String title;

	@Enumerated(EnumType.STRING)
	@Column(name="RESOURCE_STATUS", nullable=false)
	protected ResourceStatus status;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ResourceStatus getStatus() {
		return status;
	}

	public void setStatus(ResourceStatus status) {
		this.status = status;
	}

}
