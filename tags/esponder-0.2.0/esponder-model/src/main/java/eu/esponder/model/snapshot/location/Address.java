package eu.esponder.model.snapshot.location;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address implements Serializable {

	private static final long serialVersionUID = -3274818026669019295L;

	@Column(name="ADDR_NUMBER")
	private String number;
	
	@Column(name="ADDR_STREET")
	private String street;
	
	@Column(name="ZIPCODE")
	private String zipCode;
	
	@Column(name="PREFECTURE")
	private String prefecture;
	
	@Column(name="COUNTRY")
	private String country;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPrefecture() {
		return prefecture;
	}

	public void setPrefecture(String prefecture) {
		this.prefecture = prefecture;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
