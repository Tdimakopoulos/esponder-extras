package eu.esponder.model.crisis.view;

public class VoIPURL extends URL {

	private static final long serialVersionUID = 3193388189632590549L;

	public VoIPURL() {
		this.setProtocol("sip");
	}
}
