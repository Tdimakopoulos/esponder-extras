package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *	See D7.0.1 page 162
 * 
 *	The following CrisisTypes have been identified so far:
 * 
 *  Natural Disasters:
 *  	Forest Fire, Earthquake, Intense Rain - City Flood, Intense Rain – River Flood, Intense Snow - Blizzard,
 *  	Frost- Hail Storm, Heat – Drought, Landslip phenomena, Volcano Activity, Other
 *  
 *  Technological Disaster or Accident
 *		Chemical, Biological, Great Fire in Industrial Area, Great Fire in a Living Region, 
 *		Great scale sea pollution, Great scale air pollution, Great scale soil or water pollution, 
 *		Dam or Irrigation network  destruction, Bridge Destruction, Mine Collapse, 
 *		Destruction of communication infrastructure, Other 
 *
 *	Other Disaster or Terrorist Action
 *		Sea Accident, Road Accident, Train Accident, 
 *		Airplane Accident – Sea Region, Airplane Accident – Living Region, Airplane Accident – Forest Region, 
 *		Airplane Accident – Dessert Area, Epidemic event, Other
 *
 * 	Terrorist Action/Attack
 * 		Explosion, Fire, Hostages
 * 
 */

@Entity
@DiscriminatorValue("CRISIS_DISASTER")
public class CrisisDisasterType extends CrisisType {

	private static final long serialVersionUID = 7344380087247404825L;

}
