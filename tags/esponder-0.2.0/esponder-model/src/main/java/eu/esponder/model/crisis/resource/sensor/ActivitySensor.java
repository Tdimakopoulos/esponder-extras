package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ACTIVITY")
public class ActivitySensor extends BiomedicalSensor implements EnumeratedMeasurementSensor {

	private static final long serialVersionUID = -7899651164427099687L;

}
