package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.category.EquipmentCategory;
import eu.esponder.model.crisis.resource.plan.PlannableResource;
import eu.esponder.model.crisis.resource.sensor.Sensor;


@Entity
@Table(name="equipment")
@NamedQueries({
	@NamedQuery(name="Equipment.findByTitle", query="select e from Equipment e where e.title=:title")
})
public class Equipment extends PlannableResource {

	private static final long serialVersionUID = -4705569532455885376L;

	@ManyToOne
	@JoinColumn(name="ACTOR_ID")
	private Actor actor;
	
	@ManyToOne
	@JoinColumn(name="CONTAINER_ID")
	private Equipment container;
	
	@OneToMany(mappedBy="container")
	private Set<Equipment> parts;
	
	@OneToMany(mappedBy="equipment")
	private Set<Sensor> sensors;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private EquipmentCategory equipmentCategory;

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	public Equipment getContainer() {
		return container;
	}

	public void setContainer(Equipment container) {
		this.container = container;
	}

	public Set<Equipment> getParts() {
		return parts;
	}

	public void setParts(Set<Equipment> parts) {
		this.parts = parts;
	}

	public Set<Sensor> getSensors() {
		return sensors;
	}

	public void setSensors(Set<Sensor> sensors) {
		this.sensors = sensors;
	}

	public EquipmentCategory getEquipmentCategory() {
		return equipmentCategory;
	}

	public void setEquipmentCategory(EquipmentCategory equipmentCategory) {
		this.equipmentCategory = equipmentCategory;
	}
	
}
