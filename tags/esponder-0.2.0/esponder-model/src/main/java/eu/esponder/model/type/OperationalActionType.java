package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("OP_ACTION")
public final class OperationalActionType extends ActionType {

	private static final long serialVersionUID = -3958343462000323443L;

}
