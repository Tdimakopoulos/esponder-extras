package eu.esponder.test.event.simulator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;

/*
 * Add a shared variable as flag to have ability to kill threads called from here
 * 
 */

public class FRUThread extends Thread {

	private Long fruId;

	private long threadPeriodFRU;

	private List<Thread> threadsList;

	private List<SensorDTO> sensors;

	public List<SensorMeasurementStatisticDTO> statisticsList;

	public FRUThread(List<SensorDTO> sensors, long threadPeriodFRU) {
		this.sensors = sensors;
		this.threadPeriodFRU = threadPeriodFRU;
		statisticsList = new ArrayList<SensorMeasurementStatisticDTO>();
	}

	private int range = 3;

	private int min = 35;

	@Override
	public void run() {

		int counter = 4;
		int threadCounter = 0;
		threadsList = new ArrayList<Thread>();
		SensorMeasurementStatisticEnvelopeDTO envelope = new SensorMeasurementStatisticEnvelopeDTO();
		envelope.setMeasurementStatistics(new ArrayList<SensorMeasurementStatisticDTO>());
		/*
		 * TODO: start SensorThreads based on sensors list
		 */

		System.out.println("Thread executing : " + Thread.currentThread().getClass() + " / " + Thread.currentThread().getName() + "with id : "+Thread.currentThread().getId());

		for(SensorDTO sensor : this.sensors) {
			for(StatisticsConfigDTO config : sensor.getConfiguration()) {
				ArithmeticSensorThread sensorThread = new ArithmeticSensorThread(new Long(config.getSamplingPeriodMilliseconds()), sensor, config.getMeasurementStatisticType(), min, range, statisticsList);
				threadCounter++;
				threadsList.add(sensorThread);
				System.out.println("New Sensor Thread created");
			}
		}


		for(Thread thread : threadsList) {
			thread.start();
		}

		while(counter > 0) {
			try {

				System.out.println("Thread executing : " + Thread.currentThread().getName() + " with id : "+Thread.currentThread().getId());

				/* Create a loop that does the following:*/
				// 1. Get measurements from SensorThreads

				if(getStatisticsList().size() != 0) {

					// 2. Put them into a SensorMeasurementStatisticEnvelopeDTO
					//	envelope.setMeasurementStatistics(getStatisticsList());
					//	statisticsList.clear();

					synchronized (threadsList) {
						for(Thread thread : threadsList)
							((ArithmeticSensorThread) thread).pleaseWait = true;
					}
					
					for( Iterator< SensorMeasurementStatisticDTO > it = statisticsList.iterator(); it.hasNext();)
			        {
						SensorMeasurementStatisticDTO statisticMeasurement = it.next();
						envelope.getMeasurementStatistics().add(statisticMeasurement);
						it.remove();
			        }
					
//					for(SensorMeasurementStatisticDTO statistic : statisticsList) {
//						envelope.getMeasurementStatistics().add(statistic);
//						statisticsList.remove(statistic);
//					}

					System.out.println("\nEnvelope Contents size is : " + envelope.getMeasurementStatistics().size() + "\n");

					synchronized (threadsList) {
						for(Thread thread : threadsList){
							((ArithmeticSensorThread) thread).pleaseWait = false;
							thread.notify();
						}
					}

					// 3. Publish a CreateSensorMeasurementStatisticEvent
					CreateSensorMeasurementStatisticEvent event = new CreateSensorMeasurementStatisticEvent();
					event.setEventAttachment(envelope);
					//	ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher = new ESponderEventPublisher<CreateSensorMeasurementStatisticEvent>(CreateSensorMeasurementStatisticEvent.class);
					//	publisher.publishEvent(event);
				}
				else {
					System.out.println("\nEnvelope is empty \n");
				}

				counter--;

				Thread.sleep(threadPeriodFRU);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
			//			catch (EventListenerException e) {
			//				e.printStackTrace();
			//			}
		}

		for(Thread thread : threadsList) {
			thread.stop();
		}

		System.out.println("\nFRUThread"+Thread.currentThread().getName()+"/"+Thread.currentThread().getId()+" is stopping\n");

	}

	//	TODO Create methods to access the statisticsList in a synchronized manners
	//	FRUThread should first read values and then delete them;
	//	Afterwards, it should create an envelope and call Publisher


	public synchronized List<SensorMeasurementStatisticDTO> getStatisticsList() {
		return statisticsList;
	}

	public synchronized void setStatisticsList(List<SensorMeasurementStatisticDTO> statisticsList) {
		this.statisticsList = statisticsList;
	}

	public Long getFruId() {
		return fruId;
	}

	public void setFruId(Long fruId) {
		this.fruId = fruId;
	}

	public long getThreadPeriodFRU() {
		return threadPeriodFRU;
	}

	public void setThreadPeriodFRU(long threadPeriod) {
		this.threadPeriodFRU = threadPeriod;
	}

	public List<SensorDTO> getSensors() {
		return sensors;
	}

	public void setSensors(List<SensorDTO> sensors) {
		this.sensors = sensors;
	}

	public List<Thread> getThreadsList() {
		return threadsList;
	}

	public void setThreadsList(List<Thread> threadsList) {
		this.threadsList = threadsList;
	}

}
