package eu.esponder.test.event.simulator;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Random;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;

public class ArithmeticSensorThread extends SensorThread {

	public boolean pleaseWait = false;

	public ArithmeticSensorThread(Long period, SensorDTO sensor,
			MeasurementStatisticTypeEnumDTO statisticType, int min, int range, List<SensorMeasurementStatisticDTO> statisticsEnvelope) {
		super(period, sensor, statisticType, min, range, statisticsEnvelope);	// statisticsEnvelope --> List of statistics, not actual envelope
	}

	@Override
	public void run() {

		while(true) {

			super.run();
			this.setStatisticMeasurement(getMeasurement());

			System.out.println("ArithmeticSensor Thread executing : " + Thread.currentThread().getName() + " with id : " + Thread.currentThread().getId());
			System.out.println("Measurement for current thread is : " + ((ArithmeticSensorMeasurementDTO)this.getStatisticMeasurement().getStatistic()).getMeasurement());

			this.getStatisticsList().add(this.getStatisticMeasurement());

			// synchronized block
			synchronized (this) {
				while (pleaseWait) {
					try {
						wait();
					} catch (Exception e) {
					}
				}
			}

			try {
				Thread.sleep(this.getPeriod());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	
	public SensorMeasurementStatisticDTO getMeasurement() {

		ArithmeticSensorMeasurementDTO measurement = new ArithmeticSensorMeasurementDTO();
		measurement.setMeasurement(getMeasurementValue(getMin(), getRange()));

		measurement.setSensor(getSensor());
		measurement.setTimestamp(new Date());
		this.getStatisticMeasurement().setStatistic(measurement);
		return getStatisticMeasurement();
	}

	
	private BigDecimal getMeasurementValue(int min, int range) {
		/*
		 * use range (choose proper type) to determine randomness of generated values
		 */
		Date seed = new Date();
		Random random = new Random(seed.getTime());
		return new BigDecimal(min + (range * random.nextDouble()));
	}
}
