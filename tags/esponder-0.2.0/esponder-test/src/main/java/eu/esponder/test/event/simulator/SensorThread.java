package eu.esponder.test.event.simulator;

import java.util.List;

import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;

public abstract class SensorThread extends Thread {

	/*
	 * The period at which sensor measurement statistics are generated in msec
	 * 
	 *  Also, the StatisticsConfig.sampling period is the time the sensor thread will be sleeping
	 *  (rate of generating measurements)
	 */

	private Long period;

	private SensorDTO sensor;

	private MeasurementStatisticTypeEnumDTO statisticType;

	private SensorMeasurementStatisticDTO statisticMeasurement;

	private List<SensorMeasurementStatisticDTO> statisticsList;

	private int min;

	private int range;


	// period -> time thread spents sleeping

	public SensorThread(Long period, SensorDTO sensor,
			MeasurementStatisticTypeEnumDTO statisticType, int min, int range, List<SensorMeasurementStatisticDTO> statisticsList) {
		super();
		this.period = period;
		this.sensor = sensor;
		this.statisticType = statisticType;
		this.statisticsList = statisticsList;
		this.min = min;
		this.range = range;
	}

	public void run() {

		setStatisticMeasurement(this.getInitialMeasurementParameters());

		/*
		 * Set all fields of the measurement that are thread-specific and common for all measurements, e.g. measurement collection period -> 10:00:00 - 10:00:10
		 */
	}

	public SensorMeasurementStatisticDTO getInitialMeasurementParameters() {
		SensorMeasurementStatisticDTO statistic = new SensorMeasurementStatisticDTO();

		/*
		 * Set all fields of the measurement that are known in advance, e.g. sensor object etc.
		 */

		//		statistic.getStatistic().setTimestamp(new Date());
		//		statistic.getStatistic().setSensor(sensor);
		statistic.setStatisticType(statisticType);

		/*
		 * +++ ki o,ti allo xreiazetai
		 */
		return statistic;
	}

	public Long getPeriod() {
		return period;
	}

	public void setPeriod(Long period) {
		this.period = period;
	}

	public SensorDTO getSensor() {
		return sensor;
	}

	public void setSensor(SensorDTO sensor) {
		this.sensor = sensor;
	}

	public MeasurementStatisticTypeEnumDTO getStatisticType() {
		return statisticType;
	}

	public void setStatisticType(MeasurementStatisticTypeEnumDTO statisticType) {
		this.statisticType = statisticType;
	}

	public SensorMeasurementStatisticDTO getStatisticMeasurement() {
		return statisticMeasurement;
	}

	public void setStatisticMeasurement(SensorMeasurementStatisticDTO statistic) {
		this.statisticMeasurement = statistic;
	}

	public synchronized List<SensorMeasurementStatisticDTO> getStatisticsList() {
		return statisticsList;
	}

	public synchronized void setStatisticsList(List<SensorMeasurementStatisticDTO> statisticsList) {
		this.statisticsList = statisticsList;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

}
