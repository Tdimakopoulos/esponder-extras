package eu.esponder.test.sensor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.test.ResourceLocator;
import eu.esponder.test.event.simulator.FRUThread;

public class MeasurementSimulator {

//	@EJB
	SensorRemoteService sensorService = ResourceLocator.lookup("esponder/SensorBean/remote");

	Long threadPeriodFRU = (long) 10000;

	List<Thread> FRUThreads = new ArrayList<Thread>();

	@SuppressWarnings("deprecation")
	@Test
	public void runScenario1() throws InterruptedException {

		int counter = 2;
		/*
		 * instantiate fru1Sensors, fru2Sensors
		 */
		List<SensorDTO> fru1Sensors = createListOfSensors(Arrays.asList(new Long(1), new Long(3), new Long(5)));
		FRUThread fru1Thread = new FRUThread(fru1Sensors, threadPeriodFRU);
		FRUThreads.add(new Thread(fru1Thread, "FRU Thread 1"));

		List<SensorDTO> fru2Sensors = createListOfSensors(Arrays.asList(new Long(2), new Long(4), new Long(6)));
		FRUThread fru2Thread = new FRUThread(fru2Sensors, threadPeriodFRU);
		FRUThreads.add(new Thread(fru2Thread, "FRU Thread 2"));

		// Start Threads
		for(Thread thread : FRUThreads) {
			thread.start();
		}

		while (counter>0) {
			counter--;
			Thread.sleep(20000);
		}

//		 Stop Threads
		for(Thread thread : FRUThreads) {
			if(thread.isAlive())
				thread.stop();
		}

	}

	private List<SensorDTO> createListOfSensors(List<Long> sensorIDs) {
		List<SensorDTO> fruSensors = new ArrayList<SensorDTO>();
		for(Long i: sensorIDs) {
			fruSensors.add(sensorService.findSensorByIdRemote(i));
		}
		return fruSensors;
	}
}
