package eu.esponder.rest.client;

import java.io.IOException;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.JAXBException;

import org.apache.http.client.ClientProtocolException;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;

import eu.esponder.util.logger.ESponderLogger;

public class ResteasyClient {

	private String mediaType;
	private String resourceURI;

	
	/****   CONSTRUCTOR   *******************************************************************************/
	public ResteasyClient(String URI, String mediaType) throws ClassNotFoundException, JAXBException {
		this.setResourceURI(URI);
		if(mediaType != null)
			this.mediaType = mediaType;
		ESponderLogger.info(this.getClass(), "ESponderLogger created successfully for ResteasyClient");
	}
	/****************************************************************************************************/
	
	@SuppressWarnings("unchecked")
	public String get(Map<String, String> serviceParams) throws RuntimeException, Exception {
		
		ClientRequest request = new ClientRequest(this.resourceURI);
		if(this.mediaType != null)
			request.accept(this.mediaType);
		for (String key : serviceParams.keySet()) {
			request.queryParameter(key, serviceParams.get(key));
		}
		
		ClientResponse<String> response;
		String output = null;
		try {
			response = request.get();
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus() + "\nResponse Message:" + printMetadata(response.getMetadata()));
			}
			
			output = response.getEntity(String.class);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}
	
	private String printMetadata(MultivaluedMap<String, Object> metadataMap) {
		String message = "";
		for (String key : metadataMap.keySet()) {
			message += " " + metadataMap.get(key);
		}
		return message;
	}
	
	/****************************************************************************************************/
	
	@SuppressWarnings("unchecked")
	public String post(Map<String, String> serviceParams, String bodyStr) throws RuntimeException, Exception {
		
		ClientRequest request = new ClientRequest(this.resourceURI);
		request.accept(this.mediaType);
		if(!serviceParams.isEmpty()) {
			for (String key : serviceParams.keySet()) {
				request.queryParameter(key, serviceParams.get(key));
			}
		}
		else 
			ESponderLogger.warn(this.getClass(), "Service Parameters are empty");
			
		
		request.body(this.mediaType, bodyStr);
		
		ClientResponse<String> response;
		String output = null;
		try {
			response = request.post();
			if (response.getStatus() != 200 && response.getStatus() != 204) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			output = response.getEntity(String.class);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}
	
	/****************************************************************************************************/
	
	@SuppressWarnings("unchecked")
	public String put(Map<String, String> serviceParams, String bodyStr) throws RuntimeException, Exception {
		
		ClientRequest request = new ClientRequest(this.resourceURI);
		request.accept(this.mediaType);
		if(!serviceParams.isEmpty()) {
			for (String key : serviceParams.keySet()) {
				request.queryParameter(key, serviceParams.get(key));
			}
		}
		else
			System.out.println("Service Parameters are empty");
		
		request.body(this.mediaType, bodyStr);
		
		ClientResponse<String> response;
		String output = null;
		try {
			response = request.put();
			if (response.getStatus() != 200 && response.getStatus() != 204) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			output = response.getEntity(String.class);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}
	
	/****************************************************************************************************/
	
	@SuppressWarnings("unchecked")
	public void delete(Map<String, String> serviceParams) throws RuntimeException, Exception {
		
		ClientRequest request = new ClientRequest(this.resourceURI);
		request.accept(this.mediaType);
		if(!serviceParams.isEmpty()) {
			for (String key : serviceParams.keySet()) {
				request.queryParameter(key, serviceParams.get(key));
			}
		}
		else
			System.out.println("Service Parameters are empty");
		
		ClientResponse<String> response;
		try {
			response = request.delete();
			if (response.getStatus() != 200 && response.getStatus() != 204) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/****************************************************************************************************/	
	
	public String getResourceURI() {
		return resourceURI;
	}

	public void setResourceURI(String baseURI) {
		this.resourceURI = baseURI;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	
}