package eu.esponder.test.rest.cris.poi;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.rest.client.ResteasyClient;
import eu.esponder.util.jaxb.Parser;
import eu.esponder.util.logger.ESponderLogger;

public class UpdateSketchPOIs {

	private String SKETCHPOI_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/view/sketch";

	@Test
	public void UpdateSketchPOIWithXml () throws ClassNotFoundException, Exception {

		Parser parser = new Parser(new Class[] {SketchPOIDTO.class});
		SketchPOIDTO sketchPOIDTO = getSketchPOIByIDXml();

		if(sketchPOIDTO !=null) {

			sketchPOIDTO.setTitle(sketchPOIDTO.getTitle()+" updated");

			String serviceName = SKETCHPOI_SERVICE_URI + "/update";
			ResteasyClient postClient = new ResteasyClient(serviceName, "application/xml");
			System.out.println("Client for createGenericEntity created successfully...");
			String xmlPayload = parser.marshall(sketchPOIDTO);

			printXML(xmlPayload);

			Map<String, String> params =  CreateSketchServiceParameters(new Long(2));
			String resultXML = postClient.put(params, xmlPayload);

			printXML(resultXML);

			SketchPOIDTO sketchDTO = (SketchPOIDTO) parser.unmarshal(resultXML);

			System.out.println("\n*****************SKETCHPOI**************************");
			System.out.println(sketchDTO.toString());
			System.out.println("\n************************************************");

		}
	}


	@Test
	public void UpdateSketchPOIWithJson () throws ClassNotFoundException, Exception {

		SketchPOIDTO sketchPOIDTO = getSketchPOIByIDXml();

		if(sketchPOIDTO !=null) {

			sketchPOIDTO.setTitle(sketchPOIDTO.getTitle()+" updated");

			String serviceName = SKETCHPOI_SERVICE_URI + "/update";
			ResteasyClient postClient = new ResteasyClient(serviceName, "application/json");
			System.out.println("Client for createGenericEntity created successfully...");
			ObjectMapper mapper = new ObjectMapper();

			String jsonPayload = mapper.writeValueAsString(sketchPOIDTO);

			printJSON(jsonPayload);

			Map<String, String> params =  CreateSketchServiceParameters(new Long(2));
			String resultJSON = postClient.put(params, jsonPayload);

			printJSON(resultJSON);

			SketchPOIDTO sketchDTO = mapper.readValue(resultJSON, SketchPOIDTO.class);

			System.out.println("\n*****************SKETCHPOI**************************");
			System.out.println(sketchDTO.toString());
			System.out.println("\n************************************************");

		}
	}


	public SketchPOIDTO getSketchPOIByIDXml() throws RuntimeException, Exception{

		ESponderLogger.info(this.getClass(), "fetching initial sketchPOI object");
		Parser parser = new Parser(new Class[] {SketchPOIDTO.class});

		String serviceName = SKETCHPOI_SERVICE_URI + "/findByID";

		ResteasyClient getClient = new ResteasyClient(serviceName, "application/xml");
		Map<String, String> params = this.getIDServiceParameters(new Long(4));

		String resultXML = getClient.get(params);
		printXML(resultXML);

		SketchPOIDTO sketchDTO= (SketchPOIDTO) parser.unmarshal(resultXML);		
		return sketchDTO;
	}


	public SketchPOIDTO getSketchPOIByIDJson() throws RuntimeException, Exception{

		ESponderLogger.info(this.getClass(), "fetching initial sketchPOI object");
		ObjectMapper mapper = new ObjectMapper();

		String serviceName = SKETCHPOI_SERVICE_URI + "/findByID";

		ResteasyClient getClient = new ResteasyClient(serviceName, "application/xml");
		Map<String, String> params = this.getIDServiceParameters(new Long(5));

		String resultJSON = getClient.get(params);
		printJSON(resultJSON);

		SketchPOIDTO sketchDTO = mapper.readValue(resultJSON, SketchPOIDTO.class);

		return sketchDTO;
	}

	private Map<String, String> getIDServiceParameters(Long sketchPOIId) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("sketchPOIId", sketchPOIId.toString());
		params.put("userID", "1");
		return params;
	}

	private Map<String, String> CreateSketchServiceParameters(Long operationsCentreID) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		params.put("operationsCentreID", operationsCentreID.toString());
		return params;
	}


	private void printJSON(String jsonStr) {
		System.out.println("\n\n******* JSON * START *******");
		System.out.println(jsonStr);
		System.out.println("******** JSON * END ********\n\n");
	}

	private void printXML(String xmlStr) {
		System.out.println("\n\n******* XML * START *******");
		System.out.println(xmlStr);
		System.out.println("******** XML * END ********\n\n");
	}

}
