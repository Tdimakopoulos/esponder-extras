package eu.esponder.rest;

import javax.naming.NamingException;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.util.ejb.ServiceLocator;

public abstract class ESponderResource {
	
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected SensorRemoteService getSensorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected ActorRemoteService getActorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected EquipmentRemoteService getEquipmentRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EquipmentBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected ESponderRemoteMappingService getMappingRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected OperationsCentreRemoteService getOperationsCentreRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/OperationsCentreBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	protected ActionRemoteService getActionRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	protected GenericRemoteService getGenericRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	protected ESponderConfigurationRemoteService getConfigurationRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderConfigurationBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	//*******************************************************************************

	protected UserRemoteService getUserRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/UserBean/remote");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

}
