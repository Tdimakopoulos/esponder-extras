package eu.esponder.rest.crisis.resource;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.rest.ESponderResource;

@Path("/crisis/resource")
public class ActorViewResource extends ESponderResource {
	
	//FindByID
	@GET
	@Path("/actor/getID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO getActor( @QueryParam("actorID") @NotNull(message="actorID may not be null" ) Long actorID,
			                  @QueryParam("userID") @NotNull(message="userID may not be null" ) Long userID) {
//		ActorDTO actorDTO =  this.getMappingService().mapActor(actorID);
		return this.getActorRemoteService().findByIdRemote(actorID);
	}
	
	//FindByTitle
	@GET
	@Path("/actor/getTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO getActor( @QueryParam("actorTitle") @NotNull(message="actorTitle may not be null" ) String actorTitle,
			                  @QueryParam("userID") @NotNull(message="userID may not be null" ) Long userID) {
//		ActorDTO actorDTO =  this.getMappingService().mapActor(actorTitle);
		return this.getActorRemoteService().findByTitleRemote(actorTitle);
		
	}
	
	// Find Subordinates By ActorID
	@GET
	@Path("/actor/getSubordinates")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getActorSubordinates(
			@QueryParam("actorID") @NotNull(message="actorID may not be null" ) Long actorID,
			@QueryParam("userID") @NotNull(message="userID may not be null" ) Long userID) {
//		ResultListDTO resultList = new ResultListDTO(this.getMappingService().mapActorSubordinates(actorID));
		ResultListDTO resultList = new ResultListDTO(this.getActorRemoteService().findSubordinatesByIdRemote(actorID));
		return resultList;
	}
	
	// create Actor
	@POST
	@Path("/actor/post")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO createActor(@NotNull(message="actorDTO may not be null" ) ActorDTO actorDTO,
			            		@QueryParam("userID") @NotNull(message="userID may not be null" ) Long userID) {
		
//		Actor actor = (Actor) this.getMappingService().mapESponderEntityDTO(actorDTO, Actor.class);
//		if(null != actor) {
//			actorDTO =  this.getMappingService().createActor(actor, userID);
//			return actorDTO;
//		}
//		else
//			return null;
		
		return this.getActorRemoteService().createActorRemote(actorDTO, userID);
		
		
	}
	
	
	// Update Actor
	@PUT
	@Path("/actor/put")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO updateActor(@NotNull(message="actorDTO may not be null" ) ActorDTO actorDTO,
			            		@QueryParam("userID") @NotNull(message="userID may not be null" ) Long userID) {
//		Actor actor = (Actor) this.getMappingService().mapESponderEntityDTO(actorDTO, Actor.class);
//		if(actor!=null) {
//			actorDTO =  this.getMappingService().updateActor(actor, userID);
//			return actorDTO;
//		}
//		else
//			return null;
		return this.getActorRemoteService().updateActorRemote(actorDTO, userID);
	}
	
	// Delete Actor
	@DELETE
	@Path("/actor/delete")
	public void deleteActor(@QueryParam("actorID") @NotNull(message="actorID may not be null" ) Long actorID,
							@QueryParam("userID") @NotNull(message="userID may not be null" ) Long userID) {
		
		this.getActorRemoteService().deleteActorRemote(actorID, userID);
	}
	
}
