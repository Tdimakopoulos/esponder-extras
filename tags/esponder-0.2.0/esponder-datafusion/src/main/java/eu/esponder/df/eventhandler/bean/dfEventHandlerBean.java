package eu.esponder.df.eventhandler.bean;


import javax.ejb.Stateless;

import eu.esponder.df.eventhandler.dfEventHandlerRemoteService;
import eu.esponder.df.eventhandler.dfEventHandlerService;
import eu.esponder.df.ruleengine.core.DomainSpecificKnowledgeParser;
import eu.esponder.df.ruleengine.core.RuleEngineGuvnorAssets;
import eu.esponder.df.ruleengine.utilities.RuleStatistics;
import eu.esponder.df.ruleengine.utilities.RuleUtilities;
import eu.esponder.df.ruleengine.utilities.events.RuleEvents;
import eu.esponder.df.ruleengine.utilities.ruleresults.RuleResults;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.event.model.ESponderEvent;


@Stateless
public class dfEventHandlerBean implements dfEventHandlerRemoteService, dfEventHandlerService {
	
	private String szFlowName=null;
	private String szRulePackageName=null;
	private String szDSLName=null;
	@SuppressWarnings("unused")
	private String szDSLRName=null;
	private RuleEngineType dInferenceType=RuleEngineType.NONE;
	
	public enum RuleEngineType {
	    NONE,DRL_RULES, DSL_RULES, FLOW_RULES, ALL_PACKAGE, MIX
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Rule Engine Calls and Variables
	private RuleEngineGuvnorAssets dEngine = new RuleEngineGuvnorAssets();
	private DomainSpecificKnowledgeParser ddsl = new DomainSpecificKnowledgeParser();
	
	public void AddDTOObjects(Object fact) {
		dEngine.InferenceEngineAddDTOObject(fact);
		ddsl.AddDTOObject(fact);
	}

	public void AddObjects(Object fact) {
		dEngine.InferenceEngineAddObject(fact);
		ddsl.AddObject(fact);
	}

	private void AddKnowledgeByCategory(String szCategory) {
		try {
			dEngine.InferenceEngineAddKnowledgeForCategory(szCategory);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Category");
			e.printStackTrace();
		}
		ddsl.AddKnowledgeForCategory(szCategory);
	}

	private void AddKnowledgeByPackage(String szPackage) {
		try {
			dEngine.InferenceEngineAddKnowledgeForPackage(szPackage);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Package");
			e.printStackTrace();
		}
		ddsl.AddKnowledgeForPackage(szPackage);
	}

	private void ExecuteRules() {
		// run simple drl rules
		dEngine.InferenceEngineRunAssets();
		
	}
	
	private void ExecuteFlow() {
		// run all processes
		ddsl.ExecuteFlow();

		// run dslr rules based on dsl and
		ddsl.RunRules();

		// close session only if all processes has finished
		ddsl.Dispose();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void LoadKnowledge()
	{
		if(this.dInferenceType==RuleEngineType.ALL_PACKAGE)
		{
			AddKnowledgeByPackage(szRulePackageName);
		}
		
		if(this.dInferenceType==RuleEngineType.DRL_RULES)
		{
			AddKnowledgeByCategory(szRulePackageName);
		}
		
		if(this.dInferenceType==RuleEngineType.FLOW_RULES)
		{
			AddKnowledgeByCategory(szFlowName);
		}
		
		if(this.dInferenceType==RuleEngineType.DSL_RULES)
		{
			AddKnowledgeByCategory(szDSLName);
		}
		
		
	}
	
	public void ProcessRules()
	{
		////////////////////////////////////////////////////////////////////////
		//Load Default Objects into drools
		// RuleUtils
		// MathUtils
		// EventUtils
		// RuleResultsUtils
		RuleStatistics pStatistics = new RuleStatistics();
		RuleUtilities pUtils = new RuleUtilities();
		RuleResults pResults = new RuleResults();
		RuleEvents pEvents = new RuleEvents();
		
		AddObjects(pStatistics);
		AddObjects(pUtils);
		AddObjects(pResults);
		AddObjects(pEvents);
		////////////////////////////////////////////////////////////////////////
		
		//Execute Rules using package, DRL,DSL,DSLR,Flow
		if (this.dInferenceType==RuleEngineType.ALL_PACKAGE)
		{
			ExecuteFlow();
			ExecuteRules();
		}
		
		//Execute Rules using Mix, DRL,DSL,DSLR,Flow
		if (this.dInferenceType==RuleEngineType.MIX)
		{
			ExecuteFlow();
			ExecuteRules();
		}
		
		//Execute Rules using  DRL
		if (this.dInferenceType==RuleEngineType.DRL_RULES)
		{
			ExecuteRules();
		}
		
		//Execute Rules using  DSL
		if (this.dInferenceType==RuleEngineType.DSL_RULES)
		{
			ExecuteFlow();
		}
		
		//Execute Rules using  Flow or/and DRSR
		if (this.dInferenceType==RuleEngineType.FLOW_RULES)
		{
			ExecuteFlow();
		}
	}
	
/*	// What kind of event type ??
	public void PublishEvent()
	{
		
	}
	
	*/
	public SensorSnapshotDTO CreateSensorSnapshot(ESponderEvent<?> pEvent)
	{
		return null;
	}
	
	
	
	public void SetRuleEngineType(RuleEngineType dType,String szPackageNameOrFlowName)
	{
		SetRuleType(dType,szPackageNameOrFlowName,null,null);
	}
	
	public void SetRuleEngineType(RuleEngineType dType,String szDSLName,String szDSLRName)
	{
		SetRuleType(dType,null,szDSLName,szDSLRName);
	}
	
	private void SetRuleType(RuleEngineType dType,String szPackageNameOrFlowName,String szDSLNamep,String szDSLRNamep)
	{
		if (dType==RuleEngineType.DRL_RULES)
		{
			szRulePackageName=szPackageNameOrFlowName;
			SetInferenceType(dType);
		}
		if (dType==RuleEngineType.ALL_PACKAGE)
		{
			szRulePackageName=szPackageNameOrFlowName;
			dInferenceType=RuleEngineType.ALL_PACKAGE;
		}
		if (dType==RuleEngineType.FLOW_RULES)
		{
			szFlowName=szPackageNameOrFlowName;
			SetInferenceType(dType);
		}
		if(dType==RuleEngineType.DSL_RULES)
		{
			szDSLName=szDSLNamep;
			szDSLRName=szDSLRNamep;
			SetInferenceType(dType);
		}
	}
	
	private void SetInferenceType(RuleEngineType dType)
	{
		if (dInferenceType==RuleEngineType.ALL_PACKAGE)
		{}else{
		if (dInferenceType==RuleEngineType.NONE)
			dInferenceType=dType;
		else
			dInferenceType=RuleEngineType.MIX;
		}
	}
}
	
	