/**
 * DroolSettings
 * 
 * This Java class store all settings need by the project to access the drool repository
 * and identify the temporary local repository using a file path 
 *
 * @Project   esponder
 * @package   Datafusion
*/
package eu.esponder.df.ruleengine.settings;

import javax.naming.NamingException;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
//import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.test.ResourceLocator;


/**
 * @author tdim
 *
 */
public class DroolSettings {

	// Variable to store the local repository (file path)
	String szTmpRepositoryDirectoryPath="c:\\tmprepo\\";
	// Drool server URL
	//String szDroolApplicationServerURL="http://localhost:8080/";
	String szDroolApplicationServerURL="http://localhost:8181/";
	//Drool deployment name
	//String szDroolWarDeploymentPath="guvnor-5.4.0.Final-tomcat-6.0";
	String szDroolWarDeploymentPath="guvnor";
	//Service entry point
	String szDroolServiceEntryPoint="rest";
	//Drool username
	String szDroolUsername="guest";
	//Drool password
	String szDroolPassword="";
	//Reset repo
	String szDroolRepo="local";
	
	
    public DroolSettings()
    {
    	try {
			LoadAllOptions();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	public void LoadAllOptions() throws NamingException, ClassNotFoundException
	{
		//System.out.println("Getting Options from DB");
		ESponderConfigurationRemoteService cfgService = ResourceLocator.lookup("esponder/ESponderConfigurationBean/remote");
		
		

		//Get DroolsTempRep
		ESponderConfigParameterDTO dTempRep=cfgService.findESponderConfigByNameRemote("DroolTempRep");
		szTmpRepositoryDirectoryPath=dTempRep.getParameterValue();
		
		//Get DroolsURL
		ESponderConfigParameterDTO dAppURL=cfgService.findESponderConfigByNameRemote("DroolURL");
		szDroolApplicationServerURL=dAppURL.getParameterValue();

		//Get DroolsWarDeployment
		ESponderConfigParameterDTO dWarName=cfgService.findESponderConfigByNameRemote("DroolWARNAME");
		szDroolWarDeploymentPath=dWarName.getParameterValue();

		//Get DroolsWarDeployment
		ESponderConfigParameterDTO dSPoint=cfgService.findESponderConfigByNameRemote("DroolSPoint");
		szDroolServiceEntryPoint=dSPoint.getParameterValue();
		
		//Get Droolsusername
		ESponderConfigParameterDTO dDUser=cfgService.findESponderConfigByNameRemote("DroolUsername");
		szDroolUsername=dDUser.getParameterValue();		
		
		//Get Droolspassword
		ESponderConfigParameterDTO dDPassword=cfgService.findESponderConfigByNameRemote("DroolPassword");
		szDroolPassword=dDPassword.getParameterValue();		
		
		//Get DroolLocalRepoRefresh
		ESponderConfigParameterDTO dDLRepo=cfgService.findESponderConfigByNameRemote("DroolRefresh");
		szDroolRepo=dDLRepo.getParameterValue();	
		//System.out.println("Getting Options from DB --- "+szDroolRepo);
	    
	}
	
	public String getSzDroolRepo() {
		return szDroolRepo;
	}


	public void setSzDroolRepo(String szDroolRepo) {
		System.out.println("Update Option To DB");
		ESponderConfigurationRemoteService cfgService = ResourceLocator.lookup("esponder/ESponderConfigurationBean/remote");
		
	

		//Get DroolsTempRep
		//ESponderConfigParameterDTO dTempRep=cfgService.findESponderConfigByNameRemote("DroolTempRep");
		//szTmpRepositoryDirectoryPath=dTempRep.getParameterValue();
		try {
			ESponderConfigParameterDTO dDLRepo=cfgService.findESponderConfigByNameRemote("DroolRefresh");
			dDLRepo.setParameterValue(szDroolRepo);
			cfgService.updateESponderConfigRemote(dDLRepo, (long) 1);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.szDroolRepo = szDroolRepo;
	}


	/**
	 * @return drool username (user account manage by drool web interface)
	 */
	public String getSzDroolUsername() {
		return szDroolUsername;
	}

	
	/**
	 * @param szDroolUsername
	 */
	public void setSzDroolUsername(String szDroolUsername) {
		this.szDroolUsername = szDroolUsername;
	}

	
	/**
	 * @return drool password
	 */
	public String getSzDroolPassword() {
		return szDroolPassword;
	}

	/**
	 * @param szDroolPassword
	 */
	public void setSzDroolPassword(String szDroolPassword) {
		this.szDroolPassword = szDroolPassword;
	}
	
	/**
	 * @return temporary local repository file path
	 */
	public String getSzTmpRepositoryDirectoryPath() {
		return szTmpRepositoryDirectoryPath;
	}
	
	/**
	 * @param szTmpRepositoryDirectoryPath
	 */
	public void setSzTmpRepositoryDirectoryPath(String szTmpRepositoryDirectoryPath) {
		this.szTmpRepositoryDirectoryPath = szTmpRepositoryDirectoryPath;
	}
	
	/**
	 * @return the url where drool and all sub components are install
	 */
	public String getSzDroolApplicationServerURL() {
		return szDroolApplicationServerURL;
	}
	
	/**
	 * @param szDroolApplicationServerURL
	 */
	public void setSzDroolApplicationServerURL(String szDroolApplicationServerURL) {
		this.szDroolApplicationServerURL = szDroolApplicationServerURL;
	}
	
	/**
	 * @return drool guvnor deployment path
	 */
	public String getSzDroolWarDeploymentPath() {
		return szDroolWarDeploymentPath;
	}
	
	/**
	 * @param szDroolWarDeploymentPath
	 */
	public void setSzDroolWarDeploymentPath(String szDroolWarDeploymentPath) {
		this.szDroolWarDeploymentPath = szDroolWarDeploymentPath;
	}
	
	/**
	 * @return service entry point ( default is rest )
	 */
	public String getSzDroolServiceEntryPoint() {
		return szDroolServiceEntryPoint;
	}

	/**
	 * @param szDroolServiceEntryPoint
	 */
	public void setSzDroolServiceEntryPoint(String szDroolServiceEntryPoint) {
		this.szDroolServiceEntryPoint = szDroolServiceEntryPoint;
	}
}
