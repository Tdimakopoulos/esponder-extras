package eu.esponder.df.rules.profile;

import eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType;

public class ProfileManager {

	public ProfileData GetProfileNameForSensor() {
		ProfileData pReturn = new ProfileData();

		pReturn.setSzProfileName("Sensors");
		pReturn.setSzProfileType(RuleEngineType.DRL_RULES);

		return pReturn;
	}
	
	public ProfileData GetProfileNameForAction() {
		ProfileData pReturn = new ProfileData();

		pReturn.setSzProfileName("Action");
		pReturn.setSzProfileType(RuleEngineType.DRL_RULES);

		return pReturn;
	}
}
