/**
 * RuleEngineRulesAssetsHelper
 * 
 * This Java class implement a simple rule list, because this list is used in different places this class is 
 * created, all modification of handling the rule list can take place here.
 * 
 * 
 * @Project   esponder
 * @package   Datafusion
*/
package eu.esponder.df.ruleengine.core;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author tdim
 *
 */
public class RuleEngineRulesAssetsHelper {

	
	@SuppressWarnings("rawtypes")
	private List dlocal = new LinkedList();

	
	/**
	 * @param dRules this initialize the class from an already exist list
	 */
	@SuppressWarnings("rawtypes")
	public void SetRulesAssets(List dRules) {
		dlocal = new ArrayList();
		dlocal=dRules;
	}
	
	/**
	 *  intiliaze the class without any existing list
	 */
	@SuppressWarnings("rawtypes")
	public void Initialize()
	{
		dlocal = new ArrayList();
	}
	
	/**
	 * @param szElement the source point
	 */
	@SuppressWarnings("unchecked")
	public void AddSourcePoint(String szElement)
	{
		dlocal.add(szElement);
	}
	
	/**
	 * @return the total rules in list
	 */
	public int GetRulesNumber()
	{
		return dlocal.size();
	}
	
	/**
	 * @param iPosition the position
	 * @return the element in the position
	 */
	public String GetRuleSourcePoint(int iPosition)
	{
		return dlocal.get(iPosition).toString();
	}
}
