package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.event.model.ESponderEvent;


public class ResourceEvent<T extends ResourceDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = 790086496951447162L;

	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
