package eu.esponder.event.model;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;


public abstract class ESponderTypeEvent<T extends ESponderEntityDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = -9036535870972195826L;
	
}
