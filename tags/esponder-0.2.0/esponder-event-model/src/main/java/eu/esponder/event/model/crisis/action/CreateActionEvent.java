package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.event.model.CreateEvent;


public class CreateActionEvent extends ActionEvent<ActionDTO> implements CreateEvent {

	private static final long serialVersionUID = 2006035559724305579L;

	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo=" Journal Info";
		return JournalMessageInfo;
	}
}
