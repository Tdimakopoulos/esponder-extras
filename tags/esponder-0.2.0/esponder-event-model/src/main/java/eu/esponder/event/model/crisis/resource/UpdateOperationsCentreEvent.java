package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateOperationsCentreEvent extends OperationsCentreEvent<OperationsCentreDTO> implements UpdateEvent {

	private static final long serialVersionUID = -9085599295942847029L;

}
