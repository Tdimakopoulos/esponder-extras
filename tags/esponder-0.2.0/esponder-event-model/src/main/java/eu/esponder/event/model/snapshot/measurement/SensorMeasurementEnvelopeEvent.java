package eu.esponder.event.model.snapshot.measurement;

import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.event.model.ESponderEvent;

public abstract class SensorMeasurementEnvelopeEvent<T extends SensorMeasurementEnvelopeDTO> extends ESponderEvent<T> {

	private static final long serialVersionUID = -8470374826798188512L;

}
