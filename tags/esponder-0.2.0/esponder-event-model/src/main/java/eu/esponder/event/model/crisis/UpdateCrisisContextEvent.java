package eu.esponder.event.model.crisis;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.event.model.UpdateEvent;


public class UpdateCrisisContextEvent extends CrisisContextEvent<CrisisContextDTO> implements UpdateEvent {

	private static final long serialVersionUID = 6676309937264842792L;

	@Override
	public String getJournalMessageInfo() {
		String JournalMessageInfo="CrisisContextEvent Journal Info";
		return JournalMessageInfo;
	}
	
}
