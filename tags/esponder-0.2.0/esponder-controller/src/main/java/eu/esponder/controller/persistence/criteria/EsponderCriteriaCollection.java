package eu.esponder.controller.persistence.criteria;

import java.util.Collection;


public class EsponderCriteriaCollection extends EsponderQueryRestriction {
	
	private static final long serialVersionUID = -5707952404179111086L;

	private Collection<EsponderQueryRestriction> restrictions;
	
	public EsponderCriteriaCollection() { }
	
	public EsponderCriteriaCollection(Collection<EsponderQueryRestriction> restrictions) {
		this.restrictions = restrictions;
	}
	
	public Collection<EsponderQueryRestriction> getRestrictions() {
		return restrictions;
	}
	
	public void setRestrictions(Collection<EsponderQueryRestriction> restrictions) {
		this.restrictions = restrictions;
	}
	
	public void add(EsponderQueryRestriction restriction) {
		this.getRestrictions().add(restriction);
	}
	
	public void remove(EsponderQueryRestriction restriction) {
		this.getRestrictions().remove(restriction);
	}
	
}
