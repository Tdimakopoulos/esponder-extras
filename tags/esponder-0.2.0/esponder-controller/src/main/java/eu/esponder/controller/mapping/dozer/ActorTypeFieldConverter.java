package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.TypeService;
import eu.esponder.model.type.ActorType;
import eu.esponder.util.ejb.ServiceLocator;


public class ActorTypeFieldConverter implements CustomConverter {

	protected TypeService getTypeService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Object convert(Object destination, 
			Object source, 
			Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == String.class && source != null) {
			String type = (String) source;
			ActorType actorType = (ActorType) getTypeService().findByTitle(type);
			destination = actorType;
		}
		else if(ActorType.class.isAssignableFrom(sourceClass)) {
			ActorType actorType = (ActorType) source;
			destination = (String) actorType.getTitle();
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}