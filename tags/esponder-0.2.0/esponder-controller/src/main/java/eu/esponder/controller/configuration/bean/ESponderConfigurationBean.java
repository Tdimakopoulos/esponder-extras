package eu.esponder.controller.configuration.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.controller.configuration.ESponderConfigurationService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
import eu.esponder.model.config.ESponderConfigParameter;

@Stateless
public class ESponderConfigurationBean implements ESponderConfigurationService, ESponderConfigurationRemoteService {
	
	@EJB
	private CrudService<ESponderConfigParameter> configCrudService;

	@EJB 
	private ESponderMappingService mappingService;

	// -------------------------------------------------------------------------

	@Override
	public ESponderConfigParameterDTO findESponderConfigByNameRemote(String configDTOTitle) throws ClassNotFoundException {
		ESponderConfigParameter configParameter = findESponderConfigByName(configDTOTitle);
		if(configParameter != null)
			return (ESponderConfigParameterDTO) mappingService.mapObject(configParameter, ESponderConfigParameterDTO.class);
		else
			return null;
	}
	
	@Override
	public ESponderConfigParameter findESponderConfigByName(String configName)	throws ClassNotFoundException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("parameterName", configName);
		return (ESponderConfigParameter) configCrudService.findSingleWithNamedQuery("ESponderConfigParameter.findByName", params);
	}

	// -------------------------------------------------------------------------

	@Override
	public ESponderConfigParameterDTO findESponderConfigByIdRemote(Long configDTOId) throws ClassNotFoundException {
		ESponderConfigParameter configParameter = findESponderConfigById(configDTOId);
		if(configParameter != null)
			return (ESponderConfigParameterDTO) mappingService.mapObject(configParameter, ESponderConfigParameterDTO.class);
		else
			return null;
	}

	@Override
	public ESponderConfigParameter findESponderConfigById(Long configId)throws ClassNotFoundException {
		return (ESponderConfigParameter) configCrudService.find(ESponderConfigParameter.class, configId);
	}

	// -------------------------------------------------------------------------

	@Override
	public ESponderConfigParameterDTO createESponderConfigRemote(ESponderConfigParameterDTO configDTO, Long userID) throws ClassNotFoundException {
		ESponderConfigParameter configParameter = (ESponderConfigParameter) mappingService.mapObject(configDTO, ESponderConfigParameter.class); 
		configParameter = createESponderConfig(configParameter, userID);
		configDTO = (ESponderConfigParameterDTO) mappingService.mapObject(configParameter, ESponderConfigParameterDTO.class);
		return configDTO;
	}

	@Override
	public ESponderConfigParameter createESponderConfig(ESponderConfigParameter config, Long userID)	throws ClassNotFoundException {
		return configCrudService.create(config);
	}

	// -------------------------------------------------------------------------
	
	@Override
	public ESponderConfigParameterDTO updateESponderConfigRemote(ESponderConfigParameterDTO configDTO, Long userID) {
		ESponderConfigParameter configParameter = (ESponderConfigParameter) mappingService.mapObject(configDTO, ESponderConfigParameter.class);
		configParameter = updateESponderConfig(configParameter, userID);
		configDTO = (ESponderConfigParameterDTO) mappingService.mapObject(configParameter, ESponderConfigParameterDTO.class);
		return configDTO;
	}
	
	@Override
	
	public ESponderConfigParameter updateESponderConfig(ESponderConfigParameter config, Long userID) {
		return configCrudService.update(config);
	}

	// -------------------------------------------------------------------------

	@Override
	public void deleteESponderConfigRemote(Long ESponderConfigDTOID, Long userID) {
		deleteESponderConfig(ESponderConfigDTOID, userID);
	}
	
	@Override
	public void deleteESponderConfig(Long ESponderConfigID, Long userID) {
		configCrudService.delete(ESponderConfigParameter.class, ESponderConfigID);
	}
	
	
	
}
