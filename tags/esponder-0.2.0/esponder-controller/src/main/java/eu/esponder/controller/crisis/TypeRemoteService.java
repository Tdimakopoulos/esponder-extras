package eu.esponder.controller.crisis;

import javax.ejb.Remote;

import eu.esponder.dto.model.type.ESponderTypeDTO;

@Remote
public interface TypeRemoteService {
	
	public ESponderTypeDTO findDTOByTitle(String title) throws ClassNotFoundException;

	public ESponderTypeDTO findDTOById(Long typeId) throws ClassNotFoundException;

	public ESponderTypeDTO createTypeRemote(ESponderTypeDTO typeDTO, Long userID) throws ClassNotFoundException;
	
	public ESponderTypeDTO updateTypeRemote(ESponderTypeDTO typeDTO, Long userID);
	
	public void deleteTypeRemote(Long ESponderTypeDTOID, Long userID);

}
