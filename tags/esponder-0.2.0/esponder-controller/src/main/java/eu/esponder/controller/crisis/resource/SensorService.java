package eu.esponder.controller.crisis.resource;

import java.util.Date;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.model.snapshot.resource.SensorSnapshot;
import eu.esponder.model.snapshot.sensor.config.StatisticsConfig;

@Local
public interface SensorService extends SensorRemoteService {
	
	public Sensor findSensorById(Long sensorID);
	
	public Sensor findSensorByTitle(String title);
	
	public Sensor createSensor(Sensor sensor, Long userID);
	
	public Sensor updateSensor(Sensor sensor, Long userID);
	
	public void deleteSensor(Long sensorId, Long userID);
	
	public SensorSnapshot findSensorSnapshotByDate(Long sensorID, Date dateTo);
	
	public SensorSnapshot findSensorSnapshotById(Long sensorID);
	
	public SensorSnapshot findPreviousSensorSnapshot(Long sensorID);
	
	public SensorSnapshot createSensorSnapshot(SensorSnapshot snapshot, Long userID);
	
	public SensorSnapshot updateSensorSnapshot(SensorSnapshot snapshot, Long userID);
	
	public void deleteSensorSnapshot(Long sensorSnapshotId, Long userID);
	
	public StatisticsConfig findConfigById(Long configID);

	public StatisticsConfig createStatisticConfig(StatisticsConfig statisticConfig, Long userID);
	
	public StatisticsConfig updateStatisticConfig(StatisticsConfig statisticConfig, Long userID);
	
	public void deleteStatisticConfig(Long statisticsConfigId, Long userID);
	
}
