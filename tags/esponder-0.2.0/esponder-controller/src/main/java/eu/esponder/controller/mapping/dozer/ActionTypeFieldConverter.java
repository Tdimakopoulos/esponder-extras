package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.TypeService;
import eu.esponder.model.type.ActionType;
import eu.esponder.util.ejb.ServiceLocator;


public class ActionTypeFieldConverter implements CustomConverter {

	protected TypeService getTypeService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Object convert(Object destination, 
			Object source, 
			Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == String.class && source != null) {
			String type = (String) source;
			ActionType actionType = (ActionType) getTypeService().findByTitle(type);
			destination = actionType;
		}
		else if(ActionType.class.isAssignableFrom(sourceClass)) {
			ActionType ActionType = (ActionType) source;
			destination = (String) ActionType.getTitle();
		}
		else {
			//FIXME Implement custom ESponderException
		}
		return destination;
	}

}