package eu.esponder.jaxb.model.snapshot.resource;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import eu.esponder.jaxb.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.status.ActorSnapshotStatusDTO;

@XmlRootElement(name="actorSnapshot")
@XmlType(name="ActorSnapshot")
@JsonPropertyOrder({"status", "locationArea", "period"})
public class ActorSnapshotDTO extends SpatialSnapshotDTO {
	
	private ActorSnapshotStatusDTO status;

	public ActorSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActorSnapshotStatusDTO status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ActorSnapshotDTO [status=" + status + ", locationArea=" + locationArea + ", id=" + id  + "]";
	}
	
}
