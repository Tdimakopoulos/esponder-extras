package eu.esponder.jaxb.model.criteria;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

//@XmlTransient
//@XmlSeeAlso({
//	EsponderIntersectionCriteriaCollectionDTO.class,
//	EsponderUnionCriteriaCollectionDTO.class,
//	EsponderNegationCriteriaCollectionDTO.class})

//@XmlRootElement(name="EsponderCriteriaCollectionDTO")
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class EsponderCriteriaCollectionDTO extends EsponderQueryRestrictionDTO {
	
	private static final long serialVersionUID = -7905934732438391487L;
	
//	@XmlElementWrapper(name = "restrictions")
	@XmlElement(name="EsponderQueryRestrictionDTO")
	private Collection<EsponderQueryRestrictionDTO> restrictions;
	
	public EsponderCriteriaCollectionDTO () {
		restrictions = new ArrayList<EsponderQueryRestrictionDTO>();
	}
	
	public EsponderCriteriaCollectionDTO(Collection<EsponderQueryRestrictionDTO> restrictions) {
		this.restrictions = restrictions;
	}

	public void setRestrictions(Collection<EsponderQueryRestrictionDTO> restrictions) {
		this.restrictions = restrictions;
	}
	
	public Collection<EsponderQueryRestrictionDTO> getRestrictions() {
		return restrictions;
	}

	public void addRestriction(EsponderQueryRestrictionDTO restriction) {
		this.getRestrictions().add(restriction);
	}
	
	public void removeRestriction(EsponderQueryRestrictionDTO restriction) {
		this.getRestrictions().remove(restriction);
	}
	
}
