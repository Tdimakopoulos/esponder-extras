package eu.esponder.jaxb.model.crisis.action;

public enum SeverityLevelDTO {
	MINIMAL,
	MEDIUM,
	SERIOUS
}
