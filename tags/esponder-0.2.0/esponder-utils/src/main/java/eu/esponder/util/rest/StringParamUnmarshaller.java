package eu.esponder.util.rest;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jboss.resteasy.annotations.StringParameterUnmarshallerBinder;
import org.jboss.resteasy.spi.StringParameterUnmarshaller;
import org.jboss.resteasy.test.BaseResourceTest;
import org.jboss.resteasy.util.FindAnnotation;

public class StringParamUnmarshaller extends BaseResourceTest {

	@Retention(RetentionPolicy.RUNTIME)
	@StringParameterUnmarshallerBinder(DateFormatter.class)
	public @interface DateFormat {
		String value();
	}

	public static class DateFormatter implements StringParameterUnmarshaller<Date> {
	   
		private SimpleDateFormat formatter;

		public void setAnnotations(Annotation[] annotations) {
			DateFormat format = FindAnnotation.findAnnotation(annotations, DateFormat.class);
			formatter = new SimpleDateFormat(format.value());
		}

		public Date fromString(String str) {
			try {
				return formatter.parse(str);
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
