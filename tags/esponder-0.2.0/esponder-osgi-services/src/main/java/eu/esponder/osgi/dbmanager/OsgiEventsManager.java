package eu.esponder.osgi.dbmanager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import eu.esponder.osgi.DB.OsgiEventsEntity;

public class OsgiEventsManager {

	@PersistenceContext
	private EntityManager em;
	
	
	private Object mergeEntity(Object entity) {
    	return em.merge(entity);
  	}

	
  	private Object persistEntity(Object entity) {
    	em.persist(entity);
    	return entity;
	}
  	
	public Object SaveEvent(OsgiEventsEntity dEvent)
	{
		return persistEntity(dEvent);
	}

	public Object EditEvent(OsgiEventsEntity dEvent)
	{
		return mergeEntity(dEvent);
	}
	
	public List<OsgiEventsEntity> findbyID() {
		@SuppressWarnings("unchecked")
		List<OsgiEventsEntity> resultList = em.createNamedQuery("OsgiEventsEntity.findByID").getResultList();
		return resultList;
	}
	
	public List<OsgiEventsEntity> findBySeverity() {
		@SuppressWarnings("unchecked")
		List<OsgiEventsEntity> resultList = em.createNamedQuery("OsgiEventsEntity.findBySeverity").getResultList();
		return resultList;
	}
	
	public List<OsgiEventsEntity> findBySourceid() {
		@SuppressWarnings("unchecked")
		List<OsgiEventsEntity> resultList = em.createNamedQuery("OsgiEventsEntity.findBySourceid").getResultList();
		return resultList;
	}
}
