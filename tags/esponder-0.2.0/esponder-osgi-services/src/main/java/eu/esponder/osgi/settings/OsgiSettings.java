package eu.esponder.osgi.settings;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
import eu.esponder.test.ResourceLocator;

public class OsgiSettings {

	String szPropertiesFileName = "C://Development//osgi.config.properties";

	/*public OsgiSettings()
	{
		try {
			GetAllSettings();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	public void GetAllSettings() throws ClassNotFoundException {

		ESponderConfigurationRemoteService cfgService = ResourceLocator
				.lookup("esponder/ESponderConfigurationBean/remote");

		// Get DroolsTempRep
		ESponderConfigParameterDTO dTempRep = cfgService
				.findESponderConfigByNameRemote("OSGIPropertiesFiles");
		szPropertiesFileName = dTempRep.getParameterValue();
	}

	public String getSzPropertiesFileName() {
		return szPropertiesFileName;
	}

	public void setSzPropertiesFileName(String szPropertiesFileName) {
		this.szPropertiesFileName = szPropertiesFileName;
	}

}
