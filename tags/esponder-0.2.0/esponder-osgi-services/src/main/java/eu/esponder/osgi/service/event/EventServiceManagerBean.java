package eu.esponder.osgi.service.event;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;

@Stateless
public class EventServiceManagerBean implements EventServiceManager {

	private static Map<Class<? extends ESponderEvent<? extends ESponderEntityDTO>>, ESponderEventListener<? extends ESponderEvent<? extends ESponderEntityDTO>>> listeners 
			= new HashMap<Class<? extends ESponderEvent<? extends ESponderEntityDTO>>, ESponderEventListener<? extends ESponderEvent<? extends ESponderEntityDTO>>>();

	private static Map<Class<? extends ESponderEvent<? extends ESponderEntityDTO>>, ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>>> publishers 
			= new HashMap<Class<? extends ESponderEvent<? extends ESponderEntityDTO>>, ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>>>();

	static {
		
		ESponderEventPublisher<CreateActionSnapshotEvent> publisher = new ESponderEventPublisher<CreateActionSnapshotEvent>(CreateActionSnapshotEvent.class);
		createPublisher(publisher);
		
		ESponderEventListener<CreateActionSnapshotEvent> listener = new ESponderEventListener<CreateActionSnapshotEvent>(CreateActionSnapshotEvent.class);
		createListener(listener);
		
		ESponderEventListener<CreateCrisisContextEvent> listener2 = new ESponderEventListener<CreateCrisisContextEvent>(CreateCrisisContextEvent.class);
		createListener(listener2);
	}

	public static void createListener(ESponderEventListener<? extends ESponderEvent<? extends ESponderEntityDTO>> listener) {
		listeners.put(listener.getEventTopicClass(), listener);
	}
	
	public static void createPublisher(ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>> publisher) {
		publishers.put(publisher.getEventTopicClass(), publisher);
	}
	
	public ESponderEventListener<? extends ESponderEvent<? extends ESponderEntityDTO>> getListener(Class<? extends ESponderEvent<? extends ESponderEntityDTO>> eventClass) {
		return listeners.get(eventClass);
	}

	public ESponderEventPublisher<? extends ESponderEvent<? extends ESponderEntityDTO>> getPublisher(Class<? extends ESponderEvent<? extends ESponderEntityDTO>> eventClass) {
		return publishers.get(eventClass);
	}
	
}
