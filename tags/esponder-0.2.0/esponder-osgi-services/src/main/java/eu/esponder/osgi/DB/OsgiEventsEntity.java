package eu.esponder.osgi.DB;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="osgievents")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@NamedQueries({
	@NamedQuery(name="OsgiEventsEntity.findByID", query="select t from OsgiEventsEntity t where t.id=:id"),
	@NamedQuery(name="OsgiEventsEntity.findBySeverity", query="select t from OsgiEventsEntity t where t.severity=:severity"),
	@NamedQuery(name="OsgiEventsEntity.findBySourceid", query="select t from OsgiEventsEntity t where t.sourceid=:sourceid")
})
public class OsgiEventsEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="OSGIEVENTS_ID")
	private Long id;
	
	@Column(name="OSGIEVENTS_JOURNALMSG", nullable=true,  length=255)
	private String journalMsg;
	
	@Column(name="OSGIEVENTS_JOURNALMSGINFO", nullable=true,  length=255)
	private String journalMsgInfo;
	
	@Column(name="OSGIEVENTS_TIMESTAMP", nullable=true,  length=255)
	private Date timeStamp;
	
	@Column(name="OSGIEVENTS_SEVERITY", nullable=true,  length=255)
	private String severity;
	
	@Column(name="OSGIEVENTS_ATTACHMENT", nullable=true)
	private String attachment;
	
	@Column(name="OSGIEVENTS_SOURCETYPE", nullable=true)
	private String source;
	
	@Column(name="OSGIEVENTS_SOURCEID", nullable=true)
	private Long sourceid;

	public Long getSourceid() {
		return sourceid;
	}

	public void setSourceid(Long sourceid) {
		this.sourceid = sourceid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJournalMsg() {
		return journalMsg;
	}

	public void setJournalMsg(String journalMsg) {
		this.journalMsg = journalMsg;
	}

	public String getJournalMsgInfo() {
		return journalMsgInfo;
	}

	public void setJournalMsgInfo(String journalMsgInfo) {
		this.journalMsgInfo = journalMsgInfo;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
}
