/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.personnel;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;
import eu.esponder.objects.ResourcePlanInfo;
import eu.esponder.objects.person;
import eu.esponder.ws.client.logic.CrisisActionAndActionPartManager;
import eu.esponder.ws.client.logic.CrisisResourceManager;
import eu.esponder.ws.client.logic.ResourcePlanInfoQueries;
import eu.esponder.ws.client.query.QueryManager;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.primefaces.event.DragDropEvent;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class PersonnelManager {

    private List<person> personnel;
    private List<person> selectedPersonnel;
    private List<ResourcePlanInfo> pPersonnelPlanInfo;
    private String persontoremove;
      private List<String> selectedcrisis;  
  
    private Map<String,String> crisis;  
    
    private String selectedcrisisID;
    private String crisiscontextparam;
    ActionDTO pAction;
    
    public String selectcrisisbtn()
    {
        selectedcrisisID=selectedcrisis.get(0).toString();
        personnel = new ArrayList<person>();
        selectedPersonnel = new ArrayList<person>();
        pPersonnelPlanInfo = new ArrayList<ResourcePlanInfo>();
        PopulatePersonnel();
        PopulateActionPlanInfo();
         return "CrisisResource?faces-redirect=true";
    }
    private void PopulateCrisis()
    {
        crisis = new HashMap<String, String>();
        try {
            QueryManager pQuery = new QueryManager();
                          ResultListDTO pResults=pQuery.getCrisisContextAll("1");
                          for (int i=0;i<pResults.getResultList().size();i++)
                          {
                              CrisisContextDTO pItem = (CrisisContextDTO) pResults.getResultList().get(i);
                                
                              crisis.put(pItem.getTitle(), pItem.getId().toString());  
                          }
        } catch (JsonParseException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void PopulatePersonnel()
    {
        try {
           ResourcePlanInfoQueries pFind = new ResourcePlanInfoQueries();
                      QueryManager pQuery = new QueryManager();
                      ResultListDTO pResults=pQuery.getPersonnelAll("1");
                      for (int i=0;i<pResults.getResultList().size();i++)
                      {
                          PersonnelDTO pItem = (PersonnelDTO) pResults.getResultList().get(i);
                          QueryManager pqm = new QueryManager();
                          // fix me data need update   
                          person pperson=new person(pItem.getFirstName()+" "+pItem.getLastName(),Integer.parseInt(pqm.getPersonnelTitleID("1", pItem.getTitle())), "NoPhoto", pFind.GetresourceCategoryforPersonnel(pItem.getPersonnelCategory().getId()));
                          pperson.setLat(pItem.getOrganisation().getLocation().getLatitude());
                          pperson.setLon(pItem.getOrganisation().getLocation().getLongitude());
                          pperson.setTitle(pItem.getTitle());
                          pperson.setOrganization(pItem.getOrganisation().getTitle());
        
                          
                           
                          
                     //     pperson.setNumber(pItem.getId().intValue());
                          getPersonnel().add(pperson);
                      }
        } catch (JsonParseException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void PopulateActionPlanInfo()
    {
        //create one action , we will use this to add actors into the cc
        CrisisActionAndActionPartManager pAAManager = new CrisisActionAndActionPartManager();
        QueryManager pQuery = new QueryManager();
        try {
            pAction=pAAManager.CreateAction(pQuery.getCrisisContextID("1", selectedcrisisID));
        } catch (JsonGenerationException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String plantitle="0";
        QueryManager pq=new QueryManager();
        try {
            CrisisContextDTO pcc=pq.getCrisisContextID("1", this.selectedcrisisID);
            System.out.print(pcc.getTitle());
            CrisisTypeDTO[] pct=pcc.getCrisisTypes().toArray(new CrisisTypeDTO[pcc.getCrisisTypes().size()]);
            if(pct.length>=0)
                plantitle=pct[0].getTitle();
            
        } catch (JsonParseException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ResourcePlanInfoQueries pinfoQuery=new ResourcePlanInfoQueries();
            pPersonnelPlanInfo=pinfoQuery.GetQueryLists(1,plantitle);
            //getpPersonnelPlanInfo().add(new ResourcePlanInfo("Police Officer",10,0));
            //getpPersonnelPlanInfo().add(new ResourcePlanInfo("Fire Fighter Special",5,0));
            //getpPersonnelPlanInfo().add(new ResourcePlanInfo("Fire Fighter Special",5,0));
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public PersonnelManager() {
        personnel = new ArrayList<person>();
        selectedPersonnel = new ArrayList<person>();
        pPersonnelPlanInfo = new ArrayList<ResourcePlanInfo>();
        //PopulatePersonnel();
        //PopulateActionPlanInfo();
        PopulateCrisis();
    }

    public List<person> getPersonnel() {
        return personnel;
    }

    public List<person> getSelectedPersonnel() {
        return selectedPersonnel;
    }

    private void UpdateSelectedInfo(String szPosition,int iadd)
    {
        for (int i=0;i<getpPersonnelPlanInfo().size();i++)
        {
            ResourcePlanInfo pElement=getpPersonnelPlanInfo().get(i);
            if (pElement.getPtype().equalsIgnoreCase(szPosition))
            {
                if (iadd==1)
                {
                    int iselected=pElement.getIselected();
                    int ineeded=pElement.getIneeded();
                    pElement.setIselected(iselected+1);
                    pElement.setIneeded(ineeded-1);
                }
                if (iadd==-1)
                {
                    int iselected=pElement.getIselected();
                    int ineeded=pElement.getIneeded();
                    pElement.setIselected(iselected-1);
                    pElement.setIneeded(ineeded+1);
                }
            }
        }
    }
    public void onPersonDrop(DragDropEvent event) {
        person player = (person) event.getData();
UpdateSelectedInfo(player.getPosition(),1);
        
        getPersonnel().remove(player);
        
        CrisisResourceManager pCCManager = new CrisisResourceManager();
        CrisisActionAndActionPartManager pAAManager = new CrisisActionAndActionPartManager();
        QueryManager pQuery = new QueryManager();
        ActionPartDTO pActionPart = null;
        ActorDTO pActoradded=null;
        try {
            pActoradded=pCCManager.AssociateActor( String.valueOf(player.getNumber()), "1");
        } catch (JsonGenerationException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            pActionPart=pAAManager.CreateActionPartPersonnel(pAction, pQuery.getCrisisContextID("1", selectedcrisisID),pActoradded.getId().toString());
        } catch (JsonGenerationException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        player.setNumber2(pActoradded.getId().intValue());
        getSelectedPersonnel().add(player);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(player.getName() + " added", "Position:" + event.getDropId()));
    }

    /**
     * @return the pPersonnelPlanInfo
     */
    public List<ResourcePlanInfo> getpPersonnelPlanInfo() {
        return pPersonnelPlanInfo;
    }

    /**
     * @param pPersonnelPlanInfo the pPersonnelPlanInfo to set
     */
    public void setpPersonnelPlanInfo(List<ResourcePlanInfo> pPersonnelPlanInfo) {
       this.pPersonnelPlanInfo = pPersonnelPlanInfo;
    }
    
    public String removeperson() {
        int ifind=Integer.parseInt(getPersontoremove());
        for(int i=0;i<getSelectedPersonnel().size();i++)
        {
            if(getSelectedPersonnel().get(i).getNumber()==ifind)
            {
                person pelement=getSelectedPersonnel().get(i);
                UpdateSelectedInfo(pelement.getPosition(),-1);
                getSelectedPersonnel().remove(i);
                getPersonnel().add(pelement);
                CrisisResourceManager pCCManager = new CrisisResourceManager();
                try {
                    pCCManager.deAssociateActor(String.valueOf(pelement.getNumber()), String.valueOf(pelement.getNumber2()), "1");
                } catch (JsonGenerationException ex) {
                    Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
                } catch (JsonMappingException ex) {
                    Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
         } 
        
        //selectedPersonnel.add(player);
        //personnel.remove(player);
        
       // System.out.print("removeperson "+persontoremove);
                return "CrisisResource?faces-redirect=true";
        }

    /**
     * @return the persontoremove
     */
    public String getPersontoremove() {
        return persontoremove;
    }

    /**
     * @param persontoremove the persontoremove to set
     */
    public void setPersontoremove(String persontoremove) {
        this.persontoremove = persontoremove;
    }

    /**
     * @param personnel the personnel to set
     */
    public void setPersonnel(List<person> personnel) {
        this.personnel = personnel;
    }

    /**
     * @param selectedPersonnel the selectedPersonnel to set
     */
    public void setSelectedPersonnel(List<person> selectedPersonnel) {
        this.selectedPersonnel = selectedPersonnel;
    }

    /**
     * @param pPersonnelPlanInfo the pPersonnelPlanInfo to set
     */
//    public void /*setpPersonnelPlanInfo*/(List<ResourcePlanInfo> pPersonnelPlanInfo) {
//        this.pPersonnelPlanInfo = pPersonnelPlanInfo;
//    }

    /**
     * @return the selectedcrisis
     */
    public List<String> getSelectedcrisis() {
        return selectedcrisis;
    }

    /**
     * @param selectedcrisis the selectedcrisis to set
     */
    public void setSelectedcrisis(List<String> selectedcrisis) {
        this.selectedcrisis = selectedcrisis;
    }

    /**
     * @return the crisis
     */
    public Map<String,String> getCrisis() {
        return crisis;
    }

    /**
     * @param crisis the crisis to set
     */
    public void setCrisis(Map<String,String> crisis) {
        this.crisis = crisis;
    }

    /**
     * @return the selectedcrisisID
     */
    public String getSelectedcrisisID() {
        return selectedcrisisID;
    }

    /**
     * @param selectedcrisisID the selectedcrisisID to set
     */
    public void setSelectedcrisisID(String selectedcrisisID) {
        this.selectedcrisisID = selectedcrisisID;
    }

    /**
     * @return the crisiscontextparam
     */
    public String getCrisiscontextparam() {
        return crisiscontextparam;
    }

    /**
     * @param crisiscontextparam the crisiscontextparam to set
     */
    public void setCrisiscontextparam(String crisiscontextparam) {
        this.crisiscontextparam = crisiscontextparam;
    }
}
