/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.sensors;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.sensor.ActivitySensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BreathRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.EnvironmentTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.GasSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HeartBeatRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.ws.client.logic.SensorSnapshotFullDetails;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author Tom
 */
@ManagedBean
@SessionScoped
public class sensorspage {

    private CartesianChartModel linearModel;
    private CartesianChartModel linearModel2;
    private CartesianChartModel linearModel3;
    private CartesianChartModel linearModel4;
    private CartesianChartModel linearModel5;
    private CartesianChartModel linearModel6;
    private List<String> selectedactor;
    SensorSnapshotFullDetails pSensors = new SensorSnapshotFullDetails();
    private Map<String, String> actors;

    public sensorspage() {
        actors = new HashMap<String, String>();

        try {
            pSensors.LoadSensorsSnapshots("1");
        } catch (JsonParseException ex) {
            Logger.getLogger(sensorspage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(sensorspage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(sensorspage.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < pSensors.getpSensorsSnapshots().size(); i++) {
            actors.put(pSensors.getpSensorsSnapshots().get(i).getpActor().getTitle(), pSensors.getpSensorsSnapshots().get(i).getpActor().getId().toString());
        }
        createLinearModel();
    }

    private void updateDiagram2() {
        setLinearModel2(new CartesianChartModel());

        LineChartSeries series1 = new LineChartSeries();
        LineChartSeries series2 = new LineChartSeries();
        LineChartSeries series3 = new LineChartSeries();

        boolean b1 = false;
        boolean b2 = false;
        boolean b3 = false;
        boolean bhavev1 = false;
        boolean bhavev2 = false;
        boolean bhavev3 = false;

        for (int x = 0; x < selectedactor.size(); x++) {
            if (x == 0) {
                series1.setLabel("Actor ID : " + this.selectedactor.get(x));
                b1 = true;
            }

            if (x == 1) {
                series2.setLabel("Actor ID : " + this.selectedactor.get(x));
                b2 = true;
            }

            if (x == 2) {
                series3.setLabel("Actor ID : " + this.selectedactor.get(x));
                b3 = true;
            }

            int d = 1;
            for (int i = 0; i < pSensors.getpSensorsSnapshots().size(); i++) {
                if (pSensors.getpSensorsSnapshots().get(i).getpActor().getId().toString().equalsIgnoreCase(this.selectedactor.get(x))) {
                    /*
                     * ActivitySensorDTO
                     * BreathRateSensorDTO
                     * HeartBeatRateSensorDTO
                     * EnvironmentTemperatureSensorDTO
                     * GasSensorDTO
                     */
//                    if (pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getSensor() instanceof EnvironmentTemperatureSensorDTO) {
//                        if (x == 0) {
//                            //series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
//                            series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
//                            bhavev1 = true;
//                        }
//                        if (x == 1) {
//                            //series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
//                            series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
//                            bhavev2 = true;
//                        }
//                        if (x == 2) {
//                            //series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
//                            series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
//                            bhavev3 = true;
//                        }
//                        d = d + 1;
//                    }
                }

            }
        }
        if (!bhavev1 && !bhavev2 && !bhavev3) {
            series1.setLabel("Actor ID : ");
            series1.set(1, 1);
            getLinearModel2().addSeries(series1);
        } else {
            if (!b1 && !b2 && !b3) {
                series1.setLabel("Actor ID : ");
                series1.set(1, 1);
                getLinearModel2().addSeries(series1);
            }
            if (b1) {
                if (bhavev1 = false) {
                    series1.set(1, 1);
                }
                getLinearModel2().addSeries(series1);
            }
            if (b2) {
                if (bhavev2 = false) {
                    series2.set(1, 1);
                }
                getLinearModel2().addSeries(series2);
            }
            if (b3) {
                if (bhavev3 = false) {
                    series3.set(1, 1);
                }
                getLinearModel2().addSeries(series3);
            }
        }
    }

    private void updateDiagram3() {
        setLinearModel3(new CartesianChartModel());

        LineChartSeries series1 = new LineChartSeries();
        LineChartSeries series2 = new LineChartSeries();
        LineChartSeries series3 = new LineChartSeries();

        boolean b1 = false;
        boolean b2 = false;
        boolean b3 = false;
        boolean bhavev1 = false;
        boolean bhavev2 = false;
        boolean bhavev3 = false;

        for (int x = 0; x < selectedactor.size(); x++) {
            if (x == 0) {
                series1.setLabel("Actor ID : " + this.selectedactor.get(x));
                b1 = true;
            }

            if (x == 1) {
                series2.setLabel("Actor ID : " + this.selectedactor.get(x));
                b2 = true;
            }

            if (x == 2) {
                series3.setLabel("Actor ID : " + this.selectedactor.get(x));
                b3 = true;
            }

            int d = 1;
            for (int i = 0; i < pSensors.getpSensorsSnapshots().size(); i++) {
                if (pSensors.getpSensorsSnapshots().get(i).getpActor().getId().toString().equalsIgnoreCase(this.selectedactor.get(x))) {
                    /*
                     * ActivitySensorDTO
                     * BreathRateSensorDTO
                     * HeartBeatRateSensorDTO
                     * EnvironmentTemperatureSensorDTO
                     * GasSensorDTO
                     */
//                    if (pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getSensor() instanceof BreathRateSensorDTO) {
//                        if (x == 0) {
//                            //series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
//                            series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
//                            bhavev1 = true;
//                        }
//                        if (x == 1) {
//                            //series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
//                            series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
//                            bhavev2 = true;
//                        }
//                        if (x == 2) {
//                            //series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
//                            series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
//                            bhavev3 = true;
//                        }
//                        d = d + 1;
//                    }
                }

            }
        }
        if (!bhavev1 && !bhavev2 && !bhavev3) {
            series1.setLabel("Actor ID : " );
            series1.set(1, 1);
            getLinearModel3().addSeries(series1);
        }else{
        if (!b1 && !b2 && !b3) {
            series1.setLabel("Actor ID : ");
            series1.set(1, 1);
            getLinearModel3().addSeries(series1);
        }
        if (b1) {
            if (bhavev1 = false) {
                series1.set(1, 1);
            }
            getLinearModel3().addSeries(series1);
        }
        if (b2) {
            if (bhavev2 = false) {
                series2.set(1, 1);
            }
            getLinearModel3().addSeries(series2);
        }
        if (b3) {
            if (bhavev3 = false) {
                series3.set(1, 1);
            }
            getLinearModel3().addSeries(series3);
        }
        }
    }

    private void updateDiagram4() {
        setLinearModel4(new CartesianChartModel());

        LineChartSeries series1 = new LineChartSeries();
        LineChartSeries series2 = new LineChartSeries();
        LineChartSeries series3 = new LineChartSeries();

        boolean b1 = false;
        boolean b2 = false;
        boolean b3 = false;
        boolean bhavev1 = false;
        boolean bhavev2 = false;
        boolean bhavev3 = false;

        for (int x = 0; x < selectedactor.size(); x++) {
            if (x == 0) {
                series1.setLabel("Actor ID : " + this.selectedactor.get(x));
                b1 = true;
            }

            if (x == 1) {
                series2.setLabel("Actor ID : " + this.selectedactor.get(x));
                b2 = true;
            }

            if (x == 2) {
                series3.setLabel("Actor ID : " + this.selectedactor.get(x));
                b3 = true;
            }

            int d = 1;
            for (int i = 0; i < pSensors.getpSensorsSnapshots().size(); i++) {
                
                if (pSensors.getpSensorsSnapshots().get(i).getpActor().getId().toString().equalsIgnoreCase(this.selectedactor.get(x))) {
                    /*
                     * ActivitySensorDTO
                     * BreathRateSensorDTO
                     * HeartBeatRateSensorDTO
                     * EnvironmentTemperatureSensorDTO
                     * GasSensorDTO
                     */
                    if (pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getSensor()==2) {
                       System.out.println("Size D0 "+pSensors.getpSensorsSnapshots().size()+" V "+pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue());
                        if (x == 0) {
                            //series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev1 = true;
                        }
                        if (x == 1) {
                            //series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev2 = true;
                        }
                        if (x == 2) {
                            //series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev3 = true;
                        }
                        d = d + 1;
                    }
                }

            }
        }
        if (!bhavev1 && !bhavev2 && !bhavev3) {
            series1.setLabel("Actor ID : " );
            series1.set(1, 1);
            getLinearModel4().addSeries(series1);
        }else{
        if (!b1 && !b2 && !b3) {
            series1.setLabel("Actor ID : ");
            series1.set(1, 1);
            getLinearModel4().addSeries(series1);
        }
        if (b1) {
            if (bhavev1 = false) {
                series1.set(1, 1);
            }
            getLinearModel4().addSeries(series1);
        }
        if (b2) {
            if (bhavev2 = false) {
                series2.set(1, 1);
            }
            getLinearModel4().addSeries(series2);
        }
        if (b3) {
            if (bhavev3 = false) {
                series3.set(1, 1);
            }
            getLinearModel4().addSeries(series3);
        }
        }
    }

    private void updateDiagram5() {
        setLinearModel5(new CartesianChartModel());

        LineChartSeries series1 = new LineChartSeries();
        LineChartSeries series2 = new LineChartSeries();
        LineChartSeries series3 = new LineChartSeries();

        boolean b1 = false;
        boolean b2 = false;
        boolean b3 = false;
        boolean bhavev1 = false;
        boolean bhavev2 = false;
        boolean bhavev3 = false;

        for (int x = 0; x < selectedactor.size(); x++) {
            if (x == 0) {
                series1.setLabel("Actor ID : " + this.selectedactor.get(x));
                b1 = true;
            }

            if (x == 1) {
                series2.setLabel("Actor ID : " + this.selectedactor.get(x));
                b2 = true;
            }

            if (x == 2) {
                series3.setLabel("Actor ID : " + this.selectedactor.get(x));
                b3 = true;
            }

            int d = 1;
            for (int i = 0; i < pSensors.getpSensorsSnapshots().size(); i++) {
                if (pSensors.getpSensorsSnapshots().get(i).getpActor().getId().toString().equalsIgnoreCase(this.selectedactor.get(x))) {
                    /*
                     * ActivitySensorDTO
                     * BreathRateSensorDTO
                     * HeartBeatRateSensorDTO
                     * EnvironmentTemperatureSensorDTO
                     * GasSensorDTO
                     */
                    if (pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getSensor()==4) {
System.out.println("Size D0 "+pSensors.getpSensorsSnapshots().size()+" V "+pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue());
                        if (x == 0) {
                            //series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev1 = true;
                        }
                        if (x == 1) {
                            //series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev2 = true;
                        }
                        if (x == 2) {
                            //series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev3 = true;
                        }
                        d = d + 1;
                    }
                }

            }
        }
        if (!bhavev1 && !bhavev2 && !bhavev3) {
            series1.setLabel("Actor ID : " );
            series1.set(1, 1);
            getLinearModel5().addSeries(series1);
        }else{
        if (!b1 && !b2 && !b3) {
            series1.setLabel("Actor ID : ");
            series1.set(1, 1);
            getLinearModel5().addSeries(series1);
        }
        if (b1) {
            if (bhavev1 = false) {
                series1.set(1, 1);
            }
            getLinearModel5().addSeries(series1);
        }
        if (b2) {
            if (bhavev2 = false) {
                series2.set(1, 1);
            }
            getLinearModel5().addSeries(series2);
        }
        if (b3) {
            if (bhavev3 = false) {
                series3.set(1, 1);
            }
            getLinearModel5().addSeries(series3);
        }
        }
    }

    private void updateDiagram6() {
        setLinearModel6(new CartesianChartModel());

        LineChartSeries series1 = new LineChartSeries();
        LineChartSeries series2 = new LineChartSeries();
        LineChartSeries series3 = new LineChartSeries();

        boolean b1 = false;
        boolean b2 = false;
        boolean b3 = false;
        boolean bhavev1 = false;
        boolean bhavev2 = false;
        boolean bhavev3 = false;

        for (int x = 0; x < selectedactor.size(); x++) {
            if (x == 0) {
                series1.setLabel("Actor ID : " + this.selectedactor.get(x));
                b1 = true;
            }

            if (x == 1) {
                series2.setLabel("Actor ID : " + this.selectedactor.get(x));
                b2 = true;
            }

            if (x == 2) {
                series3.setLabel("Actor ID : " + this.selectedactor.get(x));
                b3 = true;
            }

            int d = 1;
            for (int i = 0; i < pSensors.getpSensorsSnapshots().size(); i++) {
                if (pSensors.getpSensorsSnapshots().get(i).getpActor().getId().toString().equalsIgnoreCase(this.selectedactor.get(x))) {
                    /*
                     * ActivitySensorDTO
                     * BreathRateSensorDTO
                     * HeartBeatRateSensorDTO
                     * EnvironmentTemperatureSensorDTO
                     * GasSensorDTO
                     */
                    if (pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getSensor()==3) {
System.out.println("Size D0 "+pSensors.getpSensorsSnapshots().size()+" V "+pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue());
                        if (x == 0) {
                            //series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev1 = true;
                        }
                        if (x == 1) {
                            //series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev2 = true;
                        }
                        if (x == 2) {
                            //series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev3 = true;
                        }
                        d = d + 1;
                    }
                }

            }
        }
        if (!bhavev1 && !bhavev2 && !bhavev3) {
            series1.setLabel("Actor ID : " );
            series1.set(1, 1);
            getLinearModel6().addSeries(series1);
        }else
        {
        if (!b1 && !b2 && !b3) {
            series1.setLabel("Actor ID : ");
            series1.set(1, 1);
            getLinearModel6().addSeries(series1);
        }
        if (b1) {
            if (bhavev1 = false) {
                series1.set(1, 1);
            }
            getLinearModel6().addSeries(series1);
        }
        if (b2) {
            if (bhavev2 = false) {
                series2.set(1, 1);
            }
            getLinearModel6().addSeries(series2);
        }
        if (b3) {
            if (bhavev3 = false) {
                series3.set(1, 1);
            }
            getLinearModel6().addSeries(series3);
        }
        }
    }

    private void updateDiagram() {
        setLinearModel(new CartesianChartModel());

        LineChartSeries series1 = new LineChartSeries();
        LineChartSeries series2 = new LineChartSeries();
        LineChartSeries series3 = new LineChartSeries();

        boolean b1 = false;
        boolean b2 = false;
        boolean b3 = false;
        boolean bhavev1 = false;
        boolean bhavev2 = false;
        boolean bhavev3 = false;

        for (int x = 0; x < selectedactor.size(); x++) {
            if (x == 0) {
                series1.setLabel("Actor ID : " + this.selectedactor.get(x));
                b1 = true;
            }

            if (x == 1) {
                series2.setLabel("Actor ID : " + this.selectedactor.get(x));
                b2 = true;
            }

            if (x == 2) {
                series3.setLabel("Actor ID : " + this.selectedactor.get(x));
                b3 = true;
            }

            int d = 1;
            for (int i = 0; i < pSensors.getpSensorsSnapshots().size(); i++) {
                if (pSensors.getpSensorsSnapshots().get(i).getpActor().getId().toString().equalsIgnoreCase(this.selectedactor.get(x))) {
                    /*
                     * ActivitySensorDTO
                     * BreathRateSensorDTO
                     * HeartBeatRateSensorDTO
                     * EnvironmentTemperatureSensorDTO
                     * GasSensorDTO
                     */
                    if (pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getSensor()==1) {
                        System.out.println("Size D0 "+pSensors.getpSensorsSnapshots().size()+" V "+pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue());
                        if (x == 0) {
                            //series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series1.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev1 = true;
                        }
                        if (x == 1) {
                            //series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series2.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev2 = true;
                        }
                        if (x == 2) {
                            //series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), Long.parseLong(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue()));
                            series3.set(pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().getValue(), pSensors.getpSensorsSnapshots().get(i).getpSensorSnapshot().GetValueAsDouble());
                            bhavev3 = true;
                        }
                        d = d + 1;
                    }
                }

            }
        }
        if (!bhavev1 && !bhavev2 && !bhavev3) {
            series1.set(1, 1);
            getLinearModel().addSeries(series1);
        }
        if (!b1 && !b2 && !b3) {
            series1.set(1, 1);
            getLinearModel().addSeries(series1);
        }
        if (b1) {
            if (bhavev1 = false) {
                series1.set(1, 1);
            }
            getLinearModel().addSeries(series1);
        }
        if (b2) {
            if (bhavev2 = false) {
                series2.set(1, 1);
            }
            getLinearModel().addSeries(series2);
        }
        if (b3) {
            if (bhavev3 = false) {
                series3.set(1, 1);
            }
            getLinearModel().addSeries(series3);
        }
    }

    private void createLinearModel() {

        setLinearModel(new CartesianChartModel());
        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("");
        series1.set(1, 2);
        getLinearModel().addSeries(series1);


        setLinearModel2(new CartesianChartModel());
        LineChartSeries series2 = new LineChartSeries();
        series2.setLabel("");
        series2.set(1, 2);
        getLinearModel2().addSeries(series2);

        setLinearModel3(new CartesianChartModel());
        LineChartSeries series3 = new LineChartSeries();
        series3.setLabel("");
        series3.set(1, 2);
        getLinearModel3().addSeries(series3);

        setLinearModel4(new CartesianChartModel());
        LineChartSeries series4 = new LineChartSeries();
        series4.setLabel("");
        series4.set(1, 2);
        getLinearModel4().addSeries(series4);

        setLinearModel5(new CartesianChartModel());
        LineChartSeries series5 = new LineChartSeries();
        series5.setLabel("");
        series5.set(1, 2);
        getLinearModel5().addSeries(series5);

        setLinearModel6(new CartesianChartModel());
        LineChartSeries series6 = new LineChartSeries();
        series6.setLabel("");
        series6.set(1, 2);
        getLinearModel6().addSeries(series6);

    }

    /**
     * @return the linearModel
     */
    public CartesianChartModel getLinearModel() {
        return linearModel;
    }

    /**
     * @param linearModel the linearModel to set
     */
    public void setLinearModel(CartesianChartModel linearModel) {
        this.linearModel = linearModel;
    }

    /**
     * @return the selectedactor
     */
    public List<String> getSelectedactor() {
        return selectedactor;
    }

    /**
     * @param selectedactor the selectedactor to set
     */
    public void setSelectedactor(List<String> selectedactor) {
        this.selectedactor = selectedactor;
        updateDiagram();
        updateDiagram2();
        updateDiagram3();
        updateDiagram4();
        updateDiagram5();
        updateDiagram6();
    }

    /**
     * @return the actors
     */
    public Map<String, String> getActors() {
        return actors;
    }

    public void chooseactor() {
        updateDiagram();
        updateDiagram2();
        updateDiagram3();
        updateDiagram4();
        updateDiagram5();
        updateDiagram6();
    }

    /**
     * @param actors the actors to set
     */
    public void setActors(Map<String, String> actors) {
        this.actors = actors;
    }

    /**
     * @return the linearModel2
     */
    public CartesianChartModel getLinearModel2() {
        return linearModel2;
    }

    /**
     * @param linearModel2 the linearModel2 to set
     */
    public void setLinearModel2(CartesianChartModel linearModel2) {
        this.linearModel2 = linearModel2;
    }

    /**
     * @return the linearModel3
     */
    public CartesianChartModel getLinearModel3() {
        return linearModel3;
    }

    /**
     * @param linearModel3 the linearModel3 to set
     */
    public void setLinearModel3(CartesianChartModel linearModel3) {
        this.linearModel3 = linearModel3;
    }

    /**
     * @return the linearModel4
     */
    public CartesianChartModel getLinearModel4() {
        return linearModel4;
    }

    /**
     * @param linearModel4 the linearModel4 to set
     */
    public void setLinearModel4(CartesianChartModel linearModel4) {
        this.linearModel4 = linearModel4;
    }

    /**
     * @return the linearModel5
     */
    public CartesianChartModel getLinearModel5() {
        return linearModel5;
    }

    /**
     * @param linearModel5 the linearModel5 to set
     */
    public void setLinearModel5(CartesianChartModel linearModel5) {
        this.linearModel5 = linearModel5;
    }

    /**
     * @return the linearModel6
     */
    public CartesianChartModel getLinearModel6() {
        return linearModel6;
    }

    /**
     * @param linearModel6 the linearModel6 to set
     */
    public void setLinearModel6(CartesianChartModel linearModel6) {
        this.linearModel6 = linearModel6;
    }
}
