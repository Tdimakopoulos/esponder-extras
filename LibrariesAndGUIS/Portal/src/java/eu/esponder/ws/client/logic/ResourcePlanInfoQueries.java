/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.ws.client.logic;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.category.ConsumableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.OperationsCentreCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PlannableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.crisis.resource.plan.PlannableResourcePowerDTO;
import eu.esponder.objects.ResourcePlanInfo;
import eu.esponder.ws.client.logic.CrisisTypeQueryManager;
import eu.esponder.ws.client.query.QueryManager;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author tdim
 */
public class ResourcePlanInfoQueries {

    private List<ResourcePlanInfo> pPersonnelPlanInfo;
    private List<ResourcePlanInfo> pLogisticPlanInfo;
    private List<ResourcePlanInfo> pOCPlanInfo;
    //private List<ResourcePlanInfo> pLogisticPlanInfo;
    
    public List<ResourcePlanInfo> GetQueryLists(int i,String plantitle) throws IOException
    {
        QueryRestful(plantitle);
        if (i==1)
            return pPersonnelPlanInfo;
        if (i==2)
            return pLogisticPlanInfo;
        if (i==3)
            return pOCPlanInfo;
        return null;
    }
    
    public String GetresourceCategoryforPersonnel(Long CategoryID) throws IOException
    {
        QueryManager pMan = new QueryManager();
        PlannableResourceCategoryDTO pCategory = pMan.getresourceCategory("1", CategoryID.toString());
                if (pCategory instanceof PersonnelCategoryDTO) {
                    PersonnelCategoryDTO pperson = (PersonnelCategoryDTO) pCategory;
                    return pperson.getOrganisationCategory().getDisciplineType().getTitle();
                }
          return null;      
    }
    
        public String GetresourceCategoryforRegResuable(Long CategoryID) throws IOException
    {
        QueryManager pMan = new QueryManager();
        PlannableResourceCategoryDTO pCategory = pMan.getresourceCategory("1", CategoryID.toString());
                if (pCategory instanceof ReusableResourceCategoryDTO) {
                    ReusableResourceCategoryDTO pres = (ReusableResourceCategoryDTO) pCategory;
                 //   System.out.println("res type " + pres.getReusableResourceType().getTitle());
                   // getpLogisticPlanInfo().add(new ResourcePlanInfo(pres.getReusableResourceType().getTitle(),pitem.getPower(),0));
                    return pres.getReusableResourceType().getTitle();
                }
          return null;      
    }

            public String GetresourceCategoryforRegConsumables(Long CategoryID) throws IOException
    {
        QueryManager pMan = new QueryManager();
        PlannableResourceCategoryDTO pCategory = pMan.getresourceCategory("1", CategoryID.toString());
                if (pCategory instanceof ConsumableResourceCategoryDTO) {
                    ConsumableResourceCategoryDTO pcons = (ConsumableResourceCategoryDTO) pCategory;
                  //  System.out.println("cons type " + pcons.getConsumableResourceType().getTitle());
//                    getpLogisticPlanInfo().add(new ResourcePlanInfo(pcons.getConsumableResourceType().getTitle(),pitem.getPower(),0));
                    return pcons.getConsumableResourceType().getTitle();
                }
          return null;      
    }

                public String GetresourceCategoryforRegOC(Long CategoryID) throws IOException
    {
        QueryManager pMan = new QueryManager();
        PlannableResourceCategoryDTO pCategory = pMan.getresourceCategory("1", CategoryID.toString());
                 if (pCategory instanceof OperationsCentreCategoryDTO) {
                    OperationsCentreCategoryDTO popecenter = (OperationsCentreCategoryDTO) pCategory;
                 //   System.out.println("opcen type " + popecenter.getOperationsCentreType().getTitle());
                   // getpOCPlanInfo().add(new ResourcePlanInfo(popecenter.getOperationsCentreType().getTitle(),pitem.getPower(),0));
                    return popecenter.getOperationsCentreType().getTitle();
                }
          return null;      
    }

    public void QueryRestful(String plantitle) throws JsonParseException, JsonMappingException, IOException {
        QueryManager pMan = new QueryManager();
        pPersonnelPlanInfo=new ArrayList<ResourcePlanInfo>();
        pLogisticPlanInfo=new ArrayList<ResourcePlanInfo>();
        pOCPlanInfo=new ArrayList<ResourcePlanInfo>();

        ResultListDTO pre = pMan.getCrisisResourcePlanAll("1");
        ObjectMapper mapper = new ObjectMapper();
        for (int i = 0; i < pre.getResultList().size(); i++) {
            CrisisResourcePlanDTO pItem = (CrisisResourcePlanDTO) pre.getResultList().get(i);
            if(pItem.getCrisisType().getTitle().equalsIgnoreCase(plantitle)){
            Set<PlannableResourcePowerDTO> pStorage = pItem.getPlannableResources();
            System.out.println("plan title " + pItem.getTitle());
            //System.out.println(pItem.getCrisisType().getTitle());
            Object[] arrayView = pStorage.toArray();
            for (int d = 0; d < arrayView.length; d++) {
                PlannableResourcePowerDTO pitem = (PlannableResourcePowerDTO) arrayView[d];
                
                System.out.println("item power " + pitem.getPower());
                System.out.println("item constraint " + pitem.getConstraint());
                //System.out.println(pitem.getPlanableResourceCategoryId());
                PlannableResourceCategoryDTO pCategory = pMan.getresourceCategory("1", pitem.getPlanableResourceCategoryId().toString());
                if (pCategory instanceof PersonnelCategoryDTO) {
                    PersonnelCategoryDTO pperson = (PersonnelCategoryDTO) pCategory;
                    //System.out.println(pperson.getRank().getTitle());
                    //System.out.println(pperson.getOrganisationCategory().getOrganisationType().getTitle());
                    System.out.println("personnel type " + pperson.getOrganisationCategory().getDisciplineType().getTitle());
                    getpPersonnelPlanInfo().add(new ResourcePlanInfo(pperson.getOrganisationCategory().getDisciplineType().getTitle(),pitem.getPower(),0));

                }
                if (pCategory instanceof OperationsCentreCategoryDTO) {
                    OperationsCentreCategoryDTO popecenter = (OperationsCentreCategoryDTO) pCategory;
                    System.out.println("opcen type " + popecenter.getOperationsCentreType().getTitle());
                    getpOCPlanInfo().add(new ResourcePlanInfo(popecenter.getOperationsCentreType().getTitle(),pitem.getPower(),0));
                }

                if (pCategory instanceof ConsumableResourceCategoryDTO) {
                    ConsumableResourceCategoryDTO pcons = (ConsumableResourceCategoryDTO) pCategory;
                    System.out.println("cons type " + pcons.getConsumableResourceType().getTitle());
                    getpLogisticPlanInfo().add(new ResourcePlanInfo(pcons.getConsumableResourceType().getTitle(),pitem.getPower(),0));
                }

                if (pCategory instanceof ReusableResourceCategoryDTO) {
                    ReusableResourceCategoryDTO pres = (ReusableResourceCategoryDTO) pCategory;
                    System.out.println("res type " + pres.getReusableResourceType().getTitle());
                    getpLogisticPlanInfo().add(new ResourcePlanInfo(pres.getReusableResourceType().getTitle(),pitem.getPower(),0));
                }
            }
        }
        }
    }

    /**
     * @return the pPersonnelPlanInfo
     */
    public List<ResourcePlanInfo> getpPersonnelPlanInfo() {
        return pPersonnelPlanInfo;
    }

    /**
     * @param pPersonnelPlanInfo the pPersonnelPlanInfo to set
     */
    public void setpPersonnelPlanInfo(List<ResourcePlanInfo> pPersonnelPlanInfo) {
        this.pPersonnelPlanInfo = pPersonnelPlanInfo;
    }

    /**
     * @return the pLogisticPlanInfo
     */
    public List<ResourcePlanInfo> getpLogisticPlanInfo() {
        return pLogisticPlanInfo;
    }

    /**
     * @param pLogisticPlanInfo the pLogisticPlanInfo to set
     */
    public void setpLogisticPlanInfo(List<ResourcePlanInfo> pLogisticPlanInfo) {
        this.pLogisticPlanInfo = pLogisticPlanInfo;
    }

    /**
     * @return the pOCPlanInfo
     */
    public List<ResourcePlanInfo> getpOCPlanInfo() {
        return pOCPlanInfo;
    }

    /**
     * @param pOCPlanInfo the pOCPlanInfo to set
     */
    public void setpOCPlanInfo(List<ResourcePlanInfo> pOCPlanInfo) {
        this.pOCPlanInfo = pOCPlanInfo;
    }
}
