package eu.esponder.ws.client.urlmanager;

public class UrlManager {


    String szURLHost = "http://lists.exodussa.com:";
//    String szURLHost="http://192.168.2.248:";

    String szURLPort = "8080";
    
    private String cparamscreate = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisContextParameter/create";
    private String cparamsdelete = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisContextParameter/delete";
    private String cparamsfind = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisContextParameter/findAllByCrisis";
    private String cparamsupdate = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisContextParameter/update";
    
    String szGetAllSensorSnapshosts = szURLHost + szURLPort + "/esponder-restful/crisis/snapshot/sensorsnapshotfindall";
    String szGetAllEsponderTypes = szURLHost + szURLPort + "/esponder-restful/portal/types/findAll";
    String szSpherePathCreate = szURLHost + szURLPort + "/esponder-restful/crisis/generic/create";
    String szGetActorByID = szURLHost + szURLPort + "/esponder-restful/crisis/resource/actor/getID";
    String szCreateCrisisContext = szURLHost + szURLPort + "/esponder-restful/crisis/generic/create";
    
    String szCreateRegisteredOC = szURLHost + szURLPort + "/esponder-restful/crisis/context/regoc/create";
    String szGetAllRegisteredOC = szURLHost + szURLPort + "/esponder-restful/crisis/context/regoc/findAll";
    String szCreateRegisterConsumableResource = szURLHost + szURLPort + "/esponder-restful/crisis/view/regConsumables/create";
    String szCreateRegisterReusableResource = szURLHost + szURLPort + "/esponder-restful/crisis/view/reusables/create";
    String szOperationCenterFindAll = szURLHost + szURLPort + "/esponder-restful/crisis/context/oc/findAll";
    String szPersonnelFindAll = szURLHost + szURLPort + "/esponder-restful/crisis/view/personnel/findAll";
    String szRegisterReusablesFindAll = szURLHost + szURLPort + "/esponder-restful/crisis/view/regReusables/findAll";
    String szRegisterConsumablesFindAll = szURLHost + szURLPort + "/esponder-restful/crisis/view/regConsumables/findAll";
    String szConsumablesFindAll = szURLHost + szURLPort + "/esponder-restful/crisis/view/consumables/findAll";
    String szCrisisResourcePlanFindAll = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisResourcePlan/findAll";
    String szCrisisContextFindAll = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisContext/findAll";
    String szReusablesFindAll = szURLHost + szURLPort + "/esponder-restful/crisis/view/reusables/findAll";
    String szCreateAction = szURLHost + szURLPort + "/esponder-restful/crisis/view/action/create";
    String szCreateActionPart = szURLHost + szURLPort + "/esponder-restful/crisis/view/actionPart/create";
    String szCrisisContentUpdate = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisContext/update";
    String szResourcePlanUpdate = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisResourcePlan/update";
    String szResourcePlanCreate = szURLHost + szURLPort + "/esponder-restful/crisis/view//crisisResourcePlan/create";
    String szUpdateRegisteredOC = szURLHost + szURLPort + "/esponder-restful/crisis/context/regoc/update";
    String szgetAllEquipments = szURLHost + szURLPort + "/esponder-restful/crisis/view/equipments/findByID";
    String szgetEventsBySeverity = szURLHost + szURLPort + "/esponder-restful/events/view/osgievents/getBySeverity";
    String szgetEventsByID = szURLHost + szURLPort + "/esponder-restful/events/view/osgievents/getByID";
    private String szCreateCrisisWithEvent = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisContext/createwithevent";
    private String szPublishCrisisWithEvent = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisContext/publishcrisiscontextevent";
    private String szresourceplanfindall = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisResourcePlan/findAll";
    private String szcreatecrisissnapshot = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisContextSnapshot/create";
    private String szgetplanablecategories = szURLHost + szURLPort + "/esponder-restful/portal/categories/findById";
    private String szCrisisContextFindbyID = szURLHost + szURLPort + "/esponder-restful/crisis/view/crisisContext/findByID";
    private String szLogicassociateactor = szURLHost + szURLPort + "/esponder-restful/business/logic/crisis/associateActor";
    private String szLogicdeassociateactor = szURLHost + szURLPort + "/esponder-restful/business/logic/crisis/deassociateActor";
    private String szLogicassociatereusables = szURLHost + szURLPort + "/esponder-restful/business/logic/crisis/associateReusable";
    private String szLogicdeassociatereusables = szURLHost + szURLPort + "/esponder-restful/business/logic/crisis/deassociateReusable";
    private String szLogicassociateconsumables = szURLHost + szURLPort + "/esponder-restful/business/logic/crisis/associateConsumable";
    private String szLogicdeassociateconsumables = szURLHost + szURLPort + "/esponder-restful/business/logic/crisis/deassociateConsumable";
    private String szLogicassociateoc = szURLHost + szURLPort + "/esponder-restful/business/logic/crisis/associateoc";
    private String szLogicdeassociateoc = szURLHost + szURLPort + "/esponder-restful/business/logic/crisis/deassociateoc";
    private String szSensorSnapshotGetAll = szURLHost + szURLPort + "/esponder-restful/business/logic/sensorsnapshots/getfulldetails";
    private String szCreateCrisisContextSnapshot = szURLHost + szURLPort + "/esponder-restful/business/logic/crisis/createcrisissnapshot";


    private String szDFSync = szURLHost + szURLPort + "/esponder-restful/datafusion/repository/remotetolocal";
    private String szDFdeletelocal = szURLHost + szURLPort + "/esponder-restful/datafusion/repository/deleterepo";
    private String szDFswitchlocal = szURLHost + szURLPort + "/esponder-restful/datafusion/repository/switchtolocal";
    private String szDFswitchguvnor = szURLHost + szURLPort + "/esponder-restful/datafusion/repository/switchtoguvnor";
    private String szDFruleresults = szURLHost + szURLPort + "/esponder-restful/datafusion/repository/ruleresults";
    
    
    private String szccdeassociateactor = szURLHost + szURLPort + "/esponder-restful/business/logic/crisis/deassociateActor";
    private String szccassociateactor = szURLHost + szURLPort + "/esponder-restful/business/logic/crisis/associateActor";
    
    private String szpersonnelfindbytitle = szURLHost + szURLPort + "/esponder-restful/crisis/view/personnel/findByTitle";
    
    //user
    private String szpuserfindall = szURLHost + szURLPort + "/esponder-restful/portal/users/findAll";
        private String szpuserfindbyid = szURLHost + szURLPort + "/esponder-restful/portal/users/findById";
        private String szpusercreate = szURLHost + szURLPort + "/esponder-restful/portal/users/createUser";
        private String szpuserupdate = szURLHost + szURLPort + "/esponder-restful/portal/users/updateUser";
        private String szpuserdelete = szURLHost + szURLPort + "/esponder-restful/portal/users/deleteUser";
    private String szgetallsnapshots = szURLHost + szURLPort + "/esponder-restful/business/logic/sensorsnapshots/getAllSnapshotsByActor";//getAllSnapshotsByAllActors";
    private String szrunsimulator = szURLHost + szURLPort + "/esponder-restful/business/logic/sensorsnapshots/getAllSnapshotsByAllActors";
    
    private String szResourceCategoryFindForOrganization=szURLHost + szURLPort + "/esponder-restful/portal/categories/ResourceCategories/findOrganisationCategoryByType";
    
    private String szOrganizationcreate=szURLHost + szURLPort + "/esponder-restful/portal/categories/Organization/create";
    
    private String szResourceCategoryOrganizationUpdate=szURLHost + szURLPort + "/esponder-restful/portal/categories/ResourceCategories/updateOrganisationCategory";
            
    private String szOrganizationsFindAll=szURLHost + szURLPort + "/esponder-restful/business/logic/org/findAll";
    private String szReusableCatByType=szURLHost + szURLPort + "/esponder-restful/portal/categories/ResourceCategories/findReusableCategoryByType";
    
    private String szregReusableCreate=szURLHost + szURLPort + "/esponder-restful/crisis/view/regReusables/create";
    
    private String sztest=szURLHost + szURLPort + "/esponder-restful/optimizer/oc/createeoc";
    
    public String getSzgetEventsByID() {
        return szgetEventsByID;
    }

    public void setSzgetEventsByID(String szgetEventsByID) {
        this.szgetEventsByID = szgetEventsByID;
    }

    public String getSzgetEventsBySeverity() {
        return szgetEventsBySeverity;
    }

    public void setSzgetEventsBySeverity(String szgetEventsBySeverity) {
        this.szgetEventsBySeverity = szgetEventsBySeverity;
    }

    public String getSzGetAllEsponderTypes() {
        return szGetAllEsponderTypes;
    }

    public String getSzgetAllEquipments() {
        return szgetAllEquipments;
    }

    public void setSzgetAllEquipments(String szgetAllEquipments) {
        this.szgetAllEquipments = szgetAllEquipments;
    }

    public void setSzGetAllEsponderTypes(String szGetAllEsponderTypes) {
        this.szGetAllEsponderTypes = szGetAllEsponderTypes;
    }

    public String getSzGetAllSensorSnapshosts() {
        return szGetAllSensorSnapshosts;
    }

    public void setSzGetAllSensorSnapshosts(String szGetAllSensorSnapshosts) {
        this.szGetAllSensorSnapshosts = szGetAllSensorSnapshosts;
    }

    public String getSzUpdateRegisteredOC() {
        return szUpdateRegisteredOC;
    }

    public void setSzUpdateRegisteredOC(String szUpdateRegisteredOC) {
        this.szUpdateRegisteredOC = szUpdateRegisteredOC;
    }

    public String getSzResourcePlanCreate() {
        return szResourcePlanCreate;
    }

    public void setSzResourcePlanCreate(String szResourcePlanCreate) {
        this.szResourcePlanCreate = szResourcePlanCreate;
    }

    public String getSzCreateAction() {
        return szCreateAction;
    }

    public String getSzCrisisContentUpdate() {
        return szCrisisContentUpdate;
    }

    public void setSzCrisisContentUpdate(String szCrisisContentUpdate) {
        this.szCrisisContentUpdate = szCrisisContentUpdate;
    }

    public String getSzResourcePlanUpdate() {
        return szResourcePlanUpdate;
    }

    public void setSzResourcePlanUpdate(String szResourcePlanUpdate) {
        this.szResourcePlanUpdate = szResourcePlanUpdate;
    }

    public void setSzCreateAction(String szCreateAction) {
        this.szCreateAction = szCreateAction;
    }

    public String getSzCreateActionPart() {
        return szCreateActionPart;
    }

    public void setSzCreateActionPart(String szCreateActionPart) {
        this.szCreateActionPart = szCreateActionPart;
    }

    public String getSzOperationCenterFindAll() {
        return szOperationCenterFindAll;
    }

    public void setSzOperationCenterFindAll(String szOperationCenterFindAll) {
        this.szOperationCenterFindAll = szOperationCenterFindAll;
    }

    public String getSzPersonnelFindAll() {
        return szPersonnelFindAll;
    }

    public void setSzPersonnelFindAll(String szPersonnelFindAll) {
        this.szPersonnelFindAll = szPersonnelFindAll;
    }

    public String getSzRegisterReusablesFindAll() {
        return szRegisterReusablesFindAll;
    }

    public void setSzRegisterReusablesFindAll(String szRegisterReusablesFindAll) {
        this.szRegisterReusablesFindAll = szRegisterReusablesFindAll;
    }

    public String getSzRegisterConsumablesFindAll() {
        return szRegisterConsumablesFindAll;
    }

    public void setSzRegisterConsumablesFindAll(String szRegisterConsumablesFindAll) {
        this.szRegisterConsumablesFindAll = szRegisterConsumablesFindAll;
    }

    public String getSzConsumablesFindAll() {
        return szConsumablesFindAll;
    }

    public void setSzConsumablesFindAll(String szConsumablesFindAll) {
        this.szConsumablesFindAll = szConsumablesFindAll;
    }

    public String getSzCrisisResourcePlanFindAll() {
        return szCrisisResourcePlanFindAll;
    }

    public void setSzCrisisResourcePlanFindAll(String szCrisisResourcePlanFindAll) {
        this.szCrisisResourcePlanFindAll = szCrisisResourcePlanFindAll;
    }

    public String getSzCrisisContextFindAll() {
        return szCrisisContextFindAll;
    }

    public void setSzCrisisContextFindAll(String szCrisisContextFindAll) {
        this.szCrisisContextFindAll = szCrisisContextFindAll;
    }

    public String getSzReusablesFindAll() {
        return szReusablesFindAll;
    }

    public void setSzReusablesFindAll(String szReusablesFindAll) {
        this.szReusablesFindAll = szReusablesFindAll;
    }

    public String getSzCreateRegisterReusableResource() {
        return szCreateRegisterReusableResource;
    }

    public void setSzCreateRegisterReusableResource(
            String szCreateRegisterReusableResource) {
        this.szCreateRegisterReusableResource = szCreateRegisterReusableResource;
    }

    public String getSzCreateRegisterConsumableResource() {
        return szCreateRegisterConsumableResource;
    }

    public void setSzCreateRegisterConsumableResource(
            String szCreateRegisterConsumableResource) {
        this.szCreateRegisterConsumableResource = szCreateRegisterConsumableResource;
    }

    public String getSzURLHost() {
        return szURLHost;
    }

    public void setSzURLHost(String szURLHost) {
        this.szURLHost = szURLHost;
    }

    public String getSzURLPort() {
        return szURLPort;
    }

    public void setSzURLPort(String szURLPort) {
        this.szURLPort = szURLPort;
    }

    public String getSzSpherePathCreate() {
        return szSpherePathCreate;
    }

    public void setSzSpherePathCreate(String szSpherePathCreate) {
        this.szSpherePathCreate = szSpherePathCreate;
    }

    public String getSzGetActorByID() {
        return szGetActorByID;
    }

    public void setSzGetActorByID(String szGetActorByID) {
        this.szGetActorByID = szGetActorByID;
    }

    public String getSzCreateCrisisContext() {
        return szCreateCrisisContext;
    }

    public void setSzCreateCrisisContext(String szCreateCrisisContext) {
        this.szCreateCrisisContext = szCreateCrisisContext;
    }

    public String getSzCreateRegisteredOC() {
        return szCreateRegisteredOC;
    }

    public void setSzCreateRegisteredOC(String szCreateRegisteredOC) {
        this.szCreateRegisteredOC = szCreateRegisteredOC;
    }

    public String getSzGetAllRegisteredOC() {
        return szGetAllRegisteredOC;
    }

    public void setSzGetAllRegisteredOC(String szGetAllRegisteredOC) {
        this.szGetAllRegisteredOC = szGetAllRegisteredOC;
    }

    public String getSzCreateCrisisWithEvent() {
        return szCreateCrisisWithEvent;
    }

    public void setSzCreateCrisisWithEvent(String szCreateCrisisWithEvent) {
        this.szCreateCrisisWithEvent = szCreateCrisisWithEvent;
    }

    /**
     * @return the szresourceplanfindall
     */
    public String getSzresourceplanfindall() {
        return szresourceplanfindall;
    }

    /**
     * @param szresourceplanfindall the szresourceplanfindall to set
     */
    public void setSzresourceplanfindall(String szresourceplanfindall) {
        this.szresourceplanfindall = szresourceplanfindall;
    }

    /**
     * @return the szcreatecrisissnapshot
     */
    public String getSzcreatecrisissnapshot() {
        return szcreatecrisissnapshot;
    }

    /**
     * @param szcreatecrisissnapshot the szcreatecrisissnapshot to set
     */
    public void setSzcreatecrisissnapshot(String szcreatecrisissnapshot) {
        this.szcreatecrisissnapshot = szcreatecrisissnapshot;
    }

    /**
     * @return the szgetplanablecategories
     */
    public String getSzgetplanablecategories() {
        return szgetplanablecategories;
    }

    /**
     * @param szgetplanablecategories the szgetplanablecategories to set
     */
    public void setSzgetplanablecategories(String szgetplanablecategories) {
        this.szgetplanablecategories = szgetplanablecategories;
    }

    /**
     * @return the szCrisisContextFindbyID
     */
    public String getSzCrisisContextFindbyID() {
        return szCrisisContextFindbyID;
    }

    /**
     * @param szCrisisContextFindbyID the szCrisisContextFindbyID to set
     */
    public void setSzCrisisContextFindbyID(String szCrisisContextFindbyID) {
        this.szCrisisContextFindbyID = szCrisisContextFindbyID;
    }

    /**
     * @return the szLogicassociateactor
     */
    public String getSzLogicassociateactor() {
        return szLogicassociateactor;
    }

    /**
     * @param szLogicassociateactor the szLogicassociateactor to set
     */
    public void setSzLogicassociateactor(String szLogicassociateactor) {
        this.szLogicassociateactor = szLogicassociateactor;
    }

    /**
     * @return the szLogicdeassociateactor
     */
    public String getSzLogicdeassociateactor() {
        return szLogicdeassociateactor;
    }

    /**
     * @param szLogicdeassociateactor the szLogicdeassociateactor to set
     */
    public void setSzLogicdeassociateactor(String szLogicdeassociateactor) {
        this.szLogicdeassociateactor = szLogicdeassociateactor;
    }

    /**
     * @return the szLogicassociatereusables
     */
    public String getSzLogicassociatereusables() {
        return szLogicassociatereusables;
    }

    /**
     * @param szLogicassociatereusables the szLogicassociatereusables to set
     */
    public void setSzLogicassociatereusables(String szLogicassociatereusables) {
        this.szLogicassociatereusables = szLogicassociatereusables;
    }

    /**
     * @return the szLogicdeassociatereusables
     */
    public String getSzLogicdeassociatereusables() {
        return szLogicdeassociatereusables;
    }

    /**
     * @param szLogicdeassociatereusables the szLogicdeassociatereusables to set
     */
    public void setSzLogicdeassociatereusables(String szLogicdeassociatereusables) {
        this.szLogicdeassociatereusables = szLogicdeassociatereusables;
    }

    /**
     * @return the szLogicassociateconsumables
     */
    public String getSzLogicassociateconsumables() {
        return szLogicassociateconsumables;
    }

    /**
     * @param szLogicassociateconsumables the szLogicassociateconsumables to set
     */
    public void setSzLogicassociateconsumables(String szLogicassociateconsumables) {
        this.szLogicassociateconsumables = szLogicassociateconsumables;
    }

    /**
     * @return the szLogicdeassociateconsumables
     */
    public String getSzLogicdeassociateconsumables() {
        return szLogicdeassociateconsumables;
    }

    /**
     * @param szLogicdeassociateconsumables the szLogicdeassociateconsumables to
     * set
     */
    public void setSzLogicdeassociateconsumables(String szLogicdeassociateconsumables) {
        this.szLogicdeassociateconsumables = szLogicdeassociateconsumables;
    }

    /**
     * @return the szLogicassociateoc
     */
    public String getSzLogicassociateoc() {
        return szLogicassociateoc;
    }

    /**
     * @param szLogicassociateoc the szLogicassociateoc to set
     */
    public void setSzLogicassociateoc(String szLogicassociateoc) {
        this.szLogicassociateoc = szLogicassociateoc;
    }

    /**
     * @return the szLogicdeassociateoc
     */
    public String getSzLogicdeassociateoc() {
        return szLogicdeassociateoc;
    }

    /**
     * @param szLogicdeassociateoc the szLogicdeassociateoc to set
     */
    public void setSzLogicdeassociateoc(String szLogicdeassociateoc) {
        this.szLogicdeassociateoc = szLogicdeassociateoc;
    }

    /**
     * @return the szSensorSnapshotGetAll
     */
    public String getSzSensorSnapshotGetAll() {
        return szSensorSnapshotGetAll;
    }

    /**
     * @param szSensorSnapshotGetAll the szSensorSnapshotGetAll to set
     */
    public void setSzSensorSnapshotGetAll(String szSensorSnapshotGetAll) {
        this.szSensorSnapshotGetAll = szSensorSnapshotGetAll;
    }

    /**
     * @return the szCreateCrisisContextSnapshot
     */
    public String getSzCreateCrisisContextSnapshot() {
        return szCreateCrisisContextSnapshot;
    }

    /**
     * @param szCreateCrisisContextSnapshot the szCreateCrisisContextSnapshot to
     * set
     */
    public void setSzCreateCrisisContextSnapshot(String szCreateCrisisContextSnapshot) {
        this.szCreateCrisisContextSnapshot = szCreateCrisisContextSnapshot;
    }

    /**
     * @return the szDFSync
     */
    public String getSzDFSync() {
        return szDFSync;
    }

    /**
     * @param szDFSync the szDFSync to set
     */
    public void setSzDFSync(String szDFSync) {
        this.szDFSync = szDFSync;
    }

    /**
     * @return the szDFdeletelocal
     */
    public String getSzDFdeletelocal() {
        return szDFdeletelocal;
    }

    /**
     * @param szDFdeletelocal the szDFdeletelocal to set
     */
    public void setSzDFdeletelocal(String szDFdeletelocal) {
        this.szDFdeletelocal = szDFdeletelocal;
    }

    /**
     * @return the szDFswitchlocal
     */
    public String getSzDFswitchlocal() {
        return szDFswitchlocal;
    }

    /**
     * @param szDFswitchlocal the szDFswitchlocal to set
     */
    public void setSzDFswitchlocal(String szDFswitchlocal) {
        this.szDFswitchlocal = szDFswitchlocal;
    }

    /**
     * @return the szDFswitchguvnor
     */
    public String getSzDFswitchguvnor() {
        return szDFswitchguvnor;
    }

    /**
     * @param szDFswitchguvnor the szDFswitchguvnor to set
     */
    public void setSzDFswitchguvnor(String szDFswitchguvnor) {
        this.szDFswitchguvnor = szDFswitchguvnor;
    }

    /**
     * @return the szccdeassociateactor
     */
    public String getSzccdeassociateactor() {
        return szccdeassociateactor;
    }

    /**
     * @param szccdeassociateactor the szccdeassociateactor to set
     */
    public void setSzccdeassociateactor(String szccdeassociateactor) {
        this.szccdeassociateactor = szccdeassociateactor;
    }

    /**
     * @return the szccassociateactor
     */
    public String getSzccassociateactor() {
        return szccassociateactor;
    }

    /**
     * @param szccassociateactor the szccassociateactor to set
     */
    public void setSzccassociateactor(String szccassociateactor) {
        this.szccassociateactor = szccassociateactor;
    }

    /**
     * @return the szpersonnelfindbytitle
     */
    public String getSzpersonnelfindbytitle() {
        return szpersonnelfindbytitle;
    }

    /**
     * @param szpersonnelfindbytitle the szpersonnelfindbytitle to set
     */
    public void setSzpersonnelfindbytitle(String szpersonnelfindbytitle) {
        this.szpersonnelfindbytitle = szpersonnelfindbytitle;
    }

    /**
     * @return the szpuserfindall
     */
    public String getSzpuserfindall() {
        return szpuserfindall;
    }

    /**
     * @param szpuserfindall the szpuserfindall to set
     */
    public void setSzpuserfindall(String szpuserfindall) {
        this.szpuserfindall = szpuserfindall;
    }

    /**
     * @return the szpuserfindbyid
     */
    public String getSzpuserfindbyid() {
        return szpuserfindbyid;
    }

    /**
     * @param szpuserfindbyid the szpuserfindbyid to set
     */
    public void setSzpuserfindbyid(String szpuserfindbyid) {
        this.szpuserfindbyid = szpuserfindbyid;
    }

    /**
     * @return the szpusercreate
     */
    public String getSzpusercreate() {
        return szpusercreate;
    }

    /**
     * @param szpusercreate the szpusercreate to set
     */
    public void setSzpusercreate(String szpusercreate) {
        this.szpusercreate = szpusercreate;
    }

    /**
     * @return the szpuserupdate
     */
    public String getSzpuserupdate() {
        return szpuserupdate;
    }

    /**
     * @param szpuserupdate the szpuserupdate to set
     */
    public void setSzpuserupdate(String szpuserupdate) {
        this.szpuserupdate = szpuserupdate;
    }

    /**
     * @return the szpuserdelete
     */
    public String getSzpuserdelete() {
        return szpuserdelete;
    }

    /**
     * @param szpuserdelete the szpuserdelete to set
     */
    public void setSzpuserdelete(String szpuserdelete) {
        this.szpuserdelete = szpuserdelete;
    }

    /**
     * @return the szDFruleresults
     */
    public String getSzDFruleresults() {
        return szDFruleresults;
    }

    /**
     * @param szDFruleresults the szDFruleresults to set
     */
    public void setSzDFruleresults(String szDFruleresults) {
        this.szDFruleresults = szDFruleresults;
    }

    /**
     * @return the szgetallsnapshots
     */
    public String getSzgetallsnapshots() {
        return szgetallsnapshots;
    }

    /**
     * @param szgetallsnapshots the szgetallsnapshots to set
     */
    public void setSzgetallsnapshots(String szgetallsnapshots) {
        this.szgetallsnapshots = szgetallsnapshots;
    }

    /**
     * @return the szrunsimulator
     */
    public String getSzrunsimulator() {
        return szrunsimulator;
    }

    /**
     * @param szrunsimulator the szrunsimulator to set
     */
    public void setSzrunsimulator(String szrunsimulator) {
        this.szrunsimulator = szrunsimulator;
    }

    /**
     * @return the cparamscreate
     */
    public String getCparamscreate() {
        return cparamscreate;
    }

    /**
     * @param cparamscreate the cparamscreate to set
     */
    public void setCparamscreate(String cparamscreate) {
        this.cparamscreate = cparamscreate;
    }

    /**
     * @return the cparamsdelete
     */
    public String getCparamsdelete() {
        return cparamsdelete;
    }

    /**
     * @param cparamsdelete the cparamsdelete to set
     */
    public void setCparamsdelete(String cparamsdelete) {
        this.cparamsdelete = cparamsdelete;
    }

    /**
     * @return the cparamsfind
     */
    public String getCparamsfind() {
        return cparamsfind;
    }

    /**
     * @param cparamsfind the cparamsfind to set
     */
    public void setCparamsfind(String cparamsfind) {
        this.cparamsfind = cparamsfind;
    }

    /**
     * @return the cparamsupdate
     */
    public String getCparamsupdate() {
        return cparamsupdate;
    }

    /**
     * @param cparamsupdate the cparamsupdate to set
     */
    public void setCparamsupdate(String cparamsupdate) {
        this.cparamsupdate = cparamsupdate;
    }

    /**
     * @return the szPublishCrisisWithEvent
     */
    public String getSzPublishCrisisWithEvent() {
        return szPublishCrisisWithEvent;
    }

    /**
     * @param szPublishCrisisWithEvent the szPublishCrisisWithEvent to set
     */
    public void setSzPublishCrisisWithEvent(String szPublishCrisisWithEvent) {
        this.szPublishCrisisWithEvent = szPublishCrisisWithEvent;
    }

    /**
     * @return the szResourceCategoryFindForOrganization
     */
    public String getSzResourceCategoryFindForOrganization() {
        return szResourceCategoryFindForOrganization;
    }

    /**
     * @param szResourceCategoryFindForOrganization the szResourceCategoryFindForOrganization to set
     */
    public void setSzResourceCategoryFindForOrganization(String szResourceCategoryFindForOrganization) {
        this.szResourceCategoryFindForOrganization = szResourceCategoryFindForOrganization;
    }

    /**
     * @return the szOrganizationcreate
     */
    public String getSzOrganizationcreate() {
        return szOrganizationcreate;
    }

    /**
     * @param szOrganizationcreate the szOrganizationcreate to set
     */
    public void setSzOrganizationcreate(String szOrganizationcreate) {
        this.szOrganizationcreate = szOrganizationcreate;
    }

    /**
     * @return the szResourceCategoryUpdate
     */
    public String getSzResourceCategoryUpdate() {
        return szResourceCategoryOrganizationUpdate;
    }

    /**
     * @param szResourceCategoryUpdate the szResourceCategoryUpdate to set
     */
    public void setSzResourceCategoryUpdate(String szResourceCategoryUpdate) {
        this.szResourceCategoryOrganizationUpdate = szResourceCategoryUpdate;
    }

    /**
     * @return the szOrganizationsFindAll
     */
    public String getSzOrganizationsFindAll() {
        return szOrganizationsFindAll;
    }

    /**
     * @param szOrganizationsFindAll the szOrganizationsFindAll to set
     */
    public void setSzOrganizationsFindAll(String szOrganizationsFindAll) {
        this.szOrganizationsFindAll = szOrganizationsFindAll;
    }

    /**
     * @return the szReusableCatByType
     */
    public String getSzReusableCatByType() {
        return szReusableCatByType;
    }

    /**
     * @param szReusableCatByType the szReusableCatByType to set
     */
    public void setSzReusableCatByType(String szReusableCatByType) {
        this.szReusableCatByType = szReusableCatByType;
    }

    /**
     * @return the szregReusableCreate
     */
    public String getSzregReusableCreate() {
        return szregReusableCreate;
    }

    /**
     * @param szregReusableCreate the szregReusableCreate to set
     */
    public void setSzregReusableCreate(String szregReusableCreate) {
        this.szregReusableCreate = szregReusableCreate;
    }

    /**
     * @return the sztest
     */
    public String getSztest() {
        return sztest;
    }

    /**
     * @param sztest the sztest to set
     */
    public void setSztest(String sztest) {
        this.sztest = sztest;
    }
}
