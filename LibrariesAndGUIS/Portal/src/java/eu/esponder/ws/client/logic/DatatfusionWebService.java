/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.ws.client.logic;

import eu.esponder.rest.datafusion.RuleResults;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import eu.esponder.ws.client.urlmanager.UrlManager;
import java.io.IOException;
import javax.ws.rs.core.MultivaluedMap;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author tdim
 */
public class DatatfusionWebService {

    UrlManager URL = new UrlManager();
    ObjectMapper mapper = new ObjectMapper();
    public void SyncRepository(String userID) {


        Client client = Client.create();
        WebResource webResource = client.resource(URL.getSzDFSync());

        // Initialize Parameters
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);


        // Call Webservice
        String szReturn = webResource.queryParams(queryParams)
                .get(String.class);

        

        
    }

    public void SwitchToLocal(String userID) {
        Client client = Client.create();
        WebResource webResource = client.resource(URL.getSzDFswitchlocal());

        // Initialize Parameters
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);


        // Call Webservice
        String szReturn = webResource.queryParams(queryParams)
                .get(String.class);
    }

    public void SwitchToGuvnor(String userID) {
        Client client = Client.create();
        WebResource webResource = client.resource(URL.getSzDFswitchguvnor());

        // Initialize Parameters
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);


        // Call Webservice
        String szReturn = webResource.queryParams(queryParams)
                .get(String.class);
    }

    public void DeleteLocal(String userID) {
        Client client = Client.create();
        WebResource webResource = client.resource(URL.getSzDFdeletelocal());

        // Initialize Parameters
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);


        // Call Webservice
        String szReturn = webResource.queryParams(queryParams)
                .get(String.class);
    }
    
    public RuleResults getRuleResults(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		RuleResults pResults = null;
		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzDFruleresults());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
                queryParams.add("sessionID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, RuleResults.class);

		return pResults;
	}
}
