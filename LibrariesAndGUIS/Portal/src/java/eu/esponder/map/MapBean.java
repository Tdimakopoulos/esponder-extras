/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.map;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextAlertEnumDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.CrisisContextParameterDTO;
import eu.esponder.dto.model.crisis.action.CrisisContextStatusEnumDTO;
import eu.esponder.map.beans.MarkerList;
import eu.esponder.newcrisis.crisiselementsmanagerbean;
import eu.esponder.objects.CrisisContextDash;
import eu.esponder.objects.CrisisParams;
import eu.esponder.ws.client.crisisparams.crisisparamswsmanager;
import eu.esponder.ws.client.logic.CrisisTypeQueryManager;
import eu.esponder.ws.client.query.QueryManager;
import eu.esponder.ws.client.update.UpdateOperations;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.primefaces.context.RequestContext;
import org.primefaces.event.DashboardReorderEvent;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Circle;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class MapBean implements Serializable {

    private DashboardModel model;
    private MapModel circleModel;
    private DashboardModel pMapmodel;
    private List<CrisisContextDash> pDashList = new ArrayList();
    private CrisisContextDash pSelectedCrisis;
    private String status;
    private String addinfo;
    private String id;
    private String titlec;
    private Date date1;
    private Date date2;
    private Date date3;
    private List<String> naturalDisaster;
    private Map<String, String> nDisaster;
    private List<String> otherDisaster;
    private Map<String, String> oDisaster;
    private MapModel emptyModel;
    private String title;
    private double lat;
    private double lng;
    private CrisisTypeQueryManager pct = new CrisisTypeQueryManager();
    private crisiselementsmanagerbean.CrisisContextAlert alertlevel;
    private List<CrisisParams> crisisParams;

    private CrisisParams pSelectedCrisisParams;
    private String szname;
    private String szvalue;
    
    
    public String addparams()
    {
        szname="";
        szvalue="";
        return "AddCrisisParams?faces-redirect=true";
    }
    
    public String saveparams()
    {
        crisisparamswsmanager pcreate= new crisisparamswsmanager();
        try {
            pcreate.CreateParam(String.valueOf(pSelectedCrisis.getpCrisisDTO().getId()), szname, szvalue, "", "");
        } catch (Exception ex) {
            Logger.getLogger(MapBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        LoadParams();
        return "EditParams?faces-redirect=true";
    }
    
    public String deleteparams()
    {
        crisisparamswsmanager pcreate= new crisisparamswsmanager();
        try {
            pcreate.deletepcrisis("", "", String.valueOf(pSelectedCrisisParams.getId()));//.CreateParam(String.valueOf(pSelectedCrisis.getpCrisisDTO().getId()), szname, szvalue, "", "");
        System.out.println("Delete : "+String.valueOf(pSelectedCrisisParams.getId()));
        } catch (Exception ex) {
            Logger.getLogger(MapBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        LoadParams();
        return "EditParams?faces-redirect=true";
    }
    
    
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the addinfo
     */
    public String getAddinfo() {
        return addinfo;
    }

    /**
     * @param addinfo the addinfo to set
     */
    public void setAddinfo(String addinfo) {
        this.addinfo = addinfo;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the titlec
     */
    public String getTitlec() {
        return titlec;
    }

    /**
     * @param titlec the titlec to set
     */
    public void setTitlec(String titlec) {
        this.titlec = titlec;
    }

    /**
     * @return the date1
     */
    public Date getDate1() {
        return date1;
    }

    /**
     * @param date1 the date1 to set
     */
    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    /**
     * @return the date2
     */
    public Date getDate2() {
        return date2;
    }

    /**
     * @param date2 the date2 to set
     */
    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    /**
     * @return the date3
     */
    public Date getDate3() {
        return date3;
    }

    /**
     * @param date3 the date3 to set
     */
    public void setDate3(Date date3) {
        this.date3 = date3;
    }

    /**
     * @return the naturalDisaster
     */
    public List<String> getNaturalDisaster() {
        return naturalDisaster;
    }

    /**
     * @param naturalDisaster the naturalDisaster to set
     */
    public void setNaturalDisaster(List<String> naturalDisaster) {
        this.naturalDisaster = naturalDisaster;
    }

    /**
     * @return the nDisaster
     */
    public Map<String, String> getnDisaster() {
        return nDisaster;
    }

    /**
     * @param nDisaster the nDisaster to set
     */
    public void setnDisaster(Map<String, String> nDisaster) {
        this.nDisaster = nDisaster;
    }

    /**
     * @return the otherDisaster
     */
    public List<String> getOtherDisaster() {
        return otherDisaster;
    }

    /**
     * @param otherDisaster the otherDisaster to set
     */
    public void setOtherDisaster(List<String> otherDisaster) {
        this.otherDisaster = otherDisaster;
    }

    /**
     * @return the oDisaster
     */
    public Map<String, String> getoDisaster() {
        return oDisaster;
    }

    /**
     * @param oDisaster the oDisaster to set
     */
    public void setoDisaster(Map<String, String> oDisaster) {
        this.oDisaster = oDisaster;
    }

    /**
     * @return the emptyModel
     */
    public MapModel getEmptyModel() {
        return emptyModel;
    }

    /**
     * @param emptyModel the emptyModel to set
     */
    public void setEmptyModel(MapModel emptyModel) {
        this.emptyModel = emptyModel;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the lat
     */
    public double getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * @return the lng
     */
    public double getLng() {
        return lng;
    }

    /**
     * @param lng the lng to set
     */
    public void setLng(double lng) {
        this.lng = lng;
    }

    /**
     * @return the pct
     */
    public CrisisTypeQueryManager getPct() {
        return pct;
    }

    /**
     * @param pct the pct to set
     */
    public void setPct(CrisisTypeQueryManager pct) {
        this.pct = pct;
    }

    
    /**
     * @return the alertlevel
     */
    public crisiselementsmanagerbean.CrisisContextAlert getAlertlevel() {
        return alertlevel;
    }

    /**
     * @param alertlevel the alertlevel to set
     */
    public void setAlertlevel(crisiselementsmanagerbean.CrisisContextAlert alertlevel) {
        this.alertlevel = alertlevel;
    }

    /**
     * @return the values
     */
    public crisiselementsmanagerbean.CrisisContextAlert[] getValues() {
        return values;
    }

    /**
     * @param values the values to set
     */
    public void setValues(crisiselementsmanagerbean.CrisisContextAlert[] values) {
        this.values = values;
    }

    /**
     * @return the crisisParams
     */
    public List<CrisisParams> getCrisisParams() {
        return crisisParams;
    }

    /**
     * @param crisisParams the crisisParams to set
     */
    public void setCrisisParams(List<CrisisParams> crisisParams) {
        this.crisisParams = crisisParams;
    }

    /**
     * @return the pSelectedCrisisParams
     */
    public CrisisParams getpSelectedCrisisParams() {
        return pSelectedCrisisParams;
    }

    /**
     * @param pSelectedCrisisParams the pSelectedCrisisParams to set
     */
    public void setpSelectedCrisisParams(CrisisParams pSelectedCrisisParams) {
        this.pSelectedCrisisParams = pSelectedCrisisParams;
    }

    /**
     * @return the szname
     */
    public String getSzname() {
        return szname;
    }

    /**
     * @param szname the szname to set
     */
    public void setSzname(String szname) {
        this.szname = szname;
    }

    /**
     * @return the szvalue
     */
    public String getSzvalue() {
        return szvalue;
    }

    /**
     * @param szvalue the szvalue to set
     */
    public void setSzvalue(String szvalue) {
        this.szvalue = szvalue;
    }

    public enum CrisisContextAlert {

        Level1,
        Level2,
        Level3,
        Level4,
        Level5
    }
    private crisiselementsmanagerbean.CrisisContextAlert values[] = crisiselementsmanagerbean.CrisisContextAlert.values();

    //call edit
    public String updateInfoCrisis() {
        addinfo = pSelectedCrisis.getpCrisisDTO().getCrisisAdditionalInfo();
        Date pdate3 = new Date();
        pdate3.setTime(pSelectedCrisis.getpCrisisDTO().getEndDate());
        date2 = pdate3;
        if (pSelectedCrisis.getpCrisisDTO().getCrisisStatus() == CrisisContextStatusEnumDTO.INPROGRESS) {
            status = "In Progress";
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisStatus() == CrisisContextStatusEnumDTO.FINISHED) {
            status = "Finished";
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisContextAlert() == CrisisContextAlertEnumDTO.Level1) {
            alertlevel = alertlevel.Level1;
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisContextAlert() == CrisisContextAlertEnumDTO.Level2) {
            alertlevel = alertlevel.Level2;
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisContextAlert() == CrisisContextAlertEnumDTO.Level3) {
            alertlevel = alertlevel.Level3;
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisContextAlert() == CrisisContextAlertEnumDTO.Level4) {
            alertlevel = alertlevel.Level4;
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisContextAlert() == CrisisContextAlertEnumDTO.Level5) {
            alertlevel = alertlevel.Level5;
        }
        return "EditCrisis?faces-redirect=true";
    }

    public String updateInfoCrisisFromManager() {
        addinfo = pSelectedCrisis.getpCrisisDTO().getCrisisAdditionalInfo();
        Date pdate3 = new Date();
        pdate3.setTime(pSelectedCrisis.getpCrisisDTO().getEndDate());
        date2 = pdate3;
        if (pSelectedCrisis.getpCrisisDTO().getCrisisStatus() == CrisisContextStatusEnumDTO.INPROGRESS) {
            status = "In Progress";
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisStatus() == CrisisContextStatusEnumDTO.FINISHED) {
            status = "Finished";
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisContextAlert() == CrisisContextAlertEnumDTO.Level1) {
            alertlevel = alertlevel.Level1;
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisContextAlert() == CrisisContextAlertEnumDTO.Level2) {
            alertlevel = alertlevel.Level2;
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisContextAlert() == CrisisContextAlertEnumDTO.Level3) {
            alertlevel = alertlevel.Level3;
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisContextAlert() == CrisisContextAlertEnumDTO.Level4) {
            alertlevel = alertlevel.Level4;
        }
        if (pSelectedCrisis.getpCrisisDTO().getCrisisContextAlert() == CrisisContextAlertEnumDTO.Level5) {
            alertlevel = alertlevel.Level5;
        }
        return "EditCrisisM?faces-redirect=true";
    }

    public String updateInfoCrisisSave() {
        CrisisContextDTO pdto = pSelectedCrisis.getpCrisisDTO();
        pdto.setEndDate(date2.getTime());

        if (status.equalsIgnoreCase("In Progress")) {
            pdto.setCrisisStatus(CrisisContextStatusEnumDTO.INPROGRESS);
        }
        if (status.equalsIgnoreCase("Finished")) {
            pdto.setCrisisStatus(CrisisContextStatusEnumDTO.FINISHED);
        }
        pdto.setCrisisAdditionalInfo(addinfo);
        if (CrisisContextAlertEnumDTO.Level1.toString() == alertlevel.toString()) {
            pdto.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level1);
        }
        if (CrisisContextAlertEnumDTO.Level2.toString() == alertlevel.toString()) {
            pdto.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level2);
        }
        if (CrisisContextAlertEnumDTO.Level3.toString() == alertlevel.toString()) {
            pdto.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level3);
        }
        if (CrisisContextAlertEnumDTO.Level4.toString() == alertlevel.toString()) {
            pdto.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level4);
        }
        if (CrisisContextAlertEnumDTO.Level5.toString() == alertlevel.toString()) {
            pdto.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level5);
        }
        UpdateOperations pUpdateop = new UpdateOperations();
        try {
            pUpdateop.updateCrisisContext("1", pdto);
        } catch (JsonGenerationException ex) {
            Logger.getLogger(MapBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(MapBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MapBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        Refresh();
        return "EsponderDashBoard?faces-redirect=true";
    }

    public String updateInfoCrisisSaveFromManager() {
        CrisisContextDTO pdto = pSelectedCrisis.getpCrisisDTO();
        pdto.setEndDate(date2.getTime());

        if (status.equalsIgnoreCase("In Progress")) {
            pdto.setCrisisStatus(CrisisContextStatusEnumDTO.INPROGRESS);
        }
        if (status.equalsIgnoreCase("Finished")) {
            pdto.setCrisisStatus(CrisisContextStatusEnumDTO.FINISHED);
        }
        pdto.setCrisisAdditionalInfo(addinfo);
        if (CrisisContextAlertEnumDTO.Level1.toString() == alertlevel.toString()) {
            pdto.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level1);
        }
        if (CrisisContextAlertEnumDTO.Level2.toString() == alertlevel.toString()) {
            pdto.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level2);
        }
        if (CrisisContextAlertEnumDTO.Level3.toString() == alertlevel.toString()) {
            pdto.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level3);
        }
        if (CrisisContextAlertEnumDTO.Level4.toString() == alertlevel.toString()) {
            pdto.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level4);
        }
        if (CrisisContextAlertEnumDTO.Level5.toString() == alertlevel.toString()) {
            pdto.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level5);
        }
        UpdateOperations pUpdateop = new UpdateOperations();
        try {
            pUpdateop.updateCrisisContext("1", pdto);
        } catch (JsonGenerationException ex) {
            Logger.getLogger(MapBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(MapBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MapBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        Refresh();
        return "CrisisManager?faces-redirect=true";
    }

    public String manageSubcrisis() {
        return "EsponderDashBoard?faces-redirect=true";
    }

    public void LoadParams()
    {
        crisisparamswsmanager pque=new crisisparamswsmanager();
        ResultListDTO presults=pque.FindAllForCrisis(String.valueOf(pSelectedCrisis.getpCrisisDTO().getId()), null, null);
        System.out.println(presults.getResultList().size());
         //ist<CrisisParams> 
        crisisParams=new ArrayList<CrisisParams>();
        for(int i=0;i<presults.getResultList().size();i++)
        {
            CrisisContextParameterDTO pEntry=(CrisisContextParameterDTO)presults.getResultList().get(i);
            CrisisParams pEntry2= new CrisisParams();
            pEntry2.setId(pEntry.getId());
            pEntry2.setSzname(pEntry.getParamName());
            pEntry2.setSzvalue(pEntry.getParamValue());
            crisisParams.add(pEntry2);
        }
    }
    public String manageParams() {
        LoadParams();
        return "EditParams?faces-redirect=true";
    }

    public String manageSubcrisisFromManager() {

        return "CrisisManager?faces-redirect=true";
    }

    public String manageParamsFromManager() {
        LoadParams();
        return "EditParams?faces-redirect=true";
        
    }
 public void RefreshActionListener(ActionEvent actionEvent)
    {
        System.out.println("Refresh Called");
       Refresh(); 
    }
    public void handleReorder(DashboardReorderEvent event) {
        FacesMessage message = new FacesMessage();
        message.setSeverity(FacesMessage.SEVERITY_INFO);
        message.setSummary("Reordered: " + event.getWidgetId());
        message.setDetail("Item index: " + event.getItemIndex() + ", Column index: " + event.getColumnIndex() + ", Sender index: " + event.getSenderColumnIndex());

        addMessage(message);
    }

    public String LoadDash()
    {
        Refresh();
        return "EsponderDashBoard?faces-redirect=true";
        
    }
    public String LoadCrisisManager()
    {
        Refresh();
        return "CrisisManager?faces-redirect=true";
        
    }
    public void Refresh() {
        pDashList.clear();

        try {
            QueryManager pQuery = new QueryManager();
            ResultListDTO Results = pQuery.getCrisisContextAll("1");

            for (int i = 0; i < Results.getResultList().size(); i++) {
                CrisisContextDTO pItem = (CrisisContextDTO) Results.getResultList().get(i);
                double pLat = pItem.getCrisisLocation().getCentre().getLatitude().doubleValue();
                double pLon = pItem.getCrisisLocation().getCentre().getLongitude().doubleValue();
                LatLng coord4 = new LatLng(pLat, pLon);
                CrisisContextDash pEntry = new CrisisContextDash();
                pEntry.setId(pItem.getId());
                pEntry.setpCrisisDTO(pItem);
                if (pItem.getCrisisContextAlert() != null) {
                    pEntry.setSzAlert(pItem.getCrisisContextAlert().toString());
                }
                if (pItem.getCrisisStatus() == CrisisContextStatusEnumDTO.INPROGRESS) {
                    pEntry.setSzStatus("In Progress");
                } else if (pItem.getCrisisStatus() == CrisisContextStatusEnumDTO.FINISHED) {
                    pEntry.setSzStatus("Finished");
                }
                pEntry.setSzTitle(pItem.getTitle());
                Date pdate = new Date();
                if (pItem.getStartDate() != null) {
                    pdate.setTime(pItem.getStartDate());
                    pEntry.setpDateStart(pdate);
                }
                if (pItem.getEndDate() != null) {
                    pdate.setTime(pItem.getEndDate());
                    pEntry.setpDateEnd(pdate);
                }
                pDashList.add(pEntry);
                //Circle
                Circle circle1 = new Circle(coord4, 500);
                circle1.setStrokeColor("#d93c3c");
                circle1.setFillColor("#d93c3c");
                circle1.setFillOpacity(0.5);
                circle1.setId(pItem.getId().toString());

                circleModel.addOverlay(circle1);
            }
        } catch (JsonParseException ex) {
            Logger.getLogger(MarkerList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(MarkerList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MarkerList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public MapBean() {
        
//         RequestContext context = RequestContext.getCurrentInstance();
//        FacesMessage msg = null;
//        boolean loggedIn = false;

//        if (username != null && username.equals("admin") && password != null && password.equals("admin")) {
//            loggedIn = true;
//            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", username);
//        } else {
//            loggedIn = false;
//            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Error", "Invalid credentials");
//        }

//        FacesContext.getCurrentInstance().addMessage(null, msg);
//        context.addCallbackParam("loggedIn", loggedIn);
        
        pDashList.clear();
        pMapmodel = new DefaultDashboardModel();
        DashboardColumn column1 = new DefaultDashboardColumn();
        DashboardColumn column2 = new DefaultDashboardColumn();
        column1.addWidget("cmap");
        column1.addWidget("cdetails");
        column2.addWidget("extraspace");
        pMapmodel.addColumn(column1);
        pMapmodel.addColumn(column2);
        circleModel = new DefaultMapModel();


        ///

        model = new DefaultDashboardModel();
        DashboardColumn column3 = new DefaultDashboardColumn();


        column3.addWidget("details");
        column3.addWidget("typearea");



        column3.addWidget("additionalinfo");

        model.addColumn(column1);


        nDisaster = new HashMap<String, String>();

        oDisaster = new HashMap<String, String>();

        try {
            pct.LoadAllCrisisTypes();
        } catch (JsonParseException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < pct.getpCrisisDisasterTypes().size(); i++) {
            nDisaster.put(pct.getpCrisisDisasterTypes().get(i).getTitle(), pct.getpCrisisDisasterTypes().get(i).getTitle());
            System.out.println(pct.getpCrisisDisasterTypes().get(i).getTitle());
        }
        for (int i = 0; i < pct.getpCrisisFeatureTypes().size(); i++) {
            oDisaster.put(pct.getpCrisisFeatureTypes().get(i).getTitle(), pct.getpCrisisFeatureTypes().get(i).getTitle());
            System.out.println(pct.getpCrisisFeatureTypes().get(i).getTitle());
        }



        Refresh();
    }

    public MapModel getCircleModel() {
        return circleModel;
    }

    public void onCircleSelect(OverlaySelectEvent event) {

//            int iindex=event.getOverlay().getZindex();
        System.out.println("Circle Selected " + event.getOverlay().getId());
    }

    public void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * @return the pMapmodel
     */
    public DashboardModel getpMapmodel() {
        return pMapmodel;
    }

    /**
     * @param pMapmodel the pMapmodel to set
     */
    public void setpMapmodel(DashboardModel pMapmodel) {
        this.pMapmodel = pMapmodel;
    }

    /**
     * @return the pDashList
     */
    public List<CrisisContextDash> getpDashList() {
        return pDashList;
    }

    /**
     * @param pDashList the pDashList to set
     */
    public void setpDashList(List<CrisisContextDash> pDashList) {
        this.pDashList = pDashList;
    }

    /**
     * @return the pSelectedCrisis
     */
    public CrisisContextDash getpSelectedCrisis() {
        return pSelectedCrisis;
    }

    /**
     * @param pSelectedCrisis the pSelectedCrisis to set
     */
    public void setpSelectedCrisis(CrisisContextDash pSelectedCrisis) {
        this.pSelectedCrisis = pSelectedCrisis;
    }

    /**
     * @return the model
     */
    public DashboardModel getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(DashboardModel model) {
        this.model = model;
    }
}
