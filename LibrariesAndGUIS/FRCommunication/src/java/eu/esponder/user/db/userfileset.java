/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.user.db;

import java.io.Serializable;

/**
 *
 * @author MEOC-WS1
 */
public class userfileset implements Serializable {

    private Long id;
    private Long fileid;
    private Long typeid;
    private int status;
    private Long p1;
    private Long p2;
    private Long p3;
    private Long p4;
    private Long p5;
    private Long p6;
    private Long p7;
    private Long p8;
    private Long p9;
    private Long p10;
    private Long p11;
    private Long p12;
    private Long p13;
    private Long p14;
    private Long p15;
    private Long p16;
    private Long p17;
    private Long p18;
    private Long p19;
    private Long p20;
    private int imax;
    private String sp1;
    private String sp2;
    private String sp3;
    private String sp4;
    private String sp5;
    private String sp6;
    private String sp7;
    private String sp8;
    private String sp9;
    private String sp10;
    private String sp11;
    private String sp12;
    private String sp13;
    private String sp14;
    private String sp15;
    private String sp16;
    private String sp17;
    private String sp18;
    private String sp19;
    private String sp20;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the voipid
     */
    public Long getFileid() {
        return fileid;
    }

    /**
     * @param voipid the voipid to set
     */
    public void setFileid(Long fileid) {
        this.fileid = fileid;
    }

    /**
     * @return the typeid
     */
    public Long getTypeid() {
        return typeid;
    }

    /**
     * @param typeid the typeid to set
     */
    public void setTypeid(Long typeid) {
        this.typeid = typeid;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the p1
     */
    public Long getP1() {
        return p1;
    }

    /**
     * @param p1 the p1 to set
     */
    public void setP1(Long p1) {
        this.p1 = p1;
    }

    /**
     * @return the p2
     */
    public Long getP2() {
        return p2;
    }

    /**
     * @param p2 the p2 to set
     */
    public void setP2(Long p2) {
        this.p2 = p2;
    }

    /**
     * @return the p3
     */
    public Long getP3() {
        return p3;
    }

    /**
     * @param p3 the p3 to set
     */
    public void setP3(Long p3) {
        this.p3 = p3;
    }

    /**
     * @return the p4
     */
    public Long getP4() {
        return p4;
    }

    /**
     * @param p4 the p4 to set
     */
    public void setP4(Long p4) {
        this.p4 = p4;
    }

    /**
     * @return the p5
     */
    public Long getP5() {
        return p5;
    }

    /**
     * @param p5 the p5 to set
     */
    public void setP5(Long p5) {
        this.p5 = p5;
    }

    /**
     * @return the p6
     */
    public Long getP6() {
        return p6;
    }

    /**
     * @param p6 the p6 to set
     */
    public void setP6(Long p6) {
        this.p6 = p6;
    }

    /**
     * @return the p7
     */
    public Long getP7() {
        return p7;
    }

    /**
     * @param p7 the p7 to set
     */
    public void setP7(Long p7) {
        this.p7 = p7;
    }

    /**
     * @return the p8
     */
    public Long getP8() {
        return p8;
    }

    /**
     * @param p8 the p8 to set
     */
    public void setP8(Long p8) {
        this.p8 = p8;
    }

    /**
     * @return the p9
     */
    public Long getP9() {
        return p9;
    }

    /**
     * @param p9 the p9 to set
     */
    public void setP9(Long p9) {
        this.p9 = p9;
    }

    /**
     * @return the p10
     */
    public Long getP10() {
        return p10;
    }

    /**
     * @param p10 the p10 to set
     */
    public void setP10(Long p10) {
        this.p10 = p10;
    }

    /**
     * @return the p11
     */
    public Long getP11() {
        return p11;
    }

    /**
     * @param p11 the p11 to set
     */
    public void setP11(Long p11) {
        this.p11 = p11;
    }

    /**
     * @return the p12
     */
    public Long getP12() {
        return p12;
    }

    /**
     * @param p12 the p12 to set
     */
    public void setP12(Long p12) {
        this.p12 = p12;
    }

    /**
     * @return the p13
     */
    public Long getP13() {
        return p13;
    }

    /**
     * @param p13 the p13 to set
     */
    public void setP13(Long p13) {
        this.p13 = p13;
    }

    /**
     * @return the p14
     */
    public Long getP14() {
        return p14;
    }

    /**
     * @param p14 the p14 to set
     */
    public void setP14(Long p14) {
        this.p14 = p14;
    }

    /**
     * @return the p15
     */
    public Long getP15() {
        return p15;
    }

    /**
     * @param p15 the p15 to set
     */
    public void setP15(Long p15) {
        this.p15 = p15;
    }

    /**
     * @return the p16
     */
    public Long getP16() {
        return p16;
    }

    /**
     * @param p16 the p16 to set
     */
    public void setP16(Long p16) {
        this.p16 = p16;
    }

    /**
     * @return the p17
     */
    public Long getP17() {
        return p17;
    }

    /**
     * @param p17 the p17 to set
     */
    public void setP17(Long p17) {
        this.p17 = p17;
    }

    /**
     * @return the p18
     */
    public Long getP18() {
        return p18;
    }

    /**
     * @param p18 the p18 to set
     */
    public void setP18(Long p18) {
        this.p18 = p18;
    }

    /**
     * @return the p19
     */
    public Long getP19() {
        return p19;
    }

    /**
     * @param p19 the p19 to set
     */
    public void setP19(Long p19) {
        this.p19 = p19;
    }

    /**
     * @return the p20
     */
    public Long getP20() {
        return p20;
    }

    /**
     * @param p20 the p20 to set
     */
    public void setP20(Long p20) {
        this.p20 = p20;
    }

    /**
     * @return the imax
     */
    public int getImax() {
        return imax;
    }

    /**
     * @param imax the imax to set
     */
    public void setImax(int imax) {
        this.imax = imax;
    }

    /**
     * @return the sp1
     */
    public String getSp1() {
        return sp1;
    }

    /**
     * @param sp1 the sp1 to set
     */
    public void setSp1(String sp1) {
        this.sp1 = sp1;
    }

    /**
     * @return the sp2
     */
    public String getSp2() {
        return sp2;
    }

    /**
     * @param sp2 the sp2 to set
     */
    public void setSp2(String sp2) {
        this.sp2 = sp2;
    }

    /**
     * @return the sp3
     */
    public String getSp3() {
        return sp3;
    }

    /**
     * @param sp3 the sp3 to set
     */
    public void setSp3(String sp3) {
        this.sp3 = sp3;
    }

    /**
     * @return the sp4
     */
    public String getSp4() {
        return sp4;
    }

    /**
     * @param sp4 the sp4 to set
     */
    public void setSp4(String sp4) {
        this.sp4 = sp4;
    }

    /**
     * @return the sp5
     */
    public String getSp5() {
        return sp5;
    }

    /**
     * @param sp5 the sp5 to set
     */
    public void setSp5(String sp5) {
        this.sp5 = sp5;
    }

    /**
     * @return the sp6
     */
    public String getSp6() {
        return sp6;
    }

    /**
     * @param sp6 the sp6 to set
     */
    public void setSp6(String sp6) {
        this.sp6 = sp6;
    }

    /**
     * @return the sp7
     */
    public String getSp7() {
        return sp7;
    }

    /**
     * @param sp7 the sp7 to set
     */
    public void setSp7(String sp7) {
        this.sp7 = sp7;
    }

    /**
     * @return the sp8
     */
    public String getSp8() {
        return sp8;
    }

    /**
     * @param sp8 the sp8 to set
     */
    public void setSp8(String sp8) {
        this.sp8 = sp8;
    }

    /**
     * @return the sp9
     */
    public String getSp9() {
        return sp9;
    }

    /**
     * @param sp9 the sp9 to set
     */
    public void setSp9(String sp9) {
        this.sp9 = sp9;
    }

    /**
     * @return the sp10
     */
    public String getSp10() {
        return sp10;
    }

    /**
     * @param sp10 the sp10 to set
     */
    public void setSp10(String sp10) {
        this.sp10 = sp10;
    }

    /**
     * @return the sp11
     */
    public String getSp11() {
        return sp11;
    }

    /**
     * @param sp11 the sp11 to set
     */
    public void setSp11(String sp11) {
        this.sp11 = sp11;
    }

    /**
     * @return the sp12
     */
    public String getSp12() {
        return sp12;
    }

    /**
     * @param sp12 the sp12 to set
     */
    public void setSp12(String sp12) {
        this.sp12 = sp12;
    }

    /**
     * @return the sp13
     */
    public String getSp13() {
        return sp13;
    }

    /**
     * @param sp13 the sp13 to set
     */
    public void setSp13(String sp13) {
        this.sp13 = sp13;
    }

    /**
     * @return the sp14
     */
    public String getSp14() {
        return sp14;
    }

    /**
     * @param sp14 the sp14 to set
     */
    public void setSp14(String sp14) {
        this.sp14 = sp14;
    }

    /**
     * @return the sp15
     */
    public String getSp15() {
        return sp15;
    }

    /**
     * @param sp15 the sp15 to set
     */
    public void setSp15(String sp15) {
        this.sp15 = sp15;
    }

    /**
     * @return the sp16
     */
    public String getSp16() {
        return sp16;
    }

    /**
     * @param sp16 the sp16 to set
     */
    public void setSp16(String sp16) {
        this.sp16 = sp16;
    }

    /**
     * @return the sp17
     */
    public String getSp17() {
        return sp17;
    }

    /**
     * @param sp17 the sp17 to set
     */
    public void setSp17(String sp17) {
        this.sp17 = sp17;
    }

    /**
     * @return the sp18
     */
    public String getSp18() {
        return sp18;
    }

    /**
     * @param sp18 the sp18 to set
     */
    public void setSp18(String sp18) {
        this.sp18 = sp18;
    }

    /**
     * @return the sp19
     */
    public String getSp19() {
        return sp19;
    }

    /**
     * @param sp19 the sp19 to set
     */
    public void setSp19(String sp19) {
        this.sp19 = sp19;
    }

    /**
     * @return the sp20
     */
    public String getSp20() {
        return sp20;
    }

    /**
     * @param sp20 the sp20 to set
     */
    public void setSp20(String sp20) {
        this.sp20 = sp20;
    }
    
    
}
