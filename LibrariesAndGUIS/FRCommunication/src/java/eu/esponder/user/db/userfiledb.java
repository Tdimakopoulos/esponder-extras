/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.user.db;

import eu.esponder.osgi.settings.OsgiSettings;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MEOC-WS1
 */
public class userfiledb {

    ArrayList<userfileset> pdb = new ArrayList<userfileset>();
    int ipos;
    
    
   public void SaveOnFile() throws FileNotFoundException, IOException {
        OsgiSettings pset=new OsgiSettings();
        
        FileOutputStream fos = new FileOutputStream(pset.GetUserFileDataFileLocation());
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(pdb);
        oos.close();
    }

    public void LoadFronFile() throws FileNotFoundException, IOException, ClassNotFoundException {
        OsgiSettings pset=new OsgiSettings();
        pdb.clear();
        FileInputStream fis = new FileInputStream(pset.GetUserFileDataFileLocation());
        ObjectInputStream ois = new ObjectInputStream(fis);
        pdb = (ArrayList<userfileset>) (List<userfileset>) ois.readObject();
        ois.close();
        System.out.println("ID"+pdb.get(0).getId());
        ipos=pdb.size();
    }

    public userfileset getItem(int indexpos) {
        return pdb.get(indexpos);
    }

    public int getpos() {
        return ipos;
    }

    public void Add(userfileset pitem) {
        ipos = ipos + 1;
        pdb.add(pitem);
    }

    public boolean Edit(userfileset pitem) {
        for (int i = 0; i < pdb.size(); i++) {
            if (pdb.get(i).getId().toString().equalsIgnoreCase(pitem.getId().toString())) {
                pdb.set(i, pitem);
                return true;
            }
        }
        return false;
    }

    public boolean Delete(userfileset pitem) {
        for (int i = 0; i < pdb.size(); i++) {
            if (pdb.get(i).getId().toString().equalsIgnoreCase(pitem.getId().toString())) {
                pdb.remove(i);
                return true;
            }
        }
        return false;
    }
}
