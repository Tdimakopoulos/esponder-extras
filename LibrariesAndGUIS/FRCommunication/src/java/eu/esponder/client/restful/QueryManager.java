/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.client.restful;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.category.PlannableResourceCategoryDTO;
import java.io.IOException;
import javax.ws.rs.core.MultivaluedMap;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class QueryManager {
	ObjectMapper mapper = new ObjectMapper();
        
        String szURLHost = "http://192.168.2.248:";
        String szURLPort = "8080";
	private String szURLSendFile = szURLHost + szURLPort + "/esponder-restful/portal/files/sendfile";
        private String szURLSendMessage = szURLHost + szURLPort + "/esponder-restful/portal/notifications/sendmessage";

        private String szFindAllUsers = szURLHost + szURLPort + "/esponder-restful/portal/users/findAll";//?pkikey=pki
        
        
        public ResultListDTO getAllEsponderUsers()
            throws JsonParseException, JsonMappingException, IOException {

        // Variables
        ResultListDTO pResults = null;
        Client client = Client.create();
        WebResource webResource = client
                .resource(szFindAllUsers);

        // Initialize Parameters
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("pkikey", "pki");
        String szReturn = webResource.queryParams(queryParams)
                .get(String.class);

        // Convert to DTO
        pResults = mapper.readValue(szReturn, ResultListDTO.class);

        return pResults;
    }
        
        public  String SendFileRestfulCall(String imei, String host,String port,String dir,String file,String description)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		PlannableResourceCategoryDTO actorDTO = null;
		Client client = Client.create();
		WebResource webResource = client.resource(szURLSendFile);

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("imei", imei);
		queryParams.add("host", host);
                queryParams.add("port", port);
                queryParams.add("dir", dir);
                queryParams.add("file", file);
                queryParams.add("description", description);

		// Call Webservice
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		return szReturn;
	}
        
        public  String SendMessageRestfulCall(String imei, String origin,String origintype,String message)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		PlannableResourceCategoryDTO actorDTO = null;
		Client client = Client.create();
		WebResource webResource = client.resource(szURLSendMessage);

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("imei", imei);
		queryParams.add("origin", origin);
                queryParams.add("origintype", origintype);
                queryParams.add("message", message);

		// Call Webservice
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		return szReturn;
	}
}
