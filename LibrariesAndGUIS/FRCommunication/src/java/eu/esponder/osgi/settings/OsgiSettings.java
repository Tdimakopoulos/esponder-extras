/*
 * 
 */
package eu.esponder.osgi.settings;

import java.io.File;

//import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
//import eu.esponder.test.ResourceLocator;


// TODO: Auto-generated Javadoc
/**
 * The Class OsgiSettings.
 */
public class OsgiSettings {

    
    /** The sz properties file name unix. */
	private String szPropertiesFileNameUnixuserfile = "//home//exodus//osgi//userfileid.dat";

	// comment for unix, uncomment for windows
	/** The sz properties file name windows. */
	private String szPropertiesFileNameWindowsuserfile = "C://Development//userfileid.dat";
        
        

	/** The sz properties file name unix. */
	private String szPropertiesFileNameUnix = "//home//exodus//osgi//osgi.config.properties";

	// comment for unix, uncomment for windows
	/** The sz properties file name windows. */
	private String szPropertiesFileNameWindows = "C://Development//osgi.config.properties";

	/** The sz properties file name. */
	private String szPropertiesFileName = "";
	
	/** The sz properties file name unix. */
	private String szPropertiesFileNameUnixOtherSRV = "//home//exodus//osgi//osgiothersrv.config.properties";

	// comment for unix, uncomment for windows
	/** The sz properties file name windows. */
	private String szPropertiesFileNameWindowsOtherSRV = "C://Development//osgiothersrv.config.properties";

	/** The sz properties file name. */
	private String szPropertiesFileNameOtherSRV = "";

        public String GetUserFileDataFileLocation()   {

				if (isUnix()) {
					return szPropertiesFileNameUnixuserfile;
				} else {
					return szPropertiesFileNameWindowsuserfile;
				}
				
		

	}
        
	/**
	 * Checks if is unix.
	 *
	 * @return true, if is unix
	 */
	private boolean isUnix() {
		if (File.separatorChar == '/')
			return true;
		else
			return false;
	}

	/**
	 * Instantiates a new osgi settings.
	 */
	public OsgiSettings() {
		
			GetAllSettings();

		
	}

	/**
	 * Load settings.
	 */
	public void LoadSettings() {
		
			GetAllSettings();
		
	}

	/**
	 * Gets the all settings.
	 */
	private void GetAllSettings()   {

				if (isUnix()) {
					szPropertiesFileName = szPropertiesFileNameUnix;
				} else {
					szPropertiesFileName = szPropertiesFileNameWindows;
				}
				if (isUnix()) {
					szPropertiesFileNameOtherSRV = szPropertiesFileNameUnixOtherSRV;
				} else {
					szPropertiesFileNameOtherSRV = szPropertiesFileNameWindowsOtherSRV;
				}
		

	}

	/**
	 * Gets the sz properties file name.
	 *
	 * @return the sz properties file name
	 */
	public String getSzPropertiesFileNameOtherSRV() {
		if (szPropertiesFileNameOtherSRV == null || szPropertiesFileNameOtherSRV.length() < 2) {
			if (isUnix()) {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameUnixOtherSRV;
			} else {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameWindowsOtherSRV;
			}
		}
		return szPropertiesFileName;
	}

	/**
	 * Sets the sz properties file name.
	 *
	 * @param szPropertiesFileName the new sz properties file name
	 */
	public void setSzPropertiesFileNameOtherSRV(String szPropertiesFileNameOtherSRV) {
		this.szPropertiesFileNameOtherSRV = szPropertiesFileNameOtherSRV;
	}
	
	/**
	 * Gets the sz properties file name.
	 *
	 * @return the sz properties file name
	 */
	public String getSzPropertiesFileName() {
		if (szPropertiesFileName == null || szPropertiesFileName.length() < 2) {
			if (isUnix()) {
				szPropertiesFileName = szPropertiesFileNameUnix;
			} else {
				szPropertiesFileName = szPropertiesFileNameWindows;
			}
		}
		return szPropertiesFileName;
	}

	/**
	 * Sets the sz properties file name.
	 *
	 * @param szPropertiesFileName the new sz properties file name
	 */
	public void setSzPropertiesFileName(String szPropertiesFileName) {
		this.szPropertiesFileName = szPropertiesFileName;
	}
}
