/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.network.beans;

/**
 *
 * @author MEOC-WS1
 */
public class VOIPEntries {
   private String callid;
       
        private String attendants;
        private String starttime;
        private String imei;
        private String ip;
        private String type;
        
        
        public VOIPEntries(String model,  String manufacturer, String color, String color1, String color2, String color3) {
                this.callid = model;
       
                this.attendants = manufacturer;
                this.starttime = color;
                imei=color1;
                ip=color2;
                type=color3;
        }

    /**
     * @return the callid
     */
    public String getCallid() {
        return callid;
    }

    /**
     * @param callid the callid to set
     */
    public void setCallid(String callid) {
        this.callid = callid;
    }

    /**
     * @return the attendants
     */
    public String getAttendants() {
        return attendants;
    }

    /**
     * @param attendants the attendants to set
     */
    public void setAttendants(String attendants) {
        this.attendants = attendants;
    }

    /**
     * @return the starttime
     */
    public String getStarttime() {
        return starttime;
    }

    /**
     * @param starttime the starttime to set
     */
    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

    /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

      
}
