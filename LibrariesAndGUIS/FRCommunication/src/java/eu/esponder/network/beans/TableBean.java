/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.network.beans;



import eu.esponder.client.restful.QueryManager;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.ResultListDTO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;



import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.datafusion.DatafusionResultsDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.mobile.MobileFileNotifierEvent;
import eu.esponder.event.mobile.MobileMessageEvent;
import eu.esponder.event.model.datafusion.DatafusionResultsGeneratedEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;
import eu.esponder.osgi.threadsafe.server.main.EsponderEventServerThreadSafe;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.codehaus.jackson.map.JsonMappingException;
import org.primefaces.event.FileUploadEvent;


/**
 *
 * @author MEOC-WS1
 */
@ManagedBean
@SessionScoped
public class TableBean {

   
    private List<VOIPEntries> carsSmall;
    private VOIPEntries selectedCar;
private String firstname;
private String firstname22;
// URL Error 0
// File not supported 1
// File not Found 2 
// IO Exception 3 
    public TableBean() {
        
        carsSmall = new ArrayList<VOIPEntries>();
    
        populateRandomCars();
        
         System.out.println("Start");
//        EsponderEventServerThreadSafe pserver = new EsponderEventServerThreadSafe();
//        pserver.run();
        System.out.println("Finish");
    }

    public void populateRandomCars() {
        carsSmall.clear();
       System.out.println("Load");
       QueryManager pFind=new QueryManager();
       ResultListDTO pres=null; 
       try {
            pres=pFind.getAllEsponderUsers();
        } catch (JsonMappingException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TableBean.class.getName()).log(Level.SEVERE, null, ex);
        }
       for (int i=0;i<pres.getResultList().size();i++)
       {
           ESponderUserDTO puser=(ESponderUserDTO)pres.getResultList().get(i);
          
           if(puser.getIMEI().equalsIgnoreCase("NA")){}else{
                String myip=puser.getVoIPURL().getHost();
           carsSmall.add(new VOIPEntries(puser.getTitle(),  new Date().toString(),new Date().toString(),puser.getIMEI(),myip,puser.getUsername()));
           }
           }
//           carsSmall.add(new VOIPEntries("EOC Panou",  new Date().toString(),new Date().toString(),"N/A","192.168.2.249","EOC"));
//        carsSmall.add(new VOIPEntries("MEOC Panou",  new Date().toString(),new Date().toString(),"N/A","192.168.2.248","MEOC"));
//        carsSmall.add(new VOIPEntries("FRC",  new Date().toString(),new Date().toString(),"353581051146316","192.168.2.216","FRC"));
       
    }
    
       
    public VOIPEntries getSelectedCar() {
        return selectedCar;
    }

    public void setSelectedCar(VOIPEntries selectedCar) {
        this.selectedCar = selectedCar;
    }

    public List<VOIPEntries> getCarsSmall() {
        return carsSmall;
    }
 public void handleFileUpload(FileUploadEvent event) {
        try {
            File targetFolder = new File("/var/www/html/mob/ftp");
            InputStream inputStream = event.getFile().getInputstream();
            OutputStream out = new FileOutputStream(new File(targetFolder,
                    event.getFile().getFileName()));
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            inputStream.close();
            out.flush();
            out.close();
            SendFile(selectedCar.getImei(),"192.168.2.28","80","mob",event.getFile().getFileName(),"Filename "+event.getFile().getFileName());
            System.out.println("Filename to upload : "+event.getFile().getFileName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
 public String SendFile(
			String imei,
			String host,
			String port,
			String dir,
			String file,
			String description) {
		
			ESponderEventPublisher<MobileFileNotifierEvent> publisher = 
					new ESponderEventPublisher<MobileFileNotifierEvent>(MobileFileNotifierEvent.class);
			
			MobileFileNotifierEvent event = new MobileFileNotifierEvent();
                        String id;
                        Date d=new Date();
                        id=String.valueOf(d.getTime());
			event.setFileid(id);
                        event.setSourcemeoc("MEOC Panou");
                        event.setDescription(description);
                        //%20
                        file=file.replace(" ", "%20");
                        
			event.setFileURL("http://"+host+"/"+dir+"/ftp/"+file);
			event.setImei(imei);
			event.setEventSeverity(SeverityLevelDTO.SERIOUS);
			event.setEventTimestamp(new Date());
			event.setJournalMessage("File notification has been generated for FRC with imei "+imei);
			event.setJournalMessageInfo("File notification has been generated for FRC with imei "+imei);
			
                        System.out.println("http://"+host+"/"+dir+"/ftp/"+file);
                                
			try {
				publisher.publishEvent(event);
			} catch (Exception e) {
				return e.getMessage();
			}
			publisher.CloseConnection();
                        firstname="File send : "+event.getFileURL()+ " To "+event.getImei();
			return "Success";
	}
  public void sendalarm() {
 ESponderEventPublisher<DatafusionResultsGeneratedEvent> publisher = 
					new ESponderEventPublisher<DatafusionResultsGeneratedEvent>(DatafusionResultsGeneratedEvent.class);
			Long frid=new Long(4);
			DatafusionResultsGeneratedEvent event = new DatafusionResultsGeneratedEvent();
			DatafusionResultsDTO dfResult = new DatafusionResultsDTO();
			dfResult.setId(frid);
			dfResult.setDetails1("");
			dfResult.setResultsText1(firstname22);
			dfResult.setResultsText2(String.valueOf(frid));
			event.setEventAttachment(dfResult);
			event.setEventSeverity(SeverityLevelDTO.SERIOUS);
			event.setEventTimestamp(new Date());
			event.setJournalMessage("Alarm generated for FR with id : "+frid);
			event.setJournalMessageInfo("Alarm generated for FR with id : "+frid);
			
			try {
				publisher.publishEvent(event);
			} catch (Exception e) {
				System.out.println("Error on Publish Message"+e.getMessage());	
			}
			publisher.CloseConnection();
}
 
 public void sendmessage() {
    
        System.out.println("Message Send : "+selectedCar.getImei()+"  "+firstname);
        
        ESponderEventPublisher<MobileMessageEvent> publisher = 
					new ESponderEventPublisher<MobileMessageEvent>(MobileMessageEvent.class);
			
			MobileMessageEvent event  = new MobileMessageEvent();
			event.setMobileimei(selectedCar.getImei());
			event.setSourceMEoc("MEOC Panou");
			event.setEventSeverity(SeverityLevelDTO.MEDIUM);
			event.setEventTimestamp(new Date());
			event.setJournalMessage("Notification generated from MEOC");
			event.setJournalMessageInfo("Notification generated from MEOC");
			event.setSzMessage(firstname);
			event.setEventAttachment(new ESponderUserDTO());
			
			try {
				publisher.publishEvent(event);
			} catch (Exception e) {
			 System.out.println("Error on Publish Message"+e.getMessage());	
			}
			publisher.CloseConnection();
                        
    }
            
    public void deleteCar() {
       carsSmall.remove(selectedCar);
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the firstname22
     */
    public String getFirstname22() {
        return firstname22;
    }

    /**
     * @param firstname22 the firstname22 to set
     */
    public void setFirstname22(String firstname22) {
        this.firstname22 = firstname22;
    }

    
}
