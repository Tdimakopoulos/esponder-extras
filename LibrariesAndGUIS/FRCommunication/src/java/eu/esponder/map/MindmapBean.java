/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.UUID;  
import org.primefaces.event.SelectEvent;  
import org.primefaces.model.mindmap.DefaultMindmapNode;  
import org.primefaces.model.mindmap.MindmapNode;  
/**
 *
 * @author MEOC-WS1
 */
@ManagedBean
@SessionScoped
public class MindmapBean {

   private MindmapNode root;  
      
    private MindmapNode selectedNode;  
      
    public MindmapBean() {  
        root = new DefaultMindmapNode("EOC Panou", "Eoc in Panou", "FFCC00", false);  
          
        MindmapNode ips = new DefaultMindmapNode("Meocs", "meocs", "6e9ebf", true);  
        MindmapNode ns = new DefaultMindmapNode("FRCs", "FRCs", "6e9ebf", true);  
        MindmapNode malware = new DefaultMindmapNode("Teams", "Teams", "6e9ebf", true);  
          
        root.addNode(ips);  
        root.addNode(ns);  
        root.addNode(malware);  
    }  
  
    public MindmapNode getRoot() {  
        return root;  
    }  
  
    public MindmapNode getSelectedNode() {  
        return selectedNode;  
    }  
    public void setSelectedNode(MindmapNode selectedNode) {  
        this.selectedNode = selectedNode;  
    }  
  
    public void onNodeSelect(SelectEvent event) {  
        MindmapNode node = (MindmapNode) event.getObject();  
          
        //populate if not already loaded  
        if(node.getChildren().isEmpty()) {  
            Object label = node.getLabel();  
  
            if(label.equals("Meocs")) {  
                for(int i = 0; i < 25; i++) {  
                    node.addNode(new DefaultMindmapNode("Meocs" + i + ".google.com", "Namespace " + i + " of Google", "82c542"));  
                }  
            }  
            else if(label.equals("FRCs")) {  
                for(int i = 0; i < 18; i++) {  
                    node.addNode(new DefaultMindmapNode("1.1.1."  + i, "IP Number: 1.1.1." + i, "fce24f"));  
                }   
  
            }  
            else if(label.equals("Teams")) {  
                for(int i = 0; i < 18; i++) {  
                    String random = UUID.randomUUID().toString();  
                    node.addNode(new DefaultMindmapNode("Malware-"  + random, "Malicious Software: " + random, "3399ff", false));  
                }  
            }  
        }  
          
    }  
      
    public void onNodeDblselect(SelectEvent event) {  
        this.selectedNode = (MindmapNode) event.getObject();          
    }  
}
