/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.settings;

import eu.esponder.library.fileReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author tdim
 */
public class managesettings {

    private String szoPropertyFile;
    private String szjHost;
    private String szjport;
    private String szvHost;
    private String szvport;
    private String szwHost;
    private String szwport;
    private String s8;
    private String s9;
    private String s10;
    /**
     * The sz properties file name unix.
     */
    private String szPropertiesFileNameUnix = "//home//exodus//global.set";
    // comment for unix, uncomment for windows
    /**
     * The sz properties file name windows.
     */
    private String szPropertiesFileNameWindows = "C://Development//global.set";

    private boolean isUnix() {
        if (File.separatorChar == '/') {
            return true;
        } else {
            return false;
        }
    }

    public void LoadAllOptions() throws FileNotFoundException, IOException {
        fileReader pfr = new fileReader();
        if (isUnix()) {
            pfr.setFile(szPropertiesFileNameUnix);
        } else {
            pfr.setFile(szPropertiesFileNameWindows);
        }
        pfr.Loader();

        szoPropertyFile = pfr.getS1();
        szjHost = pfr.getS2();
        szjport = pfr.getS3();
        szvHost = pfr.getS4();
        szvport = pfr.getS5();
        szwHost = pfr.getS6();
        szwport = pfr.getS7();
        s8 = pfr.getS8();
        s9 = pfr.getS9();
        s10 = pfr.getS10();
    }

    /**
     * @return the szoPropertyFile
     */
    public String getSzoPropertyFile() {
        return szoPropertyFile;
    }

    /**
     * @param szoPropertyFile the szoPropertyFile to set
     */
    public void setSzoPropertyFile(String szoPropertyFile) {
        this.szoPropertyFile = szoPropertyFile;
    }

    /**
     * @return the szjHost
     */
    public String getSzjHost() {
        return szjHost;
    }

    /**
     * @param szjHost the szjHost to set
     */
    public void setSzjHost(String szjHost) {
        this.szjHost = szjHost;
    }

    /**
     * @return the szjport
     */
    public String getSzjport() {
        return szjport;
    }

    /**
     * @param szjport the szjport to set
     */
    public void setSzjport(String szjport) {
        this.szjport = szjport;
    }

    /**
     * @return the szvHost
     */
    public String getSzvHost() {
        return szvHost;
    }

    /**
     * @param szvHost the szvHost to set
     */
    public void setSzvHost(String szvHost) {
        this.szvHost = szvHost;
    }

    /**
     * @return the szvport
     */
    public String getSzvport() {
        return szvport;
    }

    /**
     * @param szvport the szvport to set
     */
    public void setSzvport(String szvport) {
        this.szvport = szvport;
    }

    /**
     * @return the szwHost
     */
    public String getSzwHost() {
        return szwHost;
    }

    /**
     * @param szwHost the szwHost to set
     */
    public void setSzwHost(String szwHost) {
        this.szwHost = szwHost;
    }

    /**
     * @return the szwport
     */
    public String getSzwport() {
        return szwport;
    }

    /**
     * @param szwport the szwport to set
     */
    public void setSzwport(String szwport) {
        this.szwport = szwport;
    }

    /**
     * @return the s8
     */
    public String getS8() {
        return s8;
    }

    /**
     * @param s8 the s8 to set
     */
    public void setS8(String s8) {
        this.s8 = s8;
    }

    /**
     * @return the s9
     */
    public String getS9() {
        return s9;
    }

    /**
     * @param s9 the s9 to set
     */
    public void setS9(String s9) {
        this.s9 = s9;
    }

    /**
     * @return the s10
     */
    public String getS10() {
        return s10;
    }

    /**
     * @param s10 the s10 to set
     */
    public void setS10(String s10) {
        this.s10 = s10;
    }
}
