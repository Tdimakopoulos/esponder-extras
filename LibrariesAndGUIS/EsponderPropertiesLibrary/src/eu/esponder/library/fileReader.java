/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.library;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author tdim
 */
public class fileReader {

    private String file;
    private String s1;
    private String s2;
    private String s3;
    private String s4;
    private String s5;
    private String s6;
    private String s7;
    private String s8;
    private String s9;
    private String s10;
    private int i=0;
    
    public void Loader() throws FileNotFoundException, IOException {
        BufferedReader in = new BufferedReader(new FileReader(file));

        while (in.ready()) {
            String s = in.readLine();
            if(i==0)
                s1=s;
            if(i==1)
                s2=s;
            if(i==2)
                s3=s;
            if(i==3)
                s4=s;
            if(i==4)
                s5=s;
            if(i==5)
                s6=s;
            if(i==6)
                s7=s;
            if(i==7)
                s8=s;
            if(i==8)
                s9=s;
            if(i==9)
                s10=s;
            i++;
        }
        in.close();
    }

    /**
     * @return the file
     */
    public String getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     * @return the s1
     */
    public String getS1() {
        return s1;
    }

    /**
     * @param s1 the s1 to set
     */
    public void setS1(String s1) {
        this.s1 = s1;
    }

    /**
     * @return the s2
     */
    public String getS2() {
        return s2;
    }

    /**
     * @param s2 the s2 to set
     */
    public void setS2(String s2) {
        this.s2 = s2;
    }

    /**
     * @return the s3
     */
    public String getS3() {
        return s3;
    }

    /**
     * @param s3 the s3 to set
     */
    public void setS3(String s3) {
        this.s3 = s3;
    }

    /**
     * @return the s4
     */
    public String getS4() {
        return s4;
    }

    /**
     * @param s4 the s4 to set
     */
    public void setS4(String s4) {
        this.s4 = s4;
    }

    /**
     * @return the s5
     */
    public String getS5() {
        return s5;
    }

    /**
     * @param s5 the s5 to set
     */
    public void setS5(String s5) {
        this.s5 = s5;
    }

    /**
     * @return the s6
     */
    public String getS6() {
        return s6;
    }

    /**
     * @param s6 the s6 to set
     */
    public void setS6(String s6) {
        this.s6 = s6;
    }

    /**
     * @return the s7
     */
    public String getS7() {
        return s7;
    }

    /**
     * @param s7 the s7 to set
     */
    public void setS7(String s7) {
        this.s7 = s7;
    }

    /**
     * @return the s8
     */
    public String getS8() {
        return s8;
    }

    /**
     * @param s8 the s8 to set
     */
    public void setS8(String s8) {
        this.s8 = s8;
    }

    /**
     * @return the s9
     */
    public String getS9() {
        return s9;
    }

    /**
     * @param s9 the s9 to set
     */
    public void setS9(String s9) {
        this.s9 = s9;
    }

    /**
     * @return the s10
     */
    public String getS10() {
        return s10;
    }

    /**
     * @param s10 the s10 to set
     */
    public void setS10(String s10) {
        this.s10 = s10;
    }

    /**
     * @return the i
     */
    public int getI() {
        return i;
    }

    /**
     * @param i the i to set
     */
    public void setI(int i) {
        this.i = i;
    }
}
