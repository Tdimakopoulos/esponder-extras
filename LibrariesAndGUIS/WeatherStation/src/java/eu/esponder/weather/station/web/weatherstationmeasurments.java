/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.weather.station.web;

//import eu.read.hw.SNMP4JHelper;
//import eu.read.hw.readValues;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.core.MultivaluedMap;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.MeterGaugeChartModel;

/**
 *
 * @author MEOC-WS1
 */
@ManagedBean
@SessionScoped
public class weatherstationmeasurments {

    private String valuewww;
    int igust;
//gauge meter    
private MeterGaugeChartModel TemperatureModel;  

//	
private MeterGaugeChartModel evaporation;

//gauge meter	
private MeterGaugeChartModel barometricpressure;

//gauge meter	
private MeterGaugeChartModel windspeed;

//	
private MeterGaugeChartModel winddirection;

//bar model	
private CartesianChartModel windgust;

//gauge meter	
private MeterGaugeChartModel relativehumidity;

//	chart
private CartesianChartModel solarradiation;

private String gpcurl;

//bar model	
private CartesianChartModel rain_gauge;

//	
private MeterGaugeChartModel disdrometer;

//	
private MeterGaugeChartModel transmissometer;

//	
private MeterGaugeChartModel ceilometer;
private String textvalue;


    /**
     * Creates a new instance of weatherstationmeasurments
     */
    public weatherstationmeasurments() {
    
     List<Number> intervals = new ArrayList<Number>(){{  
            add(10);  
            add(20);  
            add(30);  
            add(70);  
        }};  
  
        TemperatureModel = new MeterGaugeChartModel(30, intervals);
        
        
        List<Number> intervalsbarometricpressure = new ArrayList<Number>(){{  
            add(400);  
            add(600);  
            add(800);  
            add(1000);  
        }};
        
        
        barometricpressure= new MeterGaugeChartModel(760, intervalsbarometricpressure);
        
        List<Number> intervalswindspeed = new ArrayList<Number>(){{  
            add(5);  
            add(8);  
            add(10);  
            add(12);  
        }};
        
        windspeed= new MeterGaugeChartModel(5, intervalswindspeed);
        
         List<Number> intervalrelativehumidity = new ArrayList<Number>(){{  
            add(10);  
            add(30);  
            add(60);  
            add(100);  
        }};
         
        relativehumidity= new MeterGaugeChartModel(40, intervalrelativehumidity);
        igust=0;
        windgust = new CartesianChartModel();  
  
        ChartSeries gusts = new ChartSeries();  
        gusts.setLabel("Wind Gust");  
  
          
        gusts.set(" ", 10);  
  
       
  
        windgust.addSeries(gusts);  
        
        rain_gauge = new CartesianChartModel();
        
        ChartSeries rain = new ChartSeries();  
        rain.setLabel("Rain");  
  
          
        rain.set(" ", 90);  
  
       
  
        rain_gauge.addSeries(rain);  
        
        solarradiation= new CartesianChartModel();
         ChartSeries sr = new ChartSeries();  
        sr.setLabel("Solar Radiation Spectrum");  
  
          
        sr.set("250", 1);  
  sr.set("500", 2);  
  sr.set("1000", 3);  
  sr.set("1250", 5);  
  sr.set("1500",4);     
  sr.set("1750",4);
  sr.set("2000",5);
        solarradiation.addSeries(sr);
        
this.valuewww="N";
         populateRandomCars();
    }

    
    
//    public void populateRandomCars() {
//        System.out.println("Run");
//    }
     public void populateRandomCars() {
        try {
            Client client = Client.create();
            WebResource webResource = client
                    .resource("http://api.wunderground.com/api/42810a2bedd41804/geolookup/conditions/q/NL/Amsterdam.json");
            
            // Initialize Parameters
            MultivaluedMap queryParams = new MultivaluedMapImpl();
            queryParams.add("pkikey", "pki");
            String szReturn = webResource.queryParams(queryParams)
                    .get(String.class);
            System.out.println(szReturn);
            
            JSONObject obj = new JSONObject(szReturn);
            
            JSONObject jObject = obj.getJSONObject("current_observation");
            
            String weather = jObject.getString("weather");
            String temperature_string = jObject.getString("temperature_string");
            String temperature_c = jObject.getString("temp_c");
            
            String wind_s = jObject.getString("wind_string");
            
            String wind_dir = jObject.getString("wind_dir");
            valuewww="Wind Direction : "+wind_dir;
            
            
            String wind_kph = jObject.getString("wind_kph");
            String wind_gust = jObject.getString("wind_gust_kph");
            
            String pre_mb = jObject.getString("pressure_mb");
            String pre_in = jObject.getString("pressure_in");
            String relative_humidity = jObject.getString("relative_humidity");
            
            String icon_url = jObject.getString("icon_url");
            String precip = jObject.getString("precip_today_metric");
            setGpcurl(icon_url);
            textvalue=weather+" "+temperature_string+" "+wind_s+pre_mb+pre_in+precip;
            
            TemperatureModel.setValue((Number)Double.valueOf(temperature_c));
            relative_humidity=relative_humidity.replace("%","");
         relativehumidity.setValue((Number)(Double.valueOf(relative_humidity)));
         windspeed.setValue((Number)(Double.valueOf(wind_kph)));
         //valuewww=pp.getsDir();
          windgust.clear();
         ChartSeries gusts = new ChartSeries();  
        gusts.setLabel("Wind Gust");  
  
          
        gusts.set(" ", Double.valueOf(wind_gust));  
  
       
  
        windgust.addSeries(gusts);  
       
        
        rain_gauge.clear();
        
        ChartSeries rain = new ChartSeries();  
        rain.setLabel("Rain");  
  
          
        rain.set(" ", Double.valueOf(precip));  
  
       
  
        rain_gauge.addSeries(rain);
        
        
        } catch (JSONException ex) {
            Logger.getLogger(weatherstationmeasurments.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
    
     
     
     
     
    /**
     * @return the TemperatureModel
     */
    public MeterGaugeChartModel getTemperatureModel() {
        return TemperatureModel;
    }

    /**
     * @param TemperatureModel the TemperatureModel to set
     */
    public void setTemperatureModel(MeterGaugeChartModel TemperatureModel) {
        this.TemperatureModel = TemperatureModel;
    }

    /**
     * @return the barometricpressure
     */
    public MeterGaugeChartModel getBarometricpressure() {
        return barometricpressure;
    }

    /**
     * @param barometricpressure the barometricpressure to set
     */
    public void setBarometricpressure(MeterGaugeChartModel barometricpressure) {
        this.barometricpressure = barometricpressure;
    }

    /**
     * @return the windspeed
     */
    public MeterGaugeChartModel getWindspeed() {
        return windspeed;
    }

    /**
     * @param windspeed the windspeed to set
     */
    public void setWindspeed(MeterGaugeChartModel windspeed) {
        this.windspeed = windspeed;
    }

    /**
     * @return the relativehumidity
     */
    public MeterGaugeChartModel getRelativehumidity() {
        return relativehumidity;
    }

    /**
     * @param relativehumidity the relativehumidity to set
     */
    public void setRelativehumidity(MeterGaugeChartModel relativehumidity) {
        this.relativehumidity = relativehumidity;
    }

    /**
     * @return the windgust
     */
    public CartesianChartModel getWindgust() {
        return windgust;
    }

    /**
     * @param windgust the windgust to set
     */
    public void setWindgust(CartesianChartModel windgust) {
        this.windgust = windgust;
    }

    /**
     * @return the rain_gauge
     */
    public CartesianChartModel getRain_gauge() {
        return rain_gauge;
    }

    /**
     * @param rain_gauge the rain_gauge to set
     */
    public void setRain_gauge(CartesianChartModel rain_gauge) {
        this.rain_gauge = rain_gauge;
    }

    /**
     * @return the solarradiation
     */
    public CartesianChartModel getSolarradiation() {
        return solarradiation;
    }

    /**
     * @param solarradiation the solarradiation to set
     */
    public void setSolarradiation(CartesianChartModel solarradiation) {
        this.solarradiation = solarradiation;
    }

    /**
     * @return the valuewww
     */
    public String getValuewww() {
        return valuewww;
    }

    /**
     * @param valuewww the valuewww to set
     */
    public void setValuewww(String valuewww) {
        this.valuewww = valuewww;
    }

    /**
     * @return the textvalue
     */
    public String getTextvalue() {
        return textvalue;
    }

    /**
     * @param textvalue the textvalue to set
     */
    public void setTextvalue(String textvalue) {
        this.textvalue = textvalue;
    }

    /**
     * @return the gpcurl
     */
    public String getGpcurl() {
        return gpcurl;
    }

    /**
     * @param gpcurl the gpcurl to set
     */
    public void setGpcurl(String gpcurl) {
        this.gpcurl = gpcurl;
    }

   
}
