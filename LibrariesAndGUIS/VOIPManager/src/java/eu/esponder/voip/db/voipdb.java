/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.voip.db;

import eu.esponder.osgi.settings.OsgiSettings;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MEOC-WS1
 */
public class voipdb {

    ArrayList<voipset> pdb = new ArrayList<voipset>();
    
    ArrayList<voipset> pdbarc = new ArrayList<voipset>();
    
    int ipos;
    int iposa;
    
   public void SaveOnFile() throws FileNotFoundException, IOException {
        OsgiSettings pset=new OsgiSettings();
        
        FileOutputStream fos = new FileOutputStream(pset.GetVoipDataFileLocation());
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(pdb);
        oos.close();
    }

    public void LoadFronFile() throws FileNotFoundException, IOException, ClassNotFoundException {
        OsgiSettings pset=new OsgiSettings();
        pdb.clear();
        FileInputStream fis = new FileInputStream(pset.GetVoipDataFileLocation());
        ObjectInputStream ois = new ObjectInputStream(fis);
        pdb = (ArrayList<voipset>) (List<voipset>) ois.readObject();
        ois.close();
        System.out.println("ID"+pdb.get(0).getId());
        ipos=pdb.size();
        
        
        pdbarc.clear();
        FileInputStream fisa = new FileInputStream(pset.GetVoipDataFileLocation()+"arc");
        ObjectInputStream oisa = new ObjectInputStream(fisa);
        pdbarc = (ArrayList<voipset>) (List<voipset>) oisa.readObject();
        oisa.close();
        iposa=pdbarc.size();
    }

    public voipset getItema(int indexpos) {
        return pdbarc.get(indexpos);
    }

    public int getposa() {
        return iposa;
    }

    
    public voipset getItem(int indexpos) {
        return pdb.get(indexpos);
    }

    public int getpos() {
        return ipos;
    }

    public void Add(voipset pitem) {
        ipos = ipos + 1;
        pdb.add(pitem);
    }

    public boolean Edit(voipset pitem) {
        for (int i = 0; i < pdb.size(); i++) {
            if (pdb.get(i).getId().toString().equalsIgnoreCase(pitem.getId().toString())) {
                pdb.set(i, pitem);
                return true;
            }
        }
        return false;
    }

    public boolean Delete(voipset pitem) {
        for (int i = 0; i < pdb.size(); i++) {
            if (pdb.get(i).getId().toString().equalsIgnoreCase(pitem.getId().toString())) {
                pdb.remove(i);
                return true;
            }
        }
        return false;
    }
}
