/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.net.voip;

import eu.esponder.osgi.settings.OsgiSettings;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author MEOC-WS1
 */
public class voipcalls {
    
    public void DeleteCall(int ii,String sz1) throws Exception
    {
     if (sz1.contains("MEOC"))
         if (sz1.contains("FRC"))
         {MeocToFrcCall(ii);}
     if (sz1.contains("FRC"))
         if (sz1.contains("MEOC"))
         {MeocToFrcCall(ii);}
     
     if (sz1.contains("MEOC"))
         if (sz1.contains("EOC"))
         {EocToMeocCall(ii);}
     if (sz1.contains("EOC"))
         if (sz1.contains("MEOC"))
         {EocToMeocCall(ii);}
    }
    public void EocToMeocCall(int ii) throws Exception {

		String url = "http://192.168.5.60:8080/esponder-comm-manager/rest/"+ii;
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("DELETE");
		con.setRequestProperty("Content-Type", "application/json");

		String urlParameters = "{\"eoc\":\"eoc1\",\"ian\":[{\"meoc2eoc\":\"meoc2eoc1\",\"frt\":null,\"meoc\":null}]}";
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println(response.toString());
	}
    
    public void MeocToFrcCall(int ii) throws Exception {

        //read from file crisisID
                    OsgiSettings pfile = new OsgiSettings();
                    
                    BufferedReader in2 = new BufferedReader(new FileReader(pfile.getcFileID()));
                    String s = "1";
                    while (in2.ready()) {
                        s = in2.readLine();
                    }
                    in2.close();
		String url = "http://192.168.5.60:8080/esponder-comm-manager/rest/conferences/"+s;
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("DELETE");
		con.setRequestProperty("Content-Type", "application/json");

                BufferedReader in3 = new BufferedReader(new FileReader(pfile.getcFileID2()));
                    String ss = "1";
                    while (in3.ready()) {
                        ss = in3.readLine();
                    }
                    in3.close();
                    
		String urlParameters = ss;//"{\"eoc\":null,\"ian\":[{\"meoc2eoc\":null,\"frt\":[{\"frc\": \"frc1\", \"fru\": null}],\"meoc\":[\"meoc1\", \"meoc2\"]}]}"; 
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println(response.toString());
	}
}
