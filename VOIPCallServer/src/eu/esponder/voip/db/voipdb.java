/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.voip.db;

import eu.esponder.osgi.settings.OsgiSettings;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MEOC-WS1
 */
public class voipdb {

    ArrayList<voipset> pdb = new ArrayList<voipset>();
    Long ipos = new Long(0);

    public void SaveOnFile() throws FileNotFoundException, IOException {
        OsgiSettings pset=new OsgiSettings();
        
        FileOutputStream fos = new FileOutputStream(pset.GetVoipDataFileLocation());
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(pdb);
        oos.close();
        
//        FileOutputStream fos1 = new FileOutputStream(pset.GetVoipDataFileLocation()+"arc");
//        ObjectOutputStream oos1 = new ObjectOutputStream(fos1);
//        oos1.writeObject(pdb);
//        oos1.close();
    }

    public void SaveOnFile2() throws FileNotFoundException, IOException {
        OsgiSettings pset=new OsgiSettings();
        
//        FileOutputStream fos = new FileOutputStream(pset.GetVoipDataFileLocation());
//        ObjectOutputStream oos = new ObjectOutputStream(fos);
//        oos.writeObject(pdb);
//        oos.close();
        
        FileOutputStream fos1 = new FileOutputStream(pset.GetVoipDataFileLocation()+"arc");
        ObjectOutputStream oos1 = new ObjectOutputStream(fos1);
        oos1.writeObject(pdb);
        oos1.close();
    }
    
    public void LoadFronFile() throws FileNotFoundException, IOException, ClassNotFoundException {
        OsgiSettings pset=new OsgiSettings();
        pdb.clear();
        FileInputStream fis = new FileInputStream(pset.GetVoipDataFileLocation());
        ObjectInputStream ois = new ObjectInputStream(fis);
        pdb = (ArrayList<voipset>) (List<voipset>) ois.readObject();
        ois.close();
    }

     public void LoadFronFile2() throws FileNotFoundException, IOException, ClassNotFoundException {
        OsgiSettings pset=new OsgiSettings();
        pdb.clear();
        FileInputStream fis = new FileInputStream(pset.GetVoipDataFileLocation()+"arc");
        ObjectInputStream ois = new ObjectInputStream(fis);
        pdb = (ArrayList<voipset>) (List<voipset>) ois.readObject();
        ois.close();
    }
    public voipset getItem(int indexpos) {
        return pdb.get(indexpos);
    }

    public Long getpos() {
        return ipos;
    }

    public void Add(voipset pitem) {
        ipos = ipos + 1;
        pdb.add(pitem);
    }

    public boolean Edit(voipset pitem) {
        for (int i = 0; i < pdb.size(); i++) {
            if (pdb.get(i).getId().toString().equalsIgnoreCase(pitem.getId().toString())) {
                pdb.set(i, pitem);
                return true;
            }
        }
        return false;
    }

    public boolean Delete(voipset pitem) {
        for (int i = 0; i < pdb.size(); i++) {
            if (pdb.get(i).getId().toString().equalsIgnoreCase(pitem.getId().toString())) {
                pdb.remove(i);
                return true;
            }
        }
        return false;
    }
}
