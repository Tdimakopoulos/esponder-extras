/*
 * 
 */
package eu.esponder.osgi.service.event;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.prosyst.mprm.backend.event.EventListenerException;
import com.prosyst.mprm.backend.event.EventService;
import com.prosyst.mprm.common.ManagementException;
import com.prosyst.mprm.data.DictionaryInfo;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;
//import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.osgi.service.connection.ConnectionHandler;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderEventPublisher.
 *
 * @param <T> the generic type
 */
public class ESponderEventPublisher<T extends ESponderEvent<? extends ESponderEntityDTO>> {

    private ConnectionHandler connectionHandler = null;//new ConnectionHandler("local");
    /**
     * The event topic.
     */
    private String eventTopic;
    /**
     * The event service.
     */
    private EventService eventService;
    /**
     * The event topic class.
     */
    private Class<T> eventTopicClass;

    /**
     * Close connection.
     */
    public void CloseConnection() {
        connectionHandler.CloseConnection();
    }

    /**
     * Instantiates a new e sponder event publisher.
     *
     * @param eventClass the event class
     */
    public ESponderEventPublisher(Class<T> eventClass) {

        connectionHandler = new ConnectionHandler("local");
        this.eventTopicClass = eventClass;
        this.setEventTopic(eventClass.getCanonicalName().replace('.', '/'));
        System.out.println("OSGI Publisher Topic --> " + eventClass.getCanonicalName().replace('.', '/'));

        try {
            if (connectionHandler.getRac() == null) {
                connectionHandler.connect();
            }
            eventService = (EventService) connectionHandler.getRac().getService(EventService.class.getName());
        } catch (ManagementException me) {
//			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+me.getMessage());
        }
    }

    /**
     * Instantiates a new e sponder event publisher.
     *
     * @param eventClass the event class
     */
    public ESponderEventPublisher(Class<T> eventClass, String EventServer) {

        connectionHandler = new ConnectionHandler(EventServer);

        this.eventTopicClass = eventClass;
        this.setEventTopic(eventClass.getCanonicalName().replace('.', '/'));
        System.out.println("OSGI Publisher Topic --> " + eventClass.getCanonicalName().replace('.', '/'));

        try {
            if (connectionHandler.getRac() == null) {
                connectionHandler.connect();
            }
            eventService = (EventService) connectionHandler.getRac().getService(EventService.class.getName());
        } catch (ManagementException me) {
//			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+me.getMessage());
        }
    }

    /**
     * Gets the event topic.
     *
     * @return the event topic
     */
    public String getEventTopic() {
        return eventTopic;
    }

    /**
     * Sets the event topic.
     *
     * @param eventTopic the new event topic
     */
    public void setEventTopic(String eventTopic) {
        this.eventTopic = eventTopic;
    }

    /**
     * Publish event.
     *
     * @param event the event
     * @throws EventListenerException the event listener exception
     */
    public void publishEvent(ESponderEvent<? extends ESponderEntityDTO> event) throws EventListenerException {
        DictionaryInfo data = new DictionaryInfo();
        ObjectMapper mapper;
        mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            data.put(ConnectionHandler.EVENT_PROPERTY_NAME, mapper.writeValueAsString(event));
            //System.out.print("Data Send OK!");
        } catch (JsonGenerationException e) {
//	    	  new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
        } catch (JsonMappingException e) {

            System.out.println(e.getMessage());
            //	new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
        } catch (IOException e) {
//			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
        }

        eventService.event(eventTopic, data);
        //System.out.println("Data have been sent"+data.toString());
    }

    /**
     * Gets the event topic class.
     *
     * @return the event topic class
     */
    public Class<T> getEventTopicClass() {
        return eventTopicClass;
    }

    /**
     * Sets the event topic class.
     *
     * @param eventTopicClass the new event topic class
     */
    public void setEventTopicClass(Class<T> eventTopicClass) {
        this.eventTopicClass = eventTopicClass;
    }
}
