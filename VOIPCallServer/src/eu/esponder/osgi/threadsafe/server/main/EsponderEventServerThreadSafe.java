package eu.esponder.osgi.threadsafe.server.main;

import java.io.IOException;
import java.util.Date;

import com.prosyst.mprm.common.ManagementException;


import eu.esponder.event.voip.ESponderVOIPCallOrConferenceResponseEvent;
import eu.esponder.event.voip.ESponderVOIPCallRequestEvent;
import eu.esponder.event.voip.ESponderVOIPConferenceRequestEvent;
//import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.osgi.service.event.ESponderEventListener;
import eu.esponder.osgi.settings.OsgiSettings;

public class EsponderEventServerThreadSafe  extends Thread{


	

        ESponderEventListener<ESponderVOIPCallRequestEvent> v1 = null;
        ESponderEventListener<ESponderVOIPConferenceRequestEvent> v2 = null;
                ESponderEventListener<ESponderVOIPCallOrConferenceResponseEvent> v3 = null; 
	public boolean Running;
    public boolean StopRequest;
    String szgfilename;
    
    public void CreateListenersConnections() throws ManagementException {

    	System.out.println("----->  Event Server (Version 2.0) Listeners Starting");

        		if (v1 == null)
			v1 = new ESponderEventListener<ESponderVOIPCallRequestEvent>(
					ESponderVOIPCallRequestEvent.class);

                        
                        		if (v2 == null)
			v2 = new ESponderEventListener<ESponderVOIPConferenceRequestEvent>(
					ESponderVOIPConferenceRequestEvent.class);

                                        
                                        		if (v3 == null)
			v3 = new ESponderEventListener<ESponderVOIPCallOrConferenceResponseEvent>(
					ESponderVOIPCallOrConferenceResponseEvent.class);

                                                        
		
		
		System.out.println("----->  Event Server (Version 2.0) Listeners Started");
	}
    
    public void stopserver() {
    	System.out.println("----->  Event Server (Version 2.0) Server Stopping");
	if (v1 == null)
			v1.CloseConnection();	
        
        if (v2 == null)
			v2.CloseConnection();
        
        if (v3 == null)
			v3.CloseConnection();
        
		
v1=null;
v2=null;
v3=null;
		System.out.println("----->  Event Server (Version 2.0) Server Stopped");
	}

	
	public void startserver() {
		System.out.println("----->  Event Server (Version 2.0) Server Starting");
		
		
		try {
			CreateListenersConnections();
			v1.subscribe();
                        v2.subscribe();
                        v3.subscribe();
                        
			
		} catch (ManagementException e) {

//			new EsponderCheckedException(this.getClass(),
//					"ESponder Exception : " + e.getMessage());
		}

		System.out.println("----->  Event Server (Version 2.0) Server Started");
	}
    
    public EsponderEventServerThreadSafe()
    {
        Running=true;
        StopRequest=false;
    }
    
    public boolean IsServerRunning()
    {
        return Running;
    }
    
    public void StopServer()
    {
        StopRequest=true;
    }
    
    public String GetServerStatus()
    {
        return (new Date())+" Running : "+Running+" Stop Request : "+StopRequest;
    }
    
	/**
	 * Close server.
	 * 
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String CloseServer() throws IOException {

		ServerStatus pStatus = new ServerStatus();
		String szFilename = null;

		if (szgfilename == null) {
			OsgiSettings pSettings = new OsgiSettings();
			pSettings.LoadSettings();
			szFilename = pSettings.getSzPropertiesFileName();
			szgfilename = szFilename;
		} else {
			szFilename = szgfilename;
		}
		String sz = pStatus.readFile(pStatus.AppandFileOnPath(pStatus
				.RemoveFilename(szFilename)));

		return sz;
	}

    public void run() {

    	System.out.println("--> Event Server Started (Server Version 2.0)");
    	startserver();
        while (true)
        {
            try {

                Thread.sleep(1000);

                if(CloseServer().equalsIgnoreCase("0"))
                {
                	StopServer();
                }
                if(StopRequest)
                {
                	if(Running)
                	{
                    Running=false;
                    stopserver();
                    System.out.println("--> Event Server Stopped (Server Version 2.0)");
                	}else
                	{
                		if(CloseServer().equalsIgnoreCase("1"))
                		{
                			
                	    	startserver();
                	    	System.out.println("--> Event Server Started (Server Version 2.0)");
                	    	Running=true;
                	    	StopRequest=false;
                		}
                	}
                }
                
            } catch (InterruptedException ex) {
                System.out.println("Main Server Loop Catch in sleep (V 2.0): "+ex.getMessage());
            } catch (IOException e) {
            	System.out.println("Main Server problem on status file (V 2.0): "+e.getMessage());
			}
        }
        
        
    }

	
}
