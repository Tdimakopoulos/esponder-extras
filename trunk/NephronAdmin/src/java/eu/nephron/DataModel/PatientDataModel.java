/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.DataModel;

import eu.nephron.model.Patient;
import java.util.List;

/**
 *
 * @author tdim
 */
public class PatientDataModel extends PrimeDataModel<Patient> {

    public PatientDataModel() {
    }

    public PatientDataModel(Object data) {
        super(data);
    }

    @Override
    public Patient getRowData(String rowKey) {

        List<Patient> patients = (List<Patient>) getWrappedData();
        for (Patient patient : patients) {
            if (patient.getId().toString().equals(rowKey)) {
                return patient;
            }
        }

        return null;
    }

    @Override
    public String getRowKey(Patient patient) {
        return patient.getId().toString();
    }
}
