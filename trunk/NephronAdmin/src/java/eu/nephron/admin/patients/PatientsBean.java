/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.admin.patients;

import eu.nephron.model.Doctors;
import eu.nephron.webservices.KeyManagerService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;

import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.hl7.cache.ws.Cacheconnection_Service;
import org.hl7.cache.ws.Cachedoctors_Service;
import org.hl7.cache.ws.Cachepatients_Service;

import org.hl7.cache.ws.Hl7Patients;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class PatientsBean {

    private Cacheconnection_Service service_2 = new Cacheconnection_Service();
    private Cachepatients_Service service_1 = new Cachepatients_Service();
    private Cachedoctors_Service service = new Cachedoctors_Service();
    private String code;
    private String name;
    private String administrativeArea;
    private String city;
    private String street;
    private String streetNo;
    private String floor;
    private String postCode;
    private String title;
    private String firstName;
    private String middleName;
    private String lastName;
    private String fatherName;
    private int maritalStatus;
    private int educationalLevel;
    private String phone;
    private String companyPhone;
    private String mobile;
    private String companyMobile;
    private String email;
    private String companyEmail;
    private List<Doctors> pLists = new ArrayList<Doctors>();
    private String doctoridforedit;
    private Doctors selectedDoctor;
    private String username;
    private String password;
    private Long pdid;
    private Long icreid;
    private Map<String, String> doctors = new HashMap<String, String>();
    private String doctorl = "0";
    private String mobileIMEI;

    public void initializecombos() {
        doctors.clear();
        java.util.List<org.hl7.cache.ws.Hl7Doctors> pd = findAlldoctors();
        for (int i = 0; i < pd.size(); i++) {
            doctors.put(pd.get(i).getFirstName() + " " + pd.get(i).getLastName(), String.valueOf(pd.get(i).getId()));
        }
    }

    public String deletedoctor() {

        String dd = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pdoctorID").toString();
        System.out.println("Calling listener Delete : " + dd);
        long ifind = Long.valueOf(dd);//Long.parseLong(getDoctoridforedit());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("pdoctorID", String.valueOf(ifind));
        System.out.println("***** patient ID For delete :  " + ifind);

        for (int i = 0; i < pLists.size(); i++) {
            if (pLists.get(i).getId().toString().equalsIgnoreCase(String.valueOf(ifind))) {
                selectedDoctor = pLists.get(i);
            }
        }
        System.out.println("removing " + ifind);
        removePatient(findPatient(ifind));
        refresh();
        return "PatientPage?faces-redirect=true";
    }

    public String editcredoctor() {

        return "Credentials?faces-redirect=true";
    }

    public String editdoctor() {
        String dd = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pdoctorID").toString();
        System.out.println("Calling listener edit patient : " + dd);
        long ifind = Long.valueOf(dd);//Long.parseLong(getDoctoridforedit());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("pdoctorID", String.valueOf(ifind));
        System.out.println("***** patient ID For Edit :  " + ifind);

        for (int i = 0; i < pLists.size(); i++) {
            if (pLists.get(i).getId().toString().equalsIgnoreCase(String.valueOf(ifind))) {
                selectedDoctor = pLists.get(i);
            }
        }
        doctorl = String.valueOf(selectedDoctor.getPdid());
        code = selectedDoctor.getCode();
        name = selectedDoctor.getName();
        administrativeArea = selectedDoctor.getAdministrativeArea();
        city = selectedDoctor.getCity();
        street = selectedDoctor.getStreet();
        streetNo = selectedDoctor.getStreetNo();
        floor = selectedDoctor.getFloor();
        postCode = selectedDoctor.getPostCode();
        title = selectedDoctor.getTitle();
        firstName = selectedDoctor.getFirstName();
        middleName = selectedDoctor.getMiddleName();
        lastName = selectedDoctor.getLastName();
        fatherName = selectedDoctor.getFatherName();
        maritalStatus = selectedDoctor.getMaritalStatus();
        educationalLevel = selectedDoctor.getEducationalLevel();
        phone = selectedDoctor.getPhone();
        companyPhone = selectedDoctor.getCompanyPhone();
        mobile = selectedDoctor.getMobile();
        companyMobile = selectedDoctor.getCompanyMobile();
        email = selectedDoctor.getEmail();
        companyEmail = selectedDoctor.getCompanyEmail();
        mobileIMEI = selectedDoctor.getMobileIMEI();
        return "Edit_Patient?faces-redirect=true";
    }

    public String adddoctor() {
        code = "";
        name = "";
        administrativeArea = "";
        city = "";
        street = "";
        streetNo = "";
        floor = "";
        postCode = "";
        title = "";
        firstName = "";
        middleName = "";
        lastName = "";
        fatherName = "";
        maritalStatus = 0;
        educationalLevel = 0;
        phone = "";
        companyPhone = "";
        mobile = "";
        companyMobile = "";
        email = "";
        companyEmail = "";
        doctorl = "0";
        return "Add_Patient?faces-redirect=true";
    }

    public String savecreadddoctor() {

        return "PatientPage?faces-redirect=true";
    }

    public String saveadddoctor() {
        org.hl7.cache.ws.Hl7Patients item = new org.hl7.cache.ws.Hl7Patients();
        item.setIdoctor(Long.valueOf(doctorl));
        item.setFatherName(fatherName);
        item.setFirstName(firstName);
        item.setLastName(lastName);
        item.setAdministrativeArea(administrativeArea);
        item.setPostCode(postCode);
        item.setTitle(title);
        item.setPhone(phone);
        item.setMobile(mobile);
        item.setEmail(email);
        item.setFloor(floor);
        item.setCompanyEmail(companyEmail);
        item.setCompanyMobile(companyMobile);
        item.setCompanyPhone(companyPhone);
        item.setStreet(street);
        item.setStreetNo(streetNo);
        item.setCode(code);
        item.setCity(city);
        createPatient(item);
        generateUserKeyPair(firstName + " " + lastName, "Patient", "Patient", Long.valueOf(-1));
        java.util.List<org.hl7.cache.ws.Hl7Patients> pp = findAllPatient();
        for (int i = 0; i < pp.size(); i++) {
            if (pp.get(i).getFirstName().equalsIgnoreCase(firstName)) {
                if (pp.get(i).getLastName().equalsIgnoreCase(lastName)) {
                    if (pp.get(i).getFatherName().equalsIgnoreCase(fatherName)) {
                        AddConnection(pp.get(i).getId(), mobileIMEI);
                    }
                }
            }
        }
        refresh();
        return "PatientPage?faces-redirect=true";
    }

    private String generateUserKeyPair(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.Long arg3) {
        KeyManagerService service = new KeyManagerService();
        eu.nephron.webservices.KeyManager port = service.getKeyManagerPort();
        return port.generateUserKeyPair(arg0, arg1, arg2, arg3);
    }

    public String canceldoctor() {

        code = "";
        name = "";
        administrativeArea = "";
        city = "";
        street = "";
        streetNo = "";
        floor = "";
        postCode = "";
        title = "";
        firstName = "";
        middleName = "";
        lastName = "";
        fatherName = "";
        maritalStatus = 0;
        educationalLevel = 0;
        phone = "";
        companyPhone = "";
        mobile = "";
        companyMobile = "";
        email = "";
        companyEmail = "";
        return "PatientPage?faces-redirect=true";
    }

    public String saveeditdoctor() {

        long ifind = selectedDoctor.getId();
        System.out.println("Save Doctor : " + ifind);
        org.hl7.cache.ws.Hl7Patients item = new org.hl7.cache.ws.Hl7Patients();
        item.setId(ifind);
        item.setFatherName(fatherName);
        item.setFirstName(firstName);
        item.setLastName(lastName);
        item.setAdministrativeArea(administrativeArea);
        item.setIdoctor(Long.valueOf(doctorl));
        item.setPostCode(postCode);
        item.setTitle(title);
        item.setPhone(phone);
        item.setMobile(mobile);
        item.setEmail(email);
        item.setFloor(floor);
        item.setCompanyEmail(companyEmail);
        item.setCompanyMobile(companyMobile);
        item.setCompanyPhone(companyPhone);
        item.setStreet(street);
        item.setStreetNo(streetNo);
        item.setCode(code);
        item.setCity(city);
        editPatient(item);
        EditMobilePatient(ifind, mobileIMEI);
        refresh();
        return "PatientPage?faces-redirect=true";
    }

//    public String editpdoctor() {
//        long ifind = Long.parseLong(getDoctoridforedit());
//        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("pdoctorID", getDoctoridforedit());
//        System.out.println("***** Doctor ID For pEdit :  " + getDoctoridforedit());
//        return "Edit_Patient?faces-redirect=true";
//    }

    public void refresh() {
        pLists.clear();
        java.util.List<org.hl7.cache.ws.Hl7Patients> pList = findAllPatient();
        for (int i = 0; i < pList.size(); i++) {
            Doctors item = new Doctors();
            item.setPdid(pList.get(i).getIdoctor());
            //doctorl=String.valueOf(pList.get(i).getIdoctor());
            item.setId(pList.get(i).getId());
            item.setFatherName(pList.get(i).getFatherName());
            item.setFirstName(pList.get(i).getFirstName());
            item.setLastName(pList.get(i).getLastName());
            item.setAdministrativeArea(pList.get(i).getAdministrativeArea());
            item.setEducationalLevel(pList.get(i).getEducationalLevel());
            item.setMaritalStatus(pList.get(i).getMaritalStatus());
            item.setPostCode(pList.get(i).getPostCode());
            item.setTitle(pList.get(i).getTitle());
            item.setPhone(pList.get(i).getPhone());
            item.setMobile(pList.get(i).getMobile());
            item.setEmail(pList.get(i).getEmail());
            item.setFloor(pList.get(i).getFloor());
            item.setCompanyEmail(pList.get(i).getCompanyEmail());
            item.setCompanyMobile(pList.get(i).getCompanyMobile());
            item.setCompanyPhone(pList.get(i).getCompanyPhone());
            item.setStreet(pList.get(i).getStreet());
            item.setStreetNo(pList.get(i).getStreetNo());
            item.setCode(pList.get(i).getCode());
            item.setCity(pList.get(i).getCity());
            item.setMobileIMEI(FindMobileIMEI(pList.get(i).getId()));
            pLists.add(item);
        }
        initializecombos();
    }

    /**
     * Creates a new instance of DoctorsBean
     */
    public PatientsBean() {

        java.util.List<org.hl7.cache.ws.Hl7Patients> pList = findAllPatient();
        for (int i = 0; i < pList.size(); i++) {
            Doctors item = new Doctors();
            item.setPdid(pList.get(i).getIdoctor());
            item.setId(pList.get(i).getId());
            item.setFatherName(pList.get(i).getFatherName());
            item.setFirstName(pList.get(i).getFirstName());
            item.setLastName(pList.get(i).getLastName());
            item.setAdministrativeArea(pList.get(i).getAdministrativeArea());
            item.setEducationalLevel(pList.get(i).getEducationalLevel());
            item.setMaritalStatus(pList.get(i).getMaritalStatus());
            item.setPostCode(pList.get(i).getPostCode());
            item.setTitle(pList.get(i).getTitle());
            item.setPhone(pList.get(i).getPhone());
            item.setMobile(pList.get(i).getMobile());
            item.setEmail(pList.get(i).getEmail());
            item.setFloor(pList.get(i).getFloor());
            item.setCompanyEmail(pList.get(i).getCompanyEmail());
            item.setCompanyMobile(pList.get(i).getCompanyMobile());
            item.setCompanyPhone(pList.get(i).getCompanyPhone());
            item.setStreet(pList.get(i).getStreet());
            item.setStreetNo(pList.get(i).getStreetNo());
            item.setCode(pList.get(i).getCode());
            item.setCity(pList.get(i).getCity());
            item.setMobileIMEI(FindMobileIMEI(pList.get(i).getId()));
            pLists.add(item);
        }
        initializecombos();
    }

////////////////////////////////////////////////// Get Set////////////////////////////////////////////////////////////////////
    /**
     * @return the doctoridforedit
     */
    public String getDoctoridforedit() {
        return doctoridforedit;
    }

    /**
     * @param doctoridforedit the doctoridforedit to set
     */
    public void setDoctoridforedit(String doctoridforedit) {
        this.doctoridforedit = doctoridforedit;
    }

    /**
     * @return the selectedDoctor
     */
    public Doctors getSelectedDoctor() {
        return selectedDoctor;
    }

    /**
     * @param selectedDoctor the selectedDoctor to set
     */
    public void setSelectedDoctor(Doctors selectedDoctor) {
        this.selectedDoctor = selectedDoctor;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the administrativeArea
     */
    public String getAdministrativeArea() {
        return administrativeArea;
    }

    /**
     * @param administrativeArea the administrativeArea to set
     */
    public void setAdministrativeArea(String administrativeArea) {
        this.administrativeArea = administrativeArea;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the streetNo
     */
    public String getStreetNo() {
        return streetNo;
    }

    /**
     * @param streetNo the streetNo to set
     */
    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    /**
     * @return the floor
     */
    public String getFloor() {
        return floor;
    }

    /**
     * @param floor the floor to set
     */
    public void setFloor(String floor) {
        this.floor = floor;
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * @param postCode the postCode to set
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the fatherName
     */
    public String getFatherName() {
        return fatherName;
    }

    /**
     * @param fatherName the fatherName to set
     */
    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    /**
     * @return the maritalStatus
     */
    public int getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * @param maritalStatus the maritalStatus to set
     */
    public void setMaritalStatus(int maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * @return the educationalLevel
     */
    public int getEducationalLevel() {
        return educationalLevel;
    }

    /**
     * @param educationalLevel the educationalLevel to set
     */
    public void setEducationalLevel(int educationalLevel) {
        this.educationalLevel = educationalLevel;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the companyPhone
     */
    public String getCompanyPhone() {
        return companyPhone;
    }

    /**
     * @param companyPhone the companyPhone to set
     */
    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the companyMobile
     */
    public String getCompanyMobile() {
        return companyMobile;
    }

    /**
     * @param companyMobile the companyMobile to set
     */
    public void setCompanyMobile(String companyMobile) {
        this.companyMobile = companyMobile;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the companyEmail
     */
    public String getCompanyEmail() {
        return companyEmail;
    }

    /**
     * @param companyEmail the companyEmail to set
     */
    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the pdid
     */
    public Long getPdid() {
        return pdid;
    }

    /**
     * @param pdid the pdid to set
     */
    public void setPdid(Long pdid) {
        this.pdid = pdid;
    }

    /**
     * @return the icreid
     */
    public Long getIcreid() {
        return icreid;
    }

    /**
     * @param icreid the icreid to set
     */
    public void setIcreid(Long icreid) {
        this.icreid = icreid;
    }

    /**
     * @return the pLists
     */
    public List<Doctors> getpLists() {
        return pLists;
    }

    /**
     * @param pLists the pLists to set
     */
    public void setpLists(List<Doctors> pLists) {
        this.pLists = pLists;
    }

    private void createPatient(org.hl7.cache.ws.Hl7Patients hl7Patients) {
        org.hl7.cache.ws.Cachepatients port = service_1.getCachepatientsPort();
        port.createPatient(hl7Patients);
    }

    private void editPatient(org.hl7.cache.ws.Hl7Patients hl7Patients) {
        org.hl7.cache.ws.Cachepatients port = service_1.getCachepatientsPort();
        port.editPatient(hl7Patients);
    }

    private Hl7Patients findPatient(java.lang.Object id) {
        org.hl7.cache.ws.Cachepatients port = service_1.getCachepatientsPort();
        return port.findPatient(id);
    }

    private void removePatient(org.hl7.cache.ws.Hl7Patients hl7Patients) {
        org.hl7.cache.ws.Cachepatients port = service_1.getCachepatientsPort();
        port.removePatient(hl7Patients);
    }

    private java.util.List<org.hl7.cache.ws.Hl7Patients> findAllPatient() {
        org.hl7.cache.ws.Cachepatients port = service_1.getCachepatientsPort();
        return port.findAllPatient();
    }

    private java.util.List<org.hl7.cache.ws.Hl7Doctors> findAlldoctors() {
        org.hl7.cache.ws.Cachedoctors port = service.getCachedoctorsPort();
        return port.findAlldoctors();
    }

    /**
     * @return the doctors
     */
    public Map<String, String> getDoctors() {
        return doctors;
    }

    /**
     * @param doctors the doctors to set
     */
    public void setDoctors(Map<String, String> doctors) {
        this.doctors = doctors;
    }

    /**
     * @return the doctorl
     */
    public String getDoctorl() {
        return doctorl;
    }

    /**
     * @param doctorl the doctorl to set
     */
    public void setDoctorl(String doctorl) {
        this.doctorl = doctorl;
    }

    /**
     * @return the mobileIMEI
     */
    public String getMobileIMEI() {
        return mobileIMEI;
    }

    /**
     * @param mobileIMEI the mobileIMEI to set
     */
    public void setMobileIMEI(String mobileIMEI) {
        this.mobileIMEI = mobileIMEI;
    }

    private java.util.List<org.hl7.cache.ws.Hl7Cacheconnectivity> findAllconnection() {
        org.hl7.cache.ws.Cacheconnection port = service_2.getCacheconnectionPort();
        return port.findAllconnection();
    }

    private void editconnection(org.hl7.cache.ws.Hl7Cacheconnectivity hl7Cacheconnectivity) {
        org.hl7.cache.ws.Cacheconnection port = service_2.getCacheconnectionPort();
        port.editconnection(hl7Cacheconnectivity);
    }

    private void createconnection(org.hl7.cache.ws.Hl7Cacheconnectivity hl7Cacheconnectivity) {
        org.hl7.cache.ws.Cacheconnection port = service_2.getCacheconnectionPort();
        port.createconnection(hl7Cacheconnectivity);
    }

    private String FindMobileIMEI(Long patientID) {
        java.util.List<org.hl7.cache.ws.Hl7Cacheconnectivity> pmm = findAllconnection();
        for (int i = 0; i < pmm.size(); i++) {
            if (pmm.get(i).getIpatientID().toString().equalsIgnoreCase(patientID.toString())) {
                return pmm.get(i).getMobileimei();
            }
        }
        return "N/A";
    }

    public void EditMobilePatient(Long ifind, String mobileIMEI) {
        java.util.List<org.hl7.cache.ws.Hl7Cacheconnectivity> pmm = findAllconnection();
        for (int i = 0; i < pmm.size(); i++) {
            if (pmm.get(i).getIpatientID().toString().equalsIgnoreCase(ifind.toString())) {
                org.hl7.cache.ws.Hl7Cacheconnectivity item = pmm.get(i);
                item.setMobileimei(mobileIMEI);
                editconnection(item);
                return;
            }
        }

    }

    public void AddConnection(Long id, String mobileIMEI) {
        org.hl7.cache.ws.Hl7Cacheconnectivity pitem = new org.hl7.cache.ws.Hl7Cacheconnectivity();
        pitem.setIpatientID(id);
        pitem.setMobileimei(mobileIMEI);
        createconnection(pitem);
    }
}
