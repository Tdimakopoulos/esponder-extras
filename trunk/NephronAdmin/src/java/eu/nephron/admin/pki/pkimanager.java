/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.admin.pki;

import eu.nephron.model.Doctors;
import eu.nephron.webservices.KeyManagerService;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipOutputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class pkimanager {

    OutputStream out = null;
    String filename = "pki.key";
    OutputStream outs = null;
    String filenames = "secret.key";
    private List<pkidetails> pList = new ArrayList<pkidetails>();
    private pkidetails selectedDoctor;
    private String szKey1;

    /**
     * Creates a new instance of pkimanager
     */
    public String download() throws IOException {


        //   byte[] b = getUserPublicKey(selectedDoctor.getUsername(), selectedDoctor.getRole(), selectedDoctor.getType());
        System.out.println("Looking for key for " + selectedDoctor.getUsername() + " -- " + selectedDoctor.getRole() + " -- " + selectedDoctor.getType());
        String szKey = getUserPublicKeyString(selectedDoctor.getUsername(), selectedDoctor.getRole(), selectedDoctor.getType());
        System.out.println("Download" + szKey);
        szKey1 = szKey;
        return "umngr_1?faces-redirect=true";
    }

    public String downloadback() throws IOException {



        return "umngr?faces-redirect=true";
    }

    public pkimanager() {

        java.util.List<eu.nephron.webservices.KeyDB> pFind = queryAllUsers();
        for (int i = 0; i < pFind.size(); i++) {
            pkidetails item = new pkidetails();
            item.setId(pFind.get(i).getId());
            item.setRole(pFind.get(i).getRole());
            item.setType(pFind.get(i).getOrganization());
            item.setUsername(pFind.get(i).getUsername());
            if (pFind.get(i).getOrganization().equalsIgnoreCase("SmartPhone")) {
            } else {
                if(pFind.get(i).getRole().equalsIgnoreCase("Doctor")){}else{
                pList.add(item);}
            }
        }
    }

    public void refresh() {
        pList.clear();
        java.util.List<eu.nephron.webservices.KeyDB> pFind = queryAllUsers();
        for (int i = 0; i < pFind.size(); i++) {
            pkidetails item = new pkidetails();
            item.setId(pFind.get(i).getId());
            item.setRole(pFind.get(i).getRole());
            item.setType(pFind.get(i).getOrganization());
            item.setUsername(pFind.get(i).getUsername());
            if (pFind.get(i).getOrganization().equalsIgnoreCase("SmartPhone")) {
            } else {
                if(pFind.get(i).getRole().equalsIgnoreCase("Doctor")){}else{
                pList.add(item);}
            }
        }
    }

    private java.util.List<eu.nephron.webservices.KeyDB> queryAllUsers() {
        KeyManagerService service = new KeyManagerService();
        eu.nephron.webservices.KeyManager port = service.getKeyManagerPort();
        return port.queryAllUsers();
    }

    /**
     * @return the pList
     */
    public List<pkidetails> getpList() {
        refresh();
        return pList;
    }

    /**
     * @param pList the pList to set
     */
    public void setpList(List<pkidetails> pList) {
        this.pList = pList;
    }

    /**
     * @return the selectedDoctor
     */
    public pkidetails getSelectedDoctor() {
        return selectedDoctor;
    }

    /**
     * @param selectedDoctor the selectedDoctor to set
     */
    public void setSelectedDoctor(pkidetails selectedDoctor) {
        this.selectedDoctor = selectedDoctor;
    }

    private byte[] getUserPublicKey(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) {
        KeyManagerService service = new KeyManagerService();
        eu.nephron.webservices.KeyManager port = service.getKeyManagerPort();
        return port.getUserPublicKey(arg0, arg1, arg2);
    }

    private byte[] getUserSecretKey(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) {
        KeyManagerService service = new KeyManagerService();
        eu.nephron.webservices.KeyManager port = service.getKeyManagerPort();
        return port.getUserSecretKey(arg0, arg1, arg2);
    }

    private String getUserPublicKeyString(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) {
        KeyManagerService service = new KeyManagerService();
        eu.nephron.webservices.KeyManager port = service.getKeyManagerPort();
        return port.getUserPublicKeyString(arg0, arg1, arg2);
    }

    /**
     * @return the szKey1
     */
    public String getSzKey1() {
        //szKey1=getUserPublicKeyString(selectedDoctor.getUsername(), selectedDoctor.getRole(), selectedDoctor.getType());
        //System.out.println("getter "+szKey1);
        return szKey1;
    }

    /**
     * @param szKey1 the szKey1 to set
     */
    public void setSzKey1(String szKey1) {
        this.szKey1 = szKey1;
    }
}
