/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tdim
 */
public class Patient implements Serializable {

    private Long id;
    private String code;
    private String name;
    private Integer quantity;
    private Date effectiveDate;
    private Long addressid;
    private String addresscode;
    private String addressname;
    private String administrativeArea;
    private String city;
    private String street;
    private String streetNo;
    private String floor;
    private String postCode;
    private Long contactinfoid;
    private String phone;
    private String companyPhone;
    private String mobile;
    private String companyMobile;
    private String email;
    private String companyEmail;
    private Long personalinfoid;
    private String title;
    private String firstName;
    private String middleName;
    private String lastName;
    private String fatherName;
    private int maritalStatus;
    private int educationalLevel;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the effectiveDate
     */
    public Date getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * @param effectiveDate the effectiveDate to set
     */
    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * @return the addressid
     */
    public Long getAddressid() {
        return addressid;
    }

    /**
     * @param addressid the addressid to set
     */
    public void setAddressid(Long addressid) {
        this.addressid = addressid;
    }

    /**
     * @return the addresscode
     */
    public String getAddresscode() {
        return addresscode;
    }

    /**
     * @param addresscode the addresscode to set
     */
    public void setAddresscode(String addresscode) {
        this.addresscode = addresscode;
    }

    /**
     * @return the addressname
     */
    public String getAddressname() {
        return addressname;
    }

    /**
     * @param addressname the addressname to set
     */
    public void setAddressname(String addressname) {
        this.addressname = addressname;
    }

    /**
     * @return the administrativeArea
     */
    public String getAdministrativeArea() {
        return administrativeArea;
    }

    /**
     * @param administrativeArea the administrativeArea to set
     */
    public void setAdministrativeArea(String administrativeArea) {
        this.administrativeArea = administrativeArea;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the streetNo
     */
    public String getStreetNo() {
        return streetNo;
    }

    /**
     * @param streetNo the streetNo to set
     */
    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    /**
     * @return the floor
     */
    public String getFloor() {
        return floor;
    }

    /**
     * @param floor the floor to set
     */
    public void setFloor(String floor) {
        this.floor = floor;
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * @param postCode the postCode to set
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    /**
     * @return the contactinfoid
     */
    public Long getContactinfoid() {
        return contactinfoid;
    }

    /**
     * @param contactinfoid the contactinfoid to set
     */
    public void setContactinfoid(Long contactinfoid) {
        this.contactinfoid = contactinfoid;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the companyPhone
     */
    public String getCompanyPhone() {
        return companyPhone;
    }

    /**
     * @param companyPhone the companyPhone to set
     */
    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the companyMobile
     */
    public String getCompanyMobile() {
        return companyMobile;
    }

    /**
     * @param companyMobile the companyMobile to set
     */
    public void setCompanyMobile(String companyMobile) {
        this.companyMobile = companyMobile;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the companyEmail
     */
    public String getCompanyEmail() {
        return companyEmail;
    }

    /**
     * @param companyEmail the companyEmail to set
     */
    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    /**
     * @return the personalinfoid
     */
    public Long getPersonalinfoid() {
        return personalinfoid;
    }

    /**
     * @param personalinfoid the personalinfoid to set
     */
    public void setPersonalinfoid(Long personalinfoid) {
        this.personalinfoid = personalinfoid;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the fatherName
     */
    public String getFatherName() {
        return fatherName;
    }

    /**
     * @param fatherName the fatherName to set
     */
    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    /**
     * @return the maritalStatus
     */
    public int getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * @param maritalStatus the maritalStatus to set
     */
    public void setMaritalStatus(int maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * @return the educationalLevel
     */
    public int getEducationalLevel() {
        return educationalLevel;
    }

    /**
     * @param educationalLevel the educationalLevel to set
     */
    public void setEducationalLevel(int educationalLevel) {
        this.educationalLevel = educationalLevel;
    }
}
