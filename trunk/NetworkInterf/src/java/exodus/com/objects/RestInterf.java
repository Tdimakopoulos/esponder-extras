package exodus.com.objects;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * XML Annotate Pojo to use for passing object into the web service
 * The class is annotated as XMLRootElement
 * And each class propertie as XMLElement
 * 
 */
@XmlRootElement
public class RestInterf {
	@XmlElement public int interfId;
	@XmlElement public  String name;
	@XmlElement public  String device;
	
	
}
