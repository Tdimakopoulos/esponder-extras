package exodus.com.objects;

import java.util.Vector;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * XML Annotate Pojo to use for passing object into the web service
 * The class is annotated as XMLRootElement
 * And each class propertie as XMLElement
 * 
 */
@XmlRootElement
public class RestMonitorData {
	
	@XmlElement public Vector<RestListA> interfVectorA;//Holds all available Interfaces
	@XmlElement public Vector<RestListB> interfVectorB;//Holds all available Wireless Interfaces
	@XmlElement public Vector<RestListC> interfVectorC;//Holds all available Access Points
	@XmlElement public Vector<RestInterf> VectorInterf;
  	
}
