package exodus.com.objects;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * XML Annotate Pojo to use for passing object into the web service
 * The class is annotated as XMLRootElement
 * And each class propertie as XMLElement
 * 
 */
@XmlRootElement
public class RestJDBCSet {

    @XmlElement
    public String url;
    @XmlElement
    public String dbname;
    @XmlElement
    public String driver;
    @XmlElement
    public String username;
    @XmlElement
    public String password;
}
