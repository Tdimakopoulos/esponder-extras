/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exodus.com.dbsettings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 *
 * @author tdim
 */
public class dbsettings {

    private String szPath=".";
     private String url = "jdbc:mysql://localhost:3306/";
    private String dbname = "interfdb";
    private String driver = "com.mysql.jdbc.Driver";
    private String username = "root";
    private String password = "1234";
  //  public void loadParams() {}
    public void loadParams() {
        
        //initilize variables
        Properties props = new Properties();
        InputStream is = null;

        // First try loading from the current directory
        try {
            File f = new File(szPath+"server.properties");
            is = new FileInputStream(f);
        } catch (Exception e) {
            is = null;
        }

        try {
            if (is == null) {
                // Try loading from classpath
                is = getClass().getResourceAsStream("server.properties");
            }
            // Try loading properties from the file (if found)
            props.load(is);
        } catch (Exception e) {
            //we got an error
            System.out.print("Error on load property file"+e.getMessage());
        }
        
        //load properties if no catch error
        url=props.getProperty("ServerURL", "jdbc:mysql://localhost:3306/");
        dbname=props.getProperty("DBName", "interfdb");
        driver=props.getProperty("DBDriver", "com.mysql.jdbc.Driver");
        username=props.getProperty("Username", "root");
        password=props.getProperty("Password", "1234");
        
    }

    public void saveParamChanges() {
        
        //try to save the params into propertie file
        try {
            //initialize properties var
            Properties props = new Properties();
            
            //set properties
            props.setProperty("ServerURL",url );
            props.setProperty("DBName", dbname);
            props.setProperty("DBDriver",driver );
            props.setProperty("Username",username );
            props.setProperty("Password",password );
            
            //initialize file variables
            File f = new File(szPath+"server.properties");
            OutputStream out = new FileOutputStream(f);
            
            //save into output stream with comment
            props.store(out, "Database Settings");
        } catch (Exception e) {
            //we got an error
            System.out.print("Error on save property file"+e.getMessage());
        }
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the dbname
     */
    public String getDbname() {
        return dbname;
    }

    /**
     * @param dbname the dbname to set
     */
    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    /**
     * @return the driver
     */
    public String getDriver() {
        return driver;
    }

    /**
     * @param driver the driver to set
     */
    public void setDriver(String driver) {
        this.driver = driver;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
