package exodus.com.version;

import javax.ejb.Singleton;

/** Singleton session bean used to store the name parameter for "/version" resource
 */
@Singleton
public class NameStorageBean {

    // name field
    private String name = "1.0.0";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
 
}
