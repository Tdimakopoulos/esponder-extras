/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exodus.com.dbmanager;

import exodus.com.dbcontrol.DBControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author tdim
 */
@Path("/DBManager")
public class DBManagerResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of DBManagerResource
     */
    public DBManagerResource() {
    }

    
    /**
     * PUT method for updating or creating an instance of DBManagerResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Path("/DeleteDB")
    @Consumes("application/xml")
    public void DeleteDatabase(String content) {
        DBControl pControl= new DBControl();
        pControl.DeleteAllInterf();
        pControl.DeleteAllListA();
        pControl.DeleteAllListB();
        pControl.DeleteAllListC();
    }
}
