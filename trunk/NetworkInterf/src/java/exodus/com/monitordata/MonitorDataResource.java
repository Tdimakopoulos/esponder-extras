/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exodus.com.monitordata;

import exodus.com.dbcontrol.DBControl;
import exodus.com.objects.RestInterf;
import exodus.com.objects.RestListA;
import exodus.com.objects.RestListB;
import exodus.com.objects.RestListC;
import exodus.com.objects.RestMonitorData;
import java.util.Vector;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author tdim
 */
@Path("/MonitorData")
public class MonitorDataResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of MonitorDataResource
     */
    public MonitorDataResource() {
    }

    /**
     * PUT method for updating or creating an instance of MonitorDataResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Path("/CreateMonitorData")
    @Consumes("application/xml")
    public void putXml(RestMonitorData content) {

        Vector<RestListA> vlista = content.interfVectorA;
        Vector<RestListB> vlistb = content.interfVectorB;
        Vector<RestListC> vlistc = content.interfVectorC;
        Vector<RestInterf> vinterf = content.VectorInterf;
        int isizea = -1;
        int isizeb = -1;
        int isizec = -1;
        int ivv = -1;
        if (vinterf != null) {
            ivv = vinterf.size();
        }

        if (vlista != null) {
            isizea = vlista.size();
        }

        if (vlistb != null) {
            isizeb = vlistb.size();
        }

        if (vlistc != null) {
            isizec = vlistc.size();
        }

        System.out.println("List  sizes -> Interf : " + ivv + " List A : " + isizea + " List B : " + isizeb + " List C : " + isizec);
        if (ivv == -1) {
        } else {
            DBControl dbc = new DBControl();
            DBControl pControl = new DBControl();
            pControl.DeleteAllInterf();
            pControl.DeleteAllListA();
            
          

            for (int i = 0; i < vlista.size(); i++) {
                dbc.createListA(vlista.get(i));
            }
            if (isizeb != -1) {
                pControl.DeleteAllListB();
                for (int i = 0; i < vlistb.size(); i++) {
                    dbc.createListB(vlistb.get(i));
                }
            }else
            {
                System.out.println("Skip List B");
            }
            if (isizec != -1) {
                  pControl.DeleteAllListC();
                for (int i = 0; i < vlistc.size(); i++) {
                    if (vlistc.get(i).listCId > -1) {
                        dbc.createListC(vlistc.get(i));
                    }
                }
            }else
            {
                System.out.println("Skip List C");
            }

            for (int i = 0; i < vinterf.size(); i++) {
                dbc.createInterf(vinterf.get(i));
            }
        }
    }
}
