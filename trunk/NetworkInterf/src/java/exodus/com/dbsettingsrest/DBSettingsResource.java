/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exodus.com.dbsettingsrest;

import exodus.com.dbsettings.dbsettings;
import exodus.com.objects.RestJDBCSet;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author tdim
 */
@Path("DBSettings")
public class DBSettingsResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of DBSettingsResource
     */
    public DBSettingsResource() {
    }

    
    /**
     * PUT method for updating or creating an instance of DBSettingsResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Path("/SetDBParams")
    @Consumes("application/xml")
    public void putXml(RestJDBCSet pSetvar) {
        dbsettings pSet= new dbsettings();
        pSet.setDbname(pSetvar.dbname);
        pSet.setDriver(pSetvar.driver);
        pSet.setUrl(pSetvar.url);
        pSet.setPassword(pSetvar.password);
        pSet.setUsername(pSetvar.username);
        pSet.saveParamChanges();
    }
}
