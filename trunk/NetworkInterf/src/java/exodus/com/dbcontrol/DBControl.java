/*

--------db--------
create database interfdb;

show databases;

use interfdb;

insert into interf(interfId,interfname,deviceName) values (1, 'eth0');

insert into listA(id,interfId,macAddress, ipAddress, netMask, broadcastAddress, gateway, maxTxRate, curTxRate, curPercLineRate, packetErrorRate, txBytes, RxBytes, rxPackets, txPackets, rxErrors, txErrors) values (1, 1, '', '', '','','',0.0,0.0,0.0,0.0,0.0,0.0,0,0,0,0);

insert into listB(id, interfId, status, txPower, linkQuality, signalLevel, linkLevelNoise, discardedPackets) values (1, 1, '', '', '','','','');

insert into listBwithAP(wirId,apId) values (1,1);

insert into listC(id, acPoMac, essid, channel, status, signalLevel) values(1,'','','','','');

select a.id as ID, a.interfName as NAME, b.macAddress as "MAC ADDR",b.netMask as "MASK", b.broadcastAddress as "BROADCAST", b.maxTXRate as "MAX Rate" from interf a join listA b on a.id = b.interfId;

CREATE TABLE interf ( interfId integer NOT NULL, name varchar(255) NOT NULL,device varchar(255), primary key(interfId) );


CREATE TABLE lista ( listAId integer NOT NULL, interfId integer NOT NULL, macAddress varchar(255), ipAddress varchar(255), netMask varchar(255), netAddress varchar(255), broadcastAddress varchar(255), gateway varchar(255), maxTxRate double(10,2), curTxRate double(10,2), curPercLineRate double(10,2), packetErrorRate double(10,2), TxBytes double(15,2), RxBytes double(15,2), rxPackets integer, txPackets integer, rxErrors integer, txErrors integer, primary key (listAId));

 ALTER TABLE lista ADD CONSTRAINT FK_listA foreign key (interfId) references interf(interfId);

CREATE TABLE listb ( listBId integer NOT NULL, interfId integer NOT NULL, status varchar(255), txPower varchar(255), linkQuality varchar(255), macAddressAP varchar(255),  Essid varchar(255), Channel varchar(255)signalLevel varchar(255), linkLevelNoise varchar(255), discardedPackets varchar(255), primary key (listBId));

ALTER TABLE listb ADD CONSTRAINT FK_listB foreign key (interfId) references interf(interfId);

CREATE TABLE listc ( listCId integer NOT NULL, AcPoMAC varchar(255)  NOT NULL, Essid varchar(255), Channel varchar(255), Status varchar(255), SignalLevel varchar(255), primary key (listCId));




 */
package exodus.com.dbcontrol;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import exodus.com.dbsettings.dbsettings;
import exodus.com.objects.RestInterf;
import exodus.com.objects.RestListA;
import exodus.com.objects.RestListB;
import exodus.com.objects.RestListC;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tom
 */
public class DBControl {

    /*
     * Private variables
     */
    Connection con = null;
    Statement st = null;
    /*
     * Set From outside
     */
    private String url = "jdbc:mysql://localhost:3306/";
    private String db = "ninterf";
    private String driver = "com.mysql.jdbc.Driver";
    private String DBUsername = "root";
    private String DBPassword = "cb1312ef";

    /*
     * DB Connection managers
     */
    public void MakeConnection() throws ClassNotFoundException, SQLException {
        //load settings 
        dbsettings pset = new dbsettings();
        pset.loadParams();
        this.setDBPassword(pset.getPassword());
        this.setDBUsername(pset.getUsername());
        this.setDb(pset.getDbname());
        this.setDriver(pset.getDriver());
        this.setUrl(pset.getUrl());
        
        //set the database driver
        Class.forName(getDriver());
        
        //make new connection
        con = (Connection) DriverManager.getConnection(getUrl() + getDb(), getDBUsername(), getDBPassword());
    }

    public void FreeConnections() throws SQLException {
        
        //close connection
        con.close();
    }

    /*
     * Delete All Records
     */
    public int DeleteAllInterf() {
        int rows = 0;
        try {
            MakeConnection();
            try {
                String sql = "DELETE FROM interf";
                PreparedStatement statement = (PreparedStatement) con.prepareStatement(sql);
                rows = statement.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
            return -1;
        }
        return rows;
    }

    public int DeleteAllListA() {
        int rows = 0;
        try {
            MakeConnection();
            try {
                String sql = "DELETE FROM lista";
                PreparedStatement statement = (PreparedStatement) con.prepareStatement(sql);
                rows = statement.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
            return -1;
        }
        return rows;
    }

    public int DeleteAllListB() {
        int rows = 0;
        try {
            MakeConnection();
            try {
                String sql = "DELETE FROM listb";
                PreparedStatement statement = (PreparedStatement) con.prepareStatement(sql);
                rows = statement.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
            return -1;
        }
        return rows;
    }

    public int DeleteAllListC() {
        int rows = 0;
        try {
            MakeConnection();
            try {
                String sql = "DELETE FROM listc";
                PreparedStatement statement = (PreparedStatement) con.prepareStatement(sql);
                rows = statement.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
            return -1;
        }
        return rows;
    }

    /*
     * Delete only one based on ID
     */
    public int DeleteInterf(int interfId) {
        int rows = 0;
        try {
            MakeConnection();
            try {
                String sql = "DELETE FROM interf WHERE interfId=?";
                PreparedStatement statement = (PreparedStatement) con.prepareStatement(sql);
                statement.setInt(1, interfId);
                rows = statement.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
            return -1;
        }
        return rows;
    }

    public int DeleteListA(int listAId) {
        int rows = 0;
        try {
            MakeConnection();
            try {
                String sql = "DELETE FROM lista WHERE listAId=?";
                PreparedStatement statement = (PreparedStatement) con.prepareStatement(sql);
                statement.setInt(1, listAId);
                rows = statement.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
            return -1;
        }
        return rows;
    }

    public int DeleteListB(int listBId) {
        int rows = 0;
        try {
            MakeConnection();
            try {
                String sql = "DELETE FROM listb WHERE listBId=?";
                PreparedStatement statement = (PreparedStatement) con.prepareStatement(sql);
                statement.setInt(1, listBId);
                rows = statement.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
            return -1;
        }
        return rows;
    }

    public int DeleteListC(int listCId) {
        int rows = 0;
        try {
            MakeConnection();
            try {
                String sql = "DELETE FROM listc WHERE listCId=?";
                PreparedStatement statement = (PreparedStatement) con.prepareStatement(sql);
                statement.setInt(1, listCId);
                rows = statement.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
            return -1;
        }
        return rows;
    }

    /*
     * Update record
     */
    public void UpdateInterf(RestInterf Values) {
        try {
            MakeConnection();
            try {
                // create the java mysql update preparedstatement
                String query = "update interf set device = ? , name = ? where interfid = ?";
                PreparedStatement preparedStmt = (PreparedStatement) con.prepareStatement(query);
                preparedStmt.setString(1, Values.device);
                preparedStmt.setString(2, Values.name);
                preparedStmt.setInt(3, Values.interfId);
                // execute the java preparedstatement
                preparedStmt.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        }

    }

    public void UpdateListA(RestListA Values) {
        try {
            MakeConnection();
            try {
                // create the java mysql update preparedstatement
                String query =
                        "update lista set name=?,macAddress=?,ipAddress=?,netMask=?,netAddress=?,broadcastAddress=?,"
                        + "gateway=?,maxTxRate=?,curTxRate=?,curPercLineRate=?,packetErrorRate=?,"
                        + "TxBytes=?,RxBytes=?,rxPackets=?,txPackets=?,rxErrors=?,txErrors=? where listAId=?";
                PreparedStatement preparedStmt = (PreparedStatement) con.prepareStatement(query);

                // execute the java preparedstatement
                //FIXME
                //preparedStmt.setString(1, Values.name);
                preparedStmt.setString(2, Values.macAddress);
                preparedStmt.setString(3, Values.ipAddress);
                preparedStmt.setString(4, Values.netMask);
                preparedStmt.setString(5, Values.netAddress);
                preparedStmt.setString(6, Values.broadcastAddress);
                preparedStmt.setString(7, Values.gateway);
                preparedStmt.setDouble(8, Values.maxTxRate);
                preparedStmt.setDouble(9, Values.curTxRate);
                preparedStmt.setDouble(10, Values.curPercLineRate);
                preparedStmt.setDouble(11, Values.packetErrorRate);
                preparedStmt.setDouble(12, Values.TxBytes);
                preparedStmt.setDouble(13, Values.RxBytes);
                preparedStmt.setInt(14, Values.rxPackets);
                preparedStmt.setInt(15, Values.txPackets);
                preparedStmt.setInt(16, Values.rxErrors);
                preparedStmt.setInt(17, Values.txErrors);
                preparedStmt.setInt(18, Values.listAId);

                preparedStmt.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        }

    }

    public void UpdateListB(RestListB Values) {
        try {
            MakeConnection();
            try {
                // create the java mysql update preparedstatement
                String query = "update listb set listBId=?,name=?,macAddressAP=?,essid=?,channel=?,"
                        + "accessPointStatus=?,txPower=?,linkQuality=?,signalLevel=?,"
                        + "linkLevelNoise=?,discardedPackets=? where listBId = ?";
                PreparedStatement preparedStmt = (PreparedStatement) con.prepareStatement(query);
                //FIXME
                //preparedStmt.setString(1, Values.name);
                preparedStmt.setString(2, Values.macAddressAP);
                preparedStmt.setString(3, Values.essid);
                preparedStmt.setString(4, Values.channel);
                preparedStmt.setString(5, Values.accessPointStatus);
                preparedStmt.setString(6, Values.txPower);
                preparedStmt.setString(7, Values.linkQuality);
                preparedStmt.setString(8, Values.signalLevel);
                preparedStmt.setString(9, Values.linkLevelNoise);
                preparedStmt.setString(10, Values.discardedPackets);
                preparedStmt.setInt(11, Values.listBId);

                // execute the java preparedstatement
                preparedStmt.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        }

    }

    public void UpdateListC(RestListC Values) {
        try {
            MakeConnection();
            try {
                // create the java mysql update preparedstatement
                String query = "update listc set AcPoMAC=?,Essid=?,Channel=?,Status=?,SignalLevel=? where listCId = ?";
                PreparedStatement preparedStmt = (PreparedStatement) con.prepareStatement(query);
                preparedStmt.setString(1, Values.AcPoMAC);
                preparedStmt.setString(2, Values.Essid);
                preparedStmt.setString(3, Values.Channel);
                preparedStmt.setString(4, Values.Status);
                preparedStmt.setString(5, Values.SignalLevel);
                preparedStmt.setInt(6, Values.listCId);
                // execute the java preparedstatement
                preparedStmt.executeUpdate();
                FreeConnections();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        }

    }

    /*
     * 
     * Select Records
     * 
     */
    public List<RestInterf> SelectFromInterf(String szSelectCriteria) {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        List<RestInterf> pReturn=null;
        pReturn = new ArrayList();
        
        try {
            MakeConnection();
            String query = "select interfid,device,name from interf " + szSelectCriteria;

            pstmt = (PreparedStatement) con.prepareStatement(query); // create a statement
            
            rs = pstmt.executeQuery();
            // extract data from the ResultSet
            while (rs.next()) {
                RestInterf element=new RestInterf();
                element.interfId=rs.getInt(1);
                element.device=rs.getString(2);
                element.name=rs.getString(3);
                pReturn.add(element);
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        } finally {
            try {
                rs.close();
                pstmt.close();
                FreeConnections();
            } catch (SQLException e) {
                System.out.println("Close Connection has Errors! Error : " + e.getMessage());
            }
        }
        return pReturn;
    }
    
    public List<RestListA> SelectFromListA(String szSelectCriteria) {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        List<RestListA> pReturn=null;
        pReturn = new ArrayList();
        
        try {
            MakeConnection();
            String query = "select listAId,name,macAddress,ipAddress,netMask,netAddress,broadcastAddress,gateway,maxTxRate,curTxRate,curPercLineRate,packetErrorRate,TxBytes,RxBytes,rxPackets,txPackets,rxErrors,txErrors from lista " + szSelectCriteria;

            pstmt = (PreparedStatement) con.prepareStatement(query); // create a statement
            
            rs = pstmt.executeQuery();
            // extract data from the ResultSet
            while (rs.next()) {
                RestListA element=new RestListA();
                element.listAId=rs.getInt(1);
                //FIXME
                //element.name=rs.getString(2);
                element.macAddress=rs.getString(3);
                element.ipAddress=rs.getString(4);
                element.netMask=rs.getString(5);
                element.netAddress=rs.getString(6);
                element.broadcastAddress=rs.getString(7);
                element.gateway=rs.getString(8);
                element.maxTxRate=rs.getDouble(9);
                element.curTxRate=rs.getDouble(10);
                element.curPercLineRate=rs.getDouble(11);
                element.packetErrorRate=rs.getDouble(12);
                element.TxBytes=rs.getDouble(13);
                element.RxBytes=rs.getDouble(14);
                element.rxPackets=rs.getInt(15);
                element.txPackets=rs.getInt(16);
                element.rxErrors=rs.getInt(17);
                element.txErrors=rs.getInt(18);
                pReturn.add(element);
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        } finally {
            try {
                rs.close();
                pstmt.close();
                FreeConnections();
            } catch (SQLException e) {
                System.out.println("Close Connection has Errors! Error : " + e.getMessage());
            }
        }
        return pReturn;
    }
    
    
    public List<RestListB> SelectFromListB(String szSelectCriteria) {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        List<RestListB> pReturn=null;
        pReturn = new ArrayList();
        
        try {
            MakeConnection();
            String query = "select listBId,name,macAddressAP,essid,channel,accessPointStatus,txPower,linkQuality,signalLevel,linkLevelNoise,discardedPackets from listb " + szSelectCriteria;

            pstmt = (PreparedStatement) con.prepareStatement(query); // create a statement
            
            rs = pstmt.executeQuery();
            // extract data from the ResultSet
            while (rs.next()) {
                RestListB element=new RestListB();
                element.listBId=rs.getInt(1);
                //FIXME
                //element.name=rs.getString(2);
                element.macAddressAP=rs.getString(3);
                element.essid=rs.getString(4);
                element.channel=rs.getString(5);
                element.accessPointStatus=rs.getString(6);
                element.txPower=rs.getString(7);
                element.linkQuality=rs.getString(8);
                element.signalLevel=rs.getString(9);
                element.linkLevelNoise=rs.getString(10);
                element.discardedPackets=rs.getString(11);
                pReturn.add(element);
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        } finally {
            try {
                rs.close();
                pstmt.close();
                FreeConnections();
            } catch (SQLException e) {
                System.out.println("Close Connection has Errors! Error : " + e.getMessage());
            }
        }
        return pReturn;
    }
    
    public List<RestListC> SelectFromListC(String szSelectCriteria) {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        List<RestListC> pReturn=null;
        pReturn = new ArrayList();
        
        try {
            MakeConnection();
            String query = "select listCId,AcPoMAC,Essid,Channel,Status,SignalLevel from listc " + szSelectCriteria;

            pstmt = (PreparedStatement) con.prepareStatement(query); // create a statement
            
            rs = pstmt.executeQuery();
            // extract data from the ResultSet
            while (rs.next()) {
                RestListC element=new RestListC();
                element.listCId=rs.getInt(1);
                element.AcPoMAC=rs.getString(2);
                element.Essid=rs.getString(3);
                element.Channel=rs.getString(4);
                element.Status=rs.getString(5);
                element.SignalLevel=rs.getString(6);
                pReturn.add(element);
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        } finally {
            try {
                rs.close();
                pstmt.close();
                FreeConnections();
            } catch (SQLException e) {
                System.out.println("Close Connection has Errors! Error : " + e.getMessage());
            }
        }
        return pReturn;
    }
    
    
    
    /*
     * Create record
     */

    public void createInterf(RestInterf Values) {
        try {
            MakeConnection();
            try {
                st = (Statement) con.createStatement();
                int val = st.executeUpdate("INSERT interf (interfid,device,name) VALUES(" + Values.interfId+ "," + "'" + Values.device+ "'," + "'" + Values.name+ "'" + ")");
                FreeConnections();
                st.close();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        }

    }

    public void createListA(RestListA Values) {
        try {
            MakeConnection();
            try {
                st = (Statement) con.createStatement();
                int val = st.executeUpdate("INSERT lista (listAId,interfId,macAddress,ipAddress,netMask,netAddress,broadcastAddress,"
                        + "gateway,maxTxRate,curTxRate,curPercLineRate,packetErrorRate,"
                        + "TxBytes,RxBytes,rxPackets,txPackets,rxErrors,txErrors) VALUES("
                        + Values.listAId+ ","
                        + "" + Values.interfId+ ","
                        + "'" + Values.macAddress+ "',"
                        + "'" + Values.ipAddress+ "',"
                        + "'" + Values.netMask+ "',"
                        + "'" + Values.netAddress+ "',"
                        + "'" + Values.broadcastAddress+ "',"
                        + "'" + Values.gateway+ "',"
                        + Values.maxTxRate+ ","
                        + Values.curTxRate+ ","
                        + Values.curPercLineRate+ ","
                        + Values.packetErrorRate+ ","
                        + Values.TxBytes+ ","
                        + Values.RxBytes+ ","
                        + Values.rxPackets+ ","
                        + Values.txPackets+ ","
                        + Values.rxErrors+ ","
                        + Values.txErrors+ ")");

                FreeConnections();
                st.close();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        }

    }

    public void createListB(RestListB Values) {
        try {
            MakeConnection();
            try {
                st = (Statement) con.createStatement();
                int val = st.executeUpdate("INSERT listb (listBId,interfId,macAddressAP,Essid,Channel,"
                        + "status,txPower,linkQuality,signalLevel,"
                        + "linkLevelNoise,discardedPackets) VALUES("
                        + Values.listBId+ ","
                        + "" + Values.interfId+ ","
                        + "'" + Values.macAddressAP+ "',"
                        + "'" + Values.essid+ "',"
                        + "'" + Values.channel+ "',"
                        + "'" + Values.accessPointStatus+ "',"
                        + "'" + Values.txPower+ "',"
                        + "'" + Values.linkQuality+ "',"
                        + "'" + Values.signalLevel+ "',"
                        + "'" + Values.linkLevelNoise+ "',"
                        + "'" + Values.discardedPackets+ "'"
                        + ")");

                FreeConnections();
                st.close();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        }

    }

    public void createListC(RestListC Values) {
        try {
            MakeConnection();
            try {
                st = (Statement) con.createStatement();
                int val = st.executeUpdate("INSERT listc (listCId,AcPoMAC,Essid,"
                        + "Channel,Status,SignalLevel,device) VALUES("
                        + Values.listCId+ ","
                        + "'" + Values.AcPoMAC+ "',"
                        + "'" + Values.Essid+ "',"
                        + "'" + Values.Channel+ "',"
                        + "'" + Values.Status+ "',"
                        + "'" + Values.SignalLevel+ "'"
                        + "'" + Values.device+ "'"
                        + ")");

                FreeConnections();
                st.close();
            } catch (SQLException s) {
                System.out.println("SQL statement is not executed! Error : " + s.getMessage());
            }
        } catch (Exception e) {
            System.out.println("Make Connection has Errors! Error : " + e.getMessage());
        }

    }

    /*
     * Getters and Setters
     */
    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the db
     */
    public String getDb() {
        return db;
    }

    /**
     * @param db the db to set
     */
    public void setDb(String db) {
        this.db = db;
    }

    /**
     * @return the driver
     */
    public String getDriver() {
        return driver;
    }

    /**
     * @param driver the driver to set
     */
    public void setDriver(String driver) {
        this.driver = driver;
    }

    /**
     * @return the DBUsername
     */
    public String getDBUsername() {
        return DBUsername;
    }

    /**
     * @param DBUsername the DBUsername to set
     */
    public void setDBUsername(String DBUsername) {
        this.DBUsername = DBUsername;
    }

    /**
     * @return the DBPassword
     */
    public String getDBPassword() {
        return DBPassword;
    }

    /**
     * @param DBPassword the DBPassword to set
     */
    public void setDBPassword(String DBPassword) {
        this.DBPassword = DBPassword;
    }
}
