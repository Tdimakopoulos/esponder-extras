/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cache.database;

import java.io.Serializable;


public class CDWAKDMeasurments implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;

    private Long type;
    private double dvalue;
    private String IMEI;
    private Long ddate;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CDWAKDMeasurments)) {
            return false;
        }
        CDWAKDMeasurments other = (CDWAKDMeasurments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.secure.DB.WAKDMeasurments[ id=" + id + " ]";
    }

    /**
     * @return the type
     */
    public Long getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Long type) {
        this.type = type;
    }

    /**
     * @return the value
     */
    public double getValue() {
        return dvalue;
    }

    /**
     * @param value the value to set
     */
    public void setValue(double value) {
        this.dvalue = value;
    }

    /**
     * @return the IMEI
     */
    public String getIMEI() {
        return IMEI;
    }

    /**
     * @param IMEI the IMEI to set
     */
    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    /**
     * @return the date
     */
    public Long getDate() {
        return ddate;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.ddate = date;
    }
    
}
