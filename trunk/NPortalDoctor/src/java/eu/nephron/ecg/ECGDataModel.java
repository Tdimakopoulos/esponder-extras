/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ecg;

/**
 *
 * @author tdim
 */
public class ECGDataModel {
    private String sdate;
    private String sFilename;

    /**
     * @return the sdate
     */
    public String getSdate() {
        return sdate;
    }

    /**
     * @param sdate the sdate to set
     */
    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    /**
     * @return the sFilename
     */
    public String getsFilename() {
        return sFilename;
    }

    /**
     * @param sFilename the sFilename to set
     */
    public void setsFilename(String sFilename) {
        this.sFilename = sFilename;
    }
    
}
