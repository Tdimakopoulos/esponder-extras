/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.applicationbean;

import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.hl7.cache.ws.Cachepatients_Service;
import org.hl7.cache.ws.Hl7Patients;

/**
 *
 * @author tdim
 */
@ManagedBean
@RequestScoped
public class PersonalInfo {

    private int ilevel;
    private String fathernamep;
    private String firstnamep;
    private String lastnamep;
    private int imstatus;
    private String middlename;
    private String title;
    private String companyemail;
    private String companymobile;
    private String companyphone;
    private String email;
    private String mobile;
    private String phone;
    private String administrativearea;
    private String city;
    private String code;
    private String floor;
    private String postcode;
    private String street;
    private String streetno;
    private String patienteducationlevel;
    private String patientmaritalstatus;
    private Cachepatients_Service service = new Cachepatients_Service();
    private String fathername;
    private String firstname;
    private String lastname;

    public String LoadPatientDetails() {

        

        

        

        return "PatientDetails?faces-redirect=true";
    }
    
    private java.util.List<org.hl7.cache.ws.Hl7Patients> findPatientsByDoctor(java.lang.Long doctorID) {
        org.hl7.cache.ws.Cachepatients port = service.getCachepatientsPort();
        return port.findPatientsByDoctor(doctorID);
    }
    
    /**
     * Creates a new instance of PersonalInfo
     */
    public PersonalInfo() {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
        long ifind=Long.valueOf(puserid);
    Hl7Patients ppat=findPatient(ifind);
        
        //for (int is = 0; is < ppat.size(); is++) {
          //  if (ppat.get(is).getId().toString().equalsIgnoreCase(String.valueOf(ifind))) {
                System.out.println("Patient Personal Info : "+ppat.getFirstName()+"  "+ppat.getLastName());
                fathername = ppat.getFatherName();
                firstname = ppat.getFirstName();
                lastname = ppat.getLastName();
//    private int imstatus;
                middlename = ppat.getMiddleName();
                title = ppat.getTitle();
                companyemail = ppat.getCompanyEmail();
                companymobile = ppat.getCompanyMobile();
                companyphone = ppat.getCompanyPhone();
                email = ppat.getEmail();
                mobile = ppat.getMobile();
                phone = ppat.getPhone();
                administrativearea = ppat.getAdministrativeArea();
                city = ppat.getCity();
                code = ppat.getCode();
                floor = ppat.getFloor();
                postcode = ppat.getPostCode();
                street = ppat.getStreet();
                streetno = ppat.getStreetNo();
            //}
        //}
    }

    
    public String edituser() {

       
    
    
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
        long ifind=Long.valueOf(puserid);
    Hl7Patients ppat=findPatient(ifind);
        
        //for (int is = 0; is < ppat.size(); is++) {
          //  if (ppat.get(is).getId().toString().equalsIgnoreCase(String.valueOf(ifind))) {
                System.out.println("Patient Personal Info : "+ppat.getFirstName()+"  "+ppat.getLastName());
                fathername = ppat.getFatherName();
                firstname = ppat.getFirstName();
                lastname = ppat.getLastName();
//    private int imstatus;
                middlename = ppat.getMiddleName();
                title = ppat.getTitle();
                companyemail = ppat.getCompanyEmail();
                companymobile = ppat.getCompanyMobile();
                companyphone = ppat.getCompanyPhone();
                email = ppat.getEmail();
                mobile = ppat.getMobile();
                phone = ppat.getPhone();
                administrativearea = ppat.getAdministrativeArea();
                city = ppat.getCity();
                code = ppat.getCode();
                floor = ppat.getFloor();
                postcode = ppat.getPostCode();
                street = ppat.getStreet();
                streetno = ppat.getStreetNo();
            //}
        //}
                    return "Patient_Details?faces-redirect=true";
    }
    
    /**
     * @return the ilevel
     */
    public int getIlevel() {
        return ilevel;
    }

    /**
     * @param ilevel the ilevel to set
     */
    public void setIlevel(int ilevel) {
        this.ilevel = ilevel;
    }

    /**
     * @return the fathernamep
     */
    public String getFathernamep() {
        return fathernamep;
    }

    /**
     * @param fathernamep the fathernamep to set
     */
    public void setFathernamep(String fathernamep) {
        this.fathernamep = fathernamep;
    }

    /**
     * @return the firstnamep
     */
    public String getFirstnamep() {
        return firstnamep;
    }

    /**
     * @param firstnamep the firstnamep to set
     */
    public void setFirstnamep(String firstnamep) {
        this.firstnamep = firstnamep;
    }

    /**
     * @return the lastnamep
     */
    public String getLastnamep() {
        return lastnamep;
    }

    /**
     * @param lastnamep the lastnamep to set
     */
    public void setLastnamep(String lastnamep) {
        this.lastnamep = lastnamep;
    }

    /**
     * @return the imstatus
     */
    public int getImstatus() {
        return imstatus;
    }

    /**
     * @param imstatus the imstatus to set
     */
    public void setImstatus(int imstatus) {
        this.imstatus = imstatus;
    }

    /**
     * @return the middlename
     */
    public String getMiddlename() {
        return middlename;
    }

    /**
     * @param middlename the middlename to set
     */
    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the companyemail
     */
    public String getCompanyemail() {
        return companyemail;
    }

    /**
     * @param companyemail the companyemail to set
     */
    public void setCompanyemail(String companyemail) {
        this.companyemail = companyemail;
    }

    /**
     * @return the companymobile
     */
    public String getCompanymobile() {
        return companymobile;
    }

    /**
     * @param companymobile the companymobile to set
     */
    public void setCompanymobile(String companymobile) {
        this.companymobile = companymobile;
    }

    /**
     * @return the companyphone
     */
    public String getCompanyphone() {
        return companyphone;
    }

    /**
     * @param companyphone the companyphone to set
     */
    public void setCompanyphone(String companyphone) {
        this.companyphone = companyphone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the administrativearea
     */
    public String getAdministrativearea() {
        return administrativearea;
    }

    /**
     * @param administrativearea the administrativearea to set
     */
    public void setAdministrativearea(String administrativearea) {
        this.administrativearea = administrativearea;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the floor
     */
    public String getFloor() {
        return floor;
    }

    /**
     * @param floor the floor to set
     */
    public void setFloor(String floor) {
        this.floor = floor;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode the postcode to set
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the streetno
     */
    public String getStreetno() {
        return streetno;
    }

    /**
     * @param streetno the streetno to set
     */
    public void setStreetno(String streetno) {
        this.streetno = streetno;
    }

    /**
     * @return the patienteducationlevel
     */
    public String getPatienteducationlevel() {
        return patienteducationlevel;
    }

    /**
     * @param patienteducationlevel the patienteducationlevel to set
     */
    public void setPatienteducationlevel(String patienteducationlevel) {
        this.patienteducationlevel = patienteducationlevel;
    }

    /**
     * @return the patientmaritalstatus
     */
    public String getPatientmaritalstatus() {
        return patientmaritalstatus;
    }

    /**
     * @param patientmaritalstatus the patientmaritalstatus to set
     */
    public void setPatientmaritalstatus(String patientmaritalstatus) {
        this.patientmaritalstatus = patientmaritalstatus;
    }

    private Hl7Patients findPatient(java.lang.Object id) {
        org.hl7.cache.ws.Cachepatients port = service.getCachepatientsPort();
        return port.findPatient(id);
    }

    /**
     * @return the fathername
     */
    public String getFathername() {
        return fathername;
    }

    /**
     * @param fathername the fathername to set
     */
    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
