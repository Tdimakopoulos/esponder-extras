/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.alerts;

import eu.nephron.applicationbean.BasicApplicationBean;
import eu.nephron.cache.alert.db.aledb;
import eu.nephron.cache.alerts.alertscached;
import eu.nephron.model.DSSResults;
import eu.nephron.opt.ws.NephronOptionWS_Service;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.hl7.cache.ws.Cacheconnection_Service;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class AlertsBean {

    private Cacheconnection_Service service_3 = new Cacheconnection_Service();
    private java.util.List<org.hl7.cache.ws.Hl7Alerts> paler = new ArrayList();
    private List<DSSResults> originalsumups;
    private List<DSSResults> sumups2;
    private Date dateFromM = new Date();
    private Date dateToM = new Date();
    private String preset = "0";
    private NephronOptionWS_Service service_5 = new NephronOptionWS_Service();
    private List<String> pValuesDB = new ArrayList();

    ///////////////////////////// LOGIC Functions ///////////////////////////////////////////////
    /**
     * Constructor
     */
    public AlertsBean() {
        sumups2 = new ArrayList<DSSResults>();
        originalsumups = new ArrayList<DSSResults>();

        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
        long ifind = Long.valueOf(puserid);
        String imei = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        paler = findAlertsByDoctorAndDateFrom(ifind, imei);
        initialize_Alerts(ifind);
        LoadFromDB();
    }

    private List reverseList(List myList) {
        List invertedList = new ArrayList();
        for (int i = myList.size() - 1; i >= 0; i--) {
            invertedList.add(myList.get(i));
        }
        return invertedList;
    }

    public void UpdateRecords(ActionEvent actionEvent) {
        sumups2.clear();
        for (int i = 0; i < paler.size(); i++) {
            DSSResults pNew = new DSSResults();
            pNew.setId(paler.get(i).getId());
            pNew.setRuleresults(FindPatientNameForIMEI(paler.get(i).getSzvalue()));
            pNew.setRuleresults1(paler.get(i).getAlertmsg());
            pNew.setRuleresults3(paler.get(i).getSzvalue());
            Date ndate = new Date();
            ndate.setTime(paler.get(i).getCDate());
            pNew.setRuleresults2(ndate.toString());
            pNew.setRulename(paler.get(i).getCDate().toString());

            if (paler.get(i).getCDate() > dateFromM.getTime()) {
                if (paler.get(i).getCDate() < dateToM.getTime()) {

                    if(preset.equalsIgnoreCase("1"))
                    {
                        for (int ixx = 0; ixx < pValuesDB.size(); ixx++) {
                            if (pNew.getRuleresults1().equalsIgnoreCase(pValuesDB.get(ixx))) {
                                sumups2.add(pNew);
                            }
                        }
                    }else{
                        sumups2.add(pNew);
                    }
                    
                    
                }
            }

        }
        sumups2 = reverseList(sumups2);
    }

    public String LoadAlertsPage() {
        dateFromM = new Date();
        dateToM = new Date();
        sumups2 = new ArrayList<DSSResults>();
        Date pdate = new Date();
        Calendar pcal = new GregorianCalendar();
        pcal.setTime(pdate);
        pcal.add(Calendar.DATE, -7);
        dateFromM.setTime(pcal.getTime().getTime());
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
        long ifind = Long.valueOf(puserid);
        String imei = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        paler = findAlertsByDoctorAndDateFrom(ifind, imei);
        initialize_Alerts(ifind);
        return "Alerts?faces-redirect=true";

    }

    public void initialize_Alerts(Long ifind) {


        sumups2.clear();
        for (int i = 0; i < paler.size(); i++) {
            DSSResults pNew = new DSSResults();
            pNew.setId(paler.get(i).getId());
            pNew.setRuleresults(FindPatientNameForIMEI(paler.get(i).getSzvalue()));
            pNew.setRuleresults1(paler.get(i).getAlertmsg());
            pNew.setRuleresults3(paler.get(i).getSzvalue());
            Date ndate = new Date();
            ndate.setTime(paler.get(i).getCDate());
            pNew.setRuleresults2(ndate.toString());
            pNew.setRulename(paler.get(i).getCDate().toString());

            if (paler.get(i).getCDate() > dateFromM.getTime()) {
                if (paler.get(i).getCDate() < dateToM.getTime()) {
                    sumups2.add(pNew);
                }
            }

        }
        sumups2 = reverseList(sumups2);
    }

    private String FindPatientNameForIMEI(String IMEI) {

//        String patientname = "";
//        for (int i = 0; i < pcon.size(); i++) {
//            if (pcon.get(i).getMobileimei().equalsIgnoreCase(IMEI)) {
//
//                for (int id = 0; id < Patients.size(); id++) {
//                    if (Patients.get(id).getId().toString().equalsIgnoreCase(pcon.get(i).getIpatientID().toString())) {
//                        patientname = Patients.get(id).getFirstName() + " " + Patients.get(id).getLastName();
//                        return patientname;
//                    }
//                }
//            }
//        }
        return IMEI;
    }

    private java.util.List<org.hl7.cache.ws.Hl7Alerts> findAlertsByDoctorAndDateFrom(java.lang.Long patientID, String imei) {

        java.util.List<org.hl7.cache.ws.Hl7Alerts> pList = new ArrayList();
        try {
            aledb pdb = new aledb();
            pdb.SetIMEI(imei);
            pdb.LoadFromFile();
            ArrayList<alertscached> plist = pdb.getList();

            for (int ix = 0; ix < plist.size(); ix++) {

                org.hl7.cache.ws.Hl7Alerts aa = new org.hl7.cache.ws.Hl7Alerts();
                aa.setIddoctor(plist.get(ix).getIddoctor());
                aa.setIdpatient(plist.get(ix).getIdpatient());
                aa.setSzvalue(plist.get(ix).getSzvalue());
                aa.setCDate(plist.get(ix).getcDate());
                aa.setAlertmsg(plist.get(ix).getAlertmsg());
                aa.setAlertmsg1(plist.get(ix).getAlertmsg1());
                pList.add(aa);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BasicApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BasicApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BasicApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pList;
    }

    ///////////////////////////////// Web Service Calls ///////////////////////////////////////////////////////
    private java.util.List<org.hl7.cache.ws.Hl7Cacheconnectivity> findAllconnection() {
        org.hl7.cache.ws.Cacheconnection port = service_3.getCacheconnectionPort();
        return port.findAllconnection();
    }

    /**
     * @return the paler
     */
    public java.util.List<org.hl7.cache.ws.Hl7Alerts> getPaler() {
        return paler;
    }

    /**
     * @param paler the paler to set
     */
    public void setPaler(java.util.List<org.hl7.cache.ws.Hl7Alerts> paler) {
        this.paler = paler;
    }

    /**
     * @return the sumups2
     */
    public List<DSSResults> getSumups2() {
        return sumups2;
    }

    /**
     * @param sumups2 the sumups2 to set
     */
    public void setSumups2(List<DSSResults> sumups2) {
        this.sumups2 = sumups2;
    }

    /**
     * @return the dateFromM
     */
    public Date getDateFromM() {
        return dateFromM;
    }

    /**
     * @param dateFromM the dateFromM to set
     */
    public void setDateFromM(Date dateFromM) {
        this.dateFromM = dateFromM;
    }

    /**
     * @return the dateToM
     */
    public Date getDateToM() {
        return dateToM;
    }

    /**
     * @param dateToM the dateToM to set
     */
    public void setDateToM(Date dateToM) {
        this.dateToM = dateToM;
    }

    /**
     * @return the preset
     */
    public String getPreset() {
        return preset;
    }

    /**
     * @param preset the preset to set
     */
    public void setPreset(String preset) {
        this.preset = preset;
    }

    private void LoadFromDB() {
        java.util.List<eu.nephron.opt.ws.Optionsall> popt = nephronOptionWSfindAll();
        pValuesDB.clear();
        for (int i = 0; i < popt.size(); i++) {
            if (popt.get(i).getOpttype().equalsIgnoreCase("alerts")) {
                pValuesDB.add(popt.get(i).getOptvalue());
            }
        }
    }

    private java.util.List<eu.nephron.opt.ws.Optionsall> nephronOptionWSfindAll() {
        eu.nephron.opt.ws.NephronOptionWS port = service_5.getNephronOptionWSPort();
        return port.nephronOptionWSfindAll();
    }
}
