/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.timeline;

import eu.nephron.applicationbean.BasicApplicationBean;
import eu.nephron.cache.alert.db.aledb;
import eu.nephron.cache.alerts.alertscached;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.hl7.cache.ws.Cacheconnection_Service;
import org.primefaces.event.SelectEvent;
import org.primefaces.extensions.model.timeline.DefaultTimeLine;
import org.primefaces.extensions.model.timeline.DefaultTimelineEvent;
import org.primefaces.extensions.model.timeline.TimelineEvent;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class TimeLine {

    private List<org.primefaces.extensions.model.timeline.Timeline> timelines;
    private TimelineEvent selectedEvent;
    private String eventStyle = "box";
    private String axisPosition = "bottom";
    private boolean showNavigation = true;
    private String previousid = "NA";
    private Cacheconnection_Service service_3 = new Cacheconnection_Service();
    java.util.List<org.hl7.cache.ws.Hl7Alerts> paler = new ArrayList();

    ///////////////////////////// LOGIC Functions ///////////////////////////////////////////////
    /**
     * Constructor
     */
    public TimeLine() {

        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
        long ifind = Long.valueOf(puserid);
        String imei = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        paler = findAlertsByDoctorAndDateFrom(ifind, imei);
        initialize_TimeLine(ifind);
    }

    public String loadTimeline() {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
        long ifind = Long.valueOf(puserid);
        String imei = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        paler = findAlertsByDoctorAndDateFrom(ifind, imei);
        initialize_TimeLine(ifind);
        return "DashBoard_Patient?faces-redirect=true";

    }

    public void initialize_TimeLine(Long ifind) {
        timelines = new ArrayList<org.primefaces.extensions.model.timeline.Timeline>();
        Calendar cal = Calendar.getInstance();
        Date ddate = new Date();
        cal.setTime(ddate);
        org.primefaces.extensions.model.timeline.Timeline timeline = new DefaultTimeLine("customEventTimeline", "Nephron Timeline");
        String IMEI = "";


        for (int i = 0; i < paler.size(); i++) {
            if (paler.get(i).getIdpatient().toString().equalsIgnoreCase(String.valueOf(ifind))) {
                String title = paler.get(i).getAlertmsg();
                IMEI = paler.get(i).getSzvalue();
                Date ddated = new Date();
                ddated.setTime(paler.get(i).getCDate());
                TimelineEvent timelineEvent = new DefaultTimelineEvent(title, ddated);
                timelineEvent.setStyleClass("alarm");
                timeline.addEvent(timelineEvent);
            }
        }


        timelines.add(timeline);
    }

    private java.util.List<org.hl7.cache.ws.Hl7Alerts> findAlertsByDoctorAndDateFrom(java.lang.Long patientID, String imei) {

        java.util.List<org.hl7.cache.ws.Hl7Alerts> pList = new ArrayList();
        try {
            aledb pdb = new aledb();
            pdb.SetIMEI(imei);
            pdb.LoadFromFile();
            ArrayList<alertscached> plist = pdb.getList();

            for (int ix = 0; ix < plist.size(); ix++) {

                org.hl7.cache.ws.Hl7Alerts aa = new org.hl7.cache.ws.Hl7Alerts();
                aa.setIddoctor(plist.get(ix).getIddoctor());
                aa.setIdpatient(plist.get(ix).getIdpatient());
                aa.setSzvalue(plist.get(ix).getSzvalue());
                aa.setCDate(plist.get(ix).getcDate());
                aa.setAlertmsg(plist.get(ix).getAlertmsg());
                aa.setAlertmsg1(plist.get(ix).getAlertmsg1());
                pList.add(aa);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BasicApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BasicApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BasicApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pList;
    }

    
    ///////////////////////////////// Web Service Calls ///////////////////////////////////////////////////////
    private java.util.List<org.hl7.cache.ws.Hl7Cacheconnectivity> findAllconnection() {
        org.hl7.cache.ws.Cacheconnection port = service_3.getCacheconnectionPort();
        return port.findAllconnection();
    }
    
    
    /////////////////// GETTERS/SETTERS/JSF Event Functions //////////////////////////////////////////////////////
    public void onEventSelect(SelectEvent event) {
        selectedEvent = (TimelineEvent) event.getObject();
    }

    /**
     * @return the timelines
     */
    public List<org.primefaces.extensions.model.timeline.Timeline> getTimelines() {
        return timelines;
    }

    /**
     * @param timelines the timelines to set
     */
    public void setTimelines(List<org.primefaces.extensions.model.timeline.Timeline> timelines) {
        this.timelines = timelines;
    }

    /**
     * @return the selectedEvent
     */
    public TimelineEvent getSelectedEvent() {
        return selectedEvent;
    }

    /**
     * @param selectedEvent the selectedEvent to set
     */
    public void setSelectedEvent(TimelineEvent selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    /**
     * @return the eventStyle
     */
    public String getEventStyle() {
        return eventStyle;
    }

    /**
     * @param eventStyle the eventStyle to set
     */
    public void setEventStyle(String eventStyle) {
        this.eventStyle = eventStyle;
    }

    /**
     * @return the axisPosition
     */
    public String getAxisPosition() {
        return axisPosition;
    }

    /**
     * @param axisPosition the axisPosition to set
     */
    public void setAxisPosition(String axisPosition) {
        this.axisPosition = axisPosition;
    }

    /**
     * @return the showNavigation
     */
    public boolean isShowNavigation() {
        return showNavigation;
    }

    /**
     * @param showNavigation the showNavigation to set
     */
    public void setShowNavigation(boolean showNavigation) {
        this.showNavigation = showNavigation;
    }

    /**
     * @return the previousid
     */
    public String getPreviousid() {
        return previousid;
    }

    /**
     * @param previousid the previousid to set
     */
    public void setPreviousid(String previousid) {
        this.previousid = previousid;
    }
}
