/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.newv.wakdmess;

import com.google.common.collect.Lists;
import eu.cache.database.CDWAKDMeasurments;
import eu.nephron.cache.measurments.mesdb;
import eu.nephron.mestypes.mestypes;
import eu.nephron.model.Sensors;
import eu.nephron.secure.ws.WAKDMeasurmentsManager_Service;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class wakdmess {

    private List<Sensors> sensors;
    private List<Sensors> sensorsOriginal;
    private Date dateFromM = new Date();
    private Date dateToM = new Date();
    private String number;
    private int brefresh;
    private String preset = "1";

    /**
     * Creates a new instance of wakdmess
     */
    public wakdmess() {


        sensors = new ArrayList<Sensors>();
        sensorsOriginal = new ArrayList<Sensors>();
    }

    public double ConvertValues(int itype, double dvalue) {
        double dnew;
        dnew = dvalue;
        if (itype == 12) {
            dnew = dvalue / 10;
        }
        if (itype == 11) {
            dnew = dvalue / 10;
        }
        if (itype == 13) {
            dnew = dvalue / 10;
        }
        if (itype == 15) {
            dnew = dvalue / 10;
        }
        if (itype == 17) {
            dnew = dvalue / 10;
        }
        if (itype == 18) {
            dnew = dvalue / 10;
        }
        if (itype == 5) {
            dnew = dvalue * 10;
        }
        return dnew;
    }

    public void UpdateRecords(ActionEvent actionEvent) {
        if (preset.equalsIgnoreCase("0")) {
            //daily
            setBrefresh(15);

            dateToM = new Date();

            Date pdate = new Date();
            Calendar pcal = new GregorianCalendar();
            pcal.setTime(pdate);
            pcal.add(Calendar.DATE, -1);
            dateFromM.setTime(pcal.getTime().getTime());
        }
        if (preset.equalsIgnoreCase("1")) {
            setBrefresh(6000);
            //weekly
            dateToM = new Date();
            Date pdate = new Date();
            Calendar pcal = new GregorianCalendar();
            pcal.setTime(pdate);
            pcal.add(Calendar.DATE, -7);
            dateFromM.setTime(pcal.getTime().getTime());

        }
 if (preset.equalsIgnoreCase("2")) {
            setBrefresh(6000);
 }
 sensors.clear();
            sensorsOriginal.clear();
        java.util.List<eu.nephron.secure.ws.WakdMeasurments> pReturn = findWAKDMeasurmentsManagerWithIMEIAndDateFromTo(dateFromM.getTime(), dateToM.getTime());
        if (pReturn == null) {
        } else {
            sensors.clear();
            sensorsOriginal.clear();
            mestypes ptypes = new mestypes();
            for (int i = 0; i < pReturn.size(); i++) {
                Sensors psensor = new Sensors();
                Date dd = new Date();
                dd.setTime(pReturn.get(i).getDate());
                psensor.setTime(dd);
                psensor.setType(pReturn.get(i).getType().toString());
                String typecomp = String.valueOf(ptypes.GetType(number));
                String typecomp1 = pReturn.get(i).getType().toString();

                double dValue = ConvertValues(pReturn.get(i).getType().intValue(), pReturn.get(i).getValue());
                //psensor.setValue(pReturn.get(i).getValue());
                psensor.setValue(dValue);
                if (typecomp1.equalsIgnoreCase(typecomp)) {
                    sensors.add(psensor);
                    sensorsOriginal.add(psensor);
                } else if (number.equalsIgnoreCase("All")) {
                    typecomp = "11";//14,16
                    if (typecomp1.equalsIgnoreCase(typecomp)) {
                    } else {
                        typecomp = "14";
                        if (typecomp1.equalsIgnoreCase(typecomp)) {
                        } else {
                            typecomp = "16";
                            if (typecomp1.equalsIgnoreCase(typecomp)) {
                            } else {
                                typecomp = "10";
                                if (typecomp1.equalsIgnoreCase(typecomp)) {
                                } else {
                                    sensors.add(psensor);
                                    sensorsOriginal.add(psensor);
                                }
                            }
                        }

                    }
                }
            }
        sensors = reverseList(sensors);
        }
        
    }

    public void increment() {
        sensors.clear();
        sensorsOriginal.clear();
        //setBrefresh(6000);
        java.util.List<eu.nephron.secure.ws.WakdMeasurments> pReturn = findWAKDMeasurmentsManagerWithIMEIAndDateFromTo(dateFromM.getTime(), dateToM.getTime());
        if (pReturn == null) {
        } else {
            sensors.clear();
            sensorsOriginal.clear();
            mestypes ptypes = new mestypes();
            for (int i = 0; i < pReturn.size(); i++) {
                Sensors psensor = new Sensors();
                Date dd = new Date();
                dd.setTime(pReturn.get(i).getDate());
                psensor.setTime(dd);
                psensor.setType(pReturn.get(i).getType().toString());
                String typecomp = String.valueOf(ptypes.GetType(number));
                String typecomp1 = pReturn.get(i).getType().toString();

                double dValue = ConvertValues(pReturn.get(i).getType().intValue(), pReturn.get(i).getValue());
                //psensor.setValue(pReturn.get(i).getValue());
                psensor.setValue(dValue);
                if (typecomp1.equalsIgnoreCase(typecomp)) {
                    sensors.add(psensor);
                    sensorsOriginal.add(psensor);
                } else if (number.equalsIgnoreCase("All")) {
                    typecomp = "11";//14,16
                    if (typecomp1.equalsIgnoreCase(typecomp)) {
                    } else {
                        typecomp = "14";
                        if (typecomp1.equalsIgnoreCase(typecomp)) {
                        } else {
                            typecomp = "16";
                            if (typecomp1.equalsIgnoreCase(typecomp)) {
                            } else {
                                typecomp = "10";
                                if (typecomp1.equalsIgnoreCase(typecomp)) {
                                } else {
                                    sensors.add(psensor);
                                    sensorsOriginal.add(psensor);
                                }
                            }
                        }

                    }
                }
            }
        sensors = reverseList(sensors);
        }
        
//        Date pdate = new Date();
//        Calendar pcal = new GregorianCalendar();
//        pcal.setTime(pdate);
//        pcal.add(Calendar.MINUTE, -1);
//        Date pdate2 = new Date();
//        java.util.List<eu.nephron.secure.ws.WakdMeasurments> pReturn = findWAKDMeasurmentsManagerWithIMEIAndDateFromTo(pcal.getTime().getTime(), pdate2.getTime());
//        //sensors.clear();
//        //sensorsOriginal.clear();
//        if (pReturn == null) {
//        } else {
//            mestypes ptypes = new mestypes();
//            for (int i = 0; i < pReturn.size(); i++) {
//                Sensors psensor = new Sensors();
//                Date dd = new Date();
//                dd.setTime(pReturn.get(i).getDate());
//                psensor.setTime(dd);
//                psensor.setType(pReturn.get(i).getType().toString());
//                String typecomp = String.valueOf(ptypes.GetType(number));
//                String typecomp1 = pReturn.get(i).getType().toString();
//
//
//                //psensor.setValue(pReturn.get(i).getValue());
//                double dValue = ConvertValues(pReturn.get(i).getType().intValue(), pReturn.get(i).getValue());
//                //psensor.setValue(pReturn.get(i).getValue());
//                psensor.setValue(dValue);
//                if (typecomp1.equalsIgnoreCase(typecomp)) {
//                    sensors.add(psensor);
//                    sensorsOriginal.add(psensor);
//                } else if (number.equalsIgnoreCase("All")) {
//                    sensors.add(psensor);
//                    sensorsOriginal.add(psensor);
//                }
//            }
//        }
    }

    public String loadmeasurments() {
        setBrefresh(6000);
        sensors.clear();
        sensorsOriginal.clear();
        Date pdate = new Date();
        Calendar pcal = new GregorianCalendar();
        pcal.setTime(pdate);
        pcal.add(Calendar.DATE, -7);
        dateFromM.setTime(pcal.getTime().getTime());
        dateToM = new Date();
        preset = "1";
        number = "All";
        java.util.List<eu.nephron.secure.ws.WakdMeasurments> pReturn = findWAKDMeasurmentsManagerWithIMEIAndDateFrom(pcal.getTime().getTime());
        sensors.clear();
        sensorsOriginal.clear();
        if (pReturn == null) {
        } else {
            for (int i = 0; i < pReturn.size(); i++) {
                Sensors psensor = new Sensors();
                Date dd = new Date();
                dd.setTime(pReturn.get(i).getDate());
                psensor.setTime(dd);
                psensor.setType(pReturn.get(i).getType().toString());
                double dValue = ConvertValues(pReturn.get(i).getType().intValue(), pReturn.get(i).getValue());
                psensor.setValue(dValue);

                if (psensor.getTypename().equalsIgnoreCase("Outlet Uria")) {
                } else {
                    if (psensor.getTypename().equalsIgnoreCase("Pressure BCO")) {
                    } else {
                        if (psensor.getTypename().equalsIgnoreCase("Pressure FCO")) {
                        }else{sensors.add(psensor);
                sensorsOriginal.add(psensor);}
                    }
                }
                
            }
        }
        sensors = reverseList(sensors);
        return "Measurments_1?faces-redirect=true";
    }

    private java.util.List<eu.nephron.secure.ws.WakdMeasurments> findWAKDMeasurmentsManagerWithIMEIAndDateFrom(java.lang.Long arg1) {
        try {
            String imei = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
            System.out.println("Load Measurments IMEI " + imei);
            mesdb dd = new mesdb();
            java.util.List<eu.nephron.secure.ws.WakdMeasurments> pp = new ArrayList();
            System.out.println("Load Measurments set IMEI " + imei);
            dd.SetIMEI(imei);
            System.out.println("Load Measurments from file start IMEI " + imei);
            dd.LoadFromFile();
            System.out.println("Load Measurments from file end IMEI " + imei);
            ArrayList<CDWAKDMeasurments> pl = dd.getList();
            System.out.println("Load Measurments Size : " + pl.size());
            for (int i = 0; i < pl.size(); i++) {
                if (pl.get(i).getDate() >= arg1) {
                    eu.nephron.secure.ws.WakdMeasurments pitem = new eu.nephron.secure.ws.WakdMeasurments();
                    pitem.setDate(pl.get(i).getDate());
                    pitem.setIMEI(pl.get(i).getIMEI());
                    pitem.setId(pl.get(i).getId());
                    pitem.setType(pl.get(i).getType());
                    pitem.setValue(pl.get(i).getValue());
                    pp.add(pitem);
                }
            }
            System.out.println("Measurment Size After Filter : " + pp.size());
            if(pp.isEmpty())
            {
                WAKDMeasurmentsManager_Service service = new WAKDMeasurmentsManager_Service();
                        eu.nephron.secure.ws.WAKDMeasurmentsManager port = service.getWAKDMeasurmentsManagerPort();
                        return port.findWAKDMeasurmentsManagerWithIMEIAndDateFrom(imei, arg1);
            }
            return pp;
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        } catch (ClassNotFoundException ex) {
            return null;
        }
    }

    private java.util.List<eu.nephron.secure.ws.WakdMeasurments> findWAKDMeasurmentsManagerWithIMEIAndDateFromTo(java.lang.Long arg1, java.lang.Long arg2) {

        String imei = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");



        try {

            mesdb dd = new mesdb();
            java.util.List<eu.nephron.secure.ws.WakdMeasurments> pp = new ArrayList();
            dd.SetIMEI(imei);
            dd.LoadFromFile();
            ArrayList<CDWAKDMeasurments> pl = dd.getList();
            System.out.println("Measurment Size : " + pl.size());
            for (int i = 0; i < pl.size(); i++) {
                if (pl.get(i).getDate() >= arg1) {
                    if (pl.get(i).getDate() <= arg2) {
                        eu.nephron.secure.ws.WakdMeasurments pitem = new eu.nephron.secure.ws.WakdMeasurments();
                        pitem.setDate(pl.get(i).getDate());
                        pitem.setIMEI(pl.get(i).getIMEI());
                        pitem.setId(pl.get(i).getId());
                        pitem.setType(pl.get(i).getType());
                        pitem.setValue(pl.get(i).getValue());
                        pp.add(pitem);
                    }
                }
            }
            System.out.println("Measurment Size After Filter : " + pp.size());
            if(pp.isEmpty())
            {
                        WAKDMeasurmentsManager_Service service = new WAKDMeasurmentsManager_Service();
                        eu.nephron.secure.ws.WAKDMeasurmentsManager port = service.getWAKDMeasurmentsManagerPort();
                        return port.findWAKDMeasurmentsManagerWithIMEIAndDateFromTo(imei, arg1, arg2);
            }
            return pp;
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        } catch (ClassNotFoundException ex) {
            return null;
        }
    }

    private List reverseList(List myList) {
        List invertedList = new ArrayList();
        for (int i = myList.size() - 1; i >= 0; i--) {
            invertedList.add(myList.get(i));
        }
        return invertedList;
    }

    /**
     * @return the sensors
     */
    public List<Sensors> getSensors() {
//        Collections.reverse(sensors);
//        sensors=Lists.reverse(sensors);
        return sensors;
    }

    /**
     * @param sensors the sensors to set
     */
    public void setSensors(List<Sensors> sensors) {
        this.sensors = sensors;
    }

    /**
     * @return the sensorsOriginal
     */
    public List<Sensors> getSensorsOriginal() {
        return sensorsOriginal;
    }

    /**
     * @param sensorsOriginal the sensorsOriginal to set
     */
    public void setSensorsOriginal(List<Sensors> sensorsOriginal) {
        this.sensorsOriginal = sensorsOriginal;
    }

    /**
     * @return the dateFromM
     */
    public Date getDateFromM() {
        return dateFromM;
    }

    /**
     * @param dateFromM the dateFromM to set
     */
    public void setDateFromM(Date dateFromM) {
        this.dateFromM = dateFromM;
    }

    /**
     * @return the dateToM
     */
    public Date getDateToM() {
        return dateToM;
    }

    /**
     * @param dateToM the dateToM to set
     */
    public void setDateToM(Date dateToM) {
        this.dateToM = dateToM;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return the brefresh
     */
    public int getBrefresh() {
        return brefresh;
    }

    /**
     * @param brefresh the brefresh to set
     */
    public void setBrefresh(int brefresh) {
        this.brefresh = brefresh;
    }

    /**
     * @return the preset
     */
    public String getPreset() {
        return preset;
    }

    /**
     * @param preset the preset to set
     */
    public void setPreset(String preset) {
        this.preset = preset;
    }
}
