/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakdstatus;

import eu.nephron.commandmaker.WAKDCommandmaker;
import eu.nephron.secure.ws.WAKDMeasurmentsManager_Service;
//import eu.nephron.helper.entityhelper;
import eu.nephron.spcm.webservices.BackEndCommunicationManager_Service;
import eu.nephron.wakd.api.incoming.CurrentStateMsg;
import eu.nephron.weightdatavalues.ws.WeightDataValuesWS_Service;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim 18
 */
@ManagedBean
@RequestScoped
public class wakdstatusbean {

    private WAKDMeasurmentsManager_Service service_1 = new WAKDMeasurmentsManager_Service();
    private WeightDataValuesWS_Service service = new WeightDataValuesWS_Service();
    private String outcomestatus;
    private String batterystatus;
    private String currentstatus;
    private String opstatus;
    private String temperature;
    private String scalebattery;
    private String wakdweight;

    ////////////// LOGIC Functions /////////////////////////////////////////////////
    public static byte[] convertStringToByteArray(String s) {

        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public void increment() {

        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        System.out.println("Status for : "+PhoneIMEI);
        String wakdstring = retrieveWAKDStatus(PhoneIMEI);
        if (wakdstring.equalsIgnoreCase("-1")) {
        } else {
            byte[] pmsg2 = convertStringToByteArray(wakdstring);
            CurrentStateMsg pMsg = new CurrentStateMsg();
            pMsg.decode(pmsg2);
            opstatus = pMsg.getCurrentOpState().toString();
            currentstatus = pMsg.getCurrentState().toString();
        }
        wakdweight = FindLastWeight(PhoneIMEI);
    }

    /**
     * Creates a new instance of wakdstatusbean
     */
    public wakdstatusbean() {


        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");

        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date today7 = cal.getTime();
        Long DateFrom = today7.getTime();

        java.util.List<eu.nephron.secure.ws.WakdMeasurments> pp = findWAKDMeasurmentsManagerWithIMEIAndTypeAmdDateFrom(PhoneIMEI, new Long(18), DateFrom);

        if (pp.size() > 0) {
            temperature = String.valueOf(pp.get(0).getValue());
        } else {
            temperature = "0";
        }

        String wakdstring = retrieveWAKDStatus(PhoneIMEI);
        if (wakdstring.equalsIgnoreCase("-1")) {
        } else {
            byte[] pmsg2 = convertStringToByteArray(wakdstring);
            CurrentStateMsg pMsg = new CurrentStateMsg();
            pMsg.decode(pmsg2);
            opstatus = pMsg.getCurrentOpState().toString();
            currentstatus = pMsg.getCurrentState().toString();

        }
        wakdweight = FindLastWeight(PhoneIMEI);
    }

    private String retrieveWAKDStatus(java.lang.String imei) {
        BackEndCommunicationManager_Service service = new BackEndCommunicationManager_Service();
        eu.nephron.spcm.webservices.BackEndCommunicationManager port = service.getBackEndCommunicationManagerPort();
        return port.retrieveWAKDStatus(imei);
    }

    public void shutdownWAKDListener(ActionEvent actionEvent) {
        System.err.println("***************************** WAKD Shutdown Called ************************************");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Please Wait", "Shuting Down WAKD"));
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        Date ddate = new Date();
        WAKDCommandmaker pCommander = new WAKDCommandmaker();
        storePhoneCommand(PhoneIMEI, pCommander.MakeShutdown(), ddate.getTime(), ".", ".", ".", ".");
    }

    public String shutdown() {
        System.err.println("***************************** WAKD Shutdown Called ************************************");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Please Wait", "Shuting Down WAKD"));
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        Date ddate = new Date();
        WAKDCommandmaker pCommander = new WAKDCommandmaker();
        storePhoneCommand(PhoneIMEI, pCommander.MakeShutdown(), ddate.getTime(), ".", ".", ".", ".");
        return "WAKDStatus?faces-redirect=true";
    }

    private void storePhoneCommand(java.lang.String imei, java.lang.String command, java.lang.Long ddate, java.lang.String param1, java.lang.String param2, java.lang.String param3, java.lang.String param4) {
        System.err.println("Store WAKD Shutdown Called");
        BackEndCommunicationManager_Service service = new BackEndCommunicationManager_Service();
        eu.nephron.spcm.webservices.BackEndCommunicationManager port = service.getBackEndCommunicationManagerPort();
        port.storePhoneCommand(imei, command, ddate, param1, param2, param3, param4);
        System.err.println("Store WAKD Shutdown Finished");
    }

    public String FindLastWeight(String imei) {
        String value = "0";
        String weight = "0";
        Long itemp = new Long(0);
        java.util.List<eu.nephron.weightdatavalues.ws.Weightdatavalues> pweight = findWAKDMeasurmentsManagerWithIMEI(imei);
        System.out.println("Weight Measurments size : "+pweight.size());
        for (int i = 0; i < pweight.size(); i++) {
            eu.nephron.weightdatavalues.ws.Weightdatavalues pi = pweight.get(i);
            Long itime = pi.getWtime();
            weight = pi.getWeight();
            System.out.println(itime + " " + weight);
            if (itemp > itime) {
                itemp = itime;
                value = weight;
            } else {
                itemp = itime;
            }
        }
        if (value.length() < 1) {
            return weight;
        }
        return value;
    }

    /////////////////////////// WS CALLS ///////////////////////////////////////////////
    private java.util.List<eu.nephron.weightdatavalues.ws.Weightdatavalues> findWAKDMeasurmentsManagerWithIMEI(java.lang.String imei) {
        eu.nephron.weightdatavalues.ws.WeightDataValuesWS port = service.getWeightDataValuesWSPort();
        return port.findWAKDMeasurmentsManagerWithIMEI(imei);
    }

    private java.util.List<eu.nephron.secure.ws.WakdMeasurments> returnAllMeasurmentsForLastDayAndType(java.lang.Long arg0) {
        eu.nephron.secure.ws.WAKDMeasurmentsManager port = service_1.getWAKDMeasurmentsManagerPort();
        return port.returnAllMeasurmentsForLastDayAndType(arg0);
    }

    private java.util.List<eu.nephron.secure.ws.WakdMeasurments> findWAKDMeasurmentsManagerWithIMEIAndTypeAmdDateFrom(java.lang.String imei, java.lang.Long arg1, java.lang.Long arg2) {
        eu.nephron.secure.ws.WAKDMeasurmentsManager port = service_1.getWAKDMeasurmentsManagerPort();
        return port.findWAKDMeasurmentsManagerWithIMEIAndTypeAmdDateFrom(imei, arg1, arg2);
    }

    /////////////////////////////////////// GETTERS / SETTERS /////////////////////////////////////////////////
    /**
     * @return the wakdweight
     */
    public String getWakdweight() {
        return wakdweight;
    }

    /**
     * @param wakdweight the wakdweight to set
     */
    public void setWakdweight(String wakdweight) {
        this.wakdweight = wakdweight;
    }

    /**
     * @return the outcomestatus
     */
    public String getOutcomestatus() {
        return "20Kg";//outcomestatus;
    }

    /**
     * @param outcomestatus the outcomestatus to set
     */
    public void setOutcomestatus(String outcomestatus) {
        this.outcomestatus = outcomestatus;
    }

    /**
     * @return the batterystatus
     */
    public String getBatterystatus() {
        return "N/A";//batterystatus;
    }

    /**
     * @param batterystatus the batterystatus to set
     */
    public void setBatterystatus(String batterystatus) {
        this.batterystatus = batterystatus;
    }

    /**
     * @return the currentstatus
     */
    public String getCurrentstatus() {
        return currentstatus;
    }

    /**
     * @param currentstatus the currentstatus to set
     */
    public void setCurrentstatus(String currentstatus) {
        this.currentstatus = currentstatus;
    }

    /**
     * @return the opstatus
     */
    public String getOpstatus() {
        return opstatus;
    }

    /**
     * @param opstatus the opstatus to set
     */
    public void setOpstatus(String opstatus) {
        this.opstatus = opstatus;
    }

    /**
     * @return the temperature
     */
    public String getTemperature() {
        Double ltemp=Double.valueOf(temperature);
        ltemp=ltemp/10;
        //return temperature;//
        return String.valueOf(ltemp);
    }

    /**
     * @param temperature the temperature to set
     */
    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    /**
     * @return the scalebattery
     */
    public String getScalebattery() {
        return scalebattery;
    }

    /**
     * @param scalebattery the scalebattery to set
     */
    public void setScalebattery(String scalebattery) {
        this.scalebattery = scalebattery;
    }
}
