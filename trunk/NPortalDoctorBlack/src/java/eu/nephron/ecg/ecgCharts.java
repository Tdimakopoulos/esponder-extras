/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ecg;

import eu.nephron.filerepo.ws.FileNotFoundException_Exception;
import eu.nephron.filerepo.ws.FileRepoWS_Service;
import eu.nephron.filerepo.ws.IOException_Exception;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class ecgCharts {

    private CartesianChartModel chartECG;
    List<eu.nephron.filerepo.ws.FileRepository> pMyFiles = new ArrayList();
    List<ECGDataModel> pModel = new ArrayList();
    private Map<String, String> datesandecgs = new HashMap<String, String>();
    String datestoshow;
private boolean render;

    public void RefreshECGList(ActionEvent actionEvent)
    {
        
        datesandecgs.clear();
        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        pMyFiles = findAll();
        for (int i = 0; i < pMyFiles.size(); i++) {
            if (pMyFiles.get(i).getImei().equalsIgnoreCase(PhoneIMEI)) {
                
            } else {
                pMyFiles.remove(i);
            }
        }
        
        for (int i = 0; i < pMyFiles.size(); i++) {
            ECGDataModel pentry = new ECGDataModel();
            Date dddate = new Date();
            dddate.setTime(pMyFiles.get(i).getDdate());
            pentry.setSdate(dddate.toString());
            pentry.setsFilename(pMyFiles.get(i).getFilename());
            File f = new File("/home/exodus/nephron/wsfile/" + pMyFiles.get(i).getFilename());
            if (f.exists()) {
                datesandecgs.put(dddate.toString(), pMyFiles.get(i).getFilename());
            }
        }
    }
    public void increment() {  
        LoadECGList();
    }
    
    public String LoadECGList()
    {
        setRender(false);
        datesandecgs.clear();
        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        pMyFiles = findAll();
        for (int i = 0; i < pMyFiles.size(); i++) {
            Date pdate;
            pdate = new Date();
            pdate.setTime(pMyFiles.get(i).getDdate());
            System.out.println(pdate+" --- "+pMyFiles.get(i).getFilename()+" -- ID --"+pMyFiles.get(i).getId());
            if (pMyFiles.get(i).getImei().equalsIgnoreCase(PhoneIMEI)) {
            } else {
                pMyFiles.remove(i);
            }
        }
        
        for (int i = 0; i < pMyFiles.size(); i++) {
            ECGDataModel pentry = new ECGDataModel();
            Date dddate = new Date();
            dddate.setTime(pMyFiles.get(i).getDdate());
            pentry.setSdate(dddate.toString());
            pentry.setsFilename(pMyFiles.get(i).getFilename());
            File f = new File("/home/exodus/nephron/wsfile/" + pMyFiles.get(i).getFilename());
            if (f.exists()) {
                datesandecgs.put(dddate.toString(), pMyFiles.get(i).getFilename());
                
            }
        }
        return "Ecg?faces-redirect=true";
    }
            
    public void UpdateRecords(ActionEvent actionEvent) {
        setRender(true);
        chartECG.clear();
        String dfile = datesandecgs.get(datestoshow);
        try {
            java.util.List<eu.nephron.filerepo.ws.EcgModel> pCharts = loadFile(dfile, "$");
            LineChartSeries lpWeight = new LineChartSeries();


            lpWeight.setLabel("ECG");
            for (int i = 0; i < pCharts.size(); i++) {
                lpWeight.set(i, pCharts.get(i).getDvalue());
            }
            chartECG.addSeries(lpWeight);
        } catch (FileNotFoundException_Exception ex) {
            Logger.getLogger(ecgCharts.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException_Exception ex) {
            Logger.getLogger(ecgCharts.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Creates a new instance of ecgCharts
     */
    public ecgCharts() {
        chartECG = new CartesianChartModel();
        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        pMyFiles = findAll();
        for (int i = 0; i < pMyFiles.size(); i++) {
            if (pMyFiles.get(i).getImei().equalsIgnoreCase(PhoneIMEI)) {
            } else {
                pMyFiles.remove(i);
            }
        }
        LineChartSeries lpWeight = new LineChartSeries();


        lpWeight.setLabel("ECG");
        lpWeight.set(0, 0);

        chartECG.addSeries(lpWeight);
        for (int i = 0; i < pMyFiles.size(); i++) {
            ECGDataModel pentry = new ECGDataModel();
            Date dddate = new Date();
            dddate.setTime(pMyFiles.get(i).getDdate());
            pentry.setSdate(dddate.toString());
            pentry.setsFilename(pMyFiles.get(i).getFilename());
            File f = new File("/home/exodus/nephron/wsfile/" + pMyFiles.get(i).getFilename());
            if (f.exists()) {
                datesandecgs.put(dddate.toString(), pMyFiles.get(i).getFilename());
            }
        }
    }

    private java.util.List<eu.nephron.filerepo.ws.EcgModel> loadFile(java.lang.String filename, java.lang.String sep) throws FileNotFoundException_Exception, IOException_Exception {
        FileRepoWS_Service service = new FileRepoWS_Service();
        eu.nephron.filerepo.ws.FileRepoWS port = service.getFileRepoWSPort();
        return port.loadFile(filename, sep);
    }

    private java.util.List<eu.nephron.filerepo.ws.FileRepository> findAll() {
        FileRepoWS_Service service = new FileRepoWS_Service();
        eu.nephron.filerepo.ws.FileRepoWS port = service.getFileRepoWSPort();
        return port.findAll();
    }

    /**
     * @return the chartECG
     */
    public CartesianChartModel getChartECG() {
        return chartECG;
    }

    /**
     * @param chartECG the chartECG to set
     */
    public void setChartECG(CartesianChartModel chartECG) {
        this.chartECG = chartECG;
    }

    /**
     * @return the pMyFiles
     */
    public List<eu.nephron.filerepo.ws.FileRepository> getpMyFiles() {
        return pMyFiles;
    }

    /**
     * @param pMyFiles the pMyFiles to set
     */
    public void setpMyFiles(List<eu.nephron.filerepo.ws.FileRepository> pMyFiles) {
        this.pMyFiles = pMyFiles;
    }

    /**
     * @return the pModel
     */
    public List<ECGDataModel> getpModel() {
        return pModel;
    }

    /**
     * @param pModel the pModel to set
     */
    public void setpModel(List<ECGDataModel> pModel) {
        this.pModel = pModel;
    }

    /**
     * @return the datesandecgs
     */
    public Map<String, String> getDatesandecgs() {
        return datesandecgs;
    }

    /**
     * @param datesandecgs the datesandecgs to set
     */
    public void setDatesandecgs(Map<String, String> datesandecgs) {
        this.datesandecgs = datesandecgs;
    }

    /**
     * @return the datestoshow
     */
    public String getDatestoshow() {
        return datestoshow;
    }

    /**
     * @param datestoshow the datestoshow to set
     */
    public void setDatestoshow(String datestoshow) {
        this.datestoshow = datestoshow;
    }

    /**
     * @return the render
     */
    public boolean isRender() {
        return render;
    }

    /**
     * @param render the render to set
     */
    public void setRender(boolean render) {
        this.render = render;
    }
}
