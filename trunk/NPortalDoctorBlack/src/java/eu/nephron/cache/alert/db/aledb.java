/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.cache.alert.db;



import eu.nephron.cache.alerts.alertscached;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MEOC-WS1
 */
public class aledb {

    ArrayList<alertscached> pdb = new ArrayList<alertscached>();
    
    int ipos;
    String imei;
    
    public void SetIMEI(String szimei)
    {
        imei=szimei;
    }
    
    public ArrayList<alertscached> getList()
    {
        return pdb;
    }
    
   public void SaveOnFile() throws FileNotFoundException, IOException {
        
        String szPath;
		if (File.separatorChar == '/')
			szPath="/home/exodus/osgi/";
		else
			szPath="c:\\imei\\";
                
        FileOutputStream fos = new FileOutputStream(szPath+imei+"a");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(pdb);
        oos.close();
    }

    public void LoadFromFile() throws FileNotFoundException, IOException, ClassNotFoundException {
        
        String szPath;
		if (File.separatorChar == '/')
			szPath="/home/exodus/osgi/";
		else
			szPath="c:\\imei\\";
                
        pdb.clear();
        
        File f = new File(szPath+imei+"a");
        if(f.exists()) 
        { 
        FileInputStream fis = new FileInputStream(szPath+imei+"a");
        ObjectInputStream ois = new ObjectInputStream(fis);
        pdb = (ArrayList<alertscached>) (List<alertscached>) ois.readObject();
        ois.close();
        ipos=pdb.size();
        }else
        {
        ipos=0;
        }
    }

    
    public alertscached getItem(int indexpos) {
        return pdb.get(indexpos);
    }

    public int getpos() {
        return ipos;
    }

    public void Add(alertscached pitem) {
        ipos = ipos + 1;
       
        pdb.add(pitem);
    }

    
}
