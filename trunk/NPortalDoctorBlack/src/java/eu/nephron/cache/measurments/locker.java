/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.cache.measurments;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tdim
 */
public class locker {

    String imei;
    String pathw = "/home/exodus/osgi/";
    String path = "c:\\dd\\";

    public void SetIMEI(String dd) {
        imei = dd;
    }

    public void waitforfree() {

        File f = new File(pathw + imei + ".loc");
        while (f.exists()) {
            System.out.println("File is locked");
            f = new File(pathw + imei + ".loc");
            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
            }
        }
        System.out.println("File is Free");


    }

    public void createlock() {
        try {
            System.out.println("Make Lock !");
            File yourFile = new File(pathw + imei + ".loc");

            yourFile.createNewFile();

            FileOutputStream oFile = new FileOutputStream(yourFile, false);
            oFile.close();

        } catch (IOException ex) {
            Logger.getLogger(locker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deletelock() {
        try {

            File file = new File(pathw + imei + ".loc");

            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    public static void main(String[] args) {
        locker pp = new locker();
        pp.SetIMEI("test");
        pp.deletelock();
        pp.waitforfree();
        pp.createlock();
        pp.waitforfree();
    }
}
