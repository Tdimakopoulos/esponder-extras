/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tdim
 */
public class DSSResults implements Serializable {

    private Long id;
    private String rulename;
    private String ruleresults;
    private String ruleresults1;
    private String ruleresults2;
    private String ruleresults3;
    private int iresult1;
    private int iresult2;
    private int iresult3;
    private boolean bresult1;
    private boolean bresult2;
    private boolean bresult3;
    private String stylest;
    
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the rulename
     */
    public String getRulename() {
        return rulename;
    }

    /**
     * @param rulename the rulename to set
     */
    public void setRulename(String rulename) {
        this.rulename = rulename;
    }

    /**
     * @return the ruleresults
     */
    public String getRuleresults() {
        return ruleresults;
    }

    /**
     * @param ruleresults the ruleresults to set
     */
    public void setRuleresults(String ruleresults) {
        this.ruleresults = ruleresults;
    }

    /**
     * @return the ruleresults1
     */
    public String getRuleresults1() {
        return ruleresults1;
    }

    /**
     * @param ruleresults1 the ruleresults1 to set
     */
    public void setRuleresults1(String ruleresults1) {
        this.ruleresults1 = ruleresults1;
    }

    /**
     * @return the ruleresults2
     */
    public String getRuleresults2() {
        return ruleresults2;
    }

    /**
     * @param ruleresults2 the ruleresults2 to set
     */
    public void setRuleresults2(String ruleresults2) {
        this.ruleresults2 = ruleresults2;
    }

    /**
     * @return the ruleresults3
     */
    public String getRuleresults3() {
        return ruleresults3;
    }

    /**
     * @param ruleresults3 the ruleresults3 to set
     */
    public void setRuleresults3(String ruleresults3) {
        this.ruleresults3 = ruleresults3;
    }

    /**
     * @return the iresult1
     */
    public int getIresult1() {
        return iresult1;
    }

    /**
     * @param iresult1 the iresult1 to set
     */
    public void setIresult1(int iresult1) {
        this.iresult1 = iresult1;
    }

    /**
     * @return the iresult2
     */
    public int getIresult2() {
        return iresult2;
    }

    /**
     * @param iresult2 the iresult2 to set
     */
    public void setIresult2(int iresult2) {
        this.iresult2 = iresult2;
    }

    /**
     * @return the iresult3
     */
    public int getIresult3() {
        return iresult3;
    }

    /**
     * @param iresult3 the iresult3 to set
     */
    public void setIresult3(int iresult3) {
        this.iresult3 = iresult3;
    }

    /**
     * @return the bresult1
     */
    public boolean isBresult1() {
        return bresult1;
    }

    /**
     * @param bresult1 the bresult1 to set
     */
    public void setBresult1(boolean bresult1) {
        this.bresult1 = bresult1;
    }

    /**
     * @return the bresult2
     */
    public boolean isBresult2() {
        return bresult2;
    }

    /**
     * @param bresult2 the bresult2 to set
     */
    public void setBresult2(boolean bresult2) {
        this.bresult2 = bresult2;
    }

    /**
     * @return the bresult3
     */
    public boolean isBresult3() {
        return bresult3;
    }

    /**
     * @param bresult3 the bresult3 to set
     */
    public void setBresult3(boolean bresult3) {
        this.bresult3 = bresult3;
    }

    /**
     * @return the stylest
     */
    public String getStylest() {
        return stylest;
    }

    /**
     * @param stylest the stylest to set
     */
    public void setStylest(String stylest) {
        this.stylest = stylest;
    }
}
