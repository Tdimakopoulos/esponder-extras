/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.session;

import eu.nephron.credentials.ws.CredentialsWS_Service;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.print.attribute.standard.Severity;
import javax.xml.ws.WebServiceRef;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Thomas
 */
@ManagedBean
@SessionScoped
public class LoginManager {

    private UIComponent mybutton;
    private String username;
    private String password;
    private String userid;
    private int irole;
    private boolean loginok = false;
    private boolean loginerror = true;
private boolean bfail=false;
    /**
     * Creates a new instance of LoginManager
     */
    public LoginManager() {
    }

    ////////////////////////// GETTERS / SETTERS //////////////////////////////
    /**
     * @return the username
     */
    public String getUsername() {

        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the userid
     */
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid the userid to set
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return the irole
     */
    public int getIrole() {
        return irole;
    }

    /**
     * @param irole the irole to set
     */
    public void setIrole(int irole) {
        this.irole = irole;
    }

    /**
     * @return the loginok
     */
    public boolean isLoginok() {
        return loginok;
    }

    /**
     * @param loginok the loginok to set
     */
    public void setLoginok(boolean loginok) {
        this.loginok = loginok;
    }

    /**
     * @return the loginerror
     */
    public boolean isLoginerror() {
        return loginerror;
    }

    /**
     * @param loginerror the loginerror to set
     */
    public void setLoginerror(boolean loginerror) {
        this.loginerror = loginerror;
    }

    /**
     * @return the mybutton
     */
    public UIComponent getMybutton() {
        return mybutton;
    }

    /**
     * @param mybutton the mybutton to set
     */
    public void setMybutton(UIComponent mybutton) {
        this.mybutton = mybutton;
    }

    
    ///////////////////// LOGIC FUNCTIONS /////////////////////////////////////////
    
    public String loginack() {
        System.out.println("Login");
        
         FacesContext context2 = FacesContext.getCurrentInstance();
         
        
        context2.addMessage(null, new FacesMessage("Second Message", "Additional Message Detail"));
   
            
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Please Wait", "Checking Username and Password"));
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage msg = null;
        String puserid = "-1";
        java.util.List<eu.nephron.credentials.ws.Credentialsdb> pfind = findAll();
        System.out.println("Login loop : " + pfind.size());
        for (int i = 0; i < pfind.size(); i++) {
            if (pfind.get(i).getUsername().equalsIgnoreCase(username)) {
                if (pfind.get(i).getPassword().equalsIgnoreCase(password)) {
                    puserid = String.valueOf(pfind.get(i).getDoctorid());
                }
            }

        }
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("DUserID", puserid);
        if (puserid.equalsIgnoreCase("-1")) {
            username = "";
            password = "";
            loginok = false;
            loginerror = true;
            bfail=true;
            return "index?faces-redirect=true";

        } else {

            loginok = true;
            loginerror = false;
            bfail=false;
            return "DashBoard?faces-redirect=true";
        }
    }
    
    public void login() {
        System.out.println("Login");
         FacesContext context2 = FacesContext.getCurrentInstance();
         
        
        context2.addMessage(null, new FacesMessage("Second Message", "Additional Message Detail"));
   
            
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Please Wait", "Checking Username and Password"));
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage msg = null;
        String puserid = "-1";
        java.util.List<eu.nephron.credentials.ws.Credentialsdb> pfind = findAll();
        System.out.println("Login loop : " + pfind.size());
        for (int i = 0; i < pfind.size(); i++) {
            if (pfind.get(i).getUsername().equalsIgnoreCase(username)) {
                if (pfind.get(i).getPassword().equalsIgnoreCase(password)) {
                    puserid = String.valueOf(pfind.get(i).getDoctorid());
                }
            }

        }
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("DUserID", puserid);
        
    }

    public String logout() {
        username = "";
        password = "";
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        loginok = false;
        loginerror = true;
        return "index?faces-redirect=true";
    }

    private java.util.List<eu.nephron.credentials.ws.Credentialsdb> findAll() {
        CredentialsWS_Service service = new CredentialsWS_Service();
        eu.nephron.credentials.ws.CredentialsWS port = service.getCredentialsWSPort();
        return port.findAll();
    }

    /**
     * @return the bfail
     */
    public boolean isBfail() {
        return bfail;
    }

    /**
     * @param bfail the bfail to set
     */
    public void setBfail(boolean bfail) {
        this.bfail = bfail;
    }
}
