package eu.esponder.fru.thread;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateLocationMeasurementStatisticEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.fru.DataFusionController;
import eu.esponder.fru.JSONParser;
import eu.esponder.fru.helper.Utils;
import eu.esponder.jsonrcp.JSONRPCException;

public class EnvelopeSender extends Thread {

	private List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticsList,sensorMeasurementStatisticToSend;
	private SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticEnvelope; 
	private ESponderEvent<SensorMeasurementStatisticEnvelopeDTO> event;
	private Intent eventIntent;
	ObjectMapper  mapperEvent;
	String jsonEvent;
    String testdata="";
    String enveloptype="";
	int secs;
	Context context;
	
    public EnvelopeSender(
			List<SensorMeasurementStatisticDTO> sensorMeasurementStatisticsList,int secs,Context context) {
		this.sensorMeasurementStatisticsList = sensorMeasurementStatisticsList;
		this.secs=secs;
		this.context=context;
	}

	public List<SensorMeasurementStatisticDTO> getSensorMeasurementStatisticsList() {
		return sensorMeasurementStatisticsList;
	}

	public void run() {
		while (true) {

			try {
				Thread.sleep(secs*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			sendEnvelope();
		}

	}

	private synchronized void sendEnvelope() {
		if(sensorMeasurementStatisticsList!=null){
		if (sensorMeasurementStatisticsList.isEmpty()){
			System.out.println("send TO ENVELOPE  NO DATA stis = " + Utils.getTimeString());
			return;}
		
		sensorMeasurementStatisticEnvelope = new SensorMeasurementStatisticEnvelopeDTO();
		sensorMeasurementStatisticToSend = new ArrayList<SensorMeasurementStatisticDTO>();
		
		int size = sensorMeasurementStatisticsList.size();
		sensorMeasurementStatisticToSend.add(sensorMeasurementStatisticsList.get(size - 1));
		sensorMeasurementStatisticsList.clear();
		for (Iterator<SensorMeasurementStatisticDTO> iter = sensorMeasurementStatisticToSend.iterator(); iter.hasNext();) {
			SensorMeasurementStatisticDTO statisticMeasurement = iter.next();
		//	Log.d("test","test = " +statisticMeasurement + " sensoras = " + statisticMeasurement.getStatistic().getSensor().getLabel());
			try {
				DataFusionController.client.call("esponderdb/addESponderDTO", null, statisticMeasurement);
			} catch (JSONRPCException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 	}
	 
		sensorMeasurementStatisticEnvelope.setMeasurementStatistics(sensorMeasurementStatisticToSend);
 		 
	String type = "Steilame TO ENVELOPE ",value="";
 		//send sensorMeasurementStatisticEnvelope !!!
 		for(int i=0;i<sensorMeasurementStatisticEnvelope.getMeasurementStatistics().size();i++){
 			enveloptype = sensorMeasurementStatisticEnvelope.getMeasurementStatistics().get(i).getStatistic().getSensor().getClass().getSimpleName().toString();
 			if(enveloptype.equals("LocationSensorDTO")){
 				type = "LOCATION";
 				//System.out.println("send TO ENVELOPE LOCATION "+ enveloptype +" at = "+ Utils.getTimeString() +" value "+  ((LocationSensorMeasurementDTO) sensorMeasurementStatisticEnvelope.getMeasurementStatistics().get(i).getStatistic()).getPoint().getLatitude());                                                                  
 				value = value + ((LocationSensorMeasurementDTO) sensorMeasurementStatisticEnvelope.getMeasurementStatistics().get(i).getStatistic()).getPoint().getLatitude()+ " -- ";
 			}else{
 				type = "MEASUREMENT";
 	 	  		//System.out.println("send TO ENVELOPE MEASUREMENT "+ enveloptype +" at = "+ Utils.getTimeString() +" value "+ ((ArithmeticSensorMeasurementDTO) sensorMeasurementStatisticEnvelope.getMeasurementStatistics().get(i).getStatistic()).getMeasurement().toString());
 				value = value + ((ArithmeticSensorMeasurementDTO) sensorMeasurementStatisticEnvelope.getMeasurementStatistics().get(i).getStatistic()).getMeasurement().toString() + " -- ";
 			}
  
 		}
 		System.out.println("send TO ENVELOPE " + type + " at = "+ Utils.getTimeString() + " value " + value);
 		
 		JSONParser jpa = new JSONParser();
		ActorDTO actor = jpa.getActor();
		 
		
		if(sensorMeasurementStatisticEnvelope.getMeasurementStatistics().get(0).getStatistic() instanceof LocationSensorMeasurementDTO){
	 	       event = new CreateLocationMeasurementStatisticEvent(); 
	           eventIntent = new Intent(CreateLocationMeasurementStatisticEvent.class.getName()+"."+actor.getId());
		}
		else if(sensorMeasurementStatisticEnvelope.getMeasurementStatistics().get(0).getStatistic() instanceof ArithmeticSensorMeasurementDTO){
	 		event = new CreateSensorMeasurementStatisticEvent(); 
			eventIntent = new Intent(CreateSensorMeasurementStatisticEvent.class.getName()+"."+actor.getId());
		}
 		   
	       event.setEventSeverity(SeverityLevelDTO.MEDIUM);
		   event.setEventTimestamp(new Date());
		   event.setEventAttachment(sensorMeasurementStatisticEnvelope);
		   event.setEventSource(actor);
	 		  
		   mapperEvent = new ObjectMapper();
		   mapperEvent.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
		   mapperEvent.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
		   jsonEvent = null ;
		     mapperEvent.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	         try {
	        	 jsonEvent = mapperEvent.writeValueAsString(event);
	 		} catch (JsonGenerationException e) {
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 		} catch (JsonMappingException e) {
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 		} catch (IOException e) {
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 		}
    		 
		eventIntent.addCategory("org.osgi.service.event.EventAdmin");
		eventIntent.putExtra("event", jsonEvent); 
	   	context.sendBroadcast(eventIntent);
	   	
  
 		//testdata="";
		}
}

	}

 
