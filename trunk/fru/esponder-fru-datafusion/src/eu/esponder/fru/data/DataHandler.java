package eu.esponder.fru.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;
import eu.esponder.fru.DataFusionController;
import eu.esponder.fru.helper.Utils;

public class DataHandler extends BroadcastReceiver  {
 
	private List<SensorMeasurementEnvelopeDTO> sensorMeasurementsEnvelopeList;
    public static boolean flag=true;
	
/*	public DataHandler(
			List<SensorMeasurementEnvelopeDTO> sensorMeasurementEnvelope,
			Map<Class<? extends SensorDTO>, Map<MeasurementStatisticTypeEnum, Queue<SensorMeasurementDTO>>> sensorMeasurementsMap) {
		this.sensorMeasurementsEnvelopeList = sensorMeasurementEnvelope;
		this.sensorMeasurementsMap = sensorMeasurementsMap;
	}

	public List<SensorMeasurementEnvelopeDTO> getSensorMeasurementsEnvelope() {
		return sensorMeasurementsEnvelopeList;
	}

	public void setSensorMeasurementsEnvelope(
			List<SensorMeasurementEnvelopeDTO> sensorMeasurementsEnvelopeList) {
		this.sensorMeasurementsEnvelopeList = sensorMeasurementsEnvelopeList;
	} 

	public synchronized void run() {
		while (true) {

			try {
				Thread.sleep(5000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			processSensorMeasurementEnvelope();
		}

	}
*/
	private synchronized void processSensorMeasurementEnvelope(List<SensorMeasurementEnvelopeDTO> sensorMeasurementsEnvelopeList) {
       if(sensorMeasurementsEnvelopeList!=null){
		if (sensorMeasurementsEnvelopeList.isEmpty())
			return;
		if (!flag){
			System.out.println("has stopped!!");
			return;}

		System.out.println("Get (DataHandler)!! at = "
				+ Utils.getTimeString());
		for (SensorMeasurementEnvelopeDTO sensorMeasurementsEnvelope : sensorMeasurementsEnvelopeList) {
			// for (Iterator<SensorMeasurementEnvelopeDTO> it =
			// sensorMeasurementsEnvelopeList.iterator(); it.hasNext();) {
			// SensorMeasurementEnvelopeDTO
			// sensorMeasurementsEnvelope=it.next();
			if (!sensorMeasurementsEnvelope.getMeasurements().isEmpty()) {

				for (SensorMeasurementDTO measurement : sensorMeasurementsEnvelope
						.getMeasurements()) {
					
					/*
					 * Get Measurement from OSGi Envelope for a specific sensor
					 */

					Map<MeasurementStatisticTypeEnumDTO, Queue<SensorMeasurementDTO>> sensorMeasurementsStatisticsMap = DataFusionController.sensorMeasurementMaps
							.get(measurement.getSensor().getClass());
					System.out.println("[" + Utils.getTimeString()
							+ "] - Adding Measurement for sensor"
							+ measurement.getSensor().getName() + " kai size = "+ measurement.getSensor().getClass().toString());
					
					 
					/*
					 * Put raw measurement to ALL Queues (i.e. for all statistic
					 * types)
					 */
					for (MeasurementStatisticTypeEnumDTO statisticType : sensorMeasurementsStatisticsMap
							.keySet()) {
						Queue<SensorMeasurementDTO> sensorMeasurementQueue = sensorMeasurementsStatisticsMap
								.get(statisticType);
						sensorMeasurementQueue.add(measurement);
						if( measurement instanceof ArithmeticSensorMeasurementDTO){
						
						System.out
								.println("["
										+ Utils.getTimeString()
										+ "] - Added Measurement for sensor"
										+ measurement.getSensor()
										+ " for statistic "
										+ statisticType.toString()
										+ ". Value = " +
									 	+ ((ArithmeticSensorMeasurementDTO) measurement).getMeasurement().doubleValue());
						   }else{
								System.out
								.println("["
										+ Utils.getTimeString()
										+ "] - Added Measurement for sensor"
										+ measurement.getSensor()
										+ " for statistic "
										+ statisticType.toString()
										+ ". Value = " +
									 	   ((LocationSensorMeasurementDTO) measurement).getPoint() );
						   }
						}
				}
				// sensorMeasurementsEnvelope.clear();
			}
		}
	}
	//	sensorMeasurementsEnvelopeList.clear();
	}

	@Override
	public void onReceive(Context context, Intent intent) {
			  if (intent.hasExtra("event")) {
				  
				  Log.d("data arived","!!!!!!Data Arived!!!!!");
				  
				  String incomingEvent = intent.getStringExtra("event");
				  ObjectMapper	mapper = new ObjectMapper();
				  mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
 				  JsonFactory jsonFactory = new JsonFactory(); 
 				  try {
 				      JsonParser jp = jsonFactory.createJsonParser(incomingEvent);
 				      // get the event
 				     CreateSensorMeasurementEnvelopeEvent event = mapper.readValue(jp, CreateSensorMeasurementEnvelopeEvent.class);
 				       Log.d("!! name !!", "!! name !! is "+event.getEventAttachment().getMeasurements().get(0).getSensor().getClass());  
 	
 				     sensorMeasurementsEnvelopeList = new ArrayList<SensorMeasurementEnvelopeDTO>(); 
 				     sensorMeasurementsEnvelopeList.add(event.getEventAttachment());
 				     processSensorMeasurementEnvelope(sensorMeasurementsEnvelopeList);
 				   /*  for(int i=0;i<sensorMeasurementsEnvelopeList.get(0).getMeasurements().size();i++){
 				   // 	 System.out.println("its"+ ((ArithmeticSensorMeasurementDTO)sensorMeasurementsEnvelopeList.get(0).getMeasurements().get(i)).getMeasurement().intValue() );
 				     }*/
		 
 				  } catch (Exception e) {
 				      e.printStackTrace();
 				    }
			}  
		 
	}

}
