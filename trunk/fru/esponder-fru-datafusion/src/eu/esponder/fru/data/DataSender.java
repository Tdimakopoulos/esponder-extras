package eu.esponder.fru.data;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import android.content.Context;
import android.content.Intent;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.SensorMeasurementEnvelopeDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;
import eu.esponder.fru.helper.Utils;

public class DataSender extends Thread {
	Context context;
	ArithmeticSensorMeasurementDTO sensorMeasurement1,sensorMeasurement2,sensorMeasurement3;
	
	 public DataSender(Context ctx)
	    {
	        context = ctx;
	    }
	 
	private synchronized void createSensorMeasurementsEnvelope() {
     	Random rand = new Random();
		
     	BodyTemperatureSensorDTO bodyTemperatureSensor = new BodyTemperatureSensorDTO();
		bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
		bodyTemperatureSensor.setName("Sensor_Temperature");
		bodyTemperatureSensor.setType("Sensor_Temperature");
		bodyTemperatureSensor.setTitle("Body Temperature Sensor");
		bodyTemperatureSensor.setId(System.currentTimeMillis());
		bodyTemperatureSensor.setStatus(ResourceStatusDTO.AVAILABLE);
		//SensorDTO gasSensor = new GasSensorDTO();
		
		  sensorMeasurement1 = new ArithmeticSensorMeasurementDTO();
		  sensorMeasurement2 = new ArithmeticSensorMeasurementDTO();
		  sensorMeasurement3 = new ArithmeticSensorMeasurementDTO();
		
		sensorMeasurement1.setSensor(bodyTemperatureSensor);
		sensorMeasurement2.setSensor(bodyTemperatureSensor);
		sensorMeasurement3.setSensor(bodyTemperatureSensor);
		
		//ArithmeticSensorMeasurementDTO sensorMeasurement4 = new ArithmeticSensorMeasurementDTO();
	//	ArithmeticSensorMeasurementDTO sensorMeasurement5 = new ArithmeticSensorMeasurementDTO();
		
		//sensorMeasurement4.setSensor(gasSensor.getClass());
		//sensorMeasurement5.setSensor(gasSensor.getClass());
		
		sensorMeasurement1.setMeasurement(new BigDecimal(rand.nextInt(80 - 60 + 1) + 60));
		sensorMeasurement1.setTimestamp(new Date());
		sensorMeasurement2.setMeasurement(new BigDecimal(rand.nextInt(80 - 60 + 1) + 60));
		sensorMeasurement2.setTimestamp(new Date());
		sensorMeasurement3.setMeasurement(new BigDecimal(rand.nextInt(80 - 60 + 1) + 60));
		sensorMeasurement3.setTimestamp(new Date());
		
	//	sensorMeasurement4.setMeasurement(new BigDecimal(rand.nextInt(rand.nextInt(500 - 300 + 1) + 300)));
	//	sensorMeasurement4.setTimestamp(new Date());
	//	sensorMeasurement5.setMeasurement(new BigDecimal(rand.nextInt(rand.nextInt(500 - 300 + 1) + 300)));
	//	sensorMeasurement5.setTimestamp(new Date());
		
		SensorMeasurementEnvelopeDTO sensorMeasurementsEnvelope = new SensorMeasurementEnvelopeDTO();
		
		sensorMeasurementsEnvelope.add(sensorMeasurement1); 
		sensorMeasurementsEnvelope.add(sensorMeasurement2);
		sensorMeasurementsEnvelope.add(sensorMeasurement3);
	//	sensorMeasurementsEnvelope.add(sensorMeasurement4);
	//	sensorMeasurementsEnvelope.add(sensorMeasurement5);
		
		
		//Stelnei
		// DataFusionController.sensorMeasurementsEnvelopeList.add(sensorMeasurementsEnvelope);
 
		
		ESponderEvent<SensorMeasurementEnvelopeDTO> event = new CreateSensorMeasurementEnvelopeEvent();
	   //	CreateSensorMeasurementStatisticEvent
	   	
		//CreateArithmeticSensorMeasurementEvent event = new CreateArithmeticSensorMeasurementEvent();
	   	event.setEventSeverity(SeverityLevelDTO.MEDIUM);
	 	event.setEventTimestamp(new Date());
		event.setEventAttachment(sensorMeasurementsEnvelope);
		event.setEventSource(bodyTemperatureSensor);
		//event.setJournalMessage("xexexe");
		System.out.println("to date "+event.getEventTimestamp());
	//	System.out.println("to date ");
		   ObjectMapper  mapper = new ObjectMapper();
		     mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
	         mapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
		     String json = null ;
		    mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	         try {
	 			  json = mapper.writeValueAsString(event);
	 		} catch (JsonGenerationException e) {
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 		} catch (JsonMappingException e) {
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 		} catch (IOException e) {
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 		}
	         
	         
	     	/*Bundle envelope= new Bundle();
	     	envelope.putString("event", json); */
		    Intent i = new Intent(CreateSensorMeasurementEnvelopeEvent.class.getName()+".4");
		 	i.putExtra("event", json); 
		 	i.addCategory("org.osgi.service.event.EventAdmin");
	        context.sendBroadcast(i);
		
		 	
		 	//////----------
		 	
			 LocationSensorDTO sensorDTO = new LocationSensorDTO(); 
			    sensorDTO.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES);
			    sensorDTO.setName("LPS");
			    
			    sensorDTO.setType("LPS");
			    sensorDTO.setTitle("Local Positioning Sensor");
			    sensorDTO.setId(System.currentTimeMillis());
			    sensorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
			    
			    
			    PointDTO pointDTO = new PointDTO();
			    pointDTO.setAltitude(new BigDecimal(220000));
			    pointDTO.setLongitude(new BigDecimal(23762063));
			    pointDTO.setLatitude(new BigDecimal(37983599));
			    
			  //  SphereDTO sphereDTO = new SphereDTO();
			  //  sphereDTO.setCentre(pointDTO);
			    
			    LocationSensorMeasurementDTO sensorData = new LocationSensorMeasurementDTO();
			    sensorData.setPoint(pointDTO);
			    sensorData.setSensor(sensorDTO);
			    
			    SensorMeasurementEnvelopeDTO sensorMeasurementsEnvelopeloc = new SensorMeasurementEnvelopeDTO();
			    sensorMeasurementsEnvelopeloc.add(sensorData); 
			    
		 /*   ESponderEvent<SensorMeasurementEnvelopeDTO> eventloc = new CreateSensorMeasurementEnvelopeEvent();
			   //	CreateSensorMeasurementStatisticEvent
			   	
				//CreateArithmeticSensorMeasurementEvent event = new CreateArithmeticSensorMeasurementEvent();
		    eventloc.setEventSeverity(SeverityLevelDTO.MEDIUM);
		    eventloc.setEventTimestamp(new Date());
		    eventloc.setEventAttachment(sensorMeasurementsEnvelopeloc);
		    eventloc.setEventSource(sensorDTO);
				
				   ObjectMapper  mapperloc = new ObjectMapper();
				   mapperloc.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
				   mapperloc.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
				     String jsonloc = null ;
				     mapperloc.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			         try {
			        	 jsonloc = mapperloc.writeValueAsString(eventloc);
			 		} catch (JsonGenerationException e) {
			 			// TODO Auto-generated catch block
			 			e.printStackTrace();
			 		} catch (JsonMappingException e) {
			 			// TODO Auto-generated catch block
			 			e.printStackTrace();
			 		} catch (IOException e) {
			 			// TODO Auto-generated catch block
			 			e.printStackTrace();
			 		}
			         
			         
			     	
				    Intent iloc = new Intent(CreateSensorMeasurementEnvelopeEvent.class.getName());
				    iloc.putExtra("event", jsonloc); */
				 // 	context.sendBroadcast(iloc);
		 	////------------
				   	
		/*////////////   -----------------------     UpdateConfigurationEvent  -----------------------   //////////// 
					BodyTemperatureSensorDTO bodyTempe  = new BodyTemperatureSensorDTO();
				   	bodyTempe.setMeasurementUnit(MeasurementUnitEnum.DEGREES_CELCIUS);
				   	bodyTempe.setName("Body Temperature");
				   	bodyTempe.setType("Body Temperature");
				   	bodyTempe.setLabel("BdTemp");
				   	bodyTempe.setTitle("Body Temperature Sensor");
				   	bodyTempe.setId(System.currentTimeMillis());
				   	bodyTempe.setStatus(ResourceStatusDTO.AVAILABLE);
				   	
				   	
					BodyTemperatureSensorDTO bodyTempeSub  = new BodyTemperatureSensorDTO();
					bodyTempeSub.setMeasurementUnit(MeasurementUnitEnum.DEGREES_CELCIUS);
					bodyTempeSub.setName("Body Temperature");
					bodyTempeSub.setType("Body Temperature");
					bodyTempeSub.setLabel("BdTemp");
					bodyTempeSub.setTitle("Body Temperature Sensor");
					bodyTempeSub.setId(System.currentTimeMillis());
					bodyTempeSub.setStatus(ResourceStatusDTO.AVAILABLE);
					
					
					BodyTemperatureSensorDTO bodyTempeSubNo2  = new BodyTemperatureSensorDTO();
					bodyTempeSubNo2.setMeasurementUnit(MeasurementUnitEnum.DEGREES_CELCIUS);
					bodyTempeSubNo2.setName("Body Temperature");
					bodyTempeSubNo2.setType("Body Temperature");
					bodyTempeSubNo2.setLabel("BdTemp");
					bodyTempeSubNo2.setTitle("Body Temperature Sensor");
					bodyTempeSubNo2.setId(System.currentTimeMillis());
					bodyTempeSubNo2.setStatus(ResourceStatusDTO.AVAILABLE);
				   	
				   	LocationSensorDTO lpsSensor = new LocationSensorDTO(); 
				    lpsSensor.setMeasurementUnit(MeasurementUnitEnum.DEGREES);
				    lpsSensor.setName("Location Sensor");
				    lpsSensor.setType("Location Sensor");
				    lpsSensor.setLabel("LPS");
				    lpsSensor.setTitle("Local Positioning Sensor");
				    lpsSensor.setId(System.currentTimeMillis());
				    lpsSensor.setStatus(ResourceStatusDTO.AVAILABLE);
				    
				    
				    
				    
				    
				    
				    
				    ///  -------------    SubActor    -------------  ////
				   	ActorDTO subactorDTO= new ActorDTO();
				   	subactorDTO.setId(new Long(4));
				   	subactorDTO.setType("FRC 5555");
				   	subactorDTO.setTitle("FRC #5555");
				   	subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				   	
				   	HashSet equipSub = new HashSet(); 
				   	EquipmentDTO eqSub= new EquipmentDTO();
				   	eqSub.setId(new Long(2));
				   	eqSub.setType("equip2");
				   	eqSub.setTitle("Title2");
		 	 	   
				   	StatisticsConfigDTO statisticAVGSub = new StatisticsConfigDTO();
				   	statisticAVGSub.setMeasurementStatisticType(MeasurementStatisticTypeEnum.FIRST);
				   	statisticAVGSub.setSamplingPeriodMilliseconds(new Long(5));
				   	
					HashSet statisticsBodyTempSub = new HashSet(); 
					statisticsBodyTempSub.add(statisticAVGSub); 
					bodyTempeSub.setStatisticsConfig(statisticsBodyTempSub);
				   	
				   	HashSet sensorSub = new HashSet(); 
				   	sensorSub.add(bodyTempeSub);
					eqSub.setSensors(sensorSub);
				   	
				   	
					equipSub.add(eqSub);
				  	subactorDTO.setEquipmentSet(equipSub);
				   	
				    ///  -------------    SubActor    -------------  ////
				   	
				   	
				    ///  -------------    SubActor 2    -------------  ////
				   	ActorDTO subactorDTONo2= new ActorDTO();
				   	subactorDTONo2.setId(new Long(7));
				   	subactorDTONo2.setType("FRC 5555");
				   	subactorDTONo2.setTitle("FRC #5555");
				   	subactorDTONo2.setStatus(ResourceStatusDTO.AVAILABLE);
				   	
				   	HashSet equipSubNo2 = new HashSet(); 
				   	EquipmentDTO eqSubNo2= new EquipmentDTO();
				   	eqSubNo2.setId(new Long(2));
				   	eqSubNo2.setType("No2");
				   	eqSubNo2.setTitle("No2");
		 	 	   
				   	StatisticsConfigDTO statisticAVGSubNo2 = new StatisticsConfigDTO();
				   	statisticAVGSubNo2.setMeasurementStatisticType(MeasurementStatisticTypeEnum.FIRST);
				   	statisticAVGSubNo2.setSamplingPeriodMilliseconds(new Long(5));
				   	
					HashSet statisticsBodyTempSubNo2 = new HashSet(); 
					statisticsBodyTempSubNo2.add(statisticAVGSub); 
					bodyTempeSubNo2.setStatisticsConfig(statisticsBodyTempSubNo2);
				   	
				   	HashSet sensorSubNo2 = new HashSet(); 
				   	sensorSubNo2.add(bodyTempeSubNo2);
					eqSubNo2.setSensors(sensorSubNo2);
				   	
				   	
					equipSubNo2.add(eqSubNo2);
				  	subactorDTONo2.setEquipmentSet(equipSubNo2);
				   	
				  	
				  	
				    ///  -------------    SubActor 3 Me   -------------  ////
				   	ActorDTO subactorDTOMe= new ActorDTO();
				   	subactorDTOMe.setId(new Long(10));
				   	subactorDTOMe.setType("FRC 1010");
				   	subactorDTOMe.setTitle("FRC #1010");
				   	subactorDTOMe.setStatus(ResourceStatusDTO.AVAILABLE);
				  
				  	
				    ///  -------------    Chief    -------------  ////
				  	
				   	
				   	ActorDTO actorDTO= new ActorDTO();
				   	actorDTO.setId(new Long(1));
				   	actorDTO.setType("FRC 3333");
				   	actorDTO.setTitle("FRC #3333");
				   	actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				   	
				   	HashSet equip = new HashSet(); 
				   	EquipmentDTO eq= new EquipmentDTO();
				   	eq.setId(new Long(2));
				   	eq.setType("typosss");
				   	eq.setTitle("Titlos");
		 	 	   
				   	StatisticsConfigDTO statisticAVG = new StatisticsConfigDTO();
				   	statisticAVG.setMeasurementStatisticType(MeasurementStatisticTypeEnum.MEAN);
				   	statisticAVG.setSamplingPeriodMilliseconds(new Long(5));
				   	
				   	StatisticsConfigDTO statisticMAX = new StatisticsConfigDTO();
				   	statisticMAX.setMeasurementStatisticType(MeasurementStatisticTypeEnum.MAXIMUM);
				   	statisticMAX.setSamplingPeriodMilliseconds(new Long(3));
				   	
				   	HashSet statisticsBodyTemp = new HashSet(); 
				   	statisticsBodyTemp.add(statisticAVG);
				   	statisticsBodyTemp.add(statisticMAX);
				   	bodyTempe.setStatisticsConfig(statisticsBodyTemp);
				   	
			 	   	
				    StatisticsConfigDTO statisticLAST = new StatisticsConfigDTO();
				    statisticLAST.setMeasurementStatisticType(MeasurementStatisticTypeEnum.LAST);
				    statisticLAST.setSamplingPeriodMilliseconds(new Long(2));
				    
				 	HashSet statisticsLocation = new HashSet(); 
				 	statisticsLocation.add(statisticLAST); 
				 	lpsSensor.setStatisticsConfig(statisticsLocation);
					
					HashSet sensor = new HashSet(); 
					sensor.add(bodyTempe);
					sensor.add(lpsSensor);
					eq.setSensors(sensor);
				   	
				   	
				  	equip.add(eq);
				   	actorDTO.setEquipmentSet(equip);   
				   	
				   	HashSet subActors = new HashSet(); 
				   	subActors.add(subactorDTO);
				   	subActors.add(subactorDTONo2);
			 
				   //	subActors.add(subactorDTOMe);
				   	 
		 		   	actorDTO.setSubordinates(subActors);
		 		   	
		 		    ///  -------------    SubActor 3 egw   -------------  ////
				   	ActorDTO subactorDTOegw = new ActorDTO();
				   	subactorDTOegw.setId(new Long(10));
				   	subactorDTOegw.setType("FRC 1010");
				   	subactorDTOegw.setTitle("FRC #1010");
				   	subactorDTOegw.setStatus(ResourceStatusDTO.AVAILABLE);
				   	
				   	subactorDTOegw.setSupervisor(actorDTO);
		 		   
				   	UpdateConfigurationEvent eventConfig = new UpdateConfigurationEvent();
				   	eventConfig.setEventSeverity(SeverityLevelDTO.MEDIUM);
				   	eventConfig.setEventTimestamp(new Date());
				   	eventConfig.setEventAttachment(actorDTO);
						
						   ObjectMapper  mapperConfig = new ObjectMapper();
						 //  mapperConfig.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
						//   mapperConfig.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL); 
						     String jsonConfig= null ;
						   //  mapperConfig.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					         try {
					        	 jsonConfig = mapperloc.writeValueAsString(eventConfig);
					 	//	 	
					 		} catch (JsonGenerationException e) {
					 			// TODO Auto-generated catch block
					 			e.printStackTrace();
					 		} catch (JsonMappingException e) {
					 			// TODO Auto-generated catch block
					 			e.printStackTrace();
					 		} catch (IOException e) {
					 			// TODO Auto-generated catch block
					 			e.printStackTrace();
					 		}
					         
	
					        Bundle envelopeConfig= new Bundle();
					         envelopeConfig.putString("event", jsonConfig); 
					        // envelopeConfig.putString("event", "{\"status\":\"AVAILABLE\",\"title\":\"Actor1\",\"type\":\"ActorType1\",\"equipmentSet\":[\"java.util.HashSet\",[{\"status\":null,\"title\":\"Equipment1\",\"type\":\"EquipmentType1\",\"sensors\":null,\"snapshot\":null},{\"status\":null,\"title\":\"Equipment2\",\"type\":\"EquipmentType2\",\"sensors\":null,\"snapshot\":null}]],\"snapshot\":null,\"subordinates\":null}"); 
						    
					     //   envelopeConfig.putString("event", "  {\"status\":null,\"title\":\"Actor1\",\"type\":\"ActorType1\",\"equipmentSet\":[{\"status\":null,\"title\":\"Equipment1\",\"type\":\"EquipmentType1\",\"sensors\":null,\"snapshot\":null},{\"status\":null,\"title\":\"Equipment2\",\"type\":\"EquipmentType2\",\"sensors\":null,\"snapshot\":null}],\"snapshot\":null,\"subordinates\":null}"); 
					        Intent iConfig = new Intent(UpdateConfigurationEvent.class.getName());
						    iConfig.putExtra("event", envelopeConfig); 
						//   	context.sendBroadcast(iConfig);
					////////////   -----------------------     UpdateConfigurationEvent  -----------------------   //////////// 	
*/						   	 
	}

	public void run() {

		while (true) 
		{
			
			try {
				Thread.sleep(5000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			createSensorMeasurementsEnvelope();
			
		

		}

	}
}
