package eu.esponder.fru.activities;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;
import eu.esponder.fru.DataFusionController;
import eu.esponder.fru.JSONParser;
import eu.esponder.fru.data.DataHandler;
import eu.esponder.fru.data.DataSender;

public class Controller extends Service {

	private boolean isRunning = true;
	private Thread dataSenderThread;

	private String filter = CreateSensorMeasurementEnvelopeEvent.class.getName();
	private DataHandler receiver;

/*	private String filterConfing = UpdateConfigurationEvent.class.getName();
	private UpdateConfig receiverConfig;*/

	@Override
	public IBinder onBind(Intent intent) {
		// Not implemented...this sample is only for starting and stopping
		// services.
		// Service binding will be covered in another tutorial
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	public int onStartCommand(Intent intent, int flags, int startId) {

		// Announcement about starting
		Toast.makeText(this, "Starting the Controller Service",
				Toast.LENGTH_SHORT).show();

		Context con = this;
		DataFusionController dataFusion = new DataFusionController(con);
		dataFusion.init();

		 //startThread(con);
		
		JSONParser jpa = new JSONParser();
		ActorDTO actor = jpa.getActor();

		receiver = new DataHandler();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(filter+"."+actor.getId());
		intentFilter.addCategory("org.osgi.service.event.EventAdmin");
		registerReceiver(receiver, intentFilter);
		  
	/*	receiverConfig = new UpdateConfig();
		IntentFilter configFilter = new IntentFilter(filterConfing);
		registerReceiver(receiverConfig, configFilter);
*/
		 

		// Start a Background thread
		// isRunning = true;
		// backgroundThread = new Thread(new BackgroundThread());
		// backgroundThread.start();
		// startThread();
		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
 
		return 0;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// Stop the Background thread
		// isRunning = false;
		// stopThread();
		// Announcement about stopping
		Toast.makeText(this, "Stopping the Controller Service",
				Toast.LENGTH_SHORT).show();
		unregisterReceiver(receiver);
	//	unregisterReceiver(receiverConfig);
		stopThread();
	}

	public synchronized void startThread(Context context) {
		if (dataSenderThread == null) {
			dataSenderThread = new Thread(new DataSender(context));
			dataSenderThread.start();
		}
	}

	public synchronized void stopThread() {
	//	if (dataSenderThread != null) {
		//	Thread moribund = dataSenderThread;
		//	dataSenderThread = null;
		//	moribund.interrupt();
	 		for(int i=0;i<DataFusionController.processorThreads.size();i++){
				Thread moribund = DataFusionController.processorThreads.get(i);
				DataFusionController.processorThreads.get(i);
 			}
		// }
	}

	/*private class BackgroundThread implements Runnable {
		int counter = 0;

		public void run() {
			try {
				counter = 0;
				while (Thread.currentThread() == backgroundThread) {
					System.out.println("" + counter++);
					Thread.currentThread();
					Thread.sleep(5000);

					// Bundle b = new Bundle();
					// b.putInt("wakdStateIs",2);

					// Intent i2 = new Intent("Event");
					// i2.putExtra("count", counter);
					// sendBroadcast(i2);
					
					  if(counter==3){ Intent dialogIntent = new
					  Intent(getBaseContext(), Alert.class);
					  dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					  getApplication().startActivity(dialogIntent); }
					  
				}

				System.out.println("Background Thread is finished.........");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	} */
}
