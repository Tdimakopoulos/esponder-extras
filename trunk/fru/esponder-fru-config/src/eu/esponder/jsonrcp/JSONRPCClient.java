package eu.esponder.jsonrcp;

import java.util.UUID;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class JSONRPCClient {

  private ObjectMapper mapper;
  private JsonFactory jsonFactory;

  public JSONRPCClient() {
    mapper = new ObjectMapper();
    mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
    jsonFactory = new JsonFactory(); 
  }

  /**
   * Create a JSONRPCClient from a given uri
   * 
   * @param uri
   *          The URI of the JSON-RPC service
   * @return a JSONRPCClient instance acting as a proxy for the web service
   */
  public static JSONRPCClient create(String uri) {
    JSONRPCClient client = new JSONRPCHttpClient(uri);
    return client;
  }

  protected abstract JSONObject doJSONRequest(JSONObject request) throws JSONRPCException;

  protected static JSONArray getJSONArray(Object[] array) {
    JSONArray arr = new JSONArray();
    for (Object item : array) {
      if (item.getClass().isArray()) {
        arr.put(getJSONArray((Object[])item));
      } else {
        arr.put(item);
      }
    }
    return arr;
  }

  protected JSONObject doRequest(String method, Object[] params) throws JSONRPCException {
    // Copy method arguments in a json array
    JSONArray jsonParams = new JSONArray();
    for (int i = 0; i < params.length; i++) {
      try {
        jsonParams.put(mapper.writeValueAsString(params[i]));
      } catch (Exception e) {
        e.printStackTrace();
        throw new JSONRPCException("Invalid parameters: " + e.getMessage(), e);
      }
    }

    // Create the json request object
    JSONObject jsonRequest = new JSONObject();
    try {
      jsonRequest.put("id", UUID.randomUUID().hashCode());
      jsonRequest.put("method", method);
      jsonRequest.put("params", jsonParams);
      jsonRequest.put("jsonrpc", "2.0");
    } catch (JSONException e1) {
      throw new JSONRPCException("Invalid JSON request", e1);
    }
    return doJSONRequest(jsonRequest);
  }

  protected int soTimeout = 0, connectionTimeout = 0;

  /**
   * Get the socket operation timeout in milliseconds
   */
  public int getSoTimeout() {
    return soTimeout;
  }

  /**
   * Set the socket operation timeout
   * 
   * @param soTimeout
   *          timeout in milliseconds
   */
  public void setSoTimeout(int soTimeout) {
    this.soTimeout = soTimeout;
  }

  /**
   * Get the connection timeout in milliseconds
   */
  public int getConnectionTimeout() {
    return connectionTimeout;
  }

  /**
   * Set the connection timeout
   * 
   * @param connectionTimeout
   *          timeout in milliseconds
   */
  public void setConnectionTimeout(int connectionTimeout) {
    this.connectionTimeout = connectionTimeout;
  }

  /**
   * Perform a remote JSON-RPC method call
   * 
   * @param method
   *          The name of the method to invoke
   * @param returnType
   *          The result class, null for void
   * @param params
   *          Arguments of the method
   * @return The result of the RPC
   * @throws JSONRPCException
   *           if an error is encountered during JSON-RPC method call
   */
  public Object call(String method, Class returnType, Object... params) throws JSONRPCException {
    try {
      Object result = doRequest(method, params).get("result");
      System.err.println("RESULT : " + result);
      System.err.println("RESULT CN : " + result.getClass().getName());
      if (returnType == null || result == null) {
        return result;
      } else {
        return mapper.readValue(jsonFactory.createJsonParser(result.toString()), returnType);
      }
    } catch (Throwable e) {
      e.printStackTrace();
      throw new JSONRPCException("Cannot convert result", e);
    }
  }

}
