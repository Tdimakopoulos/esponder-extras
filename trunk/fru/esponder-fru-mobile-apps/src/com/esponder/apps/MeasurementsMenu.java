package com.esponder.apps;
 

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View; 

public class MeasurementsMenu extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.measurementsmenu); 	
	}

	// Click on the Measurements button of Main
	public void clickHandler(View view) {
		switch (view.getId()) {
		case R.id.tableMeasurements: // tableMeasurements
			Intent openMeasurements= new Intent(MeasurementsMenu.this, TableMeasurements.class);
			startActivity(openMeasurements);
			break;
		case R.id.graphicMeasurements: // graphicMeasurements
			Intent openMap = new Intent(MeasurementsMenu.this, MeasurementSelection.class);
			startActivity(openMap);
			break;
		default:
			break;
		}
	}

}