package InterfChecker;

import netinterf.soap.clients.DBSettingsClient;
import netinterf.soap.clients.DatabaseManagerClient;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;

public class Application {

    protected static String sleepTime;//parameter from Property File
    protected static String c;//parameter from Property File
    protected static String k;//parameter from Property File
    protected static String x;//parameter from Property File
    protected static String maxRate;//parameter from Property File // Max Rate for other Interfaces except ethernet
    protected static String url;
    protected static String dbName;
    protected static String driver;
    protected static String userName;
    protected static String password;
    protected static String nochangeTTL;
    static String info = " INFO ";
    static String propertyFileTitle = " Monitor initialized with the following properties:";
    static String name = "[main]";
    protected static double timeout;//converted parameter from Property File
    protected static int counterNoSuccess;//converted parameter from Property File
    protected static int states;//converted parameter from Property File
    protected static double changeRate;//converted parameter from Property File
    protected static double TTL;
    static Connection conn;
    public static Statement stmt;
    static int id;
    String intname = "";
    String address = new String();
    protected static String hostname="";

    public static void main(String[] args) {
        try {
            //connection conn = new connection();
            //Statement st = conn.createStatement;
//			System.out.println("OK!!");
//			Class.forName(driver).newInstance();
//			 Connection connection= DriverManager.getConnection("jdbc:mysql://localhost/interfdb?"
//	  					+ "user=root&password=1134"
//						);
//
//			  stmt = connection.createStatement();
//			  
//			  stmt.executeUpdate("delete from listA;");
//			  stmt.executeUpdate("delete from listC;");
//			  stmt.executeUpdate("delete from listBwithAP;");
//			  stmt.executeUpdate("delete from listB;");
//			  stmt.executeUpdate("delete from interf;");
//			  



            //Reading Property File
            FileInputStream fstream = new FileInputStream(args[0]);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                String[] tokens = strLine.split(" ");
                for (int i = 0; i < tokens.length; i++) {
                    String var = tokens[i];
                    if (var.equalsIgnoreCase("sleepTime")) {
                        sleepTime = tokens[i + 1];
                    } else if (var.equalsIgnoreCase("c")) {
                        c = tokens[i + 1];
                    } else if (var.equalsIgnoreCase("k")) {
                        k = tokens[i + 1];
                    } else if (var.equalsIgnoreCase("x")) {
                        x = tokens[i + 1];
                    } else if (var.equalsIgnoreCase("maxRate")) {
                        maxRate = tokens[i + 1];
                    } else if (var.equalsIgnoreCase("url")) {
                        url = tokens[i + 1];
                    } else if (var.equalsIgnoreCase("dbName")) {
                        dbName = tokens[i + 1];
                    } else if (var.equalsIgnoreCase("driver")) {
                        driver = tokens[i + 1];
                    } else if (var.equalsIgnoreCase("userName")) {
                        userName = tokens[i + 1];
                    } else if (var.equalsIgnoreCase("password")) {
                        password = tokens[i + 1];
                    } else if (var.equalsIgnoreCase("nochangeTTL")) {
                        nochangeTTL = tokens[i + 1];
                    }
                }
            }
            br.close();

            //Convert parameters to needed types
            timeout = Double.parseDouble(Application.sleepTime.trim());
            states = Integer.parseInt(Application.k.trim());
            counterNoSuccess = Integer.parseInt(Application.c.trim());
            changeRate = Double.parseDouble(Application.x.trim());
            TTL = Double.parseDouble(Application.nochangeTTL.trim());

            //Initial PrintOut of parameters
            System.out.println(name + info + propertyFileTitle);
            System.out.println(name + info + "TIMEOUT: " + Application.timeout);
            System.out.println(name + info + "Change Rate: " + Application.changeRate);
            System.out.println(name + info + "States: " + Application.states);
            System.out.println(name + info + "Counter of No Success: " + Application.counterNoSuccess);
            System.out.println(name + info + "Max Rate for other Interfaces: " + Application.maxRate);
//
                           String command = "hostname";
                           String cmdout="";
                           
                BufferedReader hostnameCmd = InterfaceThread.ExecuteLinuxCommand(command);//Executing Command

                while ((cmdout = hostnameCmd.readLine()) != null) {
                    String[] tokens = cmdout.split(" ");
                    for (int i = 0; i < tokens.length; i++) {
                        hostname = tokens[i];
                    }
                }
                
                System.out.println("---------------------Sending DB settings to Web Server-------------------");
            DBSettingsClient pclient = new DBSettingsClient();
            interfaces.soap.ws.SoapMessageJDBCSet pdat = new interfaces.soap.ws.SoapMessageJDBCSet();
            pdat.setDbname(dbName);
            pdat.setDriver(driver);
            pdat.setPassword(password);
            pdat.setUrl(url);
            pdat.setUsername(userName);
            pclient.putXml(pdat);
            System.out.println("---------------------Clearing DB-------------------");
            DatabaseManagerClient dbManager = new DatabaseManagerClient();
            dbManager.DeleteDatabase(hostname);
            
            
            
            ResultSetTableModelFactory factory =
                    new ResultSetTableModelFactory();
            NetworkManagerGUI qf = new NetworkManagerGUI(factory);

            // Set the size of the QueryFrame, then pop it up
            qf.setSize(900, 600);
            qf.setVisible(true);

            new InterfaceThread("Thread").start();//Starts Main Thread
        } catch (Exception e) {
            e.printStackTrace();//Printing Exception
            return;
        }
    }
}
