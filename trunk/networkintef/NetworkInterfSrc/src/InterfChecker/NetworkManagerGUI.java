package InterfChecker;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;

/**
 * This class creates a Swing GUI that allows the user to enter a SQL query.
 * It then obtains a ResultSetTableModel for the query and uses it to display
 * the results of the query in a scrolling JTable component.
 **/
public class NetworkManagerGUI extends JFrame {
    ResultSetTableModelFactory factory;   // A factory to obtain our table data
    JTextField query;                     // A field to enter a query in
    JTable table;                         // The table for displaying data
    JLabel msgline;                       // For displaying messages
    public JMenu jm ;
    public JMenuItem jmitem1 ;
    public JMenuItem jmitem2 ;
    public JMenuBar jmb ;
    public JButton[] numbers = null;

    /**
     * This constructor method creates a simple GUI and hooks up an event
     * listener that updates the table when the user enters a new query.
     **/
    public NetworkManagerGUI(ResultSetTableModelFactory f) {
	super("Linux Network Interface");  // Set window title

	// Arrange to quit the program when the user closes the window
	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent e) { System.exit(0); }
	    });

	// Remember the factory object that was passed to us
	this.factory = f;

	// Create the Swing components we'll be using
	query = new JTextField();     // Lets the user enter a query
	table = new JTable();         // Displays the table
	msgline = new JLabel();       // Displays messages

	// Place the components within this window
	Container contentPane = getContentPane();
	NetworkManagerGUIActionListener fal = new NetworkManagerGUIActionListener(this);
    GridBagLayout gbl = new GridBagLayout();
    GridBagConstraints gbc = new GridBagConstraints();
    JPanel p = new JPanel();

    p.setLayout(gbl);
    this.numbers = new JButton[3];
    
    gbc.gridx=0;
    
    for(int i=0;i<3;i++){
//        if(i%4==0){
//            gbc.gridy++;
//            gbc.gridx=0;                
//        }

        this.numbers[i] = new JButton();
        this.numbers[i].setSize(45,45);
        this.numbers[i].setMaximumSize(new Dimension(45,45));
        this.numbers[i].setMinimumSize(new Dimension(45,45));
        this.numbers[i].addActionListener(fal);
//        if(i<10)
//            this.numbers[i].setText(String.valueOf(i));            
//        else{
            switch(i){
                case 0:
                    this.numbers[i].setText("Interface List");          
                    break;
                case 1:
                    this.numbers[i].setText("Wireless List");                                  
                    break;
                case 2:
                    this.numbers[i].setText("Access Points List");                                                          
                    break;
            }

        p.add(this.numbers[i], gbc);
        gbc.gridx++;
                    
    }
    
	query.setVisible(false);
	contentPane.add(query, BorderLayout.NORTH);
	contentPane.add(p, BorderLayout.NORTH);
	
	contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
	contentPane.add(msgline, BorderLayout.SOUTH);
    jm = new JMenu("File");
    jmitem1 = new JMenuItem("Credits");
    jmitem1.addActionListener(fal);
    jmitem2 = new JMenuItem("Exit");
    jmitem2.addActionListener(fal);
    jmb = new JMenuBar();
    jm.add(jmitem1);
    jm.add(jmitem2);
    jmb.add(jm);                
    this.setJMenuBar(jmb);
    

	// Now hook up the JTextField so that when the user types a query
	// and hits ENTER, the query results get displayed in the JTable
	query.addActionListener(new ActionListener() {
		// This method is invoked when the user hits ENTER in the field
		public void actionPerformed(ActionEvent e) {
		    // Get the user's query and pass to displayQueryResults()
		    displayQueryResults(query.getText());
		}
	    });
    }

    /**
     * This method uses the supplied SQL query string, and the 
     * ResultSetTableModelFactory object to create a TableModel that holds
     * the results of the database query.  It passes that TableModel to the
     * JTable component for display.
     **/
    public void displayQueryResults(final String q) {
	// It may take a while to get the results, so give the user some
	// immediate feedback that their query was accepted.
	msgline.setText("Contacting database...");
	
	// In order to allow the feedback message to be displayed, we don't
	// run the query directly, but instead place it on the event queue
	// to be run after all pending events and redisplays are done.
	EventQueue.invokeLater(new Runnable() {
		public void run() {
		    try {
			// This is the crux of it all.  Use the factory object
			// to obtain a TableModel object for the query results
			// and display that model in the JTable component.
			table.setModel(factory.getResultSetTableModel(q));
			// We're done, so clear the feedback message
			msgline.setText(" ");  
		    }
		    catch (SQLException ex) {
			// If something goes wrong, clear the message line
			msgline.setText(" ");
			// Then display the error in a dialog box
			JOptionPane.showMessageDialog(NetworkManagerGUI.this,
			          new String[] {  // Display a 2-line message
				      ex.getClass().getName() + ": ",
				      ex.getMessage()
				  });
		    }
		}
	    });
    }
}
