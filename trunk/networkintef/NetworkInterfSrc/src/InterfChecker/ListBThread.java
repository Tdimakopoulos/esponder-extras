package InterfChecker;

import java.io.BufferedReader;
import java.util.Date;
import java.util.Vector;

import InterfChecker.data.ListB;
import InterfChecker.data.ListC;


public class ListBThread extends Thread {

	private volatile boolean stop = false;//variable if thread needs to close--If true then we are stopping the thread
	private String info=" INFO ";
	static int state=1;
	protected  Date time1;//Xronos stin arxi tou iteration tou thread
	protected  Date time2;//Xronos meta tis leitourgies/elegxous tou thread
	protected  Date time3;//Xronos meta to timeout
	protected double T;//xronos twn metrisewn dil.time2-time1
	protected static double totalTime;//Synolikos xronos dil.time3-time1
	private boolean changed = false;//Metavliti gia to an vrikame allagi
	protected String cmdAccessPoints;//Metavliti gia to parsing tis entolis iwlist
	protected Boolean previousReadyPreviousAP;//Metavliti gia otan exoume ena AP etoimo na to kratismoume ston vector interfVectorC
	protected static Vector<ListC> addedInterfVectorC;//Holds previous Access Points 
	protected double timeout=Application.timeout;//To timeout pou tha kanei sleep to thread -- Arxikopoieitai stin timi tou property file
	protected int counterNoSuccess;//H metavliti gia tis anepitixeis epanalispeis
	int states;//H metavliti gia tis katastaseis
	protected static double changeRate;//Syntelestis metavolis x
	double totalTimeTimeout=Application.timeout;//To timeout pou yplogizei kai stelnei pisw h markov -- Arxikopoieitai stin timi tou property file
	
	public ListBThread(String str) {
		super(str);
	}
	
	  public String toString() {//print message
		    return "[Thread-" +getId()+ "] ";
		  }
	
	  public String monitorInterf() {//print message
		  return this+info+"- "+this.getName()+" -ListBThread- " +
		  		"State is "+(Application.states - states+1)+" Timeout is:"+timeout+"ms"+" Counter of No Success:"+counterNoSuccess+" Change Rate:"+changeRate;
		  }
	
	public void run() {
		this.states =Application.states; // Arxikopoieitai stin timi tou property file 
		this.counterNoSuccess=Application.counterNoSuccess;  // Arxikopoieitai stin timi tou property file
		changeRate=Application.changeRate; // Arxikopoieitai stin timi tou property file
		while(!stop) {//Otan to stop ginei true tha stamatisei
			try {
			System.out.println(monitorInterf());
			time1 = new Date();//Arxikos xronos
			InterfaceThread.interfVectorC= new Vector<ListC>();//Initializing Vector C
			previousReadyPreviousAP=false;//Arxikopoioume ti metavliti gia otan exoume ena AP etoimo na to kratismoume ston vector interfVectorC
			
			String command = "iwlist "+this.getName()+" scan";//
			BufferedReader iwlist = InterfaceThread.ExecuteLinuxCommand(command);//Executing the command
			
			generateListC(iwlist);//GDimiourgoume ti lista C
			
			iwlist.close();
			ischanged();//Elegxoume gia allages ston arithmo tvn AP
			addedInterfVectorC =new Vector<ListC>();
			if(InterfaceThread.interfVectorC!=null){//Otan exw vrei AP tote to grafw kai ston addedInterfVectorC
				for (int a = 0; a < InterfaceThread.interfVectorC.size(); a++){
					ListC test = InterfaceThread.interfVectorC.elementAt(a);
					addedInterfVectorC.add(test);
				}
			}
			
			time2 = new Date();//xronos meta apo leitourgies kai elegxous
			if(timeout<0)//An to timeout einai arnitiko tote pairnw tin arxiki timi apo to property file--H entoli iwlist ka8ysterei
				timeout=Application.timeout;
			
			sleep((int)(timeout));
			time3 = new Date();//Xronos meta to sleep
			timeout = this.markovSleepTime(time1,time2,time3,changed);//Calculating timeouts
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return;
			}
			if (stop)
			      System.out.println("Stoppping thread "+ this);
		}
	}


	//Method to stop thread
	  public void requestStop() {
		    stop = true;
	  }
	  
	  //Method to generate List for Access Points 
	  public void generateListC(BufferedReader iwList) throws Exception{
			ListC accessPoint = new ListC();	
                        int id=0;
			while((cmdAccessPoints = iwList.readLine())!=null) {//if next line exists
				if (cmdAccessPoints.contains("Cell")){
					if(previousReadyPreviousAP&&accessPoint.getAcPoMAC()!=null){//An einai etoimo to object to krataw ston interfVectorC
                                            accessPoint.setListCId(++id);
                                            accessPoint.setDevice(InterfaceThread.hostname);
                                            InterfaceThread.interfVectorC.add(accessPoint);
                                            previousReadyPreviousAP=false;
					}
					accessPoint = new ListC();
					String[] tokens=cmdAccessPoints.split(" ");//Split to tokens
					for (int i = 0; i < tokens.length; i++)
					{
						String var = tokens[i];
						if (var.contains("Address")){//If I found Address then get next token that contains MAC
							accessPoint.setAcPoMAC(tokens[i+1]);
						}
					}
				}
				else{
					String[] tokens=cmdAccessPoints.split(" ");//Split to tokens
					previousReadyPreviousAP=true;
					for (int i = 0; i < tokens.length; i++)
					{
						String var = tokens[i];
						if (var.contains("ESSID:")){//if the line contains the unique word Essid
							int index = cmdAccessPoints.indexOf("ESSID:\"");
							int indexEnd = cmdAccessPoints.lastIndexOf("\"");
							String essid = cmdAccessPoints.substring(index+7, indexEnd);
							accessPoint.setEssid(essid);
							for (int a = 0; a < InterfaceThread.interfVectorB.size(); a++){//Tha psaksoume sti Lista B to antistoixo wireless interface me to idio essid
								ListB test = InterfaceThread.interfVectorB.elementAt(a);
								if(accessPoint.getEssid().compareToIgnoreCase(test.getEssid())==0){//An einai idio kratame to channel
									test.setChannel(accessPoint.getChannel());
								}
							}
						}
						else if (var.contains("Channel:")){//if the line line contains the unique word channel
							int index = var.indexOf("Channel:");
							String channel = var.substring(index+8, var.length());

							accessPoint.setChannel(channel);
						}
						else if (var.contains("level=")){//if the line line contains the unique word level
							int index = var.indexOf("level=");
							String signalLevel = var.substring(index+6, var.length());
							accessPoint.setSignalLevel(signalLevel);
						}
						else if (var.contains("Mode:")){//if the line line contains the unique word mode
							int index = var.indexOf("Mode:");
							String mode = var.substring(index+5, var.length());
							accessPoint.setStatus(mode);
						}
					}
				}
			}
			if((iwList.readLine())==null&&accessPoint.getAcPoMAC()!=null){//An den exw alli grammi kai exw gemato object tote to grafw ston interfVectorC
                            accessPoint.setListCId(++id);
                            accessPoint.setDevice(InterfaceThread.hostname);
                            InterfaceThread.interfVectorC.add(accessPoint);
			}
		  
	  }
	  
	  	//Method Markov to calculate new timeout according state and counter of no success
		public double markovSleepTime(Date time1,Date time2,Date time3,boolean changed) throws Exception{
			
			if(time2!=null){//Tin prwti fora den exw parei xronous ara den ypologizw timeout
				long l1 = time1.getTime();
				long l2 = time2.getTime();
				long l3 = time3.getTime();
				T = (l2 - l1);
				totalTime = (l3 - l1);
				if(this.changed){//An vrw allages tote ta arxikopoiw ola
                                    InterfaceThread.dataCheckerThread.changed=this.changed;
					this.states=Application.states;
					this.totalTimeTimeout =Double.parseDouble(Application.sleepTime.trim())-T;//Afairw xrono leitoourgias/elegxwn 
					this.changed=false;
					this.counterNoSuccess=Application.counterNoSuccess;
					return this.totalTimeTimeout;
				}
				else{//An den vrw allages,tote meiwnw to metriti anepityxwn epanalipsewn(c)mexri na ftasei sto 1--Otan ftasei sto 1 ypologizw to neo timeout 			
					if(this.counterNoSuccess>1){
						this.counterNoSuccess--;
						this.totalTimeTimeout=(((Application.states-this.states+1)*Application.timeout)-T);
					}
					else if(this.counterNoSuccess==1){
						if(this.states>0){
							this.states--;
							this.totalTimeTimeout=(((Application.states-this.states+1)*Application.timeout)-T);
							this.counterNoSuccess=Application.counterNoSuccess;
						}
					}
				}
			}
			return this.totalTimeTimeout;
		}
		
	  
		//An exw allages
		public void ischanged() throws Exception
		{
			if((InterfaceThread.interfVectorC!=null)&& (addedInterfVectorC!=null)){//Checking size of new vector and current vector C
				if(InterfaceThread.interfVectorC.size()!=addedInterfVectorC.size()){
					System.out.println(this.getName()+" ACCESS POINTS CHANGED - INITIALIZING STATE AND TIMEOUT");
					this.changed=true;
                                        InterfaceThread.dataCheckerThread.changed=this.changed;
				}
			}
			else {
				this.changed=false;
                                InterfaceThread.dataCheckerThread.changed=this.changed;
                        }
		}
}
