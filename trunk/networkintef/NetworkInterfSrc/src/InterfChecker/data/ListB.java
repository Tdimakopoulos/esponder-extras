package InterfChecker.data;

//Attributes for ListB

import javax.xml.bind.annotation.XmlElement;

public class ListB {
	protected int listBId;
        private int interfId;
	protected String name;
	protected String macAddressAP;
	protected String essid;
	protected String channel;
	protected String status;
	protected String txPower;
	protected String linkQuality;
	protected String signalLevel;
	protected String linkLevelNoise;
	protected String discardedPackets;

	public ListB() {

	}
	
	public int getListBId() {
		return listBId;
	}


	public void setListBId(int listBId) {
		this.listBId = listBId;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEssid() {
		return essid;
	}

	public void setEssid(String essid) {
		this.essid = essid;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getDiscardedPackets() {
		return discardedPackets;
	}

	public void setDiscardedPackets(String discardedPackets) {
		this.discardedPackets = discardedPackets;
	}

	public String getLinkLevelNoise() {
		return linkLevelNoise;
	}

	public String getLinkQuality() {
		return linkQuality;
	}

	public void setLinkQuality(String linkQuality) {
		this.linkQuality = linkQuality;
	}

	public String getSignalLevel() {
		return signalLevel;
	}

	public void setSignalLevel(String signalLevel) {
		this.signalLevel = signalLevel;
	}

	public void setLinkLevelNoise(String linkLevelNoise) {
		this.linkLevelNoise = linkLevelNoise;
	}

	public String getMacAddressAP() {
		return macAddressAP;
	}

	public void setMacAddressAP(String macAddress) {
		this.macAddressAP = macAddress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTxPower() {
		return txPower;
	}

	public void setTxPower(String txPower) {
		this.txPower = txPower;
	}

    /**
     * @return the interfId
     */
    public int getInterfId() {
        return interfId;
    }

    /**
     * @param interfId the interfId to set
     */
    public void setInterfId(int interfId) {
        this.interfId = interfId;
    }
	
	
	
	
}
