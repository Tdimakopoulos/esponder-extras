package InterfChecker.data;

import java.util.Vector;


public class MonitorData {
	
	protected Vector<ListA> interfVectorA;//Holds all available Interfaces
	protected Vector<ListB> interfVectorB;//Holds all available Wireless Interfaces
	protected Vector<ListC> interfVectorC;//Holds all available Access Points
	private Vector<Interf> VectorInterf;
        
	public Vector<ListA> getInterfVectorA() {
		return interfVectorA;
	}
	public void setInterfVectorA(Vector<ListA> interfVectorA) {
		this.interfVectorA = interfVectorA;
	}
	public Vector<ListB> getInterfVectorB() {
		return interfVectorB;
	}
	public void setInterfVectorB(Vector<ListB> interfVectorB) {
		this.interfVectorB = interfVectorB;
	}
	public Vector<ListC> getInterfVectorC() {
		return interfVectorC;
	}
	public void setInterfVectorC(Vector<ListC> interfVectorC) {
		this.interfVectorC = interfVectorC;
	}
	
	public MonitorData() {

	}

    /**
     * @return the VectorInterf
     */
    public Vector<Interf> getVectorInterf() {
        return VectorInterf;
    }

    /**
     * @param VectorInterf the VectorInterf to set
     */
    public void setVectorInterf(Vector<Interf> VectorInterf) {
        this.VectorInterf = VectorInterf;
    }
	
}
