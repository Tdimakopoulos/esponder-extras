package InterfChecker;

import InterfChecker.data.Interf;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Random;
import java.util.Vector;

import InterfChecker.data.ListA;
import InterfChecker.data.ListB;
import InterfChecker.data.ListC;
import InterfChecker.data.MonitorData;

public class InterfaceThread extends Thread {

    private volatile boolean keepOn = true;//variable if thread needs to close--If true then we are stopping the thread
    protected String cmdout;//Metavliti gia to parsing tis entolis ifconfig
    protected String cmdRouteOut;//Metavliti gia to parsing tis entolis route
    protected String cmdWirelessOut;//Metavliti gia to parsing tis entolis iwconfig
    protected String cmdCatWireless;//Metavliti gia to parsing tou arxeiou wireless
    protected String cmdEthTool;//Metavliti gia to parsing tis entolis ethtool
    protected String info = " INFO ";
    protected Boolean previousReady;//Metavliti gia otan exoume ena Interface etoimo na to kratismoume ston vector interfVectorA
    protected Boolean previousReadyPreviousWireless;//Metavliti gia otan exoume ena Wireless Interface etoimo na to kratismoume ston vector interfVectorB
    protected static Vector<ListA> interfVectorA;//Holds all available Interfaces
    protected static Vector<ListB> interfVectorB;//Holds all available Wireless Interfaces
    protected static Vector<ListC> interfVectorC;//Holds all available Access Points
    protected static Vector<ListAThread> threadListA = new Vector<ListAThread>();//Holds active ListAThread 
    protected static Vector<ListBThread> threadListB = new Vector<ListBThread>();//Holds active ListBThread
    protected static Vector<Interf> VectorInterf;
    protected static DataCheckerThread dataCheckerThread;//Holds active ListBThread
    protected static Vector<ListA> activeListA = new Vector<ListA>();//Holds active ListA Interfaces 
    protected static Vector<ListB> activeListB = new Vector<ListB>();//Holds active ListB Interfaces 
    protected static MonitorData monitorData;
    protected static String hostname;
    protected String deviceName;
    protected boolean found;
    protected static boolean changed = false;//Metavliti gia to an vrikame allagi
    protected boolean changedB = false;
    //parameters from property file
    protected double timeout = Application.timeout;//To timeout pou tha kanei sleep to thread -- Arxikopoieitai stin timi tou property file
    protected int counterNoSuccess;//H metavliti gia tis anepitixeis epanalispeis
    int states;//H metavliti gia tis katastaseis
    protected static double changeRate;//Syntelestis metavolis x
    int interfaceCount = 0;
    protected Date time1;//Xronos stin arxi tou iteration tou thread
    protected Date time2;//Xronos meta tis leitourgies/elegxous tou thread
    protected Date time3;//Xronos meta to timeout
    int k;
    int c;
    double totalTimeTimeout = Application.timeout;//To timeout pou yplogizei kai stelnei pisw h markov -- Arxikopoieitai stin timi tou property file
    protected double T;//xronos twn metrisewn dil.time2-time1
    protected static double totalTime;//Synolikos xronos dil.time3-time1
    int state = 1;
    int counter = 0;

    //to catch Control-C
    public class RunWhenShuttingDown extends Thread {

        public synchronized void run() {
            System.out.println("Control-C caught. Shutting down...");
            System.out.println("Stoppping thread " + this);
            keepOn = false;
            try {
                for (int k = 0; k < threadListA.size() && !found; k++) {//Iterating Thread List A to terminate active threads A
                    ListAThread threadToTest = threadListA.elementAt(k);
                    threadToTest.requestStop();//To stop thread
                    System.out.println("Threads Online before closing= " + threadToTest.getName());
                }
                for (int k = 0; k < threadListB.size() && !found; k++) {//Iterating Thread List B to terminate active threads B
                    ListBThread threadToTest = threadListB.elementAt(k);
                    threadToTest.requestStop();//To stop thread
                    System.out.println("Threads Online before closing= " + threadToTest.getName());
                }
                if (dataCheckerThread != null) {
                    dataCheckerThread.requestStop();
                    System.out.println("Data checker Thread closing");
                }
                Thread.sleep(2000);//Sleeping for 2000ms 
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public InterfaceThread(String str) {
        super(str + "-0");
    }

    public String toString() {
        return "[" + getName() + "] ";
    }

    public String monitorInterf() {
        return this + info + " -MainThread- State is " + (Application.states - this.states + 1) + " Timeout is:" + timeout + "ms" + " Counter of No Success:" + counterNoSuccess + " Change Rate:" + changeRate;
    }

    public void run() {
        this.states = Application.states;
        this.counterNoSuccess = Application.counterNoSuccess;
        changeRate = Application.changeRate;

        String monitoringThread = "Initializing Monitoring thread";
        Runtime.getRuntime().addShutdownHook(new RunWhenShuttingDown());//To catch Ctrl-C
        System.out.println(this + info + monitoringThread);
        dataCheckerThread = new DataCheckerThread("DataChecker");
        dataCheckerThread.start();//Starts DataChecker Thread
        monitorData = new MonitorData();
        while (keepOn) {
            try {
                time1 = new Date();//Initializing time1

                System.out.println(monitorInterf());

                interfVectorA = new Vector<ListA>();
                VectorInterf = new Vector<Interf>();

                String command = "ifconfig";
                BufferedReader ifConfig = ExecuteLinuxCommand(command);//Executing Command
                command = "route -n";
                BufferedReader route = ExecuteLinuxCommand(command);//Executing Command
                command = "iwconfig";
                BufferedReader iwConfig = ExecuteLinuxCommand(command);//Executing Command
                command = "cat /proc/net/wireless";
                BufferedReader catWireless = ExecuteLinuxCommand(command);//Executing Command
                command = "hostname";
                BufferedReader hostnameCmd = ExecuteLinuxCommand(command);//Executing Command

                while ((cmdout = hostnameCmd.readLine()) != null) {
                    String[] tokens = cmdout.split(" ");
                    for (int i = 0; i < tokens.length; i++) {
                        hostname = tokens[i];
                    }
                }

                previousReady = false;//Inializing flags needed to add to vector
                previousReadyPreviousWireless = false;

                generateListA(ifConfig, route);//Generating List A
                generateListB(iwConfig, catWireless);//Generating List B

                ifConfig.close();
                route.close();
                iwConfig.close();
                catWireless.close();

                checkActiveInterfacesListA_B();//check if interface in ListA and ListB exist-If not stop

                //checking if Interface has started a thread so not to have duplicates
                for (int a = 0; a < interfVectorA.size(); a++) {
                    ListA test = interfVectorA.elementAt(a);
                    boolean found = false;
                    for (int k = 0; k < threadListA.size() && !found; k++) {
                        ListAThread threadToTest = threadListA.elementAt(k);
                        if (threadToTest.getName().compareToIgnoreCase(test.getName()) == 0) {
                            found = true;
                        }
                    }
                    if (!found) {//if not found then start new thread
                        ListAThread listAThread = new ListAThread(test.getName());
                        listAThread.start();
                        threadListA.add(listAThread);//Add thread to active thread vector
                        activeListA.add(test);//Add ListA Object to active ListA vector
                        System.out.println("INTERFACE NUMBER CHANGED - INITIALIZING STATE AND TIMEOUT");
                        changed = true;
                        InterfaceThread.dataCheckerThread.changed = changed;
                    }
                }
                int rnd = 0;//To pick a random wireless 
                ListB interf_picked = new ListB();
                if (threadListB.size() == 0 && interfVectorB.size() > 0) {//Iterating vector of wireless List B
                    if (interfVectorB.size() > 1) {//If more than one wireless
                        for (int k = 0; k < interfVectorB.size(); k++) {
                            Random generator = new Random();
                            rnd = generator.nextInt(interfVectorB.size() - 1);
                            interf_picked = interfVectorB.elementAt(rnd);//
                        }
                    } else if (interfVectorB.size() == 1) {//Else show message and pick that one
                        System.out.println("ERROR. Found 1 wireless interface. Successful execution requires at least 2. Execution will continue "
                                + "yet results are not guaranteed.");
                        interf_picked = interfVectorB.elementAt(rnd);
                    }
                    ListBThread listBThread = new ListBThread(interf_picked.getName());

                    listBThread.start();
                    threadListB.add(listBThread);
                    activeListB.add(interf_picked);
                    System.out.println("INTERFACE NUMBER CHANGED - INITIALIZING STATE AND TIMEOUT");
                    changed = true;
                    InterfaceThread.dataCheckerThread.changed = changed;
                }
                time2 = new Date();//xronos meta apo leitourgies kai elegxous
                if (timeout < 0) {
                    timeout = Application.timeout;
                }
                sleep((int) (timeout));

                printListA();//Printing List A
                printListB();//Printing List B

                if (InterfaceThread.interfVectorC != null) {//If list C has an element
                    printListC();//Printing List C
                }

                time3 = new Date();//Xronos meta to sleep
                timeout = this.markovSleepTime(time1, time2, time3, changed);//Calculating Sleep tim

            } catch (InterruptedException e) {
                System.out.println("Ended...");
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }

    //Method to execute Linux Commands
    public static BufferedReader ExecuteLinuxCommand(String command) throws Exception {
        Process proc = Runtime.getRuntime().exec(command);
        proc.waitFor();
        InputStream istr = proc.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(istr));
        return br;
    }

    //Method Markov to calculate new timeout according state and counter of no success
    public double markovSleepTime(Date time1, Date time2, Date time3, boolean changed) throws Exception {
        if (time2 != null) {//Tin prwti fora den exw parei xronous ara den ypologizw timeout
            long l1 = time1.getTime();
            long l2 = time2.getTime();
            long l3 = time3.getTime();
            T = (l2 - l1);
            totalTime = (l3 - l1);
            if (changed) {//An vrw allages tote ta arxikopoiw ola
                InterfaceThread.dataCheckerThread.changed = changed;
                this.states = Application.states;
                this.totalTimeTimeout = Double.parseDouble(Application.sleepTime.trim()) - T;//Afairw xrono leitoourgias/elegxwn
                this.changed = false;
                this.counterNoSuccess = Application.counterNoSuccess;
                return this.totalTimeTimeout;
            } else {//An den vrw allages,tote meiwnw to metriti anepityxwn epanalipsewn(c)mexri na ftasei sto 1--Otan ftasei sto 1 ypologizw to neo timeout 					
                if (this.counterNoSuccess > 1) {
                    this.counterNoSuccess--;
                    this.totalTimeTimeout = (((Application.states - this.states + 1) * Application.timeout) - T);
                } else if (counterNoSuccess == 1) {
                    if (this.states > 0) {
                        this.states--;
                        this.totalTimeTimeout = (((Application.states - this.states + 1) * Application.timeout) - T);
                        this.counterNoSuccess = Application.counterNoSuccess;
                    }
                }
            }
        }
        return this.totalTimeTimeout;
    }

    //Method to check if interface in ListA and ListB exist-If not requestStop
    public void checkActiveInterfacesListA_B() throws Exception {
        boolean exists = false;

        for (int k = 0; k < threadListA.size(); k++) {//Iterating Thread listA
            exists = false;
            ListAThread threadToTest = threadListA.elementAt(k);
            for (int i = 0; i < interfVectorA.size(); i++) {

                ListA test = interfVectorA.elementAt(i);
                if (threadToTest.getName().compareToIgnoreCase(test.getName()) == 0) {
                    exists = true;//Element Found
                }
            }
            if (!exists) {//If not found need to stop
                for (int i = 0; i < threadListB.size(); i++) {//Iterating Thread listB
                    ListBThread threadToRemove = threadListB.elementAt(i);
                    if (threadToTest.getName().compareToIgnoreCase(threadToRemove.getName()) == 0) {
                        threadToRemove.requestStop();//stopping thread B
                        threadListB.removeElementAt(i);//removing thread A
                        activeListB.removeElementAt(i);//removing thread A
                    }
                }
                threadToTest.requestStop();//stopping thread A
                threadListA.removeElementAt(k);//removing thread B
                activeListA.removeElementAt(k);//removing thread B
                System.out.println("INTERFACE NUMBER CHANGED - INITIALIZING STATE AND TIMEOUT");
                changed = true;//Change state-we hava a change
                InterfaceThread.dataCheckerThread.changed = changed;
            }
        }
    }

    //Method to generate ListA for all interfaces
    public void generateListA(BufferedReader ifConfig, BufferedReader route) throws Exception {
        ListA interf = new ListA();
        int id = 0;
        while ((cmdout = ifConfig.readLine()) != null) {//if next line exists
            //interf.setListAId(id);
            if (cmdout.contains("encap")) {//The first line contains the unique word encap 
                if (previousReady) {//If ready to be written
                    interf.setListAId(++id);
                    interf.setInterfId(interf.getListAId());
                    interfVectorA.add(interf);//Add to vector A
                    if (interf.getName().contains("eth0")) {
                        if (interf.getIpAddress() != null) {
                            deviceName = interf.getIpAddress() + " " + hostname;
                        } else {
                            deviceName = hostname;
                        }

                    }
                    Interf vInterf = new Interf();
                    vInterf.setInterfId(id);
                    vInterf.setDevice(hostname);
                    vInterf.setName(interf.getName());
                    VectorInterf.add(vInterf);

                    previousReady = false;
                }
                interf = new ListA();
                String[] tokens = cmdout.split(" ");
                for (int i = 0; i < tokens.length; i++) {
                    String var = tokens[i];
                    interf.setName(tokens[0]);
                    while ((cmdRouteOut = route.readLine()) != null) {//getting gateway from comand route
                        if (cmdRouteOut.contains(tokens[0])) {
                            String[] tokensRoute = cmdRouteOut.split(" ");
                            if (tokensRoute[0].equalsIgnoreCase("0.0.0.0")) {
                                int k = 1;
                                while (tokensRoute[k].equalsIgnoreCase("")) {
                                    k++;
                                }
                                interf.setGateway(tokensRoute[k]);
                            }
                        }
                    }
                    String command = "sudo ethtool " + tokens[0];//command to get speed of ethernet card
                    BufferedReader ethtool = ExecuteLinuxCommand(command);
                    interf.setMaxTxRate(Double.valueOf(Application.maxRate));//Setting max Rate from property file
                    while ((cmdEthTool = ethtool.readLine()) != null) {
                        if (cmdEthTool.contains("Speed:")) {//if the line line contains the unique word speed
                            String[] tokensEthtool = cmdEthTool.split(" ");
                            for (int a = 0; a < tokensEthtool.length; a++) {
                                String speerVar = tokensEthtool[a];
                                if (speerVar.contains("Speed:")) {
                                    String maxspeed = tokensEthtool[a + 1];
                                    int o = 0;
                                    char character;
                                    String speedonly = "";
                                    for (o = 0; o < maxspeed.length(); o++) {//We need only numbers
                                        character = maxspeed.charAt(o);
                                        if (character >= '0' && character <= '9') {
                                            speedonly = speedonly + character;
                                        }
                                    }
                                    interf.setMaxTxRate(Double.valueOf(speedonly));
                                }
                            }
                        }
                    }
                    if (var.contains("HWaddr")) {//if the line line contains the unique word HWaddr 
                        interf.setMacAddress(tokens[i + 1]);
                    }
                }
            } else {
                String[] tokens = cmdout.split(" ");
                previousReady = true;
                for (int i = 0; i < tokens.length; i++) {
                    String var = tokens[i];

                    if (var.contains("addr:") && cmdout.contains("inet addr:")) {//if the line line contains the words inet addr then get ip						
                        int index = var.indexOf("addr:");
                        String ipAddress = var.substring(index + 5, var.length());
                        interf.setIpAddress(ipAddress);
                    } else if (var.contains("Mask:")) {//if the line line contains the unique word Mask 
                        int index = var.indexOf("Mask:");
                        String mask = var.substring(index + 5, var.length());
                        interf.setNetMask(mask);
                        String ipAddress = interf.getIpAddress();
                        if (ipAddress != null) {
                            String[] ipArray = ipAddress.split("\\.");
                            String ipBinary = "";
                            for (String string : ipArray) {//To get binary representation of IP address
                                int binary = Integer.parseInt(string);
                                String binaryString = Integer.toBinaryString(binary);
                                String zeroes = "";
                                for (int b = 0; b < 8 - binaryString.length(); b++) {
                                    zeroes = zeroes + "0";
                                }
                                ipBinary = ipBinary + zeroes + binaryString + ".";
                            }
                            ipBinary = ipBinary.substring(0, ipBinary.length() - 1);

                            String[] maskArray = mask.split("\\.");
                            String maskBinary = "";
                            for (String string : maskArray) {//To get binary representation of subnet mask
                                int binary = Integer.parseInt(string);
                                String binaryOctet = Integer.toBinaryString(binary);
                                String zeroes = "";
                                for (int c = 0; c < 8 - binaryOctet.length(); c++) {
                                    zeroes = zeroes + "0";
                                }
                                maskBinary = maskBinary + zeroes + binaryOctet + ".";
                            }
                            maskBinary = maskBinary.substring(0, maskBinary.length() - 1);

                            String[] ipTokens = ipBinary.split("");
                            String[] maskTokens = maskBinary.split("");
                            String NetworkAddress = "";
                            for (int tok = 0; tok < ipTokens.length; tok++) {//Adding binary representation of ip and mask to get network address
                                String tocheckIp = ipTokens[tok];
                                String tocheckmask = maskTokens[tok];
                                if (tocheckIp.equalsIgnoreCase("1") && tocheckmask.equalsIgnoreCase("1")) {
                                    NetworkAddress = NetworkAddress + "1";
                                } else if (tocheckIp.equalsIgnoreCase(".")) {
                                    NetworkAddress = NetworkAddress + ".";
                                } else {
                                    NetworkAddress = NetworkAddress + "0";
                                }
                            }
                            String[] netwArray = NetworkAddress.split("\\.");
                            String netAddress = "";
                            for (String string : netwArray) {
                                int net = Integer.parseInt(string, 2);//2*for*binaryb
                                netAddress = netAddress + Integer.toString(net) + ".";
                            }
                            netAddress = netAddress.substring(0, netAddress.length() - 1);
                            interf.setNetAddress(netAddress);
                        }
                    } else if (var.contains("Bcast:")) {//if the line line contains the unique word Bcast
                        int index = var.indexOf("Bcast:");
                        String broadcast = var.substring(index + 6, var.length());
                        interf.setBroadcastAddress(broadcast);
                    } else if (var.contains("packets:")) {
                        if (tokens[i - 1].equalsIgnoreCase("RX")) {
                            int index = var.indexOf("packets:");
                            String packetsRx = var.substring(index + 8, var.length());
                            interf.setRxPackets(Integer.parseInt(packetsRx));
                        } else if (tokens[i - 1].equalsIgnoreCase("TX")) {
                            int index = var.indexOf("packets:");
                            String packetsTx = var.substring(index + 8, var.length());
                            interf.setTxPackets(Integer.parseInt(packetsTx));
                        }
                    } else if (var.contains("errors:")) {
                        if (cmdout.contains("RX")) {
                            int index = var.indexOf("errors:");
                            String errorsRx = var.substring(index + 7, var.length());
                            interf.setRxErrors(Integer.parseInt(errorsRx));
                        } else if (cmdout.contains("TX")) {
                            int index = var.indexOf("errors:");
                            String errorsTx = var.substring(index + 7, var.length());
                            interf.setTxErrors(Integer.parseInt(errorsTx));
                            int totalpackets = interf.getTxPackets() + interf.getRxPackets();
                            int totalerrors = interf.getTxErrors() + interf.getRxErrors();
                            //System.out.println("--------------totalpackets*"+totalpackets);
                            //System.out.println("--------------totalerrors*"+totalerrors);
                            DecimalFormat twoDForm = new DecimalFormat("#.##");//To get two decimals

                            String value="0";
                            
                            if(totalpackets>0)                            
                                value = twoDForm.format(totalerrors / totalpackets);

                            interf.setPacketErrorRate(Double.parseDouble(value));
                        }
                    } else if (var.contains("bytes:")) {//Calculating rates
                        if (tokens[i - 1].equalsIgnoreCase("RX")) {
                            int indexRx = var.indexOf("bytes:");
                            String rxBytes = var.substring(indexRx + 6, var.length());
                            interf.setRxBytes(Double.parseDouble(rxBytes.trim()));//Getting rx Bytes
                        } else if (tokens[i - 1].equalsIgnoreCase("TX")) {
                            int indexTx = var.indexOf("bytes:");
                            String txBytes = var.substring(indexTx + 6, var.length());
                            interf.setTxBytes(Double.parseDouble(txBytes.trim()));//Getting tx Bytes
                        }
                        if ((interf.getTxBytes() != null) && (interf.getRxBytes() != null) && (totalTime > 0)) {

                            double txkbps = interf.getTxBytes();
                            double rxkbps = interf.getRxBytes();
                            //System.out.println("--------------totalpackets*"+txkbps);
                            //System.out.println("--------------totalpackets*"+rxkbps);
                            double currentTxRate = ((rxkbps - txkbps) * 8.0 / 1024.0 / 1024.0) / (totalTime / 1000);

                            DecimalFormat twoDForm = new DecimalFormat("#.##");//To get two decimals

                            String value = twoDForm.format(currentTxRate);
                            String curRate = "";
                            String[] rateTokens = value.split("");
                            for (String string : rateTokens) {//
                                if (string.equalsIgnoreCase(",")) {
                                    curRate = curRate + ".";
                                } else {
                                    curRate = curRate + string;
                                }
                            }
                            interf.setCurTxRate(Double.parseDouble(curRate));

                            double currentPercRate = (currentTxRate) * 100 / (interf.getMaxTxRate());
                            String value2 = twoDForm.format(currentPercRate);
                            String curPerc = "";
                            String[] percRateTokens = value2.split("");
                            for (String string : percRateTokens) {//
                                if (string.equalsIgnoreCase(",")) {
                                    curPerc = curPerc + ".";
                                } else {
                                    curPerc = curPerc + string;
                                }
                            }
                            interf.setCurPercLineRate(Double.parseDouble(curPerc));

                        }
                    }
                }
            }
        }
        if ((ifConfig.readLine()) == null && interf.getName() != null) {//To add last line if i don't have a line and I have the object full
            interf.setListAId(++id);
            interf.setInterfId(interf.getListAId());
            interfVectorA.add(interf);
            if (interf.getName().contains("eth0")) {
                if (interf.getIpAddress() != null) {
                    deviceName = interf.getIpAddress() + " " + hostname;
                } else {
                    deviceName = hostname;
                }

            }
            Interf vInterf = new Interf();
            vInterf.setInterfId(id);
            vInterf.setDevice(hostname);
            vInterf.setName(interf.getName());
            VectorInterf.add(vInterf);
        }
    }

    //Method to generate ListA for wireless interfaces
    public void generateListB(BufferedReader iwConfig, BufferedReader catWireless) throws Exception {
        interfVectorB = new Vector<ListB>();
        ListB wirelessinterf = new ListB();
        int id = 0;
        while ((cmdWirelessOut = iwConfig.readLine()) != null) {
            if (cmdWirelessOut.contains("ESSID")) {//if the line line contains the unique word ESSID
                if (previousReadyPreviousWireless) {
                    wirelessinterf.setListBId(++id);
                    for (int a = 0; a < interfVectorA.size(); a++) {
                        ListA test = interfVectorA.elementAt(a);
                        if (test.getName().compareToIgnoreCase(wirelessinterf.getName()) == 0) {
                            wirelessinterf.setInterfId(test.getInterfId());
                            interfVectorB.add(wirelessinterf);
                        }
                    }
                    previousReadyPreviousWireless = false;
                }
                String[] tokens = cmdWirelessOut.split(" ");
                for (int i = 0; i < tokens.length; i++) {
                    String var = tokens[i];
                    wirelessinterf.setName(tokens[0]);
                    while ((cmdCatWireless = catWireless.readLine()) != null) {//parsing file wireless
                        if (cmdCatWireless.contains(tokens[0])) {
                            String[] tokensWir = cmdCatWireless.split(" ");//Splitting file
                            Vector<String> tokensNoWhiteSpaces = new Vector<String>();
                            for (int b = 0; b < tokensWir.length; b++) {
                                if (!tokensWir[b].equalsIgnoreCase("")) {
                                    tokensNoWhiteSpaces.add(tokensWir[b]);//if it contains spaces then add to a vector without spaces
                                }
                            }
                            wirelessinterf.setLinkLevelNoise(tokensNoWhiteSpaces.elementAt(4));//Element 4 is Link Level Noise
                            int discarded = 0;
                            for (int j = 5; j <= 9; j++) {//Element from 5 to 9 are discarded packets
                                discarded = discarded + Integer.parseInt(tokensNoWhiteSpaces.elementAt(j));
                            }
                            wirelessinterf.setDiscardedPackets("" + discarded);
//							for (int a = 0; a < InterfaceThread.interfVectorA.size(); a++){
//								ListA test = InterfaceThread.interfVectorA.elementAt(a);
//								if(wirelessinterf.getName().compareToIgnoreCase(test.getName())==0){
//									test.setPacketErrorRate(Double.parseDouble(""+discarded));
//								}
//							}
                        }
                    }
                    if (var.contains("ESSID:") && !cmdWirelessOut.contains("off")) {//setting wireless essid
                        int index = cmdWirelessOut.indexOf("ESSID:\"");
                        int indexEnd = cmdWirelessOut.lastIndexOf("\"");
                        String essid = cmdWirelessOut.substring(index + 7, indexEnd);
                        wirelessinterf.setEssid(essid);
                    }
                }
            } else {
                previousReadyPreviousWireless = true;
                String[] tokens = cmdWirelessOut.split(" ");
                for (int i = 0; i < tokens.length; i++) {
                    String var = tokens[i];
                    if (var.contains("Mode:")) {//if the line line contains the unique word mode
                        int index = var.indexOf("Mode:");
                        String mode = var.substring(index + 5, var.length());
                        wirelessinterf.setStatus(mode);
                    } else if (var.contains("Point:")) {
                        if (cmdWirelessOut.contains("Not-Associated")) {//if the line line contains the unique word Not-Associated
                            wirelessinterf.setMacAddressAP("Not-Associated");
                            wirelessinterf.setEssid("Not-Associated");
                            wirelessinterf.setChannel("Not-Associated");
                            wirelessinterf.setStatus("Not-Associated");
                        } else {
                            wirelessinterf.setMacAddressAP(tokens[i + 1]);
                        }
                    } else if (var.contains("Power=")) {//if the line line contains the unique word power
                        int index = var.indexOf("Power=");
                        String txPower = var.substring(index + 6, var.length());
                        wirelessinterf.setTxPower(txPower);
                    } else if (var.contains("Quality=")) {//if the line line contains the unique word quality
                        int index = var.indexOf("Quality=");
                        String linkQuality = var.substring(index + 8, var.length());
                        wirelessinterf.setLinkQuality(linkQuality);
                    } else if (var.contains("level=")) {//if the line line contains the unique word level
                        int index = var.indexOf("level=");
                        String signalLevel = var.substring(index + 6, var.length());
                        wirelessinterf.setSignalLevel(signalLevel);
                    }
                }
            }
        }

        if ((iwConfig.readLine()) == null && wirelessinterf.getName() != null && previousReadyPreviousWireless) {//To add last line if i don't have a line and I have the object full
            for (int a = 0; a < interfVectorA.size(); a++) {
                ListA test = interfVectorA.elementAt(a);
                if (test.getName().compareToIgnoreCase(wirelessinterf.getName()) == 0) {
                    wirelessinterf.setInterfId(test.getInterfId());
                    wirelessinterf.setListBId(++id);
                    interfVectorB.add(wirelessinterf);
                }
            }
        }

    }

    //Method to print Interfaces in ListA
    public void printListA() {
        System.out.println("||--Interface List(ListA)--||");
        System.out.println("||--ListA Size--||" + InterfaceThread.interfVectorA.size());
        for (int a = 0; a < InterfaceThread.interfVectorA.size(); a++) {//Iterating Vestor A
            ListA test = InterfaceThread.interfVectorA.elementAt(a);
            System.out.println("Interface:" + test.getListAId() + " IntID " + test.getInterfId() + " Name=" + test.getName() + " |MAC=" + test.getMacAddress()
                    + " |IP=" + test.getIpAddress() + " |Mask=" + test.getNetMask()
                    + " |Network Address=" + test.getNetAddress() + " |Broadcast Address=" + test.getBroadcastAddress()
                    + " |Gateway=" + test.getGateway() + " |Max Rate=" + test.getMaxTxRate() + "mbps"
                    + " |Tx Bytes=" + test.getTxBytes() + " |Rx Bytes=" + test.getRxBytes()
                    + " |Current Rate=" + test.getCurTxRate() + "mbps" + " |Ratio of Current Rate/Max Rate=" + test.getCurPercLineRate() + "% "
                    + " |Packet Error Rate=" + test.getPacketErrorRate() + "% ");
        }
        System.out.println("||-------------------------||");
    }

    //Method to print Wireless interfaces in ListB
    public void printListB() {
        System.out.println("||--Wireless Interfaces List(ListB)--||");
        System.out.println("||--ListB Size--||" + InterfaceThread.interfVectorB.size());
        for (int a = 0; a < InterfaceThread.interfVectorB.size(); a++) {//Iterating Vestor B
            ListB test = InterfaceThread.interfVectorB.elementAt(a);
            System.out.println("Wireless Interface:" + test.getListBId() + " IntID " + test.getInterfId() + " |Name=" + test.getName() + " |MAC=" + test.getMacAddressAP()
                    + " |ESSID=" + test.getEssid() + " |Channel=" + test.getChannel()
                    + " |Mode=" + test.getStatus() + " |Tx Power=" + test.getTxPower()
                    + " |Link Quality=" + test.getLinkQuality() + " |Signal Level=" + test.getSignalLevel()
                    + " |Noise=" + test.getLinkLevelNoise() + " |Discarded packets=" + test.getDiscardedPackets());
        }
        System.out.println("||-----------------------------------||");
    }

    //Method to print Access Points in ListC
    public void printListC() {
        System.out.println("||--Access Points List(ListC)--||");
        System.out.println("||--ListC Size--||" + InterfaceThread.interfVectorC.size());
        for (int a = 0; a < InterfaceThread.interfVectorC.size(); a++) {//Iterating Vestor C
            ListC test = InterfaceThread.interfVectorC.elementAt(a);
            System.out.println("Access Point:" + test.getListCId() + " |MAC=" + test.getAcPoMAC()
                    + " |ESSID=" + test.getEssid() + " |Channel=" + test.getChannel()
                    + " |Mode=" + test.getStatus() + " |Signal Level=" + test.getSignalLevel());
        }
        System.out.println("||-----------------------------||");
    }

}
