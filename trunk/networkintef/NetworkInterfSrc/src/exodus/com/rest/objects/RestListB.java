package exodus.com.rest.objects;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * XML Annotate Pojo to use for passing object into the web service
 * The class is annotated as XMLRootElement
 * And each class propertie as XMLElement
 * 
 */
@XmlRootElement
public class RestListB {

    @XmlElement
    public int listBId;
    @XmlElement public int interfId;
    
    @XmlElement
    public String macAddressAP;
    @XmlElement
    public String essid;
    @XmlElement
    public String channel;
    @XmlElement
    public String accessPointStatus;
    @XmlElement
    public String txPower;
    @XmlElement
    public String linkQuality;
    @XmlElement
    public String signalLevel;
    @XmlElement
    public String linkLevelNoise;
    @XmlElement
    public String discardedPackets;
}
