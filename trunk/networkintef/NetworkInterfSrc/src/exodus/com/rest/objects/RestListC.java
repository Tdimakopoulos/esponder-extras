package exodus.com.rest.objects;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * XML Annotate Pojo to use for passing object into the web service
 * The class is annotated as XMLRootElement
 * And each class propertie as XMLElement
 * 
 */
@XmlRootElement
public class RestListC {

    @XmlElement
    public int listCId;
    @XmlElement
    public String AcPoMAC;
    @XmlElement
    public String Essid;
    @XmlElement
    public String Channel;
    @XmlElement
    public String Status;
    @XmlElement
    public String SignalLevel;
}
