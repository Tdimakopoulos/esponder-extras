package exodus.com.rest.objects;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * XML Annotate Pojo to use for passing object into the web service
 * The class is annotated as XMLRootElement
 * And each class propertie as XMLElement
 * 
 */
@XmlRootElement
public class RestListA {

    @XmlElement
    public int listAId;
    @XmlElement public int interfId;
    
    @XmlElement
    public String macAddress;
    @XmlElement
    public String ipAddress;
    @XmlElement
    public String netMask;
    @XmlElement
    public String netAddress;
    @XmlElement
    public String broadcastAddress;
    @XmlElement
    public String gateway;
    @XmlElement
    public Double maxTxRate;
    @XmlElement
    public Double curTxRate;
    @XmlElement
    public Double curPercLineRate;
    @XmlElement
    public Double packetErrorRate;
    @XmlElement
    public Double TxBytes;
    @XmlElement
    public Double RxBytes;
    @XmlElement
    public int rxPackets;
    @XmlElement
    public int txPackets;
    @XmlElement
    public int rxErrors;
    @XmlElement
    public int txErrors;
}
