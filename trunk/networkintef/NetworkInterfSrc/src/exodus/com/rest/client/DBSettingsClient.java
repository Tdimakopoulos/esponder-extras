package exodus.com.rest.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

/**
 * @author tdim
 * 
 * Rest client for DB Settings web service
 * 
 */
public class DBSettingsClient {

    private WebResource webResource; // web resource variable
    private Client client; // client variable
    private static final String BASE_URI = "http://localhost:8080/NetworkInterf/resources"; // the url where the web service is deployed

    //initialize the client 
    public DBSettingsClient() {
        com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
        client = Client.create(config); // default config, no security
        webResource = client.resource(BASE_URI).path("DBSettings"); // base path of the web service
    }

    public void putXml(Object requestEntity) throws UniformInterfaceException {
        //call the web service for help : http://jersey.java.net/nonav/apidocs/1.4/jersey/com/sun/jersey/api/client/WebResource.html             
        webResource.path("SetDBParams").type(javax.ws.rs.core.MediaType.APPLICATION_XML).put(requestEntity);
    }

    public void close() {
        //close the connection and destroy the object variable
        client.destroy();
    }
}
