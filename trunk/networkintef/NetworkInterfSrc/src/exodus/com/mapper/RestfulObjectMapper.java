package exodus.com.mapper;

import InterfChecker.data.*;
import exodus.com.rest.objects.*;
import java.util.Vector;

/**
 *
 * @author tdim
 * 
 * Object mapper from Java Pojo (Plain Old Java Object) to XML annotation Objects
 */
public class RestfulObjectMapper {
    
    //Convert a MonitorData to RestMonitorData to pass as parameter into the web service client
    public RestMonitorData MapObjectMonitorDataToRest(MonitorData var) {
        RestMonitorData pReturn = new RestMonitorData();
        pReturn.interfVectorB= new Vector<RestListB>();
        pReturn.interfVectorC= new Vector<RestListC>();
        pReturn.VectorInterf= new Vector<RestInterf>();
        pReturn.interfVectorA= new Vector<RestListA>();
        System.out.println("*********************2******************");
        for (int i=0;i<var.getInterfVectorA().size();i++)
        {
            System.out.println("*********************3******************");
            RestListA pListitem=MapObjectListAToRest(var.getInterfVectorA().get(i));
            
            pReturn.interfVectorA.add(pListitem);
        }
        System.out.println("*********************4******************");
        for (int i=0;i<var.getInterfVectorB().size();i++)
        {
            pReturn.interfVectorB.add(MapObjectListBToRest(var.getInterfVectorB().get(i)));
        }
        
        System.out.println("*********************5******************");
        if(var.getInterfVectorC()==null){
            RestListC pv= new RestListC();
            pv.listCId=-1;
            pReturn.interfVectorC.add(pv);
        }else{
        for (int i=0;i<var.getInterfVectorC().size();i++)
        {
            pReturn.interfVectorC.add(MapObjectListCToRest(var.getInterfVectorC().get(i)));
        }
        }
        System.out.println("************ "+var.getVectorInterf().size()+" ***********");
        for (int i=0;i<var.getVectorInterf().size();i++)
        {
            System.out.println("---"+var.getVectorInterf().get(i).getName() +"---");
            pReturn.VectorInterf.add(MapObjectInterfToRest(var.getVectorInterf().get(i)));
        }
        return pReturn;
    }

    //Convert a Interf to RestInterf to pass as parameter into the web service client
    public RestInterf MapObjectInterfToRest(Interf var) {
        RestInterf pReturn = new RestInterf();
        pReturn.device = var.getDevice();
        pReturn.name = var.getName();
        pReturn.interfId = var.getInterfId();
        return pReturn;
    }

    //Convert a ListC to RestListC to pass as parameter into the web service client
    public RestListC MapObjectListCToRest(ListC var) {
        RestListC pReturn = new RestListC();
        //p//Return.
        pReturn.AcPoMAC = var.getAcPoMAC();
        pReturn.Channel = var.getChannel();
        pReturn.Essid = var.getEssid();
        pReturn.SignalLevel = var.getSignalLevel();
        pReturn.Status = var.getStatus();
        pReturn.listCId = var.getListCId();
        return pReturn;
    }

    //Convert a ListB to RestListB to pass as parameter into the web service client
    public RestListB MapObjectListBToRest(ListB var) {
        RestListB pReturn = new RestListB();
        pReturn.accessPointStatus = var.getStatus();
        pReturn.channel = var.getChannel();
        pReturn.discardedPackets = var.getDiscardedPackets();
        pReturn.essid = var.getEssid();
        pReturn.linkLevelNoise = var.getLinkLevelNoise();
        pReturn.linkQuality = var.getLinkQuality();
        pReturn.listBId = var.getListBId();
        pReturn.macAddressAP = var.getMacAddressAP();
        //pReturn.name = var.getName();
        pReturn.signalLevel = var.getSignalLevel();
        pReturn.txPower = var.getTxPower();
        pReturn.interfId=var.getInterfId();
        return pReturn;
    }

    //Convert a ListA to RestListA to pass as parameter into the web service client
    public RestListA MapObjectListAToRest(ListA var) {
        RestListA pReturn = new RestListA();
        pReturn.RxBytes = var.getRxBytes();
        pReturn.TxBytes = var.getTxBytes();
        pReturn.broadcastAddress = var.getBroadcastAddress();
        pReturn.curPercLineRate = var.getCurPercLineRate();
        pReturn.curTxRate = var.getCurTxRate();
        pReturn.gateway = var.getGateway();
        pReturn.ipAddress = var.getIpAddress();
        pReturn.listAId = var.getListAId();
        pReturn.macAddress = var.getMacAddress();
        pReturn.maxTxRate = var.getMaxTxRate();
        //pReturn.name = var.getName();
        pReturn.netAddress = var.getNetAddress();
        pReturn.netMask = var.getNetMask();
        pReturn.packetErrorRate = var.getPacketErrorRate();
        pReturn.rxErrors = var.getRxErrors();
        pReturn.rxPackets = var.getRxPackets();
        pReturn.txErrors = var.getTxErrors();
        pReturn.txPackets = var.getTxPackets();
        pReturn.interfId=var.getInterfId();
        return pReturn;
    }
}
