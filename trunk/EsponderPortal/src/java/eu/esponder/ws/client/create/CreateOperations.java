package eu.esponder.ws.client.create;

import java.io.IOException;
import java.math.BigDecimal;

import javax.ws.rs.core.MultivaluedMap;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.resource.OrganisationDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.ws.client.urlmanager.UrlManager;

public class CreateOperations {

    ObjectMapper mapper = new ObjectMapper();
    UrlManager URL = new UrlManager();

    public SphereDTO CreateSphereNoRadius(String userID, String szTitle,
            PointDTO pointDTO) throws JsonGenerationException,
            JsonMappingException, IOException {

        // Variables
        SphereDTO spherePersisted = null;
        SphereDTO sphereDTO = new SphereDTO(pointDTO, new BigDecimal(2),
                szTitle);
        String sphereJSON = mapper.writeValueAsString(sphereDTO);
        String szReturn = null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getSzSpherePathCreate());

        // Initialize Params
        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("userID", userID);

        // Call webservice
        szReturn = webResource2.queryParams(queryParams2)
                .type("application/json").post(String.class, sphereJSON);

        // Convert to DTO
        spherePersisted = mapper.readValue(szReturn, SphereDTO.class);

        return spherePersisted;
    }

    public SphereDTO CreateSphereWithRadius(String userID, String szTitle,
            PointDTO pointDTO, BigDecimal iRadius) throws JsonGenerationException,
            JsonMappingException, IOException {

        // Variables
        SphereDTO spherePersisted = null;
        SphereDTO sphereDTO = new SphereDTO(pointDTO, iRadius,
                szTitle);
        String sphereJSON = mapper.writeValueAsString(sphereDTO);
        String szReturn = null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getSzSpherePathCreate());

        // Initialize Params
        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("userID", userID);

        // Call webservice
        szReturn = webResource2.queryParams(queryParams2)
                .type("application/json").post(String.class, sphereJSON);

        // Convert to DTO
        spherePersisted = mapper.readValue(szReturn, SphereDTO.class);

        return spherePersisted;
    }

    public CrisisContextDTO createCrisisContext(String userID,
            CrisisContextDTO crisisContextDTO, int itype) throws JsonGenerationException,
            JsonMappingException, IOException {

        String crisisContextJSON = mapper.writeValueAsString(crisisContextDTO);
        Client client = Client.create();

        String szURL = null;
        if (itype==1) {
            szURL = URL.getSzCreateCrisisWithEvent();
        } else if (itype==2) {
            szURL = URL.getSzCreateCrisisContext();
        } else if (itype==3) {
            szURL = URL.getSzPublishCrisisWithEvent();
        }
        WebResource webResource = client.resource(szURL);

        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class, crisisContextJSON);
        return mapper.readValue(szReturn, CrisisContextDTO.class);

    }

    public RegisteredOperationsCentreDTO createRegisterOC(String userID,
            RegisteredOperationsCentreDTO registeredOperationsCentreDTO)
            throws JsonGenerationException, JsonMappingException, IOException {

        String crisisContextJSON = mapper
                .writeValueAsString(registeredOperationsCentreDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getSzCreateRegisteredOC());
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class, crisisContextJSON);
        return mapper.readValue(szReturn, RegisteredOperationsCentreDTO.class);
    }
    
    public OrganisationDTO createOrganization(String userID,
            OrganisationDTO orgnizationDTO)
            throws JsonGenerationException, JsonMappingException, IOException {

        String crisisContextJSON = mapper
                .writeValueAsString(orgnizationDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getSzOrganizationcreate());
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("pkiKey", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class, crisisContextJSON);
        return mapper.readValue(szReturn, OrganisationDTO.class);
    }

    public RegisteredConsumableResourceDTO createRegisterConsumableResource(String userID,
            RegisteredConsumableResourceDTO registeredConsumableResourceDTO)
            throws JsonGenerationException, JsonMappingException, IOException {

        String consumableResourceJSON = mapper
                .writeValueAsString(registeredConsumableResourceDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getSzCreateRegisterConsumableResource());
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class, consumableResourceJSON);
        return mapper.readValue(szReturn, RegisteredConsumableResourceDTO.class);
    }

    public ReusableResourceDTO createRegisterReusableResource(String userID,
            ReusableResourceDTO registeredReusableResourceDTO)
            throws JsonGenerationException, JsonMappingException, IOException {

        String reusableResourceJSON = mapper
                .writeValueAsString(registeredReusableResourceDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getSzCreateRegisterReusableResource());
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class, reusableResourceJSON);
        return mapper.readValue(szReturn, ReusableResourceDTO.class);
    }

    public ActionDTO createAction(String userID,
            ActionDTO actionDTO)
            throws JsonGenerationException, JsonMappingException, IOException {

        String actionJSON = mapper
                .writeValueAsString(actionDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getSzCreateAction());
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class, actionJSON);
        return mapper.readValue(szReturn, ActionDTO.class);
    }

    public ActionPartDTO createActionPart(String userID,
            ActionPartDTO actionPartDTO)
            throws JsonGenerationException, JsonMappingException, IOException {

        String actionPartJSON = mapper
                .writeValueAsString(actionPartDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getSzCreateActionPart());
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class, actionPartJSON);
        return mapper.readValue(szReturn, ActionPartDTO.class);
    }

    public CrisisResourcePlanDTO createResourcePlan(String userID,
            CrisisResourcePlanDTO actionPartDTO)
            throws JsonGenerationException, JsonMappingException, IOException {

        String actionPartJSON = mapper
                .writeValueAsString(actionPartDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getSzResourcePlanCreate());
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class, actionPartJSON);
        return mapper.readValue(szReturn, CrisisResourcePlanDTO.class);
    }

    public CrisisContextSnapshotDTO createCrisisContextSnapshot(String userID,
            CrisisContextSnapshotDTO crisisContextSnapshotDTO, boolean bevents) throws JsonGenerationException,
            JsonMappingException, IOException {

        String crisisContextSnapshotJSON = mapper.writeValueAsString(crisisContextSnapshotDTO);
        Client client = Client.create();

        String szURL = null;
        if (bevents) {
            szURL = URL.getSzcreatecrisissnapshot();
        } else {
            szURL = URL.getSzcreatecrisissnapshot();
        }
        WebResource webResource = client.resource(szURL);

        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class, crisisContextSnapshotJSON);
        return mapper.readValue(szReturn, CrisisContextSnapshotDTO.class);

    }

    public CrisisContextSnapshotDTO createCrisisContextSnap(String userID,
            String ccID, String LocationTitle, boolean bevents) throws JsonGenerationException,
            JsonMappingException, IOException {


        Client client = Client.create();

        String szURL = null;
        if (bevents) {
            szURL = URL.getSzCreateCrisisContextSnapshot();
        } else {
            szURL = URL.getSzCreateCrisisContextSnapshot();
        }
        WebResource webResource = client.resource(szURL);

        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        queryParams.add("ccID", ccID);
        queryParams.add("LocationTitle", LocationTitle);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class);
        return mapper.readValue(szReturn, CrisisContextSnapshotDTO.class);

    }
    
    public ESponderUserDTO createEsponderUser(String userID,
            ESponderUserDTO esponderuserdto) throws JsonGenerationException,
            JsonMappingException, IOException {

        String esuserJSON = mapper.writeValueAsString(esponderuserdto);
        Client client = Client.create();

        
        WebResource webResource = client.resource(URL.getSzpusercreate());

        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class, esuserJSON);
        return mapper.readValue(szReturn, ESponderUserDTO.class);

    }
    
     public RegisteredReusableResourceDTO createRegisterReusableResourceReal(String userID,
            RegisteredReusableResourceDTO registeredReusableResourceDTO)
            throws JsonGenerationException, JsonMappingException, IOException {

        String reusableResourceJSON = mapper
                .writeValueAsString(registeredReusableResourceDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getSzregReusableCreate());
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        queryParams.add("sessionID", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .post(String.class, reusableResourceJSON);
        return mapper.readValue(szReturn, RegisteredReusableResourceDTO.class);
    }
}
