package eu.esponder.ws.client.query;

import java.io.IOException;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MultivaluedMap;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.SensorResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PlannableResourceCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.events.entity.OsgiEventsEntityDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.ws.client.urlmanager.UrlManager;

public class QueryManager {
	ObjectMapper mapper = new ObjectMapper();
	UrlManager URL = new UrlManager();

        public  PlannableResourceCategoryDTO getresourceCategory(String userID, String categoryID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		PlannableResourceCategoryDTO actorDTO = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzgetplanablecategories());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		queryParams.add("categoryID", categoryID);

		// Call Webservice
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		actorDTO = mapper.readValue(szReturn, PlannableResourceCategoryDTO.class);

		return actorDTO;
	}
                
	public ActorDTO getActorID(String userID, String actorID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ActorDTO actorDTO = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzGetActorByID());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		queryParams.add("actorID", actorID);

		// Call Webservice
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		actorDTO = mapper.readValue(szReturn, ActorDTO.class);

		return actorDTO;
	}
        
        public ESponderUserDTO getUserID(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ESponderUserDTO actorDTO = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzpuserfindbyid());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		

		// Call Webservice
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		actorDTO = mapper.readValue(szReturn, ESponderUserDTO.class);

		return actorDTO;
	}
        
        public PersonnelDTO getPersonnelTitle(String userID, String ptitle)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		PersonnelDTO actorDTO = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzpersonnelfindbytitle());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		queryParams.add("personnelTitle", ptitle);

		// Call Webservice
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		actorDTO = mapper.readValue(szReturn, PersonnelDTO.class);

		return actorDTO;
	}
        
        public String getPersonnelTitleID(String userID, String ptitle)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzpersonnelfindbytitle());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		queryParams.add("personnelTitle", ptitle);

		// Call Webservice
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		//actorDTO = mapper.readValue(szReturn, PersonnelDTO.class);
                String sz="";
                int i=szReturn.indexOf("id");
                int i2=szReturn.indexOf(",", i);
                sz=szReturn.substring(i+4, i2);
		return sz;
	}
        
        public CrisisContextDTO getCrisisContextID(String userID, String actorID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		CrisisContextDTO pCrisisContextDTO = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzCrisisContextFindbyID());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		queryParams.add("crisisContextID", actorID);

		// Call Webservice
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pCrisisContextDTO = mapper.readValue(szReturn, CrisisContextDTO.class);

		return pCrisisContextDTO;
	}

	public ResultListDTO getRegisterdOCAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzGetAllRegisteredOC());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}
        
        	public ResultListDTO getAllUsers(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzpuserfindall());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getRegisterdConsumablesAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL
				.getSzRegisterConsumablesFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getRegisterdReusablesAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL
				.getSzRegisterReusablesFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getConsumablesAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzConsumablesFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getCrisisContextAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL
				.getSzCrisisContextFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
                queryParams.add("sessionID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);
//System.out.println(szReturn);
		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getCrisisResourcePlanAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL
				.getSzCrisisResourcePlanFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getOperationCentersAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL
				.getSzOperationCenterFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getPersonnelAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzPersonnelFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

	public ResultListDTO getRusablesAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzReusablesFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}
	
	public ResultListDTO getAllSensorSnapshots(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzGetAllSensorSnapshosts());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}
        
        public SensorResultListDTO getAllSensorSnapshotsNew(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		SensorResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzgetallsnapshots());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, SensorResultListDTO.class);

		return pResults;
	}
	
	public ResultListDTO getAllTypes(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzGetAllEsponderTypes());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}

    public EquipmentDTO getEquipment(String userID,Long eid)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		EquipmentDTO pResults = null;
		Client client = Client.create();

		WebResource webResource = client.resource(URL.getSzgetAllEquipments());
		// Initialize Parameters
				MultivaluedMap queryParams = new MultivaluedMapImpl();
				queryParams.add("equipmentID", eid.toString());
				queryParams.add("userID", userID);
				String szReturn = webResource.queryParams(queryParams)
						.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, EquipmentDTO.class);

		return pResults;
	}
    
    public ResultListDTO getEventsBySeverity(String userID,String Severity)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();

		WebResource webResource = client.resource(URL.getSzgetEventsBySeverity());
		// Initialize Parameters
				MultivaluedMap queryParams = new MultivaluedMapImpl();
				queryParams.add("osgiEventsEntitySeverity", Severity);
				queryParams.add("userID", userID);
				String szReturn = webResource.queryParams(queryParams)
						.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}
    
    public OsgiEventsEntityDTO getEventsByid(String userID,String eventid)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
    	OsgiEventsEntityDTO pResults = null;
		Client client = Client.create();

		WebResource webResource = client.resource(URL.getSzgetEventsByID());
		// Initialize Parameters
				MultivaluedMap queryParams = new MultivaluedMapImpl();
				queryParams.add("osgiEventsEntityDTOID", eventid);
				queryParams.add("userID", userID);
				String szReturn = webResource.queryParams(queryParams)
						.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, OsgiEventsEntityDTO.class);

		return pResults;
	}
    
   public ResultListDTO getResourcePlansAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzReusablesFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}
   
      public ResultListDTO getOrgAll(String userID)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		ResultListDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzOrganizationsFindAll());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
                queryParams.add("sessionID", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

		// Convert to DTO
		pResults = mapper.readValue(szReturn, ResultListDTO.class);

		return pResults;
	}
   
   public OrganisationCategoryDTO getResourceCategoryOrganization(Long did,Long tid,String userID)
			throws JsonParseException, JsonMappingException, IOException {

		OrganisationCategoryDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzResourceCategoryFindForOrganization());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
                queryParams.add("disciplineTypeID", did.toString());
                queryParams.add("organisationTypeID", tid.toString());
		queryParams.add("pkiKey", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

                
		// Convert to DTO
		pResults = mapper.readValue(szReturn, OrganisationCategoryDTO.class);

		return pResults;
	}
    
   			//@QueryParam("reusableTypeID") @NotNull(message = "Reusable Type ID may not be null") Long reusableTypeID,
			//@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws ClassNotFoundException {
   //ReusableResourceCategoryDTO
   
   public ReusableResourceCategoryDTO getReusableResourceCategoryDTO(Long rtypeid,String userID)
			throws JsonParseException, JsonMappingException, IOException {

		ReusableResourceCategoryDTO pResults = null;
		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzResourceCategoryFindForOrganization());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
                queryParams.add("reusableTypeID", rtypeid.toString());
                
		queryParams.add("pkiKey", userID);
		String szReturn = webResource.queryParams(queryParams)
				.get(String.class);

                
		// Convert to DTO
		pResults = mapper.readValue(szReturn, ReusableResourceCategoryDTO.class);

		return pResults;
	}
}
