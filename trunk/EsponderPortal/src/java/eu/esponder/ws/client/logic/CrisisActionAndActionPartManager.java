package eu.esponder.ws.client.logic;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.xml.sax.SAXException;

import eu.esponder.CoordinatesForAddress.Coordinates;
import eu.esponder.CoordinatesForAddress.addressToCordinates;
import eu.esponder.dto.model.crisis.CrisisContextAlertEnumDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.ws.client.create.CreateOperations;
import eu.esponder.ws.client.logic.objects.ActionTypeQueryManager;
import eu.esponder.ws.client.query.QueryManager;

public class CrisisActionAndActionPartManager {


    public CrisisContextSnapshotDTO CreateCrisisContextSnap(String userid, String ccid, String locationtile) throws IOException
    {
        CreateOperations pCreate = new CreateOperations();
        return pCreate.createCrisisContextSnap(userid, ccid, locationtile, false);
    }
  

	public ActionDTO CreateAction(CrisisContextDTO persistedcc) throws JsonGenerationException, JsonMappingException, IOException
    {
		ActionTypeQueryManager pActionManager = new ActionTypeQueryManager();
		CreateOperations pCreate = new CreateOperations();
		
		pActionManager.LoadAllCrisisTypes();
		
        ActionDTO actionDTO= new ActionDTO();
        actionDTO.setCrisisContext(persistedcc);
        actionDTO.setTitle(persistedcc.getTitle()+new Date().toString());
        actionDTO.setActionOperation(ActionOperationEnumDTO.FIX);
		actionDTO.setType(pActionManager.getpActionTypes().get(0).getTitle());
		actionDTO.setSeverityLevel(SeverityLevelDTO.MEDIUM);
		
		return pCreate.createAction("1", actionDTO);
    }
    
    public ActionPartDTO CreateActionPart(ActionDTO action,CrisisContextDTO cc) throws JsonGenerationException, JsonMappingException, IOException
    {
        CreateOperations pCreate = new CreateOperations();
        ActionPartDTO actionpartDTO= new ActionPartDTO();
        actionpartDTO.setActionId(action.getId());
        actionpartDTO.setTitle(cc.getTitle()+new Date().toString());
        actionpartDTO.setActionId(action.getId());
        actionpartDTO.setActionOperation(ActionOperationEnumDTO.FIX);
        actionpartDTO.setSeverityLevel(SeverityLevelDTO.MEDIUM);
	//actionpartDTO.setActor(action.);
		//ActorDTO actorDTO = actorService.findByTitleRemote("FR #1.1");
//		actionPart1.setActor(actorDTO);
		
	QueryManager pQuery = new QueryManager();
        actionpartDTO.setActor(new Long(1));
		
        return pCreate.createActionPart("1", actionpartDTO);
        
    }
    
     public ActionPartDTO CreateActionPartPersonnel(ActionDTO action,CrisisContextDTO cc,String actorid) throws JsonGenerationException, JsonMappingException, IOException
    {
        CreateOperations pCreate = new CreateOperations();
        ActionPartDTO actionpartDTO= new ActionPartDTO();
        actionpartDTO.setActionId(action.getId());
        actionpartDTO.setTitle(cc.getTitle()+new Date().toString());
        actionpartDTO.setActionId(action.getId());
        actionpartDTO.setActionOperation(ActionOperationEnumDTO.FIX);
        actionpartDTO.setSeverityLevel(SeverityLevelDTO.MEDIUM);
		
		//ActorDTO actorDTO = actorService.findByTitleRemote("FR #1.1");
//		actionPart1.setActor(actorDTO);
		
	QueryManager pQuery = new QueryManager();
        actionpartDTO.setActor(Long.valueOf(actorid));
		
        return pCreate.createActionPart("1", actionpartDTO);
        
    }
  
}
