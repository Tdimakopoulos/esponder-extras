package eu.esponder.ws.client.update;

import java.io.IOException;

import javax.ws.rs.core.MultivaluedMap;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.OrganisationDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.ws.client.urlmanager.UrlManager;

public class UpdateOperations {
	ObjectMapper mapper = new ObjectMapper();
	UrlManager URL = new UrlManager();
	
	public CrisisContextDTO updateCrisisContext(String userID,
			CrisisContextDTO crisisContextDTO) throws JsonGenerationException,
			JsonMappingException, IOException {

		String crisisContextJSON = mapper.writeValueAsString(crisisContextDTO);
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzCrisisContentUpdate());
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn;

		szReturn = webResource.queryParams(queryParams).type("application/json")
				.put(String.class, crisisContextJSON);
		return mapper.readValue(szReturn, CrisisContextDTO.class);

	}
	
	public CrisisResourcePlanDTO ResourcePlanUpdate(String userID,
			CrisisResourcePlanDTO actionPartDTO)
			throws JsonGenerationException, JsonMappingException, IOException {

		String actionPartJSON = mapper
				.writeValueAsString(actionPartDTO);

		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzResourcePlanUpdate());
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn;

		szReturn = webResource.queryParams(queryParams).type("application/json")
				.put(String.class, actionPartJSON);
		return mapper.readValue(szReturn, CrisisResourcePlanDTO.class);
	}
	
	public RegisteredOperationsCentreDTO updateRegisterOC(String userID,
			RegisteredOperationsCentreDTO registeredOperationsCentreDTO)
			throws JsonGenerationException, JsonMappingException, IOException {

		String crisisContextJSON = mapper
				.writeValueAsString(registeredOperationsCentreDTO);

		Client client = Client.create();
		WebResource webResource = client
				.resource(URL.getSzUpdateRegisteredOC());
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		String szReturn;

		szReturn = webResource.queryParams(queryParams).type("application/json")
				.put(String.class, crisisContextJSON);
		return mapper.readValue(szReturn, RegisteredOperationsCentreDTO.class);
	}
        
        public ESponderUserDTO updateEsponderUser(String userID,
            ESponderUserDTO esponderuserdto) throws JsonGenerationException,
            JsonMappingException, IOException {

        String esuserJSON = mapper.writeValueAsString(esponderuserdto);
        Client client = Client.create();

        
        WebResource webResource = client.resource(URL.getSzpuserupdate());

        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("userID", userID);
        String szReturn;

        szReturn = webResource.queryParams(queryParams).type("application/json")
                .put(String.class, esuserJSON);
        return mapper.readValue(szReturn, ESponderUserDTO.class);

    }
        
        
        public OrganisationCategoryDTO updateOrganizationCategory(Long organisationCategoryID,
Long disciplineTypeID,
Long organisationTypeId,
			Long organisationID,
                        OrganisationDTO porg,
			String pkiKey) throws IOException {

		
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzResourceCategoryUpdate());
		MultivaluedMap queryParams = new MultivaluedMapImpl();
                queryParams.add("organisationCategoryID", organisationCategoryID.toString());
                queryParams.add("disciplineTypeID", disciplineTypeID.toString());
                queryParams.add("organisationTypeId", organisationTypeId.toString());
                queryParams.add("organisationID", organisationID.toString());
                
		queryParams.add("pkiKey", pkiKey);
		String szReturn;
String crisisContextJSON = mapper
				.writeValueAsString(porg);
		szReturn = webResource.queryParams(queryParams).type("application/json")
				.put(String.class,crisisContextJSON );
		return mapper.readValue(szReturn, OrganisationCategoryDTO.class);

	}
}
