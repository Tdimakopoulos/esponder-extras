/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.languagecontroller;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author tdim
 */
@ManagedBean
@ApplicationScoped
public class LocaleBean {

    private String language;
    
    /**
     * Creates a new instance of LocaleBean
     */
    public LocaleBean() {
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }
}
