//package eu.esponder.test.rest.config;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.xml.bind.JAXBException;
//
//import org.codehaus.jackson.JsonParseException;
//import org.codehaus.jackson.map.JsonMappingException;
//import org.codehaus.jackson.map.ObjectMapper;
////import org.testng.annotations.Test;
//
//import eu.esponder.dto.model.config.DroolsConfigurationDTO;
//import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
//import eu.esponder.rest.client.ResteasyClient;
//import eu.esponder.util.jaxb.Parser;
////import eu.esponder.util.logger.ESponderLogger;
//
//public class ESponderConfigurationTest {
//
//	String CONFIGURATION_SERVICE_URI = "http://localhost:8080/esponder-restful/system/configuration/";
//
//
//	//	Create Config, accept XML
////	@Test
//	public void CreateConfigurationXML() throws RuntimeException, Exception {
//
//		String URI = CONFIGURATION_SERVICE_URI + "create";
//
//		DroolsConfigurationDTO configDTO1 = new DroolsConfigurationDTO();
//		configDTO1.setParameterName("DROOLS_BASE_URI");
//		configDTO1.setParameterValue("http://192.168.3.19:8080/");
//		configDTO1.setDescription("Drools base URI for IntegrationTest");
//
//		DroolsConfigurationDTO configDTO2 = new DroolsConfigurationDTO();
//		configDTO2.setParameterName("DROOLS_BASE_URI2");
//		configDTO2.setParameterValue("http://192.168.4.19:8080/");
//		configDTO2.setDescription("Drools base URI 2 for IntegrationTest");
//
//		DroolsConfigurationDTO configDTO3 = new DroolsConfigurationDTO();
//		configDTO3.setParameterName("DROOLS_BASE_URI3");
//		configDTO3.setParameterValue("http://192.168.5.19:8080/");
//		configDTO3.setDescription("Drools base URI 3 for IntegrationTest");
//
//		Parser parser = new Parser(new Class[] {ESponderConfigParameterDTO.class});
//
//		ResteasyClient postClient = new ResteasyClient(URI, "application/xml");
//
//		Map<String, String> params = this.getIDServiceParameters();
//
//		String bodyStr1 = parser.marshall(configDTO1);
//		String resultXML1 = postClient.post(params, bodyStr1);
//		printResultsXML(parser, resultXML1);
//
//		String bodyStr2 = parser.marshall(configDTO2);
//		String resultXML2 = postClient.post(params, bodyStr2);
//		printResultsXML(parser, resultXML2);
//
//		String bodyStr3 = parser.marshall(configDTO3);
//		String resultXML3 = postClient.post(params, bodyStr3);
//		printResultsXML(parser, resultXML3);
//
//	}
//
//
////	@Test
//	public void CreateConfigurationJSON() throws RuntimeException, Exception {
//
//		String URI = CONFIGURATION_SERVICE_URI + "create";
//
//		DroolsConfigurationDTO configDTO1 = new DroolsConfigurationDTO();
//		configDTO1.setParameterName("DROOLS_BASE_URI4");
//		configDTO1.setParameterValue("http://192.168.7.19:8080/");
//		configDTO1.setDescription("Drools base URI for IntegrationTest");
//
//		DroolsConfigurationDTO configDTO2 = new DroolsConfigurationDTO();
//		configDTO2.setParameterName("DROOLS_BASE_URI5");
//		configDTO2.setParameterValue("http://192.168.8.19:8080/");
//		configDTO2.setDescription("Drools base URI 2 for IntegrationTest");
//
//		DroolsConfigurationDTO configDTO3 = new DroolsConfigurationDTO();
//		configDTO3.setParameterName("DROOLS_BASE_URI6");
//		configDTO3.setParameterValue("http://192.168.9.19:8080/");
//		configDTO3.setDescription("Drools base URI 3 for IntegrationTest");
//
//		ObjectMapper mapper = new ObjectMapper();
//
//		ResteasyClient postClient = new ResteasyClient(URI, "application/json");
//
//		Map<String, String> params = this.getIDServiceParameters();
//
//		String bodyStr1 = mapper.writeValueAsString(configDTO1);
//		String resultJSON1 = postClient.post(params, bodyStr1);
//		printResultsJSON(resultJSON1);
//
//		String bodyStr2 = mapper.writeValueAsString(configDTO2);
//		String resultJSON2 = postClient.post(params, bodyStr2);
//		printResultsJSON(resultJSON2);
//
//		String bodyStr3 = mapper.writeValueAsString(configDTO3);
//		String resultJSON3 = postClient.post(params, bodyStr3);
//		printResultsJSON(resultJSON3);
//
//	}
//
//
//
//	//Returns XML
//	@SuppressWarnings("unused")
////	@Test
//	public void getConfigByIDXML() throws RuntimeException, Exception{
//
//		String URI = CONFIGURATION_SERVICE_URI + "getByID";
//
//		Parser parser = new Parser(new Class[] {ESponderConfigParameterDTO.class});
//
//		ResteasyClient getClient = new ResteasyClient(URI, "application/xml");
//		System.out.println("Client for getID created successfully...\n");
//
//		Map<String, String> params = getIDServiceParameters();
//		params.put("configurationId", "4");
//
//		String result = getClient.get(params);
//
//
//		printResultsXML(parser, result);
//
//		DroolsConfigurationDTO configDTO = (DroolsConfigurationDTO) parser.unmarshal(result);
//	}
//
//
//
//
//	//	returns JSON
//	@SuppressWarnings("unused")
////	@Test
//	public void getConfigByIDJSON() throws RuntimeException, Exception{
//
//		String URI = CONFIGURATION_SERVICE_URI + "getByID";
//
//		ObjectMapper mapper = new ObjectMapper();
//
//		ResteasyClient getClient = new ResteasyClient(URI, "application/json");
//		System.out.println("Client for getID created successfully...\n");
//
//		Map<String, String> params = getIDServiceParameters();
//		params.put("configurationId", "5");
//
//		String resultJSON = getClient.get(params);
//
//
//		System.out.println("\n*********FULL_JSON*******************************\n");
//		System.out.println(resultJSON+"\n\n");
//
//		DroolsConfigurationDTO configDTO = (DroolsConfigurationDTO) mapper.readValue(resultJSON, DroolsConfigurationDTO.class);
//	}
//
//
//
//	//Returns XML
////	@Test
//	public ESponderConfigParameterDTO getConfigByNameXML() throws RuntimeException, Exception{
//
//		String URI = CONFIGURATION_SERVICE_URI + "getByName";
//
//		Parser parser = new Parser(new Class[] {ESponderConfigParameterDTO.class});
//
//		ResteasyClient getClient = new ResteasyClient(URI, "application/xml");
//		System.out.println("Client for getID created successfully...\n");
//
//		Map<String, String> params = getIDServiceParameters();
//		params.put("configurationName", "DROOLS_BASE_URI2");
//
//		String result = getClient.get(params);
//
//
//		printResultsXML(parser, result);
//
//		DroolsConfigurationDTO configDTO = (DroolsConfigurationDTO) parser.unmarshal(result);
//		return configDTO;
//	}
//
//
//
//
//	//returns JSON
//	@SuppressWarnings("unused")
////	@Test
//	public void getConfigByNameJSON() throws RuntimeException, Exception{
//
//		String URI = CONFIGURATION_SERVICE_URI + "getByName";
//
//		ObjectMapper mapper = new ObjectMapper();
//
//		ResteasyClient getClient = new ResteasyClient(URI, "application/json");
//		System.out.println("Client for getID created successfully...\n");
//
//		Map<String, String> params = getIDServiceParameters();
//		params.put("configurationName", "DROOLS_BASE_URI6");
//
//		String resultJSON = getClient.get(params);
//
//		printResultsJSON(resultJSON);
//	}
//
//
////	@Test
//	public void updateConfigByNameXML() throws RuntimeException, Exception{
//
//		String URI = CONFIGURATION_SERVICE_URI + "update";
//
//		Parser parser = new Parser(new Class[] {ESponderConfigParameterDTO.class});
//
//		ESponderConfigParameterDTO configDTO = getConfigByNameXML();
//		if(configDTO != null) {
//
//			configDTO.setDescription(configDTO.getDescription()+"updated");
//
//			ResteasyClient getClient = new ResteasyClient(URI, "application/xml");
//
//			Map<String, String> params = getIDServiceParameters();
//
//			String bodyStr = parser.marshall(configDTO);
//
//			String result = getClient.put(params, bodyStr);
//
//			printResultsXML(parser, result);
//
//			configDTO = (DroolsConfigurationDTO) parser.unmarshal(result);
//
//		}
//                else{}
////			ESponderLogger.debug(this.getClass(), "No entity found, cannot proceed with update ");
//
//
//	}
//
//
//
//	private Map<String, String> getIDServiceParameters() {
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("userID", "1");
//		return params;
//	}
//
//	@SuppressWarnings("unused")
//	private Map<String, String> getTitleServiceParameters() {
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("userID", "1");
//		params.put("actorTitle", "FR #1.1");
//		return params;
//	}
//
//	@SuppressWarnings("unused")
//	private Map<String, String> postCreateServiceParameters() {
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("userID", "1");
//		return params;
//	}
//
//	private void printResultsXML(Parser parser, String result) throws JAXBException {
//		if(result != null) {
//			System.out.println("\n*********FULL_XML*******************************\n");
//			System.out.println(result+"\n\n");
//
//			DroolsConfigurationDTO configDTO = (DroolsConfigurationDTO) parser.unmarshal(result);
////			ESponderLogger.debug(this.getClass(), "\n\nSuccessful marshalling for "+configDTO.getParameterName()+"\n\n");
//		}
//                else{}
////			ESponderLogger.debug(this.getClass(), "\n\nNo response, nothing was created...\n\n");
//	}
//
//
//	private void printResultsJSON(String result) throws JAXBException, JsonParseException, JsonMappingException, IOException {
//
//		if(result != null) {
//
//			ObjectMapper mapper = new ObjectMapper();
//			System.out.println("\n*********FULL_XML*******************************\n");
//			System.out.println(result+"\n\n");
//
//
//			DroolsConfigurationDTO configDTO = (DroolsConfigurationDTO) mapper.readValue(result, DroolsConfigurationDTO.class);
//			//ESponderLogger.debug(this.getClass(), "\n\nSuccessful marshalling for "+configDTO.getParameterName()+"\n\n");
//		}
//                else{}
//			//ESponderLogger.debug(this.getClass(), "\n\nNo response, nothing was created...\n\n");
//	}
//
//
//}
