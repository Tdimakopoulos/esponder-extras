/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.datafusion.web;

import eu.esponder.ws.client.logic.DatatfusionWebService;
import eu.esponder.rest.datafusion.RuleResults;
import eu.esponder.rest.datafusion.RuleResultsXML;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

/**
 *
 * @author tdim
 */
@ManagedBean
@RequestScoped
public class DatafusionWebAdmin {
    
    private List<RuleResultsXML> pRules;
    /**
     * Creates a new instance of DatafusionWebAdmin
     */
    public DatafusionWebAdmin() {
        DatatfusionWebService pweb = new DatatfusionWebService();
        try {
            RuleResults pQuery=pweb.getRuleResults("1");
            pRules=pQuery.getResultList();
        } catch (JsonParseException ex) {
            Logger.getLogger(DatafusionWebAdmin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(DatafusionWebAdmin.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DatafusionWebAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the pRules
     */
    public List<RuleResultsXML> getpRules() {
        return pRules;
    }

    /**
     * @param pRules the pRules to set
     */
    public void setpRules(List<RuleResultsXML> pRules) {
        this.pRules = pRules;
    }
}
