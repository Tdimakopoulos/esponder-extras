/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.themes;

import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class jasonbean {

    String someJsonString="{\"requestID\":20,\"queryID\":1,\"mapPositions\":{\"@class\":\"eu.esponder.dto.model.crisis.alternative.OCEocALTDTO\",\"title\":\"EOC Attica\"," +
                                                "\"subordinateMEOCs\":[{\"@class\":\"eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO\",\"title\":\"MEOC Piraeus\",\"supervisingOC\":1,"+
                                                "\"incidentCommander\":{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorICALTDTO\",\"title\":\"IC #2\",\"personnel\":9,"+
                                                "\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"path\":\"IC#2\",\"host\":\"192.168.11.1\"},"+
                                                "\"crisisManager\":9,\"frTeams\":[],\"operationsCentre\":3,\"id\":11},\"meocCrisisContext\":1,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO\","+
                                                "\"id\":4,\"status\":\"MOVING\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":11,"+
                                                "\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":38.025334,\"longitude\":23.802717},\"radius\":0.00,\"title\":\"Sphere 04\"},"+
                                                "\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1368606756157,\"dateTo\":1368606786157},\"operationsCentre\":3},\"id\":3,\"users\":[7],"+
                                                "\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"path\":\"meocPiraeus\",\"host\":\"192.168.10.3\"}},"+
                                                "{\"@class\":\"eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO\",\"title\":\"MEOC Athens\",\"supervisingOC\":1,"+
                                                "\"incidentCommander\":{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorICALTDTO\",\"title\":\"IC #1\",\"personnel\":9,"+
                                                "\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"path\":\"IC#1\",\"host\":\"192.168.11.1\"},"+
                                                "\"crisisManager\":9,\"frTeams\":[{\"@class\":\"eu.esponder.dto.model.crisis.alternative.FRTeamALTDTO\",\"snapshots\":[],"+
                                                "\"incidentCommander\":10,\"id\":2,\"frchief\":{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO\",\"title\":\"FRC #2\",\"personnel\":7,"+
                                                "\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"path\":\"FRC#2\",\"host\":\"192.168.11.1\"},"+
                                                "\"subordinates\":[{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO\",\"title\":\"FR #2.2\",\"personnel\":9,"+
                                                "\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"path\":\"FR#2.1\",\"host\":\"192.168.11.1\"},"+
                                                "\"equipmentSet\":[6],\"frchief\":15,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\",\"id\":6,\"status\":\"ACTIVE\","+
                                                "\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":17,"+
                                                "\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":38.025334,\"longitude\":23.802717},\"radius\":0.00,\"title\":\"Sphere 6\"},"+
                                                "\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1368606757828,\"dateTo\":1368606787828},\"actor\":17},\"id\":17},"+
                                                "{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO\",\"title\":\"FR #2.1\",\"personnel\":8,"+
                                                "\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"path\":\"FR#2.1\",\"host\":\"192.168.11.1\"},"+
                                                "\"equipmentSet\":[5],\"frchief\":15,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\",\"id\":5,"+
                                                "\"status\":\"ACTIVE\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":16,"+
                                                "\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":38.025334,\"longitude\":23.802717},\"radius\":0.00,\"title\":\"Sphere 5\"},"+
                                                "\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1368606757828,\"dateTo\":1368606787828},\"actor\":16},\"id\":16}],"+
                                                "\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\",\"id\":4,\"status\":\"ACTIVE\","+
                                                "\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":15,"+
                                                "\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":38.025334,\"longitude\":23.802717},\"radius\":0.00,\"title\":\"Sphere 4\"},"+
                                                "\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1368606757828,\"dateTo\":1368606787828},\"actor\":15},\"equipmentSet\":[4],\"id\":15}},"+
                                                "{\"@class\":\"eu.esponder.dto.model.crisis.alternative.FRTeamALTDTO\",\"snapshots\":[],\"incidentCommander\":10,\"id\":1,"+
                                                "\"frchief\":{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO\",\"title\":\"FRC #1\",\"personnel\":4,"+
                                                "\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"path\":\"FRC#1\",\"host\":\"192.168.11.1\"},"+
                                                "\"subordinates\":[{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO\",\"title\":\"FR #1.2\",\"personnel\":6,"+
                                                "\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"path\":\"FR#1.2\",\"host\":\"192.168.11.1\"},"+
                                                "\"equipmentSet\":[3],\"frchief\":12,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\",\"id\":3,"+
                                                "\"status\":\"ACTIVE\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":14,"+
                                                "\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":38.025334,\"longitude\":23.802717},\"radius\":0.00,"+
                                                "\"title\":\"Sphere 3\"},\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1368606757828,\"dateTo\":1368606787828},"+
                                                "\"actor\":14},\"id\":14},{\"@class\":\"eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO\",\"title\":\"FR #1.1\",\"personnel\":5,"+
                                                "\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"path\":\"FR#1.1\",\"host\":\"192.168.11.1\"},"+
                                                "\"equipmentSet\":[2],\"frchief\":12,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\",\"id\":2,"+
                                                "\"status\":\"ACTIVE\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":13,"+
                                                "\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":38.025334,\"longitude\":23.802717},"+
                                                "\"radius\":0.00,\"title\":\"Sphere 2\"},\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1368606757828,"+
                                                "\"dateTo\":1368606787828},\"actor\":13},\"id\":13}],\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO\","+
                                                "\"id\":1,\"status\":\"ACTIVE\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":12,"+
                                                "\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":38.025334,\"longitude\":23.802717},\"radius\":0.00,\"title\":\"Sphere 1\"},"+
                                                "\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1368606757828,\"dateTo\":1368606787828},\"actor\":12},\"equipmentSet\":[1],\"id\":12}}],"+
                                                "\"operationsCentre\":2,\"id\":10},\"meocCrisisContext\":1,\"snapshot\":{\"@class\":\"eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO\",\"id\":2,"+
                                                "\"status\":\"MOVING\",\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":9,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\","+
                                                "\"latitude\":38.025334,\"longitude\":23.802717},\"radius\":0.00,\"title\":\"Sphere 02\"},\"period\":{\"@class\":\"eu.esponder.dto.model.snapshot.PeriodDTO\",\"dateFrom\":1368606756157,\"dateTo\":1368606786157},"+
                                                "\"operationsCentre\":2},\"id\":2,\"users\":[6],\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"path\":\"meocAthens\",\"host\":\"192.168.10.2\"}}],\"eocCrisisContext\":1,"+
                                                "\"locationArea\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.SphereDTO\",\"id\":7,\"centre\":{\"@class\":\"eu.esponder.dto.model.snapshot.location.PointDTO\",\"latitude\":37.988581,\"longitude\":23.738773},"+
                                                "\"radius\":0.00,\"title\":\"Eoc LOcation\"},\"id\":1,\"users\":[5],\"voIPURL\":{\"@class\":\"eu.esponder.dto.model.crisis.view.VoIPURLDTO\",\"protocol\":\"http\",\"path\":\"eocAttica\",\"host\":\"192.168.10.1\"}},\"pResponse\":null,\"eventDest\":null,"+
                                                "\"journalMessageInfo\":\"ESponder DF Query Response Event\",\"eventAttachment\":{\"@class\":\"eu.esponder.dto.model.user.ESponderUserDTO\",\"resourceId\":\"0:null\",\"id\":0},\"eventSource\":{\"@class\":\"eu.esponder.dto.model.user.ESponderUserDTO\","+
                                                "\"resourceId\":\"0:null\",\"id\":0},\"eventTimestamp\":1368610069315,\"eventSeverity\":\"UNDEFINED\",\"journalMessage\":\"Test Event\"}";

    /**
     * Creates a new instance of jasonbean
     */
    public jasonbean() {
    }
    public void renderJson() throws IOException {
    FacesContext facesContext = FacesContext.getCurrentInstance();
    ExternalContext externalContext = facesContext.getExternalContext();
    externalContext.setResponseContentType("application/json");
    externalContext.setResponseCharacterEncoding("UTF-8");
    externalContext.getResponseOutputWriter().write(someJsonString);
    facesContext.responseComplete();
}
}
