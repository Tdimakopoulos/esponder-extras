/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.objects;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author tdim
 */
public class person {
  private String name;
        
        private String title;
        private int number;
        private int number2;

        private String photo;
        
        private String position;
        
        private String nationality;
        
        private String height;
        
        private String weight;
        private String age;
        
        private Date birth;
        
        private String organization;
        
        private BigDecimal lon;
        
        private BigDecimal lat;
        
        private String personnelcategory;
        
        public person() {
                
        }
        
        public person(String name) {
                this.name = name;
        }

    public person(String name, int number, String photo, String position) {
                this.name = name;
        this.number = number;
                this.photo = photo;
        this.position = position;
        }
        
        public String getHeight() {
                return height;
        }

        public void setHeight(String height) {
                this.height = height;
        }

        public String getWeight() {
                return weight;
        }

        public void setWeight(String weight) {
                this.weight = weight;
        }

        public Date getBirth() {
                return birth;
        }

        public void setBirth(Date birth) {
                this.birth = birth;
        }
        
        public person(String name, int number) {
                this.name = name;
                this.number = number;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getPhoto() {
                return photo;
        }

        public void setPhoto(String photo) {
                this.photo = photo;
        }
        
        public String getPosition() {
                return position;
        }

        public void setPosition(String position) {
                this.position = position;
        }

        public String getNationality() {
                return nationality;
        }

        public void setNationality(String nationality) {
                this.nationality = nationality;
        }
        

        public int getNumber() {
                return number;
        }

        public void setNumber(int number) {
                this.number = number;
        }

        @Override
        public boolean equals(Object obj) {
                if(obj == null) {
                return false;
            }
                if(!(obj instanceof person)) {
                return false;
            }
                
                return ((person)obj).getNumber() == this.getNumber();
        }

        @Override
        public int hashCode() {
            int hash = 1;
            return hash * 31 + getName().hashCode();
        }

        @Override
        public String toString() {
                return getName();
        }  

    /**
     * @return the organization
     */
    public String getOrganization() {
        return organization;
    }

    /**
     * @param organization the organization to set
     */
    public void setOrganization(String organization) {
        this.organization = organization;
    }

    /**
     * @return the age
     */
    public String getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(String age) {
        this.age = age;
    }

    /**
     * @return the lon
     */
    public BigDecimal getLon() {
        return lon;
    }

    /**
     * @param lon the lon to set
     */
    public void setLon(BigDecimal lon) {
        this.lon = lon;
    }

    /**
     * @return the lat
     */
    public BigDecimal getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    /**
     * @return the personnelcategory
     */
    public String getPersonnelcategory() {
        return personnelcategory;
    }

    /**
     * @param personnelcategory the personnelcategory to set
     */
    public void setPersonnelcategory(String personnelcategory) {
        this.personnelcategory = personnelcategory;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the number2
     */
    public int getNumber2() {
        return number2;
    }

    /**
     * @param number2 the number2 to set
     */
    public void setNumber2(int number2) {
        this.number2 = number2;
    }
}
