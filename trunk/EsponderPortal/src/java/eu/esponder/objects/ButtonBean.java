/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.objects;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.faces.context.FacesContext;


/**
 *
 * @author Thomas
 */
@ManagedBean
@RequestScoped
public class ButtonBean {

    /**
     * Creates a new instance of ButtonBean
     */
    public ButtonBean() {
    }
    
    public void saveStrategic() {
		addMessage("Data saved");
	}
	
	public void updateStrategic() {
		addMessage("Data updated");
	}
	
	public void deleteStrategic() {
		addMessage("Data deleted");
	}
        
         public void saveTactical() {
		addMessage("Data saved");
	}
	
	public void updateTactical() {
		addMessage("Data updated");
	}
	
	public void deleteTactical() {
		addMessage("Data deleted");
	}
	
         public void saveTeam() {
		addMessage("Data saved");
	}
	
	public void updateTeam() {
		addMessage("Data updated");
	}
	
	public void deleteTeam() {
		addMessage("Data deleted");
	}
	
        
        
         public void saveTeamMembers() {
		addMessage("Data saved");
	}
	
	public void updateTeamMembers() {
		addMessage("Data updated");
	}
	
	public void deleteTeamMembers() {
		addMessage("Data deleted");
	}
        
	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
}
