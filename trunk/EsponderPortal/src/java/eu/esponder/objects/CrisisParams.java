/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.objects;

/**
 *
 * @author tdim
 */
public class CrisisParams {
    private String szname;
    private String szvalue;
    private Long id;

    /**
     * @return the szname
     */
    public String getSzname() {
        return szname;
    }

    /**
     * @param szname the szname to set
     */
    public void setSzname(String szname) {
        this.szname = szname;
    }

    /**
     * @return the szvalue
     */
    public String getSzvalue() {
        return szvalue;
    }

    /**
     * @param szvalue the szvalue to set
     */
    public void setSzvalue(String szvalue) {
        this.szvalue = szvalue;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
}
