/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.equipments;

import eu.esponder.objects.ResourcePlanInfo;
import eu.esponder.objects.person;
import eu.esponder.personnel.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.DragDropEvent;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class EquipmentManager {

    private List<person> personnel;
    private List<person> selectedPersonnel;
private List<ResourcePlanInfo> pPersonnelPlanInfo;
    
    public EquipmentManager() {
        personnel = new ArrayList<person>();
        selectedPersonnel = new ArrayList<person>();
    pPersonnelPlanInfo = new ArrayList<ResourcePlanInfo>();
        pPersonnelPlanInfo.add(new ResourcePlanInfo("Fire Fighter Truck",2,0));
        pPersonnelPlanInfo.add(new ResourcePlanInfo("Ambulance",1,0));
        
        personnel.add(new person("Messi", 10, "po.jpg", "Police Officer"));
        personnel.add(new person("Villa", 7, "po.jpg", "Police Officer"));
        personnel.add(new person("Pedro", 17, "po.jpg", "Police Officer"));
        personnel.add(new person("Bojan", 9, "po.jpg", "Police Officer"));
        personnel.add(new person("Xavi", 6, "po.jpg", "Police Officer"));
        personnel.add(new person("Iniesta", 8, "ff.jpg", "Fire Fighter"));
        personnel.add(new person("Mascherano", 16, "ff.jpg", "Fire Fighter"));
        personnel.add(new person("Puyol", 5, "ff.jpg", "Fire Fighter"));
        personnel.add(new person("Alves", 2, "ff.jpg", "Fire Fighter"));
        personnel.add(new person("Valdes", 1, "ff.jpg", "Fire Fighter"));
        personnel.add(new person("Abidal", 22, "ff.jpg", "Fire Fighter"));
        personnel.add(new person("Adriano", 16, "ff.jpg", "Fire Fighter"));
        personnel.add(new person("Pinto", 13, "ffs.jpg", "Fire Fighter Special"));
        personnel.add(new person("Pique", 3, "ffs.jpg", "Fire Fighter Special"));
        personnel.add(new person("Keita", 7, "ffs.jpg", "Fire Fighter Special"));
        personnel.add(new person("Maxwell", 5, "ffs.jpg", "Fire Fighter Special"));
    }

    public List<person> getPersonnel() {
        return personnel;
    }

    public List<person> getSelectedPersonnel() {
        return selectedPersonnel;
    }

    public void onPersonDrop(DragDropEvent event) {
        person player = (person) event.getData();

        selectedPersonnel.add(player);
        personnel.remove(player);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(player.getName() + " added", "Position:" + event.getDropId()));
    }
    /**
     * @return the pPersonnelPlanInfo
     */
    public List<ResourcePlanInfo> getpPersonnelPlanInfo() {
        return pPersonnelPlanInfo;
    }

    /**
     * @param pPersonnelPlanInfo the pPersonnelPlanInfo to set
     */
    public void setpPersonnelPlanInfo(List<ResourcePlanInfo> pPersonnelPlanInfo) {
        this.pPersonnelPlanInfo = pPersonnelPlanInfo;
    }
}
