/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.newcrisis;

import eu.esponder.client.rccall;
import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextAlertEnumDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.CrisisContextParameterDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.CrisisContextStatusEnumDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.status.CrisisContextSnapshotStatusDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;
import eu.esponder.map.MapBean;
import eu.esponder.map.geocoder.Coordinates;
import eu.esponder.map.geocoder.addressToCordinates;
import eu.esponder.objects.CrisisParams;
import eu.esponder.ws.client.create.CreateOperations;
import eu.esponder.ws.client.crisisparams.crisisparamswsmanager;
import eu.esponder.ws.client.logic.CrisisActionAndActionPartManager;
import eu.esponder.ws.client.logic.CrisisTypeQueryManager;
//import eu.eventadmin.managebeans.EventAdminController;
//import eu.eventadmin.managebeans.EventAdminServer;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.primefaces.context.RequestContext;
import org.primefaces.event.DashboardReorderEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.xml.sax.SAXException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
/**
 *
 * @author Thomas
 */
@ManagedBean
@ApplicationScoped
public class crisiselementsmanagerbean implements Serializable {

    private DashboardModel model;
    private String status;
    private String addinfo;
    private String id;
    private String titlec;
    private String location = "Leoforos Mesogion 17, Athens, Greece";
    private String location1 = "Leoforos Mesogion 17";
    private String location2 = "";
    private String location3 = "Athens";
    private String location4 = "Greece";
    private Date date1;
    private Date date2;
    private Date date3;
    private List<String> naturalDisaster;
    private Map<String, String> nDisaster;
    private List<String> otherDisaster;
    private Map<String, String> oDisaster;
    private MapModel emptyModel;
    private String title;
    private double lat;
    private double lng;
    CrisisTypeQueryManager pct = new CrisisTypeQueryManager();
    public CrisisContextAlert alertlevel;
    private List<CrisisParams> crisisParams;
    private CrisisParams pSelectedCrisisParams;
    private String szname;
    private String szvalue;
    Long pCrisisSavedID;
    CrisisContextDTO persistedcc = null;

    public String CreateNewCrisisProcess() {


        status = "";
        addinfo = "";
        id = "";
        titlec = "";
        location = "Leoforos Mesogion 17, Athens, Greece";
        location1 = "Leoforos Mesogion 17";
        location2 = "";
        location3 = "Athens";
        location4 = "Greece";
        date1 = new Date();
        date2 = new Date();
        date3 = new Date();

        title = "";




        szname = "";
        szvalue = "";
        persistedcc = null;
        crisisParams = new ArrayList();
        return "CreateNewCrisis?faces-redirect=true";
    }

    public String addparams() {
        szname = "";
        szvalue = "";
        return "AddCrisisParams_c?faces-redirect=true";
    }

    public String saveparamsandpublish() {
        CreateOperations pCreate = new CreateOperations();
        try {
            CrisisContextDTO persistedcc2 = pCreate.createCrisisContext("1", persistedcc, 3);
        } catch (JsonGenerationException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "EsponderDashBoard?faces-redirect=true";
    }

    public String saveparams() {
        crisisparamswsmanager pcreate = new crisisparamswsmanager();
        try {
            pcreate.CreateParam(String.valueOf(pCrisisSavedID), szname, szvalue, "", "");
        } catch (Exception ex) {
            Logger.getLogger(MapBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        LoadParams();
        return "EditParams_c?faces-redirect=true";
    }

    public String deleteparams() {
        crisisparamswsmanager pcreate = new crisisparamswsmanager();
        try {
            pcreate.deletepcrisis("", "", String.valueOf(pSelectedCrisisParams.getId()));//.CreateParam(String.valueOf(pSelectedCrisis.getpCrisisDTO().getId()), szname, szvalue, "", "");
            System.out.println("Delete : " + String.valueOf(pSelectedCrisisParams.getId()));
        } catch (Exception ex) {
            Logger.getLogger(MapBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        LoadParams();
        return "EditParams_c?faces-redirect=true";
    }

    public void LoadParams() {
        crisisparamswsmanager pque = new crisisparamswsmanager();
        ResultListDTO presults = pque.FindAllForCrisis(String.valueOf(pCrisisSavedID), null, null);
        System.out.println(presults.getResultList().size());
        //ist<CrisisParams> 
        crisisParams = new ArrayList<CrisisParams>();
        for (int i = 0; i < presults.getResultList().size(); i++) {
            CrisisContextParameterDTO pEntry = (CrisisContextParameterDTO) presults.getResultList().get(i);
            CrisisParams pEntry2 = new CrisisParams();
            pEntry2.setId(pEntry.getId());
            pEntry2.setSzname(pEntry.getParamName());
            pEntry2.setSzvalue(pEntry.getParamValue());
            crisisParams.add(pEntry2);
        }
    }

    /**
     * @return the location1
     */
    public String getLocation1() {
        return location1;
    }

    /**
     * @param location1 the location1 to set
     */
    public void setLocation1(String location1) {
        this.location1 = location1;
    }

    /**
     * @return the location2
     */
    public String getLocation2() {
        return location2;
    }

    /**
     * @param location2 the location2 to set
     */
    public void setLocation2(String location2) {
        this.location2 = location2;
    }

    /**
     * @return the location3
     */
    public String getLocation3() {
        return location3;
    }

    /**
     * @param location3 the location3 to set
     */
    public void setLocation3(String location3) {
        this.location3 = location3;
    }

    /**
     * @return the location4
     */
    public String getLocation4() {
        return location4;
    }

    /**
     * @param location4 the location4 to set
     */
    public void setLocation4(String location4) {
        this.location4 = location4;
    }

    /**
     * @return the crisisParams
     */
    public List<CrisisParams> getCrisisParams() {
        return crisisParams;
    }

    /**
     * @param crisisParams the crisisParams to set
     */
    public void setCrisisParams(List<CrisisParams> crisisParams) {
        this.crisisParams = crisisParams;
    }

    /**
     * @return the pSelectedCrisisParams
     */
    public CrisisParams getpSelectedCrisisParams() {
        return pSelectedCrisisParams;
    }

    /**
     * @param pSelectedCrisisParams the pSelectedCrisisParams to set
     */
    public void setpSelectedCrisisParams(CrisisParams pSelectedCrisisParams) {
        this.pSelectedCrisisParams = pSelectedCrisisParams;
    }

    /**
     * @return the szname
     */
    public String getSzname() {
        return szname;
    }

    /**
     * @param szname the szname to set
     */
    public void setSzname(String szname) {
        this.szname = szname;
    }

    /**
     * @return the szvalue
     */
    public String getSzvalue() {
        return szvalue;
    }

    /**
     * @param szvalue the szvalue to set
     */
    public void setSzvalue(String szvalue) {
        this.szvalue = szvalue;
    }

    /**
     * @return the values
     */
    public CrisisContextAlert[] getValues() {
        return values;
    }

    /**
     * @param values the values to set
     */
    public void setValues(CrisisContextAlert[] values) {
        this.values = values;
    }

    public enum CrisisContextAlert {

        Level1,
        Level2,
        Level3,
        Level4,
        Level5
    }
    private CrisisContextAlert values[] = CrisisContextAlert.values();

    public CrisisContextAlert[] getvalues() {
        return CrisisContextAlert.values();
    }

    public void doSearchajax(ActionEvent actionEvent) {
        try {
            System.err.print("Location = " + this.location);
            addressToCordinates pGPS = new addressToCordinates();
            location = location1 + "," + location3 + " " + location2 + "," + location4;
            Coordinates pCordinates = pGPS.FindAddressCoordinates(this.location);
            
        } catch (IOException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XPathExpressionException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String doSearch(ActionEvent actionEvent) {
        System.err.print("Location = " + this.location);
        return null;
    }

    public void updateLocation() {
        System.err.print("Location = " + this.location);
    }

    public MapModel getEmptyModel() {
        return emptyModel;
    }

    public void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void addMarker(ActionEvent actionEvent) {
        Marker marker = new Marker(new LatLng(lat, lng), title);
        emptyModel.addOverlay(marker);

        addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "Marker Added", "Lat:" + lat + ", Lng:" + lng));
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public Date getDate3() {
        return date3;
    }

    public void setDate3(Date date3) {
        this.date3 = date3;
    }

    public String GetPath()
    {
        if(File.separatorChar == '/')
            return "//home//esponder//uploads//";
        else
            return "c://data//uploads//";
    }
    public void handleFileUpload(FileUploadEvent event) {
       try {
            File targetFolder = new File("c:\\data\\");
            InputStream inputStream = event.getFile().getInputstream();
            OutputStream out = new FileOutputStream(new File(targetFolder,
                    event.getFile().getFileName()));
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            inputStream.close();
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public crisiselementsmanagerbean() {
        emptyModel = new DefaultMapModel();
        model = new DefaultDashboardModel();
        DashboardColumn column1 = new DefaultDashboardColumn();
        DashboardColumn column2 = new DefaultDashboardColumn();

        column1.addWidget("details");
        column1.addWidget("typearea");

        column2.addWidget("maps");
        column1.addWidget("alert");
        column1.addWidget("additionalinfo");

        model.addColumn(column1);
        model.addColumn(column2);

        nDisaster = new HashMap<String, String>();

        oDisaster = new HashMap<String, String>();

        try {
            pct.LoadAllCrisisTypes();
        } catch (JsonParseException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < pct.getpCrisisDisasterTypes().size(); i++) {
            nDisaster.put(pct.getpCrisisDisasterTypes().get(i).getTitle(), pct.getpCrisisDisasterTypes().get(i).getTitle());
            System.out.println(pct.getpCrisisDisasterTypes().get(i).getTitle());
        }
        for (int i = 0; i < pct.getpCrisisFeatureTypes().size(); i++) {
            oDisaster.put(pct.getpCrisisFeatureTypes().get(i).getTitle(), pct.getpCrisisFeatureTypes().get(i).getTitle());
            System.out.println(pct.getpCrisisFeatureTypes().get(i).getTitle());
        }

    }

    public void handleReorder(DashboardReorderEvent event) {
        FacesMessage message = new FacesMessage();
        message.setSeverity(FacesMessage.SEVERITY_INFO);
        message.setSummary("Reordered: " + event.getWidgetId());
        message.setDetail("Item index: " + event.getItemIndex() + ", Column index: " + event.getColumnIndex() + ", Sender index: " + event.getSenderColumnIndex());

        addMessage(message);
    }

    public DashboardModel getModel() {
        return model;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the addinfo
     */
    public String getAddinfo() {
        return addinfo;
    }

    /**
     * @param addinfo the addinfo to set
     */
    public void setAddinfo(String addinfo) {
        this.addinfo = addinfo;
    }

    /**
     * @return the alertlevel
     */
    public CrisisContextAlert getAlertlevel() {
        return alertlevel;
    }

    /**
     * @param alertlevel the alertlevel to set
     */
    public void setAlertlevel(CrisisContextAlert alertlevel) {
        this.alertlevel = alertlevel;
    }

    /**
     * @return the naturalDisaster
     */
    public List<String> getNaturalDisaster() {
        return naturalDisaster;
    }

    /**
     * @param naturalDisaster the naturalDisaster to set
     */
    public void setNaturalDisaster(List<String> naturalDisaster) {
        this.naturalDisaster = naturalDisaster;
    }

    /**
     * @return the nDisaster
     */
    public Map<String, String> getnDisaster() {
        return nDisaster;
    }

    /**
     * @param nDisaster the nDisaster to set
     */
    public void setnDisaster(Map<String, String> nDisaster) {
        this.nDisaster = nDisaster;
    }

    /**
     * @return the otherDisaster
     */
    public List<String> getOtherDisaster() {
        return otherDisaster;
    }

    /**
     * @param otherDisaster the otherDisaster to set
     */
    public void setOtherDisaster(List<String> otherDisaster) {
        this.otherDisaster = otherDisaster;
    }

    /**
     * @return the oDisaster
     */
    public Map<String, String> getoDisaster() {
        return oDisaster;
    }

    /**
     * @param oDisaster the oDisaster to set
     */
    public void setoDisaster(Map<String, String> oDisaster) {
        this.oDisaster = oDisaster;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitlec() {
        return titlec;
    }

    /**
     * @param title the title to set
     */
    public void setTitlec(String titlec) {
        this.titlec = titlec;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return getLocation1() + "," + getLocation3() + " " + getLocation2() + "," + getLocation4();
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    public SphereDTO CreateSphere(PointDTO pPoint, String title) throws IOException {
        CreateOperations pCreate = new CreateOperations();
        return pCreate.CreateSphereNoRadius("1", title, pPoint);
    }

    public String createCrisis() {
        try {
            System.out.println("\n\n******* Create New crisis *******");
            CreateOperations pCreate = new CreateOperations();
            addressToCordinates pGPS = new addressToCordinates();
            location = location1 + "," + location3 + " " + location2 + "," + location4;
            Coordinates pCordinates = pGPS.FindAddressCoordinates(this.location);
            PointDTO pPoint = new PointDTO();
            pPoint.setAltitude(BigDecimal.ZERO);
            pPoint.setLatitude(new BigDecimal(Float.toString(pCordinates.getLatitude())));
            pPoint.setLongitude(new BigDecimal(Float.toString(pCordinates.getLongitude())));

            SphereDTO pSphere = CreateSphere(pPoint, this.titlec + " - Location");

            CrisisContextDTO pCrisisContext = new CrisisContextDTO();
            pCrisisContext.setCrisisLocation(pSphere);
            pCrisisContext.setTitle(titlec);
            if (status.equalsIgnoreCase("In Progress")) {
                pCrisisContext.setCrisisStatus(CrisisContextStatusEnumDTO.INPROGRESS);
            } else {
                pCrisisContext.setCrisisStatus(CrisisContextStatusEnumDTO.FINISHED);
            }

            pCrisisContext.setCrisisAdditionalInfo(addinfo);
            if (date1 != null) {
                pCrisisContext.setStartDate(date1.getTime());
                //pCrisisContext.setStartDate(this.date1.getTime());
            }
            if (date2 != null) {
                pCrisisContext.setEndDate(date2.getTime());
            }

            if (CrisisContextAlertEnumDTO.Level1.toString() == alertlevel.toString()) {
                pCrisisContext.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level1);
            }
            if (CrisisContextAlertEnumDTO.Level2.toString() == alertlevel.toString()) {
                pCrisisContext.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level2);
            }
            if (CrisisContextAlertEnumDTO.Level3.toString() == alertlevel.toString()) {
                pCrisisContext.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level3);
            }
            if (CrisisContextAlertEnumDTO.Level4.toString() == alertlevel.toString()) {
                pCrisisContext.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level4);
            }
            if (CrisisContextAlertEnumDTO.Level5.toString() == alertlevel.toString()) {
                pCrisisContext.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level5);
            }

            pCrisisContext.setCrisisTypes(new HashSet<CrisisTypeDTO>());

            for (int i = 0; i < naturalDisaster.size(); i++) {
                String szTitle = naturalDisaster.get(i);
                if (pct.FindCrisisDisasterWithTitle(szTitle) == null) {
                    if (pct.FindCrisisDisasterFeatureWithTitle(szTitle) == null) {
                    } else {
                        pCrisisContext.getCrisisTypes().add((CrisisTypeDTO) pct.FindCrisisDisasterFeatureWithTitle(szTitle));

                    }
                } else {
                    pCrisisContext.getCrisisTypes().add((CrisisTypeDTO) pct.FindCrisisDisasterWithTitle(szTitle));

                }
            }
            persistedcc = pCreate.createCrisisContext("1", pCrisisContext, 2);
            pCrisisSavedID = persistedcc.getId();


            CrisisActionAndActionPartManager pAAPManager = new CrisisActionAndActionPartManager();
            ActionDTO pAction = pAAPManager.CreateAction(persistedcc);
            //ActionPartDTO pActionPart = pAAPManager.CreateActionPart(pAction, persistedcc);

            pAAPManager.CreateCrisisContextSnap("1", persistedcc.getId().toString(), this.titlec + " - Location");


            return "EditParams_c?faces-redirect=true";
            //return "#{mapBean.UpdateCrisisList}";
        } catch (XPathExpressionException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (SAXException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (JsonGenerationException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (JsonMappingException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(crisiselementsmanagerbean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }
}