//package eu.esponder.client;
//
//import java.io.IOException;
//import java.math.BigDecimal;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
//import javax.ws.rs.core.MultivaluedMap;
//
//import org.codehaus.jackson.JsonGenerationException;
//import org.codehaus.jackson.map.JsonMappingException;
//import org.codehaus.jackson.map.ObjectMapper;
//import org.codehaus.jackson.type.TypeReference;
//
//import com.sun.jersey.api.client.Client;
//import com.sun.jersey.api.client.WebResource;
//import com.sun.jersey.core.util.MultivaluedMapImpl;
//
//import eu.esponder.dto.model.ResultListDTO;
//import eu.esponder.dto.model.crisis.CrisisContextDTO;
//import eu.esponder.dto.model.crisis.resource.ActorDTO;
//import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
//import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
//import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
//import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
//import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
//import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
//import eu.esponder.dto.model.snapshot.location.PointDTO;
//import eu.esponder.dto.model.snapshot.location.SphereDTO;
//import eu.esponder.ws.client.create.CreateOperations;
//import eu.esponder.ws.client.query.QueryManager;
//
//public class rccall_1 {
//
//	ObjectMapper mapper = new ObjectMapper();
//
//	public ActorDTO getActorID(String userID, String actorID) {
//		ActorDTO actorDTO = null;
//		Client client = Client.create();
//		WebResource webResource = client
//				.resource("http://rdocs.exodussa.com:8080/esponder-restful/crisis/resource/actor/getID");
//		MultivaluedMap queryParams = new MultivaluedMapImpl();
//		queryParams.add("userID", userID);
//		queryParams.add("actorID", actorID);
//		String s = webResource.queryParams(queryParams).get(String.class);
//		ObjectMapper mapper = new ObjectMapper();
//		try {
//			actorDTO = mapper.readValue(s, ActorDTO.class);
//
//		} catch (IOException ex) {
//			Logger.getLogger(rccall.class.getName())
//					.log(Level.SEVERE, null, ex);
//		}
//
//		return actorDTO;
//	}
//
//	public SphereDTO CreateSphere(String userID,String szTitle,PointDTO pointDTO) throws JsonGenerationException, JsonMappingException, IOException
//	{
//
//		SphereDTO sphereDTO = new SphereDTO(pointDTO, new BigDecimal(2),szTitle);
//		String sphereJSON = mapper.writeValueAsString(sphereDTO);
//
//		Client client2 = Client.create();
//		WebResource webResource2 = client2
//				.resource("http://rdocs.exodussa.com:8080/esponder-restful/crisis/generic/create");
//		MultivaluedMap queryParams2 = new MultivaluedMapImpl();
//		queryParams2.add("userID", userID);
//		String s2;
//		s2 = webResource2.queryParams(queryParams2)
//				.type("application/json").post(String.class, sphereJSON);
//		SphereDTO spherePersisted = mapper.readValue(s2, SphereDTO.class);
//		return spherePersisted;
//	}
//	
//	public void createCrisisContext(String userID) {
//		try {
//			PointDTO pointDTO = new PointDTO(new BigDecimal(3), new BigDecimal(
//					3), new BigDecimal(3));
//			SphereDTO sphereDTO = new SphereDTO(pointDTO, new BigDecimal(2),
//					"portal - Fire Brigade Drill Location Area 3");
//			String sphereJSON = mapper.writeValueAsString(sphereDTO);
//
//			Client client2 = Client.create();
//			WebResource webResource2 = client2
//					.resource("http://rdocs.exodussa.com:8080/esponder-restful/crisis/generic/create");
//			MultivaluedMap queryParams2 = new MultivaluedMapImpl();
//			queryParams2.add("userID", userID);
//			String s2;
//			s2 = webResource2.queryParams(queryParams2)
//					.type("application/json").post(String.class, sphereJSON);
//			SphereDTO spherePersisted = mapper.readValue(s2, SphereDTO.class);
//
//			CrisisContextDTO crisisContextDTO = new CrisisContextDTO();
//			crisisContextDTO.setCrisisLocation(spherePersisted);
//			crisisContextDTO.setTitle("Fire Brigade Drill test 22");
//
//			String crisisContextJSON = mapper
//					.writeValueAsString(crisisContextDTO);
//			
//			Client client = Client.create();
//			WebResource webResource = client
//					.resource("http://rdocs.exodussa.com:8080/esponder-restful/crisis/view/crisisContext/create");
//			MultivaluedMap queryParams = new MultivaluedMapImpl();
//			queryParams.add("userID", userID);
//			String s;
//
//			s = webResource.queryParams(queryParams).type("application/json")
//					.post(String.class, crisisContextJSON);
//		} catch (IOException ex) {
//			Logger.getLogger(rccall.class.getName())
//					.log(Level.SEVERE, null, ex);
//		}
//
//	}
//	
//	
//	public void createRegisterOC(String userID) throws JsonGenerationException, JsonMappingException, IOException {
//		///crisis/context/regoc/create
//		RegisteredOperationsCentreDTO registeredOperationsCentreDTO=new RegisteredOperationsCentreDTO();
//		VoIPURLDTO voIPURL=new VoIPURLDTO();
//		voIPURL.setHost("host");
//		voIPURL.setPath("path");
//		voIPURL.setProtocol("protocol");
//		//registeredOperationsCentreDTO.setResourceId(resourceId);
//		registeredOperationsCentreDTO.setStatus(ResourceStatusDTO.AVAILABLE);
//		
//		registeredOperationsCentreDTO.setTitle("MEOC Athens II");
//		//registeredOperationsCentreDTO.setType(type);
//		registeredOperationsCentreDTO.setVoIPURL(voIPURL);
//		String crisisContextJSON = mapper.writeValueAsString(registeredOperationsCentreDTO);
//		
//		Client client = Client.create();
//		WebResource webResource = client
//				.resource("http://localhost:8080/esponder-restful/crisis/context/regoc/create");
//		MultivaluedMap queryParams = new MultivaluedMapImpl();
//		queryParams.add("userID", userID);
//		String s;
//
//		s = webResource.queryParams(queryParams).type("application/json")
//				.post(String.class, crisisContextJSON);
//	}
//
//	public ResultListDTO getRegisterdOCAll(String userID) {
//		ResultListDTO  pResults = null;
//		Client client = Client.create();
//		WebResource webResource = client
//				.resource("http://localhost:8080/esponder-restful/crisis/context/regoc/findAll");
//		MultivaluedMap queryParams = new MultivaluedMapImpl();
//		queryParams.add("userID", userID);
//		String s = webResource.queryParams(queryParams).get(String.class);
//		System.out.println("Json : "+s);
//		
//		ObjectMapper mapper = new ObjectMapper();
//		try {
//			pResults = mapper.readValue(s,ResultListDTO.class);
//
//		} catch (IOException ex) {
//			ex.printStackTrace();
//		}
//        System.out.println("Size : "+pResults.getResultList().size());
//		return pResults;
//	}
//	/**
//	 * @param args
//	 * @throws IOException 
//	 * @throws JsonMappingException 
//	 * @throws JsonGenerationException 
//	 */
//	public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
////		QueryManager pQuery = new QueryManager();
////                      ResultListDTO pResults=pQuery.getPersonnelAll("1");
////                      PersonnelDTO pitem = new PersonnelDTO();
////                      pitem=(PersonnelDTO) pResults.getResultList().get(0);
////                      System.out.print(pitem.getTitle());
//                      CreateOperations pop = new CreateOperations();
//                      
//                      RegisteredReusableResourceDTO pResource = new RegisteredReusableResourceDTO();
//                      pResource.setTitle("Tent");
//                      pResource.setQuantity(BigDecimal.TEN);
//                      pResource.setStatus(ResourceStatusDTO.AVAILABLE);
//                      pop.createRegisterReusableResource("1", pResource);
//                              
//
//	}
//
//}
