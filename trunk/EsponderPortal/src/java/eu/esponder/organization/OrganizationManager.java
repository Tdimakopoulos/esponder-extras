/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.organization;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextAlertEnumDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.CrisisContextStatusEnumDTO;
import eu.esponder.dto.model.crisis.resource.OrganisationDTO;
import eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.dto.model.snapshot.location.AddressDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.map.geocoder.Coordinates;
import eu.esponder.map.geocoder.addressToCordinates;
import eu.esponder.newcrisis.crisiselementsmanagerbean;
import eu.esponder.ws.client.create.CreateOperations;
import eu.esponder.ws.client.logic.CrisisActionAndActionPartManager;
import eu.esponder.ws.client.query.QueryManager;
import eu.esponder.ws.client.update.UpdateOperations;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.xml.sax.SAXException;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class OrganizationManager {
private Map<String, String> valuesDisciplineType;
private Map<String, String> valuesOrganizationType;
private String valuesDisciplineTypeSelected;
private String valuesOrganizationTypeSelected;
private String title;
private String street;
private String number;
private String town;
private String zip;
private String country;
private String ratitle;
private String log;
private String lat;
private String alt;
private String radius;


ResultListDTO pRes=null;
ResultListDTO pOrgs=null;
private List<OrganisationDTO> pAllOrgs=new ArrayList<OrganisationDTO>();


public void LoadALlOrg() throws JsonParseException, IOException
{
    QueryManager pQuery=new QueryManager();
    pOrgs=pQuery.getOrgAll("6");
    for(int i=0;i<pOrgs.getResultList().size();i++)
    {
            getpAllOrgs().add((OrganisationDTO)pOrgs.getResultList().get(i));
    }
    
    OrganisationDTO p=new OrganisationDTO();
//    p.getResponsibilityArea().getTitle()
}
    /**
     * Address -> country, number, prefecture, street, zip
     * Point -> Log,Lat,Alt
     * Sphere -> Point, Radius,Title
     * Organization -> Title,address,point,responsibility title
     */
    
    
    public DisciplineTypeDTO FindDisciByID(Long ID)
    {
        for(int i=0;i<pRes.getResultList().size();i++)
            {
                ESponderTypeDTO pEnt=(ESponderTypeDTO) pRes.getResultList().get(i);
                if(pEnt instanceof DisciplineTypeDTO)
                {
                    if(pEnt.getId()==ID)
                    {
                        return (DisciplineTypeDTO)pEnt;
                    }
                }
            }
        return null;
    }
    
    public OrganisationTypeDTO FindOrgTypeByID(Long ID)
    {
        for(int i=0;i<pRes.getResultList().size();i++)
            {
                ESponderTypeDTO pEnt=(ESponderTypeDTO) pRes.getResultList().get(i);
                if(pEnt instanceof OrganisationTypeDTO)
                {
                    if(pEnt.getId()==ID)
                    {
                        return (OrganisationTypeDTO)pEnt;
                    }
                }
            }
        return null;
    }
    /**
     * Creates a new instance of OrganizationManager
     */
    public OrganizationManager() throws IOException {
                valuesDisciplineType = new HashMap<String, String>();
                valuesOrganizationType = new HashMap<String, String>();
    try {        
        QueryManager pQuery= new QueryManager();
            pRes=pQuery.getAllTypes("6");
            System.out.println(" Size "+pRes.getResultList().size());
            for(int i=0;i<pRes.getResultList().size();i++)
            {
                ESponderTypeDTO pEnt=(ESponderTypeDTO) pRes.getResultList().get(i);
                if(pEnt instanceof DisciplineTypeDTO)
                {
                
                valuesDisciplineType.put( pEnt.getTitle(),pEnt.getId().toString());
                }
                if(pEnt instanceof OrganisationTypeDTO)
                {
                
                valuesOrganizationType.put( pEnt.getTitle(),pEnt.getId().toString());
                }
            }
    } catch (JsonParseException ex) {
        Logger.getLogger(OrganizationManager.class.getName()).log(Level.SEVERE, null, ex);
    } catch (JsonMappingException ex) {
        Logger.getLogger(OrganizationManager.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
        Logger.getLogger(OrganizationManager.class.getName()).log(Level.SEVERE, null, ex);
    }
    LoadALlOrg();
    }

    /**
     * @return the valuesDisciplineType
     */
    public Map<String, String> getValuesDisciplineType() {
        return valuesDisciplineType;
    }

    /**
     * @param valuesDisciplineType the valuesDisciplineType to set
     */
    public void setValuesDisciplineType(Map<String, String> valuesDisciplineType) {
        this.valuesDisciplineType = valuesDisciplineType;
    }

    /**
     * @return the valuesOrganizationType
     */
    public Map<String, String> getValuesOrganizationType() {
        return valuesOrganizationType;
    }

    /**
     * @param valuesOrganizationType the valuesOrganizationType to set
     */
    public void setValuesOrganizationType(Map<String, String> valuesOrganizationType) {
        this.valuesOrganizationType = valuesOrganizationType;
    }

    /**
     * @return the valuesDisciplineTypeSelected
     */
    public String getValuesDisciplineTypeSelected() {
        return valuesDisciplineTypeSelected;
    }

    /**
     * @param valuesDisciplineTypeSelected the valuesDisciplineTypeSelected to set
     */
    public void setValuesDisciplineTypeSelected(String valuesDisciplineTypeSelected) {
        this.valuesDisciplineTypeSelected = valuesDisciplineTypeSelected;
    }

    /**
     * @return the valuesOrganizationTypeSelected
     */
    public String getValuesOrganizationTypeSelected() {
        return valuesOrganizationTypeSelected;
    }

    /**
     * @param valuesOrganizationTypeSelected the valuesOrganizationTypeSelected to set
     */
    public void setValuesOrganizationTypeSelected(String valuesOrganizationTypeSelected) {
        this.valuesOrganizationTypeSelected = valuesOrganizationTypeSelected;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return the town
     */
    public String getTown() {
        return town;
    }

    /**
     * @param town the town to set
     */
    public void setTown(String town) {
        this.town = town;
    }

    /**
     * @return the zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param zip the zip to set
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the ratitle
     */
    public String getRatitle() {
        return ratitle;
    }

    /**
     * @param ratitle the ratitle to set
     */
    public void setRatitle(String ratitle) {
        this.ratitle = ratitle;
    }

    /**
     * @return the log
     */
    public String getLog() {
        return log;
    }

    /**
     * @param log the log to set
     */
    public void setLog(String log) {
        this.log = log;
    }

    /**
     * @return the lat
     */
    public String getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     * @return the alt
     */
    public String getAlt() {
        return alt;
    }

    /**
     * @param alt the alt to set
     */
    public void setAlt(String alt) {
        this.alt = alt;
    }

    /**
     * @return the radius
     */
    public String getRadius() {
        return radius;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(String radius) {
        this.radius = radius;
    }
    
    public SphereDTO CreateSphere(PointDTO pPoint, String title) throws IOException {
        CreateOperations pCreate = new CreateOperations();
        return pCreate.CreateSphereNoRadius("1", title, pPoint);
    }
    
    public String createOrg() throws IOException {
        
            
            CreateOperations pCreate = new CreateOperations();
            
            PointDTO pPoint = new PointDTO();
            pPoint.setAltitude(BigDecimal.valueOf(Long.valueOf(alt)));
            pPoint.setLatitude(BigDecimal.valueOf(Long.valueOf(lat)));
            pPoint.setLongitude(BigDecimal.valueOf(Long.valueOf(log)));

            SphereDTO pSphere = CreateSphere(pPoint, this.ratitle);

            AddressDTO address = new AddressDTO();
		address.setCountry(country);
		address.setNumber(number);
		address.setPrefecture(town);
		address.setStreet(street);
		address.setZipCode(zip);

                
            OrganisationDTO organisation = new OrganisationDTO();
			organisation.setAddress(address);
			organisation.setLocation(pPoint);
			organisation.setResponsibilityArea(pSphere);
			organisation.setTitle(title);
            OrganisationDTO  organisation2=pCreate.createOrganization("1", organisation);
            
            DisciplineTypeDTO pDisc=FindDisciByID(Long.valueOf(this.getValuesDisciplineTypeSelected()));
            OrganisationTypeDTO pOrgType=FindOrgTypeByID(Long.valueOf(this.getValuesOrganizationTypeSelected()));
            QueryManager pquery= new QueryManager();
             OrganisationCategoryDTO pFind=pquery.getResourceCategoryOrganization(Long.valueOf(this.getValuesDisciplineTypeSelected()),Long.valueOf(this.getValuesOrganizationTypeSelected()),"6");
            //OrganisationCategoryDTO pFind=(OrganisationCategoryDTO)pFind2.getResultList().get(0);
             //pFind.setOrganisationId(organisation2.getId());
            UpdateOperations pUpdate=new UpdateOperations();
            pUpdate.updateOrganizationCategory(pFind.getId(),
pDisc.getId(),
pOrgType.getId(),
			organisation2.getId(),
                        organisation2,
			"6");
            LoadALlOrg();
            return "Organization_sel?faces-redirect=true";
            
    }

    /**
     * @return the pAllOrgs
     */
    public List<OrganisationDTO> getpAllOrgs() {
        return pAllOrgs;
    }

    /**
     * @param pAllOrgs the pAllOrgs to set
     */
    public void setpAllOrgs(List<OrganisationDTO> pAllOrgs) {
        this.pAllOrgs = pAllOrgs;
    }
}
