/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.journal;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.events.entity.OsgiEventsEntityDTO;
import eu.esponder.events.EventsDTO;
import eu.esponder.objects.Logistic;
import eu.esponder.ws.client.query.QueryManager;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.SessionScoped;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

/**
 *
 * @author Thomas
 */
@ManagedBean
@SessionScoped
public class journalview implements Serializable {
    
    private List<EventsDTO> eventsDTO;
    private Date date1;
    private Date date2;
    private String jcrisiscontext;
    private String jactor;
    private String jaction;
    private String jsnapshot;
    private String joperationcenter;
    private String jsystem;

    /**
     * Creates a new instance of journalview
     */
    public journalview() {
        eventsDTO = new ArrayList<EventsDTO>();
        
        populateEvents();
    }
    
    public String searchonlist() {
        eventsDTO.clear();
        QueryManager pQuery = new QueryManager();
        ResultListDTO presults = null;
        //System.out.println(jaction);
        try {
            presults = pQuery.getEventsBySeverity("1", jaction);
        } catch (JsonParseException ex) {
            Logger.getLogger(journalview.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(journalview.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(journalview.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < presults.getResultList().size(); i++) //list.add(new Car(getRandomModel(), getRandomYear(), getRandomManufacturer(), getRandomColor()));
        {
            OsgiEventsEntityDTO pitem = (OsgiEventsEntityDTO) presults.getResultList().get(i);
            Long p1 = pitem.getTimeStamp();
          //  System.out.print("check dates"+date1+date2);
            if (date1 != null && date2 != null) {
            //    System.out.print("check dates");
                Long i1 = date1.getTime();
                Long i2 = date2.getTime();
                if ((p1 <= i2) && (p1 >= i1)) {
                    EventsDTO e = new EventsDTO();
                    e.setEaddinfo(pitem.getAttachment());
                    e.setEseverity(pitem.getSeverity());
                    e.setEsource(pitem.getSource());
                    e.setEtimestamp(pitem.getTimeStamp().toString());
                    
                    eventsDTO.add(e);
                }
            } else {
                EventsDTO e = new EventsDTO();
                e.setEaddinfo(pitem.getAttachment());
                e.setEseverity(pitem.getSeverity());
                e.setEsource(pitem.getSource());
                e.setEtimestamp(pitem.getTimeStamp().toString());
                
                eventsDTO.add(e);
            }
        }
        return "CrisisJournal?faces-redirect=true";
    }

    /*
     * MINIMAL
     * MEDIUM
     * SERIOUS
     * SEVERE
     * FATAL
     * UNDEFINED
     */
    private void populateEvents() {
        QueryManager pQuery = new QueryManager();
        ResultListDTO presults = null;
        try {
            presults = pQuery.getEventsBySeverity("1", "FATAL");
        } catch (JsonParseException ex) {
            Logger.getLogger(journalview.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(journalview.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(journalview.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < presults.getResultList().size(); i++) //list.add(new Car(getRandomModel(), getRandomYear(), getRandomManufacturer(), getRandomColor()));
        {
            OsgiEventsEntityDTO pitem = (OsgiEventsEntityDTO) presults.getResultList().get(i);
            EventsDTO e = new EventsDTO();
            e.setEaddinfo(pitem.getAttachment());
            e.setEseverity(pitem.getSeverity());
            e.setEsource(pitem.getSource());
            e.setEtimestamp(pitem.getTimeStamp().toString());

            //e.setEtype(pitem.g);
            getEventsDTO().add(e);
        }
    }

    /**
     * @return the eventsDTO
     */
    public List<EventsDTO> getEventsDTO() {
        return eventsDTO;
    }

    /**
     * @param eventsDTO the eventsDTO to set
     */
    public void setEventsDTO(List<EventsDTO> eventsDTO) {
        this.eventsDTO = eventsDTO;
    }

    /**
     * @return the date1
     */
    public Date getDate1() {
        return date1;
    }

    /**
     * @param date1 the date1 to set
     */
    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    /**
     * @return the date2
     */
    public Date getDate2() {
        return date2;
    }

    /**
     * @param date2 the date2 to set
     */
    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    /**
     * @return the jcrisiscontext
     */
    public String getJcrisiscontext() {
        return jcrisiscontext;
    }

    /**
     * @param jcrisiscontext the jcrisiscontext to set
     */
    public void setJcrisiscontext(String jcrisiscontext) {
        this.jcrisiscontext = jcrisiscontext;
    }

    /**
     * @return the jactor
     */
    public String getJactor() {
        return jactor;
    }

    /**
     * @param jactor the jactor to set
     */
    public void setJactor(String jactor) {
        this.jactor = jactor;
    }

    /**
     * @return the jaction
     */
    public String getJaction() {
        return jaction;
    }

    /**
     * @param jaction the jaction to set
     */
    public void setJaction(String jaction) {
        this.jaction = jaction;
    }

    /**
     * @return the jsnapshot
     */
    public String getJsnapshot() {
        return jsnapshot;
    }

    /**
     * @param jsnapshot the jsnapshot to set
     */
    public void setJsnapshot(String jsnapshot) {
        this.jsnapshot = jsnapshot;
    }

    /**
     * @return the joperationcenter
     */
    public String getJoperationcenter() {
        return joperationcenter;
    }

    /**
     * @param joperationcenter the joperationcenter to set
     */
    public void setJoperationcenter(String joperationcenter) {
        this.joperationcenter = joperationcenter;
    }

    /**
     * @return the jsystem
     */
    public String getJsystem() {
        return jsystem;
    }

    /**
     * @param jsystem the jsystem to set
     */
    public void setJsystem(String jsystem) {
        this.jsystem = jsystem;
    }
}
