/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.main;

import eu.exus.esponder.biomeda.client.BiomedAHandler;
import java.io.IOException;
import uilib.exus.UILibEXUS;

/**
 *
 * @author tdim
 */
public class SensorsManager {

    public void RunMe(boolean uistatus) throws IOException, InterruptedException {
        BiomedAHandler pBiomedA = new BiomedAHandler();

        UILibEXUS pUI = new UILibEXUS();

        //if ui is on then open connection and start read thread
        if (uistatus) {
            pUI.OpenConnection();
            pUI.StartReadUIThread();
        }
        //open biomeda 
        pBiomedA.OpenConnection();
        //close streaming just in case didnt close correctly
        pBiomedA.StopStreaming();
        //wait for command execution
        Thread.sleep(500);
        //start streaming and read thread
        pBiomedA.StartStreaming();
        pBiomedA.StartReadingThread();
        
        //wait some time and then close all for restart
        Thread.sleep((1000 * 60) * 2);

        //if ui is enable close it
        if (uistatus) {
            pUI.StopReadUIThread();
            pUI.CloseConnection();
        }
        //close biomeda anyway
        pBiomedA.StopStreaming();
        pBiomedA.StopReadingThread();
        pBiomedA.CloseConnection();

    }
}
