/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;


/**
 *
 * @author afa
 */
 public class BiomedBPacket extends BiomedAPacket {

    public BiomedBPacket(boolean hasLargeFlags, int index) {
        super(hasLargeFlags, index);
    }

    /**
     * parse the buffer starting from the index setted in the constructor
     * @param buffer
     * @return 
     */
    @Override
    public boolean process(byte buffer[]) {
        boolean hasLargeSize = false;
        int channel = 0;

        if ((this.index + 3) >= buffer.length) {
            return false;   /*Remaining buffer sizeOfPayload is too short for a minimal packet!*/
        }
        flags = buffer[this.index++] & 0xff;
        if (hasLargeFlags) {
            this.flags = (this.flags << 8 | buffer[this.index++]) & 0xffff;
        }
        hasLargeSize = ((this.flags & 1 << 7) != 0);
        this.sizeOfPayload = buffer[this.index++] & 0xff;
        if (hasLargeSize) {
            this.sizeOfPayload = ((this.sizeOfPayload << 8) | buffer[this.index++]) & 0xffff;
        }
        channel = (short) (buffer[this.index++] & 0xff);
        this.payloadStartIndex = this.index;
        switch (this.getType(channel)) {
            case COMMAND_RESPONSE:
                //tomd
//                this.payload = new BiomedACommand(buffer, this.payloadStartIndex, this.sizeOfPayload);
                break;
           /* case SAMPLES_STRUCTURE:
                SigmaDictionaryEntry entry = new SigmaDictionaryEntry(this.sizeOfPayload, buffer);
                entry.number = channel;
                entry.parse(this.payloadStartIndex);
                SigmaUtils.dictionary.entryList.add(entry);
                break;*/
            default:
                this.payload = new BiomedBData(this.flags, channel, buffer, this.payloadStartIndex, this.sizeOfPayload);
                break;
        }
        return true;
    }
}
