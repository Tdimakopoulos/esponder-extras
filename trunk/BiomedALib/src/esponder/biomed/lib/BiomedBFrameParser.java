/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

import java.util.LinkedList;
import java.util.List;
/**
 *
 * @author afa
 */
public class BiomedBFrameParser extends BiomedAFrameParser{
        
    @Override
    public void setFrame(byte buffer[]){
        this.frame = new BiomedBFrame(buffer);
    }
    
    /**
     * Parse the frame and extract command responses and data payloads
     * The commands responoses and the payloads extracted are accessible then via the getters
     */
    @Override
    public void process() {
        List<BiomedACommand> cmdList = new LinkedList<BiomedACommand>();
        List<RegularDataBlock> dataList = new LinkedList<RegularDataBlock>();
        Payload payload = null;
        BiomedAPacket packets[] = null;

        if (this.frame.isValid()) {
            this.frame.process();
            packets =  this.frame.getPackets();
            if (packets != null) {
                for (int i = 0; i < packets.length; i++) {
                    payload = packets[i].getPayload();
                    if (payload instanceof BiomedACommand) {
                        cmdList.add((BiomedACommand) payload);
                    }
                    if (payload instanceof BiomedBData) {
                        ((BiomedBData) payload).parseHeader();
                        RegularDataBlock rdbs[] = ((BiomedBData) payload).getDataBlocks();
                        if(rdbs != null){
                            for(int rdbIdx = 0; rdbIdx < rdbs.length; rdbIdx++){
                                dataList.add(rdbs[rdbIdx]);
                            }
                        }
                    }
                }
            }
        }else
        {
            System.out.println("*************************** Invalid frame");
        }
        if (cmdList != null) {            
            this.cmds = cmdList.toArray(new BiomedACommand[0]);
        }
        if (dataList != null) {            
            this.rdbs = dataList.toArray(new RegularDataBlock[0]);
        }
    }
}
