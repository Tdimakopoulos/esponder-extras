/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

/**
 *
 * @author afa
 */
 class BiomedBDictionaryBlock {    
    public static final int FROM_PREVIOUS_SAMPLE = 0;
    public static final int FROM_PACKET_HEADER = 1;
        
    public int channel;
    public int sampleCount;    
    public int sampleSize;
    public boolean signed;
    public int timeStampIncrementation;    
    public int intervalMultiplier;
    public int timeStampMultiplier;        
    public int interval;
    public int timeStampOffset;
}
