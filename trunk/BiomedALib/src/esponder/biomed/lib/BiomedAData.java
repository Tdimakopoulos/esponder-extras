/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

/**
 *
 * @author afa
 */
 class BiomedAData extends Payload {

    protected int channel;
    protected int flags;
    protected int timeStamp = 0;
    protected int index = 0;
    protected boolean isunsigned = false;
    protected int sampleSize = 0;
    protected int numberOfSamples = 0;
    protected int sampleInterval = 0;

    /**
     * Constructor
     * @param flags the flags of the packet contains some information about parsing the payload
     * @param channel the channel number specifying the signal
     * @param stream the byte stream
     * @param payloadStartIndex the start index of the payload in the packet stream
     * @param sizeOfPayload the size of payload
     */
    public BiomedAData(int flags, int channel, byte stream[], int payloadStartIndex, int sizeOfPayload) {        
        this.flags = flags;
        this.channel = channel;
        this.size = sizeOfPayload;
        this.buffer = new byte[sizeOfPayload];
        System.arraycopy(stream, payloadStartIndex, this.buffer, 0, sizeOfPayload);
        this.index = 0;
        this.numberOfSamples = 0;
    }

    /**
     * Parse the header of the Payload and assign values to the attributes
     * @return 
     */
    public boolean parseHeader() {
        int timestampType = (flags >> 5) & (1 << 1 | 1 << 0);
        int intervalType = (flags >> 3) & (1 << 1 | 1 << 0);

        this.isunsigned = (flags & (1 << 0)) != 0;
        this.sampleSize = (flags >> 1) & (1 << 1 | 1 << 0);
        switch (timestampType) {
            case 0://no time stamp
                this.timeStamp = 0;
                break;
            case 1://32 absolute time stamp
                this.timeStamp = (this.buffer[this.index++] & 0xff) << (3 * 8);
                this.timeStamp |= (this.buffer[this.index++] & 0xff) << (2 * 8);
                this.timeStamp |= (this.buffer[this.index++] & 0xff) << (1 * 8);
                this.timeStamp |= (this.buffer[this.index++] & 0xff) << (0 * 8);
                break;
            case 2://relative 8 bits time stamp
                this.timeStamp = this.buffer[this.index++];
                break;
            default:
                break;
        }
        switch (intervalType) {
            case 0://no interval
                this.sampleInterval = 0;
                break;
            case 1://interval 8 bits
                this.sampleInterval = this.buffer[this.index++] & 0xff;
                break;
            default:
                break;
        }
        //get the number of the samples in the payload
        //index point exactly on the first sample
        float numberOfBytesPerSample = 1.0f + 0.5f*(float)this.sampleSize;
        this.numberOfSamples = (int)((float)(this.size - this.index)/numberOfBytesPerSample);      
        return true;
    }

    /**
     * Get the subsquent value in the stream. This value can either be 8, 12 or 16 bits.
     * The flags attribute will make the decision about how to parse the value
     * @return 
     */
    protected int getValue(int sample) {
        int value = 0;
        
        switch (this.sampleSize) {
            case 0: /*8bits*/
                value = this.buffer[this.index++];
                if (this.isunsigned) {
                    value &= 0xff;
                }
                break;
            case 1: /*12bits*/
                if ((sample % 2) == 0) {
                    value = (((int)buffer[this.index + 0]) & 0xff) | ((((int)buffer[this.index + 1]) & 0x0f) << 8);
                    ++this.index;//increment only for the first byte, the second byte will be used again
                } else {
                    value = ((((int)buffer[this.index + 0]) & 0xf0) << 4) | (((int)buffer[this.index + 1]) & 0xff);
                    this.index += 2;//increment with 2 after 
                }
                if (!this.isunsigned) {
                    value = ((value & (1 << 11)) != 0) ? value - 4096 : value;
                }
             break;
            case 2: /*16bits*/
                //LSB is stored before
                value = ((((int)buffer[this.index + 1])) << 8);
                value += (((int)buffer[this.index + 0]) & 0xff);
                this.index += 2;
                if (this.isunsigned) {
                    value &= 0xffff;
                }
                break;
        }
        this.timeStamp += this.sampleInterval;
        return value;
    }
    
    /**
     * Encapsulate the information and the data in a RegularDataBlock instance
     * 
     * @return 
     */
    public RegularDataBlock getDataBlock(){
        float values[] = null;
        
        String name = BiomedUtils.getName(this.channel);
        
        if(name!=null){}else{
            name ="C"+String.valueOf(this.channel);}
        
        RegularDataBlock rdb = new RegularDataBlock(name, this.channel, this.numberOfSamples);        
        values = new float[this.numberOfSamples];
        
        for(int i = 0; i < this.numberOfSamples; i++){
            values[i] = (float)this.getValue(i);
        }
        rdb.setStartTimeInMillis((long)this.timeStamp);
        rdb.setValues(values);
        return rdb;
    }
}
