package esponder.biomed.lib;
/**
 * This base class is rough representation of signals samples. To be consistent, timing should be added to samples.
 * @author afa
 */
public class DataBlock {
    protected String name;
    protected int id;
    protected float values[];

    /**
     * Constructor
     * @param name signal name
     * @param id signal id
     */
    public DataBlock(String name, int id){
        this.name = name;
        this.id = id;
    }

    /**
     * Constructor
     * @param name signal name
     * @param id signal id
     * @param values samples values array
     */
    public DataBlock(String name, int id, float values[]){
        this.name = name;
        this.id = id;
        this.values = values;
    }

    /**
     * Copies an array of data with regular length at the specified index.<br>
     * It is used to merge small data array with the same size in the values attribute.<br>
     * The values attribute should be allocated with the sum of the sizes.
     * @param data array of data 
     * @param index the index of the array
     */
    public void copyData(float data[], int index){
        System.arraycopy(data, 0, this.values, index*data.length, data.length); 
    }
    
    /**
     * add a sample at the given position
     * @param value the value of the sample
     * @param index the position of the sample in the array
     */
    public void addSample(float value, int index){
        
        this.values[index] = value;
    }
    
    /**
     * Gets the name of the signal which the samples are contained in this data block
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the signal which the samples will be contained in this data block
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the id of the signals which the samples are contained in this data block
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the id of the signals which the samples will be contained in this data block
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the array of the signal samples contained in this data block
     * @return the values
     */
    public float[] getValues() {
        return values;
    }

    /**
     * Sets the values array
     * @param values the values to set 
     */
    public void setValues(float values[]) {
        this.values = values;
    }

}
