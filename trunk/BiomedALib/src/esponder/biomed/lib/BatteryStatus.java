/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

/**
 * This class encapsulates the status of the battery of the device
 * @author afa
 */
public class BatteryStatus {
    /**
     * The current voltage of the battery in millivolts
     */
    public int volageInMV;
    /**
     * The status of the battery if is charging or not
     */
    public boolean isCharging;
    /**
     * The remaining percentage of the battery
     */
    public int percentage;
}
