/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.esponder.ui.client;

import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

/**
 *
 * Name : null channel 163
 *
 * "activity/MET"
 *
 * > Name : null channel 164
 *
 * "actvity/instMET"
 *
 * > Name : null channel 154
 *
 * "ecg/RespSynch" (I don't think that this one is relevant for you!)
 *
 * > Name : null channel 149
 *
 * "activity/period"
 *
 * @author tdim
 */
public class BiomedAHandler {

    StreamConnection sc = null;
    BiomedACommandSender command = null;
    BiomedAReadThread rt = new BiomedAReadThread();

    public void OpenConnection() throws IOException {
        sc = (StreamConnection) Connector.open("btspp://000BCE075BBA:1;authenticate=false;encrypt=false;master=false");
        command = new BiomedACommandSender();
        command.OpenStream(sc);
    }

    public void CloseConnection() throws IOException {
        command.CloseStream();
        sc.close();
    }

    public void StartStreaming() throws IOException {
        command.StartStreaming();
    }

    public void StopStreaming() throws IOException {
        command.StopStreaming();
    }

    public void StartReadingThread() {
        rt.SetBTConnection(sc);
        rt.start();
    }

    public void StopReadingThread() {
        rt.StopReadThread();
    }

//    public static void main(String[] args) throws IOException, InterruptedException {
//        StreamConnection sc = (StreamConnection) Connector.open("btspp://000BCE075BBA:1;authenticate=false;encrypt=false;master=false");
//        BiomedACommandSender command = new BiomedACommandSender();
//        command.OpenStream(sc);
//        command.StopStreaming();
//        command.StartStreaming();
//
//        Thread.sleep(200);
//        BiomedAReadThread rt = new BiomedAReadThread();
//        rt.SetBTConnection(sc);
//        rt.start();
//        System.out.println("Get Values for 1 Minute");
//        Thread.sleep(60000);
//        System.out.println("Streaming Finished");
//        rt.StopReadThread();
//        System.out.println("Close Stream and free resources");
//
//        command.StopStreaming();
//        command.CloseStream();
//        sc.close();
//    }
}
