/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.exus.esponder.ui.declarations;

import java.util.UUID;

/**
 *
 * @author tom
 */
public class UIDeclarations {
    private static UUID serialuuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private String DEVICE_PREFIX = "E-Sponder UI";
    private String TAG = "UIDevice";

//    public static void main(String[] args)
//    {
//        System.out.println("-->"+serialuuid.toString());
//    }
    /**
     * @return the serialuuid
     */
    public UUID getSerialuuid() {
        return serialuuid;
    }

    /**
     * @param serialuuid the serialuuid to set
     */
    public void setSerialuuid(UUID serialuuid) {
        this.serialuuid = serialuuid;
    }

    /**
     * @return the DEVICE_PREFIX
     */
    public String getDEVICE_PREFIX() {
        return DEVICE_PREFIX;
    }

    /**
     * @param DEVICE_PREFIX the DEVICE_PREFIX to set
     */
    public void setDEVICE_PREFIX(String DEVICE_PREFIX) {
        this.DEVICE_PREFIX = DEVICE_PREFIX;
    }

    /**
     * @return the TAG
     */
    public String getTAG() {
        return TAG;
    }

    /**
     * @param TAG the TAG to set
     */
    public void setTAG(String TAG) {
        this.TAG = TAG;
    }
    
    
}
