/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.esponder.udp.client;

import java.io.*; 
import java.net.*;
import java.util.Date;
/**
 *
 * @author tdim
 */
public class udpclient {

    public void SendToServer(String sAddress,int iPort,String sHeader,String sType,float fValue) throws SocketException, UnknownHostException, IOException {
        
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName(sAddress);
        byte[] sendData = new byte[10000];
        String sentence = sHeader+","+sType+","+String.valueOf(fValue);
        sendData = sentence.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, iPort);
        clientSocket.send(sendPacket);
        clientSocket.close();
        
    }
    
    
    public void SendToServerAMob(String sAddress,int iPort,String sHeader,String sType,float fValue) throws SocketException, UnknownHostException, IOException {
        Boolean bsend=false;
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName(sAddress);
        byte[] sendData = new byte[10000];
//        String sType="";
//        System.out.println("******** ");
        if(sType.equalsIgnoreCase("pace"))
            sType="0";
        
        if(sType.equalsIgnoreCase("br"))
            sType="2";
        
        if(sType.equalsIgnoreCase("hr"))
            sType="3";
        
        if(sType.equalsIgnoreCase("C125"))
            sType="4";
        
        if(sType.equalsIgnoreCase("0"))
            bsend=true;
            if(sType.equalsIgnoreCase("2"))
                bsend=true;
                if(sType.equalsIgnoreCase("3"))
                    bsend=true;
                    if(sType.equalsIgnoreCase("4"))
                        bsend=true;
                    if(bsend){
            
        String sentence = sHeader+","+sType+","+String.valueOf(fValue)+"a";
        System.out.println("Sending to Mobile : "+sentence+" Date : "+new Date());
        sendData = sentence.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, iPort);
        clientSocket.send(sendPacket);
        clientSocket.close();
                    }
    }
    
    public void SendToServerA(String sAddress,int iPort,String sHeader,String sType,float fValue) throws SocketException, UnknownHostException, IOException {
        Boolean bsend=false;
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName(sAddress);
        byte[] sendData = new byte[10000];
//        String sType="";
//        System.out.println("******** ");
        if(sType.equalsIgnoreCase("pace"))
            sType="0";
        
        if(sType.equalsIgnoreCase("br"))
            sType="2";
        
        if(sType.equalsIgnoreCase("hr"))
            sType="3";
        
        if(sType.equalsIgnoreCase("C125"))
            sType="4";
        
        if(sType.equalsIgnoreCase("0"))
            bsend=true;
            if(sType.equalsIgnoreCase("2"))
                bsend=true;
                if(sType.equalsIgnoreCase("3"))
                    bsend=true;
                    if(sType.equalsIgnoreCase("4"))
                        bsend=true;
                    if(bsend){
            
        String sentence = "X"+sHeader+","+sType+","+String.valueOf(fValue)+"a";
        sendData = sentence.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, iPort);
        clientSocket.send(sendPacket);
        clientSocket.close();
                    }
    }
    
    
     public void SendToServer(String sAddress,int iPort,String sHeader,String sType,String sValue) throws SocketException, UnknownHostException, IOException {
        
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName(sAddress);
        byte[] sendData = new byte[10000];
        String sentence = sHeader+","+sType+","+sValue;
        sendData = sentence.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, iPort);
        clientSocket.send(sendPacket);
        clientSocket.close();
        
    }
}
