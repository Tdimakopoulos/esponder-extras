/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.esponder.biomeda.client;

import esponder.biomed.lib.BiomedACommand;
import esponder.biomed.lib.BiomedAFrameParser;
import esponder.biomed.lib.Cobs;
import esponder.biomed.lib.RegularDataBlock;
import exus.esponder.receive.processor.processor;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.io.StreamConnection;

/**
 *
 * @author tdim
 */
public class BiomedAReadThread extends Thread {

    StreamConnection scglobal;
    boolean brun = true;

    public void SetBTConnection(StreamConnection sc) {
        scglobal = sc;
    }

    public void StopReadThread() {
        brun = false;
    }

    byte[] combine(byte[] a, byte[] b) {
        if (a == null) {
            return b;
        } else {
            byte[] result = new byte[a.length + b.length];
            System.arraycopy(a, 0, result, 0, a.length);
            System.arraycopy(b, 0, result, a.length, b.length);
            return result;
        }
    }

    @Override
    public void run() {
        try {
            DataInputStream datain = null;
            BiomedAFrameParser parser = new BiomedAFrameParser();
            BiomedACommand cmds[] = null;
            RegularDataBlock dataBlocks[] = null;
            processor pProcessor = new processor();
            datain = scglobal.openDataInputStream();
            int iloop = 0;
            int il = 0;
            int ill = 0;

            byte b1[] = new byte[1048];
            byte b3[] = null;
            byte b2[] = null;
            while (brun) {
                try {

                    il = datain.read(b1);
                    if(il>=0)
                    {
                    b3 = new byte[il];

                    System.arraycopy(b1, 0, b3, 0, il);
                    b2 = combine(b2, b3);

                    iloop++;
                    if (il == 1) {
                        iloop = 0;
                        try {
                            byte incoming[] = Cobs.decode(b2, b2.length);
                            if (incoming != null) {
                                parser.setFrame(incoming);
                                parser.process();
                                cmds = parser.getCmds();
                                dataBlocks = parser.getDataBlocks();
                                pProcessor.ProcessBiomedACommand(dataBlocks);

//                        for (int idb = 0; idb < dataBlocks.length; idb++) {
//                            int iflv = dataBlocks[idb].getValues().length;
//                            for (int iif = 0; iif < iflv; iif++) {
//                                System.out.println("*** Name :" + dataBlocks[idb].getName() + "  Values : " + dataBlocks[idb].getValues()[iif]);
//                            }
//                        }
//                        Function in comments show break down of the byffer input                          
//                        ProcessInput(b2);

                            }
                        } catch (Exception e) {
                        }
                        b1 = new byte[1048];

                        b3 = new byte[1048];
                        b2 = new byte[1];
                    }
                }
                } catch (IOException ex) {
                    Logger.getLogger(BiomedAReadThread.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            System.out.println("Closing down BiomedA");
            datain.close();
        } catch (IOException ex) {
            Logger.getLogger(BiomedAReadThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Closing down");
    }
//    public void ProcessInput(byte b[]) {
//        BiomedACommand pin = new BiomedACommand(b, 0, b.length);
//
//
//        // System.out.println("Class : " + pin.getClass());
//        //System.out.println("Command Class : " + pin.getCommandClass());
////        System.out.println("Command Op Code : " + pin.getCommandOpcode());
//        //System.out.println("Index : " + pin.getIndex());
//        //System.out.println(pin.getParameter());
//        //System.out.println("Sequence : " + pin.getSequenceNumber());
//        //System.out.println("Size : " + pin.getSize());
//        //System.out.println("Type : " + pin.getType());
//        //pin.getParameterValue(parameterCode);
//
//    }
}
