/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.esponder.biomeda.client;

import javax.microedition.io.StreamConnection;
import esponder.biomed.lib.BiomedACommand;
import esponder.biomed.lib.BiomedACommandsDefinition;
import esponder.biomed.lib.FrameMaker;
import eu.exus.esponder.bluetooth.service.manager.ServicesSearch;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

/**
 *
 * @author tdim
 */
public class BiomedACommandSender {

    DataOutputStream dataout = null;

    public void StartStreaming() throws IOException {
        BiomedACommand cmd = new BiomedACommand(BiomedACommandsDefinition.Classes.SYSTEM, BiomedACommandsDefinition.OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.SYSTEM.OPCODE.STATE);
        cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.STREAM, true, 1);
        cmd.insertSequenceNumber(1);

        FrameMaker pp = new FrameMaker();
        byte outgoing[] = pp.getFrame(cmd, 2);



        dataout.write(outgoing);
    }

    public void OpenStream(StreamConnection sc) throws IOException {
        dataout = sc.openDataOutputStream();
    }

    public void CloseStream() throws IOException {
        dataout.close();
    }

    public void Status() throws IOException {
        BiomedACommand cmd = new BiomedACommand(BiomedACommandsDefinition.Classes.SYSTEM, BiomedACommandsDefinition.OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.SYSTEM.OPCODE.STATE);
        FrameMaker pp = new FrameMaker();
        byte outgoing[] = pp.getFrame(cmd, 2);

        dataout.write(outgoing);
    }

    public void StopStreaming() throws IOException {
        BiomedACommand cmd = new BiomedACommand(BiomedACommandsDefinition.Classes.SYSTEM, BiomedACommandsDefinition.OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.SYSTEM.OPCODE.STATE);
        cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.STREAM, true, 0);
        cmd.insertSequenceNumber(1);

        FrameMaker pp = new FrameMaker();
        byte outgoing[] = pp.getFrame(cmd, 2);

        dataout.write(outgoing);

    }
}
