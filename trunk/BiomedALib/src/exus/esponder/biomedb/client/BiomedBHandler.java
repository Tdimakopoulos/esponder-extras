/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exus.esponder.biomedb.client;

import eu.exus.esponder.biomeda.client.BiomedACommandSender;
import java.io.IOException;

/**
 *
 * @author tdim
 */
public class BiomedBHandler {
    
    
    public static void main(String[] args) throws IOException, InterruptedException {
        BiomedBClient pClient=new BiomedBClient();
        pClient.OpenConnection();
        BiomedACommandSender pp = new BiomedACommandSender();
        pp.OpenStream(pClient.sc);
        pp.StartStreaming();

        
        BiomedBReadThread rt = new BiomedBReadThread();
        rt.SetBTConnection(pClient.sc);
        rt.start();
        Thread.sleep(1000*30);
        pp.StopStreaming();
        rt.StopReadThread();
        pp.CloseStream();
        pClient.CloseConnection();
    }
}
