/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exus.esponder.receive.processor;

import esponder.biomed.lib.RegularDataBlock;
import eu.exus.esponder.udp.client.udpclient;
import exus.esponder.filedatabase.handlers.PersistSensorsReadings;
import exus.esponder.settings.settingsmanager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tdim
 */
public class processor {

    byte[] binput = null;
    String szinput = "";

    public void ProcessUICommand(String frame) {
        settingsmanager pp = new settingsmanager();
        pp.ReadValues();
        try {
            //PersistSensorsReadings ppo = new PersistSensorsReadings();
            //ppo.SaveUI(frame);
            udpclient pcl = new udpclient();
            pcl.SendToServer(pp.getUiip(), Integer.valueOf(pp.getUiport()), pp.getSheader(), "UI ", frame);

            if (pp.getMobUIip().equalsIgnoreCase("0")) {
            } else {
                udpclient pclm = new udpclient();
                pclm.SendToServer(pp.getMobUIip(), Integer.valueOf(pp.getMobUIport()), pp.getSheader(), "UI ", frame);
            }

            System.out.println("** UI Receive : " + frame);
        } catch (FileNotFoundException ex) {
            System.out.println("File Save Error" + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("File Save Error" + ex.getMessage());
        }
        //catch (ClassNotFoundException ex) {
        //  System.out.println("File Save Error" + ex.getMessage());
        //}
    }

    public void ProcessBiomedACommand(RegularDataBlock dataBlocks[]) {
        PersistSensorsReadings ppo = new PersistSensorsReadings();
        settingsmanager pp = new settingsmanager();
        pp.ReadValues();
        for (int idb = 0; idb < dataBlocks.length; idb++) {
            int iflv = dataBlocks[idb].getValues().length;
            for (int iif = 0; iif < iflv; iif++) {
                
                if (dataBlocks[idb].getName().equalsIgnoreCase("ecg")) {
                    //try {
                    // ppo.SaveECG(dataBlocks[idb].getValues()[iif]);
                    //     udpclient pcl=new udpclient();
                    //       pcl.SendToServer(pp.getAip(), Integer.valueOf(pp.getAport()), pp.getSheader(), "ECG ", dataBlocks[idb].getValues()[iif]);
                    // } catch (FileNotFoundException ex) {
                    //   System.out.println("File Save Error"+ex.getMessage());
                    // } catch (IOException ex) {
                    //   System.out.println("File Save Error"+ex.getMessage());
                    // } 
                    //catch (ClassNotFoundException ex) {
                    // System.out.println("File Save Error"+ex.getMessage());
                    //}
                } else {
                    try {

                        //System.out.println("*** Name :" + dataBlocks[idb].getName() + "  Values : " + dataBlocks[idb].getValues()[iif]);
                        udpclient pcl = new udpclient();

                        pcl.SendToServerA(pp.getAip(), Integer.valueOf(pp.getAport()), pp.getSheader(), dataBlocks[idb].getName(), dataBlocks[idb].getValues()[iif]);

                        if (pp.getMobABip().equalsIgnoreCase("0")) {
                        } else {
                            udpclient pclM = new udpclient();
                            pclM.SendToServerAMob(pp.getMobABip(), Integer.valueOf(pp.getMobABport()), pp.getSheader(), dataBlocks[idb].getName(), dataBlocks[idb].getValues()[iif]);
                        //System.out.println("-------------------> Sending to mobile  :"+pp.getMobABip()+":"+pp.getMobABport());
                        
                        }

                    } catch (FileNotFoundException ex) {
                        System.out.println("File Save Error" + ex.getMessage());
                    } catch (IOException ex) {
                        System.out.println("File Save Error" + ex.getMessage());
                    }
                    //catch (ClassNotFoundException ex) {
                    //  System.out.println("File Save Error"+ex.getMessage());
                    //}
                }
//                System.out.println("*** Name :" + dataBlocks[idb].getName() + "  Values : " + dataBlocks[idb].getValues()[iif]);
            }
        }
    }
}
