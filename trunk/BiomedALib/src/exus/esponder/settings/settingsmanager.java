/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exus.esponder.settings;

import exus.esponder.filedatabase.model.Pathmanager;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tdim
 */
public class settingsmanager {
    
    private String bUI;
    private String bA;
    private String bB;
    private String sheader;
    private String uiip;
    private String uiport;
    private String aip;
    private String aport;
    private String bip;
    private String bport;
    
    private String MobABip;
    private String MobABport;
    
    private String MobUIip;
    private String MobUIport;
    
    public void ReadValues()
    {
        BufferedReader br = null;
        Pathmanager pp = new Pathmanager();
        try {
            br = new BufferedReader(new FileReader(pp.GetPath()+"settings.txt"));
            String line;
            int i=0;
            while ((line = br.readLine()) != null) {
                if(i==0)
                    bUI=line;
                if(i==1)
                    bA=line;
                if(i==2)
                    bB=line;
                if(i==3)
                    sheader=line;
                if(i==4)
                    uiip=line;
                if(i==5)
                    uiport=line;
                if(i==6)
                    aip=line;
                if(i==7)
                    aport=line;
                if(i==8)
                    bip=line;
                if(i==9)
                    bport=line;
                            if(i==10)
                MobABip=line;
                            if(i==11)
    MobABport=line;
    if(i==12)
    MobUIip=line;
    if(i==13)
    MobUIport=line;
    
               i++;
            }
            br.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(settingsmanager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(settingsmanager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(settingsmanager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * @return the bUI
     */
    public String getbUI() {
        return bUI;
    }

    /**
     * @param bUI the bUI to set
     */
    public void setbUI(String bUI) {
        this.bUI = bUI;
    }

    /**
     * @return the bA
     */
    public String getbA() {
        return bA;
    }

    /**
     * @param bA the bA to set
     */
    public void setbA(String bA) {
        this.bA = bA;
    }

    /**
     * @return the bB
     */
    public String getbB() {
        return bB;
    }

    /**
     * @param bB the bB to set
     */
    public void setbB(String bB) {
        this.bB = bB;
    }

    /**
     * @return the sheader
     */
    public String getSheader() {
        return sheader;
    }

    /**
     * @param sheader the sheader to set
     */
    public void setSheader(String sheader) {
        this.sheader = sheader;
    }

    /**
     * @return the uiip
     */
    public String getUiip() {
        return uiip;
    }

    /**
     * @param uiip the uiip to set
     */
    public void setUiip(String uiip) {
        this.uiip = uiip;
    }

    /**
     * @return the uiport
     */
    public String getUiport() {
        return uiport;
    }

    /**
     * @param uiport the uiport to set
     */
    public void setUiport(String uiport) {
        this.uiport = uiport;
    }

    /**
     * @return the aip
     */
    public String getAip() {
        return aip;
    }

    /**
     * @param aip the aip to set
     */
    public void setAip(String aip) {
        this.aip = aip;
    }

    /**
     * @return the aport
     */
    public String getAport() {
        return aport;
    }

    /**
     * @param aport the aport to set
     */
    public void setAport(String aport) {
        this.aport = aport;
    }

    /**
     * @return the bip
     */
    public String getBip() {
        return bip;
    }

    /**
     * @param bip the bip to set
     */
    public void setBip(String bip) {
        this.bip = bip;
    }

    /**
     * @return the bport
     */
    public String getBport() {
        return bport;
    }

    /**
     * @param bport the bport to set
     */
    public void setBport(String bport) {
        this.bport = bport;
    }

    /**
     * @return the MobABip
     */
    public String getMobABip() {
        return MobABip;
    }

    /**
     * @param MobABip the MobABip to set
     */
    public void setMobABip(String MobABip) {
        this.MobABip = MobABip;
    }

    /**
     * @return the MobABport
     */
    public String getMobABport() {
        return MobABport;
    }

    /**
     * @param MobABport the MobABport to set
     */
    public void setMobABport(String MobABport) {
        this.MobABport = MobABport;
    }

    /**
     * @return the MobUIip
     */
    public String getMobUIip() {
        return MobUIip;
    }

    /**
     * @param MobUIip the MobUIip to set
     */
    public void setMobUIip(String MobUIip) {
        this.MobUIip = MobUIip;
    }

    /**
     * @return the MobUIport
     */
    public String getMobUIport() {
        return MobUIport;
    }

    /**
     * @param MobUIport the MobUIport to set
     */
    public void setMobUIport(String MobUIport) {
        this.MobUIport = MobUIport;
    }
    
}
