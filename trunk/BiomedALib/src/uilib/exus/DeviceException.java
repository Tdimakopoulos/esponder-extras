package uilib.exus;

/**
 * This class of exception is made to handle errors of operations inside the hardware.<br>
 * Device exceptions are thrown when an operation fails or there are some problems in the operation scheduling, e.g. starting the streaming when the device is not connected
 * @author afa
 */
public class DeviceException extends Exception {

    /**
     * Code of a non authorized command exception
     */
    public static final int API_ERROR_NON_AUTORIZED_COMMAND = 3012;
    private String message = null;
    private int code = 0;

    /**
     * Constructor
     */
    public DeviceException() {
    }

    /**
     * Gets the message of the exception
     * @return the message
     */
    @Override
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message of the exception
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets the code of the exception
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * Sets the code of the exception
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }
}
