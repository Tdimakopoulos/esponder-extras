/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.pki.access;

import eu.esponder.role.ws.EsRoleService;
import eu.esponder.webservices.EsKeyManager;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "EsponderAccessService")
public class EsponderAccessService {

    @WebMethod(operationName = "GetRoleForKey")
    public String GetRoleForKey(@WebParam(name = "PKIKey") String PKIKey) {
        return getUserRole(PKIKey);
    }

    @WebMethod(operationName = "GetAuthorizationListForKey")
    public List<String> GetAuthorizationListForKey(@WebParam(name = "PKIKey") String PKIKey) {
        return QueryRoles(getUserRole(PKIKey));
    }

    private List<String> QueryRoles(String Role) {
        List<String> pRet = new ArrayList();
        List<eu.esponder.role.ws.RoleDB> pFind = esFindAll();
        for (int i = 0; i < pFind.size(); i++) {
            if (pFind.get(i).getRole().equalsIgnoreCase(Role)) {
                pRet.add(pFind.get(i).getAccessfunction());
            }
        }
        return pRet;
    }

    private String getUserRole(java.lang.String key) {
        EsKeyManager service = new EsKeyManager();
        eu.esponder.webservices.KeyManager port = service.getKeyManagerPort();
        return port.getUserRole(key);
    }

    private java.util.List<eu.esponder.role.ws.RoleDB> esFindAll() {
        EsRoleService service = new EsRoleService();
        eu.esponder.role.ws.RoleService port = service.getRoleServicePort();
        return port.esFindAll();
    }
}
