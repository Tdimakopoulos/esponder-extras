/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.session.bean;

import eu.esponder.session.db.sessiontable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class sessiontableFacade extends AbstractFacade<sessiontable> {
    @PersistenceContext(unitName = "EsponderPKIAccessServicesPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public sessiontableFacade() {
        super(sessiontable.class);
    }
    
}
