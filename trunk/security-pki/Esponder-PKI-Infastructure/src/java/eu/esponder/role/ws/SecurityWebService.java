/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.role.ws;

import eu.esponder.entity.KeyDB;
import eu.esponder.entity.RoleDB;
import eu.esponder.role.bean.RoleDBFacadeLocal;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import org.security.utils.Util;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "es_SecurityWebService")
public class SecurityWebService {
    //declare a persistence manager factory, we will use it
    //to get a entity manager

    @PersistenceUnit
    private EntityManagerFactory emf;
    //declare a transaction manager
    //transactions are used when we do add or edit
    @Resource
    private UserTransaction utx;
    @EJB
    private RoleDBFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    

    @WebMethod(operationName = "es_GetAllRoles")
    public List<RoleDB> findAll() {
        return ejbRef.findAll();
    }
    
    @WebMethod(operationName = "es_GetFunctionsOfRole")
     public List<RoleDB> QueryByUsername(@WebParam(name = "Role")String Username) {
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();
        return em.createQuery("select u from RoleDB u where u.role=:Uservar").setParameter("Uservar", Username).getResultList();
    }
    
    @WebMethod(operationName = "es_GetKeyOfUser")
     public List<RoleDB> QueryByUsernameForKey(@WebParam(name = "Role")String Username) {
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();
        return em.createQuery("select u from KeyDB u where u.Username=:Uservar").setParameter("Uservar", Username).getResultList();
    }
    @WebMethod(operationName = "es_GetUserRole")
    public String GetUserRole(@WebParam(name = "PublicKey") String key) {
        assert emf != null;  //Make sure injection went through correctly.
        String s = null;
        System.err.println("start");

        EntityManager em = null;
        try {
            em = emf.createEntityManager();

            String sQuery = "select u from KeyDB u";
            //byte[] keybytes=convertStringToByteArray(key);

            List Keys = em.createQuery(sQuery).getResultList();
            System.err.println(Keys.size());
            for (int i = 0; i < Keys.size(); i++) {
                KeyDB p = (KeyDB) Keys.get(i);
                //String s1=convertByteArrayToString(p.getPublicKey());
                Util pu = new Util();
                String s1 = pu.byteArray2Hex(p.getPublicKey());
                System.err.println(s1);
                System.err.println(key);

                if (key.equalsIgnoreCase(s1)) {
                    s = p.getRole();
                }
            }


        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        } finally {
            //close the em to release any resources held up by the persistebce provider
            em.close();
            return s;
        }
    }
}
