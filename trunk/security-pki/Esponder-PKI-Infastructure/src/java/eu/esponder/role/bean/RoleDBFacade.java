/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.role.bean;

import eu.esponder.entity.RoleDB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class RoleDBFacade extends AbstractFacade<RoleDB> implements RoleDBFacadeLocal {
    @PersistenceContext(unitName = "Esponder-PKI-InfastructurePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RoleDBFacade() {
        super(RoleDB.class);
    }
    
}
