package eu.esponder.webservices;

import eu.esponder.key.KeyGeneratornephron;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import javax.jws.WebService;
import javax.persistence.PersistenceUnit;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import eu.esponder.entity.KeyDB;
import java.security.SecureRandom;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.annotation.Resource;
import javax.jws.WebParam;
import org.certificate.create.CreateCertificate;
import org.certificate.export.exportcertificate;
import org.security.utils.Util;

@WebService(serviceName = "es_KeyManager")
public class KeyManager {

    //declare a persistence manager factory, we will use it
    //to get a entity manager
    @PersistenceUnit
    private EntityManagerFactory emf;
    //declare a transaction manager
    //transactions are used when we do add or edit
    @Resource
    private UserTransaction utx;

    /**
     * Convert a String to Byte Array
     */
    private byte[] convertStringToByteArray(String string) {
        byte[] byteArray = new byte[string.length()];

        for (int i = 0; i < string.length(); i++) {
            byteArray[i] = (byte) (string.charAt(i));
        }

        return byteArray;
    }

    private static String convertByteArrayToString(byte byteArray[]) {
        char[] charArray = new char[byteArray.length];

        for (int i = 0; i < byteArray.length; i++) {
            charArray[i] = (char) (byteArray[i]);
        }

        return new String(charArray);
    }
    /////////////////////////////////////////////////////////////////////////////

    public String GetUserRole(@WebParam(name = "key") String key) {
        assert emf != null;  //Make sure injection went through correctly.
        String s = "NO_USER_FOUND";
        System.err.println("Key : " + key);


        EntityManager em = null;
        try {
            em = emf.createEntityManager();

            String sQuery = "select u from KeyDB u";
            //byte[] keybytes=convertStringToByteArray(key);

            List Keys = em.createQuery(sQuery).getResultList();
            System.err.println("Size : "+Keys.size());
            for (int i = 0; i < Keys.size(); i++) {
                KeyDB p = (KeyDB) Keys.get(i);
                //String s1=convertByteArrayToString(p.getPublicKey());
                Util pu = new Util();
                String s1 = pu.byteArray2Hex(p.getPublicKey());
                System.err.println(p.getRole());

                System.err.println(key);
                System.err.println(s1);

                if (key.equalsIgnoreCase(s1)) {
                    s = p.getRole();
                }
            }


        } catch (Exception ex) {
            System.err.println("exeption : " + ex.getMessage());
            return null;
        } finally {
            //close the em to release any resources held up by the persistebce provider
            em.close();
            return s;
        }
    }
    ///////////////////////////////////////////////////////////////////////////////

    public void GenerateCertificates() throws Exception {
        CreateCertificate pexport = new CreateCertificate();
        pexport.Generate("CN", "OU", "O", "L", "S", "C", 30);
        pexport.export();
    }

    public String GenerateSignature() {
        String szReturn;
        byte[] seed = "nephron".getBytes();
        SecureRandom DRandom = new SecureRandom(seed);
        byte[] dd = DRandom.generateSeed(128);
        szReturn = dd.toString();
        return szReturn;
    }

    public String GenerateUserKeyPair(String Username, String Organization, String Role, Long iuserid) {
        EntityManager em = null;
        KeyGeneratornephron KG;
        KG = new KeyGeneratornephron();
        em = emf.createEntityManager();

        KeyDB pDatabase;
        pDatabase = new KeyDB();
        pDatabase.setOrganization(Organization);
        pDatabase.setRole(Role);
        pDatabase.setUsername(Username);
        pDatabase.setPuserID(iuserid);
        
        try {
            KG.generateKeys();
        } catch (Exception ex) {
            return "Error on Key Generation";
        }
        //System.err.println(KG.GetPrivateKey());
        pDatabase.setPrivateKey(KG.GetPrivateKey());
        pDatabase.setPublicKey(KG.GetPublicKey());
        pDatabase.setSecretKey(KG.getSzSecretKey());
        try {
            utx.begin();
            em.persist(pDatabase);
            utx.commit();
            return "No Errors";
        } catch (Exception e) {

            return "Errors on Persist";
        }
    }

    public List<KeyDB> QueryByUsername(String Username) {
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();
        return em.createQuery("select u from KeyDB u where u.Username=:Uservar").setParameter("Uservar", Username).getResultList();
    }

    public List<KeyDB> QueryAllUsers() {
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();
        return em.createQuery("select u from KeyDB u ").getResultList();
    }

    public byte[] GetUserPublicKey(String Username, String Organization, String Role) {
        assert emf != null;  //Make sure injection went through correctly.
        byte[] s = null;


        EntityManager em = null;
        try {
            em = emf.createEntityManager();

            String sQuery = "select u from KeyDB u where u.Username = :var2 and u.Organization = :Var1 and u.Role = :Var3 ";
            List Keys = em.createQuery(sQuery).setParameter("var2", Username).setParameter("Var1", Organization).setParameter("Var3", Role).getResultList();

            KeyDB p = (KeyDB) Keys.get(0);

            s = p.getPublicKey();

        } catch (Exception ex) {

            return null;
        } finally {
            //close the em to release any resources held up by the persistebce provider
            em.close();
            return s;
        }
    }

    public String GetUserPublicKeyString(String Username, String Organization, String Role) {
        assert emf != null;  //Make sure injection went through correctly.
        byte[] s = null;


        EntityManager em = null;
        try {
            em = emf.createEntityManager();

            String sQuery = "select u from KeyDB u where u.Username = :var2 and u.Organization = :Var1 and u.Role = :Var3 ";
            List Keys = em.createQuery(sQuery).setParameter("var2", Username).setParameter("Var1", Organization).setParameter("Var3", Role).getResultList();

            KeyDB p = (KeyDB) Keys.get(0);

            s = p.getPublicKey();

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        } finally {
            //close the em to release any resources held up by the persistebce provider
            em.close();
            Util pu = new Util();
            return pu.byteArray2Hex(s);
        }
    }

    public byte[] GetUserPrivateKey(String Username, String Organization, String Role) {
        assert emf != null;  //Make sure injection went through correctly.
        byte[] s = null;


        EntityManager em = null;
        try {
            em = emf.createEntityManager();

            String sQuery = "select u from KeyDB u where u.Username = :var2 and u.Organization = :Var1 and u.Role = :Var3";
            List Keys = em.createQuery(sQuery).setParameter("var2", Username).setParameter("Var1", Organization).setParameter("Var3", Role).getResultList();

            KeyDB p = (KeyDB) Keys.get(0);

            s = p.getPrivateKey();

        } catch (Exception ex) {

            return null;
        } finally {
            //close the em to release any resources held up by the persistebce provider
            em.close();
            return s;
        }
    }

    public byte[] GetUserSecretKey(String Username, String Organization, String Role) {
        assert emf != null;  //Make sure injection went through correctly.
        byte[] s = null;


        EntityManager em = null;
        try {
            em = emf.createEntityManager();

            String sQuery = "select u from KeyDB u where u.Username = :var2 and u.Organization = :Var1 and u.Role = :Var3";
            List Keys = em.createQuery(sQuery).setParameter("var2", Username).setParameter("Var1", Organization).setParameter("Var3", Role).getResultList();

            KeyDB p = (KeyDB) Keys.get(0);

            s = p.getSecretKey();

        } catch (Exception ex) {

            return null;
        } finally {
            //close the em to release any resources held up by the persistebce provider
            em.close();
            return s;
        }
    }
}
