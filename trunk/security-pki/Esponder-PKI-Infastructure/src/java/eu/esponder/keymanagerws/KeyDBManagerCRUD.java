/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.keymanagerws;

import eu.esponder.entity.KeyDB;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "KeyDBManagerCRUD")
public class KeyDBManagerCRUD {
   @PersistenceContext(unitName = "Esponder-PKI-InfastructurePU")
    private EntityManager em;

    
    protected EntityManager getEntityManager() {
        return em;
    }

    
    public void ecreate(KeyDB entity) {
        getEntityManager().persist(entity);
    }

    public void eedit(KeyDB entity) {
        getEntityManager().merge(entity);
    }

    public void eremove(KeyDB entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public KeyDB efind(Object id) {
        return getEntityManager().find(KeyDB.class, id);
        
    }

    public List<KeyDB> efindAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(KeyDB.class));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<KeyDB> efindRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(KeyDB.class));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int ecount() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<KeyDB> rt;
        rt = cq.from(KeyDB.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "keyDB") KeyDB keyDB) {
        ecreate(keyDB);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "keyDB") KeyDB keyDB) {
        eedit(keyDB);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "keyDB") KeyDB keyDB) {
        eremove(keyDB);
    }

    @WebMethod(operationName = "find")
    public KeyDB find(@WebParam(name = "id") Object id) {
        return efind(id);
    }

    @WebMethod(operationName = "findAll")
    public List<KeyDB> findAll() {
        return efindAll();
    }

    @WebMethod(operationName = "findRange")
    public List<KeyDB> findRange(@WebParam(name = "range") int[] range) {
        return efindRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ecount();
    }
    
}
