/*
 * KeyGenerator.java
 *
 * Created on September 12, 2007, 2:59 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package eu.esponder.key;


import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Enumeration;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;

import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.SecretKeySpec;
/**
 *
 * @author tdi
 */
public class KeyGeneratornephron {
     /** Static Variables */
    private static final String KEY_ALGORITHM = "RSA";
    private static final String KEY_ALGO_TRANSFORMATION = "RSA/ECB/OAEP";	//algorithm, mode, padding
    private static final String KEY_ALGO_TRANSFORMATION2 = "RSA/ECB/PKCS1Padding";
    private static final String ALGORITHM_AES = "AES";
    private static final String ALGORITHM_3DES = "DESede"; 
    private static byte[] szPublicKey;
    private static byte[] szPrivateKey;
    private static SecretKey secretKey;
    private static byte[] szSecretKey;

    public byte[] getSzSecretKey() {
        return szSecretKey;
    }

  
    /** Creates a new instance of KeyGenerator */
    public KeyGeneratornephron() {
        
    }
    
     /** Convert a PublicKey to Byte Array*/
    private static byte[] publicKeyToBytes(PublicKey publicKey) throws Exception {
            try {
                    KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
                    EncodedKeySpec publicKeySpec = (EncodedKeySpec)keyFactory.getKeySpec(publicKey, X509EncodedKeySpec.class);
                    return publicKeySpec.getEncoded();
            } catch (Exception e) {
                    System.out.println("Failed to convert opaque key (PublicKey) into transparent key specification (X509)");
                    return null;
            }
    }

    /** Convert a PrivateKey to Byte Array*/
    private static byte[] privateKeyToBytes(PrivateKey privateKey) throws Exception {
            try {
                    KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
                    EncodedKeySpec privateKeySpec = (EncodedKeySpec)keyFactory.getKeySpec(privateKey, PKCS8EncodedKeySpec.class);
                    return privateKeySpec.getEncoded();
            } catch (Exception e) {
                    System.err.println("Failed to convert opaque key (PrivateKey) into transparent key specification (PKCS8)");
                    return null;
            }
    }

    /** Convert a Byte Array to String */
    private static String convertByteArrayToString(byte byteArray[]) {
            char[] charArray = new char[byteArray.length];

            for (int i = 0; i < byteArray.length; i++) {
                    charArray[i] = (char)(byteArray[i]);
            }

            return new String(charArray);
    }

    /** Convert a String to Byte Array*/
    private byte[] convertStringToByteArray(String string) {
            byte[] byteArray = new byte[string.length()];

            for (int i = 0; i < string.length(); i++) {
                    byteArray[i] = (byte)(string.charAt(i));
            }

            return byteArray;
    }
        
    /** Methods */
    public void generateKeys () throws Exception {
            PublicKey publicKey;
            PrivateKey privateKey;
            KeyPairGenerator rsaGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
            SecureRandom random = new SecureRandom();
            rsaGen.initialize(2048, random);
            KeyPair pair = rsaGen.generateKeyPair();
            publicKey = pair.getPublic();
            privateKey = pair.getPrivate() ;
            szPublicKey=publicKeyToBytes(publicKey);
            szPrivateKey=privateKeyToBytes(privateKey);
            szSecretKey=secretKeyToBytes(generateSymmetricKey(ALGORITHM_3DES));
    }
    /** Convert a SecretKey to Byte Array*/
	private static byte[] secretKeyToBytes(SecretKey secretKey) throws Exception {
		try {
			SecretKeyFactory keyfactory = SecretKeyFactory.getInstance(ALGORITHM_3DES);
			DESedeKeySpec keyspec = (DESedeKeySpec) keyfactory.getKeySpec(secretKey, DESedeKeySpec.class);
			byte[] rawkey = keyspec.getKey();
			return rawkey;
		} catch (Exception e) {
			System.err.println("Failed to convert SecretKey into key specification DESedeKeySpec");
			return null;
		}
	}
    private static SecretKey generateSymmetricKey(String algorithm) throws NoSuchAlgorithmException {
		byte[] seed = "nephron".getBytes();
		try {
			KeyGenerator keyGen = KeyGenerator.getInstance(algorithm);
			if (algorithm.equals(ALGORITHM_3DES))
				keyGen.init(168, new SecureRandom(seed));	//SecureRandom.getInstance("SHA1PRNG", "SUN");
			else if (algorithm.equals(ALGORITHM_AES))
				keyGen.init(256);
			
			secretKey = keyGen.generateKey();
		}catch (Exception e){
			System.err.println("Encryption: generateSymmetricKey(): <<Failed>> ");
			e.printStackTrace();
		}  
		return secretKey;	
	}
    
    public byte[] GetPublicKey()
    {
        return szPublicKey;
    }
    
    public byte[] GetPrivateKey()
    {
        return szPrivateKey;
    }
    
    
    
}
