package eu.esponder.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class KeyDB
 * 
 * @author tdi
 */
@Entity
@Table(name = "es_KeyPairDB")
public class KeyDB implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "USERNAME")
    private String Username;
    
    @Column(name = "ORGANIZATION")
    private String Organization;
    
    @Column(name = "ROLE")
    private String Role;
    
    private Long puserID;
    
    
    
    
    @Column(name = "PRIVATEKEY")
    private byte[] PrivateKey;
    
    @Column(name = "PUBLICKEY")
    private byte[] PublicKey;
    
    @Column(name = "SECRETKEY")
    private byte[] SecretKey;
    
    /** Creates a new instance of KeyDB */
    public KeyDB() {
    }

    /**
     * Gets the id of this KeyDB.
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id of this KeyDB to the specified value.
     * @param id the new id
     */
    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

	public String getOrganization() {
		return Organization;
	}

	public void setOrganization(String organization) {
		Organization = organization;
	}

	public String getRole() {
		return Role;
	}

	public void setRole(String role) {
		Role = role;
	}
	
   

    
    public byte[] getPrivateKey() {
        return PrivateKey;
    }

    public void setPrivateKey(byte[] PrivateKey) {
        this.PrivateKey = PrivateKey;
    }

    public byte[] getPublicKey() {
        return PublicKey;
    }

    public void setPublicKey(byte[] PublicKey) {
        this.PublicKey = PublicKey;
    }
    /**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this KeyDB.  The result is 
     * <code>true</code> if and only if the argument is not null and is a KeyDB object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KeyDB)) {
            return false;
        }
        KeyDB other = (KeyDB)object;
        if (this.getId() != other.getId() && (this.getId() == null || !this.getId().equals(other.getId()))) return false;
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return "eu.nephron.entity.KeyDB[id=" + getId() + "]";
    }

    public byte[] getSecretKey() {
        return SecretKey;
    }

    public void setSecretKey(byte[] SecretKey) {
        this.SecretKey = SecretKey;
    }

    /**
     * @return the puserID
     */
    public Long getPuserID() {
        return puserID;
    }

    /**
     * @param puserID the puserID to set
     */
    public void setPuserID(Long puserID) {
        this.puserID = puserID;
    }



    
    
}
