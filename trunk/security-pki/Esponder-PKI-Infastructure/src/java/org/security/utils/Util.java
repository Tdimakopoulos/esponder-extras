/**
 * Part of The Security Library
 * 
 * Convertor util
 * 
 * Functions
 * ---------------
 * 
 * Byte Array to Hex
 * 
 * @author tdi
 */
package org.security.utils;

public class Util {

    private static char[] hexChars = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };

    public static String byteArray2Hex(byte[] ba) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ba.length; i++) {
            int hbits = (ba[i] & 0x000000f0) >> 4;
            int lbits = ba[i] & 0x0000000f;

            sb.append("" + hexChars[hbits] + hexChars[lbits]);
        }
        return sb.toString();
    }
}