/**
 * Part of The Security Library
 * 
 * Manage Certificates and Keys
 * 
 * Functions
 * ---------------
 * Intialize : gets the dpath variable from the main caller
 * Export : Export the certificate based on the dpath variable
 * Generate : Generate the certificate based on the passed arguments
 * 
 * @author tdi
 */
package org.certificate.create;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import java.security.*;
import javax.security.auth.*;
import org.certificate.export.exportcertificate;
import org.key.util.keyutils;
import org.security.paths.securitypaths;

public class CreateCertificate {

    keyutils dKUtil = new keyutils();
    securitypaths dpaths = new securitypaths();
    java.security.cert.X509Certificate[] chain = new java.security.cert.X509Certificate[1];
    PrivateKey thisPrivateKey = null;
    PublicKey thisPublicKey = null;
    KeyStore keystore = null;

    
    public void Initialize(securitypaths dpathspas) {
        dpaths = dpathspas;
    }

    public void export() {
        try {
            exportcertificate ddex = new exportcertificate();
            ddex.export(chain, dpaths.getCerPath(), dpaths.getCerName());
        } catch (FileNotFoundException ex) {
        } catch (UnsupportedEncodingException ex) {
        } catch (IOException ex) {
        } catch (CertificateEncodingException ex) {
        }
    }

    public void Generate(String CN, String OU, String O, String L, String S, String C, int Days) throws Exception {
        // Generate the KeyPair, get the public and private keys, and assign a certificate
        sun.security.x509.CertAndKeyGen keypair = new sun.security.x509.CertAndKeyGen("RSA", "MD5WithRSA");

        //Generate the certificate names
        sun.security.x509.X500Name x500Name = new sun.security.x509.X500Name(
                CN, OU, O, L, S, C);

        //initialize the keypair
        keypair.generate(1024);

        //get the keys
        thisPrivateKey = keypair.getPrivateKey();
        thisPublicKey = keypair.getPublicKey();

        //get the certificates
        chain[0] = keypair.getSelfCertificate(x500Name, Days * 24 * 60 * 60);

    }
}
