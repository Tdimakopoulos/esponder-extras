/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.utilities;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

/**
 *
 * @author tdim
 */
public class FileMutex {

    private String keyvalue;
    FileLock lock;
    FileChannel channel;

    public FileMutex(String appName) {
        this.keyvalue = appName;
    }

    public boolean isAppActive() throws Exception {
        File file = new File(System.getProperty("user.home"), keyvalue + ".tmp");
        channel = new RandomAccessFile(file, "rw").getChannel();

        lock = channel.tryLock();
        if (lock == null) {
            lock.release();
            channel.close();
            return true;
        }
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    lock.release();
                    channel.close();
                } catch (Exception e) {
                    System.out.println("Error on Releasing Lock : " + e.getMessage());
                }
            }
        });
        return false;
    }

    public boolean ReleaseLock() {
        try {
            lock.release();
            channel.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error on Releasing Lock : " + e.getMessage());
            return false;
        }
    }
}
