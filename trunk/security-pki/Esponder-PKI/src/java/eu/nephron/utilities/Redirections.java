/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.utilities;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 *
 * @author tdim
 */
public class Redirections {

    public void RedirectErrOnlyToFile(String szFilename) throws FileNotFoundException {
        System.setErr(new PrintStream(new FileOutputStream(szFilename)));
    }

    public void RedirectOutOnlyToFile(String szFilename) throws FileNotFoundException {
        System.setOut(new PrintStream(new FileOutputStream(szFilename)));
    }

    public void RedirectErrTerminalAndFile(String szFilename) throws FileNotFoundException {
        System.setErr(new LogStream(System.err, new PrintStream(new FileOutputStream(szFilename))));
    }

    public void RedirectOutTerminalAndFile(String szFilename) throws FileNotFoundException {
        System.setOut(new LogStream(System.out, new PrintStream(new FileOutputStream(szFilename))));
    }

    class LogStream extends PrintStream {

        PrintStream out;

        public LogStream(PrintStream out1, PrintStream out2) {
            super(out1);
            this.out = out2;
        }

        public void write(byte buf[], int off, int len) {
            try {
                super.write(buf, off, len);
                out.write(buf, off, len);
            } catch (Exception e) {
            }
        }

        public void flush() {
            super.flush();
            out.flush();
        }
    }
}
