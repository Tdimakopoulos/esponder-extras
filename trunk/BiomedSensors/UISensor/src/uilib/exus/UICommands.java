/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uilib.exus;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.io.StreamConnection;

/**
 *
 * @author tom
 */
public class UICommands {
      DataOutputStream dataout=null;
      
       public void OpenStream(StreamConnection sc) throws IOException
    {
         dataout = sc.openDataOutputStream();
          try {
              Thread.sleep(100);
          } catch (InterruptedException ex) {
              Logger.getLogger(UICommands.class.getName()).log(Level.SEVERE, null, ex);
          }
    }
    
    public void CloseStream() throws IOException
    {
        dataout.close();
    }
    
    /**
     * Request the battery status parameters from the UI
     * @throws CommunicationException with the code API_ERROR_BLUETOOTH_COMMUNICATION if writing to the bluetooth channel is not possible
     */
    public String sendBatteryStatusRequest(){
        return send("BATTERYSTATUS");
    }
    
    /**
     * Request the firmware version
     * @throws CommunicationException with the code API_ERROR_BLUETOOTH_COMMUNICATION if writing to the bluetooth channel is not possible
     */
    public String sendFirmwareVersionRequest(){
        return send("FWVERSION");
    }
    /**
     * Set the alarm off
     * @throws CommunicationException with the code API_ERROR_BLUETOOTH_COMMUNICATION if writing to the bluetooth channel is not possible
     */
    public String setAlarmOFF(){
        return send("ALARM 0");
    }
    /**
     * Set the message alarm mode
     * @throws CommunicationException with the code API_ERROR_BLUETOOTH_COMMUNICATION if writing to the bluetooth channel is not possible
     */
    public String setMessageAlarm(){
        return send("ALARM 1");
    }
    
    /**
     * Set the warning alarm mode
     * @throws CommunicationException with the code API_ERROR_BLUETOOTH_COMMUNICATION if writing to the bluetooth channel is not possible
     */
    public String  setWarningAlarm(){
        return send("ALARM 2");
    }
    /**
     * Set the full alarm mode
     * @throws CommunicationException with the code API_ERROR_BLUETOOTH_COMMUNICATION if writing to the bluetooth channel is not possible
     */
    public String setFullAlarm(){
        return send("ALARM 3");
    }
    
    /**
     * Set the SOS alarm mode
     * @throws CommunicationException with the code API_ERROR_BLUETOOTH_COMMUNICATION if writing to the bluetooth channel is not possible
     */
    public String setSOSAlarm(){
        return send("ALARM 4");
    }
    
    public String send(String commands)  {
          try {
              String command;
              command=commands;
              CRCManager crcm = new CRCManager();
              byte buffer[] = command.getBytes();
              int crc = crcm.crc16(buffer, 0, buffer.length);
              command = command.concat("*").concat(Integer.toHexString(crc).toUpperCase());
              //insert header and footer
              command = "+".concat(command).concat("-");
            
              System.out.println(command);
            dataout.write(command.getBytes());
          
              return command;
          } catch (IOException ex) {
              return null;
          }
        
    }
}
