package uilib.exus;

/**
 * This class of exception is made to handle to problems of the communication.<br>
 * Communication exceptions are thrown when the connection fails, or when a command doesn't get a response or when data are corrupted
 * @author afa
 */
public class CommunicationException extends Exception {

    public static final int API_ERROR_BLUETOOTH_COMMUNICATION = 1000;
    public static final int API_ERROR_BLUETOOTH_DEVICE_NOTINRANGE = 1001;
    public static final int API_ERROR_COMMAND_FAILED = 2000;
    public static final int API_ERROR_DATALOGGER_NOTCONNECTED = 2001;
    public static final int API_ERROR_COMMUNICATION_CORRUPTED_DATA = 2002;
    public static final int API_ERROR_COMMUNICATION_INCOMPLETE_DATA = 2003;
    public static final int API_ERROR_COMMUNICATION_TIMEOUT = 2004;
    public static final int API_ERROR_FAILEDTOGETMEMORY = 2005;
    public static final int API_ERROR_STREAMING_NOT_STARTED = 2006;
    public static final int API_ERROR_DOWNLOAD_NOT_STARTED  = 2007;
    public static final int DLL_ERROR_STREAMING_ENDOFSESSION = 2008;
    public static final int DLL_ERROR_STREAMING_NOTRECEIVING = 2009;
    
    private String message = null;
    private int code = 0;

    /**
     * Constructor
     */
    public CommunicationException() {
    }

    /**
     * Gets the message of the exception
     * @return the message
     */
    @Override
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message of the exception
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets the code of the exception
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * Sets the code of the exception
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }
}
