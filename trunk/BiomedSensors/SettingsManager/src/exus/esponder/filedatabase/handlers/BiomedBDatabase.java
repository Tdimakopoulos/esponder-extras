/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exus.esponder.filedatabase.handlers;

import exus.esponder.filedatabase.model.BiomedBTable;
import exus.esponder.filedatabase.model.Pathmanager;
import exus.esponder.filedatabase.model.UITable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MEOC-WS1
 */
public class BiomedBDatabase {

    ArrayList<BiomedBTable> pdb = new ArrayList<BiomedBTable>();
    int ipos;
    String imei;

    public void SetFilename(String szimei) {
        imei = szimei;
    }

    public ArrayList<BiomedBTable> getList() {
        return pdb;
    }

    public void SaveOnFile() throws FileNotFoundException, IOException {

        String szPath;
        Pathmanager pmanager = new Pathmanager();
        szPath = pmanager.GetPath();
        FileOutputStream fos = new FileOutputStream(szPath + imei);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(pdb);
        oos.close();
    }

    public void LoadFromFile() throws FileNotFoundException, IOException, ClassNotFoundException {

        String szPath;
        Pathmanager pmanager = new Pathmanager();
        szPath = pmanager.GetPath();

        pdb.clear();

        File f = new File(szPath + imei);
        if (f.exists()) {
            FileInputStream fis = new FileInputStream(szPath + imei);
            ObjectInputStream ois = new ObjectInputStream(fis);
            pdb = (ArrayList<BiomedBTable>) (List<BiomedBTable>) ois.readObject();
            ois.close();
            ipos = pdb.size();
        } else {
            ipos = 0;
        }
    }

    public BiomedBTable getItem(int indexpos) {
        return pdb.get(indexpos);
    }

    public int getpos() {
        return ipos;
    }

    public void Add(BiomedBTable pitem) {
        ipos = ipos + 1;

        pdb.add(pitem);
    }
}
