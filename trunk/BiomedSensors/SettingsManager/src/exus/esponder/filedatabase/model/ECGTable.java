/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exus.esponder.filedatabase.model;

import java.io.Serializable;

/**
 *
 * @author tdim
 */
public class ECGTable implements Serializable{
 
    private Long lvalue;
    private float fvalue;
    private String svalue;
    private Long time;
    private String szField1;
    private String szField2;
    private String szField3;
    private Long lfield1;
    private Long lfield2;
    private int ifield1;
    private int ifield2;

    /**
     * @return the time
     */
    public Long getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(Long time) {
        this.time = time;
    }

    /**
     * @return the szField1
     */
    public String getSzField1() {
        return szField1;
    }

    /**
     * @param szField1 the szField1 to set
     */
    public void setSzField1(String szField1) {
        this.szField1 = szField1;
    }

    /**
     * @return the szField2
     */
    public String getSzField2() {
        return szField2;
    }

    /**
     * @param szField2 the szField2 to set
     */
    public void setSzField2(String szField2) {
        this.szField2 = szField2;
    }

    /**
     * @return the szField3
     */
    public String getSzField3() {
        return szField3;
    }

    /**
     * @param szField3 the szField3 to set
     */
    public void setSzField3(String szField3) {
        this.szField3 = szField3;
    }

    /**
     * @return the lfield1
     */
    public Long getLfield1() {
        return lfield1;
    }

    /**
     * @param lfield1 the lfield1 to set
     */
    public void setLfield1(Long lfield1) {
        this.lfield1 = lfield1;
    }

    /**
     * @return the lfield2
     */
    public Long getLfield2() {
        return lfield2;
    }

    /**
     * @param lfield2 the lfield2 to set
     */
    public void setLfield2(Long lfield2) {
        this.lfield2 = lfield2;
    }

    /**
     * @return the ifield1
     */
    public int getIfield1() {
        return ifield1;
    }

    /**
     * @param ifield1 the ifield1 to set
     */
    public void setIfield1(int ifield1) {
        this.ifield1 = ifield1;
    }

    /**
     * @return the ifield2
     */
    public int getIfield2() {
        return ifield2;
    }

    /**
     * @param ifield2 the ifield2 to set
     */
    public void setIfield2(int ifield2) {
        this.ifield2 = ifield2;
    }

    /**
     * @return the lvalue
     */
    public Long getLvalue() {
        return lvalue;
    }

    /**
     * @param lvalue the lvalue to set
     */
    public void setLvalue(Long lvalue) {
        this.lvalue = lvalue;
    }

    /**
     * @return the fvalue
     */
    public float getFvalue() {
        return fvalue;
    }

    /**
     * @param fvalue the fvalue to set
     */
    public void setFvalue(float fvalue) {
        this.fvalue = fvalue;
    }

    /**
     * @return the svalue
     */
    public String getSvalue() {
        return svalue;
    }

    /**
     * @param svalue the svalue to set
     */
    public void setSvalue(String svalue) {
        this.svalue = svalue;
    }
}
