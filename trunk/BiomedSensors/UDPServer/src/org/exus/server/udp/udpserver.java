/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.server.udp;

import java.io.*;
import java.net.*;

/**
 *
 * @author tdim
 */
public class udpserver {

    public void WaitForMessage(String szMessage,int iport) throws SocketException, IOException {
        DatagramSocket serverSocket = new DatagramSocket(iport);
        byte[] receiveData = new byte[1024];
        byte[] sendData = new byte[1024];
        boolean brun=true;
        while (brun) {
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);
            String sentence = new String(receivePacket.getData());
            System.out.println("RECEIVED: " + sentence);
            if (sentence.equalsIgnoreCase(szMessage))
                brun=false;
        }
        serverSocket.close();
    }
}
