/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author afa
 */
 public class BiomedBFrame extends BiomedAFrame{
    public BiomedBFrame(byte buffer[]) {
        super(buffer);
    }
    
    /**
     * Process the parsing of the frame
     */
    @Override
    public void process(){
        BiomedBPacket packet = null;
        List<BiomedBPacket> packetList = new LinkedList<BiomedBPacket>();
        
        //System.out.println("frame number : "+this.getFrameNumber());
        boolean hasLargeFlags = ((this.buffer[2] & 1 << 3) != 0);
        this.index = 3;
        do{
          packet = new BiomedBPacket(hasLargeFlags, this.index);
          packet.process(this.buffer);
          packetList.add(packet);
          this.index += packet.getSizeOfPayload() + 1 + 1 +1;// channel field + size field + flag field + payload size
          if(hasLargeFlags == true){
              this.index += 1;
          }
          if(packet.hasLargeFlags() == true){
              this.index += 1;
          }
        }while(this.index < (this.buffer.length-2));
        this.packets = packetList.toArray(new BiomedBPacket[0]);
    }
    /**
     * @return the packets
     */
    @Override
    public BiomedBPacket[] getPackets() {
        return (BiomedBPacket[])packets;
    }
}
