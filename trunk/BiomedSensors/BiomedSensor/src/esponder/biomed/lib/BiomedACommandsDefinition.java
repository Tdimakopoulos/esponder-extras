/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

/**
 *
 * @author afa
 */
 public final class BiomedACommandsDefinition {

    public enum Classes {

        SYSTEM(1),
        ACQUISITION(2),
        COMMUNICATION(3),
        MEMORY(4),
        BOOTMANAGER(5),
        POWER(6),
        SENSORBUS(7),
        SECURITY(8),
        NEPHRONPLUS_SYS(9), /*owner:GDu*/
        NEPHRONPLUS_DATA(10), /*owner:GDu*/
        NEPHRONPLUS_CTRL(11), /*owner:GDu*/
        CLASS_ANT(12),
        _LAST(13);
        /*Keep as last!*/
        public final short code;

        private Classes(int myclass) {
            this.code = (short) myclass;
        }

        public static Classes get(short code) {
            for (Classes f : Classes.values()) {
                if (f.code == code) {
                    return f;
                }
            }
            return null;
        }
    }

    public enum OpcodeFlag {

        COMMAND(0),
        RESPONSE(1 << 6),
        STATUS(1 << 7),
        ERROR(1 << 6 | 1 << 7);
        public final short flag;

        private OpcodeFlag(int myflag) {
            this.flag = (short) myflag;
        }

        public static OpcodeFlag get(short flag) {
            for (OpcodeFlag f : OpcodeFlag.values()) {
                if (f.flag == flag) {
                    return f;
                }
            }
            return null;
        }
    }

    public final class OpcodeError {

        public static final short UNSUPPORTEDCOMMAND = 1;
        public static final short WRONGPARAMETER = 2;
        public static final short COMMANDFAILED = 3;
        public static final short OUTOFMEMORY = 4;
        public static final short HARDWAREERROR = 5;
    }

    public final class Class {

        public final class SYSTEM {

            public final class OPCODE {

                public static final short VERSION = 1;
                public static final short PARSECONFIGSTRING = 2;
                public static final short GETCLOCK = 3;
                public static final short SETCLOCK = 4;
                public static final short DUMPCONFIGURATIONBLOCK = 5;
                public static final short PLLMULTIPLICATOR = 6;
                public static final short LOG = 7;
                public static final short DEVICEID = 8;
                public static final short CONSOLE = 9;
                public static final short STATE = 10;
                public static final short SESSIONSTATE = 11;       /*Owner:OGr*/

                public static final short DEVICEINFO = 12; /*Owner:OGr*/

                public static final short _LAST = 13;       /*Keep as last!*/

            }

            public final class PARAMETER {

                public static final short SOFTWAREVERSION = 1;
                public static final short PLATFORMTYPE = 2;
                public static final short BUILDNUMBER = 3;
                public static final short STRING = 4;
                public static final short CLOCK_DAY = 5;
                public static final short CLOCK_MONTH = 6;
                public static final short CLOCK_YEAR = 7;
                public static final short CLOCK_DAYOFWEEK = 8;
                public static final short CLOCK_DAYOFYEAR = 9;
                public static final short CLOCK_HOUR = 10;
                public static final short CLOCK_MINUTE = 11;
                public static final short CLOCK_SECOND = 12;
                public static final short BLOCKNUMBER = 13;
                public static final short START = 14;
                public static final short PLLMULTIPLICATOR = 15;
                public static final short END = 16;
                public static final short SIZE = 17;
                public static final short RECORD = 18;
                public static final short STREAM = 19;
                public static final short REPLAY = 20;
                public static final short STREAM_TEST_FRAMES = 21;
                public static final short RECORDSUSPEND = 22; /*creator:OGr*/

                public static final short HARDWAREVERSION = 23;       /*creator:OGr*/

                public static final short HARDWAREREVISION = 24;      /*creator:OGr*/

                public static final short SHOWHR = 25;        /*Owner:OGr*/

                public static final short LOGMODE = 26;       /*Owner:OGr*/

                public static final short ESTMODE = 27;       /*Owner:OGr*/

                public static final short _LAST = 28;  /*Keep as last!*/

            }
        }//end class SYSTEM

        public final class BOOT_MANAGER {

            public final class OPCODE {

                public static final short CHECKCURRENTFIRMWARE = 1;
                public static final short FIRMWAREPROGRAMM_START = 2;
                public static final short FIRMWAREPROGRAMM_END = 3;
                public static final short FIRMWAREPROGRAMM_BURN = 4;
                public static final short FIRMWAREPROGRAMM_ERASEAREA = 5;
                public static final short BUILDCRC32 = 6;
                public static final short REBOOT = 7;
                public static final short ENTER = 8;
                public static final short GETVERSION = 9; /*Owner:OGr*/

                public static final short _LAST = 10;/*Keep as last!*/

            }

            public final class PARAMETER {

                public static final short BUILDNUMBER = 1;
                public static final short PLATFORMID = 2;
                public static final short CPUID = 3;
                public static final short CRC32 = 4;
                public static final short CRC16 = 5;
                public static final short SIZE = 6;
                public static final short ADDRESS = 7;
                public static final short DATA = 8;
                public static final short MAXDATASIZE = 9;
                public static final short COUNTER = 10;
                public static final short VERSION = 11;  /*creator:OGr*/

                public static final short REVISION = 12; /*creator:OGr*/

                public static final short _LAST = 13;    /*Keep as last!*/

            }
        }//end class SYSTEM

        public final class ACQUISITION {

            public final class OPCODE {

                public static final short TIMESTAMP = 1;
                public static final short ACQSIGNALINFO = 2;
                public static final short ALGORITHMINFO = 3;
                public static final short SETACQSIGNALS = 4;
                public static final short SETALGORITHMS = 5;
                public static final short STATE = 6;
                public static final short SENSING_FAULT = 7;
                public static final short CALIBRATE_ALTIMETER = 8;   /*Owner: OGr*/

                public static final short _LAST = 9;  /*Keep as last!*/

            }

            public final class PARAMETER {

                public static final short FREQUENCY = 1;
                public static final short TIMESTAMP = 2;
                public static final short CHANNEL = 3;
                public static final short COMMCHANNEL = 4;
                public static final short SIGNALSIZE = 5;
                public static final short CHANNELID = 6;
                public static final short TIMESTAMPINTERVAL = 7;
                public static final short FREQUENCY10 = 8;
                public static final short SHIFT = 9;
                public static final short AVERAGE = 10;
                public static final short INVERT = 11;
                public static final short OFFSET0ADCU = 12;
                public static final short ENABLE = 13;
                public static final short NAME = 14;
                public static final short BASEID = 15;
                public static final short CHANNELCOUNT = 16;
                public static final short UNSIGNED = 17;
                public static final short RECORD = 18;
                public static final short STREAM = 19;
                public static final short REPLAY = 20;
                public static final short PHYSICALUNITNAME = 21;
                public static final short PHYISCALUNITSCALEMULTIPLIER_STRING = 22;
                public static final short PRIVATE = 23;
                public static final short CONTINUOUS = 24;
                public static final short ANNOTATION = 25;
                public static final short AC_FREQUENCY_HZ = 26;
                public static final short FREQUENCYSTRING = 27;  /*Owner:OGr*/
                public static final short LEFTSHIFTCOUNT = 28;
                public static final short MERGE_TARGETID = 29;
                public static final short RECORDSUSPEND = 30;    /*Owner:OGr*/
                public static final short PARAMUPDATE = 31;      /*Owner:OGr*/
                public static final short ALTITUDE = 32; /*Owner:OGr*/                
                public static final short DEBUGENABLE = 33;      /*Owner:OGr*/
                public static final short GROUPEID = 34;
                public static final short PHYISCALUNITSCALE_NUMERATOR = 35;
                public static final short PHYISCALUNITSCALE_DENOMINATOR = 36;
                public static final short _LAST = 37;     /*Keep as last!*/
            }
        }//end class ACQUISITION

        public final class COMMUNICATION {

            public final class OPCODE {
                public static final short BTRATE = 1;
                public static final short FLUSHINTERVAL = 2;
                public static final short CHANNELFILTER = 3;
                public static final short BTPINCODE = 4;   /*Owner:OGr*/
                public static final short _LAST = 5;       /*Keep as last!*/
            }

            public final class PARAMETER {
                public static final short BAUDRATE = 1;
                public static final short INTERVAL = 2;
                public static final short NUMBER = 3;
                public static final short ENABLE = 4;
                public static final short NUMBER_RANGE_FROM = 5;
                public static final short NUMBER_RANGE_TO = 6;
                public static final short STRING = 7; /*Owner:OGr*/
                public static final short _LAST = 8;   /*Keep as last!*/
            }
        }//end class COMMUNICATION

        public final class MEMORY {
            public final class OPCODE {
                public static final short FLASHPAGE = 1;
                public static final short STORETIMESYNC = 2;
                public static final short SEARCHPAGE = 3;
                public static final short TELLCURRENTWRITEPOSITION = 3;
                public static final short READFLASHPAGE = 4;
                public static final short STARTOFDOWNLOAD = 5;
                public static final short ENDOFDOWNLOAD = 6;
                public static final short READWORKINGBUFFER = 7;
                public static final short SET = 8;
                public static final short STAT = 9;
                public static final short BOOTDATE = 10;
                public static final short ANNOTATION = 11;
                public static final short ERASE = 12;      /*creator:OGr*/
                public static final short GET = 13;        /*creator:OGr*/
                public static final short ABORTDOWNLOAD = 14;      /*creator:OGr*/
                public static final short REWINDDOWNLOAD = 15;     /*creator:OGr*/
                public static final short LAST = 16;        /*Keep as last!*/
            }

            public final class PARAMETER {
                public static final short FLASHROW = 1;
                public static final short PAGENR = 2;
                public static final short SESSIONNR = 3;
                public static final short DOWNLOADNR = 4;
                public static final short SYNCSECONDS = 5;
                public static final short SYNCMILLISECONDS = 6;
                public static final short DOWNLOADNROFFSET = 7;
                public static final short SYNCTIMESTAMP = 8;
                public static final short PAGESIZE = 9;
                public static final short CRC16 = 10; /*also public static final short CRC16_0_1*/
                public static final short NEXTFLASHROW = 11;
                public static final short BADBLOCK = 12;
                public static final short ECCFAILURE = 13;
                public static final short BLANK = 14;
                public static final short YEAR = 15;
                public static final short MONTH = 16;
                public static final short DAY = 17;
                public static final short HOUR = 18;
                public static final short MINUTE = 19;
                public static final short SECOND = 20;
                public static final short WRITEDATE = 21;
                public static final short SESSIONSTARTROW = 22;
                public static final short INDEX = 23;
                public static final short USER_DATA = 24;
                public static final short ISALREADYDOWNLOADED = 25;
                public static final short PAGECOUNT = 26;
                public static final short DATAOFFSET_HEAD = 27;
                public static final short DATAOFFSET_TAIL = 28;
                public static final short CRC16_2_3 = 29;
                public static final short CRC16_4_5 = 30;
                public static final short CRC16_6_7 = 31;
                public static final short CRC16_8_9 = 32;
                public static final short SECONDARY_FLASHROW = 33;
                public static final short SECONDARY_PAGENR = 34;
                public static final short SECONDARY_SESSIONSTARTROW = 35;
                public static final short STRING = 36;
                public static final short ERASE_ALL = 37;     /*creator:OGr*/
                public static final short NUMBEROFSESSION = 38;       /*creator:OGr*/
                public static final short SESSIONID = 39;     /*creator:OGr*/
                public static final short SESSIONSTOPROW = 40;        /*creator:OGr*/
                public static final short NBPAGE = 41;        /*creator:OGr*/
                public static final short ERASE_ALLUSED = 42; /*creator:OGr*/
                public static final short ERASE_SECTORNR = 43;        /*creator:OGr*/
                public static final short STAT_FULL = 44;     /*creator:OGr*/
                public static final short STAT_ERASING = 45;  /*creator:OGr*/
                public static final short STAT_UNRESPONSIVE = 46;     /*creator:OGr*/
                public static final short USERSESSIONDURATION = 47;   /*creator:OGr*/
                public static final short USERSESSIONFLAG = 48;       /*creator:OGr*/
                public static final short _LAST = 49;  /*Keep as last!*/
            }
        }//end class MEMORY

        public final class POWER {

            public final class OPCODE {
                public static final short GETSTATUS = 1;
                public static final short _LAST = 2;       /*Keep as last!*/
            }

            public final class PARAMETER {
                public static final short ISCHARGING = 1;
                public static final short ISEXTERNALYPOWERED = 2;
                public static final short BATTERYREMAININGPERCENT = 3;
                public static final short BATTERYREMAINING_ISRELIABLE = 4;
                public static final short BATTERYVOLTAGE = 5;
                public static final short BATTERYCURRENT__uA = 6;
                public static final short _LAST = 7;       /*Keep as last!*/
            }
        }//end class POWER

        public final class ANT {
            public final class OPCODE {
                public static final short OPEN = 1;
                public static final short MESSAGE = 2;
                public static final short CLOSE = 3;
                public static final short _LAST = 4; /*Keep as last!*/
            }

            public final class PARAMETER {
                public static final short CHANNEL = 1;
                public static final short DEVICE_NUMBER = 2;
                public static final short DEVICE_TYPE = 3;
                public static final short TRANSMISSION_TYPE = 4;
                public static final short DATA = 5;
                public static final short _LAST = 6;   /*Keep as last!*/
            }
        }//end class POWER
    }
}