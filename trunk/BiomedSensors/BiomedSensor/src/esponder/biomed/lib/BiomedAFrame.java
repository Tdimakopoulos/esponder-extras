/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author afa
 */
class BiomedAFrame {

    protected byte buffer[] = null;
    protected int index = 0;
    protected BiomedAPacket packets[] = null;

    /**
     * Constructor
     * @param buffer COBS decoded buffer without the sync byte 0 
     */
    public BiomedAFrame(byte buffer[]) {
        this.buffer = buffer;
        this.index = 0;
    }

    /**
     * Get the number of the frame
     * @return the frame number
     */
    public int getFrameNumber() {
        return (((int)(this.buffer[0])&0xFF) << 8 | ((int)this.buffer[1]&0xFF)) & 0xffff;
    }

    /**
     * A quick check if the frame is valid by checking the size and CRC
     * @return true if the frame is valid, otherwise false
     */
    public boolean isValid() {
        if (this.buffer.length < 5) {
            return false;
        }
        int computedcrc = CRC.crc16(this.buffer, this.buffer.length - 2);
        int framecrc = ((int)this.buffer[this.buffer.length - 2 + 0]&0x00FF) << 8 ; 
        framecrc += this.buffer[this.buffer.length - 2 + 1]&0xFF;
        //int framecrc = (((int)this.buffer[this.buffer.length - 2 + 0]) << 8 | this.buffer[this.buffer.length - 2 + 1]) & 0xffff;
        if (framecrc != computedcrc) {
            return false;
        }
        return true;
    }
    
    /**
     * Process the parsing of the frame
     */
    public void process(){
        BiomedAPacket packet = null;
        List<BiomedAPacket> packetList = new LinkedList<BiomedAPacket>();
        
        //System.out.println("frame number : "+this.getFrameNumber());
        boolean hasLargeFlags = ((this.buffer[2] & 1 << 3) != 0);
        this.index = 3;
        do{
          packet = new BiomedAPacket(hasLargeFlags, this.index);
          packet.process(this.buffer);
          packetList.add(packet);
          this.index += packet.getSizeOfPayload() + 1 + 1 +1;// channel field + size field + flag field + payload size
          if(hasLargeFlags == true){
              this.index += 1;
          }
          if(packet.hasLargeFlags() == true){
              this.index += 1;
          }
        }while(this.index < (this.buffer.length-2));
        this.packets = new BiomedAPacket[packetList.size()];
        for(int i = 0; i < this.packets.length; i++){
            this.packets[i] = packetList.get(i);
        }
    }

    /**
     * @return the packets
     */
    public BiomedAPacket[] getPackets() {
        return packets;
    }
}
