/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;


/**
 *
 * @author afa
 */
class BiomedAPacket {

    public static enum PacketType {
        COMMAND_RESPONSE, SAMPLES_CHANNEL, SAMPLES_STRUCTURE
    };
    
    protected int flags;
    protected int sizeOfPayload;
    protected boolean hasLargeFlags;
    protected int payloadStartIndex;
    protected int index;
    protected Payload payload = null;

    public BiomedAPacket(boolean hasLargeFlags, int index){
        this.hasLargeFlags = hasLargeFlags;
        this.index = index;
    }
    
    /**
     * parse the buffer starting from the index setted in the constructor
     * @param buffer
     * @return 
     */
    public boolean process(byte buffer[]) {
        boolean hasLargeSize = false;
        int channel = 0;
        
        if (this.index + 3 >= buffer.length) {
            return false;   /*Remaining buffer sizeOfPayload is too short for a minimal packet!*/
        }
        flags = buffer[this.index++] & 0xff;
        if (hasLargeFlags) {
            this.flags = (this.flags << 8 | buffer[this.index++]) & 0xffff;
        }
        hasLargeSize = ((this.flags & 1 << 7) != 0);
        this.sizeOfPayload = buffer[this.index++] & 0xff;
        if (hasLargeSize) {
            this.sizeOfPayload = ((this.sizeOfPayload << 8) | buffer[this.index++]) & 0xffff;
        }
        channel = (short) (buffer[this.index++] & 0xff);
        this.payloadStartIndex = this.index;
        switch(this.getType(channel)){
            case COMMAND_RESPONSE:
                this.payload = new BiomedACommand(buffer, this.payloadStartIndex, this.sizeOfPayload);
                break;
            case SAMPLES_CHANNEL:
                this.payload = new BiomedAData(this.flags, channel, buffer, this.payloadStartIndex, this.sizeOfPayload);
                break;
        }
        return true;
    }

     /**
     * get the packet type respected to the values : COMMAND_RESPONSE, SAMPLES_CHANNEL, SAMPLES_STRUCTURE
     * @return 
     */
    protected PacketType getType(int channel) {
        if (channel == 0x00) {
            return PacketType.COMMAND_RESPONSE;
        }
        if ((((this.flags >> 3) & (0x03)) == 3)&&(((this.flags >> 1) & (0x03)) == 2)) {
            return PacketType.SAMPLES_STRUCTURE;
        }
        return PacketType.SAMPLES_CHANNEL;
    }
    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }
    
     /**
     * @return the payloadStartIndex
     */
    public int getPayloadStartIndex() {
        return payloadStartIndex;
    }
    
    /**
     * @return the sizeOfPayload
     */
    public int getSizeOfPayload() {
        return sizeOfPayload;
    }
    
    public boolean hasLargeFlags(){
        return this.hasLargeFlags;
    }
    
    /**
     * @return the payload
     */
    public Payload getPayload() {
        return payload;
    }
}
