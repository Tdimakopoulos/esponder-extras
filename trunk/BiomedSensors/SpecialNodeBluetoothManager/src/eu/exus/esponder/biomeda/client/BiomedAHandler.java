/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.exus.esponder.biomeda.client;

import exus.esponder.settings.settingsmanager;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

/**
 *
 * Name : null channel 163
 *
 * "activity/MET"
 *
 * > Name : null channel 164
 *
 * "actvity/instMET"
 *
 * > Name : null channel 154
 *
 * "ecg/RespSynch" (I don't think that this one is relevant for you!)
 *
 * > Name : null channel 149
 *
 * "activity/period"
 *
 * @author tdim
 */
public class BiomedAHandler {
    
    StreamConnection sc = null;
    BiomedACommandSender command = null;
    BiomedAReadThread rt = new BiomedAReadThread();
    
    public BiomedACommandSender  GetCommander()
    {
        return command;
    }
    public void OpenConnection() throws IOException {
        settingsmanager pp = new settingsmanager();
        pp.ReadValues();
        System.out.println("Open Bluetooth Connection with the BiomedA... Please wait ....");
        sc = (StreamConnection) Connector.open(pp.getbA());//("btspp://000BCE075BBA:1;authenticate=false;encrypt=false;master=false");
        command = new BiomedACommandSender();
        command.OpenStream(sc);
        System.out.println("Connection and streaming is open for BiomedA ....");
    }
    
    public void CloseConnection() throws IOException {
        System.out.println("Closing connection with BiomedA");
        command.CloseStream();
        sc.close();
        System.out.println("Connection with biomedA closed");
    }
    
    public void StartStreaming() throws IOException, InterruptedException {
        command.StopStreaming();
        Thread.sleep(500);
        command.StartStreaming();
    }
    
    public void StopStreaming() throws IOException {
        command.StopStreaming();
    }
    
    public void StartReadingThread() {
        rt.SetBTConnection(sc);
        rt.start();
    }
    
    public void StopReadingThread() {
        rt.StopReadThread();
    }
//    public static void main(String[] args) throws IOException, InterruptedException {
//        StreamConnection sc = (StreamConnection) Connector.open("btspp://000BCE075BBA:1;authenticate=false;encrypt=false;master=false");
//        BiomedACommandSender command = new BiomedACommandSender();
//        command.OpenStream(sc);
//        command.StopStreaming();
//        command.StartStreaming();
//
//        Thread.sleep(200);
//        BiomedAReadThread rt = new BiomedAReadThread();
//        rt.SetBTConnection(sc);
//        rt.start();
//        System.out.println("Get Values for 1 Minute");
//        Thread.sleep(30000);
//        System.out.println("Streaming Finished");
//        rt.StopReadThread();
//        System.out.println("Close Stream and free resources");
//
//        command.StopStreaming();
//        command.CloseStream();
//        sc.close();
//    }
}
