package eu.esponder.controller.crisis.terrain.bean;

import java.util.HashMap;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.terrain.TerrainRemoteService;
import eu.esponder.controller.crisis.terrain.TerrainService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.TerrainAndVegetationDTO;
import eu.esponder.model.crisis.TerrainAndVegetation;

@Stateless
public class TerrainBean implements TerrainRemoteService, TerrainService {

	/** The crisis crud service. */
	@EJB
	private CrudService<TerrainAndVegetation> tvCrudService;

	/** The mapping service. */
	@EJB
	ESponderMappingService mappingService;


	// -------------------------------------------------------------------------

	@Override
	public TerrainAndVegetationDTO findTerrainAndVegetationDTOById(Long terrainAndVegetationID) {
		TerrainAndVegetation terrainAndVegetation = findTerrainAndVegetationById(terrainAndVegetationID);
		if(terrainAndVegetation != null)
			return (TerrainAndVegetationDTO) mappingService.mapESponderEntity(terrainAndVegetation, TerrainAndVegetationDTO.class);
		else
			return null;
	}	

	@Override
	public TerrainAndVegetation findTerrainAndVegetationById(Long terrainAndVegetationID) {
		return (TerrainAndVegetation) tvCrudService.find(TerrainAndVegetation.class, terrainAndVegetationID);
	}

	// -------------------------------------------------------------------------

	@Override
	public TerrainAndVegetationDTO findTerrainAndVegetationByTitleRemote(String tvTitle, Long userId) {
		TerrainAndVegetation terrainAndVegetation = findTerrainAndVegetationByTitle(tvTitle, userId);
		if(terrainAndVegetation != null)
			return (TerrainAndVegetationDTO) mappingService.mapESponderEntity(terrainAndVegetation, TerrainAndVegetationDTO.class);
		else
			return null;
	}

	@Override
	public TerrainAndVegetation findTerrainAndVegetationByTitle(String tvTitle, Long userId) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("title", tvTitle);
		return (TerrainAndVegetation) tvCrudService.findSingleWithNamedQuery("TerrainAndVegetation.findByTitle", params);
	}
	
	// -------------------------------------------------------------------------

	@Override
	public TerrainAndVegetationDTO createTerrainAndVegetationRemote(TerrainAndVegetationDTO terrainAndVegetationDTO, Long userID) {
		TerrainAndVegetation terrainAndVegetation = (TerrainAndVegetation) mappingService.mapESponderEntityDTO(terrainAndVegetationDTO, TerrainAndVegetation.class);
		terrainAndVegetation = createTerrainAndVegetation(terrainAndVegetation, userID);
		TerrainAndVegetationDTO terrainAndVegetationPersisted = (TerrainAndVegetationDTO) mappingService.mapESponderEntity(terrainAndVegetation, TerrainAndVegetationDTO.class);
		return terrainAndVegetationPersisted;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public TerrainAndVegetation createTerrainAndVegetation(TerrainAndVegetation terrainAndVegetation, Long userID) {
		return (TerrainAndVegetation) tvCrudService.create(terrainAndVegetation);
	}

	//-------------------------------------------------------------------------

	@Override
	public void deleteTerrainAndVegetationRemote(Long tvID, Long userID) {
		this.deleteTerrainAndVegetation(tvID, userID);
	}
	
	@Override
	public void deleteTerrainAndVegetation(Long tvID, Long userID) {
		tvCrudService.delete(TerrainAndVegetation.class, tvID);
	}

	// -------------------------------------------------------------------------

	@Override
	public TerrainAndVegetationDTO updateTerrainAndVegetationRemote( TerrainAndVegetationDTO terrainAndVegetationDTO, Long userID) {
		TerrainAndVegetation terrainAndVegetation = (TerrainAndVegetation) mappingService.mapESponderEntityDTO(terrainAndVegetationDTO, TerrainAndVegetation.class);
		terrainAndVegetation = updateTerrainAndVegetation(terrainAndVegetation, userID);
		terrainAndVegetationDTO = (TerrainAndVegetationDTO) mappingService.mapESponderEntity(terrainAndVegetation, TerrainAndVegetationDTO.class);
		return terrainAndVegetationDTO;
	}

	@Override
	public TerrainAndVegetation updateTerrainAndVegetation(TerrainAndVegetation terrainAndVegetation, Long userID) {
		TerrainAndVegetation terrainAndVegetationPersisted = findTerrainAndVegetationById(terrainAndVegetation.getId());
		mappingService.mapEntityToEntity(terrainAndVegetation, terrainAndVegetationPersisted);
		terrainAndVegetationPersisted = tvCrudService.update(terrainAndVegetationPersisted);
		return terrainAndVegetationPersisted;
	}


}
