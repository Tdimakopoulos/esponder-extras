/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.generic.GenericService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class SetOfSnapshotsInMeocConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected GenericService getgenericService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				Object next = it.next();
				if(next.getClass() == OperationsCentreSnapshot.class) {

					Set<OperationsCentreSnapshot> sourceSnapshotSet = (Set<OperationsCentreSnapshot>) source;
					Set<Long> destSnapshotDTOSet = new HashSet<Long>();
					for(OperationsCentreSnapshot snapshot : sourceSnapshotSet) {
						destSnapshotDTOSet.add(snapshot.getId());
					}
					destination = destSnapshotDTOSet;
				}
				else
					if(next.getClass() == Long.class) {

						Set<Long> sourceSnapshotDTOSet = (Set<Long>) source;
						Set<OperationsCentreSnapshot> destSnapshotSet = new HashSet<OperationsCentreSnapshot>();
						for(Long snapshotID : sourceSnapshotDTOSet) {
							OperationsCentreSnapshot snapshot = (OperationsCentreSnapshot) this.getgenericService().
									getEntity(OperationsCentreSnapshot.class, snapshotID);
							destSnapshotSet.add(snapshot);
						}
						destination = destSnapshotSet;
					}
					else
						destination = null;
			}
		}
		else {
			//new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
