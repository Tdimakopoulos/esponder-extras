/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.action.ActionService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.action.ActionObjective;
import eu.esponder.util.ejb.ServiceLocator;
// TODO: Auto-generated Javadoc
//import eu.esponder.model.crisis.action.Action;

// 
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class SetOfActionObjectivesInActionsConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected ActionService getActionService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				Object next = it.next();
				if(next.getClass() == ActionObjective.class) {

					Set<ActionObjective> sourceActionObjectiveSet = (Set<ActionObjective>) source;
					Set<Long> destActionObjectiveDTOSet = new HashSet<Long>();
					for(ActionObjective actionObjective : sourceActionObjectiveSet) {
						destActionObjectiveDTOSet.add(actionObjective.getId());
					}
					destination = destActionObjectiveDTOSet;
					System.out.println("transforming Action Part Objective Set to Set of Longs");
				}
				else
					if(next.getClass() == Long.class) {

						Set<Long> sourceActionObjectiveDTOSet = (Set<Long>) source;
						Set<ActionObjective> destActionObjectiveSet = new HashSet<ActionObjective>();
						for(Long cActionObjectiveID : sourceActionObjectiveDTOSet) {
							ActionObjective destActionObjective = this.getActionService().findActionObjectiveById(cActionObjectiveID);
							destActionObjectiveSet.add(destActionObjective);
						}
						destination = destActionObjectiveSet;
						System.out.println("transforming Set of Longs to Action Part Objective Set");
					}
					else
						destination = null;
			}
		}
		else {
			//new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
