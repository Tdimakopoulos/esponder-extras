/*
 * 
 */
package eu.esponder.controller.crisis.action;

import javax.ejb.Local;

import eu.esponder.model.crisis.action.Action;
import eu.esponder.model.crisis.action.ActionObjective;
import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.model.crisis.action.ActionPartObjective;
import eu.esponder.model.snapshot.action.ActionPartSnapshot;
import eu.esponder.model.snapshot.action.ActionSnapshot;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface ActionService.
 */
@Local
public interface ActionService {

	/**
	 * Find action by id.
	 *
	 * @param actionID the action id
	 * @return the action
	 */
	public Action findActionById(Long actionID);

	/**
	 * Find action by title.
	 *
	 * @param title the title
	 * @return the action
	 */
	public Action findActionByTitle(String title);

	/**
	 * Creates the action.
	 *
	 * @param action the action
	 * @param userID the user id
	 * @return the action
	 */
	public Action createAction(Action action, Long userID);

	/**
	 * Update action.
	 *
	 * @param action the action
	 * @param userID the user id
	 * @return the action
	 */
	public Action updateAction(Action action, Long userID);

	/**
	 * Delete action.
	 *
	 * @param actionDTOID the action dtoid
	 * @param userID the user id
	 */
	public void deleteAction(Long actionDTOID, Long userID);

	/**
	 * Find action part by id.
	 *
	 * @param actionID the action id
	 * @return the action part
	 */
	public ActionPart findActionPartById(Long actionID);

	/**
	 * Find action part by title.
	 *
	 * @param title the title
	 * @return the action part
	 */
	public ActionPart findActionPartByTitle(String title);

	/**
	 * Creates the action part.
	 *
	 * @param actionPart the action part
	 * @param userID the user id
	 * @return the action part
	 */
	public ActionPart createActionPart(ActionPart actionPart, Long userID);

	/**
	 * Update action part.
	 *
	 * @param actionPart the action part
	 * @param userID the user id
	 * @return the action part
	 */
	public ActionPart updateActionPart(ActionPart actionPart, Long userID);

	/**
	 * Delete action part.
	 *
	 * @param actionPartID the action part id
	 * @param userID the user id
	 */
	public void deleteActionPart(Long actionPartID, Long userID);

	/**
	 * Find action objective by title.
	 *
	 * @param title the title
	 * @return the action objective
	 */
	public ActionObjective findActionObjectiveByTitle(String title);

	/**
	 * Find action objective by id.
	 *
	 * @param actionID the action id
	 * @return the action objective
	 */
	public ActionObjective findActionObjectiveById(Long actionID);

	/**
	 * Creates the action objective.
	 *
	 * @param actionObjective the action objective
	 * @param userID the user id
	 * @return the action objective
	 */
	public ActionObjective createActionObjective(ActionObjective actionObjective, Long userID);

	/**
	 * Update action objective.
	 *
	 * @param actionObjective the action objective
	 * @param userID the user id
	 * @return the action objective
	 */
	public ActionObjective updateActionObjective(ActionObjective actionObjective, Long userID);

	/**
	 * Delete action objective.
	 *
	 * @param actionObjectiveId the action objective id
	 * @param userID the user id
	 */
	public void deleteActionObjective(Long actionObjectiveId, Long userID);

	/**
	 * Find action part objective by id.
	 *
	 * @param actionID the action id
	 * @return the action part objective
	 */
	public ActionPartObjective findActionPartObjectiveById(Long actionID);

	/**
	 * Find action part objective by title.
	 *
	 * @param title the title
	 * @return the action part objective
	 */
	public ActionPartObjective findActionPartObjectiveByTitle(String title);

	/**
	 * Creates the action part objective.
	 *
	 * @param actionPartObjective the action part objective
	 * @param userID the user id
	 * @return the action part objective
	 */
	public ActionPartObjective createActionPartObjective(ActionPartObjective actionPartObjective, Long userID);

	/**
	 * Update action part objective.
	 *
	 * @param actionPartObjective the action part objective
	 * @param userID the user id
	 * @return the action part objective
	 */
	public ActionPartObjective updateActionPartObjective(ActionPartObjective actionPartObjective, Long userID);

	/**
	 * Delete action part objective.
	 *
	 * @param actionPartObjectiveId the action part objective id
	 * @param userID the user id
	 */
	public void deleteActionPartObjective(Long actionPartObjectiveId, Long userID);

	/**
	 * Find action snapshot by id.
	 *
	 * @param actionSnapshotID the action snapshot id
	 * @return the action snapshot
	 */
	public ActionSnapshot findActionSnapshotById(Long actionSnapshotID);

	/**
	 * Creates the action snapshot.
	 *
	 * @param actionSnapshot the action snapshot
	 * @param userID the user id
	 * @return the action snapshot
	 */
	public ActionSnapshot createActionSnapshot(ActionSnapshot actionSnapshot,Long userID);
	
	/**
	 * Find action part snapshot by id.
	 *
	 * @param actionPartSnapshotID the action part snapshot id
	 * @return the action part snapshot
	 */
	public ActionPartSnapshot findActionPartSnapshotById(Long actionPartSnapshotID);

	/**
	 * Creates the action part snapshot.
	 *
	 * @param actionPartSnapshot the action part snapshot
	 * @param userID the user id
	 * @return the action part snapshot
	 */
	public ActionPartSnapshot createActionPartSnapshot(ActionPartSnapshot actionPartSnapshot, Long userID);

	/**
	 * Update action snapshot.
	 *
	 * @param actionSnapshot the action snapshot
	 * @param userID the user id
	 * @return the action snapshot
	 */
	public ActionSnapshot updateActionSnapshot(ActionSnapshot actionSnapshot, Long userID);

	/**
	 * Update action part snapshot.
	 *
	 * @param actionPartSnapshot the action part snapshot
	 * @param userID the user id
	 * @return the action part snapshot
	 */
	public ActionPartSnapshot updateActionPartSnapshot(ActionPartSnapshot actionPartSnapshot, Long userID);

	/**
	 * Delete action snapshot.
	 *
	 * @param actionSnapshotID the action snapshot id
	 * @param userID the user id
	 */
	public void deleteActionSnapshot(Long actionSnapshotID, Long userID);

	/**
	 * Delete action part snapshot.
	 *
	 * @param actionPartSnapshotID the action part snapshot id
	 * @param userID the user id
	 */
	public void deleteActionPartSnapshot(Long actionPartSnapshotID, Long userID);
}
