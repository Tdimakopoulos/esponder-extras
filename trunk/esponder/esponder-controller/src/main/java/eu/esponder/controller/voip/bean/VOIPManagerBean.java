package eu.esponder.controller.voip.bean;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.controller.voip.VOIPManagerRemoteService;
import eu.esponder.controller.voip.VOIPManagerService;
import eu.esponder.dto.model.voip.ConferenceParticipantsDTO;
import eu.esponder.dto.model.voip.VoipConferenceDTO;
import eu.esponder.model.voip.ConferenceParticipants;
import eu.esponder.model.voip.VoipConference;




@Stateless
public class VOIPManagerBean implements VOIPManagerService, VOIPManagerRemoteService {

	@EJB
	private CrudService<VoipConference> ConferenceCrudService;
	
	@EJB
	private CrudService<ConferenceParticipants> ConferenceParticipantsCrudService;

	@EJB
	private ESponderMappingService mappingService;
	
	@Override
	public VoipConferenceDTO AddConferenceRemote(
			VoipConferenceDTO voipConferenceDTO,
			ConferenceParticipantsDTO conferenceParticipantsDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConferenceDTO UpdateConferenceRemote(
			VoipConferenceDTO voipConferenceDTO,
			ConferenceParticipantsDTO conferenceParticipantsDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConferenceDTO FindByConfEnIDRemort(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConferenceDTO FindByConIDRemort(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConferenceDTO FindByConfIDRemort(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConferenceDTO FindByConfTitleRemort(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConferenceDTO CreateConferenceRemort(VoipConferenceDTO pCreate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConferenceDTO UpdateConferenceRemort(VoipConferenceDTO pUpdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipantsDTO FindByConfPEnIDRemort(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipantsDTO FindByConPIDRemort(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipantsDTO FindByConfPIDRemort(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipantsDTO FindByConfPTitleRemort(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipantsDTO CreateConferenceParticipantsRemort(
			ConferenceParticipantsDTO pCreate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipantsDTO UpdateConferenceParticipantsRemort(
			ConferenceParticipantsDTO pUpdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConference AddConference(VoipConference voipConference,
			ConferenceParticipants conferenceParticipants) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConference UpdateConference(VoipConferenceDTO voipConference,
			ConferenceParticipantsDTO conferenceParticipants) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConference FindByConfEnID(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConference FindByConID(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConference FindByConfID(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConference FindByConfTitle(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConference CreateConference(VoipConferenceDTO pCreate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VoipConference UpdateConference(VoipConferenceDTO pUpdate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipants FindByConfPEnID(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipants FindByConPID(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipants FindByConfPID(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipants FindByConfPTitle(Long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipants CreateConferenceParticipants(
			ConferenceParticipantsDTO pCreate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConferenceParticipantsDTO UpdateConferenceParticipants(
			ConferenceParticipantsDTO pUpdate) {
		// TODO Auto-generated method stub
		return null;
	}

//	/** The user crud service. */
//	@EJB
//	private CrudService<ESponderSession> userCrudService;
//
//	/** The mapping service. */
//	@EJB
//	private ESponderMappingService mappingService;
//	
//	// -------------------------------------------------------------------------
//
//		/* (non-Javadoc)
//		 * @see eu.esponder.controller.crisis.user.UserRemoteService#createUserRemote(eu.esponder.dto.model.user.ESponderUserDTO, java.lang.Long)
//		 */
//		@Override
//		public ESponderSessionDTO createsessionRemote(ESponderSessionDTO userDTO, Long userID) {
//	
//			ESponderSession user = (ESponderSession) mappingService.mapESponderEntityDTO(
//					userDTO, ESponderSession.class);
//			user = createsession(user, userID);
//			return (ESponderSessionDTO) mappingService.mapESponderEntity(user,
//					ESponderSessionDTO.class);
//		}
//
//		/* (non-Javadoc)
//		 * @see eu.esponder.controller.crisis.user.UserService#createUser(eu.esponder.model.user.ESponderUser, java.lang.Long)
//		 */
//		@Override
//		@Interceptors(ActionAuditInterceptor.class)
//		public ESponderSession createsession(ESponderSession user, Long userID) {
//			
//			userCrudService.create(user);
//			return user;
//		}
//
//		// -------------------------------------------------------------------------
//
//		/* (non-Javadoc)
//		 * @see eu.esponder.controller.crisis.user.UserRemoteService#updateUserRemote(eu.esponder.dto.model.user.ESponderUserDTO, java.lang.Long)
//		 */
//		@Override
//		public ESponderSessionDTO updatesessionRemote(ESponderSessionDTO userDTO, Long userID) {
//			
//			
//			ESponderSession user = (ESponderSession) mappingService.mapESponderEntityDTO(userDTO, ESponderSession.class);
//			user = updatesession(user, userID);
//			return (ESponderSessionDTO) mappingService.mapESponderEntity(user, ESponderSessionDTO.class);
//		}
//
//		/* (non-Javadoc)
//		 * @see eu.esponder.controller.crisis.user.UserService#updateUser(eu.esponder.model.user.ESponderUser, java.lang.Long)
//		 */
//		@Override
//		@Interceptors(ActionAuditInterceptor.class)
//		public ESponderSession updatesession(ESponderSession user, Long userID) {
//						
//			ESponderSession userPersisted = userCrudService.find(ESponderSession.class, user.getId());
//			
//			mappingService.mapEntityToEntity(user, userPersisted);
//			return (ESponderSession) userCrudService.update(userPersisted);
//		}
//
//		// -------------------------------------------------------------------------
//
//		/* (non-Javadoc)
//		 * @see eu.esponder.controller.crisis.user.UserRemoteService#deleteUserRemote(java.lang.Long, java.lang.Long)
//		 */
//		@Override
//		public Long deletesessionRemote(Long deletedUserId, Long userID) {
//			return deletesession(deletedUserId, userID);
//		}
//
//		/* (non-Javadoc)
//		 * @see eu.esponder.controller.crisis.user.UserService#deleteUser(long, java.lang.Long)
//		 */
//		@Override
//		@Interceptors(ActionAuditInterceptor.class)
//		public Long deletesession(long deletedUserId, Long userID) {
//			ESponderSession resource = userCrudService.find(ESponderSession.class, deletedUserId);
//
//			userCrudService.delete(resource);
//
//			return deletedUserId;
//			
//		}

}
