/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.persistence.CrudService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.resource.OCMeoc;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartActionIDFieldConverter.
 */
public class FRTeamSupervisingMeocConverter implements CustomConverter {
	
	/**
	 * Gets the action service.
	 *
	 * @return the action service
	 */
	@SuppressWarnings("rawtypes")
	protected CrudService getCrudService() {
		try {
			return ServiceLocator.getResource("esponder/CrudBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if (sourceClass == OCMeoc.class && source != null) {
			OCMeoc sourceMeoc = (OCMeoc) source;
			Long meocID = sourceMeoc.getId();
			destination = meocID;
		}
		else if(sourceClass == Long.class && source!=null) { 
			Long meocID = (Long) source;
			OCMeoc destMeoc = (OCMeoc) this.getCrudService().find(destinationClass, meocID);
			destination = destMeoc;
		}
		else {
			//new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
