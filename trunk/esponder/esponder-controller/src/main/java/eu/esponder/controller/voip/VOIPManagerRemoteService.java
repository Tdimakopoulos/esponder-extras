package eu.esponder.controller.voip;

import javax.ejb.Remote;

import eu.esponder.dto.model.voip.ConferenceParticipantsDTO;
import eu.esponder.dto.model.voip.VoipConferenceDTO;
import eu.esponder.session.ESponderSessionDTO;

@Remote
public interface VOIPManagerRemoteService {

	VoipConferenceDTO AddConferenceRemote(
			VoipConferenceDTO voipConferenceDTO,
			ConferenceParticipantsDTO conferenceParticipantsDTO);
	
	VoipConferenceDTO UpdateConferenceRemote(
			VoipConferenceDTO voipConferenceDTO,
			ConferenceParticipantsDTO conferenceParticipantsDTO);
	
	
	VoipConferenceDTO FindByConfEnIDRemort(Long ID);
	VoipConferenceDTO FindByConIDRemort(Long ID);
	VoipConferenceDTO FindByConfIDRemort(Long ID);
	VoipConferenceDTO FindByConfTitleRemort(Long ID);
	
	VoipConferenceDTO CreateConferenceRemort(VoipConferenceDTO pCreate);
	
	VoipConferenceDTO UpdateConferenceRemort(VoipConferenceDTO pUpdate);
	
	ConferenceParticipantsDTO FindByConfPEnIDRemort(Long ID);
	ConferenceParticipantsDTO FindByConPIDRemort(Long ID);
	ConferenceParticipantsDTO FindByConfPIDRemort(Long ID);
	ConferenceParticipantsDTO FindByConfPTitleRemort(Long ID);
	
	ConferenceParticipantsDTO CreateConferenceParticipantsRemort(ConferenceParticipantsDTO pCreate);
	
	ConferenceParticipantsDTO UpdateConferenceParticipantsRemort(ConferenceParticipantsDTO pUpdate);
	
	
	
	
}
