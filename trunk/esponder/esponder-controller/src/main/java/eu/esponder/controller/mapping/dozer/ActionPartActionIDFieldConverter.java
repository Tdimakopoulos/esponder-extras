/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.action.ActionService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.action.Action;
import eu.esponder.util.ejb.ServiceLocator;


// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartActionIDFieldConverter.
 */
public class ActionPartActionIDFieldConverter implements CustomConverter {
	
	/**
	 * Gets the action service.
	 *
	 * @return the action service
	 */
	protected ActionService getActionService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if (sourceClass == Action.class && source != null) {
			Action sourceAction = (Action) source;
			Long actionID = sourceAction.getId();
			destination = actionID;
		}
		else if(sourceClass == Long.class && source!=null) { 
			Long actionID = (Long) source;
			Action destAction = (Action) this.getActionService().findActionById(actionID);
			destination = destAction;
		}
		else {
			//new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
