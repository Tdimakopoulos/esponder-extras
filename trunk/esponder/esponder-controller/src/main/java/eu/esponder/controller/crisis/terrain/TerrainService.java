package eu.esponder.controller.crisis.terrain;

import javax.ejb.Local;

import eu.esponder.model.crisis.TerrainAndVegetation;

@Local
public interface TerrainService {

	public TerrainAndVegetation findTerrainAndVegetationById(
			Long terrainAndVegetationID);

	public TerrainAndVegetation findTerrainAndVegetationByTitle(String tvTitle,
			Long userId);

	public TerrainAndVegetation createTerrainAndVegetation(
			TerrainAndVegetation terrainAndVegetation, Long userID);

	public void deleteTerrainAndVegetation(Long tvID, Long userID);

	public TerrainAndVegetation updateTerrainAndVegetation(
			TerrainAndVegetation terrainAndVegetation, Long userID);

}
