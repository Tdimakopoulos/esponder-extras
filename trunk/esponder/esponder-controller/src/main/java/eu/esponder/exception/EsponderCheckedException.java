/*
 * 
 */
package eu.esponder.exception;



// TODO: Auto-generated Javadoc
/**
 * The Class EsponderCheckedException.
 * The code will continue execution and will throw a exception, this kind of exception can be manage by the developer.
 */
public class EsponderCheckedException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2233804346787160257L;

	/**
	 * Instantiates a new esponder checked exception.
	 */
	public EsponderCheckedException() {
	}

	/**
	 * Instantiates a new esponder checked exception.
	 *
	 * @param clz the clz
	 * @param message the message
	 */
	public EsponderCheckedException(Class<?> clz, String message) {
		super(prepare(clz, message));

	}

	/**
	 * Instantiates a new esponder checked exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public EsponderCheckedException(String message, Throwable cause) {
		super(prepare(cause, message), cause);

	}

	/**
	 * Prepare.
	 *
	 * @param cause the cause
	 * @param msg the msg
	 * @return the string
	 */
	private static String prepare(Throwable cause, String msg) {
		EsponderExceptionLogger.LogError(cause.getClass(), msg);
		return msg;
	}

	/**
	 * Prepare.
	 *
	 * @param cls the cls
	 * @param msg the msg
	 * @return the string
	 */
	private static String prepare(Class<?> cls, String msg) {
		EsponderExceptionLogger.LogError(cls, msg);
		return msg;
	}
}


