/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.action.ActionService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.action.Action;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class SetOfActionsInCrisisContextConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected ActionService getActionService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				Object next = it.next();
				if(next.getClass() == Action.class) {

					Set<Action> sourceActionSet = (Set<Action>) source;
					Set<Long> destActionDTOSet = new HashSet<Long>();
					for(Action action : sourceActionSet) {
						destActionDTOSet.add(action.getId());
					}
					destination = destActionDTOSet;
				}
				else
					if(next.getClass() == Long.class) {

						Set<Long> sourceActionDTOSet = (Set<Long>) source;
						Set<Action> destActionSet = new HashSet<Action>();
						for(Long actionID : sourceActionDTOSet) {
							Action action = (Action) this.getActionService().findActionById(actionID);
							destActionSet.add(action);
						}
						destination = destActionSet;
					}
					else
						destination = null;
			}
		}
		else {
			//new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
