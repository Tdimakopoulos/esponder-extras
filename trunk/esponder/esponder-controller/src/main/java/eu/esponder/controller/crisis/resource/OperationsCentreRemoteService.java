/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.ActorCMDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.OCEocDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.dto.model.snapshot.ReferencePOISnapshotDTO;
import eu.esponder.dto.model.snapshot.SketchPOISnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface OperationsCentreRemoteService.
 */
@Remote
public interface OperationsCentreRemoteService {

	/**
	 * Find operation centre by id remote.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the operations centre dto
	 */
	public OperationsCentreDTO findOperationCentreByIdRemote(Long operationsCentreID);

	/**
	 * Find operations centre by user remote.
	 *
	 * @param operationsCentreID the operations centre id
	 * @param userID the user id
	 * @return the operations centre dto
	 */
	public OperationsCentreDTO findOperationsCentreByUserRemote(Long operationsCentreID, Long userID);

	/**
	 * Creates the operations centre remote.
	 *
	 * @param operationsCentreDTO the operations centre dto
	 * @param userID the user id
	 * @return the operations centre dto
	 */
	public Object createOperationsCentreRemote(Object operationsCentreDTO, Long userID);

	/**
	 * Update operations centre remote.
	 *
	 * @param operationsCentreId the operations centre id
	 * @param userID the user id
	 * @return the operations centre dto
	 */
//	public OperationsCentreDTO updateOperationsCentreRemote(OperationsCentreDTO operationsCentreDTO, Long userID);

	/**
	 * Delete operations centre remote.
	 *
	 * @param operationsCentreId the operations centre id
	 * @param userID the user id
	 */
	public void deleteOperationsCentreRemote(Long operationsCentreId, Long userID);

	/**
	 * Find operations centre snapshot by date remote.
	 *
	 * @param operationsCentreID the operations centre id
	 * @param maxDate the max date
	 * @return the operations centre snapshot dto
	 */
	public OperationsCentreSnapshotDTO findOperationsCentreSnapshotByDateRemote(Long operationsCentreID, Date maxDate);

	/**
	 * Creates the operations centre snapshot remote.
	 *
	 * @param operationsCentreDTO the operations centre dto
	 * @param snapshotDTO the snapshot dto
	 * @param userID the user id
	 * @return the operations centre snapshot dto
	 */
	public OperationsCentreSnapshotDTO createOperationsCentreSnapshotRemote(
			OCMeocDTO operationsCentreDTO,
			OperationsCentreSnapshotDTO snapshotDTO, Long userID);
	
	/**
	 * Update operations centre snapshot dto.
	 *
	 * @param operationsCentreDTO the operations centre dto
	 * @param snapshotDTO the snapshot dto
	 * @param userID the user id
	 * @return the operations centre snapshot dto
	 */
	public OperationsCentreSnapshotDTO updateOperationsCentreSnapshotDTO(OperationsCentreDTO operationsCentreDTO, OperationsCentreSnapshotDTO snapshotDTO, Long userID);

	/**
	 * Delete operations centre snapshot remote.
	 *
	 * @param operationsCentreSnapshotDTOID the operations centre snapshot dtoid
	 */
	public void deleteOperationsCentreSnapshotRemote(Long operationsCentreSnapshotDTOID);
	
	/**
	 * Find sketch poi by id remote.
	 *
	 * @param sketchPOIId the sketch poi id
	 * @return the sketch poidto
	 */
	public SketchPOIDTO findSketchPOIByIdRemote(Long sketchPOIId);

	/**
	 * Find sketch poi by title remote.
	 *
	 * @param title the title
	 * @return the sketch poidto
	 */
	public SketchPOIDTO findSketchPOIByTitleRemote(String title);
	
	/**
	 * Find sketch poi remote.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the list
	 */
	public List<SketchPOIDTO> findSketchPOIRemote(Long operationsCentreID);

	/**
	 * Creates the sketch poi remote.
	 *
	 * @param operationsCentreDTO the operations centre dto
	 * @param sketchDTO the sketch dto
	 * @param userID the user id
	 * @return the sketch poidto
	 */
	public SketchPOIDTO createSketchPOIRemote(OperationsCentreDTO operationsCentreDTO, SketchPOIDTO sketchDTO, Long userID);

	/**
	 * Update sketch poi remote.
	 *
	 * @param operationsCentreDTO the operations centre dto
	 * @param sketchDTO the sketch dto
	 * @param userID the user id
	 * @return the sketch poidto
	 */
	public SketchPOIDTO updateSketchPOIRemote(OperationsCentreDTO operationsCentreDTO, SketchPOIDTO sketchDTO, Long userID);
	
	/**
	 * Delete sketch poi remote.
	 *
	 * @param sketchPOIId the sketch poi id
	 * @param userID the user id
	 */
	public void deleteSketchPOIRemote(Long sketchPOIId, Long userID);
	
	/**
	 * Find reference poi by title remote.
	 *
	 * @param title the title
	 * @return the reference poidto
	 */
	public ReferencePOIDTO findReferencePOIByTitleRemote(String title);

	/**
	 * Find reference poi by id remote.
	 *
	 * @param referencePOIId the reference poi id
	 * @return the reference poidto
	 */
	public ReferencePOIDTO findReferencePOIByIdRemote(Long referencePOIId);

	/**
	 * Find reference po is remote.
	 *
	 * @return the list
	 */
	public List<ReferencePOIDTO> findReferencePOIsRemote();

	/**
	 * Creates the reference poi remote.
	 *
	 * @param operationsCentreDTO the operations centre dto
	 * @param referencePOIDTO the reference poidto
	 * @param userID the user id
	 * @return the reference poidto
	 */
	public ReferencePOIDTO createReferencePOIRemote(ReferencePOIDTO referencePOIDTO, Long userID);

	/**
	 * Update reference poi remote.
	 *
	 * @param operationsCentreDTO the operations centre dto
	 * @param referencePOIDTO the reference poidto
	 * @param userID the user id
	 * @return the reference poidto
	 */
	public ReferencePOIDTO updateReferencePOIRemote(ReferencePOIDTO referencePOIDTO, Long userID);
	
	/**
	 * Delete reference poi remote.
	 *
	 * @param referencePOIId the reference poi id
	 * @param userID the user id
	 */
	public void deleteReferencePOIRemote(Long referencePOIId, Long userID);

	/**
	 * Find all operations centres remote.
	 *
	 * @return the list
	 */
	public List<OperationsCentreDTO> findAllOperationsCentresRemote();

	/**
	 * Find registered operation centre by id remote.
	 *
	 * @param registeredOperationsCentreID the registered operations centre id
	 * @return the registered operations centre dto
	 */
	public RegisteredOperationsCentreDTO findRegisteredOperationCentreByIdRemote(Long registeredOperationsCentreID);

	/**
	 * Find registered operations centre by title remote.
	 *
	 * @param title the title
	 * @return the registered operations centre dto
	 */
	public RegisteredOperationsCentreDTO findRegisteredOperationsCentreByTitleRemote(String title);

	/**
	 * Find all registered operations centres remote.
	 *
	 * @return the list
	 */
	public List<RegisteredOperationsCentreDTO> findAllRegisteredOperationsCentresRemote();

	/**
	 * Creates the registered operations centre remote.
	 *
	 * @param registeredOperationsCentreDTO the registered operations centre dto
	 * @param userID the user id
	 * @return the registered operations centre dto
	 */
	public RegisteredOperationsCentreDTO createRegisteredOperationsCentreRemote(
			RegisteredOperationsCentreDTO registeredOperationsCentreDTO, Long userID);

	/**
	 * Delete registered operations centre remote.
	 *
	 * @param registeredOperationsCentreId the registered operations centre id
	 * @param userID the user id
	 */
	public void deleteRegisteredOperationsCentreRemote(Long registeredOperationsCentreId, Long userID);

	/**
	 * Update registered operations centre remote.
	 *
	 * @param registeredOperationsCentreDTO the registered operations centre dto
	 * @param userID the user id
	 * @return the registered operations centre dto
	 */
	public RegisteredOperationsCentreDTO updateRegisteredOperationsCentreRemote(
			RegisteredOperationsCentreDTO registeredOperationsCentreDTO, Long userID);

	/**
	 * Find all operations centres by crisis context remote.
	 *
	 * @param crisisContextID the crisis context id
	 * @return the list
	 */
	public List<OperationsCentreDTO> findAllOperationsCentresByCrisisContextRemote(
			Long crisisContextID);
	
	/**
	 * Find sketch poi snapshot by id remote.
	 *
	 * @param sensorID the sensor id
	 * @return the sketch poi snapshot dto
	 */
	public SketchPOISnapshotDTO findSketchPOISnapshotByIdRemote(Long sensorID);
	
	/**
	 * Find all sketch poi snapshots remote.
	 *
	 * @return the list
	 */
	public List<SketchPOISnapshotDTO> findAllSketchPOISnapshotsRemote();
	
	/**
	 * Find all sketch poi snapshots by poi remote.
	 *
	 * @param sensorDTO the sensor dto
	 * @param resultLimit the result limit
	 * @return the list
	 */
	public List<SketchPOISnapshotDTO> findAllSketchPOISnapshotsByPoiRemote(Long sensorDTO, int resultLimit);
	
	/**
	 * Find sketch poi snapshot by date remote.
	 *
	 * @param sensorID the sensor id
	 * @param dateTo the date to
	 * @return the sketch poi snapshot dto
	 */
	public SketchPOISnapshotDTO findSketchPOISnapshotByDateRemote(Long sensorID, Date dateTo);
	
	/**
	 * Find previous sketch poi snapshot remote.
	 *
	 * @param sensorID the sensor id
	 * @return the sketch poi snapshot dto
	 */
	public SketchPOISnapshotDTO findPreviousSketchPOISnapshotRemote(Long sensorID);
	
	/**
	 * Creates the sketch poi snapshot remote.
	 *
	 * @param snapshotDTO the snapshot dto
	 * @param userID the user id
	 * @return the sketch poi snapshot dto
	 */
	public SketchPOISnapshotDTO createSketchPOISnapshotRemote(SketchPOISnapshotDTO snapshotDTO, Long userID);
	
	/**
	 * Update sketch poi snapshot remote.
	 *
	 * @param sensorSnapshotDTO the sensor snapshot dto
	 * @param userID the user id
	 * @return the sketch poi snapshot dto
	 */
	public SketchPOISnapshotDTO updateSketchPOISnapshotRemote( SketchPOISnapshotDTO sensorSnapshotDTO, Long userID);
	
	/**
	 * Delete sketch poi snapshot remote.
	 *
	 * @param sensorSnapshotId the sensor snapshot id
	 * @param userID the user id
	 */
	public void deleteSketchPOISnapshotRemote(Long sensorSnapshotId, Long userID);

	/**
	 * Find reference poi snapshot by id remote.
	 *
	 * @param referencePOIID the reference poiid
	 * @return the reference poi snapshot dto
	 */
	ReferencePOISnapshotDTO findReferencePOISnapshotByIdRemote(
			Long referencePOIID);

	/**
	 * Find all reference poi snapshots remote.
	 *
	 * @return the list
	 */
	List<ReferencePOISnapshotDTO> findAllReferencePOISnapshotsRemote();

	
	/**
	 * Find reference poi snapshot by date remote.
	 *
	 * @param referencePOIID the reference poiid
	 * @param dateTo the date to
	 * @return the reference poi snapshot dto
	 */
	ReferencePOISnapshotDTO findReferencePOISnapshotByDateRemote(
			Long referencePOIID, Date dateTo);

	/**
	 * Find previous reference poi snapshot remote.
	 *
	 * @param referencePOIID the reference poiid
	 * @return the reference poi snapshot dto
	 */
	ReferencePOISnapshotDTO findPreviousReferencePOISnapshotRemote(
			Long referencePOIID);

	/**
	 * Creates the reference poi snapshot remote.
	 *
	 * @param snapshotDTO the snapshot dto
	 * @param userID the user id
	 * @return the reference poi snapshot dto
	 */
	ReferencePOISnapshotDTO createReferencePOISnapshotRemote(
			ReferencePOISnapshotDTO snapshotDTO, Long userID);

	/**
	 * Update reference poi snapshot remote.
	 *
	 * @param referencePOISnapshotDTO the reference poi snapshot dto
	 * @param userID the user id
	 * @return the reference poi snapshot dto
	 */
	ReferencePOISnapshotDTO updateReferencePOISnapshotRemote(
			ReferencePOISnapshotDTO referencePOISnapshotDTO, Long userID);

	/**
	 * Delete reference poi snapshot remote.
	 *
	 * @param referencePOISnapshotId the reference poi snapshot id
	 * @param userID the user id
	 */
	void deleteReferencePOISnapshotRemote(Long referencePOISnapshotId,
			Long userID);

	/**
	 * Find all reference poi snapshots by reference poi remote.
	 *
	 * @param referencePOIDTO the reference poidto
	 * @param resultLimit the result limit
	 * @return the list
	 * @throws ClassNotFoundException the class not found exception
	 */
	List<ReferencePOISnapshotDTO> findAllReferencePOISnapshotsByReferencePOIRemote(
			Long referencePOIDTO, int resultLimit)
			throws ClassNotFoundException;


	/**
	 * Find meocs by crisis remote.
	 *
	 * @param crisisContextID the crisis context id
	 * @return the list
	 */
	public List<OCMeocDTO> findMeocsByCrisisRemote(Long crisisContextID);

	/**
	 * Find eoc by crisis remote.
	 *
	 * @param crisisContextID the crisis context id
	 * @return the operations centre dto
	 */
	public OperationsCentreDTO findEocByCrisisRemote(Long crisisContextID);

	/**
	 * Find meoc supervisor remote.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the actor dto
	 */
	public ActorICDTO findMeocSupervisorRemote(Long operationsCentreID);

	/**
	 * Find eoc supervisor remote.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the actor dto
	 */
	public ActorCMDTO findEocSupervisorRemote(Long operationsCentreID);

	
	/**
	 * Find user operations centre remote.
	 *
	 * @param userID the user id
	 * @return the Operations Centre
	 */
	public Object findUserOperationsCentreRemote(Long userID);

	/**
	 * Find meoc by title remote.
	 *
	 * @param title the title
	 * @return the oC meoc dto
	 */
	public OCMeocDTO findMeocByTitleRemote(String title);

	/**
	 * Find eoc by title remote.
	 *
	 * @param title the title
	 * @return the oC eoc dto
	 */
	public OCEocDTO findEocByTitleRemote(String title);

	/**
	 * Update eoc remote.
	 *
	 * @param operationsCentreDTO the operations centre dto
	 * @param userID the user id
	 * @return the oC eoc dto
	 */
	public OCEocDTO updateEOCRemote(OCEocDTO operationsCentreDTO, Long userID);

	/**
	 * Update meoc remote.
	 *
	 * @param operationsCentreDTO the operations centre dto
	 * @param userID the user id
	 * @return the oC meoc dto
	 */
	public OCMeocDTO updateMEOCRemote(OCMeocDTO operationsCentreDTO, Long userID);

	/**
	 * Find oc by id remote.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the object
	 * @throws ClassNotFoundException the class not found exception
	 */
	public Object findOCByIdRemote(Long operationsCentreID) throws ClassNotFoundException;

	/**
	 * Find operations centre class by id remote.
	 *
	 * @param ocID the oc id
	 * @return the string
	 */
	public String findOperationsCentreClassByIdRemote(Long ocID);

	/**
	 * Find eoc by id remote.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the oC eoc dto
	 */
	public OCEocDTO findEocByIdRemote(Long operationsCentreID);

	/**
	 * Find meoc by id remote.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the oC meoc dto
	 */
	public OCMeocDTO findMeocByIdRemote(Long operationsCentreID);

	/**
	 * Find eoc idby meoc id remote.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the long
	 */
	public Long findEocIDBYMeocIDRemote(Long operationsCentreID);

	/**
	 * Find operations centre snapshot by id remote.
	 *
	 * @param operationsCentreID the operations centre id
	 * @return the operations centre snapshot dto
	 */
	public OperationsCentreSnapshotDTO findOperationsCentreSnapshotByIdRemote(
			Long operationsCentreID);
	

}
