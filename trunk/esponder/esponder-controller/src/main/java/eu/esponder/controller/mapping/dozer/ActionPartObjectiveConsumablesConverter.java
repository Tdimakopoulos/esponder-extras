/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.LogisticsService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.resource.ConsumableResource;
import eu.esponder.util.ejb.ServiceLocator;
// TODO: Auto-generated Javadoc
// 
//import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;

// 
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class ActionPartObjectiveConsumablesConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected LogisticsService getLogisticsService() {
		try {
			return ServiceLocator.getResource("esponder/LogisticsBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				if(it.next().getClass() == ConsumableResource.class) {

					Set<ConsumableResource> sourceConsumablesSet = (Set<ConsumableResource>) source;
					Set<Long> destConsumablesDTOSet = new HashSet<Long>();
					for(ConsumableResource cResource : sourceConsumablesSet) {
						destConsumablesDTOSet.add(cResource.getId());
					}
					destination = destConsumablesDTOSet;
				}
				else
					if(it.next().getClass() == Long.class) {

						Set<Long> sourceConsumablesDTOSet = (Set<Long>) source;
						Set<ConsumableResource> destConsumablesSet = new HashSet<ConsumableResource>();
						for(Long cResourceID : sourceConsumablesDTOSet) {
							ConsumableResource destResource = this.getLogisticsService().findConsumableResourceById(cResourceID);
							destConsumablesSet.add(destResource);
						}
						destination = destConsumablesSet;
					}
					else
						destination = null;
			}
		}
		else {
			//new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}
