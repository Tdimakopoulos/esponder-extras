/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.generic.GenericService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.snapshot.location.CubeVolumeDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.snapshot.location.CubeVolume;
import eu.esponder.model.snapshot.location.Sphere;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class LocationAreaDTOCustomConverter.
 */
public class LocationAreaOCSnapshotConverter implements CustomConverter {


	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the generic service.
	 *
	 * @return the generic service
	 */
	protected GenericService getGenericService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}


	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	public Object convert(Object destination,
			Object source, Class<?> destinationClass,
			Class<?> sourceClass) {

		if (source instanceof Sphere && source != null) {
				return destination = this.getMappingService().mapObject(source, SphereDTO.class);
		}
		else if (source instanceof CubeVolume && source != null) {
			return destination = this.getMappingService().mapObject(source, CubeVolumeDTO.class);
	}
		else 
			if (source instanceof SphereDTO && source != null) {
				Long laID = ((SphereDTO)source).getId();
				destination = this.getGenericService().getEntity(Sphere.class, laID);
				return destination;
			}
			else if (source instanceof CubeVolumeDTO && source != null) {
				Long laID = ((CubeVolumeDTO)source).getId();
				destination = this.getGenericService().getEntity(CubeVolume.class, laID);
				return destination;
			}

		return null;
	}

}
