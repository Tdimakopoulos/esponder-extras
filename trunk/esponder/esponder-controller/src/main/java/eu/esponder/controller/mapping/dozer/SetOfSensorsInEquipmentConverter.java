/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.SensorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.resource.sensor.Sensor;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class SetOfSensorsInEquipmentConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected SensorService getSensorService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				Object next = it.next();
				if(Sensor.class.isInstance(next)) {

					Set<Sensor> sourceSensorSet = (Set<Sensor>) source;
					Set<Long> destSensorDTOSet = new HashSet<Long>();
					for(Sensor sensor : sourceSensorSet) {
						destSensorDTOSet.add(sensor.getId());
					}
					destination = destSensorDTOSet;
				}
				else
					if(next.getClass() == Long.class) {

						Set<Long> sourceSensorDTOSet = (Set<Long>) source;
						Set<Sensor> destSensorSet = new HashSet<Sensor>();
						for(Long sensorID : sourceSensorDTOSet) {
							Sensor destSensor = this.getSensorService().findSensorById(sensorID);
							destSensorSet.add(destSensor);
						}
						destination = destSensorSet;
					}
					else
						destination = null;
			}
		}
		else {
			 }
		return destination;
	}

}
