/*
 * 
 */
package eu.esponder.controller.persistence;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;
import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;

import eu.esponder.controller.persistence.criteria.EsponderQueryRestriction;
import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface CrudRemoteService.
 *
 * @param <T> the generic type
 */
@Remote
public interface CrudRemoteService<T>{

	/**
	 * Creates the.
	 *
	 * @param t the t
	 * @return the t
	 */
	public T create(T t);
    
	/**
	 * Find.
	 *
	 * @param type the type
	 * @param id the id
	 * @return the t
	 */
	public T find(Class<T> type, Object id);
	
	/**
	 * Gets the reference.
	 *
	 * @param type the type
	 * @param id the id
	 * @return the reference
	 */
	public T getReference(Class<T> type, Object id);
    
	/**
	 * Update.
	 *
	 * @param t the t
	 * @return the t
	 */
	public T update(T t);
	
	/**
	 * Refresh.
	 *
	 * @param t the t
	 */
	public void refresh(T t);
    
	/**
	 * Delete.
	 *
	 * @param type the type
	 * @param id the id
	 */
	public void delete(Class<T> type, Object id);
	
    /**
     * Delete.
     *
     * @param entity the entity
     */
    public void delete(T entity);
    
    /**
     * Flush.
     */
    public void flush();

    /**
     * Find with named query.
     *
     * @param queryName the query name
     * @return the list
     */
    public List<T> findWithNamedQuery(String queryName);
    
    /**
     * Find with named query.
     *
     * @param queryName the query name
     * @param resultLimit the result limit
     * @return the list
     */
    public List<T> findWithNamedQuery(String queryName, int resultLimit);
    
    /**
     * Find with named query.
     *
     * @param namedQueryName the named query name
     * @param parameters the parameters
     * @return the list
     */
    public List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters);
    
    /**
     * Find with named query.
     *
     * @param namedQueryName the named query name
     * @param parameters the parameters
     * @param resultLimit the result limit
     * @return the list
     */
    public List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit);
    
    /**
     * Find with query.
     *
     * @param queryName the query name
     * @return the list
     */
    public List<T> findWithQuery(String queryName);
    
    /**
     * Find with query.
     *
     * @param queryName the query name
     * @param resultLimit the result limit
     * @return the list
     */
    public List<T> findWithQuery(String queryName, int resultLimit);
    
    /**
     * Find with query.
     *
     * @param namedQueryName the named query name
     * @param parameters the parameters
     * @return the list
     */
    public List<T> findWithQuery(String namedQueryName, Map<String, Object> parameters);
    
    /**
     * Find with query.
     *
     * @param namedQueryName the named query name
     * @param parameters the parameters
     * @param resultLimit the result limit
     * @return the list
     */
    public List<T> findWithQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit);

    /**
     * Find with criteria query.
     *
     * @param cls the cls
     * @param criteria the criteria
     * @param pageSize the page size
     * @param pageNumber the page number
     * @return the list
     */
    @SuppressWarnings("rawtypes")
	public List<T> findWithCriteriaQuery(Class<? extends ESponderEntity> cls, EsponderQueryRestriction criteria, int pageSize, int pageNumber);
    
    /**
     * Find single with named query.
     *
     * @param namedQueryName the named query name
     * @param parameters the parameters
     * @return the object
     */
    public Object findSingleWithNamedQuery(String namedQueryName, Map<String,Object> parameters);
	
	/**
	 * Gets the entity manager.
	 *
	 * @return the entity manager
	 */
	public EntityManager getEntityManager();
	
}
