/*
 * 
 */
package eu.esponder.controller.events;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.events.entity.OsgiEventsEntityDTO;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface EventsEntityRemoteService.
 */
@Remote
public interface EventsEntityRemoteService {

	/**
	 * Find osgi events entity by id remote.
	 *
	 * @param osgiEventsEntityID the osgi events entity id
	 * @return the osgi events entity dto
	 */
	public OsgiEventsEntityDTO findOsgiEventsEntityByIdRemote(Long osgiEventsEntityID);

	/**
	 * Find all osgi events entities remote.
	 *
	 * @return the list
	 */
	public List<OsgiEventsEntityDTO> findAllOsgiEventsEntitiesRemote();

	/**
	 * Find osgi events entities by severity remote.
	 *
	 * @param severity the severity
	 * @return the list
	 */
	public List<OsgiEventsEntityDTO> findOsgiEventsEntitiesBySeverityRemote(
			String severity);

	/**
	 * Find osgi events entities by source id remote.
	 *
	 * @param sourceId the source id
	 * @return the list
	 */
	public List<OsgiEventsEntityDTO> findOsgiEventsEntitiesBySourceIdRemote(
			Long sourceId);

	/**
	 * Creates the osgi events entity remote.
	 *
	 * @param osgiEventsEntityDTO the osgi events entity dto
	 * @param userID the user id
	 * @return the osgi events entity dto
	 */
	public OsgiEventsEntityDTO createOsgiEventsEntityRemote(
			OsgiEventsEntityDTO osgiEventsEntityDTO, Long userID);

	/**
	 * Delete osgi events entity remote.
	 *
	 * @param osgiEventsEntityID the osgi events entity id
	 * @param userID the user id
	 */
	public void deleteOsgiEventsEntityRemote(Long osgiEventsEntityID, Long userID);

	/**
	 * Update osgi events entity remote.
	 *
	 * @param osgiEventsEntityDTO the osgi events entity dto
	 * @param userID the user id
	 * @return the osgi events entity dto
	 */
	public OsgiEventsEntityDTO updateOsgiEventsEntityRemote(
			OsgiEventsEntityDTO osgiEventsEntityDTO, Long userID);
	

}
