/*
 * 
 */
package eu.esponder.controller.crisis.resource;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.Organisation;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface OrganisationService.
 */
@Local
public interface OrganisationService {
	
	/**
	 * Find by id.
	 *
	 * @param organisationID the organisation id
	 * @return the organisation
	 */
	public Organisation findById(Long organisationID);
	
	/**
	 * Find by title.
	 *
	 * @param organisationTitle the organisation title
	 * @return the organisation
	 */
	public Organisation findByTitle(String organisationTitle);

	/**
	 * Creates the organisation.
	 *
	 * @param organisation the organisation
	 * @return the organisation
	 */
	public Organisation createOrganisation(Organisation organisation);

	/**
	 * Update organisation.
	 *
	 * @param organisation the organisation
	 * @return the organisation
	 */
	public Organisation updateOrganisation(Organisation organisation);

	/**
	 * Delete organisation.
	 *
	 * @param organisation the organisation
	 */
	public void deleteOrganisation(Organisation organisation);
	
	/**
	 * Delete organisation by id.
	 *
	 * @param organisationID the organisation id
	 */
	public void deleteOrganisationByID(Long organisationID);

	/**
	 * Find allorganisations.
	 *
	 * @return the list
	 */
	public List<Organisation> findAllorganisations();

}
