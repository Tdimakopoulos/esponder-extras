/*
 * 
 */
package eu.esponder.controller.datafusion;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.model.datafusion.DatafusionResults;

// TODO: Auto-generated Javadoc
/**
 * The Interface DatafusionService.
 */
@Local
public interface DatafusionService extends DatafusionRemoteService{

	/**
	 * Find df resutls by id.
	 *
	 * @param DFResultsID the dF results id
	 * @return the datafusion results
	 */
	public DatafusionResults findDFResutlsById(Long DFResultsID);

	/**
	 * Find df results by rulename.
	 *
	 * @param Rulename the rulename
	 * @return the datafusion results
	 */
	public DatafusionResults findDFResultsByRulename(String Rulename);

	/**
	 * Creates the df results.
	 *
	 * @param DFResults the dF results
	 * @param userID the user id
	 * @return the datafusion results
	 */
	public DatafusionResults createDFResults(DatafusionResults DFResults, Long userID);
	
	/**
	 * Find all df results.
	 *
	 * @return the list
	 */
	public List<DatafusionResults> findAllDFResults();


}


