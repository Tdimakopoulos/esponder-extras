/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.TypeService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.type.SensorType;
import eu.esponder.util.ejb.ServiceLocator;


// TODO: Auto-generated Javadoc
// 
/**
 * The Class SensorTypeFieldConverter.
 */
public class SensorTypeFieldConverter implements CustomConverter {

	/**
	 * Gets the type service.
	 *
	 * @return the type service
	 */
	protected TypeService getTypeService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert(Object destination, 
			Object source, 
			Class<?> destinationClass,
			Class<?> sourceClass) {

		if (sourceClass == String.class && source != null) {
			String type = (String) source;
			SensorType sensorType = (SensorType) getTypeService().findByTitle(type);
			destination = sensorType;
		}
		else if(SensorType.class.isAssignableFrom(sourceClass)) {
			SensorType sensorType = (SensorType) source;
			destination = (String) sensorType.getTitle();
		}
		else {
			//new EsponderCheckedException(this.getClass(),"Object Conversion Error ");
		}
		return destination;
	}

}