/*
 * 
 */
package eu.esponder.model.user;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.crisis.resource.Actor;
import eu.esponder.model.crisis.resource.OperationsCentre;
import eu.esponder.model.crisis.resource.Resource;
import eu.esponder.model.crisis.view.VoIPURL;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderUser. Entity that manage the esponderuser, esponder user is
 * system users, for portal, optimizer etc.
 */
@Entity
@Table(name = "esponderuser")
@NamedQueries({
		@NamedQuery(name = "ESponderUser.findAll", query = "select u from ESponderUser u"),
		@NamedQuery(name = "ESponderUser.findByTitle", query = "select u from ESponderUser u where u.title=:title"),
		@NamedQuery(name = "ESponderUser.findByPKI", query = "select u from ESponderUser u where u.pkikey=:pkikey"),
		@NamedQuery(name = "ESponderUser.findByUsername", query = "select u from ESponderUser u where u.username=:username"),
		@NamedQuery(name = "ESponderUser.findBySIPUsername", query = "select u from ESponderUser u where u.sIPusername=:sIPusername") })
public class ESponderUser extends Resource {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2512583407793541959L;

	/** The operations centre. */
	@ManyToOne
	@JoinColumn(name = "OC_ID")
	private OperationsCentre operationsCentre;

	/** The actor that represent the user. */
	private Long actorID;

	private int actorRole;

	private String fullName;

	public int getActorRole() {
		return actorRole;
	}

	public void setActorRole(int actorRole) {
		this.actorRole = actorRole;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	// Media variables
	private String sIPusername;
	private String sIPpassword;
	private String sIPaddressMEOC;
	private String sIPaddressEOC;
	private String sIPaddressFRC;
	private String sIPaddress;
	private String sIPaddresssrv;

	// webcam ip
	private String webcamip;

	// rasberry ip
	private String rasberryip;

	String IMEI;

	@Embedded
	private VoIPURL voIPURL;

	String pkikey;
	String username;
	String password;
	String securityRole;

	public String getSecurityRole() {
		return securityRole;
	}

	public void setSecurityRole(String securityRole) {
		this.securityRole = securityRole;
	}

	public String getPkikey() {
		return pkikey;
	}

	public void setPkikey(String pkikey) {
		this.pkikey = pkikey;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public VoIPURL getVoIPURL() {
		return voIPURL;
	}

	public void setVoIPURL(VoIPURL voIPURL) {
		this.voIPURL = voIPURL;
	}

	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String iMEI) {
		IMEI = iMEI;
	}

	public String getsIPusername() {
		return sIPusername;
	}

	public void setsIPusername(String sIPusername) {
		this.sIPusername = sIPusername;
	}

	public String getsIPpassword() {
		return sIPpassword;
	}

	public void setsIPpassword(String sIPpassword) {
		this.sIPpassword = sIPpassword;
	}

	public String getsIPaddressMEOC() {
		return sIPaddressMEOC;
	}

	public void setsIPaddressMEOC(String sIPaddressMEOC) {
		this.sIPaddressMEOC = sIPaddressMEOC;
	}

	public String getsIPaddressEOC() {
		return sIPaddressEOC;
	}

	public void setsIPaddressEOC(String sIPaddressEOC) {
		this.sIPaddressEOC = sIPaddressEOC;
	}

	public String getsIPaddressFRC() {
		return sIPaddressFRC;
	}

	public void setsIPaddressFRC(String sIPaddressFRC) {
		this.sIPaddressFRC = sIPaddressFRC;
	}

	public String getsIPaddress() {
		return sIPaddress;
	}

	public void setsIPaddress(String sIPaddress) {
		this.sIPaddress = sIPaddress;
	}

	public String getsIPaddresssrv() {
		return sIPaddresssrv;
	}

	public void setsIPaddresssrv(String sIPaddresssrv) {
		this.sIPaddresssrv = sIPaddresssrv;
	}

	public String getWebcamip() {
		return webcamip;
	}

	public void setWebcamip(String webcamip) {
		this.webcamip = webcamip;
	}

	public String getRasberryip() {
		return rasberryip;
	}

	public void setRasberryip(String rasberryip) {
		this.rasberryip = rasberryip;
	}

	/**
	 * Gets the actor id.
	 * 
	 * @return the actor id
	 */
	public Long getActorID() {
		return actorID;
	}

	/**
	 * Sets the actor id.
	 * 
	 * @param actorID
	 *            the new actor id
	 */
	public void setActorID(Long actorID) {
		this.actorID = actorID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// return "ESponderUser [id=" + id + ", userName=" + actorID + "]";
		return id.toString();
	}

	/**
	 * Gets the operations centre.
	 * 
	 * @return the operations centre
	 */
	public OperationsCentre getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 * 
	 * @param operationsCentre
	 *            the new operations centre
	 */
	public void setOperationsCentre(OperationsCentre operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

}
