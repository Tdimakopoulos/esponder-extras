/*
 * 
 */
package eu.esponder.model.crisis.action;



// TODO: Auto-generated Javadoc
/**
 * The Enum ActionOperationEnum.
 * This class is Enum and define the type of Action Operations
 */
public enum CrisisContextStatusEnum {

	/** The move. */
	STARTUP,
	
	/** The transport. */
	INPROGRESS,
	
	/** The fix. */
	FINISHED
}
