/*
 * 
 */
package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class LocationSensor.
 */
@Entity
@DiscriminatorValue("LPS")
public class LPSSensor extends Sensor implements LocationMeasurementSensor {


	private static final long serialVersionUID = 8566645257878941438L;

	Double dFRxmetersrefbase;
	Double dFRymetersrefbase;
	Double dFRzmetersrefbase;
	Double dFRxmetersglobal;
	Double dFRymetersglobal;
	Double dFRzmetersglobal;
	
	Double dfirstRefBaseStationLat;
	Double dfirstRefBaseStationLon;
	Double dfirstRefBaseStationAlt;
	Double dsecondRefBaseStationLat;
	Double dsecondRefBaseStationLon;
	Double dsecondRefBaseStationAlt;
	
	Double dDataV1;
	Double dDataV2;
	Double dDataV3;
	
	Double dDeployedLat;
	Double dDeployedLon;
	Double dDeployedAlt;
	
	boolean bDeployed;
	Long responsibleActoID;
	public Double getdFRxmetersrefbase() {
		return dFRxmetersrefbase;
	}
	public void setdFRxmetersrefbase(Double dFRxmetersrefbase) {
		this.dFRxmetersrefbase = dFRxmetersrefbase;
	}
	public Double getdFRymetersrefbase() {
		return dFRymetersrefbase;
	}
	public void setdFRymetersrefbase(Double dFRymetersrefbase) {
		this.dFRymetersrefbase = dFRymetersrefbase;
	}
	public Double getdFRzmetersrefbase() {
		return dFRzmetersrefbase;
	}
	public void setdFRzmetersrefbase(Double dFRzmetersrefbase) {
		this.dFRzmetersrefbase = dFRzmetersrefbase;
	}
	public Double getdFRxmetersglobal() {
		return dFRxmetersglobal;
	}
	public void setdFRxmetersglobal(Double dFRxmetersglobal) {
		this.dFRxmetersglobal = dFRxmetersglobal;
	}
	public Double getdFRymetersglobal() {
		return dFRymetersglobal;
	}
	public void setdFRymetersglobal(Double dFRymetersglobal) {
		this.dFRymetersglobal = dFRymetersglobal;
	}
	public Double getdFRzmetersglobal() {
		return dFRzmetersglobal;
	}
	public void setdFRzmetersglobal(Double dFRzmetersglobal) {
		this.dFRzmetersglobal = dFRzmetersglobal;
	}
	public Double getDfirstRefBaseStationLat() {
		return dfirstRefBaseStationLat;
	}
	public void setDfirstRefBaseStationLat(Double dfirstRefBaseStationLat) {
		this.dfirstRefBaseStationLat = dfirstRefBaseStationLat;
	}
	public Double getDfirstRefBaseStationLon() {
		return dfirstRefBaseStationLon;
	}
	public void setDfirstRefBaseStationLon(Double dfirstRefBaseStationLon) {
		this.dfirstRefBaseStationLon = dfirstRefBaseStationLon;
	}
	public Double getDfirstRefBaseStationAlt() {
		return dfirstRefBaseStationAlt;
	}
	public void setDfirstRefBaseStationAlt(Double dfirstRefBaseStationAlt) {
		this.dfirstRefBaseStationAlt = dfirstRefBaseStationAlt;
	}
	public Double getDsecondRefBaseStationLat() {
		return dsecondRefBaseStationLat;
	}
	public void setDsecondRefBaseStationLat(Double dsecondRefBaseStationLat) {
		this.dsecondRefBaseStationLat = dsecondRefBaseStationLat;
	}
	public Double getDsecondRefBaseStationLon() {
		return dsecondRefBaseStationLon;
	}
	public void setDsecondRefBaseStationLon(Double dsecondRefBaseStationLon) {
		this.dsecondRefBaseStationLon = dsecondRefBaseStationLon;
	}
	public Double getDsecondRefBaseStationAlt() {
		return dsecondRefBaseStationAlt;
	}
	public void setDsecondRefBaseStationAlt(Double dsecondRefBaseStationAlt) {
		this.dsecondRefBaseStationAlt = dsecondRefBaseStationAlt;
	}
	public Double getdDataV1() {
		return dDataV1;
	}
	public void setdDataV1(Double dDataV1) {
		this.dDataV1 = dDataV1;
	}
	public Double getdDataV2() {
		return dDataV2;
	}
	public void setdDataV2(Double dDataV2) {
		this.dDataV2 = dDataV2;
	}
	public Double getdDataV3() {
		return dDataV3;
	}
	public void setdDataV3(Double dDataV3) {
		this.dDataV3 = dDataV3;
	}
	public Double getdDeployedLat() {
		return dDeployedLat;
	}
	public void setdDeployedLat(Double dDeployedLat) {
		this.dDeployedLat = dDeployedLat;
	}
	public Double getdDeployedLon() {
		return dDeployedLon;
	}
	public void setdDeployedLon(Double dDeployedLon) {
		this.dDeployedLon = dDeployedLon;
	}
	public Double getdDeployedAlt() {
		return dDeployedAlt;
	}
	public void setdDeployedAlt(Double dDeployedAlt) {
		this.dDeployedAlt = dDeployedAlt;
	}
	public boolean isbDeployed() {
		return bDeployed;
	}
	public void setbDeployed(boolean bDeployed) {
		this.bDeployed = bDeployed;
	}
	public Long getResponsibleActoID() {
		return responsibleActoID;
	}
	public void setResponsibleActoID(Long responsibleActoID) {
		this.responsibleActoID = responsibleActoID;
	}
	
}
