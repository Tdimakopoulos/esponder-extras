/*
 * 
 */
package eu.esponder.model.crisis.resource.category;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.type.ReusableResourceType;

// TODO: Auto-generated Javadoc
/**
 * The Class ReusableResourceCategory. Manage information associated with the
 * Reusable Resource Category
 */
@Entity
@Table(name = "reusables_category")
@NamedQueries({ @NamedQuery(name = "ReusableResourceCategory.findByType", query = "select a from ReusableResourceCategory a where a.reusableResourceType=:reusableResourceType") })
public class ReusableResourceCategory extends LogisticsCategory {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 145276614065666955L;

	/** The reusable resource type. */
	@OneToOne
	@JoinColumn(name = "REUSABLE_RESOURCE_TYPE_ID", nullable = false)
	private ReusableResourceType reusableResourceType;

	/**
	 * Gets the reusable resource type.
	 * 
	 * @return the reusable resource type
	 */
	public ReusableResourceType getReusableResourceType() {
		return reusableResourceType;
	}

	/**
	 * Sets the reusable resource type.
	 * 
	 * @param reusableResourceType
	 *            the new reusable resource type
	 */
	public void setReusableResourceType(
			ReusableResourceType reusableResourceType) {
		this.reusableResourceType = reusableResourceType;
	}

}
