/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.snapshot.location.LocationArea;

// TODO: Auto-generated Javadoc
/**
 * The Class OCEoc. Entity class that manage the Operations Centers EOC
 */
@Entity
@DiscriminatorValue("EOC")
@NamedQueries({
		@NamedQuery(name = "OCEoc.findByTitle", query = "select c from OCEoc c where c.title=:title"),
		@NamedQuery(name = "OCEoc.findAll", query = "select c from OCEoc c")

})
public class OCEoc extends OperationsCentre {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7620437338448445871L;

	/** The crisis manager. */
	@OneToOne
	@JoinColumn(name = "CRISIS_MANAGER")
	private ActorCM crisisManager;

	/** The subordinate meo cs. */
	@OneToMany(mappedBy = "supervisingOC")
	private Set<OCMeoc> subordinateMEOCs;

	/** The eoc crisis context. */
	@ManyToOne
	@JoinColumn(name = "CRISIS_CONTEXT_FOR_EOC")
	private CrisisContext eocCrisisContext;

	@OneToOne
	@JoinColumn(name = "LOCATION_AREA_ID")
	protected LocationArea locationArea;

	// extras
	private String dlnaip;
	private String dlnaprop1;
	private String dlnaprop2;
	private String dlnaprop3;
	private String dlnaprop4;
	private String sIPAddress;
	private String netip;
	private String satip;
	private String wimaxip;
	private String wifiip;
	private String meoc3gip;

	private String sIPusername;
	private String sIPpassword;

	private String sIPaddresssrv;
	private String sIPaddresscli;
	
	private String sIPusername2;
	private String sIPpassword2;

	private String sIPaddresssrv2;
	private String sIPaddresscli2;
	
	private String sIPusername3;
	private String sIPpassword3;

	private String sIPaddresssrv3;
	private String sIPaddresscli3;

	
	public String getsIPusername2() {
		return sIPusername2;
	}

	public void setsIPusername2(String sIPusername2) {
		this.sIPusername2 = sIPusername2;
	}

	public String getsIPpassword2() {
		return sIPpassword2;
	}

	public void setsIPpassword2(String sIPpassword2) {
		this.sIPpassword2 = sIPpassword2;
	}

	public String getsIPaddresssrv2() {
		return sIPaddresssrv2;
	}

	public void setsIPaddresssrv2(String sIPaddresssrv2) {
		this.sIPaddresssrv2 = sIPaddresssrv2;
	}

	public String getsIPaddresscli2() {
		return sIPaddresscli2;
	}

	public void setsIPaddresscli2(String sIPaddresscli2) {
		this.sIPaddresscli2 = sIPaddresscli2;
	}

	public String getsIPusername3() {
		return sIPusername3;
	}

	public void setsIPusername3(String sIPusername3) {
		this.sIPusername3 = sIPusername3;
	}

	public String getsIPpassword3() {
		return sIPpassword3;
	}

	public void setsIPpassword3(String sIPpassword3) {
		this.sIPpassword3 = sIPpassword3;
	}

	public String getsIPaddresssrv3() {
		return sIPaddresssrv3;
	}

	public void setsIPaddresssrv3(String sIPaddresssrv3) {
		this.sIPaddresssrv3 = sIPaddresssrv3;
	}

	public String getsIPaddresscli3() {
		return sIPaddresscli3;
	}

	public void setsIPaddresscli3(String sIPaddresscli3) {
		this.sIPaddresscli3 = sIPaddresscli3;
	}

	public String getDlnaip() {
		return dlnaip;
	}

	public void setDlnaip(String dlnaip) {
		this.dlnaip = dlnaip;
	}

	public String getDlnaprop1() {
		return dlnaprop1;
	}

	public void setDlnaprop1(String dlnaprop1) {
		this.dlnaprop1 = dlnaprop1;
	}

	public String getDlnaprop2() {
		return dlnaprop2;
	}

	public void setDlnaprop2(String dlnaprop2) {
		this.dlnaprop2 = dlnaprop2;
	}

	public String getDlnaprop3() {
		return dlnaprop3;
	}

	public void setDlnaprop3(String dlnaprop3) {
		this.dlnaprop3 = dlnaprop3;
	}

	public String getDlnaprop4() {
		return dlnaprop4;
	}

	public void setDlnaprop4(String dlnaprop4) {
		this.dlnaprop4 = dlnaprop4;
	}

	public String getsIPAddress() {
		return sIPAddress;
	}

	public void setsIPAddress(String sIPAddress) {
		this.sIPAddress = sIPAddress;
	}

	public String getNetip() {
		return netip;
	}

	public void setNetip(String netip) {
		this.netip = netip;
	}

	public String getSatip() {
		return satip;
	}

	public void setSatip(String satip) {
		this.satip = satip;
	}

	public String getWimaxip() {
		return wimaxip;
	}

	public void setWimaxip(String wimaxip) {
		this.wimaxip = wimaxip;
	}

	public String getWifiip() {
		return wifiip;
	}

	public void setWifiip(String wifiip) {
		this.wifiip = wifiip;
	}

	public String getMeoc3gip() {
		return meoc3gip;
	}

	public void setMeoc3gip(String meoc3gip) {
		this.meoc3gip = meoc3gip;
	}

	public String getsIPusername() {
		return sIPusername;
	}

	public void setsIPusername(String sIPusername) {
		this.sIPusername = sIPusername;
	}

	public String getsIPpassword() {
		return sIPpassword;
	}

	public void setsIPpassword(String sIPpassword) {
		this.sIPpassword = sIPpassword;
	}

	public String getsIPaddresssrv() {
		return sIPaddresssrv;
	}

	public void setsIPaddresssrv(String sIPaddresssrv) {
		this.sIPaddresssrv = sIPaddresssrv;
	}

	public String getsIPaddresscli() {
		return sIPaddresscli;
	}

	public void setsIPaddresscli(String sIPaddresscli) {
		this.sIPaddresscli = sIPaddresscli;
	}

	public LocationArea getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationArea locationArea) {
		this.locationArea = locationArea;
	}

	/**
	 * Gets the crisis manager.
	 * 
	 * @return the crisis manager
	 */
	public ActorCM getCrisisManager() {
		return crisisManager;
	}

	/**
	 * Sets the crisis manager.
	 * 
	 * @param crisisManager
	 *            the new crisis manager
	 */
	public void setCrisisManager(ActorCM crisisManager) {
		this.crisisManager = crisisManager;
	}

	/**
	 * Gets the subordinate meo cs.
	 * 
	 * @return the subordinate meo cs
	 */
	public Set<OCMeoc> getSubordinateMEOCs() {
		return subordinateMEOCs;
	}

	/**
	 * Sets the subordinate meo cs.
	 * 
	 * @param subordinateMEOCs
	 *            the new subordinate meo cs
	 */
	public void setSubordinateMEOCs(Set<OCMeoc> subordinateMEOCs) {
		this.subordinateMEOCs = subordinateMEOCs;
	}

	/**
	 * Gets the eoc crisis context.
	 * 
	 * @return the eoc crisis context
	 */
	public CrisisContext getEocCrisisContext() {
		return eocCrisisContext;
	}

	/**
	 * Sets the eoc crisis context.
	 * 
	 * @param eocCrisisContext
	 *            the new eoc crisis context
	 */
	public void setEocCrisisContext(CrisisContext eocCrisisContext) {
		this.eocCrisisContext = eocCrisisContext;
	}

}
