/*
 * 
 */
package eu.esponder.model.crisis;


// TODO: Auto-generated Javadoc
// 
/**
 * The Enum CrisisContextAlertEnum.
 * Manage the level of alerts for each Crisis Context - Crisis Incident
 */
public enum CrisisContextAlertEnum {
	
	/** The Level1. */
	Level1,
	
	/** The Level2. */
	Level2,
	
	/** The Level3. */
	Level3,
	
	/** The Level4. */
	Level4,
	
	/** The Level5. */
	Level5
}
