/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


// TODO: Auto-generated Javadoc
/**
 * The Class Actor First Responder Cheif.
 */
@Entity
@DiscriminatorValue("FRC")
@NamedQueries({
	@NamedQuery(name="ActorFRC.findByTitle", query="select a from ActorFRC a where a.title=:title"),
	@NamedQuery(name="ActorFRC.findAll", query="select a from ActorFRC a")
})
public class ActorFRC extends FirstResponderActor {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -79921366204329899L;
	
	/** The team. */
	@OneToOne(mappedBy="frchief")
	private FRTeam team;
	
	/** The subordinates. */
	@OneToMany(mappedBy="FRChief")
	private Set<ActorFR> subordinates;

	/**
	 * Gets the team.
	 *
	 * @return the team
	 */
	public FRTeam getTeam() {
		return team;
	}

	/**
	 * Sets the team.
	 *
	 * @param team the new team
	 */
	public void setTeam(FRTeam team) {
		this.team = team;
	}

	/**
	 * Gets the subordinates.
	 *
	 * @return the subordinates
	 */
	public Set<ActorFR> getSubordinates() {
		return subordinates;
	}

	/**
	 * Sets the subordinates.
	 *
	 * @param subordinates the new subordinates
	 */
	public void setSubordinates(Set<ActorFR> subordinates) {
		this.subordinates = subordinates;
	}
	
}
