/*
 * 
 */
package eu.esponder.session;


import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import eu.esponder.model.crisis.resource.Resource;


// TODO: Auto-generated Javadoc
/**
 * The Class ESponderUser. Entity that manage the esponderuser, esponder user is
 * system users, for portal, optimizer etc.
 */
@Entity
@Table(name="espondersession")
@NamedQueries({
		@NamedQuery(name = "ESponderSession.findAll", query = "select u from ESponderSession u"),
		@NamedQuery(name = "ESponderSession.findByTitle", query = "select u from ESponderSession u where u.title=:title"),
		@NamedQuery(name = "ESponderSession.findByPKI", query = "select u from ESponderSession u where u.pkikey=:pkikey"),
		@NamedQuery(name = "ESponderSession.findByUserID", query = "select u from ESponderSession u where u.esponderuserID=:esponderuserID"),
		@NamedQuery(name = "ESponderSession.findBySessionID", query = "select u from ESponderSession u where u.sessionID=:sessionID"),
		@NamedQuery(name = "ESponderSession.findByUsername", query = "select u from ESponderSession u where u.username=:username")})
public class ESponderSession extends Resource {

	private static final long serialVersionUID = -3112216622362212071L;

	Long esponderuserID;

	String IMEI;

	String pkikey;
	
	String username;
	
	String sessionID;
	
	String userip;
	
	String lastActiveDate;
	
	String loginDate;
	
	LoginType sType;
	
	
	
	public LoginType getsType() {
		return sType;
	}



	public void setsType(LoginType sType) {
		this.sType = sType;
	}



	public Long getEsponderuserID() {
		return esponderuserID;
	}



	public void setEsponderuserID(Long esponderuserID) {
		this.esponderuserID = esponderuserID;
	}



	public String getIMEI() {
		return IMEI;
	}



	public void setIMEI(String iMEI) {
		IMEI = iMEI;
	}



	public String getPkikey() {
		return pkikey;
	}



	public void setPkikey(String pkikey) {
		this.pkikey = pkikey;
	}



	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}



	public String getSessionID() {
		return sessionID;
	}



	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}



	public String getUserip() {
		return userip;
	}



	public void setUserip(String userip) {
		this.userip = userip;
	}



	public String getLastActiveDate() {
		return lastActiveDate;
	}



	public void setLastActiveDate(String lastActiveDate) {
		this.lastActiveDate = lastActiveDate;
	}



	public String getLoginDate() {
		return loginDate;
	}



	public void setLoginDate(String loginDate) {
		this.loginDate = loginDate;
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id.toString();
	}


	

}
