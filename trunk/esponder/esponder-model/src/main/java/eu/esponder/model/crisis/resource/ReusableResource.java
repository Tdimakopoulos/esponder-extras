/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.action.ActionPart;
import eu.esponder.model.crisis.resource.category.ReusableResourceCategory;
import eu.esponder.model.type.VehicleEquipmentType;
import eu.esponder.model.type.VehiclePurposeType;
import eu.esponder.model.type.VehicleType;



// TODO: Auto-generated Javadoc
/**
 * The Class ReusableResource.
 * Entity class that manage information about the Reusable Resource
 */
@Entity
@Table(name="reusable_resource")
@NamedQueries({
	@NamedQuery(name="ReusableResource.findByTitle", query="select rr from ReusableResource rr where rr.title=:title"),
	@NamedQuery(name="ReusableResource.findAll", query="select rr from ReusableResource rr")
})
public class ReusableResource extends LogisticsResource {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8161494840654388942L;

	/** The quantity. */
	@Column(name="QUANTITY")
	private BigDecimal quantity;
	
	/** The consumables. */
	@OneToMany(mappedBy="container")
	private Set<ConsumableResource> consumables;
	
	/** The parent. */
	@ManyToOne
	@JoinColumn(name="PARENT_ID")
	private ReusableResource parent;
	
	/** The children. */
	@OneToMany(mappedBy="parent")
	private Set<ReusableResource> children;
	
	/** The action part. */
	@ManyToOne
	@JoinColumn(name="ACTION_PART_ID")
	private ActionPart actionPart;
	
	/** The reusable resource category. */
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private ReusableResourceCategory reusableResourceCategory;

	/** The crisis context. */
	@ManyToOne
	@JoinColumn(name="CRISIS_CONTEXT_ID")
	private CrisisContext crisisContext;
	
	
	@OneToMany(mappedBy="vehiclequipments")
	private Set<VehicleEquipmentType> vehicleEquipments;
	
	@OneToMany(mappedBy="vehiclepurposes")
	private Set<VehiclePurposeType> vehiclePurposes;
	
	@OneToOne
	@JoinColumn(name="ENTITY_TYPE_ID")
	private VehicleType vehicleType;
	
	
	public Set<VehicleEquipmentType> getVehicleEquipments() {
		return vehicleEquipments;
	}

	public void setVehicleEquipments(Set<VehicleEquipmentType> vehicleEquipments) {
		this.vehicleEquipments = vehicleEquipments;
	}

	public Set<VehiclePurposeType> getVehiclePurposes() {
		return vehiclePurposes;
	}

	public void setVehiclePurposes(Set<VehiclePurposeType> vehiclePurposes) {
		this.vehiclePurposes = vehiclePurposes;
	}

	public VehicleType getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(VehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}

	/**
	 * Gets the crisis context.
	 *
	 * @return the crisis context
	 */
	public CrisisContext getCrisisContext() {
		return crisisContext;
	}

	/**
	 * Sets the crisis context.
	 *
	 * @param crisisContext the new crisis context
	 */
	public void setCrisisContext(CrisisContext crisisContext) {
		this.crisisContext = crisisContext;
	}

	/**
	 * Gets the reusable resource category.
	 *
	 * @return the reusable resource category
	 */
	public ReusableResourceCategory getReusableResourceCategory() {
		return reusableResourceCategory;
	}

	/**
	 * Sets the reusable resource category.
	 *
	 * @param reusableResourceCategory the new reusable resource category
	 */
	public void setReusableResourceCategory(
			ReusableResourceCategory reusableResourceCategory) {
		this.reusableResourceCategory = reusableResourceCategory;
	}

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Gets the consumables.
	 *
	 * @return the consumables
	 */
	public Set<ConsumableResource> getConsumables() {
		return consumables;
	}

	/**
	 * Sets the consumables.
	 *
	 * @param consumables the new consumables
	 */
	public void setConsumables(Set<ConsumableResource> consumables) {
		this.consumables = consumables;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public ReusableResource getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(ReusableResource parent) {
		this.parent = parent;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public Set<ReusableResource> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the new children
	 */
	public void setChildren(Set<ReusableResource> children) {
		this.children = children;
	}

	/**
	 * Gets the action part.
	 *
	 * @return the action part
	 */
	public ActionPart getActionPart() {
		return actionPart;
	}

	/**
	 * Sets the action part.
	 *
	 * @param actionPart the new action part
	 */
	public void setActionPart(ActionPart actionPart) {
		this.actionPart = actionPart;
	}

}
