/*
 * 
 */
package eu.esponder.model.crisis.action;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;


// TODO: Auto-generated Javadoc
//
/**
 * The Class ActionPartSchedule.
 * Entity class that manage the schedule of Action part
 */
@Entity
@Table(name="actionpart_schedule")
public class ActionPartSchedule extends ESponderEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6440269024338454840L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACTIONPART_SCHEDULE_ID")
	private Long id;
	
	/** The title. Not Null,Unique*/
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	/** The action. */
	@ManyToOne
	@JoinColumn(name="ACTIONPART_ID", nullable=false)
	private ActionPart action;
	
	/** The prerequisite action. */
	@ManyToOne
	@JoinColumn(name="PREREQUISITE_ACTIONPART_ID", nullable=false)
	private ActionPart prerequisiteAction;
	
	/** The criteria. */
	@Embedded
	private ActionPartScheduleCriteria criteria;

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public ActionPart getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(ActionPart action) {
		this.action = action;
	}

	/**
	 * Gets the prerequisite action.
	 *
	 * @return the prerequisite action
	 */
	public ActionPart getPrerequisiteAction() {
		return prerequisiteAction;
	}

	/**
	 * Sets the prerequisite action.
	 *
	 * @param prerequisiteAction the new prerequisite action
	 */
	public void setPrerequisiteAction(ActionPart prerequisiteAction) {
		this.prerequisiteAction = prerequisiteAction;
	}

	/**
	 * Gets the criteria.
	 *
	 * @return the criteria
	 */
	public ActionPartScheduleCriteria getCriteria() {
		return criteria;
	}

	/**
	 * Sets the criteria.
	 *
	 * @param criteria the new criteria
	 */
	public void setCriteria(ActionPartScheduleCriteria criteria) {
		this.criteria = criteria;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

		
}


