/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.snapshot.resource.OperationsCentreSnapshot;

// TODO: Auto-generated Javadoc
/**
 * The Class OCMeoc. Entity class that manage the Operations Centers MEOC
 */
@Entity
@DiscriminatorValue("MEOC")
@NamedQueries({
		@NamedQuery(name = "OCMeoc.findByTitle", query = "select c from OCMeoc c where c.title=:title"),
		@NamedQuery(name = "OCMeoc.findAll", query = "select c from OCMeoc c") })
public class OCMeoc extends OperationsCentre {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1867112082251273511L;

	/** The supervising oc. */
	@ManyToOne
	@JoinColumn(name = "EOC")
	private OCEoc supervisingOC;

	/** The incident commander. */
	@OneToOne
	@JoinColumn(name = "INC_COMMANDER")
	private ActorIC incidentCommander;

	@OneToOne
	@JoinColumn(name = "WEATHERSTATION")
	private WeatherStation weatherStation;
	
	/** The meoc crisis context. */
	@ManyToOne
	@JoinColumn(name = "CRISIS_CONTEXT_FOR_MEOC")
	private CrisisContext meocCrisisContext;

	/** The snapshots. */
	@OneToMany(mappedBy = "operationsCentre")
	private Set<OperationsCentreSnapshot> snapshots;

	
	// extras
	private String dlnaip;
	private String dlnaprop1;
	private String dlnaprop2;
	private String dlnaprop3;
	private String dlnaprop4;
	private String sIPAddress;
	private String staip;
	private String wimaxip;
	private String wifiip;
	private String meoc3gip;

	private String sIPusername;
	private String sIPpassword;
	private String sIPaddressEOC;

	private String sIPaddresssrv;
	private String sIPaddresscli;
	
	private String sIPusername2;
	private String sIPpassword2;
	private String sIPaddressEOC2;

	private String sIPaddresssrv2;
	private String sIPaddresscli2;
	
	private String sIPusername3;
	private String sIPpassword3;

	private String sIPaddressEOC3;

	private String sIPaddresssrv3;
	private String sIPaddresscli3;


	
	public String getsIPusername2() {
		return sIPusername2;
	}

	public void setsIPusername2(String sIPusername2) {
		this.sIPusername2 = sIPusername2;
	}

	public String getsIPpassword2() {
		return sIPpassword2;
	}

	public void setsIPpassword2(String sIPpassword2) {
		this.sIPpassword2 = sIPpassword2;
	}

	public String getsIPaddressEOC2() {
		return sIPaddressEOC2;
	}

	public void setsIPaddressEOC2(String sIPaddressEOC2) {
		this.sIPaddressEOC2 = sIPaddressEOC2;
	}

	public String getsIPaddresssrv2() {
		return sIPaddresssrv2;
	}

	public void setsIPaddresssrv2(String sIPaddresssrv2) {
		this.sIPaddresssrv2 = sIPaddresssrv2;
	}

	public String getsIPaddresscli2() {
		return sIPaddresscli2;
	}

	public void setsIPaddresscli2(String sIPaddresscli2) {
		this.sIPaddresscli2 = sIPaddresscli2;
	}

	public String getsIPusername3() {
		return sIPusername3;
	}

	public void setsIPusername3(String sIPusername3) {
		this.sIPusername3 = sIPusername3;
	}

	public String getsIPpassword3() {
		return sIPpassword3;
	}

	public void setsIPpassword3(String sIPpassword3) {
		this.sIPpassword3 = sIPpassword3;
	}

	public String getsIPaddressEOC3() {
		return sIPaddressEOC3;
	}

	public void setsIPaddressEOC3(String sIPaddressEOC3) {
		this.sIPaddressEOC3 = sIPaddressEOC3;
	}

	public String getsIPaddresssrv3() {
		return sIPaddresssrv3;
	}

	public void setsIPaddresssrv3(String sIPaddresssrv3) {
		this.sIPaddresssrv3 = sIPaddresssrv3;
	}

	public String getsIPaddresscli3() {
		return sIPaddresscli3;
	}

	public void setsIPaddresscli3(String sIPaddresscli3) {
		this.sIPaddresscli3 = sIPaddresscli3;
	}


	public WeatherStation getWeatherStation() {
		return weatherStation;
	}

	public void setWeatherStation(WeatherStation weatherStation) {
		this.weatherStation = weatherStation;
	}

	
	public String getDlnaip() {
		return dlnaip;
	}

	public void setDlnaip(String dlnaip) {
		this.dlnaip = dlnaip;
	}

	public String getDlnaprop1() {
		return dlnaprop1;
	}

	public void setDlnaprop1(String dlnaprop1) {
		this.dlnaprop1 = dlnaprop1;
	}

	public String getDlnaprop2() {
		return dlnaprop2;
	}

	public void setDlnaprop2(String dlnaprop2) {
		this.dlnaprop2 = dlnaprop2;
	}

	public String getDlnaprop3() {
		return dlnaprop3;
	}

	public void setDlnaprop3(String dlnaprop3) {
		this.dlnaprop3 = dlnaprop3;
	}

	public String getDlnaprop4() {
		return dlnaprop4;
	}

	public void setDlnaprop4(String dlnaprop4) {
		this.dlnaprop4 = dlnaprop4;
	}

	public String getsIPAddress() {
		return sIPAddress;
	}

	public void setsIPAddress(String sIPAddress) {
		this.sIPAddress = sIPAddress;
	}

	public String getStaip() {
		return staip;
	}

	public void setStaip(String staip) {
		this.staip = staip;
	}

	public String getWimaxip() {
		return wimaxip;
	}

	public void setWimaxip(String wimaxip) {
		this.wimaxip = wimaxip;
	}

	public String getWifiip() {
		return wifiip;
	}

	public void setWifiip(String wifiip) {
		this.wifiip = wifiip;
	}

	public String getMeoc3gip() {
		return meoc3gip;
	}

	public void setMeoc3gip(String meoc3gip) {
		this.meoc3gip = meoc3gip;
	}

	public String getsIPusername() {
		return sIPusername;
	}

	public void setsIPusername(String sIPusername) {
		this.sIPusername = sIPusername;
	}

	public String getsIPpassword() {
		return sIPpassword;
	}

	public void setsIPpassword(String sIPpassword) {
		this.sIPpassword = sIPpassword;
	}

	public String getsIPaddressEOC() {
		return sIPaddressEOC;
	}

	public void setsIPaddressEOC(String sIPaddressEOC) {
		this.sIPaddressEOC = sIPaddressEOC;
	}

	public String getsIPaddresssrv() {
		return sIPaddresssrv;
	}

	public void setsIPaddresssrv(String sIPaddresssrv) {
		this.sIPaddresssrv = sIPaddresssrv;
	}

	public String getsIPaddresscli() {
		return sIPaddresscli;
	}

	public void setsIPaddresscli(String sIPaddresscli) {
		this.sIPaddresscli = sIPaddresscli;
	}

	/**
	 * Gets the meoc crisis context.
	 * 
	 * @return the meoc crisis context
	 */
	public CrisisContext getMeocCrisisContext() {
		return meocCrisisContext;
	}

	/**
	 * Sets the meoc crisis context.
	 * 
	 * @param meocCrisisContext
	 *            the new meoc crisis context
	 */
	public void setMeocCrisisContext(CrisisContext meocCrisisContext) {
		this.meocCrisisContext = meocCrisisContext;
	}

	/**
	 * Gets the snapshots.
	 * 
	 * @return the snapshots
	 */
	public Set<OperationsCentreSnapshot> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 * 
	 * @param snapshots
	 *            the new snapshots
	 */
	public void setSnapshots(Set<OperationsCentreSnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	/**
	 * Gets the supervising oc.
	 * 
	 * @return the supervising oc
	 */
	public OCEoc getSupervisingOC() {
		return supervisingOC;
	}

	/**
	 * Sets the supervising oc.
	 * 
	 * @param supervisingOC
	 *            the new supervising oc
	 */
	public void setSupervisingOC(OCEoc supervisingOC) {
		this.supervisingOC = supervisingOC;
	}

	/**
	 * Gets the incident commander.
	 * 
	 * @return the incident commander
	 */
	public ActorIC getIncidentCommander() {
		return incidentCommander;
	}

	/**
	 * Sets the incident commander.
	 * 
	 * @param incidentCommander
	 *            the new incident commander
	 */
	public void setIncidentCommander(ActorIC incidentCommander) {
		this.incidentCommander = incidentCommander;
	}

}
