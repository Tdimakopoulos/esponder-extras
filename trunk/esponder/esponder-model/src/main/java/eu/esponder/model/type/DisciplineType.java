/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * Models the various disciplines available in the system. 
 * Disciplines should not have parent and child Types.
 * 
 * We need to apply this constraint somehow.
 * 
 */
@Entity
@DiscriminatorValue("DISCIPLINE")
public class DisciplineType extends ESponderType {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7247305696249606800L;

}
