/*
 * 
 */
package eu.esponder.session;


// TODO: Auto-generated Javadoc
/**
 * The Enum SeverityLevel. Each action have a severity level, this at the moment is define as Minimal, Medium and Serious.
 */
public enum LoginType {
	
	/** The minimal. */
	EOC,
	
	/** The medium. */
	MEOC,
	
	/** The serious. */
	FRC,
	
	FR,
	
	VOIP,
	
	MISC,
	
	SC,
	
	OPT
}
