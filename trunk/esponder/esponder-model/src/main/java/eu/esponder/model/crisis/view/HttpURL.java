/*
 * 
 */
package eu.esponder.model.crisis.view;

import javax.persistence.Embeddable;

// TODO: Auto-generated Javadoc
/**
 * The Class HttpURL.
 */
@Embeddable
public class HttpURL extends URL {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2776045310365314316L;

	/**
	 * Instantiates a new http url.
	 */
	public HttpURL() {
		this.setProtocol("http");
	}
}
