package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("METHANE")
public class MethaneSensor extends GasSensor implements ArithmeticMeasurementSensor{
	
	private static final long serialVersionUID = 3558820514511398019L;

}
