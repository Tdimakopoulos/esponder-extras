/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import eu.esponder.model.snapshot.resource.ActorSnapshot;


// TODO: Auto-generated Javadoc
/**
 * The Class FirstResponderActor this is an abstract class , both ActorFR and ActorFRC extend this class.
 */
@Entity
@DiscriminatorValue("FRA")
@NamedQueries({
	@NamedQuery(name="FirstResponderActor.findByTitle", query="select a from FirstResponderActor a where a.title=:title")
})
public class FirstResponderActor extends Actor {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5282825510587220844L;

	/** The equipment set. */
	@OneToMany(mappedBy="actor")
	private Set<Equipment> equipmentSet;

	String IMEI;
	
	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String iMEI) {
		IMEI = iMEI;
	}

	/** The snapshots. */
	@OneToMany(mappedBy="actor")
	private Set<ActorSnapshot> snapshots;

	/**
	 * Gets the equipment set.
	 *
	 * @return the equipment set
	 */
	public Set<Equipment> getEquipmentSet() {
		return equipmentSet;
	}

	/**
	 * Sets the equipment set.
	 *
	 * @param equipmentSet the new equipment set
	 */
	public void setEquipmentSet(Set<Equipment> equipmentSet) {
		this.equipmentSet = equipmentSet;
	}

	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<ActorSnapshot> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<ActorSnapshot> snapshots) {
		this.snapshots = snapshots;
	}
	
}
