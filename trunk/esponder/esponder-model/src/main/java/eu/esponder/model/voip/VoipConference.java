package eu.esponder.model.voip;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;

@Entity
@Table(name="voipconference")
@NamedQueries({
		@NamedQuery(name = "VoipConference.findAll", query = "select u from VoipConference u"),
		@NamedQuery(name = "VoipConference.findByid", query = "select u from VoipConference u where u.id=:id"),
		@NamedQuery(name = "VoipConference.findByConnectionID", query = "select u from VoipConference u where u.connectionID=:connectionID"),
		@NamedQuery(name = "VoipConference.findByConferenceID", query = "select u from VoipConference u where u.conferenceID=:conferenceID"),
		@NamedQuery(name = "VoipConference.findByConferenceTitle", query = "select u from VoipConference u where u.conferenceTitle=:conferenceTitle")})
public class VoipConference extends ESponderEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5862759086706188354L;
	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="VOIPCONF_ID")
	protected Long id;
	
	Long connectionID;
	
	Long conferenceID;
	
	String conferenceTitle;
	
	int istatus;
	
	
	public Long getConnectionID() {
		return connectionID;
	}

	public void setConnectionID(Long connectionID) {
		this.connectionID = connectionID;
	}

	public Long getConferenceID() {
		return conferenceID;
	}

	public void setConferenceID(Long conferenceID) {
		this.conferenceID = conferenceID;
	}

	public String getConferenceTitle() {
		return conferenceTitle;
	}

	public void setConferenceTitle(String conferenceTitle) {
		this.conferenceTitle = conferenceTitle;
	}

	public int getIstatus() {
		return istatus;
	}

	public void setIstatus(int istatus) {
		this.istatus = istatus;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}
}
