/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.crisis.resource.category.OrganisationCategoryDTO;
import eu.esponder.dto.model.snapshot.location.AddressDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class OrganisationDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "firstName", "lastName", "rank", "availability", "organisation"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OrganisationDTO extends ESponderEntityDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1939967592244383791L;

	/**
	 * Instantiates a new organisation dto.
	 */
	public OrganisationDTO() { }
	
	/** The title. */
	private String title;
	
	/** The organisation categories. */
	private Set<OrganisationCategoryDTO> organisationCategories;
	
	/** The parent. */
	private OrganisationDTO parent;
	
	/** The children. */
	private Set<Long> children;
	
	/** The address. */
	private AddressDTO address;
	
	/** The location. */
	private PointDTO location;
	
	/** The responsibility area. */
	private LocationAreaDTO responsibilityArea;
	
	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public OrganisationDTO getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(OrganisationDTO parent) {
		this.parent = parent;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public Set<Long> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the new children
	 */
	public void setChildren(Set<Long> children) {
		this.children = children;
	}
	
	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public AddressDTO getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(AddressDTO address) {
		this.address = address;
	}

	/**
	 * Gets the location.
	 *
	 * @return the location
	 */
	public PointDTO getLocation() {
		return location;
	}

	/**
	 * Sets the location.
	 *
	 * @param location the new location
	 */
	public void setLocation(PointDTO location) {
		this.location = location;
	}

	/**
	 * Gets the responsibility area.
	 *
	 * @return the responsibility area
	 */
	public LocationAreaDTO getResponsibilityArea() {
		return responsibilityArea;
	}

	/**
	 * Sets the responsibility area.
	 *
	 * @param responsibilityArea the new responsibility area
	 */
	public void setResponsibilityArea(LocationAreaDTO responsibilityArea) {
		this.responsibilityArea = responsibilityArea;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the organisation categories.
	 *
	 * @return the organisation categories
	 */
	public Set<OrganisationCategoryDTO> getOrganisationCategories() {
		return organisationCategories;
	}

	/**
	 * Sets the organisation categories.
	 *
	 * @param organisationCategories the new organisation categories
	 */
	public void setOrganisationCategories(Set<OrganisationCategoryDTO> organisationCategories) {
		this.organisationCategories = organisationCategories;
	}
	
	

}
