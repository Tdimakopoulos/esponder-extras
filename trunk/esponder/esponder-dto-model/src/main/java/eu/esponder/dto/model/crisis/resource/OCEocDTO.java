/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class OCEocDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "resourceId", "type", "title", "status","crisisManager","subordinateMEOCs","locationArea","eocCrisisContext","dlnaip","dlnaprop1","dlnaprop2","dlnaprop3","dlnaprop4","sIPAddress","netip","satip","wimaxip",
	"wifiip","meoc3gip","sIPusername","sIPpassword","sIPaddresssrv","sIPaddresscli","sIPusername2","sIPpassword2","sIPaddresssrv2","sIPaddresscli2","sIPusername3","sIPpassword3","sIPaddresssrv3sIPaddresscli3" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class OCEocDTO extends OperationsCentreDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5218588692128620330L;

	/** The crisis manager. */
	private Long crisisManager;

	/** The subordinate meo cs. */
	private Set<Long> subordinateMEOCs;

	private LocationAreaDTO locationArea;

	/** The eoc crisis context. */
	private CrisisContextDTO eocCrisisContext;

	// extras
	private String dlnaip;
	private String dlnaprop1;
	private String dlnaprop2;
	private String dlnaprop3;
	private String dlnaprop4;
	private String sIPAddress;
	private String netip;
	private String satip;
	private String wimaxip;
	private String wifiip;
	private String meoc3gip;

	private String sIPusername;
	private String sIPpassword;

	private String sIPaddresssrv;
	private String sIPaddresscli;

	
	private String sIPusername2;
	private String sIPpassword2;

	private String sIPaddresssrv2;
	private String sIPaddresscli2;
	
	private String sIPusername3;
	private String sIPpassword3;

	private String sIPaddresssrv3;
	private String sIPaddresscli3;
	
	
	public String getsIPusername2() {
		return sIPusername2;
	}

	public void setsIPusername2(String sIPusername2) {
		this.sIPusername2 = sIPusername2;
	}

	public String getsIPpassword2() {
		return sIPpassword2;
	}

	public void setsIPpassword2(String sIPpassword2) {
		this.sIPpassword2 = sIPpassword2;
	}

	public String getsIPaddresssrv2() {
		return sIPaddresssrv2;
	}

	public void setsIPaddresssrv2(String sIPaddresssrv2) {
		this.sIPaddresssrv2 = sIPaddresssrv2;
	}

	public String getsIPaddresscli2() {
		return sIPaddresscli2;
	}

	public void setsIPaddresscli2(String sIPaddresscli2) {
		this.sIPaddresscli2 = sIPaddresscli2;
	}

	public String getsIPusername3() {
		return sIPusername3;
	}

	public void setsIPusername3(String sIPusername3) {
		this.sIPusername3 = sIPusername3;
	}

	public String getsIPpassword3() {
		return sIPpassword3;
	}

	public void setsIPpassword3(String sIPpassword3) {
		this.sIPpassword3 = sIPpassword3;
	}

	public String getsIPaddresssrv3() {
		return sIPaddresssrv3;
	}

	public void setsIPaddresssrv3(String sIPaddresssrv3) {
		this.sIPaddresssrv3 = sIPaddresssrv3;
	}

	public String getsIPaddresscli3() {
		return sIPaddresscli3;
	}

	public void setsIPaddresscli3(String sIPaddresscli3) {
		this.sIPaddresscli3 = sIPaddresscli3;
	}

	public String getsIPusername() {
		return sIPusername;
	}

	public void setsIPusername(String sIPusername) {
		this.sIPusername = sIPusername;
	}

	public String getsIPpassword() {
		return sIPpassword;
	}

	public void setsIPpassword(String sIPpassword) {
		this.sIPpassword = sIPpassword;
	}

	public String getsIPaddresssrv() {
		return sIPaddresssrv;
	}

	public void setsIPaddresssrv(String sIPaddresssrv) {
		this.sIPaddresssrv = sIPaddresssrv;
	}

	public String getsIPaddresscli() {
		return sIPaddresscli;
	}

	public void setsIPaddresscli(String sIPaddresscli) {
		this.sIPaddresscli = sIPaddresscli;
	}

	public String getDlnaip() {
		return dlnaip;
	}

	public void setDlnaip(String dlnaip) {
		this.dlnaip = dlnaip;
	}

	public String getDlnaprop1() {
		return dlnaprop1;
	}

	public void setDlnaprop1(String dlnaprop1) {
		this.dlnaprop1 = dlnaprop1;
	}

	public String getDlnaprop2() {
		return dlnaprop2;
	}

	public void setDlnaprop2(String dlnaprop2) {
		this.dlnaprop2 = dlnaprop2;
	}

	public String getDlnaprop3() {
		return dlnaprop3;
	}

	public void setDlnaprop3(String dlnaprop3) {
		this.dlnaprop3 = dlnaprop3;
	}

	public String getDlnaprop4() {
		return dlnaprop4;
	}

	public void setDlnaprop4(String dlnaprop4) {
		this.dlnaprop4 = dlnaprop4;
	}

	public String getsIPAddress() {
		return sIPAddress;
	}

	public void setsIPAddress(String sIPAddress) {
		this.sIPAddress = sIPAddress;
	}

	public String getNetip() {
		return netip;
	}

	public void setNetip(String netip) {
		this.netip = netip;
	}

	public String getSatip() {
		return satip;
	}

	public void setSatip(String satip) {
		this.satip = satip;
	}

	public String getWimaxip() {
		return wimaxip;
	}

	public void setWimaxip(String wimaxip) {
		this.wimaxip = wimaxip;
	}

	public String getWifiip() {
		return wifiip;
	}

	public void setWifiip(String wifiip) {
		this.wifiip = wifiip;
	}

	public String getMeoc3gip() {
		return meoc3gip;
	}

	public void setMeoc3gip(String meoc3gip) {
		this.meoc3gip = meoc3gip;
	}

	/**
	 * Gets the crisis manager.
	 * 
	 * @return the crisis manager
	 */
	public Long getCrisisManager() {
		return crisisManager;
	}

	/**
	 * Sets the crisis manager.
	 * 
	 * @param crisisManager
	 *            the new crisis manager
	 */
	public void setCrisisManager(Long crisisManager) {
		this.crisisManager = crisisManager;
	}

	/**
	 * Gets the subordinate meo cs.
	 * 
	 * @return the subordinate meo cs
	 */
	public Set<Long> getSubordinateMEOCs() {
		return subordinateMEOCs;
	}

	/**
	 * Sets the subordinate meo cs.
	 * 
	 * @param subordinateMEOCs
	 *            the new subordinate meo cs
	 */
	public void setSubordinateMEOCs(Set<Long> subordinateMEOCs) {
		this.subordinateMEOCs = subordinateMEOCs;
	}

	/**
	 * Gets the eoc crisis context.
	 * 
	 * @return the eoc crisis context
	 */
	public CrisisContextDTO getEocCrisisContext() {
		return eocCrisisContext;
	}

	/**
	 * Sets the eoc crisis context.
	 * 
	 * @param eocCrisisContext
	 *            the new eoc crisis context
	 */
	public void setEocCrisisContext(CrisisContextDTO eocCrisisContext) {
		this.eocCrisisContext = eocCrisisContext;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.esponder.dto.model.crisis.resource.OperationsCentreDTO#toString()
	 */
	@Override
	public String toString() {
		return "OCEocDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

}
