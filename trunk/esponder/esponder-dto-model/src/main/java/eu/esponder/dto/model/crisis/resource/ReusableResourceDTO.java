/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.math.BigDecimal;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.type.VehicleEquipmentTypeDTO;
import eu.esponder.dto.model.type.VehiclePurposeTypeDTO;
import eu.esponder.dto.model.type.VehicleTypeDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class ReusableResourceDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "quantity", "actionPart", "consumables", "children", "configuration", "reusableResourceCategoryId"
	,"vehicleEquipments"
	,"vehiclePurposes"
	,"vehicleType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ReusableResourceDTO extends LogisticsResourceDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1012441430710219912L;

	/**
	 * Instantiates a new reusable resource dto.
	 */
	public ReusableResourceDTO() { }
	
	/** The quantity. */
	private BigDecimal quantity;
	
	/** The consumables. */
	private Set<ConsumableResourceDTO> consumables;
	
	/** The children. */
	private Set<ReusableResourceDTO> children;
	
	/** The action part. */
	private ActionPartDTO actionPart;
	
	/** The reusable resource category. */
	private ReusableResourceCategoryDTO reusableResourceCategory;

	
	private Set<VehicleEquipmentTypeDTO> vehicleEquipments;
	
	
	private Set<VehiclePurposeTypeDTO> vehiclePurposes;
	
	private VehicleTypeDTO vehicleType;
	
	
	public Set<VehicleEquipmentTypeDTO> getVehicleEquipments() {
		return vehicleEquipments;
	}
	public void setVehicleEquipments(
			Set<VehicleEquipmentTypeDTO> vehicleEquipments) {
		this.vehicleEquipments = vehicleEquipments;
	}

	public Set<VehiclePurposeTypeDTO> getVehiclePurposes() {
		return vehiclePurposes;
	}

	public void setVehiclePurposes(Set<VehiclePurposeTypeDTO> vehiclePurposes) {
		this.vehiclePurposes = vehiclePurposes;
	}

	public VehicleTypeDTO getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(VehicleTypeDTO vehicleType) {
		this.vehicleType = vehicleType;
	}

	/** The crisis context id. */
	private Long crisisContextId;
	
	/**
	 * Gets the crisis context id.
	 *
	 * @return the crisis context id
	 */
	public Long getCrisisContextId() {
		return crisisContextId;
	}

	/**
	 * Sets the crisis context id.
	 *
	 * @param crisisContextId the new crisis context id
	 */
	public void setCrisisContextId(Long crisisContextId) {
		this.crisisContextId = crisisContextId;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.ResourceDTO#getType()
	 */
	public String getType() {
		return type;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.ResourceDTO#setType(java.lang.String)
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Gets the consumables.
	 *
	 * @return the consumables
	 */
	public Set<ConsumableResourceDTO> getConsumables() {
		return consumables;
	}

	/**
	 * Sets the consumables.
	 *
	 * @param consumables the new consumables
	 */
	public void setConsumables(Set<ConsumableResourceDTO> consumables) {
		this.consumables = consumables;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public Set<ReusableResourceDTO> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the new children
	 */
	public void setChildren(Set<ReusableResourceDTO> children) {
		this.children = children;
	}

	/**
	 * Gets the action part.
	 *
	 * @return the action part
	 */
	public ActionPartDTO getActionPart() {
		return actionPart;
	}

	/**
	 * Sets the action part.
	 *
	 * @param actionPart the new action part
	 */
	public void setActionPart(ActionPartDTO actionPart) {
		this.actionPart = actionPart;
	}

	/**
	 * Gets the reusable resource category.
	 *
	 * @return the reusable resource category
	 */
	public ReusableResourceCategoryDTO getReusableResourceCategory() {
		return reusableResourceCategory;
	}

	/**
	 * Sets the reusable resource category.
	 *
	 * @param reusableResourceCategory the new reusable resource category
	 */
	public void setReusableResourceCategory(ReusableResourceCategoryDTO reusableResourceCategory) {
		this.reusableResourceCategory = reusableResourceCategory;
	}
	
}
