/*
 * 
 */
package eu.esponder.dto.model.snapshot.status;


// TODO: Auto-generated Javadoc
/**
 * The Enum SensorSnapshotStatusDTO.
 */
public enum SensorSnapshotStatusDTO {
	
	/** The working. */
	WORKING,
	
	/** The malfunctioning. */
	MALFUNCTIONING,
	
	/** The damaged. */
	DAMAGED
}
