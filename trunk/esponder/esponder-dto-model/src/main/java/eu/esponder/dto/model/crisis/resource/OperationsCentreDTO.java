/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.plan.PlannableResourceDTO;
import eu.esponder.dto.model.crisis.view.VoIPURLDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId","title", "status", "users","snapshots", "operationsCentreCategory","voIPURL"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class OperationsCentreDTO extends PlannableResourceDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1138167138418691550L;

	/** The users. */
	private Set<Long> users;
	
	/** The vo ipurl. */
	private VoIPURLDTO voIPURL;
	
	/**
	 * Instantiates a new operations centre dto.
	 */
	public OperationsCentreDTO() { }
	
		
	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public Set<Long> getUsers() {
		return users;
	}


	/**
	 * Sets the users.
	 *
	 * @param users the new users
	 */
	public void setUsers(Set<Long> users) {
		this.users = users;
	}


	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}


	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}


	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.ResourceDTO#toString()
	 */
	@Override
	public String toString() {
		return "OperationsCentreDTO [type=" + type + ", status=" + status
				+ ", id=" + id + ", title=" + title + "]";
	}
	
}
