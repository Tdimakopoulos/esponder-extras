/*
 * 
 */
package eu.esponder.dto.model.crisis.action;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;



// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartScheduleDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "actionpart", "prerequisiteActionpart", "criteria"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActionPartScheduleDTO extends ESponderEntityDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -703599359640439512L;

	/** The title. */
	private String title;
	
	/** The actionpart. */
	private ActionPartDTO actionpart;
	
	/** The prerequisite actionpart. */
	private ActionPartDTO prerequisiteActionpart;
	
	/** The criteria. */
	private ActionPartScheduleCriteriaDTO criteria;

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the actionpart.
	 *
	 * @return the actionpart
	 */
	public ActionPartDTO getActionpart() {
		return actionpart;
	}

	/**
	 * Sets the actionpart.
	 *
	 * @param actionpart the new actionpart
	 */
	public void setActionpart(ActionPartDTO actionpart) {
		this.actionpart = actionpart;
	}

	/**
	 * Gets the prerequisite actionpart.
	 *
	 * @return the prerequisite actionpart
	 */
	public ActionPartDTO getPrerequisiteActionpart() {
		return prerequisiteActionpart;
	}

	/**
	 * Sets the prerequisite actionpart.
	 *
	 * @param prerequisiteActionpart the new prerequisite actionpart
	 */
	public void setPrerequisiteActionpart(ActionPartDTO prerequisiteActionpart) {
		this.prerequisiteActionpart = prerequisiteActionpart;
	}

	/**
	 * Gets the criteria.
	 *
	 * @return the criteria
	 */
	public ActionPartScheduleCriteriaDTO getCriteria() {
		return criteria;
	}

	/**
	 * Sets the criteria.
	 *
	 * @param criteria the new criteria
	 */
	public void setCriteria(ActionPartScheduleCriteriaDTO criteria) {
		this.criteria = criteria;
	}

	
}


