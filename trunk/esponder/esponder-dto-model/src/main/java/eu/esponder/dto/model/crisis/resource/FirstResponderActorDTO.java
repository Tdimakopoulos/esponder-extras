/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;



// TODO: Auto-generated Javadoc
/**
 * The Class FirstResponderActorDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "personnel", "voIPURL", "equipmentSet", "snapshots"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class FirstResponderActorDTO extends ActorDTO {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8530519046739693752L;
	
	/**
	 * Instantiates a new first responder actor dto.
	 */
	public FirstResponderActorDTO() {}
	
	/** The equipment set. */
	private Set<EquipmentDTO> equipmentSet;
	
	/** The snapshots. */
	private Set<ActorSnapshotDTO> snapshots;

	String IMEI;
	
	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String iMEI) {
		IMEI = iMEI;
	}

	/**
	 * Gets the equipment set.
	 *
	 * @return the equipment set
	 */
	public Set<EquipmentDTO> getEquipmentSet() {
		return equipmentSet;
	}

	/**
	 * Sets the equipment set.
	 *
	 * @param equipmentSet the new equipment set
	 */
	public void setEquipmentSet(Set<EquipmentDTO> equipmentSet) {
		this.equipmentSet = equipmentSet;
	}

	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public Set<ActorSnapshotDTO> getSnapshots() {
		return snapshots;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshots(Set<ActorSnapshotDTO> snapshots) {
		this.snapshots = snapshots;
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.ActorDTO#toString()
	 */
	@Override
	public String toString() {
		return "FirstResponderActorDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}

}
