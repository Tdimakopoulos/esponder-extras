/*
 * 
 */
package eu.esponder.dto.model.snapshot.status;


// TODO: Auto-generated Javadoc
/**
 * The Enum CrisisContextSnapshotStatusDTO.
 */
public enum CrisisContextSnapshotStatusDTO {
	
	/** The started. */
	STARTED,
	
	/** The resolved. */
	RESOLVED,
	
	/** The unresolved. */
	UNRESOLVED
}
