/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.sensor;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class CarbonMonoxideSensorDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "measurementUnit", "configuration", "label", "equipmentId"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class CarbonMonoxideSensorDTO extends GasSensorDTO implements ArithmeticMeasurementSensorDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5060994814862024362L;
	
	static String label="CarbonMonoxideSensor";
	
	public static String getLabel() {
		return label;
	}

	public static void setLabel(String label) {
		CarbonMonoxideSensorDTO.label = label;
	}



}
