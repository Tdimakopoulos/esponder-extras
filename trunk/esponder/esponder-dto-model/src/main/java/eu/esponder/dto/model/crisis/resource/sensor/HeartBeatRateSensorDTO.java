/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.sensor;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class HeartBeatRateSensorDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "measurementUnit", "configuration", "label", "equipmentId"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class HeartBeatRateSensorDTO extends BiomedicalSensorDTO implements ArithmeticMeasurementSensorDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7776717340125708005L;
	
	static String label="HeartBeatRateSensor";
	
	public static String getLabel() {
		return label;
	}

	public static void setLabel(String label) {
		LPSSensorDTO.label = label;
	}



}
