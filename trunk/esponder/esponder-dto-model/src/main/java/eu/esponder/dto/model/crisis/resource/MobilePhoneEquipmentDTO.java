/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;



// TODO: Auto-generated Javadoc
/**
 * The Class EquipmentDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "mobilephoneIMEI"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class MobilePhoneEquipmentDTO extends EquipmentDTO {

	private static final long serialVersionUID = 4249341949210336754L;

	String mobilephoneIMEI;
	
	
	public String getMobilephoneIMEI() {
		return mobilephoneIMEI;
	}


	public void setMobilephoneIMEI(String mobilephoneIMEI) {
		this.mobilephoneIMEI = mobilephoneIMEI;
	}


	public MobilePhoneEquipmentDTO() { }
	
		
}
