/*
 * 
 */
package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;




// TODO: Auto-generated Javadoc
/**
 * The Class ReusableResourceType.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "parent", "children"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class BiogeoclimaticzoneTypeDTO extends ESponderTypeDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2802242890074309451L;



	
	
	
}
