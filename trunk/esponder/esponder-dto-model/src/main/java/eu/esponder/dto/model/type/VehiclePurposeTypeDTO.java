/*
 * 
 */
package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;



@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "parent", "children","vehiclepurposes"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class VehiclePurposeTypeDTO extends LogisticsResourceTypeDTO {

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3272709008175792742L;
	private ReusableResourceDTO vehiclepurposes;
	
	
	public ReusableResourceDTO getVehiclepurposes() {
		return vehiclepurposes;
	}

	public void setVehiclepurposes(ReusableResourceDTO vehiclepurposes) {
		this.vehiclepurposes = vehiclepurposes;
	}

		
	
}
