/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class OCEocALTDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "resourceId", "title", "crisisManager",
		"subordinateMEOCs", "eocCrisisContext" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class OCEocALTDTO extends OperationsCentreALTDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6738011571879473184L;

	/** The crisis manager. */
	private ActorCMALTDTO crisisManager;

	/** The subordinate meo cs. */
	private Set<OCMeocALTDTO> subordinateMEOCs;

	/** The eoc crisis context. */
	private Long eocCrisisContext;

	/** The eoc location area. */
	private LocationAreaDTO locationArea;

	// extras
	private String dlnaip;
	private String dlnaprop1;
	private String dlnaprop2;
	private String dlnaprop3;
	private String dlnaprop4;
	private String sIPAddress;
	private String netip;
	private String satip;
	private String wimaxip;
	private String wifiip;
	private String meoc3gip;

	private String sIPusername;
	private String sIPpassword;

	private String sIPaddresssrv;
	private String sIPaddresscli;

	public String getDlnaip() {
		return dlnaip;
	}

	public void setDlnaip(String dlnaip) {
		this.dlnaip = dlnaip;
	}

	public String getDlnaprop1() {
		return dlnaprop1;
	}

	public void setDlnaprop1(String dlnaprop1) {
		this.dlnaprop1 = dlnaprop1;
	}

	public String getDlnaprop2() {
		return dlnaprop2;
	}

	public void setDlnaprop2(String dlnaprop2) {
		this.dlnaprop2 = dlnaprop2;
	}

	public String getDlnaprop3() {
		return dlnaprop3;
	}

	public void setDlnaprop3(String dlnaprop3) {
		this.dlnaprop3 = dlnaprop3;
	}

	public String getDlnaprop4() {
		return dlnaprop4;
	}

	public void setDlnaprop4(String dlnaprop4) {
		this.dlnaprop4 = dlnaprop4;
	}

	public String getsIPAddress() {
		return sIPAddress;
	}

	public void setsIPAddress(String sIPAddress) {
		this.sIPAddress = sIPAddress;
	}

	public String getNetip() {
		return netip;
	}

	public void setNetip(String netip) {
		this.netip = netip;
	}

	public String getSatip() {
		return satip;
	}

	public void setSatip(String satip) {
		this.satip = satip;
	}

	public String getWimaxip() {
		return wimaxip;
	}

	public void setWimaxip(String wimaxip) {
		this.wimaxip = wimaxip;
	}

	public String getWifiip() {
		return wifiip;
	}

	public void setWifiip(String wifiip) {
		this.wifiip = wifiip;
	}

	public String getMeoc3gip() {
		return meoc3gip;
	}

	public void setMeoc3gip(String meoc3gip) {
		this.meoc3gip = meoc3gip;
	}

	public String getsIPusername() {
		return sIPusername;
	}

	public void setsIPusername(String sIPusername) {
		this.sIPusername = sIPusername;
	}

	public String getsIPpassword() {
		return sIPpassword;
	}

	public void setsIPpassword(String sIPpassword) {
		this.sIPpassword = sIPpassword;
	}

	public String getsIPaddresssrv() {
		return sIPaddresssrv;
	}

	public void setsIPaddresssrv(String sIPaddresssrv) {
		this.sIPaddresssrv = sIPaddresssrv;
	}

	public String getsIPaddresscli() {
		return sIPaddresscli;
	}

	public void setsIPaddresscli(String sIPaddresscli) {
		this.sIPaddresscli = sIPaddresscli;
	}

	/**
	 * Gets the crisis manager.
	 * 
	 * @return the crisis manager
	 */
	public ActorCMALTDTO getCrisisManager() {
		return crisisManager;
	}

	/**
	 * Sets the crisis manager.
	 * 
	 * @param crisisManager
	 *            the new crisis manager
	 */
	public void setCrisisManager(ActorCMALTDTO crisisManager) {
		this.crisisManager = crisisManager;
	}

	/**
	 * Gets the subordinate meo cs.
	 * 
	 * @return the subordinate meo cs
	 */
	public Set<OCMeocALTDTO> getSubordinateMEOCs() {
		return subordinateMEOCs;
	}

	/**
	 * Sets the subordinate meocs.
	 * 
	 * @param subordinateMEOCs
	 *            the new subordinate meo cs
	 */
	public void setSubordinateMEOCs(Set<OCMeocALTDTO> subordinateMEOCs) {
		this.subordinateMEOCs = subordinateMEOCs;
	}

	/**
	 * Gets the eoc crisis context.
	 * 
	 * @return the eoc crisis context
	 */
	public Long getEocCrisisContext() {
		return eocCrisisContext;
	}

	/**
	 * Sets the eoc crisis context.
	 * 
	 * @param eocCrisisContext
	 *            the new eoc crisis context
	 */
	public void setEocCrisisContext(Long eocCrisisContext) {
		this.eocCrisisContext = eocCrisisContext;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.dto.model.crisis.alternative.OperationsCentreALTDTO#toString
	 * ()
	 */
	@Override
	public String toString() {
		return this.getClass().getName() + " [id=" + id + ", title=" + title
				+ "]";
	}

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

}
