package eu.esponder.dto.model.crisis;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.type.AngleTypeDTO;
import eu.esponder.dto.model.type.BiogeoclimaticvariantsTypeDTO;
import eu.esponder.dto.model.type.BiogeoclimaticzoneTypeDTO;
import eu.esponder.dto.model.type.DeseaseTypeDTO;
import eu.esponder.dto.model.type.EnvirconditionsTypeDTO;
import eu.esponder.dto.model.type.InsectsTypeDTO;
import eu.esponder.dto.model.type.LandTypeDTO;
import eu.esponder.dto.model.type.PlantationTypeDTO;
import eu.esponder.dto.model.type.SoilTypeDTO;
import eu.esponder.dto.model.type.WildfireTypeDTO;
import eu.esponder.dto.model.type.WindTypeDTO;


@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "soil", "land", "angle"
	, "plantation", "biogeoclimaticzone", "biogeoclimaticvariants"
	, "environmentalconditions", "wildfire", "wind"
	, "insects", "disease", "humanactivity"
	})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class TerrainAndVegetationDTO  extends ESponderEntityDTO {
	
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7025355123493921767L;

	private Long id;
	
	private String title;
	
private SoilTypeDTO soil;
	
	private LandTypeDTO land;
	
	private AngleTypeDTO angle;
	
	private PlantationTypeDTO plantation;
	
	private BiogeoclimaticzoneTypeDTO biogeoclimaticzone;
	
	private BiogeoclimaticvariantsTypeDTO biogeoclimaticvariants;
	
	private EnvirconditionsTypeDTO environmentalconditions;
	
	private WildfireTypeDTO wildfire;
	
	private WindTypeDTO wind;
	
	private InsectsTypeDTO insects;
	
	private DeseaseTypeDTO disease;
	
	private boolean humanactivity;
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}

	
	
	
	public SoilTypeDTO getSoil() {
		return soil;
	}


	public void setSoil(SoilTypeDTO soil) {
		this.soil = soil;
	}


	public LandTypeDTO getLand() {
		return land;
	}


	public void setLand(LandTypeDTO land) {
		this.land = land;
	}


	public AngleTypeDTO getAngle() {
		return angle;
	}

	public void setAngle(AngleTypeDTO angle) {
		this.angle = angle;
	}


	public PlantationTypeDTO getPlantation() {
		return plantation;
	}


	public void setPlantation(PlantationTypeDTO plantation) {
		this.plantation = plantation;
	}


	public BiogeoclimaticzoneTypeDTO getBiogeoclimaticzone() {
		return biogeoclimaticzone;
	}


	public void setBiogeoclimaticzone(
			BiogeoclimaticzoneTypeDTO biogeoclimaticzone) {
		this.biogeoclimaticzone = biogeoclimaticzone;
	}


	public BiogeoclimaticvariantsTypeDTO getBiogeoclimaticvariants() {
		return biogeoclimaticvariants;
	}


	public void setBiogeoclimaticvariants(
			BiogeoclimaticvariantsTypeDTO biogeoclimaticvariants) {
		this.biogeoclimaticvariants = biogeoclimaticvariants;
	}


	public EnvirconditionsTypeDTO getEnvironmentalconditions() {
		return environmentalconditions;
	}


	public void setEnvironmentalconditions(
			EnvirconditionsTypeDTO environmentalconditions) {
		this.environmentalconditions = environmentalconditions;
	}


	public WildfireTypeDTO getWildfire() {
		return wildfire;
	}


	public void setWildfire(WildfireTypeDTO wildfire) {
		this.wildfire = wildfire;
	}


	public WindTypeDTO getWind() {
		return wind;
	}


	public void setWind(WindTypeDTO wind) {
		this.wind = wind;
	}


	public InsectsTypeDTO getInsects() {
		return insects;
	}

	public void setInsects(InsectsTypeDTO insects) {
		this.insects = insects;
	}


	public DeseaseTypeDTO getDisease() {
		return disease;
	}


	public void setDisease(DeseaseTypeDTO disease) {
		this.disease = disease;
	}


	public boolean isHumanactivity() {
		return humanactivity;
	}
	public void setHumanactivity(boolean humanactivity) {
		this.humanactivity = humanactivity;
	}
		

}
