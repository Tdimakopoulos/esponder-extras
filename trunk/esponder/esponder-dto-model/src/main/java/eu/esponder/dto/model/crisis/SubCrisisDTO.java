package eu.esponder.dto.model.crisis;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "crisisLocation", "CrisisID", "MeocID"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SubCrisisDTO extends ESponderEntityDTO{
	private static final long serialVersionUID = 7183618032794276620L;
	private Long id;
	private String title;
	private SphereDTO crisisLocation;
	private Long CrisisID;
	private Long MeocID;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public SphereDTO getCrisisLocation() {
		return crisisLocation;
	}
	public void setCrisisLocation(SphereDTO crisisLocation) {
		this.crisisLocation = crisisLocation;
	}
	public Long getCrisisID() {
		return CrisisID;
	}
	public void setCrisisID(Long crisisID) {
		CrisisID = crisisID;
	}
	public Long getMeocID() {
		return MeocID;
	}
	public void setMeocID(Long meocID) {
		MeocID = meocID;
	}
	
	
}
