package eu.esponder.dto.model.server;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"szServerName","szNodeNames","szChannels","szLeafs"
	,"szIP","szPort","szServerID","lserverID","iserverType","szserverType","szTextField1","szTextField2"
	,"szTextField3","szTextField4","szTextField5","iField1","iField2","iField3","iField4","iField5"
	,"lFiend1","lFiend2","lFiend3","lFiend4","lFiend5"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class EsponderDeploymentSettings implements Serializable{

	private static final long serialVersionUID = -3958916153984626876L;

	String szServerName;
	String szNodeNames;
	String szChannels;
	String szLeafs;
	String szIP;
	String szPort;
	String szServerID;
	Long lserverID;
	int  iserverType;
	String szserverType;
	String szTextField1;
	String szTextField2;
	String szTextField3;
	String szTextField4;
	String szTextField5;
	int iField1;
	int iField2;
	int iField3;
	int iField4;
	int iField5;
	Long lFiend1;
	Long lFiend2;
	Long lFiend3;
	Long lFiend4;
	Long lFiend5;
	
	public String getSzServerName() {
		return szServerName;
	}

	public void setSzServerName(String szServerName) {
		this.szServerName = szServerName;
	}

	public String getSzNodeNames() {
		return szNodeNames;
	}

	public void setSzNodeNames(String szNodeNames) {
		this.szNodeNames = szNodeNames;
	}

	public String getSzChannels() {
		return szChannels;
	}

	public void setSzChannels(String szChannels) {
		this.szChannels = szChannels;
	}

	public String getSzLeafs() {
		return szLeafs;
	}

	public void setSzLeafs(String szLeafs) {
		this.szLeafs = szLeafs;
	}

	public String getSzIP() {
		return szIP;
	}

	public void setSzIP(String szIP) {
		this.szIP = szIP;
	}

	public String getSzPort() {
		return szPort;
	}

	public void setSzPort(String szPort) {
		this.szPort = szPort;
	}

	public String getSzServerID() {
		return szServerID;
	}

	public void setSzServerID(String szServerID) {
		this.szServerID = szServerID;
	}

	public Long getLserverID() {
		return lserverID;
	}

	public void setLserverID(Long lserverID) {
		this.lserverID = lserverID;
	}

	public int getIserverType() {
		return iserverType;
	}

	public void setIserverType(int iserverType) {
		this.iserverType = iserverType;
	}

	public String getSzserverType() {
		return szserverType;
	}

	public void setSzserverType(String szserverType) {
		this.szserverType = szserverType;
	}

	public String getSzTextField1() {
		return szTextField1;
	}

	public void setSzTextField1(String szTextField1) {
		this.szTextField1 = szTextField1;
	}

	public String getSzTextField2() {
		return szTextField2;
	}

	public void setSzTextField2(String szTextField2) {
		this.szTextField2 = szTextField2;
	}

	public String getSzTextField3() {
		return szTextField3;
	}

	public void setSzTextField3(String szTextField3) {
		this.szTextField3 = szTextField3;
	}

	public String getSzTextField4() {
		return szTextField4;
	}

	public void setSzTextField4(String szTextField4) {
		this.szTextField4 = szTextField4;
	}

	public String getSzTextField5() {
		return szTextField5;
	}

	public void setSzTextField5(String szTextField5) {
		this.szTextField5 = szTextField5;
	}

	public int getiField1() {
		return iField1;
	}

	public void setiField1(int iField1) {
		this.iField1 = iField1;
	}

	public int getiField2() {
		return iField2;
	}

	public void setiField2(int iField2) {
		this.iField2 = iField2;
	}

	public int getiField3() {
		return iField3;
	}

	public void setiField3(int iField3) {
		this.iField3 = iField3;
	}

	public int getiField4() {
		return iField4;
	}

	public void setiField4(int iField4) {
		this.iField4 = iField4;
	}

	public int getiField5() {
		return iField5;
	}

	public void setiField5(int iField5) {
		this.iField5 = iField5;
	}

	public Long getlFiend1() {
		return lFiend1;
	}

	public void setlFiend1(Long lFiend1) {
		this.lFiend1 = lFiend1;
	}

	public Long getlFiend2() {
		return lFiend2;
	}

	public void setlFiend2(Long lFiend2) {
		this.lFiend2 = lFiend2;
	}

	public Long getlFiend3() {
		return lFiend3;
	}

	public void setlFiend3(Long lFiend3) {
		this.lFiend3 = lFiend3;
	}

	public Long getlFiend4() {
		return lFiend4;
	}

	public void setlFiend4(Long lFiend4) {
		this.lFiend4 = lFiend4;
	}

	public Long getlFiend5() {
		return lFiend5;
	}

	public void setlFiend5(Long lFiend5) {
		this.lFiend5 = lFiend5;
	}

	public EsponderDeploymentSettings()
	{
		
	}
}
