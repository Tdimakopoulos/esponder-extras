/*
 * 
 */
package eu.esponder.dto.model.datafusion;


import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class DatafusionResultsDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id","Rulename","ResultsText1","ResultsText2","ResultsText3","ResultsText4","ResultsText5","ResultsText6",
	"SourceClass","SourceEvent","Details1","Details2","Details3","Details4","DFDate"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class DatafusionResultsDTO extends ESponderEntityDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6116685838062316408L;

	
	/** The Rulename. */
	protected String Rulename;
	
	/** The Results text1.  */
	/** alarm text **/
	protected String ResultsText1;
	
	/** The Results text2. */
	/** FR ID **/
	protected String ResultsText2;
	
	/** The Results text3. */
	/** FR Title **/
	protected String ResultsText3;
	
	/** The Results text4. */
	protected String ResultsText4;
	
	/** The Results text5. */
	protected String ResultsText5;
	
	/** The Results text6. */
	protected String ResultsText6;
	
	/** The Source class. */
	protected String SourceClass;
	
	/** The Source event. */
	protected String SourceEvent;
	
	/** The Details1. */
	protected String Details1;
	
	/** The Details2. */
	protected String Details2;
	
	/** The Details3. */
	protected String Details3;
	
	/** The Details4. */
	protected String Details4;
	
	/** The DF date. */
	protected Long DFDate;
	
	
	/**
	 * Gets the rulename.
	 *
	 * @return the rulename
	 */
	public String getRulename() {
		return Rulename;
	}

	/**
	 * Sets the rulename.
	 *
	 * @param rulename the new rulename
	 */
	public void setRulename(String rulename) {
		Rulename = rulename;
	}

	/**
	 * Gets the results text1.
	 *
	 * @return the results text1
	 */
	public String getResultsText1() {
		return ResultsText1;
	}

	/**
	 * Sets the results text1.
	 *
	 * @param resultsText1 the new results text1
	 */
	public void setResultsText1(String resultsText1) {
		ResultsText1 = resultsText1;
	}

	/**
	 * Gets the results text2.
	 *
	 * @return the results text2
	 */
	public String getResultsText2() {
		return ResultsText2;
	}

	/**
	 * Sets the results text2.
	 *
	 * @param resultsText2 the new results text2
	 */
	public void setResultsText2(String resultsText2) {
		ResultsText2 = resultsText2;
	}

	/**
	 * Gets the results text3.
	 *
	 * @return the results text3
	 */
	public String getResultsText3() {
		return ResultsText3;
	}

	/**
	 * Sets the results text3.
	 *
	 * @param resultsText3 the new results text3
	 */
	public void setResultsText3(String resultsText3) {
		ResultsText3 = resultsText3;
	}

	/**
	 * Gets the results text4.
	 *
	 * @return the results text4
	 */
	public String getResultsText4() {
		return ResultsText4;
	}

	/**
	 * Sets the results text4.
	 *
	 * @param resultsText4 the new results text4
	 */
	public void setResultsText4(String resultsText4) {
		ResultsText4 = resultsText4;
	}

	/**
	 * Gets the results text5.
	 *
	 * @return the results text5
	 */
	public String getResultsText5() {
		return ResultsText5;
	}

	/**
	 * Sets the results text5.
	 *
	 * @param resultsText5 the new results text5
	 */
	public void setResultsText5(String resultsText5) {
		ResultsText5 = resultsText5;
	}

	/**
	 * Gets the results text6.
	 *
	 * @return the results text6
	 */
	public String getResultsText6() {
		return ResultsText6;
	}

	/**
	 * Sets the results text6.
	 *
	 * @param resultsText6 the new results text6
	 */
	public void setResultsText6(String resultsText6) {
		ResultsText6 = resultsText6;
	}

	/**
	 * Gets the source class.
	 *
	 * @return the source class
	 */
	public String getSourceClass() {
		return SourceClass;
	}

	/**
	 * Sets the source class.
	 *
	 * @param sourceClass the new source class
	 */
	public void setSourceClass(String sourceClass) {
		SourceClass = sourceClass;
	}

	/**
	 * Gets the source event.
	 *
	 * @return the source event
	 */
	public String getSourceEvent() {
		return SourceEvent;
	}

	/**
	 * Sets the source event.
	 *
	 * @param sourceEvent the new source event
	 */
	public void setSourceEvent(String sourceEvent) {
		SourceEvent = sourceEvent;
	}

	/**
	 * Gets the details1.
	 *
	 * @return the details1
	 */
	public String getDetails1() {
		return Details1;
	}

	/**
	 * Sets the details1.
	 *
	 * @param details1 the new details1
	 */
	public void setDetails1(String details1) {
		Details1 = details1;
	}

	/**
	 * Gets the details2.
	 *
	 * @return the details2
	 */
	public String getDetails2() {
		return Details2;
	}

	/**
	 * Sets the details2.
	 *
	 * @param details2 the new details2
	 */
	public void setDetails2(String details2) {
		Details2 = details2;
	}

	/**
	 * Gets the details3.
	 *
	 * @return the details3
	 */
	public String getDetails3() {
		return Details3;
	}

	/**
	 * Sets the details3.
	 *
	 * @param details3 the new details3
	 */
	public void setDetails3(String details3) {
		Details3 = details3;
	}

	/**
	 * Gets the details4.
	 *
	 * @return the details4
	 */
	public String getDetails4() {
		return Details4;
	}

	/**
	 * Sets the details4.
	 *
	 * @param details4 the new details4
	 */
	public void setDetails4(String details4) {
		Details4 = details4;
	}

	/**
	 * Gets the dF date.
	 *
	 * @return the dF date
	 */
	public Long getDFDate() {
		return DFDate;
	}

	/**
	 * Sets the dF date.
	 *
	 * @param dFDate the new dF date
	 */
	public void setDFDate(Long dFDate) {
		DFDate = dFDate;
	}

	
	
}
