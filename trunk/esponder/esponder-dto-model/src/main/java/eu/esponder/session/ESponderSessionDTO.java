/*
 * 
 */
package eu.esponder.session;




import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;



// TODO: Auto-generated Javadoc
/**
 * The Class ESponderUser. Entity that manage the esponderuser, esponder user is
 * system users, for portal, optimizer etc.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"pkikey","username","esponderuserID","IMEI","sessionID","userip", "lastActiveDate", "loginDate","sType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ESponderSessionDTO extends ResourceDTO {

	private static final long serialVersionUID = 5412170181914531716L;

	Long esponderuserID;

	String IMEI;

	String pkikey;
	
	String username;
	
	String sessionID;
	
	String userip;
	
	String lastActiveDate;
	
	String loginDate;
	
	LoginTypeDTO sType;
	
	
	
	public LoginTypeDTO getsType() {
		return sType;
	}



	public void setsType(LoginTypeDTO sType) {
		this.sType = sType;
	}



	public Long getEsponderuserID() {
		return esponderuserID;
	}



	public void setEsponderuserID(Long esponderuserID) {
		this.esponderuserID = esponderuserID;
	}



	public String getIMEI() {
		return IMEI;
	}



	public void setIMEI(String iMEI) {
		IMEI = iMEI;
	}



	public String getPkikey() {
		return pkikey;
	}



	public void setPkikey(String pkikey) {
		this.pkikey = pkikey;
	}



	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}



	public String getSessionID() {
		return sessionID;
	}



	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}



	public String getUserip() {
		return userip;
	}



	public void setUserip(String userip) {
		this.userip = userip;
	}



	public String getLastActiveDate() {
		return lastActiveDate;
	}



	public void setLastActiveDate(String lastActiveDate) {
		this.lastActiveDate = lastActiveDate;
	}



	public String getLoginDate() {
		return loginDate;
	}



	public void setLoginDate(String loginDate) {
		this.loginDate = loginDate;
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id.toString();
	}


	

}
