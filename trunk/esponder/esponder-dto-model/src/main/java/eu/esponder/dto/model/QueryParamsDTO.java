/*
 * 
 */
package eu.esponder.dto.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class ResultListDTO.
 * When a service return a list of DTOs, we use the ResultListDTO return type.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({ "QueryParamList" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class QueryParamsDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -363245325332031836L;

	/** The result list. */
	//private List<String> QueryParamList;

	/** The parameters. */
	private HashMap<String,Object> QueryParamList = null;

	/**
	 * Instantiates a new query parameter.
	 *
	 * @param name the name
	 * @param value the value
	 */
	private QueryParamsDTO(String name,Object value){
		this.QueryParamList = new HashMap<String,Object>();
		this.QueryParamList.put(name, value);
	}
	
	/**
	 * With.
	 *
	 * @param name the name
	 * @param value the value
	 * @return the query parameter
	 */
	public static QueryParamsDTO with(String name,Object value){
		return new QueryParamsDTO(name, value);
	}
	
	/**
	 * And.
	 *
	 * @param name the name
	 * @param value the value
	 * @return the query parameter
	 */
	public QueryParamsDTO and(String name,Object value){
		this.QueryParamList.put(name, value);
		return this;
	}
	
	/**
	 * Parameters.
	 *
	 * @return the map
	 */
	public Map<String,Object> parameters(){
		return this.QueryParamList;
	}
	/**
	 * Instantiates a new result list dto.
	 */
	public QueryParamsDTO() {
	}


	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String result = "[QueryParamList:";
		result=result+QueryParamList.toString();
		result += "]";
		return result;
	}

	public HashMap<String, Object> getQueryParamList() {
		return QueryParamList;
	}

	public void setQueryParamList(HashMap<String, Object> queryParamList) {
		QueryParamList = queryParamList;
	}

}
