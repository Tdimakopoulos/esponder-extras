/*
 * 
 */
package eu.esponder.dto.model.crisis.resource.sensor;

import org.codehaus.jackson.annotate.JsonTypeInfo;


// TODO: Auto-generated Javadoc
/**
 * The Class EnvironmentalSensorDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class EnvironmentalSensorDTO extends SensorDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3878737094263093312L;
	
	static String label="EnvironmentalSensor";
	
	public static String getLabel() {
		return label;
	}

	public static void setLabel(String label) {
		LPSSensorDTO.label = label;
	}



}
