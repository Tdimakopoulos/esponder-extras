/*
 * 
 */
package eu.esponder.dto.model.snapshot.location;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class SphereDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "centre", "radius"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class SphereDTO extends LocationAreaDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9091311951839192515L;

	/**
	 * Instantiates a new sphere dto.
	 */
	public SphereDTO() { }
	
	/**
	 * Instantiates a new sphere dto.
	 *
	 * @param centre the centre
	 * @param radius the radius
	 * @param title the title
	 */
	public SphereDTO(PointDTO centre, BigDecimal radius, String title) {
		super();
		this.centre = centre;
		this.radius = radius;
		this.setTitle(title);
	}
	
	/** The centre. */
	private PointDTO centre;
	
	/** The radius. */
	private BigDecimal radius;

	/**
	 * Gets the centre.
	 *
	 * @return the centre
	 */
	public PointDTO getCentre() {
		return centre;
	}

	/**
	 * Sets the centre.
	 *
	 * @param centre the new centre
	 */
	public void setCentre(PointDTO centre) {
		this.centre = centre;
	}

	/**
	 * Gets the radius.
	 *
	 * @return the radius
	 */
	public BigDecimal getRadius() {
		return radius;
	}

	/**
	 * Sets the radius.
	 *
	 * @param radius the new radius
	 */
	public void setRadius(BigDecimal radius) {
		this.radius = radius;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SphereDTO [centre=" + centre + ", radius=" + radius + "]";
	}
	
}
