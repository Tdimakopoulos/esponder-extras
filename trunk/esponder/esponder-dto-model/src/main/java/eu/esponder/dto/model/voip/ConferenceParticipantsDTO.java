package eu.esponder.dto.model.voip;



import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;




@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id","voipConference","actorID","ipstatus"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ConferenceParticipantsDTO extends ESponderEntityDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2758387616684723634L;
	
	/** The id. */
	
	protected Long id;
	

	private VoipConferenceDTO voipConference;
	
	Long actorID;
	
	int ipstatus;
	
	
	
	public VoipConferenceDTO getVoipConference() {
		return voipConference;
	}

	public void setVoipConference(VoipConferenceDTO voipConference) {
		this.voipConference = voipConference;
	}

	public Long getActorID() {
		return actorID;
	}

	public void setActorID(Long actorID) {
		this.actorID = actorID;
	}

	public int getIpstatus() {
		return ipstatus;
	}

	public void setIpstatus(int ipstatus) {
		this.ipstatus = ipstatus;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}
}
