/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class OCMeocALTDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "resourceId", "title", "supervisingOC",
		"incidentCommander", "subordinateTeams", "meocCrisisContext",
		"snapshots" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class OCMeocALTDTO extends OperationsCentreALTDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6001309075526460044L;

	/** The supervising oc. */
	private Long supervisingOC;

	/** The incident commander. */
	private ActorICALTDTO incidentCommander;

	/** The meoc crisis context. */
	private Long meocCrisisContext;

	/** The snapshots. */
	private OperationsCentreSnapshotDTO snapshot;

	// extras
	private String dlnaip;
	private String dlnaprop1;
	private String dlnaprop2;
	private String dlnaprop3;
	private String dlnaprop4;
	private String sIPAddress;
	private String staip;
	private String wimaxip;
	private String wifiip;
	private String meoc3gip;

	private String sIPusername;
	private String sIPpassword;
	private String sIPaddressEOC;

	private String sIPaddresssrv;
	private String sIPaddresscli;

	public String getDlnaip() {
		return dlnaip;
	}

	public void setDlnaip(String dlnaip) {
		this.dlnaip = dlnaip;
	}

	public String getDlnaprop1() {
		return dlnaprop1;
	}

	public void setDlnaprop1(String dlnaprop1) {
		this.dlnaprop1 = dlnaprop1;
	}

	public String getDlnaprop2() {
		return dlnaprop2;
	}

	public void setDlnaprop2(String dlnaprop2) {
		this.dlnaprop2 = dlnaprop2;
	}

	public String getDlnaprop3() {
		return dlnaprop3;
	}

	public void setDlnaprop3(String dlnaprop3) {
		this.dlnaprop3 = dlnaprop3;
	}

	public String getDlnaprop4() {
		return dlnaprop4;
	}

	public void setDlnaprop4(String dlnaprop4) {
		this.dlnaprop4 = dlnaprop4;
	}

	public String getsIPAddress() {
		return sIPAddress;
	}

	public void setsIPAddress(String sIPAddress) {
		this.sIPAddress = sIPAddress;
	}

	public String getStaip() {
		return staip;
	}

	public void setStaip(String staip) {
		this.staip = staip;
	}

	public String getWimaxip() {
		return wimaxip;
	}

	public void setWimaxip(String wimaxip) {
		this.wimaxip = wimaxip;
	}

	public String getWifiip() {
		return wifiip;
	}

	public void setWifiip(String wifiip) {
		this.wifiip = wifiip;
	}

	public String getMeoc3gip() {
		return meoc3gip;
	}

	public void setMeoc3gip(String meoc3gip) {
		this.meoc3gip = meoc3gip;
	}

	public String getsIPusername() {
		return sIPusername;
	}

	public void setsIPusername(String sIPusername) {
		this.sIPusername = sIPusername;
	}

	public String getsIPpassword() {
		return sIPpassword;
	}

	public void setsIPpassword(String sIPpassword) {
		this.sIPpassword = sIPpassword;
	}

	public String getsIPaddressEOC() {
		return sIPaddressEOC;
	}

	public void setsIPaddressEOC(String sIPaddressEOC) {
		this.sIPaddressEOC = sIPaddressEOC;
	}

	public String getsIPaddresssrv() {
		return sIPaddresssrv;
	}

	public void setsIPaddresssrv(String sIPaddresssrv) {
		this.sIPaddresssrv = sIPaddresssrv;
	}

	public String getsIPaddresscli() {
		return sIPaddresscli;
	}

	public void setsIPaddresscli(String sIPaddresscli) {
		this.sIPaddresscli = sIPaddresscli;
	}

	/**
	 * Gets the meoc crisis context.
	 * 
	 * @return the meoc crisis context
	 */
	public Long getMeocCrisisContext() {
		return meocCrisisContext;
	}

	/**
	 * Gets the supervising oc.
	 * 
	 * @return the supervising oc
	 */
	public Long getSupervisingOC() {
		return supervisingOC;
	}

	/**
	 * Sets the supervising oc.
	 * 
	 * @param supervisingOC
	 *            the new supervising oc
	 */
	public void setSupervisingOC(Long supervisingOC) {
		this.supervisingOC = supervisingOC;
	}

	/**
	 * Gets the incident commander.
	 * 
	 * @return the incident commander
	 */
	public ActorICALTDTO getIncidentCommander() {
		return incidentCommander;
	}

	/**
	 * Sets the incident commander.
	 * 
	 * @param incidentCommander
	 *            the new incident commander
	 */
	public void setIncidentCommander(ActorICALTDTO incidentCommander) {
		this.incidentCommander = incidentCommander;
	}

	/**
	 * Sets the meoc crisis context.
	 * 
	 * @param meocCrisisContext
	 *            the new meoc crisis context
	 */
	public void setMeocCrisisContext(Long meocCrisisContext) {
		this.meocCrisisContext = meocCrisisContext;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.dto.model.crisis.alternative.OperationsCentreALTDTO#getSnapshots
	 * ()
	 */
	/**
	 * Gets the snapshots.
	 * 
	 * @return the snapshots
	 */
	public OperationsCentreSnapshotDTO getSnapshot() {
		return snapshot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.dto.model.crisis.alternative.OperationsCentreALTDTO#setSnapshots
	 * (java.util.Set)
	 */
	/**
	 * Sets the snapshots.
	 * 
	 * @param snapshots
	 *            the new snapshots
	 */
	public void setSnapshot(OperationsCentreSnapshotDTO snapshot) {
		this.snapshot = snapshot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.dto.model.crisis.alternative.OperationsCentreALTDTO#toString
	 * ()
	 */
	@Override
	public String toString() {
		return this.getClass().getName() + " [id=" + id + ", title=" + title
				+ "]";
	}

}
