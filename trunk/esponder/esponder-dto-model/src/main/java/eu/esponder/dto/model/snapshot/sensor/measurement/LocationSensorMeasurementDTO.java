/*
 * 
 */
package eu.esponder.dto.model.snapshot.sensor.measurement;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.location.PointDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class LocationSensorMeasurementDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "sensor", "timestamp", "point"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class LocationSensorMeasurementDTO extends SensorMeasurementDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6520366734281109172L;

	/** The point. */
	private PointDTO point;

	/**
	 * Gets the point.
	 *
	 * @return the point
	 */
	public PointDTO getPoint() {
		return point;
	}

	/**
	 * Sets the point.
	 *
	 * @param point the new point
	 */
	public void setPoint(PointDTO point) {
		this.point = point;
	}

}
