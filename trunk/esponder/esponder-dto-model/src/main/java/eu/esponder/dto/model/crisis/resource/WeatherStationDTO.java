package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;


@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "title", "status", "operationsCentre", "temperature", "evaporation",
		"barometricpressure", "windspeed", "winddirection",
		"windgust","relativehumidity",
		"solarradiation","anemometer",
		"rain_gauge","disdrometer",
		"transmissometer","ceilometer"})
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class WeatherStationDTO extends ESponderEntityDTO {

	private static final long serialVersionUID = 7509967835019265806L;
	
	protected String title;

	protected ResourceStatusDTO status;

	private OCMeocDTO operationsCentre;

	protected String temperature;
	
	protected String evaporation;
	
	protected String barometricpressure;
	
	protected String windspeed;
	
	protected String winddirection;
	
	protected String windgust;
	
	protected String relativehumidity;
	
	protected String solarradiation;
	
	protected String anemometer;
	
	protected String rain_gauge;
	
	protected String disdrometer;
	
	protected String transmissometer;
	
	protected String ceilometer;

	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ResourceStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ResourceStatusDTO status) {
		this.status = status;
	}

	public OCMeocDTO getOperationsCentre() {
		return operationsCentre;
	}

	public void setOperationsCentre(OCMeocDTO operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getEvaporation() {
		return evaporation;
	}

	public void setEvaporation(String evaporation) {
		this.evaporation = evaporation;
	}

	public String getBarometricpressure() {
		return barometricpressure;
	}

	public void setBarometricpressure(String barometricpressure) {
		this.barometricpressure = barometricpressure;
	}

	public String getWindspeed() {
		return windspeed;
	}

	public void setWindspeed(String windspeed) {
		this.windspeed = windspeed;
	}

	public String getWinddirection() {
		return winddirection;
	}

	public void setWinddirection(String winddirection) {
		this.winddirection = winddirection;
	}

	public String getWindgust() {
		return windgust;
	}

	public void setWindgust(String windgust) {
		this.windgust = windgust;
	}

	public String getRelativehumidity() {
		return relativehumidity;
	}

	public void setRelativehumidity(String relativehumidity) {
		this.relativehumidity = relativehumidity;
	}

	public String getSolarradiation() {
		return solarradiation;
	}

	public void setSolarradiation(String solarradiation) {
		this.solarradiation = solarradiation;
	}

	public String getAnemometer() {
		return anemometer;
	}

	public void setAnemometer(String anemometer) {
		this.anemometer = anemometer;
	}

	public String getRain_gauge() {
		return rain_gauge;
	}

	public void setRain_gauge(String rain_gauge) {
		this.rain_gauge = rain_gauge;
	}

	public String getDisdrometer() {
		return disdrometer;
	}

	public void setDisdrometer(String disdrometer) {
		this.disdrometer = disdrometer;
	}

	public String getTransmissometer() {
		return transmissometer;
	}

	public void setTransmissometer(String transmissometer) {
		this.transmissometer = transmissometer;
	}

	public String getCeilometer() {
		return ceilometer;
	}

	public void setCeilometer(String ceilometer) {
		this.ceilometer = ceilometer;
	}
	
	
	

}
