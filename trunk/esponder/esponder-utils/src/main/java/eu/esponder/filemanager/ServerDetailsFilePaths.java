package eu.esponder.filemanager;

import java.io.File;

public class ServerDetailsFilePaths {

	private String szPropertiesFileNameUnix = "//home//exodus//osgi//serverdetails.set";
	private String szPropertiesFileNameWindows = "C://Development//serverdetails.set";

	/**
	 * Checks if is unix.
	 *
	 * @return true, if is unix
	 */
	private boolean isUnix() {
		if (File.separatorChar == '/')
			return true;
		else
			return false;
	}
	
	public String GetFilename()
	{
		if(isUnix())
			return szPropertiesFileNameUnix;
		else
			return szPropertiesFileNameWindows;
	}
	
}
