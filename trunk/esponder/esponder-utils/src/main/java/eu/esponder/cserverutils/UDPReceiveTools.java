package eu.esponder.cserverutils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UDPReceiveTools {

	public String removeFirstChar(String s) {
		return s.substring(1);
	}

	public String Receive(int port) throws SocketException, IOException {

		// Create a socket to listen on the port.
		DatagramSocket dsocket = new DatagramSocket(port);

		// Create a buffer to read datagrams into. If anyone sends us a
		// packet containing more than will fit into this buffer, the excess
		// will simply be discarded!
		byte[] buffer = new byte[2048];

		// Create a packet with an empty buffer to receive data
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

		// Wait to receive a datagram
		dsocket.receive(packet);

		// Convert the contents to a string, and display them
		String msg = new String(buffer, 0, packet.getLength());

		return msg;

	}
}
