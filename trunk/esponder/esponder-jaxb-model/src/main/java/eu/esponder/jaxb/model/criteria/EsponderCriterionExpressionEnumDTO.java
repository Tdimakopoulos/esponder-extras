package eu.esponder.jaxb.model.criteria;

public enum EsponderCriterionExpressionEnumDTO {

	EQUAL,
	GREATER_THAN_OR_EQUAL,
	GREATER_THAN,
	LESS_THAN,
	LESS_THAN_OR_EQUAL,
	NOT
	
}
