package eu.esponder.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.dto.model.type.ConsumableResourceTypeDTO;
import eu.esponder.dto.model.type.CrisisDisasterTypeDTO;
import eu.esponder.dto.model.type.CrisisFeatureTypeDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.dto.model.type.EmergencyOperationsCentreTypeDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.dto.model.type.InitialActorTypeDTO;
import eu.esponder.dto.model.type.MobileEmergencyOperationsCentreTypeDTO;
import eu.esponder.dto.model.type.OperationalActionTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.dto.model.type.PersonnelSkillTypeDTO;
import eu.esponder.dto.model.type.PersonnelTrainingTypeDTO;
import eu.esponder.dto.model.type.RankTypeDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;
import eu.esponder.dto.model.type.SensorTypeDTO;
import eu.esponder.dto.model.type.StrategicActionTypeDTO;
import eu.esponder.dto.model.type.TacticalActionTypeDTO;
import eu.esponder.model.snapshot.status.MeasurementUnitEnum;
import eu.esponder.model.type.EmergencyOperationsCentreType;
import eu.esponder.model.type.EquipmentType;
import eu.esponder.model.type.InitialActorType;
import eu.esponder.model.type.MobileEmergencyOperationsCentreType;
import eu.esponder.model.type.SensorType;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class TypeServiceTest extends ControllerServiceTest {

	@Test(groups = "createBasics")
	public void testCreateTypes() throws ClassNotFoundException {

		InitialActorType initActorType = new InitialActorType();
		initActorType.setTitle("INIT_ACTOR");
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapObject(initActorType, InitialActorTypeDTO.class),
				this.userID);

		// Operations Centers
		EmergencyOperationsCentreType eoc = new EmergencyOperationsCentreType();
		eoc.setTitle("EOC");
		typeService.createTypeRemote(
				(ESponderTypeDTO) mappingService.mapESponderEntity(eoc,
						EmergencyOperationsCentreTypeDTO.class), this.userID);

		MobileEmergencyOperationsCentreType meoc = new MobileEmergencyOperationsCentreType();
		meoc.setTitle("MEOC");
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(meoc,
						MobileEmergencyOperationsCentreTypeDTO.class),
				this.userID);

		// Equipment
		EquipmentType wimax = new EquipmentType();
		wimax.setTitle("FRU WiMAX");
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(wimax, EquipmentTypeDTO.class), this.userID);

		EquipmentType wifi = new EquipmentType();
		wifi.setTitle("FRU WiFi");
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(wifi, EquipmentTypeDTO.class), this.userID);

		// Sensors
		
		SensorType activitySensorType = new SensorType();
		activitySensorType.setTitle("ACT");
		activitySensorType.setMeasurementUnit(MeasurementUnitEnum.BPM);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(activitySensorType, SensorTypeDTO.class),
				this.userID);
		
		SensorType bodyTempSensorType = new SensorType();
		bodyTempSensorType.setTitle("BODY_TEMP");
		bodyTempSensorType
				.setMeasurementUnit(MeasurementUnitEnum.DEGREES_CELCIUS);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(bodyTempSensorType, SensorTypeDTO.class),
				this.userID);
		
		SensorType breathrateSensorType = new SensorType();
		breathrateSensorType.setTitle("BR");
		breathrateSensorType.setMeasurementUnit(MeasurementUnitEnum.BPM);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(breathrateSensorType, SensorTypeDTO.class),
				this.userID);
		
		SensorType heartbeatSensorType = new SensorType();
		heartbeatSensorType.setTitle("HB");
		heartbeatSensorType.setMeasurementUnit(MeasurementUnitEnum.BPM);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(heartbeatSensorType, SensorTypeDTO.class),
				this.userID);

		SensorType envTempSensorType = new SensorType();
		envTempSensorType.setTitle("ENV_TEMP");
		envTempSensorType
				.setMeasurementUnit(MeasurementUnitEnum.DEGREES_CELCIUS);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(envTempSensorType, SensorTypeDTO.class),
				this.userID);
		
		SensorType gasSensorType = new SensorType();
		gasSensorType.setTitle("GAS");
		gasSensorType
				.setMeasurementUnit(MeasurementUnitEnum.BPM);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(gasSensorType, SensorTypeDTO.class),
				this.userID);
		
		SensorType methaneSensorType = new SensorType();
		methaneSensorType.setTitle("METHANE");
		methaneSensorType
				.setMeasurementUnit(MeasurementUnitEnum.PPM);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(methaneSensorType, SensorTypeDTO.class),
				this.userID);
		
		SensorType carbonMonoxideSensorType = new SensorType();
		carbonMonoxideSensorType.setTitle("CARBONMONOXIDE");
		carbonMonoxideSensorType
				.setMeasurementUnit(MeasurementUnitEnum.PPM);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(carbonMonoxideSensorType, SensorTypeDTO.class),
				this.userID);
		
		SensorType carbonDioxideSensorType = new SensorType();
		carbonDioxideSensorType.setTitle("CARBONDIOXIDE");
		carbonDioxideSensorType
				.setMeasurementUnit(MeasurementUnitEnum.PPM);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(carbonDioxideSensorType, SensorTypeDTO.class),
				this.userID);
		
		SensorType oxygenSensorType = new SensorType();
		oxygenSensorType.setTitle("OXYGEN");
		oxygenSensorType
				.setMeasurementUnit(MeasurementUnitEnum.PPM);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(oxygenSensorType, SensorTypeDTO.class),
				this.userID);
		
		SensorType hydrogenSulfideSensorType = new SensorType();
		hydrogenSulfideSensorType.setTitle("HYDROGENSULFIDE");
		hydrogenSulfideSensorType
				.setMeasurementUnit(MeasurementUnitEnum.PPM);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(hydrogenSulfideSensorType, SensorTypeDTO.class),
				this.userID);
		
		SensorType lpsSensorType = new SensorType();
		lpsSensorType.setTitle("LPS");
		lpsSensorType
				.setMeasurementUnit(MeasurementUnitEnum.DEGREES);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(lpsSensorType, SensorTypeDTO.class),
				this.userID);

		SensorType locationSensorType = new SensorType();
		locationSensorType.setTitle("LOCATION");
		locationSensorType.setMeasurementUnit(MeasurementUnitEnum.DEGREES);
		typeService.createTypeRemote((ESponderTypeDTO) mappingService
				.mapESponderEntity(locationSensorType, SensorTypeDTO.class),
				this.userID);

		// Actions
		StrategicActionTypeDTO strategicActionTypeDTO = new StrategicActionTypeDTO();
		strategicActionTypeDTO.setTitle("StratActionType");
		typeService.createTypeRemote(strategicActionTypeDTO, this.userID);

		TacticalActionTypeDTO tacticalActionTypeDTO = new TacticalActionTypeDTO();
		tacticalActionTypeDTO.setTitle("TactActionType");
		typeService.createTypeRemote(tacticalActionTypeDTO, this.userID);

		OperationalActionTypeDTO opActionTypeDTO = new OperationalActionTypeDTO();
		opActionTypeDTO.setTitle("OpActionType");
		typeService.createTypeRemote(opActionTypeDTO, this.userID);

		// Consumable Resources
		ConsumableResourceTypeDTO crType1 = new ConsumableResourceTypeDTO();
		crType1.setTitle("Drink");
		typeService.createTypeRemote(crType1, this.userID);

		ConsumableResourceTypeDTO crType2 = new ConsumableResourceTypeDTO();
		crType2.setTitle("Food");
		typeService.createTypeRemote(crType2, this.userID);

		ConsumableResourceTypeDTO crType3 = new ConsumableResourceTypeDTO();
		crType3.setTitle("Medical Resource");
		typeService.createTypeRemote(crType3, this.userID);

		// Reusable Resources
		ReusableResourceTypeDTO rrType1 = new ReusableResourceTypeDTO();
		rrType1.setTitle("Water Container");
		typeService.createTypeRemote(rrType1, this.userID);

		ReusableResourceTypeDTO rrType2 = new ReusableResourceTypeDTO();
		rrType2.setTitle("Food package");
		typeService.createTypeRemote(rrType2, this.userID);

		ReusableResourceTypeDTO rrType3 = new ReusableResourceTypeDTO();
		rrType3.setTitle("Medical Kit");
		typeService.createTypeRemote(rrType3, this.userID);

		ReusableResourceTypeDTO rrType4 = new ReusableResourceTypeDTO();
		rrType4.setTitle("Tools");
		typeService.createTypeRemote(rrType4, this.userID);

		// Organisations
		OrganisationTypeDTO organisationTypeDTO1 = new OrganisationTypeDTO();
		organisationTypeDTO1.setTitle("Headquarters");
		typeService.createTypeRemote(organisationTypeDTO1, this.userID);

		OrganisationTypeDTO organisationTypeDTO2 = new OrganisationTypeDTO();
		organisationTypeDTO2.setTitle("Local Station");
		typeService.createTypeRemote(organisationTypeDTO2, this.userID);

		OrganisationTypeDTO organisationTypeDTO233 = new OrganisationTypeDTO();
		organisationTypeDTO233.setTitle("POI");
		typeService.createTypeRemote(organisationTypeDTO233, this.userID);

		// Disciplines
		DisciplineTypeDTO disciplineTypeDTO1 = new DisciplineTypeDTO();
		disciplineTypeDTO1.setTitle("Police Force");
		typeService.createTypeRemote(disciplineTypeDTO1, this.userID);

		DisciplineTypeDTO disciplineTypeDTO2 = new DisciplineTypeDTO();
		disciplineTypeDTO2.setTitle("Fire Brigade");
		typeService.createTypeRemote(disciplineTypeDTO2, this.userID);

		DisciplineTypeDTO disciplineTypeDTO3 = new DisciplineTypeDTO();
		disciplineTypeDTO3.setTitle("Coast Guard");
		typeService.createTypeRemote(disciplineTypeDTO3, this.userID);

		DisciplineTypeDTO disciplineTypeDTO4 = new DisciplineTypeDTO();
		disciplineTypeDTO4.setTitle("Army");
		typeService.createTypeRemote(disciplineTypeDTO4, this.userID);

		DisciplineTypeDTO disciplineTypeDTO4a = new DisciplineTypeDTO();
		disciplineTypeDTO4a.setTitle("Ambulance Post");
		typeService.createTypeRemote(disciplineTypeDTO4a, this.userID);
		
		DisciplineTypeDTO disciplineTypeDTO4s = new DisciplineTypeDTO();
		disciplineTypeDTO4s.setTitle("Hospital");
		typeService.createTypeRemote(disciplineTypeDTO4s, this.userID);
		
		DisciplineTypeDTO disciplineTypeDTO4d = new DisciplineTypeDTO();
		disciplineTypeDTO4d.setTitle("Airport");
		typeService.createTypeRemote(disciplineTypeDTO4d, this.userID);
		
		DisciplineTypeDTO disciplineTypeDTO4f = new DisciplineTypeDTO();
		disciplineTypeDTO4f.setTitle("Train Station");
		typeService.createTypeRemote(disciplineTypeDTO4f, this.userID);
		
		DisciplineTypeDTO disciplineTypeDTO4g = new DisciplineTypeDTO();
		disciplineTypeDTO4g.setTitle("Park");
		typeService.createTypeRemote(disciplineTypeDTO4g, this.userID);
		
		DisciplineTypeDTO disciplineTypeDTO4h = new DisciplineTypeDTO();
		disciplineTypeDTO4h.setTitle("Goverment Building");
		typeService.createTypeRemote(disciplineTypeDTO4h, this.userID);
		
		DisciplineTypeDTO disciplineTypeDTO4j = new DisciplineTypeDTO();
		disciplineTypeDTO4j.setTitle("Hotel");
		typeService.createTypeRemote(disciplineTypeDTO4j, this.userID);
		
		DisciplineTypeDTO disciplineTypeDTO4k = new DisciplineTypeDTO();
		disciplineTypeDTO4k.setTitle("Museum");
		typeService.createTypeRemote(disciplineTypeDTO4k, this.userID);
		
		DisciplineTypeDTO disciplineTypeDTO4l = new DisciplineTypeDTO();
		disciplineTypeDTO4l.setTitle("Camping");
		typeService.createTypeRemote(disciplineTypeDTO4l, this.userID);
		
		DisciplineTypeDTO disciplineTypeDTO4q = new DisciplineTypeDTO();
		disciplineTypeDTO4q.setTitle("Shopping Center");
		typeService.createTypeRemote(disciplineTypeDTO4q, this.userID);
		

		// Rank Types
		RankTypeDTO rankTypeDTO1 = new RankTypeDTO();
		rankTypeDTO1.setTitle("RankType1");
		typeService.createTypeRemote(rankTypeDTO1, this.userID);

		RankTypeDTO rankTypeDTO2 = new RankTypeDTO();
		rankTypeDTO2.setTitle("RankType2");
		typeService.createTypeRemote(rankTypeDTO2, this.userID);

		RankTypeDTO rankTypeDTO3 = new RankTypeDTO();
		rankTypeDTO3.setTitle("RankType3");
		typeService.createTypeRemote(rankTypeDTO3, this.userID);
		
		RankTypeDTO rankTypeDTO4 = new RankTypeDTO();
		rankTypeDTO4.setTitle("RankType4");
		typeService.createTypeRemote(rankTypeDTO4, this.userID);
		
		RankTypeDTO rankTypeDTO5 = new RankTypeDTO();
		rankTypeDTO5.setTitle("RankType5");
		typeService.createTypeRemote(rankTypeDTO5, this.userID);
		
		RankTypeDTO rankTypeDTO6 = new RankTypeDTO();
		rankTypeDTO6.setTitle("RankType6");
		typeService.createTypeRemote(rankTypeDTO6, this.userID);
		
		
		RankTypeDTO rankTypeDTO7 = new RankTypeDTO();
		rankTypeDTO7.setTitle("RankType7");
		typeService.createTypeRemote(rankTypeDTO7, this.userID);
		
		
		RankTypeDTO rankTypeDTO8 = new RankTypeDTO();
		rankTypeDTO8.setTitle("RankType8");
		typeService.createTypeRemote(rankTypeDTO8, this.userID);

		// Personnel Skill Types
		PersonnelSkillTypeDTO personnelSkillTypeDTO1 = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO1.setTitle("Fire Truck Driving");
		typeService.createTypeRemote(personnelSkillTypeDTO1, this.userID);

		PersonnelSkillTypeDTO personnelSkillTypeDTO2 = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO2.setTitle("Crane Operation Handling");
		typeService.createTypeRemote(personnelSkillTypeDTO2, this.userID);

		PersonnelSkillTypeDTO personnelSkillTypeDTO3 = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO3.setTitle("Firewall Planning");
		typeService.createTypeRemote(personnelSkillTypeDTO3, this.userID);

		PersonnelSkillTypeDTO personnelSkillTypeDTO3c = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO3c.setTitle("Paramedic");
		typeService.createTypeRemote(personnelSkillTypeDTO3c, this.userID);

		PersonnelSkillTypeDTO personnelSkillTypeDTO3d = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO3d.setTitle("Doctor");
		typeService.createTypeRemote(personnelSkillTypeDTO3d, this.userID);

		PersonnelSkillTypeDTO personnelSkillTypeDTO3e = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO3e.setTitle("Police Officer");
		typeService.createTypeRemote(personnelSkillTypeDTO3e, this.userID);

		PersonnelSkillTypeDTO personnelSkillTypeDTO3ee = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO3ee.setTitle("Ambulance Driving");
		typeService.createTypeRemote(personnelSkillTypeDTO3ee, this.userID);
		
		PersonnelSkillTypeDTO personnelSkillTypeDTO3ec = new PersonnelSkillTypeDTO();
		personnelSkillTypeDTO3ec.setTitle("Police Car Driving");
		typeService.createTypeRemote(personnelSkillTypeDTO3ec, this.userID);
		
		// Personnel Training Types
		PersonnelTrainingTypeDTO personnelTrainingTypeDTO1 = new PersonnelTrainingTypeDTO();
		personnelTrainingTypeDTO1.setTitle("Driving");
		typeService.createTypeRemote(personnelTrainingTypeDTO1, this.userID);

		PersonnelTrainingTypeDTO personnelTrainingTypeDTO2 = new PersonnelTrainingTypeDTO();
		personnelTrainingTypeDTO2.setTitle("Tool Handling");
		typeService.createTypeRemote(personnelTrainingTypeDTO2, this.userID);

		PersonnelTrainingTypeDTO personnelTrainingTypeDTO3 = new PersonnelTrainingTypeDTO();
		personnelTrainingTypeDTO3.setTitle("Administration");
		typeService.createTypeRemote(personnelTrainingTypeDTO3, this.userID);

		PersonnelTrainingTypeDTO personnelTrainingTypeDTO32 = new PersonnelTrainingTypeDTO();
		personnelTrainingTypeDTO32.setTitle("Medical");
		typeService.createTypeRemote(personnelTrainingTypeDTO32, this.userID);
		
		// Crisis Disaster Types
		CrisisDisasterTypeDTO crisisDisasterTypeDTO1 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO1.setTitle("Fire");
		typeService.createTypeRemote(crisisDisasterTypeDTO1, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO2 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO2.setTitle("Earthquake");
		typeService.createTypeRemote(crisisDisasterTypeDTO2, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO3 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO3.setTitle("Flood");
		typeService.createTypeRemote(crisisDisasterTypeDTO3, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO4 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO4.setTitle("Road accident");
		typeService.createTypeRemote(crisisDisasterTypeDTO4, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO5 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO5.setTitle("Blizzard");
		typeService.createTypeRemote(crisisDisasterTypeDTO5, this.userID);

		CrisisDisasterTypeDTO crisisDisasterTypeDTO6 = new CrisisDisasterTypeDTO();
		crisisDisasterTypeDTO6.setTitle("Storm");
		typeService.createTypeRemote(crisisDisasterTypeDTO6, this.userID);

		// Crisis Feature Types
		CrisisFeatureTypeDTO crisisFeatureTypeDTO1 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO1.setTitle("Human Loss");
		typeService.createTypeRemote(crisisFeatureTypeDTO1, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO2 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO2.setTitle("Injury");
		typeService.createTypeRemote(crisisFeatureTypeDTO2, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO3 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO3.setTitle("People hemming");
		typeService.createTypeRemote(crisisFeatureTypeDTO3, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO4 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO4.setTitle("Building damage");
		typeService.createTypeRemote(crisisFeatureTypeDTO4, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO5 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO5.setTitle("Substance leak");
		typeService.createTypeRemote(crisisFeatureTypeDTO5, this.userID);

		CrisisFeatureTypeDTO crisisFeatureTypeDTO6 = new CrisisFeatureTypeDTO();
		crisisFeatureTypeDTO6.setTitle("Inaccessible city");
		typeService.createTypeRemote(crisisFeatureTypeDTO6, this.userID);

	}

}
