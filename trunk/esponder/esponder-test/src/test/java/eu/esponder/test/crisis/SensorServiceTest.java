package eu.esponder.test.crisis;

import java.math.BigDecimal;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.sensor.ActivitySensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BreathRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.CarbonDioxideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.CarbonMonoxideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.EnvironmentTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.GasSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HeartBeatRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HydrogenSulfideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LPSSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.MethaneSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.OxygenSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;
import eu.esponder.dto.model.type.SensorTypeDTO;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class SensorServiceTest extends ControllerServiceTest {

	private static int SECONDS = 10;

	@Test(groups="createResources")
	public void testCreateSensorDTO() throws ClassNotFoundException {

		SensorTypeDTO activitySensorType = (SensorTypeDTO) typeService.findDTOByTitle("ACT");
		SensorTypeDTO bodyTempSensorType = (SensorTypeDTO) typeService.findDTOByTitle("BODY_TEMP");
		SensorTypeDTO breathRateSensorType = (SensorTypeDTO) typeService.findDTOByTitle("BR");
		SensorTypeDTO heartbeatSensorType = (SensorTypeDTO) typeService.findDTOByTitle("HB");
		SensorTypeDTO envTempSensorType = (SensorTypeDTO) typeService.findDTOByTitle("ENV_TEMP");
		SensorTypeDTO gasSensorType = (SensorTypeDTO) typeService.findDTOByTitle("GAS");
		SensorTypeDTO methaneSensorType = (SensorTypeDTO) typeService.findDTOByTitle("METHANE");
		SensorTypeDTO carbonMonoxideSensorType = (SensorTypeDTO) typeService.findDTOByTitle("CARBONMONOXIDE");
		SensorTypeDTO carbonDioxideSensorType = (SensorTypeDTO) typeService.findDTOByTitle("CARBONDIOXIDE");
		SensorTypeDTO oxygenSensorType = (SensorTypeDTO) typeService.findDTOByTitle("OXYGEN");
		SensorTypeDTO hydrogenSulfideSensorType = (SensorTypeDTO) typeService.findDTOByTitle("HYDROGENSULFIDE");
		SensorTypeDTO lpsSensorType = (SensorTypeDTO) typeService.findDTOByTitle("LPS");
		SensorTypeDTO locationSensorType = (SensorTypeDTO) typeService.findDTOByTitle("LOCATION");

		if(activitySensorType == null || bodyTempSensorType == null || breathRateSensorType == null || heartbeatSensorType == null || 
				envTempSensorType == null || gasSensorType == null || lpsSensorType == null || locationSensorType == null) {



		}
		else {

			EquipmentDTO firstFRCEquipment = equipmentService.findByTitleRemote("FRU #1");
			EquipmentDTO firstFREquipment = equipmentService.findByTitleRemote("FRU #1.1");
			EquipmentDTO secondFREquipment = equipmentService.findByTitleRemote("FRU #1.2");
//			EquipmentDTO secondFRCEquipment = equipmentService.findByTitleRemote("FRU #2");
			EquipmentDTO thirdFREquipment = equipmentService.findByTitleRemote("FRU #2.1");
			EquipmentDTO fourthFREquipment = equipmentService.findByTitleRemote("FRU #2.2");
			
			EquipmentDTO thirdFREquipment1 = equipmentService.findByTitleRemote("FRU #3.1");
			EquipmentDTO fourthFREquipment1 = equipmentService.findByTitleRemote("FRU #3.2");

			
			
			createSensorDTO(activitySensorType, "FRU #5 ACT.", thirdFREquipment1, "Activity");
			createSensorDTO(bodyTempSensorType, "FRU #5 BTEMP.", thirdFREquipment1, "BdTemp");
			createSensorDTO(breathRateSensorType, "FRU #5 BR.", thirdFREquipment1, "BR");
			createSensorDTO(heartbeatSensorType, "FRU #5 HB.", thirdFREquipment1, "HB");
			createSensorDTO(envTempSensorType, "FRU #5 ENVTEMP.", thirdFREquipment1, "EnvTemp");

			createSensorDTO(methaneSensorType, "FRU #5 METH.", thirdFREquipment1, "METHANE");
			createSensorDTO(carbonMonoxideSensorType, "FRU #5 CO.", thirdFREquipment1, "CARBONMONOXIDE");
			createSensorDTO(carbonDioxideSensorType, "FRU #5 CO2.", thirdFREquipment1, "CARBONDIOXIDE");
			createSensorDTO(oxygenSensorType, "FRU #5 O2.", thirdFREquipment1, "OXYGEN");
			createSensorDTO(hydrogenSulfideSensorType, "FRU #5 H2S.", thirdFREquipment1, "HYDROGENSULFIDE");
			createSensorDTO(lpsSensorType, "FRU #5 LPS.", thirdFREquipment1, "LPS");
			createSensorDTO(locationSensorType, "FRU #5 LOC.", thirdFREquipment1, "LOC");


			
			createSensorDTO(activitySensorType, "FRU #6 ACT.", fourthFREquipment1, "Activity");
			createSensorDTO(bodyTempSensorType, "FRU #6 BTEMP.", fourthFREquipment1, "BdTemp");
			createSensorDTO(breathRateSensorType, "FRU #6 BR.", fourthFREquipment1, "BR");
			createSensorDTO(heartbeatSensorType, "FRU #6 HB.", fourthFREquipment1, "HB");
			createSensorDTO(envTempSensorType, "FRU #6 ENVTEMP.", fourthFREquipment1, "EnvTemp");

			createSensorDTO(methaneSensorType, "FRU #6 METH.", fourthFREquipment1, "METHANE");
			createSensorDTO(carbonMonoxideSensorType, "FRU #6 CO.", fourthFREquipment1, "CARBONMONOXIDE");
			createSensorDTO(carbonDioxideSensorType, "FRU #6 CO2.", fourthFREquipment1, "CARBONDIOXIDE");
			createSensorDTO(oxygenSensorType, "FRU #6 O2.", fourthFREquipment1, "OXYGEN");
			createSensorDTO(hydrogenSulfideSensorType, "FRU #6 H2S.", fourthFREquipment1, "HYDROGENSULFIDE");
			createSensorDTO(lpsSensorType, "FRU #6 LPS.", fourthFREquipment1, "LPS");
			createSensorDTO(locationSensorType, "FRU #6 LOC.", fourthFREquipment1, "LOC");
			
			
			
			
			createSensorDTO(activitySensorType, "FRU #1 ACT.", firstFRCEquipment, "Activity");
			createSensorDTO(bodyTempSensorType, "FRU #1 BTEMP.", firstFRCEquipment, "BdTemp");
			createSensorDTO(breathRateSensorType, "FRU #1 BR.", firstFRCEquipment, "BR");
			createSensorDTO(heartbeatSensorType, "FRU #1 HB.", firstFRCEquipment, "HB");
			createSensorDTO(envTempSensorType, "FRU #1 ENVTEMP.", firstFRCEquipment, "EnvTemp");

			createSensorDTO(methaneSensorType, "FRU #1 METH.", firstFRCEquipment, "METHANE");
			createSensorDTO(carbonMonoxideSensorType, "FRU #1 CO.", firstFRCEquipment, "CARBONMONOXIDE");
			createSensorDTO(carbonDioxideSensorType, "FRU #1 CO2.", firstFRCEquipment, "CARBONDIOXIDE");
			createSensorDTO(oxygenSensorType, "FRU #1 O2.", firstFRCEquipment, "OXYGEN");
			createSensorDTO(hydrogenSulfideSensorType, "FRU #1 H2S.", firstFRCEquipment, "HYDROGENSULFIDE");
			createSensorDTO(lpsSensorType, "FRU #1 LPS.", firstFRCEquipment, "LPS");
			createSensorDTO(locationSensorType, "FRU #1 LOC.", firstFRCEquipment, "LOC");

			createSensorDTO(activitySensorType, "FRU #1.1 ACT.", firstFREquipment, "Activity");
			createSensorDTO(bodyTempSensorType, "FRU #1.1 BTEMP.", firstFREquipment, "BdTemp");
			createSensorDTO(breathRateSensorType, "FRU #1.1 BR.", firstFREquipment, "BR");
			createSensorDTO(heartbeatSensorType, "FRU #1.1 HB.", firstFREquipment, "HB");
			createSensorDTO(envTempSensorType, "FRU #1.1 ENVTEMP.", firstFREquipment, "EnvTemp");
//			createSensorDTO(gasSensorType, "FRU #1.1 GAS.", firstFREquipment, "GAS");
			createSensorDTO(methaneSensorType, "FRU #1.1 METH.", firstFREquipment, "METHANE");
			createSensorDTO(carbonMonoxideSensorType, "FRU #1.1 CO.", firstFREquipment, "CARBONMONOXIDE");
			createSensorDTO(carbonDioxideSensorType, "FRU #1.1 CO2.", firstFREquipment, "CARBONDIOXIDE");
			createSensorDTO(oxygenSensorType, "FRU #1.1 O2.", firstFREquipment, "OXYGEN");
			createSensorDTO(hydrogenSulfideSensorType, "FRU #1.1 H2S.", firstFREquipment, "HYDROGENSULFIDE");
			createSensorDTO(lpsSensorType, "FRU #1.1 LPS.", firstFREquipment, "LPS");
			createSensorDTO(locationSensorType, "FRU #1.1 LOC.", firstFREquipment, "LOC");

			createSensorDTO(activitySensorType, "FRU #1.2 ACT.", secondFREquipment, "Activity");
			createSensorDTO(bodyTempSensorType, "FRU #1.2 BTEMP.", secondFREquipment, "BdTemp");
			createSensorDTO(breathRateSensorType, "FRU #1.2 BR.", secondFREquipment, "BR");
			createSensorDTO(heartbeatSensorType, "FRU #1.2 HB.", secondFREquipment, "HB");
			createSensorDTO(envTempSensorType, "FRU #1.2 ENVTEMP.", secondFREquipment, "EnvTemp");
//			createSensorDTO(gasSensorType, "FRU #1.2 GAS.", secondFREquipment, "GAS");
			createSensorDTO(methaneSensorType, "FRU #1.2 METH.", secondFREquipment, "METHANE");
			createSensorDTO(carbonMonoxideSensorType, "FRU #1.2 CO.", secondFREquipment, "CARBONMONOXIDE");
			createSensorDTO(carbonDioxideSensorType, "FRU #1.2 CO2.", secondFREquipment, "CARBONDIOXIDE");
			createSensorDTO(oxygenSensorType, "FRU #1.2 O2.", secondFREquipment, "OXYGEN");
			createSensorDTO(hydrogenSulfideSensorType, "FRU #1.2 H2S.", secondFREquipment, "HYDROGENSULFIDE");
			createSensorDTO(lpsSensorType, "FRU #1.2 LPS.", secondFREquipment, "LPS");
			createSensorDTO(locationSensorType, "FRU #1.2 LOC.", secondFREquipment, "LOC");

//			createSensorDTO(activitySensorType, "FRU #2 ACT.", secondFRCEquipment, "Activity");
//			createSensorDTO(bodyTempSensorType, "FRU #2 BTEMP.", secondFRCEquipment, "BdTemp");
//			createSensorDTO(breathRateSensorType, "FRU #2 BR.", secondFRCEquipment, "BR");
//			createSensorDTO(heartbeatSensorType, "FRU #2 HB.", secondFRCEquipment, "HB");
//			createSensorDTO(envTempSensorType, "FRU #2 ENVTEMP.", secondFRCEquipment, "EnvTemp");
////			createSensorDTO(gasSensorType, "FRU #2 GAS.", secondFRCEquipment, "GAS");
//			createSensorDTO(methaneSensorType, "FRU #2 METH.", secondFRCEquipment, "METHANE");
//			createSensorDTO(carbonMonoxideSensorType, "FRU #2 CO.", secondFRCEquipment, "CARBONMONOXIDE");
//			createSensorDTO(carbonDioxideSensorType, "FRU #2 CO2.", secondFRCEquipment, "CARBONDIOXIDE");
//			createSensorDTO(oxygenSensorType, "FRU #2 O2.", secondFRCEquipment, "OXYGEN");
//			createSensorDTO(hydrogenSulfideSensorType, "FRU #2 H2S.", secondFRCEquipment, "HYDROGENSULFIDE");
//			createSensorDTO(lpsSensorType, "FRU #2 LPS.", secondFRCEquipment, "LPS");
//			createSensorDTO(locationSensorType, "FRU #2 LOC.", secondFRCEquipment, "LOC");

			createSensorDTO(activitySensorType, "FRU #2.1 ACT.", thirdFREquipment, "Activity");
			createSensorDTO(bodyTempSensorType, "FRU #2.1 BTEMP.", thirdFREquipment, "BdTemp");
			createSensorDTO(breathRateSensorType, "FRU #2.1 BR.", thirdFREquipment, "BR");
			createSensorDTO(heartbeatSensorType, "FRU #2.1 HB.", thirdFREquipment, "HB");
			createSensorDTO(envTempSensorType, "FRU #2.1 ENVTEMP.", thirdFREquipment, "EnvTemp");
//			createSensorDTO(gasSensorType, "FRU #2.1 GAS.", thirdFREquipment, "GAS");
			createSensorDTO(methaneSensorType, "FRU #2.1 METH.", thirdFREquipment, "METHANE");
			createSensorDTO(carbonMonoxideSensorType, "FRU #2.1 CO.", thirdFREquipment, "CARBONMONOXIDE");
			createSensorDTO(carbonDioxideSensorType, "FRU #2.1 CO2.", thirdFREquipment, "CARBONDIOXIDE");
			createSensorDTO(oxygenSensorType, "FRU #2.1 O2.", thirdFREquipment, "OXYGEN");
			createSensorDTO(hydrogenSulfideSensorType, "FRU #2.1 H2S.", thirdFREquipment, "HYDROGENSULFIDE");
			createSensorDTO(lpsSensorType, "FRU #2.1 LPS.", thirdFREquipment, "LPS");
			createSensorDTO(locationSensorType, "FRU #2.1 LOC.", thirdFREquipment, "LOC");

			createSensorDTO(activitySensorType, "FRU #2.2 ACT.", fourthFREquipment, "Activity");
			createSensorDTO(bodyTempSensorType, "FRU #2.2 BTEMP.", fourthFREquipment, "BdTemp");
			createSensorDTO(breathRateSensorType, "FRU #2.2 BR.", fourthFREquipment, "BR");
			createSensorDTO(heartbeatSensorType, "FRU #2.2 HB.", fourthFREquipment, "HB");
			createSensorDTO(envTempSensorType, "FRU #2.2 ENVTEMP.", fourthFREquipment, "EnvTemp");
//			createSensorDTO(gasSensorType, "FRU #2.2 GAS.", fourthFREquipment, "GAS");
			createSensorDTO(methaneSensorType, "FRU #2.2 METH.", fourthFREquipment, "METHANE");
			createSensorDTO(carbonMonoxideSensorType, "FRU #2.2 CO.", fourthFREquipment, "CARBONMONOXIDE");
			createSensorDTO(carbonDioxideSensorType, "FRU #2.2 CO2.", fourthFREquipment, "CARBONDIOXIDE");
			createSensorDTO(oxygenSensorType, "FRU #2.2 O2.", fourthFREquipment, "OXYGEN");
			createSensorDTO(hydrogenSulfideSensorType, "FRU #2.2 H2S.", fourthFREquipment, "HYDROGENSULFIDE");
			createSensorDTO(lpsSensorType, "FRU #2.2 LPS.", fourthFREquipment, "LPS");
			createSensorDTO(locationSensorType, "FRU #2.2 LOC.", fourthFREquipment, "LOC");

		}

	}

	@SuppressWarnings("static-access")
	@Test(groups="createSnapshots")
	public void testCreateSensorSnapshots() throws ClassNotFoundException, InterruptedException {	

		SensorDTO firstFRCTemperature = sensorService.findSensorByTitleRemote("FRU #1 BTEMP.");
		SensorDTO firstFRCHeartbeat = sensorService.findSensorByTitleRemote("FRU #1 HB.");
		//		SensorDTO firstFRTemperature = sensorService.findSensorByTitleRemote("FRU #1.1 TEMP.");
		//		SensorDTO firstFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #1.1 HB.");
		//		SensorDTO secondFRTemperature = sensorService.findSensorByTitleRemote("FRU #1.2 TEMP.");
		//		SensorDTO secondFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #1.2 HB.");
		SensorDTO secondFRCTemperature = sensorService.findSensorByTitleRemote("FRU #5 BTEMP.");
		SensorDTO secondFRCHeartbeat = sensorService.findSensorByTitleRemote("FRU #5 HB.");
		//		SensorDTO thirdFRTemperature = sensorService.findSensorByTitleRemote("FRU #2.1 TEMP.");
		//		SensorDTO thirdFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #2.1 HB.");
		//		SensorDTO fourthFRTemperature = sensorService.findSensorByTitleRemote("FRU #2.2 TEMP.");
		//		SensorDTO fourthFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #2.2 HB.");


		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("30"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("70"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("73"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("32"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("76"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("35"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("78"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("37"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("84"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("35"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("86"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("37"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("84"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("39"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("87"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("36"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("85"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("35"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("81"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("33"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("79"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));


		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("32"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("73"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("33"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("75"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("34"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("78"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("37"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("80"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("39"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("86"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("37"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("88"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("39"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("86"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("41"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("89"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("38"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("87"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("37"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("83"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		Thread.currentThread().sleep(2000);
		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("35"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));
		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("81"), MeasurementStatisticTypeEnumDTO.MEAN, createPeriodDTO(1));

	}

	@Test(groups="createSnapshots")
	public void testCreateSensorSnapshotsDTO() throws ClassNotFoundException {		

		SensorDTO firstFRCTemperature = sensorService.findSensorByTitleRemote("FRU #1 BTEMP.");
		SensorDTO firstFRCHeartbeat = sensorService.findSensorByTitleRemote("FRU #1 HB.");
		SensorDTO firstFRTemperature = sensorService.findSensorByTitleRemote("FRU #1.1 BTEMP.");
		SensorDTO firstFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #1.1 HB.");
		SensorDTO secondFRTemperature = sensorService.findSensorByTitleRemote("FRU #1.2 BTEMP.");
		SensorDTO secondFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #1.2 HB.");
//		SensorDTO secondFRCTemperature = sensorService.findSensorByTitleRemote("FRU #2 BTEMP.");
//		SensorDTO secondFRCHeartbeat = sensorService.findSensorByTitleRemote("FRU #2 HB.");
		SensorDTO thirdFRTemperature = sensorService.findSensorByTitleRemote("FRU #2.1 BTEMP.");
		SensorDTO thirdFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #2.1 HB.");
		SensorDTO fourthFRTemperature = sensorService.findSensorByTitleRemote("FRU #2.2 BTEMP.");
		SensorDTO fourthFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #2.2 HB.");

		PeriodDTO period = this.createPeriodDTO(SECONDS);

		createSensorSnapshotDTO(firstFRCTemperature, new BigDecimal("30"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(firstFRCHeartbeat, new BigDecimal("80"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(firstFRTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(firstFRHeartbeat, new BigDecimal("70"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(secondFRTemperature, new BigDecimal("31"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(secondFRHeartbeat, new BigDecimal("85"), MeasurementStatisticTypeEnumDTO.MEAN, period);
//		createSensorSnapshotDTO(secondFRCTemperature, new BigDecimal("35"), MeasurementStatisticTypeEnumDTO.MEAN, period);
//		createSensorSnapshotDTO(secondFRCHeartbeat, new BigDecimal("79"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(thirdFRTemperature, new BigDecimal("36"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(thirdFRHeartbeat, new BigDecimal("74"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(fourthFRTemperature, new BigDecimal("34"), MeasurementStatisticTypeEnumDTO.MEAN, period);
		createSensorSnapshotDTO(fourthFRHeartbeat, new BigDecimal("84"), MeasurementStatisticTypeEnumDTO.MEAN, period);
	}

	@Test(groups="CreateConfiguration")
	public void testCreateSensorConfigurationDTO() {		

		SensorDTO firstFRCTemperature = sensorService.findSensorByTitleRemote("FRU #1 BTEMP.");
		SensorDTO firstFRCHeartbeat = sensorService.findSensorByTitleRemote("FRU #1 HB.");
		SensorDTO firstFRCLocation = sensorService.findSensorByTitleRemote("FRU #1 LOC.");

		SensorDTO firstFRTemperature = sensorService.findSensorByTitleRemote("FRU #1.1 BTEMP.");
		SensorDTO firstFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #1.1 HB.");
		SensorDTO firstFRLocation = sensorService.findSensorByTitleRemote("FRU #1.1 LOC.");

		SensorDTO secondFRTemperature = sensorService.findSensorByTitleRemote("FRU #1.2 BTEMP.");
		SensorDTO secondFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #1.2 HB.");
		SensorDTO secondFRLocation = sensorService.findSensorByTitleRemote("FRU #1.2 LOC.");

//		SensorDTO secondFRCTemperature = sensorService.findSensorByTitleRemote("FRU #2 BTEMP.");
//		SensorDTO secondFRCHeartbeat = sensorService.findSensorByTitleRemote("FRU #2 HB.");
//		SensorDTO secondFRCLocation = sensorService.findSensorByTitleRemote("FRU #2 LOC.");

		SensorDTO thirdFRTemperature = sensorService.findSensorByTitleRemote("FRU #2.1 BTEMP.");
		SensorDTO thirdFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #2.1 HB.");
		SensorDTO thirdFRLocation = sensorService.findSensorByTitleRemote("FRU #2.1 LOC.");

		SensorDTO fourthFRTemperature = sensorService.findSensorByTitleRemote("FRU #2.2 BTEMP.");
		SensorDTO fourthFRHeartbeat = sensorService.findSensorByTitleRemote("FRU #2.2 HB.");
		SensorDTO fourthFRLocation = sensorService.findSensorByTitleRemote("FRU #2.2 LOC.");
		
		
		SensorDTO fourthFRTemperature1 = sensorService.findSensorByTitleRemote("FRU #6 BTEMP.");
		SensorDTO fourthFRHeartbeat1 = sensorService.findSensorByTitleRemote("FRU #6 HB.");
		SensorDTO fourthFRLocation1 = sensorService.findSensorByTitleRemote("FRU #6 LOC.");
		
		SensorDTO fourthFRTemperature2 = sensorService.findSensorByTitleRemote("FRU #5 BTEMP.");
		SensorDTO fourthFRHeartbeat2 = sensorService.findSensorByTitleRemote("FRU #5 HB.");
		SensorDTO fourthFRLocation2 = sensorService.findSensorByTitleRemote("FRU #5 LOC.");


		
		createStatisticConfigDTO(fourthFRTemperature1, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(fourthFRHeartbeat1, new Long(5000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(fourthFRLocation1, new Long(10000), MeasurementStatisticTypeEnumDTO.MEAN);
		
		
		createStatisticConfigDTO(fourthFRTemperature2, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(fourthFRHeartbeat2, new Long(5000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(fourthFRLocation2, new Long(10000), MeasurementStatisticTypeEnumDTO.MEAN);
		
		
		/***************************************************************************************************/
		createStatisticConfigDTO(firstFRCTemperature, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(firstFRCHeartbeat, new Long(5000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(firstFRCLocation, new Long(10000), MeasurementStatisticTypeEnumDTO.MEAN);

		/***************************************************************************************************/

		createStatisticConfigDTO(firstFRTemperature, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(firstFRHeartbeat, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(firstFRLocation, new Long(10000), MeasurementStatisticTypeEnumDTO.MEAN);

		/***************************************************************************************************/

		createStatisticConfigDTO(secondFRTemperature, new Long(5000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(secondFRHeartbeat, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(secondFRLocation, new Long(10000), MeasurementStatisticTypeEnumDTO.MEAN);

		/***************************************************************************************************/

//		createStatisticConfigDTO(secondFRCTemperature, new Long(5000), MeasurementStatisticTypeEnumDTO.MAXIMUM);
//
//		createStatisticConfigDTO(secondFRCHeartbeat, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);
//
//		createStatisticConfigDTO(secondFRCLocation, new Long(10000), MeasurementStatisticTypeEnumDTO.MEAN);

		/***************************************************************************************************/

		createStatisticConfigDTO(thirdFRTemperature, new Long(5000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(thirdFRHeartbeat, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(thirdFRLocation, new Long(10000), MeasurementStatisticTypeEnumDTO.MEAN);

		/***************************************************************************************************/

		createStatisticConfigDTO(fourthFRTemperature, new Long(5000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(fourthFRHeartbeat, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(fourthFRLocation, new Long(10000), MeasurementStatisticTypeEnumDTO.MEAN);

	}

	private SensorDTO createSensorDTO(SensorTypeDTO type, String title, EquipmentDTO equipment, String label) throws ClassNotFoundException {
		SensorDTO sensor = null;

		if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("ACT")).getTitle())) {
			sensor = new ActivitySensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		}
		else if(type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("BODY_TEMP")).getTitle())) {
			sensor = new BodyTemperatureSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("HB")).getTitle())) {
			sensor = new HeartBeatRateSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("BR")).getTitle())) {
			sensor = new BreathRateSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("ENV_TEMP")).getTitle())) {
			sensor = new EnvironmentTemperatureSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
		} /*else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("GAS")).getTitle())) {
			sensor = new GasSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		}*/ else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("METHANE")).getTitle())) {
			sensor = new MethaneSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("CARBONMONOXIDE")).getTitle())) {
			sensor = new CarbonMonoxideSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("CARBONDIOXIDE")).getTitle())) {
			sensor = new CarbonDioxideSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("OXYGEN")).getTitle())) {
			sensor = new OxygenSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("HYDROGENSULFIDE")).getTitle())) {
			sensor = new HydrogenSulfideSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("LPS")).getTitle())) {
			sensor = new LPSSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("LOCATION")).getTitle())) {
			sensor = new LocationSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES);
		}
		else
			System.out.println("Unknown sensor type when trying to create one in test execution");
		if(sensor!=null) {
			sensor.setType(type.getTitle());

			sensor.setTitle(title);
			sensor.setStatus(ResourceStatusDTO.ALLOCATED);
			sensor.setEquipmentId(equipment.getId());
			//		sensor.setLabel(label);
			return sensorService.createSensorRemote(sensor, this.userID);
		}
		return null;
		
	}

	private SensorSnapshotDTO createSensorSnapshotDTO(SensorDTO sensor, BigDecimal value, MeasurementStatisticTypeEnumDTO statisticType, PeriodDTO period) throws ClassNotFoundException {
		SensorSnapshotDTO snapshot = new SensorSnapshotDTO();

		snapshot.setSensor(sensor.getId());
		snapshot.setStatisticType(statisticType);
		snapshot.setValue(value.toString());
		snapshot.setPeriod(period);
		snapshot.setStatus(SensorSnapshotStatusDTO.WORKING);

		return (SensorSnapshotDTO) sensorService.createSensorSnapshotRemote(snapshot, userID);
	}

	private StatisticsConfigDTO createStatisticConfigDTO(SensorDTO sensor, Long samplingPeriodMs, MeasurementStatisticTypeEnumDTO type) {
		StatisticsConfigDTO statisticsConfig = new StatisticsConfigDTO();
		statisticsConfig.setMeasurementStatisticType(type);
		statisticsConfig.setSamplingPeriodMilliseconds(samplingPeriodMs);
		statisticsConfig.setSensorId(sensor.getId());
		return sensorService.createStatisticConfigRemote(statisticsConfig, this.userID);
	}

}
