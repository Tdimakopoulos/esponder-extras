package eu.esponder.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class UserServiceTest extends ControllerServiceTest {

	@Test(groups = "createBasics")
	public void testCreateUsers() {
		 ESponderUserDTO pki = new ESponderUserDTO();
		 pki.setTitle("pki");
		 pki.setStatus(ResourceStatusDTO.AVAILABLE);
		 pki.setActorID(new Long(0)); 
		 pki.setPassword("pki");
		 pki.setUsername("pki");
		 pki.setPkikey("901929sjdkalllaas92983828919");
		 pki.setRasberryip("0.0.0.0:0");
		 pki.setActorRole(10);//pki module
		 userService.createUserRemote(pki, null);
		
		 ESponderUserDTO ctri = new ESponderUserDTO();
		 ctri.setTitle("EOC");
		 ctri.setStatus(ResourceStatusDTO.AVAILABLE);
		 ctri.setActorID(new Long(0)); 
		 ctri.setPassword("eoc");
		 ctri.setUsername("eoc");
		 ctri.setPkikey("901929sjdkalllaas92983828919");
		 ctri.setRasberryip("0.0.0.0:0");
		 ctri.setActorRole(11);//EOC Server
		 userService.createUserRemote(ctri, null);
	
		 ESponderUserDTO gleo = new ESponderUserDTO();
		 gleo.setTitle("MEOC");
		 gleo.setStatus(ResourceStatusDTO.AVAILABLE);
		 gleo.setActorID(new Long(0)); 
		 gleo.setPassword("meoc");
		 gleo.setUsername("meoc");
		 gleo.setPkikey("901929sjdkalllaas92983828919");
		 gleo.setRasberryip("0.0.0.0:0");
		 gleo.setActorRole(12);//MEOC Server
		 userService.createUserRemote(gleo, null);
		//
		 ESponderUserDTO kotso = new ESponderUserDTO();
		 kotso.setTitle("Misc Servers");
		 kotso.setStatus(ResourceStatusDTO.AVAILABLE);
		 kotso.setActorID(new Long(0)); 
		 kotso.setPassword("msrv");
		 kotso.setUsername("msrv");
		 kotso.setPkikey("901929sjdkalllaas92983828919");
		 kotso.setRasberryip("0.0.0.0:0");
		 kotso.setActorRole(13);//Misc Server
		 userService.createUserRemote(kotso, null);

		 ESponderUserDTO dimitris = new ESponderUserDTO();
		 dimitris.setTitle("Dimitris FRC");
		 dimitris.setStatus(ResourceStatusDTO.AVAILABLE);
		 dimitris.setActorID(new Long(4));//frc
		 dimitris.setPassword("test");
		 dimitris.setUsername("dimitris");
		 dimitris.setPkikey("901929sjdkalllaas92983828919");
		 dimitris.setRasberryip("192.168.2.248:3000");
		 dimitris.setActorRole(1);
		 userService.createUserRemote(dimitris, null);
		//
		 ESponderUserDTO thomas = new ESponderUserDTO();
		 thomas.setTitle("Thomas IC");
		 thomas.setActorID(new Long(2));//ic
		 thomas.setPassword("test");
		 thomas.setUsername("thomas");
		 thomas.setPkikey("96843096802398523095682095634860934683409634");
		 thomas.setStatus(ResourceStatusDTO.AVAILABLE);
		 thomas.setRasberryip("192.168.2.248:3000");
		 thomas.setActorRole(3);
		 userService.createUserRemote(thomas, null);
		//
		 ESponderUserDTO Jesus = new ESponderUserDTO();
		 Jesus.setTitle("Jesus FR");
		 Jesus.setActorID(new Long(6));//fr
		 Jesus.setPassword("test");
		 Jesus.setUsername("jesus");
		 Jesus.setPkikey("219458768909328532109832460943869043874309809");
		 Jesus.setStatus(ResourceStatusDTO.AVAILABLE);
		 Jesus.setRasberryip("192.168.2.248:3000");
		 Jesus.setActorRole(2);
		 userService.createUserRemote(Jesus, null);
		 
		 
		 ESponderUserDTO Jesus1 = new ESponderUserDTO();
		 Jesus1.setTitle("Jesus FR1");
		 Jesus1.setActorID(new Long(6));//fr
		 Jesus1.setPassword("test");
		 Jesus1.setUsername("jesus");
		 Jesus1.setPkikey("219458768909328532109832460943869043874309809");
		 Jesus1.setStatus(ResourceStatusDTO.AVAILABLE);
		 Jesus1.setRasberryip("192.168.2.248:3000");
		 Jesus1.setActorRole(2);
		 userService.createUserRemote(Jesus1, null);
		 
		 
		 ESponderUserDTO Jesus2 = new ESponderUserDTO();
		 Jesus2.setTitle("Jesus FR2");
		 Jesus2.setActorID(new Long(6));//fr
		 Jesus2.setPassword("test");
		 Jesus2.setUsername("jesus");
		 Jesus2.setPkikey("219458768909328532109832460943869043874309809");
		 Jesus2.setStatus(ResourceStatusDTO.AVAILABLE);
		 Jesus2.setRasberryip("192.168.2.248:3000");
		 Jesus2.setActorRole(2);
		 userService.createUserRemote(Jesus2, null);
		 
		 //ESponderUserDTO 
		 
		 Jesus1 = new ESponderUserDTO();
		 Jesus1.setTitle("Jesus FR3");
		 Jesus1.setActorID(new Long(6));//fr
		 Jesus1.setPassword("test");
		 Jesus1.setUsername("jesus");
		 Jesus1.setPkikey("219458768909328532109832460943869043874309809");
		 Jesus1.setStatus(ResourceStatusDTO.AVAILABLE);
		 Jesus1.setRasberryip("192.168.2.248:3000");
		 Jesus1.setActorRole(2);
		 userService.createUserRemote(Jesus1, null);
		 
		 Jesus1 = new ESponderUserDTO();
		 Jesus1.setTitle("Jesus FR4");
		 Jesus1.setActorID(new Long(6));//fr
		 Jesus1.setPassword("test");
		 Jesus1.setUsername("jesus");
		 Jesus1.setPkikey("219458768909328532109832460943869043874309809");
		 Jesus1.setStatus(ResourceStatusDTO.AVAILABLE);
		 Jesus1.setRasberryip("192.168.2.248:3000");
		 Jesus1.setActorRole(2);
		 userService.createUserRemote(Jesus1, null);
		 
		 Jesus1 = new ESponderUserDTO();
		 Jesus1.setTitle("Jesus FR5");
		 Jesus1.setActorID(new Long(6));//fr
		 Jesus1.setPassword("test");
		 Jesus1.setUsername("jesus");
		 Jesus1.setPkikey("219458768909328532109832460943869043874309809");
		 Jesus1.setStatus(ResourceStatusDTO.AVAILABLE);
		 Jesus1.setRasberryip("192.168.2.248:3000");
		 Jesus1.setActorRole(2);
		 userService.createUserRemote(Jesus1, null);
		//
		 ESponderUserDTO Fabian = new ESponderUserDTO();
		 Fabian.setTitle("VOIP");
		 Fabian.setStatus(ResourceStatusDTO.AVAILABLE);
		 Fabian.setActorID(new Long(0)); 
		 Fabian.setPassword("vsrv");
		 Fabian.setUsername("vsrv");
		 Fabian.setPkikey("901929sjdkalllaas92983828919");
		 Fabian.setRasberryip("0.0.0.0:0");
		 Fabian.setActorRole(14);//VOIP Server
		 userService.createUserRemote(Fabian, null);

	}

}
