package eu.esponder.test.crisis;

import java.math.BigDecimal;
import java.util.HashSet;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.OCEocDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.category.OperationsCentreCategoryDTO;
import eu.esponder.dto.model.crisis.view.HttpURLDTO;
import eu.esponder.dto.model.crisis.view.MapPointDTO;
import eu.esponder.dto.model.crisis.view.ReferencePOIDTO;
import eu.esponder.dto.model.crisis.view.SketchPOIDTO;
import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.ReferencePOISnapshotDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.OperationsCentreSnapshotStatusDTO;
import eu.esponder.dto.model.type.OperationsCentreTypeDTO;
import eu.esponder.model.crisis.view.MapPoint;
import eu.esponder.model.snapshot.location.Point;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class OperationsCentreServiceTest extends ControllerServiceTest {

	private static int SECONDS = 30;
	@SuppressWarnings("unused")
	private static double RADIUS = 1;

	@SuppressWarnings("unused")
	@Test(groups="createResources")
	public void testCreateOperationsCentresDTO() throws ClassNotFoundException {

		//	Create Operations Centers and place them in hierarchy
		
		PeriodDTO period = this.createPeriodDTO(SECONDS);
		PointDTO centre = new PointDTO(new BigDecimal(52.319146), new BigDecimal(4.752479), null);
		SphereDTO sphere1 = new SphereDTO();
		sphere1.setTitle("Eoc LOcation");
		sphere1.setCentre(centre);
		sphere1.setRadius(new BigDecimal(0));
		
		SphereDTO sphere1p=(SphereDTO) genericService.createEntityRemote(sphere1, new Long(1));
		
		OCEocDTO eocAttica = createOCEocDTO("EOC Schiphol", "http", "192.168.10.1", "eocAttica", sphere1p);
		OCMeocDTO meocAthens = createOCMeocDTO(eocAttica, "MEOC Schiphol I", "http", "192.168.10.2", "meocAthens");
//		OCMeocDTO meocPiraeus = createOCMeocDTO(eocAttica, "MEOC Schiphol II",  "http", "192.168.10.3", "meocPiraeus");
	}


	@Test(groups="createSnapshots")
	public void testCreateOperationsCentreSnapshotsDTO() throws ClassNotFoundException { 
		
		OCMeocDTO meocAthens = (OCMeocDTO) operationsCentreService.findMeocByTitleRemote("MEOC Schiphol I");
//		OCMeocDTO meocPiraeus = (OCMeocDTO) operationsCentreService.findMeocByTitleRemote("MEOC Schiphol II");

		PeriodDTO period = this.createPeriodDTO(SECONDS);

		PointDTO point1 = new PointDTO(new BigDecimal(52.318642), new BigDecimal(4.749287), null);


		SphereDTO sphere1 = new SphereDTO();
		sphere1.setCentre(point1);
		sphere1.setRadius(new BigDecimal(0));

		sphere1.setTitle("Sphere 01");
		SphereDTO sphere1p=(SphereDTO) genericService.createEntityRemote(sphere1, new Long(1));
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		createOperationsCentreSnapshot(meocAthens, period, sphere1p);
	}


	/*******************************************************************************************************************************************/


	@Test(groups="createPOIs")
	public void testCreateSketches() {

		OCMeocDTO meocAthens = (OCMeocDTO) operationsCentreService.findMeocByTitleRemote("MEOC Schiphol I");
//		OCMeocDTO meocPiraeus = (OCMeocDTO) operationsCentreService.findMeocByTitleRemote("MEOC Schiphol II");

		
		
		SketchPOIDTO sketch1 = new SketchPOIDTO();
		sketch1.setTitle("Sketch 1");
		sketch1.setPoints(new HashSet<MapPointDTO>());
		sketch1.getPoints().add(createMapPointDTO("point11", 1, new BigDecimal(52.1907), new BigDecimal(4.4507)));
		sketch1.getPoints().add(createMapPointDTO("point12", 1, new BigDecimal(52.2207), new BigDecimal(4.4707))); 	
		sketch1.getPoints().add(createMapPointDTO("point13", 1, new BigDecimal(52.1507), new BigDecimal(4.4907))); 	
		sketch1.setHttpURL(createHttpURLDTO("192.168.1.1:8080", "testpath1", "http"));
		sketch1.setSketchType("SketchType1");

		SketchPOIDTO sketch2 = new SketchPOIDTO();
		sketch2.setTitle("Sketch 2");
		sketch2.setPoints(new HashSet<MapPointDTO>());
		sketch2.getPoints().add(createMapPointDTO("point21", 2,  new BigDecimal(52.2507), new BigDecimal(4.3507)));
		sketch2.getPoints().add(createMapPointDTO("point22", 2,  new BigDecimal(52.2707), new BigDecimal(4.2507)));
		sketch2.setHttpURL(createHttpURLDTO("192.168.1.2:8080", "testpath2", "http"));
		sketch2.setSketchType("SketchType2");

		operationsCentreService.createSketchPOIRemote(meocAthens, sketch1, this.userID);
		operationsCentreService.createSketchPOIRemote(meocAthens, sketch2, this.userID);


//		52.1907
//		4.4507

		
		SketchPOIDTO sketch3 = new SketchPOIDTO();
		sketch3.setTitle("Sketch 3");
		sketch3.setPoints(new HashSet<MapPointDTO>());
		sketch3.getPoints().add(createMapPointDTO("point31", 3,  new BigDecimal(52.2007), new BigDecimal(4.5507)));
		sketch3.getPoints().add(createMapPointDTO("point32", 3,  new BigDecimal(52.2907), new BigDecimal(4.4907)));
		sketch3.getPoints().add(createMapPointDTO("point33", 3,  new BigDecimal(52.3107), new BigDecimal(4.4307)));
		sketch3.getPoints().add(createMapPointDTO("point34", 3,  new BigDecimal(52.3307), new BigDecimal(4.3907)));
		sketch3.setHttpURL(createHttpURLDTO("192.168.1.3:8080", "testpath3", "http"));
		sketch3.setSketchType("SketchType3");

//		operationsCentreService.createSketchPOIRemote(meocPiraeus, sketch3, this.userID);
	}


	@Test(groups="createPOIs")
	public void testCreateReferencePOIs() {

		OCMeocDTO meocAthens = (OCMeocDTO) operationsCentreService.findMeocByTitleRemote("MEOC Schiphol I");
//		OCMeocDTO meocPiraeus = (OCMeocDTO) operationsCentreService.findMeocByTitleRemote("MEOC Schiphol II");

		ReferencePOIDTO ref1 = createReferencePOIDTO("file 1", "first file location", new Float(1.5));
//		operationsCentreService.createReferencePOIRemote(meocAthens, ref1, this.userID);
		operationsCentreService.createReferencePOIRemote(ref1, this.userID);

		ReferencePOIDTO ref2 = createReferencePOIDTO("file 2", "second file location", new Float(2.5));
//		operationsCentreService.createReferencePOIRemote(meocPiraeus, ref2, this.userID);
		operationsCentreService.createReferencePOIRemote(ref2, this.userID);
	}
	
	@Test
	public void createReferencePOISnapshots() {
		
		ReferencePOIDTO ref1  = operationsCentreService.findReferencePOIByTitleRemote("first file location");
		ReferencePOIDTO ref2  = operationsCentreService.findReferencePOIByTitleRemote("second file location");
		
		ReferencePOISnapshotDTO snapshot1 = new ReferencePOISnapshotDTO();
		snapshot1.setAverageSize(ref1.getAverageSize());
		snapshot1.setReferenceFile("ref1-1");
		snapshot1.setReferencePOI(ref1);
		
		ReferencePOISnapshotDTO snapshot12 = new ReferencePOISnapshotDTO();
		snapshot12.setAverageSize(ref1.getAverageSize());
		snapshot12.setReferenceFile("ref1-2");
		snapshot12.setReferencePOI(ref1);
		
		ReferencePOISnapshotDTO snapshot2 = new ReferencePOISnapshotDTO();
		snapshot2.setAverageSize(ref1.getAverageSize());
		snapshot2.setReferenceFile("ref2-1");
		snapshot2.setReferencePOI(ref2);
		
		ReferencePOISnapshotDTO snapshot22 = new ReferencePOISnapshotDTO();
		snapshot22.setAverageSize(ref1.getAverageSize());
		snapshot22.setReferenceFile("ref2-2");
		snapshot22.setReferencePOI(ref2);
		
		operationsCentreService.createReferencePOISnapshotRemote(snapshot1, userID);
		operationsCentreService.createReferencePOISnapshotRemote(snapshot12, userID);
		operationsCentreService.createReferencePOISnapshotRemote(snapshot2, userID);
		operationsCentreService.createReferencePOISnapshotRemote(snapshot22, userID);
		
		
	}

	@SuppressWarnings("unused")
	private OperationsCentreCategoryDTO createOperationsCentreCategoryDTO(String operationsCentreCategoryTitle) throws ClassNotFoundException {
		OperationsCentreCategoryDTO operationsCentreCategoryDTO = new OperationsCentreCategoryDTO();
		OperationsCentreTypeDTO operationsCentreTypeDTO = (OperationsCentreTypeDTO) typeService.findDTOByTitle(operationsCentreCategoryTitle);
		operationsCentreCategoryDTO.setOperationsCentreType(operationsCentreTypeDTO);
		operationsCentreCategoryDTO = (OperationsCentreCategoryDTO) resourceCategoryService.create(operationsCentreCategoryDTO, userID);
		return operationsCentreCategoryDTO;
	}


	private OCEocDTO createOCEocDTO(String title, String protocol, String host, String path, SphereDTO sphere) throws ClassNotFoundException { 

		OCEocDTO operationsCentreDTO = new OCEocDTO();

		CrisisContextDTO crisisContextDTO = crisisService.findCrisisContextDTOByTitle("Fire Brigade Drill");
		
		operationsCentreDTO.setTitle(title);
		operationsCentreDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		operationsCentreDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
		operationsCentreDTO.setEocCrisisContext(crisisContextDTO);
		operationsCentreDTO.setLocationArea(sphere);
		return (OCEocDTO) operationsCentreService.createOperationsCentreRemote(operationsCentreDTO, this.userID);
	}

	private OCMeocDTO createOCMeocDTO(OCEocDTO eoc, String title, String protocol, String host, String path) throws ClassNotFoundException { 

		OCMeocDTO operationsCentreDTO = new OCMeocDTO();
		
		CrisisContextDTO crisisContextDTO = crisisService.findCrisisContextDTOByTitle("Fire Brigade Drill");

		operationsCentreDTO.setTitle(title);
		operationsCentreDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		operationsCentreDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
		operationsCentreDTO.setSupervisingOC(eoc.getId());
		operationsCentreDTO.setMeocCrisisContext(crisisContextDTO.getId());
		return (OCMeocDTO) operationsCentreService.createOperationsCentreRemote(operationsCentreDTO, this.userID);
	}

	private OperationsCentreSnapshotDTO createOperationsCentreSnapshot (OCMeocDTO operationsCentre, PeriodDTO period, LocationAreaDTO locationArea) {

		OperationsCentreSnapshotDTO snapshot = new OperationsCentreSnapshotDTO();
		snapshot.setLocationArea(locationArea);
		snapshot.setPeriod(period);
		snapshot.setStatus(OperationsCentreSnapshotStatusDTO.MOVING);
		OperationsCentreSnapshotDTO ocSnapshot = operationsCentreService.createOperationsCentreSnapshotRemote(operationsCentre, snapshot, this.userID);
		return ocSnapshot;
	}

	@SuppressWarnings("unused")
	private MapPoint createMapPoint(String title) {
		MapPoint mapPoint = new MapPoint();
		mapPoint.setTitle(title);
		Point point = new Point();
		point.setLongitude(BigDecimal.ONE);
		point.setLatitude(BigDecimal.ONE);
		mapPoint.setPoint(point);
		return mapPoint;
	}


	private MapPointDTO createMapPointDTO(String title, Integer icon, BigDecimal latitude, BigDecimal longitude ) {
		MapPointDTO mapPoint = new MapPointDTO();
		mapPoint.setTitle(title);
		mapPoint.setIcon(icon);
		PointDTO point = new PointDTO();
		point.setLongitude(longitude);
		point.setLatitude(latitude);
		mapPoint.setPoint(point);
		return mapPoint;
	}

	private ReferencePOIDTO createReferencePOIDTO(String referenceFile, String title, Float averageSize) {
		ReferencePOIDTO ref = new ReferencePOIDTO();
		ref.setTitle(title);
		ref.setAverageSize(averageSize);
		return ref;
	}

	private HttpURLDTO createHttpURLDTO(String host, String path, String protocol) {
		HttpURLDTO httpURL = new HttpURLDTO();
		httpURL.setHost(host);
		httpURL.setPath(path);
		httpURL.setProtocol(protocol);
		return httpURL;
	}
	
	
	private VoIPURLDTO createVoIPURLDTO(String host, String path, String protocol) {
		VoIPURLDTO voipURL = new VoIPURLDTO();
		voipURL.setHost(host);
		voipURL.setPath(path);
		voipURL.setProtocol(protocol);
		return voipURL;
	}

}