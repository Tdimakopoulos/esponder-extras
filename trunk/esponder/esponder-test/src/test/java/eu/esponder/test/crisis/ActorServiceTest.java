package eu.esponder.test.crisis;

import java.math.BigDecimal;

import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.resource.ActorCMDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.crisis.resource.OCEocDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActorSnapshotStatusDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.test.dbtest.ControllerServiceTest;

public class ActorServiceTest extends ControllerServiceTest {

	private static int SECONDS = 30;
	@SuppressWarnings("unused")
	private static double RADIUS = 1;
	
	
	@SuppressWarnings("unused")
	@Test(groups = "createResources")
	public void testCreateActorsDTO() throws ClassNotFoundException {

		OCEocDTO eocAttica = (OCEocDTO) operationsCentreService
				.findEocByTitleRemote("EOC Schiphol");
		
//		CrisisContextDTO crisisContext = crisisService.findCrisisContextDTOByTitle("Fire Brigade Drill");
		
		//*****************************************************************************
		
		ActorCMDTO crisisManager = (ActorCMDTO) createActorDTO(ActorCMDTO.class,eocAttica, "CM #1", null,
				"Manager No1", "192.168.11.1", "CM#1", "http");
		
		eocAttica.setCrisisManager(crisisManager.getId());
//		eocAttica.setCrisisContext(crisisContext);
		operationsCentreService.updateEOCRemote(eocAttica, userID);
		
		
		//*****************************************************************************
		OCMeocDTO meocAthens = (OCMeocDTO) operationsCentreService
				.findMeocByTitleRemote("MEOC Schiphol I");
		 
		ActorICDTO incidentCommander1 = (ActorICDTO) createActorDTO(ActorICDTO.class, meocAthens, "IC #1", crisisManager, 
				"Incident Commander No1", "192.168.11.1", "IC#1", "http");
		
		meocAthens.setIncidentCommander(incidentCommander1.getId());
//		meocAthens.setCrisisContext(crisisContext);
		operationsCentreService.updateMEOCRemote(meocAthens, userID);
		
		//*****************************************************************************
		
		OCMeocDTO meocPiraeus = (OCMeocDTO) operationsCentreService
				.findMeocByTitleRemote("MEOC Schiphol I");
//		ActorICDTO incidentCommander2 = (ActorICDTO) createActorDTO(ActorICDTO.class,meocPiraeus, "IC #2", crisisManager, 
//				"Incident Commander No2", "192.168.11.1", "IC#2", "http");
		
//		meocPiraeus.setIncidentCommander(incidentCommander2.getId());
//		meocPiraeus.setCrisisContext(crisisContext);
//		operationsCentreService.updateMEOCRemote(meocPiraeus, userID);
		
		//*****************************************************************************

		ActorFRCDTO firstChief = (ActorFRCDTO) createActorDTO(ActorFRCDTO.class, null,"FRC #1", incidentCommander1, 
				"Employee No1", "192.168.11.1", "FRC#1", "http");
		
		ActorFRDTO subordinate11 = (ActorFRDTO) createActorDTO(ActorFRDTO.class, null, "FR #1.1", firstChief,
				"Employee No2", "192.168.11.1", "FR#1.1", "http");
		
		ActorFRDTO subordinate12 = (ActorFRDTO) createActorDTO(ActorFRDTO.class, null, "FR #1.2", firstChief,
				"Employee No3", "192.168.11.1", "FR#1.2", "http");
		
		ActorFRDTO subordinate21 = (ActorFRDTO) createActorDTO(ActorFRDTO.class, null, "FR #2.1", firstChief,
		"Employee No5", "192.168.11.1", "FR#2.1", "http");

ActorFRDTO subordinate22 = (ActorFRDTO) createActorDTO(ActorFRDTO.class, null, "FR #2.2", firstChief,
		"Employee No6", "192.168.11.1", "FR#2.1", "http");

ActorFRDTO subordinate31 = (ActorFRDTO) createActorDTO(ActorFRDTO.class, null, "FR #3.1", firstChief,
"Employee No7", "192.168.11.1", "FR#2.1", "http");

ActorFRDTO subordinate32 = (ActorFRDTO) createActorDTO(ActorFRDTO.class, null, "FR #3.2", firstChief,
"Employee No8", "192.168.11.1", "FR#2.1", "http");

		
//		ActorFRCDTO secondChief = (ActorFRCDTO) createActorDTO(ActorFRCDTO.class, null, "FRC #2", incidentCommander2, 
//				"Employee No4", "192.168.11.1", "FRC#2", "http");
//		
//		ActorFRDTO subordinate21 = (ActorFRDTO) createActorDTO(ActorFRDTO.class, null, "FR #2.1", secondChief,
//				"Employee No5", "192.168.11.1", "FR#2.1", "http");
//		
//		ActorFRDTO subordinate22 = (ActorFRDTO) createActorDTO(ActorFRDTO.class, null, "FR #2.2", secondChief,
//				"Employee No6", "192.168.11.1", "FR#2.1", "http");
		
		
		//----------------------------------------------------------------------
		// Update ESponderUsers with corresponding OCs
		
//		ESponderUserDTO user1 = actorService.findESponderUserByTitleRemote("Dimitris");
//		ESponderUserDTO user2 = actorService.findESponderUserByTitleRemote("Thomas");
//		ESponderUserDTO user3 = actorService.findESponderUserByTitleRemote("Jesus");
//		
//		eocAttica = (OCEocDTO) operationsCentreService.findEocByTitleRemote("EOC Attica");
//		meocAthens = (OCMeocDTO) operationsCentreService.findMeocByTitleRemote("MEOC Athens");
//		meocPiraeus = (OCMeocDTO) operationsCentreService.findMeocByTitleRemote("MEOC Piraeus");
//		
//		user1.setOperationsCentre(eocAttica);
//		user2.setOperationsCentre(meocAthens);
//		user3.setOperationsCentre(meocPiraeus);
//		
//		actorService.updateEsponderUserRemote(user1, userID);
//		actorService.updateEsponderUserRemote(user2, userID);
//		actorService.updateEsponderUserRemote(user3, userID);
	}
	
	@Test
	public void AssociateUserandOp() throws ClassNotFoundException {
		ESponderUserDTO user1 = actorService.findESponderUserByTitleRemote("Dimitris FRC");
		ESponderUserDTO user2 = actorService.findESponderUserByTitleRemote("Thomas IC");
		ESponderUserDTO user3 = actorService.findESponderUserByTitleRemote("Jesus FR");
		
		OCEocDTO eocAttica = (OCEocDTO) operationsCentreService.findEocByTitleRemote("EOC Schiphol");
		OCMeocDTO meocAthens = (OCMeocDTO) operationsCentreService.findMeocByTitleRemote("MEOC Schiphol I");
//		OCMeocDTO meocPiraeus = (OCMeocDTO) operationsCentreService.findMeocByTitleRemote("MEOC Schiphol II");
		
		user1.setOperationsCentre(eocAttica);
		user2.setOperationsCentre(meocAthens);
		user3.setOperationsCentre(meocAthens);
		
		actorService.updateEsponderUserRemote(user1, userID);
		actorService.updateEsponderUserRemote(user2, userID);
		actorService.updateEsponderUserRemote(user3, userID);
	
	}
	@Test(groups = "createSnapshots")
	public void testCreateActorSnapshotsDTO() throws ClassNotFoundException {

		ActorDTO firstFRC = (ActorDTO) actorService.findFRCByTitleRemote("FRC #1");
		ActorDTO firstFR = (ActorDTO) actorService.findFRByTitleRemote("FR #1.1");
		ActorDTO secondFR = (ActorDTO) actorService
				.findFRByTitleRemote("FR #1.2");
//		ActorDTO secondFRC = (ActorDTO) actorService.findFRCByTitleRemote("FRC #2");
		ActorDTO thirdFR = (ActorDTO) actorService.findFRByTitleRemote("FR #2.1");
		ActorDTO fourthFR = (ActorDTO) actorService
				.findFRByTitleRemote("FR #2.2");
		
		ActorDTO fifthFR = (ActorDTO) actorService.findFRByTitleRemote("FR #3.1");
		ActorDTO sixthFR = (ActorDTO) actorService
				.findFRByTitleRemote("FR #3.2");

		PeriodDTO period = this.createPeriodDTO(SECONDS);
		
		PointDTO point1 = new PointDTO(new BigDecimal(52.318919),
				new BigDecimal(4.748723), null);
		
		PointDTO point2 = new PointDTO(new BigDecimal(52.318906),
				new BigDecimal(4.748834), null);
		
		PointDTO point3 = new PointDTO(new BigDecimal(52.318863),
				new BigDecimal(4.748906), null);
		
		PointDTO point4 = new PointDTO(new BigDecimal(52.318824),
				new BigDecimal(4.748984), null);
		
		PointDTO point5 = new PointDTO(new BigDecimal(52.318779),
				new BigDecimal(4.749064), null);
		
		PointDTO point6 = new PointDTO(new BigDecimal(52.318737),
				new BigDecimal(4.749147), null);
		
		//****************************************************

		SphereDTO sphere1 = new SphereDTO();
		sphere1.setCentre(point1);
		sphere1.setRadius(new BigDecimal(0));

		sphere1.setTitle("Sphere 1");

		SphereDTO sphere1p = (SphereDTO) genericService.createEntityRemote(
				sphere1, new Long(1));

		SphereDTO sphere2 = new SphereDTO();
		sphere2.setCentre(point2);
		sphere2.setRadius(new BigDecimal(0));

		sphere2.setTitle("Sphere 2");

		SphereDTO sphere2p = (SphereDTO) genericService.createEntityRemote(
				sphere2, new Long(1));

		SphereDTO sphere3 = new SphereDTO();
		sphere3.setCentre(point3);
		sphere3.setRadius(new BigDecimal(0));

		sphere3.setTitle("Sphere 3");

		SphereDTO sphere3p = (SphereDTO) genericService.createEntityRemote(
				sphere3, new Long(1));

		SphereDTO sphere4 = new SphereDTO();
		sphere4.setCentre(point4);
		sphere4.setRadius(new BigDecimal(0));

		sphere4.setTitle("Sphere 4");

		SphereDTO sphere4p = (SphereDTO) genericService.createEntityRemote(
				sphere4, new Long(1));

		SphereDTO sphere5 = new SphereDTO();
		sphere5.setCentre(point5);
		sphere5.setRadius(new BigDecimal(0));

		sphere5.setTitle("Sphere 5");

		SphereDTO sphere5p = (SphereDTO) genericService.createEntityRemote(
				sphere5, new Long(1));

		SphereDTO sphere6 = new SphereDTO();
		sphere6.setCentre(point6);
		sphere6.setRadius(new BigDecimal(0));

		sphere6.setTitle("Sphere 6");

		SphereDTO sphere6p = (SphereDTO) genericService.createEntityRemote(
				sphere6, new Long(1));

		
		SphereDTO sphere51 = new SphereDTO();
		sphere51.setCentre(point5);
		sphere51.setRadius(new BigDecimal(0));

		sphere51.setTitle("Sphere 51");

		SphereDTO sphere5p1 = (SphereDTO) genericService.createEntityRemote(
				sphere51, new Long(1));
		
		
		createActorSnapshotDTO(firstFRC, period, sphere1p);
		createActorSnapshotDTO(firstFR, period, sphere2p);
		createActorSnapshotDTO(secondFR, period, sphere3p);
		createActorSnapshotDTO(thirdFR, period, sphere4p);
		createActorSnapshotDTO(fourthFR, period, sphere5p);
		createActorSnapshotDTO(fifthFR, period, sphere6p);
		createActorSnapshotDTO(sixthFR, period, sphere5p1);
	}
	
	
	@SuppressWarnings("unused")
	@Test(groups = "createResources")
	public void testCreateFRTeams() throws ClassNotFoundException {
		
		ActorICDTO icDTO  = (ActorICDTO) actorService.findICByTitleRemote("IC #1");
		
		ActorFRCDTO firstChiefDTO  = (ActorFRCDTO) actorService.findFRCByTitleRemote("FRC #1");
		
//		ActorFRCDTO secondChiefDTO  = (ActorFRCDTO) actorService.findFRCByTitleRemote("FRC #2");
		
		OCMeocDTO meocAthens = (OCMeocDTO) operationsCentreService
				.findMeocByTitleRemote("MEOC Schiphol I");
//		OCMeocDTO meocPiraeus = (OCMeocDTO) operationsCentreService
//				.findMeocByTitleRemote("MEOC Schiphol II");
		
		FRTeamDTO frTeam1 = new FRTeamDTO();
		frTeam1.setFrchief(firstChiefDTO.getId());
		frTeam1.setTitle("FR TEAM #1");
		frTeam1.setIncidentCommander(icDTO.getId());
		
		
//		FRTeamDTO frTeam2 = new FRTeamDTO();
//		frTeam2.setFrchief(secondChiefDTO.getId());
//		frTeam2.setTitle("FR TEAM #2");
//		frTeam2.setIncidentCommander(icDTO.getId());
		
		frTeam1 = (FRTeamDTO) genericService.createEntityRemote(frTeam1, new Long(1));
//		frTeam2 = (FRTeamDTO) genericService.createEntityRemote(frTeam2, new Long(1));
		
	}

	private ActorDTO createActorDTO(Class<? extends ActorDTO> actorClass,OperationsCentreDTO oc,  String title,
			ActorDTO supervisor, 
			String personnelTitle, String host, String path, String protocol) {

		PersonnelDTO personnelDTO = personnelService
				.findPersonnelByTitleRemote(personnelTitle);
		if (personnelDTO != null) {
			
			if(actorClass == ActorCMDTO.class) {
				ActorCMDTO actorDTO = new ActorCMDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				actorDTO.setOperationsCentre(oc.getId());
				return actorService.createCrisisManagerRemote(actorDTO, this.userID);
			}
			else if(actorClass == ActorICDTO.class) {
				ActorICDTO actorDTO = new ActorICDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				actorDTO.setCrisisManager(supervisor.getId());
				actorDTO.setOperationsCentre(oc.getId());
				return actorService.createIncidentCommanderRemote(actorDTO, this.userID);
			}
			else if(actorClass == ActorFRCDTO.class) {
				ActorFRCDTO actorDTO = new ActorFRCDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				return actorService.createFRChiefRemote(actorDTO, this.userID);
			} 
			else if(actorClass == ActorFRDTO.class) {
				ActorFRDTO actorDTO = new ActorFRDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				actorDTO.setFRChief(supervisor.getId());
				return actorService.createFRRemote(actorDTO, this.userID);
			}
		} else {
			//ESponderLogger.debug(this.getClass(),
				//	"Requested Personnel cannot be found.");
			return null;
		}
		return null;

	}

	private ActorSnapshotDTO createActorSnapshotDTO(ActorDTO actor,
			PeriodDTO period, LocationAreaDTO locationArea) {

		ActorSnapshotDTO snapshot = new ActorSnapshotDTO();
		snapshot.setActor(actor.getId());
		snapshot.setLocationArea(locationArea);
		snapshot.setPeriod(period);
		snapshot.setStatus(ActorSnapshotStatusDTO.ACTIVE);
		return actorService.createActorSnapshotRemote(snapshot, this.userID);
	}

	private VoIPURLDTO createVoIPURLDTO(String host, String path,
			String protocol) {
		VoIPURLDTO voipURL = new VoIPURLDTO();
		voipURL.setHost(host);
		voipURL.setPath(path);
		voipURL.setProtocol(protocol);
		return voipURL;
	}

}
