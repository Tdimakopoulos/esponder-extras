package eu.esponder.test.crisis;

import org.testng.annotations.Test;

import eu.esponder.dto.model.config.DroolsConfigurationDTO;
import eu.esponder.test.dbtest.ControllerServiceTest;


public class ESponderConfigurationServiceTest extends ControllerServiceTest {
	
	
	@Test(groups="CreateConfiguration")
	public void createESponderConfigurationOSGI() throws ClassNotFoundException {
		String szOsgiFilePath="//home//exodus//osgi//osgi.config.properties";
		DroolsConfigurationDTO configDTO1 = new DroolsConfigurationDTO();
		configDTO1.setDescription("OSGI properties files");
		configDTO1.setParameterName("OSGIPropertiesFiles");
		configDTO1.setParameterValue(szOsgiFilePath);
		configService.createESponderConfigRemote(configDTO1, userID);
	}
	
	@Test(groups="CreateConfiguration")
	public void createESponderConfiguration() throws ClassNotFoundException {
        /////////////////////////////////////////////////////////////////////////
		//change this from windows to unix deployment
		String szTmpRepositoryDirectoryPath="//home//exodus//osgi//";
		String szDroolApplicationServerURL="http://192.168.2.248:8080/";
		String szDroolWarDeploymentPath="guvnor";
		String szDroolServiceEntryPoint="rest";
		////////////////////////////////////////////////////////////////////////
		
		String szDroolUsername="guest";
		String szDroolPassword="";
		String szDroolRepo="local";
		
		DroolsConfigurationDTO configDTO1 = new DroolsConfigurationDTO();
		configDTO1.setDescription("Temp Repository");
		configDTO1.setParameterName("DroolTempRep");
		configDTO1.setParameterValue(szTmpRepositoryDirectoryPath);
		
		DroolsConfigurationDTO configDTO2 = new DroolsConfigurationDTO();
		configDTO2.setDescription("Drools Server URL");
		configDTO2.setParameterName("DroolURL");
		configDTO2.setParameterValue(szDroolApplicationServerURL);
		
		DroolsConfigurationDTO configDTO3 = new DroolsConfigurationDTO();
		configDTO3.setDescription("Service Point");
		configDTO3.setParameterName("DroolSPoint");
		configDTO3.setParameterValue(szDroolServiceEntryPoint);
		
		DroolsConfigurationDTO configDTO4 = new DroolsConfigurationDTO();
		configDTO4.setDescription("Drools War Name");
		configDTO4.setParameterName("DroolWARNAME");
		configDTO4.setParameterValue(szDroolWarDeploymentPath);
		
		DroolsConfigurationDTO configDTO5 = new DroolsConfigurationDTO();
		configDTO5.setDescription("Username");
		configDTO5.setParameterName("DroolUsername");
		configDTO5.setParameterValue(szDroolUsername);
		
		DroolsConfigurationDTO configDTO6 = new DroolsConfigurationDTO();
		configDTO6.setDescription("Password");
		configDTO6.setParameterName("DroolPassword");
		configDTO6.setParameterValue(szDroolPassword);
		
		DroolsConfigurationDTO configDTO7 = new DroolsConfigurationDTO();
		configDTO7.setDescription("Refresh");
		configDTO7.setParameterName("DroolRefresh");
		configDTO7.setParameterValue(szDroolRepo);
		
		configService.createESponderConfigRemote(configDTO1, userID);
		configService.createESponderConfigRemote(configDTO2, userID);
		configService.createESponderConfigRemote(configDTO3, userID);
		configService.createESponderConfigRemote(configDTO4, userID);
		configService.createESponderConfigRemote(configDTO5, userID);
		configService.createESponderConfigRemote(configDTO6, userID);
		configService.createESponderConfigRemote(configDTO7, userID);
		
		/*String URI = CONFIGURATION_SERVICE_URI + "create";
		DroolsConfigurationDTO configDTO1 = new DroolsConfigurationDTO();
		configDTO1.setParameterName("DROOLS_BASE_URI");
		configDTO1.setParameterValue("http://192.168.3.19:8080/");
		configDTO1.setDescription("Drools base URI for IntegrationTest");

		DroolsConfigurationDTO configDTO2 = new DroolsConfigurationDTO();
		configDTO2.setParameterName("DROOLS_BASE_URI2");
		configDTO2.setParameterValue("http://192.168.4.19:8080/");
		configDTO2.setDescription("Drools base URI 2 for IntegrationTest");

		DroolsConfigurationDTO configDTO3 = new DroolsConfigurationDTO();
		configDTO3.setParameterName("DROOLS_BASE_URI3");
		configDTO3.setParameterValue("http://192.168.5.19:8080/");
		configDTO3.setDescription("Drools base URI 3 for IntegrationTest");
		Parser parser = new Parser(new Class[] {ESponderConfigParameterDTO.class});

		ResteasyClient postClient = new ResteasyClient(URI, "application/xml");

		Map<String, String> params = this.getIDServiceParameters();

		String bodyStr1 = parser.marshall(configDTO1);
		String resultXML1 = postClient.post(params, bodyStr1);
		printResultsXML(parser, resultXML1);

		String bodyStr2 = parser.marshall(configDTO2);
		String resultXML2 = postClient.post(params, bodyStr2);
		printResultsXML(parser, resultXML2);

		String bodyStr3 = parser.marshall(configDTO3);
		String resultXML3 = postClient.post(params, bodyStr3);
		printResultsXML(parser, resultXML3);*/
	}
}
