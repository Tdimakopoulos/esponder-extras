package eu.esponder.datasync.server;

import eu.esponder.datasync.server.main.ESponderSymmetricWebServer;


public class ESponderDataSyncServer {

	public static void main(String[] args) throws Exception {
	    
		ESponderSymmetricWebServer node= new ESponderSymmetricWebServer(
                                   "C:/development/EDSM.bin/conf/symmetric-server.properties", "C:/development/EDSM.bin/web/WEB-INF");

        // this will create the database, sync triggers, start jobs running
        node.start(31415);
        
        // this will stop the node
        node.stop();
    }   
}
