package eu.esponder.osgi.test;

//import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;

import org.testng.annotations.Test;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.dto.model.snapshot.status.ActionSnapshotStatusDTO;
import eu.esponder.dto.model.snapshot.status.CrisisContextSnapshotStatusDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.datafusion.query.ESponderDFQueryRequestEvent;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.login.LoginRequestEvent;
import eu.esponder.event.model.snapshot.CreateCrisisContextSnapshotEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;
import eu.esponder.util.ejb.ServiceLocator;

public class PublisherTest {

	public static void main(String[] args) {
		ESponderEventPublisher<ESponderDFQueryRequestEvent> publisher = new ESponderEventPublisher<ESponderDFQueryRequestEvent>(
				ESponderDFQueryRequestEvent.class);

		try {
		

			
				ESponderUserDTO actionSnapshot = new ESponderUserDTO();
				actionSnapshot.setId(new Long(0));

		
///////////////////////////////// TEST QUERY /////////////////////////////////////////////////////
			ESponderDFQueryRequestEvent pEvent = new ESponderDFQueryRequestEvent();
			//Map<String, Object> m = new HashMap<String, Object>() {{
			  //  put("USERID", "2");
			//}};
			pEvent.setQueryID(new Long(1));
			pEvent.setRequestID(new Long(20));
			pEvent.setJournalMessage("JM");
			QueryParamsDTO pParams= new QueryParamsDTO().with("USERID", "6");
			
			//pParams.setQueryParamList(m);
			pEvent.setParams(pParams);
			pEvent.setEventAttachment(actionSnapshot);
			pEvent.setJournalMessage("Test Event");
			pEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
			pEvent.setEventTimestamp(new Date());
			pEvent.setEventSeverity(SeverityLevelDTO.UNDEFINED);
			 pEvent.setEventSource(actionSnapshot);
///////////////////////////////// TEST QUERY /////////////////////////////////////////////////////
			
			publisher.publishEvent(pEvent);
			Thread.currentThread().sleep(20000);
		} catch (EventListenerException e) {

			e.printStackTrace();
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}
	// ESponderDFQueryRequestEvent
	@SuppressWarnings("static-access")
	public static void main33(String[] args) {
		ESponderEventPublisher<LoginRequestEvent> publisher = new ESponderEventPublisher<LoginRequestEvent>(
				LoginRequestEvent.class);

		try {
		

			
				ESponderUserDTO actionSnapshot = new ESponderUserDTO();
				actionSnapshot.setId(new Long(0));

		///////////////////////////////// TEST LOGIN /////////////////////////////////////////////////////
			LoginRequestEvent pEvent = new LoginRequestEvent();
			pEvent.setPassword("Password");
			pEvent.setUsername("FRC");
			pEvent.setPkikey("FRC");
			 pEvent.setEventAttachment(actionSnapshot);
			pEvent.setJournalMessage("Login request Username : "+pEvent.getUsername()+" PKI : "+pEvent.getPkikey());
			pEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
			pEvent.setEventTimestamp(new Date());
			//pEvent.setEventSeverity(SeverityLevelDTO.UNDEFINED);
			 pEvent.setEventSource(actionSnapshot);
			
///////////////////////////////// TEST QUERY /////////////////////////////////////////////////////
//			ESponderDFQueryRequestEvent pEvent = new ESponderDFQueryRequestEvent();
//			pEvent.setQueryID(new Long(10));
//			pEvent.setRequestID(new Long(20));
//			pEvent.setJournalMessage("JM");
//
//			 pEvent.setEventAttachment(actionSnapshot);
//			pEvent.setJournalMessage("Test Event");
//			pEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
//			pEvent.setEventTimestamp(new Date());
//			pEvent.setEventSeverity(SeverityLevelDTO.UNDEFINED);
//			 pEvent.setEventSource(actionSnapshot);
///////////////////////////////// TEST QUERY /////////////////////////////////////////////////////
			//publisher.setEventTopic("eu/esponder/login/request");
			publisher.publishEvent(pEvent);
			Thread.currentThread().sleep(20000);
		} catch (EventListenerException e) {

			e.printStackTrace();
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	@Test
	public void genericPublisherTest() {
		ESponderEventPublisher<CreateActionSnapshotEvent> publisher = new ESponderEventPublisher<CreateActionSnapshotEvent>(
				CreateActionSnapshotEvent.class);

		try {
			// publisher.setEventTopic("createaction");
			publisher.publishEvent(createActionSnapshotEvent());
		} catch (EventListenerException e) {

			e.printStackTrace();
		}
	}

	// public void sendSampleEvent() {
	// try {
	// // create simple event, it will be received from other connected
	// // clients
	// DictionaryInfo data = new DictionaryInfo();
	// data.put("event", createSampleEvent());
	//
	// // send the event
	// String eventTopic = CreateActionEvent.class.getName();
	// eventService.event(eventTopic, data);
	// System.out.println("Event Sent: " + data);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/*
	 * public ESponderEvent createActionSnapshotEvent() {
	 * BodyTemperatureSensorDTO bodyTemperatureSensor = new
	 * BodyTemperatureSensorDTO();
	 * bodyTemperatureSensor.setMeasurementUnit(MeasurementUnitEnumDTO
	 * .DEGREES_CELCIUS); bodyTemperatureSensor.setName("Body Temperature");
	 * bodyTemperatureSensor.setType("BodyTemp");
	 * bodyTemperatureSensor.setLabel("BdTemp");
	 * bodyTemperatureSensor.setTitle("Body Temperature Sensor");
	 * bodyTemperatureSensor.setId(System.currentTimeMillis());
	 * bodyTemperatureSensor.setStatus(ResourceStatusDTO.AVAILABLE);
	 * bodyTemperatureSensor
	 * .setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
	 * 
	 * ArithmeticSensorMeasurementDTO sensorMeasurement1 = new
	 * ArithmeticSensorMeasurementDTO(); sensorMeasurement1.setMeasurement(new
	 * BigDecimal(200)); sensorMeasurement1.setId(99L);
	 * sensorMeasurement1.setTimestamp(new Date());
	 * sensorMeasurement1.setSensor(bodyTemperatureSensor);
	 * 
	 * SensorMeasurementStatisticDTO statisticArethmetic = new
	 * SensorMeasurementStatisticDTO();
	 * statisticArethmetic.setStatistic(sensorMeasurement1);
	 * statisticArethmetic.
	 * setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
	 * sensorMeasurement1.setId(89L);
	 * 
	 * List<SensorMeasurementStatisticDTO> sensorMeasurementStatistic = new
	 * ArrayList<SensorMeasurementStatisticDTO>();
	 * sensorMeasurementStatistic.add(statisticArethmetic);
	 * 
	 * SensorMeasurementStatisticEnvelopeDTO sensorMeasurementStatisticEnvelope
	 * = new SensorMeasurementStatisticEnvelopeDTO();
	 * sensorMeasurementStatisticEnvelope
	 * .setMeasurementStatistics(sensorMeasurementStatistic);
	 * sensorMeasurementStatisticEnvelope.setId(System.currentTimeMillis());
	 * 
	 * ActorDTO subactorDTO = new ActorDTO(); subactorDTO.setId(new Long(2));
	 * subactorDTO.setType("FRC 2222"); subactorDTO.setTitle("FRC #2222");
	 * subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
	 * 
	 * ESponderEvent<SensorMeasurementStatisticEnvelopeDTO> statisticEvent2 =
	 * new CreateSensorMeasurementStatisticEvent();
	 * statisticEvent2.setEventSeverity(SeverityLevelDTO.SERIOUS);
	 * statisticEvent2.setEventTimestamp(new Date());
	 * statisticEvent2.setEventAttachment(sensorMeasurementStatisticEnvelope);
	 * statisticEvent2.setEventSource(subactorDTO);
	 * 
	 * return statisticEvent2; }
	 */

	private static ESponderEvent<? extends ESponderEntityDTO> createActionSnapshotEvent() {
		ActionSnapshotDTO actionSnapshot = new ActionSnapshotDTO();
		ActorDTO subactorDTO = new ActorDTO();
		subactorDTO.setId(new Long(2));
		subactorDTO.setType("FRC 2222");
		subactorDTO.setTitle("FRC #2222");
		subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		actionSnapshot.setStatus(ActionSnapshotStatusDTO.STARTED);
		actionSnapshot.setId(new Long(10));

		CreateActionSnapshotEvent result = new CreateActionSnapshotEvent();
		result.setEventAttachment(actionSnapshot);
		result.setJournalMessage("Test Event");
		result.setEventSeverity(SeverityLevelDTO.SERIOUS);
		result.setEventTimestamp(new Date());
		result.setEventSeverity(SeverityLevelDTO.UNDEFINED);
		result.setEventSource(subactorDTO);
		return result;
	}

	private static ESponderEvent<? extends ESponderEntityDTO> createCrisisContextSnapshotEvent() {

		CrisisRemoteService crisisService = null;
		try {
			crisisService = ServiceLocator
					.getResource("esponder/CrisisBean/remote");
		} catch (NamingException e) {

			e.printStackTrace();
		}

		CrisisContextSnapshotDTO crisisContextSnapshot = new CrisisContextSnapshotDTO();
		CrisisContextDTO crisisContextDTO = crisisService
				.findCrisisContextDTOByTitle("Fire Brigade Drill");
		crisisContextSnapshot.setCrisisContext(crisisContextDTO);
		crisisContextSnapshot.setLocationArea(crisisContextDTO
				.getCrisisLocation());
		crisisContextSnapshot.setPeriod(new PeriodDTO(new Date().getTime(),
				new Date().getTime() + (new Long(300000))));
		crisisContextSnapshot.setStatus(CrisisContextSnapshotStatusDTO.STARTED);

		ActorDTO subactorDTO = new ActorDTO();
		subactorDTO.setId(new Long(2));
		subactorDTO.setType("FRC 2223");
		subactorDTO.setTitle("FRC #2223");
		subactorDTO.setStatus(ResourceStatusDTO.AVAILABLE);

		CreateCrisisContextSnapshotEvent result = new CreateCrisisContextSnapshotEvent();
		result.setEventAttachment(crisisContextSnapshot);
		result.setJournalMessage("Test Event");
		result.setEventSeverity(SeverityLevelDTO.SERIOUS);
		result.setEventTimestamp(new Date());
		result.setEventSeverity(SeverityLevelDTO.UNDEFINED);

		result.setEventSource(subactorDTO);
		return result;
	}
}
