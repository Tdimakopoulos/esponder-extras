package eu.esponder.osgi.threadsafe.server.main;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class CacheServer implements Runnable {

	int port=0;
	String clientHost="";
	int clientPort=0;
	
	public String removeFirstChar(String s) {
		return s.substring(1);
	}
	
	public void StartServer(int portd, String clientHostd, int clientPortd)
	{
		port=portd;
		clientHost=clientHostd;
		clientPort=clientPortd;	
	}
	
	public void run()
			 {

		ArrayList<String> cachesink = new ArrayList<String>();

		UDPSend psend = new UDPSend();

		// Create a socket to listen on the port.
		DatagramSocket dsocket=null;
		try {
			dsocket = new DatagramSocket(port);
		} catch (SocketException e) {
			System.out.println(" Cache Server Error : "+e.getMessage());
		}

		// Create a buffer to read datagrams into. If anyone sends us a
		// packet containing more than will fit into this buffer, the excess
		// will simply be discarded!
		byte[] buffer = new byte[2048];

		boolean brun = true;
		while (brun) {

			// Create a packet with an empty buffer to receive data
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

			// Wait to receive a datagram
			try {
				dsocket.receive(packet);
			} catch (IOException e) {
				//System.out.println(" Cache Server Error : "+e.getMessage());
			}

			// Convert the contents to a string, and display them
			String msg = new String(buffer, 0, packet.getLength());
			System.out.println(msg);
			if (msg.charAt(0) == 'q') {
				brun = false;
			}
			if (msg.charAt(0) == 's') {
				// save values
				cachesink.add(removeFirstChar(msg));
			}
			if (msg.charAt(0) == 'f') {
				// search values
				String FindOnCache = removeFirstChar(msg);
				boolean bfind = false;
				for (int i = 0; i < cachesink.size(); i++) {
					if (cachesink.get(i).equalsIgnoreCase(FindOnCache)) {
						System.out.println("Find");
						try {
							psend.SendValue(clientHost, clientPort, "1");
						} catch (UnknownHostException e) {
							System.out.println(" Cache Server Error : "+e.getMessage());
						} catch (SocketException e) {
							System.out.println(" Cache Server Error : "+e.getMessage());
						} catch (IOException e) {
							System.out.println(" Cache Server Error : "+e.getMessage());
						}
						bfind = true;
					}
				}
				if (bfind == false) {
					try {
						psend.SendValue(clientHost, clientPort, "0");
					} catch (UnknownHostException e) {
						System.out.println(" Cache Server Error : "+e.getMessage());
					} catch (SocketException e) {
						System.out.println(" Cache Server Error : "+e.getMessage());
					} catch (IOException e) {
						System.out.println(" Cache Server Error : "+e.getMessage());
					}
				}
			}
		}
		dsocket.close();

	}
}
