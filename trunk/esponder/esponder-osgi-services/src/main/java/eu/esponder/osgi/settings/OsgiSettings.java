/*
 * 
 */
package eu.esponder.osgi.settings;

import java.io.File;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
import eu.esponder.test.ResourceLocator;


// TODO: Auto-generated Javadoc
/**
 * The Class OsgiSettings.
 */
public class OsgiSettings {


	/** The sz properties file name unix. */
	private String szPropertiesFileNameUnix = "//home//exodus//osgi//osgi.config.properties";

	// comment for unix, uncomment for windows
	/** The sz properties file name windows. */
	private String szPropertiesFileNameWindows = "C://Development//osgi.config.properties";

	/** The sz properties file name. */
	private String szPropertiesFileName = "";
	
	/** The sz properties file name unix. */
	private String szPropertiesFileNameUnixOtherSRV = "//home//exodus//osgi//osgiothersrv.config.properties";

	// comment for unix, uncomment for windows
	/** The sz properties file name windows. */
	private String szPropertiesFileNameWindowsOtherSRV = "C://Development//osgiothersrv.config.properties";

	/** The sz properties file name. */
	private String szPropertiesFileNameOtherSRV = "";

	/**
	 * Checks if is unix.
	 *
	 * @return true, if is unix
	 */
	private boolean isUnix() {
		if (File.separatorChar == '/')
			return true;
		else
			return false;
	}

	/**
	 * Instantiates a new osgi settings.
	 */
	public OsgiSettings() {
		
			GetAllSettings();

		
	}

	/**
	 * Load settings.
	 */
	public void LoadSettings() {
		
			GetAllSettings();
		
	}

	/**
	 * Gets the all settings.
	 */
	private void GetAllSettings()   {

		try {
			ESponderConfigurationRemoteService cfgService = ResourceLocator
					.lookup("esponder/ESponderConfigurationBean/remote");

			// Get DroolsTempRep
			ESponderConfigParameterDTO dTempRep = cfgService
					.findESponderConfigByNameRemote("OSGIPropertiesFiles");
			try {
				szPropertiesFileName = dTempRep.getParameterValue();
			} catch (Exception e) {
				if (isUnix()) {
					szPropertiesFileName = szPropertiesFileNameUnix;
				} else {
					szPropertiesFileName = szPropertiesFileNameWindows;
				}
			}
			
			
			//javax.naming.NoInitialContextException
			
		} catch(NoClassDefFoundError e) {
			if (isUnix()) {
				szPropertiesFileName = szPropertiesFileNameUnix;
			} else {
				szPropertiesFileName = szPropertiesFileNameWindows;
			}
		}
		catch(RuntimeException e2) {
			if (isUnix()) {
				szPropertiesFileName = szPropertiesFileNameUnix;
			} else {
				szPropertiesFileName = szPropertiesFileNameWindows;
			}
		}
		catch(ExceptionInInitializerError e3) {
			if (isUnix()) {
				szPropertiesFileName = szPropertiesFileNameUnix;
			} else {
				szPropertiesFileName = szPropertiesFileNameWindows;
			}
		} catch (ClassNotFoundException e1) {
			if (isUnix()) {
				szPropertiesFileName = szPropertiesFileNameUnix;
			} else {
				szPropertiesFileName = szPropertiesFileNameWindows;
			}
		}
		
		//other srv
		try {
			ESponderConfigurationRemoteService cfgService = ResourceLocator
					.lookup("esponder/ESponderConfigurationBean/remote");

			// Get DroolsTempRep
			ESponderConfigParameterDTO dTempRep = cfgService
					.findESponderConfigByNameRemote("OSGIPropertiesFilesOtherSRV");
			try {
				szPropertiesFileNameOtherSRV = dTempRep.getParameterValue();
			} catch (Exception e) {
				if (isUnix()) {
					szPropertiesFileNameOtherSRV = szPropertiesFileNameUnixOtherSRV;
				} else {
					szPropertiesFileNameOtherSRV = szPropertiesFileNameWindowsOtherSRV;
				}
			}
			
			
			//javax.naming.NoInitialContextException
			
		} catch(NoClassDefFoundError e) {
			if (isUnix()) {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameUnixOtherSRV;
			} else {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameWindowsOtherSRV;
			}
		}
		catch(RuntimeException e2) {
			if (isUnix()) {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameUnixOtherSRV;
			} else {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameWindowsOtherSRV;
			}
		}
		catch(ExceptionInInitializerError e3) {
			if (isUnix()) {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameUnixOtherSRV;
			} else {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameWindowsOtherSRV;
			}
		} catch (ClassNotFoundException e1) {
			if (isUnix()) {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameUnixOtherSRV;
			} else {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameWindowsOtherSRV;
			}
		}
		

	}

	/**
	 * Gets the sz properties file name.
	 *
	 * @return the sz properties file name
	 */
	public String getSzPropertiesFileNameOtherSRV() {
		if (szPropertiesFileNameOtherSRV == null || szPropertiesFileNameOtherSRV.length() < 2) {
			if (isUnix()) {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameUnixOtherSRV;
			} else {
				szPropertiesFileNameOtherSRV = szPropertiesFileNameWindowsOtherSRV;
			}
		}
		
//		if (isUnix()) {
//			szPropertiesFileNameOtherSRV = szPropertiesFileNameUnixOtherSRV;
//		} else {
//			szPropertiesFileNameOtherSRV = szPropertiesFileNameWindowsOtherSRV;
//		}
		
		
		return szPropertiesFileName;
	}

	/**
	 * Sets the sz properties file name.
	 *
	 * @param szPropertiesFileName the new sz properties file name
	 */
	public void setSzPropertiesFileNameOtherSRV(String szPropertiesFileNameOtherSRV) {
		this.szPropertiesFileNameOtherSRV = szPropertiesFileNameOtherSRV;
	}
	
	/**
	 * Gets the sz properties file name.
	 *
	 * @return the sz properties file name
	 */
	public String getSzPropertiesFileName() {
		if (szPropertiesFileName == null || szPropertiesFileName.length() < 2) {
			if (isUnix()) {
				szPropertiesFileName = szPropertiesFileNameUnix;
			} else {
				szPropertiesFileName = szPropertiesFileNameWindows;
			}
		}
		
		if (isUnix()) {
			szPropertiesFileName = szPropertiesFileNameUnix;
		} else {
			szPropertiesFileName = szPropertiesFileNameWindows;
		}
		
		
		return szPropertiesFileName;
	}

	/**
	 * Sets the sz properties file name.
	 *
	 * @param szPropertiesFileName the new sz properties file name
	 */
	public void setSzPropertiesFileName(String szPropertiesFileName) {
		this.szPropertiesFileName = szPropertiesFileName;
	}
}
