/*
 * 
 */
package eu.esponder.osgi.service.event;



import javax.ejb.Stateless;

import eu.esponder.datafusion.hserver.COPServer;
import eu.esponder.osgi.threadsafe.server.main.CacheServer;
import eu.esponder.osgi.threadsafe.server.main.EsponderEventServerThreadSafe;


/**
 * The Class EventServiceManagerBean.
 */
@Stateless
public class EventServiceManagerBean implements EventServiceManager,EventServiceManagerRemote {
	static{ 	
		COPServer threadcontrollercop = new COPServer(new Long(5));
		threadcontrollercop.start();
	EsponderEventServerThreadSafe threadcontroller = new EsponderEventServerThreadSafe();
	threadcontroller.start();
	CacheServer pobj=new CacheServer();
	pobj.StartServer(8899, "localhost", 9988);
	Thread pthred=new Thread(pobj);
	pthred.start();
	}
		
	
	public void StartServer()
	
	{
		//threadcontroller = new EsponderEventServerThreadSafe();
		
	}
	
	public void StopServer()
	{
		//threadcontroller.StopServer();
	}
	
}
