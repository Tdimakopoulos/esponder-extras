/*
 * 
 */
package eu.esponder.osgi.service.event;

import javax.ejb.Local;
import javax.ejb.Remote;



/**
 * The Interface EventServiceManager.
 */
@Remote
public interface EventServiceManagerRemote {

	public void StartServer();
	public void StopServer();
	
}
