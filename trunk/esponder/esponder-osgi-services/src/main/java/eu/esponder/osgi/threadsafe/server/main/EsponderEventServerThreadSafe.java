package eu.esponder.osgi.threadsafe.server.main;

import java.io.IOException;
import java.net.SocketException;
import java.util.Date;

import com.prosyst.mprm.common.ManagementException;

import eu.esponder.event.datafusion.crud.ESponderDFCreateActionCRUDEvent;
import eu.esponder.event.datafusion.crud.ESponderDFCreateActionPartCRUDEvent;
import eu.esponder.event.datafusion.crud.ESponderDFUpdateActionCRUDEvent;
import eu.esponder.event.datafusion.crud.ESponderDFUpdateActionPartCRUDEvent;
import eu.esponder.event.datafusion.message.ESponderDFConfirmActionEvent;
import eu.esponder.event.datafusion.message.ESponderDFConfirmActionPartEvent;
import eu.esponder.event.datafusion.message.ESponderDFCreateActionEvent;
import eu.esponder.event.datafusion.message.ESponderDFCreateActionPartEvent;
import eu.esponder.event.datafusion.query.ESponderDFQueryRequestEvent;
import eu.esponder.event.datafusion.query.ESponderDFQueryResponseEvent;
import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.crisis.action.CreateActionPartEvent;
import eu.esponder.event.model.login.LoginRequestEvent;
import eu.esponder.event.model.login.LogoutEsponderUserEvent;
import eu.esponder.event.model.snapshot.CreateCrisisContextSnapshotEvent;
import eu.esponder.event.model.snapshot.action.CreateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionPartSnapshotEvent;
import eu.esponder.event.model.snapshot.action.UpdateActionSnapshotEvent;
import eu.esponder.event.model.snapshot.measurement.CreateSensorMeasurementEnvelopeEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.osgi.service.event.ESponderEventListener;
import eu.esponder.osgi.settings.OsgiSettings;

public class EsponderEventServerThreadSafe  extends Thread{


	ESponderEventListener<CreateActionSnapshotEvent> listener = null;
	ESponderEventListener<CreateCrisisContextEvent> listener2 = null;
	ESponderEventListener<CreateActionEvent> listener3 = null;
	ESponderEventListener<CreateActionPartEvent> listener4 = null;
	ESponderEventListener<CreateSensorMeasurementEnvelopeEvent> listener5 = null;
	ESponderEventListener<UpdateActionSnapshotEvent> listener6 = null;
	ESponderEventListener<UpdateActionPartSnapshotEvent> listener7 = null;
	ESponderEventListener<CreateSensorMeasurementStatisticEvent> listener8 = null;
	ESponderEventListener<CreateCrisisContextSnapshotEvent> listener9 = null;
	ESponderEventListener<ESponderDFQueryResponseEvent> listener10 = null;
	ESponderEventListener<ESponderDFQueryRequestEvent> listener11 = null;
	ESponderEventListener<ESponderDFCreateActionCRUDEvent> listener12 = null;
	ESponderEventListener<ESponderDFCreateActionPartCRUDEvent> listener13 = null;
	ESponderEventListener<ESponderDFUpdateActionCRUDEvent> listener14 = null;
	ESponderEventListener<ESponderDFUpdateActionPartCRUDEvent> listener15 = null;
	ESponderEventListener<ESponderDFConfirmActionEvent> listener16 = null;
	ESponderEventListener<ESponderDFConfirmActionPartEvent> listener17 = null;
	ESponderEventListener<ESponderDFCreateActionEvent> listener18 = null;
	ESponderEventListener<ESponderDFCreateActionPartEvent> listener19 = null;
	ESponderEventListener<LoginRequestEvent> listenerex1 = null;
	ESponderEventListener<LogoutEsponderUserEvent> listenerex2 = null;
	
	public boolean Running;
    public boolean StopRequest;
    String szgfilename;
    
    public void CreateListenersConnections() throws ManagementException {

    	System.out.println("----->  Event Server (Version 2.0) Listeners Starting");

		if (listenerex1 == null)
			listenerex1 = new ESponderEventListener<LoginRequestEvent>(
					LoginRequestEvent.class);
		if (listenerex2 == null)
			listenerex2 = new ESponderEventListener<LogoutEsponderUserEvent>(
					LogoutEsponderUserEvent.class);

		if (listener12 == null)
			listener12 = new ESponderEventListener<ESponderDFCreateActionCRUDEvent>(
					ESponderDFCreateActionCRUDEvent.class);

		if (listener13 == null)
			listener13 = new ESponderEventListener<ESponderDFCreateActionPartCRUDEvent>(
					ESponderDFCreateActionPartCRUDEvent.class);

		if (listener14 == null)
			listener14 = new ESponderEventListener<ESponderDFUpdateActionCRUDEvent>(
					ESponderDFUpdateActionCRUDEvent.class);

		if (listener15 == null)
			listener15 = new ESponderEventListener<ESponderDFUpdateActionPartCRUDEvent>(
					ESponderDFUpdateActionPartCRUDEvent.class);

		if (listener16 == null)
			listener16 = new ESponderEventListener<ESponderDFConfirmActionEvent>(
					ESponderDFConfirmActionEvent.class);

		if (listener17 == null)
			listener17 = new ESponderEventListener<ESponderDFConfirmActionPartEvent>(
					ESponderDFConfirmActionPartEvent.class);

		if (listener18 == null)
			listener18 = new ESponderEventListener<ESponderDFCreateActionEvent>(
					ESponderDFCreateActionEvent.class);

		if (listener19 == null)
			listener19 = new ESponderEventListener<ESponderDFCreateActionPartEvent>(
					ESponderDFCreateActionPartEvent.class);

		if (listener10 == null)
			listener10 = new ESponderEventListener<ESponderDFQueryResponseEvent>(
					ESponderDFQueryResponseEvent.class);

		if (listener11 == null)
			listener11 = new ESponderEventListener<ESponderDFQueryRequestEvent>(
					ESponderDFQueryRequestEvent.class);
	
		if (listener == null)
			listener = new ESponderEventListener<CreateActionSnapshotEvent>(
					CreateActionSnapshotEvent.class);

		if (listener2 == null)
			listener2 = new ESponderEventListener<CreateCrisisContextEvent>(
					CreateCrisisContextEvent.class);

		if (listener3 == null)
			listener3 = new ESponderEventListener<CreateActionEvent>(
					CreateActionEvent.class);

		if (listener4 == null)
			listener4 = new ESponderEventListener<CreateActionPartEvent>(
					CreateActionPartEvent.class);

		if (listener5 == null)
			listener5 = new ESponderEventListener<CreateSensorMeasurementEnvelopeEvent>(
					CreateSensorMeasurementEnvelopeEvent.class);

		if (listener6 == null)
			listener6 = new ESponderEventListener<UpdateActionSnapshotEvent>(
					UpdateActionSnapshotEvent.class);

		if (listener7 == null)
			listener7 = new ESponderEventListener<UpdateActionPartSnapshotEvent>(
					UpdateActionPartSnapshotEvent.class);

		if (listener8 == null)
			listener8 = new ESponderEventListener<CreateSensorMeasurementStatisticEvent>(
					CreateSensorMeasurementStatisticEvent.class);

		if (listener9 == null)
			listener9 = new ESponderEventListener<CreateCrisisContextSnapshotEvent>(
					CreateCrisisContextSnapshotEvent.class);
		
		System.out.println("----->  Event Server (Version 2.0) Listeners Started");
	}
    
    public void stopserver() {
    	System.out.println("----->  Event Server (Version 2.0) Server Stopping");
		
		if (listenerex1 == null)
			listenerex1.CloseConnection();
		if (listenerex2 == null)
			listenerex2.CloseConnection();
		if (listener != null)
			listener.CloseConnection();
		if (listener2 != null)
			listener2.CloseConnection();
		if (listener3 != null)
			listener3.CloseConnection();
		if (listener4 != null)
			listener4.CloseConnection();
		if (listener5 != null)
			listener5.CloseConnection();
		if (listener6 != null)
			listener6.CloseConnection();
		if (listener7 != null)
			listener7.CloseConnection();
		if (listener8 != null)
			listener8.CloseConnection();
		if (listener9 != null)
			listener9.CloseConnection();
		if (listener10 != null)
			listener10.CloseConnection();
		if (listener11 != null)
			listener11.CloseConnection();
		if (listener12 != null)
			listener12.CloseConnection();
		if (listener13 != null)
			listener13.CloseConnection();
		if (listener14 != null)
			listener14.CloseConnection();
		if (listener15 != null)
			listener15.CloseConnection();
		if (listener16 != null)
			listener16.CloseConnection();
		if (listener17 != null)
			listener17.CloseConnection();
		if (listener18 != null)
			listener18.CloseConnection();
		if (listener19 != null)
			listener19.CloseConnection();

		listener = null;
		listener2 = null;
		listener3 = null;
		listener4 = null;
		listener5 = null;
		listener6 = null;
		listener7 = null;
		listener8 = null;
		listener9 = null;
		listener10 = null;
		listener11 = null;
		listener12 = null;
		listener13 = null;
		listener14 = null;
		listener15 = null;
		listener16 = null;
		listener17 = null;
		listener18 = null;
		listener19 = null;
		listenerex1 = null;
		listenerex2 = null;

		System.out.println("----->  Event Server (Version 2.0) Server Stopped");
	}

	
	public void startserver() {
		System.out.println("----->  Event Server (Version 2.0) Server Starting");
		
		
		try {
			CreateListenersConnections();

			listener.subscribe();
			listener2.subscribe();
			listener3.subscribe();
			listener4.subscribe();
			listener5.subscribe();
			listener6.subscribe();
			listener7.subscribe();
			listener8.subscribe();
			listener9.subscribe();
			listener10.subscribe();
			listener11.subscribe();
			listener12.subscribe();
			listener13.subscribe();
			listener14.subscribe();
			listener15.subscribe();
			listener16.subscribe();
			listener17.subscribe();
			listener18.subscribe();
			listener19.subscribe();

			//listenerex.subscribe();
			listenerex1.subscribe();
			listenerex2.subscribe();
		} catch (ManagementException e) {

			new EsponderCheckedException(this.getClass(),
					"ESponder Exception : " + e.getMessage());
		}

		System.out.println("----->  Event Server (Version 2.0) Server Started");
	}
    
    public EsponderEventServerThreadSafe()
    {
        Running=true;
        StopRequest=false;
    }
    
    public boolean IsServerRunning()
    {
        return Running;
    }
    
    public void StopServer()
    {
        StopRequest=true;
    }
    
    public String GetServerStatus()
    {
        return (new Date())+" Running : "+Running+" Stop Request : "+StopRequest;
    }
    
	/**
	 * Close server.
	 * 
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String CloseServer() throws IOException {

		ServerStatus pStatus = new ServerStatus();
		String szFilename = null;

		if (szgfilename == null) {
			OsgiSettings pSettings = new OsgiSettings();
			pSettings.LoadSettings();
			szFilename = pSettings.getSzPropertiesFileName();
			szgfilename = szFilename;
		} else {
			szFilename = szgfilename;
		}
		String sz = pStatus.readFile(pStatus.AppandFileOnPath(pStatus
				.RemoveFilename(szFilename)));

		return sz;
	}

    public void run() {

    	System.out.println("--> Event Server Started (Server Version 2.0)");
    	startserver();
    	
//    	CacheServer pServer=new CacheServer();
//    	try {
//			pServer.StartServer(8899, "localhost", 9988);
//		} catch (SocketException e1) {
//			System.out.println("Error on Cache Server : "+e1.getMessage());
//		} catch (IOException e1) {
//			System.out.println("Error on Cache Server : "+e1.getMessage());
//		}
    	
        while (true)
        {
            try {

                Thread.sleep(100);

                if(CloseServer().equalsIgnoreCase("0"))
                {
                	StopServer();
                }
                if(StopRequest)
                {
                	if(Running)
                	{
                    Running=false;
                    stopserver();
                    System.out.println("--> Event Server Stopped (Server Version 2.0)");
                	}else
                	{
                		if(CloseServer().equalsIgnoreCase("1"))
                		{
                			
                	    	startserver();
                	    	System.out.println("--> Event Server Started (Server Version 2.0)");
                	    	Running=true;
                	    	StopRequest=false;
                		}
                	}
                }
                
            } catch (InterruptedException ex) {
                System.out.println("Main Server Loop Catch in sleep (V 2.0): "+ex.getMessage());
            } catch (IOException e) {
            	System.out.println("Main Server problem on status file (V 2.0): "+e.getMessage());
			}
        }
        
        
    }

	
}
