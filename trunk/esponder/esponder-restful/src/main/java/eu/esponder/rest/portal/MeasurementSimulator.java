package eu.esponder.rest.portal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.controller.persistence.CrudRemoteService;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.snapshot.measurement.statistic.CreateSensorMeasurementStatisticEvent;
import eu.esponder.model.crisis.resource.ActorFR;
import eu.esponder.model.crisis.resource.ActorFRC;
import eu.esponder.model.crisis.resource.FirstResponderActor;
import eu.esponder.osgi.service.event.ESponderEventPublisher;
import eu.esponder.test.ResourceLocator;


@XmlTransient
public class MeasurementSimulator {

	PointDTO startingPointFRU1;
	PointDTO destinationPointFRU1;

	PointDTO startingPointFRU2;
	PointDTO destinationPointFRU2;

	SensorRemoteService sensorService = ResourceLocator.lookup("esponder/SensorBean/remote");
	ActorRemoteService actorService = ResourceLocator.lookup("esponder/ActorBean/remote");
	CrisisRemoteService crisisService = ResourceLocator.lookup("esponder/CrisisBean/remote");
	ActionRemoteService actionService = ResourceLocator.lookup("esponder/ActionBean/remote");
	GenericRemoteService genericService = ResourceLocator.lookup("esponder/GenericBean/remote");
	CrudRemoteService<FirstResponderActor> crudService = ResourceLocator.lookup("esponder/CrudBean/remote");

	long threadPeriodFRU = (long) 30000;

	List<Thread> FRUThreads = new ArrayList<Thread>();

	//	Temperature Configuration
	int minTemp = 35;		//	minimum produced temperature
	int rangeTemp = 3;		//	temperature deviation

	//	Location Configuration
	int minStep = 60;		//	minimum distance travelled
	int maxStep = 70;		//	maximum distance travelled

	List<String> actorsTitles = new ArrayList<String>(Arrays.asList("FRC #1", "FR #1.1", "FR #1.2","FRC #2",  "FR #2.1", "FR #2.2"));

	List<ESponderEventPublisher<CreateSensorMeasurementStatisticEvent>> publishersList = new ArrayList<ESponderEventPublisher<CreateSensorMeasurementStatisticEvent>>();

	public MeasurementSimulator() {
		// FRU Routes, Coordinates for source and destination points  
		// Syggrou-Fix Station -----> Faliriko Delta, along Syggrou Avenue
		startingPointFRU1 = new PointDTO(new BigDecimal(37.959655), new BigDecimal(23.720305), new BigDecimal(3));
		destinationPointFRU1 = new PointDTO(new BigDecimal(37.940703), new BigDecimal(23.696158), new BigDecimal(3));

		// Nea Smirni Square -----> Axilleos St, along Eleftheriou Venizelou Avenue
		startingPointFRU2 = new PointDTO(new BigDecimal(37.946997), new BigDecimal(23.715169), new BigDecimal(2));
		destinationPointFRU2 = new PointDTO(new BigDecimal(37.929668), new BigDecimal(23.709848), new BigDecimal(2));
	}
	
	public MeasurementSimulator(Double startPointLat1, Double startPointLong1,
			Double destPointLat1, Double destPointLong1,
			Double startingPointLat2, Double startPointLong2,
			Double destPointLat2, Double destPointLong2) {
		// FRU Routes, Coordinates for source and destination points  
		// Syggrou-Fix Station -----> Faliriko Delta, along Syggrou Avenue
		startingPointFRU1 = new PointDTO(new BigDecimal(startPointLat1), new BigDecimal(startPointLong1), new BigDecimal(3));
		destinationPointFRU1 = new PointDTO(new BigDecimal(destPointLat1), new BigDecimal(destPointLong1), new BigDecimal(3));

		// Nea Smirni Square -----> Axilleos St, along Eleftheriou Venizelou Avenue
		startingPointFRU2 = new PointDTO(new BigDecimal(startingPointLat2), new BigDecimal(startPointLong2), new BigDecimal(2));
		destinationPointFRU2 = new PointDTO(new BigDecimal(destPointLat2), new BigDecimal(destPointLong2), new BigDecimal(2));
	}


	public void runSimulation() throws InterruptedException {

		int counter = 0;

		ActorFRCDTO frc = new ActorFRCDTO();
		ActorFRDTO fr = new ActorFRDTO();


		for(String actorTitle : actorsTitles) {

			ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher = null;
			try {
				publisher = new ESponderEventPublisher<CreateSensorMeasurementStatisticEvent>(CreateSensorMeasurementStatisticEvent.class);
			} catch (Exception e) {
				System.out.println("\n\nUnable to get a connection to EventAdmin, simulation will be terminated\n\n");
				continue;
			}
			publishersList.add(publisher);

			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("title", actorTitle);
			Object actor = crudService.findSingleWithNamedQuery("FirstResponderActor.findByTitle", params);


			Object actorDTO = null;


			if(actor!= null) {

				if(ActorFRC.class.isInstance(actor)) {
					ActorFRC actor2 = (ActorFRC) actor;//crudService.findSingleWithNamedQuery("FirstResponderActor.findByTitle", params);
					//actor2.getPersonnel().getOrganisation().setChildren(null);
					//actor2.getPersonnel().getOrganisation().setOrganisationCategories(null);
					actorDTO = actorService.findFRCByTitleRemote(actorTitle);//getMappingService().mapObject(actor2, ActorFRCDTO.class);
				}
				else if(ActorFR.class.isInstance(actor)) {
					ActorFR actor3 = (ActorFR) actor;//crudService.findSingleWithNamedQuery("FirstResponderActor.findByTitle", params);
					//actor3.getPersonnel().getOrganisation().setChildren(null);
					//actor3.getPersonnel().getOrganisation().setOrganisationCategories(null);
					actorDTO = actorService.findFRByTitleRemote(actorTitle);//getMappingService().mapObject(actor3, ActorFRDTO.class);
				}
			}



			FRUThread fruThread;
			if(actorDTO != null) {
				try {
					if(counter<=2)
						fruThread = new FRUThread(publisher, actorDTO, threadPeriodFRU, rangeTemp, minTemp, startingPointFRU1, destinationPointFRU1, minStep, maxStep );
					else
						fruThread = new FRUThread(publisher, actorDTO, threadPeriodFRU, rangeTemp, minTemp, startingPointFRU2, destinationPointFRU2, minStep, maxStep );

					FRUThreads.add(fruThread);
				} catch (ClassNotFoundException e) {
					// 
					e.printStackTrace();
				}
			}
			else {
				publisher.CloseConnection();
				System.out.println("\n\nUnable to find selected actor with title "+actorTitle+", actor will not be added to simulation\n\n");
			}
			counter++;
		}

		// Start Threads
		for(Thread thread : FRUThreads)
			thread.start();

		boolean threadStatus = true;

		while (threadStatus == true) {
			for(Thread thread : FRUThreads)
				if(!thread.isAlive()) 
					threadStatus = false;
				else {
					threadStatus =true;
					break;
				}
		}

		System.out.println("\n\nSimulation has ended successfully");

		for(ESponderEventPublisher<CreateSensorMeasurementStatisticEvent> publisher: publishersList)
			publisher.CloseConnection();

	}

	@SuppressWarnings("unused")
	private void sendCreateActionEvent() throws ClassNotFoundException {

		String actorTitle = "FRC #1";


		//FIXME Update to v3 Model
		//		ActorDTO actor = actorService.findByTitleRemote(actorTitle);
		FirstResponderActorDTO actor = null;
		if(actor != null) {

			CrisisContextDTO crisisContext = crisisService.findCrisisContextDTOByTitle("Fire Brigade Drill");
			Long userID = new Long(1);

			ESponderEventPublisher<CreateActionEvent> publisher = new ESponderEventPublisher<CreateActionEvent>(CreateActionEvent.class);

			String date = (new Date()).toString();
			ActionDTO action = new ActionDTO();
			action.setActionOperation(ActionOperationEnumDTO.MOVE);
			action.setCrisisContext(crisisContext);
			action.setSeverityLevel(SeverityLevelDTO.MEDIUM);
			action.setTitle("Initial DF action"+date);
			action.setType("OpActionType");
			ActionDTO paction = actionService.createActionRemote(action, userID);



			ActionPartDTO actionPart = new ActionPartDTO();
			actionPart.setActionOperation(ActionOperationEnumDTO.MOVE);
			actionPart.setActor(actor.getId());
			actionPart.setSeverityLevel(SeverityLevelDTO.MEDIUM);
			actionPart.setTitle("Initial DF Action Part"+(new Date()).toString());
			actionPart.setActionId(paction.getId());
			ActionPartDTO pactionPart = actionService.createActionPartRemote(actionPart, userID);

			SphereDTO location = new SphereDTO(new PointDTO(new BigDecimal(37.940703), new BigDecimal(23.696158), null), new BigDecimal(2),"Initial DF location Area"+date );
			SphereDTO plocation = (SphereDTO) genericService.createEntityRemote(location, userID);

			ActionPartObjectiveDTO objective = new ActionPartObjectiveDTO();
			objective.setActionPartID(pactionPart.getId());
			objective.setLocationArea(plocation);
			objective.setTitle("Initial DF Action Part Objective"+(new Date()).toString());
			objective.setPeriod(new PeriodDTO(new Date().getTime(), new Date().getTime()));
			actionService.createActionPartObjectiveRemote(objective, userID);


			ActionDTO actionForEvent = actionService.findActionDTOByTitle("Initial DF action"+date);

			CreateActionEvent event = new CreateActionEvent();
			event.setEventAttachment(actionForEvent);
			event.setEventSeverity(SeverityLevelDTO.MEDIUM);
			event.setEventSource(actor);
			event.setEventTimestamp(new Date());
			event.setJournalMessage("Initial DF CreateActionEvent");

			try {
				publisher.publishEvent(event);
			} catch (EventListenerException e) {
				System.out.println("Unable to send event through publisher");
			}

			System.out.println("Event has been sent...");
			publisher.CloseConnection();

		}
		else
			System.out.println("\nActor not found, event will not be published...\n");

	}

	protected ESponderRemoteMappingService getMappingService() {
		return ResourceLocator.lookup("esponder/ESponderMappingBean/remote");
		//			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
	}

}
