/*
 * 
 */
package eu.esponder.rest.action;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.CrisisContextParameterDTO;
import eu.esponder.dto.model.crisis.SubCrisisDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.dto.model.snapshot.action.ActionSnapshotDTO;
import eu.esponder.event.model.crisis.CreateCrisisContextEvent;
import eu.esponder.event.model.crisis.action.CreateActionEvent;
import eu.esponder.event.model.crisis.action.CreateActionPartEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisViewActions.
 * ,Path("/crisis/view")
 */
@Path("/crisis/view")
public class CrisisViewActions extends ESponderResource {



	/**
	 * Read crisis context by id.
	 *
	 * @param crisisContextID the crisis context id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context dto,
	 * 
	 * 
	 * Path("/crisisContext/findByID")
	 * Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/crisisContext/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextDTO readCrisisContextById(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextId may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

				CrisisContextDTO crisisContextDTO = this.getCrisisRemoteService().findCrisisContextDTOById(crisisContextID);
		return crisisContextDTO;
	}
	
	
	@GET
	@Path("/subCrisis/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public SubCrisisDTO readSubCrisisById(
			@QueryParam("subCrisisID") @NotNull(message="subCrisisID may not be null") Long subCrisisID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		SubCrisisDTO subCrisisDTO = this.getCrisisRemoteService().findSubCrisisDTOById(subCrisisID);
		return subCrisisDTO;
	}

	/**
	 * Read crisis resource plan by id.
	 *
	 * @param crisisResourcePlanID the crisis resource plan id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis resource plan dto,
	 * 
	 * Path("/crisisResourcePlan/findByID")
	 * Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/crisisResourcePlan/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisResourcePlanDTO readCrisisResourcePlanById(
			@QueryParam("crisisResourcePlanID") @NotNull(message="crisisResourcePlanId may not be null") Long crisisResourcePlanID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		CrisisResourcePlanDTO crisisResourcePlanDTO = this.getCrisisRemoteService().findCrisisResourcePlanByIdRemote(crisisResourcePlanID, userID);
		return crisisResourcePlanDTO;
	}
	
	
	/**
	 * Find crisis context parameter by id.
	 *
	 * @param crisisContextParameterID the crisis context parameter id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context parameter dto
	 */
	@GET
	@Path("/crisisContextParameter/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextParameterDTO findCrisisContextParameterById(
			@QueryParam("crisisContextParameterID") @NotNull(message="crisisContextParameterID may not be null") Long crisisContextParameterID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		CrisisContextParameterDTO crisisContextParameterDTO = this.getCrisisRemoteService().findCrisisContextParameterDTOById(crisisContextParameterID);
		return crisisContextParameterDTO;
	}
	
	
	/**
	 * Find crisis context parameter by id.
	 *
	 * @param crisisContextParameterID the crisis context parameter id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context parameter dto
	 */
	@GET
	@Path("/crisisContextParameter/findAllByCrisis")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO findAllCrisisContextsParametersByCrisis(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextID may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		List<CrisisContextParameterDTO> crisisContextParameters = this.getCrisisRemoteService().findCrisisContextParametersByCrisisRemote(crisisContextID);
		if(!crisisContextParameters.isEmpty())
			return new ResultListDTO(crisisContextParameters);
		else
			return null;
	}
	

	/**
	 * Read all crisis contexts.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the result list dto
	 * 
	 * ,Path("/crisisContext/findAll")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/crisisContext/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllCrisisContexts(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return new ResultListDTO(this.getCrisisRemoteService().findAllCrisisContextsRemote());

	}

	/**
	 * Read all crisis resource plans.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the result list dto
	 * 
	 * ,Path("/crisisResourcePlan/findAll")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/crisisResourcePlan/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllCrisisResourcePlans(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ResultListDTO resultsList = new ResultListDTO(this.getCrisisRemoteService().findAllCrisisResourcePlansRemote());
		return resultsList;

	}

	/**
	 * Read crisis context snapshot by id.
	 *
	 * @param crisisContextSnapshotID the crisis context snapshot id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context snapshot dto
	 * 
	 * ,Path("/crisisContextSnapshot/findByID")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/crisisContextSnapshot/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextSnapshotDTO readCrisisContextSnapshotById(
			@QueryParam("crisisContextSnapshotID") @NotNull(message="crisisContextSnapshotId may not be null") Long crisisContextSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		CrisisContextSnapshotDTO crisisContextSnapshotDTO = this.getCrisisRemoteService().findCrisisContextSnapshotDTOById(crisisContextSnapshotID);
		return crisisContextSnapshotDTO;
	}

	/**
	 * Read action by id.
	 *
	 * @param actionID the action id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action dto
	 * 
	 * ,Path("/action/findByID")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/action/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionDTO readActionById(
			@QueryParam("actionID") @NotNull(message="actionId may not be null") Long actionID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ActionDTO actionDTO = this.getActionRemoteService().findActionDTOById(actionID);
		return actionDTO;
	}

	/**
	 * Read action snapshot by id.
	 *
	 * @param actionSnapshotID the action snapshot id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action snapshot dto
	 * 
	 * ,Path("/actionSnapshot/findByID")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/actionSnapshot/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionSnapshotDTO readActionSnapshotById(
			@QueryParam("actionSnapshotID") @NotNull(message="actionSnapshotId may not be null") Long actionSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ActionSnapshotDTO actionSnapshotDTO = this.getActionRemoteService().findActionSnapshotDTOById(actionSnapshotID);
		return actionSnapshotDTO;
	}

	/**
	 * Read action objective by id.
	 *
	 * @param actionObjectiveID the action objective id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action objective dto
	 * 
	 * ,Path("/actionObjective/findByID")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/actionObjective/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionObjectiveDTO readActionObjectiveById(
			@QueryParam("actionObjectiveID") @NotNull(message="actionObjectiveId may not be null") Long actionObjectiveID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ActionObjectiveDTO actionObjectiveDTO = this.getActionRemoteService().findActionObjectiveDTOById(actionObjectiveID);
		return actionObjectiveDTO;
	}

	/**
	 * Read action part by id.
	 *
	 * @param actionPartID the action part id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part dto
	 * 
	 * ,Path("/actionPart/findByID")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	
	//FIXME STACK OVERFLOW ERROR
	@GET
	@Path("/actionPart/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartDTO readActionPartById(
			@QueryParam("actionPartID") @NotNull(message="actionPartId may not be null") Long actionPartID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ActionPartDTO actionPartDTO = this.getActionRemoteService().findActionPartDTOById(actionPartID);
		return actionPartDTO;
	}

	/**
	 * Read action part snapshot by id.
	 *
	 * @param actionPartSnapshotID the action part snapshot id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part snapshot dto
	 * 
	 * ,Path("/actionPartSnapshot/findByID")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/actionPartSnapshot/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartSnapshotDTO readActionPartSnapshotById(
			@QueryParam("actionPartSnapshotID") @NotNull(message="actionPartSnapshotID may not be null") Long actionPartSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ActionPartSnapshotDTO actionPartSnapshotDTO = this.getActionRemoteService().findActionPartSnapshotDTOById(actionPartSnapshotID);
		return actionPartSnapshotDTO;
	}

	/**
	 * Read action part objective by id.
	 *
	 * @param actionPartObjectiveID the action part objective id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part objective dto
	 * 
	 * ,Path("/actionPartObjective/findByID")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/actionPartObjective/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartObjectiveDTO readActionPartObjectiveById(
			@QueryParam("actionPartObjectiveID") @NotNull(message="actionPartObjectiveId may not be null") Long actionPartObjectiveID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionPartObjectiveDTO actionPartObjectiveDTO = this.getActionRemoteService().findActionPartObjectiveDTOById(actionPartObjectiveID);
		return actionPartObjectiveDTO;
	}


	
	
	
	/**
	 * Read crisis context by title.
	 *
	 * @param crisisContextTitle the crisis context title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context dto
	 * 
	 * ,Path("/crisisContext/findByTitle")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/crisisContext/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextDTO readCrisisContextByTitle(
			@QueryParam("crisisContextTitle") @NotNull(message="crisisContextTitle may not be null") String crisisContextTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		CrisisContextDTO crisisContextDTO = this.getCrisisRemoteService().findCrisisContextDTOByTitle(crisisContextTitle);
		return crisisContextDTO;
	}
	
	
	@GET
	@Path("/subCrisis/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public SubCrisisDTO readSubCrisisByTitle(
			@QueryParam("subCrisisTitle") @NotNull(message="subCrisisTitle may not be null") String subCrisisTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		SubCrisisDTO subCrisisDTO = this.getCrisisRemoteService().findSubCrisisDTOByTitle(subCrisisTitle);
		return subCrisisDTO;
	}
	
	/**
	 * Read crisis context parameter by title.
	 *
	 * @param paramName the param name
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context parameter dto
	 */
	@GET
	@Path("/crisisContextParameter/findByName")
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextParameterDTO readCrisisContextParameterByTitle(
			@QueryParam("paramName") @NotNull(message="crisisContextTitle may not be null") String paramName,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		CrisisContextParameterDTO crisisContextParametersDTO = this.getCrisisRemoteService().findCrisisParameterByNameRemote(paramName, userID);
		return crisisContextParametersDTO;
	}

	/**
	 * Read action by title.
	 *
	 * @param actionTitle the action title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action dto
	 * 
	 * ,Path("/action/findByTitle")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/action/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionDTO readActionByTitle(
			@QueryParam("actionTitle") @NotNull(message="actionTitle may not be null") String actionTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionDTO actionDTO = this.getActionRemoteService().findActionDTOByTitle(actionTitle);
		return actionDTO;
	}

	/**
	 * Read action objective by title.
	 *
	 * @param actionObjectiveTitle the action objective title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action objective dto
	 * 
	 * ,Path("/actionObjective/findByTitle")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/actionObjective/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionObjectiveDTO readActionObjectiveByTitle(
			@QueryParam("actionObjectiveTitle") @NotNull(message="actionObjectiveTitle may not be null") String actionObjectiveTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionObjectiveDTO actionObjectiveDTO = this.getActionRemoteService().findActionObjectiveDTOByTitle(actionObjectiveTitle);
		return actionObjectiveDTO;
	}

	/**
	 * Read action part by title.
	 *
	 * @param actionPartTitle the action part title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part dto
	 * 
	 * ,Path("/actionPart/findByTitle")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/actionPart/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartDTO readActionPartByTitle(
			@QueryParam("actionPartTitle") @NotNull(message="actionPartTitle may not be null") String actionPartTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionPartDTO actionPartDTO = this.getActionRemoteService().findActionPartDTOByTitle(actionPartTitle);
		return actionPartDTO;
	}

	/**
	 * Read action part objective by title.
	 *
	 * @param actionPartObjectiveTitle the action part objective title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part objective dto
	 * 
	 * ,Path("/actionPartObjective/findByTitle")
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@GET
	@Path("/actionPartObjective/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartObjectiveDTO readActionPartObjectiveByTitle(
			@QueryParam("actionPartObjectiveTitle") @NotNull(message="actionPartObjectiveTitle may not be null") String actionPartObjectiveTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionPartObjectiveDTO actionPartObjectiveDTO = this.getActionRemoteService().findActionPartObjectiveDTOByTitle(actionPartObjectiveTitle);
		return actionPartObjectiveDTO;
	}

	/**
	 * Creates the crisis context.
	 *
	 * @param crisisContextDTO the crisis context dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context dto
	 * 
	 * ,Path("/crisisContext/create")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/crisisContext/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextDTO createCrisisContext(
			@NotNull(message="crisisContext object may not be null") CrisisContextDTO crisisContextDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		CrisisContextDTO crisisContextDTOPersisted = this.getCrisisRemoteService().createCrisisContextRemote(crisisContextDTO, userID);
		return crisisContextDTOPersisted;
	}
	
	
	@POST
	@Path("/subCrisis/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SubCrisisDTO createSubCrisis(
			@NotNull(message="subCrisis object may not be null") SubCrisisDTO subCrisisDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		SubCrisisDTO subCrisisDTOPersisted = this.getCrisisRemoteService().createSubCrisisRemote(subCrisisDTO, userID);
		return subCrisisDTOPersisted;
	}
	
	
	/**
	 * Creates the crisis context parameters.
	 *
	 * @param paramName the param name
	 * @param paramValue the param value
	 * @param crisisContextID the crisis context id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context parameter dto
	 */
	@POST
	@Path("/crisisContextParameter/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextParameterDTO createCrisisContextParameters(
			@NotNull(message="CrisisContextParameterDTO object may not be null") CrisisContextParameterDTO crisisContextParameterDTO,
			@QueryParam("paramName") @NotNull(message="pkiKey may not be null") String paramName,
			@QueryParam("paramValue") @NotNull(message="pkiKey may not be null") String paramValue,
			@QueryParam("crisisContextID") @NotNull(message="pkiKey may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		CrisisContextParameterDTO crisisContextParametersDTOPersisted = this.getCrisisRemoteService().createCrisisParameterRemote(crisisContextID, paramName, paramValue, userID);
		return crisisContextParametersDTOPersisted;
	}

	/**
	 * Creates the crisis context with event.
	 *
	 * @param crisisContextDTO the crisis context dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context dto
	 * 
	 * ,Path("/crisisContext/createwithevent")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/crisisContext/createwithevent")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextDTO createCrisisContextWithEvent(
			@NotNull(message="crisisContext object may not be null") CrisisContextDTO crisisContextDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		CrisisContextDTO crisisContextDTOPersisted = this.getCrisisRemoteService().createCrisisContextRemote(crisisContextDTO, userID);

		CreateCrisisContextEvent crisisContextEvent = new CreateCrisisContextEvent();
		crisisContextEvent.setEventAttachment(crisisContextDTOPersisted);
		crisisContextEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
		crisisContextEvent.setEventTimestamp(new Date());
		crisisContextEvent.setJournalMessage(crisisContextEvent.getJournalMessageInfo());
		ActorDTO subActorDTO = this.getActorRemoteService().findByIdRemote(userID);
		crisisContextEvent.setEventSource(subActorDTO);

		ESponderEventPublisher<CreateCrisisContextEvent> publisher = new ESponderEventPublisher<CreateCrisisContextEvent>(CreateCrisisContextEvent.class);
		try {
			publisher.publishEvent(crisisContextEvent);
		} catch (EventListenerException e) {
			e.printStackTrace();
		}
		publisher.CloseConnection();

		return crisisContextDTOPersisted;
	}
	
	@POST
	@Path("/crisisContext/publishcrisiscontextevent")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextDTO publishCrisisContextEvent(
			@NotNull(message="crisisContext object may not be null") CrisisContextDTO crisisContextDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		//CrisisContextDTO crisisContextDTOPersisted = this.getCrisisRemoteService().createCrisisContextRemote(crisisContextDTO, userID);

		CreateCrisisContextEvent crisisContextEvent = new CreateCrisisContextEvent();
		crisisContextEvent.setEventAttachment(crisisContextDTO);
		crisisContextEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
		crisisContextEvent.setEventTimestamp(new Date());
		crisisContextEvent.setJournalMessage(crisisContextEvent.getJournalMessageInfo());
		ActorDTO subActorDTO = this.getActorRemoteService().findByIdRemote(userID);
		crisisContextEvent.setEventSource(subActorDTO);

		ESponderEventPublisher<CreateCrisisContextEvent> publisher = new ESponderEventPublisher<CreateCrisisContextEvent>(CreateCrisisContextEvent.class);
		try {
			publisher.publishEvent(crisisContextEvent);
		} catch (EventListenerException e) {
			e.printStackTrace();
		}
		publisher.CloseConnection();

		return crisisContextDTO;
	}

	/**
	 * Creates the crisis resource plan.
	 *
	 * @param crisisResourcePlanDTO the crisis resource plan dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis resource plan dto
	 * 
	 * ,Path("/crisisResourcePlan/create")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/crisisResourcePlan/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisResourcePlanDTO createCrisisResourcePlan(
			@NotNull(message="crisisResourcePlan object may not be null") CrisisResourcePlanDTO crisisResourcePlanDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		CrisisResourcePlanDTO crisisResourcePlanDTOPersisted = this.getCrisisRemoteService().createCrisisResourcePlanRemote(crisisResourcePlanDTO, userID);
		return crisisResourcePlanDTOPersisted;
	}


	/**
	 * Creates the crisis context snapshot.
	 *
	 * @param crisisContextSnapshotDTO the crisis context snapshot dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context snapshot dto
	 * 
	 * ,Path("/crisisContextSnapshot/create")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/crisisContextSnapshot/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextSnapshotDTO createCrisisContextSnapshot(
			@NotNull(message="crisisContextSnapshot object may not be null") CrisisContextSnapshotDTO crisisContextSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		CrisisContextDTO crisisContextPersisted = this.getCrisisRemoteService().findCrisisContextDTOById(crisisContextSnapshotDTO.getCrisisContext().getId());
		if(crisisContextPersisted != null) {
			CrisisContextSnapshotDTO crisisContextSnapshotDTOPersisted = this.getCrisisRemoteService().createCrisisContextSnapshotRemote(crisisContextSnapshotDTO, userID);
			return crisisContextSnapshotDTOPersisted;
		}
		else
			return null;
	}

	/**
	 * Creates the action.
	 *
	 * @param actionDTO the action dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action dto
	 * 
	 * ,Path("/action/create")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/action/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionDTO createAction(
			@NotNull(message="Action object may not be null") ActionDTO actionDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionDTO actionDTOPersisted = this.getActionRemoteService().createActionRemote(actionDTO, userID);
		return actionDTOPersisted;
	}

	/**
	 * Creates the action with event.
	 *
	 * @param actionDTO the action dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action dto
	 * 
	 * ,Path("/action/createWithEvent")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/action/createWithEvent")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionDTO createActionWithEvent(
			@NotNull(message="Action object may not be null") ActionDTO actionDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionDTO actionDTOPersisted = this.getActionRemoteService().createActionRemote(actionDTO, userID);

		CreateActionEvent createActionEvent = new CreateActionEvent();
		createActionEvent.setEventAttachment(actionDTOPersisted);
		createActionEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
		createActionEvent.setEventTimestamp(new Date());
		createActionEvent.setJournalMessage(createActionEvent.getJournalMessageInfo());
		ActorDTO subActorDTO = this.getActorRemoteService().findByIdRemote(userID);
		createActionEvent.setEventSource(subActorDTO);

		ESponderEventPublisher<CreateActionEvent> publisher = new ESponderEventPublisher<CreateActionEvent>(CreateActionEvent.class);
		try {
			publisher.publishEvent(createActionEvent);
		} catch (EventListenerException e) {
			e.printStackTrace();
		}
		publisher.CloseConnection();

		return actionDTOPersisted;
	}

	/**
	 * Creates the action snapshot.
	 *
	 * @param actionSnapshotDTO the action snapshot dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action snapshot dto
	 * 
	 * ,Path("/actionSnapshot/create")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/actionSnapshot/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionSnapshotDTO createActionSnapshot(
			@NotNull(message="ActionSnapshot object may not be null") ActionSnapshotDTO actionSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionSnapshotDTO actionSnapshotDTOPersisted = this.getActionRemoteService().createActionSnapshotRemote(actionSnapshotDTO, userID);
		return actionSnapshotDTOPersisted;
	}

	/**
	 * Creates the action objective.
	 *
	 * @param actionObjectiveDTO the action objective dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action objective dto
	 * 
	 * ,Path("/actionObjective/create")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/actionObjective/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionObjectiveDTO createActionObjective(
			@NotNull(message="ActionObjective object may not be null") ActionObjectiveDTO actionObjectiveDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionObjectiveDTO actionObjectiveDTOPersisted = this.getActionRemoteService().createActionObjectiveRemote(actionObjectiveDTO, userID);
		return actionObjectiveDTOPersisted;
	}

	/**
	 * Creates the action part.
	 *
	 * @param actionPartDTO the action part dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part dto
	 * 
	 * ,Path("/actionPart/create")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/actionPart/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartDTO createActionPart(
			@NotNull(message="ActionPart object may not be null") ActionPartDTO actionPartDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionPartDTO actionPartDTOPersisted = this.getActionRemoteService().createActionPartRemote(actionPartDTO, userID);
		return actionPartDTOPersisted;
	}

	/**
	 * Creates the action part with event.
	 *
	 * @param actionPartDTO the action part dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part dto
	 * 
	 * ,Path("/actionPart/createwithevent")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/actionPart/createwithevent")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartDTO createActionPartWithEvent(
			@NotNull(message="ActionPart object may not be null") ActionPartDTO actionPartDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionPartDTO actionPartDTOPersisted = this.getActionRemoteService().createActionPartRemote(actionPartDTO, userID);

		CreateActionPartEvent createActionPartEvent = new CreateActionPartEvent();
		createActionPartEvent.setEventAttachment(actionPartDTOPersisted);
		createActionPartEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
		createActionPartEvent.setEventTimestamp(new Date());
		createActionPartEvent.setJournalMessage(createActionPartEvent.getJournalMessageInfo());
		ActorDTO subActorDTO = this.getActorRemoteService().findByIdRemote(userID);
		createActionPartEvent.setEventSource(subActorDTO);

		ESponderEventPublisher<CreateActionPartEvent> publisher = new ESponderEventPublisher<CreateActionPartEvent>(CreateActionPartEvent.class);
		try {
			publisher.publishEvent(createActionPartEvent);
		} catch (EventListenerException e) {
			e.printStackTrace();
		}
		publisher.CloseConnection();

		return actionPartDTOPersisted;
	}

	/**
	 * Creates the action part snapshot.
	 *
	 * @param actionPartSnapshotDTO the action part snapshot dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part snapshot dto
	 * 
	 * ,Path("/actionPartSnapshot/create")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/actionPartSnapshot/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartSnapshotDTO createActionPartSnapshot(
			@NotNull(message="ActionPartSnapshot object may not be null") ActionPartSnapshotDTO actionPartSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionPartSnapshotDTO actionPartSnapshotDTOPersisted = this.getActionRemoteService().createActionPartSnapshotRemote(actionPartSnapshotDTO, userID);
		return actionPartSnapshotDTOPersisted;
	}

	/**
	 * Creates the action part objective.
	 *
	 * @param actionPartObjectiveDTO the action part objective dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part objective dto
	 * 
	 * ,Path("/actionPartObjective/create")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@POST
	@Path("/actionPartObjective/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartObjectiveDTO createActionPartObjective(
			@NotNull(message="ActionPartObjective object may not be null") ActionPartObjectiveDTO actionPartObjectiveDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionPartObjectiveDTO actionPartObjectiveDTOPersisted = this.getActionRemoteService().createActionPartObjectiveRemote(actionPartObjectiveDTO, userID);
		return actionPartObjectiveDTOPersisted;
	}

	/**
	 * Update crisis context.
	 *
	 * @param crisisContextDTO the crisis context dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context dto
	 * 
	 * ,Path("/crisisContext/update")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@PUT
	@Path("/crisisContext/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextDTO updateCrisisContext(
			@NotNull(message="CrisisContext object may not be null") CrisisContextDTO crisisContextDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		CrisisContextDTO crisisContextDTOUpdated = this.getCrisisRemoteService().updateCrisisContextRemote(crisisContextDTO, userID);
		return crisisContextDTOUpdated;
	}
	
	
	@PUT
	@Path("/subCrisis/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public SubCrisisDTO updateSubCrisis(
			@NotNull(message="subCrisis object may not be null") SubCrisisDTO subCrisis,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		SubCrisisDTO subCrisisDTOUpdated = this.getCrisisRemoteService().updateSubCrisisRemote(subCrisis, userID);
		return subCrisisDTOUpdated;
	}
	
	/**
	 * Update crisis context parameter.
	 *
	 * @param paramID the param id
	 * @param paramValue the param value
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context parameter dto
	 */
	@PUT
	@Path("/crisisContextParameter/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextParameterDTO updateCrisisContextParameter(
			@NotNull(message="CrisisContextParameterDTO object may not be null") CrisisContextParameterDTO crisisContextParameterDTO,
			@QueryParam("paramID") @NotNull(message="pkiKey may not be null") Long paramID,
			@QueryParam("paramValue") @NotNull(message="pkiKey may not be null") String paramValue,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		CrisisContextParameterDTO crisisContextParametersDTOUpdated = this.getCrisisRemoteService().updateCrisisContextParametersRemote(paramID, paramValue, userID);
		return crisisContextParametersDTOUpdated;
	}

	/**
	 * Update crisis resource plan.
	 *
	 * @param crisisResourcePlanDTO the crisis resource plan dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis resource plan dto
	 * 
	 * ,Path("/crisisResourcePlan/update")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@PUT
	@Path("/crisisResourcePlan/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisResourcePlanDTO updateCrisisResourcePlan(
			@NotNull(message="CrisisResourcePlan object may not be null") CrisisResourcePlanDTO crisisResourcePlanDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		CrisisResourcePlanDTO crisisResourcePlanDTOUpdated = this.getCrisisRemoteService().updateCrisisResourcePlanRemote(crisisResourcePlanDTO, userID);
		return crisisResourcePlanDTOUpdated;
	}

	/**
	 * Update crisis context snapshot.
	 *
	 * @param crisisContextSnapshotDTO the crisis context snapshot dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context snapshot dto
	 * 
	 * ,Path("/crisisContextSnapshot/update")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@PUT
	@Path("/crisisContextSnapshot/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextSnapshotDTO updateCrisisContextSnapshot(
			@NotNull(message="CrisisContextSnapshot object may not be null") CrisisContextSnapshotDTO crisisContextSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		CrisisContextSnapshotDTO crisisContextSnapshotDTOUpdated = this.getCrisisRemoteService().updateCrisisContextSnapshotRemote(crisisContextSnapshotDTO, userID);
		return crisisContextSnapshotDTOUpdated;
	}

	/**
	 * Update action.
	 *
	 * @param actionDTO the action dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action dto
	 * 
	 * ,Path("/action/update")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@PUT
	@Path("/action/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionDTO updateAction(
			@NotNull(message="Action object may not be null") ActionDTO actionDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionDTO actionDTOUpdated = this.getActionRemoteService().updateActionRemote(actionDTO, userID);
		return actionDTOUpdated;
	}

	/**
	 * Update action snapshot.
	 *
	 * @param actionSnapshotDTO the action snapshot dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action snapshot dto
	 * 
	 * ,Path("/actionSnapshot/update")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@PUT
	@Path("/actionSnapshot/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionSnapshotDTO updateActionSnapshot(
			@NotNull(message="ActionSnapshot object may not be null") ActionSnapshotDTO actionSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionSnapshotDTO actionSnapshotDTOUpdated = this.getActionRemoteService().updateActionSnapshotRemote(actionSnapshotDTO, userID);
		return actionSnapshotDTOUpdated;
	}

	/**
	 * Update action objective.
	 *
	 * @param actionObjectiveDTO the action objective dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action objective dto
	 * 
	 * ,Path("/actionObjective/update")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@PUT
	@Path("/actionObjective/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionObjectiveDTO updateActionObjective(
			@NotNull(message="ActionObjective object may not be null") ActionObjectiveDTO actionObjectiveDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionObjectiveDTO actionObjectiveDTOUpdated = this.getActionRemoteService().updateActionObjectiveRemote(actionObjectiveDTO, userID);
		return actionObjectiveDTOUpdated;
	}

	/**
	 * Update action part.
	 *
	 * @param actionPartDTO the action part dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part dto
	 * 
	 * ,Path("/actionPart/update")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@PUT
	@Path("/actionPart/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartDTO updateActionPart(
			@NotNull(message="ActionPart object may not be null") ActionPartDTO actionPartDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionPartDTO actionPartDTOUpdated = this.getActionRemoteService().updateActionPartRemote(actionPartDTO, userID);
		return actionPartDTOUpdated;
	}

	/**
	 * Update action part snapshot.
	 *
	 * @param actionPartSnapshotDTO the action part snapshot dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part snapshot dto
	 * 
	 * ,Path("/actionPartSnapshot/update")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@PUT
	@Path("/actionPartSnapshot/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartSnapshotDTO updateActionPartSnapshot(
			@NotNull(message="ActionPartSnapshot object may not be null") ActionPartSnapshotDTO actionPartSnapshotDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionPartSnapshotDTO actionPartSnapshotDTOUpdated = this.getActionRemoteService().updateActionPartSnapshotRemote(actionPartSnapshotDTO, userID);
		return actionPartSnapshotDTOUpdated;
	}

	/**
	 * Update action part objective.
	 *
	 * @param actionPartObjectiveDTO the action part objective dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the action part objective dto
	 * 
	 * ,Path("/actionPartObjective/update")
	 * ,Consumes({MediaType.APPLICATION_JSON})
	 * ,Produces({MediaType.APPLICATION_JSON})
	 */
	@PUT
	@Path("/actionPartObjective/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ActionPartObjectiveDTO updateActionPartObjective(
			@NotNull(message="ActionPartObjective object may not be null") ActionPartObjectiveDTO actionPartObjectiveDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ActionPartObjectiveDTO actionPartObjectiveDTOUpdated = this.getActionRemoteService().updateActionPartObjectiveRemote(actionPartObjectiveDTO, userID);
		return actionPartObjectiveDTOUpdated;
	}

	/**
	 * Delete crisis context.
	 *
	 * @param crisisContextID the crisis context id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 * 
	 * ,Path("/crisisContext/delete")
	 */
	@DELETE
	@Path("/crisisContext/delete")
	public Long deleteCrisisContext(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextId may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getCrisisRemoteService().deleteCrisisContextRemote(crisisContextID, userID);
		return crisisContextID;
	}
	
	
	@DELETE
	@Path("/subCrisis/delete")
	public Long deleteSubCrisis(
			@QueryParam("subCrisisID") @NotNull(message="subCrisisID may not be null") Long subCrisisID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getCrisisRemoteService().deleteSubCrisisRemote(subCrisisID, userID);
		return subCrisisID;
	}
	
	
	/**
	 * Delete crisis context parameter.
	 *
	 * @param crisisContextParameterID the crisis context parameter id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/crisisContextParameter/delete")
	public Long deleteCrisisContextParameter(
			@QueryParam("crisisContextParameterID") @NotNull(message="crisisContextParameterID may not be null") Long crisisContextParameterID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		crisisContextParameterID = this.getCrisisRemoteService().deleteCrisisContextParameterRemote(crisisContextParameterID, userID);
		return crisisContextParameterID;
	}
	

	/**
	 * Delete crisis resource plan.
	 *
	 * @param crisisResourcePlanID the crisis resource plan id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 * 
	 * ,Path("/crisisResourcePlan/delete")
	 */
	@DELETE
	@Path("/crisisResourcePlan/delete")
	public Long deleteCrisisResourcePlan(
			@QueryParam("crisisResourcePlanID") @NotNull(message="crisisResourcePlanId may not be null") Long crisisResourcePlanID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getCrisisRemoteService().deleteCrisisResourcePlanRemote(crisisResourcePlanID, userID);
		return crisisResourcePlanID;
	}

	/**
	 * Delete crisis context snapshot.
	 *
	 * @param crisisContextSnapshotID the crisis context snapshot id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 * 
	 * ,Path("/crisisContextSnapshot/delete")
	 */
	@DELETE
	@Path("/crisisContextSnapshot/delete")
	public Long deleteCrisisContextSnapshot(
			@QueryParam("crisisContextSnapshotID") @NotNull(message="crisisContextSnapshotId may not be null") Long crisisContextSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getCrisisRemoteService().deleteCrisisContextSnapshotRemote(crisisContextSnapshotID, userID);
		return crisisContextSnapshotID;
	}

	/**
	 * Delete action.
	 *
	 * @param actionID the action id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 * 
	 * ,Path("/action/delete")
	 */
	@DELETE
	@Path("/action/delete")
	public Long deleteAction(
			@QueryParam("actionID") @NotNull(message="actionId may not be null") Long actionID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getActionRemoteService().deleteActionRemote(actionID, userID);
		return actionID;
	}

	/**
	 * Delete action snapshot.
	 *
	 * @param actionSnapshotID the action snapshot id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 * 
	 * ,Path("/actionSnapshot/delete")
	 */
	@DELETE
	@Path("/actionSnapshot/delete")
	public Long deleteActionSnapshot(
			@QueryParam("actionSnapshotID") @NotNull(message="actionSnapshotId may not be null") Long actionSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getActionRemoteService().deleteActionRemote(actionSnapshotID, userID);
		return actionSnapshotID;
	}

	/**
	 * Delete action objective.
	 *
	 * @param actionObjectiveID the action objective id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 * 
	 * ,Path("/actionObjective/delete")
	 */
	@DELETE
	@Path("/actionObjective/delete")
	public Long deleteActionObjective(
			@QueryParam("actionObjectiveID") @NotNull(message="actionObjectiveId may not be null") Long actionObjectiveID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getActionRemoteService().deleteActionObjectiveRemote(actionObjectiveID, userID);
		return actionObjectiveID;
	}


	/**
	 * Delete action part.
	 *
	 * @param actionPartID the action part id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 * 
	 * ,Path("/actionPart/delete")
	 */
	@DELETE
	@Path("/actionPart/delete")
	public Long deleteActionPart(
			@QueryParam("actionPartID") @NotNull(message="actionPartId may not be null") Long actionPartID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getActionRemoteService().deleteActionPartRemote(actionPartID, userID);
		return actionPartID;
	}

	/**
	 * Delete action part snapshot.
	 *
	 * @param actionPartSnapshotID the action part snapshot id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 * 
	 * ,Path("/actionPartSnapshot/delete")
	 */
	@DELETE
	@Path("/actionPartSnapshot/delete")
	public Long deleteActionPartSnapshot(
			@QueryParam("actionPartSnapshotID") @NotNull(message="actionPartSnapshotId may not be null") Long actionPartSnapshotID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getActionRemoteService().deleteActionPartRemote(actionPartSnapshotID, userID);
		return actionPartSnapshotID;
	}

	/**
	 * Delete action part objective.
	 *
	 * @param actionPartObjectiveID the action part objective id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 * 
	 * ,Path("/actionPartObjective/delete")
	 */
	@DELETE
	@Path("/actionPartObjective/delete")
	public Long deleteActionPartObjective(
			@QueryParam("actionPartObjectiveID") @NotNull(message="actionPartObjectiveId may not be null") Long actionPartObjectiveID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getActionRemoteService().deleteActionPartObjectiveRemote(actionPartObjectiveID, userID);
		return actionPartObjectiveID;
	}
}