/*
 * 
 */
package eu.esponder.rest.PointsAndCircles;

import org.codehaus.enunciate.XmlTransient;

// TODO: Auto-generated Javadoc
/**
 * The Class MovablePoint.
 */
@XmlTransient
public class MovablePoint  {
    
	/** The correction. */
	public int CORRECTION = 0;
	
	/** The Constant SIZE. */
	public static final int SIZE = 8;
    
    /** The Constant SIZE2. */
    public static final int SIZE2 = SIZE / 2;
    
    
    /** The i alt. */
    private double iLog = 0, iLat = 0,iAlt=0;
    
    
    /**
     * Instantiates a new movable point.
     *
     * @param x the x
     * @param y the y
     * @param a the a
     */
    public MovablePoint(double x, double y,double a)
    {
                this.iLog=x;
                this.iLat=y;
                this.iAlt=a;
    }
    
    /**
     * Gets the x.
     *
     * @return the x
     */
    public double getX() { return iLog; }
    
    /**
     * Gets the y.
     *
     * @return the y
     */
    public double getY() { return iLat; }
    
    /**
     * Gets the a.
     *
     * @return the a
     */
    public double getA() { return iAlt; }
    
    /**
     * Gets the mid x.
     *
     * @return the mid x
     */
    public double getMidX() 
    {
    	if (CORRECTION==0)
    		return iLog;
    	else	
    		return iLog + SIZE2; 
    }
    
    /**
     * Gets the mid y.
     *
     * @return the mid y
     */
    public double getMidY() 
    { 
    	if (CORRECTION==0)
    		return iLat;
    	else	
    		return iLat + SIZE2; 
    }
    
    /**
     * Gets the mid a.
     *
     * @return the mid a
     */
    public double getMidA() 
    { 
    	if (CORRECTION==0)
    		return iAlt;
    	else	
    		return iAlt + SIZE2; 
    }
    
}
