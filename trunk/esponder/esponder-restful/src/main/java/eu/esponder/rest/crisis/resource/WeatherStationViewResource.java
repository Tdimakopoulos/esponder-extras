/*
 * 
 */
package eu.esponder.rest.crisis.resource;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.WeatherStationDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewResource.
 */
@Path("/crisis/resource")
public class WeatherStationViewResource extends ESponderResource {
	
	
	@GET
	@Path("/weatherstation/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public WeatherStationDTO getWeatherStationDTOById( @QueryParam("weatherStationID") @NotNull(message="weatherStationID may not be null" ) Long weatherStationID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getWeatherStatioRemoteService().findWeatherStationDTOById(weatherStationID);
	}
	
	
	@GET
	@Path("/weatherstation/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public WeatherStationDTO getWeatherStationDTOByTitle( @QueryParam("weatherStationTitle") @NotNull(message="weatherStationTitle may not be null" ) String weatherStationTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getWeatherStatioRemoteService().findWeatherStationByTitleRemote(weatherStationTitle, userID);
	}
	
	
	
	@POST
	@Path("/weatherstation/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public WeatherStationDTO createWeatherStationDTO(@NotNull(message="weatherStationDTO may not be null" ) WeatherStationDTO weatherStationDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getWeatherStatioRemoteService().createWeatherStationRemote(weatherStationDTO, userID);
	}
	
	
	@PUT
	@Path("/weatherstation/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public WeatherStationDTO updateWeatherStationDTO(@NotNull(message="weatherStationDTO may not be null" ) WeatherStationDTO weatherStationDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getWeatherStatioRemoteService().updateWeatherStationRemote(weatherStationDTO, userID);
	}
	
	
	
	@DELETE
	@Path("/weatherstation/delete")
	public Long deleteWeatherStation(@QueryParam("weatherStationID") @NotNull(message="weatherStationID may not be null" ) Long weatherStationID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getWeatherStatioRemoteService().deleteWeatherStationRemote(weatherStationID, userID);
		return weatherStationID;
	}
	
	
}
