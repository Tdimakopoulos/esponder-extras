/*
 * 
 */
package eu.esponder.rest.dbsync;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlTransient;

import eu.esponder.dto.model.server.EsponderDeploymentSettings;
import eu.esponder.filemanager.EsponderFileManager;
import eu.esponder.filemanager.ServerDetailsFilePaths;
import eu.esponder.rest.ESponderResource;



// TODO: Auto-generated Javadoc
/**
 * The Class DatafusionRepositoryManager.
 */
@Path("/dbsyncmanager")
@XmlTransient
public class DBSyncManager extends ESponderResource {

	@GET
	@Path("/LocalToRemote")
	public String dbsyncLocalToRemote (
			@QueryParam("LocalChannel") @NotNull(message="LocalChannel may not be null") String LocalChannel,
			@QueryParam("LocalNode") @NotNull(message="LocalNode may not be null") String LocalNode,
			@QueryParam("LocalLeaf") @NotNull(message="LocalLeaf may not be null") String LocalLeaf,
			@QueryParam("RemoteChannel") @NotNull(message="RemoteChannel may not be null") String RemoteChannel,
			@QueryParam("RemoteNode") @NotNull(message="RemoteNode may not be null") String RemoteNode,
			@QueryParam("RemoteLeaf") @NotNull(message="RemoteLeaf may not be null") String RemoteLeaf,
			@QueryParam("Router") @NotNull(message="Router may not be null") String Router,
			@QueryParam("Priority") @NotNull(message="Priority may not be null") String Priority,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		return "1: No Errors : ETA - 10ms";
	}
	
	@GET
	@Path("/RemoteToLocal")
	public String dbsyncRemoteToLocal (
			@QueryParam("LocalChannel") @NotNull(message="LocalChannel may not be null") String LocalChannel,
			@QueryParam("LocalNode") @NotNull(message="LocalNode may not be null") String LocalNode,
			@QueryParam("LocalLeaf") @NotNull(message="LocalLeaf may not be null") String LocalLeaf,
			@QueryParam("RemoteChannel") @NotNull(message="RemoteChannel may not be null") String RemoteChannel,
			@QueryParam("RemoteNode") @NotNull(message="RemoteNode may not be null") String RemoteNode,
			@QueryParam("RemoteLeaf") @NotNull(message="RemoteLeaf may not be null") String RemoteLeaf,
			@QueryParam("Router") @NotNull(message="Router may not be null") String Router,
			@QueryParam("Priority") @NotNull(message="Priority may not be null") String Priority,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		return "1: No Errors : ETA - 10ms";
	}
	
	@GET
	@Path("/StartDBSyncEsponderServer")
	public String StartEsponderServer () {
	
		return "1: DBSYNC Server Started";
	}
	
	@GET
	@Path("/StopDBSyncEsponderServer")
	public String StopEsponderServer () {
	
		return "1: DBSYNC Server Stopped";
	}
	
	@GET
	@Path("/StatusDBSyncEsponderServer")
	public String StatusEsponderServer () {
	
		return "1: DBSYNC Server Running";
	}
	
	
}
