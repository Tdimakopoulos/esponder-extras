package eu.esponder.rest.optimizer;

import java.math.BigDecimal;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.time.DateUtils;

import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.OCEocDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.model.snapshot.Period;
import eu.esponder.rest.ESponderResource;

@Path("/optimizer/oc")
public class OCManager extends ESponderResource {

	ActorRemoteService actorService = getActorRemoteService();
	Long userID;
	PersonnelRemoteService personnelService = getPersonnelRemoteService();
	GenericRemoteService genericService = getGenericRemoteService();
	OperationsCentreRemoteService operationsCentreService = getOperationsCentreRemoteService();
	private static int SECONDS = 30;
	CrisisRemoteService crisisService = getCrisisRemoteService();
	ESponderRemoteMappingService mappingService = getMappingRemoteService();

	
	@GET
	@Path("/createeoc")
	@Produces({ MediaType.APPLICATION_JSON })
	public OCEocDTO CreateEOC(
			@QueryParam("eoclocationname") String eoclocationname,
			@QueryParam("log") BigDecimal log,
			@QueryParam("lat") BigDecimal lat,
			@QueryParam("eoctitle") String eoctitle,
			@QueryParam("szIP") String szIP,
			@QueryParam("szPath") String szPath,
			@QueryParam("szProtocol") String szProtocol,
			@QueryParam("crisisTitle") String crisistitle)
			throws ClassNotFoundException {
		PeriodDTO period = this.createPeriodDTO(SECONDS);
		PointDTO centre = new PointDTO(log, lat, null);
		SphereDTO sphere1 = new SphereDTO();
		sphere1.setTitle(eoclocationname);
		sphere1.setCentre(centre);
		sphere1.setRadius(new BigDecimal(0));

		SphereDTO sphere1p = (SphereDTO) genericService.createEntityRemote(
				sphere1, new Long(1));

		OCEocDTO eocAttica = createOCEocDTO(eoctitle, szProtocol, szIP, szPath,
				sphere1p,crisistitle);
		return eocAttica;
	}

	@POST
	@Path("/createmeoc")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_JSON })
	public OCMeocDTO CreateMEOC(OCEocDTO eocAttica,
			@QueryParam("meoctitle") String meoctitle,
			@QueryParam("szIP") String szIP,
			@QueryParam("szPath") String szPath,
			@QueryParam("szProtocol") String szProtocol,
			@QueryParam("crisisTitle") String crisistitle)
			throws ClassNotFoundException {
		OCMeocDTO meocAthens = createOCMeocDTO(eocAttica, meoctitle,
				szProtocol, szIP, szPath,crisistitle);
		return meocAthens;
	}

	protected PeriodDTO createPeriodDTO(int seconds) {
		java.util.Date now = new java.util.Date();
		Period period = new Period(now.getTime(), DateUtils.addSeconds(now,
				seconds).getTime());
		PeriodDTO periodDTO = (PeriodDTO) mappingService.mapObject(period,
				PeriodDTO.class);
		return periodDTO;
	}

	private OCEocDTO createOCEocDTO(String title, String protocol, String host,
			String path, SphereDTO sphere,String crisistitle) throws ClassNotFoundException {

		OCEocDTO operationsCentreDTO = new OCEocDTO();

		CrisisContextDTO crisisContextDTO = crisisService
				.findCrisisContextDTOByTitle(crisistitle);//"Fire Brigade Drill");

		operationsCentreDTO.setTitle(title);
		operationsCentreDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		operationsCentreDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
		operationsCentreDTO.setEocCrisisContext(crisisContextDTO);
		operationsCentreDTO.setLocationArea(sphere);
		return (OCEocDTO) operationsCentreService.createOperationsCentreRemote(
				operationsCentreDTO, this.userID);
	}

	private OCMeocDTO createOCMeocDTO(OCEocDTO eoc, String title,
			String protocol, String host, String path,String crisistitle)
			throws ClassNotFoundException {

		OCMeocDTO operationsCentreDTO = new OCMeocDTO();

		CrisisContextDTO crisisContextDTO = crisisService
				.findCrisisContextDTOByTitle(crisistitle);

		operationsCentreDTO.setTitle(title);
		operationsCentreDTO.setStatus(ResourceStatusDTO.AVAILABLE);
		operationsCentreDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
		operationsCentreDTO.setSupervisingOC(eoc.getId());
		operationsCentreDTO.setMeocCrisisContext(crisisContextDTO.getId());
		return (OCMeocDTO) operationsCentreService
				.createOperationsCentreRemote(operationsCentreDTO, this.userID);
	}

	private VoIPURLDTO createVoIPURLDTO(String host, String path,
			String protocol) {
		VoIPURLDTO voipURL = new VoIPURLDTO();
		voipURL.setHost(host);
		voipURL.setPath(path);
		voipURL.setProtocol(protocol);
		return voipURL;
	}

}
