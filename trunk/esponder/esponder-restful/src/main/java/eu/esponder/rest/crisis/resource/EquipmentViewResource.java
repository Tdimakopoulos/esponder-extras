/*
 * 
 */
package eu.esponder.rest.crisis.resource;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.MobilePhoneEquipmentDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewResource.
 */
@Path("/crisis/resource")
public class EquipmentViewResource extends ESponderResource {
	
	
	@GET
	@Path("/equipment/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public EquipmentDTO getEquipment( @QueryParam("equipmentID") @NotNull(message="equipmentID may not be null" ) Long equipmentID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getEquipmentRemoteService().findByIdRemote(equipmentID);
	}
	
	@GET
	@Path("/mobilephoneequipment/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public MobilePhoneEquipmentDTO getMobilePhoneEquipment( @QueryParam("mpequipmentID") @NotNull(message="mobile phone equipmentID may not be null" ) Long mpequipmentID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getEquipmentRemoteService().findMobilePhoneByIdRemote(mpequipmentID);
	}
	
	
	@GET
	@Path("/equipment/getEquipmentByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public EquipmentDTO getEquipmentDTOByTitle( @QueryParam("equipmentTitle") @NotNull(message="equipmentTitle may not be null" ) String equipmentTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getEquipmentRemoteService().findByTitleRemote(equipmentTitle);
	}
	
	
	@GET
	@Path("/mobilephoneequipment/getByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public MobilePhoneEquipmentDTO getMobilePhoneEquipmentDTOByTitle( @QueryParam("mpequipmentTitle") @NotNull(message="mpequipmentTitle may not be null" ) String mpequipmentTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getEquipmentRemoteService().findMobilePhoneEquipmentByTitleRemote(mpequipmentTitle);
	}
	
	
	@GET
	@Path("/mobilephoneequipment/getByIMEI")
	@Produces({MediaType.APPLICATION_JSON})
	public MobilePhoneEquipmentDTO getMobilePhoneEquipmentDTOByIMEI( @QueryParam("mpequipmentIMEI") @NotNull(message="mpequipmentIMEI may not be null" ) String mpequipmentIMEI,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getEquipmentRemoteService().findMobilePhoneEquipmentByIMEIRemote(mpequipmentIMEI);
	}
	
	
	@POST
	@Path("/equipment/createEquipment")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public EquipmentDTO createEquipment(@NotNull(message="EquipmentDTO may not be null" ) EquipmentDTO equipmentDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getEquipmentRemoteService().createEquipmentRemote(equipmentDTO, userID);
	}
	
	@POST
	@Path("/equipment/createMobilePhoneEquipment")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public MobilePhoneEquipmentDTO createMobilePhoneEquipment(@NotNull(message="mobilePhoneEquipmentDTO may not be null" ) MobilePhoneEquipmentDTO mobilePhoneEquipmentDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getEquipmentRemoteService().createMobilePhoneEquipmentRemote(mobilePhoneEquipmentDTO, userID);
	}
	
	
	@PUT
	@Path("/equipment/updateEquipment")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public EquipmentDTO updateEquipment(@NotNull(message="equipmentDTO may not be null" ) EquipmentDTO equipmentDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getEquipmentRemoteService().updateEquipmentRemote(equipmentDTO, userID);
	}
	
	
	@PUT
	@Path("/equipment/updateMobilePhoneEquipment")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public MobilePhoneEquipmentDTO updateMobilePhoneEquipment(@NotNull(message="mobilePhoneEquipmentDTO may not be null" ) MobilePhoneEquipmentDTO mobilePhoneEquipmentDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getEquipmentRemoteService().updateMobilePhoneEquipmentRemote(mobilePhoneEquipmentDTO, userID);
	}
	
	
	@DELETE
	@Path("/equipment/delete")
	public Long deleteEquipment(@QueryParam("equipmentID") @NotNull(message="equipmentID may not be null" ) Long equipmentID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getEquipmentRemoteService().deleteEquipmentRemote(equipmentID, userID);
		return equipmentID;
	}
	
	
	@DELETE
	@Path("/equipment/mobilephoneequipmentdelete")
	public Long deleteMobilePhoneEquipment(@QueryParam("mobilePhoneEquipmentID") @NotNull(message="mobilePhoneEquipmentID may not be null" ) Long mobilePhoneEquipmentID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getEquipmentRemoteService().deleteMobilePhoneEquipmentRemote(mobilePhoneEquipmentID, userID);
		return mobilePhoneEquipmentID;
	}
	
	
}
