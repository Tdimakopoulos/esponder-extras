package eu.esponder.rest.optimizer;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextParameterDTO;
import eu.esponder.rest.ESponderResource;

@Path("/optimizer/crisisParameter")
public class CrisisContextParameters extends ESponderResource {

	@GET
	@Path("/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public CrisisContextParameterDTO findCrisisContextParameterById(
			@QueryParam("crisisContextParameterID") @NotNull(message="crisisContextParameterID may not be null") Long crisisContextParameterID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		CrisisContextParameterDTO crisisContextParameterDTO = this.getCrisisRemoteService().findCrisisContextParameterDTOById(crisisContextParameterID);
		return crisisContextParameterDTO;
	}
	
	
	/**
	 * Find crisis context parameter by id.
	 *
	 * @param crisisContextParameterID the crisis context parameter id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the crisis context parameter dto
	 */
	@GET
	@Path("/findByCrisis")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO findAllCrisisContextsParametersByCrisis(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextID may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		List<CrisisContextParameterDTO> crisisContextParameters = this.getCrisisRemoteService().findCrisisContextParametersByCrisisRemote(crisisContextID);
		if(!crisisContextParameters.isEmpty())
			return new ResultListDTO(crisisContextParameters);
		else
			return null;
	}
}
