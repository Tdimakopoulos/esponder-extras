/*
 * 
 */
package eu.esponder.rest;

import javax.naming.NamingException;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.LogisticsRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.OrganisationRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService;
import eu.esponder.controller.crisis.terrain.TerrainRemoteService;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.crisis.weatherstation.WeatherStationRemoteService;
import eu.esponder.controller.events.EventsEntityRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.osgi.service.event.EventServiceManagerRemote;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderResource.
 */
public abstract class ESponderResource {

	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @param FunctionID the function id
	 * @return the long
	 */
	protected boolean SecurityCheck(String userID,int FunctionID)
	{
		//return new Long(userID);
		return true;
	}

	/**
	 * Security get user id.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	protected Long SecurityGetUserID(String userID)
	{
		
		return new Long(4);
//		return getUserRemoteService().findUserByPKIRemote(userID).getActorID();
	}

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the Terrain / Vegetation  remote service.
	 *
	 * @return the Terrain / Vegetation station remote service
	 */
	protected TerrainRemoteService getTerrainRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/TerrainAndVegatationBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the weather station remote service.
	 *
	 * @return the weather station remote service
	 */
	protected WeatherStationRemoteService getWeatherStatioRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/WeatherStatioBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the sensor remote service.
	 *
	 * @return the sensor remote service
	 */
	protected SensorRemoteService getSensorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}		

	//*******************************************************************************

	/**
	 * Gets the actor remote service.
	 *
	 * @return the actor remote service
	 */
	protected ActorRemoteService getActorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the equipment remote service.
	 *
	 * @return the equipment remote service
	 */
	protected EquipmentRemoteService getEquipmentRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EquipmentBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the mapping remote service.
	 *
	 * @return the mapping remote service
	 */
	protected ESponderRemoteMappingService getMappingRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the operations centre remote service.
	 *
	 * @return the operations centre remote service
	 */
	protected OperationsCentreRemoteService getOperationsCentreRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/OperationsCentreBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the action remote service.
	 *
	 * @return the action remote service
	 */
	protected ActionRemoteService getActionRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the generic remote service.
	 *
	 * @return the generic remote service
	 */
	protected GenericRemoteService getGenericRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the configuration remote service.
	 *
	 * @return the configuration remote service
	 */
	protected ESponderConfigurationRemoteService getConfigurationRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderConfigurationBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the user remote service.
	 *
	 * @return the user remote service
	 */
	protected UserRemoteService getUserRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/UserBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the personnel remote service.
	 *
	 * @return the personnel remote service
	 */
	protected PersonnelRemoteService getPersonnelRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/PersonnelBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the personnel remote service.
	 *
	 * @return the personnel remote service
	 */
	protected PersonnelRemoteService getPersonnelCompetencesRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/PersonnelBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the logistics remote service.
	 *
	 * @return the logistics remote service
	 */
	protected LogisticsRemoteService getLogisticsRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/LogisticsBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the osgi events entity remote service.
	 *
	 * @return the osgi events entity remote service
	 */
	protected EventsEntityRemoteService getOsgiEventsEntityRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EventsEntityBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the resource category remote service.
	 *
	 * @return the resource category remote service
	 */
	protected ResourceCategoryRemoteService getResourceCategoryRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ResourceCategoryBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the type remote service.
	 *
	 * @return the type remote service
	 */
	protected TypeRemoteService getTypeRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the crisis remote service.
	 *
	 * @return the crisis remote service
	 */
	protected CrisisRemoteService getCrisisRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/CrisisBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}
	
	protected OrganisationRemoteService getOrganizationRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/OrganisationBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	protected EventServiceManagerRemote getEventServerRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EventServiceManagerBean/remote");
		} catch (NamingException e) {
			System.out.println(" Error on Get Resource Locator : "+e.getMessage());
			return null;
		}
	}

}
