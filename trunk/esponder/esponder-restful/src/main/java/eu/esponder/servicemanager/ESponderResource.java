/*
 * 
 */
package eu.esponder.servicemanager;

import java.math.BigDecimal;

import javax.naming.NamingException;

import org.apache.commons.lang.time.DateUtils;


import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.LogisticsRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.OrganisationRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.events.EventsEntityRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.controller.persistence.CrudRemoteService;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.snapshot.Period;
import eu.esponder.model.snapshot.location.Point;
import eu.esponder.model.snapshot.location.Sphere;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderResource.
 */
public class ESponderResource {

	protected static final String USER_NAME = "ctri";
	protected Long userID;
	
	protected ActorRemoteService actorService;
	protected EquipmentRemoteService equipmentService;
	protected OperationsCentreRemoteService operationsCentreService;
	protected SensorRemoteService sensorService;
	protected TypeRemoteService typeService;
	protected UserRemoteService userService;
	protected ActionRemoteService actionService;
	protected ResourceCategoryRemoteService resourceCategoryService;
	protected GenericRemoteService genericService;
	protected ESponderRemoteMappingService mappingService;
	protected OrganisationRemoteService organisationService;
	protected PersonnelRemoteService personnelService;
	protected ESponderConfigurationRemoteService configService;
	protected LogisticsRemoteService logisticsService;
	protected CrisisRemoteService crisisService;
	@SuppressWarnings("rawtypes")
	protected CrudRemoteService crudService;
		
	
	public ESponderResource() throws NamingException {
		configService = ServiceLocator.getResource("esponder/ESponderConfigurationBean/remote");
		crudService = ServiceLocator.getResource("esponder/CrudBean/remote");
		actorService = ServiceLocator.getResource("esponder/ActorBean/remote");
		equipmentService = ServiceLocator.getResource("esponder/EquipmentBean/remote");
		operationsCentreService = ServiceLocator.getResource("esponder/OperationsCentreBean/remote");
		sensorService = ServiceLocator.getResource("esponder/SensorBean/remote");
		typeService = ServiceLocator.getResource("esponder/TypeBean/remote");
		userService = ServiceLocator.getResource("esponder/UserBean/remote");
		actionService = ServiceLocator.getResource("esponder/ActionBean/remote");
		resourceCategoryService = ServiceLocator.getResource("esponder/ResourceCategoryBean/remote");
		genericService = ServiceLocator.getResource("esponder/GenericBean/remote");
		mappingService = ServiceLocator.getResource("esponder/ESponderMappingBean/remote");
		organisationService = ServiceLocator.getResource("esponder/OrganisationBean/remote");
		personnelService = ServiceLocator.getResource("esponder/PersonnelBean/remote");
		logisticsService = ServiceLocator.getResource("esponder/LogisticsBean/remote");
		crisisService = ServiceLocator.getResource("esponder/CrisisBean/remote");
		

			userID= new Long(4);

	}
	
	protected Period createPeriod(int seconds) {
		java.util.Date now = new java.util.Date();
		Period period = new Period(now.getTime(), DateUtils.addSeconds(now, seconds).getTime());
		return period; 
	}

	protected PeriodDTO createPeriodDTO(int seconds) {
		java.util.Date now = new java.util.Date();
		Period period = new Period(now.getTime(), DateUtils.addSeconds(now, seconds).getTime());
		PeriodDTO periodDTO = (PeriodDTO) mappingService.mapObject(period, PeriodDTO.class);
		return periodDTO;
	}
	
	

	protected Sphere createSphere(
			Double latitude, Double longitude, Double altitude, Double radius) {
		
		Point centre = new Point(
				new BigDecimal(latitude), new BigDecimal(longitude), null != altitude ? new BigDecimal(altitude) : null);
		Sphere sphere = new Sphere(centre, null != radius ? new BigDecimal(radius) : null);
		return sphere;
	}
	
	protected SphereDTO createSphereDTO(
			Double latitude, Double longitude, Double altitude, Double radius, String title) {
		
		PointDTO centre = new PointDTO(
				new BigDecimal(latitude), new BigDecimal(longitude), null != altitude ? new BigDecimal(altitude) : null);
		SphereDTO sphere = new SphereDTO(centre, null != radius ? new BigDecimal(radius) : null, title);
		return sphere;
	}
	
	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @param FunctionID the function id
	 * @return the long
	 */
	public boolean SecurityCheck(String userID,int FunctionID)
	{
		//return new Long(userID);
		return true;
	}
	
	/**
	 * Security get user id.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	public Long SecurityGetUserID(String userID)
	{
		//return new Long(userID);
		return new Long(1);
	}
	
	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	public ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the sensor remote service.
	 *
	 * @return the sensor remote service
	 */
	public SensorRemoteService getSensorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}		
		
	//*******************************************************************************

	/**
	 * Gets the actor remote service.
	 *
	 * @return the actor remote service
	 */
	public ActorRemoteService getActorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the equipment remote service.
	 *
	 * @return the equipment remote service
	 */
	public EquipmentRemoteService getEquipmentRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EquipmentBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the mapping remote service.
	 *
	 * @return the mapping remote service
	 */
	public ESponderRemoteMappingService getMappingRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the operations centre remote service.
	 *
	 * @return the operations centre remote service
	 */
	public OperationsCentreRemoteService getOperationsCentreRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/OperationsCentreBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the action remote service.
	 *
	 * @return the action remote service
	 */
	public ActionRemoteService getActionRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the generic remote service.
	 *
	 * @return the generic remote service
	 */
	public GenericRemoteService getGenericRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the configuration remote service.
	 *
	 * @return the configuration remote service
	 */
	public ESponderConfigurationRemoteService getConfigurationRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderConfigurationBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the user remote service.
	 *
	 * @return the user remote service
	 */
	public UserRemoteService getUserRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/UserBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the personnel remote service.
	 *
	 * @return the personnel remote service
	 */
	public PersonnelRemoteService getPersonnelRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/PersonnelBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the personnel remote service.
	 *
	 * @return the personnel remote service
	 */
	public PersonnelRemoteService getPersonnelCompetencesRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/PersonnelBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the logistics remote service.
	 *
	 * @return the logistics remote service
	 */
	public LogisticsRemoteService getLogisticsRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/LogisticsBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the osgi events entity remote service.
	 *
	 * @return the osgi events entity remote service
	 */
	public EventsEntityRemoteService getOsgiEventsEntityRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EventsEntityBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the resource category remote service.
	 *
	 * @return the resource category remote service
	 */
	public ResourceCategoryRemoteService getResourceCategoryRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ResourceCategoryBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the type remote service.
	 *
	 * @return the type remote service
	 */
	public TypeRemoteService getTypeRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the crisis remote service.
	 *
	 * @return the crisis remote service
	 */
	public CrisisRemoteService getCrisisRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/CrisisBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}
	
	

}
