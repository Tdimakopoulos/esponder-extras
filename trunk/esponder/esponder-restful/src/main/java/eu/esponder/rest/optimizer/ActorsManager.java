package eu.esponder.rest.optimizer;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.df.optimizer.ManageTeams;
import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorCMDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.OCEocDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.rest.ESponderResource;

@Path("/optimizer/actor")
public class ActorsManager extends ESponderResource {

	ActorRemoteService actorService = getActorRemoteService();
	Long userID;
	PersonnelRemoteService personnelService = getPersonnelRemoteService();
	GenericRemoteService genericService = getGenericRemoteService();
	OperationsCentreRemoteService operationsCentreService = getOperationsCentreRemoteService();

	@GET
	@Path("/createteam")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO CreateTeam(@QueryParam("members") int imembers,
			@QueryParam("title") String title,
			@QueryParam("ictitle") String incidentCommanderTitle,
			@QueryParam("personneltitle") String personnelTitle,
			@QueryParam("meoctitle") String meocTitle,
			@QueryParam("OrganizationTitle") String orgTitle
			) {

		List pret = new ArrayList();
		pret.add("Error on Creation of Team");

		try {
			ManageTeams pManage = new ManageTeams();
			return pManage
					.CreateTeam(imembers, title, incidentCommanderTitle,
							personnelTitle, "Voip Host", "Voip Path", "voip",
							meocTitle,orgTitle);
		} catch (NamingException e) {
			System.out.println("Error on Team creation for Optimizer : "
					+ e.getMessage());
		}

		return new ResultListDTO(pret);
	}

	@GET
	@Path("/createcrisismanager")
	@Produces({ MediaType.APPLICATION_JSON })
	public ActorCMDTO CreateCrisisManager(
			@QueryParam("EOCTitle") String EOCTitle,
			@QueryParam("CMTitle") String szCMTitle,
			@QueryParam("pTitle") String szpTitle,
			@QueryParam("szIP") String szIP,
			@QueryParam("szPath") String szPath,
			@QueryParam("szProtocol") String szProtocol) {
		OCEocDTO eocForUpdate = (OCEocDTO) operationsCentreService
				.findEocByTitleRemote(EOCTitle);
		// createActorDTO(Class<? extends ActorDTO> actorClass,
		// OperationsCentreDTO oc, String title, ActorDTO supervisor,
		// String personnelTitle, String host, String path, String protocol)

		ActorCMDTO crisisManager = (ActorCMDTO) createActorDTO(
				ActorCMDTO.class, eocForUpdate, szCMTitle, null, szpTitle,
				szIP, szPath, szProtocol);
		System.out.println(crisisManager.getId());
		System.out.println(eocForUpdate.getId());
		eocForUpdate.setCrisisManager(crisisManager.getId());

		operationsCentreService.updateEOCRemote(eocForUpdate, userID);
		return crisisManager;
	}

	@POST
	@Path("/createincidentcommander")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ActorICDTO CreateIncidentCommander(ActorCMDTO crisisManager,
			@QueryParam("MEOCTitle") String MEOCTitle,
			@QueryParam("ICTitle") String szICTitle,
			@QueryParam("pTitle") String szpTitle,
			@QueryParam("szIP") String szIP,
			@QueryParam("szPath") String szPath,
			@QueryParam("szProtocol") String szProtocol) {
		OCMeocDTO meocForUpdate = (OCMeocDTO) operationsCentreService
				.findMeocByTitleRemote(MEOCTitle);

		ActorICDTO incidentCommander = (ActorICDTO) createActorDTO(
				ActorICDTO.class, meocForUpdate, szICTitle, crisisManager,
				szpTitle, szIP, szPath, szProtocol);

		meocForUpdate.setIncidentCommander(incidentCommander.getId());

		operationsCentreService.updateMEOCRemote(meocForUpdate, userID);
		return incidentCommander;
	}

	@POST
	@Path("/createfrc")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ActorFRCDTO CreateFRC(ActorICDTO incidentCommander,
			@QueryParam("FRCTitle") String szFRCTitle,
			@QueryParam("pTitle") String szpTitle,
			@QueryParam("szIP") String szIP,
			@QueryParam("szPath") String szPath,
			@QueryParam("szProtocol") String szProtocol) {
		ActorFRCDTO firstChief = (ActorFRCDTO) createActorDTO(
				ActorFRCDTO.class, null, szFRCTitle, incidentCommander,
				szpTitle, szIP, szPath, szProtocol);
		return firstChief;
	}

	@POST
	@Path("/createfr")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ActorFRDTO CreateFR(ActorFRCDTO firstChief,
			@QueryParam("FRTitle") String szFRTitle,
			@QueryParam("pTitle") String szpTitle,
			@QueryParam("szIP") String szIP,
			@QueryParam("szPath") String szPath,
			@QueryParam("szProtocol") String szProtocol) {
		ActorFRDTO subordinate11 = (ActorFRDTO) createActorDTO(
				ActorFRDTO.class, null, szFRTitle, firstChief, szpTitle, szIP,
				szPath, szProtocol);
		return subordinate11;
	}

	private ActorDTO createActorDTO(Class<? extends ActorDTO> actorClass,
			OperationsCentreDTO oc, String title, ActorDTO supervisor,
			String personnelTitle, String host, String path, String protocol) {

		PersonnelDTO personnelDTO = personnelService
				.findPersonnelByTitleRemote(personnelTitle);
		if (personnelDTO != null) {
			System.out.println("Personnel not null");
			if (actorClass == ActorCMDTO.class) {
				System.out.println("OC in Actor Creation CM : " + oc.getId());
				System.out.println("Personnel in Actor Creation CM : "
						+ personnelDTO.getId());
				ActorCMDTO actorDTO = new ActorCMDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				actorDTO.setOperationsCentre(oc.getId());
				return actorService.createCrisisManagerRemote(actorDTO,
						new Long(4));
			} else if (actorClass == ActorICDTO.class) {

				System.out.println("OC in Actor Creation IC : " + oc.getId());
				System.out.println("Personnel in Actor Creation IC : "
						+ personnelDTO.getId());
				
				ActorICDTO actorDTO = new ActorICDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				actorDTO.setCrisisManager(supervisor.getId());
				actorDTO.setOperationsCentre(oc.getId());
				return actorService.createIncidentCommanderRemote(actorDTO,
						new Long(4));
			} else if (actorClass == ActorFRCDTO.class) {
				ActorFRCDTO actorDTO = new ActorFRCDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				return actorService.createFRChiefRemote(actorDTO, new Long(4));
			} else if (actorClass == ActorFRDTO.class) {
				ActorFRDTO actorDTO = new ActorFRDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				actorDTO.setFRChief(supervisor.getId());
				return actorService.createFRRemote(actorDTO, new Long(4));
			}
		} else {

			return null;
		}
		return null;

	}

	private VoIPURLDTO createVoIPURLDTO(String host, String path,
			String protocol) {
		VoIPURLDTO voipURL = new VoIPURLDTO();
		voipURL.setHost(host);
		voipURL.setPath(path);
		voipURL.setProtocol(protocol);
		return voipURL;
	}
}
