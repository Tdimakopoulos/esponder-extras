/*
 * 
 */
package eu.esponder.rest.crisis;

import java.io.IOException;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.security.KeyStorageDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class HierarchyResource.
 */
@Path("/crisis/context")
public class HierarchyResource  extends ESponderResource {

	

	/**
	 * Gets the meocs for crisis.
	 *
	 * @param crisisContextID the crisis context id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the operations centre dto
	 */
	@GET
	@Path("/Hierarchy/GetMeocsForCrisis")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO GetMeocsForCrisis(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextID may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		ResultListDTO resultList = new ResultListDTO();
		resultList.setResultList(this.getOperationsCentreRemoteService().findMeocsByCrisisRemote(crisisContextID));
		return resultList;
	}

	/**
	 * Gets the eoc for crisis.
	 *
	 * @param crisisContextID the crisis context id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the operations centre dto
	 */
	@GET
	@Path("/Hierarchy/GetEocForCrisis")
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreDTO GetEocForCrisis(
			@QueryParam("crisisContextID") @NotNull(message="crisisContextID may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOperationsCentreRemoteService().findEocByCrisisRemote(crisisContextID);
	}

	/**
	 * Gets the supervisors for meoc.
	 *
	 * @param meocID the meoc id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@GET
	@Path("/Hierarchy/GetSupervisorForMeoc")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO GetSupervisorsForMeoc(
			@QueryParam("meocID") @NotNull(message="meocID may not be null") Long meocID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOperationsCentreRemoteService().findMeocSupervisorRemote(meocID);
	}

	/**
	 * Gets the supervisors for eoc.
	 *
	 * @param eocID the eoc id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@GET
	@Path("/Hierarchy/GetSupervisorForEoc")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO GetSupervisorsForEoc(
			@QueryParam("eocID") @NotNull(message="eocID may not be null") Long eocID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOperationsCentreRemoteService().findEocSupervisorRemote(eocID);
	}

	/**
	 * Gets the supervisors for actor.
	 *
	 * @param actorID the actor id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the actor dto
	 */
	@GET
	@Path("/Hierarchy/GetSupervisorForActor")
	@Produces({MediaType.APPLICATION_JSON})
	public ActorDTO GetSupervisorsForActor(
			@QueryParam("actorID") @NotNull(message="actorID may not be null") Long actorID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getActorRemoteService().findSupervisorForActorRemote(actorID);
	}

}