/*
 * 
 */
package eu.esponder.rest.crisis;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.CrisisContextParameterDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.rest.ESponderResource;
//import eu.esponder.rest.action.Consumes;
//import eu.esponder.rest.action.POST;
import eu.esponder.util.rest.StringParamUnmarshaller.DateFormat;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisSnapshotResource.
 */
@Path("/crisis/snapshot")
public class CrisisSnapshotResource extends ESponderResource {

	@POST
	@Path("/sensorsall")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	public ResultListDTO getSensorsSnapshotByDate(
			@NotNull(message="Equipments may not be null")  EquipmentDTO pequip,
			@QueryParam("sensorID") @NotNull(message="operationsCentreID may not be null")  Long sensorID, 
			@QueryParam("maxDate") @DateFormat("yyyy-MM-dd'T'HH:mm:ss") @NotNull(message="maxDate may not be null") Date maxDate, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {


		List<SensorSnapshotDTO> sensorsReading = new ArrayList();
		List<Long> list = new ArrayList(pequip.getSensors());
		for(int i=0;i<pequip.getSensors().size();i++)
			sensorsReading.add(getSensorSnapshot(list.get(i).longValue(), maxDate));

		return new ResultListDTO(sensorsReading);

	}

	/**
	 * Gets the snapshot by date.
	 *
	 * @param operationsCentreID the operations centre id
	 * @param maxDate the max date
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the snapshot by date
	 */
	@GET()
	@Path("/oc")
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreSnapshotDTO getSnapshotByDate(
			@QueryParam("operationsCentreID") @NotNull(message="operationsCentreID may not be null")  Long operationsCentreID,
			@QueryParam("maxDate") @DateFormat("yyyy-MM-dd'T'HH:mm:ss") @NotNull(message="maxDate may not be null") Date maxDate, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		OperationsCentreSnapshotDTO snapshot = this.getOperationsCentreRemoteService().
				findOperationsCentreSnapshotByDateRemote(operationsCentreID, maxDate);
		
		if(snapshot != null)
			return snapshot;
		else
			return null;
	}
	
	

	/**
	 * Gets the sensor snapshot by date.
	 *
	 * @param sensorID the sensor id
	 * @param maxDate the max date
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the sensor snapshot by date
	 */
	@GET()
	@Path("/sensor")
	@Produces({MediaType.APPLICATION_JSON})
	public SensorSnapshotDTO getSensorSnapshotByDate(
			@QueryParam("sensorID") @NotNull(message="operationsCentreID may not be null")  Long sensorID, 
			@QueryParam("maxDate") @DateFormat("yyyy-MM-dd'T'HH:mm:ss") @NotNull(message="maxDate may not be null") Date maxDate, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return getSensorSnapshot(sensorID, maxDate);
	}

	
	
	
	
	/**
	 * Gets the all sensor snapshot.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the all sensor snapshot
	 */
	@GET()
	@Path("/sensorSnapshot/findall")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getAllSensorSnapshots(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		
		

		// We place nulls inside the Sensor field of each SensorSnapshot in order to avoid cycles
		List<SensorSnapshotDTO> sensorSnapshots = this.getSensorRemoteService().findAllSensorSnapshotsRemote();
		Iterator<SensorSnapshotDTO> iterator = sensorSnapshots.iterator();
		while(iterator.hasNext()) {
			iterator.next().setSensor(null);
		}
		return new ResultListDTO(sensorSnapshots);
	}


	/**
	 * Gets the all sensor snapshots by sensor.
	 *
	 * @param sensorID the sensor id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the all sensor snapshots by sensor
	 */
	@GET()
	@Path("/sensorSnapshot/findallBySensor")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getAllSensorSnapshotsBySensor(
			@QueryParam("sensorID") @NotNull(message="sensorID may not be null") Long sensorID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		List<SensorSnapshotDTO> sensorSnapshots = this.getSensorRemoteService().findAllSensorSnapshotsRemote();
		return new ResultListDTO(sensorSnapshots);
	}



	/**
	 * Sets the equipment snapshot.
	 *
	 * @param equipment the equipment
	 * @param maxDate the max date
	 */
	private void setEquipmentSnapshot(EquipmentDTO equipment, Date maxDate) {

		//		EquipmentSnapshotDTO snapshot = this.getMappingService().mapEquipmentSnapshot(equipment.getId(), maxDate);
		EquipmentSnapshotDTO snapshot = this.getEquipmentRemoteService().findEquipmentSnapshotByDateRemote(equipment.getId(), maxDate);
		equipment.setSnapshot(snapshot);
	}

	/**
	 * Gets the sensor snapshot.
	 *
	 * @param sensorID the sensor id
	 * @param maxDate the max date
	 * @return the sensor snapshot
	 */
	private SensorSnapshotDTO getSensorSnapshot(Long sensorID, Date maxDate) {
		SensorSnapshotDTO snapshot = this.getSensorRemoteService().findSensorSnapshotByDateRemote(sensorID, maxDate);
		return snapshot;
	}

}
