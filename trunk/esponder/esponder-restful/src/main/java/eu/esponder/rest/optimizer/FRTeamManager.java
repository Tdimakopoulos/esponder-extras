package eu.esponder.rest.optimizer;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.rest.ESponderResource;


@Path("/optimizer/team")
public class FRTeamManager extends ESponderResource {

	ActorRemoteService actorService = getActorRemoteService();
	Long userID;
	PersonnelRemoteService personnelService = getPersonnelRemoteService();
	GenericRemoteService genericService = getGenericRemoteService();
	OperationsCentreRemoteService operationsCentreService = getOperationsCentreRemoteService();
	
	
	
	@GET
	@Path("/createteam")
	@Produces({ MediaType.APPLICATION_JSON })
	public FRTeamDTO CreateTeamWithTitles(@QueryParam("szic")String szic,@QueryParam("szfirstChief")String szfirstChief,@QueryParam("szTeamTitle")String szTeamTitle) throws ClassNotFoundException
	{
		ActorICDTO icDTO  = (ActorICDTO) actorService.findICByTitleRemote(szic);
		ActorFRCDTO firstChiefDTO  = (ActorFRCDTO) actorService.findFRCByTitleRemote(szfirstChief);
		
		FRTeamDTO frTeam = new FRTeamDTO();
		frTeam.setFrchief(firstChiefDTO.getId());
		frTeam.setTitle(szTeamTitle);
		frTeam.setIncidentCommander(icDTO.getId());
		
		FRTeamDTO frTeamret = (FRTeamDTO) genericService.createEntityRemote(frTeam, userID);
		
		return frTeamret;
	}
	
}
