/*
 * 
 */
package eu.esponder.rest.crisis.resource;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.crisis.TerrainAndVegetationDTO;
import eu.esponder.rest.ESponderResource;

@Path("/crisis/resource")
public class TerrainAndVegetationViewResource extends ESponderResource {
	
	
	@GET
	@Path("/terrain/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public TerrainAndVegetationDTO getTerrainDTOById( @QueryParam("terrainID") @NotNull(message="terrainID may not be null" ) Long terrainID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getTerrainRemoteService().findTerrainAndVegetationDTOById(terrainID);
	}
	
	
	@GET
	@Path("/terrain/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public TerrainAndVegetationDTO getTerrainByTitle( @QueryParam("terrainTitle") @NotNull(message="terrainTitle may not be null" ) String terrainTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getTerrainRemoteService().findTerrainAndVegetationByTitleRemote(terrainTitle, userID);
	}
	
	
	
	@POST
	@Path("/terrain/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public TerrainAndVegetationDTO createTerrainAndVegetationDTO(@NotNull(message="terrainAndVegetationDTO may not be null" ) TerrainAndVegetationDTO terrainAndVegetationDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getTerrainRemoteService().createTerrainAndVegetationRemote(terrainAndVegetationDTO, userID);
	}
	
	
	@PUT
	@Path("/terrain/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public TerrainAndVegetationDTO updateTerrainAndVegetationDTO(@NotNull(message="terrainAndVegetationDTO may not be null" ) TerrainAndVegetationDTO terrainAndVegetationDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getTerrainRemoteService().updateTerrainAndVegetationRemote(terrainAndVegetationDTO, userID);
	}
	
	
	
	@DELETE
	@Path("/terrain/delete")
	public Long deleteTerrainAndVegetation(@QueryParam("terrainID") @NotNull(message="terrainID may not be null" ) Long terrainID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getTerrainRemoteService().deleteTerrainAndVegetationRemote(terrainID, userID);
		return terrainID;
	}
	
}
