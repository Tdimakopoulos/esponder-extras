package eu.esponder.rest.voip;


import java.io.Serializable;

/**
 *
 * @author MEOC-WS1
 */
public class voipset implements Serializable {

    private Long voipid;
    Boolean bvalue;
    String frUsername;
    
	public String getFrUsername() {
		return frUsername;
	}
	public void setFrUsername(String frUsername) {
		this.frUsername = frUsername;
	}
	public Long getVoipid() {
		return voipid;
	}
	public void setVoipid(Long voipid) {
		this.voipid = voipid;
	}
	public Boolean getBvalue() {
		return bvalue;
	}
	public void setBvalue(Boolean bvalue) {
		this.bvalue = bvalue;
	}
    
    
}
