/*
 * 
 */
package eu.esponder.rest.serversettings;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlTransient;

import eu.esponder.dto.model.server.EsponderDeploymentSettings;
import eu.esponder.filemanager.EsponderFileManager;
import eu.esponder.filemanager.ServerDetailsFilePaths;
import eu.esponder.rest.ESponderResource;



// TODO: Auto-generated Javadoc
/**
 * The Class DatafusionRepositoryManager.
 */
@Path("/servercontrol")
@XmlTransient
public class EsponderServerSettings extends ESponderResource {

	@GET
	@Path("/getsettings")
	@Produces({ MediaType.APPLICATION_JSON })
	public EsponderDeploymentSettings getSettings () {
	
		EsponderDeploymentSettings pSettings= new EsponderDeploymentSettings();
		EsponderFileManager pFileManager= new EsponderFileManager();
		ServerDetailsFilePaths pFilenames=new ServerDetailsFilePaths();
		pFileManager.OpenFileForReading(pFilenames.GetFilename());
		
		pFileManager.ReadNext();
		pSettings.setSzServerID(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setLserverID(pFileManager.GetReadedLineLong());
		pFileManager.ReadNext();
		pSettings.setSzServerName(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setIserverType(pFileManager.GetReadedLineint());
		pFileManager.ReadNext();
		pSettings.setSzserverType(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setSzIP(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setSzPort(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setSzChannels(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setSzNodeNames(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setSzLeafs(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setiField1(pFileManager.GetReadedLineint());
		pFileManager.ReadNext();
		pSettings.setiField2(pFileManager.GetReadedLineint());
		pFileManager.ReadNext();
		pSettings.setiField3(pFileManager.GetReadedLineint());
		pFileManager.ReadNext();
		pSettings.setiField3(pFileManager.GetReadedLineint());
		pFileManager.ReadNext();
		pSettings.setiField4(pFileManager.GetReadedLineint());
		pFileManager.ReadNext();
		pSettings.setiField5(pFileManager.GetReadedLineint());
		pFileManager.ReadNext();
		pSettings.setlFiend1(pFileManager.GetReadedLineLong());
		pFileManager.ReadNext();
		pSettings.setlFiend2(pFileManager.GetReadedLineLong());
		pFileManager.ReadNext();
		pSettings.setlFiend3(pFileManager.GetReadedLineLong());
		pFileManager.ReadNext();
		pSettings.setlFiend4(pFileManager.GetReadedLineLong());
		pFileManager.ReadNext();
		pSettings.setlFiend5(pFileManager.GetReadedLineLong());
		pFileManager.ReadNext();
		pSettings.setSzTextField1(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setSzTextField2(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setSzTextField3(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setSzTextField4(pFileManager.GetReadedLine());
		pFileManager.ReadNext();
		pSettings.setSzTextField5(pFileManager.GetReadedLine());
		pFileManager.closeConnection();
		return pSettings;
	}
	
	@GET
	@Path("/StartEsponderServer")
	public String StartEsponderServer () {
	
		return "1: Server Started";
	}
	
	@GET
	@Path("/StopEsponderServer")
	public String StopEsponderServer () {
	
		return "1: Server Stopped";
	}
	
	@GET
	@Path("/StatusEsponderServer")
	public String StatusEsponderServer () {
	
		return "1: Server Running";
	}
	
	
}
