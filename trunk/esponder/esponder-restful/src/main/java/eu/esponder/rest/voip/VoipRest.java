package eu.esponder.rest.voip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.voip.ConferenceParticipantsDTO;
import eu.esponder.dto.model.voip.VoipConferenceDTO;
import eu.esponder.rest.ESponderResource;
import eu.esponder.dto.model.ResultListDTO;

@Path("/Voip")
public class VoipRest extends ESponderResource  {

		
	@GET
	@Path("/CallEOC")
	@Produces({MediaType.APPLICATION_JSON})
	public String callEOC(
			@QueryParam("eocID") @NotNull(message="subCrisisID may not be null") Long eocID,
			@QueryParam("meocID") @NotNull(message="subCrisisID may not be null") Long meocID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return "Success";
	}
	
	@GET
	@Path("/CallMEOC")
	@Produces({MediaType.APPLICATION_JSON})
	public String callMEOC(
			@QueryParam("eocID") @NotNull(message="subCrisisID may not be null") Long eocID,
			@QueryParam("meocID") @NotNull(message="subCrisisID may not be null") Long meocID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return "Success";
	}
	
	@GET
	@Path("/MEOCCallFRC")
	@Produces({MediaType.APPLICATION_JSON})
	public String meoccallFRC(
			@QueryParam("frcID") @NotNull(message="subCrisisID may not be null") Long frcID,
			@QueryParam("meocID") @NotNull(message="subCrisisID may not be null") Long meocID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return "Success";
	}
	
	@GET
	@Path("/EOCCallFRC")
	@Produces({MediaType.APPLICATION_JSON})
	public String eoccallFRC(
			@QueryParam("frcID") @NotNull(message="subCrisisID may not be null") Long frcID,
			@QueryParam("eocID") @NotNull(message="subCrisisID may not be null") Long eocID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return "Success";
	}
	
	
	@GET
	@Path("/ConferenceCall")
	@Produces({MediaType.APPLICATION_JSON})
	public String eoccallFRC(
			@QueryParam("frsIDs") @NotNull(message="subCrisisID may not be null") String frsIDs,
			@QueryParam("eocID") @NotNull(message="subCrisisID may not be null") Long eocID,
			@QueryParam("meocID") @NotNull(message="subCrisisID may not be null") Long meocID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return "Success";
	}
	
	@POST
	@Path("/updateconference")
	public String UpdateConference(
			@QueryParam("ConfID")  @NotNull(message="dConferenceID Params object may not be null Long") Long dConferenceID,
			@QueryParam("frUsername") @NotNull(message="frUsername Params object may not be null String") String frUsername,
			@QueryParam("Status") @NotNull(message="Status Params object may not be null Boolean") Boolean bstatus) {
		
		String filePathString=String.valueOf(dConferenceID)+".dat";
		File f = new File(filePathString);
		if(f.exists() && !f.isDirectory()) 
		{ 
			voipDB pdb=new voipDB();
			pdb.setFilename(filePathString);
			try {
				pdb.LoadFronFile();
				
				voipset pset= new voipset();
				pset.setVoipid(dConferenceID);
				pset.setFrUsername(frUsername);
				pset.setBvalue(bstatus);
				if(pdb.Edit(pset)){}else{
				pdb.Add(pset);}
				try {
					pdb.SaveOnFile();
				} catch (FileNotFoundException e) {
					System.out.println("Error on save file for VOIP : File no found "+e.getMessage());
				} catch (IOException e) {
					System.out.println("Error on save file for VOIP : IO "+e.getMessage());
				}
				
			} catch (FileNotFoundException e) {
				System.out.println("Error on save file for VOIP : File no found "+e.getMessage());
			} catch (IOException e) {
				System.out.println("Error on save file for VOIP : IO "+e.getMessage());
			} catch (ClassNotFoundException e) {
				System.out.println("Error on save file for VOIP : Class "+e.getMessage());
			}
			
		}else
		{
			voipDB pdb=new voipDB();
			pdb.setFilename(filePathString);
			voipset pset= new voipset();
			pset.setVoipid(dConferenceID);
			pset.setFrUsername(frUsername);
			pset.setBvalue(bstatus);
			pdb.Add(pset);
			try {
				pdb.SaveOnFile();
			} catch (FileNotFoundException e) {
				System.out.println("Error on save file for VOIP : File no found "+e.getMessage());
			} catch (IOException e) {
				System.out.println("Error on save file for VOIP : IO "+e.getMessage());
			}
		}
		return "0";
	}
}
