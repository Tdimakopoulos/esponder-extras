/*
 * 
 */
package eu.esponder.rest.crisis;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.OCEocDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.rest.ESponderResource;


// TODO: Auto-generated Javadoc
/**
 * The Class CrisisContextResource.
 */
@Path("/crisis/context")
public class CrisisContextResource extends ESponderResource {

	

	/**
	 * Gets the user operations centre.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the user operations centre
	 */
	@GET
	@Path("/oc")
	@Produces({MediaType.APPLICATION_JSON})
	public Object getUserOperationsCentres(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOperationsCentreRemoteService().findUserOperationsCentreRemote(userID);
	}

	/**
	 * Gets the operations centre by id.
	 *
	 * @param operationsCentreID the operations centre id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the operations centre by id
	 */
	@GET
	@Path("/oc/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public OperationsCentreDTO getOperationsCentreByID(
			@QueryParam("operationsCentreID") @NotNull(message="Operations Centre Id may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOperationsCentreRemoteService().findOperationCentreByIdRemote(operationsCentreID);
	}

	/**
	 * Gets the operations centre by title.
	 *
	 * @param operationsCentreTitle the operations centre title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the operations centre by title
	 */
	@GET
	@Path("/oc/findEocByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public OCEocDTO getEocByTitle(
			@QueryParam("operationsCentreTitle") @NotNull(message="Operations Centre Title may not be null") String operationsCentreTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOperationsCentreRemoteService().findEocByTitleRemote(operationsCentreTitle);
	}

	/**
	 * Gets the operations centre by title.
	 *
	 * @param operationsCentreTitle the operations centre title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the operations centre by title
	 */
	@GET
	@Path("/oc/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public OCMeocDTO getMeocByTitle(
			@QueryParam("operationsCentreTitle") @NotNull(message="Operations Centre Title may not be null") String operationsCentreTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findMeocByTitleRemote(operationsCentreTitle);
	}


	/**
	 * Gets the all operations centres.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the all operations centres
	 */
	@GET
	@Path("/oc/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getAllOperationsCentres(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return new ResultListDTO(this.getOperationsCentreRemoteService().findAllOperationsCentresRemote());
	}

	/**
	 * Gets the all operations centres by crisis c ontext.
	 *
	 * @param crisisContextID the crisis context id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the all operations centres by crisis c ontext
	 */
	@GET
	@Path("/oc/findAllByCrisisContext")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getAllOperationsCentresByCrisisContext(
			@QueryParam("crisisContextID") @NotNull(message="Crisis Context ID may not be null") Long crisisContextID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return new ResultListDTO(this.getOperationsCentreRemoteService().findAllOperationsCentresByCrisisContextRemote(crisisContextID));
	}

	/**
	 * Creates the operations centre.
	 *
	 * @param operationsCentreDTO the operations centre dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the operations centre dto
	 */
	@POST
	@Path("/oc/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Object createOperationsCentre(
			@NotNull(message="Operations Centre object may not be null") Object operationsCentreDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOperationsCentreRemoteService().createOperationsCentreRemote(operationsCentreDTO, userID);
	}

	/**
	 * Update EOC.
	 *
	 * @param operationsCentreDTO the EOC dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the EOC dto
	 */
	@PUT
	@Path("/oc/updateEOC")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public OCEocDTO updateEOC(
			@NotNull(message="EOC object may not be null") OCEocDTO operationsCentreDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return this.getOperationsCentreRemoteService().updateEOCRemote(operationsCentreDTO, userID);
	}
	
	/**
	 * Update MEOC.
	 *
	 * @param operationsCentreDTO the MEOC dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the MEOC dto
	 */
	@PUT
	@Path("/oc/updateEOC")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public OCMeocDTO updateMEOC(
			@NotNull(message="MEOC object may not be null") OCMeocDTO operationsCentreDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().updateMEOCRemote(operationsCentreDTO, userID);
	}

	/**
	 * Delete operations centre.
	 *
	 * @param operationsCentreID the operations centre id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/oc/delete")
	public Long deleteOperationsCentre(
			@QueryParam("operationsCentreID") @NotNull(message="Operations Centre ID may not be null") Long operationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getOperationsCentreRemoteService().deleteOperationsCentreRemote(operationsCentreID, userID);
		return operationsCentreID;
	}

	/**
	 * Gets the registered operations centre by id.
	 *
	 * @param registeredOperationsCentreID the registered operations centre id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered operations centre by id
	 */
	@GET
	@Path("/regoc/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredOperationsCentreDTO getRegisteredOperationsCentreByID(
			@QueryParam("registeredOperationsCentreID") @NotNull(message="Registered Operations Centre Id may not be null") Long registeredOperationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findRegisteredOperationCentreByIdRemote(registeredOperationsCentreID);
	}

	/**
	 * Gets the registered operations centre by title.
	 *
	 * @param registeredOperationsCentreTitle the registered operations centre title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered operations centre by title
	 */
	@GET
	@Path("/regoc/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredOperationsCentreDTO getRegisteredOperationsCentreByTitle(
			@QueryParam("registeredOperationsCentreTitle") @NotNull(message="Registered Operations Centre Title may not be null") String registeredOperationsCentreTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().findRegisteredOperationsCentreByTitleRemote(registeredOperationsCentreTitle);
	}

	/**
	 * Gets the all registered operations centres.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the all registered operations centres
	 */
	@GET
	@Path("/regoc/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getAllRegisteredOperationsCentres(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return new ResultListDTO(this.getOperationsCentreRemoteService().findAllRegisteredOperationsCentresRemote());
	}

	/**
	 * Creates the registered registered operations centre.
	 *
	 * @param registeredOperationsCentreDTO the registered operations centre dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered operations centre dto
	 */
	@POST
	@Path("/regoc/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredOperationsCentreDTO createRegisteredRegisteredOperationsCentre(
			@NotNull(message="Registered Operations Centre object may not be null") RegisteredOperationsCentreDTO registeredOperationsCentreDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().createRegisteredOperationsCentreRemote(registeredOperationsCentreDTO, userID);
	}

	/**
	 * Update registered operations centre.
	 *
	 * @param registeredOperationsCentreDTO the registered operations centre dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered operations centre dto
	 */
	@PUT
	@Path("/regoc/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredOperationsCentreDTO updateRegisteredOperationsCentre(
			@NotNull(message="Registered Operations Centre object may not be null") RegisteredOperationsCentreDTO registeredOperationsCentreDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getOperationsCentreRemoteService().updateRegisteredOperationsCentreRemote(registeredOperationsCentreDTO, userID);
	}

	/**
	 * Delete registered operations centre.
	 *
	 * @param registeredOperationsCentreID the registered operations centre id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/regoc/delete")
	public Long deleteRegisteredOperationsCentre(
			@QueryParam("registeredOperationsCEntreID") @NotNull(message="Registered Operations Centre ID may not be null") Long registeredOperationsCentreID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getOperationsCentreRemoteService().deleteRegisteredOperationsCentreRemote(registeredOperationsCentreID, userID);
		return registeredOperationsCentreID;
	}




}
