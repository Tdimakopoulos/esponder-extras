package eu.esponder.test.rest.cris.poi;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.crisis.alternative.OCEocALTDTO;
import eu.esponder.rest.client.ResteasyClient;

public class ImmersionTests {
	
	@Test
	public void run() {
		
		GetEocDTO("6", "31391e2d-d369-441f-9ada-e133374128ee");
		
	}

	
	public OCEocALTDTO GetEocDTO(String _userID, String _sessionID)
    {
           
           Map<String, String> params = new HashMap<String, String>();
           
           params.put("userID", String.valueOf(_userID));
           params.put("sessionID",String.valueOf(_sessionID)); 

           ResteasyClient getClient = null;
           try {
                  getClient = new ResteasyClient("http://rdocs.exodussa.com:8080/esponder-restful/crisis/view/2d/oc", "application/json");
           } catch (ClassNotFoundException e1) {
                  e1.printStackTrace();
           } catch (JAXBException e1) {
                  e1.printStackTrace();
           }
           
           String JsonMessage = null;
           try {
                  JsonMessage = getClient.get(params);
                  System.out.println(JsonMessage);
           } catch (RuntimeException e) {
                  e.printStackTrace();
           } catch (Exception e) {
                  e.printStackTrace();
           }
           
           ObjectMapper mapper = new ObjectMapper();
           OCEocALTDTO OC = null;
           
           if(JsonMessage!=null){
                  try {
                        OC = mapper.readValue(JsonMessage, OCEocALTDTO.class);
                  } catch (JsonParseException e) {
                        System.out.println("EsponderBackend JSON parse error");
                  } catch (JsonMappingException e) {
                        System.out.println("EsponderBackend JSON mapping error");
                        System.out.println(e.getMessage());
                  } catch (IOException e) {
                        System.out.println("EsponderBackend JSON error");
                        e.printStackTrace();
                  }
           }
           
    
           return OC;    
    }

	
	
}
