package eu.esponder.rest.generic;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.annotations.ClientResponseType;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.criteria.EsponderQueryRestrictionDTO;

@Path("/crisis/generic")
public interface CrisisEntityResourceService {
		
	@POST
	@Path("/create")
	@Consumes ({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public ESponderEntityDTO createEntity( 
			ESponderEntityDTO entityDTO,
			@QueryParam("userID") Long userID) throws Exception;
	
	
	@DELETE
	@Path("/delete")
	public void deleteEntity(
			@QueryParam("entityClass") String entityClass,
			@QueryParam("entityID") Long entityID,
			@QueryParam("userID") Long userID) throws Exception;
	
	
	@PUT
	@Path("/update")
	@Consumes ({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public ESponderEntityDTO updateEntity( 
			ESponderEntityDTO entityDTO,
			@QueryParam("userID") Long userID) throws Exception ;
	
	
	@GET
	@Path("/get")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public ResultListDTO getEntity (
			@QueryParam("criteriaDTO") String criteria,
			@QueryParam("queriedEntityDTO") String queriedEntityDTO,  
			@QueryParam("pageNumber") int pageNumber,
			@QueryParam("pageSize") int pageSize,
			@QueryParam("userID") Long userID) throws Exception;
	
	
	@POST
	@Path("/post")
	@Consumes ({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public ResultListDTO postEntity(
			EsponderQueryRestrictionDTO criteriaDTO,
			@QueryParam("queriedEntityDTO") String queriedEntityDTO,  
			@QueryParam("pageNumber") int pageNumber,
			@QueryParam("pageSize") int pageSize,
			@QueryParam("userID")  Long userID) throws Exception;
	
	/*
	 *  
	 */
	@GET
	@Path("/leoleis")
	@ClientResponseType(entityType = ResultListDTO.class)
	@Consumes ({MediaType.APPLICATION_XML/*, MediaType.APPLICATION_JSON*/})
	@Produces({MediaType.APPLICATION_XML/*, MediaType.APPLICATION_JSON*/})
	public ResultListDTO testRestEasyProxy(@QueryParam("userID") Long userID);
	
}