package eu.esponder.optimizerscenarios.first.objects;

public class OperationCenter {
	private String name;

	private int number;

	private String category;

	public OperationCenter() {

	}

	public OperationCenter(String name) {
		this.name = name;
	}

	public OperationCenter(String name, int number, String position) {
		this.name = name;
		this.number = number;

		this.category = position;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
