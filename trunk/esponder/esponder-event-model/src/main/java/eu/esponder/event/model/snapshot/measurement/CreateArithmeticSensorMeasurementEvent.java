/*
 * 
 */
package eu.esponder.event.model.snapshot.measurement;

import eu.esponder.dto.model.snapshot.sensor.measurement.ArithmeticSensorMeasurementDTO;
import eu.esponder.event.model.CreateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class CreateArithmeticSensorMeasurementEvent.
 */
public class CreateArithmeticSensorMeasurementEvent extends ArithmeticSensorMeasurementEvent<ArithmeticSensorMeasurementDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4057646930053868206L;

	/**
	 * Instantiates a new creates the arithmetic sensor measurement event.
	 */
	public CreateArithmeticSensorMeasurementEvent(){
		setJournalMessageInfo("CreateArithmeticSensorMeasurementEvent");
	}
}
