/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.event.model.CreateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActorEvent.
 */
public class CreateFRTeamEvent extends FRTeamEvent<FRTeamDTO> implements CreateEvent {

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 2037088567151808364L;

	/**
	 * Instantiates a new creates the actor event.
	 */
	public CreateFRTeamEvent(){
		setJournalMessageInfo("CreateFRTeamEvent");
	}
	
}
