/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.event.model.UpdateEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class UpdateOperationsCentreEvent.
 */
public class MoveOperationsCentreEvent extends
		OperationsCentreEvent<OperationsCentreDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9085599295942847029L;

	PointDTO pFinalPosition;

	public PointDTO getpFinalPosition() {
		return pFinalPosition;
	}

	public void setpFinalPosition(PointDTO pFinalPosition) {
		this.pFinalPosition = pFinalPosition;
	}

	/**
	 * Instantiates a new update operations centre event.
	 */
	public MoveOperationsCentreEvent() {
		setJournalMessageInfo("UpdateOperationsCentreEvent");
	}

}
