/*
 * 
 */
package eu.esponder.event.datafusion.message;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.event.model.UpdateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateActionPartEvent.
 */
public class ESponderDFConfirmActionPartEvent extends EsponderDFActionPartEvent<ActionPartDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 161564239638577602L;
	
	/**
	 * Instantiates a new update action part event.
	 */
	public ESponderDFConfirmActionPartEvent(){
		setJournalMessageInfo("UpdateActionPartEvent");
	}
}
