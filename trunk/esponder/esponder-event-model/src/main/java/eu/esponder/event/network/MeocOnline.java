/*
 * 
 */
package eu.esponder.event.network;

import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.ESponderEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionEvent.
 */
public class MeocOnline extends ESponderEvent<ESponderUserDTO> implements ESponderNetworkEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2006345559724305533L;

	private Long iNetInfoFlag;
	private String iNetInfoString;
	private String iNetInfoString2;
	private String iNetInfoString3;
		
	
	public Long getiNetInfoFlag() {
		return iNetInfoFlag;
	}


	public void setiNetInfoFlag(Long iNetInfoFlag) {
		this.iNetInfoFlag = iNetInfoFlag;
	}


	public String getiNetInfoString() {
		return iNetInfoString;
	}


	public void setiNetInfoString(String iNetInfoString) {
		this.iNetInfoString = iNetInfoString;
	}


	public String getiNetInfoString2() {
		return iNetInfoString2;
	}


	public void setiNetInfoString2(String iNetInfoString2) {
		this.iNetInfoString2 = iNetInfoString2;
	}


	public String getiNetInfoString3() {
		return iNetInfoString3;
	}


	public void setiNetInfoString3(String iNetInfoString3) {
		this.iNetInfoString3 = iNetInfoString3;
	}


	public MeocOnline(){
		setJournalMessageInfo("MEOC Online");
		setJournalMessage("MEOC Online");
	}
}
