/*
 * 
 */
package eu.esponder.event.model.datafusion;

import eu.esponder.dto.model.datafusion.DatafusionResultsDTO;
import eu.esponder.event.model.ESponderEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class ActionEvent.
 *
 * @param <T> the generic type
 */
public abstract class DataFusionEvent<T extends DatafusionResultsDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -345759696434734591L;
	
}
