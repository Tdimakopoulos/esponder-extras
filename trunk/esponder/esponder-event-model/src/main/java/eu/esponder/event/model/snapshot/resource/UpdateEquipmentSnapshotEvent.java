/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.event.model.UpdateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class UpdateEquipmentSnapshotEvent.
 */
public class UpdateEquipmentSnapshotEvent extends EquipmentSnapshotEvent<EquipmentSnapshotDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5506469989744092195L;
	
	/**
	 * Instantiates a new update equipment snapshot event.
	 */
	public UpdateEquipmentSnapshotEvent(){
		setJournalMessageInfo("UpdateEquipmentSnapshotEvent");
	}
}
