/*
 * 
 */
package eu.esponder.event.datafusion.crud;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.event.model.UpdateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateActionEvent.
 */
public class ESponderDFUpdateActionCRUDEvent extends ESponderDFActionCRUDEvent<ActionDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1950437603072013048L;
	
	/**
	 * Instantiates a new update action event.
	 */
	public ESponderDFUpdateActionCRUDEvent(){
		setJournalMessageInfo("Update Action Event");
	}	
}
