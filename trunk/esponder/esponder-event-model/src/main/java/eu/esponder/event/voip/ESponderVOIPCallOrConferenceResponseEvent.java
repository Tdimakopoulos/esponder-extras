/*
 * 
 */
package eu.esponder.event.voip;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.event.datafusion.model.QueryResponseEvent;
import eu.esponder.event.model.ESponderEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionEvent.
 */
public class ESponderVOIPCallOrConferenceResponseEvent extends ESponderEvent<CrisisContextDTO> implements QueryResponseEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2006035559724305579L;

	private Long VOIPTypeID;
	private Long VOIPRequestID;
	private ResultListDTO pResponse;
	private boolean callConferecemade;
	private boolean callConfereceerror;
	private String callConfereceerrormsg;
	


	public Long getVOIPTypeID() {
		return VOIPTypeID;
	}



	public void setVOIPTypeID(Long vOIPTypeID) {
		VOIPTypeID = vOIPTypeID;
	}



	public Long getVOIPRequestID() {
		return VOIPRequestID;
	}



	public void setVOIPRequestID(Long vOIPRequestID) {
		VOIPRequestID = vOIPRequestID;
	}



	public ResultListDTO getpResponse() {
		return pResponse;
	}



	public void setpResponse(ResultListDTO pResponse) {
		this.pResponse = pResponse;
	}



	public boolean isCallConferecemade() {
		return callConferecemade;
	}



	public void setCallConferecemade(boolean callConferecemade) {
		this.callConferecemade = callConferecemade;
	}


	public boolean isCallConfereceerror() {
		return callConfereceerror;
	}



	public void setCallConfereceerror(boolean callConfereceerror) {
		this.callConfereceerror = callConfereceerror;
	}



	public String getCallConfereceerrormsg() {
		return callConfereceerrormsg;
	}



	public void setCallConfereceerrormsg(String callConfereceerrormsg) {
		this.callConfereceerrormsg = callConfereceerrormsg;
	}



	/**
	 * Instantiates a new creates the action event.
	 */
	public ESponderVOIPCallOrConferenceResponseEvent(){
		setJournalMessageInfo("ESponder DF Query Response Event");
	}
}
