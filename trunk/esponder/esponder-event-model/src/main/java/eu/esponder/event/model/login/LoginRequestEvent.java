package eu.esponder.event.model.login;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.crisis.resource.EsponderUserEvent;

public class LoginRequestEvent extends EsponderUserEvent<ESponderUserDTO> implements LoginEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5408128404989456038L;
	
	private String username;
	private String password;
	private String pkikey;
	
	String IMEI;
	
	String myip;

	int iLoginType; //0 login - 1 PKI
	
	public String getMyip() {
		return myip;
	}

	public void setMyip(String myip) {
		this.myip = myip;
	}

	
	public int getiLoginType() {
		return iLoginType;
	}

	public void setiLoginType(int iLoginType) {
		this.iLoginType = iLoginType;
	}

	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String iMEI) {
		IMEI = iMEI;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPkikey() {
		return pkikey;
	}

	public void setPkikey(String pkikey) {
		this.pkikey = pkikey;
	}

	/**
	 * Instantiates a new login esponder user event.
	 */
	public LoginRequestEvent(){
		setJournalMessageInfo("Request Login Esponder User Event");
	}

}