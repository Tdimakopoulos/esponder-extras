/*
 * 
 */
package eu.esponder.event.datafusion.query;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.alternative.OCEocALTDTO;
import eu.esponder.event.datafusion.model.QueryResponseEvent;
import eu.esponder.event.model.ESponderEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionEvent.
 */
public class ESponderDFQueryResponseEvent extends ESponderEvent<CrisisContextDTO> implements QueryResponseEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2006035559724305579L;

	private Long QueryID;
	private Long RequestID;
	private ResultListDTO pResponse;
	private OCEocALTDTO MapPositions;
	
String IMEI;
	
public String getIMEI() {
	return IMEI;
}

public void setIMEI(String iMEI) {
	IMEI = iMEI;
}

	public OCEocALTDTO getMapPositions() {
		return MapPositions;
	}


	public void setMapPositions(OCEocALTDTO mapPositions) {
		MapPositions = mapPositions;
	}


	public Long getQueryID() {
		return QueryID;
	}


	public void setQueryID(Long queryID) {
		QueryID = queryID;
	}


	public Long getRequestID() {
		return RequestID;
	}


	public void setRequestID(Long requestID) {
		RequestID = requestID;
	}


	public ResultListDTO getpResponse() {
		return pResponse;
	}


	public void setpResponse(ResultListDTO pResponse) {
		this.pResponse = pResponse;
	}


	/**
	 * Instantiates a new creates the action event.
	 */
	public ESponderDFQueryResponseEvent(){
		setJournalMessageInfo("ESponder DF Query Response Event");
	}
}
