package eu.esponder.event.mobile;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.ESponderEvent;

public class MobileStatusEvent extends ESponderEvent<ESponderUserDTO> {
	private static final long serialVersionUID = -5049321795600339854L;

	private String wimaxrssi;
	private String wimaxping;
	private String s3grssi;
	private String s3gping;
	private String wifirssi;
	private String wifiping;
	private String battery;
	String bluetooth;
	boolean wimaxstatus;
	boolean wifistatus;
	boolean s3gstatus;
	boolean bluetoothstatus;
	
	private String imei;
	

	public String getBluetooth() {
		return bluetooth;
	}

	public void setBluetooth(String bluetooth) {
		this.bluetooth = bluetooth;
	}

	public boolean isWimaxstatus() {
		return wimaxstatus;
	}

	public void setWimaxstatus(boolean wimaxstatus) {
		this.wimaxstatus = wimaxstatus;
	}

	public boolean isWifistatus() {
		return wifistatus;
	}

	public void setWifistatus(boolean wifistatus) {
		this.wifistatus = wifistatus;
	}

	public boolean isS3gstatus() {
		return s3gstatus;
	}

	public void setS3gstatus(boolean s3gstatus) {
		this.s3gstatus = s3gstatus;
	}

	public boolean isBluetoothstatus() {
		return bluetoothstatus;
	}

	public void setBluetoothstatus(boolean bluetoothstatus) {
		this.bluetoothstatus = bluetoothstatus;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	
	public String getWimaxrssi() {
		return wimaxrssi;
	}

	public void setWimaxrssi(String wimaxrssi) {
		this.wimaxrssi = wimaxrssi;
	}

	public String getWimaxping() {
		return wimaxping;
	}

	public void setWimaxping(String wimaxping) {
		this.wimaxping = wimaxping;
	}

	public String getS3grssi() {
		return s3grssi;
	}

	public void setS3grssi(String s3grssi) {
		this.s3grssi = s3grssi;
	}

	public String getS3gping() {
		return s3gping;
	}

	public void setS3gping(String s3gping) {
		this.s3gping = s3gping;
	}

	public String getWifirssi() {
		return wifirssi;
	}

	public void setWifirssi(String wifirssi) {
		this.wifirssi = wifirssi;
	}

	public String getWifiping() {
		return wifiping;
	}

	public void setWifiping(String wifiping) {
		this.wifiping = wifiping;
	}

	public String getBattery() {
		return battery;
	}

	public void setBattery(String battery) {
		this.battery = battery;
	}

}
