/*
 * 
 */
package eu.esponder.event.voip;

import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.ESponderEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionEvent.
 */
public class ESponderVOIPConferenceEvent extends ESponderEvent<ESponderUserDTO>
		implements VOIPRequestEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2006035559724305579L;

	private Long VOIPTypeID;

	private QueryParamsDTO params;

	public enum ESponderVOIPEventType {

		CallEstablished,

		CallDropped,

		CallingError,

		CallWaiting,

		Error
	}

	ESponderVOIPEventType VOIPEventType;

	public ESponderVOIPEventType getVOIPEventType() {
		return VOIPEventType;
	}

	public void setVOIPEventType(ESponderVOIPEventType vOIPEventType) {
		VOIPEventType = vOIPEventType;
	}

	public Long getVOIPTypeID() {
		return VOIPTypeID;
	}

	public void setVOIPTypeID(Long vOIPTypeID) {
		VOIPTypeID = vOIPTypeID;
	}

	public QueryParamsDTO getParams() {
		return params;
	}

	public void setParams(QueryParamsDTO params) {
		this.params = params;
	}

	/**
	 * Instantiates a new creates the action event.
	 */
	public ESponderVOIPConferenceEvent() {
		setJournalMessageInfo("ESponder DF Query Request Event");
		setJournalMessage("ESponder DF Query Request Event");
	}
}
