/*
 * 
 */
package eu.esponder.event.datafusion.message;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.event.model.CreateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionEvent.
 */
public class ESponderDFCreateActionEvent extends ESponderDFActionMessageEvent<ActionDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2006035559724305579L;

	/**
	 * Instantiates a new creates the action event.
	 */
	public ESponderDFCreateActionEvent(){
		setJournalMessageInfo("Create Action Event");
	}
}
