/*
 * 
 */
package eu.esponder.event.model.login;

import eu.esponder.event.model.ESponderOperationEvent;

/**
 * The Interface LogoutEvent.
 */
public abstract interface  LogoutEvent  extends ESponderOperationEvent {

}
