/*
 * 
 */
package eu.esponder.event.model.snapshot.measurement;

import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.event.model.CreateEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateLocationSensorMeasurementEvent.
 */
public class CreateLocationSensorMeasurementEvent extends LocationSensorMeasurementEvent<LocationSensorMeasurementDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4057646930053868206L;

	/**
	 * Instantiates a new creates the location sensor measurement event.
	 */
	public CreateLocationSensorMeasurementEvent(){
		setJournalMessageInfo("CreateLocationSensorMeasurementEvent");
	}
}
