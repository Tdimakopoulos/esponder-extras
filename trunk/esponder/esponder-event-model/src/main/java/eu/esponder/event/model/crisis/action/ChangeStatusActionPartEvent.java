/*
 * 
 */
package eu.esponder.event.model.crisis.action;





import eu.esponder.dto.model.crisis.action.ActionPartDTO;

import eu.esponder.event.model.CreateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionEvent.
 */
public class ChangeStatusActionPartEvent extends ActionPartEvent<ActionPartDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2006035559724305579L;

	
	/**
	 * Instantiates a new creates the action event.
	 */
	public ChangeStatusActionPartEvent(){
		setJournalMessageInfo("Create Action Event");
	}
}
