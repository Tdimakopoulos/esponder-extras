/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class OperationsCentreEvent.
 * 
 * @param <T>
 *            the generic type
 */
public abstract class OperationsCentreEvent<T extends OperationsCentreDTO>
		extends ResourceEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4415380713125894915L;

	/**
	 * The Enum ESponderOCType.
	 */
	public enum ESponderOCType {

		/** The Operation center. */
		OperationCenter,
		/** The OC meoc. */
		OCMeoc,
		/** The OC eoc. */
		OCEoc
	}

	/** The Operation center type. */
	ESponderOCType OperationCenterType;
	
	OCMeocDTO pMeoc;
	

	public OCMeocDTO getpMeoc() {
		return pMeoc;
	}

	public void setpMeoc(OCMeocDTO pMeoc) {
		this.pMeoc = pMeoc;
	}

	/**
	 * Gets the operation center type.
	 * 
	 * @return the operation center type
	 */
	public ESponderOCType getOperationCenterType() {
		return OperationCenterType;
	}

	/**
	 * Sets the operation center type.
	 * 
	 * @param operationCenterType
	 *            the new operation center type
	 */
	public void setOperationCenterType(ESponderOCType operationCenterType) {
		OperationCenterType = operationCenterType;
	}

}
