/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.event.model.UpdateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class UpdateActorSnapshotEvent.
 */
public class UpdateActorSnapshotEvent extends ActorSnapshotEvent<ActorSnapshotDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4454191896380135838L;
	
	/**
	 * Instantiates a new update actor snapshot event.
	 */
	public UpdateActorSnapshotEvent(){
		setJournalMessageInfo("UpdateActorSnapshotEvent");
	}
}
