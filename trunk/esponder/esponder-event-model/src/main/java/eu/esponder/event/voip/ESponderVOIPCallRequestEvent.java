/*
 * 
 */
package eu.esponder.event.voip;

import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.ESponderEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionEvent.
 */
public class ESponderVOIPCallRequestEvent extends ESponderEvent<ESponderUserDTO> implements VOIPRequestEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2006035559724305579L;

	private Long VOIPTypeID;
	private Long VOIPRequestID;
	private QueryParamsDTO params;
	
		
	public enum ESponderVOIPActionType {

		Establish,

		Drop,

		

		CallWaiting
	}

	ESponderVOIPActionType VOIPActionType;
	
	public ESponderVOIPActionType getVOIPActionType() {
		return VOIPActionType;
	}

	public void setVOIPActionType(ESponderVOIPActionType vOIPActionType) {
		VOIPActionType = vOIPActionType;
	}

	public Long getVOIPTypeID() {
		return VOIPTypeID;
	}

	public void setVOIPTypeID(Long vOIPTypeID) {
		VOIPTypeID = vOIPTypeID;
	}

	public Long getVOIPRequestID() {
		return VOIPRequestID;
	}

	public void setVOIPRequestID(Long vOIPRequestID) {
		VOIPRequestID = vOIPRequestID;
	}

	public QueryParamsDTO getParams() {
		return params;
	}

	public void setParams(QueryParamsDTO params) {
		this.params = params;
	}

	/**
	 * Instantiates a new creates the action event.
	 */
	public ESponderVOIPCallRequestEvent(){
		setJournalMessageInfo("ESponder DF Query Request Event");
		setJournalMessage("ESponder DF Query Request Event");
	}
}
