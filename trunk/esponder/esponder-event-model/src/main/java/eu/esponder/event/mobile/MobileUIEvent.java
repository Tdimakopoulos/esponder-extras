package eu.esponder.event.mobile;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.ESponderEvent;

public class MobileUIEvent extends ESponderEvent<ESponderUserDTO> {

	private static final long serialVersionUID = -8943247386151947279L;
	private boolean button1;
	private boolean button2;
	private boolean button3;
	private boolean alertindicator;
	private String battery;
	private String valumelevel;

	// to be set by voip apk
	String mobileimei;

	public String getMobileimei() {
		return mobileimei;
	}

	public void setMobileimei(String mobileimei) {
		this.mobileimei = mobileimei;
	}

	public boolean isButton1() {
		return button1;
	}

	public void setButton1(boolean button1) {
		this.button1 = button1;
	}

	public boolean isButton2() {
		return button2;
	}

	public void setButton2(boolean button2) {
		this.button2 = button2;
	}

	public boolean isButton3() {
		return button3;
	}

	public void setButton3(boolean button3) {
		this.button3 = button3;
	}

	public boolean isAlertindicator() {
		return alertindicator;
	}

	public void setAlertindicator(boolean alertindicator) {
		this.alertindicator = alertindicator;
	}

	public String getBattery() {
		return battery;
	}

	public void setBattery(String battery) {
		this.battery = battery;
	}

	public String getValumelevel() {
		return valumelevel;
	}

	public void setValumelevel(String valumelevel) {
		this.valumelevel = valumelevel;
	}

}
