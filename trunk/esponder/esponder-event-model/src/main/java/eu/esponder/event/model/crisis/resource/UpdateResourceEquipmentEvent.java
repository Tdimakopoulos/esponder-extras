/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.event.model.UpdateEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class UpdateResourceEquipmentEvent.
 */
public class UpdateResourceEquipmentEvent extends ResourceEquipmentEvent<EquipmentDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5017996688146629527L;
	
	/**
	 * Instantiates a new update resource equipment event.
	 */
	public UpdateResourceEquipmentEvent(){
		setJournalMessageInfo("UpdateResourceEquipmentEvent");
	}
}
