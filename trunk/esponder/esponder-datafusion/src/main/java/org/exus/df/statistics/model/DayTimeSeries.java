package org.exus.df.statistics.model;

public class DayTimeSeries {

	Long dTimeStart;
	Long dTimeEnd;
	public Long getdTimeStart() {
		return dTimeStart;
	}
	public void setdTimeStart(Long dTimeStart) {
		this.dTimeStart = dTimeStart;
	}
	public Long getdTimeEnd() {
		return dTimeEnd;
	}
	public void setdTimeEnd(Long dTimeEnd) {
		this.dTimeEnd = dTimeEnd;
	}
	
	
}
