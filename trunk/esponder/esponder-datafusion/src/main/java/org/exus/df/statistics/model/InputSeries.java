package org.exus.df.statistics.model;

import java.util.Date;

public class InputSeries {

	//Date in date format
	Date pDate;
	
	//Date in long format
	Long lDate;
	
	//Value in double
	Double dValue;
	
	public Date getpDate() {
		return pDate;
	}
	public void setpDate(Date pDate) {
		this.pDate = pDate;
	}
	public Long getlDate() {
		return lDate;
	}
	public void setlDate(Long lDate) {
		this.lDate = lDate;
	}
	public Double getdValue() {
		return dValue;
	}
	public void setdValue(Double dValue) {
		this.dValue = dValue;
	}
	
}
