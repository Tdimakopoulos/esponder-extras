/*
 * 
 */
package eu.esponder.df.ruleengine.utilities.ruleresults;

import java.io.Serializable;
import java.util.Date;

import eu.esponder.controller.datafusion.DatafusionRemoteService;
import eu.esponder.df.ruleengine.utilities.RuleLookup;
import eu.esponder.df.ruleengine.utilities.RuleLookup.RuleLookupType;
import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;
import eu.esponder.dto.model.datafusion.DatafusionResultsDTO;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.test.ResourceLocator;


// TODO: Auto-generated Javadoc
/**
 * The Class RuleResults.
 */
public class RuleResults implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5287233281894129154L;

	/** The DF remote service. */
	DatafusionRemoteService DFRemoteService = ResourceLocator
			.lookup("esponder/DatafusionBean/remote");
	
	/**
	 * Rule result.
	 *
	 * @param szRuleName the sz rule name
	 * @param szResults1 the sz results1
	 * @param szResults2 the sz results2
	 * @param szResults3 the sz results3
	 */
	public void RuleResult(String szRuleName,String szResults1,String szResults2,String szResults3)
	{

		// call the bean
		
		RuleLookup pLookup= new RuleLookup();
		pLookup.Initialize2();
		try {
			pLookup.ReadXMLFile(RuleLookupType.DLU_RULERESULTS);
		} catch (Exception e) {
			System.out.println("WARN : Rule Results Error (Read XML File) : "+e.getMessage());
			System.out.println("WARN : No results on the XML files, initialize the file");
			
			pLookup= new RuleLookup();
			pLookup.Initialize2();
		}
		
		RuleResultsXML pNewResults= new RuleResultsXML();
		pNewResults.setSzRuleName(szRuleName);
		pNewResults.setSzResultsText1(szResults1);
		pNewResults.setSzResultsText2(szResults2);
		pNewResults.setSzResultsText3(szResults3);
		pNewResults.setSzResultsText4((new Date()).toString());
		pLookup.AddRuleResults(pNewResults);
		
		Long userID=new Long(1);
		
		DatafusionResultsDTO DFResults=new DatafusionResultsDTO();
		DFResults.setRulename(szRuleName);
		DFResults.setResultsText1(szResults1);
		DFResults.setResultsText2(szResults2);
		DFResults.setResultsText3(szResults3);
		DFResults.setDFDate((new Date()).getTime());
		DFRemoteService.createDFResultsRemote(DFResults, userID);
		
		try {
			pLookup.WriteXMLFile(RuleLookupType.DLU_RULERESULTS);
		} catch (Exception e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			System.out.println("Rule Results Error (Update XML File) : "+e.getMessage());
		}
	}
}
