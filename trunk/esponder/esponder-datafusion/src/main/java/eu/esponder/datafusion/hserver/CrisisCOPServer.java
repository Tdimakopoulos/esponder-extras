/*
 * 
 */
package eu.esponder.datafusion.hserver;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.naming.NamingException;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.LogisticsRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.events.EventsEntityRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.dto.model.crisis.alternative.ActorCMALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorFRCALTDTO;
import eu.esponder.dto.model.crisis.alternative.ActorICALTDTO;
import eu.esponder.dto.model.crisis.alternative.FRTeamALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCEocALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO;
import eu.esponder.dto.model.crisis.resource.ActorCMDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamSnapshotDTO;
import eu.esponder.dto.model.crisis.resource.OCEocDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class YMAP2DView.
 */

public class CrisisCOPServer {



	/**
	 * Find2 d details for oc.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the oC eoc altdto
	 */
	
	public OCEocALTDTO pEOC;
	
	public OCEocALTDTO find2DDetailsForOC(
	Long userID) {

		OCEocALTDTO altEoc = new OCEocALTDTO();
		OCEocDTO eoc = new OCEocDTO();

		ESponderUserDTO esponderUser = (ESponderUserDTO) this.getActorRemoteService().findESponderUserByIdRemote(userID);
		if(esponderUser != null) {
			if(esponderUser.getOperationsCentre() != null) {
				
				Long ocID = esponderUser.getOperationsCentre().getId();
				System.out.println(" *** OC ID :"+ocID);
				String ocClass = this.getOperationsCentreRemoteService().findOperationsCentreClassByIdRemote(ocID);
				System.out.println(" *** OC Class :"+ocClass);
				
				if(ocClass != null || !ocClass.isEmpty()) {

					if(ocClass.equalsIgnoreCase("OCEocDTO")) {
						//System.out.println("YMAP2DView.class, It's an EOC");
						try {
							eoc = (OCEocDTO) getOperationsCentreRemoteService().findOCByIdRemote(ocID);
							System.out.println(" *** EOC title :"+eoc.getTitle());
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
							return null;
						}
					}
					else if(ocClass.equalsIgnoreCase("OCMeocDTO")) {
						//System.out.println("YMAP2DView.class, It's a MEOC");
						Long eocID = this.getOperationsCentreRemoteService().findEocIDBYMeocIDRemote(ocID);
						if(eocID != null)
							try {
								eoc = (OCEocDTO) getOperationsCentreRemoteService().findOCByIdRemote(eocID);
							} catch (ClassNotFoundException e) {
								e.printStackTrace();
								return null;
							}
					}
					else
						System.out.println("YMAP2DView.class, It's unknown");

					if(eoc != null) {
						//System.out.println("YMAP2DView.class, EOC has been FOUND");

						altEoc = copyEocToEocAlt(eoc);
						this.pEOC = altEoc;
						return altEoc;
					}

				}
				else
					return null;
			}
			else
				return null;
		}
		else
			return null;

		

		return null;
	}



	//----------------------------------------------------------------------------------------
	//Method to initially copy OCEocDTO to OCEocALTDTO object
	/**
	 * Copy eoc to eoc alt.
	 *
	 * @param eoc the eoc
	 * @return the oC eoc altdto
	 */
	private OCEocALTDTO copyEocToEocAlt(OCEocDTO eoc) {
		OCEocALTDTO altEoc = new OCEocALTDTO();
		altEoc.setEocCrisisContext(eoc.getEocCrisisContext().getId());
		System.out.println(" **************** CM ID : "+eoc.getCrisisManager());
		ActorCMDTO crisisManager = this.getActorRemoteService().findCrisisManagerByIdRemote(eoc.getCrisisManager());
		if(crisisManager != null)
			altEoc.setCrisisManager(convertCMToCMALT(crisisManager));
		altEoc.setId(eoc.getId());
		altEoc.setSubordinateMEOCs(copyMeocSetToMeocAltSet(eoc.getSubordinateMEOCs()));
		altEoc.setTitle(eoc.getTitle());
		altEoc.setUsers(eoc.getUsers());
		altEoc.setVoIPURL(eoc.getVoIPURL());
		altEoc.setLocationArea(eoc.getLocationArea());
		return altEoc;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy meoc set to meoc alt set.
	 *
	 * @param meocSet the meoc set
	 * @return the sets the
	 */
	private Set<OCMeocALTDTO> copyMeocSetToMeocAltSet(Set<Long> meocSet) {

		Set<OCMeocALTDTO> meocAltSet = new HashSet<OCMeocALTDTO>();
		try{
		for(Long meocID : meocSet) {
			OCMeocDTO meocDTO = this.getOperationsCentreRemoteService().findMeocByIdRemote(meocID);
			if(meocDTO != null)
				meocAltSet.add(copyMeocToMeocAlt(meocDTO));
		}
		}
		catch(Exception ex)
		{
			System.out.println("Error 1 COPServer-> "+ex.getMessage());
		}
		return meocAltSet;

	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy meoc to meoc alt.
	 *
	 * @param meoc the meoc
	 * @return the oC meoc altdto
	 */
	private OCMeocALTDTO copyMeocToMeocAlt(OCMeocDTO meoc) {

		OCMeocALTDTO altMeoc = new OCMeocALTDTO();
		altMeoc.setId(meoc.getId());
		ActorICDTO incidentCommander = this.getActorRemoteService().findIncidentCommanderByIdRemote(meoc.getIncidentCommander());
		if(incidentCommander != null)
			altMeoc.setIncidentCommander(copyICToICAlt(incidentCommander));
		altMeoc.setMeocCrisisContext(meoc.getMeocCrisisContext());
//		if(meoc.getSnapshots()!= null ) {
//			if(!meoc.getSnapshots().isEmpty() || meoc.getSnapshots().size() == 0) {
//				Set<OperationsCentreSnapshotDTO> snapshots = new HashSet<OperationsCentreSnapshotDTO>();
//				for(Long snaphotID : meoc.getSnapshots()) {
//					snapshots.add(this.getOperationsCentreRemoteService().findOperationsCentreSnapshotByIdRemote(snaphotID));
//				}
//				altMeoc.setSnapshots(snapshots);
//			}
//		}
		
		OperationsCentreSnapshotDTO latestMeocSnashot = this.getOperationsCentreRemoteService().findOperationsCentreSnapshotByDateRemote(meoc.getId(), new Date(new java.util.Date().getTime()));
		if(latestMeocSnashot != null)
			altMeoc.setSnapshot(latestMeocSnashot);
		altMeoc.setSupervisingOC(meoc.getSupervisingOC());
		altMeoc.setTitle(meoc.getTitle());
		altMeoc.setUsers(meoc.getUsers());
		altMeoc.setVoIPURL(meoc.getVoIPURL());
		return altMeoc;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Convert cm to cmalt.
	 *
	 * @param actor the actor
	 * @return the actor cmaltdto
	 */
	private ActorCMALTDTO convertCMToCMALT(ActorCMDTO actor) {
		ActorCMALTDTO altCM = new ActorCMALTDTO();
		altCM.setId(actor.getId());
		altCM.setOperationsCentre(actor.getOperationsCentre());
		altCM.setPersonnel(actor.getPersonnel().getId());
		altCM.setSubordinates(copyICSetToICAltSet(actor.getSubordinates()));
		altCM.setTitle(actor.getTitle());
		altCM.setVoIPURL(actor.getVoIPURL());
		return null;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy ic set to ic alt set.
	 *
	 * @param icSet the ic set
	 * @return the sets the
	 */
	private Set<ActorICALTDTO> copyICSetToICAltSet(Set<Long> icSet) {

		Set<ActorICALTDTO> altICSet = new HashSet<ActorICALTDTO>();
	try{
		for(Long icID : icSet) {
			ActorICDTO actorICDTO = this.getActorRemoteService().findIncidentCommanderByIdRemote(icID);
			if(actorICDTO != null)
				altICSet.add(copyICToICAlt(actorICDTO));
		}
	}
	catch(Exception ex)
	{
		System.out.println("Error -> 2"+ex.getMessage());
	}
		return altICSet;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy ic to ic alt.
	 *
	 * @param ic the ic
	 * @return the actor icaltdto
	 */
	private ActorICALTDTO copyICToICAlt(ActorICDTO ic) {

		ActorICALTDTO altIC = new ActorICALTDTO();
		altIC.setId(ic.getId());
		altIC.setCrisisManager(ic.getCrisisManager());
		altIC.setOperationsCentre(ic.getOperationsCentre());
		altIC.setFrTeams(copyFRTeamSetToFRTeamAltSet(ic.getFrTeams()));
		altIC.setPersonnel(ic.getCrisisManager());
		altIC.setTitle(ic.getTitle());
		altIC.setVoIPURL(ic.getVoIPURL());
		return altIC;
	}


	//----------------------------------------------------------------------------------------

	/**
	 * Copy fr team set to fr team alt set.
	 *
	 * @param teamSet the team set
	 * @return the sets the
	 */
	private Set<FRTeamALTDTO> copyFRTeamSetToFRTeamAltSet(Set<FRTeamDTO> teamSet) {

		Set<FRTeamALTDTO> altTeamSet = new HashSet<FRTeamALTDTO>();
	try{
		for(FRTeamDTO team : teamSet) {
			altTeamSet.add(copyFRTeamToFRTeamAlt(team));
		}
	}
	catch(Exception ex)
	{
		System.out.println("Error 3-> "+ex.getMessage());
	}
		return altTeamSet;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy fr team to fr team alt.
	 *
	 * @param team the team
	 * @return the fR team altdto
	 */
	private FRTeamALTDTO copyFRTeamToFRTeamAlt(FRTeamDTO team) {

		FRTeamALTDTO altTeam = new FRTeamALTDTO();

		altTeam.setId(team.getId());
		altTeam.setIncidentCommander(team.getIncidentCommander());
		altTeam.setSnapshots(convertSetOfTeamSnapshotsToSetOfLongs(team.getSnapshots()));
		ActorFRCDTO chief = this.getActorRemoteService().findFRChiefByIdRemote(team.getFrchief());
		if(chief != null)
			altTeam.setFrchief(copyFRCToFRCAlt(chief));
		return altTeam ;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Convert set of team snapshots to set of longs.
	 *
	 * @param snapshots the snapshots
	 * @return the sets the
	 */
	private Set<Long> convertSetOfTeamSnapshotsToSetOfLongs(Set<FRTeamSnapshotDTO> snapshots) {

		Set<Long> snapshotIDs = new HashSet<Long>();
	try{
		for(FRTeamSnapshotDTO snapshot : snapshots)
			snapshotIDs.add(snapshot.getId());
	}
	catch(Exception ex)
	{
		System.out.println("Error 4-> "+ex.getMessage());
	}
		return snapshotIDs;
	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy frc to frc alt.
	 *
	 * @param chief the chief
	 * @return the actor frcaltdto
	 */
	private ActorFRCALTDTO copyFRCToFRCAlt(ActorFRCDTO chief) {

		ActorFRCALTDTO chiefAlt = new ActorFRCALTDTO();
		chiefAlt.setId(chief.getId());
		chiefAlt.setEquipmentSet(convertEquipmentSetToLong(chief.getEquipmentSet()));
		chiefAlt.setPersonnel(chief.getPersonnel().getId());
		//System.out.println("Use query values "+chief.getId()+ ""+new java.sql.Date(new java.util.Date().getTime()));
		ActorSnapshotDTO latestActorSnapshot = this.getActorRemoteService().findActorSnapshotByDateRemote(chief.getId(), new java.sql.Date(new java.util.Date().getTime()));
		if(latestActorSnapshot != null)
			chiefAlt.setSnapshot(latestActorSnapshot);
//		chiefAlt.setSnapshots(convertFRCSnapshotSetToLongSet(chief.getSnapshots()));
		chiefAlt.setSubordinates(convertSetOfLongToSetOfFRs(chief.getSubordinates()));
		chiefAlt.setTitle(chief.getTitle());
		chiefAlt.setVoIPURL(chief.getVoIPURL());
		return chiefAlt;
	}

	/**
	 * Convert equipment set to long.
	 *
	 * @param equipmentSet the equipment set
	 * @return the sets the
	 */
	private Set<Long> convertEquipmentSetToLong(Set<EquipmentDTO> equipmentSet) {

		Set<Long> longEquipment = new HashSet<Long>();
	try{
		for(EquipmentDTO equipment : equipmentSet)
			longEquipment.add(equipment.getId());
	}
	catch(Exception ex)
	{
		System.out.println("Error 5-> "+ex.getMessage());
	}
		return longEquipment;
	}

	/**
	 * Convert frc snapshot set to long set.
	 *
	 * @param snapshotSet the snapshot set
	 * @return the sets the
	 */
	private Set<Long> convertFRCSnapshotSetToLongSet(Set<ActorSnapshotDTO> snapshotSet) {

		Set<Long> longSnapshots = new HashSet<Long>();
	try{
		for(ActorSnapshotDTO snapshot : snapshotSet)
			longSnapshots.add(snapshot.getId());
	}
	catch(Exception ex)
	{
		System.out.println("Error 6-> "+ex.getMessage());
	}
		return longSnapshots;
	}

	/**
	 * Convert set of long to set of f rs.
	 *
	 * @param frs the frs
	 * @return the sets the
	 */
	private Set<ActorFRALTDTO> convertSetOfLongToSetOfFRs(Set<Long> frs) {
	try{
		if(frs != null) {
			ActorFRDTO actor = null;
			Set<ActorFRALTDTO> altSet = new HashSet<ActorFRALTDTO>();
			for(Long frID : frs) {
				actor = this.getActorRemoteService().findFRByIdRemote(frID);
				if(actor != null)
					altSet.add(copyFRToFRAlt(actor));
			}

			return altSet;
		}
		else
			return null;
	}
	catch(Exception ex)
	{
		System.out.println("Error 7-> "+ex.getMessage());
	return null;
	}

	}

	//----------------------------------------------------------------------------------------

	/**
	 * Copy fr to fr alt.
	 *
	 * @param actorFR the actor fr
	 * @return the actor fraltdto
	 */
	private ActorFRALTDTO copyFRToFRAlt(ActorFRDTO actorFR) {

		ActorFRALTDTO frAlt = new ActorFRALTDTO();
		frAlt.setEquipmentSet(convertEquipmentSetToLong(actorFR.getEquipmentSet()));
		frAlt.setFrchief(actorFR.getFRChief());
		frAlt.setId(actorFR.getId());
		frAlt.setPersonnel(actorFR.getPersonnel().getId());
		ActorSnapshotDTO latestActorSnapshot = this.getActorRemoteService().findActorSnapshotByDateRemote(actorFR.getId(), new java.sql.Date(new java.util.Date().getTime()));
		if(latestActorSnapshot != null)
			frAlt.setSnapshot(latestActorSnapshot);
//		frAlt.setSnapshots(convertFRCSnapshotSetToLongSet(actorFR.getSnapshots()));
		frAlt.setTitle(actorFR.getTitle());
		frAlt.setVoIPURL(actorFR.getVoIPURL());
		return frAlt ;
	}

	//----------------------------------------------------------------------------------------


	/**
	 * Security check.
	 *
	 * @param userID the user id
	 * @param FunctionID the function id
	 * @return the long
	 */
	protected boolean SecurityCheck(String userID,int FunctionID)
	{
		//return new Long(userID);
		return true;
	}
	
	/**
	 * Security get user id.
	 *
	 * @param userID the user id
	 * @return the long
	 */
	protected Long SecurityGetUserID(String userID)
	{
		//return new Long(userID);
		return new Long(1);
	}
	
	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the sensor remote service.
	 *
	 * @return the sensor remote service
	 */
	protected SensorRemoteService getSensorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/SensorBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}		
		
	//*******************************************************************************

	/**
	 * Gets the actor remote service.
	 *
	 * @return the actor remote service
	 */
	protected ActorRemoteService getActorRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the equipment remote service.
	 *
	 * @return the equipment remote service
	 */
	protected EquipmentRemoteService getEquipmentRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EquipmentBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the mapping remote service.
	 *
	 * @return the mapping remote service
	 */
	protected ESponderRemoteMappingService getMappingRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the operations centre remote service.
	 *
	 * @return the operations centre remote service
	 */
	protected OperationsCentreRemoteService getOperationsCentreRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/OperationsCentreBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the action remote service.
	 *
	 * @return the action remote service
	 */
	protected ActionRemoteService getActionRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the generic remote service.
	 *
	 * @return the generic remote service
	 */
	protected GenericRemoteService getGenericRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/GenericBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the configuration remote service.
	 *
	 * @return the configuration remote service
	 */
	protected ESponderConfigurationRemoteService getConfigurationRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderConfigurationBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the user remote service.
	 *
	 * @return the user remote service
	 */
	protected UserRemoteService getUserRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/UserBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the personnel remote service.
	 *
	 * @return the personnel remote service
	 */
	protected PersonnelRemoteService getPersonnelRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/PersonnelBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the personnel remote service.
	 *
	 * @return the personnel remote service
	 */
	protected PersonnelRemoteService getPersonnelCompetencesRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/PersonnelBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the logistics remote service.
	 *
	 * @return the logistics remote service
	 */
	protected LogisticsRemoteService getLogisticsRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/LogisticsBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the osgi events entity remote service.
	 *
	 * @return the osgi events entity remote service
	 */
	protected EventsEntityRemoteService getOsgiEventsEntityRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/EventsEntityBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the resource category remote service.
	 *
	 * @return the resource category remote service
	 */
	protected ResourceCategoryRemoteService getResourceCategoryRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/ResourceCategoryBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the type remote service.
	 *
	 * @return the type remote service
	 */
	protected TypeRemoteService getTypeRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/TypeBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	//*******************************************************************************

	/**
	 * Gets the crisis remote service.
	 *
	 * @return the crisis remote service
	 */
	protected CrisisRemoteService getCrisisRemoteService() {
		try {
			return ServiceLocator.getResource("esponder/CrisisBean/remote");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}
}
