/*
 * 
 */
package eu.esponder.df.ruleengine.utilities.actorlookup;

import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorConfigurationXML.
 */
@Root
public class ActorConfigurationXML {

	/** The entries. */
	@ElementList(inline = true)
	private List<ActorXMLFileEntry> entries;

	/**
	 * Sets the entries.
	 *
	 * @param entries the new entries
	 */
	public void setEntries(List<ActorXMLFileEntry> entries) {
		this.entries = entries;
	}

	/**
	 * Gets the entries.
	 *
	 * @return the entries
	 */
	public List<ActorXMLFileEntry> getEntries() {
		return entries;
	}
}