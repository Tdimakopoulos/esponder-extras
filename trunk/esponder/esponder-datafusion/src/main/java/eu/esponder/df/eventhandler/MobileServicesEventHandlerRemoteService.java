/*
 * 
 */
package eu.esponder.df.eventhandler;

import eu.esponder.event.model.ESponderEvent;


// TODO: Auto-generated Javadoc
/**
 * The Interface SensorMeasurmentEventHandlerRemoteService.
 */
public interface MobileServicesEventHandlerRemoteService extends
MobileServicesEventHandlerService {
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.eventhandler.SensorMeasurmentEventHandlerService#ProcessEvent(eu.esponder.event.model.ESponderEvent)
	 */
	public void ProcessEvent(ESponderEvent<?> pEvent);
}
