/*
 * 
 */
package eu.esponder.df.eventhandler.bean;

import java.io.IOException;
import java.util.Date;

import javax.ejb.Stateless;
import javax.naming.NamingException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.df.eventhandler.MobileServicesEventHandlerRemoteService;
import eu.esponder.df.eventhandler.MobileServicesEventHandlerService;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.login.LoginEsponderUserEvent;
import eu.esponder.event.model.login.LoginRequestEvent;
import eu.esponder.eventadmin.ESponderEventPublisher;
//import eu.esponder.osgi.service.event.ESponderEventPublisher;
import eu.esponder.servicemanager.ESponderResource;

/**
 * The Class ActionEventHandlerBean.
 */
@Stateless
public class SecurityEventHandlerBean extends dfEventHandlerBean implements
		MobileServicesEventHandlerService,
		MobileServicesEventHandlerRemoteService {

	private void PublishReply(ESponderUserDTO pUser,String imei) throws JsonGenerationException, JsonMappingException, IOException {
		ESponderEventPublisher<LoginEsponderUserEvent> publisher = new ESponderEventPublisher<LoginEsponderUserEvent>(
				LoginEsponderUserEvent.class);

		try {
			ESponderUserDTO actionSnapshot = new ESponderUserDTO();
			actionSnapshot.setId(new Long(1));
			//actor fr or frc  as attachment
			ESponderResource pServices=new ESponderResource();
			LoginEsponderUserEvent pEvent = new LoginEsponderUserEvent();
		//	int iUserType=pServices.getActorRemoteService().getActorTypeIntRemote(pUser.getActorID());//.findActorClassByIdRemote(pUser.getActorID());
			int iUserType=0;
			if(pUser.getActorRole()==1)
				iUserType=4;
			if(pUser.getActorRole()==2)
				iUserType=5;
			if(pUser.getActorRole()==3)
				iUserType=2;
			//4 - FRC 5-FR 2-IC
			
			if(iUserType==2)
			{
				//ActorICDTO pFR=pServices.getActorRemoteService().findIncidentCommanderByIdRemote(pUser.getActorID());//.findFRByIdRemote(pUser.getActorID());
				ActorICDTO pIC= new ActorICDTO();
				pIC.setId(pUser.getActorID());
				//pServices.getActorRemoteService().f
			
				pEvent.setIMEI(imei);
				pEvent.setRasberryip(pUser.getRasberryip());
				pEvent.setsIPaddress(pUser.getsIPaddress());
				pEvent.setsIPaddressFRC(pUser.getsIPaddressFRC());
				pEvent.setsIPaddressEOC(pUser.getsIPaddressEOC());
				pEvent.setsIPaddressMEOC(pUser.getsIPaddressMEOC());
				pEvent.setsIPaddresssrv(pUser.getsIPaddresssrv());
				pEvent.setsIPpassword(pUser.getsIPpassword());
				pEvent.setsIPusername(pUser.getsIPusername());
				pEvent.setWebcamip(pUser.getWebcamip());
				pEvent.setEventAttachment((ActorDTO)pIC);
				pEvent.setJournalMessage("Login OK ");
				pEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
				pEvent.setEventTimestamp(new Date());
				pEvent.setEventSeverity(SeverityLevelDTO.UNDEFINED);
				pEvent.setEventSource(actionSnapshot);
				//0 = FRC
				//1 = FR
				//2 = IC
				pEvent.setiRole(2);
			}
			
			if(iUserType==4)
			{
				//ActorFRCDTO pFRC=pServices.getActorRemoteService().findFRChiefByIdRemote(pUser.getActorID());
			
				ActorFRCDTO pFRC= new ActorFRCDTO();
				pFRC.setId(pUser.getActorID());
			//pServices.getActorRemoteService().f
			
			pEvent.setIMEI(imei);
			pEvent.setRasberryip(pUser.getRasberryip());
			pEvent.setsIPaddress(pUser.getsIPaddress());
			pEvent.setsIPaddressFRC("");
			pEvent.setsIPaddressEOC(pUser.getsIPaddressEOC());
			pEvent.setsIPaddressMEOC(pUser.getsIPaddressMEOC());
			pEvent.setsIPaddresssrv(pUser.getsIPaddresssrv());
			pEvent.setsIPpassword(pUser.getsIPpassword());
			pEvent.setsIPusername(pUser.getsIPusername());
			pEvent.setWebcamip(pUser.getWebcamip());
			pEvent.setEventAttachment((FirstResponderActorDTO)pFRC);
			pEvent.setJournalMessage("Login OK ");
			pEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
			pEvent.setEventTimestamp(new Date());
			pEvent.setEventSeverity(SeverityLevelDTO.UNDEFINED);
			pEvent.setEventSource(actionSnapshot);
			//0 = FRC
			//1 = FR
			//2 = IC
			pEvent.setiRole(0);
			}
			
			if(iUserType==5)
			{
				//ActorFRDTO pFR=pServices.getActorRemoteService().findFRByIdRemote(pUser.getActorID());
			
			//pServices.getActorRemoteService().f
			
				ActorFRDTO pFR= new ActorFRDTO();
				pFR.setId(pUser.getActorID());
				
			pEvent.setIMEI(imei);
			pEvent.setRasberryip(pUser.getRasberryip());
			pEvent.setsIPaddress(pUser.getsIPaddress());
			pEvent.setsIPaddressFRC(pUser.getsIPaddressFRC());
			pEvent.setsIPaddressEOC(pUser.getsIPaddressEOC());
			pEvent.setsIPaddressMEOC(pUser.getsIPaddressMEOC());
			pEvent.setsIPaddresssrv(pUser.getsIPaddresssrv());
			pEvent.setsIPpassword(pUser.getsIPpassword());
			pEvent.setsIPusername(pUser.getsIPusername());
			pEvent.setWebcamip(pUser.getWebcamip());
			pEvent.setEventAttachment((FirstResponderActorDTO)pFR);
			pEvent.setJournalMessage("Login OK ");
			pEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
			pEvent.setEventTimestamp(new Date());
			pEvent.setEventSeverity(SeverityLevelDTO.UNDEFINED);
			pEvent.setEventSource(actionSnapshot);
			//0 = FRC
			//1 = FR
			//2 = IC
			pEvent.setiRole(1);
			}
			//System.out.println("Sending Login information event");
		//	ObjectMapper mapper = new ObjectMapper();
		//	String sphereJSON = mapper.writeValueAsString(pEvent);
			publisher.publishEvent(pEvent);
			
			Thread.currentThread().sleep(100);
			//System.out.println("Sending Login information event send OK!!!! Json : "+sphereJSON);
		} catch (EventListenerException e) {

			e.printStackTrace();
		} catch (InterruptedException e) {

			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.df.eventhandler.ActionEventHandlerService#ProcessEvent(eu
	 * .esponder.event.model.ESponderEvent)
	 */

	public void ProcessEvent(ESponderEvent<?> pEvent) {
		//System.out.print("Security event received");
		//System.out.print("Message : " + pEvent.getJournalMessage());
		LoginRequestEvent pEventLRE = (LoginRequestEvent)pEvent;
		String IMEI= pEventLRE.getIMEI();
		int iType = pEventLRE.getiLoginType(); //0 login - 1 PKI
		ESponderResource pServices=null;
		try {
			pServices = new ESponderResource();
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ESponderUserDTO pUser=new ESponderUserDTO();
		if(iType==0)
			pUser=pServices.getUserRemoteService().findUserByUsernameRemote(pEventLRE.getUsername());
		
		if(iType==1)
			pUser=pServices.getUserRemoteService().findUserByPKIRemote(pEventLRE.getPkikey());
		
		
			try {
				PublishReply(pUser,pEventLRE.getIMEI());
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			
			
	}
}
