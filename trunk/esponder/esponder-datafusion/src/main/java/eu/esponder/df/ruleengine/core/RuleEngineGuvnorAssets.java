/**
 * RuleEngineGuvnorAssets
 * 
 * This Java class implement a rule engine which can get the rules from the drools guvnor web repository.
 * What acctualy does is that get all rules from the repository , create a local repository and then execute 
 * the rules based on facts localy.
 * 
 * 
 * @Project   esponder
 * @package   Datafusion
 */
package eu.esponder.df.ruleengine.core;



import eu.esponder.df.ruleengine.repository.RepositoryController;
import eu.esponder.df.ruleengine.repository.XMLRepositoryManager;
import eu.esponder.df.ruleengine.settings.DroolSettings;
import eu.esponder.exception.EsponderCheckedException;


// TODO: Auto-generated Javadoc
/**
 * The Class RuleEngineGuvnorAssets.
 *
 * @author tdim
 */
public class RuleEngineGuvnorAssets extends RuleEngine {

	/** The d settings. */
	DroolSettings dSettings = new DroolSettings();
	
	/** The d local repository. */
	EngineKnowledgeLocalRepository dLocalRepository = new EngineKnowledgeLocalRepository();
	
	/** The d rule assets access. */
	RuleEngineRulesAssetsHelper dRuleAssetsAccess = new RuleEngineRulesAssetsHelper();
	
	/** The d xml helper. */
	RuleEngineXMLHelper dXMLHelper = new RuleEngineXMLHelper();
	
	/** The d rep controller. */
	RepositoryController dRepController=new RepositoryController();
	
	/** The b session. */
	private boolean bSession = false;

	/**
	 * Populate local repository for package.
	 *
	 * @param szPackage the package name to get all rules
	 * @return the rules for package
	 */
	public String[] PopulateLocalRepositoryForPackage(String szPackage) {

		return ProcessXMLAssets(szPackage, GetAssetsXMLForPackage(szPackage));
	}

	/**
	 * Populate local repository for category.
	 *
	 * @param szCategory the category to get all rules
	 * @return the rules for category
	 */
	public String[] PopulateLocalRepositoryForCategory(String szCategory) {
		//System.out.print("Load by category");

		return ProcessXMLAssets(szCategory, GetAssetsXMLForCategory(szCategory));
	}

	/**
	 * Inference engine add knowledge for package.
	 *
	 * @param szPackage the package name to use the rules
	 * @throws Exception the exception
	 */
	public void InferenceEngineAddKnowledgeForPackage(String szPackage)
			throws Exception {
		AddRules(PopulateLocalRepositoryForPackage(szPackage));
	}

	/**
	 * Inference engine add knowledge for category.
	 *
	 * @param szCategory the category name to use the rules
	 * @throws Exception the exception
	 */
	public void InferenceEngineAddKnowledgeForCategory(String szCategory)
			throws Exception {
		AddRules(PopulateLocalRepositoryForCategory(szCategory));
	}

	/**
	 * Inference engine add knowledge.
	 *
	 * @param Rules the array of rules
	 * @throws Exception the exception
	 */
	public void InferenceEngineAddKnowledge(String[] Rules) throws Exception {
		AddRules(Rules);
	}

	/**
	 * Inference engine add object initilize sessions.
	 */
	private void InferenceEngineAddObjectInitilizeSessions() {
		if (bSession == false) {
			//System.out.println("Adding DTO Objetc IE- Create New Session Start");
			CreateSession();
			//System.out.println("Adding DTO Objetc IE- Create New Session Finish");
			bSession = true;
		}
	}

	/**
	 * Inference engine add object.
	 *
	 * @param fact the fact
	 */
	public void InferenceEngineAddObject(Object fact) {
		InferenceEngineAddObjectInitilizeSessions();
		AddObject(fact);
	}

	/**
	 * Inference engine add dto object.
	 *
	 * @param fact the fact
	 */
	public void InferenceEngineAddDTOObject(Object fact) {
		//System.out.println("Adding DTO Objetc IE- Session Initialize");
		InferenceEngineAddObjectInitilizeSessions();
		//System.out.println("Adding DTO Objetc IE- Add Object");
		AddObject(fact);
	}

	/**
	 * Inference engine add objects.
	 *
	 * @param facts the facts to be added in rule engine session
	 */
	public void InferenceEngineAddObjects(Object[] facts) {
		InferenceEngineAddObjectInitilizeSessions();
		AddObjects(facts);
	}

	/**
	 * rule all rules.
	 */
	public void InferenceEngineRunAssets() {
		try {
			RunRules();
		} catch (Exception e) {
			System.out.println("Problem with rule execution exception : "+e.getMessage());
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
		}
		CloseSession();
	}

	

	/**
	 * Process xml assets.
	 *
	 * @param szPackage the package name
	 * @param szXMLAssets the assets string
	 * @return the rules in file system
	 */
	private String[] ProcessXMLAssets(String szPackage, String szXMLAssets) {

		String szURL = null;
		String szContent = null;
		//System.out.print("--- process XML ---");
		//System.out.print("XML : "+szXMLAssets);
		//System.out.print("--- process XML ---");
		dRuleAssetsAccess.SetRulesAssets(dXMLHelper.ProcessAssets(szXMLAssets));
	//	System.out.print("Total Rules : " + dRuleAssetsAccess.GetRulesNumber());
		for (int i = 0; i < dRuleAssetsAccess.GetRulesNumber(); i++) {
			szURL = dRuleAssetsAccess.GetRuleSourcePoint(i);
			szContent = GetAssetsTextForURL(szURL);
			dLocalRepository.AddNewRuleOnLocalRepository(szURL,
					dSettings.getSzTmpRepositoryDirectoryPath(), szContent);
		}
		
		return dLocalRepository.ReturnRules();
	}

	/**
	 * Creates the authorization header.
	 *
	 * @param szUser username
	 * @param szPassword password
	 * @return session string
	 */
	@SuppressWarnings("unused")
	private String CreateAuthorizationHeader(String szUser, String szPassword) {
		String szAuthorizationHeader=null;
		//		String szAuthorization = szUser + ":" + szPassword;
//		String szAuthorizationHeader = "Basic "
//				+ org.apache.cxf.common.util.Base64Utility
//						.encode(szAuthorization.getBytes());
		return szAuthorizationHeader;
		//return null;
	}

	/**
	 * Gets the assets xml for package.
	 *
	 * @param szPackage the package name
	 * @return the xml assets
	 */
	public String GetAssetsXMLForPackage(String szPackage) {
		String content=null;
		//System.out.println("------> Start load xml for package : "+szPackage);
		if(dRepController.RefreshRequest(szPackage,true))
		{
			//System.out.println("------> Start load package from guvnor: "+szPackage);
			content=GetAssetsXMLForPackagehttp(szPackage);
		}
		else
		{
			//System.out.println("------> Start load package from xml repo : "+szPackage);
			XMLRepositoryManager dXMLRep=new XMLRepositoryManager();
			content=dXMLRep.LoadPackage(szPackage);
		}
		return content;
		//return null;
	}

	/**
	 * Gets the assets xml for packagehttp.
	 *
	 * @param szPackage the sz package
	 * @return the string
	 */
	public String GetAssetsXMLForPackagehttp(String szPackage) {

		RestGetClient pclient = new RestGetClient(dSettings
				.getSzDroolApplicationServerURL()+dSettings.getSzDroolWarDeploymentPath() + "/"
				+ dSettings.getSzDroolServiceEntryPoint()
				+ "/packages/" + szPackage + "/assets");
		
		return pclient.getXML();
	}
	
	/**
	 * Gets the assets xml for category.
	 *
	 * @param szCategory the category name
	 * @return the xml assets
	 */
	public String GetAssetsXMLForCategory(String szCategory) {
		String content =null;
		if(dRepController.RefreshRequest(szCategory,true))
		{
			content=GetAssetsXMLForCategoryhttp(szCategory);
		}
		else
		{
			XMLRepositoryManager dXMLRep=new XMLRepositoryManager();
			content=dXMLRep.LoadCategory(szCategory);
		}
		//System.out.println("Asset ------> "+content);
		return content;
//		return null;
	}

	/**
	 * Gets the assets xml for categoryhttp.
	 *
	 * @param szCategory the sz category
	 * @return the string
	 */
	public String GetAssetsXMLForCategoryhttp(String szCategory) {
		String content=null;

		
		RestGetClient pclient = new RestGetClient(dSettings
				.getSzDroolApplicationServerURL()+dSettings.getSzDroolWarDeploymentPath() + "/"
				+ dSettings.getSzDroolServiceEntryPoint()
				+ "/"+"categories/" + szCategory + "/assets");
		
		content=pclient.getXML();
		 
		XMLRepositoryManager dXMLRep=new XMLRepositoryManager();
		dXMLRep.SaveCategory(szCategory, content);
		return content;
	}
	
	/**
	 * Gets the assets text for url.
	 *
	 * @param szURL asset source url
	 * @return the source code of asset
	 */
	public String GetAssetsTextForURL(String szURL) {
		EngineKnowledgeLocalRepository dLP=new EngineKnowledgeLocalRepository();
		String szPlainName=dLP.GetRuleNameFromSourceURL(szURL);
		String content=null;
		if(dRepController.RefreshRequest(szPlainName,false))
		{
			content=GetAssetsTextForURLhttp(szURL,szPlainName);
		}else
		{
			XMLRepositoryManager dXMLRep=new XMLRepositoryManager();
			content=dXMLRep.LoadAsset(szPlainName);
		}
		return content;
		//return null;
	}
	
	/**
	 * Gets the assets text for ur lhttp.
	 *
	 * @param szURL the sz url
	 * @param szPlainName the sz plain name
	 * @return the string
	 */
	@SuppressWarnings("unused")
	public String GetAssetsTextForURLhttp(String szURL,String szPlainName) {
		String content=null;
		
		
		
		String szn = szURL.substring(dSettings.getSzDroolApplicationServerURL()
				.length(), szURL.length());
		
	
		
		RestGetClient pclient = new RestGetClient(/*dSettings
				.getSzDroolApplicationServerURL()+*/szURL);
		
		content=pclient.getText();
		//System.out.println("Rule URL :"+szURL);
		//System.out.println("Rule :"+content);
		XMLRepositoryManager dXMLRep=new XMLRepositoryManager();
		dXMLRep.SaveAsset(szPlainName,content);
		return content;
	}
}
