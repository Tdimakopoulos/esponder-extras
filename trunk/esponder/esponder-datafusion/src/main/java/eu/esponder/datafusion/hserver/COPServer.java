package eu.esponder.datafusion.hserver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.NamingException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.time.DateUtils;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;

import com.prosyst.mprm.backend.event.EventListenerException;
import com.prosyst.mprm.common.ManagementException;

import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.controller.crisis.CrisisRemoteService;
import eu.esponder.controller.crisis.TypeRemoteService;
import eu.esponder.controller.crisis.action.ActionRemoteService;
import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.controller.crisis.resource.EquipmentRemoteService;
import eu.esponder.controller.crisis.resource.LogisticsRemoteService;
import eu.esponder.controller.crisis.resource.OperationsCentreRemoteService;
import eu.esponder.controller.crisis.resource.OrganisationRemoteService;
import eu.esponder.controller.crisis.resource.PersonnelRemoteService;
import eu.esponder.controller.crisis.resource.SensorRemoteService;
import eu.esponder.controller.crisis.resource.category.ResourceCategoryRemoteService;
import eu.esponder.controller.crisis.user.UserRemoteService;
import eu.esponder.controller.generic.GenericRemoteService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.controller.persistence.CrudRemoteService;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionStageEnumDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.alternative.OCEocALTDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionExpressionEnumDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.LocationAreaDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.type.ActionTypeDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.datafusion.query.ESponderDFQueryRequestEvent;
import eu.esponder.event.datafusion.query.ESponderDFQueryResponseEvent;
import eu.esponder.event.mobile.MobileFileNotifierEvent;
import eu.esponder.event.mobile.MobileMessageEvent;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.crisis.action.NewActionEvent;
import eu.esponder.event.model.crisis.resource.MoveFRTeamEvent;
import eu.esponder.event.model.crisis.resource.MoveOperationsCentreEvent;
import eu.esponder.event.model.snapshot.resource.CreateActorSnapshotEvent;
import eu.esponder.event.model.snapshot.resource.CreateOperationsCentreSnapshotEvent;
import eu.esponder.event.model.snapshot.resource.UpdateActorSnapshotEvent;
import eu.esponder.event.model.snapshot.resource.UpdateOperationsCentreSnapshotEvent;
import eu.esponder.eventadmin.ESponderEventListener;
import eu.esponder.eventadmin.ESponderEventPublisher;
//import eu.esponder.model.snapshot.Period;
//import eu.esponder.model.snapshot.location.Point;
//import eu.esponder.model.snapshot.location.Sphere;
import eu.esponder.servicemanager.ESponderResource;
import eu.esponder.util.ejb.ServiceLocator;

public class COPServer extends Thread {

	Long useridglobal = new Long(1);
	CrisisCOPServer pvar = new CrisisCOPServer();
	OCEocALTDTO pGlobal = null;

	protected static final String USER_TITLE = "Dimitris FRC";
	protected Long userID;
	
	protected ActorRemoteService actorService;
	protected EquipmentRemoteService equipmentService;
	protected OperationsCentreRemoteService operationsCentreService;
	protected SensorRemoteService sensorService;
	protected TypeRemoteService typeService;
	protected UserRemoteService userService;
	protected ActionRemoteService actionService;
	protected ResourceCategoryRemoteService resourceCategoryService;
	protected GenericRemoteService genericService;
	protected ESponderRemoteMappingService mappingService;
	protected OrganisationRemoteService organisationService;
	protected PersonnelRemoteService personnelService;
	protected ESponderConfigurationRemoteService configService;
	protected LogisticsRemoteService logisticsService;
	protected CrisisRemoteService crisisService;
	@SuppressWarnings("rawtypes")
	protected CrudRemoteService crudService;
		
	
	public void InitializeServices() throws NamingException {
		configService = ServiceLocator.getResource("esponder/ESponderConfigurationBean/remote");
		crudService = ServiceLocator.getResource("esponder/CrudBean/remote");
		actorService = ServiceLocator.getResource("esponder/ActorBean/remote");
		equipmentService = ServiceLocator.getResource("esponder/EquipmentBean/remote");
		operationsCentreService = ServiceLocator.getResource("esponder/OperationsCentreBean/remote");
		sensorService = ServiceLocator.getResource("esponder/SensorBean/remote");
		typeService = ServiceLocator.getResource("esponder/TypeBean/remote");
		userService = ServiceLocator.getResource("esponder/UserBean/remote");
		actionService = ServiceLocator.getResource("esponder/ActionBean/remote");
		resourceCategoryService = ServiceLocator.getResource("esponder/ResourceCategoryBean/remote");
		genericService = ServiceLocator.getResource("esponder/GenericBean/remote");
		mappingService = ServiceLocator.getResource("esponder/ESponderMappingBean/remote");
		organisationService =ServiceLocator.getResource("esponder/OrganisationBean/remote");
		personnelService = ServiceLocator.getResource("esponder/PersonnelBean/remote");
		logisticsService = ServiceLocator.getResource("esponder/LogisticsBean/remote");
		crisisService = ServiceLocator.getResource("esponder/CrisisBean/remote");
		
		ESponderUserDTO userDTO = userService.findUserByNameRemote(USER_TITLE);
		if (null != userDTO) {
			userID = userDTO.getId();
		}
		else
		{
			userID= new Long(5);
		}
	}
	
	protected PeriodDTO createPeriod(int seconds) {
		java.util.Date now = new java.util.Date();
		PeriodDTO period = new PeriodDTO(now.getTime(), DateUtils.addSeconds(now, seconds).getTime());
		return period; 
	}

	protected PeriodDTO createPeriodDTO(int seconds) {
		java.util.Date now = new java.util.Date();
		PeriodDTO period = new PeriodDTO(now.getTime(), DateUtils.addSeconds(now, seconds).getTime());
		PeriodDTO periodDTO = (PeriodDTO) mappingService.mapObject(period, PeriodDTO.class);
		return periodDTO;
	}
	
	

	protected SphereDTO createSphere(
			Double latitude, Double longitude, Double altitude, Double radius) {
		
		PointDTO centre = new PointDTO(
				new BigDecimal(latitude), new BigDecimal(longitude), new BigDecimal(altitude));
		SphereDTO sphere = new SphereDTO(centre, new BigDecimal(radius),(new Date()).toString());
		return sphere;
	}
	
	protected SphereDTO createSphereDTO(
			Double latitude, Double longitude, Double altitude, Double radius, String title) {
		
		PointDTO centre = new PointDTO(
				new BigDecimal(latitude), new BigDecimal(longitude), null != altitude ? new BigDecimal(altitude) : null);
		SphereDTO sphere = new SphereDTO(centre, null != radius ? new BigDecimal(radius) : null, title);
		return sphere;
	}
	
	public COPServer(Long userid) {
		System.out
				.println("*********** New COP Thread Server Initializing ***********");
		useridglobal = userid;
		pGlobal = pvar.find2DDetailsForOC(useridglobal);
		System.out
				.println("*********** New COP Thread Server Initialized ***********");
	}

	public void SaveFile() {
		String szPropertiesFileNameUnix = "//home//exodus//osgi//map.json";
		String szPropertiesFileNameWindows = "c:\\development\\map.json";
		
			if (File.separatorChar == '/')
				szPropertiesFileNameUnix=szPropertiesFileNameUnix;
			else
				szPropertiesFileNameUnix=szPropertiesFileNameWindows;
		
		
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(
					new FileWriter(szPropertiesFileNameUnix));
			writer.write(ClassToString(pGlobal));

		} catch (IOException e) {
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
			}
		}
	}

	public String ClassToString(OCEocALTDTO pclass)
			throws JsonGenerationException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		String pret = mapper.writeValueAsString(pclass);

		return pret;
	}

	public ActionDTO CreateAction(String crisisTitle,String actionType,String actionTitle) throws ClassNotFoundException {
		CrisisContextDTO crisisContextDTO = crisisService
				.findCrisisContextDTOByTitle(crisisTitle);
		ActionTypeDTO actionTypeDTO = (ActionTypeDTO) typeService
				.findDTOByTitle(actionType);

		if (crisisContextDTO != null && actionTypeDTO != null) {

			ActionDTO actionDTO1 = new ActionDTO();
			actionDTO1.setActionOperation(ActionOperationEnumDTO.FIX);
			actionDTO1.setType(actionTypeDTO.getTitle());
			actionDTO1.setCrisisContext(crisisContextDTO);
			actionDTO1.setSeverityLevel(SeverityLevelDTO.MEDIUM);
			actionDTO1.setTitle(actionTitle);
			actionDTO1.setActionStage(ActionStageEnumDTO.InProgress);
			
			return actionService.createActionRemote(actionDTO1, this.userID);
			

		}
		return null;
	}

	public void CreateActionObjectivesLocationAreas(String locationTitle,BigDecimal bigDecimal,BigDecimal bigDecimal2,BigDecimal bigDecimal3,Long radius)
			throws ClassNotFoundException {

		PointDTO point1 = new PointDTO(bigDecimal2, bigDecimal,
				bigDecimal3);
	

		SphereDTO sphere1 = new SphereDTO();
		sphere1.setCentre(point1);
		sphere1.setRadius(new BigDecimal(radius));

		sphere1.setTitle(locationTitle);
		genericService.createEntityRemote(sphere1, userID);

		
	}

	public ActionObjectiveDTO CreateActionObjectives(String locationTitle,String actionTitle,String objTitle) throws InstantiationException,
			IllegalAccessException, Exception {

		LocationAreaDTO areaDTO1 = null;

		List<LocationAreaDTO> results = (List<LocationAreaDTO>) genericService
				.getDTOEntities(SphereDTO.class.getName(),
						new EsponderCriterionDTO("title",
								EsponderCriterionExpressionEnumDTO.EQUAL,
								locationTitle), 10, 0);

		if (!results.isEmpty() && results.size() < 2) {
			areaDTO1 = results.get(0);
		}

		ActionDTO actionDTO = actionService
				.findActionDTOByTitle(actionTitle);

		if (areaDTO1 != null && actionDTO != null) {

			PeriodDTO period1 = createPeriodDTO(10);
			

			ActionObjectiveDTO objective1 = new ActionObjectiveDTO();
			objective1.setAction(actionDTO);
			objective1.setLocationArea(areaDTO1);
			objective1.setPeriod(period1);
			objective1.setTitle(objTitle);
			objective1.setActionStage(ActionStageEnumDTO.InProgress);
			

			return actionService.createActionObjectiveRemote(objective1, userID);
			
		}
		return null;
	}

	public ActionPartDTO CreateActionParts(String actionTitle,String actorTitle,String actionpartTitle) throws ClassNotFoundException {

		ActionDTO parentActionDTO = actionService
				.findActionDTOByTitle(actionTitle);
		if (parentActionDTO != null) {

			ActionPartDTO actionPart1 = new ActionPartDTO();
			actionPart1.setActionId(parentActionDTO.getId());
			actionPart1.setActionOperation(ActionOperationEnumDTO.FIX);
			ActorFRCDTO actorDTO1 = actorService.findFRCByTitleRemote(actorTitle);
			System.out.println("Actor Title : "+actorTitle);
			System.out.println("Actor ID : "+actorDTO1.getId());
			actionPart1.setActor(new Long(4));
			actionPart1.setSeverityLevel(SeverityLevelDTO.MEDIUM);
			actionPart1.setTitle(actionpartTitle);
			actionPart1.setActionStage(ActionStageEnumDTO.InProgress);
			
			return actionService.createActionPartRemote(actionPart1, userID);
			

		}
		return null;

	}

	public ActionPartObjectiveDTO CreateActionPartsObjectives(String locationTitle,String actionPartTitle,Long Partid,String objPartTitle) throws InstantiationException,
			IllegalAccessException, Exception {

		LocationAreaDTO areaDTO = null;

		List<LocationAreaDTO> results = (List<LocationAreaDTO>) genericService
				.getDTOEntities(SphereDTO.class.getName(),
						new EsponderCriterionDTO("title",
								EsponderCriterionExpressionEnumDTO.EQUAL,
								locationTitle), 10, 0);

		if (!results.isEmpty() && results.size() < 2) {
			areaDTO = results.get(0);
		}

		PeriodDTO period1 = createPeriodDTO(10);
		
		if (areaDTO != null) {

			ActionPartObjectiveDTO objective1 = new ActionPartObjectiveDTO();
			ActionPartDTO pfind= actionService.findActionPartDTOByTitle(actionPartTitle);
			System.out.println(" Action part title : "+pfind.getTitle()+" id : "+pfind.getId());
			objective1.setActionPartID(pfind.getId());
			//objective1.s
			objective1.setTitle(objPartTitle);
			objective1.setLocationArea(areaDTO);
			objective1.setPeriod(period1);
			objective1.setActionStage(ActionStageEnumDTO.InProgress);
			return actionService.createActionPartObjectiveRemote(objective1, userID);
			
		}
		return null;
	}

	public XMLGregorianCalendar getXMLGregorianCalendarNow() throws DatatypeConfigurationException {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
		XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
		return now;
	}


	
	private PeriodDTO getPeriod() {
		PeriodDTO period = new PeriodDTO();
		period.setDateFrom(new Date().getTime());
		period.setDateTo(getDate().getTime());
		return period;
	}

	private Date getDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.MINUTE, 1);
		return c.getTime();
	}
	
	public void ProcessEventForFRTEAMMove(FRTeamDTO pTeam,PointDTO pFinalPosition) throws InstantiationException, IllegalAccessException, Exception
	{
		String sRandom=new Date().toString();
		
		System.out.println(" --- Creating Action ");
		
		
		ActionDTO pAction=CreateAction("Fire Brigade Drill","OpActionType","Move Team Action"+sRandom);
		
		System.out.println(" --- Creating Action Location");
		CreateActionObjectivesLocationAreas("Move Team"+sRandom,pFinalPosition.getLongitude(),pFinalPosition.getLatitude(),pFinalPosition.getAltitude(),new Long(5));
		
		System.out.println(" --- Creating Action objective");
		ActionObjectiveDTO pActionObj=CreateActionObjectives("Move Team"+sRandom,"Move Team Action"+sRandom,"Move Team Action Objective"+sRandom);
		
		System.out.println(" --- Creating Action Parts");
		ActionPartDTO pActionPart=CreateActionParts("Move Team Action"+sRandom,"FRC #1","Move Team Action Part"+sRandom);
		sRandom=new Date().toString();
		ActionPartDTO pActionPart1=CreateActionParts("Move Team Action"+sRandom,"FR #1.1","Move Team Action Part"+sRandom);
		sRandom=new Date().toString();
		ActionPartDTO pActionPart2=CreateActionParts("Move Team Action"+sRandom,"FR #1.2","Move Team Action Part"+sRandom);
		sRandom=new Date().toString();
		ActionPartDTO pActionPart3=CreateActionParts("Move Team Action"+sRandom,"FR #2.1","Move Team Action Part"+sRandom);
		sRandom=new Date().toString();
		ActionPartDTO pActionPart4=CreateActionParts("Move Team Action"+sRandom,"FR #2.2","Move Team Action Part"+sRandom);
		sRandom=new Date().toString();
		ActionPartDTO pActionPart5=CreateActionParts("Move Team Action"+sRandom,"FR #3.1","Move Team Action Part"+sRandom);
		
		System.out.println(" --- Creating Action Parts Objectives");
		ActionPartObjectiveDTO pActionPartObj=CreateActionPartsObjectives("Move Team"+sRandom,"Move Team Action Part"+sRandom,pActionPart.getActionId(),"Move Team Action Part Objective"+sRandom);
		
		System.out.println(" --- Make Event to publish ");
		NewActionEvent pPublishEvent= new NewActionEvent();
		pPublishEvent.setEventAttachment(pAction);
		
		ResultListDTO actionObjectives= new ResultListDTO();
		ResultListDTO actionParts= new ResultListDTO();
		ResultListDTO actionPartObjectives= new ResultListDTO();
		
		//actionObjectives.add(pActionObj);
		//actionParts.add(pActionPart);
		//actionPartObjectives.add(pActionPartObj);
		
		List<ActionObjectiveDTO> List1= new ArrayList<ActionObjectiveDTO>();
		
		List<ActionPartDTO> List2= new ArrayList<ActionPartDTO>();
		
		List<ActionPartObjectiveDTO> List3= new ArrayList<ActionPartObjectiveDTO>();
		
		List1.add(pActionObj);
		
		List2.add(pActionPart);
		List2.add(pActionPart1);
		List2.add(pActionPart2);
		List2.add(pActionPart3);
		List2.add(pActionPart4);
		List2.add(pActionPart5);
		
		List3.add(pActionPartObj);
		
		
		actionObjectives.setResultList(List1);
		actionParts.setResultList(List2);
		actionPartObjectives.setResultList(List3);
		
		pPublishEvent.setActionObjectives(actionObjectives);
		pPublishEvent.setActionPartObjectives(actionPartObjectives);
		pPublishEvent.setActionParts(actionParts);
		
		
		System.out.println(" --- Initialize Publisher ");
		ESponderEventPublisher<NewActionEvent> publisher = 
				new ESponderEventPublisher<NewActionEvent>(NewActionEvent.class);
		
		pPublishEvent.setEventSeverity(SeverityLevelDTO.SERIOUS);
		pPublishEvent.setEventTimestamp(new Date());
		pPublishEvent.setJournalMessage("New Action");
		pPublishEvent.setJournalMessageInfo("New Action");
		
		
		System.out.println(" --- Try to publish ");
		try {
			publisher.publishEvent(pPublishEvent);
		} catch (Exception e) {
			
			System.out.println(" --- Publish error "+e.getMessage());
		}
		
		System.out.println(" --- Closing connection ");
		publisher.CloseConnection();
		
	}
	
	public void EventReceived(ESponderEvent<? extends ESponderEntityDTO> event2) throws InstantiationException, IllegalAccessException, Exception {
		System.out.println("*********** New Event Received : "
				+ event2.getClass().getCanonicalName() + " Event Message : "
				+ event2.getJournalMessage());
		String pname = event2.getClass().getSimpleName();

		// move frteam
		if (pname.equalsIgnoreCase("MoveFRTeamEvent")) {
			System.out.println(" --- Move FRteam event received");
			MoveFRTeamEvent pEventMoveFRTEAM = (MoveFRTeamEvent) event2;

			ProcessEventForFRTEAMMove(pEventMoveFRTEAM.getEventAttachment(),pEventMoveFRTEAM.getpFinalPosition());
			
			System.out.println(" --- Move FRteam event Published to mobile");
		}

		// move meoc
		if (pname.equalsIgnoreCase("MoveOperationsCentreEvent")) {

		}

		if (pname.equalsIgnoreCase("ESponderDFQueryRequestEvent")) {

			ESponderDFQueryRequestEvent pEventDF = (ESponderDFQueryRequestEvent) event2;
			Long RequestID = pEventDF.getRequestID();
			String IMEI = pEventDF.getIMEI();
			Long QueryID = pEventDF.getQueryID();
			QueryParamsDTO params = pEventDF.getParams();
			params.getQueryParamList();

			if (QueryID == 1) {
				System.out.println("******** Map Location Request cached");

				String userid = null;
				Iterator entries = params.getQueryParamList().entrySet()
						.iterator();
				while (entries.hasNext()) {
					Map.Entry entry = (Map.Entry) entries.next();
					String key = (String) entry.getKey();
					String value = (String) entry.getValue();
					if (key.equalsIgnoreCase("userid")) {

						userid = value;
					}

				}

				OCEocALTDTO pRes = null;

				pRes = pGlobal;
				SaveFile();

				ESponderUserDTO actionSnapshot = new ESponderUserDTO();
				actionSnapshot.setId(new Long(0));
				// crisis context dto as attachement
			
				ESponderDFQueryResponseEvent pEventSend = new ESponderDFQueryResponseEvent();
				pEventSend.setMapPositions(pRes);
				pEventSend.setRequestID(RequestID);
				pEventSend.setQueryID(QueryID);
				pEventSend.setIMEI(IMEI);
				pEventSend.setpResponse(null);
				ESponderResource pServices = new ESponderResource();
				CrisisContextDTO pFind = pServices.getCrisisRemoteService()
						.findCrisisContextDTOById(
								new Long(pRes.getEocCrisisContext()));
				pEventSend.setEventAttachment(pFind);
				pEventSend.setJournalMessage("Test Event");
				pEventSend.setEventSeverity(SeverityLevelDTO.SERIOUS);
				pEventSend.setEventTimestamp(new Date());
				pEventSend.setEventSeverity(SeverityLevelDTO.UNDEFINED);
				pEventSend.setEventSource(actionSnapshot);

				try {
					ESponderEventPublisher<ESponderDFQueryResponseEvent> publisher2 = new ESponderEventPublisher<ESponderDFQueryResponseEvent>(
							ESponderDFQueryResponseEvent.class);
					publisher2.publishEvent(pEventSend);
					publisher2.CloseConnection();
					SaveFile();
					System.out.println("Published Event for Map Finished");
					Thread.currentThread().sleep(10);
				} catch (EventListenerException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}

			}// query 1 end

		} else {
			HierarchySnapshotUpdater phsu = new HierarchySnapshotUpdater();
			System.out.println("Update Cached Location Check Called ");
			phsu.identifyAndUpdateSnapshot(pGlobal, event2.getEventAttachment());
			SaveFile();
		}
	}

	@Override
	public void run() {

		try {
			InitializeServices();
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		SaveFile();
		int irefcount = 0;
		try {
			ESponderEventListener<CreateOperationsCentreSnapshotEvent> listener = null;
			
			ESponderEventListener<MoveFRTeamEvent> listenerMFRT = null;
			ESponderEventListener<MoveOperationsCentreEvent> listenerMOC = null;

			ESponderEventListener<CreateActorSnapshotEvent> listener2 = null;
			ESponderEventListener<UpdateActorSnapshotEvent> listener3 = null;
			ESponderEventListener<UpdateOperationsCentreSnapshotEvent> listener4 = null;
			ESponderEventListener<ESponderDFQueryRequestEvent> listener5 = null;

			listener = new ESponderEventListener<CreateOperationsCentreSnapshotEvent>(
					CreateOperationsCentreSnapshotEvent.class, this);
			listener2 = new ESponderEventListener<CreateActorSnapshotEvent>(
					CreateActorSnapshotEvent.class, this);
			listener3 = new ESponderEventListener<UpdateActorSnapshotEvent>(
					UpdateActorSnapshotEvent.class, this);
			listener4 = new ESponderEventListener<UpdateOperationsCentreSnapshotEvent>(
					UpdateOperationsCentreSnapshotEvent.class, this);

			listener5 = new ESponderEventListener<ESponderDFQueryRequestEvent>(
					ESponderDFQueryRequestEvent.class, this);

			listenerMOC = new ESponderEventListener<MoveOperationsCentreEvent>(
					MoveOperationsCentreEvent.class, this);

			listenerMFRT = new ESponderEventListener<MoveFRTeamEvent>(
					MoveFRTeamEvent.class, this);

			listenerMFRT.subscribe();
			listenerMOC.subscribe();
			listener.subscribe();
			listener2.subscribe();
			listener3.subscribe();
			listener4.subscribe();

			listener5.subscribe();

			while (true) {
				Thread.sleep(100);
				irefcount = irefcount + 1;
				if (irefcount == 30) {
					irefcount = 0;

				}
			}
		} catch (ManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
