/*
 * 
 */
package eu.esponder.df.eventhandler;

import eu.esponder.event.model.ESponderEvent;


// TODO: Auto-generated Javadoc
/**
 * The Interface SensorMeasurmentEventHandlerService.
 */
public interface SecurityEventHandlerService extends
		dfEventHandlerService {
	
	/**
	 * Process event.
	 *
	 * @param pEvent the event
	 */
	public void ProcessEvent(ESponderEvent<?> pEvent);
}
