/*
 * 
 */
package eu.esponder.df.ruleengine.core;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//import org.apache.commons.codec.EncoderException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import eu.esponder.exception.EsponderCheckedException;


// TODO: Auto-generated Javadoc
/**
 * The Class RestGetClient.
 */
public class RestGetClient {

	/** The xml request type header. */
	private String XML_REQUEST_TYPE_HEADER = "application/xml";
	
	/** The json request type header. */
	private String JSON_REQUEST_TYPE_HEADER = "application/json";
	//private String TEXT_REQUEST_TYPE_HEADER = "application/plaintext";
	/** The text request type header. */
	private String TEXT_REQUEST_TYPE_HEADER = "text/plain";
	
	/** The resource uri. */
	private String resourceURI = "";

	/**
	 * Instantiates a new rest get client.
	 *
	 * @param resourceURI the resource uri
	 */
	public RestGetClient(String resourceURI) {
		
		//System.out.println("Guvnor URL : "+resourceURI);
		this.resourceURI = resourceURI;
	}

	/**
	 * Gets the xml.
	 *
	 * @return the xml
	 */
	public String getXML() {
		return get(XML_REQUEST_TYPE_HEADER);
	}
	
	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return get(TEXT_REQUEST_TYPE_HEADER);
	}

	/**
	 * Gets the json.
	 *
	 * @return the json
	 */
	public String getJSON() {
		return get(JSON_REQUEST_TYPE_HEADER);
	}

	/**
	 * Gets the.
	 *
	 * @param serviceType the service type
	 * @return the string
	 */
	@SuppressWarnings("unused")
	public String get(String serviceType) {
		String output = "";
		String outputResponse = "";
		DefaultHttpClient httpClient = null;
		try {

			httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet(resourceURI);
			
			//String szAuthorization = "guest" + ":" + "";
			//org.apache.commons.codec.binary.Base32 Base64Encoder = new org.apache.commons.codec.binary.Base32();
			
			String encoding = null;
			
				String ss="guest" + ":" + "guest";
				encoding =  org.apache.commons.codec.binary.Base64.encodeBase64(ss.getBytes()).toString();
			
			//System.out.println("-->>> Authorization"+"Basic "+encoding);
			
			/////////////////////////////////////////////////////////////////////////////////////////////
			//FIXME : DANGER 
			//DANGER ONLY WORKING WITH GUEST> NEED TO FIND DIFFERENCE BETWEEN CXF AND APACHE ENCODING
			getRequest.addHeader("Authorization","Basic Z3Vlc3Q6");
			////////////////////////////////////////////////////////////////////////////////////////////
			
			if (serviceType.equalsIgnoreCase(TEXT_REQUEST_TYPE_HEADER)){}else{
			getRequest.addHeader("accept", serviceType);
			}
			//System.out.println("URL - "+getRequest);
			HttpResponse response = httpClient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));

			//System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				//System.out.println(output);
				outputResponse += output+"\n"; 
			}
			//System.out.println("Output from Server FINISHED");


		} catch (ClientProtocolException e) {

			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());

		} catch (IOException e) {

			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
		} finally {
			if (httpClient != null) {
				httpClient.getConnectionManager().shutdown();
			}
		}
		return outputResponse;
	}

}
