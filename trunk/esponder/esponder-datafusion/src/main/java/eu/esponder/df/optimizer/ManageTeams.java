package eu.esponder.df.optimizer;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;



import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorCMDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.crisis.resource.ActorICDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO;
import eu.esponder.dto.model.crisis.resource.OCEocDTO;
import eu.esponder.dto.model.crisis.resource.OCMeocDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.OrganisationDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.category.EquipmentCategoryDTO;
import eu.esponder.dto.model.crisis.resource.category.PersonnelCategoryDTO;
import eu.esponder.dto.model.crisis.resource.sensor.ActivitySensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BreathRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.CarbonDioxideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.CarbonMonoxideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.EnvironmentTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HeartBeatRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HydrogenSulfideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LPSSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.MethaneSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.OxygenSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.crisis.view.VoIPURLDTO;
import eu.esponder.dto.model.snapshot.sensor.config.StatisticsConfigDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementUnitEnumDTO;
import eu.esponder.dto.model.type.EquipmentTypeDTO;
import eu.esponder.dto.model.type.RankTypeDTO;
import eu.esponder.dto.model.type.SensorTypeDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.servicemanager.ESponderResource;

public class ManageTeams extends ESponderResource {

	public ManageTeams() throws NamingException {
		super();
		// TODO Auto-generated constructor stub
	}


	public ResultListDTO CreateTeam(int imembers, String title,
			String incidentCommanderTitle, String personnelTitle, String host,
			String path, String protocol,String meocTitle) {

		List plist=new ArrayList();
		
		try {
			ActorICDTO IC = actorService
					.findICByTitleRemote(incidentCommanderTitle);
			
			System.out.println("IC Find : "+IC.getId());
			
			CreatePersonnelWithOptions(new Long(14),true, "RankType1", "Organisation1", personnelTitle
					, "FRC", personnelTitle+ "FRC");
			
			
			ActorFRCDTO firstChief = CreateFRC(IC, title + " FRC",
					personnelTitle + "FRC", host, path+"FRC", protocol);
			plist.add(firstChief);
			
			CreateUsers(title + " FRC", title + "USR", title + "PWD",
					firstChief.getId(), "FRC",meocTitle);
			CreateFRCEquipmentDTO(title + " FRC");
			//+" WF"
			CreateSensors(title + " FRC"+" WF");
			for (int i = 0; i < imembers - 1; i++) {
				CreatePersonnelWithOptions(new Long(13),true, "RankType1", "Organisation1", personnelTitle
						, "FR"+i, personnelTitle+ "FR"+i);
				
				ActorFRDTO firstResponder = CreateFR(firstChief, title
						+ " FR" + i, personnelTitle + "FR" + i, host, path+"FR"+i,
						protocol);
				plist.add(firstResponder);
				CreateUsers(title + " FR" + i, title + "USR", title + "PWD",
						firstResponder.getId(), "FR",meocTitle);
				CreateFREquipmentDTO(title + " FR" + i);
				CreateSensors(title + " FR" + i+" WF");
			}

			FRTeamDTO frTeam1 = new FRTeamDTO();
			frTeam1.setFrchief(firstChief.getId());
			frTeam1.setTitle(title + " Team 1");
			frTeam1.setIncidentCommander(IC.getId());

			frTeam1 = (FRTeamDTO) genericService.createEntityRemote(frTeam1,
					new Long(1));

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResultListDTO(plist);
	}

	private void CreatePersonnelWithOptions(Long CatID,Boolean availability,
			String rankType, String organisationTitle, String firstName,
			String lastName, String personnelTitle)
	{
		PersonnelCategoryDTO personnelCategory1;
		
		try {
			personnelCategory1 = (PersonnelCategoryDTO) genericService.getEntityDTO(PersonnelCategoryDTO.class, CatID);
			
			PersonnelDTO personnelDTOCM = createPersonnel(availability,
					rankType, organisationTitle, firstName,
					lastName, personnelTitle, ResourceStatusDTO.AVAILABLE, personnelCategory1);
			

		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}
		
	}
	
	private PersonnelDTO createPersonnel(Boolean availability,
			String rankType, String organisationTitle, String firstName,
			String lastName, String personnelTitle, ResourceStatusDTO status,
			PersonnelCategoryDTO personnelCategory)
			throws ClassNotFoundException {

		RankTypeDTO rankTypeDTO = (RankTypeDTO) typeService
				.findDTOByTitle(rankType);
		OrganisationDTO organisationDTO = organisationService
				.findDTOByTitle(organisationTitle);
		if (rankTypeDTO != null && organisationDTO != null) {
			PersonnelDTO personnelDTO = new PersonnelDTO();
			personnelDTO.setAvailability(availability);
			personnelDTO.setFirstName(firstName);
			personnelDTO.setLastName(lastName);
			personnelDTO.setTitle(personnelTitle);
			personnelDTO.setOrganisation(organisationDTO);
			personnelDTO.setRank(rankTypeDTO);
			personnelDTO.setStatus(status);
			personnelDTO.setPersonnelCategory(personnelCategory);
			return personnelService.createPersonnelRemote(personnelDTO,
					this.userID);
		} else {
			// ESponderLogger.debug(this.getClass(),
			// "Unknown rank type or organisation passed for personnel creation");
			return null;
		}

	}

	private void CreateUsers(String title, String username, String password,
			Long ActorID, String Type,String meocTitle) {
		int iRole = 0;

		if (Type.equalsIgnoreCase("CM"))
			iRole = 4;
		if (Type.equalsIgnoreCase("IC"))
			iRole = 3;
		if (Type.equalsIgnoreCase("FR"))
			iRole = 2;
		if (Type.equalsIgnoreCase("FRC"))
			iRole = 1;

		ESponderUserDTO pki = new ESponderUserDTO();
		pki.setTitle(title);
		pki.setStatus(ResourceStatusDTO.AVAILABLE);
		pki.setActorID(ActorID);
		pki.setPassword(password);
		pki.setUsername(username);
		pki.setPkikey("Auto Generated");
		pki.setRasberryip("Define IP");
		pki.setActorRole(iRole);// pki module
		userService.createUserRemote(pki, null);
		
		AssociateUserWithMEOC(title,meocTitle);
	}
	
	private void AssociateUserWithMEOC(String userTitle,String meocTitle)
	{
		ESponderUserDTO user = actorService.findESponderUserByTitleRemote(userTitle);
		
		
		
		OCMeocDTO meoc = (OCMeocDTO) operationsCentreService.findMeocByTitleRemote(meocTitle);
		
		
		user.setOperationsCentre(meoc);
		
		
		actorService.updateEsponderUserRemote(user, userID);
		
	}
	
	private void AssociateUserWithEOC(String userTitle,String eocTitle)
	{
		ESponderUserDTO user = actorService.findESponderUserByTitleRemote(userTitle);
		
		
		
		OCEocDTO eoc = (OCEocDTO) operationsCentreService.findEocByTitleRemote(eocTitle);
		
		
		user.setOperationsCentre(eoc);
		
		
		actorService.updateEsponderUserRemote(user, userID);
		
	}
	
	
	public ActorCMDTO CreateCrisisManager(String eocTitle,String cmTitle,String personnelTitle, String host, String path, String protocol)
	{
		OCEocDTO eocParent = (OCEocDTO) operationsCentreService
				.findEocByTitleRemote(eocTitle);
		
		ActorCMDTO crisisManager = (ActorCMDTO) createActorDTO(ActorCMDTO.class,eocParent, cmTitle, null,
				personnelTitle, host, path, protocol);
		
		eocParent.setCrisisManager(crisisManager.getId());
		operationsCentreService.updateEOCRemote(eocParent, userID);
		
		return crisisManager;
	}
	
	public ActorICDTO CreateIncidentCommander(ActorCMDTO crisisManager,String meocTitle,String icTitle,String personnelTitle, String host, String path, String protocol)
	{
		OCMeocDTO meocParent = (OCMeocDTO) operationsCentreService
				.findMeocByTitleRemote(meocTitle);
		 
		ActorICDTO incidentCommander = (ActorICDTO) createActorDTO(ActorICDTO.class, meocParent, icTitle, crisisManager, 
				personnelTitle, host, path, protocol);
		
		meocParent.setIncidentCommander(incidentCommander.getId());

		operationsCentreService.updateMEOCRemote(meocParent, userID);
		
		return incidentCommander;
	}
	
	public ActorFRCDTO CreateFRC(ActorICDTO incidentCommander,String frcTitle,String personnelTitle, String host, String path, String protocol)
	{
		ActorFRCDTO firstChief = (ActorFRCDTO) createActorDTO(ActorFRCDTO.class, null,frcTitle, incidentCommander, 
				personnelTitle, host, path, protocol);
		
		return firstChief;
	}

	public ActorFRDTO CreateFR(ActorFRCDTO firstChief,String frTitle,String personnelTitle, String host, String path, String protocol)
	{
		System.out.println("--- Personnel Title : "+personnelTitle);
		ActorFRDTO subordinate = (ActorFRDTO) createActorDTO(ActorFRDTO.class, null, frTitle, firstChief,
				personnelTitle, host, path, protocol);
		
		return subordinate;
	}
	
	private ActorDTO createActorDTO(Class<? extends ActorDTO> actorClass,OperationsCentreDTO oc,  String title,
			ActorDTO supervisor, 
			String personnelTitle, String host, String path, String protocol) {

		System.out.println("-- P Title : "+personnelTitle);
		
		if(personnelService==null)
			System.out.println("Service Error");
			
		PersonnelDTO personnelDTO = personnelService
				.findPersonnelByTitleRemote(personnelTitle);
		
		
		System.out.println("-- After Find : "+personnelDTO.getId());
		if (personnelDTO != null) {
			
			if(actorClass == ActorCMDTO.class) {
				ActorCMDTO actorDTO = new ActorCMDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				actorDTO.setOperationsCentre(oc.getId());
				return actorService.createCrisisManagerRemote(actorDTO, this.userID);
			}
			else if(actorClass == ActorICDTO.class) {
				ActorICDTO actorDTO = new ActorICDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				actorDTO.setCrisisManager(supervisor.getId());
				actorDTO.setOperationsCentre(oc.getId());
				return actorService.createIncidentCommanderRemote(actorDTO, this.userID);
			}
			else if(actorClass == ActorFRCDTO.class) {
				ActorFRCDTO actorDTO = new ActorFRCDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				return actorService.createFRChiefRemote(actorDTO, this.userID);
			} 
			else if(actorClass == ActorFRDTO.class) {
				ActorFRDTO actorDTO = new ActorFRDTO();
				actorDTO.setTitle(title);
				actorDTO.setStatus(ResourceStatusDTO.AVAILABLE);
				actorDTO.setVoIPURL(createVoIPURLDTO(host, path, protocol));
				actorDTO.setPersonnel(personnelDTO);
				actorDTO.setFRChief(supervisor.getId());
				return actorService.createFRRemote(actorDTO, this.userID);
			}
		} else {
			//ESponderLogger.debug(this.getClass(),
				//	"Requested Personnel cannot be found.");
			return null;
		}
		return null;

	}

	private VoIPURLDTO createVoIPURLDTO(String host, String path,
			String protocol) {
		VoIPURLDTO voipURL = new VoIPURLDTO();
		voipURL.setHost(host);
		voipURL.setPath(path);
		voipURL.setProtocol(protocol);
		return voipURL;
	}
	
	public void CreateFRCEquipmentDTO(String frcTitle) throws ClassNotFoundException {
		
		EquipmentTypeDTO wimax = (EquipmentTypeDTO) typeService.findDTOByTitle("FRU WiMAX");
		EquipmentTypeDTO wifi = (EquipmentTypeDTO) typeService.findDTOByTitle("FRU WiFi");
		
		ActorFRCDTO firstFRC = actorService.findFRCByTitleRemote(frcTitle);
		
		
		createEquipmentDTO(wimax, frcTitle+" WM", firstFRC);
		createEquipmentDTO(wifi, frcTitle+" WF", firstFRC);
		
	}


	public void CreateFREquipmentDTO(String frTitle) throws ClassNotFoundException {
		
		EquipmentTypeDTO wimax = (EquipmentTypeDTO) typeService.findDTOByTitle("FRU WiMAX");
		EquipmentTypeDTO wifi = (EquipmentTypeDTO) typeService.findDTOByTitle("FRU WiFi");
		
		ActorFRDTO firstFR = actorService.findFRByTitleRemote(frTitle);
		
		
		createEquipmentDTO(wimax, frTitle+" WM", firstFR);
		createEquipmentDTO(wifi, frTitle+" WF", firstFR);
		
	}


	private EquipmentDTO createEquipmentDTO(EquipmentTypeDTO type, String title, FirstResponderActorDTO actor) {
			
			EquipmentDTO equipment = new EquipmentDTO();		
			EquipmentCategoryDTO equipmentCategory = resourceCategoryService.findEquipmentCategoryDTOByType(type);
			equipment.setEquipmentCategoryId(equipmentCategory.getId());
			equipment.setTitle(title);
			equipment.setStatus(ResourceStatusDTO.AVAILABLE);
			equipment.setActor(actor.getId());
			return equipmentService.createEquipmentRemote(equipment, this.userID);
		}
	
	public ResultListDTO CreateSensors(String actorTitle) throws ClassNotFoundException {

		List<SensorDTO> pResults = new ArrayList<SensorDTO>();
		
		SensorTypeDTO activitySensorType = (SensorTypeDTO) typeService.findDTOByTitle("ACT");
		SensorTypeDTO bodyTempSensorType = (SensorTypeDTO) typeService.findDTOByTitle("BODY_TEMP");
		SensorTypeDTO breathRateSensorType = (SensorTypeDTO) typeService.findDTOByTitle("BR");
		SensorTypeDTO heartbeatSensorType = (SensorTypeDTO) typeService.findDTOByTitle("HB");
		SensorTypeDTO envTempSensorType = (SensorTypeDTO) typeService.findDTOByTitle("ENV_TEMP");
		SensorTypeDTO gasSensorType = (SensorTypeDTO) typeService.findDTOByTitle("GAS");
		SensorTypeDTO methaneSensorType = (SensorTypeDTO) typeService.findDTOByTitle("METHANE");
		SensorTypeDTO carbonMonoxideSensorType = (SensorTypeDTO) typeService.findDTOByTitle("CARBONMONOXIDE");
		SensorTypeDTO carbonDioxideSensorType = (SensorTypeDTO) typeService.findDTOByTitle("CARBONDIOXIDE");
		SensorTypeDTO oxygenSensorType = (SensorTypeDTO) typeService.findDTOByTitle("OXYGEN");
		SensorTypeDTO hydrogenSulfideSensorType = (SensorTypeDTO) typeService.findDTOByTitle("HYDROGENSULFIDE");
		SensorTypeDTO lpsSensorType = (SensorTypeDTO) typeService.findDTOByTitle("LPS");
		SensorTypeDTO locationSensorType = (SensorTypeDTO) typeService.findDTOByTitle("LOCATION");

		if(activitySensorType == null || bodyTempSensorType == null || breathRateSensorType == null || heartbeatSensorType == null || 
				envTempSensorType == null || gasSensorType == null || lpsSensorType == null || locationSensorType == null) {



		}
		else {

			EquipmentDTO firstFRCEquipment = equipmentService.findByTitleRemote(actorTitle);
	
			pResults.add(createSensorDTO(activitySensorType, actorTitle+" ACT.", firstFRCEquipment, "Activity"));
			pResults.add(createSensorDTO(bodyTempSensorType, actorTitle+" BTEMP.", firstFRCEquipment, "BdTemp"));
			pResults.add(createSensorDTO(breathRateSensorType, actorTitle+" BR.", firstFRCEquipment, "BR"));
			pResults.add(createSensorDTO(heartbeatSensorType, actorTitle+" HB.", firstFRCEquipment, "HB"));
			pResults.add(createSensorDTO(envTempSensorType, actorTitle+" ENVTEMP.", firstFRCEquipment, "EnvTemp"));
			pResults.add(createSensorDTO(methaneSensorType, actorTitle+" METH.", firstFRCEquipment, "METHANE"));
			pResults.add(createSensorDTO(carbonMonoxideSensorType, actorTitle+" CO.", firstFRCEquipment, "CARBONMONOXIDE"));
			pResults.add(createSensorDTO(carbonDioxideSensorType, actorTitle+" CO2.", firstFRCEquipment, "CARBONDIOXIDE"));
			pResults.add(createSensorDTO(oxygenSensorType, actorTitle+" O2.", firstFRCEquipment, "OXYGEN"));
			pResults.add(createSensorDTO(hydrogenSulfideSensorType, actorTitle+" H2S.", firstFRCEquipment, "HYDROGENSULFIDE"));
			pResults.add(createSensorDTO(lpsSensorType, actorTitle+" LPS.", firstFRCEquipment, "LPS"));
			pResults.add(createSensorDTO(locationSensorType, actorTitle+" LOC.", firstFRCEquipment, "LOC"));


		}

		CreateSensorsConfiguration(actorTitle);
		
		return new ResultListDTO(pResults);
	}
	
	
	
	private void CreateSensorsConfiguration(String actorTitle) {		

		SensorDTO firstFRCTemperature = sensorService.findSensorByTitleRemote(actorTitle+" BTEMP.");
		SensorDTO firstFRCHeartbeat = sensorService.findSensorByTitleRemote(actorTitle+" HB.");
		SensorDTO firstFRCLocation = sensorService.findSensorByTitleRemote(actorTitle+" LOC.");



		/***************************************************************************************************/
		createStatisticConfigDTO(firstFRCTemperature, new Long(5000), MeasurementStatisticTypeEnumDTO.MEAN);

		createStatisticConfigDTO(firstFRCHeartbeat, new Long(5000), MeasurementStatisticTypeEnumDTO.MAXIMUM);

		createStatisticConfigDTO(firstFRCLocation, new Long(10000), MeasurementStatisticTypeEnumDTO.MEAN);

		/***************************************************************************************************/



	}

	private SensorDTO createSensorDTO(SensorTypeDTO type, String title, EquipmentDTO equipment, String label) throws ClassNotFoundException {
		SensorDTO sensor = null;

		if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("ACT")).getTitle())) {
			sensor = new ActivitySensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		}
		else if(type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("BODY_TEMP")).getTitle())) {
			sensor = new BodyTemperatureSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("HB")).getTitle())) {
			sensor = new HeartBeatRateSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("BR")).getTitle())) {
			sensor = new BreathRateSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("ENV_TEMP")).getTitle())) {
			sensor = new EnvironmentTemperatureSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES_CELCIUS);
		} /*else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("GAS")).getTitle())) {
			sensor = new GasSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		}*/ else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("METHANE")).getTitle())) {
			sensor = new MethaneSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("CARBONMONOXIDE")).getTitle())) {
			sensor = new CarbonMonoxideSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("CARBONDIOXIDE")).getTitle())) {
			sensor = new CarbonDioxideSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("OXYGEN")).getTitle())) {
			sensor = new OxygenSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("HYDROGENSULFIDE")).getTitle())) {
			sensor = new HydrogenSulfideSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.BPM);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("LPS")).getTitle())) {
			sensor = new LPSSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES);
		} else if (type.getTitle().equals(((SensorTypeDTO) typeService.findDTOByTitle("LOCATION")).getTitle())) {
			sensor = new LocationSensorDTO();
			sensor.setMeasurementUnit(MeasurementUnitEnumDTO.DEGREES);
		}
		else
			System.out.println("Unknown sensor type when trying to create one in test execution");
		if(sensor!=null) {
			sensor.setType(type.getTitle());

			sensor.setTitle(title);
			sensor.setStatus(ResourceStatusDTO.ALLOCATED);
			sensor.setEquipmentId(equipment.getId());
			//		sensor.setLabel(label);
			return sensorService.createSensorRemote(sensor, this.userID);
		}
		return null;
		
	}
	
	private StatisticsConfigDTO createStatisticConfigDTO(SensorDTO sensor, Long samplingPeriodMs, MeasurementStatisticTypeEnumDTO type) {
		StatisticsConfigDTO statisticsConfig = new StatisticsConfigDTO();
		statisticsConfig.setMeasurementStatisticType(type);
		statisticsConfig.setSamplingPeriodMilliseconds(samplingPeriodMs);
		statisticsConfig.setSensorId(sensor.getId());
		return sensorService.createStatisticConfigRemote(statisticsConfig, this.userID);
	}
	
}
