/*
 * 
 */
package eu.esponder.df.eventhandler;



import java.util.List;

import javax.ejb.Remote;

import eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType;
import eu.esponder.event.model.ESponderEvent;


// TODO: Auto-generated Javadoc
/**
 * The Interface dfEventHandlerRemoteService.
 */
@Remote
public interface dfEventHandlerRemoteService extends dfEventHandlerService{

	/* (non-Javadoc)
	 * @see eu.esponder.df.eventhandler.dfEventHandlerService#AddDTOObjects(java.lang.Object)
	 */
	public void AddDTOObjects(Object fact);
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.eventhandler.dfEventHandlerService#AddObjects(java.lang.Object)
	 */
	public void AddObjects(Object fact);
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.eventhandler.dfEventHandlerService#LoadKnowledge()
	 */
	public void LoadKnowledge();
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.eventhandler.dfEventHandlerService#ProcessRules()
	 */
	public void ProcessRules();
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.eventhandler.dfEventHandlerService#SetRuleEngineType(eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType, java.lang.String)
	 */
	public void SetRuleEngineType(RuleEngineType dType,String szPackageNameOrFlowName);
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.eventhandler.dfEventHandlerService#SetRuleEngineType(eu.esponder.df.eventhandler.bean.dfEventHandlerBean.RuleEngineType, java.lang.String, java.lang.String)
	 */
	public void SetRuleEngineType(RuleEngineType dType,String szDSLName,String szDSLRName);
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.eventhandler.dfEventHandlerService#CreateSensorSnapshot(eu.esponder.event.model.ESponderEvent)
	 */
	public List<Object> CreateSensorSnapshot(ESponderEvent<?> pEvent);
}
