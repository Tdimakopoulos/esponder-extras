/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package udp.server.event.publish;

import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.event.datafusion.query.ESponderDFQueryRequestEvent;
import eu.esponder.event.mobile.MobileFileNotifierEvent;
import eu.esponder.osgi.service.event.ESponderEventPublisher;
import java.util.Date;

/**
 *
 * @author tdim
 */
public class PublishEventMeasurment {
    
    public void PublishM(String measurments,String imei)
    {
        
         
        ESponderEventPublisher<ESponderDFQueryRequestEvent> publisher = 
					new ESponderEventPublisher<ESponderDFQueryRequestEvent>(ESponderDFQueryRequestEvent.class);
			
                        System.out.println("Publish Sensors Reading Event  : "+measurments);
        
			ESponderDFQueryRequestEvent event = new ESponderDFQueryRequestEvent();
                        
                        event.setIMEI(imei);
                        
                        event.setQueryID(new Long(698));
                        event.setRequestID((new Date()).getTime());
			
                        
                        QueryParamsDTO pParams=new QueryParamsDTO().with("sensorMeasurements", measurments);
                        
                        //pParams.and("sensorMeasurements", measurments);
                        event.setParams(pParams);
			
                        
                                
			try {
				publisher.publishEvent(event);
			} catch (Exception e) {
				System.out.println("Error on Publish event : "+e.getMessage());
			}
			publisher.CloseConnection();
    }
}
