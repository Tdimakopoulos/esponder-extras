/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facebook.info.org;

import com.restfb.FacebookClient;
import com.restfb.types.User;

/**
 *
 * @author tom
 */
public class fbuser {
    
    private User user=null;
    private FacebookClient facebookClient=null;
    private String fbObject="me";
    
    public void GetUser()
    {
        setUser(getFacebookClient().fetchObject(getFbObject(), User.class));
    }


    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the facebookClient
     */
    public FacebookClient getFacebookClient() {
        return facebookClient;
    }

    /**
     * @param facebookClient the facebookClient to set
     */
    public void setFacebookClient(FacebookClient facebookClient) {
        this.facebookClient = facebookClient;
    }

    /**
     * @return the fbObject
     */
    public String getFbObject() {
        return fbObject;
    }

    /**
     * @param fbObject the fbObject to set
     */
    public void setFbObject(String fbObject) {
        this.fbObject = fbObject;
    }
        
}
