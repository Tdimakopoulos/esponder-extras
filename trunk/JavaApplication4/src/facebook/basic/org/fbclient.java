/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facebook.basic.org;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;

/**
 *http://restfb.com/#fetching-single-objects
 * @author tom
 */
public class fbclient {
    private String szToken="AAAFsYpgIzxcBAONOf3EiA37ZC1F1HFsV52h3iPRAwSZAy47AxargJE43x9NZA7Ryu8y3cCZCx2ZAeO0lSRfUbxmTKQbNBo12s8nkBprTYBAZDZD";
    
    public FacebookClient GetFBClient()
    {
        FacebookClient facebookClient = new DefaultFacebookClient(getSzToken());
        return facebookClient;
    }

    /**
     * @return the szToken
     */
    public String getSzToken() {
        return szToken;
    }

    /**
     * @param szToken the szToken to set
     */
    public void setSzToken(String szToken) {
        this.szToken = szToken;
    }
    
}
