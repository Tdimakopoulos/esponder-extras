/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facebook.main.org;

import com.restfb.FacebookClient;
import facebook.basic.org.fbclient;
import facebook.publish.org.fbpublishmessage;

/**
 *
 * @author tom
 */
public class Executor {

    public static void main(String[] args) {
        fbclient pClient=new fbclient();
        fbpublishmessage pfbPublishMessage= new fbpublishmessage();
        
        FacebookClient pfbClient=pClient.GetFBClient();
        pfbPublishMessage.setFacebookClient(pfbClient);
        pfbPublishMessage.setSzText("Hello from Java Status Server Auto Update");
        pfbPublishMessage.PublishText();
    }
}
