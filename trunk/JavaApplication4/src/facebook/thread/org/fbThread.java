/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facebook.thread.org;

import com.restfb.FacebookClient;
import facebook.basic.org.fbclient;
import facebook.publish.org.fbpublishmessage;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tom
 */
public class fbThread extends Thread {

    private int countDown = 5;
    private static int threadCount = 0;

    public fbThread() {
        super("" + ++threadCount); // Store the thread name
        start();
    }

    public String toString() {
        return "#" + getName() + ": " + countDown;
    }

    public void run() {
        while (true) {
        //    System.out.println(this);
            
            fbclient pClient=new fbclient();
        fbpublishmessage pfbPublishMessage= new fbpublishmessage();
        
        FacebookClient pfbClient=pClient.GetFBClient();
        pfbPublishMessage.setFacebookClient(pfbClient);
        pfbPublishMessage.setSzText(this.toString());//"Hello from Java Status Server Auto Update");
        pfbPublishMessage.PublishText();
        
            if (--countDown == 0) {
                return;
            }
            try {
                sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(fbThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
  public static void main(String[] args) throws InterruptedException {
    for(int i = 0; i < 5; i++)
    {
      new fbThread();
      sleep(300);
    }
  }
}
