package org.jumpmind.db.sql;

public interface ISqlStatementSource {

    public String readSqlStatement();
}
