package org.jumpmind.db.model;

final public class ColumnTypes {

    private ColumnTypes() {
    }
    
    public static final int SQLXML = 2009;
    public static final int NCHAR = -15;
    public static final int NCLOB = 2011;
    public static final int NVARCHAR = -9;
    public static final int LONGNVARCHAR = -16;
    
}
