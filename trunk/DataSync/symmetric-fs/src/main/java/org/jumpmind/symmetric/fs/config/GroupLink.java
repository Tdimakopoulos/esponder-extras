package org.jumpmind.symmetric.fs.config;

public class GroupLink {

    protected String clientGroupId;
    protected String serverGroupId;
    
    public String getClientGroupId() {
        return clientGroupId;
    }
    
    public String getServerGroupId() {
        return serverGroupId;
    }
    
}
