package eu.esponder.ws.client.logic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.type.CrisisDisasterTypeDTO;
import eu.esponder.dto.model.type.CrisisFeatureTypeDTO;
import eu.esponder.ws.client.query.QueryManager;

public class CrisisTypeQueryManager {

	List<CrisisDisasterTypeDTO> pCrisisDisasterTypes = new ArrayList<CrisisDisasterTypeDTO>();
	List<CrisisFeatureTypeDTO> pCrisisFeatureTypes = new ArrayList<CrisisFeatureTypeDTO>();
	
	public void LoadAllCrisisTypes() throws JsonParseException, JsonMappingException, IOException {
		QueryManager pMan = new QueryManager();
		ResultListDTO pre = pMan.getAllTypes("&p^y7*p8tj7%oca^%%?|9v|my5po>}j>0x}uv%8^|()3jo!&n{#f!y&3!:$2t>wd&5*!u@lluiv1z3$iguokp*>&gb?2@b{#s0xn");
		
		for (int i = 0; i < pre.getResultList().size(); i++) {
			if (pre.getResultList().get(i) instanceof CrisisDisasterTypeDTO) {
				CrisisDisasterTypeDTO pitem=(CrisisDisasterTypeDTO)pre.getResultList().get(i);
				pCrisisDisasterTypes.add(pitem);
			}
			if (pre.getResultList().get(i) instanceof CrisisFeatureTypeDTO) {
				CrisisFeatureTypeDTO pitem=(CrisisFeatureTypeDTO)pre.getResultList().get(i);
				pCrisisFeatureTypes.add(pitem);
			}
		}
	}

        public CrisisDisasterTypeDTO FindCrisisDisasterWithTitle(String szTitle)
        {
            
            for (int i = 0; i < pCrisisDisasterTypes.size(); i++) {
            
                if (pCrisisDisasterTypes.get(i).getTitle().equalsIgnoreCase(szTitle))
                {
                    return pCrisisDisasterTypes.get(i);
                }
            }
            return null;
        }
        
        public CrisisFeatureTypeDTO FindCrisisDisasterFeatureWithTitle(String szTitle)
        {
            
            for (int i = 0; i < pCrisisFeatureTypes.size(); i++) {
            
                if (pCrisisFeatureTypes.get(i).getTitle().equalsIgnoreCase(szTitle))
                {
                    return pCrisisFeatureTypes.get(i);
                }
            }
            return null;
        }
        
	public List<CrisisDisasterTypeDTO> getpCrisisDisasterTypes() {
		return pCrisisDisasterTypes;
	}

	public void setpCrisisDisasterTypes(
			List<CrisisDisasterTypeDTO> pCrisisDisasterTypes) {
		this.pCrisisDisasterTypes = pCrisisDisasterTypes;
	}

	public List<CrisisFeatureTypeDTO> getpCrisisFeatureTypes() {
		return pCrisisFeatureTypes;
	}

	public void setpCrisisFeatureTypes(
			List<CrisisFeatureTypeDTO> pCrisisFeatureTypes) {
		this.pCrisisFeatureTypes = pCrisisFeatureTypes;
	}
}
