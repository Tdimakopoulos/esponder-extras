/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.scr.server;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tdim
 */
public class scriptgenerator {

    PrintWriter myFileP = null;
    List<String> pfrus = new ArrayList();
    List<String> ptimes = new ArrayList();
    List<String> pids = new ArrayList();
    List<String> pfilenames = new ArrayList();
    int imax;

    public void setMax(int ipmax) {
        imax = ipmax;
    }

    public void CreateFRC() {
        CreateFRCFirst();
        CreatefrcComplex();
        CreateFRCEnd();
    }

    private void CreateFRCFirst() {
        myFileP.println("var avgsensors = require('../avg');");
    }

    private void CreateFRCEnd() {
        myFileP.println("exports.findByIdlatest = function (req, res) {");
        myFileP.println("var id = req.params.id;");
        myFileP.println("var id2 = req.params.id2;");

        myFileP.println("var id33 = 0;");
        myFileP.println("avgsensors.GetAvg(id, id2, function (id33) {");
        //console.log(id33);    
        myFileP.println("});");

        //console.log("save "+id33);    
        myFileP.println("res.send(\"AVG : \");");
        myFileP.println("};");

        myFileP.println("exports.findByIdavg = function (req, res) {");
        myFileP.println("var id = req.params.id;");
        myFileP.println("var id2 = req.params.id2;");


        myFileP.println("var fs = require('fs'); // file system module");

        myFileP.println("fs.readFile(id + id2, 'utf-8', function (err, data) {");
        myFileP.println("if (err) throw err;");

        myFileP.println("var lines = data.trim().split('\n');");
        myFileP.println("var ilength = lines.length;");

        myFileP.println("var t = 0;");
        myFileP.println("if (ilength > 3) {");
        myFileP.println("for (var i = lines.length - 3, length = lines.length; i < length; i++) {");

        myFileP.println("var fields = lines[i].split(',');");
        myFileP.println("t = t + Number(fields[0]);");

        myFileP.println("}");

        myFileP.println("var avg = Number(t) / Number(3);");
        myFileP.println("} else {");
        myFileP.println("for (var i = 0, length = lines.length; i < length; i++) {");

        myFileP.println("                            var fields = lines[i].split(',');");
        myFileP.println("                            t = t + Number(fields[0]);");

        myFileP.println("                        }");

        myFileP.println("                        var avg = Number(t) / Number(i);");
        myFileP.println("                    }");



        myFileP.println("                    res.send({");
        myFileP.println("                        id: req.params.id,");
        myFileP.println("                        time: Number(avg),");
        myFileP.println("                        sensorvalueavg: Number(avg)");
        myFileP.println("                    });");
        myFileP.println("                });");
        myFileP.println("            };");

        myFileP.println("            exports.findByIdmin = function (req, res) {");
        myFileP.println("                res.send({");
        myFileP.println("                    id: req.params.id,");
        myFileP.println("                    name: \"The Name\",");
        myFileP.println("                    description: \"description\"");
        myFileP.println("                });");
        myFileP.println("            };");

        myFileP.println("            exports.findByIdmax = function (req, res) {");
        myFileP.println("                res.send({");
        myFileP.println("                    id: req.params.id,");
        myFileP.println("                    name: \"The Name\",");
        myFileP.println("                    description: \"description\"");
        myFileP.println("                });");
        myFileP.println("            };");
        myFileP.println("            exports.addFRt = function (req, res) {");
        //console.log("call");
        myFileP.println("            }");
        myFileP.println("            exports.addFR = function (req, res) {");
        myFileP.println("                var id = req.params.id;");
        myFileP.println("                var id2 = req.params.id2;");
        myFileP.println("                var id3 = req.params.id3;");
        myFileP.println("                var id4 = req.params.id4;");
        myFileP.println("                //console.log(id);");
        myFileP.println("                console.log(\"Sensor - \" + id2);");
        myFileP.println("                console.log(\"Value - \" + id3);");
        myFileP.println("                console.log(\"Time - \" + id4);");
        myFileP.println("                var fs = require('fs');");
        myFileP.println("                fs.appendFile(id + id2, id3 + ',' + id4 + '\\n', function (err) {");

        myFileP.println("                    if (err) {");

        myFileP.println("                        res.send([{");
        myFileP.println("                            Return: '0'");
        myFileP.println("                        }, {");
        myFileP.println("                            Errors: '1'");
        myFileP.println("                        }, {");
        myFileP.println("                            Save: '0'");
        myFileP.println("                        }]);");
        myFileP.println("                    } else {");

        myFileP.println("                        res.send([{");
        myFileP.println("                            Return: '1'");
        myFileP.println("                        }, {");
        myFileP.println("                            Errors: '0'");
        myFileP.println("                        }, {");
        myFileP.println("                            Save: '1'");
        myFileP.println("                        }]);");
        myFileP.println("                    }");
        myFileP.println("                });");
        myFileP.println("");
        myFileP.println("            };");
    }

    private void CreatefrcComplex() {
        myFileP.println("exports.latestfrteam = function (req, res) {");
        myFileP.println("var fs = require('fs'); // file system module");
        myFileP.println("var sendstr = \"\";");
        myFileP.println("var sendstr2 = \"\";");
        myFileP.println("var sendstr3 = \"\";");
        myFileP.println("var sendstr4 = \"\";");
        myFileP.println("var sendstr5 = \"\";");
        myFileP.println("var sendstr6 = \"\";");
        myFileP.println("var fs = require('fs');");
        for (int i = 0; i < imax; i++) {
            myFileP.println("var " + pfilenames.get(i) + " = fs.readFileSync('./routes/" + pfilenames.get(i) + "').toString();");
        }

        myFileP.print("res.send(");
        myFileP.print("\"" + pfrus.get(0) + "\"");
        myFileP.print("+");
        myFileP.print("\"a\"");
        myFileP.print("+");
        myFileP.print(pfilenames.get(0));

        for (int i = 1; i < imax; i++) {
            myFileP.print("+");
            myFileP.print("\"b\"");
            myFileP.print("+");
            myFileP.print("\"" + pfrus.get(i) + "\"");
            myFileP.print("+");
            myFileP.print("\"a\"");
            myFileP.print("+");
            myFileP.print(pfilenames.get(i));

        }
        myFileP.println(");");
        myFileP.println("};");
    }

    public void OpenFIle(String filename) throws IOException {

        for (int i = 0; i < 300; i++) {
            pids.add(String.valueOf(i));
        }

        for (int i = 0; i < 300; i++) {
            ptimes.add(String.valueOf(5000 - (i * 100)));
        }

        for (int i = 0; i < 300; i++) {
            pfilenames.add("latest" + String.valueOf(i));
        }
//        pfrus.add("4");
//        pfrus.add("5");
//        pfrus.add("6");
//        pfrus.add("7");
//        pfrus.add("8");
//        pfrus.add("9");
LoadFRUS();
        myFileP = new PrintWriter(new FileWriter(filename));
    }

    private void LoadFRUS() throws IOException {
        BufferedReader br = null;
        try {
            
            br = new BufferedReader(new FileReader("C:\\nodejs\\frus.txt"));
            String line;
            while ((line = br.readLine()) != null) {
                pfrus.add(line);
                System.out.println(line);
                imax++;
            }
            
            System.out.println("max : "+imax);
            br.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(scriptgenerator.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(scriptgenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void CloseFile() {
        myFileP.close();
    }

    public void WriteOnFile(String line) {
        myFileP.println(line);
    }

    public void CreateServer() {
        createFirst();
        for (int i = 0; i < imax; i++) {
            CreateNeverCall(pids.get(i), pfrus.get(i), pfilenames.get(i));
        }

        for (int i = 0; i < imax; i++) {
            MakeCaller(pids.get(i), ptimes.get(i));
        }

        CreateEnd();
    }

    public void CreatePackage() {
        myFileP.println("{");
        myFileP.println("\"name\": \"FRC-EMB-APP\",");
        myFileP.println("\"description\": \"ARM Web Services for FRC and FR communication\",");
        myFileP.println("\"version\": \"0.0.1\",");
        myFileP.println("\"private\": true,");
        myFileP.println("\"dependencies\": {");
        myFileP.println("\"express\": \"3.x\"");
        myFileP.println("}");
        myFileP.println("}");
    }

    public void CreateAVG() {
        myFileP.println("exports.GetAvg = function (id, id2, cb) {");

        myFileP.println("var fs = require('fs'); // file system module");
        myFileP.println("var fmin = -1;");
        myFileP.println("var fmax = 0;");
        myFileP.println("fs.readFile(id + id2, 'utf-8', function (err, data) {");
        myFileP.println("if (err) {} else {");

        myFileP.println("var lines = data.trim().split('\\n');");
        myFileP.println("var ilength = lines.length;");
        myFileP.println("var t = 0;");
        myFileP.println("if (ilength > 2) {");
        myFileP.println("for (var i = lines.length - 2, length = lines.length; i < length; i++) {");

        myFileP.println("var fields = lines[i].split(',');");
        myFileP.println("t = t + Number(fields[0]);");
        myFileP.println("var tempd = Number(fields[0]);");
        myFileP.println("var tempa = Number(fmin);");
        myFileP.println("if (fmin == -1)");
        myFileP.println("fmin = tempd;");
        myFileP.println("if (tempa > tempd)");
        myFileP.println("fmin = tempd;");
        myFileP.println("var tempb = Number(fmax);");
        myFileP.println("if (tempb < tempd)");
        myFileP.println("fmax = tempd;");
        myFileP.println("var t2 = fields[1];");
        myFileP.println("}");
        myFileP.println("var i1 = ilength - 2;");
        myFileP.println("var f1 = lines[i1].split(',');");
        myFileP.println("var t1 = f1[1];");
        myFileP.println("var avg = Number(t) / Number(2);");
        myFileP.println("} else {");
        myFileP.println("for (var i = 0, length = lines.length; i < length; i++) {");

        myFileP.println("var fields = lines[i].split(',');");
        myFileP.println("t = t + Number(fields[0]);");
        myFileP.println("var t2 = fields[1];");
        myFileP.println("var tempd = Number(fields[0]);");
        myFileP.println("var tempa = Number(fmin);");
        myFileP.println("if (tempa > tempd)");
        myFileP.println("fmin = tempd;");
        myFileP.println("var tempb = Number(fmax);");
        myFileP.println("if (tempb < tempd)");
        myFileP.println("fmax = tempd;");
        myFileP.println("}");

        myFileP.println("var f1 = lines[0].split(',');");
        myFileP.println("var t1 = f1[1];");
        myFileP.println("var avg = Number(t) / Number(i);");
        myFileP.println("}");
        myFileP.println("avg = avg + \",\" + fmin + \",\" + fmax + \",\" + t1 + \",\" + t2;");
        myFileP.println("cb(avg);");
        myFileP.println("}");
        myFileP.println("});");
        myFileP.println("}");
    }

    private void createFirst() {
        myFileP.println("var express = require('express'),");
        myFileP.println("webservices = require('./routes/frc');");
        myFileP.println("var avgsensors2 = require('./avg');");
        myFileP.println("var app = express();");
    }

    private void CreateEnd() {
        myFileP.println("app.get('/avgfrteam', webservices.latestfrteam);");
        myFileP.println("app.get('/latestfr/:id/:id2', webservices.findByIdlatest);");
        myFileP.println("app.get('/avgfr/:id/:id2', webservices.findByIdavg);");
        myFileP.println("app.get('/minfr/:id/:id2', webservices.findByIdmin);");
        myFileP.println("app.get('/maxfr/:id/:id2', webservices.findByIdmax);");
        myFileP.println("app.post('/addfr/:id/:id2/:id3/:id4', webservices.addFR);");
        myFileP.println("app.listen(3000);");
        myFileP.println("console.log('Listening on port 3000...');");
    }

    private void CreateNeverCall(String id, String actorid, String filename) {
        myFileP.println("function never_call" + id + "() {");

        myFileP.println("var fs = require('fs')");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', '', function () {");
        myFileP.println("console.log('done')");
        myFileP.println("})");
        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd2 = \"0\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd2, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd2 + ',' + id33 + 'a');");
        myFileP.println("});");

        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd22 = \"1\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd22, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd22 + ',' + id33 + 'a');");
        myFileP.println("});");

        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd23 = \"2\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd23, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd23 + ',' + id33 + 'a');");
        myFileP.println("});");

        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd24 = \"3\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd24, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd24 + ',' + id33 + 'a');");
        myFileP.println("});");

        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd25 = \"4\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd25, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd25 + ',' + id33 + 'a');");
        myFileP.println("});");

        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd26 = \"5\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd26, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd26 + ',' + id33 + 'a');");
        myFileP.println("});");

        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd27 = \"6\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd27, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd27 + ',' + id33 + 'a');");
        myFileP.println("});");

        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd28 = \"7\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd28, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd28 + ',' + id33 + 'a');");
        myFileP.println("});");

        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd29 = \"8\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd29, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd29 + ',' + id33 + 'a');");
        myFileP.println("});");

        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd249 = \"9\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd249, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd249 + ',' + id33 + 'a');");
        myFileP.println("});");

        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd2444 = \"10\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd2444, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd2444 + ',' + id33 + 'a');");
        myFileP.println("});");

        myFileP.println("var dd = \"" + actorid + "\";");
        myFileP.println("var dd2455 = \"11\";");
        myFileP.println("avgsensors2.GetAvg(dd, dd2455, function (id33) {");
        myFileP.println("fs.appendFileSync('./routes/" + filename + "', dd2455 + ',' + id33 + 'a');");
        myFileP.println("});");
        myFileP.println("}");

    }

    private void MakeCaller(String id, String time) {
        myFileP.println("var id" + id + " = setInterval(never_call" + id + ", " + time + ");");
    }
}
