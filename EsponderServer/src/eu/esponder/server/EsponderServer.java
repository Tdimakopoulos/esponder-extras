package eu.esponder.server;

import java.io.*;
import java.net.*;



public class EsponderServer {

	public EsponderServer(int port){
		try{
			ServerSocket connectionSocket = new ServerSocket(port);//make a new socket to accept connections on
			//System.out.println("Server Running");
			while (true) {//forever!
				Socket clientSocket = connectionSocket.accept();//wait for a conncetion from a client
				Connection con = new Connection(clientSocket);//make a new connection object
				new Thread(con).start(); //start that threaded object.
			}
		}catch(SocketException e){

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		new EsponderServer(4444);
	}
}
