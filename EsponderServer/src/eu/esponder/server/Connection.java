package eu.esponder.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Vector;


public class Connection implements Runnable
{
	Socket clientSocket;
	
	static final String THE_PASSWORD ="esponder";//the password
	boolean authorised = false;
	
	BufferedReader inFromClient;//buffered input from client
	DataOutputStream outToClient;//data output to client
	
	ServerState state;//the state that the server is in.
		
	public Connection(Socket clientSocket)
	{
		this.clientSocket = clientSocket;//client socket that the client has connceted through
		state = new UnAuthorised();
		try{
			inFromClient =new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			outToClient =new DataOutputStream(clientSocket.getOutputStream());
		}catch(Exception e){}
	}
	
	

	public void run()
	{
		//System.out.println("Client connected @ " +clientSocket.getInetAddress());
		while(!clientSocket.isClosed()){

			try{

			    state.update();
		    }catch(Exception e){}
		}
		//System.out.println("Client disconnected");
		
	}
	
	//interface defines a server state
	private interface ServerState
	{
		public abstract void update();
	}
	
	//when the client has yet ot enter a valid password
	private class UnAuthorised implements ServerState
	{
		public void update() {
			// read a sentence from the client
		    try {
		    	
		    	String clientIp = clientSocket.getInetAddress().getHostAddress();
		    	if(!inBlockList(clientIp)){//if the client's ip is not blocked (in forbidden.txt)
		    		outToClient.flush();
					String password = inFromClient.readLine();//wait for the password from the client
					if(password.equals(THE_PASSWORD)){//if password is correct
						outToClient.writeBytes("001 PASSWD OK\n");
						state = new Authorised();//move to the authorised state
					}else{
						outToClient.writeBytes("002 PASSWD WRONG\n");//otherwise tell the client and stay in this state
					}
		    	}else{
		    		outToClient.writeBytes("003 REFUSED\n");//if blocked, tell the client
		    		state = new Disconnect();//disconnect the client
		    	}
			} catch (IOException e) {
			//	e.printStackTrace();
			}
			
		}
		
		private ArrayList<String> getBlockList()
		{
			FileInputStream fStream;
			ArrayList<String> blocklist = new ArrayList<String>();
			try {
				fStream = new FileInputStream("forbidden.txt");
				BufferedReader in = new BufferedReader(new InputStreamReader(fStream));
	            while (in.ready()) {
	                blocklist.add(in.readLine());//adds each line in the forbidden file to the blocked array list.
	            }
	            in.close();//close the forbidden.txt's reader
			} catch (FileNotFoundException e) {
				return new ArrayList<String>(); //if no block list exists, return a blank list
			} catch (IOException e) {
				//e.printStackTrace();
			}
			return blocklist;// return the blocklist
		}
		
		private boolean inBlockList(String clientIp)
		{
			ArrayList<String> blocklist = getBlockList();
			String clientDomain = clientSocket.getInetAddress().getCanonicalHostName();//attempts to resolve the hostname of the ip address
			if(blocklist.contains(clientIp) || blocklist.contains(clientDomain)){
				return true; //if the ip/cleint domain is in the list, return true
			}else{
				return false;//otherwise, return false
			}
		}
		
	}
	
	private class Authorised implements ServerState
	{
		public void update() {
			
		    try {
				String command = inFromClient.readLine();// read a command from the client
				//if it is a valid command word
				if(command.equals("exit")){
					state = new Disconnect(); //change state to disconnect
				}else if(command.equals("log")){
					state = new TailLogFile(); //move to lit remote files state
				}else if(command.equals("")){
				}
			} catch (IOException e) {
				//e.printStackTrace();
			}	
		}
	}
	
	private class Disconnect implements ServerState
	{
		public void update() {
		    try {
		    	Thread.sleep(4000);//wait 4 seconds
				clientSocket.close(); //close the connection
			} catch (IOException e) {
				//e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			
		}
		
	}
	
	
	private class TailLogFile implements ServerState
	{
		public void update() {
		  
			String fileName="/home/exodus/esponder/jboss-6.1.0/server/default/log/server.log";
			int linesFromEnd=4000;
			Vector atTheEnd = new Vector();
	        RandomAccessFile raf;
			try {
				raf = new RandomAccessFile( fileName , "r");
				// Line size to 100 bytes, shold be a better way to make a guess.
		        int lineSize = 100;
		        // set the file pointer to "linesFromEnd" before  end.
		        long iseek=raf.length() - linesFromEnd * lineSize;
		        
		        if (iseek<=0)
			           iseek=raf.length() - 3000 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 2500 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 2000 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 1800 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 1600 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 1500 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 1300 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 1100 * lineSize;
		        if (iseek<=0)
		           iseek=raf.length() - 1000 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 500 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 400 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 300 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 100 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 50 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 10 * lineSize;
		        if (iseek<=0)
			           iseek=raf.length() - 1 * lineSize;
		        
		        raf.seek(  iseek);
		        // read from that place to end of file
		        
		        String line = "";
		        while( ( line = raf.readLine() ) != null ){
		             atTheEnd.add( line  );
		        }
		        // release resources
		        raf.close();
		        System.out.println("Size -- "+atTheEnd.size());
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
	        
			
			try {
				outToClient.writeBytes("031 REMOTEFILELOG\n");//tells the client that it will receive a remote file list
				
				for(int i=0;i<atTheEnd.size();i++)
				{
					outToClient.writeBytes(atTheEnd.elementAt( i ).toString()+"\n");
		            
				}
				outToClient.writeBytes("032 ENDOFFILELOG\n");//end of remote fiel list.
			} catch (IOException e) {
				//e.printStackTrace();
			}
			state = new Authorised();
			
		}
		
	}
}