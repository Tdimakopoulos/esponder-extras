package eu.esponder.locationlib;

public class MovablePoint  {
    
	public int CORRECTION = 0;
	public static final int SIZE = 8;
    public static final int SIZE2 = SIZE / 2;
    
    
    private double iLog = 0, iLat = 0,iAlt=0;
    
    
    public MovablePoint(double x, double y,double a)
    {
                this.iLog=x;
                this.iLat=y;
                this.iAlt=a;
    }
    
    public double getX() { return iLog; }
    
    public double getY() { return iLat; }
    
    public double getA() { return iAlt; }
    
    public double getMidX() 
    {
    	if (CORRECTION==0)
    		return iLog;
    	else	
    		return iLog + SIZE2; 
    }
    
    public double getMidY() 
    { 
    	if (CORRECTION==0)
    		return iLat;
    	else	
    		return iLat + SIZE2; 
    }
    
    public double getMidA() 
    { 
    	if (CORRECTION==0)
    		return iAlt;
    	else	
    		return iAlt + SIZE2; 
    }
    
}
