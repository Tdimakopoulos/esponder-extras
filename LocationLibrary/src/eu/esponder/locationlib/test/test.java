package eu.esponder.locationlib.test;

import java.util.Vector;

import eu.esponder.locationlib.Mec;
import eu.esponder.locationlib.MovablePoint;


public class test {

	private static Vector<MovablePoint> points = new Vector<MovablePoint>();

	public static int getWidth()
	{
		return 480;
	}
	
	public static int getHeight()
	{
		return 640;
	}
	 public static void createRandPoints(int num)
	    {
	        clearPoints();
	        
	        // create new random points
	        for (int i = 0; i < num; i++) {
				points.add(new MovablePoint((double)(getWidth() / 6 + (2*getWidth() / 3) * Math.random()), 
	                                              (double)(getHeight() / 6 + (2*getHeight() / 3) * Math.random()),(double)10));
			}
	        
	    }
	 
	 public static void clearPoints()
	    {
	        for (int i = points.size(); i > 0; i--)
	        {
	            
	            points.remove(0);
	        }
	    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		clearPoints();
		createRandPoints(10);
		 Mec mec = new Mec(points);
	     mec.calcMec();
	}

}
