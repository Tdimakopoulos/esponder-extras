/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.read.hw;

import java.net.InetAddress;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.event.ResponseListener;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.*;
import org.snmp4j.transport.DefaultUdpTransportMapping;

/**
 *
 * @author MEOC-WS1
 */
public class SNMP4JHelper {

    public static final String READ_COMMUNITY = "public";
    public static final String WRITE_COMMUNITY = "private";
    public static final int mSNMPVersion = 0; // 0 represents SNMP version=1 
    public static final String TEMP =
            "1.3.6.1.4.1.38783.3.9.0";
    public static final String HUM =
            "1.3.6.1.4.1.38783.3.11.0";
String SNMP_PORT="34455";
private String temp;
private String hum;

    public void readvalues() {
        try {
            String strIPAddress = "192.168.2.252";
            SNMP4JHelper objSNMP = new SNMP4JHelper();
            //objSNMP.snmpSet();  /////////////////////////////////////////// 
            //Set Value=2 to trun OFF UPS OUTLET Group1 
            //Value=1 to trun ON UPS OUTLET Group1 
            ////////////////////////////////////////// 
//            int Value = 2;
//            objSNMP.snmpSet(strIPAddress, WRITE_COMMUNITY,
//                    OID_UPS_OUTLET_GROUP1, Value);
            ////////////////////////////////////////////////////////// 
            //Get Basic state of UPS 
            ///////////////////////////////////////////////////////// 
            String humCap = objSNMP.snmpGet(strIPAddress,
                    READ_COMMUNITY,
                    HUM);
            
            String tempCap = objSNMP.snmpGet(strIPAddress,
                    READ_COMMUNITY,
                    TEMP);
            temp=tempCap;
            hum=humCap;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /* 
     * The following code valid only SNMP version1. This 
     * method is very useful to set a parameter on remote device. 
     */

    public void snmpSet(String strAddress, String community, String strOID, int Value) {
        strAddress = strAddress + "/" + SNMP_PORT;
        Address targetAddress = GenericAddress.parse(strAddress);
        Snmp snmp;
        try {
            TransportMapping transport = new DefaultUdpTransportMapping();
            snmp = new Snmp(transport);
            transport.listen();
            CommunityTarget target = new CommunityTarget();
            target.setCommunity(new OctetString(community));
            target.setAddress(targetAddress);
            target.setRetries(2);
            target.setTimeout(5000);
            target.setVersion(SnmpConstants.version1);
            PDU pdu = new PDU();
            pdu.add(new VariableBinding(new OID(strOID), new Integer32(Value)));
            pdu.setType(PDU.SET);
            ResponseListener listener = new ResponseListener() {
                public void onResponse(ResponseEvent event) {
                    // Always cancel async request when response has been received 
                    // otherwise a memory leak is created! Not canceling a request 
                    // immediately can be useful when sending a request to a broadcast 
                    // address. 
                    ((Snmp) event.getSource()).cancel(event.getRequest(), this);
                    System.out.println(
                
            
            "Set Status is:"+event.getResponse().getErrorStatusText()); 
 } 
 }; 
 snmp.send(pdu, target, null, listener);
            snmp.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /* 
     * The code is valid only SNMP version1. SnmpGet method 
     * return Response for given OID from the Device. 
     */

    public String snmpGet(String strAddress, String community, String strOID) {
        String str = "";
        try {
            OctetString 
                    community2 = new OctetString(community);
            strAddress = strAddress + "/" + SNMP_PORT;
            Address targetaddress = new UdpAddress(strAddress);
            TransportMapping transport = new DefaultUdpTransportMapping();
            transport.listen();
            CommunityTarget comtarget = new CommunityTarget();
            comtarget.setCommunity(community2);
            comtarget.setVersion(SnmpConstants.version1);
            comtarget.setAddress(targetaddress);
            comtarget.setRetries(2);
            comtarget.setTimeout(5000);
            PDU pdu = new PDU();
            ResponseEvent response;
            Snmp snmp;
            //pdu.add(new VariableBinding(new OID(strOID)));
            OID dd=new OID();
            dd.setValue(strOID);
            pdu.add(new VariableBinding(dd));
            pdu.setType(PDU.GET);
            snmp = new Snmp(transport);
            response = snmp.get(pdu, comtarget);
            if (response != null) {
                if (response.getResponse().getErrorStatusText().
                        equalsIgnoreCase("Success")) {
                    PDU pduresponse = response.getResponse();
                    str = pduresponse.getVariableBindings().firstElement().toString();
                    if (str.contains("=")) {
                        int len = str.indexOf("=");
                        str = str.substring(len + 1, str.length());
                    }
                }
            } else {
                System.out.println("Feeling like a TimeOut occured ");
            }
            snmp.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    //    System.out.println("Response=" + str);
        return str;
    }

    /**
     * @return the temp
     */
    public String getTemp() {
        return temp;
    }

    /**
     * @return the hum
     */
    public String getHum() {
        return hum;
    }
}