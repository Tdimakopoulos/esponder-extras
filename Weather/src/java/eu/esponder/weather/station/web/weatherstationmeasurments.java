/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.weather.station.web;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.MeterGaugeChartModel;

/**
 *
 * @author MEOC-WS1
 */
@ManagedBean
@SessionScoped
public class weatherstationmeasurments {

    
//gauge meter    
private MeterGaugeChartModel TemperatureModel;  

//	
private MeterGaugeChartModel evaporation;

//gauge meter	
private MeterGaugeChartModel barometricpressure;

//gauge meter	
private MeterGaugeChartModel windspeed;

//	
private MeterGaugeChartModel winddirection;

//bar model	
private CartesianChartModel windgust;

//gauge meter	
private MeterGaugeChartModel relativehumidity;

//	chart
private CartesianChartModel solarradiation;



//bar model	
private CartesianChartModel rain_gauge;

//	
private MeterGaugeChartModel disdrometer;

//	
private MeterGaugeChartModel transmissometer;

//	
private MeterGaugeChartModel ceilometer;


    /**
     * Creates a new instance of weatherstationmeasurments
     */
    public weatherstationmeasurments() {
    
     List<Number> intervals = new ArrayList<Number>(){{  
            add(10);  
            add(20);  
            add(30);  
            add(70);  
        }};  
  
        TemperatureModel = new MeterGaugeChartModel(30, intervals);
        
        
        List<Number> intervalsbarometricpressure = new ArrayList<Number>(){{  
            add(400);  
            add(600);  
            add(800);  
            add(1000);  
        }};
        
        
        barometricpressure= new MeterGaugeChartModel(760, intervalsbarometricpressure);
        
        List<Number> intervalswindspeed = new ArrayList<Number>(){{  
            add(10);  
            add(20);  
            add(30);  
            add(40);  
        }};
        
        windspeed= new MeterGaugeChartModel(15, intervalswindspeed);
        
         List<Number> intervalrelativehumidity = new ArrayList<Number>(){{  
            add(10);  
            add(30);  
            add(60);  
            add(80);  
        }};
         
        relativehumidity= new MeterGaugeChartModel(40, intervalrelativehumidity);
        
        windgust = new CartesianChartModel();  
  
        ChartSeries gusts = new ChartSeries();  
        gusts.setLabel("Wind Gust");  
  
          
        gusts.set(" ", 90);  
  
       
  
        windgust.addSeries(gusts);  
        
        rain_gauge = new CartesianChartModel();
        
        ChartSeries rain = new ChartSeries();  
        rain.setLabel("Rain");  
  
          
        rain.set(" ", 90);  
  
       
  
        rain_gauge.addSeries(rain);  
        
        solarradiation= new CartesianChartModel();
         ChartSeries sr = new ChartSeries();  
        sr.setLabel("Solar Radiation Spectrum");  
  
          
        sr.set("250", 1);  
  sr.set("500", 2);  
  sr.set("1000", 3);  
  sr.set("1250", 5);  
  sr.set("1500",4);     
  sr.set("1750",4);
  sr.set("2000",5);
        solarradiation.addSeries(sr);
    }

    
    /**
     * @return the TemperatureModel
     */
    public MeterGaugeChartModel getTemperatureModel() {
        return TemperatureModel;
    }

    /**
     * @param TemperatureModel the TemperatureModel to set
     */
    public void setTemperatureModel(MeterGaugeChartModel TemperatureModel) {
        this.TemperatureModel = TemperatureModel;
    }

    /**
     * @return the barometricpressure
     */
    public MeterGaugeChartModel getBarometricpressure() {
        return barometricpressure;
    }

    /**
     * @param barometricpressure the barometricpressure to set
     */
    public void setBarometricpressure(MeterGaugeChartModel barometricpressure) {
        this.barometricpressure = barometricpressure;
    }

    /**
     * @return the windspeed
     */
    public MeterGaugeChartModel getWindspeed() {
        return windspeed;
    }

    /**
     * @param windspeed the windspeed to set
     */
    public void setWindspeed(MeterGaugeChartModel windspeed) {
        this.windspeed = windspeed;
    }

    /**
     * @return the relativehumidity
     */
    public MeterGaugeChartModel getRelativehumidity() {
        return relativehumidity;
    }

    /**
     * @param relativehumidity the relativehumidity to set
     */
    public void setRelativehumidity(MeterGaugeChartModel relativehumidity) {
        this.relativehumidity = relativehumidity;
    }

    /**
     * @return the windgust
     */
    public CartesianChartModel getWindgust() {
        return windgust;
    }

    /**
     * @param windgust the windgust to set
     */
    public void setWindgust(CartesianChartModel windgust) {
        this.windgust = windgust;
    }

    /**
     * @return the rain_gauge
     */
    public CartesianChartModel getRain_gauge() {
        return rain_gauge;
    }

    /**
     * @param rain_gauge the rain_gauge to set
     */
    public void setRain_gauge(CartesianChartModel rain_gauge) {
        this.rain_gauge = rain_gauge;
    }

    /**
     * @return the solarradiation
     */
    public CartesianChartModel getSolarradiation() {
        return solarradiation;
    }

    /**
     * @param solarradiation the solarradiation to set
     */
    public void setSolarradiation(CartesianChartModel solarradiation) {
        this.solarradiation = solarradiation;
    }

   
}
