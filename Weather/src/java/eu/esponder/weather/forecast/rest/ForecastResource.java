/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.weather.forecast.rest;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author MEOC-WS1
 */
@Path("forecast")
public class ForecastResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ForecastResource
     */
    public ForecastResource() {
    }

    /**
     * Retrieves representation of an instance of eu.esponder.weather.forecast.rest.ForecastResource
     * @return an instance of java.lang.String
     */
    @GET
    public String getXml() {
        //TODO return proper representation object
        return "test";
    }

    
}
